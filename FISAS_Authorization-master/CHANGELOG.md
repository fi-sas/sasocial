# [1.36.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.35.3...v1.36.0) (2022-06-08)


### Features

* **authorization:** new accommodation permissions ([2638fec](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2638fec2e24c6ef2a6ca615782ffd8425cda91ed))

## [1.35.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.35.2...v1.35.3) (2022-05-24)


### Bug Fixes

* **auth:** change secure attbr action ([76e4857](https://gitlab.com/fi-sas/FISAS_Authorization/commit/76e485753990607819b09c501601aa14a0f53cf9))

## [1.35.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.35.1...v1.35.2) (2022-05-24)


### Bug Fixes

* **auth:** change cookies samesite config by device type ([6e6846a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6e6846a5a14b9e2fb4a4edb95d7481268accc511))

## [1.35.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.35.0...v1.35.1) (2022-05-06)


### Bug Fixes

* **users:** getUserIds return user_id of resources ([1f4fe95](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1f4fe951891ae99cfc1a09f8c65dd664478872d0))

# [1.35.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.34.0...v1.35.0) (2022-04-19)


### Features

* **volunteering:** add volunteering experiences list and read to all ([a80ae9b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a80ae9bbc67e03790a5d96f675c110e4016d1588))

# [1.34.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.33.0...v1.34.0) (2022-04-13)


### Features

* **tokens:** perform auto clean kiosk and tv tokens every 12 hours ([6413d67](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6413d670d1cfc35f51c8f6dd96cd3a5a4aebf9fe))

# [1.33.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.32.0...v1.33.0) (2022-04-06)


### Bug Fixes

* **authorize:** fix login remainging attempts  response ([ea2c593](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ea2c5936c5a7ff5b61e863d794695515ad6559a2))


### Features

* **login_attempts:** add limit of login attempts ([2745a61](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2745a61afce132d3786eac07f15ddadd2ff6c619))

# [1.32.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.31.0...v1.32.0) (2022-03-29)


### Bug Fixes

* **sasocial:** remove dependency ([8774c54](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8774c54605b77b8791200948cf57b2114eb60f45))


### Features

* add system service and new scopes ([f749209](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f749209329c11e28c1b7b3c1559af175a014820a))

# [1.31.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.30.0...v1.31.0) (2022-03-28)


### Features

* **authorization:** scope residence admin ([150206a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/150206a43196c57813a9d9722867a02ad5bba7bd))

# [1.30.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.29.1...v1.30.0) (2022-02-28)


### Features

* **authorize:** add logout by middleware ([eead241](https://gitlab.com/fi-sas/FISAS_Authorization/commit/eead24146db489112f4a73ac46030066fee4f6fa))

## [1.29.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.29.0...v1.29.1) (2022-02-28)


### Performance Improvements

* remove size of save refresh token ([6543dbe](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6543dbe9a711e8696c54251ac68fcb0f261bea55))
* **scopes:** change get all scopes from user to single query ([6567bb3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6567bb3ffb7a4db7375ab76f4dd84323e6e43d87))

# [1.29.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.28.0...v1.29.0) (2022-02-07)


### Features

* **scope:** new permission for administrator ([662395d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/662395df6bf2eee457f28b2a0ca5ede45f96db7d))

# [1.28.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.27.0...v1.28.0) (2022-01-25)


### Features

* **health:** health configuration permission ([a8188b0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a8188b010362184c787d2f7c061503bdb839aa7e))

# [1.27.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.26.1...v1.27.0) (2022-01-11)


### Features

* address publications - users filter ([a684c95](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a684c95d802a0b75e6b75188fbf07a34a0fb76ba))

## [1.26.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.26.0...v1.26.1) (2021-12-20)


### Bug Fixes

* revert scope remove ([01e8f1e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/01e8f1e0231e6ec4ebe8df620e8f0d6d0598e81a))

# [1.26.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.25.3...v1.26.0) (2021-12-16)


### Features

* **health:** scopes and defaults ([c916d80](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c916d80b292422793a464e254d0ab3cf4bb00cf3))
* **saude:** scopes e defaults saude ([8f9238a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8f9238a4b0a3d14e81b808e8afdfe83427ce9bfe))
* **scopes:** scopes saúde ([dbd62af](https://gitlab.com/fi-sas/FISAS_Authorization/commit/dbd62af7a73880d650ac0f7d89a99c0e9503c1d5))

## [1.25.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.25.2...v1.25.3) (2021-12-14)


### Bug Fixes

* **seeds:** disable remove defaults scopes ([31b0497](https://gitlab.com/fi-sas/FISAS_Authorization/commit/31b04974bee89036ca6f18255a51d2faadceb354))

## [1.25.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.25.1...v1.25.2) (2021-12-14)


### Bug Fixes

* remove bugs ([c597cf4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c597cf48644d05d14dfc97d4039102a81c3d0ccf))

## [1.25.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.25.0...v1.25.1) (2021-12-10)


### Bug Fixes

* **users:** change  validation type ([495ee87](https://gitlab.com/fi-sas/FISAS_Authorization/commit/495ee87b15992f3dacd5db21bef58c2834e57596))

# [1.25.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.24.0...v1.25.0) (2021-12-06)


### Features

* address publications, new filters correction ([b66792d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b66792d5130f4e564f35e3b67759f1c252e17746))

# [1.24.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.23.2...v1.24.0) (2021-12-03)


### Features

* address publications, new filters ([68b3a57](https://gitlab.com/fi-sas/FISAS_Authorization/commit/68b3a57cb5b68cc9f0aad06393f26fb07fe5f801))

## [1.23.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.23.1...v1.23.2) (2021-12-02)


### Bug Fixes

* **scopes:** change cache keys of userHasScope ([e711346](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e7113464bfd2f009127247dd442d393fac1d962f))

## [1.23.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.23.0...v1.23.1) (2021-11-30)


### Bug Fixes

* **users:** add updated_at on patch ([f76e20a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f76e20a99b581d59e7ab162d634f030a925d749d))

# [1.23.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.22.1...v1.23.0) (2021-11-12)


### Features

* **users:** add blocked fields on users ([a8c62c3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a8c62c3fac189f9b1035f265f11fa439ef44c21d))

## [1.22.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.22.0...v1.22.1) (2021-11-11)


### Bug Fixes

* **users:** random pin of logged user ([5c570d2](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5c570d248ff28d320c8bc307288dcf545cabf26f))

# [1.22.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.21.1...v1.22.0) (2021-11-05)


### Features

* addressed publications ([a3704cb](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a3704cb67498106d1e670e2c4f7d769ed8e97829))

## [1.21.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.21.0...v1.21.1) (2021-11-03)


### Bug Fixes

* **users:** add email to validation ([c234ddc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c234ddcb71bcff149f349cc10f25e29e1f5b9d3b))

# [1.21.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.20.0...v1.21.0) (2021-11-03)


### Features

* **users:** sennd random pin now support email ([aeeafe0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/aeeafe0ab10e03024effda81755cf8da7c28099c))

# [1.20.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.19.1...v1.20.0) (2021-10-26)


### Features

* **user:** add is_final_consumer field ([0770c69](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0770c6908e76cf96b445ace3afa58a4a5fffd12d))

## [1.19.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.19.0...v1.19.1) (2021-09-27)


### Bug Fixes

* add log error on sso_acs ([88efe0c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/88efe0cda732101d16dd6e29beeec445b07b8836))

# [1.19.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.18.0...v1.19.0) (2021-09-17)


### Features

* **users:** sendRandomPin with user_name ([a22415a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a22415a88d00e1c2234b7ad9fa2188e4fb1273cb))

# [1.18.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.17.1...v1.18.0) (2021-09-07)


### Features

* **seed:** Externo Saude Seed ([d191886](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d191886b14fa2743591f2509b80d88f3b14b3fdb))

## [1.17.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.17.0...v1.17.1) (2021-09-06)


### Bug Fixes

* **users:** add tin size 24 and disable student_number validation ([3e670b5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3e670b54aaab54049b9a7e1623c6eb4d8e4ba83e))

# [1.17.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.16.1...v1.17.0) (2021-08-30)


### Bug Fixes

* **scopes:** update scopes ms calendar ([9e0a9d9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9e0a9d978c524c491452ca59e2c9591b84eb55ce))


### Features

* **scopes:** add calendar scopes ([e4391e4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e4391e436355ca0406ebf52fe7f77d69840bd1f9))

## [1.16.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.16.0...v1.16.1) (2021-08-24)


### Bug Fixes

* **users:** randomPin endpoint get salt of user ([74ca1f0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/74ca1f04cca61d831da2abc98ae33a12b5ce58c8))

# [1.16.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.15.0...v1.16.0) (2021-08-23)


### Bug Fixes

* change  language of scopes and create a user group for health ([828cd87](https://gitlab.com/fi-sas/FISAS_Authorization/commit/828cd87d37d55ee35e400318f3e16928252473a2))
* emergency_fund conflict ([38f4b31](https://gitlab.com/fi-sas/FISAS_Authorization/commit/38f4b3112af83e27dbc9656960105e70bb6e67c1))


### Features

* medical-health appointment service ([3d82d2f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3d82d2f208b32ed78a8d5ddca6f1d4d7484663af))

# [1.15.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.14.0...v1.15.0) (2021-08-09)

### Bug Fixes

- **users:** remove await, async e ctx ([bc363ee](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bc363eeaa41d556b0e22b582becc26883eff5654))

### Features

- **users:** add endpoint for count users_by_profile ([adc86f6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/adc86f66687b8a623c9b5d9c4fd5234dd411a364))

# [1.14.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.13.1...v1.14.0) (2021-08-06)

### Features

- **users:** add endpoint to return randomPin to user ([bca73cd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bca73cd1793d8c3a1140bfbbb6aa02308a4b5095))

## [1.13.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.13.0...v1.13.1) (2021-08-06)

### Bug Fixes

- **authorize:** add showCPUID to cache keys on validate token ([f28798e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f28798eca747d29a3bcf1f39bfb60f6ac3fcef6a))

# [1.13.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.12.0...v1.13.0) (2021-08-05)

### Features

- **users:** send verification email ([04aa89b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/04aa89b6858c8d8bf4da02f9eaffe18481dc35af))

# [1.12.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.5...v1.12.0) (2021-08-05)

### Features

- **scopes:** add scopes ms monitoring ([8d8ed73](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8d8ed73cb2963b5b5786c36fdc1e2f9b985746bb))

## [1.11.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.4...v1.11.5) (2021-07-30)

### Bug Fixes

- **users:** validation errors types ([9f449d4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9f449d4de874411acb18c825a77d1ce1ef028004))

## [1.11.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.3...v1.11.4) (2021-07-28)

### Bug Fixes

- **authorize:** clear cache on device update ([16bf3d9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/16bf3d902b486472a5b6fd3ac96721691f70d59f))

## [1.11.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.2...v1.11.3) (2021-07-28)

### Bug Fixes

- **authorize:** remove showCPUID from endpoint ([376eb5f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/376eb5f0419659f73b6e2052b9c5fe0e0e37ac18))

## [1.11.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.1...v1.11.2) (2021-07-28)

### Bug Fixes

- **authorize:** get cpuid to meta ([73f349a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/73f349a909239ebdbaae610d52499dbac8e8fb96))

## [1.11.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.11.0...v1.11.1) (2021-07-27)

### Bug Fixes

- add scope is_employee to meta ([1985d35](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1985d35b04932912bd7d182eefe21b9f02571161))

# [1.11.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.10.2...v1.11.0) (2021-07-27)

### Features

- **scopes:** add action to check if user has scope ([9e6ac4c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9e6ac4ce7b24d35b840be22d6b7b085a58a3ea07))

## [1.10.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.10.1...v1.10.2) (2021-07-23)

### Bug Fixes

- **users:** bad ValidationError expection and fix validations before created ([0e2e10f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0e2e10fbc946847e26e491b596d3b824f1fedb86))
- **users:** bad ValidationError expection and fix validations before created ([daeeb01](https://gitlab.com/fi-sas/FISAS_Authorization/commit/daeeb01bd77a286ac00919e9ff247b48e6884e13))

## [1.10.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.10.0...v1.10.1) (2021-07-08)

### Bug Fixes

- **various:** correct bad error key ([dc32910](https://gitlab.com/fi-sas/FISAS_Authorization/commit/dc32910ee7a43ff811a7adbe72992f7fecf971c7))

# [1.10.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.9.2...v1.10.0) (2021-07-02)

### Bug Fixes

- **targets:** change count endpoint to target service ([073e2b2](https://gitlab.com/fi-sas/FISAS_Authorization/commit/073e2b274a7a80f99afebe130a9058fde669c194))

### Features

- **users:** count users by target conditions ([c59a578](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c59a578c1f2d673b69e14fafac80cd23fcf5bd30))

## [1.9.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.9.1...v1.9.2) (2021-06-28)

### Bug Fixes

- **users:** change identification max size to 50 ([8271484](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8271484bdb434c77dbc2415b299c5feca5c813c8))

## [1.9.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.9.0...v1.9.1) (2021-06-24)

### Bug Fixes

- **users:** change student_number regex ([0d4ac9f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0d4ac9fe657bf3af68754c19b2b640411783a241))

# [1.9.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.6...v1.9.0) (2021-06-21)

### Features

- **users:** add nationality field to users ([063ecb4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/063ecb449ee56290fbbb0381a05d7510c76cb0d8))

## [1.8.6](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.5...v1.8.6) (2021-06-17)

### Bug Fixes

- **authorize:** add path / to SSO Cookies ([3dcf644](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3dcf644bf26809ff36da2a93240c48199b4ddeb4))

## [1.8.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.4...v1.8.5) (2021-06-11)

### Bug Fixes

- **users:** add admission date to user create action ([3b3fb59](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3b3fb5934914e4fc3f0a55314a666f32eb0905d5))
- **users:** add course_year to create action ([0810b20](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0810b2064fc7b20d6d37c71f02276a6357eee9e2))

## [1.8.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.3...v1.8.4) (2021-06-08)

### Bug Fixes

- **sso:** add catch and redirect to error page ([88e74a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/88e74a9bd1eff9d2e53d3e0bc8cbd8b1d6d75b36))

## [1.8.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.2...v1.8.3) (2021-06-01)

### Bug Fixes

- **users:** add document_type_id to PUT ([38a7196](https://gitlab.com/fi-sas/FISAS_Authorization/commit/38a71966fa1b7e31ee173e672aad23c13e301ed8))

## [1.8.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.1...v1.8.2) (2021-05-28)

### Bug Fixes

- **document-types:** change external_ref max size ([19ac6b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/19ac6b66eeb87b208840d1c0114f1dfd558912d3))

## [1.8.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.8.0...v1.8.1) (2021-05-27)

### Bug Fixes

- **authorization:** cast setting from env ([eddd085](https://gitlab.com/fi-sas/FISAS_Authorization/commit/eddd08522abb3a20b2b014f131d82296848382f4))

# [1.8.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.7.0...v1.8.0) (2021-05-27)

### Features

- **accommodation:** add scopes dependencies ([9baaf10](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9baaf109040fbf27056a0d65f3f92a77c6c703b5))

# [1.7.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.5...v1.7.0) (2021-05-25)

### Features

- **users:** add change pin endpoint ([b4edd1a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b4edd1af156a28a4767481d6108b1c3c57eeda95))

## [1.6.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.4...v1.6.5) (2021-05-25)

### Bug Fixes

- **authorize:** fix set-cookies sso-ack ([02498ee](https://gitlab.com/fi-sas/FISAS_Authorization/commit/02498ee35717a835eb5c952bcf2ff22ac18a3b30))

## [1.6.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.3...v1.6.4) (2021-05-24)

### Bug Fixes

- **users:** remove requireds from patch ([53f16f5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/53f16f5f0f0d1113b1dfa8c6189bacf015f49e19))

## [1.6.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.2...v1.6.3) (2021-05-18)

### Bug Fixes

- **user_group_scopes:** allow multiple user_groups per user ([a39d8aa](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a39d8aa8aa665c5900ba34bda0351cbf0034a9ea))

## [1.6.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.1...v1.6.2) (2021-05-14)

### Bug Fixes

- **scopes:** clear cache on profile event ([8b8898e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8b8898ec51f84b054ba63213cbc83e6f2656acbd))

## [1.6.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.6.0...v1.6.1) (2021-05-10)

### Bug Fixes

- **users:** add document type validation to users ([5f8f30b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5f8f30bd34660d51d0f23be9eadaf24b00189a43))

# [1.6.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.5.1...v1.6.0) (2021-05-04)

### Bug Fixes

- **document_type:** fix error on remove ([2aedfe5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2aedfe539897ba7f5e9e8acd82be4673e0f5118b))
- **targets:** add targets and target_conditions ([2d89e86](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2d89e86123ab51a957f943cd1bb117911c457e69))
- **targets:** remove targets implementations ([5d390dd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5d390dd63bc3264db35766f0104eb98cb767c5a1))
- **users:** add department_id to update ([8eb2834](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8eb28345e546050faed862f07dc9338b41a0664c))
- **users:** add department_id to user ([a374241](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a37424110277629d835f3ccbcbfbece83909365e))
- **users:** add optional to section_id field ([55708e7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/55708e7af5539fd13a24b4449bf7344cd0ddd139))
- **users:** fix active filter in get users by target_id ([972fbcd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/972fbcdadc0cd3106f7ac05d5be72a1f252346d7))

### Features

- **sections:** add departments and sections ([85adda8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/85adda800e9a91cda6f589d3a86bb3fd593a3061))
- **users:** get user ids's by target_id ([f4854f4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f4854f40a1112bc23f34836ea86187415677726c))

## [1.5.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.5.0...v1.5.1) (2021-05-03)

### Bug Fixes

- **users:** re public patch action to middlewares ([1da6141](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1da6141f69f370433bab931cb3d963c2de35d0e9))

# [1.5.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.7...v1.5.0) (2021-05-03)

### Features

- **cocument_types:** add external ref ([dc13cee](https://gitlab.com/fi-sas/FISAS_Authorization/commit/dc13ceef1a3d327873523d2e172c9330a39dcd63))

## [1.4.7](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.6...v1.4.7) (2021-05-03)

### Bug Fixes

- **users:** change user_name limit to 100 chars ([c7367cb](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c7367cb6b50864d76327be7848f512326fdc7c1f))

## [1.4.6](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.5...v1.4.6) (2021-05-03)

### Bug Fixes

- **users:** remove pattern from user_name ([863000d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/863000dbe0159cc68081838c9832880dfbb709ad))

## [1.4.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.4...v1.4.5) (2021-04-30)

### Bug Fixes

- **document_type:** change docuemnt type to table ([b258d55](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b258d5587088542ab028af3f79f160e1a7290381))

## [1.4.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.3...v1.4.4) (2021-04-29)

### Bug Fixes

- **users:** fix on create account verification email dont send errors ([1b4553f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1b4553f3a35088d2cba3d0da915c45042986b7a5))

## [1.4.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.2...v1.4.3) (2021-04-28)

### Bug Fixes

- **authorize:** add entityID to sso/url endpoint ([90d4a25](https://gitlab.com/fi-sas/FISAS_Authorization/commit/90d4a25392753393c1bab4bc9315b1309c57f407))

## [1.4.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.1...v1.4.2) (2021-04-27)

### Bug Fixes

- **authorize:** fix pin password validation ([1a25f87](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1a25f875a05a7f3c1e9fdaec3bb385990f1d50a8))

## [1.4.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.4.0...v1.4.1) (2021-04-27)

### Bug Fixes

- add some cascade logic to user scopes relation ([6247deb](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6247debb92cc4b36c122c8723a8ae21d9b63008d))

# [1.4.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.3.0...v1.4.0) (2021-04-26)

### Features

- **users:** add new password validations and verification ([a68d938](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a68d93897d0597159d2e230ec41b179adfddd17b))

# [1.3.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.2.0...v1.3.0) (2021-04-23)

### Features

- **users:** add admission_date to user ([e7c86ce](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e7c86ce525b7a358529e2c5a4d825fb3c1ce6762))

# [1.2.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.1.1...v1.2.0) (2021-04-19)

### Features

- **users:** add identification document type to users ([e30efd3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e30efd361fd467da026bd96113fdbabf89d8ec1f))

## [1.1.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.1.0...v1.1.1) (2021-04-12)

### Bug Fixes

- add various validation for deleting and cascading ([744d9f7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/744d9f7a8626124ee9f895d9e2ff5ea266dbeff5))

# [1.1.0](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.5...v1.1.0) (2021-04-08)

### Features

- **users:** add usrer sourced by and source name fields ([1338de2](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1338de23d03f2926c54e7349d671ff60501643d6))

## [1.0.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.4...v1.0.5) (2021-03-30)

### Bug Fixes

- **scopes:** fetchScopesByProfile withRealted false ([f369ae3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f369ae3bf64d7d467b7487a828ed6f9371e5f9db))
- **user_scopes:** reove withRelateds from fetch ([d22fa78](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d22fa7802ab4992cc4210557dcacc1ff4a7924da))

## [1.0.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.3...v1.0.4) (2021-03-26)

### Bug Fixes

- **scopes:** add grants to scopes of user ([ba76089](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ba760898b1f6b16c1d15884d20b54f6dcd8671cd))

## [1.0.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.2...v1.0.3) (2021-03-25)

### Bug Fixes

- **seeds:** current_account scopes ([ce31105](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ce311055d558a6bed2bb57c76eeda39cf6b2fd18))

## [1.0.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.1...v1.0.2) (2021-03-19)

### Bug Fixes

- **auth:** sso by backoffice ([91a5bcb](https://gitlab.com/fi-sas/FISAS_Authorization/commit/91a5bcb0463389743096ad36e105daf119428294))
- **auth sso:** add student number when create user ([f076626](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f0766262ddf60076cbe1599c0f8120ff44812727))
- **auth sso:** remove employed number ([063fb05](https://gitlab.com/fi-sas/FISAS_Authorization/commit/063fb05fc20f1f2c761a53b03900ae62e6c70f85))
- **auth-sso:** auth user for sso fixed ([a663125](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a663125301525e23acac2d3f89d1342291ba7cea))
- **auth-sso:** get device by body ([3f15e68](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3f15e682a7ed2e204e80c64012a4b11cd681c9c7))
- **core:** serach and searchFeilds methods ([715a18e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/715a18e7b36b172d41cb63372e21319533b8087e))
- **sso-auth:** set device in generate token for saml ([825f707](https://gitlab.com/fi-sas/FISAS_Authorization/commit/825f70769fe62705de3b548648916f0613013bb0))
- **user:** allow null in student number ([f8410e3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f8410e37673d8af610920e568f8fa567ef8169a8))
- **user:** birth date turn to opcional ([2416dfa](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2416dfa161e73b195302a89287db9efa27bb54f6))
- **user:** remove max char in identification ([aaf38b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/aaf38b61e22adbae30cb34b82b88de6cd9c5332f))
- **users:** add 'U' gender ([b8cd237](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b8cd237529b5a8735ca018d4752f790cf71778c5))
- **users:** dont restart first login user on update ([038aea7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/038aea7c7b020f24dd2018fdc62ad0278e223282))
- **users:** fix internal update ([2acf3f9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2acf3f9561146c2e7ac152021f6c1bbb05223c72))
- **users:** fix users can_change_bo optional ([b56f163](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b56f163d76aa5c1124df153241c3f266a9a7a7a0))
- allow create user with null username and/or email @ user ([297e30b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/297e30b43f3888371bba1c0c13a16811414e8171))

## [1.0.1](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0...v1.0.1) (2021-03-19)

### Bug Fixes

- **users:** fix checkPin method ([cb578b5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cb578b541553af66ee109d02c16a98bd3977fd2c))

# 1.0.0 (2021-03-08)

### Bug Fixes

- **authorize:** fix refresh-token return isguest always false ([d6b49a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d6b49a98159b73c855c95d3c5b257945e3570f14))
- **authorize:** metada return XML content type ([3d845d0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3d845d02fb7af65e32ac7e3a091a1a3759bb7941))
- add convert in fields ([a396046](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a396046c2d6771b17e947bc292a782d5f8d12ea6))
- endpoint update-me to receive null data ([50fd0d6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/50fd0d6aa3a7ca788e805f75bdd36a681241138c))
- fix email and user_name validation on creation ([a908763](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a908763f7842bb542591b68590383723cccbdec9))
- nullable validation ([b3cf306](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b3cf3068763cd679382d5b599e44f4130053e773))
- optional and nullable update-me ([e72f658](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e72f658a1e0f83db67b3b9f86ea2fa61088de318))
- **authorize:** add create user from sso ([c12790d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c12790df4da36f805d517b2f1190677b0f19bdb6))
- **authorize:** declare token algorithm ([88700be](https://gitlab.com/fi-sas/FISAS_Authorization/commit/88700be5b24d587cbc7213328f86f5b152978915))
- **authorize:** fix create user from sso ([b94821a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b94821a8393a94b2506ad92fd663262558eca2c7))
- **authorize:** fix erro codes of untorized and forbidden type ([fa97549](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fa97549af0b75db44256102f775f1e5699ad018e))
- **authorize:** fix validate token response ([3f4ae7e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3f4ae7edece828e06bbadfc573cd295ff846dd7e))
- **authorize:** move create and update user to sso ([7e20c55](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7e20c558b3f04040f6487a4ca6ac1fb0113a14bd))
- **authorize:** normalize email and user_name field ([9a6193c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9a6193c0316101f712a0fe9f239fc6b920c5d10a))
- **authorize:** resolve cookie overwrite ([6dca2ac](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6dca2ac9da58c1d0d526b5c7e55733b5132a334e))
- **authorize:** SSO validate if user has acess to BO ([7380df6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7380df65187d556a804eff2dc714482d3d3af332))
- actions visibility changed to published ([41a3d64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/41a3d64afd0addeed91191cdc104b7e5cfc08d65))
- add new env vars to SSO and Middlware ([d7be9f3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d7be9f3fdcd77bc534eb75b36ed778d158065a70))
- fixing node version (node:12-buster-slim) ([8d60c9c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8d60c9c3cf29c24ef6567894492c6ec9548ebd77))
- minor fixs ([5b1fb57](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5b1fb5728ab9bbe77a487d724f9cc37c20cf680c))
- remove unused logs ([583822d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/583822d3e3e17b0c63ea73f92732cfd0364c6715))
- sso ack token ([1fac8be](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1fac8beb07bcbeb8ef2ab996c0d610cc8f841c1b))
- update table user and update endpoint 'update-me' ([01e25dc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/01e25dc01ac2c1d9a2e291b738e293e5ac6dbc96))
- **auth:** validate tokenInfo.device array ([24e6257](https://gitlab.com/fi-sas/FISAS_Authorization/commit/24e62578bfce3801a2dec95025674ea13b71c85c))
- **authorize:** fix guest user login ([f6d332d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f6d332de17e054a7e63799e39a2552afb70f05b8))
- **authorize:** remoe scopes and sensible info from token ([a9b8caf](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a9b8caf31e6c448ef6625b0fb168a8eb41680771))
- **authorize:** use middleware to false ([07ec83c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07ec83c8e28d0f1f1bc8431b513520d6df952969))
- **dotenv:** fix configuration path ([e7f26ba](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e7f26ba5421bd956b8c2bfefbd97ffe33850349d))
- **knex:** add knex pool to fix error connection ([adac620](https://gitlab.com/fi-sas/FISAS_Authorization/commit/adac62067f12776cb0be44900dfea4c99160e8a4))
- **knex:** fix knex file ([e67a091](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e67a0913cee46d7d63e9ee8952d332cc2cc7a74c))
- **ms_core:** update ms_core package ([20308ef](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20308ef4ab7231d85151774d41d86bba688ac37a))
- **profiles:** save the scopes ids from create and update ([41f0533](https://gitlab.com/fi-sas/FISAS_Authorization/commit/41f05331ffae0614a51fb0d438902806f934f03f))
- **scopes:** add alert-templates to the scopes ([75ee5da](https://gitlab.com/fi-sas/FISAS_Authorization/commit/75ee5dad48fa82341b2f89b1e50d9a5049f7b26f))
- **scopes:** remove infity cicly from withRelated ([5072656](https://gitlab.com/fi-sas/FISAS_Authorization/commit/507265624b440190ce608135b7e21c32503771b1))
- **scopes:** update services names ([da1974b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/da1974b08ccf39a3f02c9ded376b6f0528cf87f0))
- **seed:** fix insert 01_external_target ([84b41c1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/84b41c155971ce07d1c858fbbb078da31ca2dcc8))
- **seeds:** add new scopes and fiix a seed ([8ca7acd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8ca7acdc67c36edb96351f938f50c95b9bd967f6))
- **seeds:** change email domain to [@sasocial](https://gitlab.com/sasocial).pt ([1260222](https://gitlab.com/fi-sas/FISAS_Authorization/commit/12602220308f3ab5fc22f01e74a2a56c1abad039))
- **seeds:** set admin to userGroup ([3e8a28a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3e8a28af9a4fc95da8b808b232da0d3133b445d0))
- **semantic-release:** changed test branch ([9ba7215](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9ba721544c37092fef94461a7b5d0ae323c34885))
- **semantic-release:** fixed semantic-release branch syntax ([4b306a0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4b306a0811b73aa43798139efac8145193c1e70c))
- **users:** add 'U' gender ([1bd0d24](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1bd0d24270dcab6704885d1c2ae7fc16d4e41749))
- **users:** fix user create ([2c11cdc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2c11cdc95a581b8bd21dd4f3112c347df6a35f79))
- **users:** fix user fields and relations ([9dc3814](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9dc38143ea8287c44ef0d998c0f0a4a648729f87))
- **users:** minor fixs ([eec25ea](https://gitlab.com/fi-sas/FISAS_Authorization/commit/eec25ea12a917a9b08f3769f7ca2a93fea09b7c2))
- **users:** normalize email on creation ([468f862](https://gitlab.com/fi-sas/FISAS_Authorization/commit/468f862eabd491da3f058b9d0eeae1c1d092b50b))
- validate token return isGuest ([3a316c9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3a316c9cdbd528d9e1a72b6d3052e1e55079adbc))
- **api-composer-validations:** Fix error when validating incomplete data - PATCH constant problem ([2ff4cc5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2ff4cc5756133365ce2964d2ae8ae0142ff3f488))
- **api-validations:** Api validations only using defined and not null values ([19d20cc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/19d20ccdb5bbc0513c0835bed984ee890877018b))
- **apiComposerValidation:** Fix of unhandled promises and sent headers errors on validation ([f2477ef](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f2477ef5edba5a24242db87cbeb3d3874974369a))
- **auth:** add logs for testing ([d05d318](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d05d318ea6188aefce01013710496df9b7b17067))
- **auth:** add logs to sso ([221f61a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/221f61adbb2a344d9b83cf86c4d8f664f36059b6))
- **auth:** add redirect to erro page ([a243158](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a2431587708e933abfcda93de0fe2aa0a4df8746))
- **auth:** add redirect to erro page ([d008f8d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d008f8db84a643339063bea9f89ab528821d42a0))
- **auth:** BadRequest with invalid credentials error when asking for token ([4e9207c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4e9207cbd441971ca57d402960cbd22263f4a8cd))
- **auth:** change sso email field ([59ed3e1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/59ed3e1672fcd481e9b5043a2db3896b7848ae8e))
- **auth:** change sso email field ([c456323](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c45632378b743a73962f1b358c7c9949d408de72))
- **auth:** fix auth user creation on SSO new user ([95f1445](https://gitlab.com/fi-sas/FISAS_Authorization/commit/95f14453e97c61fe51d8da3a2f85032b233442b8))
- **auth:** fix error 500 in case of no metadata ([5f689e8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5f689e8e75b57d87129391eef27a47acd887e8cd))
- **auth:** fix sso auth user creation ([b380758](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b380758a3e847990b45d3f5d4675b5f55976c1dc))
- **auth:** remove logs from sso ([58bf346](https://gitlab.com/fi-sas/FISAS_Authorization/commit/58bf346bf219ac9fdcae3078db3a492ebeb0914b))
- **auth:** token expire for testing ([aa9bf64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/aa9bf64c16dba1ae90daf8883e00c440458d4737))
- **can-change-password:** add can-change-password in users ([acdc0ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/acdc0ae6e16302c58b95fa990dc47da40f97637c))
- **confirm_token:** change param token to token validate ([1f8c251](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1f8c25133a52fa49e2e42242c8559ec47ed28066))
- **core:** fix temporarily the query method ([8674f64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8674f64168dd1fc68e2a7ec38f488e2f2b5f9ef7))
- **doc:** Small fix on common documentation ([32d4622](https://gitlab.com/fi-sas/FISAS_Authorization/commit/32d4622ee287ba8355667a1042f585e8d467a85f))
- **docker-compose:** add dev and prod composers ([ab4a4ad](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ab4a4adaa8fffa0f2288c6d70fdce0d9524921fb))
- **docker-compose:** create docker-compose to prod and dev ([1fb68e7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1fb68e7ea13189336360ca34ebb7fa9d8bc4f801))
- **env:** add new path for new pin ans new password ([760ea28](https://gitlab.com/fi-sas/FISAS_Authorization/commit/760ea28002beb525aed1283c10e32edc8d0fa90f))
- **env:** change env ([1bf8645](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1bf86458cfadb0d739000f73bef234f9c719be1f))
- **env:** fix new pin and password urls ([43a60dd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/43a60ddc839174a549aefcdcd4ebcbf011fd8e4c))
- **exemple.env:** fix confirm pin url ([d4cbbca](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d4cbbca0d49bd58687595e65753f6ae629273569))
- **gitignore:** add .idea and .vscode ([fe1fe1c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fe1fe1c242fd765f5d1ede0a313646d808112044))
- **loadMedias:** Missing media in single data ([bb9d5b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bb9d5b6dcaaafd14f2c39965ebfb76f25f185a75))
- **middlewares:** Docs ([d255048](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d255048035a74c292e68bc8fc52a78ab7da60ce8))
- **migrations:** remove '201901071146_rename_profiles_targets' ([6e5fa0e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6e5fa0e6d9218eb02520c13b42ec668e3b15ae3d))
- **mock:** Mock for got package returning promises ([174d87c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/174d87c79da06d396e739bcdd8d04ac690b5f260))
- **new-pin:** add validate token ([643f799](https://gitlab.com/fi-sas/FISAS_Authorization/commit/643f799a30eaa60f37bd13a1b1ad490f21a9f3d2))
- **pin:** reset and confirm pin ([d59e4f5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d59e4f5ecaa68a3554f75c49dc0ece52f65ecfa9))
- **readme:** add new annotations ([239b89d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/239b89d829dc8fa147cdfb0bbd321619f7276d08))
- **reset-pin:** change token to token validate ([636f012](https://gitlab.com/fi-sas/FISAS_Authorization/commit/636f012413eff99195d0883d64dc42414f7e92ae))
- **reset-pin:** fix errors ([0453f8b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0453f8b608d10bfbeb7e901dd752441daef6d154))
- **reset-pin:** fix url to validate pin ([9901f19](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9901f19285ba6c567a4b0f0bc049ca8fefc07dd2))
- **run-seeds:** Ordering ([0abc928](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0abc928e4204f2ab17499860e33cd84d59432f22))
- **users:** change update me to PATCH ([55da396](https://gitlab.com/fi-sas/FISAS_Authorization/commit/55da396f711dbb5bc60f9aff365c32c68fb24f04))
- **users:** change update me to POST method ([3283a10](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3283a104d06b56027700e7413f4ab8ab0c2b857b))
- **users:** fix noDefaults on patch ([709a3cc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/709a3cc7ff8f87e3fb44755107687ee464766bf5))
- **users:** fix update.me endpoint ([89ea73d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/89ea73d81de47cc1207ecb9fc5eb0785d94e62fa))
- **users:** fix users relateds ([ce0ba86](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ce0ba866b99e29e4c7ff2bbd9532087449b14897))
- **users:** log info from token ([7ca0341](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7ca03419ba39dc51b34506ee981cfc461daeeb2b))
- **users:** log info from token ([504f453](https://gitlab.com/fi-sas/FISAS_Authorization/commit/504f4531efcdbb84bfa7b04bcc598626921466bb))
- **users:** remove can_change_password required ([cc94e79](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cc94e79f0e480fe70e53c430971860b3140dd02d))
- **users:** remove default with relateds to soolve timeouts ([51db0a1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/51db0a1a120c241e56c1d0f7d1e97f8923608319))
- **users:** update me fix deleting all the permissions ([4204b9b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4204b9bb76b770b8498ce5ab98983728dc9874b8))
- **users:** update me fix deleting all the permissions ([cb6fd30](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cb6fd30f6e1835f7d661b38f26caafe0f65de958))
- **users_scopes:** rename users scoeps on users relateds ([9370d4a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9370d4a10ac9e05e71b2e529959f0afd19efec35))
- broken RFID auth ([cb75c8b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cb75c8bbda9d9f15787e7a48c7c7707c7782f965))
- broken run seed ([7e5d0df](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7e5d0dfc24b0ad37dc12c5d378a53213ce8c9536))
- **withRelated:** Fixed handling of withRelated parameter ([3afed46](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3afed46084eb7046788a63a648c372ddccea4f2b))
- Fixed typos on common tests ([d8e7f98](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d8e7f98ef763041b811b85381ad1a21c34cc1dc2))
- migration gender ([d80d8e3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d80d8e3fcb863e127b34069abf40a0cb27864cd2))

### Features

- add migration file new user fields ([d56b2a5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d56b2a55a3476bb929e93e6b87e34bd43b59e29a))
- add seed to external profiles ([709d394](https://gitlab.com/fi-sas/FISAS_Authorization/commit/709d394d90b902158ef20787ac31262c6a7ab1d7))
- add withrelateds and change update-me ([77573c9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/77573c9215aa2d567488b7ac73e702584612d6ab))
- seed exaluno ([efd2a77](https://gitlab.com/fi-sas/FISAS_Authorization/commit/efd2a773f20fdc4c70fc9b49b00be621767de6f1))
- **auth:** change auth method to refresh token ([795db16](https://gitlab.com/fi-sas/FISAS_Authorization/commit/795db1638168944fb956077fbf8d0f671b212e49))
- **authorize:** add action return the logged user ([11b3149](https://gitlab.com/fi-sas/FISAS_Authorization/commit/11b3149d1ef6f1b4df53ecc9a24a7dcc214a4c04))
- **authorize:** add initial endpoints to SSO ([c9c5197](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c9c5197dcfe974a61b9512e4351fe737ce529b0e))
- **authorize:** clear cache on scopes change ([e4cd209](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e4cd209618cf1a4612a7370c50f7f5e4ae68482c))
- **change-password:** Require previous password on change password endpoint ([f7a267f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f7a267fc2981751ad28f10c61edc1f97298b242f))
- **error-codes:** Add error code for invalid operation (use with business and model design logic) ([6d4a26c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6d4a26c74960c9623ba55b1bded7ec139fe09242))
- **load-related:** Helper with loadRelated function ([f039020](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f039020b3ff7d5f4be491124e7b003f49dee370b))
- **migration:** add external_ref in profile ([b916fdc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b916fdc8d0319bd308a7d9f836f426147df7c67f))
- **profile:** add validation for external ref ([e52af9c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e52af9c5166d19cb59f0f42d495748bb66efbe87))
- **profiles:** Run seed for profile of private accommodation MS ([7ad6e23](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7ad6e2363c073074ba9bc2bce9386616d4f9e5f9))
- **request-parameters:** Add possibility of remove filter value in req.parameters.getFilterValue function ([214f3a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/214f3a9ea6f3abd3b97791e70df34e4621ffcdfa))
- **reset-password:** reset password and change ([4e11e7e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4e11e7ebf58793028cd39e0821b7a6bf47ef28ac))
- **reset-pin:** reset pin ([f23d681](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f23d681cd9cd15a24c4721c0b4bd7efe149f811a))
- **scope-permissions:** New internal request detection middleware. Scope permission given to internal requests ([1cc9075](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1cc9075b67db1a0e8d1a391c079b1bb23e08b6c7))
- **scopes:** Remove scopes validation middleware and usage inside MS ([02355b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/02355b633686d1de885a5cc293e671f19f89e5f0))
- **search:** add new method for query from url ([c8fbea8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c8fbea8b859d28befb5a07c68bb61b39a56beeb8))
- **search:** query search fileds ([35a0970](https://gitlab.com/fi-sas/FISAS_Authorization/commit/35a0970f3bfa34b488f93c51e79bbaadc5d6176e))
- **seeds:** add new seeds for staff and faculty ([1a554d1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1a554d1def2bca0efcbabaf84784d19b5a55fb6a))
- **seeds:** separate seeds from sample_seeds ([e191fc7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e191fc7d32f7064151846cf7ad306fcead3125d0))
- **services:** add main services of auth ([e137055](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e137055f12e380c6ce05236be82083b530f76a5a))
- **soo:** add update data from sso ([b437fd1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b437fd14bcc93fda6998ed4a40839940d9f2f137))
- **spec:** Common parameter of ID filter specification ([5d227f1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5d227f1202f2bab270d27d1f8f4dcc6bf91bd459))
- **users:** add birth date to the user update me ([603c954](https://gitlab.com/fi-sas/FISAS_Authorization/commit/603c95409cf0489b9c6b1ba7456a535531cdaf25))
- **users:** add first login endpoint ([ec1efb9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ec1efb904505695abd1bb35a5139f74b94712691))
- add address fields to the user ([8620267](https://gitlab.com/fi-sas/FISAS_Authorization/commit/86202673f73f88b8f8115c513ddb7e1eb7c48b83))
- add column to allow global openness of scopes ([cf928a4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cf928a485206481a01b63e665399a72574248bf2))
- add communication middleware ([1e42927](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1e42927eafb39fed30e90aa72e19c3732829e698))
- add country calling code validation to the users phone number ([d59e62d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d59e62dc3523ab9df8905fc575dbc69d02bd22bd))
- add cron job setup ([a0458f0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a0458f01c585508170ad008c42b7d534c21a1fe5))
- add device information to the token payload ([6e04eb9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6e04eb9b7c1e0c2389e725beee3f2a7ded644648))
- add device relationships and endpoints to users ([27f3ad7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/27f3ad70bc4c02f9ba3b6feefa1aa5f83ef17de8))
- add docs for complex expressions in target conditions ([9903c5c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9903c5c5a96356a21b626bb952d9b352a1bb6146))
- add endpoint to allow auth based on the type of the device ([5654110](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5654110a78e5ab06f1ed978cf270089958d075b0))
- add example value as false to the global property of scopes ([5a655db](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5a655db87e3b930cacbaefcdb390efb338e4f9cd))
- add external profile for users which have external to true, and add automatic profile calculation on the token payload ([b02420f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b02420f9fb3726eec25e815e286e60dcc7edcfbf))
- add fb messenger id field to users ([330cad0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/330cad051858278cb33087c66e2b08c91119636c))
- add filters for searchable filters in users get endpoint ([98091e7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/98091e7834588d96b2825155b9fe726396c719f3))
- add language headers to allowed headers ([f3b6261](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f3b6261c8b4cb27163a15f5ed597086a0622762b))
- add logging middleware ([fd606de](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fd606de935cf931f3967a08952078f829e3367ed))
- add management of profile conditions in profile endpoints ([6905076](https://gitlab.com/fi-sas/FISAS_Authorization/commit/69050760113b4890acb90dac28f3781846d8c61c))
- add method recognition to the scopes validation ([7cfb78d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7cfb78dbe8a33dcae3eac0588a61dce377e09df2))
- add migration for profile conditions and always existing profile for external students ([3878afd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3878afd4d02a0fd31498c1cdd95b722ffb8e0071))
- add more complex target conditions ([5bfd300](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5bfd300b7b6442361c033ff700df6833f10d2c33))
- add possibility of providing group by clauses to the filter order and fetch method ([d7845b0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d7845b036b91d9736ad85511b1322cc47525a1c4))
- add possibility of providing joins to filter order and fetch method ([431647d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/431647d6c4a615b0aa43ea7591b59d8b56688af6))
- add possibility of validating array elements ([f56c8a5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f56c8a5ac6530534d2279dbe4a7ae56e3e6da50d))
- add regex field to scopes ([d52219e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d52219e2bc16151f62c80370f50bbd215afde372))
- add request toke method to communication helper ([046345d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/046345d58acb23dfa950f0cafecdf01d780e6cda))
- add support for single key API composer validations ([e9d665a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e9d665a6bc96295a2aec97243bc687f70e65e8b5))
- add swagger filters for scopes router ([bc5ee95](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bc5ee958f6ef9478d2327cff435d5183523ae07a))
- add v2 endpoint for validating scopes in the gateway ([9cc5075](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9cc5075a2f52fb93f85261702de81017c4c812c1))
- add validation of mandatory profile id for users ([b6cbe80](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b6cbe80f41a698d0eed430e99eff4aa8fb5cdc9a))
- add validations for profile condition field name and operation ([6db84b3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6db84b36232a3856ec73851d6b61efc1d3292ad5))
- add with related entities for users' profile ([ae251fd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ae251fdff87b221c7877de3ef3520370377227fb))
- bad request response thrown in cases where pin is provided without email/username ([bb0bb7c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bb0bb7c5102331af0691329ac79b48e4abefd1fd))
- crud for profiles ([c16537a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c16537a685f13e71c53ed2de40dc9afd6e02faf9))
- filter undefined ids for API composer validation ([3e2215e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3e2215e7254cec03a45ac61d4a1100a989e28c18))
- fix tests for common.js ([20cc7d5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20cc7d50b9899c283ebf751cdd61a43905bacee7))
- fix usage of translation filters ([c3681ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c3681ae682326733bc1be7f4d6cb5290c05d36ce))
- implementation of more complex target conditions (wip) ([a061103](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a0611033bcf412d2598e45fc9673c14d7c516d60))
- improve docs for apiComposeValidations middleware ([7b1dd46](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7b1dd465a21a0a1dee785d78dc346c5390957a38))
- improved implementation for endpoint for getting users in target ([1db09c7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1db09c7fdaecb2fd5a6a0d32f4952b91d2336687))
- initial changes for boilerplate v2.0 ([4b2353e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4b2353ef4b30f9956076978a8c2935623f6763a0))
- loadTranslations fetching active languages and returning only translations associated with active languages ([0b6dd0f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0b6dd0f04f64a315442934137b9c88b443cdf27d))
- migrate from MySQL into Postgres ([a8363e0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a8363e0873f1043a247000101d8db094ccaa6537))
- migration for regex column in scopes ([484f2dc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/484f2dcf3f205ddd3779cb0f09cf996cbf220ee8))
- optimize active language fetch ([20bd210](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20bd2102f86032ff5a89828eb30d67a65224a929))
- optimized even more the code to fecth active languages ([0216c3b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0216c3b08e1ea397bbe3ffd6d43b7398decc24b2))
- prevent profiles with duplicate names ([8d4063a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8d4063ab7c84727aaba5b8e0709ae6a987b74a6c))
- reduce size of token payload ([04da99c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/04da99ca1b0b7f20eed55f4e47cb7f8061211624))
- rename profiles to targets and readd profiles ([1053394](https://gitlab.com/fi-sas/FISAS_Authorization/commit/105339440c0ec07d3ba1bee42b166067eb368880))
- return anonymous user information when no user is provided ([ee71d71](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ee71d714cce6bec8fbd6fcb07dfac9a4f1569ee2))
- rfid not a required field ([ae0bff1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ae0bff163d41df5431660e9c8104da50f47d4056))
- scope tree object endpoints ([83a0508](https://gitlab.com/fi-sas/FISAS_Authorization/commit/83a05087cad3b64a61c7762e7eea00a5c50a48c2))
- **users:** Add can_access_BO flag to user and consider it into auth flow. Add validation of active user in auth flow. ([df520f0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/df520f0f0c3079574f80c7555b979de52b0f75c5))
- **users:** add card rfid validity ([365454e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/365454e4e9fc2f304bab096f830243b1b5b9da1f))
- **users:** add update me endpoint ([95504ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/95504aefadd3b65e4798486ad227da89a569d8c8))
- **users:** change usergroups and scopes condition ([555e8bd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/555e8bd12c8cda373f9beb7d5248b16b91849c6f))
- Remove deprecated from /v1/authorize/validate-token endpoint ([07b21bc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07b21bca83516c0ae5d28449ec274763d1c01fb4))
- some missing changes for renaming profiles to targets ([2de89c7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2de89c7fb34a7aa0c2c3afc7efe4c5c84a1b7b5d))
- undeprecate endpoint ([e9b552d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e9b552d76713a106051826110638cfecb9913804))
- update backoffice middleware to take into account changes to the token payload ([e3103f8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e3103f88dfbbbedb8c5d35c72c733b41b1102b65))
- update communication helper to request token if req is not provided ([0a15695](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0a15695803227f0f5410fbd17a769f789d0aa1f5))
- update regex swagger example ([0e4fabc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0e4fabc6ac4f704557694a99cd5bda035f944bf2))
- update scope validations by adding regular expressions ([8672c89](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8672c894940dbb1e15dd72ec4aaa3a41c6eb2cfe))
- **backoffice:** Simple middleware to detect backoffice ([170cee7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/170cee7334dda2b10e160ea6ce206b00e3de00a8))
- **bookshelf:** Bookshelf plugins for formatting timestamps and active attributes ([a85d6a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a85d6a9baea962294005c2591fbb5029cd396f9d))
- **bookshelf:** Bookshlef plugin upgraded to include in Model the parseBooleans and formatBooleans methods ([fdc2daa](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fdc2daa79556b3925c8b316ed49bc8789e6c71cf))
- **common-helper:** Add getDeviceId method to helper. Included status code of error in api validations ([19b36ea](https://gitlab.com/fi-sas/FISAS_Authorization/commit/19b36ea00464ee5ea65b9a2fe9ee757e09e0fddb))
- **error-codes:** New error codes for commonly used entities ([a08e1a1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a08e1a15354ab4b2fa73c58d02c9b1b5f10bfe9a))
- **errors:** New generic errors and error codes ([07bfe4c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07bfe4c85a0f08ac78c6b6e44767374d3fdf7b4d))
- **joi:** Update joi version ([956a773](https://gitlab.com/fi-sas/FISAS_Authorization/commit/956a773ea7424803c30b9f90db4c05ab2e350836))
- **run-seeds:** New command to run necessary seeds ([9c0af8d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9c0af8ddbcda7506e3471c6b1bdee857540e91fc))
- **users:** Users supporting encryption disabling when req is safe ([10587a2](https://gitlab.com/fi-sas/FISAS_Authorization/commit/10587a240feef9ea26f312cb1d16f1506125edc9))
- update db collation to case insensitive ([47a18b0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/47a18b024dca64620a93b42d0044304d249e631f))
- **utf8mb4:** Database encoding utf8mb4 ([a16a3b3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a16a3b3047314d53b02f65cad361fd9255833843))
- **validations:** Api composer validations allowing validation for nestes objects ([7474319](https://gitlab.com/fi-sas/FISAS_Authorization/commit/747431967e120e41d643ce8f18cad8530f831199))
- separate auth middleware in two - one for authenticating the user and another for decoding the token info ([973f347](https://gitlab.com/fi-sas/FISAS_Authorization/commit/973f34777d348d7625b8bad8f44d14ad85169a3f))
- updated communication helper ([a622d0b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a622d0b0504a67a44fbb39f7ff13ddbe24ae16db))
- use condition function instead of providing fields in joins for filter order and fetch method ([21da6f4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/21da6f448ae08d374ebd0a53b456f96cf200c8be))
- **error-codes:** New error code for invalid delete operation ([84319fe](https://gitlab.com/fi-sas/FISAS_Authorization/commit/84319fed4ebda1aab624d63fca231ab845b109ac))
- **helper:** Common helper to getData from raw object ([6d2e485](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6d2e485b8218f99a0a7090656b7b9b577ba16439))
- **language:** Language ID from header used to load translations ([6a6d64a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6a6d64a1e3599bafe562fc130eef79c54e967a51))
- **request-parameters:** New addFilter and getFilterMethods on request parameters middleware ([109a67c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/109a67c38bd661262fb09c1b3e5b7e0ab7ce12da))
- **x-language-acronym:** Common helper function getLanguageId updated to read X-Language-Acronym header ([1f9187b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1f9187bcbe8605e40e82e035dd15a86802f71ac7))
- **x-language-acronym:** Header parameter spec for X-Language-Acronym ([9bed352](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9bed3520aa1f4ad6c27c16d05b5639758cb1f897))

# [1.0.0-rc.38](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.37...v1.0.0-rc.38) (2021-03-03)

### Bug Fixes

- **authorize:** metada return XML content type ([3d845d0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3d845d02fb7af65e32ac7e3a091a1a3759bb7941))

# [1.0.0-rc.37](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.36...v1.0.0-rc.37) (2021-02-25)

### Bug Fixes

- **authorize:** fix refresh-token return isguest always false ([d6b49a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d6b49a98159b73c855c95d3c5b257945e3570f14))

# [1.0.0-rc.36](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.35...v1.0.0-rc.36) (2021-02-22)

### Bug Fixes

- add convert in fields ([a396046](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a396046c2d6771b17e947bc292a782d5f8d12ea6))
- optional and nullable update-me ([e72f658](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e72f658a1e0f83db67b3b9f86ea2fa61088de318))

# [1.0.0-rc.35](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.34...v1.0.0-rc.35) (2021-02-22)

### Bug Fixes

- nullable validation ([b3cf306](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b3cf3068763cd679382d5b599e44f4130053e773))

# [1.0.0-rc.34](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.33...v1.0.0-rc.34) (2021-02-22)

### Bug Fixes

- endpoint update-me to receive null data ([50fd0d6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/50fd0d6aa3a7ca788e805f75bdd36a681241138c))

# [1.0.0-rc.33](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.32...v1.0.0-rc.33) (2021-02-22)

### Bug Fixes

- **authorize:** SSO validate if user has acess to BO ([7380df6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7380df65187d556a804eff2dc714482d3d3af332))

# [1.0.0-rc.32](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-02-22)

### Bug Fixes

- **authorize:** resolve cookie overwrite ([6dca2ac](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6dca2ac9da58c1d0d526b5c7e55733b5132a334e))

# [1.0.0-rc.31](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-02-22)

### Bug Fixes

- fix email and user_name validation on creation ([a908763](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a908763f7842bb542591b68590383723cccbdec9))
- sso ack token ([1fac8be](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1fac8beb07bcbeb8ef2ab996c0d610cc8f841c1b))

# [1.0.0-rc.30](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-02-18)

### Bug Fixes

- update table user and update endpoint 'update-me' ([01e25dc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/01e25dc01ac2c1d9a2e291b738e293e5ac6dbc96))

# [1.0.0-rc.29](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-02-08)

### Bug Fixes

- **knex:** fix knex file ([e67a091](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e67a0913cee46d7d63e9ee8952d332cc2cc7a74c))

# [1.0.0-rc.28](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-02-07)

### Bug Fixes

- remove unused logs ([583822d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/583822d3e3e17b0c63ea73f92732cfd0364c6715))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-02-05)

### Bug Fixes

- minor fixs ([5b1fb57](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5b1fb5728ab9bbe77a487d724f9cc37c20cf680c))

### Features

- add migration file new user fields ([d56b2a5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d56b2a55a3476bb929e93e6b87e34bd43b59e29a))
- add withrelateds and change update-me ([77573c9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/77573c9215aa2d567488b7ac73e702584612d6ab))
- seed exaluno ([efd2a77](https://gitlab.com/fi-sas/FISAS_Authorization/commit/efd2a773f20fdc4c70fc9b49b00be621767de6f1))
- **auth:** change auth method to refresh token ([795db16](https://gitlab.com/fi-sas/FISAS_Authorization/commit/795db1638168944fb956077fbf8d0f671b212e49))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-01-29)

### Bug Fixes

- **users:** normalize email on creation ([468f862](https://gitlab.com/fi-sas/FISAS_Authorization/commit/468f862eabd491da3f058b9d0eeae1c1d092b50b))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-01-29)

### Bug Fixes

- **authorize:** add create user from sso ([c12790d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c12790df4da36f805d517b2f1190677b0f19bdb6))
- **authorize:** fix create user from sso ([b94821a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b94821a8393a94b2506ad92fd663262558eca2c7))
- **authorize:** move create and update user to sso ([7e20c55](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7e20c558b3f04040f6487a4ca6ac1fb0113a14bd))
- add new env vars to SSO and Middlware ([d7be9f3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d7be9f3fdcd77bc534eb75b36ed778d158065a70))

### Features

- **authorize:** add initial endpoints to SSO ([c9c5197](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c9c5197dcfe974a61b9512e4351fe737ce529b0e))
- **migration:** add external_ref in profile ([b916fdc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b916fdc8d0319bd308a7d9f836f426147df7c67f))
- **profile:** add validation for external ref ([e52af9c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e52af9c5166d19cb59f0f42d495748bb66efbe87))
- **seeds:** add new seeds for staff and faculty ([1a554d1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1a554d1def2bca0efcbabaf84784d19b5a55fb6a))
- **soo:** add update data from sso ([b437fd1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b437fd14bcc93fda6998ed4a40839940d9f2f137))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-01-29)

### Features

- add seed to external profiles ([709d394](https://gitlab.com/fi-sas/FISAS_Authorization/commit/709d394d90b902158ef20787ac31262c6a7ab1d7))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-01-05)

### Bug Fixes

- validate token return isGuest ([3a316c9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3a316c9cdbd528d9e1a72b6d3052e1e55079adbc))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2020-12-31)

### Bug Fixes

- **authorize:** use middleware to false ([07ec83c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07ec83c8e28d0f1f1bc8431b513520d6df952969))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2020-12-31)

### Bug Fixes

- **users:** fix user create ([2c11cdc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2c11cdc95a581b8bd21dd4f3112c347df6a35f79))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2020-12-21)

### Bug Fixes

- **users:** remove default with relateds to soolve timeouts ([51db0a1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/51db0a1a120c241e56c1d0f7d1e97f8923608319))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2020-12-18)

### Bug Fixes

- **authorize:** normalize email and user_name field ([9a6193c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9a6193c0316101f712a0fe9f239fc6b920c5d10a))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2020-12-14)

### Bug Fixes

- **knex:** add knex pool to fix error connection ([adac620](https://gitlab.com/fi-sas/FISAS_Authorization/commit/adac62067f12776cb0be44900dfea4c99160e8a4))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2020-12-14)

### Bug Fixes

- **authorize:** declare token algorithm ([88700be](https://gitlab.com/fi-sas/FISAS_Authorization/commit/88700be5b24d587cbc7213328f86f5b152978915))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2020-12-03)

### Bug Fixes

- **authorize:** fix erro codes of untorized and forbidden type ([fa97549](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fa97549af0b75db44256102f775f1e5699ad018e))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2020-11-20)

### Bug Fixes

- **scopes:** add alert-templates to the scopes ([75ee5da](https://gitlab.com/fi-sas/FISAS_Authorization/commit/75ee5dad48fa82341b2f89b1e50d9a5049f7b26f))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2020-11-20)

### Bug Fixes

- **authorize:** fix validate token response ([3f4ae7e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3f4ae7edece828e06bbadfc573cd295ff846dd7e))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2020-11-19)

### Bug Fixes

- **users:** fix user fields and relations ([9dc3814](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9dc38143ea8287c44ef0d998c0f0a4a648729f87))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2020-11-18)

### Bug Fixes

- **seed:** fix insert 01_external_target ([84b41c1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/84b41c155971ce07d1c858fbbb078da31ca2dcc8))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2020-11-18)

### Bug Fixes

- **ms_core:** update ms_core package ([20308ef](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20308ef4ab7231d85151774d41d86bba688ac37a))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2020-11-17)

### Bug Fixes

- **scopes:** remove infity cicly from withRelated ([5072656](https://gitlab.com/fi-sas/FISAS_Authorization/commit/507265624b440190ce608135b7e21c32503771b1))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2020-11-16)

### Bug Fixes

- **seeds:** add new scopes and fiix a seed ([8ca7acd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8ca7acdc67c36edb96351f938f50c95b9bd967f6))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2020-11-13)

### Bug Fixes

- **authorize:** fix guest user login ([f6d332d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f6d332de17e054a7e63799e39a2552afb70f05b8))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2020-11-13)

### Bug Fixes

- **authorize:** remoe scopes and sensible info from token ([a9b8caf](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a9b8caf31e6c448ef6625b0fb168a8eb41680771))
- **profiles:** save the scopes ids from create and update ([41f0533](https://gitlab.com/fi-sas/FISAS_Authorization/commit/41f05331ffae0614a51fb0d438902806f934f03f))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-11-04)

### Bug Fixes

- fixing node version (node:12-buster-slim) ([8d60c9c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8d60c9c3cf29c24ef6567894492c6ec9548ebd77))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-11-02)

### Bug Fixes

- **seeds:** change email domain to [@sasocial](https://gitlab.com/sasocial).pt ([1260222](https://gitlab.com/fi-sas/FISAS_Authorization/commit/12602220308f3ab5fc22f01e74a2a56c1abad039))

### Features

- **users:** add first login endpoint ([ec1efb9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ec1efb904505695abd1bb35a5139f74b94712691))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-10-27)

### Bug Fixes

- actions visibility changed to published ([41a3d64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/41a3d64afd0addeed91191cdc104b7e5cfc08d65))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-10-23)

### Bug Fixes

- **semantic-release:** changed test branch ([9ba7215](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9ba721544c37092fef94461a7b5d0ae323c34885))
- **semantic-release:** fixed semantic-release branch syntax ([4b306a0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4b306a0811b73aa43798139efac8145193c1e70c))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-10-23)

### Bug Fixes

- **semantic-release:** changed test branch ([9ba7215](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9ba721544c37092fef94461a7b5d0ae323c34885))
- **semantic-release:** fixed semantic-release branch syntax ([4b306a0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4b306a0811b73aa43798139efac8145193c1e70c))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/FISAS_Authorization/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-10-20)

### Bug Fixes

- **auth:** validate tokenInfo.device array ([24e6257](https://gitlab.com/fi-sas/FISAS_Authorization/commit/24e62578bfce3801a2dec95025674ea13b71c85c))
- **seeds:** set admin to userGroup ([3e8a28a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3e8a28af9a4fc95da8b808b232da0d3133b445d0))

# 1.0.0-rc.1 (2020-10-19)

### Bug Fixes

- **api-composer-validations:** Fix error when validating incomplete data - PATCH constant problem ([2ff4cc5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2ff4cc5756133365ce2964d2ae8ae0142ff3f488))
- **api-validations:** Api validations only using defined and not null values ([19d20cc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/19d20ccdb5bbc0513c0835bed984ee890877018b))
- **apiComposerValidation:** Fix of unhandled promises and sent headers errors on validation ([f2477ef](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f2477ef5edba5a24242db87cbeb3d3874974369a))
- **auth:** add logs for testing ([d05d318](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d05d318ea6188aefce01013710496df9b7b17067))
- **auth:** add logs to sso ([221f61a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/221f61adbb2a344d9b83cf86c4d8f664f36059b6))
- **auth:** add redirect to erro page ([a243158](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a2431587708e933abfcda93de0fe2aa0a4df8746))
- **auth:** add redirect to erro page ([d008f8d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d008f8db84a643339063bea9f89ab528821d42a0))
- **auth:** BadRequest with invalid credentials error when asking for token ([4e9207c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4e9207cbd441971ca57d402960cbd22263f4a8cd))
- **auth:** change sso email field ([59ed3e1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/59ed3e1672fcd481e9b5043a2db3896b7848ae8e))
- **auth:** change sso email field ([c456323](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c45632378b743a73962f1b358c7c9949d408de72))
- **auth:** fix auth user creation on SSO new user ([95f1445](https://gitlab.com/fi-sas/FISAS_Authorization/commit/95f14453e97c61fe51d8da3a2f85032b233442b8))
- **auth:** fix error 500 in case of no metadata ([5f689e8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5f689e8e75b57d87129391eef27a47acd887e8cd))
- **auth:** fix sso auth user creation ([b380758](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b380758a3e847990b45d3f5d4675b5f55976c1dc))
- **auth:** remove logs from sso ([58bf346](https://gitlab.com/fi-sas/FISAS_Authorization/commit/58bf346bf219ac9fdcae3078db3a492ebeb0914b))
- **auth:** token expire for testing ([aa9bf64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/aa9bf64c16dba1ae90daf8883e00c440458d4737))
- **can-change-password:** add can-change-password in users ([acdc0ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/acdc0ae6e16302c58b95fa990dc47da40f97637c))
- **confirm_token:** change param token to token validate ([1f8c251](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1f8c25133a52fa49e2e42242c8559ec47ed28066))
- **core:** fix temporarily the query method ([8674f64](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8674f64168dd1fc68e2a7ec38f488e2f2b5f9ef7))
- **doc:** Small fix on common documentation ([32d4622](https://gitlab.com/fi-sas/FISAS_Authorization/commit/32d4622ee287ba8355667a1042f585e8d467a85f))
- **docker-compose:** add dev and prod composers ([ab4a4ad](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ab4a4adaa8fffa0f2288c6d70fdce0d9524921fb))
- **docker-compose:** create docker-compose to prod and dev ([1fb68e7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1fb68e7ea13189336360ca34ebb7fa9d8bc4f801))
- **dotenv:** fix configuration path ([e7f26ba](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e7f26ba5421bd956b8c2bfefbd97ffe33850349d))
- **env:** add new path for new pin ans new password ([760ea28](https://gitlab.com/fi-sas/FISAS_Authorization/commit/760ea28002beb525aed1283c10e32edc8d0fa90f))
- **env:** change env ([1bf8645](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1bf86458cfadb0d739000f73bef234f9c719be1f))
- **env:** fix new pin and password urls ([43a60dd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/43a60ddc839174a549aefcdcd4ebcbf011fd8e4c))
- **exemple.env:** fix confirm pin url ([d4cbbca](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d4cbbca0d49bd58687595e65753f6ae629273569))
- **gitignore:** add .idea and .vscode ([fe1fe1c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fe1fe1c242fd765f5d1ede0a313646d808112044))
- **loadMedias:** Missing media in single data ([bb9d5b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bb9d5b6dcaaafd14f2c39965ebfb76f25f185a75))
- **middlewares:** Docs ([d255048](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d255048035a74c292e68bc8fc52a78ab7da60ce8))
- **migrations:** remove '201901071146_rename_profiles_targets' ([6e5fa0e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6e5fa0e6d9218eb02520c13b42ec668e3b15ae3d))
- **mock:** Mock for got package returning promises ([174d87c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/174d87c79da06d396e739bcdd8d04ac690b5f260))
- **new-pin:** add validate token ([643f799](https://gitlab.com/fi-sas/FISAS_Authorization/commit/643f799a30eaa60f37bd13a1b1ad490f21a9f3d2))
- **pin:** reset and confirm pin ([d59e4f5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d59e4f5ecaa68a3554f75c49dc0ece52f65ecfa9))
- **readme:** add new annotations ([239b89d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/239b89d829dc8fa147cdfb0bbd321619f7276d08))
- **reset-pin:** change token to token validate ([636f012](https://gitlab.com/fi-sas/FISAS_Authorization/commit/636f012413eff99195d0883d64dc42414f7e92ae))
- **reset-pin:** fix errors ([0453f8b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0453f8b608d10bfbeb7e901dd752441daef6d154))
- **reset-pin:** fix url to validate pin ([9901f19](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9901f19285ba6c567a4b0f0bc049ca8fefc07dd2))
- **run-seeds:** Ordering ([0abc928](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0abc928e4204f2ab17499860e33cd84d59432f22))
- **scopes:** update services names ([da1974b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/da1974b08ccf39a3f02c9ded376b6f0528cf87f0))
- **users:** add 'U' gender ([1bd0d24](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1bd0d24270dcab6704885d1c2ae7fc16d4e41749))
- **users:** change update me to PATCH ([55da396](https://gitlab.com/fi-sas/FISAS_Authorization/commit/55da396f711dbb5bc60f9aff365c32c68fb24f04))
- **users:** change update me to POST method ([3283a10](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3283a104d06b56027700e7413f4ab8ab0c2b857b))
- **users:** fix noDefaults on patch ([709a3cc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/709a3cc7ff8f87e3fb44755107687ee464766bf5))
- **users:** fix update.me endpoint ([89ea73d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/89ea73d81de47cc1207ecb9fc5eb0785d94e62fa))
- **users:** fix users relateds ([ce0ba86](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ce0ba866b99e29e4c7ff2bbd9532087449b14897))
- **users:** log info from token ([7ca0341](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7ca03419ba39dc51b34506ee981cfc461daeeb2b))
- **users:** log info from token ([504f453](https://gitlab.com/fi-sas/FISAS_Authorization/commit/504f4531efcdbb84bfa7b04bcc598626921466bb))
- **users_scopes:** rename users scoeps on users relateds ([9370d4a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9370d4a10ac9e05e71b2e529959f0afd19efec35))
- broken RFID auth ([cb75c8b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cb75c8bbda9d9f15787e7a48c7c7707c7782f965))
- broken run seed ([7e5d0df](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7e5d0dfc24b0ad37dc12c5d378a53213ce8c9536))
- Fixed typos on common tests ([d8e7f98](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d8e7f98ef763041b811b85381ad1a21c34cc1dc2))
- migration gender ([d80d8e3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d80d8e3fcb863e127b34069abf40a0cb27864cd2))
- **users:** minor fixs ([eec25ea](https://gitlab.com/fi-sas/FISAS_Authorization/commit/eec25ea12a917a9b08f3769f7ca2a93fea09b7c2))
- **users:** remove can_change_password required ([cc94e79](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cc94e79f0e480fe70e53c430971860b3140dd02d))
- **users:** update me fix deleting all the permissions ([4204b9b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4204b9bb76b770b8498ce5ab98983728dc9874b8))
- **users:** update me fix deleting all the permissions ([cb6fd30](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cb6fd30f6e1835f7d661b38f26caafe0f65de958))
- **withRelated:** Fixed handling of withRelated parameter ([3afed46](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3afed46084eb7046788a63a648c372ddccea4f2b))

### Features

- **authorize:** add action return the logged user ([11b3149](https://gitlab.com/fi-sas/FISAS_Authorization/commit/11b3149d1ef6f1b4df53ecc9a24a7dcc214a4c04))
- **seeds:** separate seeds from sample_seeds ([e191fc7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e191fc7d32f7064151846cf7ad306fcead3125d0))
- add address fields to the user ([8620267](https://gitlab.com/fi-sas/FISAS_Authorization/commit/86202673f73f88b8f8115c513ddb7e1eb7c48b83))
- initial changes for boilerplate v2.0 ([4b2353e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4b2353ef4b30f9956076978a8c2935623f6763a0))
- migrate from MySQL into Postgres ([a8363e0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a8363e0873f1043a247000101d8db094ccaa6537))
- scope tree object endpoints ([83a0508](https://gitlab.com/fi-sas/FISAS_Authorization/commit/83a05087cad3b64a61c7762e7eea00a5c50a48c2))
- **authorize:** clear cache on scopes change ([e4cd209](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e4cd209618cf1a4612a7370c50f7f5e4ae68482c))
- **change-password:** Require previous password on change password endpoint ([f7a267f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f7a267fc2981751ad28f10c61edc1f97298b242f))
- **error-codes:** Add error code for invalid operation (use with business and model design logic) ([6d4a26c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6d4a26c74960c9623ba55b1bded7ec139fe09242))
- **profiles:** Run seed for profile of private accommodation MS ([7ad6e23](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7ad6e2363c073074ba9bc2bce9386616d4f9e5f9))
- **request-parameters:** Add possibility of remove filter value in req.parameters.getFilterValue function ([214f3a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/214f3a9ea6f3abd3b97791e70df34e4621ffcdfa))
- **reset-password:** reset password and change ([4e11e7e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/4e11e7ebf58793028cd39e0821b7a6bf47ef28ac))
- **reset-pin:** reset pin ([f23d681](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f23d681cd9cd15a24c4721c0b4bd7efe149f811a))
- **scopes:** Remove scopes validation middleware and usage inside MS ([02355b6](https://gitlab.com/fi-sas/FISAS_Authorization/commit/02355b633686d1de885a5cc293e671f19f89e5f0))
- **search:** add new method for query from url ([c8fbea8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c8fbea8b859d28befb5a07c68bb61b39a56beeb8))
- **search:** query search fileds ([35a0970](https://gitlab.com/fi-sas/FISAS_Authorization/commit/35a0970f3bfa34b488f93c51e79bbaadc5d6176e))
- **services:** add main services of auth ([e137055](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e137055f12e380c6ce05236be82083b530f76a5a))
- **spec:** Common parameter of ID filter specification ([5d227f1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5d227f1202f2bab270d27d1f8f4dcc6bf91bd459))
- **users:** add birth date to the user update me ([603c954](https://gitlab.com/fi-sas/FISAS_Authorization/commit/603c95409cf0489b9c6b1ba7456a535531cdaf25))
- **users:** Add can_access_BO flag to user and consider it into auth flow. Add validation of active user in auth flow. ([df520f0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/df520f0f0c3079574f80c7555b979de52b0f75c5))
- **users:** add card rfid validity ([365454e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/365454e4e9fc2f304bab096f830243b1b5b9da1f))
- **users:** add update me endpoint ([95504ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/95504aefadd3b65e4798486ad227da89a569d8c8))
- **users:** change usergroups and scopes condition ([555e8bd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/555e8bd12c8cda373f9beb7d5248b16b91849c6f))
- add column to allow global openness of scopes ([cf928a4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/cf928a485206481a01b63e665399a72574248bf2))
- add communication middleware ([1e42927](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1e42927eafb39fed30e90aa72e19c3732829e698))
- add country calling code validation to the users phone number ([d59e62d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d59e62dc3523ab9df8905fc575dbc69d02bd22bd))
- add cron job setup ([a0458f0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a0458f01c585508170ad008c42b7d534c21a1fe5))
- add device information to the token payload ([6e04eb9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6e04eb9b7c1e0c2389e725beee3f2a7ded644648))
- add device relationships and endpoints to users ([27f3ad7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/27f3ad70bc4c02f9ba3b6feefa1aa5f83ef17de8))
- add docs for complex expressions in target conditions ([9903c5c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9903c5c5a96356a21b626bb952d9b352a1bb6146))
- add endpoint to allow auth based on the type of the device ([5654110](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5654110a78e5ab06f1ed978cf270089958d075b0))
- add example value as false to the global property of scopes ([5a655db](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5a655db87e3b930cacbaefcdb390efb338e4f9cd))
- add external profile for users which have external to true, and add automatic profile calculation on the token payload ([b02420f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b02420f9fb3726eec25e815e286e60dcc7edcfbf))
- add fb messenger id field to users ([330cad0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/330cad051858278cb33087c66e2b08c91119636c))
- add filters for searchable filters in users get endpoint ([98091e7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/98091e7834588d96b2825155b9fe726396c719f3))
- add logging middleware ([fd606de](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fd606de935cf931f3967a08952078f829e3367ed))
- add management of profile conditions in profile endpoints ([6905076](https://gitlab.com/fi-sas/FISAS_Authorization/commit/69050760113b4890acb90dac28f3781846d8c61c))
- add migration for profile conditions and always existing profile for external students ([3878afd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3878afd4d02a0fd31498c1cdd95b722ffb8e0071))
- add possibility of providing group by clauses to the filter order and fetch method ([d7845b0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d7845b036b91d9736ad85511b1322cc47525a1c4))
- add possibility of providing joins to filter order and fetch method ([431647d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/431647d6c4a615b0aa43ea7591b59d8b56688af6))
- add validations for profile condition field name and operation ([6db84b3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6db84b36232a3856ec73851d6b61efc1d3292ad5))
- add with related entities for users' profile ([ae251fd](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ae251fdff87b221c7877de3ef3520370377227fb))
- bad request response thrown in cases where pin is provided without email/username ([bb0bb7c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bb0bb7c5102331af0691329ac79b48e4abefd1fd))
- fix tests for common.js ([20cc7d5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20cc7d50b9899c283ebf751cdd61a43905bacee7))
- fix usage of translation filters ([c3681ae](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c3681ae682326733bc1be7f4d6cb5290c05d36ce))
- prevent profiles with duplicate names ([8d4063a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8d4063ab7c84727aaba5b8e0709ae6a987b74a6c))
- return anonymous user information when no user is provided ([ee71d71](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ee71d714cce6bec8fbd6fcb07dfac9a4f1569ee2))
- **backoffice:** Simple middleware to detect backoffice ([170cee7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/170cee7334dda2b10e160ea6ce206b00e3de00a8))
- **bookshelf:** Bookshelf plugins for formatting timestamps and active attributes ([a85d6a9](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a85d6a9baea962294005c2591fbb5029cd396f9d))
- **bookshelf:** Bookshlef plugin upgraded to include in Model the parseBooleans and formatBooleans methods ([fdc2daa](https://gitlab.com/fi-sas/FISAS_Authorization/commit/fdc2daa79556b3925c8b316ed49bc8789e6c71cf))
- **error-codes:** New error codes for commonly used entities ([a08e1a1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a08e1a15354ab4b2fa73c58d02c9b1b5f10bfe9a))
- **errors:** New generic errors and error codes ([07bfe4c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07bfe4c85a0f08ac78c6b6e44767374d3fdf7b4d))
- **helper:** Common helper to getData from raw object ([6d2e485](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6d2e485b8218f99a0a7090656b7b9b577ba16439))
- **joi:** Update joi version ([956a773](https://gitlab.com/fi-sas/FISAS_Authorization/commit/956a773ea7424803c30b9f90db4c05ab2e350836))
- **run-seeds:** New command to run necessary seeds ([9c0af8d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9c0af8ddbcda7506e3471c6b1bdee857540e91fc))
- **users:** Users supporting encryption disabling when req is safe ([10587a2](https://gitlab.com/fi-sas/FISAS_Authorization/commit/10587a240feef9ea26f312cb1d16f1506125edc9))
- add language headers to allowed headers ([f3b6261](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f3b6261c8b4cb27163a15f5ed597086a0622762b))
- add method recognition to the scopes validation ([7cfb78d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7cfb78dbe8a33dcae3eac0588a61dce377e09df2))
- add more complex target conditions ([5bfd300](https://gitlab.com/fi-sas/FISAS_Authorization/commit/5bfd300b7b6442361c033ff700df6833f10d2c33))
- add possibility of validating array elements ([f56c8a5](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f56c8a5ac6530534d2279dbe4a7ae56e3e6da50d))
- add regex field to scopes ([d52219e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/d52219e2bc16151f62c80370f50bbd215afde372))
- add request toke method to communication helper ([046345d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/046345d58acb23dfa950f0cafecdf01d780e6cda))
- add support for single key API composer validations ([e9d665a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e9d665a6bc96295a2aec97243bc687f70e65e8b5))
- add swagger filters for scopes router ([bc5ee95](https://gitlab.com/fi-sas/FISAS_Authorization/commit/bc5ee958f6ef9478d2327cff435d5183523ae07a))
- add v2 endpoint for validating scopes in the gateway ([9cc5075](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9cc5075a2f52fb93f85261702de81017c4c812c1))
- add validation of mandatory profile id for users ([b6cbe80](https://gitlab.com/fi-sas/FISAS_Authorization/commit/b6cbe80f41a698d0eed430e99eff4aa8fb5cdc9a))
- crud for profiles ([c16537a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/c16537a685f13e71c53ed2de40dc9afd6e02faf9))
- filter undefined ids for API composer validation ([3e2215e](https://gitlab.com/fi-sas/FISAS_Authorization/commit/3e2215e7254cec03a45ac61d4a1100a989e28c18))
- implementation of more complex target conditions (wip) ([a061103](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a0611033bcf412d2598e45fc9673c14d7c516d60))
- improve docs for apiComposeValidations middleware ([7b1dd46](https://gitlab.com/fi-sas/FISAS_Authorization/commit/7b1dd465a21a0a1dee785d78dc346c5390957a38))
- improved implementation for endpoint for getting users in target ([1db09c7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1db09c7fdaecb2fd5a6a0d32f4952b91d2336687))
- loadTranslations fetching active languages and returning only translations associated with active languages ([0b6dd0f](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0b6dd0f04f64a315442934137b9c88b443cdf27d))
- migration for regex column in scopes ([484f2dc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/484f2dcf3f205ddd3779cb0f09cf996cbf220ee8))
- optimize active language fetch ([20bd210](https://gitlab.com/fi-sas/FISAS_Authorization/commit/20bd2102f86032ff5a89828eb30d67a65224a929))
- optimized even more the code to fecth active languages ([0216c3b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0216c3b08e1ea397bbe3ffd6d43b7398decc24b2))
- reduce size of token payload ([04da99c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/04da99ca1b0b7f20eed55f4e47cb7f8061211624))
- Remove deprecated from /v1/authorize/validate-token endpoint ([07b21bc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/07b21bca83516c0ae5d28449ec274763d1c01fb4))
- rename profiles to targets and readd profiles ([1053394](https://gitlab.com/fi-sas/FISAS_Authorization/commit/105339440c0ec07d3ba1bee42b166067eb368880))
- rfid not a required field ([ae0bff1](https://gitlab.com/fi-sas/FISAS_Authorization/commit/ae0bff163d41df5431660e9c8104da50f47d4056))
- some missing changes for renaming profiles to targets ([2de89c7](https://gitlab.com/fi-sas/FISAS_Authorization/commit/2de89c7fb34a7aa0c2c3afc7efe4c5c84a1b7b5d))
- undeprecate endpoint ([e9b552d](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e9b552d76713a106051826110638cfecb9913804))
- update backoffice middleware to take into account changes to the token payload ([e3103f8](https://gitlab.com/fi-sas/FISAS_Authorization/commit/e3103f88dfbbbedb8c5d35c72c733b41b1102b65))
- update communication helper to request token if req is not provided ([0a15695](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0a15695803227f0f5410fbd17a769f789d0aa1f5))
- update db collation to case insensitive ([47a18b0](https://gitlab.com/fi-sas/FISAS_Authorization/commit/47a18b024dca64620a93b42d0044304d249e631f))
- update regex swagger example ([0e4fabc](https://gitlab.com/fi-sas/FISAS_Authorization/commit/0e4fabc6ac4f704557694a99cd5bda035f944bf2))
- update scope validations by adding regular expressions ([8672c89](https://gitlab.com/fi-sas/FISAS_Authorization/commit/8672c894940dbb1e15dd72ec4aaa3a41c6eb2cfe))
- updated communication helper ([a622d0b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a622d0b0504a67a44fbb39f7ff13ddbe24ae16db))
- **common-helper:** Add getDeviceId method to helper. Included status code of error in api validations ([19b36ea](https://gitlab.com/fi-sas/FISAS_Authorization/commit/19b36ea00464ee5ea65b9a2fe9ee757e09e0fddb))
- **error-codes:** New error code for invalid delete operation ([84319fe](https://gitlab.com/fi-sas/FISAS_Authorization/commit/84319fed4ebda1aab624d63fca231ab845b109ac))
- **language:** Language ID from header used to load translations ([6a6d64a](https://gitlab.com/fi-sas/FISAS_Authorization/commit/6a6d64a1e3599bafe562fc130eef79c54e967a51))
- **load-related:** Helper with loadRelated function ([f039020](https://gitlab.com/fi-sas/FISAS_Authorization/commit/f039020b3ff7d5f4be491124e7b003f49dee370b))
- **request-parameters:** New addFilter and getFilterMethods on request parameters middleware ([109a67c](https://gitlab.com/fi-sas/FISAS_Authorization/commit/109a67c38bd661262fb09c1b3e5b7e0ab7ce12da))
- **scope-permissions:** New internal request detection middleware. Scope permission given to internal requests ([1cc9075](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1cc9075b67db1a0e8d1a391c079b1bb23e08b6c7))
- **utf8mb4:** Database encoding utf8mb4 ([a16a3b3](https://gitlab.com/fi-sas/FISAS_Authorization/commit/a16a3b3047314d53b02f65cad361fd9255833843))
- **validations:** Api composer validations allowing validation for nestes objects ([7474319](https://gitlab.com/fi-sas/FISAS_Authorization/commit/747431967e120e41d643ce8f18cad8530f831199))
- separate auth middleware in two - one for authenticating the user and another for decoding the token info ([973f347](https://gitlab.com/fi-sas/FISAS_Authorization/commit/973f34777d348d7625b8bad8f44d14ad85169a3f))
- use condition function instead of providing fields in joins for filter order and fetch method ([21da6f4](https://gitlab.com/fi-sas/FISAS_Authorization/commit/21da6f448ae08d374ebd0a53b456f96cf200c8be))
- **x-language-acronym:** Common helper function getLanguageId updated to read X-Language-Acronym header ([1f9187b](https://gitlab.com/fi-sas/FISAS_Authorization/commit/1f9187bcbe8605e40e82e035dd15a86802f71ac7))
- **x-language-acronym:** Header parameter spec for X-Language-Acronym ([9bed352](https://gitlab.com/fi-sas/FISAS_Authorization/commit/9bed3520aa1f4ad6c27c16d05b5639758cb1f897))
