const { table } = require("../services/scopes.service");

module.exports.up = async (db) =>
	db.schema

		// user profiles table
		.createTable("profile", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.boolean("active").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// user aggregation - target table
		.createTable("target", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.boolean("active").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// user aggregation - target condition table
		.createTable("target_condition", (table) => {
			table.increments();
			table.integer("target_id").notNullable().unsigned().references("id").inTable("target");
			table.string("field_name", 4096).notNullable();
			table.string("field_op", 4096).notNullable();
			table.string("field_value", 4096).notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// users database table
		.createTable("user", (table) => {
			table.increments();
			table.integer("profile_id").notNullable().unsigned().references("id").inTable("profile");
			table.text("rfid");
			table.datetime("rfid_validity");
			table.string("fb_messenger_id", 4096).defaultTo(null);
			table.string("name", 255).notNullable();
			table.string("email", 255).notNullable();
			table.string("phone", 120);
			table.string("user_name", 255).notNullable();
			table.text("password").notNullable();
			table.string("salt", 40).notNullable().defaultTo("");
			table.text("pin").notNullable();
			table.boolean("first_login").defaultTo(false).notNullable();
			table.string("institute", 255);
			table.string("student_number", 255);
			table.string("identification", 15);
			table.boolean("can_change_password").default(false);
			table.boolean("external");
			table.boolean("can_access_BO").notNullable().defaultTo(false);
			table.boolean("active").notNullable();
			table.datetime("birth_date");
			table.enu("gender", ["M", "F", "U"]);
			table.string("picture", 2048);
			table.string("address", 255).defaultTo(null);
			table.string("postal_code", 15).defaultTo(null);
			table.string("city", 255).defaultTo(null);
			table.string("country", 255).defaultTo(null);
			table.string("tin", 11).defaultTo(null);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// user groups database table
		.createTable("user_group", (table) => {
			table.increments();
			table.string("name", 120).notNullable();
		})

		// users to user groups many-to-many relationship database table
		.createTable("user_user_group", (table) => {
			table.increments();
			table.integer("user_id").unsigned().references("id").inTable("user");
			table.integer("user_group_id").unsigned().references("id").inTable("user_group");
			table.unique(["user_id", "user_group_id"]);
		})

		// scopes database table
		.createTable("scope", (table) => {
			table.increments();
			table
				.integer("parent_id")
				.unsigned()
				.references("id")
				.inTable("scope")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.json("grants");
			table.string("name", 120);
			table.string("permission", 255);
			table.unique("permission");
		})

		// scopes to profile many-to-many relationship database table
		.createTable("profile_scope", (table) => {
			table.increments();
			table.integer("scope_id").unsigned().references("id").inTable("scope");
			table.integer("profile_id").unsigned().references("id").inTable("profile");
			table.unique(["scope_id", "profile_id"]);
		})

		// scopes to user groups many-to-many relationship database table
		.createTable("user_group_scope", (table) => {
			table.increments();
			table.integer("scope_id").unsigned().references("id").inTable("scope");
			table.integer("user_group_id").unsigned().references("id").inTable("user_group");
			table.unique(["scope_id", "user_group_id"]);
		})

		// scopes to users many-to-many relationship database table
		.createTable("user_scope", (table) => {
			table.increments();
			table.integer("scope_id").unsigned().references("id").inTable("scope");
			table.integer("user_id").unsigned().references("id").inTable("user");
			table.unique(["scope_id", "user_id"]);
		})

		// jwt tokens database table
		.createTable("jwt", (table) => {
			table.increments();
			table.text("token");
			table.boolean("active");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// user associated device ids
		.createTable("user_device", (table) => {
			table.increments();
			table.integer("user_id").notNullable().unsigned().references("id").inTable("user");
			table.string("device_id", 4096).notNullable();
		})

		// user changed credentials table
		.createTable("change_credentials", (table) => {
			table.increments();
			table.string("token", 255).notNullable();
			table.integer("user_id").unsigned().notNullable();
			table.enum("type", ["PIN", "PASSWORD", "RFID"]);
			table.boolean("changed").default(false).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("target")
		.dropTable("target_condition")
		.dropTable("change_credentials")
		.dropTable("user_device")
		.dropTable("profile")
		.dropTable("jwt")
		.dropTable("user_user_group")
		.dropTable("user_group_scope")
		.dropTable("user_group")
		.dropTable("user_scope")
		.dropTable("scope")
		.dropTable("user");

module.exports.configuration = {
	transaction: true,
};
