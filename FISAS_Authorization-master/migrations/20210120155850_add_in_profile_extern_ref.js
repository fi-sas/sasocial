
module.exports.up = async db => db.schema
	.table("profile", table => table.string("external_ref", 255));

module.exports.down = async db => db.schema
	.table("profile", table => table.dropColumn("external_ref"));

module.exports.configuration = { transaction: true };

