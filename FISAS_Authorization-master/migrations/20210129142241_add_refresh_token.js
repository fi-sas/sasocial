
exports.up = (knex) => {
	return knex.schema.createTable("confirmed_token", (table) => {
		table.increments();
		table.integer("user_id").notNullable().unsigned().references("id").inTable("user");
		table.string("token", 4096).notNullable();
		table.string("device_type", 12).notNullable();
		table.datetime("expire_at").notNullable();
		table.datetime("created_at").notNullable();
	});
};

exports.down = (knex) => {
	return knex.schema
		.dropTable("confirmed_token");
};
