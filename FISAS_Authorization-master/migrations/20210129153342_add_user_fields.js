
exports.up = (knex) => {
	return knex.schema.
		alterTable("user", (table) => {
			table.dropColumn("picture");
			table.integer("picture_file_id").unsigned().nullable();
			table.integer("course_id").unsigned().nullable();
			table.string("course_year").nullable();
			table.integer("organic_unit_id").unsigned().nullable();
			table.integer("course_degree_id").unsigned().nullable();
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.dropColumn("picture_file_id");
			table.string("picture", 2048);
			table.dropColumn("course_id");
			table.dropColumn("course_year");
			table.dropColumn("organic_unit_id");
			table.dropColumn("course_degree_id");
		});
};
