exports.up = (knex) => {
    return knex.schema.
        alterTable("user", (table) => {
            table.integer("course_year").unsigned().nullable().alter();
        });
};

exports.down = (knex) => {
    return knex.schema
        .alterTable("user", (table) => {
            table.string("course_year").nullable().alter();
        });
};
