exports.up = (db) => {
	return db.schema
		.createTable("department", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.boolean("active").notNullable().default(true);
			table.integer("organic_unit_id").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})
		.createTable("section", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.boolean("active").notNullable().default(true);
			table
				.integer("department_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("department");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})
		.alterTable("user", (table) => {
			table.integer("section_id").unsigned().references("id").inTable("section");
			table.integer("department_id").unsigned().references("id").inTable("department");
		})
		.alterTable("target", (table) => {
			table
				.integer("user_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("user")
				.defaultTo(1);
		})
		.alterTable("target_condition", (table) => {
			table.dropColumn("field_op");
		});
};

exports.down = (db) => {
	return db.schema
		.alterTable("user", (table) => {
			table.dropColumn("section_id");
		})
		.alterTable("target", (table) => {
			table.dropColumn("user_id");
		})
		.alterTable("target_condition", (table) => {
			table.string("field_op", 4096).notNullable();
		})
		.dropTable("section")
		.dropTable("department");
};
module.exports.configuration = { transaction: true };
