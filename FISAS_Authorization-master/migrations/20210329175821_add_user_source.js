exports.up = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table
			.enu("sourced_by", ["BO", "MIDDLEWARE", "INTERNAL"], {
				useNative: true,
				enumName: "user_sourced_by",
			})
			.nullable()
			.defaultTo("BO");
		table.string("source_name", 50).nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.dropColumn("sourced_by");
		table.dropColumn("source_name");
	});
};
