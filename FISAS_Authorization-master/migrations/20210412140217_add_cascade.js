exports.up = (knex) => {
	return knex.schema.alterTable("user_group_scope", (table) => {
		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("CASCADE");

		table.dropForeign("user_group_id");

		table
			.foreign("user_group_id")
			.references("user_group.id")
			.onDelete("CASCADE");
	}).alterTable("user_user_group", (table) => {
		table.dropForeign("user_id");

		table
			.foreign("user_id")
			.references("user.id")
			.onDelete("CASCADE");
	}).alterTable("profile_scope", (table) => {
		table.dropForeign("profile_id");

		table
			.foreign("profile_id")
			.references("profile.id")
			.onDelete("CASCADE");

		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("CASCADE");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user_group_scope", (table) => {
		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("NO ACTION");

		table.dropForeign("user_group_id");

		table
			.foreign("user_group_id")
			.references("user_group.id")
			.onDelete("NO ACTION");
	}).alterTable("user_user_group", (table) => {
		table.dropForeign("user_id");

		table
			.foreign("user_id")
			.references("user.id")
			.onDelete("NO ACTION");
	}).alterTable("profile_scope", (table) => {
		table.dropForeign("profile_id");

		table
			.foreign("profile_id")
			.references("profile.id")
			.onDelete("NO ACTION");

		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("NO ACTION");
	});
};
