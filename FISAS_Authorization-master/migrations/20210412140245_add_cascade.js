exports.up = (knex) => {
	return knex.schema.alterTable("user_scope", (table) => {
		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("CASCADE");

		table.dropForeign("user_id");

		table
			.foreign("user_id")
			.references("user.id")
			.onDelete("CASCADE");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user_scope", (table) => {
		table.dropForeign("scope_id");

		table
			.foreign("scope_id")
			.references("scope.id")
			.onDelete("NO ACTION");

		table.dropForeign("user_id");

		table
			.foreign("user_id")
			.references("user.id")
			.onDelete("NO ACTION");
	});
};
