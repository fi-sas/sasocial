exports.up = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.datetime("disabled_from").nullable();
		table.datetime("password_expire_on").nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.dropColumn("disabled_from");
		table.dropColumn("password_expire_on");
	});
};
