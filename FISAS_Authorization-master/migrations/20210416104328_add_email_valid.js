exports.up = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.boolean("account_verified").defaultTo(false);
		table.uuid("verification_token").nullable();
		table.datetime("verification_expires").nullable();
		table.uuid("recovery_token").nullable();
		table.datetime("recovery_expires").nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.dropColumn("account_verified");
		table.dropColumn("verification_token");
		table.dropColumn("verification_expires");
		table.dropColumn("recovery_token");
		table.dropColumn("recovery_expires");
	});
};
