exports.up = (knex) => {
	return knex.raw(`update "user" set account_verified = true`);
};

exports.down = (knex) => {
	return knex.raw(`update "user" set account_verified = false`);
};
