exports.up = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table
			.enu("document_type", [
				"RESIDENCE_AUTHORIZATION",
				"BI",
				"ANGOLAN_BI",
				"FOREIGN_BI",
				"MILITARY_BI",
				"CC",
				"RESIDENCE_CARD",
				"UE_RESIDENCE_CARD",
				"UE_CITIZEN_CERTIFICATE",
				"PASSPORT"
			], {
				useNative: true,
				enumName: "identification_document_type",
			})
			.nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("user", (table) => {
		table.dropColumn("document_type");
	});
};
