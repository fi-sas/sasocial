exports.up = (knex) => {
	return knex.schema.createTable("password_history", (table) => {
		table.increments();
		table.integer("user_id").unsigned().references("id").inTable("user");
		table.text("password").notNullable();
		table.datetime("created_at").notNullable();
	});
};

exports.down = (knex) => {
	return knex.schema.dropTable("password_history");
};
