exports.up = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.datetime("admission_date");
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.dropColumn("admission_date");
		});
};
