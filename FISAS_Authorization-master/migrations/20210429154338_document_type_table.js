exports.up = (knex) => {
	return knex.schema
		.createTable("document_type", (table) => {
			table.increments();
			table.boolean("active");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("document_type_translation", (table) => {
			table.increments();
			table.string("description");
			table.integer("language_id");
			table.integer("document_type_id").references("id").inTable("document_type");
		})
		.alterTable("user", (table) => {
			table.dropColumn("document_type");
			table.integer("document_type_id").references("id").inTable("document_type");
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.dropColumn("document_type_id");
			table.enu("document_type", ["RESIDENCE_AUTHORIZATION", "BI", "ANGOLAN_BI", "FOREIGN_BI", "MILITARY_BI", "CC", "RESIDENCE_CARD", "UE_RESIDENCE_CARD", "UE_CITIZEN_CERTIFICATE", "PASSPORT"], { useNative: false, enumName: "identification_document_type", }).nullable();
		})
		.dropTable("document_type_translations")
		.dropTable("document_type");
};
