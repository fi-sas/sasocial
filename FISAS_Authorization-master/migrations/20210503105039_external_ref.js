exports.up = (knex) => {
	return knex.schema
		.alterTable("document_type", (table) => {
			table.string("external_ref", 20);
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("document_type", (table) => {
			table.dropColumn("external_ref");
		});
};
