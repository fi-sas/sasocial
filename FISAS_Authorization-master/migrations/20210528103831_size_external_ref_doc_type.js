exports.up = (knex) => {
	return knex.schema
		.alterTable("document_type", (table) => {
			table.string("external_ref", 100).alter();
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("document_type", (table) => {
			table.string("external_ref", 20).alter();
		});
};
