exports.up = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.string("nationality", 30).nullable();
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.dropColumn("nationality");
		});
};
