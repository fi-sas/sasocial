module.exports.up = async (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("identification", 50).alter();
	});
module.exports.down = async (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("identification", 15).alter();
	});

module.exports.configuration = { transaction: true };
