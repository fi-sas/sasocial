module.exports.up = (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("nationality", 100).alter();
	});
module.exports.down = (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("nationality", 30).alter();
	});

module.exports.configuration = { transaction: true };
