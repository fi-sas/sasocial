module.exports.up = async (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("tin", 24).alter();
	});
module.exports.down = async (db) =>
	db.schema.alterTable("user", (table) => {
		table.string("tin", 11).alter();
	});

module.exports.configuration = { transaction: true };
