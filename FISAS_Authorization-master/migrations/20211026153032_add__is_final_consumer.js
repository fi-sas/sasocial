exports.up = (knex) => {
    return knex.schema
        .alterTable("user", (table) => {
            table.boolean("is_final_consumer").defaultTo(false).notNullable();
        });
};

exports.down = (knex) => {
    return knex.schema
        .alterTable("user", (table) => {
            table.dropColumn("is_final_consumer");
        });
};
