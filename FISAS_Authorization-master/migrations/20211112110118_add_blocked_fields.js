
exports.up = (knex) => {
	return knex.schema.
		alterTable("user", (table) => {
			table.specificType("blocked_fields", "varchar(50)[]").nullable().defaultTo("{}");
		});
};

exports.down = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.dropColumn("blocked_fields");
		});
};
