exports.up = (knex) => {
	return knex.schema
		.alterTable("user", (table) => {
			table.index(["user_name"], "username_index");
			table.index(["email"], "email_index");
			table.index(["rfid"], "rfid_index");
		})
		.alterTable("profile_scope", (table) => {
			table.index(["profile_id"], "profile_id_index");
		})
		.alterTable("user_user_group", (table) => {
			table.index(["user_id"], "user_id_index");
		});
};

exports.down = (knex) => {};
