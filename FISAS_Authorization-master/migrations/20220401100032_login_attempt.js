exports.up = (knex) => {
	return knex.schema.createTable("login_attempt", (table) => {
		table.increments();
		table.string("entity").nullable();
		table.string("cause").nullable();
		table.integer("user_id").nullable().unsigned().references("id").inTable("user");
		table.integer("device_id").notNullable();
		table.boolean("success").notNullable();
		table.datetime("created_at").notNullable();
	});
};

exports.down = (knex) => {
	return knex.schema.dropTable("login_attempt");
};
