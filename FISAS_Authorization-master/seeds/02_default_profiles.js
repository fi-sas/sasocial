exports.seed = (knex) =>
	knex("profile")
		.select()
		.where("name", "Alunos")
		.then(async (rows) => {
			if (rows.length === 0) {
				return knex("profile").insert({
					name: "Alunos",
					external_ref: "student",
					active: true,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
			return true;
		})
		.catch((err) => {
			// eslint-disable-next-line no-console
			console.error(err);
		});
