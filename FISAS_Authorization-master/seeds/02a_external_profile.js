exports.seed = (knex) =>
	knex("profile")
		.select()
		.where("name", "External")
		.then(async (rows) => {
			if (rows.length === 0) {
				return knex("profile").insert({
					name: "External",
					external_ref: "external",
					active: true,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
