const crypto = require("crypto");
const UUIDV4 = require("uuid").v4;

exports.seed = (knex) =>
	knex("user")
		.select()
		.where("user_name", "admin")
		.then(async (rows) => {
			const external = await knex("profile").select().where("name", "External");
			if (rows.length === 0) {
				const func = await knex("profile").select().where("name", "Funcionários");

				await knex("user").insert({
					name: "GUEST USER",
					email: "guest@sasocial.pt",
					phone: "",
					user_name: "guest",
					password: "",
					gender: "F",
					pin: "",
					salt: "",
					external: true,
					active: true,
					account_verified: true,
					can_access_BO: false,
					profile_id: external[0].id,
					created_at: new Date(),
					updated_at: new Date(),
				});

				const salt = UUIDV4();
				await knex("user").insert({
					name: "ADMIN USER",
					email: "admin@sasocial.pt",
					phone: "",
					user_name: "admin",
					password: crypto.createHash("sha256").update(salt.concat("admin")).digest("hex"),
					gender: "M",
					salt,
					pin: crypto.createHash("sha256").update(salt.concat("1234")).digest("hex"),
					external: true,
					active: true,
					account_verified: true,
					can_change_password: true,
					profile_id: func[0].id,
					can_access_BO: true,
					created_at: new Date(),
					updated_at: new Date(),
				});
			} else {
				const guest = await knex("user").select().where("user_name", "guest");

				await knex("user")
					.update({
						name: "GUEST USER",
						email: "guest@sasocial.pt",
						phone: "",
						password: "",
						gender: "U",
						pin: "",
						salt: "",
						external: true,
						active: true,
						account_verified: true,
						can_access_BO: false,
						profile_id: external[0].id,
					})
					.where("id", guest[0].id)
					.returning("*");
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
