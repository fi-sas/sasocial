/* eslint-disable no-console */

const scopes = require("../scopes/scopes.json");
//const knexConfig = require("../knexfile");
//const { concat } = require("lodash");
//const knex = require("knex")(knexConfig);
exports.seed = (knex) =>
	createPromise(scopes, knex, null).then(() => {
		return checkAdminUserGroup(knex);
	});

function createPromise(s, knex, parent_id = null) {
	return Promise.all(s.map((s) => createIfNotExist(s, knex, parent_id)));
}

function createIfNotExist(s, knex, parent_id) {
	return knex("scope")
		.select()
		.where("permission", "=", s.permission)
		.then((res) => {
			if (res.length === 0) {
				return knex("scope")
					.insert({
						name: s.name,
						permission: s.permission,
						parent_id,
						grants: JSON.stringify(s.grants)
					})
					.returning("*")
					.then((res) => {
						return res[0];
					}).catch((er) => {
						console.error("Error creating scope: ", s);
						throw er;
					});
			} else {
				return knex("scope")
					.update({
						name: s.name,
						grants: JSON.stringify(s.grants)
					})
					.where("id", res[0].id)
					.returning("*")
					.then((res) => {
						return res[0];
					});
			}
		})
		.then((res) => {
			if (s.childs) {
				return createPromise(s.childs, knex, res.id);
			}
			return Promise.resolve();
		});
}

function checkAdminUserGroup(knex) {
	return knex("user_group")
		.where("name", "=", "Admin")
		.then(async (userGroup) => {
			if (userGroup.length < 1) {
				const id = await knex("user_group")
					.insert({
						name: "Admin",
					})
					.returning("id")
					.then((newUserGroup) => {
						return knex("scope")
							.select()
							.then(async (rows) => {
								for (let i = 0; i < rows.length; i++) {
									await createUserGroupScopeRelation(newUserGroup[0], rows[i].id, knex);
								}
							});
					});
			} else {
				await knex("scope")
					.select()
					.then(async (rows) => {
						for (let i = 0; i < rows.length; i++) {
							await createUserGroupScopeRelation(userGroup[0].id, rows[i].id, knex);
						}
					});
			}
		});
}

function createUserGroupScopeRelation(user_group_id, scope_id, knex) {
	return knex("user_group_scope")
		.select()
		.where("scope_id", "=", scope_id)
		.andWhere("user_group_id", "=", user_group_id)
		.then((user_group_scope) => {
			if (user_group_scope.length > 0) {
				return true;
			}

			return knex("user_group_scope")
				.insert({
					scope_id,
					user_group_id,
				})
				.then((relation) => {
					console.log("Relation created -> ".concat(relation));
				});
		});
}
