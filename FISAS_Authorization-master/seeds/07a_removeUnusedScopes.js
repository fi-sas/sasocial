/* eslint-disable no-console */

const scopes = require("../scopes/scopes.json");
//const knexConfig = require("../knexfile");
//const knex = require("knex")(knexConfig);

exports.seed = (knex) => knex("scope")
	.whereNotIn("permission", getAllPermissions(scopes))
	.del();



function getAllPermissions(scopes, permissions = []) {

	scopes.map(s => {
		permissions.push(s.permission);
		if (s.childs) {
			getAllPermissions(s.childs, permissions);
		}
	});

	return permissions;
}

