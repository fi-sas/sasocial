/* eslint-disable no-console */

const scopes_defaults = require("../scopes/defaults.json");

exports.seed = (knex) =>
	// UPDATE DEFAULTS SCOPES
	// USERGRUOPS
	Promise.all(
		scopes_defaults.user_groups.map((userGroup) => {
			return GetAndCreateUserGroupIfNotExist(userGroup.name, knex).then((userGroupEntity) => {
				return updateScopeUserGroup(userGroupEntity.id, userGroup.scopes, knex);
			});
		}),
	)
		.then(() => {
			return knex("profile").select();
		})
		.then((profiles) => {
			const transversal_scopes = scopes_defaults.profiles.find((p) => p.name === "*");
			return Promise.all(
				profiles.map((profile) => {
					const defaultProfile = scopes_defaults.profiles.find((p) => p.name === profile.name);
					const scopesOfProfile = [];
					scopesOfProfile.push(...transversal_scopes.scopes);

					if (defaultProfile) {
						scopesOfProfile.push(...defaultProfile.scopes);
					}
					return updateScopeProfile(profile.id, scopesOfProfile, knex);
				}),
			);
		});

function GetProfile(name, knex) {
	return knex("profile")
		.select()
		.where("name", "=", name)
		.then((profile) => {
			if (profile.length > 0) {
				return profile[0];
			}
			return null;
		});
}

function updateScopeProfile(profile_id, scopes, knex) {
	// CREATE THE SCOPES
	const create_subquery = knex("profile_scope").select("scope_id").where("profile_id", profile_id);

	return knex("scope")
		.select("id")
		.whereIn("permission", scopes)
		.andWhere("id", "not in", create_subquery)
		.then((scopes_to_create) => {
			//console.log("scopes_to_create");
			//console.log(scopes_to_create);
			return knex("profile_scope").insert(
				scopes_to_create.map((stc) => ({ profile_id, scope_id: stc["id"] })),
			);
		});
}

function GetAndCreateUserGroupIfNotExist(name, knex) {
	return knex("user_group")
		.select()
		.where("name", "=", name)
		.then((user_group) => {
			if (user_group.length > 0) {
				return user_group[0];
			}

			return knex("user_group")
				.insert(
					{
						name,
					},
					"*",
				)
				.then((relation) => {
					return relation[0];
				});
		});
}

function updateScopeUserGroup(user_group_id, scopes, knex) {
	//console.log("user_group_id:", user_group_id);
	// CREATE THE SCOPES
	const create_subquery = knex("user_group_scope")
		.select("scope_id")
		.where("user_group_id", user_group_id);

	return knex("scope")
		.select("id")
		.whereIn("permission", scopes)
		.andWhere("id", "not in", create_subquery)
		.then((scopes_to_create) => {
			//console.log("scopes_to_create");
			//console.log(scopes_to_create);
			return knex("user_group_scope").insert(
				scopes_to_create.map((stc) => ({ user_group_id, scope_id: stc["id"] })),
			);
		})
		.then(() => {
			// DELETE THE SCOPES
			const delete_subquery = knex("scope").select("id").whereIn("permission", scopes);

			return knex("user_group_scope")
				.select("scope_id")
				.where("user_group_id", user_group_id)
				.andWhere("scope_id", "not in", delete_subquery);
		}) /*.then(scopes_to_delete => {
			return knex("user_group_scope").delete().where("scope_id", "in", scopes_to_delete.map(std => std["scope_id"]));
		})*/;
}
