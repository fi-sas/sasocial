exports.seed = (knex) =>
	knex("user")
		.select()
		.where("user_name", "admin")
		.then(async (userRows) => {
			if (userRows.length === 1) {
				const userGroupAdmin = await knex("user_group").select().where("name", "Admin");
				const userUserGroupAdmin = await knex("user_user_group")
					.select()
					.where("user_id", "=", userRows[0].id)
					.andWhere("user_group_id", "=", userGroupAdmin[0].id);

				if (userGroupAdmin.length === 1 && userUserGroupAdmin.length < 1) {
					const userUserGroup = {
						user_id: userRows[0].id,
						user_group_id: userGroupAdmin[0].id,
					};
					await knex("user_user_group").insert(userUserGroup);
				}
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
