exports.seed = (knex) =>
	knex("user")
		.select()
		.where("user_name", "guest")
		.then(async (userRows) => {
			if (userRows.length === 1) {
				return knex("scope")
					.select()
					.whereIn("permission", [
						"communication:posts:medias-tv",
						"communication:posts:medias-kiosk",
						"communication:posts:tickers",
						"communication:posts:events",
						"communication:posts:mobile",
						"reports:reports:list",
						"reports:reports:read"
					])
					.then((scopes) => {
						return Promise.all(
							scopes.map((sc) => {
								return knex("user_scope")
									.select()
									.where("scope_id", "=", sc.id)
									.andWhere("user_id", "=", userRows[0].id)
									.then((user_scope) => {
										if (user_scope.length > 0) {
											return true;
										}
										return knex("user_scope").insert({
											scope_id: sc.id,
											user_id: userRows[0].id,
										});
									});
							}),
						);
					});
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
