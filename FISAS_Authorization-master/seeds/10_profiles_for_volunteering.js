exports.seed = (knex) =>
	knex("profile")
		.select()
		.where("name", "Externo - Voluntariado")
		.then(async (rows) => {
			if (rows.length === 0) {
				return knex("profile").insert({
					name: "Externo - Voluntariado",
					active: true,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
