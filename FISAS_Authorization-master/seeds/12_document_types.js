exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			active: true,
			translations: [
				{
					description: "Bilhete de Identidade",
					document_type_id: 1,
					language_id: 3
				},
				{
					description: "Identity Card",
					document_type_id: 1,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 2,
			active: true,
			translations: [
				{
					description: "Cartão de Cidadão",
					document_type_id: 2,
					language_id: 3
				},
				{
					description: "Citizen Card (Portuguese CC)",
					document_type_id: 2,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 3,
			active: true,
			translations: [
				{
					description: "Passaporte",
					document_type_id: 3,
					language_id: 3
				},
				{
					description: "Passport",
					document_type_id: 3,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 4,
			active: true,
			translations: [
				{
					description: "Bilhete de Identidade Estrangeiro",
					document_type_id: 4,
					language_id: 3
				},
				{
					description: "Foreign Identity Card",
					document_type_id: 4,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 5,
			active: true,
			translations: [
				{
					description: "Bilhete de Identidade Militar",
					document_type_id: 5,
					language_id: 3
				},
				{
					description: "Military Identity Card",
					document_type_id: 5,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 6,
			active: true,
			translations: [
				{
					description: "Cartão de Residência permanente da UE",
					document_type_id: 6,
					language_id: 3
				},
				{
					description: "UE permanent Residence Card",
					document_type_id: 6,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 7,
			active: true,
			translations: [
				{
					description: "Certificado de registo da UE",
					document_type_id: 7,
					language_id: 3
				},
				{
					description: "UE registration certificate",
					document_type_id: 7,
					language_id: 4
				}
			],
			external_ref: null,
			created_at: new Date(),
			updated_at: new Date()
		},
	];

	return Promise.all(
		data.map(async (d) => {
			const rows = await knex("document_type").select().where("id", d.id);
			if (rows.length === 0) {
				const translations = d.translations;
				delete d.translations;
				const dt = await knex("document_type").insert(d);
				for (const tr of translations) {
					const translation = await knex("document_type_translation").insert(tr);
				}
			}
			return true;
		}),
	);
};
