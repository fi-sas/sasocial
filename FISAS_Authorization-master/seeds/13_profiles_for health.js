exports.seed = (knex) =>
	knex("profile")
		.select()
		.where("name", "Externo Saude")
		.then(async (rows) => {
			if (rows.length === 0) {
				return knex("profile").insert({
					name: "Externo Saude",
					active: true,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
			return true;
		})
		.catch((err) => {
			// eslint-disable-next-line no-console
			console.error(err);
		});
