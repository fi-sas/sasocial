"use strict";
const { Errors } = require("@fisas/ms_core").Helpers;
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const TokenExpiredError = jwt.TokenExpiredError;
const ms = require("ms");
const moment = require("moment");

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "authorization.authorize",

	/**
	 * Settings
	 */
	settings: {
		domain: process.env.DOMAIN || "sasocial",

		token_expires_in: process.env.JWT_TOKEN_EXPIRES_IN || "15m",
		token_secret: process.env.JWT_SECRET_KEY || "fisas_secret_jwt_key",

		refresh_token_expires_in: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN || "90d",
		refresh_token_secret: process.env.JWT_REFRESH_SECRET_KEY || "fisas_refresh_secret_jwt_key",

		use_middleware: process.env.USE_MIDDLEWARE == "true" || process.env.USE_MIDDLEWARE == true,
		use_sso: process.env.USE_SSO == "true" || process.env.USE_SSO == true,

		// IN MINUTES
		check_login_attempts_interval: process.env.CHECK_LOGIN_ATTEMPTS_INTERVAL || 5,
		check_login_attempts_limit: process.env.CHECK_LOGIN_ATTEMPTS_LIMIT || 10,
	},

	mixins: [],
	/**
	 * Dependencies
	 */
	dependencies: ["configuration.devices"],

	/**
	 * Actions
	 */
	actions: {
		loginByDevice: {
			authorization: false,
			authentication: false,
			visibility: "published",
			rest: {
				method: "POST",
				path: "/device/:deviceId",
			},
			params: {
				deviceId: { type: "number", convert: true },
				email: { type: "email", normalize: true, optional: true },
				user_name: { type: "string", lowercase: true, trim: true, optional: true },
				pin: { type: "string", optional: true },
				password: { type: "string", optional: true },
				rfid: { type: "string", optional: true },
			},
			async handler(ctx) {
				let device = await ctx.call("configuration.devices.get", {
					id: ctx.params.deviceId,
					withRelated: false,
				});
				device = Array.isArray(device) ? device[0] : device;

				if (device.active === false) {
					throw new Errors.UnauthorizedError("Device not active", "DEVICE_NOT_ACTIVE", {
						device: "deviceId",
					});
				}

				// CHECK ATTEMPTS
				const remaining_attempts = await this.checkAttempts(ctx);

				return this.performeAuthFlow(ctx, ctx.params, device)
					.then((res) => {
						if (ctx.params.email || ctx.params.user_name || ctx.params.rfid) {
							ctx.call("authorization.login_attempts.create", {
								entity: null,
								user_id: res.user.id,
								device_id: res.device.id,
								success: true,
							});
						}
						return res.result;
					})
					.catch((err) => {
						ctx.call("authorization.login_attempts.create", {
							entity: ctx.params.email || ctx.params.user_name || ctx.params.rfid,
							cause: err.type,
							user_id: err.data ? err.data.user_id : null,
							device_id: device.id,
							success: false,
						});
						err.data = {};
						if (remaining_attempts !== null) {
							err.data["remaining_attempts"] = remaining_attempts;
						}
						throw err;
					});
			},
		},
		loginByDeviceType: {
			authorization: false,
			authentication: false,
			visibility: "published",
			rest: {
				authorization: false,
				method: "POST",
				path: "/device-type/:type",
			},
			params: {
				type: { type: "string" },
				email: { type: "email", normalize: true, optional: true },
				user_name: { type: "string", lowercase: true, optional: true },
				pin: { type: "string", optional: true },
				password: { type: "string", optional: true },
				rfid: { type: "string", optional: true },
			},
			async handler(ctx) {
				const supportedDeviceTypes = ["MOBILE", "BO", "WEB", "POS", "GENERIC"];

				if (!supportedDeviceTypes.includes(ctx.params.type))
					throw new Errors.UnauthorizedError(
						`The device type provided cannot be authorized using this endpoint (only available for [${supportedDeviceTypes}]). Please use the endpoint /api/v1/authorize/device/{id} (providing the ID of the device you are trying to authenticate).`,
						"INVALID_AUTH_DEVICE_TYPE",
						{},
					);

				const devices = await ctx.call("configuration.devices.find", {
					query: { type: ctx.params.type },
				});

				if (!devices || devices.length === 0) {
					throw new Errors.EntityNotFoundError("devices", ctx.params.type);
				}

				if (devices[0].active === false) {
					throw new Errors.UnauthorizedError("Device not active", "DEVICE_NOT_ACTIVE", {
						device: "deviceId",
					});
				}

				// CHECK ATTEMPTS
				const remaining_attempts = await this.checkAttempts(ctx);

				return this.performeAuthFlow(ctx, ctx.params, devices[0])
					.then((res) => {
						if (ctx.params.email || ctx.params.user_name || ctx.params.rfid) {
							ctx.call("authorization.login_attempts.create", {
								entity: null,
								user_id: res.user.id,
								device_id: res.device.id,
								success: true,
							});
						}
						return res.result;
					})
					.catch((err) => {
						ctx.call("authorization.login_attempts.create", {
							entity: ctx.params.email || ctx.params.user_name || ctx.params.rfid,
							cause: err.type,
							user_id: err.data ? err.data.user_id : null,
							device_id: devices[0].id,
							success: false,
						});
						err.data = {};
						if (remaining_attempts !== null) {
							err.data["remaining_attempts"] = remaining_attempts;
						}
						throw err;
					});
			},
		},
		validateToken: {
			authorization: false,
			authentication: false,
			visibility: "published",
			rest: {
				method: "POST",
				path: "/validate-token",
			},
			cache: {
				keys: ["token", "#showCPUID"],
				ttl: 120,
			},
			scope: "authorization:validate-token:create",
			params: {
				token: { type: "string" },
			},
			async handler(ctx) {
				let tokenInfo = null;
				try {
					tokenInfo = jwt.verify(ctx.params.token, this.settings.token_secret, {
						algorithm: "HS256",
					});
				} catch (err) {
					if (err instanceof TokenExpiredError) {
						throw new Errors.UnauthorizedError("Token expired", "ERR_TOKEN_EXPIRED");
					}

					throw new Errors.UnauthorizedError("Token invalid", "ERR_INVALID_TOKEN");
				}

				if (!tokenInfo.user || !tokenInfo.user.id) {
					return;
				}

				let oTokenDevice = Array.isArray(tokenInfo.device) ? tokenInfo.device[0] : tokenInfo.device;

				const device = await ctx.call("configuration.devices.get", {
					id: oTokenDevice.id,
					withRelated: false,
				});
				const user = await this.findUser(ctx, "id", tokenInfo.user.id);
				const scopes = await ctx.call("authorization.scopes.findAllUserAvailableScopesStrings", {
					user_id: user.id,
					profile_id: user.profile_id,
				});

				// ADD SOME INFO FROM SCOPES
				if (Array.isArray(scopes)) {
					user.is_student = scopes.includes("sasocial:is_student");
					user.is_employee = scopes.includes("sasocial:is_employee");
				}

				return { user, scopes, device: device[0], isGuest: tokenInfo.isGuest };
			},
		},
		refreshToken: {
			visibility: "published",
			authentication: false,
			cache: {
				enabled: false,
			},
			rest: {
				method: "POST",
				path: "/refresh-token/:type",
			},
			params: {
				type: { type: "string" },
			},
			async handler(ctx) {
				const refreshToken = this.getCookieRefreshToken(ctx);

				if (ctx.meta.$cookies && refreshToken) {
					return ctx
						.call("authorization.tokens.findByDeviceTypeTokenHashOrToken", {
							token: refreshToken,
							device_type: ctx.params.type,
						})
						.then(async (tokens) => {
							if (tokens.length === 0) {
								throw new Errors.UnauthorizedError(
									"No token refresh founded",
									"ERR_NO_REFRESH_TOKEN",
								);
							}

							let refreshTokenInfo = null;
							try {
								refreshTokenInfo = jwt.verify(refreshToken, this.settings.refresh_token_secret, {
									algorithm: "HS256",
								});
							} catch (err) {
								if (err instanceof TokenExpiredError) {
									throw new Errors.UnauthorizedError(
										"Refresh token expired",
										"ERR_REFRESH_TOKEN_EXPIRED",
									);
								}
								throw new Errors.UnauthorizedError(
									"Refresh token invalid",
									"ERR_REFRESH_INVALID_TOKEN",
								);
							}

							if (!refreshTokenInfo.user || !refreshTokenInfo.user.id) {
								return;
							}

							let oTokenDevice = Array.isArray(refreshTokenInfo.device)
								? refreshTokenInfo.device[0]
								: refreshTokenInfo.device;
							const device = await ctx.call("configuration.devices.get", {
								id: oTokenDevice.id,
								withRelated: false,
							});
							const user = await this.findUser(ctx, "id", refreshTokenInfo.user.id);
							return this.generateTokenFromUser(ctx, {
								user,
								device: device[0],
							}).then((result) => {
								ctx.call("authorization.tokens.remove", { id: tokens[0].id });
								return result;
							});
						});
				}

				throw new Errors.UnauthorizedError("No token refresh founded", "ERR_NO_REFRESH_TOKEN");
			},
		},
		loggedUser: {
			visibility: "published",
			cache: {
				enabled: false,
			},
			rest: {
				method: "GET",
				path: "/user",
			},
			async handler(ctx) {
				return ctx.meta.user;
			},
		},
		/**
		 * SSO
		 */
		sso_metadata: {
			authorization: false,
			authentication: false,
			visibility: "published",
			rest: {
				method: "GET",
				path: "/sso/metadata",
			},
			async handler(ctx) {
				if (!this.settings.use_sso)
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");
				ctx.meta.$responseType = "application/xml";
				return ctx.call("middleware.sso.metadata");
			},
		},
		sso_url: {
			authorization: false,
			authentication: false,
			visibility: "published",
			params: {
				device: { type: "enum", values: ["WEB", "BO"], default: "WEB", optional: true },
				entityID: { type: "string", optional: true },
			},
			rest: {
				method: "GET",
				path: "/sso/url",
			},
			async handler(ctx) {
				if (!this.settings.use_sso)
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");

				return ctx.call("middleware.sso.url", ctx.params);
			},
		},
		sso_acs: {
			authorization: false,
			authentication: false,
			visibility: "published",
			rest: {
				method: "POST",
				path: "/sso/acs",
			},
			async handler(ctx) {
				if (!this.settings.use_sso)
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");

				return ctx
					.call("middleware.sso.acknowledge", ctx.params)
					.then(async (ack) => {
						/**
						 * ack = {
						 *	 device_type: "WEB",
						 *	 user: "esadsa@asdads.com"
						 *	}
						 */

						if (!ack.device_type || !ack.user.email) {
							throw new Errors.UnauthorizedError(
								"Invalid SSO Middleware Response",
								"INVALID_SSO_MIDDLEWARE_RESPONSE",
								{},
							);
						}

						const devices = await ctx.call("configuration.devices.find", {
							query: { type: ack.device_type },
						});

						if (!devices || devices.length === 0) {
							throw new Errors.EntityNotFoundError("devices", ctx.params.type);
						}

						if (devices[0].active === false) {
							throw new Errors.UnauthorizedError("Device not active", "DEVICE_NOT_ACTIVE", {
								device: "deviceId",
							});
						}

						// TODO: SHOULD SEND USER TO AUTH ERROR PAGE
						if (ack.device_type == "BO" && !ack.user.can_access_BO) {
							ctx.meta.$statusCode = 302;
							ctx.meta.$location = `${process.env.SSO_REDIRECT}/backoffice`;
							return;
						}

						const tokenData = await this.generateTokenFromUser(ctx, {
							user: ack.user,
							device: devices[0],
						});

						// 2 minutes for use the token
						ctx.meta.$responseHeaders["Set-Cookie"].push(
							`authToken=${tokenData.token}; Expires=${new Date(
								new Date().getTime() + 1000 * 120,
							).toUTCString()};Domain=${this.settings.domain};Path=/`,
							`authFirstLogin=${ack.user.first_login}; Expires=${new Date(
								new Date().getTime() + 1000 * 120,
							).toUTCString()};Domain=${this.settings.domain};Path=/`,
						);

						switch (ack.device_type) {
							case "BO":
								ctx.meta.$statusCode = 302;
								ctx.meta.$location = `${process.env.SSO_REDIRECT}/backoffice`;
								return;
							default:
								ctx.meta.$statusCode = 302;
								ctx.meta.$location = process.env.SSO_REDIRECT;
								return;
						}
					})
					.catch((error) => {
						this.logger.error("AUTHENTICATION ERROR");
						this.logger.error(error);
						ctx.meta.$responseHeaders = {
							"Set-Cookie": [
								`authError=${error.message
									? error.message
									: "There was an error in the process, please try again."
								}; Expires=${new Date(new Date().getTime() + 1000 * 120).toUTCString()};Domain=${this.settings.domain
								};Path=/`,
							],
						};
						ctx.meta.$statusCode = 302;
						ctx.meta.$location = process.env.SSO_ERROR_REDIRECT;
						return;
					});
			},
		},
		logout: {
			visibility: "published",
			authorization: false,
			authentication: false,
			cache: {
				enabled: false,
			},
			rest: {
				method: "GET",
				path: "/logout/:type",
			},
			params: {
				type: { type: "string" },
			},
			async handler(ctx) {
				const refreshToken = this.getCookieRefreshToken(ctx);
				if (ctx.meta.$cookies && refreshToken) {
					return ctx
						.call("authorization.tokens.findByDeviceTypeTokenHashOrToken", {
							token: refreshToken,
							device_type: ctx.params.type,
						})
						.then(async (tokens) => {
							if (tokens.length === 0) {
								return {};
							}

							return ctx.call("authorization.tokens.remove", { id: tokens[0].id }).catch(() => {
								return {};
							});
						})
						.then(() => {
							// Delete Cookie of refresh token
							ctx.meta.$responseHeaders = {
								"Set-Cookie": [
									`refreshToken${ctx.params.type}=null;Expires=${new Date(
										0,
									).toUTCString()};HttpOnly;Domain=${this.settings.domain};Path=/;SameSite=None;`,
								],
							};

							if (this.settings.use_sso) {
								return ctx
									.call("middleware.sso.logout", ctx.params)
									.then((res) => {
										ctx.meta.$statusCode = 302;
										ctx.meta.$location = res;
										return {};
									})
									.catch((err) => {
										return {};
									});
							}

							return {};
						});
				}

				return {};
			},
		},
	},

	/**
	 * Methods
	 */
	methods: {
		getCookieRefreshToken(ctx) {
			const type = ctx.params.type;
			const possibleKeyName = `refreshToken${type}`;

			if (ctx.meta.$cookies) {
				const cookieKeys = Object.keys(ctx.meta.$cookies);
				const possibleKey = cookieKeys.find((k) => k === possibleKeyName);

				if (possibleKey) {
					return ctx.meta.$cookies[possibleKey];
				}
			}

			return null;
		},
		findUser(ctx, prop, value) {
			const params = {
				withRelated: "profile",
				query: {},
			};
			params["query"][prop] = value;

			// CEHCK RFID_VALIDITY
			if (prop === "rfid") {
				//params["query"]["rfid_validity"] = value; TODO
			}

			return ctx.call("authorization.users.find", params).then((res) => {
				if (Array.isArray(res) && res.length > 0) {
					return res[0];
				} else {
					throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
				}
			});
		},
		async performeAuthFlow(ctx, params, device) {
			this.validateEmailUsername(params);
			this.validatePinPassword(params);
			this.validateRFID(params);

			let valid = false;
			let user = null;

			/**
			 * ADD DOC
			 */
			if (this.settings.use_middleware == true && params.email && params.password) {
				/**
				 * ADD DOC 27102
				 */
				valid = await ctx.call("middleware.authorization.validate", {
					email: params.email,
					password: params.password,
				});

				if (!valid) {
					throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
				}
				// GET THE USER
				user = await this.findUser(ctx, "email", params.email);
			} else {
				// Case 1: email + password combo
				if (params.email && params.password) {
					user = await this.findUser(ctx, "email", params.email);

					valid = await ctx.call("authorization.users.checkPassword", {
						user_id: user.id,
						password: ctx.params.password,
					});

					if (!valid) {
						throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {
							user_id: user.id,
						});
					}
				}
			}

			if (!valid) {
				// Case 2: email + pin combo
				if (params.email && params.pin) {
					user = await this.findUser(ctx, "email", params.email);

					valid = await ctx.call("authorization.users.checkPin", {
						user_id: user.id,
						pin: ctx.params.pin,
					});

					if (!valid) {
						throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {
							user_id: user.id,
						});
					}
				}
			}

			if (!valid) {
				// Case 3: user_name + password combo
				if (params.user_name && params.password) {
					user = await this.findUser(ctx, "user_name", params.user_name);

					valid = await ctx.call("authorization.users.checkPassword", {
						user_id: user.id,
						password: ctx.params.password,
					});

					if (!valid) {
						throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {
							user_id: user.id,
						});
					}
				}
			}

			if (!valid) {
				// Case 4: user_name + pin combo
				if (params.user_name && params.pin) {
					user = await this.findUser(ctx, "user_name", params.user_name);

					valid = await ctx.call("authorization.users.checkPin", {
						user_id: user.id,
						pin: ctx.params.pin,
					});

					if (!valid) {
						throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {
							user_id: user.id,
						});
					}
				}
			}

			if (!valid) {
				// Case 5: RFID
				if (params.rfid) {
					user = await this.findUser(ctx, "rfid", params.rfid);
					valid = await ctx.call("authorization.users.checkPin", {
						user_id: user.id,
						pin: ctx.params.pin,
					});

					if (!valid) {
						throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {
							user_id: user.id,
						});
					}

					if (user.rfid_validity && moment(user.rfid_validity).isBefore()) {
						throw new Errors.UnauthorizedError("RFIF expired", "AUTH_RFID_EXPIRED", {});
					}
				}
			}

			if (!valid) {
				// Case 6: simple token for device
				user = undefined;
				valid = true;
			}

			if (user && !user.active) {
				throw new Errors.UnauthorizedError("Your account is not active", "AUTH_DISABLED_ACCOUNT", {
					user_id: user.id,
				});
			}

			if (user && !user.account_verified) {
				throw new Errors.UnauthorizedError(
					"Please validate your email before login",
					"AUTH_ACCOUNT_NOT_VERIFIED",
					{
						user_id: user.id,
					},
				);
			}

			if (user && user.password_expire_on && moment(user.password_expire_on).isBefore()) {
				throw new Errors.UnauthorizedError(
					"Password expired please contact the administrator.",
					"USER_PASSWORD_EXPIRED",
					{
						user_id: user.id,
					},
				);
			}

			if (device.type === "BO") {
				// Addition authorization level: can user ccess BO?
				if (!user || !user.can_access_BO) {
					throw new Errors.ForbiddenError(
						"no permission to access backoffice",
						"ERR_NO_ACCESS_BACKOFFICE",
						{
							user_id: user.id,
						},
					);
				}
			}

			const result = await this.generateTokenFromUser(ctx, {
				user,
				device,
			});

			return { result, user, device };
		},
		async generateTokenFromUser(ctx, { user, device }) {
			// Set default data in the token payload
			let payload = {
				user: {
					id: user ? user.id : null,
				},
				device: {
					id: device.id,
				},
				deviceId: device.id,
				//device,
			};
			let isGuest;

			// Check if we are handling a concrete user or a guest user
			if (user && user.user_name !== "guest") {
				isGuest = false;
				payload.isGuest = isGuest;
			} else {
				// Find anonymous user in the database
				// eslint-disable-next-line no-param-reassign
				if (!user) {
					user = await this.findUser(ctx, "user_name", "guest");
				}
				isGuest = true;
				payload.isGuest = isGuest;
				payload.user = {
					id: user.id,
				};
			}

			// Generate the token
			const expires_in_ms = ms(this.settings.token_expires_in);
			const expires_in = new Date(new Date().getTime() + expires_in_ms);
			const token = jwt.sign(payload, this.settings.token_secret, {
				expiresIn: this.settings.token_expires_in,
				algorithm: "HS256",
				subject: user.name,
				issuer: this.settings.domain,
				audience: device.type,
				notBefore: "-1m",
			});

			// Generate a refresh token
			const token_date_expires = new Date(
				new Date().getTime() + ms(this.settings.refresh_token_expires_in),
			);

			const refreshToken = jwt.sign(payload, this.settings.refresh_token_secret, {
				expiresIn: this.settings.refresh_token_expires_in,
				algorithm: "HS256",
				subject: user.name,
				issuer: this.settings.domain,
				audience: device.type,
				notBefore: "-1m",
			});

			// Set Cookie with refresh token
			ctx.meta.$responseHeaders = {
				"Set-Cookie": [
					`refreshToken${device.type
					}=${refreshToken};Expires=${token_date_expires.toUTCString()};HttpOnly;Domain=${this.settings.domain
					};Path=/;Secure;SameSite=${["BO", "WEB"].includes(device.type) ? 'Strict' : 'None'};`,
				],
			};

			ctx.call("authorization.tokens.create", {
				user_id: user.id,
				device_type: device.type,
				token: crypto.createHash("md5").update(refreshToken).digest("hex"),
				expire_at: token_date_expires,
			});

			return { token, expires_in, expires_in_ms };
		},
		validateEmailUsername(params) {
			if (
				// We have email or user_name
				(params.email || params.user_name) &&
				// We do not have password nor pin
				!params.password &&
				!params.pin
			) {
				throw new Errors.ValidationError(
					"'password' or 'pin' are required if an email/user_name is provided.",
					"AUTH_PASSWORD_OR_PIN_REQUIRED",
				);
			}
		},
		validatePinPassword(params) {
			if (
				// We have password or pin
				(params.password || params.pin) &&
				// We do not have email or user_name
				!params.email &&
				!params.user_name &&
				!params.rfid
			) {
				throw new Errors.ValidationError(
					"'email' or 'user_name' are required if an pin/password is provided.",
					"AUTH_EMAIL_OR_USERNAME_REQUIRED",
				);
			}
		},
		validateRFID(params) {
			if (
				// We have rfid
				params.rfid &&
				// We do not pin
				!params.pin
			) {
				throw new Errors.ValidationError(
					"'pin' are required if an rfid is provided.",
					"AUTH_PIN_REQUIRED",
				);
			}
		},
		async checkAttempts(ctx) {
			if (ctx.params.email || ctx.params.user_name || ctx.params.rfid) {
				const attemps = await ctx.call("authorization.login_attempts.count", {
					query: {
						entity: ctx.params.email || ctx.params.user_name || ctx.params.rfid,
						success: false,
						created_at: {
							gte: moment()
								.subtract(this.settings.check_login_attempts_interval, "minutes")
								.format("yyyy-MM-DD HH:mm:ss"),
						},
					},
				});
				if (attemps >= +this.settings.check_login_attempts_limit - 1) {
					throw new Errors.UnauthorizedError(
						"Too many login attemps",
						"ERR_TOO_MANY_LOGIN_ATTEMPTS",
						{
							attemps_fail_limit: +this.settings.check_login_attempts_limit,
							attemps_failed: attemps + 1,
							retry_in_minutes: this.settings.check_login_attempts_interval,
						},
					);
				}

				return +this.settings.check_login_attempts_limit - (attemps + 1);
			}

			return null;
		},
		/**
		 * Clear cached entities
		 *
		 * @methods
		 * @returns {Promise}
		 */
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Events
	 */
	events: {
		"authorization.*"(ctx) {
			this.logger.info("CAPTURED EVENT => authorization");
		},
		"authorization.users.updated"(ctx) {
			this.logger.info("CAPTURED EVENT => authorization.users.updated");
			this.clearCache();
		},
		"authorization.user-groups-scopes.created"(ctx) {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.created");
			this.clearCache();
		},
		"authorization.user-groups-scopes.updated"(ctx) {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.updated");
			this.clearCache();
		},
		"authorization.user-groups-scopes.removed"(ctx) {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.removed");
			this.clearCache();
		},
		"authorization.users-scopes.created"(ctx) {
			this.logger.info("CAPTURED EVENT => users-scopes.created");
			this.clearCache();
		},
		"authorization.users-scopes.updated"(ctx) {
			this.logger.info("CAPTURED EVENT => users-scopes.updated");
			this.clearCache();
		},
		"authorization.users-scopes.removed"(ctx) {
			this.logger.info("CAPTURED EVENT => users-scopes.removed");
			this.clearCache();
		},
		"authorization.users-user-groups.created"(ctx) {
			this.logger.info("CAPTURED EVENT => users-user-groups.created");
			this.clearCache();
		},
		"authorization.users-user-groups.updated"(ctx) {
			this.logger.info("CAPTURED EVENT => users-user-groups.updated");
			this.clearCache();
		},
		"authorization.users-user-groups.removed"(ctx) {
			this.logger.info("CAPTURED EVENT => users-user-groups.removed");
			this.clearCache();
		},
		"configuration.devices.cpuid_updated"(ctx) {
			this.logger.info("CAPTURED EVENT => configuration.devices.updated");
			this.clearCache();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
