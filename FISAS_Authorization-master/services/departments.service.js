"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;



/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.departments",
	table: "department",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "departments")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "organic_unit_id", "active", "created_at", "updated_at"],
		defaultWithRelateds: [
			"organic_unit"
		],
		withRelateds: {
			"organic_unit"(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"infrastructure.organic-units",
					"organic_unit",
					"organic_unit_id"
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			organic_unit_id: { type: "number", integer: true, convert: true },
			active: { type: "boolean", convert: true, default: true }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateOrganicUnit",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateOrganicUnit",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validateOrganicUnit",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateOrganicUnit(ctx) {
			if (ctx.params.organic_unit_id)
				await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
