"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.document-type-translations",
	table: "document_type_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "document-type-translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "description", "document_type_id", "language_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			description: { type: "string" },
			document_type_id: { type: "number", integer: true, convert: true },
			language_id: { type: "number", integer: true, convert: true }
		},
	},
	/**
	 * Hooks
	 */
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_document_type_translations: {
			params: {
				document_type_id: { type: "number", integer: true, convert: true, },
				translations: {
					type: "array", items: {
						type: "object", props: {
							language_id: { type: "number", integer: true, convert: true, },
							description: { type: "string" },
						}
					}, min: 1
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					document_type_id: ctx.params.document_type_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translations) => ({
					document_type_id: ctx.params.document_type_id,
					description: translations.description,
					language_id: translations.language_id
				}));
				return this._insert(ctx, { entities });
			},
		},
		delete_translations: {
			params: {
				document_type_id: { type: "number", integer: true, convert: true, }
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					document_type_id: ctx.params.document_type_id,
				});
				this.clearCache();
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {


	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
