"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("moleculer").Errors;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.document-types",
	table: "document_type",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "document-types")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "external_ref", "created_at", "updated_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("authorization.document-type-translations.find", { query: { document_type_id: doc.id, language_id: ctx.meta.language_id } })
								.then((translation) => { doc.translations = translation; });
						}),
					);
				} else {
					return hasMany(docs, ctx, "authorization.document-type-translations", "translations", "id", "document_type_id",);
				}
			},
		},
		entityValidator: {
			active: { type: "boolean" },
			external_ref: { type: "string", max: 100, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			translations: {
				type: "array", items: {
					type: "object", props: {
						language_id: { type: "number", integer: true, convert: true, },
						description: { type: "string" },
					}
				}, min: 1
			}
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateLanguages"
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateLanguages"
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateLanguages"
			],
			remove: [
				"isPossibleRemove",
				"removeTranslations"
			],
			list: [
				function validateRequestOrigin(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.active = true;
					}
				},
			]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
			patch: ["saveTranslations"]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#language_id",
				],
			},
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			cache: {
				keys:
					["id", "withRelated", "fields", "mapping", "#language_id"],
			},
			visibility: "published",

		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countUsers = await ctx.call("authorization.users.count", {
					query: {
						document_type_id: ctx.params.id
					}
				});
				if (countUsers > 0) {
					throw new ValidationError("You have users with this document type associated", "AUTH_USERS_WITH_DOCUMENT_TYPE");
				}
			}
		},
		async saveTranslations(ctx, res) {
			if (ctx.params.translations) {
				res[0].translations = await ctx.call("authorization.document-type-translations.save_document_type_translations", {
					document_type_id: res[0].id,
					translations: ctx.params.translations,
				});
			}
			return res;
		},
		async validateLanguages(ctx) {
			if (Array.isArray(ctx.params.translations)) {
				for (const translation of ctx.params.translations) {
					await ctx.call("configuration.languages.get", { id: translation.language_id });
				}
			}
		},
		async removeTranslations(ctx) {
			if (ctx.params.id) {
				await ctx.call("authorization.document-type-translations.delete_translations", {
					document_type_id: ctx.params.id
				});
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
