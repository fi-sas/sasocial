"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.login_attempts",
	table: "login_attempt",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "login_attempts")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "entity", "cause", "user_id", "device_id", "success", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			entity: { type: "string", convert: true, optional: true },
			cause: { type: "string", convert: true, optional: true },
			user_id: { type: "number", convert: true, optional: true },
			device_id: { type: "number", convert: true, optional: true },
			success: { type: "boolean", convert: true, default: false },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
				},
			],
			update: [],
			patch: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
