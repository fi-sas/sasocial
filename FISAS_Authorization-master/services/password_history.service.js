"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
const Cron = require("moleculer-cron");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.password_history",
	table: "password_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "password_history"), Cron],


	/*
	 * Remove old carts
	 */
	crons: [
	],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "token", "password", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			user_id: { type: "number", positive: true },
			password: { type: "string" },
			created_at: { type: "date", optional: true },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
				}
			],
			update: [],
			patch: [],
		},
		after: {
			create: [],
			update: [],
			patch: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		checkPasswordOfUser: {
			visibility: "public",
			params: {
				hashed_password: { type: "string" },
				user_id: { type: "number", positive: true }
			},
			handler(ctx) {
				return this._count(ctx, {
					query: {
						password: ctx.params.hashed_password,
						user_id: ctx.params.user_id,
					}
				}).then(pw => {
					return pw > 0;
				});
			}
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
