"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError, ValidationError } = require("moleculer").Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.profiles",
	table: "profile",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "profiles")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "external_ref", "active", "created_at", "updated_at"],
		defaultWithRelateds: [
			//"scopes"
		],
		withRelateds: {
			scopes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.profiles-scopes.fetchScopesByProfile", { profile_id: doc.id })
							.then((res) => {
								doc.scopes = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			external_ref: { type: "string", optional: true },
			scope_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			active: { type: "boolean" },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateScopes",
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateScopes",
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateScopes",
			],
			remove: [
				"isPossibleRemove"
			]
		},
		after: {
			create: ["saveScopes"],
			update: ["saveScopes"],
			patch: ["saveScopes"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countUsers = await ctx.call("authorization.users.count", {
					query: {
						profile_id: ctx.params.id
					}
				});

				if (countUsers > 0) {
					throw new ValidationError("You have users with this profile associated", "AUTH_PROFILE_WITH_USERS");
				}
			}
		},
		async saveScopes(ctx, response) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];
				await ctx.call("authorization.profiles-scopes.create_scopes_profiles", {
					profile_id: response[0].id,
					scopes_ids,
				});
				response[0].scopes = await ctx.call("authorization.profiles-scopes.fetchScopesByProfile", {
					profile_id: response[0].id,
				});
			}
			return response;
		},
		async validateScopes(ctx) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];

				const scopes = await ctx.call("authorization.scopes.count", {
					query: {
						id: scopes_ids,
					},
				});

				if (scopes < scopes_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}
			} else {
				ctx.scope_ids = [];
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
