"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.profiles-scopes",
	table: "profile_scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "profiles-scopes")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "scope_id", "profile_id"],
		withRelateds: {},
	},

	/**
	 * Hooks
	 */
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		fetchScopesByProfile: {
			visibility: "published",
			rest: {
				path: "GET /profiles/:profile_id/scopes",
			},
			params: {
				profile_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						profile_id: ctx.params.profile_id,
					},
				}).then((res) => {
					return ctx.call("authorization.scopes.find", {
						withRelated: false,
						query: {
							id: res.map((sc) => sc.scope_id),
						},
					});
				});
			},
		},
		fetchProfilesByScope: {
			visibility: "published",
			rest: {
				path: "GET /scopes/:scope_id/profiles",
			},
			params: {
				scope_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						scope_id: ctx.params.scope_id,
					},
				}).then((res) => {
					return ctx.call("authorization.profiles.find", {
						query: {
							id: res.map((usc) => usc.profile_id),
						},
					});
				});
			},
		},
		create_profiles_scopes: {
			params: {
				scope_id: {
					type: "number",
					convert: true,
				},
				profile_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					scope_id: ctx.params.scope_id,
				});
				this.clearCache();

				const entities = ctx.params.profile_ids.map((profile_id) => ({
					scope_id: ctx.params.scope_id,
					profile_id,
				}));
				return this._insert(ctx, {
					entities,
				});
			},
		},
		create_scopes_profiles: {
			params: {
				profile_id: {
					type: "number",
					convert: true,
				},
				scopes_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					profile_id: ctx.params.profile_id,
				});
				this.clearCache();

				const entities = ctx.params.scopes_ids.map((scope_id) => ({
					profile_id: ctx.params.profile_id,
					scope_id,
				}));
				return this._insert(ctx, {
					entities,
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	async created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
