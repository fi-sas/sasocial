"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {
	EntityNotFoundError,
	MoleculerError,
	ValidationError,
} = require("@fisas/ms_core").Helpers.Errors;
const _ = require("lodash");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.scopes",
	table: "scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "scopes")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "parent_id", "grants", "name", "permission"],
		defaultWithRelateds: ["parent"],
		withRelateds: {
			parent(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.scopes.find", {
								query: {
									id: doc.parent_id,
								},
							})
							.then((res) => (doc.parent = res[0]));
					}),
				);
			},
			profiles(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						ctx
							.call("authorization.profiles-scopes.fetchProfilesByScope", {
								scope_id: doc.id,
							})
							.then((res) => (doc.profiles = res));
					}),
				);
			},
			user_groups(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						ctx
							.call("authorization.user-groups-scopes.user_groups_of_scope", {
								scope_id: doc.id,
							})
							.then((res) => (doc.user_groups = res));
					}),
				);
			},
		},
		entityValidator: {
			parent_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
			},
			name: {
				type: "string",
				max: 120,
			},
			permission: {
				type: "string",
				max: 255,
			},
			grants: {
				type: "array",
				items: {
					type: "string",
					max: 255,
				},
				optional: true,
			},
			profile_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			user_group_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: ["validateUserGroups", "validateProfiles"],
			update: ["validateUserGroups", "validateProfiles"],
			patch: ["validateUserGroups", "validateProfiles"],
			remove: ["isPossibleRemove"],
		},
		after: {
			create: ["saveUserGroups", "saveProfiles"],
			update: ["saveUserGroups", "saveProfiles"],
			patch: ["saveUserGroups", "saveProfiles"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
			handler(ctx) {
				let entity = ctx.params;
				return (
					this.validateEntity(entity)
						.then((entity) => this.removeForeignFields(entity))
						.then((entity) => {
							entity.grants = JSON.stringify(entity.grants);
							return entity;
						})
						// Apply idField
						.then((entity) => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
						.then((entity) => this.adapter.insert(entity))
						.then((entity) => this.adapter.findById(entity[0]))
						.then((doc) => this.transformDocuments(ctx, {}, doc))
						.then((json) => this.entityChanged("created", json, ctx).then(() => json))
				);
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
			handler(ctx) {
				let params = ctx.params;
				let id;
				let sets = {};
				// Convert fields from params to "$set" update object
				Object.keys(params).forEach((prop) => {
					if (prop == "id" || prop == this.settings.idField) id = this.decodeID(params[prop]);
					else sets[prop] = params[prop];
				});
				return this.validateEntity(sets, false)
					.then((entity) => this.removeForeignFields(entity))
					.then((entity) => {
						entity.grants = JSON.stringify(entity.grants);
						return entity;
					})
					.then((entity) => this.adapter.updateById(id, entity))
					.then(() => this.adapter.findById(id))
					.then((doc) => {
						return doc;
					})
					.then((doc) => {
						if (!doc || (Array.isArray(doc) && doc.length === 0))
							return Promise.reject(new EntityNotFoundError("scope", id));
						return this.transformDocuments(ctx, {}, doc).then((json) =>
							this.entityChanged("updated", json, ctx).then(() => json),
						);
					});
			},
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
			handler(ctx) {
				let params = ctx.params;
				let id;
				let sets = {};
				// Convert fields from params to "$set" update object
				Object.keys(params).forEach((prop) => {
					if (prop == "id" || prop == this.settings.idField) id = this.decodeID(params[prop]);
					else sets[prop] = params[prop];
				});
				return this.validateEntity(sets, true)
					.then((entity) => this.removeForeignFields(entity))
					.then((entity) => {
						entity.grants = JSON.stringify(entity.grants);
						return entity;
					})
					.then((entity) => this.adapter.updateById(id, entity))
					.then(() => this.adapter.findById(id))
					.then((doc) => {
						return doc;
					})
					.then((doc) => {
						if (!doc || (Array.isArray(doc) && doc.length === 0))
							return Promise.reject(new EntityNotFoundError("scope", id));
						return this.transformDocuments(ctx, {}, doc).then((json) =>
							this.entityChanged("updated", json, ctx).then(() => json),
						);
					});
			},
		},

		findAllUserAvailableScopesStrings: {
			visibility: "public",
			cache: {
				keys: ["user_id", "profile_id"],
			},
			params: {
				user_id: {
					type: "number",
					convert: true,
				},
				profile_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				const allScopesRaw = await this.adapter.raw(
					`select * from (select scope.permission, scope.grants::jsonb from scope
					join profile_scope on profile_scope.scope_id = scope.id
					where profile_scope.profile_id = ?
					union
					select scope.permission, scope.grants::jsonb from scope
					join user_group_scope on user_group_scope.scope_id = scope.id
					join user_user_group on user_user_group.user_group_id = user_group_scope.user_group_id
					where user_user_group.user_id = ?
					union
					select scope.permission, scope.grants::jsonb from scope
					join user_scope on user_scope.scope_id = scope.id
					where user_scope.user_id = ?) as allScopes`,
					[ctx.params.profile_id, ctx.params.user_id, ctx.params.user_id],
				);

				let allScopes = allScopesRaw.rows;
				let scopeItems = [];
				_.forEach(allScopes, (scope) => {
					scopeItems.push(scope.permission);
					if (_.isArray(scope.grants) && scope.grants.length > 0) {
						scopeItems.push(...scope.grants);
					}
				});

				return _.uniq(scopeItems.filter((el) => el && el !== ""));
			},
		},
		userHasScope: {
			visibility: "public",
			cache: {
				keys: ["scope", "#user.id"],
			},
			params: {
				scope: {
					type: "string",
				},
			},
			async handler(ctx) {
				return ctx
					.call("authorization.scopes.findAllUserAvailableScopesStrings", {
						user_id: ctx.meta.user.id,
						profile_id: ctx.meta.user.profile_id,
					})
					.then((scopes) => {
						if (Array.isArray(scopes)) {
							return scopes.includes(ctx.params.scope);
						}
						return false;
					});
			},
		},
		postCreateScopeTree: {
			visibility: "published",
			rest: "POST /tree",
			scope: "authorization:scopes:create",
			params: {
				scopes: [
					{
						type: "object",
					},
					{
						type: "array",
					},
				],
			},
			async handler(ctx) {
				let newScopes = [];

				if (!_.has(ctx, "params.scopes")) {
					throw new MoleculerError("Params scopes is missing.");
				}

				if (_.isArray(ctx.params.scopes)) {
					// TODO - wrong array serilization - { "0": {...}}
					// https://stackoverflow.com/a/29820709
					const scopes = ctx.params.scopes;

					_.forEach(scopes, (scope) => {
						newScopes.push(scope);
					});
				} else {
					const scope = _.assign({}, ctx.params.scopes);
					newScopes.push(scope);
				}

				// Delete leafs using newScopes as remove pivots
				const removePromises = _.map(newScopes, (scope) =>
					this.adapter.removeMany({
						permission: scope.permission,
					}),
				);

				await Promise.all(removePromises);

				// Create roots
				const createPromises = _.map(newScopes, (scope) => this.createScopeTree(scope));

				await Promise.all(createPromises);

				return newScopes;
			},
		},

		getScopesTree: {
			visibility: "published",
			rest: "GET /tree",
			scope: "authorization:scopes:read",
			async handler(ctx) {
				const parentScopes = await this.fetchParentScopes();
				return Promise.all(
					_.map(parentScopes, (scope) => this.fetchRecursiveScopeTree(ctx, scope)),
				);
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"authorization.users.updated"() {
			this.logger.info("CAPTURED EVENT => authorization.users.updated");
			this.clearCache();
		},
		"authorization.user-groups-scopes.created"() {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.created");
			this.clearCache();
		},
		"authorization.user-groups-scopes.updated"() {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.updated");
			this.clearCache();
		},
		"authorization.user-groups-scopes.removed"() {
			this.logger.info("CAPTURED EVENT => user-groups-scopes.removed");
			this.clearCache();
		},
		"authorization.users-scopes.created"() {
			this.logger.info("CAPTURED EVENT => users-scopes.created");
			this.clearCache();
		},
		"authorization.users-scopes.updated"() {
			this.logger.info("CAPTURED EVENT => users-scopes.updated");
			this.clearCache();
		},
		"authorization.users-scopes.removed"() {
			this.logger.info("CAPTURED EVENT => users-scopes.removed");
			this.clearCache();
		},
		"authorization.users-user-groups.created"() {
			this.logger.info("CAPTURED EVENT => users-user-groups.created");
			this.clearCache();
		},
		"authorization.users-user-groups.updated"() {
			this.logger.info("CAPTURED EVENT => users-user-groups.updated");
			this.clearCache();
		},
		"authorization.users-user-groups.removed"() {
			this.logger.info("CAPTURED EVENT => users-user-groups.removed");
			this.clearCache();
		},
		"authorization.profiles.*"() {
			this.logger.info("CAPTURED EVENT => authorization.profiles.*");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countUsers = await ctx.call("authorization.users-scopes.count", {
					query: {
						scope_id: ctx.params.id,
					},
				});

				if (countUsers > 0) {
					throw new ValidationError(
						"You have users with this scope associated",
						"AUTH_SCOPE_WITH_USERS",
					);
				}

				const countUserGroups = await ctx.call("authorization.user-groups-scopes.count", {
					query: {
						scope_id: ctx.params.id,
					},
				});

				if (countUserGroups > 0) {
					throw new ValidationError(
						"You have user groups with this scope associated",
						"AUTH_SCOPE_WITH_USER_GROUPS",
					);
				}

				const countProfiles = await ctx.call("authorization.profiles-scopes.count", {
					query: {
						scope_id: ctx.params.id,
					},
				});

				if (countProfiles > 0) {
					throw new ValidationError(
						"You have profiles with this scope associated",
						"AUTH_SCOPE_WITH_PROFILES",
					);
				}
			}
		},
		async createScopeTree(scope) {
			if (!_.has(scope, "permission")) {
				throw new MoleculerError("permission is required", scope);
			}
			const parent = _.extend(
				{},
				{
					name: _.has(scope, "name") ? scope.name : "",
					parent_id: _.has(scope, "parent_id") ? scope.parent_id : null,
					grants: _.has(scope, "grants") && !_.isEmpty(scope.grants) ? scope.grants : [],
					permission: scope.permission,
					// profile_ids: [],
					// user_group_ids: []
				},
			);

			// TODO check why this insert is not returning id
			// const newScope = await ctx.call("authorization.scopes.insert", {entity: parent});

			const newScope = await this.adapter.db("scope").returning("id").insert(parent);

			let createPromises = [];
			if (_.has(scope, "children")) {
				createPromises = _.map(scope.children, async (child) => {
					const newborn = _.assign({}, child, {
						parent_id: newScope[0],
					});
					return this.createScopeTree(newborn);
				});
			}

			return Promise.all(createPromises);
		},
		fetchChildrenScopes(scope) {
			return this.adapter.db("scope").where("parent_id", scope.id);
		},

		fetchParentScopes() {
			return this.adapter.db("scope").whereNull("parent_id");
		},

		parseScope(scope, children) {
			return _.assign(
				{},
				{
					id: scope.id,
					name: scope.name,
					permission: scope.permission,
					grants: scope.grants && !_.isEmpty(scope.grants) ? scope.grants : [],
					children: children,
				},
			);
		},

		async fetchRecursiveScopeTree(ctx, scope) {
			// Initial state
			const children = await this.fetchChildrenScopes(scope);

			// Base case (termination condition)
			if (!children || _.isEmpty(children)) {
				return this.parseScope(scope, []);
			}

			// Recursive call
			const promises = _.map(children, async (child) => {
				return this.fetchRecursiveScopeTree(ctx, child);
			});

			// Stack exit call
			return this.parseScope(scope, await Promise.all(promises));
		},

		getScopeString(scope, item) {
			return scope[item] ? `${scope.permission}:${item}` : "";
		},

		async saveUserGroups(ctx, response) {
			if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
				const user_group_ids = [...new Set(ctx.params.user_group_ids)];
				await ctx.call("authorization.user-groups-scopes.save_groups_of_scopes", {
					scope_id: response[0].id,
					user_group_ids,
				});
				response[0].user_groups = await ctx.call(
					"authorization.user-groups-scopes.user_groups_of_scope",
					{
						scope_id: response[0].id,
					},
				);
			}
			return response;
		},

		async validateUserGroups(ctx) {
			if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
				const user_group_ids = [...new Set(ctx.params.user_group_ids)];

				const scopes = await ctx.call("authorization.user-groups.count", {
					query: {
						id: user_group_ids,
					},
				});

				if (scopes < user_group_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}
			} else {
				ctx.scope_ids = [];
			}
		},

		async saveProfiles(ctx, response) {
			if (ctx.params.profile_ids && Array.isArray(ctx.params.profile_ids)) {
				const profile_ids = [...new Set(ctx.params.profile_ids)];
				await ctx.call("authorization.profiles-scopes.create_profiles_scopes", {
					scope_id: response[0].id,
					profile_ids,
				});
				response[0].profiles = await ctx.call(
					"authorization.profiles-scopes.fetchProfilesByScope",
					{
						scope_id: response[0].id,
					},
				);
			}
			return response;
		},

		async validateProfiles(ctx) {
			if (ctx.params.profile_ids && Array.isArray(ctx.params.profile_ids)) {
				const profile_ids = [...new Set(ctx.params.profile_ids)];

				const scopes = await ctx.call("authorization.profiles.count", {
					query: {
						id: profile_ids,
					},
				});

				if (scopes < profile_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}
			} else {
				ctx.scope_ids = [];
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
