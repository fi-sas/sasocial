"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;



/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.sections",
	table: "section",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "sections")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "department_id", "active", "created_at", "updated_at"],
		defaultWithRelateds: [
			"department"
		],
		withRelateds: {
			"department"(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.departments",
					"department",
					"department_id"
				);
			},
		},
		entityValidator: {
			name: { type: "string", convert: true },
			department_id: { type: "number", integer: true, convert: true },
			active: { type: "boolean", convert: true, default: true }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateDepartment",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateDepartment",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validateDepartment",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateDepartment(ctx) {
			if (ctx.params.department_id)
				await ctx.call("authorization.departments.get", { id: ctx.params.department_id });
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
