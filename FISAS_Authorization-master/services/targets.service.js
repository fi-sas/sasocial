"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.targets",
	table: "target",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "targets")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "user_id", "active", "created_at", "updated_at"],
		defaultWithRelateds: ["conditions"],
		withRelateds: {
			conditions(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"authorization.targets_conditions",
					"conditions",
					"id",
					"target_id",
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			conditions: {
				type: "array",
				items: {
					type: "object",
					props: {
						field_name: {
							type: "enum",
							values: [
								"user_id",
								"profile_id",
								"course_degree_id",
								"course_id",
								"organic_unit_id",
								"department_id",
								"section_id",
								"active",
								"accommodation_contracted",
								"residence_id",
								"ubike_bike_assigned",
								"scholarship_application_approved",
								"volunteering_application_approved",
								"food_tickets_purchased",
							],
						},
						field_values: {
							type: "array",
							items: {
								type: "number",
								integer: true,
								convert: true,
							},
							min: 1,
						},
					},
				},
			},
			active: { type: "boolean", default: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					if (ctx.params.conditions) this.parseConditionIds(ctx);
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					if (ctx.params.conditions) this.parseConditionIds(ctx);
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					if (ctx.params.conditions) this.parseConditionIds(ctx);
				},
			],
			list: [
				function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query.user_id = ctx.meta.user.id;
				},
			],
		},
		after: {
			create: ["saveTargetConditions"],
			update: ["saveTargetConditions"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		count_users_by_target_conditions: {
			visibility: "published",
			cache: { keys: ["conditions"] },
			rest: "POST /count",
			params: {
				conditions: {
					type: "array",
					items: {
						type: "object",
						props: {
							field_name: {
								type: "enum",
								values: [
									"user_id",
									"profile_id",
									"course_degree_id",
									"course_id",
									"organic_unit_id",
									"department_id",
									"section_id",
									"active",
									"accommodation_contracted",
									"residence_id",
									"ubike_bike_assigned",
									"scholarship_application_approved",
									"volunteering_application_approved",
									"food_tickets_purchased",
								],
							},
							field_values: {
								type: "array",
								items: {
									type: "number",
									integer: true,
									convert: true,
								},
								min: 1,
							},
						},
					},
				},
			},
			async handler(ctx) {
				this.parseConditionIds(ctx);

				const user_ids = await ctx.call("authorization.users.get_users_by_target_conditions", {
					conditions: ctx.params.conditions,
				});

				return user_ids.length;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveTargetConditions(ctx, res) {
			if (ctx.params.conditions && Array.isArray(ctx.params.conditions)) {
				await ctx.call("authorization.targets_conditions.save_targets", {
					target_id: res[0].id,
					conditions: ctx.params.conditions,
				});
				res[0].conditions = await ctx.call("authorization.targets_conditions.find", {
					query: {
						target_id: res[0].id,
					},
				});
			}
			return res;
		},
		parseConditionIds(ctx) {
			for (const condition of ctx.params.conditions) {
				condition.field_value = "";
				condition.field_values.forEach((value, i) => {
					condition.field_value = condition.field_value + value;
					if (i != condition.field_values.length - 1) {
						condition.field_value = condition.field_value + ",";
					}
				});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
