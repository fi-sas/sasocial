"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
const Cron = require("moleculer-cron");
const crypto = require("crypto");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.tokens",
	table: "confirmed_token",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "confirmed_token"), Cron],

	/*
	 * Remove old tokens
	 */
	crons: [
		{
			name: "removeOldTokens",
			cronTime: "* * * * *",
			onTick: function () {
				//this.logger.info("cron saveDaylyMetrics ticked!");
				this.getLocalService("authorization.tokens")
					.actions.removeOldTokens()
					.then(() => { });
			},
			runOnInit: function () {
				//this.logger.info("cron saveDaylyMetrics created!");
			},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "token", "device_type", "expire_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			user_id: { type: "number", positive: true },
			token: { type: "string" },
			device_type: { type: "string" },
			expire_at: { type: "date" },
			created_at: { type: "date", optional: true },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
				},
			],
			update: [],
			patch: [],
		},
		after: {
			create: [],
			update: [],
			patch: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		removeTokensOfUser: {
			visibility: "public",
			params: {
				user_id: { type: "number", positive: true },
			},
			handler(ctx) {
				return this.adapter
					.removeMany((qb) => {
						return qb.where("user_id", ctx.params.user_id);
					})
					.then((res) => {
						this.clearCache();
						this.logger.info(`TICK RESULT: ${res} tokens removed!`);
					});
			},
		},
		findByDeviceTypeTokenHashOrToken: {
			params: {
				token: { type: "string" },
				device_type: { type: "string" },
			},
			handler(ctx) {
				return this._find(ctx, {
					query: (qb) => {
						qb.where("device_type", ctx.params.device_type);
						qb.where((qb) => {
							qb.where("token", crypto.createHash("md5").update(ctx.params.token).digest("hex"));
							qb.orWhere("token", ctx.params.token);
						});
					},
					limit: 1,
				});
			},
		},
		list: {
			// REST: GET /
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		removeOldTokens: {
			handler(ctx) {
				this.logger.info("TICK: Trying to clear old refresh tokens!");
				this._find(ctx, {
					query: (qb) => {
						qb.whereRaw("expire_at < NOW()");
						qb.orWhere((qbOr) => {
							qbOr.whereIn("device_type", ["KIOSK", "TV"]);
							qbOr.whereRaw("created_at < (NOW() -  interval '12 hours')");
						});
					},
				}).then((tokens) => {
					this.logger.info(`TICK: ${tokens.length} old refresh tokens found!`);
					this.adapter
						.removeMany((qb) => {
							return qb.whereIn(
								"id",
								tokens.map((token) => token.id),
							);
						})
						.then((res) => {
							this.clearCache();
							this.logger.info(`TICK RESULT: ${res} tokens removed!`);
						});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
