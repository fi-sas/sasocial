"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError, ValidationError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.user-groups",
	table: "user_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "user-groups")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name"],
		defaultWithRelateds: [
			//"scopes"
		],
		withRelateds: {
			scopes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.user-groups-scopes.fetchGroupScopes", { user_group_id: doc.id })
							.then((res) => {
								doc.scopes = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			scope_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
			},
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: ["validateScopes"],
			update: ["validateScopes"],
			patch: ["validateScopes"],
			remove: [
				"isPossibleRemove"
			]
		},
		after: {
			create: ["saveScopes"],
			update: ["saveScopes"],
			patch: ["saveScopes"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countUsers = await ctx.call("authorization.users-user-groups.count", {
					query: {
						user_group_id: ctx.params.id
					}
				});

				if (countUsers > 0) {
					throw new ValidationError("You have users with this user group associated", "AUTH_USER_GROUP_WITH_USERS");
				}
			}
		},
		async saveScopes(ctx, response) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];
				await ctx.call("authorization.user-groups-scopes.save_scopes_of_group", {
					user_group_id: response[0].id,
					scopes_ids,
				});
				response[0].scopes = await ctx.call("authorization.user-groups-scopes.fetchGroupScopes", {
					user_group_id: response[0].id,
				});
			}
			return response;
		},
		async validateScopes(ctx) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];

				const scopes = await ctx.call("authorization.scopes.count", {
					query: {
						id: scopes_ids,
					},
				});

				if (scopes < scopes_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}
			} else {
				ctx.scope_ids = [];
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
