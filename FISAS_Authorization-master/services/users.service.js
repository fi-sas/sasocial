"use strict";
const crypto = require("crypto");
const uuid = require("uuid").v4;
const { MoleculerError, ValidationError } = require("moleculer").Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const Validator = require("fastest-validator");

const moment = require("moment");
const _ = require("lodash");
const Cron = require("moleculer-cron");
const passwordGenerator = require("secure-random-password");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users",
	table: "user",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users"), Cron],

	/*
	 * Alert close expire period password and expired
	 */
	crons: [
		{
			name: "alertCloseExpired",
			cronTime: "0 6 * * *", // AT 6 AM EVERY DAY
			onTick: function () {
				this.getLocalService("authorization.users")
					.actions.notifyUsersPasswordsPeriod()
					.then(() => { });
			},
			runOnInit: function () { },
		},
	],

	/**
	 * Settings
	 */
	settings: {
		passwordRegex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$&|.+_-])[A-Za-z\d!@#$&|.+_-]{9,}$/,
		studentNumberRegex: /^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){0,17}$/,
		fields: [
			"id",
			"rfid",
			"rfid_validity",
			"name",
			"email",
			"phone",
			"user_name",
			"institute",
			"student_number",
			"can_change_password",
			"identification",
			"external",
			"can_access_BO",
			"active",
			"birth_date",
			"gender",
			"picture_file_id",
			"course_id",
			"course_year",
			"first_login",
			"course_id",
			"course_degree_id",
			"organic_unit_id",
			"updated_at",
			"created_at",
			"fb_messenger_id",
			"address",
			"postal_code",
			"city",
			"country",
			"nationality",
			"tin",
			"profile_id",
			"section_id",
			"department_id",
			"sourced_by",
			"sourced_by",
			"disabled_from",
			"source_name",
			"document_type_id",
			"admission_date",
			"password_expire_on",
			"account_verified",
			"verification_token",
			"verification_expires",
			"recovery_token",
			"recovery_expires",
			"is_final_consumer",
			"blocked_fields",
		],
		defaultWithRelateds: [
			//"profile", "scopes", "user_groups"
		],
		withRelateds: {
			profile(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.profiles.get", {
								id: doc.profile_id,
							})
							.then((res) => (doc.profile = res.length > 0 ? res[0] : null));
					}),
				);
			},
			scopes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.users-scopes.fetchScopesByUser", { user_id: doc.id })
							.then((res) => (doc.scopes = res));
					}),
				);
			},
			user_groups(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("authorization.users-user-groups.fetchUserGroupsByUser", { user_id: doc.id })
							.then((res) => (doc.user_groups = res));
					}),
				);
			},
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			organicUnit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organicUnit", "organic_unit_id");
			},
			section(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.sections", "section", "section_id");
			},
			department(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.departments", "department", "department_id");
			},
			document_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.document-types",
					"document_type",
					"document_type_id",
				);
			},
		},
		entityValidator: {
			rfid: { type: "string", optional: true },
			rfid_validity: { type: "string", optional: true },
			name: { type: "string" },
			email: { type: "email" },
			phone: { type: "string", optional: true },
			user_name: { type: "string" },
			institute: { type: "string", optional: true },
			student_number: { type: "string", optional: true },
			document_type_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			identification: { type: "string", optional: true },
			external: { type: "boolean" },
			can_change_password: { type: "boolean" },
			can_access_BO: { type: "boolean" },
			active: { type: "boolean" },
			disabled_from: { type: "date", convert: true, optional: true },
			password_expire_on: { type: "date", convert: true, optional: true },
			birth_date: { type: "date", convert: true },
			gender: { type: "enum", values: ["M", "F", "U"] },
			picture_file_id: { type: "number", optional: true, integer: true },
			fb_messenger_id: { type: "string", optional: true },
			address: { type: "string", optional: true },
			postal_code: { type: "string", optional: true },
			city: { type: "string", optional: true },
			country: { type: "string", optional: true },
			nationality: { type: "string", optional: true },
			tin: { type: "string", optional: true },
			first_login: { type: "boolean" },
			course_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			course_degree_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			organic_unit_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			profile_id: { type: "number", positive: true, integer: true, convert: true },
			sourced_by: { type: "enum", values: ["BO", "MIDDLEWARE", "INTERNAL"], optional: true },
			source_name: { type: "string", max: 50, optional: true },
			scope_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			user_group_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			department_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			section_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			admission_date: { type: "date", convert: true, optional: true },
			account_verified: { type: "boolean", optional: true },
			verification_token: { type: "uuid", optional: true },
			verification_expires: { type: "date", convert: true, optional: true },
			recovery_token: { type: "uuid", optional: true },
			recovery_expires: { type: "date", convert: true, optional: true },
			blocked_fields: {
				type: "array",
				items: "string",
				optional: true,
			},
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function checkRFID(ctx) {
					if (ctx.params.rfid) {
						const rfids = await this._count(ctx, {
							query: {
								rfid: ctx.params.rfid,
							},
						});

						if (rfids > 0) {
							return Promise.reject(
								new Errors.ValidationError(
									"Already exist a user with this rfid",
									"AUTH_RFID_ALREADY_EXIST",
									[
										{
											field: "rfid",
											type: "AUTH_RFID_ALREADY_EXIST",
											message: "Already exist a user with this rfid",
										},
									],
								),
							);
						}
					}
				},
				async function checkEmail(ctx) {
					const v = new Validator();
					const schema = {
						email: { type: "email", normalize: true, max: 80, optional: false },
					};
					const check = v.compile(schema);
					const res = check({ email: ctx.params.email });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					const countEmails = await this._count(ctx, {
						query: {
							email: ctx.params.email,
						},
					});

					if (countEmails > 0 || !ctx.params.email) {
						return Promise.reject(
							new ValidationError(
								"Already exist a user with this email",
								"AUTH_EMAIL_ALREADY_EXIST",
								[
									{
										field: "email",
										type: "AUTH_EMAIL_ALREADY_EXIST",
										message: "Already exist a user with this email",
									},
								],
							),
						);
					}
				},
				async function checkUsername(ctx) {
					const v = new Validator();
					const schema = {
						user_name: {
							type: "string",
							min: 3,
							max: 100,
							lowercase: true,
							trim: true,
							optional: false,
						},
					};
					const check = v.compile(schema);

					const res = check({ user_name: ctx.params.user_name });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);

					const userNames = await this._count(ctx, {
						query: {
							user_name: ctx.params.user_name,
						},
					});
					if (userNames > 0 || !ctx.params.user_name) {
						return Promise.reject(
							new ValidationError(
								"Already exist a user with this user_name",
								"AUTH_USER_NAME_ALREADY_EXIST",
								[
									{
										field: "user_name",
										type: "AUTH_USER_NAME_ALREADY_EXIST",
										message: "Already exist a user with this user_name",
									},
								],
							),
						);
					}
				},
				async function checkStudentNumber(ctx) {
					/*
					const v = new Validator();
					const schema = {
						student_number: {
							type: "string",
							uppercase: true,
							trim: true,
							optional: true,
							pattern: this.settings.studentNumberRegex,
						},
					};
					const check = v.compile(schema);
					const res = check({ student_number: ctx.params.student_number });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", "VALIDATION_ERROR", res),
						);

					if (ctx.params.student_number) {
						const studentNumber = await this._count(ctx, {
							query: {
								student_number: ctx.params.student_number,
								active: true,
							},
						});
						if (studentNumber > 0) {
							return Promise.reject(
								new ValidationError(
									"Already exist a user with this student_number",
									"AUTH_STUDENT_NUMBER_ALREADY_EXIST",
									[
										{
											field: "student_number",
											type: "STUDENT_NUMBER_ALREADY_EXIST",
											message: "Already exist a user with this student_number",
										},
									],
								),
							);
						}
					}*/
				},
				async function checkRelations(ctx) {
					// CHECK IF PROFILE EXIST
					const profile = await ctx.call("authorization.profiles.get", {
						id: ctx.params.profile_id,
					});
					if (profile.length === 0) {
						throw new Errors.EntityNotFoundError("profiles", ctx.params.profile_id);
					}

					// CHECK IF SCOPES EXIST
					if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
						ctx.params.scope_ids = _.uniq(ctx.params.scope_ids);
						const scopes = await Promise.all(
							ctx.params.scope_ids.map((scope_id) =>
								ctx.call("authorization.scopes.get", { id: scope_id }),
							),
						);
						scopes.map((s) => {
							if (s.length === 0) {
								throw new Errors.EntityNotFoundError("scopes", ctx.params.scope_ids);
							}
						});
					}

					// CHECK IF USER_GROUPS EXIST
					if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
						ctx.params.user_group_ids = _.uniq(ctx.params.user_group_ids);
						const user_groups = await Promise.all(
							ctx.params.user_group_ids.map((user_group_id) =>
								ctx.call("authorization.scopes.get", { id: user_group_id }),
							),
						);
						user_groups.map((s) => {
							if (s.length === 0) {
								throw new Errors.EntityNotFoundError("user_groups", ctx.params.user_group_ids);
							}
						});
					}

					// CHECK IF SECTION_ID EXIST
					if (ctx.params.section_id) {
						await ctx.call("authorization.sections.get", { id: ctx.params.section_id });
					}
					// CHECK IF DEPARTMENT_ID EXIST
					if (ctx.params.department_id) {
						await ctx.call("authorization.departments.get", { id: ctx.params.department_id });
					}
				},
				"validateDocumentType",
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateDocumentType",
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveUserGroups", "saveScopes", "savePassword", "sendVerificationEmail"],
			update: ["saveUserGroups", "saveScopes"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		checkPassword: {
			params: {
				password: { type: "string" },
				user_id: { type: "number" },
			},
			async handler(ctx) {
				return this.findPasswordOfUser(ctx.params.user_id).then((user) => {
					if (!user) {
						throw new Errors.EntityNotFoundError("users", ctx.params.user_id);
					}

					const hashBody = this.hashPassword(ctx.params.password, user[0].salt);
					return hashBody === user[0].password;
				});
			},
		},
		checkPin: {
			params: {
				pin: { type: "string", length: 4 },
				user_id: { type: "number" },
			},
			async handler(ctx) {
				return this.getById(ctx.params.user_id).then((user) => {
					if (!user) {
						throw new MoleculerError("User not found", 404);
					}

					const hashBody = this.hashPin(ctx.params.pin, user[0].salt);

					return hashBody === user[0].pin;
				});
			},
		},
		update_me: {
			visibility: "published",
			rest: "POST /update-me",
			params: {
				name: { type: "string" },
				birth_date: { type: "date", convert: true },
				gender: { type: "enum", values: ["M", "F", "U"], convert: true },
				phone: { type: "string" },
				document_type_id: { type: "number", positive: true, integer: true, convert: true },
				identification: { type: "string", max: 50 },
				tin: { type: "string" },
				address: { type: "string" },
				city: { type: "string" },
				postal_code: { type: "string" },
				country: { type: "string" },
				nationality: { type: "string", optional: true },
				course_id: { type: "number", convert: true, integer: true, optional: true, nullable: true },
				course_year: { type: "number", optional: true, nullable: true },
				organic_unit_id: {
					type: "number",
					integer: true,
					convert: true,
					optional: true,
					nullable: true,
				},
				course_degree_id: {
					type: "number",
					integer: true,
					convert: true,
					optional: true,
					nullable: true,
				},
				section_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				department_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				$$strict: "remove",
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					throw new Errors.ForbiddenError(
						"You cannot update the guest user info",
						"AUTH_CANNOT_CHANGE_INFO_GUEST",
						{},
					);
				}

				const user = await ctx.call("authorization.users.get", {
					id: ctx.meta.user.id,
				});

				// Validate exist organic_unit
				if (ctx.params.organic_unit_id && ctx.params.course_degree_id && ctx.params.course_id) {
					await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
					// Validate exist course_degree_id
					await ctx.call("configuration.course-degrees.get", { id: ctx.params.course_degree_id });
					// Validate exist course_id
					const course = await ctx.call("configuration.courses.get", { id: ctx.params.course_id });

					// Validate course_degree correspond the course
					if (course[0].course_degree_id !== ctx.params.course_degree_id) {
						throw new Errors.ValidationError(
							"The input 'course_degree_id' don't correspond the course selected",
							"COURSE_DEGREES_NOT_CORRESPOND_THE_COURSE",
							{},
						);
					}

					// Validate organic_unit correspond the course
					if (course[0].organic_unit_id !== ctx.params.organic_unit_id) {
						throw new Errors.ValidationError(
							"The input 'organic_unit_id' don't correspond the course selected",
							"ORGANIC_UNIT_NOT_CORRESPONDE_THE_COURSE",
							{},
						);
					}
				}

				if (ctx.params.section_id) {
					await ctx.call("authorization.sections.get", { id: ctx.params.section_id });
				}

				user[0].blocked_fields.map((bf) => {
					delete ctx.params[bf];
				});

				ctx.params.id = ctx.meta.user.id;
				ctx.params.updated_at = new Date();
				return this._update(ctx, ctx.params, true);
			},
		},
		create: {
			visibility: "published",
			rest: "POST /",
			scope: "authorization:users:create",
			params: {
				rfid: { type: "string", optional: true },
				rfid_validity: { type: "string", optional: true },
				name: { type: "string" },
				email: { type: "email", normalize: true },
				phone: { type: "string", optional: true },
				user_name: { type: "string", lowercase: true },
				institute: { type: "string", optional: true },
				student_number: { type: "string", optional: true },
				document_type_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				identification: { type: "string", max: 50, optional: true },
				external: { type: "boolean" },
				//pin: { type: "string", length: 4 },
				//password: { type: "string", min: 5 },
				//can_change_password: { type: "boolean" },
				can_access_BO: { type: "boolean" },
				active: { type: "boolean" },
				birth_date: { type: "date", convert: true },
				gender: { type: "enum", values: ["M", "F", "U"] },
				picture_file_id: { type: "number", optional: true, integer: true },
				fb_messenger_id: { type: "string", optional: true },
				address: { type: "string", optional: true },
				postal_code: { type: "string", optional: true },
				city: { type: "string", optional: true },
				country: { type: "string", optional: true },
				nationality: { type: "string", optional: true },
				tin: { type: "string", optional: true },
				course_year: { type: "number", optional: true, default: 1 },
				course_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
				course_degree_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				admission_date: { type: "date", convert: true, optional: true },
				organic_unit_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				profile_id: { type: "number", positive: true, integer: true, convert: true },
				scope_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				user_group_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				section_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				department_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				blocked_fields: {
					type: "array",
					items: "string",
					optional: true,
				},
				$$strict: "remove",
			},
			handler(ctx) {
				const middlware_callers = ["middleware.authorization", "middleware.sso"];
				const bo_caller = "api";
				const caller = ctx.caller;

				let entity = ctx.params;
				const { scope_ids, user_group_ids } = entity;
				delete entity.scope_ids;
				delete entity.user_group_ids;

				entity.first_login = true;
				entity.updated_at = new Date();
				entity.created_at = new Date();

				// GENERATE SECURE RANDOM PASSWORD AND PIN
				entity.salt = this.generateSalt();
				entity.password = this.hashPassword(passwordGenerator.randomPassword(), entity.salt);
				entity.pin = this.hashPin(
					passwordGenerator.randomPassword({ length: 4, characters: passwordGenerator.digits }),
					entity.salt,
				);

				// FIND WHO CREATE THE USER
				if (middlware_callers.includes(caller)) {
					entity.sourced_by = "MIDDLEWARE";
					entity.source_name = caller;
				}
				if (caller === bo_caller && ctx.meta.device && ctx.meta.device.type === "BO") {
					entity.sourced_by = "BO";
					entity.source_name = "BACKOFFICE";
				}
				if (!entity.sourced_by) {
					entity.sourced_by = "INTERNAL";
					entity.source_name = caller;
				}

				if (!entity.active) {
					entity.disabled_from = new Date();
				} else {
					entity.disabled_from = null;
				}

				if (entity.sourced_by === "INTERNAL" || entity.sourced_by === "BO") {
					entity.password_expire_on = moment().add(180, "days").toDate();
					entity.account_verified = false;
					entity.verification_token = uuid();
					entity.verification_expires = moment().add(24, "hour").toDate();
					entity.can_change_password = true;
				} else {
					entity.can_change_password = false;
					entity.password_expire_on = null;
					entity.account_verified = true;
					entity.verification_token = null;
					entity.verification_expires = null;
				}

				return this.validateEntity(entity)
					.then((validated) => this.adapter.beforeSaveTransformID(validated, this.settings.idField))
					.then((transformed) => this.adapter.insert(transformed))
					.then((created) => this.adapter.findById(created[0]))
					.then((users) => {
						ctx.call("authorization.password_history.create", {
							user_id: users[0].id,
							password: entity.password,
						});
						return users;
					})
					.then((doc) => this.transformDocuments(ctx, {}, doc))
					.then((json) => this.entityChanged("created", json, ctx).then(() => json));
			},
		},
		first_login: {
			visibility: "published",
			rest: "POST /first-login",
			params: {
				pin: { type: "string", length: 4 },
				$$strict: "remove",
			},
			async handler(ctx) {
				const user = await this.adapter.findById(ctx.meta.user.id);
				ctx.params.id = ctx.meta.user.id;
				ctx.params.pin = this.hashPin(ctx.params.pin, user[0].salt);
				return this.adapter
					.updateById(user[0].id, {
						pin: ctx.params.pin,
						first_login: false,
					})
					.then(() => {
						this.clearCache();
						return this.adapter.findById(ctx.meta.user.id);
					});
			},
		},
		get_users_by_target_id: {
			visibility: "public",
			//rest: "GET /get_users_by_target_id",
			//visibility: "published",
			cache: { keys: ["target_id"] },
			params: {
				target_id: { type: "number", integer: true, positive: true, convert: true },
			},
			async handler(ctx) {
				const target_conditions = await ctx.call("authorization.targets_conditions.find", {
					query: { target_id: ctx.params.target_id },
				});

				return this.getUserIds(ctx, target_conditions);
			},
		},
		get_users_by_target_conditions: {
			visibility: "public",
			//rest: "GET /get_users_by_target_conditions",
			//visibility: "published",
			cache: { keys: ["conditions"] },
			params: {
				conditions: {
					type: "array",
					items: {
						type: "object",
						props: {
							field_name: {
								type: "enum",
								values: [
									"user_id",
									"profile_id",
									"course_degree_id",
									"course_id",
									"organic_unit_id",
									"department_id",
									"section_id",
									"active",
									"accommodation_contracted",
									"residence_id",
									"ubike_bike_assigned",
									"scholarship_application_approved",
									"volunteering_application_approved",
									"food_tickets_purchased",
								],
							},
							field_values: {
								type: "array",
								items: {
									type: "number",
									integer: true,
									convert: true,
								},
								min: 1,
							},
						},
					},
				},
			},
			async handler(ctx) {
				return this.getUserIds(ctx, ctx.params.conditions);
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
			params: {
				id: { type: "number", positive: true, integer: true, convert: true },
				rfid: { type: "string", optional: true },
				rfid_validity: { type: "string", optional: true },
				name: { type: "string" },
				phone: { type: "string", optional: true },
				institute: { type: "string", optional: true },
				student_number: { type: "string", optional: true },
				identification: { type: "string", max: 50, optional: true },
				external: { type: "boolean" },
				can_access_BO: { type: "boolean" },
				birth_date: { type: "date", convert: true },
				gender: { type: "enum", values: ["M", "F", "U"] },
				picture_file_id: { type: "number", optional: true, integer: true },
				fb_messenger_id: { type: "string", optional: true },
				address: { type: "string", optional: true },
				postal_code: { type: "string", optional: true },
				city: { type: "string", optional: true },
				country: { type: "string", optional: true },
				nationality: { type: "string", optional: true },
				tin: { type: "string", optional: true },
				course_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
				course_degree_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				organic_unit_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				profile_id: { type: "number", positive: true, integer: true, convert: true },
				scope_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				user_group_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				document_type_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				is_final_consumer: { type: "boolean", optional: true },
				blocked_fields: {
					type: "array",
					items: "string",
					optional: true,
				},
				$$strict: "remove",
			},
			handler(ctx) {
				let id;
				let sets = {};
				// Convert fields from params to "$set" update object
				Object.keys(ctx.params).forEach((prop) => {
					if (prop == "id" || prop == this.settings.idField) id = this.decodeID(ctx.params[prop]);
					else sets[prop] = ctx.params[prop];
				});
				return this.validateEntity(sets, true)
					.then((entity) => this.removeForeignFields(entity))
					.then((entity) => this.adapter.updateById(id, entity))
					.then(() => this.adapter.findById(id))
					.then((doc) => {
						return doc;
					})
					.then((doc) => {
						if (!doc || (Array.isArray(doc) && doc.length === 0))
							return Promise.reject(new Errors.EntityNotFoundError("users", id));
						return this.transformDocuments(ctx, {}, doc).then((json) =>
							this.entityChanged("updated", json, ctx).then(() => json),
						);
					});
			},
		},
		remove: {
			// REST: DELETE /:id
			visibility: "private",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
			params: {
				rfid: { type: "string", optional: true },
				rfid_validity: { type: "string", optional: true },
				name: { type: "string", optional: true },
				phone: { type: "string", optional: true },
				institute: { type: "string", optional: true },
				student_number: { type: "string", optional: true },
				identification: { type: "string", max: 50, optional: true },
				can_access_BO: { type: "boolean", optional: true },
				birth_date: { type: "date", convert: true, optional: true },
				gender: { type: "enum", values: ["M", "F", "U"], optional: true },
				picture_file_id: { type: "number", optional: true, integer: true },
				fb_messenger_id: { type: "string", optional: true },
				address: { type: "string", optional: true },
				postal_code: { type: "string", optional: true },
				city: { type: "string", optional: true },
				country: { type: "string", optional: true },
				nationality: { type: "string", optional: true },
				tin: { type: "string", optional: true },
				course_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
				course_degree_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				organic_unit_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				profile_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				scope_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				user_group_ids: {
					type: "array",
					items: {
						type: "number",
						positive: true,
						integer: true,
					},
					optional: true,
				},
				document_type_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				admission_date: { type: "date", convert: true, optional: true },
				is_final_consumer: { type: "boolean", optional: true },
			},
		},
		create_account_verification: {
			rest: "POST /create_account_verify",
			visibility: "published",
			params: {
				email: { type: "email", normalize: true, max: 80, optional: false },
			},
			async handler(ctx) {
				const user = await this._find(ctx, {
					query: {
						email: ctx.params.email,
					},
				});

				if (user.length === 0) {
					return { sended: true };
				}

				if (user[0].account_verified) {
					return { sended: true };
				}

				if (!user[0].active) {
					return { sended: true };
				}

				const updatedUser = await this._update(
					ctx,
					{
						id: user[0].id,
						verification_token: uuid(),
						verification_expires: moment().add(24, "hour").toDate(),
					},
					true,
				);

				return ctx
					.call("notifications.alerts.create_alert", {
						alert_type_key: "AUTH_VERIFY_ACCOUNT",
						user_id: updatedUser[0].id,
						user_data: {},
						data: { verification_token: updatedUser[0].verification_token },
						variables: {},
						external_uuid: null,
						medias: [],
					})
					.then(() => {
						return { sended: true };
					});
			},
		},
		verify_account: {
			rest: "POST /verify_account",
			visibility: "published",
			params: {
				token: { type: "uuid" },
				password: { type: "string" },
			},
			handler(ctx) {
				this.validatePassword(ctx.params.password);

				return this._find(ctx, {
					query: {
						verification_token: ctx.params.token,
					},
					withRelated: false,
				}).then(async (users) => {
					if (users.length === 0) {
						throw new Errors.EntityNotFoundError("verification_token", ctx.params.token);
					}

					if (users[0].account_verified === true) {
						throw new Errors.ValidationError(
							"This account already as been verified",
							"AUTH_ACCOUNT_ALREADY_VERIFIED",
						);
					}

					if (moment(users[0].verification_expires).isBefore()) {
						throw new Errors.ValidationError(
							"The verification token has expired",
							"AUTH_VERIFICATION_TOKEN_EXPIRED",
						);
					}

					const userData = await this.adapter.findById(users[0].id);
					const password = this.hashPassword(ctx.params.password, userData[0].salt);

					return this.adapter
						.updateById(users[0].id, {
							password: password,
							password_expire_on: moment().add(180, "days").toDate(),
							recovery_token: null,
							recovery_expires: null,
							account_verified: true,
						})
						.then(() => {
							ctx.call("authorization.password_history.create", {
								user_id: users[0].id,
								password,
							});
							return { verified: true };
						});
				});
			},
		},
		reset_password: {
			rest: "POST /reset-password",
			visibility: "published",
			params: {
				email: { type: "email", normalize: true, max: 80, optional: false },
			},
			async handler(ctx) {
				const user = await this._find(ctx, {
					query: {
						email: ctx.params.email,
					},
					withRelated: false,
				});

				if (user.length === 0) {
					return { sended: true };
				}

				if (!user[0].can_change_password) {
					throw new Errors.ValidationError(
						"The user is not allowed to change the password",
						"AUTH_NOT_ALLOWED_CHANGE_PASSWORD",
					);
				}

				if (!user[0].active) {
					throw new Errors.ValidationError("The user is not active", "AUTH_INATIVE_USER");
				}

				const updatedUser = await this._update(
					ctx,
					{
						id: user[0].id,
						recovery_token: uuid(),
						recovery_expires: moment().add(24, "hour").toDate(),
					},
					true,
				);

				return ctx
					.call("notifications.alerts.create_alert", {
						alert_type_key: "AUTH_RESET_PASSWORD",
						user_id: updatedUser[0].id,
						user_data: {},
						data: { recovery_token: updatedUser[0].recovery_token },
						variables: {},
						external_uuid: null,
						medias: [],
					})
					.then(() => {
						return { sended: true };
					});
			},
		},
		new_password: {
			rest: "POST /new-password",
			visibility: "published",
			params: {
				token: { type: "uuid" },
				password: { type: "string" },
			},
			async handler(ctx) {
				this.validatePassword(ctx.params.password);

				const user = await this._find(ctx, {
					query: {
						recovery_token: ctx.params.token,
					},
					withRelated: false,
				});

				if (user.length === 0) {
					throw new Errors.ValidationError(
						"The recovery token has expired",
						"AUTH_RECOVERY_TOKEN_EXPIRED",
					);
				}

				if (!user[0].active) {
					throw new Errors.ValidationError("The user is not active", "AUTH_INATIVE_USER");
				}

				if (!user[0].can_change_password) {
					throw new Errors.ValidationError(
						"The user is not allowed to change the password",
						"AUTH_NOT_ALLOWED_CHANGE_PASSWORD",
					);
				}

				if (moment(user[0].recovery_expires).isBefore()) {
					throw new Errors.ValidationError(
						"The recovery token has expired",
						"AUTH_RECOVERY_TOKEN_EXPIRED",
					);
				}

				const userData = await this.adapter.findById(user[0].id);
				const password = this.hashPassword(ctx.params.password, userData[0].salt);

				const passwordAlreadyExist = await ctx.call(
					"authorization.password_history.checkPasswordOfUser",
					{
						hashed_password: password,
						user_id: user[0].id,
					},
				);

				if (passwordAlreadyExist)
					throw new Errors.ValidationError(
						"Password already as been used, please choose other",
						"AUTH_PASSWORD_ALREADY_USED",
					);

				return this.adapter
					.updateById(user[0].id, {
						password: password,
						password_expire_on: moment().add(180, "days").toDate(),
						recovery_token: null,
						recovery_expires: null,
					})
					.then(() => {
						ctx.call("authorization.password_history.create", {
							user_id: user[0].id,
							password,
						});
						ctx.call("authorization.tokens.removeTokensOfUser", {
							user_id: ctx.meta.user.id,
						});
						return { changed: true };
					});

				/*				return ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "AUTH_RESET_PASSWORD",
									user_id: updatedUser[0].id,
									user_data: {},
									data: { recovery_token: updatedUser[0].recovery_token },
									variables: {},
									external_uuid: null,
									medias: [],
								});*/
			},
		},
		change_password: {
			rest: "POST /change-password",
			visibility: "published",
			params: {
				actual_password: { type: "string" },
				new_password: { type: "string" },
			},
			async handler(ctx) {
				this.validatePassword(ctx.params.new_password);

				if (ctx.meta.isGuest) {
					throw new Errors.ForbiddenError(
						"You cannot change the password of guest user",
						"AUTH_CANNOT_CHANGE_PASSWORD_GUEST",
						{},
					);
				}

				if (!ctx.meta.user.active) {
					throw new Errors.ValidationError("The user is not active", "AUTH_INATIVE_USER");
				}

				if (!ctx.meta.user.can_change_password) {
					throw new Errors.ValidationError(
						"The user is not allowed to change the password",
						"AUTH_NOT_ALLOWED_CHANGE_PASSWORD",
					);
				}

				const actual_password_verification = await ctx.call("authorization.users.checkPassword", {
					password: ctx.params.actual_password,
					user_id: ctx.meta.user.id,
				});

				if (!actual_password_verification) {
					throw new Errors.ValidationError(
						"Failed on validate the actual password",
						"AUTH_WRONG_ACTUAL_PASSWORD",
						{},
					);
				}

				const userData = await this.adapter.findById(ctx.meta.user.id);
				const password = this.hashPassword(ctx.params.new_password, userData[0].salt);

				const passwordAlreadyExist = await ctx.call(
					"authorization.password_history.checkPasswordOfUser",
					{
						hashed_password: password,
						user_id: ctx.meta.user.id,
					},
				);

				if (passwordAlreadyExist)
					throw new Errors.ValidationError(
						"Password already as been used, please choose other",
						"AUTH_PASSWORD_ALREADY_USED",
					);

				return this.adapter
					.updateById(ctx.meta.user.id, {
						password: password,
						password_expire_on: moment().add(180, "days").toDate(),
					})
					.then(() => {
						this.clearCache();
						ctx.call("authorization.password_history.create", {
							user_id: ctx.meta.user.id,
							password,
						});
						ctx.call("authorization.tokens.removeTokensOfUser", {
							user_id: ctx.meta.user.id,
						});
						return { changed: true };
					});
			},
		},
		change_pin: {
			rest: "POST /change-pin",
			visibility: "published",
			params: {
				actual_pin: { type: "string", length: 4 },
				new_pin: { type: "string", length: 4 },
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					throw new Errors.ForbiddenError(
						"You cannot change the pin of guest user",
						"AUTH_CANNOT_CHANGE_PIN_GUEST",
						{},
					);
				}

				if (!ctx.meta.user.active) {
					throw new Errors.ValidationError("The user is not active", "AUTH_INATIVE_USER");
				}

				const actual_pin_verification = await ctx.call("authorization.users.checkPin", {
					pin: ctx.params.actual_pin,
					user_id: ctx.meta.user.id,
				});

				if (!actual_pin_verification) {
					throw new Errors.ValidationError(
						"Failed on validate the actual pin",
						"AUTH_WRONG_ACTUAL_PIN",
						{},
					);
				}

				const userData = await this.adapter.findById(ctx.meta.user.id);
				const pin = this.hashPin(ctx.params.new_pin, userData[0].salt);

				return this.adapter
					.updateById(ctx.meta.user.id, {
						pin: pin,
					})
					.then(() => {
						this.clearCache();
						return { changed: true };
					});
			},
		},
		change_status: {
			rest: "POST /:user_id/status",
			visibility: "published",
			scope: "authorization:users:change-status",
			params: {
				user_id: { type: "number", positive: true, convert: true },
				active: { type: "boolean" },
			},
			async handler(ctx) {
				const user = await ctx.call("authorization.users.get", {
					id: ctx.params.user_id,
					withRelated: false,
				});

				if (user[0].user_name === "guest") {
					throw new Errors.ForbiddenError(
						"You cannot change the status of guest user",
						"AUTH_CANNOT_CHANGE_STATUS_GUEST",
						{},
					);
				}

				return this.adapter
					.updateById(ctx.params.user_id, {
						active: ctx.params.active,
						disabled_from: ctx.params.active ? null : new Date(),
					})
					.then(() => {
						this.clearCache();
						ctx.call("authorization.tokens.removeTokensOfUser", {
							user_id: ctx.params.user_id,
						});
						//send verification email
						user[0].active = ctx.params.active;
						if (user[0].account_verified === false && user[0].active) {
							this.sendVerificationEmail(ctx, user);
						}
						return { disabled: !ctx.params.active };
					});
			},
		},
		disable_account: {
			rest: "POST /disable-account",
			visibility: "published",
			params: {
				password: { type: "string" },
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					throw new Errors.ForbiddenError(
						"You cannot change the password of guest user",
						"AUTH_CANNOT_DISABLE_GUEST",
						{},
					);
				}

				if (!ctx.meta.user.active) {
					throw new Errors.ValidationError("The user is not active", "AUTH_INATIVE_USER");
				}

				const actual_password_verification = await ctx.call("authorization.users.checkPassword", {
					password: ctx.params.password,
					user_id: ctx.meta.user.id,
				});

				if (!actual_password_verification) {
					throw new Errors.ValidationError(
						"Failed on validate the actual password",
						"AUTH_WRONG_ACTUAL_PASSWORD",
						{},
					);
				}

				return this.adapter
					.updateById(ctx.meta.user.id, {
						active: false,
						disabled_from: new Date(),
					})
					.then(() => {
						this.clearCache();
						ctx.call("authorization.tokens.removeTokensOfUser", {
							user_id: ctx.meta.user.id,
						});
						return { disabled: true };
					});
			},
		},
		notifyUsersPasswordsPeriod: {
			handler(ctx) {
				//GET USERS WITH ACTIVE = TRUE, AND VERERIFIED = TRUE AND PASSWORD_EXPIRED EXACTLY 30 DAYS FROM NOW
				return this.getQueryPasswordExpiredByDays(30)
					.then((users) => {
						return Promise.all(
							users.map((user_id) =>
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "AUTH_PASSWORD_CLOSE_EXPIRATION_PERIOD",
									user_id,
									user_data: {},
									data: { number_of_days: 30 },
									variables: {},
									external_uuid: null,
									medias: [],
								}),
							),
						);
					})
					.then(() => {
						//GET USERS WITH ACTIVE = TRUE, AND VERERIFIED = TRUE AND PASSWORD_EXPIRED EXACTLY 7 DAYS FROM NOW
						return this.getQueryPasswordExpiredByDays(7);
					})
					.then((users) => {
						return Promise.all(
							users.map((user_id) =>
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "AUTH_PASSWORD_CLOSE_EXPIRATION_PERIOD",
									user_id,
									user_data: {},
									data: { number_of_days: 7 },
									variables: {},
									external_uuid: null,
									medias: [],
								}),
							),
						);
					})
					.then(() => {
						//GET USERS WITH ACTIVE = TRUE, AND VERERIFIED = TRUE AND PASSWORD_EXPIRED EXACTLY 1 DAYS FROM NOW
						return this.getQueryPasswordExpiredByDays(1);
					})
					.then((users) => {
						return Promise.all(
							users.map((user_id) =>
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "AUTH_PASSWORD_CLOSE_EXPIRATION_PERIOD",
									user_id,
									user_data: {},
									data: { number_of_days: 1 },
									variables: {},
									external_uuid: null,
									medias: [],
								}),
							),
						);
					})
					.then(() => {
						//GET USERS WITH ACTIVE = TRUE, AND VERERIFIED = TRUE AND PASSWORD_EXPIRED EXACTLY 0 DAYS FROM NOW
						return this.getQueryPasswordExpiredByDays(0);
					})
					.then((users) => {
						return Promise.all(
							users.map((user_id) =>
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "AUTH_PASSWORD_EXPIRATED_PERIOD",
									user_id,
									user_data: {},
									data: {},
									variables: {},
									external_uuid: null,
									medias: [],
								}),
							),
						);
					})
					.then(() => {
						//GET USERS WITH ACTIVE = TRUE, AND VERERIFIED = TRUE AND PASSWORD_EXPIRED EXACTLY +180 DAYS FROM NOW
						return this.getQueryPasswordExpiredByDays(-180);
					})
					.then((users) => {
						return Promise.all(
							users.map((user_id) =>
								ctx
									.call("authorization.users.change_status", {
										user_id,
										active: false,
									})
									.then(() =>
										ctx.call("notifications.alerts.create_alert", {
											alert_type_key: "AUTH_PASSWORD_DISABLED_ACCOUNT",
											user_id,
											user_data: {},
											data: {},
											variables: {},
											external_uuid: null,
											medias: [],
										}),
									),
							),
						);
					});
			},
		},
		send_random_pin: {
			visibility: "published",
			rest: "POST /randomPin",
			params: {
				rfid: { type: "string", optional: true },
				user_name: { type: "string", optional: true },
				email: { type: "email", normalize: true, max: 80, optional: true },
			},
			async handler(ctx) {
				let user_id = -1;
				if (!ctx.params.rfid && !ctx.params.user_name && !ctx.params.email) {
					if (ctx.meta.isGuest) {
						return { changed: true };
					} else {
						user_id = ctx.meta.user.id;
					}
				} else {
					const query = {};

					if (ctx.params.rfid) {
						query["rfid"] = ctx.params.rfid;
					} else if (ctx.params.user_name) {
						query["user_name"] = ctx.params.user_name;
					} else if (ctx.params.email) {
						query["email"] = ctx.params.email;
					} else {
						return { changed: true };
					}

					const userData = await ctx.call("authorization.users.find", {
						query,
					});

					if (userData.length === 0) {
						return { changed: true };
					}
					user_id = userData[0].id;
				}

				const user = await this.getById(user_id);

				const unhashed_pin = passwordGenerator.randomPassword({
					length: 4,
					characters: passwordGenerator.digits,
				});
				const pin = this.hashPin(unhashed_pin, user[0].salt);

				return this.adapter
					.updateById(user_id, {
						pin: pin,
					})
					.then(() => {
						this.sendRandomPinAlert(ctx, user_id, unhashed_pin);
						this.clearCache();
						return { changed: true };
					});
			},
		},
		count_user_by_profile: {
			visibility: "public",
			handler() {
				return this.adapter.db("user").select("profile_id").count("id").groupBy("profile_id");
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async getUserIds(ctx, target_conditions) {
			// Verify if there are conditions related with Authorization
			let findInAuthorizationUser = false;

			const authorization_user_ids = await this.adapter.find({
				query: (q) => {
					q.select("user.id");
					q.leftJoin("section", "user.section_id", "section.id");
					for (const condition of target_conditions) {
						switch (condition.field_name) {
							case "active":
								findInAuthorizationUser = true;
								q.where("user.active ", "=", condition.field_value);
								break;
							case "user_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."id" IN (' + condition.field_value + ") ");
								break;
							case "profile_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."profile_id" IN (' + condition.field_value + ") ");
								break;
							case "organic_unit_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."organic_unit_id" IN (' + condition.field_value + ") ");
								break;
							case "course_degree_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."course_degree_id" IN (' + condition.field_value + ") ");
								break;
							case "course_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."course_id" IN (' + condition.field_value + ") ");
								break;
							case "department_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."department_id" IN (' + condition.field_value + ") ");
								break;
							case "section_id":
								findInAuthorizationUser = true;
								q.whereRaw(' "user"."section_id" IN (' + condition.field_value + ") ");
								break;
						}
					}
					//console.log("Retrieve query=" + q);
					return q;
				},
				withRelated: false,
			});

			// Final decision
			let user_ids = [];

			if (findInAuthorizationUser === true) {
				user_ids = authorization_user_ids;

				if (user_ids.length === 0) {
					return user_ids;
				}
			}

			//console.log("Teste authorization_user_ids=" + authorization_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			// Verify if there are conditions related with Accommodation
			let accommodation_user_ids = [];
			let findInAccommodationUser = false;

			for (const condition of target_conditions) {
				if (condition.field_name === "residence_id") {
					findInAccommodationUser = true;

					// Get all users with contracted accommodation by residence
					accommodation_user_ids = await ctx.call(
						"accommodation.applications.get_users_by_contracted_app_and_residence",
						{
							residence_ids: condition.field_value,
						},
					);

					break;
				}
			}

			if (findInAccommodationUser === false) {
				for (const condition of target_conditions) {
					if (condition.field_name === "accommodation_contracted") {
						findInAccommodationUser = true;

						// Get all users with contracted accommodation
						accommodation_user_ids = await ctx.call(
							"accommodation.applications.get_users_by_contracted_app_and_residence",
							{
								residence_ids: null,
							},
						);

						break;
					}
				}
			}

			// Users intersection with Accommodation Users
			if (findInAccommodationUser === true) {
				if (user_ids.length > 0) {
					user_ids = user_ids.filter((item1) =>
						accommodation_user_ids.some((item2) => item1.id === item2.user_idd),
					);
				} else {
					user_ids = accommodation_user_ids.map(app => ({ id: app.user_id }));
				}

				if (user_ids.length === 0) {
					return user_ids;
				}
			}

			//console.log("Teste accommodation_user_ids=" + accommodation_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			// Verify if there are conditions related with Ubike
			let ubike_user_ids = [];
			let findInUbikeUser = false;

			for (const condition of target_conditions) {
				if (condition.field_name === "ubike_bike_assigned") {
					findInUbikeUser = true;

					// Get all users with bike assigned
					ubike_user_ids = await ctx.call("u_bike.applications.get_users_with_bike_assigned");

					break;
				}
			}

			// Users intersection with Ubike Users
			if (findInUbikeUser === true) {
				if (user_ids.length > 0) {
					user_ids = user_ids.filter((item1) =>
						ubike_user_ids.some((item2) => item1.id === item2.user_id),
					);
				} else {
					user_ids = ubike_user_ids.map(app => ({ id: app.user_id }));
				}

				if (user_ids.length === 0) {
					return user_ids;
				}
			}

			//console.log("Teste ubike_user_ids=" + ubike_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			// Verify if there are conditions related with Scholarship
			let scholarship_user_ids = [];
			let findInScholarshipUser = false;

			for (const condition of target_conditions) {
				if (condition.field_name === "scholarship_application_approved") {
					findInScholarshipUser = true;

					// Get all users with application approved
					scholarship_user_ids = await ctx.call(
						"social_scholarship.applications.get_users_with_application_approved",
					);

					break;
				}
			}

			// Users intersection with Scholarship Users
			if (findInScholarshipUser === true) {
				if (user_ids.length > 0) {
					user_ids = user_ids.filter((item1) =>
						scholarship_user_ids.some((item2) => item1.id === item2.user_id),
					);
				} else {
					user_ids = scholarship_user_ids.map(app => ({ id: app.user_id }));
				}

				if (user_ids.length === 0) {
					return user_ids;
				}
			}

			//console.log("Teste scholarship_user_ids=" + scholarship_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			// Verify if there are conditions related with Volunteering
			let volunteering_user_ids = [];
			let findInVolunteeringUser = false;

			for (const condition of target_conditions) {
				if (condition.field_name === "volunteering_application_approved") {
					findInVolunteeringUser = true;

					// Get all users with application approved
					volunteering_user_ids = await ctx.call(
						"volunteering.applications.get_users_with_application_approved",
					);

					break;
				}
			}

			// Users intersection with Volunteering Users
			if (findInVolunteeringUser === true) {
				if (user_ids.length > 0) {
					user_ids = user_ids.filter((item1) =>
						volunteering_user_ids.some((item2) => item1.id === item2.user_id),
					);
				} else {
					user_ids = volunteering_user_ids.map(app => ({ id: app.user_id }));
				}

				if (user_ids.length === 0) {
					return user_ids;
				}
			}

			//console.log("Teste volunteering_user_ids=" + volunteering_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			// Verify if there are conditions related with Food
			let food_user_ids = [];
			let findInFoodUser = false;

			for (const condition of target_conditions) {
				if (condition.field_name === "food_tickets_purchased") {
					findInFoodUser = true;

					// Get all users with purchased tickets
					food_user_ids = await ctx.call(
						"alimentation.reservations.get_users_with_purchased_tickets",
					);

					break;
				}
			}

			// Users intersection with Food Users
			if (findInFoodUser === true) {
				if (user_ids.length > 0) {
					user_ids = user_ids.filter((item1) =>
						food_user_ids.some((item2) => item1.id === item2.user_id),
					);
				} else {
					user_ids = food_user_ids.map(app => ({ id: app.user_id }));
				}
			}

			//console.log("Teste food_user_ids=" + food_user_ids.length);
			//console.log("Teste user_ids=" + user_ids.length);

			return user_ids;
		},

		validatePassword(password) {
			const v = new Validator();
			const schema = {
				password: { type: "string", optional: false, pattern: this.settings.passwordRegex },
			};
			const check = v.compile(schema);
			const res = check({ password });
			if (res !== true)
				throw new Errors.ValidationError(
					"The password does not meet the requirements",
					"AUTH_FAULT_PASSWORD",
					{},
				);

			return true;
		},
		findPasswordOfUser(id) {
			return this.adapter.findById(id);
		},
		generateSalt() {
			return uuid();
		},
		hashPassword(password, salt) {
			return crypto
				.createHash("sha256")
				.update(salt + password)
				.digest("hex");
		},
		hashPin(pin, salt) {
			return crypto
				.createHash("sha256")
				.update(salt + pin)
				.digest("hex");
		},
		async saveUserGroups(ctx, res) {
			if (ctx.params.user_group_ids) {
				await ctx.call("authorization.users-user-groups.save_user_grous_of_user", {
					user_id: res[0].id,
					user_group_ids: ctx.params.user_group_ids,
				});

				res[0].user_groups = await ctx.call("authorization.users-user-groups.find", {
					query: {
						user_id: res[0].id,
					},
				});
			}
			return res;
		},
		async saveScopes(ctx, res) {
			if (ctx.params.scope_ids) {
				await ctx.call("authorization.users-scopes.save_scopes_of_user", {
					user_id: res[0].id,
					scope_ids: ctx.params.scope_ids,
				});

				res[0].scopes = await ctx.call("authorization.users-scopes.find", {
					query: {
						user_id: res[0].id,
					},
				});
			}
			return res;
		},
		async sendVerificationEmail(ctx, res) {
			if (res[0].verification_token && res[0].active) {
				ctx.call("notifications.alerts.create_alert", {
					alert_type_key: "AUTH_VERIFY_ACCOUNT",
					user_id: res[0].id,
					user_data: null,
					data: { verification_token: res[0].verification_token },
					variables: {},
					external_uuid: null,
					medias: [],
				});
			}

			return res;
		},
		async sendRandomPinAlert(ctx, user_id, pin) {
			ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "AUTH_SEND_RANDOM_PIN",
				user_id,
				user_data: null,
				data: { pin },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
		getQueryPasswordExpiredByDays(days = 0) {
			return this.adapter
				.raw(
					'select  u.id from "user" u where u.active = true and u.account_verified = true and u.password_expire_on notnull and (u.password_expire_on::date - NOW()::date) = ?',
					[days],
				)
				.then((result) => {
					return result.rows.map((r) => r.id);
				});
		},
		async validateDocumentType(ctx) {
			if (ctx.params.document_type_id) {
				return ctx.call("authorization.document-types.get", { id: ctx.params.document_type_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
