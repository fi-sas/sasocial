"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users-scopes",
	table: "user_scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users-scopes")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "scope_id", "user_id"],
		withRelateds: {},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		fetchScopesByUser: {
			visibility: "public",
			params: {
				user_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
					},
				}).then((res) => {
					return ctx.call("authorization.scopes.get", {
						withRelated: false,
						id: res.map((r) => r.scope_id),
					});
				});
			},
		},
		create: {
			visibility: "published",
			rest: "POST /",
			scope: "authorization:users:update",
			handler(ctx) {
				let entity = ctx.params;
				const { user_id, scope_id } = entity;

				return this.validateEntity({
					user_id,
					scope_id,
				})
					.then((entity) => this.adapter.insert(entity))
					.then((entityId) =>
						ctx.call("authorization.users-scopes.find", { query: { id: entityId } }),
					);
			},
		},
		save_scopes_of_user: {
			params: {
				user_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				scope_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					user_id: ctx.params.user_id,
				});
				this.clearCache();

				const entities = ctx.params.scope_ids.map((scope_id) => ({
					user_id: ctx.params.user_id,
					scope_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
		remove: {
			visibility: "published",
			rest: "DELETE /:user_id/:scope_id",
			scope: "authorization:users:delete",
			params: {
				user_id: { type: "number", min: 1, convert: true },
				user_group_id: { type: "number", min: 1, convert: true },
			},
			async handler(ctx) {
				const userId = ctx.params.user_id;
				const userGroupId = ctx.params.user_group_id;
				const usersUserGroup = await this._find(ctx, {
					user_id: userId,
					user_group_id: userGroupId,
				});
				return this._remove(ctx, { id: usersUserGroup[0].id }).then(() => {});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
