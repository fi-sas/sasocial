"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError, ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users-user-groups",
	table: "user_user_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users-user-groups")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "user_group_id"],
		withRelateds: {},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function validateUser(ctx) {
					const userId = ctx.params.user_id;
					if (!_.isNumber(userId)) {
						throw new ValidationError({
							field: "user_id",
							type: "error",
							message: "Identifier is invalid",
						});
					}
					const user = await ctx.call("authorization.users.find", { query: { id: userId } });
					if (!user || _.isEmpty(user)) {
						throw new EntityNotFoundError("user", userId);
					}
				},
				async function validateUserGroup(ctx) {
					const userGroupId = ctx.params.user_group_id;
					if (!_.isNumber(userGroupId)) {
						throw new ValidationError({
							field: "user_group_id",
							type: "error",
							message: "Identifier is invalid",
						});
					}
					const user = await ctx.call("authorization.user-groups.find", {
						query: { id: userGroupId },
					});
					if (!user || _.isEmpty(user)) {
						throw new EntityNotFoundError("user-group", userGroupId);
					}
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		create: {
			// REST: POST /
			visibility: "published",
		},
		remove: {
			visibility: "published",
			rest: "DELETE /:user_id/:user_group_id",
			scope: "authorization:user-groups:delete",
			params: {
				user_id: { type: "number", min: 1, convert: true },
				user_group_id: { type: "number", min: 1, convert: true },
			},
			async handler(ctx) {
				const userId = ctx.params.user_id;
				const userGroupId = ctx.params.user_group_id;
				const usersUserGroup = await this._find(ctx, {
					user_id: userId,
					user_group_id: userGroupId,
				});
				return this._remove(ctx, { id: usersUserGroup[0].id }).then(() => { });
			},
		},
		save_user_grous_of_user: {
			params: {
				user_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				user_group_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					user_id: ctx.params.user_id,
				});
				this.clearCache();

				const entities = ctx.params.user_group_ids.map((user_group_id) => ({
					user_id: ctx.params.user_id,
					user_group_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
		fetchUserGroupsByUser: {
			visibility: "public",
			params: {
				user_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
					},
				}).then((res) => {
					return ctx.call("authorization.user-groups.get", {
						id: res.map((r) => r.user_group_id),
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
