const { ServiceBroker } = require("moleculer");
const { MoleculerError } = require("moleculer").Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const jwt = require("jsonwebtoken");
const AuthorizeSchema = require("../../services/authorize.service");
const UsersSchema = require("../../services/users.service");
const ScopesSchema = require("../../services/scopes.service");


// mock 'Devices Service' to be used in 'Authorize Service' tests
const DevicesSchema = {
	name: 'configuration.devices',
	settings: {},
	dependencies: [],
	actions: {
		get: {
			handler(ctx) {
				if (ctx.params.id === 0) {
					return { active: false };
				} else if (ctx.params.id === 2) {
					return { active: true, type: 'BO' };
				}
				return { active: true };
			},
		},
		find: {
			handler(ctx) {
				if (ctx.params.query.type === 'MOBILE') {
					return [];
				} else if (ctx.params.query.type === 'POS') {
					return [{ active: false }];
				}
				return [{ active: true, type: 'WEB' }];
			},
		}
	},
};

jest.spyOn(jwt, 'sign').mockImplementation(() => {
	return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c';
});

const mockUsersFind = jest.fn((ctx) => {
	if (ctx.params.query.email && ctx.params.query.email === 'nouser@mail.com') {
		return [];
	}
	return ['User'];
});

const mockCheckPassword = jest.fn((ctx) => {
	if (ctx.params.password === '123456') {
		return false;
	}
	return true;
});

const mockCheckPin = jest.fn((ctx) => {
	if (ctx.params.pin === '1234') {
		return false;
	}
	return true;
});


describe("Test 'authorization.authorize' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// mock some actions from external services to allow 'Authorize Service' to be tested
	UsersSchema.actions.find = mockUsersFind;
	UsersSchema.actions.checkPassword = mockCheckPassword;
	UsersSchema.actions.checkPin = mockCheckPin;
	ScopesSchema.actions.findAllUserAvailableScopesStrings = jest.fn(() => Promise.resolve(["read"]));
	// create the services necessary for tests
	broker.createService(DevicesSchema);
	broker.createService(UsersSchema);
	broker.createService(ScopesSchema);
	const AuthorizeService = broker.createService(AuthorizeSchema);
	// token to be used on tests
	const TokenHash = { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c' };

	beforeAll(() => {
		AuthorizeSchema.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.authorize.loginByDevice' action", () => {
		it('should return device not active', async () => {
			await expect(broker.call("authorization.authorize.loginByDevice", { deviceId: '0' }))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Device not active", "DEVICE_NOT_ACTIVE", { device: "deviceId" }));
		});

		it('should fail with invalid credentials (email & password)', async () => {
			const params = { email: 'joe@mail.com', password: '123456', deviceId: '1' };
			await expect(broker.call("authorization.authorize.loginByDevice", params))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {}));
		});

		it('should fail with invalid credentials (email & pin)', async () => {
			const params = { email: 'joe@mail.com', pin: '1234', deviceId: '1' };
			await expect(broker.call("authorization.authorize.loginByDevice", params))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {}));
		});

		it('should fail with invalid credentials (user_name & pin)', async () => {
			const params = { user_name: 'joe', password: '123456', deviceId: '1' };
			await expect(broker.call("authorization.authorize.loginByDevice", params))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {}));
		});

		it('should fail with invalid credentials (user_name & pin)', async () => {
			const params = { user_name: 'joe', pin: '1234', deviceId: '1' };
			await expect(broker.call("authorization.authorize.loginByDevice", params))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {}));
		});

		it('should return the token based on rfid', async () => {
			const params = { rfid: '1234', pin: '1234', deviceId: '1' };
			const result = await broker.call("authorization.authorize.loginByDevice", params);
			expect(result).toMatchObject(TokenHash);
		});

		it('should return no permission to backoffice access', async () => {
			const params = { rfid: '1234', pin: '1234', deviceId: '2' };
			await expect(broker.call("authorization.authorize.loginByDevice", params))
				.rejects
				.toThrow(new MoleculerError("no permission to access backoffice", 401, "ERR_NO_ACCESS_BACKOFFICE"));
		});

		it('should return the token', async () => {
			DevicesSchema.actions.get = jest.fn(() => Promise.resolve({ active: true }));
			broker.createService(DevicesSchema);
			const result = await broker.call("authorization.authorize.loginByDevice", { deviceId: '1' });
			expect(result).toMatchObject(TokenHash);
		});
	});

	describe("Test 'authorization.authorize.loginByDeviceType' action", () => {
		const supportedDeviceTypes = ["MOBILE", "BO", "WEB", "POS", "GENERIC"];
		it('should not authorize device', async () => {
			await expect(broker.call("authorization.authorize.loginByDeviceType", { type: 'TABLET' }))
				.rejects
				.toThrow(new MoleculerError(`The device type provided cannot be authorized using this endpoint (only available for [${supportedDeviceTypes}]). Please use the endpoint /api/v1/authorize/device/{id} (providing the ID of the device you are trying to authenticate).`, 400));
		});

		it('should not find device', async () => {
			await expect(broker.call("authorization.authorize.loginByDeviceType", { type: 'MOBILE' }))
				.rejects
				.toThrow(new Errors.EntityNotFoundError("devices", 'MOBILE'));
		});

		it('should not find device active', async () => {
			await expect(broker.call("authorization.authorize.loginByDeviceType", { type: 'POS' }))
				.rejects
				.toThrow(new Errors.ValidationError("Device not active", "DEVICE_NOT_ACTIVE", { device: "deviceId" }));
		});

		it('should return the token', async () => {
			DevicesSchema.actions.get = jest.fn(() => Promise.resolve({ active: true }));
			broker.createService(DevicesSchema);
			const result = await broker.call("authorization.authorize.loginByDeviceType", { type: 'WEB' });
			expect(result).toMatchObject(TokenHash);
		});
	});

	describe("Test 'authorization.authorize.validateToken' action", () => {
		it("should validate token", async () => {
			jest.spyOn(jwt, 'verify').mockImplementation(() => {
				return { user: { id: '1' }, device: [{ id: '1' }] };
			});
			const result = await broker.call("authorization.authorize.validateToken", { token: 'testToken' });
			expect(result).toMatchObject({ "scopes": ["read"], "user": "User" });
		});

		it("should return empty token info", async () => {
			jest.spyOn(jwt, 'verify').mockImplementation(() => {
				return {};
			});
			const result = await broker.call("authorization.authorize.validateToken", { token: 'testToken' });
			expect(result).toBeUndefined();
		});

		it("should return token expired", async () => {
			jest.spyOn(jwt, 'verify').mockImplementation(() => {
				throw new jwt.TokenExpiredError();
			});
			await expect(broker.call("authorization.authorize.validateToken", { token: 'testToken' }))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Token expired", "ERR_TOKEN_EXPIRED"));
		});

		it("should return token invalid", async () => {
			jest.spyOn(jwt, 'verify').mockImplementation(() => {
				throw new Error();
			});
			await expect(broker.call("authorization.authorize.validateToken", { token: 'testToken' }))
				.rejects
				.toThrow(new Errors.UnauthorizedError("Token invalid", "ERR_INVALID_TOKEN"));
		});
	});

	// describe("Test 'authorization.authorize.loggedUser' action", () => {
	// 	it("should test loggedUser action", async () => {
	// 		const result = await broker.call("authorization.authorize.loggedUser");
	// 		expect(result).toBe("Hello Moleculer");
	// 	});
	// });

	/**
	 * Methods
	 */
	describe("Test 'findUser' method", () => {
		it("should not find a active user", async () => {
			await expect(AuthorizeService.findUser(broker, 'email', 'nouser@mail.com'))
				.rejects
				.toThrow(new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {}));
		});
	});

	describe("Test 'validateEmailUsername' method", () => {
		it("should fail validation (password or pin required)", async () => {
			const params = { user_name: 'joe' };
			expect(() => {
				AuthorizeService.validateEmailUsername(params);
			}).toThrow(new Errors.ValidationError(
				"'password' or 'pin' are required if an email/user_name is provided.",
				"AUTH_PASSWORD_OR_PIN_REQUIRED",
			));
		});
	});

	describe("Test 'validatePinPassword' method", () => {
		it("should fail validation (user_name or email required)", async () => {
			const params = { password: '123456' };
			expect(() => {
				AuthorizeService.validatePinPassword(params);
			}).toThrow(new Errors.ValidationError(
				"'email' or 'user_name' are required if an pin/password is provided.",
				"AUTH_EMAIL_OR_USERNAME_REQUIRED",
			));
		});
	});

	describe("Test 'validateRFID' method", () => {
		it("should fail validation (pin required)", async () => {
			const params = { rfid: 'DC43ED4' };
			expect(() => {
				AuthorizeService.validateRFID(params);
			}).toThrow(new Errors.ValidationError(
				"'pin' are required if an rfid is provided.",
				"AUTH_PIN_REQUIRED",
			));
		});
	});

	describe("Test 'clearCache' method", () => {
		it("should return the clean cache value", async () => {
			const result = AuthorizeService.clearCache();
			expect(result).toMatchObject(Promise.resolve());
		});

		it("should call the cacher clean method", async () => {
			broker.cacher = {};
			broker.cacher.clean = jest.fn(() => Promise.resolve());
			const result = AuthorizeService.clearCache();
			expect(result).toMatchObject(Promise.resolve());
		});
	});
});
