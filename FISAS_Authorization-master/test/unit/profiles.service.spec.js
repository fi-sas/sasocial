const { ServiceBroker } = require("moleculer");
const ProfilesSchema = require("../../services/profiles.service");

const NEW_PROFILE = { name: "Profile 1", active: true };

const mockCreate = jest.fn(() => Promise.resolve(NEW_PROFILE));


describe("Test 'authorization.user-groups-scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested
	ProfilesSchema.actions.create = mockCreate;
	// Create the services necessary for tests
	const profilesSchema = broker.createService(ProfilesSchema);
	// Replace adapter's methods with a mock

	beforeAll(() => {
		profilesSchema.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.profiles.create' action", () => {
		it("should create the profile", async () => {
			const mockDate = new Date();
			jest.spyOn(global, 'Date')
				.mockImplementation(() => mockDate);

			const result = await broker.call("authorization.profiles.create", NEW_PROFILE);
			expect(result).toMatchObject({
				...NEW_PROFILE,
				created_at: mockDate,
				updated_at: mockDate,
			});
		});
	});
});
