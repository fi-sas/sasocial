"use strict";

const _ = require("lodash");

const { ServiceBroker } = require("moleculer");
const { ValidationError, EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;

const ScopesSchema = require("../../services/scopes.service");
const UsersSchema = require("../../services/users.service");
const UsersScopesSchema = require("../../services/users_scopes.service");
const UserGroupsSchema = require("../../services/user_groups.service");
const UsersUserGroupsSchema = require("../../services/users_user_groups.service");
const UserGroupsScopesSchema = require("../../services/user_groups_scopes.service");
const ProfilesSchema = require("../../services/profiles.service");
const ProfilesScopesSchema = require("../../services/profiles_scopes.service");

const SCOPES = [
	{ id: 1, parent_id: null, name: "Calendario", dependencies: [], permission: "calendar" },
	{ id: 2, parent_id: 1, name: "Configurações", dependencies: [], permission: "calendar:configurations" },
	{ id: 3, parent_id: 2, name: "Criar", dependencies: [], permission: "calendar:configurations:create" },
	{ id: 4, parent_id: 2, name: "Alterar", dependencies: [], permission: "calendar:configurations:update" },
	{ id: 5, parent_id: 1, name: "Categorias de Eventos", dependencies: [], permission: "calendar:event-categories" },
	{ id: 6, parent_id: 5, name: "Criar", dependencies: [], permission: "calendar:event-categories:create" },
	{ id: 7, parent_id: 5, name: "Alterar", dependencies: [], permission: "calendar:event-categories:update" },
	{ id: 8, parent_id: null, name: "Comunicação", dependencies: [], permission: "communication" },
	{ id: 9, parent_id: 8, name: "Categorias", dependencies: [], permission: "communication:categories" },
	{ id: 10, parent_id: 9, name: "Criar", dependencies: [], permission: "communication:categories:create" },
	{ id: 11, parent_id: 9, name: "Alterar", dependencies: [], permission: "communication:categories:update" },
	{ id: 12, parent_id: 8, name: "Feeds RSS", dependencies: [], permission: "communication:feeds" },
	{ id: 13, parent_id: 12, name: "Criar", dependencies: [], permission: "communication:feeds:create" },
	{ id: 14, parent_id: 12, name: "Alterar", dependencies: [], permission: "communication:feeds:update" },
];

const SCOPE_TREE = [
	{
		id: 1,
		name: 'Calendario',
		permission: 'calendar',
		grants: [],
		children: [
			{
				id: 2,
				name: 'Configurações',
				permission: 'calendar:configurations',
				grants: [],
				children: [
					{
						id: 3,
						name: 'Criar',
						permission: 'calendar:configurations:create',
						grants: [],
						children: []
					},
					{
						id: 4,
						name: 'Alterar',
						permission: 'calendar:configurations:update',
						grants: [],
						children: []
					}
				]
			},
			{
				id: 5,
				name: 'Categorias de Eventos',
				permission: 'calendar:event-categories',
				grants: [],
				children: [
					{
						id: 6,
						name: 'Criar',
						permission: 'calendar:event-categories:create',
						grants: [],
						children: []
					},
					{
						id: 7,
						name: 'Alterar',
						permission: 'calendar:event-categories:update',
						grants: [],
						children: []
					}
				]
			}
		]
	},
	{
		id: 8,
		name: 'Comunicação',
		permission: 'communication',
		grants: [],
		children: [
			{
				id: 9,
				name: 'Categorias',
				permission: 'communication:categories',
				grants: [],
				children: [
					{
						id: 10,
						name: 'Criar',
						permission: 'communication:categories:create',
						grants: [],
						children: []
					},
					{
						id: 11,
						name: 'Alterar',
						permission: 'communication:categories:update',
						grants: [],
						children: []
					}
				]
			},
			{
				id: 12,
				name: 'Feeds RSS',
				permission: 'communication:feeds',
				grants: [],
				children: [
					{
						id: 13,
						name: 'Criar',
						permission: 'communication:feeds:create',
						grants: [],
						children: []
					},
					{
						id: 14,
						name: 'Alterar',
						permission: 'communication:feeds:update',
						grants: [],
						children: []
					}
				]
			}
		]
	}
];

const NEW_SCOPE_TREE = {
	name: "Alojamento",
	dependencies: [],
	permission: "accommodation",
	children: [
		{
			name: "Ausências",
			dependencies: [],
			permission: "accommodation:absences",
			children: [
				{
					name: "Criar",
					dependencies: [],
					permission: "accommodation:absences:create"
				},
				{
					name: "Alterar",
					dependencies: [],
					permission: "accommodation:absences:update"
				},
			]
		},
		{
			name: "Candidaturas",
			dependencies: [],
			permission: "accommodation:applications",
			children: [
				{
					name: "Criar",
					dependencies: [],
					permission: "accommodation:applications:create"
				},
				{
					name: "Alterar",
					dependencies: [],
					permission: "accommodation:applications:update"
				},
			]
		},
	]
};

const NEW_SCOPE = {
	id: 15,
	parent_id: null,
	name: "Test",
	permission: "test",
	grants: [],
	profile_ids: [1, 2],
	user_group_ids: [1, 2]
};

const USER_GROUPS = [
	{ id: 1, name: "Group 1", scope_ids: [15] },
	{ id: 2, name: "Group 2", scope_ids: [15] },
];

const PROFILES = [
	{ id: 1, name: "Profile 1", active: true },
	{ id: 1, name: "Profile 2", active: false },
];

const USERS = [
	{ id: 1, name: "User 1", profile_id: 1, },
	{ id: 2, name: "User 2", profile_id: 2, },
];

const USERS_USER_GROUPS = [
	{ id: 1, user_id: 1, user_group_id: 1 },
	{ id: 2, user_id: 2, user_group_id: 2 },
]

const USERS_SCOPES = [
	{ id: 1, user_id: 1, scope_id: 1 },
	{ id: 2, user_id: 2, scope_id: 9 },
]

const USER_GROUPS_SCOPES = [
	{ id: 1, user_group_id: 2, scope_id: 12 },
]

const mockUserGroupsCount = jest.fn(() => Promise.resolve(2));

const mockProfilesCount = jest.fn(() => Promise.resolve(2));

const mockAdapterFindById = jest.fn(() => Promise.resolve([NEW_SCOPE]));

const mockSaveGroupsOfScopes = jest.fn(() => Promise.resolve());

const mockCreateProfilesScopes = jest.fn(() => Promise.resolve());

const mockUserGroupsOfScope = jest.fn(() => Promise.resolve(USER_GROUPS));

const mockFetchProfilesByScope = jest.fn(() => Promise.resolve(PROFILES));

const mockFetchScopesByProfile = jest.fn(() => Promise.resolve([NEW_SCOPE]));

const mockUsersFind = jest.fn((params) => Promise.resolve(_.filter(USERS, params.query)));

const mockFetchScopesByUser = jest.fn((ctx) => {
	const usersScopes = _.filter(USERS_SCOPES, { user_id: ctx.params.user_id });
	const scopes = [];
	usersScopes.forEach(userScope => {
		scopes.push(_.find(SCOPES, { id: userScope.scope_id }));
	});
	return Promise.resolve(scopes);
})

const mockFetchUserGroupsByUser = jest.fn((ctx) => {
	const usersUserGroups = _.filter(USERS_USER_GROUPS, { user_id: ctx.params.user_id });
	const userGroups = [];
	usersUserGroups.forEach(usersUserGroup => {
		userGroups.push(_.find(USERS_USER_GROUPS, { id: usersUserGroup.user_id }));
	});
	return Promise.resolve(userGroups);
})

const mockFetchGroupScopes = jest.fn((ctx) => {
	const userGroupsScopes = _.filter(USER_GROUPS_SCOPES, { user_group_id: ctx.params.user_group_id });
	const scopes = [];
	userGroupsScopes.forEach(userGroupsScope => {
		scopes.push(_.find(SCOPES, { id: userGroupsScope.scope_id }));
	});
	return Promise.resolve(scopes);
})


describe("Test 'authorization.scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'Scopes Service' to be tested
	UserGroupsScopesSchema.actions.save_groups_of_scopes = mockSaveGroupsOfScopes;
	UserGroupsScopesSchema.actions.user_groups_of_scope = mockUserGroupsOfScope;
	UserGroupsScopesSchema.actions.fetchGroupScopes = mockFetchGroupScopes;
	UserGroupsSchema.actions.count = mockUserGroupsCount;
	ProfilesSchema.actions.count = mockProfilesCount;
	ProfilesScopesSchema.actions.create_profiles_scopes = mockCreateProfilesScopes;
	ProfilesScopesSchema.actions.fetchProfilesByScope = mockFetchProfilesByScope;
	ProfilesScopesSchema.actions.fetchScopesByProfile = mockFetchScopesByProfile;
	UsersSchema.actions.find = mockUsersFind;
	UsersScopesSchema.actions.fetchScopesByUser = mockFetchScopesByUser;
	UsersUserGroupsSchema.actions.fetchUserGroupsByUser = mockFetchUserGroupsByUser;
	// Create the services necessary for tests
	broker.createService(UserGroupsSchema);
	broker.createService(ProfilesSchema);
	broker.createService(ProfilesScopesSchema);
	broker.createService(UserGroupsScopesSchema);
	broker.createService(UsersSchema);
	broker.createService(UsersScopesSchema);
	broker.createService(UsersUserGroupsSchema);
	const scopesService = broker.createService(ScopesSchema);
	// Replace adapter's methods with a mock
	scopesService.adapter.findById = mockAdapterFindById;

	beforeAll(() => {
		scopesService.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	const mockKnex = (mockOverrides) => {
		const mockedFunctions = {
			del: jest.fn().mockReturnThis(),
			returning: jest.fn().mockReturnThis(),
			whereNull: jest.fn().mockReturnThis(),
			where: jest.fn().mockReturnThis(),
			insert: jest.fn().mockReturnThis(),
			limit: jest.fn().mockReturnThis(),
			then: jest.fn((done) => done(null)),
			...mockOverrides,
		};
		scopesService.adapter.db = () => mockedFunctions;
	}

	/**
	 * Actions
	 */
	describe("Test 'authorization.scopes.postCreateScopeTree' action", () => {
		beforeEach(() => mockKnex({
			insert: (params) => params,
		}));

		it("should create scope tree", async () => {
			const result = await broker.call("authorization.scopes.postCreateScopeTree", { scopes: NEW_SCOPE_TREE });
			expect(result).toEqual([NEW_SCOPE_TREE]);
		});

		it("should create scope tree", async () => {
			const result = await broker.call("authorization.scopes.postCreateScopeTree", { scopes: [NEW_SCOPE_TREE] });
			expect(result).toEqual([NEW_SCOPE_TREE]);
		});

		it("should throw an MoleculerError (required scopes)", async () => {
			await expect(broker.call("authorization.scopes.postCreateScopeTree", { scopes: undefined }))
				.rejects
				.toThrow(new ValidationError("Parameters validation error!"));
		});
	});

	describe("Test 'authorization.scopes.getScopesTree' action", () => {
		beforeEach(() => mockKnex({
			insert: (params) => params,
			whereNull: (field) => _.filter(SCOPES, { [field]: null }),
			where: (field, value) => _.filter(SCOPES, { [field]: value }),
		}));

		it("should return the scope tree", async () => {
			const result = await broker.call("authorization.scopes.getScopesTree");
			expect(result).toEqual(SCOPE_TREE);
		});
	});

	describe("Test 'authorization.scopes.create' action", () => {
		beforeEach(() => mockKnex({
			insert: (params) => params,
		}));

		it("should fail validation ('name' and 'permission' are required fields)", async () => {
			const newScope = { ...NEW_SCOPE };
			delete newScope.name;
			delete newScope.permission;

			await expect(broker.call("authorization.scopes.create", newScope))
				.rejects
				.toThrow(new ValidationError("Entity validation error!"));
		});

		it("should fail validation (invalid 'user_group_ids')", async () => {
			const newScope = { ...NEW_SCOPE, user_group_ids: 'a' };

			await expect(broker.call("authorization.scopes.create", newScope))
				.rejects
				.toThrow(new ValidationError("Entity validation error!"));
		});

		it("should fail validation (invalid 'profile_ids')", async () => {
			const newScope = { ...NEW_SCOPE, profile_ids: 'a' };

			await expect(broker.call("authorization.scopes.create", newScope))
				.rejects
				.toThrow(new ValidationError("Entity validation error!"));
		});

		it("should throw an EntityNotFoundError", async () => {
			const newScope = { ...NEW_SCOPE, user_group_ids: [1, 2, 3] };
			await expect(broker.call("authorization.scopes.create", newScope))
				.rejects
				.toThrow(new EntityNotFoundError("scopes", null));
		});

		it("should throw an EntityNotFoundError", async () => {
			const newScope = { ...NEW_SCOPE, profile_ids: [1, 2, 3] };
			await expect(broker.call("authorization.scopes.create", newScope))
				.rejects
				.toThrow(new EntityNotFoundError("scopes", null));

		});

		it("should create a scope", async () => {
			const newScope = { ...NEW_SCOPE };

			const expectedResult = {
				...NEW_SCOPE,
				user_groups: USER_GROUPS,
				profiles: PROFILES
			};
			delete expectedResult.profile_ids;
			delete expectedResult.user_group_ids;

			const result = await broker.call("authorization.scopes.create", newScope);
			expect(result).toEqual([expectedResult]);
			expect(mockUserGroupsCount).toHaveBeenCalled();
			expect(mockProfilesCount).toHaveBeenCalled();
		});
	});

	describe("Test 'authorization.scopes.findAllUserAvailableScopesStrings' action", () => {
		it('should return user scopes', async () => {
			const result = await broker.call("authorization.scopes.findAllUserAvailableScopesStrings", { user_id: 1, profile_id: 1 });
			expect(Array.isArray(result)).toBe(true);
			expect(result.sort()).toEqual(["test", "calendar"].sort());
		});

		it('should return user scopes', async () => {
			const result = await broker.call("authorization.scopes.findAllUserAvailableScopesStrings", { user_id: 2, profile_id: 1 });
			expect(Array.isArray(result)).toBe(true);
			expect(result.sort()).toEqual(["test", "communication:categories", "communication:feeds"].sort());
		});
	});
});
