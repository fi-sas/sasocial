const { ServiceBroker } = require("moleculer");
const { Errors } = require("@fisas/ms_core").Helpers;
const UserGroupsSchema = require("../../services/user_groups.service");
const UserGroupsScopesSchema = require("../../services/user_groups_scopes.service");
const ScopesSchema = require("../../services/scopes.service");

const USER_GROUPS = [
	{ id: 1, name: "Group 1" },
	{ id: 2, name: "Group 2" },
]

const NEW_USER_GROUP = {
	id: 3,
	name: "Group 3",
	scope_ids: [1, 2],
};

const SCOPES = [
	{ id: 1, parent_id: null, name: "Calendario", dependencies: [], permission: "calendar" },
	{ id: 2, parent_id: null, name: "Comunicação", dependencies: [], permission: "communication" },
];

const mockAdapterFind = jest.fn(() => Promise.resolve(USER_GROUPS));

const mockAdapterFindById = jest.fn(() => Promise.resolve([NEW_USER_GROUP]));

const mockAdapterInsert = jest.fn((params) => Promise.resolve([params.id, { ...params }]));

const mockAdapterCount = jest.fn(() => Promise.resolve(2));

const mockSaveScopesOfGroup = jest.fn(() => Promise.resolve());

const mockScopesOfGroups = jest.fn(() => Promise.resolve(SCOPES));


describe("Test 'authorization.user-groups' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested
	UserGroupsScopesSchema.actions.save_scopes_of_group = mockSaveScopesOfGroup;
	UserGroupsScopesSchema.actions.fetchGroupScopes = mockScopesOfGroups;
	// Create the services necessary for tests
	const userGroupsService = broker.createService(UserGroupsSchema);
	const scopesService = broker.createService(ScopesSchema);
	broker.createService(UserGroupsScopesSchema);
	// Replace adapter's methods with a mock
	userGroupsService.adapter.insert = mockAdapterInsert;
	userGroupsService.adapter.find = mockAdapterFind;
	userGroupsService.adapter.findById = mockAdapterFindById;
	scopesService.adapter.count = mockAdapterCount;

	beforeAll(() => {
		userGroupsService.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.user-groups.create' action", () => {
		it("should fail validation (required scope_ids)", async () => {
			const newUserGroup = { ...NEW_USER_GROUP };
			delete newUserGroup["scope_ids"];

			await expect(broker.call("authorization.user-groups.create", newUserGroup))
				.rejects
				.toThrow(new Errors.ValidationError("Entity validation error!"));
			expect(mockAdapterInsert).toBeCalledTimes(0);
		});

		it("should throw an EntityNotFoundError", async () => {
			const newUserGroup = { ...NEW_USER_GROUP, scope_ids: [1, 2, 300] };

			await expect(broker.call("authorization.user-groups.create", newUserGroup))
				.rejects
				.toThrow(new Errors.EntityNotFoundError("scopes", null));
			expect(mockAdapterInsert).toBeCalledTimes(0);
		});

		it("should create a User Group", async () => {
			const newUserGroup = { ...NEW_USER_GROUP };
			const expectedResult = {
				...NEW_USER_GROUP,
				scopes: SCOPES,
			};
			delete expectedResult["scope_ids"];

			const result = await broker.call("authorization.user-groups.create", newUserGroup);
			expect(result).toEqual([expectedResult]);
			expect(mockAdapterInsert).toBeCalledTimes(1);
		});
	});
});
