const { ServiceBroker } = require("moleculer");
const { Errors } = require("@fisas/ms_core").Helpers;
const UserGroupsScopesSchema = require("../../services/user_groups_scopes.service");
const ScopesSchema = require("../../services/scopes.service");
const UserGroupsSchema = require("../../services/user_groups.service");

const USER_GROUPS_SCOPES = [
	{ id: 1, scope_id: 1, user_group_id: 1 },
	{ id: 2, scope_id: 2, user_group_id: 1 },
];

const USER_GROUPS = [
	{ id: 1, name: "Group 1" },
	{ id: 2, name: "Group 2" },
]

const SCOPES = [
	{ id: 1, parent_id: null, name: "Calendario", dependencies: [], permission: "calendar" },
	{ id: 2, parent_id: null, name: "Comunicação", dependencies: [], permission: "communication" },
];

const mockAdapterRemoveMany = jest.fn(() => Promise.resolve());

const mockInsert = jest.fn((ctx, params) => {
	const entities = params.entities;
	for (let index = 0; index < entities.length; index++) {
		entities[index] = {
			id: index + 1,
			...entities[index]
		};
	}
	return Promise.resolve(entities)
});

const mockScopesFind = jest.fn(() => Promise.resolve(SCOPES));

const mockUserGroupsFind = jest.fn(() => Promise.resolve(USER_GROUPS));

const mockAdapterFind = jest.fn(() => Promise.resolve(USER_GROUPS_SCOPES));


describe("Test 'authorization.user-groups-scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested
	ScopesSchema.actions.find = mockScopesFind;
	UserGroupsSchema.actions.find = mockUserGroupsFind;
	// Create the services necessary for tests
	broker.createService(ScopesSchema);
	broker.createService(UserGroupsSchema);
	const userGroupsScopesService = broker.createService(UserGroupsScopesSchema);
	// Replace adapter's methods with a mock
	userGroupsScopesService._insert = mockInsert;
	userGroupsScopesService.adapter.find = mockAdapterFind;
	userGroupsScopesService.adapter.removeMany = mockAdapterRemoveMany;

	beforeAll(() => {
		userGroupsScopesService.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.user-groups-scopes.save_scopes_of_group' action", () => {
		const params = {
			user_group_id: 1,
			scopes_ids: [1, 2]
		};

		it("should return the user-groups-scopes array", async () => {
			const expectedResult = [{
				scope_id: 1,
				user_group_id: 1,
			}, {
				scope_id: 2,
				user_group_id: 1,
			}];

			const result = await broker.call("authorization.user-groups-scopes.save_scopes_of_group", params);
			expect(result).toMatchObject(expectedResult);
		});

		it("should call emptyCache method", async () => {
			const spyClearCache = jest.spyOn(userGroupsScopesService, "clearCache");
			await broker.call("authorization.user-groups-scopes.save_scopes_of_group", params);
			expect(spyClearCache).toBeCalled();
		});

		it("should fail validation (invalid parameters) ", async () => {
			const params = {
				user_group_id: "a",
				scopes_ids: [-1, "b"]
			};
			await expect(broker.call("authorization.user-groups-scopes.save_scopes_of_group", params))
				.rejects
				.toThrow(new Errors.ValidationError("Parameters validation error!"));
		});
	});

	describe("Test 'authorization.user-groups-scopes.fetchGroupScopes' action", () => {
		it("should return the scopes array", async () => {
			const result = await broker.call("authorization.user-groups-scopes.fetchGroupScopes", {
				user_group_id: 1
			});
			expect(result).toEqual(SCOPES);
		});

		it("should fail validation (invalid parameters) ", async () => {
			const params = {
				user_group_id: "a"
			};
			await expect(broker.call("authorization.user-groups-scopes.fetchGroupScopes", params))
				.rejects
				.toThrow(new Errors.ValidationError("Parameters validation error!"));
		});
	});

	describe("Test 'authorization.user-groups-scopes.user_groups_of_scope' action", () => {
		it("should return the user-groups array", async () => {
			const result = await broker.call("authorization.user-groups-scopes.user_groups_of_scope", {
				scope_id: 1
			});
			expect(result).toEqual(USER_GROUPS);
		});

		it("should fail validation (invalid parameters) ", async () => {
			const params = {
				scope_id: "a"
			};
			await expect(broker.call("authorization.user-groups-scopes.user_groups_of_scope", params))
				.rejects
				.toThrow(new Errors.ValidationError("Parameters validation error!"));
		});
	});

	describe("Test 'authorization.user-groups-scopes.save_groups_of_scopes' action", () => {
		const params = {
			scope_id: 1,
			user_group_ids: [1]
		};

		it("should return the user-groups-scopes array", async () => {
			const expectedResult = [{
				scope_id: 1,
				user_group_id: 1,
			}];

			const result = await broker.call("authorization.user-groups-scopes.save_groups_of_scopes", params);
			expect(result).toMatchObject(expectedResult);
		});

		it("should call emptyCache method", async () => {
			const spyClearCache = jest.spyOn(userGroupsScopesService, "clearCache");
			await broker.call("authorization.user-groups-scopes.save_groups_of_scopes", params);
			expect(spyClearCache).toBeCalled();
		});

		it("should fail validation (invalid parameters) ", async () => {
			const params = {
				scope_id: "a",
				user_group_ids: [-1, "b"]
			};
			await expect(broker.call("authorization.user-groups-scopes.save_groups_of_scopes", params))
				.rejects
				.toThrow(new Errors.ValidationError("Parameters validation error!"));
		});
	});
});
