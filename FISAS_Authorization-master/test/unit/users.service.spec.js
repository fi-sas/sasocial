const _ = require("lodash");

const { ServiceBroker } = require("moleculer");
const UsersSchema = require("../../services/users.service");

const USERS = [
	{
		id: 1,
		name: "User 1",
		email: "user1@fisas.com",
		phone: '912345678',
		user_name: "user1",
		institute: "Institute 1",
		student_number: "A1",
		identification: "12345678",
		external: false,
		// pin: salt + '1234'
		pin: "b9e12d4af29d176b94550d68b3ef6a3d8f3c14f256579a3ff60ae0746a94f593",
		// password: salt + 'passwd'
		password: "b6f1a5a029367ef9fb57606b7738c44f7cfa8fdd39431fce0ea4ec9a2ccb6fd9",
		salt: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
		can_access_BO: true,
		active: true,
		birth_date: "1996-01-01",
		gender: "M",
		picture: "",
		fb_messenger_id: "",
		address: "Rua Test",
		postal_code: "1234-123",
		city: "Test",
		country: "Portugal",
		tin: "",
		profile_id: 1,
		scope_ids: [1],
		user_group_ids: [1]
	},
];

const mockAdapterFindById = jest.fn((id) => Promise.resolve(_.filter(USERS, { id })));

const mockGetById = jest.fn((id) => Promise.resolve(_.find(USERS, { id })));


describe("Test 'authorization.user-groups-scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested

	// Create the services necessary for tests
	const usersSchema = broker.createService(UsersSchema);
	// Replace adapter's methods with a mock
	usersSchema.getById = mockGetById;
	usersSchema.adapter.findById = mockAdapterFindById;

	beforeAll(() => {
		usersSchema.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.users.checkPassword' action", () => {
		it("should return true", async () => {
			const result = await broker.call("authorization.users.checkPassword", {
				password: 'passwd',
				user_id: 1,
			});
			expect(result).toBe(true);
		});

		it("should return false", async () => {
			const result = await broker.call("authorization.users.checkPassword", {
				password: '00passwd00',
				user_id: 1,
			});
			expect(result).toBe(false);
		});
	});

	describe("Test 'authorization.users.checkPin' action", () => {
		it("should return true", async () => {
			const result = await broker.call("authorization.users.checkPin", {
				pin: '1234',
				user_id: 1,
			});
			expect(result).toBe(true);
		});

		it("should return false", async () => {
			const result = await broker.call("authorization.users.checkPin", {
				pin: '0000',
				user_id: 1,
			});
			expect(result).toBe(false);
		});
	});
});
