const { ServiceBroker } = require("moleculer");
const { Errors } = require("@fisas/ms_core").Helpers;
const UsersScopesSchema = require("../../services/users_scopes.service");
const ScopesSchema = require("../../services/scopes.service");

const SCOPES = [
	{ id: 1, parent_id: null, name: "Calendario", dependencies: [], permission: "calendar" },
	{ id: 2, parent_id: null, name: "Comunicação", dependencies: [], permission: "communication" },
];

const USERS_SCOPES = [
	{ id: 1, scope_id: 1, user_id: 1 },
];

const NEW_USERS_SCOPE = { scope_id: 2, user_id: 1 };

const mockScopesGet = jest.fn(() => Promise.resolve(SCOPES));


describe("Test 'authorization.users-scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested
	ScopesSchema.actions.get = mockScopesGet;
	// Create the services necessary for tests
	broker.createService(ScopesSchema);
	const userScopesService = broker.createService(UsersScopesSchema);

	beforeAll(() => {
		userScopesService.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.users-scopes.fetchScopesByUser' action", () => {
		beforeEach(() => {
			userScopesService.adapter.find = jest.fn(() => Promise.resolve(USERS_SCOPES));;
		});

		it("should return the scopes array", async () => {
			const result = await broker.call("authorization.users-scopes.fetchScopesByUser", {
				user_id: 1
			});
			expect(result).toMatchObject(SCOPES);
		});

		it("should fail validation (invalid parameters) ", async () => {
			const params = {
				user_id: "a",
			};
			await expect(broker.call("authorization.users-scopes.fetchScopesByUser", params))
				.rejects
				.toThrow(new Errors.ValidationError("Parameters validation error!"));
		});
	});

	describe("Test 'authorization.users-scopes.create' action", () => {
		beforeEach(() => {
			userScopesService.adapter.find = jest.fn(() => Promise.resolve([{ id: 2, ...NEW_USERS_SCOPE }]));
			userScopesService.adapter.insert = jest.fn(() => Promise.resolve(2));
		});

		it("should create users-scopes", async () => {
			const result = await broker.call("authorization.users-scopes.create", NEW_USERS_SCOPE);
			expect(result).toMatchObject([{ id: 2, ...NEW_USERS_SCOPE }]);
		});
	});
});
