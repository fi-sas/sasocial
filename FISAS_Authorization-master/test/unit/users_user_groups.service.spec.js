const { ServiceBroker } = require("moleculer");
const UsersUserGroupsSchema = require("../../services/users_user_groups.service");
const UserGroupsSchema = require("../../services/user_groups.service");


const USERS_USER_GROUPS = [
	{ id: 1, user_id: 1, user_group_id: 1, }
]

const USER_GROUPS = [
	{ id: 1, name: "Group 1" },
]

const mockUserGroupsGet = jest.fn(() => Promise.resolve(USER_GROUPS));

const mockAdapterFind = jest.fn(() => Promise.resolve(USERS_USER_GROUPS));


describe("Test 'authorization.user-groups-scopes' actions", () => {
	let broker = new ServiceBroker({ logger: false });
	// Mock some actions from external services to allow 'User Groups Service' to be tested
	UserGroupsSchema.actions.get = mockUserGroupsGet;
	// Create the services necessary for tests

	const usersUserGroupsService = broker.createService(UsersUserGroupsSchema);
	broker.createService(UserGroupsSchema);
	// Replace adapter's methods with a mock
	usersUserGroupsService.adapter.find = mockAdapterFind;

	beforeAll(() => {
		usersUserGroupsService.dependencies = jest.fn();
		return broker.start();
	});

	afterAll(() => broker.stop());

	/**
	 * Actions
	 */
	describe("Test 'authorization.users-user-groups.fetchUserGroupsByUser' action", () => {
		it("should return the user-groups array", async () => {
			const result = await broker.call("authorization.users-user-groups.fetchUserGroupsByUser", {
				user_id: 1
			});
			expect(result).toMatchObject(USER_GROUPS);
		});
	});
});
