# [1.9.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.8.4...v1.9.0) (2022-03-18)


### Features

* **posts:** add url field and UrlMediaTV channel ([c007a9e](https://gitlab.com/fi-sas/FISAS_Communication/commit/c007a9e5930382d93f8c3068b46fa46c56ea56c1))

## [1.8.4](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.8.3...v1.8.4) (2022-02-09)


### Bug Fixes

* **posts:** add sort by to webpage posts ([e5f7b50](https://gitlab.com/fi-sas/FISAS_Communication/commit/e5f7b50c69153cd19b2c795cf2fa99e3364692f3))

## [1.8.3](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.8.2...v1.8.3) (2022-02-09)


### Bug Fixes

* **posts:** change validation for Mobile channel ([09aeced](https://gitlab.com/fi-sas/FISAS_Communication/commit/09aeced41ede3d7aa48e2a667cbf7b90c96424c2))

## [1.8.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.8.1...v1.8.2) (2022-01-19)


### Bug Fixes

* **posts:** update posts validation ([fb7c8c2](https://gitlab.com/fi-sas/FISAS_Communication/commit/fb7c8c279450c213b67f0639667b5363adc9b98e))

## [1.8.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.8.0...v1.8.1) (2021-11-03)


### Bug Fixes

* **posts:** change translations cache by header language_id ([6398bd2](https://gitlab.com/fi-sas/FISAS_Communication/commit/6398bd27e151828024ca991aefdfa12746ac4433))

# [1.8.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.7.0...v1.8.0) (2021-11-02)


### Features

* filter by language_id in category ([12f0a72](https://gitlab.com/fi-sas/FISAS_Communication/commit/12f0a72970411ecd4da4e7c57062c84c9d7bd921))

# [1.7.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.6...v1.7.0) (2021-10-22)


### Features

* **posts:** add 12 hours ttl cache to posts ([32a24ce](https://gitlab.com/fi-sas/FISAS_Communication/commit/32a24ce7041fc6f9594603507f447540bd4a8d89))

## [1.6.6](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.5...v1.6.6) (2021-10-06)


### Bug Fixes

* **posts:** fix dashboard error ([bacf6f4](https://gitlab.com/fi-sas/FISAS_Communication/commit/bacf6f45efda29aaa471a83d430893ef97b79ce8))

## [1.6.5](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.4...v1.6.5) (2021-10-01)


### Bug Fixes

* **posts:** add pagination to mobile and webpage requests ([53723bf](https://gitlab.com/fi-sas/FISAS_Communication/commit/53723bfdfed708a685b8e640f5b3642ca5341f12))
* **posts:** add pagination to webpage and mobile get posts requests ([02106d9](https://gitlab.com/fi-sas/FISAS_Communication/commit/02106d94106a0407b95f1636937628f563f91f35))

## [1.6.4](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.3...v1.6.4) (2021-09-14)


### Bug Fixes

* **seed:** fix configurations seed ([7655d09](https://gitlab.com/fi-sas/FISAS_Communication/commit/7655d0912d977ed49f088d85fee990ca815cf277))
* **seed:** fix coonfigurations seed ([ff8e78c](https://gitlab.com/fi-sas/FISAS_Communication/commit/ff8e78c5cad8f8eb88513ad1ab6146a117462712))

## [1.6.3](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.2...v1.6.3) (2021-08-05)


### Bug Fixes

* **feeds:** remove createb_by on fetchRss by BO ([b699514](https://gitlab.com/fi-sas/FISAS_Communication/commit/b69951441a8019861b8efe79806b79a20976e8bd))

## [1.6.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.1...v1.6.2) (2021-08-05)


### Bug Fixes

* **feeds:** fix method call ([690b9f4](https://gitlab.com/fi-sas/FISAS_Communication/commit/690b9f4c8c0137b0157331fc4605048d7d5aea23))

## [1.6.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.6.0...v1.6.1) (2021-08-05)


### Bug Fixes

* **feeds:** on fetch remove old tickers ([85a9d9c](https://gitlab.com/fi-sas/FISAS_Communication/commit/85a9d9cf23fafd671a9cf117de19c2c631154494))

# [1.6.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.5.3...v1.6.0) (2021-07-05)


### Bug Fixes

* **target_post_files:** add migration validations ([d34d64a](https://gitlab.com/fi-sas/FISAS_Communication/commit/d34d64a1a9e0e72cc2e68c2785e3d7dc3c6ffa49))
* **target_post_files:** change mixin name ([5ec5d83](https://gitlab.com/fi-sas/FISAS_Communication/commit/5ec5d83ef1cc3b8ba52cf78a7f2e6144ce9b80df))


### Features

* **target_posts:** add files to target post ([d1fdf37](https://gitlab.com/fi-sas/FISAS_Communication/commit/d1fdf37f06f0f674f33ab8b6a4e2a26a0ed24985))

## [1.5.3](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.5.2...v1.5.3) (2021-06-30)


### Bug Fixes

* **target_post:** change add scopes to change status ([4a1fcf0](https://gitlab.com/fi-sas/FISAS_Communication/commit/4a1fcf0982317873909361af9e93760541bc9484))
* change target_posts scopes ([0377843](https://gitlab.com/fi-sas/FISAS_Communication/commit/0377843ba8bd116baf4da170ad5f9c4af9d9b769))

## [1.5.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.5.1...v1.5.2) (2021-06-29)


### Bug Fixes

* **posts:** change required fields ([5d3095b](https://gitlab.com/fi-sas/FISAS_Communication/commit/5d3095b7c1d6c9ad7ff4f116bd02490f76082bfb))

## [1.5.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.5.0...v1.5.1) (2021-06-24)


### Bug Fixes

* **posts:** add required fields validation ([7c18671](https://gitlab.com/fi-sas/FISAS_Communication/commit/7c18671b8aea63f23983f7af8043708e6f064f46))

# [1.5.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.4.2...v1.5.0) (2021-06-22)


### Features

* **posts:** add location field to translation ([9829067](https://gitlab.com/fi-sas/FISAS_Communication/commit/9829067a083edff0713977c6f1fbf2c70f35bd65))

## [1.4.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.4.1...v1.4.2) (2021-06-22)


### Bug Fixes

* **posts:** change configurations.find to list ([356ce41](https://gitlab.com/fi-sas/FISAS_Communication/commit/356ce4153d1857b6628f0974d712d0e024147e9f))
* **posts:** change facebook credentials from .env to database configurations ([e49d6d3](https://gitlab.com/fi-sas/FISAS_Communication/commit/e49d6d33f336fb5e81bf8daee4d76a5b715cb1ba))

## [1.4.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.4.0...v1.4.1) (2021-06-15)


### Bug Fixes

* **feeds:** only remove older tickers created from cron ([741f9f0](https://gitlab.com/fi-sas/FISAS_Communication/commit/741f9f0b756d8fbf0aefa0a2151cd05f1adb28b4))

# [1.4.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.3.0...v1.4.0) (2021-06-15)


### Bug Fixes

* **posts:** add try catch to facebook posting requests ([ee77977](https://gitlab.com/fi-sas/FISAS_Communication/commit/ee779777d2d0e79aab23afd6ea4d0549b2b939e7))


### Features

* **target_post:** add created_by info to target_post ([bd8506b](https://gitlab.com/fi-sas/FISAS_Communication/commit/bd8506b6a84afbe2b8aa4755a0c92965c008a372))

# [1.3.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.5...v1.3.0) (2021-06-07)


### Features

* **posts:** add wp_dashboard request ([ea503ea](https://gitlab.com/fi-sas/FISAS_Communication/commit/ea503ea7630b10b32906702661124ce175f22993))

## [1.2.5](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.4...v1.2.5) (2021-06-01)


### Bug Fixes

* **categories:** create translation and update ([5bd1d89](https://gitlab.com/fi-sas/FISAS_Communication/commit/5bd1d89273b605ddaeb3e8bb95b435dde1682d79))

## [1.2.4](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.3...v1.2.4) (2021-05-27)


### Bug Fixes

* **posts:** webpage posts scope name ([b921ef9](https://gitlab.com/fi-sas/FISAS_Communication/commit/b921ef9a02c3591d4615c784a1a8db34a693d0e0))

## [1.2.3](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.2...v1.2.3) (2021-05-26)


### Bug Fixes

* **feeds:** remove unused field ([8148f61](https://gitlab.com/fi-sas/FISAS_Communication/commit/8148f610268dbcc86457c7b6be024c66f734d2fd))

## [1.2.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.1...v1.2.2) (2021-05-12)


### Bug Fixes

* **posts:** add logs ([0787820](https://gitlab.com/fi-sas/FISAS_Communication/commit/07878206761e6bf2a91a5c98f00da3662893bfe9))

## [1.2.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.2.0...v1.2.1) (2021-05-12)


### Bug Fixes

* **communication:** add debuging logs ([da44717](https://gitlab.com/fi-sas/FISAS_Communication/commit/da4471756184ecf2a0ef8985679b4558c5c9aca4))

# [1.2.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.1.0...v1.2.0) (2021-05-12)


### Bug Fixes

* **target_posts:** missing params id and status change ([2bb74e2](https://gitlab.com/fi-sas/FISAS_Communication/commit/2bb74e27d096cebac8a35da137c9edc1d28e2ef5))


### Features

* **posts:** integration with facebook API ([3e81c37](https://gitlab.com/fi-sas/FISAS_Communication/commit/3e81c37a9c2e270137119d4da0ad7d73f2c05bdf))

# [1.1.0](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.2...v1.1.0) (2021-05-04)


### Bug Fixes

* **post_target:** change target_post_channels to target_post_notification_methods ([0aaf9a8](https://gitlab.com/fi-sas/FISAS_Communication/commit/0aaf9a8460f26e825408dee42fe94d8753101346))
* **target_post:** remove unused  alert_template_id ([c32bdee](https://gitlab.com/fi-sas/FISAS_Communication/commit/c32bdee00d8cbe57dd5e7e78d26d2354daaf9502))
* **target_posts:** add failed status ([58c7d21](https://gitlab.com/fi-sas/FISAS_Communication/commit/58c7d213c3e331b5e3691b1dce500aaf9190ef77))
* **targets:** remover targets and target_conditions ([89b771f](https://gitlab.com/fi-sas/FISAS_Communication/commit/89b771ff9ee13e4b556f5842869d5a0adfb74932))


### Features

* **target_posts:** add targets and targets posts ([356fc67](https://gitlab.com/fi-sas/FISAS_Communication/commit/356fc67b407a517ed926b0c25e013e3eb68149a1))

## [1.0.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.1...v1.0.2) (2021-04-13)


### Bug Fixes

* **posts:** shoing posts even when is not approved ([1f16d94](https://gitlab.com/fi-sas/FISAS_Communication/commit/1f16d944200d472dcbf97b5e368f767420b64106))

## [1.0.1](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0...v1.0.1) (2021-03-24)


### Bug Fixes

* **posts:** add  filters by translation ([5462ded](https://gitlab.com/fi-sas/FISAS_Communication/commit/5462ded407a9776353752952663df4eb1f6b0583))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **actions:** add-actions-visibility-published ([cdb6286](https://gitlab.com/fi-sas/FISAS_Communication/commit/cdb62864e4ad32f1c702969843eaef34de94da40))
* **common:** fix get device id ([c8024b7](https://gitlab.com/fi-sas/FISAS_Communication/commit/c8024b77faf55ddf4bcbb9f091bc7e3b985980dd))
* **docker-compose:** create docker-compose to prod and dev ([08d308e](https://gitlab.com/fi-sas/FISAS_Communication/commit/08d308e6aa642305ef4b17d5490044eb3a566b85))
* **eslint:** update rules ([7cf3533](https://gitlab.com/fi-sas/FISAS_Communication/commit/7cf353381d8dd49e4f4081c14af58fe0aeb2b745))
* **feeds:** activate cron ([0a5a04c](https://gitlab.com/fi-sas/FISAS_Communication/commit/0a5a04cb6e43546225ab22681804f8057aee06ad))
* **feeds:** disable cache on fetchRSSFeeds ([1fc9f8d](https://gitlab.com/fi-sas/FISAS_Communication/commit/1fc9f8db4a8002e49f54b64fac2e70d2e333c48a))
* **feeds:** remove all tickers before fetch rss ([4f69072](https://gitlab.com/fi-sas/FISAS_Communication/commit/4f690725e741182185531fad6808e16b58dcc065))
* **feeds:** wrong action name ([8ed38af](https://gitlab.com/fi-sas/FISAS_Communication/commit/8ed38afb86b9179542716596937cf555b74e13d7))
* **knex:** add config pool to fix error connection ([094fcbd](https://gitlab.com/fi-sas/FISAS_Communication/commit/094fcbd646171d12f530bb3440f30832821eca58))
* **migration:** wrong table fields 'file_' place ([25f3726](https://gitlab.com/fi-sas/FISAS_Communication/commit/25f3726345047ebf6233b2952b1135df5bb74286))
* **post:** remove duplicated cache setting ([bffc68f](https://gitlab.com/fi-sas/FISAS_Communication/commit/bffc68f91ee307697359c5baf7ec5d43fa18432d))
* **post:** set cache by device.id ([0c78843](https://gitlab.com/fi-sas/FISAS_Communication/commit/0c7884332cf2004006f7059e1ff7efe0cc06656e))
* **posts:** add created_by field ([6d543b3](https://gitlab.com/fi-sas/FISAS_Communication/commit/6d543b385d1bbcdf41e2fd6030b3b3b930e6f5fa))
* **posts:** add date field ([4158d56](https://gitlab.com/fi-sas/FISAS_Communication/commit/4158d56482f3e4dffad61a9b7935e7154301262a))
* **posts:** add scopes to channels ([0894a59](https://gitlab.com/fi-sas/FISAS_Communication/commit/0894a59f847ea157b995aff8f4a9b7262340bbdd))
* **posts:** change created_by to created_by_id ([e76352b](https://gitlab.com/fi-sas/FISAS_Communication/commit/e76352bc2bf413370d9b36f622a150f1a59fc748))
* **posts:** channels when empty ([0cd6a66](https://gitlab.com/fi-sas/FISAS_Communication/commit/0cd6a6671e1f1c4c79d89070de3fc6fa13b3ef5c))
* **posts:** constraint by group device ([8ef8a98](https://gitlab.com/fi-sas/FISAS_Communication/commit/8ef8a98b52e61a34fc13ff5959afd28d5b89be9e))
* **posts:** fix alive returning only approved posts ([ede9c4f](https://gitlab.com/fi-sas/FISAS_Communication/commit/ede9c4f8f9c72ec7fbe707ebf037392a4f7db91a))
* **posts:** missing channel join ([6a22e96](https://gitlab.com/fi-sas/FISAS_Communication/commit/6a22e963d59846d8848c5a4c2e347cc3f378f9ec))
* **posts:** remove some defaultWithRelateds ([5f0a9db](https://gitlab.com/fi-sas/FISAS_Communication/commit/5f0a9db7b4a728212ea5d6f2991c4b8cb4572d0a))
* **seed:** change channels 'Media' to 'MediaKiosk' ([cebc68a](https://gitlab.com/fi-sas/FISAS_Communication/commit/cebc68ac210d3d2655d145d446329ade894af57c))
* **seeds:** move channels and rename samples ([468674c](https://gitlab.com/fi-sas/FISAS_Communication/commit/468674c43d9d636d2f47ee8cb09e6a21fdc24f63))
* **seeds:** remove extra seeds ([c0fb90c](https://gitlab.com/fi-sas/FISAS_Communication/commit/c0fb90cd8a0bd42ce6495ccb92d7b5fcc4a58816))
* **seeds:** specify folder for samples ([8356eea](https://gitlab.com/fi-sas/FISAS_Communication/commit/8356eea1b0ad3fb11f1b9ba4c0ee995230808b75))
* **translation:** missing language_id ([8f88593](https://gitlab.com/fi-sas/FISAS_Communication/commit/8f88593de266ad7270d5fb51a02a3d995d605905))
* **translations:** wrong filename field ([d4a5d54](https://gitlab.com/fi-sas/FISAS_Communication/commit/d4a5d54a25877035a2254444b2dc5d8c1c8f50d8))


### Features

* **npm-scripts:** add helper scripts ([d278f5d](https://gitlab.com/fi-sas/FISAS_Communication/commit/d278f5d7a9c4294c2ee35907d5defec3a29ae233))
* **posts:** add webpage channel ([1d9eaa1](https://gitlab.com/fi-sas/FISAS_Communication/commit/1d9eaa188b16c1a804a6da488b04a9fb749e73f2))
* **seeds:** add sample seeds ([d896cfb](https://gitlab.com/fi-sas/FISAS_Communication/commit/d896cfb4443eba6649468481214a3f2fe9e2c21a))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-03-04)


### Bug Fixes

* **posts:** remove some defaultWithRelateds ([5f0a9db](https://gitlab.com/fi-sas/FISAS_Communication/commit/5f0a9db7b4a728212ea5d6f2991c4b8cb4572d0a))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-03-04)


### Features

* **posts:** add webpage channel ([1d9eaa1](https://gitlab.com/fi-sas/FISAS_Communication/commit/1d9eaa188b16c1a804a6da488b04a9fb749e73f2))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-03-02)


### Bug Fixes

* **posts:** fix alive returning only approved posts ([ede9c4f](https://gitlab.com/fi-sas/FISAS_Communication/commit/ede9c4f8f9c72ec7fbe707ebf037392a4f7db91a))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-02-23)


### Bug Fixes

* **posts:** add created_by field ([6d543b3](https://gitlab.com/fi-sas/FISAS_Communication/commit/6d543b385d1bbcdf41e2fd6030b3b3b930e6f5fa))
* **posts:** change created_by to created_by_id ([e76352b](https://gitlab.com/fi-sas/FISAS_Communication/commit/e76352bc2bf413370d9b36f622a150f1a59fc748))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-02-22)


### Bug Fixes

* **posts:** add date field ([4158d56](https://gitlab.com/fi-sas/FISAS_Communication/commit/4158d56482f3e4dffad61a9b7935e7154301262a))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-05)


### Bug Fixes

* **eslint:** update rules ([7cf3533](https://gitlab.com/fi-sas/FISAS_Communication/commit/7cf353381d8dd49e4f4081c14af58fe0aeb2b745))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-05)


### Bug Fixes

* **seeds:** move channels and rename samples ([468674c](https://gitlab.com/fi-sas/FISAS_Communication/commit/468674c43d9d636d2f47ee8cb09e6a21fdc24f63))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2020-12-29)


### Bug Fixes

* **post:** remove duplicated cache setting ([bffc68f](https://gitlab.com/fi-sas/FISAS_Communication/commit/bffc68f91ee307697359c5baf7ec5d43fa18432d))
* **post:** set cache by device.id ([0c78843](https://gitlab.com/fi-sas/FISAS_Communication/commit/0c7884332cf2004006f7059e1ff7efe0cc06656e))
* **posts:** constraint by group device ([8ef8a98](https://gitlab.com/fi-sas/FISAS_Communication/commit/8ef8a98b52e61a34fc13ff5959afd28d5b89be9e))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-12-29)


### Bug Fixes

* **feeds:** remove all tickers before fetch rss ([4f69072](https://gitlab.com/fi-sas/FISAS_Communication/commit/4f690725e741182185531fad6808e16b58dcc065))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-12-18)


### Bug Fixes

* **seeds:** remove extra seeds ([c0fb90c](https://gitlab.com/fi-sas/FISAS_Communication/commit/c0fb90cd8a0bd42ce6495ccb92d7b5fcc4a58816))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-18)


### Bug Fixes

* **knex:** add config pool to fix error connection ([094fcbd](https://gitlab.com/fi-sas/FISAS_Communication/commit/094fcbd646171d12f530bb3440f30832821eca58))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-17)


### Bug Fixes

* **feeds:** wrong action name ([8ed38af](https://gitlab.com/fi-sas/FISAS_Communication/commit/8ed38afb86b9179542716596937cf555b74e13d7))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/FISAS_Communication/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-17)


### Bug Fixes

* **feeds:** disable cache on fetchRSSFeeds ([1fc9f8d](https://gitlab.com/fi-sas/FISAS_Communication/commit/1fc9f8db4a8002e49f54b64fac2e70d2e333c48a))

# 1.0.0-rc.1 (2020-12-16)


### Bug Fixes

* **actions:** add-actions-visibility-published ([cdb6286](https://gitlab.com/fi-sas/FISAS_Communication/commit/cdb62864e4ad32f1c702969843eaef34de94da40))
* **common:** fix get device id ([c8024b7](https://gitlab.com/fi-sas/FISAS_Communication/commit/c8024b77faf55ddf4bcbb9f091bc7e3b985980dd))
* **docker-compose:** create docker-compose to prod and dev ([08d308e](https://gitlab.com/fi-sas/FISAS_Communication/commit/08d308e6aa642305ef4b17d5490044eb3a566b85))
* **feeds:** activate cron ([0a5a04c](https://gitlab.com/fi-sas/FISAS_Communication/commit/0a5a04cb6e43546225ab22681804f8057aee06ad))
* **migration:** wrong table fields 'file_' place ([25f3726](https://gitlab.com/fi-sas/FISAS_Communication/commit/25f3726345047ebf6233b2952b1135df5bb74286))
* **posts:** add scopes to channels ([0894a59](https://gitlab.com/fi-sas/FISAS_Communication/commit/0894a59f847ea157b995aff8f4a9b7262340bbdd))
* **posts:** channels when empty ([0cd6a66](https://gitlab.com/fi-sas/FISAS_Communication/commit/0cd6a6671e1f1c4c79d89070de3fc6fa13b3ef5c))
* **posts:** missing channel join ([6a22e96](https://gitlab.com/fi-sas/FISAS_Communication/commit/6a22e963d59846d8848c5a4c2e347cc3f378f9ec))
* **seed:** change channels 'Media' to 'MediaKiosk' ([cebc68a](https://gitlab.com/fi-sas/FISAS_Communication/commit/cebc68ac210d3d2655d145d446329ade894af57c))
* **seeds:** specify folder for samples ([8356eea](https://gitlab.com/fi-sas/FISAS_Communication/commit/8356eea1b0ad3fb11f1b9ba4c0ee995230808b75))
* **translation:** missing language_id ([8f88593](https://gitlab.com/fi-sas/FISAS_Communication/commit/8f88593de266ad7270d5fb51a02a3d995d605905))
* **translations:** wrong filename field ([d4a5d54](https://gitlab.com/fi-sas/FISAS_Communication/commit/d4a5d54a25877035a2254444b2dc5d8c1c8f50d8))


### Features

* **npm-scripts:** add helper scripts ([d278f5d](https://gitlab.com/fi-sas/FISAS_Communication/commit/d278f5d7a9c4294c2ee35907d5defec3a29ae233))
* **seeds:** add sample seeds ([d896cfb](https://gitlab.com/fi-sas/FISAS_Communication/commit/d896cfb4443eba6649468481214a3f2fe9e2c21a))
