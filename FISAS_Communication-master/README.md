[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# Communication Microservice
This is a [Moleculer](https://moleculer.services/)-based microservices project. Generated with the [Moleculer CLI](https://moleculer.services/docs/0.14/moleculer-cli.html).

## Usage
Start the project with `npm run dev` command. 
After starting, open the http://localhost:3000/ URL in your browser. 
On the welcome page you can test the generated services via API Gateway and check the nodes & services.

In the terminal, try the following commands:
- `nodes` - List all connected nodes.
- `actions` - List all registered service actions.
- `call service.method` - Call the `service.method` action.
- `call service.method --name value` - Call the `service.method` action with the `name` parameter.

### Step 1: Initialize docker containers

- Start development containers:

`$ docker-compose up -d --build`

### Step 2: Check and Apply Migrations

- Check pending migrations:

`$ docker exec -it fisas_communication_api npm run knex:list`

- Run migrations:

`$ docker exec -it fisas_communication_api npm run knex:migration:migrate`

- Run seeds:

`$ docker exec -it fisas_communication_api npm run knex:seed:run`

## Services

- **CronJobs**
- **Categories**
- **Categories Translations**
- **Channels**
- **Posts**
- **Post Translations**
- **Feed RSS**

## Mixins

- **DbMixin**: Database access mixin for FISAS services [@fisas/ms_core](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer)

## Useful links

* Moleculer website: https://moleculer.services/
* Moleculer Documentation: https://moleculer.services/docs/0.14/

## NPM scripts

- `npm run dev`: Start development mode (load all services locally with hot-reload & REPL)
- `npm run start`: Start production mode (set `SERVICES` env variable to load certain services)
- `npm run cli`: Start a CLI and connect to production. Don't forget to set production namespace with `--ns` argument in script
- `npm run lint`: Run ESLint
- `npm run ci`: Run continuous test mode with watching
- `npm test`: Run tests & generate coverage report

- `npm run dc:up`: Start the stack with Docker Compose
- `npm run dc:down`: Stop the stack with Docker Compose

- `npm run knex:list`: List existing migration files
- `npm run knex:migration:create`: Create database migration file
- `npm run knex:migration:migrate`: Execute database migration
- `npm run knex:migration:rollback`: Rollback database migration
- `npm run knex:seed:create`: Create seed
- `npm run knex:seed:run`: Execute seed's
