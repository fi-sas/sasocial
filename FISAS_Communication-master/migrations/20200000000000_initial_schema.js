module.exports.up = async db => 
    db.schema.createTable('category', (table) => {
        // table
        table.increments();
        table.boolean('active').notNullable();
        table.integer('weight').notNullable();
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })
    // category translations database table
    .createTable('category_translation', (table) => {
        table.increments();
        table.integer('category_id').notNullable().unsigned().references('id')
          .inTable('category');
        table.integer('language_id').notNullable().unsigned();
        table.string('name', 120).notNullable();
      })    
      // channel database table
      .createTable('channel', (table) => {
        table.increments();
        table.string('name', 120).notNullable();
      })    
      // post database table
      .createTable('post', (table) => {
        table.increments();
        table.integer('weight');
        table.string('tags', 255);
        table.string('status', 45).notNullable();
        table.datetime('date_begin').notNullable();
        table.datetime('date_end');
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();    
        // Relationships
        table.integer('category_id').unsigned().references('id').inTable('category');
      })    
      // post translations database table
      .createTable('post_translation', (table) => {
        table.increments();
        table.integer('post_id').notNullable().unsigned().references('id')
          .inTable('post');
        table.integer('language_id').notNullable().unsigned();
        table.string('title', 255);
        table.string('summary', 255);
        table.text('body');
        table.integer('file_id_9_16').unsigned();
        table.integer('file_id_16_9').unsigned();
        table.integer('file_id_1_1').unsigned();
      })    
      // posts to channels many-to-many relationship database table
      .createTable('posts_channels', (table) => {
        table.increments();
        table.integer('post_id').unsigned().references('id').inTable('post');
        table.integer('channel_id').unsigned().references('id').inTable('channel');
        table.unique(['post_id', 'channel_id']);
      })    
      // posts to groups many-to-many relationship database table
      .createTable('post_groups', (table) => {
        table.increments();
        table.integer('post_id').unsigned().references('id').inTable('post');
        table.integer('group_id').unsigned();
        table.unique(['post_id', 'group_id']);
      })
      // feed_rss
      .createTable('feed_rss', (table) => {
        table.increments();
        table.integer('category_id').unsigned().after('language_id');
        table.integer('language_id').nullable().unsigned();
        table.string('name', 120).notNullable();
        table.string('url').notNullable();
        table.boolean('active').notNullable().defaultTo(1);
        table.timestamps();
      })
      // social_network
      .createTable('social_network', (table) => {
        table.increments();
        table.string('name').notNullable();
        table.text('configurations').notNullable();
        table.integer('active').notNullable();
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
      })
      // post_history
      .createTable('post_history', (table) => {
        table.increments();
        table.integer('post_id').unsigned().references('id').inTable('post');
        table.string('status').notNullable();
        table.integer('user_id').unsigned();
        table.string('notes');
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
      })      
      // post_gallery
      .createTable('post_gallery', (table) => {
        table.increments();
        table.integer('post_id').unsigned().references('id').inTable('post');
        table.integer('file_id').unsigned();
      })
      // post_highlight
      .createTable('post_highlight', (table) => {
        table.increments();
        table.integer('post_id').unsigned().references('id').inTable('post');
        table.text('status').notNullable();
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
      })
      // feed_fetch_history
      .createTable('feed_fetch_history', (table) => {
        table.increments();
        table.integer('number_tickers').unsigned();
        table.string('message', 512);
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
      });
      


  module.exports.down = async db => db.schema
    .dropTable('post_group')
    .dropTable('posts_channels')
    .dropTable('post_translation')
    .dropTable('post_gallery')
    .dropTable('post_highlight')
    .dropTable('feed_fetch_history')
    .dropTable('post')
    .dropTable('channel')
    .dropTable('category_translation')
    .dropTable('category')
    .dropTable('social_network')
    .dropTable('post_history')
    .dropTable('category')
    .dropTable('feed_rss');
  
  module.exports.configuration = { transaction: true };
  