module.exports.up = async db =>

	await db.schema.alterTable("post", (table) => {
		table.datetime("date");
	});

module.exports.down = async db =>

	db.schema.alterTable("post", (table) => {
		table.dropColumn("date");
	});

module.exports.configuration = { transaction: true };
