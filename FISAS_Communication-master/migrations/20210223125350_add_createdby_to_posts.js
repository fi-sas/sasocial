module.exports.up = async db =>

	await db.schema.alterTable("post", (table) => {
		table.integer("created_by_id");
	});

module.exports.down = async db =>

	db.schema.alterTable("post", (table) => {
		table.dropColumn("created_by_id");
	});

module.exports.configuration = { transaction: true };
