
exports.up = async db => {
	await db.schema
		.raw("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")

		.createTable("target_post", (table) => {
			table.increments();
			table.integer("category_id").notNullable().unsigned().references("id").inTable("category");
			table.string("subject", 255).notNullable();
			table.text("message").notNullable();
			table.string("simplified_message", 255).notNullable();
			table.enu("status", ["WAITING_APPROVAL", "APPROVED", "REJECTED", "SENT", "CANCELED", "FAILED"]).notNullable().default("PENDING");
			table.datetime("schedule_date").notNullable();
			table.integer("target_id").notNullable().unsigned();
			// table.integer("alert_template_id");
			table.uuid("uuid").default(db.raw("uuid_generate_v4()")).notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})
		.createTable("target_post_notification_method", (table) => {
			table.increments();
			table.integer("target_post_id").unsigned().references("id").inTable("target_post");
			table.integer("notification_method_id").notNullable();
		})
		;
};

exports.down = async db => {
	await db.schema
		.dropTable("target_post")
		.dropTable("target_post_notification_method");
};
module.exports.configuration = { transaction: true };
