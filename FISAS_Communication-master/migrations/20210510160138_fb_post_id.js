module.exports.up = (db) =>
	db.schema.alterTable("post", (table) => {
		table.string("fb_post_id");
	});

module.exports.down = (db) =>
	db.schema.alterTable("post", (table) => {
		table.dropColumn("fb_post_id");
	});

module.exports.configuration = { transaction: true };
