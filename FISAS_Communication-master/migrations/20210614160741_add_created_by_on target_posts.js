module.exports.up = db =>
	db.schema.alterTable("target_post", (table) => {
		table.integer("created_by_id");
	});

module.exports.down = db =>

	db.schema.alterTable("target_post", (table) => {
		table.dropColumn("created_by_id");
	});

module.exports.configuration = { transaction: true };
