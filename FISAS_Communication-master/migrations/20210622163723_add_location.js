module.exports.up = (db) =>
	db.schema.alterTable("post_translation", (table) => {
		table.string("location", 128);
	});

module.exports.down = (db) =>
	db.schema.alterTable("post_translation", (table) => {
		table.dropColumn("location");
	});

module.exports.configuration = { transaction: true };
