
exports.up = async db => {
	await db.schema
		.createTable("target_post_file", (table) => {
			table.increments();
			table.integer("target_post_id").unsigned().references("id").inTable("target_post").onDelete("CASCADE");
			table.integer("file_id").unsigned().notNullable();
		});
};

exports.down = async db => {
	await db.schema
		.dropTable("target_post_file");
};
module.exports.configuration = { transaction: true };
