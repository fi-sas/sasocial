module.exports.up = (db) =>
	db.schema.alterTable("post_translation", (table) => {
		table.string("url");
	});

module.exports.down = (db) =>
	db.schema.alterTable("post_translation", (table) => {
		table.dropColumn("url");
	});

module.exports.configuration = { transaction: true };
