module.exports.seed = async (knex) => {
	const data = [
		{ id: 1, name: "MediaKiosk" },
		{ id: 2, name: "Ticker" },
		{ id: 3, name: "Event" },
		{ id: 4, name: "Mobile" },
		{ id: 5, name: "MediaTV" },
		{ id: 6, name: "Webpage" },
		{ id: 7, name: "Facebook" },
		{ id: 8, name: "UrlMediaTV" },
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("channel").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("channel").insert(d);
			}
			return true;
		}),
	);
};
