exports.seed = async (knex) => {
	const data = [
		{
			key: "FACEBOOK_ACCESS_TOKEN",
			value: JSON.stringify(""),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "FACEBOOK_PAGE_ID",
			value: JSON.stringify(""),
			updated_at: new Date(),
			created_at: new Date(),
		}
	];

	return Promise.all(
		data.map(async (d) => {
			const rows = await knex("configuration").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("configuration").insert(d);
			}
			return true;
		}),
	);
};
