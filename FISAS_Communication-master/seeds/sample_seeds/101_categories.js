exports.seed = async (knex) =>
	knex("category").select().then(results => {
		if(results.lenght === 0) {
			return knex("category_translation")
				.del()
				.then(() =>
					knex("category")
						.del()
						.then(() =>
							knex("category").insert([
								{
									id: 1,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 2,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 3,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 4,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 5,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 6,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 7,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 8,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 9,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 10,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 11,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 12,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 13,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 14,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 15,
									active: 1,
									weight: 1,
									created_at: new Date(),
									updated_at: new Date(),
								},
							]),
						),
				);
		}
		return Promise.resolve();
	});
