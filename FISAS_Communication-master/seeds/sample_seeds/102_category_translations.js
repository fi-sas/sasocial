module.exports.seed = async (knex) =>
	knex("category_translation")
		.select()
		.then((results) => {
			if (results.lenght === 0) {
				return knex("category_translation")
					.del()
					.then(() =>
						knex("category_translation").insert([
							{
								id: 1,
								category_id: 1,
								language_id: 3,
								name: "Editar",
							},
							{
								id: 2,
								category_id: 2,
								language_id: 3,
								name: "Notícias",
							},
							{
								id: 3,
								category_id: 3,
								language_id: 3,
								name: "Eventos",
							},
							{
								id: 4,
								category_id: 4,
								language_id: 3,
								name: "Ofertas de emprego",
							},
							{
								id: 5,
								category_id: 5,
								language_id: 3,
								name: "Divulgação de oportunidades de experiência",
							},
							{
								id: 6,
								category_id: 6,
								language_id: 3,
								name: "Procedimentos de emergência",
							},
							{
								id: 7,
								category_id: 7,
								language_id: 3,
								name: "Serviços de transporte",
							},
							{
								id: 8,
								category_id: 8,
								language_id: 3,
								name: "Serviços de desporto",
							},
							{
								id: 9,
								category_id: 9,
								language_id: 3,
								name: "Serviços de saúde",
							},
							{
								id: 10,
								category_id: 10,
								language_id: 3,
								name: "Serviços de orientação do aluno",
							},
							{
								id: 11,
								category_id: 11,
								language_id: 3,
								name: "Alimentação ",
							},
							{
								id: 12,
								category_id: 12,
								language_id: 3,
								name: "Alojamento",
							},
							{
								id: 13,
								category_id: 13,
								language_id: 3,
								name: "Sessões informativas",
							},
							{
								id: 14,
								category_id: 14,
								language_id: 3,
								name: "Campanhas",
							},
							{
								id: 15,
								category_id: 15,
								language_id: 3,
								name: "Intervenções",
							},
						]),
					);
			}
			return Promise.resolve();
		});
