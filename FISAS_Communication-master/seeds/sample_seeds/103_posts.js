module.exports.seed = async (knex) =>
	knex("post")
		.select()
		.then((results) => {
			if (results.lenght === 0) {
				knex("post")
					.del()
					.then(
						async () =>
							await knex("post").insert([
								{
									id: 1,
									category_id: 5,
									tags: "Example tag",
									weight: 3,
									status: "WAITING_APPROVAL",
									date_begin: new Date(),
									date_end: null,
									created_by_id: 2,
									created_at: new Date(),
									updated_at: new Date(),
								},
								{
									id: 2,
									category_id: 10,
									tags: "Example tag 2",
									weight: 1,
									status: "APPROVED",
									date_begin: new Date(),
									date_end: null,
									created_by_id: 2,
									created_at: new Date(),
									updated_at: new Date(),
								},
							]),
					);
			}
			return Promise.resolve();
		});
