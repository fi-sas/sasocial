module.exports.seed = async (knex) =>
	knex("post_translation")
		.select()
		.then((results) => {
			if (results.lenght === 0) {
				knex("post_translation")
					.del()
					.then(() =>
						knex("post_translation").insert([
							{
								post_id: 1,
								language_id: 4,
								title: "POST TITLE 1",
								summary: "Short summary for post 1",
								body: "Very long body for post 1",
							},
							{
								post_id: 2,
								language_id: 4,
								title: "POST TITLE 2",
								summary: "Short summary for post 2",
								body: "Very long body for post 2",
							},
						]),
					);
			}
			return Promise.resolve();
		});
