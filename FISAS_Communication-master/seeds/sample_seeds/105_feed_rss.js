module.exports.seed = async (knex) =>
	knex("feed_rss")
		.select()
		.then((results) => {
			if (results.lenght === 0) {
				knex("feed_rss")
					.del()
					.then(() =>
						knex("feed_rss").insert([
							{
								language_id: 3,
								category_id: 1,
								name: "Público",
								url: "https://feeds.feedburner.com/PublicoRSS",
								active: 1,
								created_at: new Date(),
								updated_at: new Date(),
							},
							{
								language_id: 3,
								category_id: 2,
								name: "Observador",
								url: "https://feeds.feedburner.com/obs-ultimas",
								active: 1,
								created_at: new Date(),
								updated_at: new Date(),
							},
							{
								language_id: 3,
								category_id: 2,
								name: "Jornal de Negócios",
								url: "https://www.jornaldenegocios.pt/rss",
								active: 1,
								created_at: new Date(),
								updated_at: new Date(),
							},
							{
								language_id: 3,
								category_id: 2,
								name: "Jornal de Notícias",
								url: "http://feeds.jn.pt/JN-Ultimas",
								active: 0,
								created_at: new Date(),
								updated_at: new Date(),
							},
							{
								language_id: 3,
								category_id: 2,
								name: "Mais Futebol",
								url: "https://feeds.feedburner.com/iol/maisfutebol",
								active: 1,
								created_at: new Date(),
								updated_at: new Date(),
							},
						]),
					);
			}
			return Promise.resolve();
		});
