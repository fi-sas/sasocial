"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.categories",
	table: "category",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "categories")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "weight", "created_at", "updated_at"],
		defaultWithRelateds: ["translations" /*, 'posts'*/],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("communication.category_translations.find", {
									query: { category_id: doc.id, language_id: ctx.meta.language_id },
								})
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return hasMany(
						docs,
						ctx,
						"communication.category_translations",
						"translations",
						"id",
						"category_id",
					);
				}
			} /*,
			"posts"(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.posts", "posts", "id", "category_id");
			},*/,
		},
		entityValidator: {
			active: { type: "boolean" },
			weight: { type: "number", positive: true, integer: true, convert: true },
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						name: { type: "string", max: 255 },
						language_id: { type: "number", positive: true, integer: true, convert: true },
					},
				},
				min: 1,
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["checkRelationBeforeDelete", "deleteRelatedTranslations"],
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveTranslations(ctx, res) {
			await ctx.call("communication.category_translations.save_translations", {
				category_id: res[0].id,
				translations: ctx.params.translations,
			});
			res[0].translations = await ctx.call("communication.category_translations.find", {
				query: {
					category_id: res[0].id,
				},
			});
			return res;
		},
		async checkRelationBeforeDelete(ctx) {
			if (ctx.params.id) {
				return ctx
					.call("communication.feeds.count", {
						query: {
							category_id: ctx.params.id,
						},
					})
					.then((feeds_count) => {
						if (feeds_count > 0) {
							throw new Errors.ValidationError(
								"Exists feeds with this category associated",
								"CATEGORY_WITH_FEEDS",
								{},
							);
						}
					})
					.then(() => {
						return ctx.call("communication.posts.count", {
							query: {
								category_id: ctx.params.id,
							},
						});
					})
					.then((post_count) => {
						if (post_count > 0) {
							throw new Errors.ValidationError(
								"Exists posts with this category associated",
								"CATEGORY_WITH_POSTS",
								{},
							);
						}
					});
			}

			return Promise.resolve();
		},
		async deleteRelatedTranslations(ctx, res) {
			if (ctx.params.id) {
				return ctx
					.call("communication.category_translations.find", {
						query: {
							category_id: ctx.params.id,
						},
					})
					.then((res) => {
						res.map(
							async (catRel) =>
								await ctx.call("communication.category_translations.remove", { id: catRel.id }),
						);
					})
					.catch((err) => this.logger.error("Unable to delete Category translations!", err));
			}
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
