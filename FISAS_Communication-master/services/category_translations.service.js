"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.category_translations",
	table: "category_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "category_translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"category_id",
			"language_id",
			"name",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: "string|max:120",
			category_id:  { type: "number", positive:true, integer:true, convert:true },
			language_id:  { type: "number", positive:true, integer:true, convert:true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				category_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id:  { type: "number", positive:true, integer:true, convert:true },
						name: "string|max:255",
					}
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					category_id: ctx.params.category_id
				});
				this.clearCache();

				const entities = ctx.params.translations.map(translation => ({
					category_id: ctx.params.category_id,
					language_id: translation.language_id,
					name: translation.name,
				}));
				return this._insert(ctx, { entities });
			}

		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
