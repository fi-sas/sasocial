"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const Validator = require("moleculer").Validator;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {

				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach(conf => {
					result[conf.key] = conf.value;
				});
				return result;
			}
		},
		create: {
			// REST: POST /
			visibility: "published",
			params: {
				FACEBOOK_ACCESS_TOKEN: { type: "string", convert: true },
				FACEBOOK_PAGE_ID: { type: "string", convert: true },
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map(key => {
					return this._find(ctx, { query: { key: key } }).then(config => {
						if (config.length > 0 && ctx.params[config[0].key] != null) {
							return this._update(ctx, { id: config[0].id, value: JSON.stringify(ctx.params[config[0].key]) }, true);
						}
					});
				});
				return Promise.all(promisses).then(() => ctx.call("communication.configurations.list", {}));
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
