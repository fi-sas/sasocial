"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.feed_logs",
	table: "feed_fetch_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "feed_logs")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"number_tickers",
			"message",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			number_tickers: { type: "number", integer:true, convert:true },
			message:  "string|max:512",
			created_at: { type: "date" },
			updated_at: { type: "date" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.message = (ctx.params.message||"").substr(0, 512)||"";
				}
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.message = (ctx.params.message||"").substr(0, 512)||"";
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
