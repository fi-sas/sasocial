"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Cron = require("moleculer-cron");
const moment = require("moment");
const _ = require("lodash");
const Parser = require("rss-parser");
const PromisePool = require("es6-promise-pool");

//const sagaMiddleware = require("@fisas/ms_core/src/middlewares/saga.middleware");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.feeds",
	table: "feed_rss",
	requestTimeout: 20 * 1000,

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "feeds"), Cron],

	//middlewares: [sagaMiddleware()],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"category_id",
			"language_id",
			"url",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["category"],
		withRelateds: {
			category(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "communication.categories", "category", "category_id");
			},
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"communication.category_translations",
					"translations",
					"id",
					"category_id",
				);
			},
		},
		entityValidator: {
			name: "string|min:2|max:120",
			url: "string|max:255",
			active: { type: "boolean" },
			category_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			language_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	crons: [
		{
			name: "FetchContentsForRSSFeeds",
			cronTime: process.env.CRON_FETCH_RSS_FEEDS || "0 3 * * *", // Every day at 3:00 AM fetch contents for RSS feeds
			//cronTime: "*/3 * * * *", // "*/10 * * * *": de 10 em 10 minutos ...
			onTick: function () {
				this.logger.info("Job - FetchContentsForRSSFeeds ticked");

				this.getLocalService("communication.feeds")
					.actions.fetchRSSFeeds()
					.then((data) => {
						typeof data;
						//console.log("Oh!", data);
					});
			},
			runOnInit: function () {
				//console.log("FetchContentsForRSSFeeds is created");
			},
			timeZone: "Europe/Lisbon",
		},
		{
			name: "PostsGarbageCollector",
			cronTime: process.env.CRON_REMOVE_OLDER_TICKERS || "55 2 * * *", //Every day at 2:55 AM clean up old Posts created from RSS Feeds
			onTick: function () {
				this.logger.info("Job - PostsGarbageCollector ticked");

				this.getLocalService("communication.feeds")
					.actions.removeOlderTickers()
					.then((data) => {
						this.logger.info("PostsGarbageCollector posts removed", data);
					});
			},
			runOnInit: function () {
				//console.log("PostsGarbageCollector is created");
			},
			timeZone: "Europe/Lisbon",
		},
	],
	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		fetchRSSFeeds: {
			timeout: 0,
			cache: {
				enabled: false,
				//ttl: 3600
			},
			visibility: "published",
			rest: "GET /fetch-rss-feeds",
			handler(ctx) {
				ctx.meta.ignore_user = true;
				return this._removeOlderTickers(ctx)
					.then(() => {
						return this._fetchRSSFeeds(ctx);
					})
					.catch((err) => this.logger.error("Unable to delete older Ticker/FeedPosts!", err));
			},
		},
		removeOlderTickers: {
			//timeout: 0,
			handler(ctx) {
				return this._removeOlderTickers(ctx);
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"communication.feeds.created"(ctx) {
			typeof ctx;
			this.logger.info("BROADCAST EVENT communication.feeds.created!");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Logs a line in the RSS feed history table and builds the formatted response of the fetchRSSFeeds
		 * function.
		 *
		 * @param {Number} tickersCreated The number of tickers created in the current fetch.
		 * @param {String} message A message to attach to the log.
		 *
		 * @returns {Object} The formatted response for the fetchRSSFeeds function.
		 */
		async logFetch(ctx, tickersCreated, message) {
			//await FeedLog.forge().save({ number_tickers: tickersCreated, message });
			await ctx.call("communication.feed_logs.create", {
				number_tickers: tickersCreated,
				message: (message || "").substr(0, 255) || "",
			});
			return { tickersCreated, message };
		},
		/**
		 * Returns the ID for the Portuguese language, or null if there was a problem fetching the ID for
		 * the Portuguese language.
		 */
		async findPTLanguageId(ctx) {
			const acronym = "PT";
			const result = await ctx.call("configuration.languages.find", {
				query: {
					acronym: acronym.toLowerCase(),
				},
			});
			if (result) {
				if (result instanceof Array && _.isObject(result[0]) && _.isNumber(result[0].id)) {
					return result[0].id;
				}
			}
			return null;
		},
		/**
		 * Remove PostFeeds older than dateTime now
		 */
		async _removeOlderTickers(ctx) {
			return ctx
				.call(
					"communication.posts.removePostsFromChannelAndSeniority",
					{
						olderThan: new Date(),
						channel: "Ticker",
					},
					{ timeout: 0 },
				)
				.catch((err) => this.logger.error("Unable to delete older Ticker/FeedPosts!", err));
		},

		/**
		 * Get RSS Feeds parser and find defined RSS Feeds
		 */
		async _fetchRSSFeeds(ctx) {
			const LIMIT_CONTENT = 100;
			try {
				// Count current number of active tickers
				const activeTickers = await this.countActiveTickers(ctx);
				if (activeTickers >= LIMIT_CONTENT) {
					return this.logFetch(
						ctx,
						0,
						`Current number of tickers (${activeTickers}) is above or equal to max number of active tickers (${LIMIT_CONTENT}).`,
					);
				}
				// Find portuguese language id and validate that language id was correctly fetched
				const language = await this.findPTLanguageId(ctx);
				if (language === null) {
					return this.logFetch(ctx, 0, "Could not find the id for the Portuguese language.");
				}
				// Find a random category to associate the tickers just in case no category
				// is set in the feed.
				const category = await ctx.call("communication.categories.list");
				if (!category) {
					return this.logFetch(
						ctx,
						0,
						"Could not find a category to associate the tickers being created.",
					);
				}
				const categoryId = category.id;
				// Init RSS Feeds parser and find defined RSS Feeds
				let parser = new Parser({
					customFields: {
						item: [["title", "title", { includeSnippet: true }]],
					},
				});
				const feeds = await ctx.call("communication.feeds.find", { query: { active: true } });

				const feedItems = [];
				for (let feed of feeds) {
					const feedCategoryId = feed.category_id ? feed.category_id : categoryId;
					try {
						const feedContent = await parser.parseURL(feed.url);
						const items = feedContent.items.map((item) =>
							Object.assign(item, { categoryId: feedCategoryId }),
						);
						feedItems.push(...items);
					} catch (err) {
						this.logger.info(
							`Could not parse feedContent with categoryId(${feedCategoryId}). Err: ${err}.`,
						);
						this.logFetch(
							ctx,
							0,
							`Could not parse feedContent with categoryId(${feedCategoryId}). Err: ${err}.`,
						);
					}
				}

				// Promise pool configurations
				const poolLimit = 1;
				let poolCounter = 0;
				let nCreatedTickers = 0;
				const promiseProducer = () => {
					if (poolCounter >= feedItems.length || activeTickers + nCreatedTickers >= LIMIT_CONTENT) {
						return null;
					}
					const item = feedItems[poolCounter];
					poolCounter += 1;
					return this.createTickerFromFeedContent(ctx, {
						title: item.titleSnippet, //item.title,
						summary: item.contentSnippet,
						date: item.isoDate,
						language,
						category: item.categoryId,
					});
				};
				// Start promise pool
				const pool = new PromisePool(promiseProducer, poolLimit);
				pool.addEventListener("fulfilled", () => {
					nCreatedTickers += 1;
				});
				await pool.start();
				// Return success response
				return this.logFetch(ctx, nCreatedTickers, `Created ${nCreatedTickers} new tickers!`);
			} catch (err) {
				this.logger.error("FetchRSSFeeds ERROR!", err);
				return this.logFetch(ctx, 0, err.message);
			}
		},
		/**
		 * Counts the existing number of valid tickers in the database.
		 *
		 * @returns {Number} The current number of valid tickers in the database.
		 */
		async countActiveTickers(ctx) {
			let nbOfValidTickers = 0;

			let channel = await ctx.call("communication.channels.find", {
				query: {
					name: "Ticker",
				},
			});
			let postChannels = await ctx.call("communication.posts_channels.find", {
				query: {
					channel_id: channel[0].id,
				},
			});
			nbOfValidTickers = await ctx.call("communication.posts.get", {
				id: postChannels.map((itemPostChannel) => itemPostChannel.post_id),
			});
			return nbOfValidTickers.length;
		},
		async createTickerFromFeedContent(ctx, { title, summary, date, language, category }) {
			let postToCreate = {
				weight: 1,
				tags: "",
				date_begin: date,
				date_end: moment(date).add(1, "d").toISOString(),
				category_id: category,
				status: "APPROVED",
				translations: [
					{
						language_id: language,
						title: title.substring(0, 255),
						summary: summary.substring(0, 255),
					},
				],
				channels: [2],
				groups: [],
			};
			return ctx
				.call("communication.posts.create", postToCreate)
				.then((post) => {
					return Object.assign(post || {}, {
						statusData: { user_id: null, notes: "Created automatically by RSS Feed fetch" },
					});
				})
				.catch((err) => {
					this.logger.error("ERR1: ", err);
					return err;
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
