"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.post_groups",
	table: "post_groups",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "post_groups")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"post_id",
			"group_id",
		],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			post_id: { type: "number", positive:true, integer:true, convert:true },
			group_id: { type: "number", positive:true, integer:true, convert:true },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		groups_of_post: {
			rest: {
				method: "GET",
				path: "/:post_id/groups"
			},
			params: {
				post_id: {
					type: "number", integer:true, positive:true, convert: true
				}
			},
			async handler(ctx) {

				return this._find(ctx, {
					query: {
						post_id: ctx.params.post_id,
					},
				}).then((res) => {
					return ctx.call("configuration.groups.find", {
						query: {
							id: res.map((g) => g.group_id),
						},
					});
				});
			}
		},
		create_relation_post_group: {
			cache: {
				keys: ["post_id","group_id"],
				ttl: 60
			},
			rest: {
				method: "POST",
				path: "/post_groups"
			},
			params: {
				post_id: {
					type: "number", integer:true, positive:true, convert: true
				},
				group_id: {
					type: "number", integer:true, positive:true, convert: true
				}
			},
			async handler(ctx) {
				await this.removeRelationsBetweenPostGroup(ctx);
				this.clearCache();

				const entities = [ { post_id: ctx.params.post_id, group_id: ctx.params.group_id } ];

				return this._insert(ctx, { entities });
			}
		},
		save_groups: {
			params: {
				post_id: { type: "number", positive: true, integer: true, convert: true },
				groups_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					post_id: ctx.params.post_id
				});
				this.clearCache();

				const entities = ctx.params.groups_ids.map(group_id => ({
					post_id: ctx.params.post_id,
					group_id,
				}));
				this.logger.info("save_groups/insert - entities:");
				this.logger.info(entities);
				return this._insert(ctx, { entities });
			}
		},
		remove_relation_post_group: {
			cache: {
				keys: ["post_id","group_id"],
				ttl: 60
			},
			rest: {
				method: "DELETE",
				path: "/:post_id/:group_id"
			},
			params: {
				post_id: {
					type: "number", integer:true, positive:true, convert: true
				},
				group_id: {
					type: "number", integer:true, positive:true, convert: true
				}
			},
			async handler(ctx) {
				return this.removeRelationsBetweenPostGroup(ctx);
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

		removeRelationsBetweenPostGroup(ctx){
			return this.adapter.find({
				query: (q) => {
					q.where({
						post_id: ctx.params.post_id,
						group_id: ctx.params.group_id })
						.del();
					return q;
				}
			})
				.then(docs => this.transformDocuments(ctx, ctx.params, docs));
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
