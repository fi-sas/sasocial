"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.post_translations",
	table: "post_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "post_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"post_id",
			"language_id",
			"title",
			"summary",
			"body",
			"location",
			"url",
			"file_id_9_16",
			"file_id_16_9",
			"file_id_1_1",
		],
		defaultWithRelateds: ["file_9_16", "file_16_9", "file_1_1"],
		withRelateds: {
			file_9_16(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file_9_16", "file_id_9_16");
			},
			file_16_9(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file_16_9", "file_id_16_9");
			},
			file_1_1(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file_1_1", "file_id_1_1");
			},
		},
		entityValidator: {
			post_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
			title: "string|max:255|optional",
			summary: "string|max:255|optional",
			body: "string|optional",
			location: "string|max:1278|optional",
			url: { type: "url", optional: true },
			file_id_9_16: { type: "number", positive: true, integer: true, optional: true },
			file_id_16_9: { type: "number", positive: true, integer: true, optional: true },
			file_id_1_1: { type: "number", positive: true, integer: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function validateEntities(ctx) {
					if (ctx.body.channel) {
						this.logger.info(ctx);
						return this.validateExternalIds(ctx, {
							file_id_9_16: { action: "media.files.get" },
						});
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_translations: {
			params: {
				post_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						title: "string|max:255|optional",
						summary: "string|max:255|optional",
						body: "string|optional",
						location: "string|max:128|optional",
						url: { type: "number", positive: true, integer: true, optional: true },
						file_id_9_16: { type: "number", positive: true, integer: true, optional: true },
						file_id_16_9: { type: "number", positive: true, integer: true, optional: true },
						file_id_1_1: { type: "number", positive: true, integer: true, optional: true },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					post_id: ctx.params.post_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					post_id: ctx.params.post_id,
					language_id: translation.language_id,
					title: translation.title,
					summary: translation.summary,
					body: translation.body,
					location: translation.location,
					url: translation.url,
					file_id_9_16: translation.file_id_9_16,
					file_id_16_9: translation.file_id_16_9,
					file_id_1_1: translation.file_id_1_1,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateExternalIds(ctx, opts) {
			const opts_fields = _.keys(opts);
			let promises = [];
			_.forEach(opts_fields, (field) => {
				this.logger.info(opts[field].action, {
					id: ctx.params[field],
				});
				if (ctx.params[field]) {
					promises.push(
						ctx.call(opts[field].action, {
							id: ctx.params[field],
						}),
					);
				}
			});
			return await Promise.all(promises)
				.then((res) => {
					this.logger.info("RES");
					this.logger.info(res);
					return res;
				})
				.catch((err) => {
					this.logger.info("ERR");
					this.logger.info(err);
					throw err;
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
