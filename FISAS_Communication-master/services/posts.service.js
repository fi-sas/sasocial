"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors, SearchRelateds } = require("@fisas/ms_core").Helpers;
const fs = require("fs");
const axios = require("axios").default;
const FB = require("fb").default;
const moment = require("moment");
const MoleculerCron = require("moleculer-cron");
const _ = require("lodash");
const Validator = require("moleculer").Validator;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.posts",
	table: "post",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "posts"), MoleculerCron],
	crons: [
		{
			name: "PublishOnFacebook",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("communication.posts").actions.validatePendingFacebookPosts();
			},
			runOnInit: function () {},
			timeZone: "Europe/Lisbon",
		},
	],
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"weight",
			"tags",
			"status",
			"date",
			"date_begin",
			"date_end",
			"category_id",
			"created_by_id",
			"fb_post_id",
			"updated_at",
			"created_at",
			"channel_name",
		],
		defaultWithRelateds: [
			"category",
			"channels",
			"gallery",
			"groups",
			"highlights",
			"translations",
		],
		withRelateds: {
			category(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "communication.categories", "category", "category_id");
			},
			gallery(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.post_gallery", "gallery", "id", "post_id");
			},
			groups(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("communication.post_groups.groups_of_post", { post_id: doc.id })
							.then((res) => (doc.groups = res || []));
					}),
				);
			},
			channels(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("communication.posts_channels.channels_of_post", { post_id: doc.id })
							.then((res) => (doc.channels = res || []));
					}),
				);
			},
			highlights(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.post_highlights", "highlights", "id", "post_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.post_history", "history", "id", "post_id");
			},
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("communication.post_translations.find", {
									query: { post_id: doc.id, language_id: ctx.meta.language_id },
								})
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return hasMany(
						docs,
						ctx,
						"communication.post_translations",
						"translations",
						"id",
						"post_id",
					);
				}
			},
			created_by(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "created_by", "created_by_id");
			},
		},
		entityValidator: {
			category_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			weight: { type: "number", positive: true, integer: true, convert: true },
			tags: "string|max:255|optional",
			status: {
				type: "enum",
				values: ["WAITING_APPROVAL", "APPROVED", "REJECTED"],
				optional: true,
			},
			translations: {
				type: "array",
				item: {
					type: "object",
					props: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						title: "string|max:255|optional",
						summary: "string|max:255|optional",
						body: "string|optional",
						location: "string|max:128|optional",
						url: { type: "number", positive: true, integer: true, optional: true },
						file_id_9_16: { type: "number", positive: true, integer: true, optional: true },
						file_id_16_9: { type: "number", positive: true, integer: true, optional: true },
						file_id_1_1: { type: "number", positive: true, integer: true, optional: true },
					},
				},
				min: 1,
			},
			date: { type: "date", optional: true, convert: true },
			date_begin: { type: "date", convert: true },
			date_end: { type: "date", convert: true },
			created_by_id: { type: "number", optional: true },
			fb_post_id: { type: "string", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			list: [
				function addSearch(ctx) {
					return SearchRelateds.addSearchRelation(
						ctx,
						["title", "summary", "body"],
						"communication.post_translations",
						"post_id",
					);
				},
			],
			create: [
				"validateStatus",
				"validateRequiredFieldsByChannel",
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();

					if (ctx.meta.ignore_user) {
						ctx.params.created_by_id = null;
					} else {
						ctx.params.created_by_id = ctx.meta.user ? ctx.meta.user.id : null;
					}
				},
			],
			update: [
				"validateStatus",
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"deleteRelatedTranslations",
				"deleteRelatedGallery",
				"deleteRelatedHighlights",
				"deleteRelatedHistory",
				"deleteRelatedChannels",
				"deleteRelatedGroups",
				"deleteFacebookPost",
			],
		},
		after: {
			create: ["saveTranslations", "saveChannels", "saveGroups"],
			update: ["saveTranslations", "saveChannels", "saveGroups", "updateFacebookPostInformation"],
			postApprove: [
				async function createFacebookPost(ctx, res) {
					if (
						moment(res[0].date_begin).isBefore(moment()) &&
						res[0].channels.find((x) => x.id == 7) &&
						this.validFacebookCredentials(ctx)
					) {
						const pt_languages = res[0].translations.find((x) => x.language_id == 3);
						if (pt_languages) {
							try {
								const post_id = await this.facebookPublishRequest(
									ctx,
									res[0].translations[0].file_1_1.url,
									res[0].translations[0].file_1_1.path.split(".")[1],
									res[0].translations[0].summary,
								);
								if (post_id) {
									return this._update(ctx, { id: res[0].id, fb_post_id: post_id }, true).then(
										(post) => {
											return post;
										},
									);
								}
							} catch (error) {
								this.logger.info("FACEBOOK PUBLISH FAILED");
								this.logger.info(error);
								return res;
							}
						}
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				const wp_posts = await ctx.call("communication.posts.postsWebpage");
				return _.sampleSize(wp_posts.rows, 3);
			},
		},
		postsMediasTV: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id"],
			},
			rest: {
				method: "GET",
				path: "/medias-tv",
			},
			scope: "communication:posts:medias-tv",
			params: {},
			async handler(ctx) {
				let groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				let groupsIds = groups.map((g) => {
					return g.id;
				});

				return this.adapter
					.find({
						query: (q) => {
							q.select("channel.name as channel_name", "post.*");
							this.joinGroups(q, groupsIds);
							this.joinChannels(q, ["MediaTV", "UrlMediaTV"]);
							this.alive(q, ctx.params.alive ? ctx.params.alive : true);
							return q;
						},
					})
					.then((docs) => this.transformDocuments(ctx, ctx.params, docs));
			},
		},
		postsMediasKiosk: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id"],
			},
			rest: {
				method: "GET",
				path: "/medias-kiosk",
			},
			scope: "communication:posts:medias-kiosk",
			params: {},
			async handler(ctx) {
				let groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				let groupsIds = groups.map((g) => {
					return g.id;
				});

				return this.adapter
					.find({
						query: (q) => {
							q.select("post.*");
							this.joinGroups(q, groupsIds);
							this.joinChannel(q, "MediaKiosk");
							this.alive(q, ctx.params.alive ? ctx.params.alive : true);
							return q;
						},
					})
					.then((docs) => this.transformDocuments(ctx, ctx.params, docs));
			},
		},
		postsTickers: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id"],
			},
			rest: {
				method: "GET",
				path: "/tickers",
			},
			scope: "communication:posts:tickers",
			params: {},
			async handler(ctx) {
				let groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				let groupsIds = groups.map((g) => {
					return g.id;
				});

				return this.adapter
					.find({
						query: (q) => {
							q.select("post.*");
							this.joinGroups(q, groupsIds);
							this.joinChannel(q, "Ticker");
							this.alive(q, ctx.params.alive ? ctx.params.alive : true);
							return q;
						},
					})
					.then((docs) => this.transformDocuments(ctx, ctx.params, docs));
			},
		},
		postsEvents: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id"],
			},
			rest: {
				method: "GET",
				path: "/events",
			},
			scope: "communication:posts:events",
			params: {},
			async handler(ctx) {
				let groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				let groupsIds = groups.map((g) => {
					return g.id;
				});

				return this.adapter
					.find({
						query: (q) => {
							q.select("post.*");
							this.joinGroups(q, groupsIds);
							this.joinChannel(q, "Event");
							this.alive(q, ctx.params.alive ? ctx.params.alive : true);
							return q;
						},
					})
					.then((docs) => this.transformDocuments(ctx, ctx.params, docs));
			},
		},
		postsWebpage: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id", "limit", "offset"],
			},
			rest: {
				method: "GET",
				path: "/webpage",
			},
			scope: "communication:posts:medias-webpage",
			params: {},
			async handler(ctx) {
				if (!ctx.params.limit) {
					ctx.params.limit = 5;
				}
				if (!ctx.params.offset) {
					ctx.params.offset = 0;
				}

				const response = await this.adapter
					.find({
						query: (q) => {
							q.select("post.*");
							this.joinChannel(q, "Webpage");
							this.alive(q, ctx.params.alive ? ctx.params.alive : true);
							q.limit(ctx.params.limit);
							q.offset(ctx.params.offset);
							q.orderBy("id", "desc");
							return q;
						},
					})
					.then((docs) => this.transformDocuments(ctx, ctx.params, docs));

				const count = await this.adapter.find({
					query: (q) => {
						q.count("*");
						this.joinChannel(q, "Webpage");
						this.alive(q, ctx.params.alive ? ctx.params.alive : true);
						return q;
					},
				});

				return {
					// Total rows
					total: count[0].count,
					// Page
					page: Math.floor(ctx.params.offset / ctx.params.limit),
					// Page size
					pageSize: +ctx.params.limit,
					// Total pages
					totalPages: Math.floor((count[0].count + +ctx.params.limit - 1) / ctx.params.limit),
					//Data
					rows: response,
				};
			},
		},
		postsMobile: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 43200,
				keys: ["#device.id", "#language_id", "limit", "offset"],
			},
			rest: {
				method: "GET",
				path: "/mobile",
			},
			scope: "communication:posts:mobile",
			params: {},
			async handler(ctx) {
				if (!ctx.params.limit) {
					ctx.params.limit = 5;
				}
				if (!ctx.params.offset) {
					ctx.params.offset = 0;
				}
				let groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				let groupsIds = groups.map((g) => {
					return g.id;
				});
				const response = await this._find(ctx, {
					query: (q) => {
						q.select("post.*");
						this.joinGroups(q, groupsIds);
						this.joinChannel(q, "Mobile");
						this.alive(q, ctx.params.alive ? ctx.params.alive : true);
						q.limit(ctx.params.limit);
						q.offset(ctx.params.offset);
						q.orderBy("id", "desc");
						return q;
					},
				}).then((docs) => this.transformDocuments(ctx, ctx.params, docs));

				const count = await this.adapter.find({
					query: (q) => {
						q.count("*");
						this.joinGroups(q, groupsIds);
						this.joinChannel(q, "Mobile");
						this.alive(q, ctx.params.alive ? ctx.params.alive : true);
						return q;
					},
				});

				return {
					// Total rows
					total: count[0].count,
					// Page
					page: Math.floor(ctx.params.offset / ctx.params.limit),
					// Page size
					pageSize: +ctx.params.limit,
					// Total pages
					totalPages: Math.floor((+count[0].count + +ctx.params.limit - 1) / ctx.params.limit),
					//Data
					rows: response,
				};
			},
		},
		postsChannels: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/channels",
			},
			params: {},
			async handler(ctx) {
				return ctx.call("communication.channels.list");
			},
		},
		postApprove: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/:id/approve",
			},
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._get(ctx, {
					id: ctx.params.id,
					fields: "status",
					withRelated: [],
				}).then((res) => {
					if (
						!ctx.params.status ||
						(ctx.params.status === "WAITING_APPROVAL" && ["APPROVED"].includes(res[0].status))
					) {
						throw new Errors.ValidationError(
							`The status provided ('${ctx.params.status}') is not valid (current Post status: '${res[0].status}').`,
							"INVALID_POST_STATUS",
						);
					}
					const patchTrue = true;
					return this._update(ctx, { id: ctx.params.id, status: ctx.params.status }, patchTrue);
				});
			},
		},
		postReject: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/:id/reject",
			},
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._get(ctx, {
					id: ctx.params.id,
					fields: "status",
					withRelated: [],
				}).then((res) => {
					if (res[0].status === "REJECTED") {
						throw new Errors.ValidationError(
							`This Post is already '${res[0].status}', its status cannot be changed.`,
							"INVALID_POST_STATUS",
						);
					}
					const patchTrue = true;
					return this._update(ctx, { id: ctx.params.id, status: "REJECTED" }, patchTrue);
				});
			},
		},
		/**
		 * Remove Posts by Channel and Seniority (older than dateTime)
		 */
		removePostsFromChannelAndSeniority: {
			timeout: 0,
			params: {
				olderThan: { type: "date", convert: true },
				channel: { type: "string" },
			},
			async handler(ctx) {
				return this.adapter
					.find({
						query: (q) => {
							return q
								.innerJoin("posts_channels", "post.id", "posts_channels.post_id")
								.innerJoin("channel", "posts_channels.channel_id", "channel.id")
								.where("channel.name", "=", ctx.params.channel)
								.andWhere((q2) => {
									q2.where("date_end", "<=", ctx.params.olderThan);
								})
								.andWhere((q2) => {
									q2.whereNull("created_by_id"); // is [null] when created by cron
								});
						},
					})
					.then(async (res) => {
						for (let p of res) {
							try {
								await ctx.call("communication.posts.remove", {
									id: p.post_id,
								});
							} catch (ex) {
								this.logger.error("An error occurered while removeing a post: " + p.id);
							}
						}
					});
			},
		},
		/**
		 * Remove all Posts by Channel
		 */
		removeAllPostsByChannel: {
			timeout: 0,
			params: {
				channel: { type: "string" },
			},
			async handler(ctx) {
				return this.adapter
					.find({
						query: (q) => {
							return q
								.innerJoin("posts_channels", "post.id", "posts_channels.post_id")
								.innerJoin("channel", "posts_channels.channel_id", "channel.id")
								.where("channel.name", "=", ctx.params.channel);
						},
					})
					.then(async (res) => {
						for (let post of res) {
							try {
								await ctx.call("communication.posts.remove", {
									id: post.post_id,
								});
							} catch (ex) {
								this.logger.error("An error occorred while deleting post: " + post.id);
							}
						}
					});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		validatePendingFacebookPosts: {
			visibility: "public",
			async handler(ctx) {
				if (this.validFacebookCredentials(ctx)) {
					ctx.params = {};
					const pending_posts_ids = await this._find(ctx, {
						query: (q) => {
							q.select("post.*");
							this.joinChannel(q, "Facebook");
							q.where("date_begin", "<=", new Date());
							q.where("status", "=", "APPROVED");
							q.whereNull("fb_post_id");
							return q;
						},
					});
					const pending_posts = await this._find(ctx, {
						query: { id: pending_posts_ids.map((x) => x.id) },
					});
					for (const post of pending_posts) {
						const pt_translation = post.translations.find((x) => x.language_id == 3);
						if (pt_translation) {
							const img_type = pt_translation.file_1_1.path.split(".")[1];
							const fb_post_id = await this.facebookPublishRequest(
								ctx,
								pt_translation.file_1_1.url,
								img_type,
								pt_translation.summary,
							);
							if (fb_post_id) await this._update(ctx, { id: post.id, fb_post_id }, true);
						}
					}
					return;
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"communication.categories.*"(ctx) {
			typeof ctx;
			this.logger.info("BROADCAST EVENT communication.categories.*!");
			this.clearCache();
		},
		"communication.posts.*"(ctx) {
			typeof ctx;
			this.logger.info("BROADCAST EVENT communication.posts.*!");
			this.clearCache();
		},
		"communication.posts.created"(ctx) {
			typeof ctx;
			this.logger.info("BROADCAST EVENT communication.posts.created!");
		},
		"communication.posts.removed"(ctx) {
			typeof ctx;
			this.logger.info("BROADCAST EVENT communication.posts.removed!");
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateStatus(ctx) {
			if (!ctx.params.id && !ctx.params.status) {
				ctx.params.status = "WAITING_APPROVAL";
			}
		},
		async saveTranslations(ctx, res) {
			await ctx.call("communication.post_translations.save_translations", {
				post_id: res[0].id,
				translations: ctx.params.translations,
			});
			res[0].translations = await ctx.call("communication.post_translations.find", {
				query: {
					post_id: res[0].id,
				},
			});
			return res;
		},
		async saveChannels(ctx, res) {
			if (ctx.params.channels && Array.isArray(ctx.params.channels)) {
				const channels_ids = [...new Set(ctx.params.channels)];
				await ctx.call("communication.posts_channels.save_channels", {
					post_id: res[0].id,
					channels_ids,
				});
				res[0].channels = await ctx.call("communication.posts_channels.channels_of_post", {
					post_id: res[0].id,
				});
			}
			return res;
		},
		async saveGroups(ctx, res) {
			if (ctx.params.groups && Array.isArray(ctx.params.groups)) {
				const groups_ids = [...new Set(ctx.params.groups)];
				await ctx.call("communication.post_groups.save_groups", {
					post_id: res[0].id,
					groups_ids,
				});
				res[0].groups = await ctx.call("communication.post_groups.groups_of_post", {
					post_id: res[0].id,
				});
			}
			return res;
		},
		async deleteRelatedTranslations(ctx, res) {
			typeof res;
			return ctx
				.call("communication.post_translations.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.post_translations.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post translations!", err));
		},
		async deleteRelatedGallery(ctx, res) {
			typeof res;
			return ctx
				.call("communication.post_gallery.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.post_gallery.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post Gallery items!", err));
		},
		async deleteRelatedGroups(ctx, res) {
			typeof res;
			return ctx
				.call("communication.post_groups.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.post_groups.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post Groups relation!", err));
		},
		async deleteRelatedHighlights(ctx, res) {
			typeof res;
			return ctx
				.call("communication.post_highlights.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.post_highlights.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post Highlights items!", err));
		},
		async deleteRelatedHistory(ctx, res) {
			typeof res;
			return ctx
				.call("communication.post_history.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.post_history.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post history items!", err));
		},
		async deleteRelatedChannels(ctx, res) {
			typeof res;
			return ctx
				.call("communication.posts_channels.find", {
					query: {
						post_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (postRel) =>
							await ctx.call("communication.posts_channels.remove", { id: postRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Post channels items!", err));
		},
		joinGroups(query, groupIds) {
			return query.leftJoin("post_groups", "post.id", "post_groups.post_id").andWhere((qB) => {
				qB.where("post_groups.group_id", "in", groupIds).orWhereNotExists((q) => {
					q.select("*").from("post_groups").whereRaw("post_groups.post_id = post.id");
				});
			});
		},
		joinChannel(query, channel) {
			const that = this;
			return query
				.innerJoin("posts_channels", "post.id", "posts_channels.post_id")
				.innerJoin("channel", function () {
					this.on("posts_channels.channel_id", "=", "channel.id").andOn(
						"channel.name",
						"=",
						that.adapter.db.raw("?", [channel]),
					);
				})
				.where("channel.name", "=", channel);
		},
		joinChannels(query, channels) {
			const that = this;

			return query
				.innerJoin("posts_channels", "post.id", "posts_channels.post_id")
				.innerJoin("channel", function () {
					this.on("posts_channels.channel_id", "=", "channel.id");
					/*.andOn(
						"channel.name",
						"=",
						that.adapter.db.raw("?", [channel]),
					);*/
				})
				.whereIn("channel.name", channels);
		},
		alive(query, alive) {
			const now = new Date();
			if (alive === "true" || alive === true) {
				query
					.where("status", "APPROVED")
					.andWhere("date_begin", "<=", now)
					.andWhere((q2) => {
						q2.where("date_end", ">=", now).orWhereNull("date_end");
					});
			} else {
				query.where("date_begin", ">=", now).orWhere("date_end", "<=", now);
			}
			return query;
		},

		clearCache() {
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},

		async facebookPublishRequest(ctx, imageUrl, imageType, text) {
			this.logger.info("Img URL", imageUrl);
			const response = await axios({
				method: "get",
				url: imageUrl,
				responseType: "stream",
			});
			this.logger.info("Get post image with sucess", response);
			response.data.pipe(fs.createWriteStream("file." + imageType));
			this.logger.info("Pipe reponse into a stream");

			const configurations = await ctx.call("communication.configurations.list", {});
			FB.setAccessToken(configurations["FACEBOOK_ACCESS_TOKEN"]);
			const post = await FB.api(configurations["FACEBOOK_PAGE_ID"] + "/photos", "post", {
				source: response.data,
				caption: text,
			});
			this.logger.info("FB Response", post);
			if (!post || post.error) {
				return null;
			}
			return post.post_id;
		},
		async updateFacebookPostInformation(ctx, res) {
			if (res[0].fb_post_id && this.validFacebookCredentials(ctx)) {
				const pt_language = res[0].translations.find((x) => x.language_id == 3);
				if (pt_language) {
					try {
						await this.facebookUpdateRequest(
							ctx,
							pt_language.file_1_1.url,
							pt_language.file_1_1.path.split(".")[1],
							pt_language.summary,
							res[0].fb_post_id,
						);
					} catch (error) {
						this.logger.info("FACEBOOK UPDATE FAILED");
						this.logger.info(error);
					}
				}
			}
			return res;
		},
		async facebookUpdateRequest(ctx, imageUrl, imageType, text, fb_post_id) {
			const response = await axios({
				method: "get",
				url: imageUrl,
				responseType: "stream",
			});
			response.data.pipe(fs.createWriteStream("file." + imageType));
			const configurations = await ctx.call("communication.configurations.list", {});
			FB.setAccessToken(configurations["FACEBOOK_ACCESS_TOKEN"]);
			await FB.api(fb_post_id, "post", {
				source: response.data,
				message: text,
				is_published: true,
			});
		},
		async deleteFacebookPost(ctx) {
			const post = await this._get(ctx, { id: ctx.params.id });
			if (post[0].fb_post_id && this.validFacebookCredentials(ctx)) {
				const configurations = await ctx.call("communication.configurations.list", {});
				FB.setAccessToken(configurations["FACEBOOK_ACCESS_TOKEN"]);
				await FB.api(post[0].fb_post_id, "delete");
			}
		},
		async validFacebookCredentials(ctx) {
			const configurations = await ctx.call("communication.configurations.list", {});
			return configurations["FACEBOOK_ACCESS_TOKEN"] && configurations["FACEBOOK_PAGE_ID"];
		},
		async validateRequiredFieldsByChannel(ctx) {
			const v = new Validator();
			if (Array.isArray(ctx.params.channels)) {
				for (const channel of ctx.params.channels) {
					let schema = null;
					if (channel == 1) {
						schema = {
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										file_id_9_16: { type: "number", positive: true, integer: true },
									},
								},
								min: 1,
							},
						};
					} else if (channel == 2) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								item: {
									language_id: { type: "number", positive: true, integer: true, convert: true },
									title: { type: "string", max: 255 },
								},
								min: 1,
							},
						};
					} else if (channel == 3) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										summary: { type: "string", max: 255 },
										body: { type: "string" },
										location: { type: "string", max: 128 },
										file_id_1_1: { type: "number", positive: true, integer: true },
									},
								},
								min: 1,
							},
							date: { type: "date", convert: true },
						};
					} else if (channel == 4) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										summary: { type: "string", max: 255 },
										body: { type: "string" },
										file_id_16_9: { type: "number", positive: true, integer: true },
									},
								},
								min: 1,
							},
						};
					} else if (channel == 5) {
						schema = {
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										file_id_16_9: { type: "number", positive: true, integer: true },
									},
								},
								min: 1,
							},
						};
					} else if (channel == 6) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										summary: { type: "string", max: 255 },
										body: { type: "string" },
										file_id_16_9: { type: "number", positive: true, integer: true, optional: true },
									},
								},
								min: 1,
							},
						};
					} else if (channel == 7) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										summary: { type: "string", max: 255 },
										body: { type: "string" },
										file_id_1_1: { type: "number", positive: true, integer: true, optional: true },
									},
								},
								min: 1,
							},
						};
					} else if (channel == 8) {
						schema = {
							category_id: { type: "number", positive: true, integer: true, convert: true },
							translations: {
								type: "array",
								items: {
									type: "object",
									props: {
										title: { type: "string", max: 255 },
										url: { type: "url" },
									},
								},
								min: 1,
							},
						};
					}

					const check = v.compile(schema);
					const res = check(ctx.params);
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
