"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.posts_channels",
	table: "posts_channels",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "posts_channels")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"post_id",
			"channel_id"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			post_id:  { type: "number", positive:true, integer:true, convert:true },
			channel_id:  { type: "number", positive:true, integer:true, convert:true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		channels_of_post: {
			rest: {
				method: "GET",
				path: "/:post_id/channels"
			},
			params: {
				post_id: {
					type: "number", integer:true, positive:true, convert: true
				}
			},
			async handler(ctx) {

				return this._find(ctx, {
					query: {
						post_id: ctx.params.post_id,
					},
				}).then((res) => {
					return ctx.call("communication.channels.find", {
						query: {
							id: res.map((chn) => chn.channel_id),
						},
					});
				});
			}
		},
		save_channels: {
			params: {
				post_id: { type: "number", positive: true, integer: true, convert: true },
				channels_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					post_id: ctx.params.post_id
				});
				this.clearCache();

				const entities = ctx.params.channels_ids.map(channel_id => ({
					post_id: ctx.params.post_id,
					channel_id,
				}));
				this.logger.info("save_channels/insert - entities:");
				this.logger.info(entities);
				return this._insert(ctx, { entities });
			}
		},
		remove_relation_post_channel: {
			cache: {
				keys: ["post_id","channel_id"],
				ttl: 60
			},
			rest: {
				method: "DELETE",
				path: "/:post_id/:channel_id"
			},
			params: {
				post_id: {
					type: "number", integer:true, positive:true, convert: true
				},
				channel_id: {
					type: "number", integer:true, positive:true, convert: true
				}
			},
			async handler(ctx) {
				return this.removeRelationsBetweenPostChannel(ctx);
			}
		},

	},

	/**
	 * Events
	 */
	events: {
		"communication.post_channel.created"(ctx) {
			typeof(ctx);
			this.logger.info("BROADCAST EVENT communication.post_channel.created!");
		},
		"communication.post_channel.removed"(ctx) {
			typeof(ctx);
			this.logger.info("BROADCAST EVENT communication.post_channel.removed!");
		},
	},

	/**
	 * Methods
	 */
	methods: {
		removeRelationsBetweenPostChannel(ctx){
			return this.adapter.find({
				query: (q) => {
					q.where({
						post_id: ctx.params.post_id,
						channel_id: ctx.params.channel_id })
						.del();
					return q;
				}
			})
				.then(docs => this.transformDocuments(ctx, ctx.params, docs));
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
