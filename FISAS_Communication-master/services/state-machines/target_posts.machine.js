let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [

			{ name: "APPROVE", from: "WAITING_APPROVAL", to: "APPROVED" },
			{ name: "REJECT", from: "WAITING_APPROVAL", to: "REJECTED" },
			{ name: "SEND", from: "APPROVED", to: "SENT" },
			{ name: "SEND", from: "FAILED", to: "SENT" },
			{ name: "FAILED", from: "APPROVED", to: "FAILED" },
			{ name: "CANCEL", from: "WAITING_APPROVAL", to: "CANCELED" },
			{ name: "CANCEL", from: "APPROVED", to: "CANCELED" }
		],
		methods: {
			onAfterApprove: function(lifecycle) {
				return this.saveTargetPostStatus(lifecycle);
			},
			onAfterReject: function(lifecycle) {
				return this.saveTargetPostStatus(lifecycle);
			},
			onAfterSend: function(lifecycle) {
				return this.saveTargetPostStatus(lifecycle);
			},
			onAfterFailed: function(lifecycle) {
				return this.saveTargetPostStatus(lifecycle);
			},
			onAfterCancel: function(lifecycle) {
				return this.saveTargetPostStatus(lifecycle);
			},
			saveTargetPostStatus: function(lifecycle) {
				return ctx.call("communication.target_posts.patch", {
					id: ctx.params.id,
					status: lifecycle.to,
					updated_at: new Date()
				}).then(result => { return result; });
			}
		}

	});
}

exports.createStateMachine = createStateMachine;
