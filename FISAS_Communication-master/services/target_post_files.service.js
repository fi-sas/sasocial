"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.target_post_files",
	table: "target_post_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "target_posts")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"target_post_id",
			"file_id"
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			target_post(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "communication.target_posts", "target_post", "target_post_id");
			},
		},
		entityValidator: {
			file_id: { type: "number", positive: true, integer: true, convert: true },
			target_post_id: { type: "number", positive: true, integer: true, convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: [
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			patch: [
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
