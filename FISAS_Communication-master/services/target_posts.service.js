"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const TargetPostsStateMachine = require("./state-machines/target_posts.machine");
const MoleculerCron = require("moleculer-cron");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "communication.target_posts",
	table: "target_post",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("communication", "target_posts"), MoleculerCron],
	crons: [
		{
			name: "SendTargetPosts",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("communication.target_posts")
					.actions.sendPendingTargetPosts();
			},
			runOnInit: function () {
			},
			timeZone: "Europe/Lisbon",
		},
	],
	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"category_id",
			"subject",
			"message",
			"simplified_message",
			"status",
			"schedule_date",
			"target_id",
			// "alert_template_id",
			"created_by_id",
			"uuid",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: ["target_post_notification_methods", "target", "target_post_files"],
		withRelateds: {
			"target_post_notification_methods"(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.target_post_notification_methods", "target_post_notification_methods", "id", "target_post_id");
			},
			"target_post_files"(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "communication.target_post_files", "target_post_files", "id", "target_post_id");
			},
			"target"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.targets", "target", "target_id");
			},
			"category"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "communication.categories", "category", "category_id");
			},
			"created_by"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "created_by", "created_by_id", "id", {}, "id,name", false);
			},
		},
		entityValidator: {
			category_id: { type: "number", positive: true, integer: true, convert: true },
			subject: { type: "string" },
			message: { type: "string" },
			simplified_message: { type: "string" },
			status: {
				type: "enum", values: ["WAITING_APPROVAL", "APPROVED", "REJECTED", "SENT", "CANCELED", "FAILED"], default: "WAITING_APPROVAL"
			},
			schedule_date: { type: "date", convert: true },
			target_id: { type: "number", positive: true, integer: true, convert: true },
			uuid: { type: "uuid", optional: true },
			// alert_template_id: { type: "number", positive: true, integer: true, convert: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			target_post_notification_methods: {
				type: "array",
				item: {
					notification_method_id: {
						type: "number", positive: true, integer: true, convert: true
					}
				},
				min: 1
			},
			target_post_files: {
				type: "array",
				item: {
					file_id: {
						type: "number", positive: true, integer: true, convert: true
					}
				},
				optional: true
			}
		}
	},
	hooks: {
		before: {
			create: [
				"validateTargetId",
				"validateNotificationMethods",
				"validateCategoryId",
				"validateFiles",
				function sanitizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.created_by_id = ctx.meta.user.id;
				}
			],
			update: [
				"validateTargetId",
				"validateNotificationMethods",
				"validateCategoryId",
				"validateFiles",
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			patch: [
				"validateTargetId",
				"validateNotificationMethods",
				"validateCategoryId",
				"validateFiles",
				function sanitizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
		after: {
			create: [
				async function sanatizeParams(ctx, response) {
					response[0].target_post_notification_methods = await this.createWithRelated(ctx, "communication.target_post_notification_methods.create", response[0].id, ctx.params.target_post_notification_methods);
					response[0].target_post_files = await this.createWithRelated(ctx, "communication.target_post_files.create", response[0].id, ctx.params.target_post_files);
					return response;
				}
			],
			update: [
				async function sanatizeParams(ctx, response) {
					await this.deleteWithRelated(ctx, "communication.target_post_notification_methods.remove", await ctx.call("communication.target_post_notification_methods.find", { query: { target_post_id: response[0].id } }));
					await this.deleteWithRelated(ctx, "communication.target_post_files.remove", await ctx.call("communication.target_post_files.find", { query: { target_post_id: response[0].id } }));
					response[0].target_post_notification_methods = await this.createWithRelated(ctx, "communication.target_post_notification_methods.create", response[0].id, ctx.params.target_post_notification_methods);
					response[0].target_post_files = await this.createWithRelated(ctx, "communication.target_post_files.create", response[0].id, ctx.params.target_post_files);
					return response;
				}
			],
			patch: [
				async function sanatizeParams(ctx, response) {
					if (ctx.params.target_post_notification_methods) {
						await this.deleteWithRelated(ctx, "communication.target_post_notification_methods.remove", await ctx.call("communication.target_post_notification_methods.find", { query: { target_post_id: response[0].id } }));
						response[0].target_post_notification_methods = await this.createWithRelated(ctx, "communication.target_post_notification_methods.create", response[0].id, ctx.params.target_post_notification_methods);

					}

					if (ctx.params.target_post_files) {
						await this.deleteWithRelated(ctx, "communication.target_post_files.remove", await ctx.call("communication.target_post_files.find", { query: { target_post_id: response[0].id } }));
						response[0].target_post_files = await this.createWithRelated(ctx, "communication.target_post_files.create", response[0].id, ctx.params.target_post_files);
					}

					return response;
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		changeStatusTargetPost: {
			visibility: "published",
			scope: "communication:target_posts:status",
			rest: {
				method: "PATCH",
				path: ":id/status",
			},
			params: {
				id: { type: "number", positive: true, integer: true, convert: true },
				action: { type: "enum", values: ["APPROVE", "REJECT", "CANCEL"] }
			},
			async handler(ctx) {
				const targetPost = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await TargetPostsStateMachine.createStateMachine(
					targetPost[0].status,
					ctx,
				);

				if (stateMachine.cannot(ctx.params.action)) {
					throw new Errors.ValidationError(
						"Unauthorized target post status change",
						"COMMUNICATION_TARGET_POST_BAD_STATUS",
						{},
					);
				}
				return stateMachine[ctx.params.action.toLowerCase()]();
			},
		},
		sendPendingTargetPosts: {
			visibility: "private",
			async handler(ctx) {
				const pendingTargetPosts = await this._find(ctx, {
					query: (q) => {
						q.whereIn("status", ["APPROVED", "FAILED"])
							.where("schedule_date", "<=", new Date());
					},
				});
				for (const targetPost of pendingTargetPosts) {
					targetPost.notification_method_ids = targetPost.target_post_notification_methods.map(pm => pm.notification_method_id);
					let stateMachine = await TargetPostsStateMachine.createStateMachine(
						targetPost.status,
						ctx,
					);
					ctx.call("notifications.alerts.create_bulk_alert", targetPost).then(result => {
						ctx.params = { id: targetPost.id };
						if (result)
							stateMachine["send"]();
						else
							stateMachine["failed"]();
					}).catch(err => {
						ctx.params = { id: targetPost.id };
						stateMachine["failed"]();
					});
				}
			}
		}
	},
	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
		async createWithRelated(ctx, service, target_post_id, list) {
			if (!(list instanceof Array)) { return []; }
			const listToReturn = [];
			for (const item of list) {
				item.target_post_id = target_post_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) { return; }
			for (const item of list) {
				await ctx.call(service, item);
			}
		},
		async validateTargetId(ctx) {
			if (ctx.params.target_id)
				await ctx.call("authorization.targets.get", { id: ctx.params.target_id });
		},
		async validateNotificationMethods(ctx) {
			if (ctx.params.channels)
				for (const channel of ctx.params.channels) {
					await ctx.call("notifications.notification-methods.get", { id: channel.notification_method_id });
				}
		},
		async validateCategoryId(ctx) {
			if (ctx.params.category_id)
				await ctx.call("communication.categories.get", { id: ctx.params.category_id });
		},
		async validateAlertTemplateId(ctx) {
			if (ctx.params.alert_template_id)
				await ctx.call("notifications.alert-templates.get", { id: ctx.params.alert_template_id });
		},
		async validateFiles(ctx) {
			if (ctx.params.target_post_files)
				for (const file of ctx.params.target_post_files) {
					await ctx.call("media.files.get", { id: file.file_id });
				}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
