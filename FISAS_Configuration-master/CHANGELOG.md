# [1.37.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.36.0...v1.37.0) (2022-06-28)


### Features

* **configurations:** get current and future academic years ([200a91f](https://gitlab.com/fi-sas/FISAS_Configuration/commit/200a91fc6bd9e6c910304c8b2f035c71d4c31f3b))

# [1.36.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.35.0...v1.36.0) (2022-05-04)


### Features

* **volunteering:** new error key ([6aea89e](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6aea89ec422460912a04e98c3050a0c6596c7aeb))

# [1.35.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.34.0...v1.35.0) (2022-04-28)


### Features

* **configurations:** new scholarship error keys ([0318488](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0318488445c2559ac62eb92169cb57e4dec9af81))

# [1.34.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.33.0...v1.34.0) (2022-04-26)


### Features

* **configurations:** volunteering error keys ([a03ed67](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a03ed67c1ba01aff509a8237daa9ceac17e583e3))

# [1.33.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.32.0...v1.33.0) (2022-04-26)


### Features

* **configuration:** ubike new error message ([c0db977](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c0db977528eecbeb5fe60b924de00b01a65659ea))

# [1.32.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.31.3...v1.32.0) (2022-03-31)


### Features

* **configuration:** social scholarship error message ([e82d60f](https://gitlab.com/fi-sas/FISAS_Configuration/commit/e82d60f2fa0478d11afd0b1ca1f199faacbc369b))

## [1.31.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.31.2...v1.31.3) (2022-03-29)


### Bug Fixes

* **sasocial:** remove dependency ([d2ed2a1](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d2ed2a1664ba45751cdb7ebdcfc36a2a773e1cf8))

## [1.31.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.31.1...v1.31.2) (2022-03-10)


### Bug Fixes

* **dashboard:** update default user widgets ([9e5b379](https://gitlab.com/fi-sas/FISAS_Configuration/commit/9e5b379beb27a60fb116151b26f41be55d296e25))

## [1.31.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.31.0...v1.31.1) (2022-01-18)


### Bug Fixes

* **health:** dashboard seed configuration ([82f16ab](https://gitlab.com/fi-sas/FISAS_Configuration/commit/82f16abbdf2e7bda0615321d444fcefc53cd0df3))

# [1.31.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.30.0...v1.31.0) (2022-01-03)


### Features

* **health:** health widget add to seed ([735c00b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/735c00be3214e47d6db540e9b01c7aba73203160))

# [1.30.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.29.0...v1.30.0) (2021-12-16)


### Bug Fixes

* **organic_unit:** organic unit optional ([0548bc2](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0548bc2bf03ac165072f4f0ecf2b83da7ac4dd3d))


### Features

* **holiday:** allow multiple organic units ([548f49d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/548f49d009c8a1888b9cb8b9023b76aa275f69ce))
* **holiday:** holiday table and ms ([cdf2f45](https://gitlab.com/fi-sas/FISAS_Configuration/commit/cdf2f450a4728ce50a4454f40216bd13eb39c2f3))
* **translations:** holiday translations service ([967e10a](https://gitlab.com/fi-sas/FISAS_Configuration/commit/967e10ac3ccd716ffae89508b7bc0d038e578cbf))

# [1.29.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.28.1...v1.29.0) (2021-12-16)


### Features

* UBike add error ([afe879c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/afe879c5fd4648fe549596a0b71e757ad7587c57))

## [1.28.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.28.0...v1.28.1) (2021-12-15)


### Bug Fixes

* disable authorization on get sections bo ([2c586a7](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2c586a709fbf00ff01bd64da2787c8e30fda8bac))

# [1.28.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.27.0...v1.28.0) (2021-12-14)


### Features

* remove section validation ([d2c45c9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d2c45c926c788d5cf5d171661edd94dba95a1006))
* remove section validation ([281052a](https://gitlab.com/fi-sas/FISAS_Configuration/commit/281052aab7cff0b0eb8817d7267a6f1b2b1efe89))

# [1.27.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.26.0...v1.27.0) (2021-12-14)


### Features

* clear cache section_bo, only active services ([3076280](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3076280096b603706f98a5657aa5597ad32872ec))

# [1.26.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.25.0...v1.26.0) (2021-12-14)


### Features

* icon, app mobile and service sections ([f004534](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f004534fb795b82fbbe41d317a52821c452597a9))
* icon, app mobile and service sections ([d431c5c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d431c5c3f40d35a2575f4829f040ba4fd35f7286))
* icon, app mobile and service sections ([5cd1af3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5cd1af3371aa8baa4fbda5962cc147dda9ad0acf))

# [1.25.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.24.2...v1.25.0) (2021-12-02)


### Features

* add error scholarship hasn´t current account ([c414e34](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c414e34efe1165a9996fe1aad7daa9493b4946f6))

## [1.24.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.24.1...v1.24.2) (2021-11-16)


### Bug Fixes

* **services:** action external_services language cache ([8b75095](https://gitlab.com/fi-sas/FISAS_Configuration/commit/8b750954c80b75da14eb8bb7e8d51975702885ce))

## [1.24.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.24.0...v1.24.1) (2021-11-16)


### Bug Fixes

* **services:** external kiosk query ([4177cdc](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4177cdc1338cffaa5ece1f6e4bad20ee77f3d150))

# [1.24.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.23.0...v1.24.0) (2021-11-15)


### Features

* **services:** new kiosk external services action ([ce70341](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ce70341dfbda34d8187fd0e823391256250f4114))

# [1.23.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.22.0...v1.23.0) (2021-11-15)


### Features

* external services - profiles ([1fddee9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/1fddee95c151eb0876de5749bf5a04024537b986))

# [1.22.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.21.0...v1.22.0) (2021-11-04)


### Features

* external services, add and remove ([c5305a9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c5305a91a0a5ef833f8f711a106fe10a941f662b))

# [1.21.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.20.0...v1.21.0) (2021-11-03)


### Features

* publish remove action ([0079be8](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0079be801ba8ec9aa1bd4cdf7d67e7605d447c1b))

# [1.20.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.19.0...v1.20.0) (2021-10-29)


### Features

* add external services ([614cbfe](https://gitlab.com/fi-sas/FISAS_Configuration/commit/614cbfe17bbeed3f63db532e2417b0e87c809cba))

# [1.19.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.18.0...v1.19.0) (2021-10-29)


### Features

* new validations for desk and desk/oper ([8126346](https://gitlab.com/fi-sas/FISAS_Configuration/commit/8126346aae138f0d113c6d3e6ee9013d2e347e93))

# [1.18.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.17.0...v1.18.0) (2021-10-21)


### Features

* errors for queue ([1279744](https://gitlab.com/fi-sas/FISAS_Configuration/commit/1279744da24dd05bd8874997856ebb9443ea1e8c))

# [1.17.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.16.0...v1.17.0) (2021-10-06)


### Features

* configuration for emergency fund v2 ([5693825](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5693825f95619c6b1e80e6129f60da8d8fe44aa9))

# [1.16.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.15.0...v1.16.0) (2021-09-16)


### Features

* **form_informations_seed:** add ubike steps ([8996589](https://gitlab.com/fi-sas/FISAS_Configuration/commit/899658985c7967ae7bcf06446c1be3708f39d605))

# [1.15.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.14.0...v1.15.0) (2021-09-10)


### Features

* **seed:** Health error messages ([ed81a49](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ed81a49bfe17be89f775f3f96a00be4bff312202))

# [1.14.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.13.2...v1.14.0) (2021-09-08)


### Features

* **seeds:** seeds add error social support ([39b4fe8](https://gitlab.com/fi-sas/FISAS_Configuration/commit/39b4fe8a748adf5b3cf3f5a1c566ee430e7d438a))

## [1.13.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.13.1...v1.13.2) (2021-09-01)


### Bug Fixes

* **academic_years:** current comparing  start and end day ([43b5edf](https://gitlab.com/fi-sas/FISAS_Configuration/commit/43b5edf1cb15733bb6b8b1ea1d2110082e65ac03))

## [1.13.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.13.0...v1.13.1) (2021-07-30)


### Bug Fixes

* **devices:** add cache key showCPUID to device GET ([65bc5f6](https://gitlab.com/fi-sas/FISAS_Configuration/commit/65bc5f64b5a01bda65214eba01f5e95395741712))

# [1.13.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.5...v1.13.0) (2021-07-30)


### Features

* initial configuration for emergency fund ([b304c46](https://gitlab.com/fi-sas/FISAS_Configuration/commit/b304c46e501a1d504f0ae10a88227aeb458cd22c))

## [1.12.5](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.4...v1.12.5) (2021-07-28)


### Bug Fixes

* **devices:** add set CPUID id validations ([accf7db](https://gitlab.com/fi-sas/FISAS_Configuration/commit/accf7db05eb65129ba5a364543600ac5a3898c36))

## [1.12.4](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.3...v1.12.4) (2021-07-28)


### Bug Fixes

* **devices:** return cpuID on backoffice ([c5b0b16](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c5b0b1656dd3eed1af8476fe2ada1c9da692849e))

## [1.12.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.2...v1.12.3) (2021-07-28)


### Bug Fixes

* **devices:** add meta showCPUID to return cpuid of devices ([c650ba3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c650ba3fd8d03c162e511a79a459988dcd2ec5a9))

## [1.12.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.1...v1.12.2) (2021-07-27)


### Bug Fixes

* **devices:** change name of cpuid ([66443a0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/66443a089a555837384d0a9a89b43422f31f7e63))

## [1.12.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.12.0...v1.12.1) (2021-07-27)


### Bug Fixes

* **devices:** change PUT cpuid to get id from token ([28649cb](https://gitlab.com/fi-sas/FISAS_Configuration/commit/28649cbd8de2d059c27668c040af95db12cea056))

# [1.12.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.11.0...v1.12.0) (2021-07-27)


### Features

* **devices:** add endpoint too update  device CPUID ([bbdc7ad](https://gitlab.com/fi-sas/FISAS_Configuration/commit/bbdc7ad3eceb5ed2b1ea43deacea842b434eafe6))

# [1.11.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.10.3...v1.11.0) (2021-07-27)


### Features

* **devices:** add new field CPUID ([d711e6c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d711e6cbc8280c7ddcad2bf7ba8e9769643b6eca))

## [1.10.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.10.2...v1.10.3) (2021-07-20)


### Bug Fixes

* **erros:** remove duplicated entrys on errors file ([d3bef20](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d3bef2007e7d1e1d7b4f7ed165f698c98a8727cf))

## [1.10.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.10.1...v1.10.2) (2021-07-13)


### Bug Fixes

* **seed:** form_information fix name ([5a709ec](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5a709ec5c99412dea05615fe6bfe60720aa1d484))

## [1.10.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.10.0...v1.10.1) (2021-07-05)


### Bug Fixes

* **languages:** remove action delete from published ([6300a4f](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6300a4fb714835ca465311a92540aa34706cd627))
* **services:** remove action create and delete from published ([16170e4](https://gitlab.com/fi-sas/FISAS_Configuration/commit/16170e45dc0e0e8ecae2daa85fa82449488d86ba))

# [1.10.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.9.0...v1.10.0) (2021-07-01)


### Bug Fixes

* **seed:** cganhe sub23 application to sub23 declaration ([bd8ba21](https://gitlab.com/fi-sas/FISAS_Configuration/commit/bd8ba21be365c9f24636b5892f48239bc78463ee))


### Features

* **seed:** add bus sub23 application form_info seed ([abe62b9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/abe62b991a47912da01e0db3dc04598221391712))

# [1.9.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.8.0...v1.9.0) (2021-06-28)


### Bug Fixes

* **terms:** on remove dont give erro on file not found ([973410a](https://gitlab.com/fi-sas/FISAS_Configuration/commit/973410a4a8cc07596000c060c1c8258539cdf588))


### Features

* **contacts:** add cascade delte on contacts translations ([37d7012](https://gitlab.com/fi-sas/FISAS_Configuration/commit/37d7012cf5c499b6bd3ba521ef0a16e101686f46))

# [1.8.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.5...v1.8.0) (2021-06-28)


### Features

* **contacts:** add search filtering to contacts translations ([b289c1c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/b289c1c7413c75cf563e1210455c4a49cfe4e4bf))
* **services:** add translation search to services ([a68c5a2](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a68c5a25b39694ea2cc79c1fea6afbb5ce6fc78a))

## [1.7.5](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.4...v1.7.5) (2021-06-25)


### Bug Fixes

* **footers:** fix footers CASCADE and withRelated ([17412b9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/17412b9ae152817d1be64cdd38413a2abaac4e78))

## [1.7.4](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.3...v1.7.4) (2021-06-25)


### Bug Fixes

* **groups:** fix groups cascade rule ([fb056b7](https://gitlab.com/fi-sas/FISAS_Configuration/commit/fb056b79cc2c064316c9f5a0f20cd0d74cca7b5a))

## [1.7.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.2...v1.7.3) (2021-06-25)


### Bug Fixes

* **regulations:** fix error on update files ([bfd3197](https://gitlab.com/fi-sas/FISAS_Configuration/commit/bfd3197b1bc5f41d5282844be6f0b108663c23a3))
* **regulations:** fix last commit ([1dbe679](https://gitlab.com/fi-sas/FISAS_Configuration/commit/1dbe679362a64b2d8e637df1f147d2bc9b4c2b8d))

## [1.7.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.1...v1.7.2) (2021-06-25)


### Bug Fixes

* **groups_footers:** fix call wrong service for groups ([c984771](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c984771a51a6c8710dfb1d2d3c7236d16699bc1c))

## [1.7.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.7.0...v1.7.1) (2021-06-21)


### Bug Fixes

* **form_info:** add service_id to form_information ([c70034b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c70034bc76af4d6fd3ce1fea45a09cc4714f748f))
* **form_info:** add service_id tof orm_information ([f97794e](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f97794e119995d7bf72405e6ec2486ec0f643e24))

# [1.7.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.6.3...v1.7.0) (2021-06-09)


### Features

* add new errors to seed and show raw message on DEBUG env ([ccf7cfd](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ccf7cfdce233f9aabf2ba6d184ec734b5269e7e6))

## [1.6.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.6.2...v1.6.3) (2021-06-07)


### Bug Fixes

* **seed:** change update seed ([db26ee7](https://gitlab.com/fi-sas/FISAS_Configuration/commit/db26ee766cb83a5d6c906d689cb67a1998f93334))
* **seed:** update news widget call path ([ef9d4d5](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ef9d4d5e7f336d69f061cd996d22641de1d0e409))

## [1.6.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.6.1...v1.6.2) (2021-05-28)


### Bug Fixes

* **courses:** change acronym size to 32 ([b57cf89](https://gitlab.com/fi-sas/FISAS_Configuration/commit/b57cf89ef2f46910de378889c76122521532eb45))

## [1.6.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.6.0...v1.6.1) (2021-05-26)


### Bug Fixes

* **course:** change course name size ([4f6ee30](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4f6ee3033ad0a6e5d5a0f75769ed42b49d024e22))

# [1.6.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.5.2...v1.6.0) (2021-05-13)


### Features

* **courses:** add external_ref field ([49a9ac0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/49a9ac0996b56b6f3478f76140ebb2e094bfbcd4))

## [1.5.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.5.1...v1.5.2) (2021-05-04)


### Bug Fixes

* **form_info:** add visible flag ([c214864](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c2148644d726961ec5d189192c21340e44b6a12e))

## [1.5.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.5.0...v1.5.1) (2021-05-04)


### Bug Fixes

* **migration:** fix table name ([2f9c2d6](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2f9c2d697f3dd2a4cfaf46d7a051f89695fe2637))
* **services:** remove unused target_id ([fa01bca](https://gitlab.com/fi-sas/FISAS_Configuration/commit/fa01bca239467b583971c82be8f65f42e191485e))

# [1.5.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.4.0...v1.5.0) (2021-04-30)


### Features

* **academic_year:** get current academic year ([eebccc0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/eebccc0f6b54ed68136a68469f91bc5c4346935f))

# [1.4.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.3.0...v1.4.0) (2021-04-27)


### Features

* **form_informations:** add requests to save forms informations ([dd7503d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/dd7503daab5d8ecb13ccce52c350cc39236da0e8))

# [1.3.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.2.1...v1.3.0) (2021-04-14)


### Bug Fixes

* **academic_year:** change currrent field to hook and update ms_core ([6a4281f](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6a4281f22e44e0a073a8180570ba09d2498e02e1))


### Features

* **academic_years:** add academic_years table ([4efd5a5](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4efd5a5b3549c6fe58d0b07a176cd9f6cbfae8c1))

## [1.2.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.2.0...v1.2.1) (2021-03-25)


### Bug Fixes

* **sampleSeeds:** prevent not found device/group ([81c52e9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/81c52e91e3753926f57c38d14e732a40bf80f16f))

# [1.2.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.1.1...v1.2.0) (2021-03-15)


### Features

* **dashboard_user:** add patch request to update user widget preferences ([6da2ae0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6da2ae024265dd700b52c805782e4d7e6d615864))

## [1.1.1](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.1.0...v1.1.1) (2021-03-09)


### Bug Fixes

* **dashboard_widget:** fix error on remove from widget list ([084d377](https://gitlab.com/fi-sas/FISAS_Configuration/commit/084d377e362bb33c56be05a24e087f2d16f742fd))

# [1.1.0](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0...v1.1.0) (2021-03-09)


### Bug Fixes

* **dashboard:** get dashboard widgets information by user ([669987b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/669987b3de8e90a78afa2feabd9bc48c78b1fb96))


### Features

* **dashboard:** initial widgets endpoints ([e2d26b8](https://gitlab.com/fi-sas/FISAS_Configuration/commit/e2d26b8f03a5376228024459b5fbd75b2d89d2fd))

# 1.0.0 (2021-03-09)


### Bug Fixes

* example dev env ([f3aaae9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f3aaae99f9190b40e984e93bef97e513f1be8e67))
* fix entrypoint ([0db224b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0db224be97d971946334c489592737268f7ccf30))
* migration file ([a038e63](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a038e63d1cf41da4dd891badf351565632cce56e))
* **courses:** add course degree withRelated ([ae6bd22](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ae6bd226a2a2b15e66d80afba7ee251d0185bee8))
* **courses:** fix withRelated courseDegree ([53036c3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/53036c3a39b3837d8eb010e3d4bbab7c207a8903))
* **devices:** add device_groups related ([9c6cafb](https://gitlab.com/fi-sas/FISAS_Configuration/commit/9c6cafb42a2d28c43a11755f53dad7f4d588827e))
* **devices:** add validatores ([7c31c12](https://gitlab.com/fi-sas/FISAS_Configuration/commit/7c31c1285bfa848b1ab5ad098272459ca31323f8))
* **docker-compose:** create docker-compose to prod and dev ([0249f91](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0249f91171cbddbd86fdfaf59f3b5e12a3052234))
* **docker-compose:** fix docker-compose for prodution ([57bede9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/57bede901074aeafaf3c39ba4b43fc7dc947cdec))
* **errors:** fix errorValidation validation ([9d029dc](https://gitlab.com/fi-sas/FISAS_Configuration/commit/9d029dc0b6d664d3f019dbfd26a89f2d9a9e9af0))
* **eslint:** update rules ([341c16b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/341c16b7d686f0eb9ff65a8409f687a80622d350))
* **group_devices:** remove cycle of withRelateds ([a99c1a0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a99c1a0f1619e93d247938136e6db2ccb7b611c0))
* **groups:** withRelated Devices ([db6427c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/db6427c16a3264b28f42e5e14875bb0a99ed00ff))
* **knex:** fix knex unexpected close connection ([a2d5178](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a2d5178c476b7ade3f03d8b1a9bbc314effd361e))
* **languages:** wrong 'visibility' location ([5ad244b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5ad244b4e1e16c13bfdac6048163834e58c7452b))
* **migrations:** add down for tax ([c368a1b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c368a1b4de5ab0f7e1b96144bdff9fc67a53f02d))
* **migrations:** fix uuid field ([992c4ec](https://gitlab.com/fi-sas/FISAS_Configuration/commit/992c4ec9308f8672e40cc5a9586c853c027c4dc0))
* **migrations:** remove '201807301637_setup_languages' ([37db38d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/37db38dbbbba9e31a2ebcf52e23d0413703522ed))
* **ms:** change docker process and add errors feature ([654d9b9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/654d9b9285e183b1f35371678aeee8588346faaa))
* **ms_core:** update ms_core package ([f058603](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f058603a2259400323d94ef0989a30d2aeb6e05e))
* **regulations:** fix regulations ([edbae5c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/edbae5ccbc5a6aa62cd179cc63d38c319f793f4f))
* **regulations:** minor fix ([4deb993](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4deb9936d3d3c51701e7c83562ffd5de0f518365))
* **regulations:** new migration and seed files ([235ea51](https://gitlab.com/fi-sas/FISAS_Configuration/commit/235ea5110ad1123f31dcb9466c0ceab18f79588b))
* **services:** add filter by language ([56a1181](https://gitlab.com/fi-sas/FISAS_Configuration/commit/56a1181cbaf815c85f449182ee2a2682d96f0d74))
* **services:** fix service seed and REST ([077cdda](https://gitlab.com/fi-sas/FISAS_Configuration/commit/077cdda55308ba4ce1a1c486a5664dd740399cfa))
* actions visibility changed to published ([3d9018a](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3d9018ae9ad89ed44ffc0c91746c47fe79787111))
* add food services withRealte to Schools ([cad8efd](https://gitlab.com/fi-sas/FISAS_Configuration/commit/cad8efd1206f974a90eabc117c6d74933afee5c4))
* fixing node version (node:12-buster-slim) ([48bd43d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/48bd43d5485e2ad6b8dee334de7a69e38981a55f))
* minor fix ([c9fdaaf](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c9fdaafedb2145353dcc88a1a238535247d315b3))
* minor fix ([74098c4](https://gitlab.com/fi-sas/FISAS_Configuration/commit/74098c4beec1d4ad516c338d562f7930a0b49369))
* minor fixs ([d816da6](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d816da614a241d0e71fdaf4eb4105dc0e6e40db5))
* regulation seed and migration ([642ce57](https://gitlab.com/fi-sas/FISAS_Configuration/commit/642ce577c419c84f422761ad87b1748d9c3c3058))
* **seed:** fix wrong table name ([4e817f3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4e817f3e8ce5213f6174a60d9ded4ded662c87e1))
* **seeds:** add errors seeds ([19953fb](https://gitlab.com/fi-sas/FISAS_Configuration/commit/19953fb3f1f6547d1c5bc303456def08a89c2195))
* **seeds:** add new errors ([00a8a08](https://gitlab.com/fi-sas/FISAS_Configuration/commit/00a8a0833f99b740e2386218a41c4cdb56ca8234))
* **seeds:** add school, course_degree, course ([effb374](https://gitlab.com/fi-sas/FISAS_Configuration/commit/effb374e77981c63a10a98c0cd9a0dcf6e5f6aea))
* **seeds:** add script run specified seed ([0d201d1](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0d201d14b88b06e4b06791b27d532b8d9f12af99))
* **seeds:** fix services seeds ([3dadf01](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3dadf017dd90d6108967551bff18b8578d39f071))
* **seeds:** remove '05_component_device' and '10_service' ([b7ec78d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/b7ec78dacb238aaf25de11108b1ff52c4781f003))
* **services:** fix with relateds keys ([68758ff](https://gitlab.com/fi-sas/FISAS_Configuration/commit/68758ff86bf514fe9f69fb959bcb6831c00c121d))


### Features

* **apresentation_text:** add apresentation text funcionality ([6c721a4](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6c721a4cf93e4b7fffbe88b1319fdddb2035aa12))
* **configs:** add courses, courses degree and schools endpoints ([3a8d342](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3a8d3429c1ecaf1d0d2344fa8806524ed7bd9aa2))
* **configurations:** add configurations endpoint ([eb19135](https://gitlab.com/fi-sas/FISAS_Configuration/commit/eb191359fa29145db8a11750b2d605467af35ba6))
* **errors:** erros return extra data now ([2e4c580](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2e4c5808b3b13e794e0061821d49d17971d3078d))
* **languages:** add getByAcronym action ([54f515c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/54f515c9447648975c3b2f47b1d1dfbf18039517))
* **languages:** add order on languages ([eb37a61](https://gitlab.com/fi-sas/FISAS_Configuration/commit/eb37a6129fb7c1433ec0d963bf7d8ad51247596c))
* **seeds:** add rebuild table sequences ([65a5340](https://gitlab.com/fi-sas/FISAS_Configuration/commit/65a5340469a2f2472333d62da6e9d98e22bf69c7))
* **seeds:** add sample seed's ([13ce231](https://gitlab.com/fi-sas/FISAS_Configuration/commit/13ce2317c437566468f8a4d3f70fdd19a9cf726f))
* **texts:** add files to personalizable texts ([5fa046c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5fa046c40c57b6b479c89cae09ea54ab5a7d3099))
* initial changes for boilerplate v2.0 ([775b32b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/775b32b78bcc0b78db5f02202238790ba296c6f7))
* migrate from MySQL into Postgres ([ad36180](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ad361808cee97d204508ad565e2981f1f66e3528))
* terms, privacy policies and informations ([40c8232](https://gitlab.com/fi-sas/FISAS_Configuration/commit/40c8232beb21a0f3690c0fb49d3f5596ab2633c3))
* **services:** add initial services setup ([2e330ce](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2e330ce719a9f018b64960cb38ad30966b4941b9))

# [1.0.0-rc.30](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-02-19)


### Features

* **texts:** add files to personalizable texts ([5fa046c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5fa046c40c57b6b479c89cae09ea54ab5a7d3099))

# [1.0.0-rc.29](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-02-17)


### Features

* **languages:** add order on languages ([eb37a61](https://gitlab.com/fi-sas/FISAS_Configuration/commit/eb37a6129fb7c1433ec0d963bf7d8ad51247596c))

# [1.0.0-rc.28](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-02-08)


### Bug Fixes

* migration file ([a038e63](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a038e63d1cf41da4dd891badf351565632cce56e))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-02-08)


### Bug Fixes

* example dev env ([f3aaae9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f3aaae99f9190b40e984e93bef97e513f1be8e67))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-02-08)


### Bug Fixes

* fix entrypoint ([0db224b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0db224be97d971946334c489592737268f7ccf30))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-01-28)


### Bug Fixes

* **services:** add filter by language ([56a1181](https://gitlab.com/fi-sas/FISAS_Configuration/commit/56a1181cbaf815c85f449182ee2a2682d96f0d74))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-01-27)


### Bug Fixes

* **services:** fix service seed and REST ([077cdda](https://gitlab.com/fi-sas/FISAS_Configuration/commit/077cdda55308ba4ce1a1c486a5664dd740399cfa))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-01-19)


### Bug Fixes

* minor fix ([c9fdaaf](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c9fdaafedb2145353dcc88a1a238535247d315b3))
* **regulations:** minor fix ([4deb993](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4deb9936d3d3c51701e7c83562ffd5de0f518365))


### Features

* terms, privacy policies and informations ([40c8232](https://gitlab.com/fi-sas/FISAS_Configuration/commit/40c8232beb21a0f3690c0fb49d3f5596ab2633c3))
* **apresentation_text:** add apresentation text funcionality ([6c721a4](https://gitlab.com/fi-sas/FISAS_Configuration/commit/6c721a4cf93e4b7fffbe88b1319fdddb2035aa12))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-01-15)


### Bug Fixes

* add food services withRealte to Schools ([cad8efd](https://gitlab.com/fi-sas/FISAS_Configuration/commit/cad8efd1206f974a90eabc117c6d74933afee5c4))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-01-12)


### Bug Fixes

* minor fixs ([d816da6](https://gitlab.com/fi-sas/FISAS_Configuration/commit/d816da614a241d0e71fdaf4eb4105dc0e6e40db5))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-01-12)


### Bug Fixes

* **group_devices:** remove cycle of withRelateds ([a99c1a0](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a99c1a0f1619e93d247938136e6db2ccb7b611c0))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-01-12)


### Bug Fixes

* minor fix ([74098c4](https://gitlab.com/fi-sas/FISAS_Configuration/commit/74098c4beec1d4ad516c338d562f7930a0b49369))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-01-12)


### Bug Fixes

* **groups:** withRelated Devices ([db6427c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/db6427c16a3264b28f42e5e14875bb0a99ed00ff))
* **regulations:** fix regulations ([edbae5c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/edbae5ccbc5a6aa62cd179cc63d38c319f793f4f))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-01-12)


### Bug Fixes

* **groups:** withRelated Devices ([db6427c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/db6427c16a3264b28f42e5e14875bb0a99ed00ff))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-01-08)


### Bug Fixes

* regulation seed and migration ([642ce57](https://gitlab.com/fi-sas/FISAS_Configuration/commit/642ce577c419c84f422761ad87b1748d9c3c3058))
* **regulations:** new migration and seed files ([235ea51](https://gitlab.com/fi-sas/FISAS_Configuration/commit/235ea5110ad1123f31dcb9466c0ceab18f79588b))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-01-06)


### Features

* **languages:** add getByAcronym action ([54f515c](https://gitlab.com/fi-sas/FISAS_Configuration/commit/54f515c9447648975c3b2f47b1d1dfbf18039517))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-01-05)


### Bug Fixes

* **eslint:** update rules ([341c16b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/341c16b7d686f0eb9ff65a8409f687a80622d350))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-01-05)


### Bug Fixes

* **seeds:** add school, course_degree, course ([effb374](https://gitlab.com/fi-sas/FISAS_Configuration/commit/effb374e77981c63a10a98c0cd9a0dcf6e5f6aea))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2020-12-30)


### Features

* **configurations:** add configurations endpoint ([eb19135](https://gitlab.com/fi-sas/FISAS_Configuration/commit/eb191359fa29145db8a11750b2d605467af35ba6))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2020-12-29)


### Bug Fixes

* **devices:** add device_groups related ([9c6cafb](https://gitlab.com/fi-sas/FISAS_Configuration/commit/9c6cafb42a2d28c43a11755f53dad7f4d588827e))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2020-12-21)


### Bug Fixes

* **knex:** fix knex unexpected close connection ([a2d5178](https://gitlab.com/fi-sas/FISAS_Configuration/commit/a2d5178c476b7ade3f03d8b1a9bbc314effd361e))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2020-12-17)


### Features

* **errors:** erros return extra data now ([2e4c580](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2e4c5808b3b13e794e0061821d49d17971d3078d))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2020-12-14)


### Bug Fixes

* **seeds:** add new errors ([00a8a08](https://gitlab.com/fi-sas/FISAS_Configuration/commit/00a8a0833f99b740e2386218a41c4cdb56ca8234))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2020-12-14)


### Bug Fixes

* **seeds:** add errors seeds ([19953fb](https://gitlab.com/fi-sas/FISAS_Configuration/commit/19953fb3f1f6547d1c5bc303456def08a89c2195))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2020-12-09)


### Bug Fixes

* **devices:** add validatores ([7c31c12](https://gitlab.com/fi-sas/FISAS_Configuration/commit/7c31c1285bfa848b1ab5ad098272459ca31323f8))
* **migrations:** add down for tax ([c368a1b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/c368a1b4de5ab0f7e1b96144bdff9fc67a53f02d))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-12-09)


### Bug Fixes

* **migrations:** fix uuid field ([992c4ec](https://gitlab.com/fi-sas/FISAS_Configuration/commit/992c4ec9308f8672e40cc5a9586c853c027c4dc0))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-11-20)


### Bug Fixes

* **seeds:** fix services seeds ([3dadf01](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3dadf017dd90d6108967551bff18b8578d39f071))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-11-18)


### Bug Fixes

* **ms_core:** update ms_core package ([f058603](https://gitlab.com/fi-sas/FISAS_Configuration/commit/f058603a2259400323d94ef0989a30d2aeb6e05e))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-11-11)


### Features

* **seeds:** add rebuild table sequences ([65a5340](https://gitlab.com/fi-sas/FISAS_Configuration/commit/65a5340469a2f2472333d62da6e9d98e22bf69c7))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/FISAS_Configuration/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-11-10)


### Bug Fixes

* **seed:** fix wrong table name ([4e817f3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/4e817f3e8ce5213f6174a60d9ded4ded662c87e1))

# 1.0.0-rc.1 (2020-11-06)


### Bug Fixes

* fixing node version (node:12-buster-slim) ([48bd43d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/48bd43d5485e2ad6b8dee334de7a69e38981a55f))
* **courses:** add course degree withRelated ([ae6bd22](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ae6bd226a2a2b15e66d80afba7ee251d0185bee8))
* **courses:** fix withRelated courseDegree ([53036c3](https://gitlab.com/fi-sas/FISAS_Configuration/commit/53036c3a39b3837d8eb010e3d4bbab7c207a8903))
* **docker-compose:** create docker-compose to prod and dev ([0249f91](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0249f91171cbddbd86fdfaf59f3b5e12a3052234))
* **docker-compose:** fix docker-compose for prodution ([57bede9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/57bede901074aeafaf3c39ba4b43fc7dc947cdec))
* **errors:** fix errorValidation validation ([9d029dc](https://gitlab.com/fi-sas/FISAS_Configuration/commit/9d029dc0b6d664d3f019dbfd26a89f2d9a9e9af0))
* **languages:** wrong 'visibility' location ([5ad244b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/5ad244b4e1e16c13bfdac6048163834e58c7452b))
* **seeds:** add script run specified seed ([0d201d1](https://gitlab.com/fi-sas/FISAS_Configuration/commit/0d201d14b88b06e4b06791b27d532b8d9f12af99))
* actions visibility changed to published ([3d9018a](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3d9018ae9ad89ed44ffc0c91746c47fe79787111))
* **migrations:** remove '201807301637_setup_languages' ([37db38d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/37db38dbbbba9e31a2ebcf52e23d0413703522ed))
* **ms:** change docker process and add errors feature ([654d9b9](https://gitlab.com/fi-sas/FISAS_Configuration/commit/654d9b9285e183b1f35371678aeee8588346faaa))
* **seeds:** remove '05_component_device' and '10_service' ([b7ec78d](https://gitlab.com/fi-sas/FISAS_Configuration/commit/b7ec78dacb238aaf25de11108b1ff52c4781f003))
* **services:** fix with relateds keys ([68758ff](https://gitlab.com/fi-sas/FISAS_Configuration/commit/68758ff86bf514fe9f69fb959bcb6831c00c121d))


### Features

* **seeds:** add sample seed's ([13ce231](https://gitlab.com/fi-sas/FISAS_Configuration/commit/13ce2317c437566468f8a4d3f70fdd19a9cf726f))
* migrate from MySQL into Postgres ([ad36180](https://gitlab.com/fi-sas/FISAS_Configuration/commit/ad361808cee97d204508ad565e2981f1f66e3528))
* **services:** add initial services setup ([2e330ce](https://gitlab.com/fi-sas/FISAS_Configuration/commit/2e330ce719a9f018b64960cb38ad30966b4941b9))
* initial changes for boilerplate v2.0 ([775b32b](https://gitlab.com/fi-sas/FISAS_Configuration/commit/775b32b78bcc0b78db5f02202238790ba296c6f7))
* **configs:** add courses, courses degree and schools endpoints ([3a8d342](https://gitlab.com/fi-sas/FISAS_Configuration/commit/3a8d3429c1ecaf1d0d2344fa8806524ed7bd9aa2))
