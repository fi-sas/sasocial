module.exports.up = async (db) => {
	return db.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
	  .createTable('device', function(table) {
	  table.increments();
	  table.uuid("uuid").default(db.raw("uuid_generate_v4()")).notNullable();
	  table.string('name', 120).notNullable();
	  table.enu('type', ['KIOSK', 'TV', 'MOBILE', 'BO', 'WEB', 'POS', 'GENERIC'],  { useNative: true, existingType: false, enumName: 'device_type' });
	  table.enu('orientation', ['PORTRAIT', 'LANDSCAPE'], { useNative: true, existingType: false, enumName: 'device_orientation' });
	  table.boolean('active').notNullable().defaultTo(false);
	  table.integer('secondsToInactivity');
	  table.datetime('updated_at').notNullable();
	  table.datetime('created_at').notNullable();
	  table.unique(['uuid']);
	})
	.createTable('group', function(table) {
	  table.increments();
	  table.string('name', 120).notNullable();
	  table.boolean('active').notNullable().defaultTo(false);
	  table.datetime('updated_at').notNullable();
	  table.datetime('created_at').notNullable();
	})
	.createTable('group_device', function(table) {
	  table.increments();
	  table.integer('group_id').unsigned().references('id').inTable('group');
	  table.integer('device_id').unsigned().references('id').inTable('device');
	  table.unique(['group_id', 'device_id']);
	})
	.createTable('component', function(table) {
		table.increments();
		table.string('name', 120).notNullable();
		table.boolean('active').notNullable().defaultTo(false);
		table.integer('device_id').unsigned().references('id').inTable('device');
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('language', function (table) {
		table.increments()
		table.string('name', 120).notNullable()
		table.string('acronym', 10).notNullable()
		table.boolean('active').notNullable().defaultTo(false)
		table.datetime('updated_at').notNullable()
		table.datetime('created_at').notNullable()
	})
	.createTable('footer', (table) => {
		table.increments();
		table.string('label', 120);
		table.datetime('date_begin').notNullable();
		table.datetime('date_end');
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('footer_translation', (table) => {
		table.increments();
		table.integer('footer_id').notNullable().unsigned().references('id').inTable('footer');
		table.integer('language_id').unsigned();
		table.integer('file_id').unsigned();
	})
	.createTable('group_footer', (table) => {
		table.increments();
		table.integer('footer_id').unsigned().references('id').inTable('footer');
		table.integer('group_id').unsigned();
		table.unique(['footer_id', 'group_id']);
	})
	.createTable('service', (table) => {
		table.increments();
		table.boolean('active').notNullable().defaultTo(false);
		table.integer('target_id');
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('tax', (table) => {
		table.increments();
		table.string('name', 255).notNullable();
		table.string('description', 255);
		table.float('tax_value').notNullable();
		table.boolean('active').notNullable();
		table.datetime('created_at').notNullable();
		table.datetime('updated_at').notNullable();
	})
	.createTable('service_translation', (table) => {
		table.increments();
		table.integer('service_id').notNullable().unsigned().references('id').inTable('service');
		table.integer('language_id').unsigned();
		table.string('name', 120);
		table.text('description', 500);
		table.unique(['service_id', 'language_id']);
	})
	.createTable('course_degree', function(table) {
		table.increments();
		table.string('name', 120).notNullable();
		table.boolean('active').notNullable().defaultTo(false);
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('school', function(table) {
		table.increments();
		table.string('name', 120).notNullable();
		table.string('acronym', 10).notNullable();
		table.boolean('active').notNullable().defaultTo(false);
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('course', function(table) {
		table.increments();
		table.string('name', 120).notNullable();
		table.boolean('active').notNullable().defaultTo(false);
		table.string('acronym', 10);
		table.integer('course_degree_id').unsigned().references('id').inTable('course_degree');
		table.integer('school_id').unsigned().references('id').inTable('school');
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('contact', (table) => {
		table.increments();
		table.string('email');
		table.string('url');
		table.datetime('updated_at').notNullable();
		table.datetime('created_at').notNullable();
	})
	.createTable('contact_translation', (table) => {
		table.increments();
		table.integer('contact_id').notNullable().unsigned().references('id').inTable('contact');
		table.integer('language_id').unsigned();
		table.string('name', 120).notNullable();
		table.text('description').notNullable();
		table.unique(['contact_id', 'language_id']);
	})
	.createTable('error', (table) => {
		table.increments();
		table.string('type').notNullable();
		table.text('message', 200);
		table.unique(['type']);
	})
	.createTable('error_translation', (table) => {
		table.increments();
		table.text('message', 200);
		table.string('type').notNullable().references('type').inTable('error');
		table.integer('language_id').unsigned();
		table.unique(['type', 'language_id']);
	})


  };

  module.exports.down = async db => db.schema
	  .dropTable('error_translation')
	  .dropTable('error')
	  .dropTable('contact_translation')
	  .dropTable('contact')
	  .dropTable('course')
	  .dropTable('school')
	  .dropTable('course_degree')
	  .dropTable('service_translation')
	  .dropTable('service')
	  .dropTable('group_footer')
	  .dropTable('footer_translation')
	  .dropTable('footer')
	  .dropTable('language')
	  .dropTable('component')
	  .dropTable('group_device')
	  .dropTable('group')
	  .dropTable('device')
	  .dropTable('tax')
	;

  module.exports.configuration = { transaction: true };
