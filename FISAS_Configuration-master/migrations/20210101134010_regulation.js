module.exports.up = async db => db.schema
	.createTable('regulation', (table) => {
		table.increments();
		table.integer('language_id').unsigned().notNullable().references('id').inTable('language');
		table.integer('service_id').unsigned().notNullable().references('id').inTable('service');
		table.text('regulation');
		table.datetime('created_at').notNullable();
		table.datetime('updated_at').notNullable();
		table.unique(['service_id', 'language_id']);
	})

module.exports.down = async db => db.schema.dropTable('regulation');
module.exports.configuration = { transaction: true };
