module.exports.up = async db => db.schema
    .createTable('apresentation_text', (table) => {
        table.increments();
        table.integer('language_id').unsigned().notNullable().references('id').inTable('language');
        table.integer('service_id').unsigned().notNullable().references('id').inTable('service');
        table.text('text');
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
        table.unique(['service_id', 'language_id']);
    })

module.exports.down = async db => db.schema.dropTable('apresentation_text');
module.exports.configuration = { transaction: true };
