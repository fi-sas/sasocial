module.exports.up = async db => db.schema
	.createTable("term", (table) => {
		table.increments();
		table.integer("language_id").unsigned().notNullable().references("id").inTable("language");
		table.integer("service_id").unsigned().notNullable().references("id").inTable("service");
		table.text("text");
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
		table.unique(["service_id", "language_id"]);
	})
	.createTable("information", (table) => {
		table.increments();
		table.integer("language_id").unsigned().notNullable().references("id").inTable("language");
		table.integer("service_id").unsigned().notNullable().references("id").inTable("service");
		table.text("text");
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
		table.unique(["service_id", "language_id"]);
	})
	.createTable("privacy_policy", (table) => {
		table.increments();
		table.integer("language_id").unsigned().notNullable().references("id").inTable("language");
		table.integer("service_id").unsigned().notNullable().references("id").inTable("service");
		table.text("text");
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
		table.unique(["service_id", "language_id"]);
	})
	;

module.exports.down = async db => db.schema.dropTable("term").dropTable("information").dropTable("privacy_policy");
module.exports.configuration = { transaction: true };