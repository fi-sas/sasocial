module.exports.up = async db => db.schema
    .alterTable("course", (table) => {
        table.dropForeign("school_id");
        table.integer("school_id").unsigned().notNullable().alter();
        table.renameColumn("school_id", "organic_unit_id");
    })
    .dropTable("school");

module.exports.down = async db => db.schema
    .createTable("school", (table) => {
        table.increments();
        table.string('name', 120).notNullable();
        table.string('acronym', 10).notNullable();
        table.boolean('active').notNullable().defaultTo(false);
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })
    .alterTable("course", (table) => {
        table.renameColumn("organic_unit_id", "school_id");
        table.integer('school_id').unsigned().references('id').inTable('school');
    });
module.exports.configuration = { transaction: true };