module.exports.up = async (db) =>
	db.schema
		.table("language", (table) => {
			table.integer("order");
		});

module.exports.down = async db => db.schema
	.table("language", (table) => {
		table.dropColumn("order");
	});


module.exports.configuration = { transaction: true };
