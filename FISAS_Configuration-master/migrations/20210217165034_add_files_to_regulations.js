module.exports.up = async db => db.schema
	.createTable("term_file", (table) => {
		table.increments();
		table.integer("term_id").unsigned().notNullable().references("id").inTable("term");
		table.integer("file_id").unsigned().notNullable();
	})
	.createTable("information_file", (table) => {
		table.increments();
		table.integer("information_id").unsigned().notNullable().references("id").inTable("information");
		table.integer("file_id").unsigned().notNullable();
	})
	.createTable("privacy_policy_file", (table) => {
		table.increments();
		table.integer("privacy_policy_id").unsigned().notNullable().references("id").inTable("privacy_policy");
		table.integer("file_id").unsigned().notNullable();
	})
	.createTable("apresentation_text_file", (table) => {
		table.increments();
		table.integer("apresentation_text_id").unsigned().notNullable().references("id").inTable("apresentation_text");
		table.integer("file_id").unsigned().notNullable();
	});

module.exports.down = async db => db.schema
	.dropTable("term_file")
	.dropTable("information_file")
	.dropTable("privacy_policy_file")
	.dropTable("apresentation_text_file");
module.exports.configuration = { transaction: true };
