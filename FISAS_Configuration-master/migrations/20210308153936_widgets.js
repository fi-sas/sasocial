module.exports.up = async (db) => db.schema.createTable("widget", (table) => {
	table.increments();
	table.string("type", 120).notNullable();
	table.integer("service_id").unsigned();
	table.boolean("active").notNullable().defaultTo(true);
	table.boolean("loginRequired").notNullable().defaultTo(false);
	table.boolean("logoutRequired").notNullable().defaultTo(false);
	table.integer("position").unsigned().notNullable().defaultTo(0);
	table.string("call_path", 120).nullable();
	table.unique(["type"]);
})
	.createTable("user_widget", (table) => {
		table.increments();
		table.integer("widget_id").unsigned().references("id").inTable("widget");
		table.integer("user_id").unsigned();
		table.boolean("active").notNullable().defaultTo(true);
		table.integer("position").unsigned().notNullable().defaultTo(0);
	});

module.exports.down = async db => db.schema
	.dropTable("user_widget")
	.dropTable("widget");

module.exports.configuration = { transaction: true };
