
exports.up = async db => {
	await db.schema
		.table("service", (table) => {
			table.dropColumn("target_id");
		});
};

exports.down = async db => {
	await db.schema
		.table("service", (table) => {
			table.integer("target_id");
		});
};
module.exports.configuration = { transaction: true };
