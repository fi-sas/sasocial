module.exports.up = (db) =>
	db.schema
		.createTable("academic_year", (table) => {
			table.increments();
			table.string("academic_year", 9).notNullable().unique();
			table.date("start_date").notNullable();
			table.date("end_date").notNullable();
		});
module.exports.down = (db) =>
	db.schema
		.dropTable("academic_year");
module.exports.configuration = { transaction: true };
