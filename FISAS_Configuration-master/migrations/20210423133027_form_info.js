module.exports.up = (db) =>
	db.schema
		.createTable("form_information", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("key").notNullable().unique();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})
		.createTable("form_information_translation", (table) => {
			table.increments();
			table.integer("form_information_id").unsigned().notNullable().references("id").inTable("form_information");
			table.integer("language_id").unsigned().notNullable().references("id").inTable("language");
			table.text("info").notNullable();
			table.unique(["form_information_id", "language_id"]);
		});
module.exports.down = (db) =>
	db.schema
		.dropTable("form_information")
		.dropTable("form_information_translations");

module.exports.configuration = { transaction: true };
