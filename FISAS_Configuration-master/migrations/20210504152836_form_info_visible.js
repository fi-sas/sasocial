module.exports.up = (db) =>
	db.schema
		.table("form_information", (table) => {
			table.boolean("visible");
		});

module.exports.down = (db) =>
	db.schema
		.table("form_information", (table) => {
			table.dropColumn("visible");
		});

module.exports.configuration = { transaction: true };
