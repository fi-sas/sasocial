module.exports.up = async (db) =>
	db.schema.alterTable("course", (table) => {
		table.string("external_ref").nullable();
	});
module.exports.down = async (db) =>
	db.schema.alterTable("course", (table) => {
		table.dropColumn("external_ref");
	});

module.exports.configuration = { transaction: true };
