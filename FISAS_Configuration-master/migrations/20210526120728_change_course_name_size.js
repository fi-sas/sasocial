module.exports.up = (db) =>
	db.schema
		.table("course", (table) => {
			table.string("name", 150).notNullable().alter();
		});

module.exports.down = (db) =>
	db.schema
		.table("course", (table) => {
			table.string("name", 120).notNullable().alter();
		});

module.exports.configuration = { transaction: true };


