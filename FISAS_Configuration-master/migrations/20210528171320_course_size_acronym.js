module.exports.up = async (db) =>
	db.schema.alterTable("course", (table) => {
		table.string("acronym", 32).alter();
	});
module.exports.down = async (db) =>
	db.schema.alterTable("course", (table) => {
		table.string("acronym").alter();
	});

module.exports.configuration = { transaction: true };
