module.exports.up = (db) =>
	db.schema
		.table("form_information", (table) => {
			table.integer("service_id").unsigned().references('id').inTable('service');
		});

module.exports.down = (db) =>
	db.schema
		.table("form_information", (table) => {
			table.dropColumn("service_id");
		});

module.exports.configuration = { transaction: true };
