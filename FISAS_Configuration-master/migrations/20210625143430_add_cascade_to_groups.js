exports.up = (knex) => {
	return knex.schema.alterTable("group_device", (table) => {
		table.dropForeign("group_id");
		table.dropForeign("device_id");

		table.foreign("group_id").references("id").inTable("group").onDelete("CASCADE");
		table.foreign("device_id").references("id").inTable("device").onDelete("CASCADE");
	}).alterTable("group_footer", (table) => {
		table.dropForeign("footer_id");
		table.foreign("footer_id").references("id").inTable("footer").onDelete("CASCADE");
		table.foreign("group_id").references("id").inTable("group").onDelete("CASCADE");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("group_device", (table) => {
		table.dropForeign("group_id");
		table.dropForeign("device_id");

		table.foreign("group_id").references("id").inTable("group").onDelete("NO ACTION");
		table.foreign("device_id").references("id").inTable("device").onDelete("NO ACTION");
	}).alterTable("group_footer", (table) => {
		table.dropForeign("footer_id");
		table.dropForeign("group_id");

		table.foreign("footer_id").references("id").inTable("footer").onDelete("NO ACTION");
	});
};
