exports.up = (knex) => {
	return knex.schema.alterTable("footer_translation", (table) => {
		table.dropForeign("footer_id");
		table.foreign("footer_id").references("id").inTable("footer").onDelete("CASCADE");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("footer_translation", (table) => {
		table.dropForeign("footer_id");
		table.foreign("footer_id").references("id").inTable("footer").onDelete("NO ACTION");
	});
};
