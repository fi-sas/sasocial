exports.up = (knex) => {
	return knex.schema.alterTable("contact_translation", (table) => {
		table.dropForeign("contact_id");
		table.foreign("contact_id").references("id").inTable("contact").onDelete("CASCADE");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("contact_translation", (table) => {
		table.dropForeign("contact_id");
		table.foreign("contact_id").references("id").inTable("contact").onDelete("NO ACTION");
	});
};
