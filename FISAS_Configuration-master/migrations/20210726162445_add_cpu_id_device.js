module.exports.up = async (db) =>
	db.schema
		.table("device", (table) => {
			table.string("CPUID", 64).nullable();
		});

module.exports.down = async db => db.schema
	.table("device", (table) => {
		table.dropColumn("CPUID");
	});


module.exports.configuration = { transaction: true };
