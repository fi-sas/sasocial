module.exports.up = async db => db.schema
	.createTable("holiday", (table) => {
		table.increments();
		table.date("date");
		table.timestamps(true, true);
	})
	.createTable("holiday_translation", (table) => {
		table.increments();
		table.integer("holiday_id").unsigned().references("id").inTable("holiday");
		table.integer("language_id").unsigned();
		table.string("name");
	})
	.createTable("holiday_organic_unit", (table) => {
		table.increments();
		table.integer("holiday_id").unsigned().references("id").inTable("holiday");
		table.integer("organic_unit_id").unsigned();
	});

module.exports.down = async db => db.schema.dropTable("holiday").dropTable("holiday_translation");
module.exports.configuration = { transaction: true };