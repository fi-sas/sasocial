module.exports.up = async (db) =>
	db.schema
		.table("service", (table) => {
			table.boolean("is_external_service").nullable().defaultTo(false);
            table.string("external_link").nullable();
		});

module.exports.down = async db => db.schema
	.table("service", (table) => {
		table.dropColumn("is_external_service");
        table.dropColumn("external_link");
	});


module.exports.configuration = { transaction: true };

