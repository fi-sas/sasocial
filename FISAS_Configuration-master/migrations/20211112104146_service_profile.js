module.exports.up = async (db) =>
	db.schema
		.table("service", (table) => {
			table.boolean("kiosk_availability").nullable().defaultTo(true);
            table.boolean("webpage_availability").nullable().defaultTo(true);
		})

        .createTable("service_profile", (table) => {
			table.increments();
			table.integer("service_id").notNullable().unsigned().references("id").inTable("service");
			table.integer("profile_id").notNullable().unsigned();
			table.unique(["service_id", "profile_id"]);
		});

module.exports.down = async db => db.schema
	.table("service", (table) => {
		table.dropColumn("kiosk_availability");
        table.dropColumn("webpage_availability");
	})
    .dropTable("service_profile");


module.exports.configuration = { transaction: true };
