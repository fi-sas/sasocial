module.exports.up = async (db) =>
	db.schema
		.table("service", (table) => {
			table.boolean("app_availability").nullable().defaultTo(true);
			table.string("icon").nullable();
		});

module.exports.down = async db => db.schema
	.table("service", (table) => {
        table.dropColumn("app_availability");		
		table.dropColumn("icon");
	});


module.exports.configuration = { transaction: true };