module.exports.up = async (db) =>
	db.schema
		.createTable("section_bo", function(table) {
			table.increments();
			table.string('name', 120).notNullable();
			table.integer("priority").notNullable().unsigned();
			table.datetime('updated_at').notNullable();
			table.datetime('created_at').notNullable();
		})
	
		.table("service", (table) => {
			table.integer("section_bo_id").unsigned().nullable().references("id").inTable("section_bo");
			table.integer("priority").unsigned();
		});

module.exports.down = async db => db.schema
	.table("service", (table) => {
        table.dropColumn("section_bo_id");		
		table.dropColumn("priority");
	})
    .dropTable("section_bo");

module.exports.configuration = { transaction: true };
