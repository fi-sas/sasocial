
exports.up = function (knex) {
	return knex("service").update({ section_bo_id: null, priority: null })
		.then(() => {
			return knex("section_bo").del();
		})
		.then(() => {
			return knex("section_bo").insert([
				{
					id: 1,
					name: "Serviços à comunidade",
					priority: 1,
					updated_at: new Date(),
					created_at: new Date(),
				},
				{
					id: 2,
					name: "Serviços internos",
					priority: 2,
					updated_at: new Date(),
					created_at: new Date(),
				},
				{
					id: 3,
					name: "Configurações",
					priority: 4,
					updated_at: new Date(),
					created_at: new Date(),
				},
			]);
		})
		.then(() => {
			return Promise.all([
				{ id: 2, section_bo_id: 1, priority: 1 },
				{ id: 1, section_bo_id: 1, priority: 2 },
				{ id: 15, section_bo_id: 1, priority: 3 },
				{ id: 17, section_bo_id: 1, priority: 4 },
				{ id: 24, section_bo_id: 1, priority: 5 },
				{ id: 21, section_bo_id: 1, priority: 6 },
				{ id: 30, section_bo_id: 1, priority: 7 },
				{ id: 22, section_bo_id: 1, priority: 8 },
				{ id: 29, section_bo_id: 1, priority: 9 },
				{ id: 3, section_bo_id: 1, priority: 10 },
				{ id: 16, section_bo_id: 1, priority: 11 },
				{ id: 13, section_bo_id: 1, priority: 12 },
				{ id: 28, section_bo_id: 1, priority: 13 },
				{ id: 18, section_bo_id: 2, priority: 14 },
				{ id: 19, section_bo_id: 2, priority: 15 },
				{ id: 11, section_bo_id: 2, priority: 16 },
				{ id: 25, section_bo_id: 2, priority: 17 },
				{ id: 26, section_bo_id: 2, priority: 18 },
				{ id: 20, section_bo_id: 2, priority: 19 },
				{ id: 7, section_bo_id: 2, priority: 20 },
				{ id: 5, section_bo_id: 2, priority: 21 },
				{ id: 12, section_bo_id: 3, priority: 1 },
				{ id: 8, section_bo_id: 3, priority: 2 },
				{ id: 6, section_bo_id: 3, priority: 3 },
				{ id: 9, section_bo_id: 3, priority: 4 },
				{ id: 23, section_bo_id: 3, priority: 5 },
				{ id: 14, section_bo_id: 3, priority: 6 },
				{ id: 10, section_bo_id: 3, priority: 7 },
				{ id: 4, section_bo_id: 3, priority: 8 },
			].map(service => {
				return knex("service").update({ section_bo_id: service.section_bo_id, priority: service.priority }).where("id", service.id);
			}));
		});
};

exports.down = function (knex) {
};
