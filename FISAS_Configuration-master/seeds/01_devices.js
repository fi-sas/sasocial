const uuidv4 = require("uuid").v4;

exports.seed = async (knex) => {
  const data = [
    {
      uuid: uuidv4(),
      name: 'MOBILE',
      type: 'MOBILE',
      orientation: 'PORTRAIT',
      active: true,
      secondsToInactivity: null,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      uuid: uuidv4(),
      name: 'BO',
      type: 'BO',
      orientation: 'LANDSCAPE',
      active: true,
      secondsToInactivity: null,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      uuid: uuidv4(),
      name: 'WEB',
      type: 'WEB',
      orientation: 'LANDSCAPE',
      active: true,
      secondsToInactivity: null,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      uuid: uuidv4(),
      name: 'POS',
      type: 'POS',
      orientation: 'PORTRAIT',
      active: true,
      secondsToInactivity: null,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ];

  return Promise.all(data.map(async (d) => {
    // Check if tax exist
    const rows = await knex('device').select().where('name', d.name);
    if (rows.length === 0) {
      await knex('device').insert(d);
    }
    return true;
  }));
};
