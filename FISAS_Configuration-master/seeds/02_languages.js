exports.seed = async (knex) => {
	const data = [
		{
			id: 3,
			name: 'Português',
			acronym: 'pt',
			order: 1,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: 4,
			name: 'Inglês',
			acronym: 'en',
			order: 2,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
	];

	return Promise.all(data.map(async (d) => {
		// Check if tax exist
		const rows = await knex('language').select().where('acronym', d.acronym);
		if (rows.length === 0) {
			await knex('language').insert(d);
		}
		return true;
	}));
};
