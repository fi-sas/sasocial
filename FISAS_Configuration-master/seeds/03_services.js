exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Alojamento",
					description: "Serviço de Alojamento",
				},
				en: {
					name: "Accomodation",
					description: "Accomodation service",
				},
			},
		},
		{
			id: 2,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Alimentação",
					description: "Serviço de Alimentação",
				},
				en: {
					name: "Food",
					description: "Food service",
				},
			},
		},
		{
			id: 3,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Mobilidade",
					description: "Serviço de Mobilidade",
				},
				en: {
					name: "Mobility",
					description: "Mobility service",
				},
			},
		},
		{
			id: 4,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Pagamentos/Carregamentos",
					description: "Serviço de Pagamentos/Carregamentos",
				},
				en: {
					name: "Payment/Charging",
					description: "Payment/Charging service",
				},
			},
		},
		{
			id: 5,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Serv. Académicos",
					description: "Serviço Académicos",
				},
				en: {
					name: "S.A.",
					description: "S.A service",
				},
			},
		},
		{
			id: 6,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Configuração",
					description: "Serviço de Configuração",
				},
				en: {
					name: "Configuration",
					description: "Configuration service",
				},
			},
		},
		{
			id: 7,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Saúde",
					description: "Serviço de Saúde",
				},
				en: {
					name: "Health",
					description: "Health Service",
				},
			},
		},
		{
			id: 8,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Autorização",
					description: "Serviço de Autorização",
				},
				en: {
					name: "Authorization",
					description: "Authorization service",
				},
			},
		},
		{
			id: 9,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Infraestrutura",
					description: "Serviço de Infraestrutura",
				},
				en: {
					name: "Infrastructure",
					description: "Infrastructure service",
				},
			},
		},
		{
			id: 10,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Medias",
					description: "Serviço de Medias",
				},
				en: {
					name: "Media",
					description: "Media service",
				},
			},
		},
		{
			id: 11,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Conta Corrente",
					description: "Conta Corrente",
				},
				en: {
					name: "Current Account",
					description: "Current Account service",
				},
			},
		},
		{
			id: 12,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Alertas e Notificações",
					description: "Serviço de Alertas e Notificações",
				},
				en: {
					name: "Notifications",
					description: "Notifications service",
				},
			},
		},
		{
			id: 13,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "U-Bike",
					description: "Serviço de U-Bike",
				},
				en: {
					name: "U-Bike",
					description: "U-Bike service",
				},
			},
		},
		{
			id: 14,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Logs",
					description: "Serviço de Logs",
				},
				en: {
					name: "Logs",
					description: "Logs service",
				},
			},
		},
		{
			id: 15,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Alojamento Privado",
					description: "Serviço de Alojamento Privado",
				},
				en: {
					name: "Private Accommodation",
					description: "Private Accommodation service",
				},
			},
		},
		{
			id: 16,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Packs",
					description: "Serviço de Packs",
				},
				en: {
					name: "Packs",
					description: "Packs service",
				},
			},
		},
		{
			id: 17,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Bolsa de apoio social",
					description: "Bolsa de apoio social",
				},
				en: {
					name: "Social Support Scholarship and Mentoring",
					description: "Social Support Scholarship and Mentoring service",
				},
			},
		},
		{
			id: 18,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Calendário",
					description: "Serviço de Calendário",
				},
				en: {
					name: "Calendar",
					description: "Calendar service",
				},
			},
		},
		{
			id: 19,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Comunicação",
					description: "Serviço de Comunicação",
				},
				en: {
					name: "Communication",
					description: "Communication service",
				},
			},
		},
		{
			id: 20,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Manutenção",
					description: "Serviço de Manutenção",
				},
				en: {
					name: "Maintenance",
					description: "Maintenance service",
				},
			},
		},
		{
			id: 21,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Fila de espera",
					description: "Serviço de filas de espera",
				},
				en: {
					name: "Queue",
					description: "Queue service",
				},
			},
		},
		{
			id: 22,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Geral",
					description: "Serviço geral",
				},
				en: {
					name: "General",
					description: "General service",
				},
			},
		},
		{
			id: 23,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Relatorios",
					description: "Serviço relatórios",
				},
				en: {
					name: "Reports",
					description: "Reports service",
				},
			},
		},
		{
			id: 24,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Desporto",
					description: "Serviço desporto",
				},
				en: {
					name: "Sport",
					description: "Sport service",
				},
			},
		},
		{
			id: 25,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Gamificação",
					description: "Serviço de Gamificação",
				},
				en: {
					name: "Gamification",
					description: "Gamification service",
				},
			},
		},
		{
			id: 26,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Gestão de energia",
					description: "Serviço de Gestão de energia",
				},
				en: {
					name: "Energy consuption",
					description: "Energy consuption service",
				},
			},
		},
		{
			id: 28,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Voluntariado",
					description: "Serviço de voluntariado",
				},
				en: {
					name: "Volunteering",
					description: "Volunteering service",
				},
			},
		},
		{
			id: 29,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Mentoria",
					description: "Serviço de mentoria",
				},
				en: {
					name: "Mentoring",
					description: "Mentoring service",
				},
			},
		},
		{
			id: 30,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Fundo de emergência",
					description: "Serviço de fundo de emergência",
				},
				en: {
					name: "Emergency fund",
					description: "Emergency fund service",
				},
			},
		},
		{
			id: 31,
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
			translation: {
				pt: {
					name: "Sistema",
					description: "Administração sistema SASocial",
				},
				en: {
					name: "System",
					description: "Administration SASocial system",
				},
			},
		},
	];

	let languages = await knex("language").select().where("acronym", "pt");
	const pt = languages.length ? languages[0].id : null;

	languages = await knex("language").select().where("acronym", "en");
	const en = languages.length ? languages[0].id : null;

	return Promise.all(
		data.map(async (d) => {
			// Check if service exist
			const rows = await knex("service").select().where("id", d.id);

			if (rows.length === 0) {
				// no matching records found
				const serviceData = Object.assign({}, d);
				delete serviceData.translation;
				await knex("service").insert(serviceData);
			}

			// Check if PT translation exist
			let translationData = null;
			let translationRows = await knex("service_translation")
				.select()
				.where({ service_id: d.id, language_id: pt });

			if (translationRows.length === 0) {
				// no matching records found
				translationData = Object.assign({}, d.translation.pt);
				translationData.service_id = d.id;
				translationData.language_id = pt;
				await knex("service_translation").insert(translationData);
			}

			// Check if PT translation exist
			translationRows = await knex("service_translation")
				.select()
				.where({ service_id: d.id, language_id: en });

			if (translationRows.length === 0) {
				// no matching records found
				translationData = Object.assign({}, d.translation.en);
				translationData.service_id = d.id;
				translationData.language_id = en;
				await knex("service_translation").insert(translationData);
			}

			return true;
		}),
	);
};
