exports.seed = async (knex) => {
  const data = [
    {
      id: 1,
      name: 'IVA 23%',
      description: 'Taxa de IVA a 23% sobre o valor total.',
      tax_value: 0.23,
      active: true,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      id: 2,
      name: 'IVA 0%',
      description: 'Taxa para artigos isentos de IVA',
      tax_value: 0,
      active: true,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ];

  return Promise.all(data.map(async (d) => {
    // Check if tax exist
    const rows = await knex('tax').select().where('id', d.id);
    if (rows.length === 0) {
      await knex('tax').insert(d);
    }
    return true;
  }));
};
