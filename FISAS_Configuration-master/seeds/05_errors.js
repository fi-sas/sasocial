exports.seed = async (knex) => {
	const data = [
		{
			type: "SERVICE_NOT_FOUND",
			message: "The service requested was not found or isn't available",
			translations:
				[
					{ message: "O serviço solicitado não foi encontrado ou não está disponível",language_id: 3, type: "SERVICE_NOT_FOUND" },
					{ message: "The service requested was not found or isn't available", language_id: 4, type: "SERVICE_NOT_FOUND" }
				]
		},
		{
			type: "PASSWORD_OR_PIN_REQUIRED",
			message: "The password or pin is requiered for procced the login",
			translations:
				[
					{ message: "É necessário senha ou pin para prosseguir com a autenticação",language_id: 3, type: "PASSWORD_OR_PIN_REQUIRED" },
					{ message: "Password or pin is required for proceed the authentication", language_id: 4, type: "PASSWORD_OR_PIN_REQUIRED" }
				]
		},
		{
			type: "ERR_TOKEN_EXPIRED",
			message: "The token was expired",
			translations:
				[
					{ message: "O token expirou",language_id: 3, type: "ERR_TOKEN_EXPIRED" },
					{ message: "The token was expired", language_id: 4, type: "ERR_TOKEN_EXPIRED" }
				]
		},
		{
			type: "NOT_FOUND",
			message: "The entity request has not found",
			translations:
				[
					{ message: "A solicitação da entidade não foi encontrada",language_id: 3, type: "NOT_FOUND" },
					{ message: "The entity request has not found", language_id: 4, type: "NOT_FOUND" }
				]
		},
		{
			type: "ERR_INVALID_CREDENTIALS",
			message: "User inserted invalid credentials on login",
			translations:
				[
					{ message: "As credências não estão válidas",language_id: 3, type: "ERR_INVALID_CREDENTIALS" },
					{ message: "User inserted invalid credentials on login", language_id: 4, type: "ERR_INVALID_CREDENTIALS" }
				]
		},
		{
			type: "SERVICE_NOT_AVAILABLE",
			message: "The service requested is not available",
			translations:
				[
					{ message: "O serviço solicitado não se encontra disponível",language_id: 3, type: "SERVICE_NOT_AVAILABLE" },
					{ message: "The requested service is not available", language_id: 4, type: "SERVICE_NOT_AVAILABLE" }
				]
		},
		{
			type: "ERR_INVALID_TOKEN",
			message: "The token is invalid",
			translations:
				[
					{ message: "O token não é válido",language_id: 3, type: "ERR_INVALID_TOKEN" },
					{ message: "The token is invalid", language_id: 4, type: "ERR_INVALID_TOKEN" }
				]
		},
		{
			type: "REQUEST_TIMEOUT",
			message: "The service is taking too long to respond",
			translations:
				[
					{ message: "O serviço está a demorar a responder, por favor tente mais tarde",language_id: 3, type: "REQUEST_TIMEOUT" },
					{ message: "The service is taking a while to respond, please try again later", language_id: 4, type: "REQUEST_TIMEOUT" }
				]
		},
		{
			type: "ERR_NO_TOKEN",
			message: "No token sended in the header",
			translations:
				[
					{ message: "A sua sessão é inválida",language_id: 3, type: "ERR_NO_TOKEN" },
					{ message: "Your session is invalid", language_id: 4, type: "ERR_NO_TOKEN" }
				]
		},
		{
			type: "SERVICE_UNAVAILABLE",
			message: "The service requested is not available",
			translations:
				[
					{ message: "O serviço solicitado não está disponível",language_id: 3, type: "SERVICE_UNAVAILABLE" },
					{ message: "The service requested is not available", language_id: 4, type: "SERVICE_UNAVAILABLE" }
				]
		},
		{
			type: "ERR_NO_ACCESS_BACKOFFICE",
			message: "Your user has not access to the backoffice",
			translations:
				[
					{ message: "Este utilizador não tem acesso ao backoffice",language_id: 3, type: "ERR_NO_ACCESS_BACKOFFICE" },
					{ message: "Your user has not access to the backoffice", language_id: 4, type: "ERR_NO_ACCESS_BACKOFFICE" }
				]
		},
		{
			type: "FILES_NOT_FOUND_ERROR",
			message: "The file requested has not found",
			translations:
				[
					{ message: "O documento não foi encontrado",language_id: 3, type: "FILES_NOT_FOUND_ERROR" },
					{ message: "The file requested has not found", language_id: 4, type: "FILES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "DEVICES_NOT_FOUND_ERROR",
			message: "The requested device has not found",
			translations:
				[
					{ message: "O dispositivo pedido não foi encontrado",language_id: 3, type: "DEVICES_NOT_FOUND_ERROR" },
					{ message: "The request device has not found", language_id: 4, type: "DEVICES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "DEVICE_NOT_ACTIVE",
			message: "The device is not active",
			translations:
				[
					{ message: "O dispositivo não está ativo",language_id: 3, type: "DEVICE_NOT_ACTIVE" },
					{ message: "The device is not active", language_id: 4, type: "DEVICE_NOT_ACTIVE" }
				]
		},
		{
			type: "NO_RIGHTS",
			message: "Your user has not access to the requested service",
			translations:
				[
					{ message: "Este utilizador não tem acesso ao serviço solicitado",language_id: 3, type: "NO_RIGHTS" },
					{ message: "Your user has not access to the requested service", language_id: 4, type: "NO_RIGHTS" }
				]
		},
		{
			type: "ERR_INVALID_SORT_FIELD",
			message: "It is not possible to sort by this sort field",
			translations:
				[
					{ message: "Não é possível classificar por este campo",language_id: 3, type: "ERR_INVALID_SORT_FIELD" },
					{ message: "It is not possible to sort by this sort field", language_id: 4, type: "ERR_INVALID_SORT_FIELD" }
				]
		},
		{
			type: "SCHEDULE",
			message: "Schedule is empty",
			translations:
				[
					{ message: "É obrigatório preencher os horários",language_id: 3, type: "SCHEDULE" },
					{ message: "Schedule is empty", language_id: 4, type: "SCHEDULE" }
				]
		},
		{
			type: "SCHOOLS_NOT_FOUND_ERROR",
			message: "Entity schools not found",
			translations:
				[
					{ message: "Escola não encontrada",language_id: 3, type: "SCHOOLS_NOT_FOUND_ERROR" },
					{ message: "School not found", language_id: 4, type: "SCHOOLS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "COURSE-DEGREES_NOT_FOUND_ERROR",
			message: "Entity course-degrees not found",
			translations:
				[
					{ message: "Grau não encontrado",language_id: 3, type: "COURSE-DEGREES_NOT_FOUND_ERROR" },
					{ message: "Course degrees not found", language_id: 4, type: "COURSE-DEGREES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "USERS_NOT_FOUND_ERROR",
			message: "Entity users not found",
			translations:
				[
					{ message: "Utilizador não encontrado",language_id: 3, type: "USERS_NOT_FOUND_ERROR" },
					{ message: "User not found", language_id: 4, type: "USERS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ALREADY_EXIST_DESK_OPERATOR_WITH_USER_ID",
			message: "Already exist desk operator with user Id",
			translations:
				[
					{ message: "Já existe um operador de balcão com esse id de utilizador",language_id: 3, type: "ALREADY_EXIST_DESK_OPERATOR_WITH_USER_ID" },
					{ message: "Already exist desk operator with user Id", language_id: 4, type: "ALREADY_EXIST_DESK_OPERATOR_WITH_USER_ID" }
				]
		},
		{
			type: "SERVICE NOT FOUND_NOT_FOUND_ERROR",
			message: "Entity Service with not found",
			translations:
				[
					{ message: "Serviço não encontrado",language_id: 3, type: "SERVICE NOT FOUND_NOT_FOUND_ERROR" },
					{ message: "Service not found", language_id: 4, type: "SERVICE NOT FOUND_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SUBJECT_ALREADY_EXISTS",
			message: "Subject with code already exists",
			translations:
				[
					{ message: "Já existe uma fila com o código enviado",language_id: 3, type: "SUBJECT_ALREADY_EXISTS" },
					{ message: "Already exist a subject with the code sended", language_id: 4, type: "SUBJECT_ALREADY_EXISTS" }
				]
		},
		{
			type: "SUBJECT_INVALID_HOURS",
			message: "Subject hour isn't valid",
			translations:
				[
					{ message: "A hora não é válida",language_id: 3, type: "SUBJECT_INVALID_HOURS" },
					{ message: "The time is not valid", language_id: 4, type: "SUBJECT_INVALID_HOURS" }
				]
		},
		{
			type: "INVALID_POST_STATUS",
			message: "This Post is already 'REJECTED', its status cannot be changed",
			translations:
				[
					{ message: "Já está rejeitado, não pode alterar novamente o estado",language_id: 3, type: "INVALID_POST_STATUS" },
					{ message: "This Post is already 'REJECTED', its status cannot be changed", language_id: 4, type: "INVALID_POST_STATUS" }
				]
		},
		{
			type: "QUEUE_FULL",
			message: "Queue is full. Request is rejected",
			translations:
				[
					{ message: "O pedido foi rejeitado. O serviço encontra-se temporariamente ocupado",language_id: 3, type: "QUEUE_FULL" },
					{ message: "The request was rejected, the service is temporarily busy", language_id: 4, type: "QUEUE_FULL" }
				]
		},
		{
			type: "REQUEST_REJECTED",
			message: "Request is rejected when call 'queue.tickets_history.getPercentageTickets' action on 'fisas_queue_services-26' node",
			translations:
				[
					{ message: "O pedido foi rejeitado, tente novamente mais tarde",language_id: 3, type: "REQUEST_REJECTED" },
					{ message: "The request was rejected, please try again later", language_id: 4, type: "REQUEST_REJECTED" }
				]
		},
		{
			type: "TICKET_INVALID_ORIGIN",
			message: "Invalid origin",
			translations:
				[
					{ message: "Origem inválida",language_id: 3, type: "TICKET_INVALID_ORIGIN" },
					{ message: "Invalid origin", language_id: 4, type: "TICKET_INVALID_ORIGIN" }
				]
		},
		{
			type: "LANGUAGE_NOT_FOUND_ERROR",
			message: "Entity LANGUAGE not found",
			translations:
				[
					{ message: "Linguagem não encontrada",language_id: 3, type: "LANGUAGE_NOT_FOUND_ERROR" },
					{ message: "Language not found", language_id: 4, type: "LANGUAGE_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "entity.parse.failed",
			message: "Unexpected token ​ in JSON at position 1",
			translations:
				[
					{ message: "O corpo do pedido contém um formato inválido",language_id: 3, type: "entity.parse.failed" },
					{ message: "Order body contains an invalid format", language_id: 4, type: "entity.parse.failed" }
				]
		},
		{
			type: "DESK_OPERATOR_NOT_ACTIVE",
			message: "Desk Operator not active",
			translations:
				[
					{ message: "O operador de balcão não está ativo",language_id: 3, type: "DESK_OPERATOR_NOT_ACTIVE" },
					{ message: "Desk Operator not active", language_id: 4, type: "DESK_OPERATOR_NOT_ACTIVE" }
				]
		},
		{
			type: "DESK_NOT_FOUND_ERROR",
			message: "Entity desk not found",
			translations:
				[
					{ message: "Balcão não encontrado",language_id: 3, type: "DESK_NOT_FOUND_ERROR" },
					{ message: "Desk not found", language_id: 4, type: "DESK_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "NEED_SEND_FISCAL_ENTITY_ID",
			message: "Need send fiscal entity Id",
			translations:
				[
					{ message: "É necessário enviar o id da entidade fiscal",language_id: 3, type: "NEED_SEND_FISCAL_ENTITY_ID" },
					{ message: "Need send fiscal entity Id", language_id: 4, type: "NEED_SEND_FISCAL_ENTITY_ID" }
				]
		},
		{
			type: "SERVICES_NOT_FOUND_ERROR",
			message: "Entity services not found",
			translations:
				[
					{ message: "Serviço não encontrado",language_id: 3, type: "SERVICES_NOT_FOUND_ERROR" },
					{ message: "Service not found", language_id: 4, type: "SERVICES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "NO_ENTITY_ID",
			message: "You need send Entity Id",
			translations:
				[
					{ message: "É necessário enviar o id da entidade",language_id: 3, type: "NO_ENTITY_ID" },
					{ message: "You need send Entity Id", language_id: 4, type: "NO_ENTITY_ID" }
				]
		},
		{
			type: "YOU_NOT_A_MANAGER_THIS_FISCAL_ENTITY",
			message: "You not a manager this fiscal entity",
			translations:
				[
					{ message: "Este utilizador não é gerente desta entidade fiscal",language_id: 3, type: "YOU_NOT_A_MANAGER_THIS_FISCAL_ENTITY" },
					{ message: "This user is not the manager of this fiscal entity", language_id: 4, type: "YOU_NOT_A_MANAGER_THIS_FISCAL_ENTITY" }
				]
		},
		{
			type: "RESIDENCES_NOT_FOUND_ERROR",
			message: "Entity residences not found",
			translations:
				[
					{ message: "Residência não encontrada",language_id: 3, type: "RESIDENCES_NOT_FOUND_ERROR" },
					{ message: "Residence not found", language_id: 4, type: "RESIDENCES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "EMAIL_OR_USERNAME_REQUIRED",
			message: "'email' or 'user_name' are required if an pin/password is provided",
			translations:
				[
					{ message: "Caso tenha preenchido o pin/senha, é obrigatório preencher o e-mail ou nome",language_id: 3, type: "EMAIL_OR_USERNAME_REQUIRED" },
					{ message: "'email' or 'user_name' are required if an pin/password is provided", language_id: 4, type: "EMAIL_OR_USERNAME_REQUIRED" }
				]
		},
		{
			type: "SUBJECT_NOT_FOUND_ERROR",
			message: "Entity subject not found",
			translations:
				[
					{ message: "Fila não encontrada",language_id: 3, type: "SUBJECT_NOT_FOUND_ERROR" },
					{ message: "Subject not found", language_id: 4, type: "SUBJECT_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SUBJECT_CLOSED",
			message: "Subject closed",
			translations:
				[
					{ message: "Fila encerrada",language_id: 3, type: "SUBJECT_CLOSED" },
					{ message: "Subject closed", language_id: 4, type: "SUBJECT_CLOSED" }
				]
		},
		{
			type: "USER_ALREADY_HAVE_TICKET",
			message: "Use only can have one ticket per subject",
			translations:
				[
					{ message: "Só é possível tirar uma senha por fila",language_id: 3, type: "USER_ALREADY_HAVE_TICKET" },
					{ message: "Use only can have one ticket per subject", language_id: 4, type: "USER_ALREADY_HAVE_TICKET" }
				]
		},
		{
			type: "SUBJECT_NOT_ACTIVE",
			message: "Subject with id 3 isn't active",
			translations:
				[
					{ message: "A fila não está ativa",language_id: 3, type: "SUBJECT_NOT_ACTIVE" },
					{ message: "Subject isn't active", language_id: 4, type: "SUBJECT_NOT_ACTIVE" }
				]
		},
		{
			type: "TICKET_NOT_FOUND_ERROR",
			message: "Entity ticket not found",
			translations:
				[
					{ message: "Senha não encontrada",language_id: 3, type: "TICKET_NOT_FOUND_ERROR" },
					{ message: "Ticket not found", language_id: 4, type: "TICKET_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "TICKET_AFTER_SCHEDULE",
			message: "The queue is closed. Please come back tomorrow",
			translations:
				[
					{ message: "A fila está fechada. Por favor, volte amanhã",language_id: 3, type: "TICKET_AFTER_SCHEDULE" },
					{ message: "The queue is closed. Please come back tomorrow", language_id: 4, type: "TICKET_AFTER_SCHEDULE" }
				]
		},
		{
			type: "UNAUTHORIZED_TICKET_CANCEL",
			message: "Unauthorized operation",
			translations:
				[
					{ message: "Operação não autorizada",language_id: 3, type: "UNAUTHORIZED_TICKET_CANCEL" },
					{ message: "Unauthorized operation", language_id: 4, type: "UNAUTHORIZED_TICKET_CANCEL" }
				]
		},
		{
			type: "AREAS_NOT_FOUND_ERROR",
			message: "Entity areas not found",
			translations:
				[
					{ message: "Área não encontrada",language_id: 3, type: "AREAS_NOT_FOUND_ERROR" },
					{ message: "Area not found", language_id: 4, type: "AREAS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SERVICE_NOT_CORRESPOND_THE_ROOM",
			message: "The service not correspond the room",
			translations:
				[
					{ message: "O serviço não corresponde ao quarto",language_id: 3, type: "SERVICE_NOT_CORRESPOND_THE_ROOM" },
					{ message: "The service not correspond the room", language_id: 4, type: "SERVICE_NOT_CORRESPOND_THE_ROOM" }
				]
		},
		{
			type: "OPERATOR_ALREADY_BUSY",
			message: "Already have one attendence open",
			translations:
				[
					{ message: "Já está em atendimento",language_id: 3, type: "OPERATOR_ALREADY_BUSY" },
					{ message: "Already have one attendence open", language_id: 4, type: "OPERATOR_ALREADY_BUSY" }
				]
		},
		{
			type: "TICKET_HISTORY_NOT_FOUND_ERROR",
			message: "Entity ticket_history not found",
			translations:
				[
					{ message: "Histórico de senhas não encontrado",language_id: 3, type: "TICKET_HISTORY_NOT_FOUND_ERROR" },
					{ message: "Ticket history not found", language_id: 4, type: "TICKET_HISTORY_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "FLOOR_NOT_CORRESPOND_THE_WING",
			message: "The floor not correspond the wing",
			translations:
				[
					{ message: "O piso não corresponde à ala",language_id: 3, type: "FLOOR_NOT_CORRESPOND_THE_WING" },
					{ message: "The floor not correspond the wing", language_id: 4, type: "FLOOR_NOT_CORRESPOND_THE_WING" }
				]
		},
		{
			type: "DESK_IS_ASSOCIATED_ANOTHER_SERVICE",
			message: "This desk is associated with another service actives",
			translations:
				[
					{ message: "Este balcão está associado a outro serviço",language_id: 3, type: "DESK_IS_ASSOCIATED_ANOTHER_SERVICE" },
					{ message: "This desk is associated with another service actives", language_id: 4, type: "DESK_IS_ASSOCIATED_ANOTHER_SERVICE" }
				]
		},
		{
			type: "WING_NOT_CORRESPOND_THE_BUILDING",
			message: "The wing not correspond the building",
			translations:
				[
					{ message: "A ala não corresponde ao edifício",language_id: 3, type: "WING_NOT_CORRESPOND_THE_BUILDING" },
					{ message: "The wing not correspond the building", language_id: 4, type: "WING_NOT_CORRESPOND_THE_BUILDING" }
				]
		},
		{
			type: "TYPOLOGIES_NOT_FOUND_ERROR",
			message: "Entity typologies not found",
			translations:
				[
					{ message: "Tipologia não encontrada",language_id: 3, type: "TYPOLOGIES_NOT_FOUND_ERROR" },
					{ message: "Typology not found", language_id: 4, type: "TYPOLOGIES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "OWNER_ID_REQUIRED",
			message: "'owner_id' is required!",
			translations:
				[
					{ message: "O id do proprietário é obrigatório",language_id: 3, type: "OWNER_ID_REQUIRED" },
					{ message: "Owner id is required", language_id: 4, type: "OWNER_ID_REQUIRED" }
				]
		},
		{
			type: "NOTIFICATIONS.CREATE_ALERT.ALERT-TYPES_NOT_FOUND_ERROR",
			message: "Entity notifications.create_alert.alert-types not found",
			translations:
				[
					{ message: "Tipo de alerta não encontrado",language_id: 3, type: "NOTIFICATIONS.CREATE_ALERT.ALERT-TYPES_NOT_FOUND_ERROR" },
					{ message: "Alert type not found", language_id: 4, type: "NOTIFICATIONS.CREATE_ALERT.ALERT-TYPES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "OWNERS_NOT_FOUND_ERROR",
			message: "Entity owners not found",
			translations:
				[
					{ message: "Proprietário não encontrado",language_id: 3, type: "OWNERS_NOT_FOUND_ERROR" },
					{ message: "Owner not found", language_id: 4, type: "OWNERS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "LISTING_NOT_PENDING",
			message: "Listing not in a Pending status",
			translations:
				[
					{ message: "Listagem sem estado pendente",language_id: 3, type: "LISTING_NOT_PENDING" },
					{ message: "Listing not in a Pending status", language_id: 4, type: "LISTING_NOT_PENDING" }
				]
		},
		{
			type: "ROOMS_INFO_IS_REQUIRED",
			message: "For 'PRIVATE_ROOM' or 'SHARED_ROOM' the rooms info is required",
			translations:
				[
					{ message: "É obrigatório referir se pretende quarto privado ou quarto partilhado",language_id: 3, type: "ROOMS_INFO_IS_REQUIRED" },
					{ message: "For 'PRIVATE_ROOM' or 'SHARED_ROOM' the rooms info is required", language_id: 4, type: "ROOMS_INFO_IS_REQUIRED" }
				]
		},
		{
			type: "EMPTY_TRANSLATIONS",
			message: "'translations' is empty",
			translations:
				[
					{ message: "As traduções são obrigatórias",language_id: 3, type: "EMPTY_TRANSLATIONS" },
					{ message: "Translations is empty", language_id: 4, type: "EMPTY_TRANSLATIONS" }
				]
		},
		{
			type: "FILE_NOT_FOUND_ERROR",
			message: "Entity file not found",
			translations:
				[
					{ message: "Ficheiro não encontrado",language_id: 3, type: "FILE_NOT_FOUND_ERROR" },
					{ message: "File not found", language_id: 4, type: "FILE_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "IBAN_INVALID",
			message: "IBAN is invalid",
			translations:
				[
					{ message: "O IBAN não é válido",language_id: 3, type: "IBAN_INVALID" },
					{ message: "IBAN is invalid", language_id: 4, type: "IBAN_INVALID" }
				]
		},
		{
			type: "CART_NO_PAYMENT_METHOD_DEFINED",
			message: "No Payment Method defined",
			translations:
				[
					{ message: "Não existe nenhum método de pagamento definido",language_id: 3, type: "CART_NO_PAYMENT_METHOD_DEFINED" },
					{ message: "No Payment Method defined", language_id: 4, type: "CART_NO_PAYMENT_METHOD_DEFINED" }
				]
		},
		{
			type: "ACCOUNTS_NOT_FOUND_ERROR",
			message: "Entity accounts not found",
			translations:
				[
					{ message: "Conta corrente não encontrada",language_id: 3, type: "ACCOUNTS_NOT_FOUND_ERROR" },
					{ message: "Current account not found", language_id: 4, type: "ACCOUNTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "MOVEMENTS_NOT_FOUND_ERROR",
			message: "Entity movements not found",
			translations:
				[
					{ message: "Movimento não encontrado",language_id: 3, type: "MOVEMENTS_NOT_FOUND_ERROR" },
					{ message: "Movement not found", language_id: 4, type: "MOVEMENTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_BIKE_ID_REQUIRED",
			message: "bike_id field is required",
			translations:
				[
					{ message: "O id da bicileta é obrigatório",language_id: 3, type: "UBIKE_BIKE_ID_REQUIRED" },
					{ message: "Bike id field is required", language_id: 4, type: "UBIKE_BIKE_ID_REQUIRED" }
				]
		},
		{
			type: "BIKES_NOT_FOUND_ERROR",
			message: "Entity bikes with not found",
			translations:
				[
					{ message: "Bicicleta não encontrada",language_id: 3, type: "BIKES_NOT_FOUND_ERROR" },
					{ message: "Bike not found", language_id: 4, type: "BIKES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_TRANSLATIONS_NOT_FOUND",
			message: "The 'translations' field is required and must be an array",
			translations:
				[
					{ message: "As traduções são obrigatórias",language_id: 3, type: "SOCIAL_SCHOLARSHIP_TRANSLATIONS_NOT_FOUND" },
					{ message: "The translations field is required and must be an array", language_id: 4, type: "SOCIAL_SCHOLARSHIP_TRANSLATIONS_NOT_FOUND" }
				]
		},
		{
			type: "APPLICATIONS_NOT_FOUND_ERROR",
			message: "Entity applications not found",
			translations:
				[
					{ message: "Candidatura não encontrada",language_id: 3, type: "APPLICATIONS_NOT_FOUND_ERROR" },
					{ message: "Application not found", language_id: 4, type: "APPLICATIONS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_STATUS_ERROR",
			message: "Impossible change the state",
			translations:
				[
					{ message: "Não é possível mudar o estado",language_id: 3, type: "UBIKE_APPLICATION_STATUS_ERROR" },
					{ message: "Impossible change the state", language_id: 4, type: "UBIKE_APPLICATION_STATUS_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_IN_PROGRESS",
			message: "The user already have one application in progress",
			translations:
				[
					{ message: "O utilizador já tem uma candidatura em processo",language_id: 3, type: "UBIKE_APPLICATION_IN_PROGRESS" },
					{ message: "The user already have one application in progress", language_id: 4, type: "UBIKE_APPLICATION_IN_PROGRESS" }
				]
		},
		{
			type: "LANGUAGES_NOT_FOUND_ERROR",
			message: "Entity languages not found",
			translations:
				[
					{ message: "Linguagem não encontrada",language_id: 3, type: "LANGUAGES_NOT_FOUND_ERROR" },
					{ message: "Languages not found", language_id: 4, type: "LANGUAGES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "BALANCES_ONLY_BACKOFFICE_CAN_SEARCH_USER_ID",
			message: "Only backoffice can search for user_id",
			translations:
				[
					{ message: "Apenas o backoffice pode pesquisar por utilizador",language_id: 3, type: "BALANCES_ONLY_BACKOFFICE_CAN_SEARCH_USER_ID" },
					{ message: "Only the backoffice can search by user", language_id: 4, type: "BALANCES_ONLY_BACKOFFICE_CAN_SEARCH_USER_ID" }
				]
		},
		{
			type: "ATTENDANCES_NOT_FOUND_ERROR",
			message: "Entity attendances not found",
			translations:
				[
					{ message: "Presença não encontrada",language_id: 3, type: "ATTENDANCES_NOT_FOUND_ERROR" },
					{ message: "Attendances not found", language_id: 4, type: "ATTENDANCES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ABSENCE_REASON_NOT_FOUND_ERROR",
			message: "Entity absence_reason not found",
			translations:
				[
					{ message: "Motivo de ausência não encontrada",language_id: 3, type: "ABSENCE_REASON_NOT_FOUND_ERROR" },
					{ message: "Absence reason not found", language_id: 4, type: "ABSENCE_REASON_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_INTERESTED_ALREADY_EXIST",
			message: "The Experience User Interested already exists",
			translations:
				[
					{ message: "A experiência já existe",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_INTERESTED_ALREADY_EXIST" },
					{ message: "The Experience User Interested already exists", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_INTERESTED_ALREADY_EXIST" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR" },
					{ message: "Unauthorized application status change", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR" }
				]
		},
		{
			type: "PRIVACY_POLICIES_NOT_FOUND_ERROR",
			message: "Entity privacy_policies not found",
			translations:
				[
					{ message: "Política de privacidade não encontrada",language_id: 3, type: "PRIVACY_POLICIES_NOT_FOUND_ERROR" },
					{ message: "Privacy policies not found", language_id: 4, type: "PRIVACY_POLICIES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "TERMS_NOT_FOUND_ERROR",
			message: "Entity terms not found",
			translations:
				[
					{ message: "Termo não encontrado",language_id: 3, type: "TERMS_NOT_FOUND_ERROR" },
					{ message: "Term  not found", language_id: 4, type: "TERMS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "INFORMATIONS_NOT_FOUND_ERROR",
			message: "Entity informations not found",
			translations:
				[
					{ message: "Informação não encontrada",language_id: 3, type: "INFORMATIONS_NOT_FOUND_ERROR" },
					{ message: "Information not found", language_id: 4, type: "INFORMATIONS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "COURSES_NOT_FOUND_ERROR",
			message: "Entity courses not found",
			translations:
				[
					{ message: "Curso não encontrado",language_id: 3, type: "COURSES_NOT_FOUND_ERROR" },
					{ message: "Course not found", language_id: 4, type: "COURSES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "EXPERIENCES_NOT_FOUND_ERROR",
			message: "Entity experiences not found",
			translations:
				[
					{ message: "Experiência não encontrada",language_id: 3, type: "EXPERIENCES_NOT_FOUND_ERROR" },
					{ message: "Experience not found", language_id: 4, type: "EXPERIENCES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ERR_NO_REFRESH_TOKEN",
			message: "No token refresh founded",
			translations:
				[
					{ message: "Não foi encontrada nenhuma atualização do token",language_id: 3, type: "ERR_NO_REFRESH_TOKEN" },
					{ message: "No token refresh founded", language_id: 4, type: "ERR_NO_REFRESH_TOKEN" }
				]
		},
		{
			type: "APPLICATION-FORMS_NOT_FOUND_ERROR",
			message: "Entity application-forms not found",
			translations:
				[
					{ message: "Formulário não encontrado",language_id: 3, type: "APPLICATION-FORMS_NOT_FOUND_ERROR" },
					{ message: "Form not found", language_id: 4, type: "APPLICATION-FORMS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ORIGIN_NOT_ALLOWED",
			message: "Forbidden",
			translations:
				[
					{ message: "Proibida",language_id: 3, type: "ORIGIN_NOT_ALLOWED" },
					{ message: "Forbidden", language_id: 4, type: "ORIGIN_NOT_ALLOWED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE",
			message: "Experience already finish or not started yet",
			translations:
				[
					{ message: "A experiência já terminou ou ainda não começou",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE" },
					{ message: "Experience already finish or not started yet", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE" }
				]
		},
		{
			type: "SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado",language_id: 3, type: "SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR" },
					{ message: "Unauthorized application status change", language_id: 4, type: "SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR" }
				]
		},
		{
			type: "INSUFFICIENT_FUNDS_FOR_REFUND",
			message: "Not enough funds to make this refund",
			translations:
				[
					{ message: "Fundos insuficientes para efetar o reembolso",language_id: 3, type: "INSUFFICIENT_FUNDS_FOR_REFUND" },
					{ message: "Not enough funds to make this refund", language_id: 4, type: "INSUFFICIENT_FUNDS_FOR_REFUND" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_ALREADY_EXIST",
			message: "User already have an active application this academic year",
			translations:
				[
					{ message: "O utilizador já tem uma candidatura ativa neste ano letivo",language_id: 3, type: "ACCOMMODATION_APPLICATION_ALREADY_EXIST" },
					{ message: "User already have an active application this academic year", language_id: 4, type: "ACCOMMODATION_APPLICATION_ALREADY_EXIST" }
				]
		},
		{
			type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_WITHDRAWAL",
			message: "Already exist a active withdrawal",
			translations:
				[
					{ message: "Já existe uma desistência ativa",language_id: 3, type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_WITHDRAWAL" },
					{ message: "Already exist a active withdrawal", language_id: 4, type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_WITHDRAWAL" }
				]
		},
		{
			type: "CHARGE_ONLY_BACKOFFICE_CAN_CHARGE_USER_ID",
			message: "Only backoffice can charge by user_id",
			translations:
				[
					{ message: "Apenas o backoffice pode cobrar ao utilizador",language_id: 3, type: "CHARGE_ONLY_BACKOFFICE_CAN_CHARGE_USER_ID" },
					{ message: "Only backoffice can charge by user id", language_id: 4, type: "CHARGE_ONLY_BACKOFFICE_CAN_CHARGE_USER_ID" }
				]
		},
		{
			type: "PRODUCT_CODE_ALREADY_EXIST",
			message: "The product code already exist",
			translations:
				[
					{ message: "O código do produto já existe",language_id: 3, type: "PRODUCT_CODE_ALREADY_EXIST" },
					{ message: "The product code already exist", language_id: 4, type: "PRODUCT_CODE_ALREADY_EXIST" }
				]
		},
		{
			type: "UNITS_NOT_FOUND_ERROR",
			message: "Entity units not found",
			translations:
				[
					{ message: "Unidade não encontrada",language_id: 3, type: "UNITS_NOT_FOUND_ERROR" },
					{ message: "Unit not found", language_id: 4, type: "UNITS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "NUTRIENTS_NOT_FOUND_ERROR",
			message: "Entity nutrients not found",
			translations:
				[
					{ message: "Nutriente não encontrado",language_id: 3, type: "NUTRIENTS_NOT_FOUND_ERROR" },
					{ message: "Nutrient not found", language_id: 4, type: "NUTRIENTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ALLERGENS_NOT_FOUND_ERROR",
			message: "Entity allergens not found",
			translations:
				[
					{ message: "Alergénico não encontrado",language_id: 3, type: "ALLERGENS_NOT_FOUND_ERROR" },
					{ message: "Allergen not found", language_id: 4, type: "ALLERGENS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "PRODUCTS_NOT_FOUND_ERROR",
			message: "Entity products not found",
			translations:
				[
					{ message: "Produto não encontrado",language_id: 3, type: "PRODUCTS_NOT_FOUND_ERROR" },
					{ message: "Products not found", language_id: 4, type: "PRODUCTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "PRODUCT_NOT_CORRESPOND_THE_ENTITY",
			message: "The product not correspond the entity",
			translations:
				[
					{ message: "O produto não corresponde à entidade",language_id: 3, type: "PRODUCT_NOT_CORRESPOND_THE_ENTITY" },
					{ message: "The product not correspond the entity", language_id: 4, type: "PRODUCT_NOT_CORRESPOND_THE_ENTITY" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIEN_NO_CERTIFICATE",
			message: "Experience don't have certificate",
			translations:
				[
					{ message: "Experiência não tem certificado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXPERIEN_NO_CERTIFICATE" },
					{ message: "Experience don't have certificate", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXPERIEN_NO_CERTIFICATE" }
				]
		},
		{
			type: "The_DISH_TYPE_NOT_EXIST",
			message: "The dish type id sent doesnt exist",
			translations:
				[
					{ message: "O id do tipo de prato não existe",language_id: 3, type: "The_DISH_TYPE_NOT_EXIST" },
					{ message: "The dish type id sent doesnt exist", language_id: 4, type: "The_DISH_TYPE_NOT_EXIST" }
				]
		},
		{
			type: "The_RECIPE_Not_EXIST",
			message: "The recipe id sent doesn't exist",
			translations:
				[
					{ message: "A receita enviada não existe",language_id: 3, type: "The_RECIPE_Not_EXIST" },
					{ message: "The recipe id sent doesn't exist", language_id: 4, type: "The_RECIPE_Not_EXIST" }
				]
		},
		{
			type: "DISH_TYPE_CODE_ALREADY_EXIST",
			message: "The dish type code already exists",
			translations:
				[
					{ message: "O código do tipo de prato já existe",language_id: 3, type: "DISH_TYPE_CODE_ALREADY_EXIST" },
					{ message: "The dish type code already exists", language_id: 4, type: "DISH_TYPE_CODE_ALREADY_EXIST" }
				]
		},
		{
			type: "NO_FISCAL_ENTITY_ID",
			message: "You need send fiscal entity Id",
			translations:
				[
					{ message: "É necessário enviar o id da entidade fiscal",language_id: 3, type: "NO_FISCAL_ENTITY_ID" },
					{ message: "You need send fiscal entity Id", language_id: 4, type: "NO_FISCAL_ENTITY_ID" }
				]
		},
		{
			type: "MEAL_ALREADY_EXIST",
			message: "Already exist a menu for this meal, service and date",
			translations:
				[
					{ message: "Já existe um menu para esta refeição, serviço e data",language_id: 3, type: "MEAL_ALREADY_EXIST" },
					{ message: "Already exist a menu for this meal, service and date", language_id: 4, type: "MEAL_ALREADY_EXIST" }
				]
		},
		{
			type: "APPOINTMENT_INVALID_HOUR",
			message: "Invalid hour",
			translations:
				[
					{ message: "Hora inválida",language_id: 3, type: "APPOINTMENT_INVALID_HOUR" },
					{ message: "Invalid hour", language_id: 4, type: "APPOINTMENT_INVALID_HOUR" }
				]
		},
		{
			type: "DISH_TYPE_NOT_CORRESPOND_THE_ENTITY",
			message: "The dish type not correspond the entity",
			translations:
				[
					{ message: "O tipo de prato não corresponde à entidade",language_id: 3, type: "DISH_TYPE_NOT_CORRESPOND_THE_ENTITY" },
					{ message: "The dish type not correspond the entity", language_id: 4, type: "DISH_TYPE_NOT_CORRESPOND_THE_ENTITY" }
				]
		},
		{
			type: "MENUS_NOT_FOUND_ERROR",
			message: "Entity menus not found",
			translations:
				[
					{ message: "Ementa não encontrada",language_id: 3, type: "MENUS_NOT_FOUND_ERROR" },
					{ message: "Menu not found", language_id: 4, type: "MENUS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SERVICE_NOT_CORRESPOND_THE_ENTITY",
			message: "The services not correspond the entity",
			translations:
				[
					{ message: "Os serviços não correspondem à entidade",language_id: 3, type: "SERVICE_NOT_CORRESPOND_THE_ENTITY" },
					{ message: "The services not correspond the entity", language_id: 4, type: "SERVICE_NOT_CORRESPOND_THE_ENTITY" }
				]
		},
		{
			type: "SERVICE_NOT_CANTEEN",
			message: "The service is not type canteen",
			translations:
				[
					{ message: "O serviço não é do tipo cantina",language_id: 3, type: "SERVICE_NOT_CANTEEN" },
					{ message: "The service is not type canteen", language_id: 4, type: "SERVICE_NOT_CANTEEN" }
				]
		},
		{
			type: "FAMILY_NOT_CORRESPOND_THE_ENTITY",
			message: "The Family not correspond the entity",
			translations:
				[
					{ message: "A família não corresponde à entidade",language_id: 3, type: "FAMILY_NOT_CORRESPOND_THE_ENTITY" },
					{ message: "The Family not correspond the entity", language_id: 4, type: "FAMILY_NOT_CORRESPOND_THE_ENTITY" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_CHANGE_STATUS",
			message: "Is not possible to change to the state",
			translations:
				[
					{ message: "Não é possível mudar o estado",language_id: 3, type: "ACCOMMODATION_APPLICATION_CHANGE_STATUS" },
					{ message: "Is not possible to change to the state", language_id: 4, type: "ACCOMMODATION_APPLICATION_CHANGE_STATUS" }
				]
		},
		{
			type: "MOVEMENT_ALREADY_HAVE_PAYMENT",
			message: "Movement already have a payment defined",
			translations:
				[
					{ message: "O movimento já tem um pagamento definido",language_id: 3, type: "MOVEMENT_ALREADY_HAVE_PAYMENT" },
					{ message: "Movement already have a payment defined", language_id: 4, type: "MOVEMENT_ALREADY_HAVE_PAYMENT" }
				]
		},
		{
			type: "DISHS_NOT_FOUND_ERROR",
			message: "Entity dishs not found",
			translations:
				[
					{ message: "Prato não encontrado",language_id: 3, type: "DISHS_NOT_FOUND_ERROR" },
					{ message: "Dishs not found", language_id: 4, type: "DISHS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "CART_ITEM_ALREADY_EXPIRED",
			message: "The item is already expired",
			translations:
				[
					{ message: "O item já expirou",language_id: 3, type: "CART_ITEM_ALREADY_EXPIRED" },
					{ message: "The item is already expired", language_id: 4, type: "CART_ITEM_ALREADY_EXPIRED" }
				]
		},
		{
			type: "UNABLE_TO_CONFIRM_PAYMENT",
			message: "At the moment is not possive to confirm this payment",
			translations:
				[
					{ message: "No momento não é possivel confirmar este pagamento",language_id: 3, type: "UNABLE_TO_CONFIRM_PAYMENT" },
					{ message: "At the moment is not possive to confirm this payment", language_id: 4, type: "UNABLE_TO_CONFIRM_PAYMENT" }
				]
		},
		{
			type: "MENUS-DISHS_NOT_FOUND_ERROR",
			message: "Entity menus-dishs not found",
			translations:
				[
					{ message: "Prato do menu não encontrado",language_id: 3, type: "MENUS-DISHS_NOT_FOUND_ERROR" },
					{ message: "Menu dish not found", language_id: 4, type: "MENUS-DISHS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_INVALID_DATES",
			message: "The application duration must be inside defined application period",
			translations:
				[
					{ message: "A duração da inscrição deve estar dentro do período de inscrição definido",language_id: 3, type: "UBIKE_APPLICATION_INVALID_DATES" },
					{ message: "The application duration must be inside defined application period", language_id: 4, type: "UBIKE_APPLICATION_INVALID_DATES" }
				]
		},
		{
			type: "UBIKE_APPLICATION_IN_PROGRESS_NOT_FOUND",
			message: "The user donat have any application in progress",
			translations:
				[
					{ message: "O utilizador não tem nenhum candidatura em processamento",language_id: 3, type: "UBIKE_APPLICATION_IN_PROGRESS_NOT_FOUND" },
					{ message: "The user doesn't have any application in progress", language_id: 4, type: "UBIKE_APPLICATION_IN_PROGRESS_NOT_FOUND" }
				]
		},
		{
			type: "MENU_ALREADY_EXIST_IN_THIS_DATES",
			message: "Already exist menus in this dates",
			translations:
				[
					{ message: "Já existem menus nestas datas",language_id: 3, type: "MENU_ALREADY_EXIST_IN_THIS_DATES" },
					{ message: "Already exist menus in this dates", language_id: 4, type: "MENU_ALREADY_EXIST_IN_THIS_DATES" }
				]
		},
		{
			type: "UBIKE_APPLICATION_DUPLICATED_TRIP",
			message: "The user already registry this trip",
			translations:
				[
					{ message: "O utilizador já se registrou nesta viagem",language_id: 3, type: "UBIKE_APPLICATION_DUPLICATED_TRIP" },
					{ message: "The user already registry this trip", language_id: 4, type: "UBIKE_APPLICATION_DUPLICATED_TRIP" }
				]
		},
		{
			type: "REGIME_WITH_APPLICATIONS",
			message: "You have applications with this regime associated",
			translations:
				[
					{ message: "Já tem candidaturas com este regime associado",language_id: 3, type: "REGIME_WITH_APPLICATIONS" },
					{ message: "You have applications with this regime associated", language_id: 4, type: "REGIME_WITH_APPLICATIONS" }
				]
		},
		{
			type: "EXTRA_WITH_APPLICATIONS",
			message: "You have applications with this extra associated",
			translations:
				[
					{ message: "Já tem candidaturas com este extra associado",language_id: 3, type: "EXTRA_WITH_APPLICATIONS" },
					{ message: "You have applications with this extra associated", language_id: 4, type: "EXTRA_WITH_APPLICATIONS" }
				]
		},
		{
			type: "RESIDENCE_WITH_APPLICATIONS",
			message: "You have applications with this residence associated",
			translations:
				[
					{ message: "Já tem inscrições associadas a esta residência",language_id: 3, type: "RESIDENCE_WITH_APPLICATIONS" },
					{ message: "You have applications with this residence associated", language_id: 4, type: "RESIDENCE_WITH_APPLICATIONS" }
				]
		},
		{
			type: "TYPOLOGY_WITH_ROOMS",
			message: "You have rooms with this typology associated",
			translations:
				[
					{ message: "Já tem quartos com esta tipologia associada",language_id: 3, type: "TYPOLOGY_WITH_ROOMS" },
					{ message: "You have rooms with this typology associated", language_id: 4, type: "TYPOLOGY_WITH_ROOMS" }
				]
		},
		{
			type: "ORGANIC-UNITS_NOT_FOUND_ERROR",
			message: "Entity organic-units not found",
			translations:
				[
					{ message: "Unidade orgânica não encontrada",language_id: 3, type: "ORGANIC-UNITS_NOT_FOUND_ERROR" },
					{ message: "Organic unit not found", language_id: 4, type: "ORGANIC-UNITS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_UNAUTHORIZED",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_UNAUTHORIZED" },
					{ message: "Unauthorized application status change", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_UNAUTHORIZED" }
				]
		},
		{
			type: "INSUFFICIENT_FUNDS_FOR_PAYMENT",
			message: "Not enough funds to make this payment",
			translations:
				[
					{ message: "Não há fundos suficientes para fazer este pagamento",language_id: 3, type: "INSUFFICIENT_FUNDS_FOR_PAYMENT" },
					{ message: "Not enough funds to make this payment", language_id: 4, type: "INSUFFICIENT_FUNDS_FOR_PAYMENT" }
				]
		},
		{
			type: "RESERVATION_ALREADY_USED",
			message: "Reservation already used",
			translations:
				[
					{ message: "Reserva já utilizada",language_id: 3, type: "RESERVATION_ALREADY_USED" },
					{ message: "Reservation already used", language_id: 4, type: "RESERVATION_ALREADY_USED" }
				]
		},
		{
			type: "MAXIMUM_HOUR_CONSUPTION_REACHED",
			message: "Maximum hour of consumption reached",
			translations:
				[
					{ message: "Hora máxima de consumo atingida",language_id: 3, type: "MAXIMUM_HOUR_CONSUPTION_REACHED" },
					{ message: "Maximum hour of consumption reached", language_id: 4, type: "MAXIMUM_HOUR_CONSUPTION_REACHED" }
				]
		},
		{
			type: "CART_NOT_FOUND_ERROR",
			message: "No cart found",
			translations:
				[
					{ message: "Nenhum carrinho encontrado",language_id: 3, type: "CART_NOT_FOUND_ERROR" },
					{ message: "No cart found", language_id: 4, type: "CART_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_WITHDRAWAL_BAD_APPLICATION",
			message: "Can't change the status of the withdrawal because application is not in contract",
			translations:
				[
					{ message: "Não é possível alterar o estado 'cancelar' porque a candidatura não está em contrato",language_id: 3, type: "ACCOMMODATION_WITHDRAWAL_BAD_APPLICATION" },
					{ message: "Can't change the status of the withdrawal because application is not in contract", language_id: 4, type: "ACCOMMODATION_WITHDRAWAL_BAD_APPLICATION" }
				]
		},
		{
			type: "ACCOMMODATION_EXTENSION_BAD_APPLICATION",
			message: "Can't change the status of the extension because application is not in contract",
			translations:
				[
					{ message: "Não é possível alterar o estado 'extensão' porque a candidatura não está em contrato",language_id: 3, type: "ACCOMMODATION_EXTENSION_BAD_APPLICATION" },
					{ message: "Can't change the status of the extension because application is not in contract", language_id: 4, type: "ACCOMMODATION_EXTENSION_BAD_APPLICATION" }
				]
		},
		{
			type: "LISTINGS_NOT_FOUND_ERROR",
			message: "Entity listings not found",
			translations:
				[
					{ message: "Propriadade não encontrada ",language_id: 3, type: "LISTINGS_NOT_FOUND_ERROR" },
					{ message: "Listing not found", language_id: 4, type: "LISTINGS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_FORM_BAD_STATUS",
			message: "Only can accept on [dispatch]  application form status",
			translations:
				[
					{ message: "Só aceita o estado do formulário de inscrição [envio]",language_id: 3, type: "UBIKE_APPLICATION_FORM_BAD_STATUS" },
					{ message: "Only can accept on [dispatch]  application form status", language_id: 4, type: "UBIKE_APPLICATION_FORM_BAD_STATUS" }
				]
		},
		{
			type: "SERVICE_NOT_FOUND_ERROR",
			message: "Entity service not found",
			translations:
				[
					{ message: "Serviço não encontrado",language_id: 3, type: "SERVICE_NOT_FOUND_ERROR" },
					{ message: "Service not found", language_id: 4, type: "SERVICE_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "APPLICATION_PHASE_WITH_APPLICATIONS",
			message: "You have applications with this application phase associated",
			translations:
				[
					{ message: "Já tem inscrições com esta fase de inscrição associada",language_id: 3, type: "APPLICATION_PHASE_WITH_APPLICATIONS" },
					{ message: "You have applications with this application phase associated", language_id: 4, type: "APPLICATION_PHASE_WITH_APPLICATIONS" }
				]
		},
		{
			type: "UBIKE_BIKE_ALREADY_IN_USE",
			message: "The bike is already in use on one application in progress",
			translations:
				[
					{ message: "A bicicleta já está em utilização, na cadidatura que está em processamento",language_id: 3, type: "UBIKE_BIKE_ALREADY_IN_USE" },
					{ message: "The bike is already in use on one application in progress", language_id: 4, type: "UBIKE_BIKE_ALREADY_IN_USE" }
				]
		},
		{
			type: "PURCHASE_NOT_ACTIVE",
			message: "Ticket purchase for this route is not active",
			translations:
				[
					{ message: "A compra de passagens para esta rota não está ativa",language_id: 3, type: "PURCHASE_NOT_ACTIVE" },
					{ message: "Ticket purchase for this route is not active", language_id: 4, type: "PURCHASE_NOT_ACTIVE" }
				]
		},
		{
			type: "DESK_OPERATOR_NOT_FOUND_ERROR",
			message: "Entity desk_operator not found",
			translations:
				[
					{ message: "Operador de balcão não encontrado",language_id: 3, type: "DESK_OPERATOR_NOT_FOUND_ERROR" },
					{ message: "Desk operator not found", language_id: 4, type: "DESK_OPERATOR_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_PHASE_OVERLAP",
			message: "The start date and end date overlap with another application phase",
			translations:
				[
					{ message: "A data de início e a data de fim sobrepõem-se a outra fase de inscrição",language_id: 3, type: "ACCOMMODATION_PHASE_OVERLAP" },
					{ message: "The start date and end date overlap with another application phase", language_id: 4, type: "ACCOMMODATION_PHASE_OVERLAP" }
				]
		},
		{
			type: "ACCOMMODATION_PHASE_ALREADY_EXIST",
			message: "Already exist a application phase for this academic year",
			translations:
				[
					{ message: "Já existe uma fase de inscrição para este ano letivo",language_id: 3, type: "ACCOMMODATION_PHASE_ALREADY_EXIST" },
					{ message: "Already exist a application phase for this academic year", language_id: 4, type: "ACCOMMODATION_PHASE_ALREADY_EXIST" }
				]
		},
		{
			type: "U_BIKE_TRIPS_FINAL_INVALID_HOURS",
			message: "Final time cannot be bigger that initial time",
			translations:
				[
					{ message: "O tempo final não pode ser maior que o tempo inicial",language_id: 3, type: "U_BIKE_TRIPS_INVALID_HOURS" },
					{ message: "Final time cannot be bigger that initial time", language_id: 4, type: "U_BIKE_TRIPS_INVALID_HOURS" }
				]
		},
		{
			type: "U_BIKE_TRIPS_INITIAL_INVALID_HOURS",
			message: "Initial time cannot be less that Final time",
			translations:
				[
					{ message: "O tempo inicial não pode ser inferior que o tempo final",language_id: 3, type: "U_BIKE_TRIPS_INVALID_HOURS" },
					{ message: "Initial time cannot be less that Final time", language_id: 4, type: "U_BIKE_TRIPS_INVALID_HOURS" }
				]
		},

		{
			type: "SOCIAL_SCHOLARSHIP_USER_NOT_APPLICATION",
			message: "This user not have application",
			translations:
				[
					{ message: "Este utilizador não tem candidaturas",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_NOT_APPLICATION" },
					{ message: "This user not have application", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_NOT_APPLICATION" }
				]
		},
		{
			type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_EXTENSION",
			message: "Already exist a active extension",
			translations:
				[
					{ message: "Já existe uma extensão ativa",language_id: 3, type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_EXTENSION" },
					{ message: "Already exist a active extension", language_id: 4, type: "ACCOMMODATION_AREADY_EXIST_ACTIVE_EXTENSION" }
				]
		},
		{
			type: "INVALID_AUTH_DEVICE_TYPE",
			message: "The device type provided cannot be authorized using this endpoint (only available for [MOBILE,BO,WEB,POS,GENERIC]). Please use the endpoint /api/v1/authorize/device/{id} (providing the ID of the device you are trying to authenticate)",
			translations:
				[
					{ message: "O tipo de dispositivo indicado não pode utilizar este endpoint (disponível apenas para [MOBILE, BO, WEB, POS, GENERIC]). Use o endpoint / api / v1 / authorize / device / {id} (fornecendo o ID do dispositivo que está tentando autenticar)",language_id: 3, type: "INVALID_AUTH_DEVICE_TYPE" },
					{ message: "The device type provided cannot be authorized using this endpoint (only available for [MOBILE,BO,WEB,POS,GENERIC]). Please use the endpoint /api/v1/authorize/device/{id} (providing the ID of the device you are trying to authenticate)", language_id: 4, type: "INVALID_AUTH_DEVICE_TYPE" }
				]
		},
		{
			type: "GROUPS_NOT_FOUND_ERROR",
			message: "Entity groups not found",
			translations:
				[
					{ message: "Grupo não encontrado",language_id: 3, type: "GROUPS_NOT_FOUND_ERROR" },
					{ message: "Group not found", language_id: 4, type: "GROUPS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_HAS_ACTIVE_APLICATION",
			message: "You have a active application",
			translations:
				[
					{ message: "Já tem uma candidatura ativa",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_HAS_ACTIVE_APLICATION" },
					{ message: "You have a active application", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_HAS_ACTIVE_APLICATION" }
				]
		},
		{
			type: "UBIKE_APPLICATION_BAD_STATUS",
			message: "Only can create a complaint on [assigned] or [unassigned] application status",
			translations:
				[
					{ message: "Só pode criar uma reclamação sobre o estado [atribuído] ou [não atribuído]",language_id: 3, type: "UBIKE_APPLICATION_BAD_STATUS" },
					{ message: "Only can create a complaint on [assigned] or [unassigned] application status", language_id: 4, type: "UBIKE_APPLICATION_BAD_STATUS" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR",
			message: "Unauthorized user interest status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR" },
					{ message: "Unauthorized user interest status change", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR" }
				]
		},
		{
			type: "EXPERIENCE-USER-INTEREST_NOT_FOUND_ERROR",
			message: "Entity experience-user-interest not found",
			translations:
				[
					{ message: "Experiência não encontrada",language_id: 3, type: "EXPERIENCE-USER-INTEREST_NOT_FOUND_ERROR" },
					{ message: "Experience not found", language_id: 4, type: "EXPERIENCE-USER-INTEREST_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_INTERVIEW_INFO_ERROR",
			message: "Unauthorized manifest interest status change, it is necessary to add notes or report in interview",
			translations:
				[
					{ message: "Alteração não autorizada, é necessário adicionar notas na entrevista",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_INTERVIEW_INFO_ERROR" },
					{ message: "Unauthorized manifest interest status change, it is necessary to add notes or report in interview", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_INTERVIEW_INFO_ERROR" }
				]
		},
		{
			type: "USER_ID_REQUIRED",
			message: "'user_id' is required!",
			translations:
				[
					{ message: "O id do utilizador é obrigatório",language_id: 3, type: "USER_ID_REQUIRED" },
					{ message: "User id is required!", language_id: 4, type: "USER_ID_REQUIRED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_ATTENDANCE_INVALID_HOURS",
			message: "Final time cannot be bigger than initial time",
			translations:
				[
					{ message: "O tempo final não pode ser maior que o tempo inicial",language_id: 3, type: "SOCIAL_SCHOLARSHIP_ATTENDANCE_INVALID_HOURS" },
					{ message: "Final time cannot be bigger than initial time", language_id: 4, type: "SOCIAL_SCHOLARSHIP_ATTENDANCE_INVALID_HOURS" }
				]
		},
		{
			type: "MONTHLY-REPORTS_NOT_FOUND_ERROR",
			message: "Entity monthly-reports not found",
			translations:
				[
					{ message: "Relatório mensal não encontrado",language_id: 3, type: "MONTHLY-REPORTS_NOT_FOUND_ERROR" },
					{ message: "Monthly report not found", language_id: 4, type: "MONTHLY-REPORTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "LINES_NOT_ORDER",
			message: "Lines 1 of the table do not have the hours in order",
			translations:
				[
					{ message: "As linhas 1 da tabela não possuem os horários em ordem",language_id: 3, type: "LINES_NOT_ORDER" },
					{ message: "Lines 1 of the table do not have the hours in order", language_id: 4, type: "LINES_NOT_ORDER" }
				]
		},
		{
			type: "ROOMS_NOT_FOUND_ERROR",
			message: "Entity rooms not found",
			translations:
				[
					{ message: "Quarto não encontrado",language_id: 3, type: "ROOMS_NOT_FOUND_ERROR" },
					{ message: "Room not found", language_id: 4, type: "ROOMS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "request.aborted",
			message: "request aborted",
			translations:
				[
					{ message: "Pedido cancelado",language_id: 3, type: "request.aborted" },
					{ message: "request aborted", language_id: 4, type: "request.aborted" }
				]
		},
		{
			type: "MENU_NOT_FOUND",
			message: "Menu not found in this date",
			translations:
				[
					{ message: "O menu não foi encontrado nesta data",language_id: 3, type: "MENU_NOT_FOUND" },
					{ message: "Menu not found in this date", language_id: 4, type: "MENU_NOT_FOUND" }
				]
		},
		{
			type: "RESERVATIONS_NOT_FOUND_ERROR",
			message: "Entity reservations not found",
			translations:
				[
					{ message: "Reserva não encontrada",language_id: 3, type: "RESERVATIONS_NOT_FOUND_ERROR" },
					{ message: "Reservation not found", language_id: 4, type: "RESERVATIONS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_FORBIDDEN",
			message: "This application id doesnt belong to the user logged",
			translations:
				[
					{ message: "O id de candidatura não pertence ao utilizador logado",language_id: 3, type: "ACCOMMODATION_APPLICATION_FORBIDDEN" },
					{ message: "This application id doesnt belong to the user logged", language_id: 4, type: "ACCOMMODATION_APPLICATION_FORBIDDEN" }
				]
		},
		{
			type: "CART-ITEM_NOT_FOUND_ERROR",
			message: "Entity cart-item not found",
			translations:
				[
					{ message: "Não foi encontrado o item do carrinho",language_id: 3, type: "CART-ITEM_NOT_FOUND_ERROR" },
					{ message: "Cart item not found", language_id: 4, type: "CART-ITEM_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UNAVAILABLE_PAYMENT_METHOD",
			message: "This Payment Method is Not Available",
			translations:
				[
					{ message: "Este método de pagamento não está disponível",language_id: 3, type: "UNAVAILABLE_PAYMENT_METHOD" },
					{ message: "This Payment Method is Not Available", language_id: 4, type: "UNAVAILABLE_PAYMENT_METHOD" }
				]
		},
		{
			type: "ASSOCIATION_IS_OPERATING",
			message: "This association is operating you can not modify",
			translations:
				[
					{ message: "Esta associação está em operação, não pode ser modificada",language_id: 3, type: "ASSOCIATION_IS_OPERATING" },
					{ message: "This association is operating you can not modify", language_id: 4, type: "ASSOCIATION_IS_OPERATING" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_ERROR",
			message: "Unauthorized application status change, it is necessary to schedule an interview",
			translations:
				[
					{ message: "Para a mudança de estado de inscrição, é necessário agendar entrevista",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_ERROR" },
					{ message: "Unauthorized application status change, it is necessary to schedule an interview", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_ERROR" }
				]
		},
		{
			type: "UNAUTHORIZED_USER_CHANGE",
			message: "This ticket is not attended by you",
			translations:
				[
					{ message: "Esta senha não é atendida por si",language_id: 3, type: "UNAUTHORIZED_USER_CHANGE" },
					{ message: "This ticket is not attended by you", language_id: 4, type: "UNAUTHORIZED_USER_CHANGE" }
				]
		},
		{
			type: "AUTHROZIZATION_PROFILE_WITH_USERS",
			message: "You have users with this profile associated",
			translations:
				[
					{ message: "Tem utilizadores com este perfil associado",language_id: 3, type: "AUTHROZIZATION_PROFILE_WITH_USERS" },
					{ message: "You have users with this profile associated", language_id: 4, type: "AUTHROZIZATION_PROFILE_WITH_USERS" }
				]
		},
		{
			type: "AUTHROZIZATION_USER_GROUP_WITH_USERS",
			message: "You have users with this user group associated",
			translations:
				[
					{ message: "Tem utilizadores com este grupo associado",language_id: 3, type: "AUTHROZIZATION_USER_GROUP_WITH_USERS" },
					{ message: "You have users with this user group associated", language_id: 4, type: "AUTHROZIZATION_USER_GROUP_WITH_USERS" }
				]
		},
		{
			type: "APPLICATION_PHASE_ACADEMIC_YEAR_NOT_FOUND",
			message: "Academic year not found",
			translations:
				[
					{ message: "Não existe ano académico",language_id: 3, type: "APPLICATION_PHASE_ACADEMIC_YEAR_NOT_FOUND" },
					{ message: "Academic year not found", language_id: 4, type: "APPLICATION_PHASE_ACADEMIC_YEAR_NOT_FOUND" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_INFO_ERROR",
			message: "Unauthorized application status change, it is necessary to add notes or report in interview",
			translations:
				[
					{ message: "Alteração não autorizada, é necessário adicionar notas na entrevista",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_INFO_ERROR" },
					{ message: "Unauthorized application status change, it is necessary to add notes or report in interview", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_INFO_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_TECNICAL_REPORT_ERROR",
			message: "Unauthorized application status change, it is necessary to add information in technical report",
			translations:
				[
					{ message: "Alteração não autorizada, é necessário adicionar informações no relatório técnico",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_TECNICAL_REPORT_ERROR" },
					{ message: "Unauthorized application status change, it is necessary to add information in technical report", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_TECNICAL_REPORT_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_EQUAL_CONTACTS",
			message: "Contact and emergency contact cannot be equals",
			translations:
				[
					{ message: "O contacto e contacto de emergência não podem ser iguais",language_id: 3, type: "ACCOMMODATION_APPLICATION_EQUAL_CONTACTS" },
					{ message: "Contact and emergency contact cannot be equals", language_id: 4, type: "ACCOMMODATION_APPLICATION_EQUAL_CONTACTS" }
				]
		},
		{
			type: "UBIKE_APPLICATION_ALREDY_COMPLAIN",
			message: "Application already have a complain",
			translations:
				[
					{ message: "A candidatura já tem uma reclamação",language_id: 3, type: "UBIKE_APPLICATION_ALREDY_COMPLAIN" },
					{ message: "Application already have a complain", language_id: 4, type: "UBIKE_APPLICATION_ALREDY_COMPLAIN" }
				]
		},
		{
			type: "EXTRAS_NOT_FOUND_ERROR",
			message: "Entity extras not found",
			translations:
				[
					{ message: "Extra não encontrado",language_id: 3, type: "EXTRAS_NOT_FOUND_ERROR" },
					{ message: "Extra not found", language_id: 4, type: "EXTRAS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "PATRIMONY-CATEGORIES_NOT_FOUND_ERROR",
			message: "Entity patrimony-categories not found",
			translations:
				[
					{ message: "Categoria de património não encontrado",language_id: 3, type: "PATRIMONY-CATEGORIES_NOT_FOUND_ERROR" },
					{ message: "Patrimony categories not found", language_id: 4, type: "PATRIMONY-CATEGORIES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "REGIMES_NOT_FOUND_ERROR",
			message: "Entity regimes not found",
			translations:
				[
					{ message: "Regime não encontrado",language_id: 3, type: "REGIMES_NOT_FOUND_ERROR" },
					{ message: "Regime not found", language_id: 4, type: "REGIMES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_BIKE_NOT_FOUND",
			message: "The user dont have associated bike",
			translations:
				[
					{ message: "O utilizador não tem uma bicicleta associada",language_id: 3, type: "UBIKE_APPLICATION_BIKE_NOT_FOUND" },
					{ message: "The user dont have associated bike", language_id: 4, type: "UBIKE_APPLICATION_BIKE_NOT_FOUND" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_DOCUMENT_TYPE_NOT_FOUND",
			message: "Document type provided not found",
			translations:
				[
					{ message: "Não existe o tipo de documento",language_id: 3, type: "ACCOMMODATION_APPLICATION_DOCUMENT_TYPE_NOT_FOUND" },
					{ message: "Document type provided not found", language_id: 4, type: "ACCOMMODATION_APPLICATION_DOCUMENT_TYPE_NOT_FOUND" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_STATEMENT_FILE_NOT_FOUND",
			message: "Its required at least one income_statement_file",
			translations:
				[
					{ message: "É necessário pelo menos uma declaração",language_id: 3, type: "ACCOMMODATION_APPLICATION_STATEMENT_FILE_NOT_FOUND" },
					{ message: "Its required at least one income_statement_file", language_id: 4, type: "ACCOMMODATION_APPLICATION_STATEMENT_FILE_NOT_FOUND" }
				]
		},
		{
			type: "CONFIGURATION_ACADEMIC_YEAR_ALREADY_EXIST",
			message: "Academic year 2021-2022 already exists",
			translations:
				[
					{ message: "O ano letivo 2021-2022 já existe",language_id: 3, type: "CONFIGURATION_ACADEMIC_YEAR_ALREADY_EXIST" },
					{ message: "Academic year 2021-2022 already exists", language_id: 4, type: "CONFIGURATION_ACADEMIC_YEAR_ALREADY_EXIST" }
				]
		},
		{
			type: "ACCOMMODATION_PATRIMONY_CATEGORY_CODE_ALREADY_EXISTS",
			message: "Already exists one patrimony category with provided code",
			translations:
				[
					{ message: "Já existe uma categoria de património com esse código",language_id: 3, type: "ACCOMMODATION_PATRIMONY_CATEGORY_CODE_ALREADY_EXISTS" },
					{ message: "Already exists one patrimony category with provided code", language_id: 4, type: "ACCOMMODATION_PATRIMONY_CATEGORY_CODE_ALREADY_EXISTS" }
				]
		},
		{
			type: "ACCOMMODATION_ACADEMIC_YEAR_NOT_FOUND",
			message: "Academic year not found",
			translations:
				[
					{ message: "Não existe o ano académico",language_id: 3, type: "ACCOMMODATION_ACADEMIC_YEAR_NOT_FOUND" },
					{ message: "Academic year not found", language_id: 4, type: "ACCOMMODATION_ACADEMIC_YEAR_NOT_FOUND" }
				]
		},
		{
			type: "AUTH_ACCOUNT_NOT_VERIFIED",
			message: "Please validate your email before login",
			translations:
				[
					{ message: "Por favor, valide o seu e-mail antes de fazer o login",language_id: 3, type: "AUTH_ACCOUNT_NOT_VERIFIED" },
					{ message: "Please validate your email before login", language_id: 4, type: "AUTH_ACCOUNT_NOT_VERIFIED" }
				]
		},
		{
			type: "LOCALS_NOT_FOUND_ERROR",
			message: "Entity locals not found",
			translations:
				[
					{ message: "Local não encontrado",language_id: 3, type: "LOCALS_NOT_FOUND_ERROR" },
					{ message: "Local not found", language_id: 4, type: "LOCALS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "AUTH_PIN_REQUIRED",
			message: "'pin' are required if an rfid is provided",
			translations:
				[
					{ message: "Se preencheu o RFID é obrigatório preencher o pin",language_id: 3, type: "AUTH_PIN_REQUIRED" },
					{ message: "'pin' are required if an rfid is provided", language_id: 4, type: "AUTH_PIN_REQUIRED" }
				]
		},
		{
			type: "AUTH_EMAIL_OR_USERNAME_REQUIRED",
			message: "'email' or 'user_name' are required if an pin/password is provided",
			translations:
				[
					{ message: "Se preencheu o pin/senha é obrigatório preencher o e-mail ou username",language_id: 3, type: "AUTH_EMAIL_OR_USERNAME_REQUIRED" },
					{ message: "'email' or 'user_name' are required if an pin/password is provided", language_id: 4, type: "AUTH_EMAIL_OR_USERNAME_REQUIRED" }
				]
		},
		{
			type: "AUTH_RFID_EXPIRED",
			message: "RFIF expired",
			translations:
				[
					{ message: "RFID expirou",language_id: 3, type: "AUTH_RFID_EXPIRED" },
					{ message: "RFIF expired", language_id: 4, type: "AUTH_RFID_EXPIRED" }
				]
		},
		{
			type: "ACCOMMODATION_DUPLICATE_ACADEMIC_YEAR",
			message: "Already exists one configuration to provided academic year",
			translations:
				[
					{ message: "Já existe uma configuração para fornecer o ano académico",language_id: 3, type: "ACCOMMODATION_DUPLICATE_ACADEMIC_YEAR" },
					{ message: "Already exists one configuration to provided academic year", language_id: 4, type: "ACCOMMODATION_DUPLICATE_ACADEMIC_YEAR" }
				]
		},
		{
			type: "AUTH_ACCOUNT_ALREADY_VERIFIED",
			message: "This account already as been verified",
			translations:
				[
					{ message: "Esta conta já foi verificada",language_id: 3, type: "AUTH_ACCOUNT_ALREADY_VERIFIED" },
					{ message: "This account already as been verified", language_id: 4, type: "AUTH_ACCOUNT_ALREADY_VERIFIED" }
				]
		},
		{
			type: "AUTH_PASSWORD_ALREADY_USED",
			message: "Password already as been used, please choose other",
			translations:
				[
					{ message: "Essa senha já foi utilizada, por favor escolha outra",language_id: 3, type: "AUTH_PASSWORD_ALREADY_USED" },
					{ message: "Password already as been used, please choose other", language_id: 4, type: "AUTH_PASSWORD_ALREADY_USED" }
				]
		},
		{
			type: "AUTH_DISABLED_ACCOUNT",
			message: "Your account is not active",
			translations:
				[
					{ message: "A sua conta não está ativa",language_id: 3, type: "AUTH_DISABLED_ACCOUNT" },
					{ message: "Your account is not active", language_id: 4, type: "AUTH_DISABLED_ACCOUNT" }
				]
		},
		{
			type: "AUTH_WRONG_ACTUAL_PASSWORD",
			message: "Failed on validate the actual password",
			translations:
				[
					{ message: "Falha ao validar a senha atual",language_id: 3, type: "AUTH_WRONG_ACTUAL_PASSWORD" },
					{ message: "Failed on validate the actual password", language_id: 4, type: "AUTH_WRONG_ACTUAL_PASSWORD" }
				]
		},
		{
			type: "AUTH_NOT_ALLOWED_CHANGE_PASSWORD",
			message: "The user is not allowed to change the password",
			translations:
				[
					{ message: "Este utilizador não tem permissão para alterar a senha",language_id: 3, type: "AUTH_NOT_ALLOWED_CHANGE_PASSWORD" },
					{ message: "The user is not allowed to change the password", language_id: 4, type: "AUTH_NOT_ALLOWED_CHANGE_PASSWORD" }
				]
		},
		{
			type: "DOCUMENT-TYPES_NOT_FOUND_ERROR",
			message: "Entity document-types not found",
			translations:
				[
					{ message: "Tipo de documento não encontrado",language_id: 3, type: "DOCUMENT-TYPES_NOT_FOUND_ERROR" },
					{ message: "Document type not found", language_id: 4, type: "DOCUMENT-TYPES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "BUS_APPLICATION_UNAUTHORIZED_REQUEST",
			message: "Don't have permissions to change state",
			translations:
				[
					{ message: "Não tem permissão para mudar de estado",language_id: 3, type: "BUS_APPLICATION_UNAUTHORIZED_REQUEST" },
					{ message: "Don't have permissions to change state", language_id: 4, type: "BUS_APPLICATION_UNAUTHORIZED_REQUEST" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_OTHER_INCOME_SOURCE_NOT_FOUND",
			message: "Its required provide other income source description",
			translations:
				[
					{ message: "É obrigatório preencher outra origem dos rendimentos",language_id: 3, type: "ACCOMMODATION_APPLICATION_OTHER_INCOME_SOURCE_NOT_FOUND" },
					{ message: "Its required provide other income source description", language_id: 4, type: "ACCOMMODATION_APPLICATION_OTHER_INCOME_SOURCE_NOT_FOUND" }
				]
		},
		{
			type: "APPLICATION-PHASES_NOT_FOUND_ERROR",
			message: "Entity application-phases not found",
			translations:
				[
					{ message: "Fase de candidatura não encontrada",language_id: 3, type: "APPLICATION-PHASES_NOT_FOUND_ERROR" },
					{ message: "Application phase not found", language_id: 4, type: "APPLICATION-PHASES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "REPORTS.TEMPLATES_NOT_FOUND_ERROR",
			message: "Entity reports.templates not found",
			translations:
				[
					{ message: "Modelo de relatório não encontrado",language_id: 3, type: "REPORTS.TEMPLATES_NOT_FOUND_ERROR" },
					{ message: "Report template not found", language_id: 4, type: "REPORTS.TEMPLATES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "RESERVATION_HAS_BEEN_CANCELLED",
			message: "This reservation already has been cancelled",
			translations:
				[
					{ message: "Esta reserva já foi cancelada",language_id: 3, type: "RESERVATION_HAS_BEEN_CANCELLED" },
					{ message: "This reservation already has been cancelled", language_id: 4, type: "RESERVATION_HAS_BEEN_CANCELLED" }
				]
		},
		{
			type: "SEND_USER_ID_IS_REQUIRED",
			message: "Nedd send the user_id",
			translations:
				[
					{ message: "Necessário enviar o id de utilizador",language_id: 3, type: "SEND_USER_ID_IS_REQUIRED" },
					{ message: "Need send the user id", language_id: 4, type: "SEND_USER_ID_IS_REQUIRED" }
				]
		},
		{
			type: "FOOD_DEVICE_WITHOUT_SERVICE",
			message: "The device not have a service associated",
			translations:
				[
					{ message: "O dispositivo não tem um serviço associado",language_id: 3, type: "FOOD_DEVICE_WITHOUT_SERVICE" },
					{ message: "The device not have a service associated", language_id: 4, type: "FOOD_DEVICE_WITHOUT_SERVICE" }
				]
		},
		{
			type: "CART_ALREADY_EXISTS",
			message: "Already exist a card in this context",
			translations:
				[
					{ message: "Já existe um cartão neste contexto",language_id: 3, type: "CART_ALREADY_EXISTS" },
					{ message: "Already exist a card in this context", language_id: 4, type: "CART_ALREADY_EXISTS" }
				]
		},
		{
			type: "USER_PASSWORD_EXPIRED",
			message: "Password expired please contact the administrator",
			translations:
				[
					{ message: "A senha expirou, por favor contacte o administrador",language_id: 3, type: "USER_PASSWORD_EXPIRED" },
					{ message: "Password expired please contact the administrator", language_id: 4, type: "USER_PASSWORD_EXPIRED" }
				]
		},
		{
			type: "ALREADY_PATMENT_FOR_MONTH",
			message: "There is already a payment this month",
			translations:
				[
					{ message: "Já existe um pagamento neste mês",language_id: 3, type: "ALREADY_PATMENT_FOR_MONTH" },
					{ message: "There is already a payment this month", language_id: 4, type: "ALREADY_PATMENT_FOR_MONTH" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_ERROR",
			message: "Already exist monthly report for send month, year and user_interest",
			translations:
				[
					{ message: "Já existe relatório mensal para envio de mês",language_id: 3, type: "SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_ERROR" },
					{ message: "Already exist monthly report for send month, year and user_interest", language_id: 4, type: "SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_ERROR" }
				]
		},
		{
			type: "BUS_APPLICATION_CHANGE_STATUS",
			message: "Impossible change the state",
			translations:
				[
					{ message: "Não é possível mudar de estado",language_id: 3, type: "BUS_APPLICATION_CHANGE_STATUS" },
					{ message: "Impossible change the state", language_id: 4, type: "BUS_APPLICATION_CHANGE_STATUS" }
				]
		},
		{
			type: "ORDERS_NOT_FOUND_ERROR",
			message: "Entity orders not found",
			translations:
				[
					{ message: "Pedido não encontrado",language_id: 3, type: "ORDERS_NOT_FOUND_ERROR" },
					{ message: "Order not found", language_id: 4, type: "ORDERS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "AUTH_PASSWORD_OR_PIN_REQUIRED",
			message: "'password' or 'pin' are required if an email/user_name is provided",
			translations:
				[
					{ message: "Se preencher o e-mail/nome é obrigatório preencher a 'senha' ou 'pin'",language_id: 3, type: "AUTH_PASSWORD_OR_PIN_REQUIRED" },
					{ message: "'password' or 'pin' are required if an email/user_name is provided", language_id: 4, type: "AUTH_PASSWORD_OR_PIN_REQUIRED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_ERROR",
			message: "Need add certificate file",
			translations:
				[
					{ message: "É necessário adicionar um documento de certificado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_ERROR" },
					{ message: "Need add certificate file", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_ERROR" }
				]
		},
		{
			type: "CART_ITEM_QUANTITY_EXCEEDED",
			message: "The quantity requested exceed the max quantity allowed",
			translations:
				[
					{ message: "A quantidade solicitada excede a quantidade máxima permitida",language_id: 3, type: "CART_ITEM_QUANTITY_EXCEEDED" },
					{ message: "The quantity requested exceed the max quantity allowed", language_id: 4, type: "CART_ITEM_QUANTITY_EXCEEDED" }
				]
		},
		{
			type: "COMPLEMENT_NOT_CORRESPOND_THE_ENTITY",
			message: "The Complement not correspond the entity",
			translations:
				[
					{ message: "O complemento não corresponde à entidade",language_id: 3, type: "COMPLEMENT_NOT_CORRESPOND_THE_ENTITY" },
					{ message: "The complement not correspond the entity", language_id: 4, type: "COMPLEMENT_NOT_CORRESPOND_THE_ENTITY" }
				]
		},
		{
			type: "ORDERS_CANCEL_MOVEMENT_ERROR",
			message: "Error in cancel order",
			translations:
				[
					{ message: "Ocorreu um erro ao cancelar o pedido",language_id: 3, type: "ORDERS_CANCEL_MOVEMENT_ERROR" },
					{ message: "There was an error canceling the order.", language_id: 4, type: "ORDERS_CANCEL_MOVEMENT_ERROR" }
				]
		},
		{
			type: "ORDERS_LINE_CANCEL_MOVEMENT_ERROR",
			message: "Error in remove item from order, cancel movement error",
			translations:
				[
					{ message: "Ocorreu um erro a remover o item do pedido",language_id: 3, type: "ORDERS_LINE_CANCEL_MOVEMENT_ERROR" },
					{ message: "An error occurred removing the item from the order", language_id: 4, type: "ORDERS_LINE_CANCEL_MOVEMENT_ERROR" }
				]
		},
		{
			type: "ORDER_LINE_SERVE_ERROR",
			message: "Error serving order item, item has already been served",
			translations:
				[
					{ message: "Erro ao servir o item do pedido. O item já foi servido",language_id: 3, type: "ORDER_LINE_SERVE_ERROR" },
					{ message: "Error serving order item, item has already been served", language_id: 4, type: "ORDER_LINE_SERVE_ERROR" }
				]
		},
		{
			type: "ORDERS_LINE_REMOVE_ITEM_ERROR",
			message: "Error removing item from order, item has already been removed",
			translations:
				[
					{ message: "Erro ao remover o item do pedido. O item já foi removido",language_id: 3, type: "ORDERS_LINE_REMOVE_ITEM_ERROR" },
					{ message: "Error removing item from order, item has already been removed", language_id: 4, type: "ORDERS_LINE_REMOVE_ITEM_ERROR" }
				]
		},
		{
			type: "AUTH_WRONG_ACTUAL_PIN",
			message: "Failed on validate the actual pin",
			translations:
				[
					{ message: "Falha ao validar o PIN atual",language_id: 3, type: "AUTH_WRONG_ACTUAL_PIN" },
					{ message: "Failed on validate the actual pin", language_id: 4, type: "AUTH_WRONG_ACTUAL_PIN" }
				]
		},
		{
			type: "SSO_INACTIVE",
			message: "The SSO Login is not active",
			translations:
				[
					{ message: "O Login SSO não está ativo",language_id: 3, type: "SSO_INACTIVE" },
					{ message: "The SSO Login is not active", language_id: 4, type: "SSO_INACTIVE" }
				]
		},
		{
			type: "RESERVATION_NOT_ASSOCIATED_LOGGED_USER",
			message: "This reservation is not associated to logged user",
			translations:
				[
					{ message: "Esta reserva não está associada ao utilizador logado",language_id: 3, type: "RESERVATION_NOT_ASSOCIATED_LOGGED_USER" },
					{ message: "This reservation is not associated to logged user", language_id: 4, type: "RESERVATION_NOT_ASSOCIATED_LOGGED_USER" }
				]
		},
		{
			type: "ASSOCIATION_NOT_ACTIVE",
			message: "This association is disable you can not active.",
			translations:
				[
					{ message: "Esta associação está desativada, não pode ativá-la",language_id: 3, type: "ASSOCIATION_NOT_ACTIVE" },
					{ message: "This association is disable you can not active.", language_id: 4, type: "ASSOCIATION_NOT_ACTIVE" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_NEED_RETURN_REASON_ERROR",
			message: "Unauthorized application status change need send return reason",
			translations:
				[
					{ message: "Não é possivel alterar o estado da candidatura, é necessário o motivo da devolução",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_NEED_RETURN_REASON_ERROR" },
					{ message: "It is not possible to change the status of the application, the reason for the return is required", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_NEED_RETURN_REASON_ERROR" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Alteração de estado não autorizada",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED" },
					{ message: "Unauthorized application status change", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_NO_USERS_IN_COLABORATION_ERROR",
			message: "Unauthorized experience status change don't have users in colaborations",
			translations:
				[
					{ message: "Alteração de estado não autorizada devido a não haver utilizadores em participação",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_NO_USERS_IN_COLABORATION_ERROR" },
					{ message: "Unauthorized experience status change don't have users in colaborations", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_NO_USERS_IN_COLABORATION_ERROR" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_APPLICATION_DATE_ERROR",
			message: "The expiration date must be equal to or less than the duration range",
			translations:
				[
					{ message: "A data de expiração deve de ser igual ou inferior ao intervalo de duração",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_APPLICATION_DATE_ERROR" },
					{ message: "The expiration date must be equal to or less than the duration range", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_APPLICATION_DATE_ERROR" }
				]
		},
		{
			type: "ERR_SQL_ERROR",
			message: "An error occorred on the sql query",
			translations:
				[
					{ message: "Ocorreu um erro inesperado, por favor contacte o administrador do sistema",language_id: 3, type: "ERR_SQL_ERROR" },
					{ message: "An unexpected error has occurred, please contact your system administrator", language_id: 4, type: "ERR_SQL_ERROR" }
				]
		},
		{
			type: "CONFIRMED_TOKEN_NOT_FOUND_ERROR",
			message: "Entity confirmed_token not found",
			translations:
				[
					{ message: "Token de confirmação não encontrado",language_id: 3, type: "CONFIRMED_TOKEN_NOT_FOUND_ERROR" },
					{ message: "Confirm token not found", language_id: 4, type: "CONFIRMED_TOKEN_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "VERIFICATION_TOKEN_NOT_FOUND_ERROR",
			message: "Entity verification_token not found",
			translations:
				[
					{ message: "Token de verificação não encontrado",language_id: 3, type: "VERIFICATION_TOKEN_NOT_FOUND_ERROR" },
					{ message: "Verification token not found", language_id: 4, type: "VERIFICATION_TOKEN_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "AUTH_VERIFICATION_TOKEN_EXPIRED",
			message: "The verification token has expired",
			translations:
				[
					{ message: "O Token de verificação expirou",language_id: 3, type: "AUTH_VERIFICATION_TOKEN_EXPIRED" },
					{ message: "The verification token has expired", language_id: 4, type: "AUTH_VERIFICATION_TOKEN_EXPIRED" }
				]
		},
		{
			type: "TICKETS_NOT_FOUND_ERROR",
			message: "Entity tickets not found",
			translations:
				[
					{ message: "Bilhete não encontrado",language_id: 3, type: "TICKETS_NOT_FOUND_ERROR" },
					{ message: "Ticket not found", language_id: 4, type: "TICKETS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOUNT_ID_REQUIRED",
			message: "'account_id' is required!",
			translations:
				[
					{ message: "O identificador da conta é obrigatório",language_id: 3, type: "ACCOUNT_ID_REQUIRED" },
					{ message: "Account identifier is required", language_id: 4, type: "ACCOUNT_ID_REQUIRED" }
				]
		},
		{
			type: "EXPERIENCE-USER-INTERESTS_NOT_FOUND_ERROR",
			message: "Entity experience-user-interests with id: 5 not found",
			translations:
				[
					{ message: "Entidade de experiência não encontrada",language_id: 3, type: "EXPERIENCE-USER-INTERESTS_NOT_FOUND_ERROR" },
					{ message: "The experiment entity was not found", language_id: 4, type: "EXPERIENCE-USER-INTERESTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "UBIKE_APPLICATION_FORM_INVALID_SUBJECT",
			message: "Application form invalid subject",
			translations:
				[
					{ message: "O assunto não é válido",language_id: 3, type: "UBIKE_APPLICATION_FORM_INVALID_SUBJECT" },
					{ message: "The subject is not valid", language_id: 4, type: "UBIKE_APPLICATION_FORM_INVALID_SUBJECT" }
				]
		},
		{
			type: "ERR_INVALID_MIME_TYPE",
			message: "Invalid Mime Type",
			translations:
				[
					{ message: "O ficheiro de tipo MIME é inválido",language_id: 3, type: "ERR_INVALID_MIME_TYPE" },
					{ message: "MIME type file is invalid", language_id: 4, type: "ERR_INVALID_MIME_TYPE" }
				]
		},
		{
			type: "PAYLOAD_TOO_LARGE",
			message: "Payload too large",
			translations:
				[
					{ message: "Erro do sistema, por favor contacte o administrador ",language_id: 3, type: "PAYLOAD_TOO_LARGE" },
					{ message: "System error, please contact the administrator", language_id: 4, type: "PAYLOAD_TOO_LARGE" }
				]
		},
		{
			type: "UBIKE_APPLICATION_FORM_NOT_DISPATCH",
			message: "Only can accept on [dispatch]  application form status",
			translations:
				[
					{ message: "Apenas pode ser aceite no estado 'despacho'",language_id: 3, type: "UBIKE_APPLICATION_FORM_NOT_DISPATCH" },
					{ message: "Only can accept on 'dispatch' application form status", language_id: 4, type: "UBIKE_APPLICATION_FORM_NOT_DISPATCH" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_HAS_COLABORATIONS",
			message: "Has active colaborations",
			translations:
				[
					{ message: "Não é possível cancelar esta candidatura porque tem colaborações ativas neste momento",language_id: 3, type: "SOCIAL_SCHOLARSHIP_HAS_COLABORATIONS" },
					{ message: "Cannot cancel because at the moment you have active collaborations.", language_id: 4, type: "SOCIAL_SCHOLARSHIP_HAS_COLABORATIONS" }
				]
		},
		{
			type: "AUTHROZIZATION_SCOPE_WITH_USER_GROUPS",
			message: "You have user groups with this scope associated",
			translations:
				[
					{ message: "Esta permissão tem grupos de utilizadores associados",language_id: 3, type: "AUTHROZIZATION_SCOPE_WITH_USER_GROUPS" },
					{ message: "You have user groups with this scope associated", language_id: 4, type: "AUTHROZIZATION_SCOPE_WITH_USER_GROUPS" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_ALREADY_EXIST",
			message: "This user have a interest manifest active in this experience",
			translations:
				[
					{ message: "Este utilizador tem uma manifestação de interesse ativa nesta oferta",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_ALREADY_EXIST" },
					{ message: "This user have a interest manifest active in this experience", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_ALREADY_EXIST" }
				]
		},
		{
			type: "COMMUNICATION_TARGET_POST_BAD_STATUS",
			message: "Unauthorized target post status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado da publicação direccionada",language_id: 3, type: "COMMUNICATION_TARGET_POST_BAD_STATUS" },
					{ message: "You are not allowed to change the status of the targeted post", language_id: 4, type: "COMMUNICATION_TARGET_POST_BAD_STATUS" }
				]
		},
		{
			type: "DELARATION_ID_FROM_REQUIRED",
			message: "'declaration_id' is required!",
			translations:
				[
					{ message: "O identificador da declaração é obrigatório",language_id: 3, type: "DELARATION_ID_FROM_REQUIRED" },
					{ message: "The declaration identifier is mandatory", language_id: 4, type: "DELARATION_ID_FROM_REQUIRED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_FORBIDDEN",
			message: "Unauthorized access to data",
			translations:
				[
					{ message: "Acesso não autorizado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_FORBIDDEN" },
					{ message: "Unauthorized access to data", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_FORBIDDEN" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_COLABORATIONS",
			message: "Has active colaborations",
			translations:
				[
					{ message: "Tem colaborações ativas",language_id: 3, type: "SOCIAL_SCHOLARSHIP_COLABORATIONS" },
					{ message: "Has active colaborations", language_id: 4, type: "SOCIAL_SCHOLARSHIP_COLABORATIONS" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_FORBIDDEN",
			message: "Unauthorized access to experience, not advisor or responsible",
			translations:
				[
					{ message: "Não pode aceder a esta oferta, não é o orientador nem o responsável",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_FORBIDDEN" },
					{ message: "You cannot access this offer, you are not the advisor or responsible", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_FORBIDDEN" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_APPLICATION_FORBIDDEN",
			message: "Unauthorized access to data",
			translations:
				[
					{ message: "Acesso não autorizado",language_id: 3, type: "SOCIAL_SCHOLARSHIP_APPLICATION_FORBIDDEN" },
					{ message: "Unauthorized access to data", language_id: 4, type: "SOCIAL_SCHOLARSHIP_APPLICATION_FORBIDDEN" }
				]
		},
		{
			type: "CHARGE_AMOUNT_LOWER_THAN_ALLOWED",
			message: "Charge amount is lower than allowed for this payment method",
			translations:
				[
					{ message: "O valor de carregamento é menor que o permitido para este método de carregamento",language_id: 3, type: "CHARGE_AMOUNT_LOWER_THAN_ALLOWED" },
					{ message: "The upload value is less than allowed for this upload method", language_id: 4, type: "CHARGE_AMOUNT_LOWER_THAN_ALLOWED" }
				]
		},
		{
			type: "CHARGE_AMOUNT_GREATER_THAN_ALLOWED",
			message: "Charge amount is greater than allowed for this payment method",
			translations:
				[
					{ message: "O valor de carregamento é maior que o permitido para este método de carregamento",language_id: 3, type: "CHARGE_AMOUNT_GREATER_THAN_ALLOWED" },
					{ message: "The upload value is greater than allowed for this upload method", language_id: 4, type: "CHARGE_AMOUNT_GREATER_THAN_ALLOWED" }
				]
		},
		{
			type: "VOLUNTEERING_APPLICATION_UNAUTHORIZED",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Não tem permissão para alteração do estado",language_id: 3, type: "VOLUNTEERING_APPLICATION_UNAUTHORIZED" },
					{ message: "Not allowed to change status", language_id: 4, type: "VOLUNTEERING_APPLICATION_UNAUTHORIZED" }
				]
		},
		{
			type: "VOLUNTEERING_ATTENDANCE_FORBIDDEN",
			message: "Unauthorized access to data",
			translations:
				[
					{ message: "Acesso não autorizado",language_id: 3, type: "VOLUNTEERING_ATTENDANCE_FORBIDDEN" },
					{ message: "Unauthorized access to data", language_id: 4, type: "VOLUNTEERING_ATTENDANCE_FORBIDDEN" }
				]
		},
		{
			type: "INTERESTS_NOT_FOUND_ERROR",
			message: "Entity experience-user-interests not found",
			translations:
				[
					{ message: "Manifestação de interesse na oferta não encontrada",language_id: 3, type: "INTERESTS_NOT_FOUND_ERROR" },
					{ message: "Experience user interest not found", language_id: 4, type: "INTERESTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "NO_OPEN_APPLICATION_PHASE",
			message: "No open application phase",
			translations:
				[
					{ message: "Não foi encontrada nenhuma fase de candidatura aberta",language_id: 3, type: "NO_OPEN_APPLICATION_PHASE" },
					{ message: "No open application phase was found", language_id: 4, type: "NO_OPEN_APPLICATION_PHASE" }
				]
		},
		{
			type: "INFRASTRUCTURE_ROOM_REMOVE_DEPENDENT_SERVICES",
			message: "You need to remove dependent services first",
			translations:
				[
					{ message: "É necessário remover os serviços dependentes",language_id: 3, type: "INFRASTRUCTURE_ROOM_REMOVE_DEPENDENT_SERVICES" },
					{ message: "You need to remove dependent services first", language_id: 4, type: "INFRASTRUCTURE_ROOM_REMOVE_DEPENDENT_SERVICES" }
				]
		},
		{
			type: "PAYMENT-METHODS_NOT_FOUND_ERROR",
			message: "Entity payment-methods with id: e9d4ad01-c1c8-4ac7-82f3-a373b08d030c not found",
			translations:
				[
					{ message: "Método de pagamento não encontrado",language_id: 3, type: "PAYMENT-METHODS_NOT_FOUND_ERROR" },
					{ message: "Payment method not found", language_id: 4, type: "PAYMENT-METHODS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "LINKS_NOT_FOUND_ERROR",
			message: "Entity links with id: 2 not found",
			translations:
				[
					{ message: "Ligação não encontrada",language_id: 3, type: "LINKS_NOT_FOUND_ERROR" },
					{ message: "Link not found", language_id: 4, type: "LINKS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "VOLUNTEERING_HAS_COLABORATIONS",
			message: "Has active colaborations",
			translations:
				[
					{ message: "Tem colaborações ativas",language_id: 3, type: "VOLUNTEERING_HAS_COLABORATIONS" },
					{ message: "Has active colaborations", language_id: 4, type: "VOLUNTEERING_HAS_COLABORATIONS" }
				]
		},
		{
			type: "AUTH_INATIVE_USER",
			message: "The user is not active",
			translations:
				[
					{ message: "O utilizador não está ativo",language_id: 3, type: "AUTH_INATIVE_USER" },
					{ message: "The user is not active", language_id: 4, type: "AUTH_INATIVE_USER" }
				]
		},
		{
			type: "STUDENT_NUMBER_ALREADY_EXIST",
			message: "Already exist a user with that student_number",
			translations:
				[
					{ message: "Já existe um utilizador com este número",language_id: 3, type: "STUDENT_NUMBER_ALREADY_EXIST" },
					{ message: "There is already a user with that number", language_id: 4, type: "STUDENT_NUMBER_ALREADY_EXIST" }
				]
		},
		{
			type: "EMAIL_ALREADY_EXIST",
			message: "Already exist a user with that email",
			translations:
				[
					{ message: "Já existe um utilizador com este email",language_id: 3, type: "EMAIL_ALREADY_EXIST" },
					{ message: "Already exist a user with this email", language_id: 4, type: "EMAIL_ALREADY_EXIST" }
				]
		},
		{
			type: "RFID_ALREADY_EXIST",
			message: "Already exist a user with that rfid",
			translations:
				[
					{ message: "Já existe um utilizador com este RFID associado",language_id: 3, type: "RFID_ALREADY_EXIST" },
					{ message: "Already exist a user with this rfid associated", language_id: 4, type: "RFID_ALREADY_EXIST" }
				]
		},
		{
			type: "USER_NAME_ALREADY_EXIST",
			message: "Already exist a user with that user_name",
			translations:
				[
					{ message: "Já existe um utilizador com este 'Nome de utilizador'",language_id: 3, type: "USER_NAME_ALREADY_EXIST" },
					{ message: "Already exist a user with this 'User name'", language_id: 4, type: "USER_NAME_ALREADY_EXIST" }
				]
		},
		{
			type: "LOCALS_IDS_NOT_FOUND",
			message: "There are no locals with these ids: ,,,",
			translations:
				[
					{ message: "Locais não encontrados",language_id: 3, type: "LOCALS_IDS_NOT_FOUND" },
					{ message: "Locations not found", language_id: 4, type: "LOCALS_IDS_NOT_FOUND" }
				]
		},
		{
			type: "LOCALS_REQUIRED",
			message: "'locals' is required!",
			translations:
				[
					{ message: "Os locais são obrigatórios",language_id: 3, type: "LOCALS_REQUIRED" },
					{ message: "Locals are required", language_id: 4, type: "LOCALS_REQUIRED" }
				]
		},
		{
			type: "DAY_ID_ATREADY_EXIST",
			message: "The day ID 0 is already exist in this type day",
			translations:
				[
					{ message: "Este dia já existe nos tipos de dias",language_id: 3, type: "DAY_ID_ATREADY_EXIST" },
					{ message: "This day already exists in the types of days", language_id: 4, type: "DAY_ID_ATREADY_EXIST" }
				]
		},
		{
			type: "DAYS_GROUP_NOT_FOUND_ERROR",
			message: "Entity days_group with id: 6 not found",
			translations:
				[
					{ message: "Grupo de dias não encontrado",language_id: 3, type: "DAYS_GROUP_NOT_FOUND_ERROR" },
					{ message: "Group of days not found", language_id: 4, type: "DAYS_GROUP_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "LINK_SAME_ROUTES",
			message: "You cannot create a link to the same route",
			translations:
				[
					{ message: "Não é possivel criar uma ligação para a mesma rota",language_id: 3, type: "LINK_SAME_ROUTES" },
					{ message: "It is not possible to create a link to the same route", language_id: 4, type: "LINK_SAME_ROUTES" }
				]
		},
		{
			type: "BUS_TYPE_DAY_IN_USE_GROUPS_DAYS",
			message: "This type day is using in groups_day",
			translations:
				[
					{ message: "Este tipo de dia já está a ser utilizado",language_id: 3, type: "BUS_TYPE_DAY_IN_USE_GROUPS_DAYS" },
					{ message: "This type of day is already being used", language_id: 4, type: "BUS_TYPE_DAY_IN_USE_GROUPS_DAYS" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICAION_NOT_FOUND",
			message: "No active application finded in accommodation",
			translations:
				[
					{ message: "Não foi encontrada nenhuma candidatura ativa no alojamento",language_id: 3, type: "ACCOMMODATION_APPLICAION_NOT_FOUND" },
					{ message: "No active application finded in accommodation", language_id: 4, type: "ACCOMMODATION_APPLICAION_NOT_FOUND" }
				]
		},
		{
			type: "AUTH_STUDENT_NUMBER_ALREADY_EXIST",
			message: "Already exist a user with that student_number",
			translations:
				[
					{ message: "Já existe um utilizador com este número",language_id: 3, type: "AUTH_STUDENT_NUMBER_ALREADY_EXIST" },
					{ message: "Already exist a user with this user number", language_id: 4, type: "AUTH_STUDENT_NUMBER_ALREADY_EXIST" }
				]
		},
		{
			type: "AUTH_EMAIL_ALREADY_EXIST",
			message: "Already exist a user with that email",
			translations:
				[
					{ message: "Já existe um utilizador com este email",language_id: 3, type: "AUTH_EMAIL_ALREADY_EXIST" },
					{ message: "Already exist a user with this email", language_id: 4, type: "AUTH_EMAIL_ALREADY_EXIST" }
				]
		},
		{
			type: "AUTH_USER_NAME_ALREADY_EXIST",
			message: "Already exist a user with that user_name",
			translations:
				[
					{ message: "Já existe um utilizador com este 'Nome de utilizador'",language_id: 3, type: "AUTH_USER_NAME_ALREADY_EXIST" },
					{ message: "Already exist a user with this 'User name'", language_id: 4, type: "AUTH_USER_NAME_ALREADY_EXIST" }
				]
		},
		{
			type: "AUTH_RFID_ALREADY_EXIST",
			message: "Already exist a user with that rfid",
			translations:
				[
					{ message: "Já existe um utilizador com este RFID associado",language_id: 3, type: "AUTH_RFID_ALREADY_EXIST" },
					{ message: "Already exist a user with this rfid associated", language_id: 4, type: "AUTH_RFID_ALREADY_EXIST" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NUM_WEEKLY_SMALLER",
			message: "The 'number weekly hours' need smaller 'total_hours_estimation'",
			translations:
				[
					{ message: "O número de horas semanais tem que ser menor que a estimativa total de horas",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NUM_WEEKLY_SMALLER" },
					{ message: "The number of weekly hours must be less than the estimated total hours", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NUM_WEEKLY_SMALLER" }
				]
		},
		{
			type: "EMERGENCY_FUND_USER_DOES_NOT_HAVE_ANY_ATIVE_APPLICATION",
			message: "User does not have any active application",
			translations:
				[
					{ message: "O utilizador não tem nenhuma candidatura ativa",language_id: 3, type: "EMERGENCY_FUND_USER_DOES_NOT_HAVE_ANY_ATIVE_APPLICATION" },
					{ message: "User does not have any active application", language_id: 4, type: "EMERGENCY_FUND_USER_DOES_NOT_HAVE_ANY_ATIVE_APPLICATION" }
				]
		},
		{
			type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_STATUS_CHANGE",
			message: "Not allowed application status change",
			translations:
				[
					{ message: "Não é possivel alterar os estado da candidatura",language_id: 3, type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_STATUS_CHANGE" },
					{ message: "Not allowed application status change", language_id: 4, type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_STATUS_CHANGE" }
				]
		},
		{
			type: "EMERGENCY_FUND_INVALID_APPLICATION_STATUS_FOR_INTENDED_OPERATION",
			message: "Invalid application status for the intended operation",
			translations:
				[
					{ message: "O estado da candidatura não lhe permite efetuar a operação",language_id: 3, type: "EMERGENCY_FUND_INVALID_APPLICATION_STATUS_FOR_INTENDED_OPERATION" },
					{ message: "The application status does not allow you to carry out the operation", language_id: 4, type: "EMERGENCY_FUND_INVALID_APPLICATION_STATUS_FOR_INTENDED_OPERATION" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_USER_UNAUTHORIZED",
			message: "User without authorization for application status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado da candidatura",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_USER_UNAUTHORIZED" },
					{ message: "Not allowed to change application status", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_USER_UNAUTHORIZED" }
				]
		},
		{
			type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION",
			message: "You have an active application",
			translations:
				[
					{ message: "Tem uma candidatura ativa",language_id: 3, type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION" },
					{ message: "You have an active application", language_id: 4, type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION" }
				]
		},
		{
			type: "EMERGENCY_FUND_NOT_ALLOWED_EXPENSE_STATUS_CHANGE",
			message: "Not allowed expense status change",
			translations:
				[
					{ message: "Não é permitido a alteração do estado da despesa",language_id: 3, type: "EMERGENCY_FUND_NOT_ALLOWED_EXPENSE_STATUS_CHANGE" },
					{ message: "Not allowed expense status change", language_id: 4, type: "EMERGENCY_FUND_NOT_ALLOWED_EXPENSE_STATUS_CHANGE" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_MANDATORY",
			message: "Change of application status not allowed, it is necessary to schedule interview(s)",
			translations:
				[
					{ message: "A alteração do estado da candidatura não é permitido, é necessário agendar entrevista(s)",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_MANDATORY" },
					{ message: "Change of application status not allowed, it is necessary to schedule interview(s)", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_MANDATORY" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEWS_CONCLUDED_MANDATORY",
			message: "Change of application status not allowed, all interviews must be concluded",
			translations:
				[
					{ message: "A alteração do estado da candidatura não é permitido, todas as entrevistas devem estar concluídas",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEWS_CONCLUDED_MANDATORY" },
					{ message: "Change of application status not allowed, all interviews must be concluded", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEWS_CONCLUDED_MANDATORY" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_REPORT_MANDATORY",
			message: "Change of application status not allowed, it is necessary to report(s) to the interview(s)",
			translations:
				[
					{ message: "A alteração do estado da candidatura não permitido, é necessário adicionar relatório(s) à(s) entrevista(s)",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_REPORT_MANDATORY" },
					{ message: "Change of application status not allowed, it is necessary to report(s) to the interview(s)", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEW_REPORT_MANDATORY" }
				]
		},
		{
			type: "EMERGENCY_FUND_INTERVIEW_ALREADY_CONCLUDED_OR_NOT_FOUND",
			message: "The interview doesn't exist or is already concluded",
			translations:
				[
					{ message: "A entrevista não existe ou já está concluída",language_id: 3, type: "EMERGENCY_FUND_INTERVIEW_ALREADY_CONCLUDED_OR_NOT_FOUND" },
					{ message: "The interview doesn't exist or is already concluded", language_id: 4, type: "EMERGENCY_FUND_INTERVIEW_ALREADY_CONCLUDED_OR_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_STATUS_ALL_INTERVIEWS_MANDATORY",
			message: "Change of application status not allowed, it is necessary to pass all the interviews in the input parameters",
			translations:
				[
					{ message: "A alteração do estado da candidatura não é permitido, é necessário ter todas as entrevistas",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_STATUS_ALL_INTERVIEWS_MANDATORY" },
					{ message: "Changing the status of the application is not allowed, all interviews are required", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_STATUS_ALL_INTERVIEWS_MANDATORY" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_STATUS_REPLIED_COMPLAINS_MANDATORY",
			message: "Change of application status not allowed, all complains must be replied",
			translations:
				[
					{ message: "A alteração do estado da candidatura não é permitido, deverá responder a todas as reclamações",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_STATUS_REPLIED_COMPLAINS_MANDATORY" },
					{ message: "Change of application status not allowed, all complains must be replied", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_STATUS_REPLIED_COMPLAINS_MANDATORY" }
				]
		},
		{
			type: "EMERGENCY_FUND_EXPENSE_INVALID_STATUS_OR_NOT_FOUND",
			message: "The expense doesn't exist or is status is invalid for the intended operation",
			translations:
				[
					{ message: "A despesa não existe ou o estado desta não lhe permite efetuar a operação",language_id: 3, type: "EMERGENCY_FUND_EXPENSE_INVALID_STATUS_OR_NOT_FOUND" },
					{ message: "The expense does not exist or its status does not allow you to carry out the operation", language_id: 4, type: "EMERGENCY_FUND_EXPENSE_INVALID_STATUS_OR_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED_OR_NOT_FOUND",
			message: "The complain doesn't exist or is already replied",
			translations:
				[
					{ message: "A reclamação não existe ou já está respondida",language_id: 3, type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED_OR_NOT_FOUND" },
					{ message: "The complain doesn't exist or is already replied", language_id: 4, type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED_OR_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED",
			message: "The complain is already replied",
			translations:
				[
					{ message: "A reclamação já está respondida",language_id: 3, type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED" },
					{ message: "The complain is already replied", language_id: 4, type: "EMERGENCY_FUND_COMPLAIN_ALREADY_REPLIED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_CREATE_USER_ERROR",
			message: "Error create user",
			translations:
				[
					{ message: "Erro ao criar utilizador",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_CREATE_USER_ERROR" },
					{ message: "Error create user", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_CREATE_USER_ERROR" }
				]
		},
		{
			type: "BUS_ROUTE_LINK_ALREADY_EXIST",
			message: "Router link alreday exist",
			translations:
				[
					{ message: "A ligação da rota já existe",language_id: 3, type: "BUS_ROUTE_LINK_ALREADY_EXIST" },
					{ message: "Router link alreday exist", language_id: 4, type: "BUS_ROUTE_LINK_ALREADY_EXIST" }
				]
		},
		{
			type: "TARIFFS_NOT_FOUND_ERROR",
			message: "Entity tariffs with id: 5 not found",
			translations:
				[
					{ message: "Tarifário não encontrado",language_id: 3, type: "TARIFFS_NOT_FOUND_ERROR" },
					{ message: "Tariff not found", language_id: 4, type: "TARIFFS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "PRIVATE_ACCOMMODATION_RESPONSE_ALREADY_EXIST",
			message: "Already exist response for this complain",
			translations:
				[
					{ message: "Esta reclamação já tem uma resposta",language_id: 3, type: "PRIVATE_ACCOMMODATION_RESPONSE_ALREADY_EXIST" },
					{ message: "Already exist response for this complain", language_id: 4, type: "PRIVATE_ACCOMMODATION_RESPONSE_ALREADY_EXIST" }
				]
		},
		{
			type: "DEVICE_ALREADY_HAS_CPUID",
			message: "The CPUID is immutable and cant be changed",
			translations:
				[
					{ message: "O CPUID não pode ser alterado",language_id: 3, type: "DEVICE_ALREADY_HAS_CPUID" },
					{ message: "The CPUID is immutable and cant be changed", language_id: 4, type: "DEVICE_ALREADY_HAS_CPUID" }
				]
		},
		{
			type: "ERROR_PAYING_PAYMENT_NOT_PENDING",
			message: "Payment must be on a pending status",
			translations:
				[
					{ message: "O pagamento deve estar em um estado pendente",language_id: 3, type: "ERROR_PAYING_PAYMENT_NOT_PENDING" },
					{ message: "Payment must be on a pending status", language_id: 4, type: "ERROR_PAYING_PAYMENT_NOT_PENDING" }
				]
		},
		{
			type: "ROOMS_TRANSLATIONS_IS_REQUIRED",
			message: "Rooms translations is required",
			translations:
				[
					{ message: "A tradução dos quartos é obrigatória",language_id: 3, type: "ROOMS_TRANSLATIONS_IS_REQUIRED" },
					{ message: "Rooms translations is required", language_id: 4, type: "ROOMS_TRANSLATIONS_IS_REQUIRED" }
				]
		},
		{
			type: "PAYMENT_TPA_CONFIRM_INVALID",
			message: "The payment callback is not valid",
			translations:
				[
					{ message: "O retorno do pagamento não é válido",language_id: 3, type: "PAYMENT_TPA_CONFIRM_INVALID" },
					{ message: "The payment callback is not valid", language_id: 4, type: "PAYMENT_TPA_CONFIRM_INVALID" }
				]
		},
		{
			type: "CHARGE_PAYMENT_METHOD_NOT_ALLOWED",
			message: "Payment method not allowed",
			translations:
				[
					{ message: "O método de pagamento não é permitido",language_id: 3, type: "CHARGE_PAYMENT_METHOD_NOT_ALLOWED" },
					{ message: "Payment method not allowed", language_id: 4, type: "CHARGE_PAYMENT_METHOD_NOT_ALLOWED" }
				]
		},
		{
			type: "TRIPS_NOT_FOUND_ERROR",
			message: "Entity trips with id: 27 not found",
			translations:
				[
					{ message: "Viagem não encontrada",language_id: 3, type: "TRIPS_NOT_FOUND_ERROR" },
					{ message: "Trip not found", language_id: 4, type: "TRIPS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "STOCK_ALREADY_EXIST_FOR_THIS_LOTE",
			message: "Already exist stock for this lote",
			translations:
				[
					{ message: "Já existe stock para este lote",language_id: 3, type: "STOCK_ALREADY_EXIST_FOR_THIS_LOTE" },
					{ message: "Already exist stock for this lote", language_id: 4, type: "STOCK_ALREADY_EXIST_FOR_THIS_LOTE" }
				]
		},
		{
			type: "EMPTY_CART",
			message: "Empty cart",
			translations:
				[
					{ message: "Carrinho vazio",language_id: 3, type: "EMPTY_CART" },
					{ message: "Empty cart", language_id: 4, type: "EMPTY_CART" }
				]
		},
		{
			type: "CC_CANCEL_PRODUCT_ITEM_VALUE_GREATER",
			message: "The total cancelled item value is greater than the original movement item value",
			translations:
				[
					{ message: "O valor total do item cancelado é maior do que o valor do item de movimento original",language_id: 3, type: "CC_CANCEL_PRODUCT_ITEM_VALUE_GREATER" },
					{ message: "The total cancelled item value is greater than the original movement item value", language_id: 4, type: "CC_CANCEL_PRODUCT_ITEM_VALUE_GREATER" }
				]
		},
		{
			type: "IS_NOT_AVAILABLE_FOR_ANNULLAMENT",
			message: "Is not available for annullament",
			translations:
				[
					{ message: "Não está disponível para anulação",language_id: 3, type: "IS_NOT_AVAILABLE_FOR_ANNULLAMENT" },
					{ message: "Is not available for annullament", language_id: 4, type: "IS_NOT_AVAILABLE_FOR_ANNULLAMENT" }
				]
		},
		{
			type: "ACCOMMODATION_PHASE_INVALID_OUTOFDATE",
			message: "The out of date must be between start date and end date",
			translations:
				[
					{ message: "A data deve estar entre a data de início e data de fim",language_id: 3, type: "ACCOMMODATION_PHASE_INVALID_OUTOFDATE" },
					{ message: "The out of date must be between start date and end date", language_id: 4, type: "ACCOMMODATION_PHASE_INVALID_OUTOFDATE" }
				]
		},
		{
			type: "AUTH_FAULT_PASSWORD",
			message: "The password does not meet the requirements",
			translations:
				[
					{ message: "A senha não cumpre com os requisitos",language_id: 3, type: "AUTH_FAULT_PASSWORD" },
					{ message: "The password does not meet the requirements", language_id: 4, type: "AUTH_FAULT_PASSWORD" }
				]
		},
		{
			type: "NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
			message: "No permission to access this wharehouse",
			translations:
				[
					{ message: "Não tem permissão para aceder a este armazém",language_id: 3, type: "NO_PERMISSION_TO_ACCESS_WHAREHOUSE" },
					{ message: "No permission to access this wharehouse", language_id: 4, type: "NO_PERMISSION_TO_ACCESS_WHAREHOUSE" }
				]
		},
		{
			type: "NO_ACTIVE_APPLICATION",
			message: "User don't have a active application",
			translations:
				[
					{ message: "Não tem uma candidatura ativa",language_id: 3, type: "NO_ACTIVE_APPLICATION" },
					{ message: "You do not have an active application", language_id: 4, type: "NO_ACTIVE_APPLICATION" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_EXTRA_CHANGE_INVALID_DATE",
			message: "User only can change extras on next or more months",
			translations:
				[
					{ message: "Só pode alterar os extras nos próximos ou mais meses",language_id: 3, type: "ACCOMMODATION_APPLICATION_EXTRA_CHANGE_INVALID_DATE" },
					{ message: "You can only change add-ons in the next or more months", language_id: 4, type: "ACCOMMODATION_APPLICATION_EXTRA_CHANGE_INVALID_DATE" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_INVALID_DATE",
			message: "User only can change extras on next or more months",
			translations:
				[
					{ message: "Só pode alterar os extras nos próximos ou mais meses",language_id: 3, type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_INVALID_DATE" },
					{ message: "You can only change add-ons in the next or more months", language_id: 4, type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_INVALID_DATE" }
				]
		},
		{
			type: "USER-INTEREST-INTERVIEWS_NOT_FOUND_ERROR",
			message: "Entity user-interest-interviews with id: 43 not found",
			translations:
				[
					{ message: "Entrevista da manifestação de interesse nao encontrada",language_id: 3, type: "USER-INTEREST-INTERVIEWS_NOT_FOUND_ERROR" },
					{ message: "Expression of interest interview not found", language_id: 4, type: "USER-INTEREST-INTERVIEWS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_ALREADY_EXISTS",
			message: "User already have ongoing room changes requests",
			translations:
				[
					{ message: "Já tem uma alteração de quarto a decorrer",language_id: 3, type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_ALREADY_EXISTS" },
					{ message: "You already have a room change in progress", language_id: 4, type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_ALREADY_EXISTS" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_INVALID_DATE",
			message: "User only can change contract on next or more months",
			translations:
				[
					{ message: "Só pode alterar o contrato nos próximos ou mais meses",language_id: 3, type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_INVALID_DATE" },
					{ message: "You can only change the contract in the next or more months", language_id: 4, type: "ACCOMMODATION_APPLICATION_ROOM_CHANGE_INVALID_DATE" }
				]
		},
		{
			type: "ACCOMMODATION_PERMUTE_USER_TIN_NOT_FOUND",
			message: "User with provided tin dont have a application in provided room",
			translations:
				[
					{ message: "O utilizador com o NIF fornecido não tem candidatura para um quarto",language_id: 3, type: "ACCOMMODATION_PERMUTE_USER_TIN_NOT_FOUND" },
					{ message: "The user with the TIN provided does not apply for a room", language_id: 4, type: "ACCOMMODATION_PERMUTE_USER_TIN_NOT_FOUND" }
				]
		},
		{
			type: "SOCIAL_SHOLARSHIP_EXPERIENCE_NO_COLABORATIONS_ERROR",
			message: "Unauthorized application status change",
			translations:
				[
					{ message: "Não tem permissão para alterar o estado, não existem alunos em colaboração nesta oferta",language_id: 3, type: "SOCIAL_SHOLARSHIP_EXPERIENCE_NO_COLABORATIONS_ERROR" },
					{ message: "Unauthorized experience status change dont have users in colaborations", language_id: 4, type: "SOCIAL_SHOLARSHIP_EXPERIENCE_NO_COLABORATIONS_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_REPORT_ERROR",
			message: "Need add certificate and report avaliation file",
			translations:
				[
					{ message: "É necessário adicionar certificado e relatório de avaliação",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_REPORT_ERROR" },
					{ message: "Need add certificate and report avaliation file", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_REPORT_ERROR" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_CLOSED",
			message: "Appointment closed!",
			translations:
				[
					{ message: "Marcação concluída",language_id: 3, type: "HEALTH_APPOINTMENT_CLOSED" },
					{ message: "Appointment closed", language_id: 4, type: "HEALTH_APPOINTMENT_CLOSED" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_ALREADY_CHANGED",
			message: "Appointment already changed!",
			translations:
				[
					{ message: "Marcação já alterada",language_id: 3, type: "HEALTH_APPOINTMENT_ALREADY_CHANGED" },
					{ message: "Appointment already changed", language_id: 4, type: "HEALTH_APPOINTMENT_ALREADY_CHANGED" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_DATE_LESS_TODAY",
			message: "Appointment Date less than today!",
			translations:
				[
					{ message: "A data da marcação tem que ser menor que hoje",language_id: 3, type: "HEALTH_APPOINTMENT_DATE_LESS_TODAY" },
					{ message: "Appointment Date less than today", language_id: 4, type: "HEALTH_APPOINTMENT_DATE_LESS_TODAY" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_NOT_CLOSED",
			message: "Appointment not closed!",
			translations:
				[
					{ message: "Marcação ainda não concluída",language_id: 3, type: "HEALTH_APPOINTMENT_NOT_CLOSED" },
					{ message: "Appointment not closed", language_id: 4, type: "HEALTH_APPOINTMENT_NOT_CLOSED" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_PENDING",
			message: "Appointment pending for approval!",
			translations:
				[
					{ message: "Marcação a espera de aprovação",language_id: 3, type: "HEALTH_APPOINTMENT_PENDING" },
					{ message: "Appointment pending for approval", language_id: 4, type: "HEALTH_APPOINTMENT_PENDING" }
				]
		},
		{
			type: "HEALTH_SCHEDULE_HOUR_NOT_AVAILABLE",
			message: "Schedule Hour not Available",
			translations:
				[
					{ message: "Hora agendada não disponível",language_id: 3, type: "HEALTH_SCHEDULE_HOUR_NOT_AVAILABLE" },
					{ message: "Schedule Hour not Available", language_id: 4, type: "HEALTH_SCHEDULE_HOUR_NOT_AVAILABLE" }
				]
		},
		{
			type: "HEALTH_SCHEDULE_DAY_NOT_AVAILABLE",
			message: "Schedule Day not Available",
			translations:
				[
					{ message: "Dia agendado não disponível",language_id: 3, type: "HEALTH_SCHEDULE_DAY_NOT_AVAILABLE" },
					{ message: "Schedule Day not Available", language_id: 4, type: "HEALTH_SCHEDULE_DAY_NOT_AVAILABLE" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_CHANGED",
			message: "Appointment not available!",
			translations:
				[
					{ message: "Marcação não disponivel",language_id: 3, type: "HEALTH_APPOINTMENT_CHANGED" },
					{ message: "Appointment not available!", language_id: 4, type: "HEALTH_APPOINTMENT_CHANGED" }
				]
		},
		{
			type: "HEALTH_SCHEDULE_NOT_AVAILABLE",
			message: "Schedule not Available",
			translations:
				[
					{ message: "Agenda não disponível",language_id: 3, type: "HEALTH_SCHEDULE_NOT_AVAILABLE" },
					{ message: "Schedule not Available", language_id: 4, type: "HEALTH_SCHEDULE_NOT_AVAILABLE" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_NOT_AVAILABLE",
			message: "Appointment not Available",
			translations:
				[
					{ message: "Marcação não disponível",language_id: 3, type: "HEALTH_APPOINTMENT_NOT_AVAILABLE" },
					{ message: "Appointment not Available", language_id: 4, type: "HEALTH_APPOINTMENT_NOT_AVAILABLE" }
				]
		},
		{
			type: "HEALTH_APPOINTMENT_TYPE_NOT_ALLOWED",
			message: "Appointment Type not Allowed",
			translations:
				[
					{ message: "Tipo de marcação não permitida",language_id: 3, type: "HEALTH_APPOINTMENT_TYPE_NOT_ALLOWED" },
					{ message: "Appointment Type not Allowed", language_id: 4, type: "HEALTH_APPOINTMENT_TYPE_NOT_ALLOWED" }
				]
		},
		{
			type: "HEALTH_ATTENDANCE_CLOSED",
			message: "Attendance closed",
			translations:
				[
					{ message: "Consulta fechada",language_id: 3, type: "HEALTH_ATTENDANCE_CLOSED" },
					{ message: "Attendance closed", language_id: 4, type: "HEALTH_ATTENDANCE_CLOSED" }
				]
		},
		{
			type: "HEALTH_COMPLAINT_CLOSED",
			message: "Complaint Closed!",
			translations:
				[
					{ message: "Reclmamação fechada",language_id: 3, type: "HEALTH_COMPLAINT_CLOSED" },
					{ message: "Complaint closed", language_id: 4, type: "HEALTH_COMPLAINT_CLOSED" }
				]
		},
		{
			type: "HEALTH_COMPLAINT_NOT_CLOSED",
			message: "There are an open complaint",
			translations:
				[
					{ message: "Existe uma reclamação ainda aberta",language_id: 3, type: "HEALTH_COMPLAINT_NOT_CLOSED" },
					{ message: "There are an open complaint", language_id: 4, type: "HEALTH_COMPLAINT_NOT_CLOSED" }
				]
		},
		{
			type: "HEALTH_PROFILE_NOT_MATCH",
			message: "Profile doesn't match!",
			translations:
				[
					{ message: "Perfil não corresponde",language_id: 3, type: "HEALTH_PROFILE_NOT_MATCH" },
					{ message: "Profile doesn't match", language_id: 4, type: "HEALTH_PROFILE_NOT_MATCH" }
				]
		},
		{
			type: "HEALTH_DATE_SCHEDULE_LESS",
			message: "Date is Less than today",
			translations:
				[
					{ message: "A data da agenda tem que ser menor que hoje",language_id: 3, type: "HEALTH_DATE_SCHEDULE_LESS" },
					{ message: "Date is Less than today", language_id: 4, type: "HEALTH_DATE_SCHEDULE_LESS" }
				]
		},
		{
			type: "HEALTH_ERROR_CREATE_DOCTOR",
			message: "Doctor creation failed",
			translations:
				[
					{ message: "Erro na criação do médico",language_id: 3, type: "HEALTH_ERROR_CREATE_DOCTOR" },
					{ message: "Doctor creation failed", language_id: 4, type: "HEALTH_ERROR_CREATE_DOCTOR" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_EXTERNAL",
			message: "Specialty is External!",
			translations:
				[
					{ message: "Especialidade Externa",language_id: 3, type: "HEALTH_SPECIALTY_EXTERNAL" },
					{ message: "Specialty is External", language_id: 4, type: "HEALTH_SPECIALTY_EXTERNAL" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_INACTIVE",
			message: "Specialty is Inactive!",
			translations:
				[
					{ message: "Especialidade Inactiva",language_id: 3, type: "HEALTH_SPECIALTY_INACTIVE" },
					{ message: "Specialty is Inactive", language_id: 4, type: "HEALTH_SPECIALTY_INACTIVE" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_WITH_DOCTOR",
			message: "You have medical specialties associated to this doctor",
			translations:
				[
					{ message: "Existe especialidades associadas ao médico",language_id: 3, type: "HEALTH_SPECIALTY_WITH_DOCTOR" },
					{ message: "You have medical specialties associated to this doctor", language_id: 4, type: "HEALTH_SPECIALTY_WITH_DOCTOR" }
				]
		},
		{
			type: "HEALTH_DOCTOR_EXISTS",
			message: "Doctor already exists",
			translations:
				[
					{ message: "Médico já existe",language_id: 3, type: "HEALTH_DOCTOR_EXISTS" },
					{ message: "Doctor already exists", language_id: 4, type: "HEALTH_DOCTOR_EXISTS" }
				]
		},
		{
			type: "HEALTH_DOCTOR_NOT_ASSOCIATED_TO_SPECIALTY",
			message: "Doctor not associated to this specialty",
			translations:
				[
					{ message: "Médico não associado à especialidade",language_id: 3, type: "HEALTH_DOCTOR_NOT_ASSOCIATED_TO_SPECIALTY" },
					{ message: "Doctor not associated to this specialty", language_id: 4, type: "HEALTH_DOCTOR_NOT_ASSOCIATED_TO_SPECIALTY" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_WITH_APPOINTMENTS",
			message: "You have open appointments associated to specialty",
			translations:
				[
					{ message: "Marcações não concluídas associadas à especialidade",language_id: 3, type: "HEALTH_SPECIALTY_WITH_APPOINTMENTS" },
					{ message: "You have open appointments associated to specialty", language_id: 4, type: "HEALTH_SPECIALTY_WITH_APPOINTMENTS" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_ACTIVE",
			message: "Specialty Active",
			translations:
				[
					{ message: "Especialidade ativa",language_id: 3, type: "HEALTH_SPECIALTY_ACTIVE" },
					{ message: "Specialty Active", language_id: 4, type: "HEALTH_SPECIALTY_ACTIVE" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_WITH_TYPE",
			message: "You have medical specialties associated",
			translations:
				[
					{ message: "Especialidades associadas a este tipo",language_id: 3, type: "HEALTH_SPECIALTY_WITH_TYPE" },
					{ message: "You have medical specialties associated", language_id: 4, type: "HEALTH_SPECIALTY_WITH_TYPE" }
				]
		},
		{
			type: "HEALTH_SPECIALTY_NO_PLACE",
			message: "No Place Defined",
			translations:
				[
					{ message: "Local da Especialidade não definido",language_id: 3, type: "HEALTH_SPECIALTY_NO_PLACE" },
					{ message: "No Place Defined", language_id: 4, type: "HEALTH_SPECIALTY_NO_PLACE" }
				]
		},
		{
			type: "HEALTH_PRICE_VARIATIONS_ASSOCIATED",
			message: "You have specialties associated",
			translations:
				[
					{ message: "Existem especialidades associadas a este preço",language_id: 3, type: "HEALTH_PRICE_VARIATIONS_ASSOCIATED" },
					{ message: "You have specialties associated", language_id: 4, type: "HEALTH_PRICE_VARIATIONS_ASSOCIATED" }
				]
		},
		{
			type: "VOLUNTEERING_USER_NOT_APPLICATION",
			message: "This user not have application",
			translations:
				[
					{ message: "Este utilizador não tem candidaturas",language_id: 3, type: "VOLUNTEERING_USER_NOT_APPLICATION" },
					{ message: "This user not have application", language_id: 4, type: "VOLUNTEERING_USER_NOT_APPLICATION" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_INTERESTED_ALREADY_EXIST",
			message: "The Experience User Interested already exists.",
			translations:
				[
					{ message: "A manifestação de interesse já existe",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_INTERESTED_ALREADY_EXIST" },
					{ message: "The expression of interest already exists", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_INTERESTED_ALREADY_EXIST" }
				]
		},
		{
			type: "VOLUNTEERING_COMPLAIN_RESPONSE_ERROR",
			message: "This complain have a response",
			translations:
				[
					{ message: "Esta reclamação já tem uma resposta",language_id: 3, type: "VOLUNTEERING_COMPLAIN_RESPONSE_ERROR" },
					{ message: "This complain have a response", language_id: 4, type: "VOLUNTEERING_COMPLAIN_RESPONSE_ERROR" }
				]
		},
		{
			type: "BUS_DECLARATION_CHANGE_STATUS",
			message: "Impossible change the state",
			translations:
				[
					{ message: "Não é possivel a alteração de estado",language_id: 3, type: "BUS_DECLARATION_CHANGE_STATUS" },
					{ message: "Impossible change the state", language_id: 4, type: "BUS_DECLARATION_CHANGE_STATUS" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_UNPROCESSED_BILLINGS",
			message: "Cannot close one application with unprocessed billings",
			translations:
				[
					{ message: "Não é possivel fechar uma candidatura que tem a faturação em processamento",language_id: 3, type: "ACCOMMODATION_APPLICATION_UNPROCESSED_BILLINGS" },
					{ message: "Cannot close one application with unprocessed billings", language_id: 4, type: "ACCOMMODATION_APPLICATION_UNPROCESSED_BILLINGS" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_NOT_FOUND",
			message: "No active application finded in accommodation",
			translations:
				[
					{ message: "Não existe nenhuma candidatura ativa no alojamento",language_id: 3, type: "ACCOMMODATION_APPLICATION_NOT_FOUND" },
					{ message: "No active application finded in accommodation", language_id: 4, type: "ACCOMMODATION_APPLICATION_NOT_FOUND" }
				]
		},
		{
			type: "SUBJECTS_NOT_FOUND_ERROR",
			message: "Entity subjects with id: 2 not found",
			translations:
				[
					{ message: "Fila não encontrada",language_id: 3, type: "SUBJECTS_NOT_FOUND_ERROR" },
					{ message: "Subject not found", language_id: 4, type: "SUBJECTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "BILLINGS_NOT_FOUND_ERROR",
			message: "Entity billings with id: 793 not found",
			translations:
				[
					{ message: "Faturamento não encontrado",language_id: 3, type: "BILLINGS_NOT_FOUND_ERROR" },
					{ message: "Billing not found", language_id: 4, type: "BILLINGS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_TARIFF_CHANGE_ALREADY_EXISTS",
			message: "User already have ongoing tariff changes requests",
			translations:
				[
					{ message: "Já tem alterações de tarifário a decorrer",language_id: 3, type: "ACCOMMODATION_APPLICATION_TARIFF_CHANGE_ALREADY_EXISTS" },
					{ message: "User already have ongoing tariff changes requests", language_id: 4, type: "ACCOMMODATION_APPLICATION_TARIFF_CHANGE_ALREADY_EXISTS" }
				]
		},
		{
			type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION_CHANGE",
			message: "You have an active application change",
			translations:
				[
					{ message: "Tem um pedido de alteração da candidatura ativa",language_id: 3, type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION_CHANGE" },
					{ message: "You have an active application change", language_id: 4, type: "EMERGENCY_FUND_USER_HAVE_ACTIVE_APLICATION_CHANGE" }
				]
		},
		{
			type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_CHANGE_STATUS_CHANGE",
			message: "Not allowed application change status change",
			translations:
				[
					{ message: "Não é permitido a alteração do estado da candidatura",language_id: 3, type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_CHANGE_STATUS_CHANGE" },
					{ message: "Not allowed application change status change", language_id: 4, type: "EMERGENCY_FUND_NOT_ALLOWED_APPLICATION_CHANGE_STATUS_CHANGE" }
				]
		},
		{
			type: "EMERGENCY_FUND_APLICATION_CHANGE_INVALID_STATUS_OR_NOT_FOUND",
			message: "The application change doesn't exist or is status is invalid for the intended operation",
			translations:
				[
					{ message: "O pedido de alteração de candidatura não existe ou o estado não lhe permite efetuar a operação",language_id: 3, type: "EMERGENCY_FUND_APLICATION_CHANGE_INVALID_STATUS_OR_NOT_FOUND" },
					{ message: "The application change doesn't exist or is status is invalid for the intended operation", language_id: 4, type: "EMERGENCY_FUND_APLICATION_CHANGE_INVALID_STATUS_OR_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_INTENDED_SUPPORT_NOT_FOUND_OR_ALREADY_UPDATED",
			message: "The intended support has not found or is already updated",
			translations:
				[
					{ message: "O apoio pretendido não foi encontrado ou já se encontra alterado",language_id: 3, type: "EMERGENCY_FUND_INTENDED_SUPPORT_NOT_FOUND_OR_ALREADY_UPDATED" },
					{ message: "The intended support has not found or is already updated", language_id: 4, type: "EMERGENCY_FUND_INTENDED_SUPPORT_NOT_FOUND_OR_ALREADY_UPDATED" }
				]
		},
		{
			type: "EMERGENCY_FUND_INTENDED_SUPPORT_ALREADY_EXISTS",
			message: "Intended support already exists",
			translations:
				[
					{ message: "O apoio pretendido já está atribuído",language_id: 3, type: "EMERGENCY_FUND_INTENDED_SUPPORT_ALREADY_EXISTS" },
					{ message: "Intended support already exists", language_id: 4, type: "EMERGENCY_FUND_INTENDED_SUPPORT_ALREADY_EXISTS" }
				]
		},
		{
			type: "EMERGENCY_FUND_APPLICATION_CHANGE_ALREADY_REPLIED",
			message: "The application change is already replied",
			translations:
				[
					{ message: "A resposta ao pedido de alteração de candidatura já foi efetuado",language_id: 3, type: "EMERGENCY_FUND_APPLICATION_CHANGE_ALREADY_REPLIED" },
					{ message: "The application change is already replied", language_id: 4, type: "EMERGENCY_FUND_APPLICATION_CHANGE_ALREADY_REPLIED" }
				]
		},
		{
			type: "EMERGENCY_FUND_NOT_ALLOWED_PAYMENT_MAP_STATUS_CHANGE",
			message: "Not allowed payment map status change",
			translations:
				[
					{ message: "Alteração do estado do mapa de pagamento não permitida",language_id: 3, type: "EMERGENCY_FUND_NOT_ALLOWED_PAYMENT_MAP_STATUS_CHANGE" },
					{ message: "Not allowed payment map status change", language_id: 4, type: "EMERGENCY_FUND_NOT_ALLOWED_PAYMENT_MAP_STATUS_CHANGE" }
				]
		},
		{
			type: "EMERGENCY_FUND_EXPENSE_ALREADY_IS_PAID_AND_IN_PAYMENT_MAP",
			message: "The expense is already included in a payment map and is already PAID",
			translations:
				[
					{ message: "A despesa já está associada a um mapa de pagamento e já se encontra paga",language_id: 3, type: "EMERGENCY_FUND_EXPENSE_ALREADY_IS_PAID_AND_IN_PAYMENT_MAP" },
					{ message: "The expense is already included in a payment map and is already PAID", language_id: 4, type: "EMERGENCY_FUND_EXPENSE_ALREADY_IS_PAID_AND_IN_PAYMENT_MAP" }
				]
		},
		{
			type: "EMERGENCY_FUND_INTENDED_SUPPORT_INVALID_INPUT",
			message: "The intended support has invalid data",
			translations:
				[
					{ message: "O apoio pretendido contém dados inválidos",language_id: 3, type: "EMERGENCY_FUND_INTENDED_SUPPORT_INVALID_INPUT" },
					{ message: "The intended support has invalid data", language_id: 4, type: "EMERGENCY_FUND_INTENDED_SUPPORT_INVALID_INPUT" }
				]
		},
		{
			type: "EMERGENCY_FUND_INTENDED_SUPPORT_EXPENSE_GREATER_THAN_LIMIT_VALUE",
			message: "The amount to pay for the expense is greater than support value limit",
			translations:
				[
					{ message: "O valor a pagar pela despesa é maior que o valor limite do apoio",language_id: 3, type: "EMERGENCY_FUND_INTENDED_SUPPORT_EXPENSE_GREATER_THAN_LIMIT_VALUE" },
					{ message: "The amount to pay for the expense is greater than support value limit", language_id: 4, type: "EMERGENCY_FUND_INTENDED_SUPPORT_EXPENSE_GREATER_THAN_LIMIT_VALUE" }
				]
		},
		{
			type: "EMERGENCY_FUND_PAYMENT_MAP_UPDATE_ERROR",
			message: "This payment map is in PAID status",
			translations:
				[
					{ message: "O mapa de pagamento está no estado pago",language_id: 3, type: "EMERGENCY_FUND_PAYMENT_MAP_UPDATE_ERROR" },
					{ message: "This payment map is in PAID status", language_id: 4, type: "EMERGENCY_FUND_PAYMENT_MAP_UPDATE_ERROR" }
				]
		},
		{
			type: "EMERGENCY_FUND_EXPENSES_NOT_FOUND",
			message: "There are no expenses to process",
			translations:
				[
					{ message: "Não existem despesas para processar",language_id: 3, type: "EMERGENCY_FUND_EXPENSES_NOT_FOUND" },
					{ message: "There are no expenses to process", language_id: 4, type: "EMERGENCY_FUND_EXPENSES_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_REPORT_ALREADY_EXISTS",
			message: "Report already exists",
			translations:
				[
					{ message: "O relatório já existente",language_id: 3, type: "EMERGENCY_FUND_REPORT_ALREADY_EXISTS" },
					{ message: "Report already exists", language_id: 4, type: "EMERGENCY_FUND_REPORT_ALREADY_EXISTS" }
				]
		},
		{
			type: "EMERGENCY_FUND_INVALID_INPUT_DATA",
			message: "Invalid input data",
			translations:
				[
					{ message: "Dados inválidos",language_id: 3, type: "EMERGENCY_FUND_INVALID_INPUT_DATA" },
					{ message: "Invalid input data", language_id: 4, type: "EMERGENCY_FUND_INVALID_INPUT_DATA" }
				]
		},
		{
			type: "EMERGENCY_FUND_REPORT_NOT_FOUND",
			message: "Report not found",
			translations:
				[
					{ message: "Relatório não encontrado",language_id: 3, type: "EMERGENCY_FUND_REPORT_NOT_FOUND" },
					{ message: "Report not found", language_id: 4, type: "EMERGENCY_FUND_REPORT_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_STUDENT_DATA_NOT_FOUND",
			message: "There are no student data to process",
			translations:
				[
					{ message: "Não existem dados do aluno / candidato para processar",language_id: 3, type: "EMERGENCY_FUND_STUDENT_DATA_NOT_FOUND" },
					{ message: "There are no student data to process", language_id: 4, type: "EMERGENCY_FUND_STUDENT_DATA_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_USER_DATA_NOT_FOUND",
			message: "There are no user data to process",
			translations:
				[
					{ message: "Não existem dados para processar",language_id: 3, type: "EMERGENCY_FUND_USER_DATA_NOT_FOUND" },
					{ message: "There are no user data to process", language_id: 4, type: "EMERGENCY_FUND_USER_DATA_NOT_FOUND" }
				]
		},
		{
			type: "EMERGENCY_FUND_RAW_DATA_NOT_FOUND",
			message: "There are no raw data to process",
			translations:
				[
					{ message: "Não existem dados em bruto para processar",language_id: 3, type: "EMERGENCY_FUND_RAW_DATA_NOT_FOUND" },
					{ message: "There are no raw data to process", language_id: 4, type: "EMERGENCY_FUND_RAW_DATA_NOT_FOUND" }
				]
		},
		{
			type: "UBIKE_APPLICATION_NOT_CONTRACTED",
			message: "Only can create a renovation on [contracted] application status",
			translations:
				[
					{ message: "Só pode efetuar a renovação se a candidatura estiver no estado 'contratado'",language_id: 3, type: "UBIKE_APPLICATION_NOT_CONTRACTED" },
					{ message: "Only can create a renovation on 'contracted' application status", language_id: 4, type: "UBIKE_APPLICATION_NOT_CONTRACTED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_ABSENCE_REASON_CREATE_ERROR",
			message: "This attendance is not a lack",
			translations:
				[
					{ message: "Esse registo não é uma falta",language_id: 3, type: "SOCIAL_SCHOLARSHIP_ABSENCE_REASON_CREATE_ERROR" },
					{ message: "This attendance is not a lack", language_id: 4, type: "SOCIAL_SCHOLARSHIP_ABSENCE_REASON_CREATE_ERROR" }
				]
		},
		{
			type: "BUS_WITHDRAWAL_APPLICATION_NOT_VALID_STATUS",
			message: "Only can withdrawal on application accepted status",
			translations:
				[
					{ message: "Só pode realizar a desistência se a candidatura já foi aceite",language_id: 3, type: "BUS_WITHDRAWAL_APPLICATION_NOT_VALID_STATUS" },
					{ message: "Only can withdrawal on application accepted status", language_id: 4, type: "BUS_WITHDRAWAL_APPLICATION_NOT_VALID_STATUS" }
				]
		},
		{
			type: "BUS_APPLICATION_ALREADY_EXIST",
			message: "The user already have application for this academic year",
			translations:
				[
					{ message: "Já tem inscrição para este ano letivo",language_id: 3, type: "BUS_APPLICATION_ALREADY_EXIST" },
					{ message: "The user already have application for this academic year", language_id: 4, type: "BUS_APPLICATION_ALREADY_EXIST" }
				]
		},
		{
			type: "USER_NOT_HAS_OWNER",
			message: "This user not registed by a owner",
			translations:
				[
					{ message: "Este utilizador não está registado como proprietário",language_id: 3, type: "USER_NOT_HAS_OWNER" },
					{ message: "This user not registed by a owner", language_id: 4, type: "USER_NOT_HAS_OWNER" }
				]
		},
		{
			type: "CART_IS_BLOCKED",
			message: "Cart is blocked",
			translations:
				[
					{ message: "Carrinho está bloqueado",language_id: 3, type: "CART_IS_BLOCKED" },
					{ message: "Cart is blocked", language_id: 4, type: "CART_IS_BLOCKED" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_PAYMENT_GRID_EXPERIENCE_CLOSED",
			message: "Cannot create a payment grid because the experience is closed",
			translations:
				[
					{ message: "Não é possível realizar grelha de pagamento porque a experiência já está fechada",language_id: 3, type: "SOCIAL_SCHOLARSHIP_PAYMENT_GRID_EXPERIENCE_CLOSED" },
					{ message: "Cannot create a payment grid because the experience is closed", language_id: 4, type: "SOCIAL_SCHOLARSHIP_PAYMENT_GRID_EXPERIENCE_CLOSED" }
				]
		},
		{
			type: "REPORTS_NOT_FOUND_ERROR",
			message: "Entity reports with id: 27 not found",
			translations:
				[
					{ message: "Relatório não encontrado",language_id: 3, type: "REPORTS_NOT_FOUND_ERROR" },
					{ message: "Report not found", language_id: 4, type: "REPORTS_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "EMERGENCY_FUND_EXPENSES_EXCEED_EXISTING_AMOUNT",
			message: "The total amount to pay exceed the existing amount",
			translations:
				[
					{ message: "O montante total a pagar excede o montante existente",language_id: 3, type: "EMERGENCY_FUND_EXPENSES_EXCEED_EXISTING_AMOUNT" },
					{ message: "The total amount to pay exceed the existing amount", language_id: 4, type: "EMERGENCY_FUND_EXPENSES_EXCEED_EXISTING_AMOUNT" }
				]
		},
		{
			type: "SUPPORT_WITH_REPORTS",
			message: "You have reports with this support associated",
			translations:
				[
					{ message: "Tem relatórios associados a este âmbito",language_id: 3, type: "SUPPORT_WITH_REPORTS" },
					{ message: "You have reports with this support associated", language_id: 4, type: "SUPPORT_WITH_REPORTS" }
				]
		},
		{
			type: "DOCUMENT_TYPE_WITH_DOCUMENTS",
			message: "You have documents with this document type associated",
			translations:
				[
					{ message: "Tem documentos associados a este tipo de documento",language_id: 3, type: "DOCUMENT_TYPE_WITH_DOCUMENTS" },
					{ message: "You have documents with this document type associated", language_id: 4, type: "DOCUMENT_TYPE_WITH_DOCUMENTS" }
				]
		},
		{
			type: "INCOME_WITH_OTHER_INCOMES",
			message: "You have other incomes with this income associated",
			translations:
				[
					{ message: "Tem outros rendimentos associados a este rendimento",language_id: 3, type: "INCOME_WITH_OTHER_INCOMES" },
					{ message: "You have other incomes with this income associated", language_id: 4, type: "INCOME_WITH_OTHER_INCOMES" }
				]
		},
		{
			type: "SUPPORT_WITH_PAYMENT_MAPS",
			message: "You have payment maps with this support associated",
			translations:
				[
					{ message: "Tem mapas de pagamento associados a este âmbito",language_id: 3, type: "SUPPORT_WITH_PAYMENT_MAPS" },
					{ message: "You have payment maps with this support associated", language_id: 4, type: "SUPPORT_WITH_PAYMENT_MAPS" }
				]
		},
		{
			type: "SUPPORT_WITH_INTENDED_SUPPORTS",
			message: "You have intended supports with this support associated",
			translations:
				[
					{ message: "Tem apoios pretendidos associados a este âmbito",language_id: 3, type: "SUPPORT_WITH_INTENDED_SUPPORTS" },
					{ message: "You have intended supports with this support associated", language_id: 4, type: "SUPPORT_WITH_INTENDED_SUPPORTS" }
				]
		},
		{
			type: "SUBJECT_HAS_OPEN_TICKETS",
			message: "Subject has open tickets",
			translations:
				[
					{ message: "A fila encontra-se em curso",language_id: 3, type: "SUBJECT_HAS_OPEN_TICKETS" },
					{ message: "Subject has open tickets", language_id: 4, type: "SUBJECT_HAS_OPEN_TICKETS" }
				]
		},
		{
			type: "DISH-TYPES_NOT_FOUND_ERROR",
			message: "Entity dish-types with id: 4 not found",
			translations:
				[
					{ message: "Tipo de prato não encontrado",language_id: 3, type: "DISH-TYPES_NOT_FOUND_ERROR" },
					{ message: "Type of dish not found", language_id: 4, type: "DISH-TYPES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "CANTEEN_DINNER_ID_NOT_FOUND_ERROR",
			message: "Entity canteen_dinner_id with id: 19 not found",
			translations:
				[
					{ message: "Cantina para jantar não encontrada",language_id: 3, type: "CANTEEN_DINNER_ID_NOT_FOUND_ERROR" },
					{ message: "Entity canteen_dinner_id with id: 19 not found", language_id: 4, type: "CANTEEN_DINNER_ID_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_IBAN_CHANGE_DONT_ALOW_DIRECT_DEBIT",
			message: "You must accept direct debit",
			translations:
				[
					{ message: "Tem que aceitar o débido direto",language_id: 3, type: "ACCOMMODATION_APPLICATION_IBAN_CHANGE_DONT_ALOW_DIRECT_DEBIT" },
					{ message: "You must accept direct debit", language_id: 4, type: "ACCOMMODATION_APPLICATION_IBAN_CHANGE_DONT_ALOW_DIRECT_DEBIT" }
				]
		},
		{
			type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_ALREADY_EXISTS",
			message: "User already have open extras change requests",
			translations:
				[
					{ message: "Já tem alterações de extras a decorrer",language_id: 3, type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_ALREADY_EXISTS" },
					{ message: "User already have open extras change requests", language_id: 4, type: "ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_ALREADY_EXISTS" }
				]
		},
		{
			type: "QUEUE_DESK_ALREADY_EXISTS",
			message: "Desk already exists",
			translations:
				[
					{ message: "Balcão já existente",language_id: 3, type: "QUEUE_DESK_ALREADY_EXISTS" },
					{ message: "Desk already exists", language_id: 4, type: "QUEUE_DESK_ALREADY_EXISTS" }
				]
		},
		{
			type: "QUEUE_DESK_IS_INACTIVE",
			message: "The queue is associated with an inactive desk",
			translations:
				[
					{ message: "A Fila está associada a um Balcão inativo",language_id: 3, type: "QUEUE_DESK_IS_INACTIVE" },
					{ message: "The queue is associated with an inactive desk", language_id: 4, type: "QUEUE_DESK_IS_INACTIVE" }
				]
		},
		{
			type: "QUEUE_MISSING_CONFIGURATION",
			message: "You must associate a desk/operator to the queue",
			translations:
				[
					{ message: "Deverá associar um Balcão/Operador à Fila",language_id: 3, type: "QUEUE_MISSING_CONFIGURATION" },
					{ message: "You must associate a desk/operator to the queue", language_id: 4, type: "QUEUE_MISSING_CONFIGURATION" }
				]
		},
		{
			type: "CONFIGURATION_IT_IS_NOT_AN_EXTERNAL_SERVICE",
			message: "You can only remove external services",
			translations:
				[
					{ message: "Apenas poderá remover serviços externos",language_id: 3, type: "CONFIGURATION_IT_IS_NOT_AN_EXTERNAL_SERVICE" },
					{ message: "You can only remove external services", language_id: 4, type: "CONFIGURATION_IT_IS_NOT_AN_EXTERNAL_SERVICE" }
				]
		},
		{
			type: "CONFIGURATION_INSERT_ONLY_EXTERNAL_SERVICES",
			message: "You can only insert external services",
			translations:
				[
					{ message: "Apenas poderá inserir serviços externos",language_id: 3, type: "CONFIGURATION_INSERT_ONLY_EXTERNAL_SERVICES" },
					{ message: "You can only insert external services", language_id: 4, type: "CONFIGURATION_INSERT_ONLY_EXTERNAL_SERVICES" }
				]
		},
		{
			type: "VOLUNTEERING_EXPERIENCE_CREATE_ERROR",
			message: "The end_date need is after start_date",
			translations:
				[
					{ message: "A data fim tem que ser superior a data de inicio",language_id: 3, type: "VOLUNTEERING_EXPERIENCE_CREATE_ERROR" },
					{ message: "An end date must be greater than the start date", language_id: 4, type: "VOLUNTEERING_EXPERIENCE_CREATE_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_REPORT_ERROR",
			message: "Need add report avaliation file",
			translations:
				[
					{ message: "Adicione o arquivo de Avaliação de Entrevista para fechar",language_id: 3, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_REPORT_ERROR" },
					{ message: "Please add the Interview Evaluation file to close this application", language_id: 4, type: "SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_REPORT_ERROR" }
				]
		},
		{
			type: "FOOD_PACK_MEAL_RESERVED",
			message: "Cant change this meal beacause is already reserved in the canteen",
			translations:
				[
					{ message: "Não é possível alterar esta refeição pois já está reservada na cantina",language_id: 3, type: "FOOD_PACK_MEAL_RESERVED" },
					{ message: "Cant change this meal beacause is already reserved in the canteen", language_id: 4, type: "FOOD_PACK_MEAL_RESERVED" }
				]
		},
		{
			type: "VOLUNTEERING_USER_INTEREST_STATUS_ERROR",
			message: "Unauthorized user interest status change",
			translations:
				[
					{ message: "Não é possível alterar o estado da manifestação de interesse",language_id: 3, type: "VOLUNTEERING_USER_INTEREST_STATUS_ERROR" },
					{ message: "Unauthorized user interest status change", language_id: 4, type: "VOLUNTEERING_USER_INTEREST_STATUS_ERROR" }
				]
		},
		{
			type: "VOLUNTEERING_USER_INTEREST_STATUS_NO_CERTIFICATE",
			message: "Unauthorized user interest status change need add certificate file",
			translations:
				[
					{ message: "É necessário o certificado para mudar o estado da manifestação de interesse",language_id: 3, type: "VOLUNTEERING_USER_INTEREST_STATUS_NO_CERTIFICATE" },
					{ message: "Certificate file needed for change the user interest status", language_id: 4, type: "VOLUNTEERING_USER_INTEREST_STATUS_NO_CERTIFICATE" }
				]
		},

		{
			type: "DISHS-TYPES_NOT_FOUND_ERROR",
			message: "Entity dishs-types with id: 112 not found",
			translations:
				[
					{ message: "Tipo de prato não encontrado",language_id: 3, type: "DISHS-TYPES_NOT_FOUND_ERROR" },
					{ message: "Type of dish not found", language_id: 4, type: "DISHS-TYPES_NOT_FOUND_ERROR" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_HAS_NOT_CURRENT_ACCOUNT",
			message: "The payment method by transfer (IBAN) must be 100% and the selection of % for current account is not allowed",
			translations: [
				{
					message: "O método de pagamento por transferência (IBAN) deverá ser 100% e a seleção de % para conta corrente não é permitida",
					language_id: 3,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_HAS_NOT_CURRENT_ACCOUNT",
				},
				{
					message: "The payment method by transfer (IBAN) must be 100% and the selection of % for current account is not allowed",
					language_id: 4,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_HAS_NOT_CURRENT_ACCOUNT",
				},
			],
		},

		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_FOUND",
			message: "Experience not found",
			translations: [
				{
					message: "Ação não encontrada",
					language_id: 3,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_FOUND",
				},
				{
					message: "Experience not found",
					language_id: 4,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_FOUND",
				},
			],
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE_CLOSING_DATE",
			message: "Experience not available to change the closing date.",
			translations: [
				{
					message: "Ação não disponível para alteração da data de fecho de candidaturas.",
					language_id: 3,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE_CLOSING_DATE",
				},
				{
					message: "Experience not available to change the closing date.",
					language_id: 4,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE_CLOSING_DATE",
				},
			],
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_DATE_TODAY",
			message: "Experience closing date must be equal or after than today.",
			translations: [
				{
					message: "Data de fecho de candidaturas da Ação deve de ser igual ou superior que hoje.",
					language_id: 3,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_DATE_TODAY",
				},
				{
					message: "Experience closing date must be equal or after than today.",
					language_id: 4,
					type: "SOCIAL_SCHOLARSHIP_EXPERIENCE_DATE_TODAY",
				},
			],
		},
		{
			type: "CONFIGURATION_SECTIONS_BO_ASSOCIATED_SERVICES",
			message: "You can not remove section, there are associated services",
			translations: [
				{
					message: "Não poderá remover a secção, existem serviços associados",
					language_id: 3,
					type: "CONFIGURATION_SECTIONS_BO_ASSOCIATED_SERVICES",
				},
				{
					message: "You can not remove section, there are associated services",
					language_id: 4,
					type: "CONFIGURATION_SECTIONS_BO_ASSOCIATED_SERVICES",
				},
			],
		},
		{
			type: "UBIKE_APPLICATION_FORBIDDEN",
			message: "Unauthorized access to data",
			translations:
				[
					{ message: "Acesso não autorizado",language_id: 3, type: "UBIKE_APPLICATION_FORBIDDEN" },
					{ message: "Unauthorized access to data", language_id: 4, type: "UBIKE_APPLICATION_FORBIDDEN" }
				]
		},
		{
			type: "SOCIAL_SCHOLARSHIP_EXPERIENCES_SAME_PERIOD",
			message: "You have an open attendances for the same period",
			translations:
				[
					{ message: "Você tem uma presença aberto para o mesmo período",language_id: 3, type: "SOCIAL_SCHOLARSHIP_EXPERIENCES_SAME_PERIOD" },
					{ message: "You have an open attendances for the same period", language_id: 4, type: "SOCIAL_SCHOLARSHIP_EXPERIENCES_SAME_PERIOD" }
				]
		},
	];

	return Promise.all(
		data.map((d) => {
			return knex("error")
				.select()
				.where("type", d.type)
				.then((rows) => {
					if (rows.length === 0) {
						return knex("error")
							.insert({
								type: d.type,
								message: d.message
							})
							.then((error) => {
								return Promise.all(
									d.translations.map((error_translation) =>
										knex("error_translation").insert(error_translation),
									),
								);
							});
					} else {
						return knex("error")
							.update({
								type: d.type,
								message: d.message
							})
							.where("id", rows[0].id)
							.then((error) => {
								return Promise.all(
									d.translations.map((error_translation) =>
										knex("error_translation")
											.select()
											.where("type", error_translation.type)
											.andWhere("language_id", error_translation.language_id)
											.then((rows_translation) => {
												if (rows_translation.length === 0) {
													return knex("error_translation").insert(error_translation);
												}

												return knex("error_translation")
													.update(error_translation)
													.where("id", rows_translation[0].id);
											}),
									),
								);
							});
					}
				});
		}),
	);
};

/*

ERR_DUPLICATED_KEY_TEMPLATE
REPORTS.TEMPLATES_NOT_FOUND_ERROR

SUBJECT_ALREADY_EXISTS

entity.parse.failed
entity.too.large*/
