exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			type: "buttons-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 22,
			position: 0,
			call_path: "configuration.services.wp_dashboard"
		},
		{
			id: 2,
			type: "posts-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 19,
			position: 1,
			call_path: "communication.posts.wp_dashboard"
		},
		{
			id: 3,
			type: "news-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 19,
			position: 2,
			call_path: null
		},
		{
			id: 4,
			type: "meals-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 2,
			position: 3,
			call_path: "alimentation.menu.wp_dashboard"
		},
		{
			id: 5,
			type: "accommodation-applications-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 1,
			position: 4,
			call_path: "private_accommodation.listings.wp_dashboard"
		},
		{
			id: 6,
			type: "current-account-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 11,
			position: 5,
			call_path: "current_account.movements.balance"
		},
		{
			id: 7,
			type: "mobility-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 3,
			position: 6,
			call_path: "bus.tickets_bought.wp_dashboard"
		},
		{
			id: 8,
			type: "ubike-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 13,
			position: 7,
			call_path: "u_bike.applications.wp_dashboard"
		},
		{
			id: 9,
			type: "applications-social-scholarship-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 17,
			position: 8,
			call_path: "social_scholarship.applications.wp_dashboard"
		},
		{
			id: 10,
			type: "experiences-social-scholarship-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 17,
			position: 9,
			call_path: "social_scholarship.experiences.wp_dashboard"
		},
		{
			id: 11,
			type: "volunteering-widget",
			active: true,
			loginRequired: false,
			logoutRequired: false,
			service_id: 28,
			position: 10,
			call_path: "volunteering.experiences.wp_dashboard"
		},
		{
			id: 12,
			type: "queue-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 21,
			position: 11,
			call_path: "queue.tickets.wp_dashboard"
		},
		{
			id: 13,
			type: "health-widget",
			active: true,
			loginRequired: true,
			logoutRequired: false,
			service_id: 7,
			position: 12,
			call_path: "health.appointment.wp_dashboard"
		}
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if widget exist
			const rows = await knex("widget").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("widget").insert(d);
			} else {
				await knex("widget").where({ id: d.id }).update("call_path", d.call_path);
			}
			return true;
		}),
	);
};
