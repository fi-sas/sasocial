exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			name: "Alojamento - 1º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_1",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 2,
			name: "Alojamento - 2º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_2",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 3,
			name: "Alojamento - 3º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_3",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 4,
			name: "Alojamento - 4º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_4",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 5,
			name: "Alojamento - 5º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_5",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 6,
			name: "Alojamento - 6º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_6",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 7,
			name: "Alojamento - 7º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_7",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 8,
			name: "Alojamento - 8º Passo da candidatura ",
			key: "ACCOMMODATION_APPLICATION_STEP_8",
			service_id: 1,
			created_at: new Date(),
			updated_at: new Date()
		},

		{
			id: 9,
			name: "Mobilidade - 1º Passo da candidatura ",
			key: "MOBILITY_APPLICATION_STEP_1",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 10,
			name: "Mobilidade - 2º Passo da candidatura ",
			key: "MOBILITY_APPLICATION_STEP_2",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 11,
			name: "Mobilidade - 3º Passo da candidatura ",
			key: "MOBILITY_APPLICATION_STEP_3",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 12,
			name: "Mobilidade - 4º Passo da candidatura ",
			key: "MOBILITY_APPLICATION_STEP_4",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 13,
			name: "Mobilidade - 5º Passo da candidatura ",
			key: "MOBILITY_APPLICATION_STEP_5",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 14,
			name: "Mobilidade - 1º Passo do pedido de declação sub 23 ",
			key: "MOBILITY_SUB23_DECLARATION_STEP_1",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 15,
			name: "Mobilidade - 2º Passo do pedido de declação sub 23 ",
			key: "MOBILITY_SUB23_DECLARATION_STEP_2",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 16,
			name: "Mobilidade - 3º Passo do pedido de declação sub 23 ",
			key: "MOBILITY_SUB23_DECLARATION_STEP_3",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 17,
			name: "Mobilidade - 4º Passo do pedido de declação sub 23 ",
			key: "MOBILITY_SUB23_DECLARATION_STEP_4",
			service_id: 3,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 18,
			name: "Ubike - 1º Passo da candidatura ",
			key: "BUS_APPLICATION_STEP_1",
			service_id: 13,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 19,
			name: "Ubike - 2º Passo da candidatura ",
			key: "BUS_APPLICATION_STEP_2",
			service_id: 13,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 20,
			name: "Ubike - 3º Passo da candidatura ",
			key: "BUS_APPLICATION_STEP_3",
			service_id: 13,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 21,
			name: "Ubike - 4º Passo da candidatura ",
			key: "BUS_APPLICATION_STEP_4",
			service_id: 13,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 22,
			name: "Ubike - 5º Passo da candidatura ",
			key: "BUS_APPLICATION_STEP_5",
			service_id: 13,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 23,
			name: "Fundo de emergência - 1º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_1",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 24,
			name: "Fundo de emergência - 2º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_2",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 25,
			name: "Fundo de emergência - 3º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_3",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 26,
			name: "Fundo de emergência - 4º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_4",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 27,
			name: "Fundo de emergência - 5º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_5",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 28,
			name: "Fundo de emergência - 6º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_6",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 29,
			name: "Fundo de emergência - 7º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_7",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 30,
			name: "Fundo de emergência - 8º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_8",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 31,
			name: "Fundo de emergência - 9º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_9",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			id: 32,
			name: "Fundo de emergência - 10º Passo da candidatura ",
			key: "EMERGENCY_FUND_APPLICATION_STEP_10",
			service_id: 30,
			created_at: new Date(),
			updated_at: new Date()
		},
	];

	return Promise.all(
		data.map(async (d) => {
			const rows = await knex("form_information").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("form_information").insert(d);
			} else {
				await knex("form_information").where({ id: d.id }).update({
					"name": d.name, "key": d.key, "service_id": d.service_id, "updated_at": new Date()
				});
			}
			return true;
		}),
	);
};
