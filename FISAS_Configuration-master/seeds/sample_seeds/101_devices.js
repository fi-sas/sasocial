const uuidv4 = require("uuid").v4;

exports.seed = async (knex) => {
  const data = [
    {
      uuid: uuidv4(),
      name: 'Kioske CA',
      type: 'KIOSK',
      orientation: 'PORTRAIT',
      active: true,
      secondsToInactivity: 30,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      uuid: uuidv4(),
      name: 'Tv dos SAS',
      type: 'TV',
      orientation: 'LANDSCAPE',
      active: true,
      secondsToInactivity: 60,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      uuid: uuidv4(),
      name: 'KIOSK ESA',
      type: 'KIOSK',
      orientation: 'PORTRAIT',
      active: true,
      secondsToInactivity: 120,
      created_at: new Date(),
      updated_at: new Date(),
    }
  ];

  return Promise.all(data.map(async (d) => {
    // Check if device exist
    const rows = await knex('device').select().where('name', d.name);
    if (rows.length === 0) {
      await knex('device').insert(d);
    }
    return true;
  }));
};
