const uuidv4 = require("uuid").v4;

exports.seed = async (knex) => {
  const data = [
    {
	  id: 1,
      name: 'Group test 1',
      active: true,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
		id: 2,
		name: 'Group test 2',
		active: true,
		created_at: new Date(),
		updated_at: new Date(),
	  },
	  {
		id: 3,
		name: 'Group test 3 - inactive',
		active: false,
		created_at: new Date(),
		updated_at: new Date(),
	  },
	  {
		id: 4,
		name: 'Group test 4',
		active: true,
		created_at: new Date(),
		updated_at: new Date(),
	  },
	  {
		id: 5,
		name: 'Group test 5',
		active: true,
		created_at: new Date(),
		updated_at: new Date(),
	  },
	  {
		id: 6,
		name: 'Group test 6',
		active: true,
		created_at: new Date(),
		updated_at: new Date(),
	  },

  ];

  return Promise.all(data.map(async (d) => {
    // Check if group exist
    const rows = await knex('group').select().where('id', d.id);
    if (rows.length === 0) {
      await knex('group').insert(d);
    }
    return true;
  }));
};
