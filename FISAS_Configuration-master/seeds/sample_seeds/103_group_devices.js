exports.seed = async (knex) => {
	const data = [
		{ group_id: 1, device_id: 1 },
		{ group_id: 1, device_id: 2 },
		{ group_id: 1, device_id: 3 },
		{ group_id: 1, device_id: 5 },
		{ group_id: 1, device_id: 6 },
		{ group_id: 2, device_id: 2 },
		{ group_id: 4, device_id: 5 },
		{ group_id: 4, device_id: 6 }
	];

	return Promise.all(data.map(async (d) => {

		const tableName = "group_device";
		
		// Check if group exists
		const existsGroup = await knex('group').count('id', { as: 'c' })
		.where({ id: d.group_id });

		// Check if device exists
		const existsDevice = await knex('device').count('id', { as: 'c' })
		.where({ id: d.device_id });
		
		if (existsGroup[0].c > 0 && existsDevice[0].c > 0) {
			
			// Check if relation exists
			const rel = await knex(tableName).select()
				.where({ group_id: d.group_id, device_id: d.device_id });

			if (rel.length === 0) {
				await knex(tableName).insert(d);
			}
		}

		return true;
	}));
};
