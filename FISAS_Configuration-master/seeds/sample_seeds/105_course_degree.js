
exports.seed = async (knex) => {
  const data = [
    {
      id: 1,
      name: "Licenciatura",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 2,
      name: "Mestrado",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 3,
      name: "Pós-Graduação",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 4,
      name: "CTeSP",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    }
  ]
  
  return Promise.all(data.map(async (d) => {
    // Check if item exist
    const rows = await knex("course_degree").select().where("id", d.id);
    if (rows.length === 0) {
      await knex("course_degree").insert(d);
    }
    return true;
  }));
};
