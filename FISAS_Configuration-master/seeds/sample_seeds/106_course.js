
exports.seed = async (knex) => {
  const data = [
    {
      id: 1,
      name: "Alimentação e Restauração Coletiva",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 2,
      name: "Construção e Reabilitação",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 3,
      name: "Desenvolvimento Web e Multimédia",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 4,
      name: "Eficiência Energética nos Edifício",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 5,
      name: "Gestão Hoteleira",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 6,
      name: "Manutenção Mecânica",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 7,
      name: "Mecatrónica ",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 8,
      name: "Processo Industrial",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 9,
      name: "Qualidade e Segurança Alimentar",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 10,
      name: "Redes e Sistemas Informáticos",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 11,
      name: "Sistemas Eletrónicos e Computadores",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 12,
      name: "Tecnologias e Programação de Sistemas de Informação",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 13,
      name: "Design de Ambientes",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 14,
      name: "Engenharia Informática",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 15,
      name: "Engenharia Mecânica",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 16,
      name: "Design do Produto",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 17,
      name: "Engenharia Alimentar",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 18,
      name: "Engenharia Mecatrónica",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 19,
      name: "Engenharia Civil e do Ambiente",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 20,
      name: "Gestão",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 21,
      name: "Engenharia da Computação Gráfica e Multimédia",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 22,
      name: "Turismo",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 23,
      name: "Engenharia de Redes e Sistemas de Computadores",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 24,
      name: "Gestão de Sistemas de Informação",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 25,
      name: "Informática de Segurança e Computação Forense",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 26,
      name: "Cibersegurança",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 27,
      name: "Contabilidade e Finanças (APNOR)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 28,
      name: "Design Integrado",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 29,
      name: "Engenharia Alimentar",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 30,
      name: "Engenharia Civil e do Ambiente",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 31,
      name: "Engenharia Informática",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 32,
      name: "Gestão das Organizações - Ramo de Gestão de Empresas (APNOR)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 33,
      name: "Turismo, Inovação e Desenvolvimento ",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 1,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 34,
      name: "Termalismo e Bem-Estar",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 35,
      name: "Enfermagem",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 36,
      name: "Enfermagem de Saúde Familiar",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 37,
      name: "Enfermagem do Trabalho",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 38,
      name: "Quiromassagem",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 39,
      name: "Supervisão Clínica",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 40,
      name: "Enfermagem de Reabilitação",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 41,
      name: "Enfermagem de Saúde Comunitária",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 42,
      name: "Enfermagem de Saúde Materna e Obstetrícia",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 43,
      name: "Enfermagem Médico-Cirúrgica",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 44,
      name: "Gerontologia Social (Parceria ESE-IPVC)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 45,
      name: "Gestão das Organizações - Ramo Gestão de Unidades de Saúde (APNOR)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 2,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 46,
      name: "Artes e Tecnologia [Luz, Som e Imagem]",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 47,
      name: "Intervenção Educativa em Creche",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 48,
      name: "Intervenção Sociocomunitária e Envelhecimento",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 49,
      name: "Ilustração e Produção Gráfica",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 50,
      name: "Serviços Educativos e Património Local",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 51,
      name: "Artes Plásticas e Tecnologias Artísticas",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 52,
      name: "Educação Básica",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 53,
      name: "Educação Social Gerontológica",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 54,
      name: "Gestão Artística e Cultural",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 55,
      name: "Administração Escolar e Inovação Educacional",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 56,
      name: "Educação Literária e Literatura para a Infância e Juventude",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 57,
      name: "Educação, Ciência e Património Local",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 58,
      name: "Educação, Memória e Herança Cultural",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 59,
      name: "Educação Artística",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 60,
      name: "Educação Motora nas Primeiras Idades",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 61,
      name: "Gerontologia Social (Parceria ESS-IPVC)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 62,
      name: "Gestão Artística e Cultural",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 63,
      name: "Supervisão Pedagógica",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 64,
      name: "Educação Pré-Escolar",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 65,
      name: "Educação Pré-Escolar e Ensino do 1º Ciclo do Ensino Básico",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 66,
      name: "Ensino do 1º Ciclo do Ensino Básico e de Matemática e Ciências Naturais no 2º ciclo do Ensino Básico",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 67,
      name: "Ensino do 1º Ciclo do Ensino Básico e de Português e História e Geografia de Portugal no 2º ciclo do Ensino Básico",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 3,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 68,
      name: "Trabalhos em Altura e Acesso por Cordas",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 69,
      name: "Treino Desportivo",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 70,
      name: "Desporto e Lazer",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 71,
      name: "Desporto Natureza",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 72,
      name: "Atividades de Fitness",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 73,
      name: "Desporto Natureza",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 74,
      name: "Treino Desportivo",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 4,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 75,
      name: "Contabilidade e Gestão para PME",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 76,
      name: "Gestão da Qualidade",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 77,
      name: "Transportes e Logística ",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 78,
      name: "Contabilidade e Fiscalidade",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 79,
      name: "Gestão da Distribuição e Logística",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 80,
      name: "Marketing e Comunicação Empresarial",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 81,
      name: "Organização e Gestão Empresariais",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 82,
      name: "Fiscalidade e Contabilidade Ibérica",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 83,
      name: "Gestão da Qualidade",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 84,
      name: "Marketing Digital e E-Business",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 85,
      name: "Logística (APNOR)",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 86,
      name: "Marketing",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 5,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 87,
      name: "Análises e Gestão Laboratorial",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 88,
      name: "Cuidados Veterinários",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 89,
      name: "Fruticultura, Viticultura e Enologia",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 90,
      name: "Geoinformática e Gestão de Recursos Naturais",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 91,
      name: "Gestão de Empresas Agrícolas",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 92,
      name: "Gestão do Turismo em Espaço Rural",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 93,
      name: "Gestão e Qualidade Ambiental",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 94,
      name: "Marketing Agroalimentar",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 95,
      name: "Mecanização e Automação Agrícola",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 96,
      name: "Riscos e Proteção Civil",
      active: true,
      acronym: null,
      course_degree_id: 4,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 97,
      name: "Agronomia",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 98,
      name: "Biotecnologia",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 99,
      name: "Ciências e Tecnologias do Ambiente",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 100,
      name: "Enfermagem Veterinária",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 101,
      name: "Engenharia do Ambiente e Geoinformática",
      active: true,
      acronym: null,
      course_degree_id: 1,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 102,
      name: "Marketing de Vinhos - 3ª Edição",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 103,
      name: "istemas Integrados de Gestão",
      active: true,
      acronym: null,
      course_degree_id: 3,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 104,
      name: "Agricultura Biológica",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 105,
      name: "Enfermagem Veterinária em Animais de Companhia",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 106,
      name: "Engenharia Agronómica",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 107,
      name: "Engenharia do Território e do Ambiente",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    },
    {
      id: 108,
      name: "Zootecnia",
      active: true,
      acronym: null,
      course_degree_id: 2,
      organic_unit_id: 6,
      updated_at: new Date(),
      created_at: new Date()
    }
  ]
   

  return Promise.all(data.map(async (d) => {
    // Check if item exist
    const rows = await knex("course").select().where("id", d.id);
    if (rows.length === 0) {
      await knex("course").insert(d);
    }
    return true;
  }));
};
