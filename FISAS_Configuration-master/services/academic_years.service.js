"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const moment = require("moment");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.academic_years",
	table: "academic_year",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "academic_years")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "academic_year", "start_date", "end_date"],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			academic_year: { type: "string", max: 9, pattern: "^\\d{4}-\\d{4}" },
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"uniqueAcademicYearValidation",
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				async function sanatize(ctx) {
					ctx.params.updated_at = new Date();
					const existent_data = await this._get(ctx, { id: ctx.params.id });
					if (existent_data[0].academic_year != ctx.params.academic_year) {
						await this.uniqueAcademicYearValidation(ctx);
					}
				},
			],
			patch: [
				async function sanatize(ctx) {
					ctx.params.updated_at = new Date();
					const existent_data = await this._get(ctx, { id: ctx.params.id });
					if (existent_data[0].academic_year != ctx.params.academic_year) {
						await this.uniqueAcademicYearValidation(ctx);
					}
				},
			],
		},
		after: {
			create: ["itsCurrentAcademicYear"],
			update: ["itsCurrentAcademicYear"],
			patch: ["itsCurrentAcademicYear"],
			list: ["itsCurrentAcademicYear"],
			get: ["itsCurrentAcademicYear"],
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		get_current_academic_year: {
			rest: "GET /current",
			visibility: "published",
			async handler(ctx) {
				return this._find(ctx, {
					query: (qb) => {
						qb.where("start_date", "<=", new Date());
						qb.where("end_date", ">=", new Date());
						return qb;
					}
				});
			}
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getCurrentAndFutureAcademicYear: {
			rest: "GET /currentAndFuture",
			visibility: "published",
			async handler(ctx) {
				return this._find(ctx, {
					query: (qb) => {
						qb.where("start_date", "<=", new Date());
						qb.where("end_date", ">=", new Date());
						qb.orWhere((qb1) => {
							qb1.where("start_date", ">=", new Date());
							qb1.andWhere("end_date", ">=", new Date());
						});
						return qb;
					}
				});
			}

		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		uniqueAcademicYearValidation(ctx) {
			return this._find(ctx, { query: { academic_year: ctx.params.academic_year } }).then(result => {
				if (result.length > 0) {
					throw new ValidationError(
						"Academic year " + ctx.params.academic_year + " already exists",
						"CONFIGURATION_ACADEMIC_YEAR_ALREADY_EXIST",
						{}
					);
				}
			});
		},
		itsCurrentAcademicYear(ctx, response) {
			if (response.data) {
				for (const element of response.data) {
					element.current = moment().isBetween(element.start_date, element.end_date) || moment().isSame(element.start_date) || moment().isSame(element.end_date);
				}
			}
			else if (response.rows) {
				for (const element of response.rows) {
					element.current = moment().isBetween(element.start_date, element.end_date) || moment().isSame(element.start_date) || moment().isSame(element.end_date);
				}
			} else {
				for (const element of response) {
					element.current = moment().isBetween(element.start_date, element.end_date) || moment().isSame(element.start_date) || moment().isSame(element.end_date);
				}
			}
			return response;
		},
	},
	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
