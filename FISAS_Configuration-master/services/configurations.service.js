"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.configuration",

	mixins: [],

	/**
	 * Settings
	 */
	settings: {
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		configuration: {
			rest: "GET /",
			handler(ctx) {

				return ctx.mcall({
					languages: { action: "configuration.languages.list", params: { limit: -1 } },
					services: { action: "configuration.services.list", params: { limit: -1 } },
					taxes: { action: "configuration.taxes.list", params: { limit: -1 } },
				}).then(results => {
					return {
						device: ctx.meta.device,
						languages: results.languages.rows,
						services: results.services.rows,
						taxes: results.taxes.rows,
					};
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
