"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.contacts",
	table: "contact",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "contacts")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "email", "description", "url", "updated_at", "created_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.contacts_translations",
					"translations",
					"id",
					"contact_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: { type: "number", positive: true, integer: true, convert: true },
					name: { type: "string" },
					description: { type: "string" },
				},
			},
			url: { type: "url" },
			email: { type: "email" },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name", "description"],
						"configuration.contacts_translations",
						"contact_id",
					);
				},
			],
			create: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveTranslations(ctx, res) {
			await ctx.call("configuration.contacts_translations.save_translations", {
				contact_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("configuration.contacts_translations.find", {
				query: {
					contact_id: res[0].id,
				},
			});

			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
