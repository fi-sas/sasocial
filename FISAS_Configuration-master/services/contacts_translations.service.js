"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.contacts_translations",
	table: "contact_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "contacts_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "contact_id", "language_id", "name", "description"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			contact_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
			name: { type: "string" },
			description: { type: "string" },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				contact_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
						description: { type: "string" },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					contact_id: ctx.params.contact_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					contact_id: ctx.params.contact_id,
					language_id: translation.language_id,
					name: translation.name,
					description: translation.description,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
