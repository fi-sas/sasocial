"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.courses",
	table: "course",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "courses")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"acronym",
			"course_degree_id",
			"organic_unit_id",
			"external_ref",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [
			"courseDegree"
		],
		withRelateds: {
			"courseDegree"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.course-degrees", "courseDegree", "course_degree_id");
			},
			"organicUnit"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organicUnit", "organic_unit_id" );
			}
		},
		entityValidator: {
			name: { type: "string" },
			acronym: { type: "string", max: 32 },
			course_degree_id: { type: "number", positive: true, integer: true, convert: true },
			organic_unit_id: { type: "number", positive: true, integer: true, convert: true },
			external_ref: { type: "string", max: 255, optional: true },
			active: { type: "boolean" },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateOrganicUnitAndCourseDegree",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateOrganicUnitAndCourseDegree",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateOrganicUnitAndCourseDegree(ctx) {
			await ctx.call("infrastructure.organic-units.get", {
				id: ctx.params.organic_unit_id,
			});
			await ctx.call("configuration.course-degrees.get", {
				id: ctx.params.course_degree_id,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
