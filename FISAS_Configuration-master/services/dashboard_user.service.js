"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.dashboard_user",
	table: "user_widget",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "dashboard")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "widget_id", "user_id", "active", "position"],
		entityValidator: {
			widget_id: { type: "number", positive: true, integer: true, convert: true },
			user_id: { type: "number", positive: true, integer: true, convert: true },
			active: { type: "boolean" },
			position: { type: "number", min: 0, integer: true, convert: true },
		},
		defaultWithRelateds: ["widget"],
		withRelateds: {
			widget(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.dashboard_widgets", "widget", "widget_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			updateUserChoises: [
				function validateAuthentication(ctx) {
					if (ctx.meta.isGuest) {
						throw new ValidationError(
							"Only logged users can change widgets preferences",
							"CONFIGURATION_DASHBOARD_UNAUTHORIZED",
							{}
						);
					}
				},
			]
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getUserDashboard: {
			rest: "GET /",
			cache: {
				ttl: 60,
				keys: ["#user.id"],
			},
			visibility: "published",
			autentication: false,
			async handler(ctx) {

				let user_widgets = await this._find(ctx, {
					query: qb => qb
						.where("user_id", ctx.meta.user.id)
						.orderBy("position")
				});

				// Update user widgets
				user_widgets = await this.updateDefaultUserWidgets(ctx, user_widgets);

				for (const uw of user_widgets) {
					if (uw.widget.call_path) {
						try {
							uw.data = await ctx.call(uw.widget.call_path);
						}
						catch (e) {
							// If fetch widget data failed, remove failed widget from list
							user_widgets = user_widgets.filter(x => x.widget_id != uw.widget_id);
						}
					}
				}

				return user_widgets;
			},
		},
		updateUserChoises: {
			rest: "PATCH /",
			visibility: "published",
			autentication: true,
			params: {
				widgets: {
					type: "array",
					item: {
						widget_id: { type: "number", positive: true, integer: true, convert: true },
						position: { type: "number", positive: true, integer: true, convert: true },
						active: { type: "boolean" }
					},
				},
			},
			async handler(ctx) {
				for (const iterator of ctx.params.widgets) {
					const widget = await this._find(ctx, { query: { widget_id: iterator.widget_id, user_id: ctx.meta.user.id } });
					if (widget.length > 0) {
						await this._update(ctx, { id: widget[0].id, position: iterator.position, active: iterator.active }, true);
					}
				}
				return await ctx.call("configuration.dashboard_user.getUserDashboard");
			}
		}

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {

		async updateDefaultUserWidgets(ctx, user_widgets) {

			const widgets = await ctx
				.call(ctx.meta.isGuest ? "configuration.dashboard_widgets.getLoggedoutWidgets" : "configuration.dashboard_widgets.getLoggedinWidgets");

			const user_widget_ids = user_widgets.map(uw => uw.widget_id);

			for (const w of widgets) {

				if(!user_widget_ids.includes(w.id)) {
					const entity = [{
						widget_id: w.id,
						user_id: ctx.meta.user.id,
						active: true,
						position: w.position
					}];
					await this._insert(ctx, { entity }).then(nuw => user_widgets.push(nuw[0]));
				}
			}

			return user_widgets;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
