"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.dashboard_widgets",
	table: "widget",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "dashboard_widgets")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"type",
			"call_path",
			"active",
			"position",
			"service_id",
			"loginRequired"
		],
		entityValidator: {
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [],
			update: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getLoggedoutWidgets: {
			rest: "GET /getLoggedoutWidgets",
			visibility: "published",
			handler(ctx) {
				return this._find(ctx, {
					query: qb => qb.whereRaw(`
					(("loginRequired" = false AND "logoutRequired" = false) AND "service_id" in (select id from service where active=true))
						OR
					(("loginRequired" = false AND "logoutRequired" = true) AND "service_id" in (select id from service where active=true))`)
						.orderBy("position")
				});
			}
		},
		getLoggedinWidgets: {
			rest: "GET /getLoggedinWidgets",
			visibility: "published",
			handler(ctx) {
				return this._find(ctx,
					{
						query: qb => qb
							.whereRaw(`
						(("loginRequired" = false AND "logoutRequired" = false) AND "service_id" in (select id from service where active=true))
							OR
						(("loginRequired" = true AND "logoutRequired" = false) AND "service_id" in (select id from service where active=true))`)
							.orderBy("position")
					}
				);
			}
		},
		list: {
			visibility: "published",
		},
		getActiveWidgets: {
			visibility: "public",
			handler(ctx) {
				return this._find(ctx, {
					query: qb => qb
						.whereRaw("active=true	AND service_id in (select id from service where active=true)")
						.orderBy("position")
				});
			}
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
