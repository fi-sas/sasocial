"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const uuid = require("uuid").v4;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.devices",
	table: "device",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "devices")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"uuid",
			"name",
			"type",
			"orientation",
			"active",
			"secondsToInactivity",
			"CPUID",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [
			"groups"
		],
		withRelateds: {
			groups(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.groups_devices.groups_of_device", { device_id: doc.id })
							.then((res) => (doc.groups = res));
					}),
				);
			},

		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean" },
			type: { type: "enum", values: ["KIOSK", "TV", "MOBILE", "BO", "WEB", "POS", "GENERIC"] },
			orientation: { type: "enum", values: ["PORTRAIT", "LANDSCAPE"] },
			secondsToInactivity: { type: "number", positive: true, default: 60 },
			CPUID: { type: "string", optional: true, max: 64 },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.uuid = uuid();
				},
			],
			update: [
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			list: [
				"removeCPUID",
			],
			get: [
				"removeCPUID",
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			cache: {
				keys: ["withRelated", "fields", "limit", "offset", "sort", "search", "searchFields", "query", "#isBackoffice", "#showCPUID"]
			},
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#isBackoffice", "#showCPUID"]
			},
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		set_cpuid: {
			rest: "PUT /cpuid",
			visibility: "published",
			params: {
				CPUID: { type: "string" }
			},
			handler(ctx) {
				if(ctx.meta.device.CPUID && ctx.meta.device.CPUID !== ctx.params.CPUID) {
					throw new ValidationError(
						"The CPUID is immutable and cant be changed",
						"DEVICE_ALREADY_HAS_CPUID",
						{}
					);
				}

				return ctx.call("configuration.devices.patch", {
					id: ctx.meta.device.id,
					CPUID: ctx.params.CPUID
				}).then(result =>  {
					ctx.emit("configuration.devices.cpuid_updated");
					return result;
				});
			}
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		removeCPUID(ctx, res) {
			if(!ctx.meta.showCPUID && ctx.meta.isBackoffice) {
				ctx.meta.showCPUID = true;
			}

			if(!ctx.meta.showCPUID) {
				if(Array.isArray(res)) {
					res = res.map(dev => {
						delete dev.CPUID;
						return dev;
					});
				}

				if(res.rows && Array.isArray(res.rows)) {
					res.rows = res.rows.map(dev => {
						delete dev.CPUID;
						return dev;
					});
				}
				delete ctx.meta.showCPUID;
			}
			return res;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
