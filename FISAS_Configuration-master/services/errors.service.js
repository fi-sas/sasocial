"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { validator_messages } = require("./validator_messages/messages");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.errors",
	table: "error",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "errors")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["type", "message"],
		defaultWithRelateds: [],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.errors_translations.find", {
								query: { type: doc.type },
								withRelated: "language",
							})
							.then((res) => (doc.translations = res));
					}),
				);
			},
		},
		entityValidator: {
			type: { type: "string" },
			message: { type: "string", optional: true },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getValidatorError: {
			params: {
				type: { type: "string" },
				message: { type: "string" },
				field: { type: "string" },
				real: { type: "string", optional: true, convert: true  },
				expected: { type: "string", optional: true, convert: true },
				action: { type: "string", optional: true, convert: true  },
			},
			handler(ctx) {
				const error = {
					code: this.hashCode(ctx.params.message),
					type: ctx.params.type,
					field: ctx.params.field,
					real: ctx.params.real,
					expected: ctx.params.expected,
					message: ctx.params.message,
					translations: [],
				};

				validator_messages.map((vml) => {
					const error_translation = {
						code: error.code,
						language_id: vml.language_id,
						message: ctx.params.message,
					};

					if (vml.messages[ctx.params.type]) {
						let message = vml.messages[ctx.params.type];
						message = message.replace("{field}", ctx.params.field);
						if (ctx.params.real) {
							message = message.replace("{real}", ctx.params.real);
						}
						if (ctx.params.expected) {
							message = message.replace("{expected}", ctx.params.expected);
						}

						error_translation.message = message;
					}
					error.translations.push(error_translation);
				});

				return error;
			},
		},
		getError: {
			params: {
				type: { type: "string" },
				message: { type: "string", optional: true },
				data: { type: "object", optional: true }
			},
			async handler(ctx) {
				let error = await ctx.call("configuration.errors.find", {
					query: {
						type: ctx.params.type,
					},
					limit: 1,
				});

				// IF THE ERROR DONT EXIST CREATE ON THE DATABASE
				if (error.length === 0) {
					let newError = await this._create(ctx, {
						...ctx.params,
					});
					newError = newError[0];

					const languages = await ctx.call("configuration.languages.find", {
						query: {
							active: 1,
						},
					});

					await Promise.all(
						languages.map((language) =>
							ctx.call("configuration.errors_translations.create", {
								language_id: language.id,
								type: newError.type,
								message: newError.message,
							})
						),
					);

					error = newError;
				} else {
					error = error[0];
				}

				const error_translations = await ctx.call("configuration.errors_translations.find", {
					query: { type: error.type },
					withRelated: "language",
				});

				error.translations = error_translations;

				if(process.env.NODE_ENV !== "production") {
					error.message = ctx.params.message;
				}

				error.data = ctx.params.data || {};

				return error;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		hashCode(str) {
			let hash = 5381;
			let i = str.length;

			while (i) {
				hash = (hash * 33) ^ str.charCodeAt(--i);
			}

			/* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
			 * integers. Since we want the results to be always positive, convert the
			 * signed int to an unsigned by doing an unsigned bitshift. */
			return hash >>> 0;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
