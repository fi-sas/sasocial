"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.errors_translations",
	table: "error_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "errors_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "type", "language_id", "language", "message"],
		defaultWithRelateds: [],
		withRelateds: {
			language(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.languages.get", {
								id: doc.language_id,
							})
							.then((res) => {
								doc.language = res[0];
							});
					}),
				);
			},
		},
		entityValidator: {
			language_id: { type: "number", convert: true },
			type: { type: "string" },
			message: { type: "string" },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
