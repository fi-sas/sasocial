"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.footers",
	table: "footer",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "footers")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "label", "date_begin", "date_end", "updated_at", "created_at"],
		defaultWithRelateds: ["groups", "translations"],
		withRelateds: {
			groups(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.groups_footers.groups_of_footer", { footer_id: doc.id })
							.then((res) => (doc.groups = res));
					}),
				);
			},
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.footers_translations",
					"translations",
					"id",
					"footer_id",
				);
			},
		},
		entityValidator: {
			label: { type: "string", max: 120 },
			date_begin: { type: "date", convert: true },
			date_end: { type: "date", convert: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			translations: {
				type: "array",
				item: {
					language_id: { type: "number", positive: true, integer: true, convert: true },
					file_id: { type: "number", positive: true, integer: true, convert: true },
				},
			},
			group_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateGroupsIds",
				"validateFileIds",
				"validateLanguageIds",
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				"validateFileIds",
				"validateGroupsIds",
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveGroups", "saveTranslations"],
			update: ["saveGroups", "saveTranslations"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateGroupsIds(ctx) {
			const ids = ctx.params.group_ids;

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.groups_devices.get", {
						id,
					});
				}),
			);
		},
		async validateFileIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.file_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("media.files.get", {
						id,
					});
				}),
			);
		},
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveGroups(ctx, res) {
			await ctx.call("configuration.groups_footers.save_groups_of_footer", {
				footer_id: res[0].id,
				group_ids: ctx.params.group_ids,
			});

			res[0].groups = await ctx.call("configuration.groups_footers.groups_of_footer", {
				footer_id: res[0].id,
			});

			return res;
		},
		async saveTranslations(ctx, res) {
			await ctx.call("configuration.footers_translations.save_translations", {
				footer_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("configuration.footers_translations.find", {
				query: {
					footer_id: res[0].id,
				},
			});

			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
