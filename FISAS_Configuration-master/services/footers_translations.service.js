"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.footers_translations",
	table: "footer_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "footers_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "footer_id", "language_id", "file_id"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			footer_id: { type: "number", positive: true, integer: true, convert: true },
			file_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				footer_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						file_id: { type: "number", positive: true, integer: true, convert: true },
						language_id: { type: "number", positive: true, integer: true, convert: true },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					footer_id: ctx.params.footer_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					footer_id: ctx.params.footer_id,
					language_id: translation.language_id,
					file_id: translation.file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
