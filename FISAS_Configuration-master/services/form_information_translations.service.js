
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.form_information_translations",
	table: "form_information_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "form_information_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"form_information_id",
			"language_id",
			"info",
		],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			form_information_id: { type: "number", convert: true },
			language_id: { type: "number", convert: true },
			info: { type: "string" },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_translations: {
			params: {
				form_information_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						info: { type: "string" },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					form_information_id: ctx.params.form_information_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					form_information_id: ctx.params.form_information_id,
					language_id: translation.language_id,
					info: translation.info,
				}));
				return this._insert(ctx, { entities });
			},
		}
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
