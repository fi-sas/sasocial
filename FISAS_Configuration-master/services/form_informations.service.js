
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const Validator = require("moleculer").Validator;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.form_informations",
	table: "form_information",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "form_informations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"key",
			"visible",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("configuration.form_information_translations.find", { query: { form_information_id: doc.id, language_id: ctx.meta.language_id } })
								.then((translation) => { doc.translations = translation; });
						}),
					);
				} else {
					return hasMany(docs, ctx, "configuration.form_information_translations", "translations", "id", "form_information_id",);
				}
			},
		},
		entityValidator: {
			name: { type: "string" },
			key: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			translations: {
				type: "array",
				item: {
					language_id: { type: "number", positive: true, integer: true, convert: true },
					info: { type: "string" },
				},
				convert: true,
				optional: true
			},
			visible: { type: "boolean" }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			update: [
				"validateLanguages",
				"preventKeyChange",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			patch: [
				"validateLanguages",
				"preventKeyChange",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],

		},
		after: {
			update: ["saveTranslations"],
			patch: ["saveTranslations"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys:
					["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguages(ctx) {
			if (Array.isArray(ctx.params.translations)) {
				for (const translation of ctx.params.translations) {
					await ctx.call("configuration.languages.get", { id: translation.language_id });
				}
			}
			if (ctx.params.visible) {
				const v = new Validator();
				const schema = {
					translations: {
						type: "array",
						item: {
							language_id: { type: "number", positive: true, integer: true, convert: true },
							info: { type: "string" },
						},
						convert: true,
						min: 1
					},
				};
				const check = v.compile(schema);
				const res = check(ctx.params);
				if (res !== true)
					return Promise.reject(
						new ValidationError("Entity validation error!", null, res),
					);
			}
		},
		async saveTranslations(ctx, res) {
			res[0].translations = await ctx.call("configuration.form_information_translations.save_translations", {
				form_information_id: res[0].id,
				translations: ctx.params.translations,
			});
			return res;
		},
		async preventKeyChange(ctx) {
			const form_translation = await this._get(ctx, { id: ctx.params.id });
			ctx.params.key = form_translation[0].key;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
