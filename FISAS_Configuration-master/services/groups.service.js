"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.groups",
	table: "group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "groups")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "active", "updated_at", "created_at"],
		defaultWithRelateds: ["devices"],
		withRelateds: {
			devices(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.groups_devices.devices_of_group", { group_id: doc.id })
							.then((res) => (doc.devices = res));
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean" },
			devices: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
				},
				optional: true,
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateDevices",
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateDevices",
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveDevices"],
			update: ["saveDevices"],
			patch: ["saveDevices"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveDevices(ctx, response) {
			if (ctx.params.devices && Array.isArray(ctx.params.devices)) {
				const device_ids = [...new Set(ctx.params.devices)];
				await ctx.call("configuration.groups_devices.save_devices_of_group", {
					group_id: response[0].id,
					device_ids,
				});
				response[0].devices = await ctx.call("configuration.groups_devices.devices_of_group", {
					group_id: response[0].id,
				});
			}
			return response;
		},
		async validateDevices(ctx) {
			if (ctx.params.devices && Array.isArray(ctx.params.devices)) {
				const devices = [...new Set(ctx.params.devices)];

				const scopes = await ctx.call("configuration.devices.count", {
					query: {
						id: devices,
					},
				});

				if (scopes < devices.length) {
					throw new EntityNotFoundError("devices", null);
				}
			} else {
				ctx.devices = [];
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
