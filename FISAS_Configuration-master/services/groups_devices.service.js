"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.groups_devices",
	table: "group_device",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "groups_devices")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "group_id", "device_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			group_id: { type: "number", positive: true, integer: true, convert: true },
			device_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		groups_of_device: {
			cache: {
				keys: ["device_id"],
				ttl: 60,
			},
			visibility: "public",
			rest: {
				path: "GET /device_groups/:device_id/groups",
			},
			params: {
				device_id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						device_id: ctx.params.device_id,
					},
				}).then((res) => {
					return ctx.call("configuration.groups.get", {
						withRelated: false,
						id: res.map((r) => r.group_id),
					});
				});
			},
		},
		devices_of_group: {
			cache: {
				keys: ["group_id"],
				ttl: 60,
			},
			visibility: "public",
			rest: {
				path: "GET /groups_devices/:group_id/devices",
			},
			params: {
				group_id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						group_id: ctx.params.group_id,
					},
				}).then((res) => {
					return ctx.call("configuration.devices.get", {
						withRelated: false,
						id: res.map((r) => r.device_id),
					});
				});
			},
		},
		save_devices_of_group: {
			params: {
				group_id: { type: "number", positive: true, integer: true, convert: true },
				device_ids: {
					type: "array",
					item: { type: "number", positive: true, integer: true, convert: true },
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					group_id: ctx.params.group_id,
				});
				this.clearCache();

				const entities = ctx.params.device_ids.map((device_id) => ({
					group_id: ctx.params.group_id,
					device_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
