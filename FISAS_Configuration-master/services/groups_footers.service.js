"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.groups_footers",
	table: "group_footer",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "groups_footers")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "footer_id", "group_id"],
		defaultWithRelateds: [],
		withRelateds: {},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		groups_of_footer: {
			cache: {
				keys: ["footer_id"],
				ttl: 60,
			},
			visibility: "public",
			rest: {
				path: "GET /footers/:footer_id/groups",
			},
			params: {
				footer_id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						footer_id: ctx.params.footer_id,
					},
				}).then((res) => {
					return ctx.call("configuration.groups.get", {
						id: res.map((r) => r.group_id),
					});
				});
			},
		},
		save_groups_of_footer: {
			params: {
				footer_id: { type: "number", positive: true, integer: true, convert: true },
				group_ids: {
					type: "array",
					item: { type: "number", positive: true, integer: true, convert: true },
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					footer_id: ctx.params.footer_id,
				});
				this.clearCache();

				const entities = ctx.params.group_ids.map((group_id) => ({
					footer_id: ctx.params.footer_id,
					group_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
