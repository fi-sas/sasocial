"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.holidays",
	table: "holiday",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "holidays")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "date", "created_at", "updated_at"],
		defaultWithRelateds: ["organic_units","translations"],
		withRelateds: {
			organic_units(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.holiday_organic_unit.holiday_organic_units", {
								holiday_id: doc.id,
							})
							.then((res) => {
								doc.organic_units = res;
							});
					}),
				);
			},
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.holiday_translations",
					"translations",
					"id",
					"holiday_id",
				);
			},
		},
		entityValidator: {
			date: { type: "date", convert: true },
			organic_unit_ids: {
				type: "array",
				optional: true,
				item: {
					organic_unit_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateDateUnique",
				"validateLanguageIds",
				"validateOrganicUnits",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateDateUnique",
				"validateLanguageIds",
				"validateOrganicUnits",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveTranslations","saveHolidayOrganicUnits"],
			update: ["saveTranslations","saveHolidayOrganicUnits"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);
				return this.adapter.db.transaction(async (trx) => {
					return this.adapter
						.db("holiday_organic_unit")
						.transacting(trx)
						.where("holiday_id", id)
						.del()
						.then(() =>
							this.adapter
								.db("holiday_translation")
								.transacting(trx)
								.where("holiday_id", id)
								.del(),
						)
						.then(() => {
							return this.adapter.db("holiday").transacting(trx).where("id", id).del();
						})
						.then((doc) => {
							if (!doc)
								return Promise.reject(new Errors.EntityNotFoundError("holiday", params.id));
							this.clearCache();
							return this.transformDocuments(ctx, {}, doc).then((json) =>
								this.entityChanged("REMOVED!", json, ctx).then(() => json),
							);
						});
				});
			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {

		async validateDateUnique(ctx){
			await this._find(ctx, {
				query: {
					date: ctx.params.date,
				},
			}).then(res => {
				const check = res.filter((r)=> r.id!=ctx.params.id);
				if(check.length > 0){
					throw new Errors.ValidationError(
						"Date already exists.",
						"CONFIGURATION_HOLIDAY_DATE_EXISTS",
					);
				}
			});
		},

		async validateOrganicUnits(ctx) {
			if (ctx.params.organic_unit_ids && Array.isArray(ctx.params.organic_unit_ids)) {
				await Promise.all(
					ctx.params.organic_unit_ids.map((id) => {
						ctx.call("infrastructure.organic-units.get", {
							id,
						});
					}),
				);
			}
		},

		async saveHolidayOrganicUnits(ctx, response) {
			if (ctx.params.organic_unit_ids && Array.isArray(ctx.params.organic_unit_ids)) {
				const organic_unit_ids = [...new Set(ctx.params.organic_unit_ids)];
				await ctx.call("configuration.holiday_organic_unit.save_specialty_appointment_types", {
					holiday_id: response[0].id,
					organic_unit_ids,
				});
				response[0].organic_units = await ctx.call(
					"configuration.holiday_organic_unit.holiday_organic_units",
					{
						holiday_id: response[0].id,
					},
				);
			}
			return response;
		},

		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveTranslations(ctx, response) {
			await ctx.call("configuration.holiday_translations.save_translations", {
				holiday_id: response[0].id,
				translations: ctx.params.translations,
			});
			response[0].translations = await ctx.call("configuration.holiday_translations.find", {
				query: {
					holiday_id: response[0].id,
				},
			});
			return response;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
