"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.holiday_organic_unit",
	table: "holiday_organic_unit",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "holiday_organic_unit")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "holiday_id", "organic_unit_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			holiday_id: { type: "number", positive: true },
			organic_unit_id: { type: "number", positive: true }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		holiday_organic_units: {
			params: {
				holiday_id: {
					type: "number",
					convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						holiday_id: ctx.params.holiday_id
					}
				}).then(response => {
					return ctx.call("infrastructure.organic-units.get", {
						withRelated: null,
						id: response.map(r => r.organic_unit_id)
					});
				});
			}
		},
		save_specialty_appointment_types: {
			params: {
				holiday_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				organic_unit_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				if(ctx.params.organic_unit_ids && Array.isArray(ctx.params.organic_unit_ids)){
					await this.adapter.removeMany({
						holiday_id: ctx.params.holiday_id
					});
					this.clearCache();
					const entities = ctx.params.organic_unit_ids.map((organic_unit_id) => ({
						holiday_id: ctx.params.holiday_id,
						organic_unit_id,
					}));
					return this._insert(ctx, { entities });
				}
			},
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
