"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.informations",
	table: "information",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "informations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "language_id", "service_id", "text", "created_at", "updated_at"],
		defaultWithRelateds: ["files"],
		withRelateds: {
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.services", "service", "service_id");
			},
			language_id(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.languages", "language", "language_id");
			},
			files(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.information_files",
					"files",
					"id",
					"information_id",
				);
			},
		},
		entityValidator: {
			language_id: { type: "number", positive: true },
			service_id: { type: "number", positive: true },
			text: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			file_ids: {
				type: "array",
				item: { type: "number", positive: true, integer: true, convert: true }
			}
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function sanatize(ctx) {
					await ctx.call("configuration.languages.get", { id: ctx.params.language_id });
					await ctx.call("configuration.services.get", { id: ctx.params.service_id });
					const existentTranslations = await this._find(ctx, { query: { language_id: ctx.params.language_id, service_id: ctx.params.service_id } });
					if (existentTranslations.length > 0) {
						throw new ValidationError(
							"This service already have a translation",
							"CONFIGURATION_TRANSLATION_ALREADY_EXIST",
							{}
						);
					}
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				async function sanatize(ctx) {
					ctx.params.updated_at = new Date();
					const regulation = await this._get(ctx, { id: ctx.params.id });
					if (regulation[0].service_id != ctx.params.service_id || regulation[0].language_id != ctx.params.language_id) {
						const list = await this._find(ctx, { query: { service_id: ctx.params.service_id, language_id: ctx.params.language_id } });
						if (list.length > 1) {
							throw new ValidationError(
								"This service already have a translation",
								"CONFIGURATION_TRANSLATION_ALREADY_EXIST",
								{}
							);
						}
						await ctx.call("configuration.languages.get", { id: ctx.params.language_id });
						await ctx.call("configuration.services.get", { id: ctx.params.service_id });
					}
				},
			],
			remove: [
				async function sanatize(ctx) {
					await ctx.call("configuration.informations.get", { id: ctx.params.id });
					await this.deleteWithRelated(ctx, "configuration.information_files.remove", await ctx.call("configuration.information_files.find", { query: { information_id: ctx.params.id } }));
				}
			],
			get: [
				function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query ? JSON.parse(ctx.params.query) : {};
						ctx.params.query.language_id = ctx.meta.language_id;
					}
				}],
			list: [
				function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query ? JSON.parse(ctx.params.query) : {};
						ctx.params.query.language_id = ctx.meta.language_id;
					}
				}]
		},
		after: {
			create: [
				async function sanatize(ctx, response) {
					const information_files = [];
					for (const file of ctx.params.file_ids) {
						information_files.push({ file_id: file });
					}
					response[0].files = await this.createWithRelated(ctx, "configuration.information_files.create", response[0].id, information_files);
					return response;
				}
			],
			update: [

				async function sanatize(ctx, response) {
					await this.deleteWithRelated(ctx, "configuration.information_files.remove", await ctx.call("configuration.information_files.find", { query: { information_id: response[0].id } }));

					const information_files = [];
					for (const file of ctx.params.file_ids) {
						information_files.push({ file_id: file });
					}
					response[0].files = await this.createWithRelated(ctx, "configuration.information_files.create", response[0].id, information_files);
					return response;
				}
			],
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],
	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys:
					["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} information_id
		 * @param {*} list
		 */
		async createWithRelated(ctx, service, information_id, list) {
			if (!(list instanceof Array)) { return []; }
			const listToReturn = [];
			for (const item of list) {
				item.information_id = information_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		*/
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) { return; }
			for (const item of list) {
				await ctx.call(service, item);
			}
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
