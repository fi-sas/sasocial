"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.languages",
	table: "language",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "languages")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "acronym", "order", "active", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			acronym: { type: "string", min: 2, max: 2, lowercase: true },
			order: { type: "number", convert: true, optional: true },
			active: { type: "boolean" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			$$strict: "remove",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					const order = await this._count(ctx);
					ctx.params.order = +order + 1;
				},
			],
			update: [
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "private",
		},
		reorder: {
			rest: "PATCH /reorder",
			visibility: "published",
			params: {
				languages: {
					type: "array", items: {
						type: "object", props: {
							id: { type: "number", positive: true },
							order: { type: "number", positive: true }
						}
					}
				},
			},
			async handler(ctx) {
				for (const lang of ctx.params.languages) {
					await ctx.call("configuration.languages.patch", { id: lang.id, order: lang.order, updated_at: new Date() });
				}
				return await ctx.call("configuration.languages.list");
			}
		},
		getByAcronym: {
			cache: {
				keys: ["acronym", "withRelated", "fields", "mapping"],
			},
			visibility: "public",
			params: {
				acronym: [{ type: "string" }],
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				mapping: { type: "boolean", optional: true },
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);

				let acronym = params.acronym;
				return this.adapter
					.find({
						limit: 1,
						query: {
							acronym,
						},
					})
					.then((docs) => {
						if (docs.length === 0)
							return Promise.reject(new EntityNotFoundError("LANGUAGE", acronym));
						return docs;
					})
					.then((docs) => this.transformDocuments(ctx, params, docs));
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "private",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "private",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "private",
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
