"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.regulations",
	table: "regulation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "regulations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "language_id", "service_id", "regulation", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.services", "service", "service_id");
			},
			language_id(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.languages", "language", "language_id");
			}

		},
		entityValidator: {
			language_id: { type: "number", positive: true },
			service_id: { type: "number", positive: true },
			regulation: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateServiceAndLanguage",
				async function sanatize(ctx) {
					const existentTranslations = await this._find(ctx, { query: { language_id: ctx.params.language_id, service_id: ctx.params.service_id } });
					if (existentTranslations.length > 0) {
						throw new ValidationError(
							"This service already have a translation",
							"CONFIGURATION_TRANSLATION_ALREADY_EXIST",
							{}
						);
					}
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateServiceAndLanguage",
				async function sanatize(ctx) {
					ctx.params.updated_at = new Date();
					const regulation = await this._get(ctx, { id: ctx.params.id });
					if (regulation[0].service_id != ctx.params.service_id || regulation[0].language_id != ctx.params.language_id) {
						const list = await this._find(ctx, { query: { service_id: ctx.params.service_id, language_id: ctx.params.language_id } });
						if (list.length > 1) {
							throw new ValidationError(
								"This service already have a translation",
								"CONFIGURATION_TRANSLATION_ALREADY_EXIST",
								{}
							);
						}
					}
				},
			],
			get: [
				function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query ? JSON.parse(ctx.params.query) : {};
						ctx.params.query.language_id = ctx.meta.language_id;
					}
				}],
			list: [
				function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query ? JSON.parse(ctx.params.query) : {};
						ctx.params.query.language_id = ctx.meta.language_id;
					}
				}]
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys:
					["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {

		async validateServiceAndLanguage(ctx) {
			await ctx.call("configuration.languages.get", { id: ctx.params.language_id });
			await ctx.call("configuration.services.get", { id: ctx.params.service_id });
		}

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
