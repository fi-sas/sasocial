"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.sections_bo",
	table: "section_bo",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "sections_bo")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "priority", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {
			services(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("configuration.services.find", {
								query: { section_bo_id: doc.id, active: true }, sort: "priority", })
							.then((service) => {
								doc.services = service;
							});
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			priority: { type: "number", convert: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				async function sanatizeParams(ctx) {
					const services = await ctx.call("configuration.services.find", {
						query: {
							section_bo_id: ctx.params.id
						},
					});

					if (services.length > 0) {
						throw new ValidationError(
							"You can not remove section, there are associated services",
							"CONFIGURATION_SECTIONS_BO_ASSOCIATED_SERVICES", {});
					}
				}
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			authorization: false,
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {
		"configuration.services.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
