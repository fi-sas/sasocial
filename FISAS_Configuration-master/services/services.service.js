"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.services",
	table: "service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "services")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "is_external_service", "external_link", "kiosk_availability", "webpage_availability", "app_availability", "icon", "section_bo_id", "priority", "updated_at", "created_at"],
		defaultWithRelateds: ["translations", "profiles", "section_bo"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.services_translations",
					"translations",
					"id",
					"service_id",
				);
			},
			profiles(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"configuration.services_profiles",
					"profiles",
					"id",
					"service_id",
				);
			},
			section_bo(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.sections_bo", "section_bo", "section_bo_id");
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: { type: "number", positive: true, integer: true, convert: true },
					name: { type: "string" },
					description: { type: "string" },
				},
			},
			profiles: {
				type: "array",
				item: {
					profile_id: { type: "number", positive: true, integer: true, convert: true },
				},
			},
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name", "description"],
						"configuration.services_translations",
						"service_id",
					);
				},
			],
			create: [
				"validateLanguageIds",
				"validateIsExternalService",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"validateIsExternalService",
				async function sanatizeParams(ctx) {
					await this.deleteWithRelated(ctx, "configuration.services_translations.remove",
						await ctx.call("configuration.services_translations.find", { query: { service_id: ctx.params.id } }));
					await this.deleteWithRelated(ctx, "configuration.services_profiles.remove",
						await ctx.call("configuration.services_profiles.find", { query: { service_id: ctx.params.id } }));
				}
			],
		},
		after: {
			create: ["saveTranslations", "saveProfiles"],
			update: ["saveTranslations", "saveProfiles"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		external_services: {
			cache: {
				keys: ["#user.profile_id","#language_id"]
			},
			rest: "GET /external_kiosk",
			scope: "configuration:services:read",
			handler(ctx) {
				return this._find(ctx, {
					withRelated: ["translations", "profiles"],
					query: (qb) => {
						qb.select("service.*");
						qb.from("service");
						qb.innerJoin("service_profile", "service.id", "service_profile.service_id");
						qb.where("service.active", "=", true);
						qb.andWhere("service.is_external_service", "=", true);
						qb.andWhere("service.kiosk_availability", "=", true);
						qb.andWhere("service_profile.profile_id", "=", ctx.meta.user.profile_id);
					},
				});
			}
		},
		wp_dashboard: {
			visibility: "public",
			params: {},
			async handler(ctx) {
				return this._find(ctx, {
					withRelated: ["translations", "profiles"],
					query: (qb) => {
						qb.select("service.*");
						qb.from("service");
						qb.where("service.active", "=", true);
						qb.andWhere("service.is_external_service", "=", false);
						qb.andWhere("service.webpage_availability", "=", true);
						qb.union(qb =>
							qb
								.select("service.*").from("service")
								.innerJoin("service_profile", "service.id", "service_profile.service_id")
								.where("service.active", "=", true)
								.andWhere("service.is_external_service", "=", true)
								.andWhere("service.webpage_availability", "=", true)
								.andWhere("service_profile.profile_id", "=", ctx.meta.user.profile_id)
						);
					},
				});
			},
		},
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async validateIsExternalService(ctx) {
			if (ctx.params.id === undefined) {
				// Insert
				if (ctx.params.is_external_service !== true) {
					throw new ValidationError(
						"You can only insert external services",
						"CONFIGURATION_INSERT_ONLY_EXTERNAL_SERVICES", {});
				}
			} else {
				// Remove
				const services = await this._find(ctx, {
					query: {
						id: ctx.params.id
					}, withRelated: false,
				});

				if (services.length > 0) {
					if (services[0].is_external_service !== true) {
						throw new ValidationError(
							"You can only remove external services",
							"CONFIGURATION_IT_IS_NOT_AN_EXTERNAL_SERVICE", {});
					}
				}
			}
		},
		async saveTranslations(ctx, res) {
			await ctx.call("configuration.services_translations.save_translations", {
				service_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("configuration.services_translations.find", {
				query: {
					service_id: res[0].id,
				},
			});

			return res;
		},
		async saveProfiles(ctx, res) {
			await ctx.call("configuration.services_profiles.save_profiles", {
				service_id: res[0].id,
				profiles: ctx.params.profiles,
			});

			res[0].profiles = await ctx.call("configuration.services_profiles.find", {
				query: {
					service_id: res[0].id,
				},
			});

			return res;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) { return; }
			for (const item of list) {
				await ctx.call(service, item);
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
