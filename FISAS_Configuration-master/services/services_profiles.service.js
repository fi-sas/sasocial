"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.services_profiles",
	table: "service_profile",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "services_profiles")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "service_id", "profile_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			service_id: { type: "number", positive: true, integer: true, convert: true },
			profile_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	hooks: {
	},

	/**
	 * Actions
	 */
	actions: {
		save_profiles: {
			params: {
				service_id: { type: "number", positive: true, integer: true, convert: true },
				profiles: {
					type: "array",
					item: {
						profile_id: { type: "number", positive: true, integer: true, convert: true },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					service_id: ctx.params.service_id,
				});
				this.clearCache();

				const entities = ctx.params.profiles.map((profile) => ({
					service_id: ctx.params.service_id,
					profile_id: profile.profile_id,
				}));
				return this._insert(ctx, { entities });
			},
		},

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
