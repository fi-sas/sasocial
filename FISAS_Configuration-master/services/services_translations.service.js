"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.services_translations",
	table: "service_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "services_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "service_id", "language_id", "name", "description"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			service_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
			name: { type: "string" },
			description: { type: "string" },
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	hooks: {
		before: {
			find: [
				function languageFilter(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query.language_id = ctx.meta.language_id;
					}
				},
			],
		},
	},

	/**
	 * Actions
	 */
	actions: {
		find: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		save_translations: {
			params: {
				service_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
						description: { type: "string" },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					service_id: ctx.params.service_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					service_id: ctx.params.service_id,
					language_id: translation.language_id,
					name: translation.name,
					description: translation.description,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
