"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "configuration.term_files",
	table: "term_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("configuration", "term_files")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "term_id", "file_id"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			}
		},
		entityValidator: {
			term_id: { type: "number", positive: true, convert: true },
			file_id: { type: "number", positive: true, convert: true }
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			remove: [
				async function sanatize(ctx) {
					const term_file = await ctx.call("configuration.term_files.get", { id: ctx.params.id });
					try {
						await ctx.call("media.files.remove", { id: term_file[0].file_id });
					} catch (error) {
						return;
					}
				}
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
