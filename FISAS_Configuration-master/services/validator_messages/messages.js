const validator_messages =
	[
		{
			language_id: 4,
			messages: {
				required: "The '{field}' field is required.",
				string: "The '{field}' field must be a string.",
				stringEmpty: "The '{field}' field must not be empty.",
				stringMin: "The '{field}' field length must be greater than or equal to {expected} characters long.",
				stringMax: "The '{field}' field length must be less than or equal to {expected} characters long.",
				stringLength: "The '{field}' field length must be {expected} characters long.",
				stringPattern: "The '{field}' field fails to match the required pattern.",
				stringContains: "The '{field}' field must contain the '{expected}' text.",
				stringEnum: "The '{field}' field does not match any of the allowed values.",
				stringNumeric: "The '{field}' field must be a numeric string.",
				stringAlpha: "The '{field}' field must be an alphabetic string.",
				stringAlphanum: "The '{field}' field must be an alphanumeric string.",
				stringAlphadash: "The '{field}' field must be an alphadash string.",
				stringHex: "The '{field}' field must be a hex string.",

				number: "The '{field}' field must be a number.",
				numberMin: "The '{field}' field must be greater than or equal to {expected}.",
				numberMax: "The '{field}' field must be less than or equal to {expected}.",
				numberEqual: "The '{field}' field must be equal to {expected}.",
				numberNotEqual: "The '{field}' field can't be equal to {expected}.",
				numberInteger: "The '{field}' field must be an integer.",
				numberPositive: "The '{field}' field must be a positive number.",
				numberNegative: "The '{field}' field must be a negative number.",

				array: "The '{field}' field must be an array.",
				arrayEmpty: "The '{field}' field must not be an empty array.",
				arrayMin: "The '{field}' field must contain at least {expected} items.",
				arrayMax: "The '{field}' field must contain less than or equal to {expected} items.",
				arrayLength: "The '{field}' field must contain {expected} items.",
				arrayContains: "The '{field}' field must contain the '{expected}' item.",
				arrayUnique: "The '{real}' value in '{field}' field does not unique the '{expected}' values.",
				arrayEnum: "The '{real}' value in '{field}' field does not match any of the '{expected}' values.",

				tuple: "The '{field}' field must be an array.",
				tupleEmpty: "The '{field}' field must not be an empty array.",
				tupleLength: "The '{field}' field must contain {expected} items.",

				boolean: "The '{field}' field must be a boolean.",

				date: "The '{field}' field must be a Date.",
				dateMin: "The '{field}' field must be greater than or equal to {expected}.",
				dateMax: "The '{field}' field must be less than or equal to {expected}.",

				enumValue: "The '{field}' field value '{expected}' does not match any of the allowed values.",

				equalValue: "The '{field}' field value must be equal to '{expected}'.",
				equalField: "The '{field}' field value must be equal to '{expected}' field value.",

				forbidden: "The '{field}' field is forbidden.",

				function: "The '{field}' field must be a function.",

				email: "The '{field}' field must be a valid e-mail.",
				emailEmpty: "The '{field}' field must not be empty.",

				luhn: "The '{field}' field must be a valid checksum luhn.",

				mac: "The '{field}' field must be a valid MAC address.",

				object: "The '{field}' must be an Object.",
				objectStrict: "The object '{field}' contains forbidden keys: '{real}'.",
				objectMinProps: "The object '{field}' must contain at least {expected} properties.",
				objectMaxProps: "The object '{field}' must contain {expected} properties at most.",

				url: "The '{field}' field must be a valid URL.",
				urlEmpty: "The '{field}' field must not be empty.",

				uuid: "The '{field}' field must be a valid UUID.",
				uuidVersion: "The '{field}' field must be a valid UUID version provided.",

				classInstanceOf: "The '{field}' field must be an instance of the '{expected}' class."
			}
		}, {
			language_id: 3,
			messages: {
				required: "O campo '{field}' é obrigatório.",
				string: "O campo '{field}' deve ser uma string.",
				stringEmpty: "O campo '{field}' não deve estar vazio.",
				stringMin: "O comprimento do campo '{field}' deve ser maior que ou igual a {expected} caracteres.",
				stringMax: "O comprimento do campo '{field}' deve ser menor ou igual a {expected} caracteres.",
				stringLength: "O tamanho do campo '{field}' deve ter {expected} caracteres.",
				stringPattern: "O campo '{field}' falha ao corresponder ao padrão necessário.",
				stringContains: "O campo '{field}' deve conter o texto '{expected}'.",
				stringEnum: "O campo '{field}' não corresponde a nenhum dos valores permitidos.",
				stringNumeric: "O campo '{field}' deve ser uma sequência numérica.",
				stringAlpha: "O campo '{field}' deve ser uma sequência alfabética.",
				stringAlphanum: "O campo '{field}' deve ser uma sequência alfanumérica.",
				stringAlphadash: "O campo '{field}' deve ser uma sequência alfadash.",
				stringHex: "O campo '{field}' deve ser uma sequência hexadecimal.",

				number: "O campo '{field}' deve ser um número.",
				numberMin: "O campo '{field}' deve ser maior ou igual a {expected}.",
				numberMax: "O campo '{field}' deve ser menor ou igual a {expected}.",
				numberEqual: "O campo '{field}' deve ser igual a {expected}.",
				numberNotEqual: "O campo '{field}' não pode ser igual a {expected}.",
				numberInteger: "O campo '{field}' deve ser um número inteiro.",
				numberPositive: "O campo '{field}' deve ser um número positivo.",
				numberNegative: "O campo '{field}' deve ser um número negativo.",

				matriz: "O campo '{field}' deve ser uma matriz.",
				arrayEmpty: "O campo '{field}' não deve ser uma matriz vazia.",
				arrayMin: "O campo '{field}' deve conter pelo menos itens {expected}.",
				arrayMax: "O campo '{field}' deve conter itens iguais ou inferiores a {expected} itens.",
				arrayLength: "O campo '{field}' deve conter itens {expected}.",
				arrayContains: "O campo '{field}' deve conter o item '{expected}'.",
				arrayUnique: "O valor '{real}' no campo '{field}' não diferencia os valores '{expected}'.",
				arrayEnum: "O valor '{real}' no campo '{field}' não corresponde a nenhum dos valores '{expected}'.",

				tupla: "O campo '{field}' deve ser uma matriz.",
				tupleEmpty: "O campo '{field}' não deve ser uma matriz vazia.",
				tupleLength: "O campo '{field}' deve conter itens {expected}.",

				boolean: "O campo '{field}' deve ser um booleano.",

				date: "O campo '{field}' deve ser uma Data.",
				dateMin: "O campo '{field}' deve ser maior ou igual a {expected}.",
				dateMax: "O campo '{field}' deve ser menor ou igual a {expected}.",

				enumValue: "O valor do campo '{field}' não corresponde a nenhum dos valores permitidos '{expected}'.",

				equalValue: "O valor do campo '{field}' deve ser igual a '{expected}'.",
				equalField: "O valor do campo '{field}' deve ser igual ao valor do campo '{expected}'.",

				proibido: "O campo '{field}' é proibido.",

				function: "O campo '{field}' deve ser uma função.",

				email: "O campo '{field}' deve ser um email válido.",
				emailEmpty: "O campo '{field}' não deve estar vazio.",

				luhn: "O campo '{field}' deve ser uma soma de verificação válida luhn.",

				mac: "O campo '{field}' deve ser um endereço MAC válido.",

				objeto: "O '{field}' deve ser um objeto.",
				objectStrict: "O objeto '{field}' contém chaves proibidas: '{actual}'.",
				objectMinProps: "O objeto '{field}' deve conter pelo menos propriedades {previstas}.",
				objectMaxProps: "O objeto '{field}' deve conter no máximo as propriedades {esperadas}.",

				url: "O campo '{field}' deve ser um URL válido.",
				urlEmpty: "O campo '{field}' não deve estar vazio.",

				uuid: "O campo '{field}' deve ser um UUID válido.",
				uuidVersion: "O campo '{field}' deve ser uma versão UUID válida fornecida.",

				classInstanceOf: "O campo '{field}' deve ser uma instância da classe '{expected}'"
			}
		}
	];


module.exports = {
	validator_messages
};