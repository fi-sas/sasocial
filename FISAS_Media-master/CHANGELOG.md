## [1.3.1](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.3.0...v1.3.1) (2022-03-29)


### Bug Fixes

* **sasocial:** remove dependency ([fcca1ee](https://gitlab.com/fi-sas/FISAS_Media/commit/fcca1eef9769aefbaf77dd26e0d8737ffcea44ed))

# [1.3.0](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.2.0...v1.3.0) (2022-01-04)


### Features

* **files:** add file name to files ([a518bae](https://gitlab.com/fi-sas/FISAS_Media/commit/a518baee2102f8fc9ab812881210178cd2c0851b))

# [1.2.0](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.1.0...v1.2.0) (2021-06-16)


### Features

* **files:** add created_by and deleted_by fields to DB ([696d924](https://gitlab.com/fi-sas/FISAS_Media/commit/696d924ad8e57ca10f91b5852effa25a1db7ee43))

# [1.1.0](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0...v1.1.0) (2021-06-14)


### Features

* **files:** remove unused category and translations table ([e8c3c89](https://gitlab.com/fi-sas/FISAS_Media/commit/e8c3c897cbef46438fba657cad83b4154dcd5961))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **antivirus:** try reconnoct to clamav ([ba5b547](https://gitlab.com/fi-sas/FISAS_Media/commit/ba5b5475a7016437b261917113897fcc8a1c12d6))
* **docker-compose:** create docker-compose to prod and dev ([b6a06c6](https://gitlab.com/fi-sas/FISAS_Media/commit/b6a06c63c38133f29479e944c46e0bcbf4771599))
* **files:** add virtual url path to files ([2ebb91f](https://gitlab.com/fi-sas/FISAS_Media/commit/2ebb91f88e7bb2f912c63d8351fece88b5d60de6))
* **files:** check file validation error ([4e2001c](https://gitlab.com/fi-sas/FISAS_Media/commit/4e2001cf75adb70e54aec79189a7d15b029a31d1))
* **knex:** fix knex config ([9290877](https://gitlab.com/fi-sas/FISAS_Media/commit/9290877861feaded2a73af16dc3b486b981a4d52))
* **ms_core:** update ms_core package ([5e0bd41](https://gitlab.com/fi-sas/FISAS_Media/commit/5e0bd418a92140584c2b583a57b1011312cfbdb7))
* actions visibility changed to published ([c04bc72](https://gitlab.com/fi-sas/FISAS_Media/commit/c04bc721145c7917104b08db2f7ab1946c4b6df3))
* fixing node version (node:12-buster-slim) ([4ab0e31](https://gitlab.com/fi-sas/FISAS_Media/commit/4ab0e31c426fdbc015a72e7f99e65c8dd31e023c))
* single initial migration ([3397fd4](https://gitlab.com/fi-sas/FISAS_Media/commit/3397fd4708d1ec1f35396d7b4ad613306a8bd96e))


### Features

* **clamav:** add clamav service ([78fae2a](https://gitlab.com/fi-sas/FISAS_Media/commit/78fae2a3f1c7e74be74805d612876ed81582fd16))
* **files:** add file delete automation ([737dcd9](https://gitlab.com/fi-sas/FISAS_Media/commit/737dcd92b1ebf1aacddfcbcc4a3050513be671c6))
* **files:** add file upload action ([d44c76b](https://gitlab.com/fi-sas/FISAS_Media/commit/d44c76bcb66e4ba64641fb0cc75bef1d900e391d))
* migrate from MySQL into Postgres ([efbf603](https://gitlab.com/fi-sas/FISAS_Media/commit/efbf603381891f44c55b0601ac43ed30badc2906))
* **ms:** add the new boierplate structure ([25e3608](https://gitlab.com/fi-sas/FISAS_Media/commit/25e36086ac9bbdbbde367306d9c622c9a0453928))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-02-26)


### Bug Fixes

* **antivirus:** try reconnoct to clamav ([ba5b547](https://gitlab.com/fi-sas/FISAS_Media/commit/ba5b5475a7016437b261917113897fcc8a1c12d6))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-13)


### Bug Fixes

* **files:** check file validation error ([4e2001c](https://gitlab.com/fi-sas/FISAS_Media/commit/4e2001cf75adb70e54aec79189a7d15b029a31d1))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-11)


### Features

* **files:** add file delete automation ([737dcd9](https://gitlab.com/fi-sas/FISAS_Media/commit/737dcd92b1ebf1aacddfcbcc4a3050513be671c6))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-12-18)


### Bug Fixes

* **knex:** fix knex config ([9290877](https://gitlab.com/fi-sas/FISAS_Media/commit/9290877861feaded2a73af16dc3b486b981a4d52))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-18)


### Features

* **clamav:** add clamav service ([78fae2a](https://gitlab.com/fi-sas/FISAS_Media/commit/78fae2a3f1c7e74be74805d612876ed81582fd16))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-11-18)


### Bug Fixes

* **ms_core:** update ms_core package ([5e0bd41](https://gitlab.com/fi-sas/FISAS_Media/commit/5e0bd418a92140584c2b583a57b1011312cfbdb7))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/FISAS_Media/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-11-03)


### Bug Fixes

* fixing node version (node:12-buster-slim) ([4ab0e31](https://gitlab.com/fi-sas/FISAS_Media/commit/4ab0e31c426fdbc015a72e7f99e65c8dd31e023c))

# 1.0.0-rc.1 (2020-10-27)


### Bug Fixes

* actions visibility changed to published ([c04bc72](https://gitlab.com/fi-sas/FISAS_Media/commit/c04bc721145c7917104b08db2f7ab1946c4b6df3))
* single initial migration ([3397fd4](https://gitlab.com/fi-sas/FISAS_Media/commit/3397fd4708d1ec1f35396d7b4ad613306a8bd96e))
* **docker-compose:** create docker-compose to prod and dev ([b6a06c6](https://gitlab.com/fi-sas/FISAS_Media/commit/b6a06c63c38133f29479e944c46e0bcbf4771599))
* **files:** add virtual url path to files ([2ebb91f](https://gitlab.com/fi-sas/FISAS_Media/commit/2ebb91f88e7bb2f912c63d8351fece88b5d60de6))


### Features

* **files:** add file upload action ([d44c76b](https://gitlab.com/fi-sas/FISAS_Media/commit/d44c76bcb66e4ba64641fb0cc75bef1d900e391d))
* migrate from MySQL into Postgres ([efbf603](https://gitlab.com/fi-sas/FISAS_Media/commit/efbf603381891f44c55b0601ac43ed30badc2906))
* **ms:** add the new boierplate structure ([25e3608](https://gitlab.com/fi-sas/FISAS_Media/commit/25e36086ac9bbdbbde367306d9c622c9a0453928))

# 1.0.0-rc.1 (2020-10-19)


### Bug Fixes

* single initial migration ([3397fd4](https://gitlab.com/fi-sas/FISAS_Media/commit/3397fd4708d1ec1f35396d7b4ad613306a8bd96e))
* **docker-compose:** create docker-compose to prod and dev ([b6a06c6](https://gitlab.com/fi-sas/FISAS_Media/commit/b6a06c63c38133f29479e944c46e0bcbf4771599))
* **files:** add virtual url path to files ([2ebb91f](https://gitlab.com/fi-sas/FISAS_Media/commit/2ebb91f88e7bb2f912c63d8351fece88b5d60de6))


### Features

* **files:** add file upload action ([d44c76b](https://gitlab.com/fi-sas/FISAS_Media/commit/d44c76bcb66e4ba64641fb0cc75bef1d900e391d))
* migrate from MySQL into Postgres ([efbf603](https://gitlab.com/fi-sas/FISAS_Media/commit/efbf603381891f44c55b0601ac43ed30badc2906))
* **ms:** add the new boierplate structure ([25e3608](https://gitlab.com/fi-sas/FISAS_Media/commit/25e36086ac9bbdbbde367306d9c622c9a0453928))
