module.exports.up = async (db) =>
	db.schema
		.createTable("file_category", (table) => {
			table.increments("id");
			table
				.integer("file_category_id")
				.nullable()
				.unsigned()
				.references("id")
				.inTable("file_category")
				.onUpdate("No Action")
				.onDelete("Set NULL");
			table.string("name", 45).notNullable();
			table.text("description");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("file", (table) => {
			table.increments("id");
			table
				.integer("file_category_id")
				.unsigned()
				.references("id")
				.inTable("file_category")
				.onDelete("Set NULL");
			table.boolean("public").notNullable().defaultTo(1);
			table.enu("type", ["IMAGE", "VIDEO", "DOCUMENT"]).notNullable();
			table.string("mime_type", 120).notNullable();
			table.text("path").notNullable();
			table.float("weight").notNullable();
			table.integer("width");
			table.integer("height");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("file_translation", (table) => {
			table.increments();
			table.integer("language_id").notNullable();
			table
				.integer("file_id")
				.unsigned()
				.references("id")
				.inTable("file")
				.notNullable()
				.onDelete("Cascade");
			table.string("name", 255);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique(["language_id", "file_id"]);
		});

module.exports.down = async (db) =>
	db.schema
		.dropTableIfExists("file_category")
		.dropTableIfExists("file")
		.dropTableIfExists("file_translation");

module.exports.configuration = {
	transaction: true,
};
