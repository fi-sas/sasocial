exports.up = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.datetime("file_deleted_at").nullable();
		table.datetime("deleted_at").nullable();
		table.boolean("file_deleted").notNullable().defaultTo(false);
		table.boolean("deleted").notNullable().defaultTo(false);
	});

exports.down = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.dropColumn("deleted_at");
		table.dropColumn("file_deleted_at");
		table.dropColumn("file_deleted");
		table.dropColumn("deleted");
	});
