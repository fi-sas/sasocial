exports.up = async (db) =>
	db.schema
		.alterTable("file", (table) => {
			table.dropColumn("file_category_id");
		})
		.dropTable("file_category")
		.dropTable("file_translation");

exports.down = async (db) =>
	db.schema
		.createTable("file_category", (table) => {
			table.increments("id");
			table
				.integer("file_category_id")
				.nullable()
				.unsigned()
				.references("id")
				.inTable("file_category")
				.onUpdate("No Action")
				.onDelete("Set NULL");
			table.string("name", 45).notNullable();
			table.text("description");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.alterTable("file", (table) => {
			table
				.integer("file_category_id")
				.nullable()
				.unsigned()
				.references("id")
				.inTable("file_category")
				.onUpdate("No Action")
				.onDelete("Set NULL");
		})
		.createTable("file_translation", (table) => {
			table.increments();
			table.integer("language_id").notNullable();
			table
				.integer("file_id")
				.unsigned()
				.references("id")
				.inTable("file")
				.notNullable()
				.onDelete("Cascade");
			table.string("name", 255);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique(["language_id", "file_id"]);
		});
