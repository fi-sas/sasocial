exports.up = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.integer("created_by").unsigned().nullable();
		table.integer("deleted_by").unsigned().nullable();
	});

exports.down = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.dropColumn("deleted_by");
		table.dropColumn("created_by");
	});
