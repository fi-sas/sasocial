exports.up = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.string("filename").notNullable().defaultTo("-");
	});

exports.down = async (db) =>
	db.schema.alterTable("file", (table) => {
		table.dropColumn("filename");
	});
