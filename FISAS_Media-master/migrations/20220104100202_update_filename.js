exports.up = function (knex) {
	return knex.raw("UPDATE file SET filename = path");
};

exports.down = function (knex) {
	return knex.raw("UPDATE file SET filename = '-'");
};
