"use strict";

const NodeClam = require("clamscan");
const Moleculer = require("moleculer");

//const AntiVirusService = require("moleculer-antivirus");
/*const {
	AntiVirusPingError,
	AntiVirusVersionError,
} = require("moleculer-antivirus/src/errors/index");*/
/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "media.antivirus",

	//mixins: [AntiVirusService],
	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		scan: {
			params: {
				path: { type: "string" },
			},
			async handler(ctx) {

				if(!this.clamAVScan) {
					await this.createClamdScan();
				}

				if (this.clamAVScan) {
					try {
						const result = await this.clamAVScan.is_infected(ctx.params.path);
						if (result.is_infected) {
							this.logger.info(`${result.file} is infected with ${result.viruses}!`);
							throw new Moleculer.Errors.MoleculerClientError(
								"Is not possible to finalize the upload, virus detected",
								400,
								"MEDIA_VIRUS_DETECTED",
								result,
							);
						}

						this.logger.info("ClamAV Result");
						this.logger.info(result);

						return result;
					} catch (err) {
						this.logger.info("ClamAV Error");
						this.logger.info(err);
						throw err;
						// Handle any errors raised by the code in the try block
					}
				} else {
					throw new Moleculer.Errors.MoleculerClientError(
						"The Antivirus is not responding",
						503,
						"MEDIA_ANTIVIRUS_NOT_RESPONDING",
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		createClamdScan() {
			return new Promise((resolve, reject) => {
				this.logger.info("Try Connect ClamAV");
				this.ClamScan = new NodeClam()
					.init({
						clamdscan: {
							socket: false, // Socket file for connecting via TCP
							host: "fisas_media_clamav", // IP of host to connect to TCP interface
							port: 3310, // Port of host to use when connecting via TCP interface
							timeout: 60000, // Timeout for scanning files
							local_fallback: false, // Do no fail over to binary-method of scanning
							path: "/usr/bin/clamdscan", // Path to the clamdscan binary on your server
							config_file: null, // Specify config file if it's in an unusual place
							multiscan: true, // Scan using all available cores! Yay!
							reload_db: false, // If true, will re-load the DB on every call (slow)
							active: true, // If true, this module will consider using the clamdscan binary
							bypass_test: false, // Check to see if socket is available when applicable
						},
						preference: "clamdscan", // If clamdscan is found and active, it will be used by default
					})
					.then((clamAVScan) => {
						this.clamAVScan = clamAVScan;
						resolve(clamAVScan);
						this.logger.info("ClamAV Connect Succeffuly");
					})
					.catch((err) => {
						this.logger.info("Erro on connection with ClamAV");
						reject(err);
					});
			});
		},
		retryPromise(fn, retriesLeft = 3, interval = 200) {
			this.logger.info("Start trying connect ClamAV");
			return new Promise((resolve, reject) => {
				return fn()
					.then(resolve)
					.catch((error) => {
						this.logger.info("Retrying Connection wiht ClamAV");
						if (retriesLeft === 1) {
							// reject('maximum retries exceeded');
							this.logger.info("Maxximmun of retrys on connection wiht ClamAV");
							reject(error);
							return;
						}

						setTimeout(() => {
							this.logger.info("retriesLeft: ", retriesLeft);
							// Passing on "reject" is the important part
							this.retryPromise(fn, retriesLeft - 1, interval).then(resolve, reject);
						}, interval);
					});
			});
		},
	},
	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.retryPromise(this.createClamdScan, 10, 5000);
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
