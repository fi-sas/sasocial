"use strict";
const { MoleculerClientError } = require("moleculer").Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Types = require("./values/types");
const crypto = require("crypto");
const path = require("path");
const fs = require("fs");
const sizeOf = require("image-size");
const _ = require("lodash");
const Cron = require("moleculer-cron");

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "media.files",
	table: "file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("media", "files"), Cron],

	/*
	 * Crons
	 */
	crons: [
		{
			name: "moveToTempAndDelete",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("media.files")
					.actions.checkDeleteFiles()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"public",
			"type",
			"mime_type",
			"path",
			"filename",
			"weight",
			"width",
			"height",
			"file_deleted",
			"deleted",
			"created_at",
			"updated_at",
			"file_deleted_at",
			"deleted_at",
			"created_by",
			"deleted_by",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			public: { type: "boolean" },
			type: { type: "enum", values: ["IMAGE", "VIDEO", "DOCUMENT"] },
			mime_type: { type: "string" },
			path: { type: "string" },
			filename: { type: "string" },
			weight: { type: "number", optional: true, default: 0, min: 0, convert: true },
			width: { type: "number", positive: true, convert: true, optional: true },
			height: { type: "number", positive: true, convert: true, optional: true },
			file_deleted: { type: "boolean", optional: true },
			file_deleted_at: { type: "date", convert: true, optional: true },
			deleted: { type: "boolean", optional: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			deleted_at: { type: "date", convert: true, optional: true },
			created_by: { type: "number", positive: true, optional: true },
			deleted_by: { type: "number", positive: true, optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			list: [
				function removeDeleted(ctx) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query.deleted = false;
				},
			],
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();

					ctx.params.file_deleted = false;
					ctx.params.file_deleted_at = null;
					ctx.params.deleted = false;
					ctx.params.deleted_at = null;
					ctx.params.created_by = ctx.meta.user ? ctx.meta.user.id : null;

					if (!ctx.params.filename) {
						ctx.params.filename = ctx.params.path;
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			get: ["validateIfIsNotDeleted", "addVirtualUrlField"],
			list: ["addVirtualUrlField"],
			find: ["addVirtualUrlField"],
			create: ["addVirtualUrlField"],
			update: ["addVirtualUrlField"],
			remove: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			async handler(ctx) {
				const file = await ctx.call("media.files.get", ctx.params);
				if (file[0].deleted) {
					throw new Errors.ValidationError("File already delete", "ERR_FILE_ALREADY_DELETED", {});
				}

				let params = {
					id: ctx.params.id,
					deleted: true,
					deleted_at: new Date(),
					deleted_by: ctx.meta.user ? ctx.meta.user.id : null,
				};
				return this._update(ctx, params, true).then(() => ({}));
			},
		},
		restoreFile: {
			visibility: "published",
			rest: "POST /:id/restore",
			scope: "media:files:restore-file",
			async handler(ctx) {
				//CHECK IF FILE EXIST AND IF IS NOT DELETED
				const files = await this._find(ctx, {
					withRelated: false,
					query: {
						id: ctx.params.id,
						deleted: true,
					},
					limit: 1,
				});
				if (files.length === 0) {
					throw new Errors.EntityNotFoundError("file", ctx.params.id);
				}

				const file = files[0];

				if (file.file_deleted) {
					// MODE FROM TEMP TO UPLOADS FOLDER
					let uploadsDir = "/uploads";
					let tempDir = "/uploads/deleteTmp";
					const oldFilePath = tempDir.concat("/", file.path);
					//CHECK IF FILE EXIST ON FOLDER
					try {
						if (fs.existsSync(oldFilePath)) {
							this.logger.info(`FILE FINDED  ${oldFilePath}`);
							// FILE EXIST MOVE TO TEMP FOLDER
							const newFilePath = uploadsDir.concat("/", file.path);
							fs.rename(oldFilePath, newFilePath, (err) => {
								if (err) throw err;
							});
						} else {
							//  FILE DONT EXIST ON  FOLDER DELETE THE ENTRY FROM DB
							this.logger.error(`FILE NOT FINDED  ${oldFilePath}`);
							this._remove(ctx, {
								id: file.id,
							});
							throw new Errors.EntityNotFoundError("file", ctx.params.id);
						}
					} catch (err) {
						this.logger.error(`ERRO ON CHECK IF FILE EXIST ${oldFilePath}`);
						this.logger.error(err);
					}
				}

				const params = {
					id: file.id,
					deleted: false,
					file_deleted: false,
					deleted_at: null,
					file_deleted_at: null,
				};
				return this._update(ctx, params, true);
			},
		},
		/**
		 * AFTER 1 HOUR MOVES THE FILE TO A TEMP FOLDER
		 * 15 DAYS AFTER IT WILL PERMANENTLY DELETE THE FILE
		 */
		checkDeleteFiles: {
			rest: "GET /checkDeleteFiles",
			visibility: "private",
			handler(ctx) {
				const result = {
					files_moved: 0,
					files_deleted: 0,
					files_not_finded: 0,
				};

				//CHECK IF TEMP FOOLDER EXIST
				let uploadsDir = "/uploads";
				let tempDir = "/uploads/deleteTmp";
				if (!fs.existsSync(tempDir)) {
					fs.mkdirSync(tempDir);
				}

				return this._list(ctx, {
					withRelated: false,
					query: (qb) => {
						qb.whereRaw("deleted_at <= (NOW() - INTERVAL '1 hours' ) and file_deleted = false");
						return qb;
					},
				})
					.then((filesToMove) => {
						if (filesToMove.rows && filesToMove.rows.length > 0) {
							this.logger.info(`FINDED ${filesToMove.rows.length} FILES TO MOVE`);
							return filesToMove.rows.map((file) => {
								const oldFilePath = uploadsDir.concat("/", file.path);
								this.logger.info(`START PROCCESSING FILE ${oldFilePath}`);

								//CHECK IF FILE EXIST ON FOLDER
								try {
									if (fs.existsSync(oldFilePath)) {
										this.logger.info(`FILE FINDED  ${oldFilePath}`);
										// FILE EXIST MOVE TO TEMP FOLDER
										const newFilePath = tempDir.concat("/", file.path);
										fs.rename(oldFilePath, newFilePath, (err) => {
											if (err) throw err;

											// IF MOVED UPDATE THE FILE TO FILE DELETED true
											this.logger.info(`FILE MOVED TO ${newFilePath}`);
											let params = {
												id: file.id,
												file_deleted: true,
												file_deleted_at: new Date(),
											};
											this._update(ctx, params, true);
											result.files_moved++;
										});
									} else {
										//  FILE DONT EXIST ON  FOLDER DELETE THE ENTRY FROM DB
										this.logger.error(`FILE NOT FINDED  ${oldFilePath}`);
										this._remove(ctx, {
											id: file.id,
										});
										result.files_not_finded++;
									}
								} catch (err) {
									this.logger.error(`ERRO ON CHECK IF FILE EXIST ${oldFilePath}`);
									this.logger.error(err);
								}
							});
						}
					})
					.then(() => {
						return this._list(ctx, {
							withRelated: false,
							query: (qb) => {
								qb.whereRaw(
									"file_deleted_at <= (NOW() - INTERVAL '15 days' ) and file_deleted = true",
								);
								return qb;
							},
						}).then((filesToDelete) => {
							if (filesToDelete.rows && filesToDelete.rows.length > 0) {
								this.logger.info(`FINDED ${filesToDelete.rows.length} FILES TO DELETE`);
								return filesToDelete.rows.map((file) => {
									const fileToDeletePath = tempDir.concat("/", file.path);
									this.logger.info(`START PROCCESSING FILE ${fileToDeletePath}`);

									//CHECK IF FILE EXIST ON FOLDER
									try {
										if (fs.existsSync(fileToDeletePath)) {
											this.logger.info(`FILE FINDED  ${fileToDeletePath}`);
											// FILE EXIST DELETE PERMANENTLY
											fs.unlinkSync(fileToDeletePath);
											this.logger.info("FILE DELETE PERMANENTLY");
											this._remove(ctx, {
												id: file.id,
											});
											result.files_deleted++;
										}
									} catch (err) {
										this.logger.error(
											`ERRO ON CHECK IF FILE EXIST OR DELETING ${fileToDeletePath}`,
										);
										this.logger.error(err);
									}
								});
							}
						});
					})
					.then(() => result);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		createUrlField(file) {
			const base = process.env.MEDIA_URL ? process.env.MEDIA_URL : null;
			file.url = `${base}/${file.path}`;
			return file;
		},
		addVirtualUrlField(ctx, response) {
			// find
			if (_.isArray(response)) {
				return response.map((file) => this.createUrlField(file));
			}

			// list
			if (response.rows) {
				response.rows = response.rows.map((file) => this.createUrlField(file));
				return response;
			}

			if (_.isObject(response)) {
				response = this.createUrlField(response);
			}

			return response;
		},
		validateIfIsNotDeleted(ctx, response) {
			if (response.length > 0 && response[0].deleted) {
				throw new Errors.EntityNotFoundError("file", response[0].id);
			}

			return response;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
