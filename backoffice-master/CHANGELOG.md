## [1.189.2](https://gitlab.com/fi-sas/backoffice/compare/v1.189.1...v1.189.2) (2022-06-29)


### Bug Fixes

* **accommodation:** fix build error ([c7d2a99](https://gitlab.com/fi-sas/backoffice/commit/c7d2a995812b80197ce47a8f05c5c4536cc50266))

## [1.189.1](https://gitlab.com/fi-sas/backoffice/compare/v1.189.0...v1.189.1) (2022-06-28)


### Bug Fixes

* **accommodation:** fix communication persistent filter ([df37783](https://gitlab.com/fi-sas/backoffice/commit/df37783f6584855f29ca64a9a31df68560ef080b))

# [1.189.0](https://gitlab.com/fi-sas/backoffice/compare/v1.188.0...v1.189.0) (2022-06-28)


### Bug Fixes

* **ubike:** get bikes when showing modal ([b1c0654](https://gitlab.com/fi-sas/backoffice/commit/b1c065486f40b79bf596904a9d89dfb09755f9b2))


### Features

* **ubike:** show available bikes ([8796f68](https://gitlab.com/fi-sas/backoffice/commit/8796f682045aea29f843b8966c51de3bf308113f))

# [1.188.0](https://gitlab.com/fi-sas/backoffice/compare/v1.187.1...v1.188.0) (2022-06-27)


### Features

* **accommodation:** fill updated user info ([f104c94](https://gitlab.com/fi-sas/backoffice/commit/f104c947aead59f3eeea8b680b332f335f33f0af))
* **accommodation:** show phases since today ([de91b21](https://gitlab.com/fi-sas/backoffice/commit/de91b21f6cd704536a3a6c60f67dfdcfdd63df7c))

## [1.187.1](https://gitlab.com/fi-sas/backoffice/compare/v1.187.0...v1.187.1) (2022-06-23)


### Bug Fixes

* **packs:** add active and desactive options and   small corrections ([80d9414](https://gitlab.com/fi-sas/backoffice/commit/80d9414f3964f8a9b6b92ad18cb99e5542d7bc31))

# [1.187.0](https://gitlab.com/fi-sas/backoffice/compare/v1.186.0...v1.187.0) (2022-06-23)


### Features

* **private_accommodation:** improve configurations page ([cd75833](https://gitlab.com/fi-sas/backoffice/commit/cd75833e6deda7734487f43033dffe750143746c))

# [1.186.0](https://gitlab.com/fi-sas/backoffice/compare/v1.185.0...v1.186.0) (2022-06-23)


### Features

* **food:** initial haccp module struct ([0b0f514](https://gitlab.com/fi-sas/backoffice/commit/0b0f5146cc86fa73a1631baca022f127b0caf5e4))

# [1.185.0](https://gitlab.com/fi-sas/backoffice/compare/v1.184.0...v1.185.0) (2022-06-20)


### Features

* **accommodation:** filter reports dropdown by academic year ([88d2c49](https://gitlab.com/fi-sas/backoffice/commit/88d2c4918b99c3ff374b6dbcdf81f511bdeacd15))

# [1.184.0](https://gitlab.com/fi-sas/backoffice/compare/v1.183.0...v1.184.0) (2022-06-20)


### Features

* **accommodation:** show extension attachments ([9d27239](https://gitlab.com/fi-sas/backoffice/commit/9d2723937c9c18c47ffcb4ee64b88392249e64b9))

# [1.183.0](https://gitlab.com/fi-sas/backoffice/compare/v1.182.0...v1.183.0) (2022-06-08)


### Bug Fixes

* **calendar:** fix event color category form ([49e0d01](https://gitlab.com/fi-sas/backoffice/commit/49e0d01bb41c493552441d76a581dafcf69c0343))
* **events:** fix descrition textarea autosize ([4f8c490](https://gitlab.com/fi-sas/backoffice/commit/4f8c490e5bc1cb144b22089745cf1aa11619efad))


### Features

* **accommodation:** new permissions ([cd81d3d](https://gitlab.com/fi-sas/backoffice/commit/cd81d3d0a1b03ca0bcf5c7659982b980f4d7b687))
* **calendar:** improve ui ux hours picker ([c92d45d](https://gitlab.com/fi-sas/backoffice/commit/c92d45d1bf1aa0d7014f9227bb26e18016d870f6))

# [1.182.0](https://gitlab.com/fi-sas/backoffice/compare/v1.181.0...v1.182.0) (2022-06-06)


### Bug Fixes

* **scholarship:** show complain attachment ([6da5e68](https://gitlab.com/fi-sas/backoffice/commit/6da5e68005d95b03b9fb9def7c05d92cc35eb01e))
* **volunteering:** show complain attachment file ([df614b6](https://gitlab.com/fi-sas/backoffice/commit/df614b6c5251844f0d253326a9a5ec9f4d96774c))


### Features

* **accommodation:** sort name and dates ([09ec9ef](https://gitlab.com/fi-sas/backoffice/commit/09ec9ef0a20cd46e2383bd52fc60db9447f3ce26))
* **ubike:** tin and phone number added to details ([5c56335](https://gitlab.com/fi-sas/backoffice/commit/5c56335bf2dc9e27bea1281aa8f3c8a15306f682))

# [1.181.0](https://gitlab.com/fi-sas/backoffice/compare/v1.180.1...v1.181.0) (2022-06-02)


### Features

* **accommodation:** block value over 100 ([1aebb8a](https://gitlab.com/fi-sas/backoffice/commit/1aebb8a7ea8174f9dde3a79f61a16ae554159b98))

## [1.180.1](https://gitlab.com/fi-sas/backoffice/compare/v1.180.0...v1.180.1) (2022-06-01)


### Bug Fixes

* **accommodation:** missing properties in acc communication model ([7293c4e](https://gitlab.com/fi-sas/backoffice/commit/7293c4e7270111511248ba55ae6f0ae26cc6c967))

# [1.180.0](https://gitlab.com/fi-sas/backoffice/compare/v1.179.0...v1.180.0) (2022-06-01)


### Features

* **ubike:** improve actions in new reports ([c9acdd9](https://gitlab.com/fi-sas/backoffice/commit/c9acdd98e0fdbc149a32bbc8452a747aa59d9ef4))
* **ubike:** new reports and modal to insert reports ([d7e1cb2](https://gitlab.com/fi-sas/backoffice/commit/d7e1cb2ec9510149c0aaddd0ac0db86b6ee2189a))

# [1.179.0](https://gitlab.com/fi-sas/backoffice/compare/v1.178.0...v1.179.0) (2022-06-01)


### Bug Fixes

* **accommodation:** show typology according with the residence ([f754484](https://gitlab.com/fi-sas/backoffice/commit/f754484aef38fa956129b91ba5f40d0b9b7cff3f))


### Features

* **accommodation:** improve renew process ([423798c](https://gitlab.com/fi-sas/backoffice/commit/423798cafee74ef5472608585ada69655659cc59))

# [1.178.0](https://gitlab.com/fi-sas/backoffice/compare/v1.177.0...v1.178.0) (2022-06-01)


### Bug Fixes

* **mobility:** change icon ([2f0d7fa](https://gitlab.com/fi-sas/backoffice/commit/2f0d7fad4c6f5a136d3de509214fcfc5c6abc5f0))


### Features

* **ubike:** new report list bike ([a7a1a12](https://gitlab.com/fi-sas/backoffice/commit/a7a1a12a8a24e67237dfd4fe81b5e8a5ff42763a))

# [1.177.0](https://gitlab.com/fi-sas/backoffice/compare/v1.176.0...v1.177.0) (2022-05-31)


### Features

* **accommodation:** add allow_application_renew configuration ([a4a9b2f](https://gitlab.com/fi-sas/backoffice/commit/a4a9b2f658a08663303f45393b2f8bc173daf573))

# [1.176.0](https://gitlab.com/fi-sas/backoffice/compare/v1.175.1...v1.176.0) (2022-05-30)


### Bug Fixes

* **accommodation:** academic year label change ([6a14bcd](https://gitlab.com/fi-sas/backoffice/commit/6a14bcd6cff8a8c437bdf6bdf20b7362b77d971b))
* **accommodation:** add unscubscrive to report requests ([98f6fd9](https://gitlab.com/fi-sas/backoffice/commit/98f6fd9fc25910ad73cfb6e5d9e29e79d637beda))
* **accommodation:** check if a fase is selected for report ([c9b2c3e](https://gitlab.com/fi-sas/backoffice/commit/c9b2c3ec022695f4685c81015c45cc84ab2d1dd3))
* **accommodation:** improve data listing by academic year ([a41bc71](https://gitlab.com/fi-sas/backoffice/commit/a41bc71b2bd13dcf673188ed398acecea53925f7))


### Features

* **accommodation:** block action if no params needed ([a4ef59c](https://gitlab.com/fi-sas/backoffice/commit/a4ef59cd9542b62cb1655cfcb066720c2e4976bd))
* **accommodation:** improve application renew and room map counting ([921f581](https://gitlab.com/fi-sas/backoffice/commit/921f5817c0235ea852fa1a304fd1f49e49ee333d))
* **accommodation:** label correction ([45bb98b](https://gitlab.com/fi-sas/backoffice/commit/45bb98b70c01ede27fd809f1c1c4101fd3cfa44f))
* **accommodation:** show exit and entry hour in communications ([153a61d](https://gitlab.com/fi-sas/backoffice/commit/153a61da0fb8f95d4105cccf17e72def4d5ba648))
* **accommodation:** show hour field in table ([fc3ca71](https://gitlab.com/fi-sas/backoffice/commit/fc3ca71ffcbd060fbaf5ad90e52623c362ea6a3f))

## [1.175.1](https://gitlab.com/fi-sas/backoffice/compare/v1.175.0...v1.175.1) (2022-05-25)


### Bug Fixes

* **accommodation:** out of date field on edit ([0eacb8e](https://gitlab.com/fi-sas/backoffice/commit/0eacb8ed0aaf7fb0d9e7fd85088be1d16d1a0317))
* **u-bike:** config forms values mal formed ([667b5db](https://gitlab.com/fi-sas/backoffice/commit/667b5db8ea81f23f39c88837f4aa47459558ca1d))

# [1.175.0](https://gitlab.com/fi-sas/backoffice/compare/v1.174.0...v1.175.0) (2022-05-18)


### Features

* **ubike:** add preferred local label to modal ([5cdd685](https://gitlab.com/fi-sas/backoffice/commit/5cdd6856ef725ca73a0b9dd76fe34dc6ae8ca279))
* **ubike:** add preferred pickup location ([842b3fb](https://gitlab.com/fi-sas/backoffice/commit/842b3fbc147addc6de045cd7f7664e2e058d9c38))

# [1.174.0](https://gitlab.com/fi-sas/backoffice/compare/v1.173.1...v1.174.0) (2022-05-18)


### Features

* **ubike:** kms traveled report ([60a966f](https://gitlab.com/fi-sas/backoffice/commit/60a966f1ccead5417c8ef36eeda14e68102b9fad))
* **ubike:** layout reports box ([48d7868](https://gitlab.com/fi-sas/backoffice/commit/48d786864d90087ae4e92b1afa7a81a81637bae5))

## [1.173.1](https://gitlab.com/fi-sas/backoffice/compare/v1.173.0...v1.173.1) (2022-05-17)


### Bug Fixes

* **mobility:** routes form ([b7abaa5](https://gitlab.com/fi-sas/backoffice/commit/b7abaa59b47191d6087be2a8c51d48ef390b91cd))

# [1.173.0](https://gitlab.com/fi-sas/backoffice/compare/v1.172.1...v1.173.0) (2022-05-17)


### Features

* **ubike:** added serial number label to dropdown ([f4f56d9](https://gitlab.com/fi-sas/backoffice/commit/f4f56d98c31835dc9ffdaf73d590d674cc07e269))
* **ubike:** applications report ([bcef5ac](https://gitlab.com/fi-sas/backoffice/commit/bcef5ac4ffe16c1930bd0c6b4b0f18ce66325b52))

## [1.172.1](https://gitlab.com/fi-sas/backoffice/compare/v1.172.0...v1.172.1) (2022-05-16)


### Bug Fixes

* **infrastructure:** label correction ([85b160a](https://gitlab.com/fi-sas/backoffice/commit/85b160add443fc36c11e01591f6cf66bb109aad8))

# [1.172.0](https://gitlab.com/fi-sas/backoffice/compare/v1.171.0...v1.172.0) (2022-05-16)


### Features

* **accommodation:** add loading action ([c4d4aa6](https://gitlab.com/fi-sas/backoffice/commit/c4d4aa6f1c9acae63b80165ebcfcbd0207b03b35))
* **accommodation:** renew application process ([61503e1](https://gitlab.com/fi-sas/backoffice/commit/61503e180b8a52306019898c71d86cb54ad47544))

# [1.171.0](https://gitlab.com/fi-sas/backoffice/compare/v1.170.0...v1.171.0) (2022-05-12)


### Bug Fixes

* **scholarship:** fix redirection ([530dff7](https://gitlab.com/fi-sas/backoffice/commit/530dff71bcb487ad81347d235ba9f53e879fe00a))


### Features

* **volunteering:** filter by experience ([85c0ec2](https://gitlab.com/fi-sas/backoffice/commit/85c0ec2f5c9d287218eaf79fe3d694aa97a7888d))

# [1.170.0](https://gitlab.com/fi-sas/backoffice/compare/v1.169.0...v1.170.0) (2022-05-12)


### Features

* **accommodation:** alert for selecting all records ([8e40f7e](https://gitlab.com/fi-sas/backoffice/commit/8e40f7e747aa537371f19610567ac5dba06b9d54))
* **accommodation:** get all billing ids ([5b0e046](https://gitlab.com/fi-sas/backoffice/commit/5b0e04638d5354654754f8d126e4f4b00d756123))

# [1.169.0](https://gitlab.com/fi-sas/backoffice/compare/v1.168.0...v1.169.0) (2022-05-10)


### Features

* **accommodation:** add new option to residences dropdown ([a4102a6](https://gitlab.com/fi-sas/backoffice/commit/a4102a6f3fb28765add3bd0bb8a88465524c1515))
* **accommodation:** make academic year mandatory in tariff report ([10f9699](https://gitlab.com/fi-sas/backoffice/commit/10f9699ab2c466f6c47b162e6740b69fb1d1308f))

# [1.168.0](https://gitlab.com/fi-sas/backoffice/compare/v1.167.0...v1.168.0) (2022-05-06)


### Features

* **alimentation:** show configuration updated info ([647cda7](https://gitlab.com/fi-sas/backoffice/commit/647cda762309aeced3dcc3689c4b70c080ab9093))

# [1.167.0](https://gitlab.com/fi-sas/backoffice/compare/v1.166.0...v1.167.0) (2022-05-06)


### Bug Fixes

* **volunteering:** fix all dispatch actions css and html ([a561ebb](https://gitlab.com/fi-sas/backoffice/commit/a561ebbde0d2b1b966dc61f3debfa55e4f8e2be9))


### Features

* **scholarship:** css and html dispatch action ([6ab0b90](https://gitlab.com/fi-sas/backoffice/commit/6ab0b9075f7e587977c4c930521b945b64ad227e))
* **scholarsship:** improve status modal actions ([0c40491](https://gitlab.com/fi-sas/backoffice/commit/0c40491c61553c3cf870ea2267eafce63178eeda))
* **volunteering:** add dispatch action to modal change status ([885fd54](https://gitlab.com/fi-sas/backoffice/commit/885fd547ca35b8072ab761709384c3c622b10767))
* **volunteering:** improve status modal actions ([9058a1c](https://gitlab.com/fi-sas/backoffice/commit/9058a1cf3523bafe6d6065460e57095d7ca33207))

# [1.166.0](https://gitlab.com/fi-sas/backoffice/compare/v1.165.0...v1.166.0) (2022-05-04)


### Features

* **food:** add alergen configuration ([cf36558](https://gitlab.com/fi-sas/backoffice/commit/cf3655868ba1671cf2e086ca372ccb89cdafd94b))
* **ubike:** show active serial number ([669a0f0](https://gitlab.com/fi-sas/backoffice/commit/669a0f0441fcd4f449d1e302b3f319c3c2298cac))

# [1.165.0](https://gitlab.com/fi-sas/backoffice/compare/v1.164.0...v1.165.0) (2022-05-04)


### Features

* **scholarship:** make at least one field mandatory ([8662e90](https://gitlab.com/fi-sas/backoffice/commit/8662e9062af9a6b9d5284007bb19d91d94d05604))

# [1.164.0](https://gitlab.com/fi-sas/backoffice/compare/v1.163.0...v1.164.0) (2022-05-04)


### Bug Fixes

* **volunteering:** fix dispatch modal and actions ([d4b7280](https://gitlab.com/fi-sas/backoffice/commit/d4b7280d243f45c0d3548298903af1e1072078a5))
* **volunteering:** td table colspan fix ([0472a13](https://gitlab.com/fi-sas/backoffice/commit/0472a13b68e9934533c39c4aaa497d8ac87f2ae0))


### Features

* **alimentation:** add new dish type  excluded user profile fields ([e37f54d](https://gitlab.com/fi-sas/backoffice/commit/e37f54df0387a7f26b0b1a5d6c5813d15f2715ff))
* **emergency-fund:** initial module ([cc1da7b](https://gitlab.com/fi-sas/backoffice/commit/cc1da7b59e69654791afeef9a9041996f7fa6d1a))
* **scholarship:** block action buttons by status ([f734208](https://gitlab.com/fi-sas/backoffice/commit/f73420843b9330db9ca4e117407d153616b85861))
* **volunteering:** block actions buttons ([76e6384](https://gitlab.com/fi-sas/backoffice/commit/76e63841c8f1843d4426f1779c7948d3534360d3))
* **volunteering:** fix and improve layout ([1fc8629](https://gitlab.com/fi-sas/backoffice/commit/1fc86298245461b2c1b13c8f524cb4cdb38d2e81))
* **volunteering:** show number of users_colaboration ([5479e57](https://gitlab.com/fi-sas/backoffice/commit/5479e57a666938583110bb3324c8c94cf39f9f81))
* **volunteering:** show student number ([a034fa2](https://gitlab.com/fi-sas/backoffice/commit/a034fa2bc66112104bf0fed0351f69a08dbc8965))

# [1.163.0](https://gitlab.com/fi-sas/backoffice/compare/v1.162.0...v1.163.0) (2022-05-03)


### Bug Fixes

* **scholarship:** added right margin to fields ([86f7e5e](https://gitlab.com/fi-sas/backoffice/commit/86f7e5ec8bddaca7ef318b06d4b72ed1e31613ad))
* **scholarship:** fix layout application perfil tab ([c27c0af](https://gitlab.com/fi-sas/backoffice/commit/c27c0af092199347b13bb497f5848357b5fc6958))
* **scholarship:** fix pipeline issue ([609795e](https://gitlab.com/fi-sas/backoffice/commit/609795eb9c54ac2671d1e353565351ea03c0d931))
* **scholarship:** fix pipeline issue ([4f9239f](https://gitlab.com/fi-sas/backoffice/commit/4f9239fbee3f834bdf42c0f8959c9e8bff853381))
* **scholarship:** fix state machine and interest tab in experiences ([0501e6e](https://gitlab.com/fi-sas/backoffice/commit/0501e6e56037639d278f8a4d6d0c9812cc61a7c8))
* **scholarship:** fix status machine options ([be2e94c](https://gitlab.com/fi-sas/backoffice/commit/be2e94c25f9d6f6cede7da1b6d8448853367832c))
* **scholarship:** function call corrected ([77efeca](https://gitlab.com/fi-sas/backoffice/commit/77efeca989769dba4953f4858b4990d5c8486e44))
* **scholarship:** remove edit button and emit event for refresh ([cbe2958](https://gitlab.com/fi-sas/backoffice/commit/cbe2958e85daa4cd9a30a8a1700f2c67cbb39ce0))
* **scholarship:** status machine actions modal ([404537d](https://gitlab.com/fi-sas/backoffice/commit/404537d795c8fbccb6a35bfef3fde3f796724deb))


### Features

* **scholarship:** fix number users active in experience, length error ([beb79c4](https://gitlab.com/fi-sas/backoffice/commit/beb79c4f4dd07c2e84c9e422e9f029b2cc515561))
* **scholarship:** show student number in tabs ([645f921](https://gitlab.com/fi-sas/backoffice/commit/645f9215efe85cfbf483158bc2e25134249b7efd))

# [1.162.0](https://gitlab.com/fi-sas/backoffice/compare/v1.161.0...v1.162.0) (2022-04-29)


### Features

* **packs:** add new user defaults list and form ([8dc471e](https://gitlab.com/fi-sas/backoffice/commit/8dc471edb93267a4eaaef78e26069a1154ff26a8))

# [1.161.0](https://gitlab.com/fi-sas/backoffice/compare/v1.160.0...v1.161.0) (2022-04-28)


### Features

* **system:** add gateway page to system module ([beef4a6](https://gitlab.com/fi-sas/backoffice/commit/beef4a6a453368dc37e6877ec644931f4593d5ad))

# [1.160.0](https://gitlab.com/fi-sas/backoffice/compare/v1.159.1...v1.160.0) (2022-04-28)


### Bug Fixes

* **infrastructure:** fix edit button ([14db40c](https://gitlab.com/fi-sas/backoffice/commit/14db40c9e4e34809153290a1fbe519f2ac9f23d4))


### Features

* **scholarship:** modal for changing experience closing date ([1e23a24](https://gitlab.com/fi-sas/backoffice/commit/1e23a247a8a544b26f528c333537de9949d16c73))

## [1.159.1](https://gitlab.com/fi-sas/backoffice/compare/v1.159.0...v1.159.1) (2022-04-26)


### Bug Fixes

* **volunteering:** minor fix list ([0175c87](https://gitlab.com/fi-sas/backoffice/commit/0175c870b8cee5c358dfe058c351477070ea5671))

# [1.159.0](https://gitlab.com/fi-sas/backoffice/compare/v1.158.4...v1.159.0) (2022-04-26)


### Features

* **volunteering:** hide experiences and applications complains ([3c2ab1f](https://gitlab.com/fi-sas/backoffice/commit/3c2ab1f37cd9b333cc87a513c0188cb10519bf7a))
* **volunteering:** tooltip added and block download button when no file ([86d4019](https://gitlab.com/fi-sas/backoffice/commit/86d40191c2ca4d0a651d9bbde498b344b38b2107))

## [1.158.4](https://gitlab.com/fi-sas/backoffice/compare/v1.158.3...v1.158.4) (2022-04-22)


### Bug Fixes

* **communication:** minor fixs ([446cb37](https://gitlab.com/fi-sas/backoffice/commit/446cb379491416ea2397c35e83a5060219571d3c))

## [1.158.3](https://gitlab.com/fi-sas/backoffice/compare/v1.158.2...v1.158.3) (2022-04-21)


### Bug Fixes

* **accommodation:** label in aplication details ([cf8d96f](https://gitlab.com/fi-sas/backoffice/commit/cf8d96f8892947407406cc001775ae8228f5e183))

## [1.158.2](https://gitlab.com/fi-sas/backoffice/compare/v1.158.1...v1.158.2) (2022-04-20)


### Bug Fixes

* **social-support:** minor fixs ([030688e](https://gitlab.com/fi-sas/backoffice/commit/030688e3436af2a10656978adb9d8b14c43d1bb4))

## [1.158.1](https://gitlab.com/fi-sas/backoffice/compare/v1.158.0...v1.158.1) (2022-04-20)


### Bug Fixes

* **accommodation:** change options of billings items ([2683163](https://gitlab.com/fi-sas/backoffice/commit/2683163e03a856fe8bd6716d32c97e794b2c64ba))

# [1.158.0](https://gitlab.com/fi-sas/backoffice/compare/v1.157.2...v1.158.0) (2022-04-13)


### Bug Fixes

* **scholarship:** modal dispatch ([befeaea](https://gitlab.com/fi-sas/backoffice/commit/befeaea37d0f9f0fddf761129e5ee02ec921ad33))
* **volunteering:** show observations and details fix ([29ed602](https://gitlab.com/fi-sas/backoffice/commit/29ed6025a2b687e05add568ee2ba4a1900bf3f21))
* **voluntering:** color dispatch box ([746b226](https://gitlab.com/fi-sas/backoffice/commit/746b2262a8cfdf32f2b22cb82fa8ce04adb7d147))


### Features

* **scholarship:** add dispatch box to pannel) ([98f3f78](https://gitlab.com/fi-sas/backoffice/commit/98f3f78a469363bbb39693d1dc61e197d16c7c82))
* **scholarship:** show observations in history changes ([ff0c446](https://gitlab.com/fi-sas/backoffice/commit/ff0c4461fd35cce45597c8e80aec6de5aef95c65))
* **volunteering:** dispatch box before approved ([a7c6020](https://gitlab.com/fi-sas/backoffice/commit/a7c6020cff5eb38c51fa9820db3a86b59ac45b00))
* **voluntering:** show observations in history changes ([169907d](https://gitlab.com/fi-sas/backoffice/commit/169907d8ea3fbff782e17bd2b02eaccc09f213d6))

## [1.157.2](https://gitlab.com/fi-sas/backoffice/compare/v1.157.1...v1.157.2) (2022-04-13)


### Bug Fixes

* **communication:** post forms replicate submissions ([6d2b263](https://gitlab.com/fi-sas/backoffice/commit/6d2b263b405fc68fe0054b9998156740e86b82d7))
* languages order on several places ([6b64940](https://gitlab.com/fi-sas/backoffice/commit/6b64940ca1ef4b57133b8f9305a83fed1646f2bf))


### Performance Improvements

* **communication:** add first operator to suscriptions ([d751bdf](https://gitlab.com/fi-sas/backoffice/commit/d751bdf9b3a248f3c80c1157d90b6f736c170d80))

## [1.157.1](https://gitlab.com/fi-sas/backoffice/compare/v1.157.0...v1.157.1) (2022-04-12)


### Bug Fixes

* **social-support:** payment-grid filters ([13f937a](https://gitlab.com/fi-sas/backoffice/commit/13f937ac1b6e12d25892c2add9e7fbdb7d1ea848))

# [1.157.0](https://gitlab.com/fi-sas/backoffice/compare/v1.156.0...v1.157.0) (2022-04-12)


### Features

* **alimentation:** add new duplicate options ([d77766d](https://gitlab.com/fi-sas/backoffice/commit/d77766d79f199bb0f6f2531d0343c77333bfef60))

# [1.156.0](https://gitlab.com/fi-sas/backoffice/compare/v1.155.0...v1.156.0) (2022-04-11)


### Bug Fixes

* **ubike:** configurations form ([5315c5f](https://gitlab.com/fi-sas/backoffice/commit/5315c5f5127a15d9d5773b3b86352433c16f3203))
* **ubike:** modal change bike ([bcfa3b9](https://gitlab.com/fi-sas/backoffice/commit/bcfa3b9f41bfed3d91e77825b8484b115b87cc53))
* **ubike:** removing commented code ([cdc20c8](https://gitlab.com/fi-sas/backoffice/commit/cdc20c8ab73b7396dcec912a3b9aa1aeca10c2db))
* **ubike:** show active tipologies in form ([cfa665c](https://gitlab.com/fi-sas/backoffice/commit/cfa665c80e27f809acd99431496ec9c49aa1cdbe))
* **ubike:** show active typologies ([65280f3](https://gitlab.com/fi-sas/backoffice/commit/65280f3b1d1519bb65f81149da4fe9ccdbafa769))
* **ubike:** ubike configurations ([b1aaa07](https://gitlab.com/fi-sas/backoffice/commit/b1aaa077a4333973d3f27846d610af39b10fdd0c))
* **ubike:** using resolver for dates ([a13846f](https://gitlab.com/fi-sas/backoffice/commit/a13846f12cd3c21dabf8a321ce30867b0b8eace9))


### Features

* **ubike:** change configs request ([8229d29](https://gitlab.com/fi-sas/backoffice/commit/8229d29b68cf974abfb812da7e3e91c81d403862))
* **ubike:** changes in modal ([269141e](https://gitlab.com/fi-sas/backoffice/commit/269141e29f8ba4676ce4508929913627dda58853))
* **ubike:** refator configurations request ([fe23abd](https://gitlab.com/fi-sas/backoffice/commit/fe23abd9c51fb601bce0fe4716fbfc5dc0fe9b00))

# [1.155.0](https://gitlab.com/fi-sas/backoffice/compare/v1.154.0...v1.155.0) (2022-04-06)


### Bug Fixes

* **accommodation:** listing users residence configurations ([03cc57a](https://gitlab.com/fi-sas/backoffice/commit/03cc57aea669bf6f4fc4b7d6ac8cfe123c4f512c))


### Features

* **accommodation:** new tariffs map report ([f495688](https://gitlab.com/fi-sas/backoffice/commit/f495688a5ebf87341cba917027a2f758c54a8da4))
* **login:** add remaining login attemps message ([2471d28](https://gitlab.com/fi-sas/backoffice/commit/2471d28a58cdab2e148c7624921850391c95ad84))
* **volunteering:** add dispatch status to stats ([f136de9](https://gitlab.com/fi-sas/backoffice/commit/f136de908e175010d6f5eec875f48cdb4c535cbb))

# [1.154.0](https://gitlab.com/fi-sas/backoffice/compare/v1.153.1...v1.154.0) (2022-04-01)


### Bug Fixes

* **voluntering:** fix routering and objects for voluntering ([4981376](https://gitlab.com/fi-sas/backoffice/commit/498137683291aeb83e194471505480d4fb154789))


### Features

* **social scholarship:** change icon ([b420511](https://gitlab.com/fi-sas/backoffice/commit/b4205110e329de07e990c15d6d3b2f7cac015be4))
* **social scholarship:** redirect to painel ([8f44443](https://gitlab.com/fi-sas/backoffice/commit/8f44443fb4b46446c1b63002194adc2e3090e691))
* **socialscholarship:** remove required signal from close applications ([d5cdcfb](https://gitlab.com/fi-sas/backoffice/commit/d5cdcfb66fb7624bf14d58933cb58fa7b45e0596))

## [1.153.1](https://gitlab.com/fi-sas/backoffice/compare/v1.153.0...v1.153.1) (2022-03-31)


### Bug Fixes

* production build error ([17e3b53](https://gitlab.com/fi-sas/backoffice/commit/17e3b53c8a970a88f622915fbc95ccad261f8eee))

# [1.153.0](https://gitlab.com/fi-sas/backoffice/compare/v1.152.0...v1.153.0) (2022-03-29)


### Bug Fixes

* **accommodation:** add new field processed_manually ([b85e2ce](https://gitlab.com/fi-sas/backoffice/commit/b85e2ce2870c3000f9ab49f0bfee6aeb838cfafb))
* **accommodation:** listing billings order by start_date ([adb7e0f](https://gitlab.com/fi-sas/backoffice/commit/adb7e0f7609cb75d3fd7cc31cb64c812209ee07f))


### Features

* **accommodation:** fix discount fields ([c8ff2b2](https://gitlab.com/fi-sas/backoffice/commit/c8ff2b25eb602bc0599084159b6197cbc20d00b5))

# [1.152.0](https://gitlab.com/fi-sas/backoffice/compare/v1.151.0...v1.152.0) (2022-03-29)


### Features

* **system:** add new system module ([ec29430](https://gitlab.com/fi-sas/backoffice/commit/ec29430043f919dc6518613a61aa9ff599a9ef72))

# [1.151.0](https://gitlab.com/fi-sas/backoffice/compare/v1.150.7...v1.151.0) (2022-03-28)


### Bug Fixes

* **accommodation:** removing guard from residences routing ([780d692](https://gitlab.com/fi-sas/backoffice/commit/780d69268dab96c8d4d74dfc199f0c5fbb0f7ce0))
* **alimentation:** minor fixs ([761ea17](https://gitlab.com/fi-sas/backoffice/commit/761ea175be80faf6d6425c43c95c791060b9db78))
* **current-account:** minor fixs ([f5a0525](https://gitlab.com/fi-sas/backoffice/commit/f5a05259d4114ba553d9eebfef091274e91ae00e))
* **fianacial:** minor fix on validations ([a714427](https://gitlab.com/fi-sas/backoffice/commit/a7144278e01e98a18bf37c0d3f210905da6f6242))
* **financial:** minor fixs ([6bc336d](https://gitlab.com/fi-sas/backoffice/commit/6bc336d0a580675026cd3765f73c2cee14fff470))
* **food:** fix style of reports page ([3dad585](https://gitlab.com/fi-sas/backoffice/commit/3dad585755334f15d64220f7f7b0c7e2a8089fe6))
* **food:** independent variables for dates and services in reports ([cfb7ea4](https://gitlab.com/fi-sas/backoffice/commit/cfb7ea4d86e730c85538e78a4909327eceaabbc9))


### Features

* **accommodation:** allow changing accommodation period ([db3eb1f](https://gitlab.com/fi-sas/backoffice/commit/db3eb1fab6b3d2747fe3d23102f462a203499f74))
* **accommodation:** guard for residences and contract changes ([dd91885](https://gitlab.com/fi-sas/backoffice/commit/dd91885b4c75d442a209af99d5d325d447450c14))

## [1.150.7](https://gitlab.com/fi-sas/backoffice/compare/v1.150.6...v1.150.7) (2022-03-18)


### Bug Fixes

* **accommodation:** regime change relatedd error ([5e14408](https://gitlab.com/fi-sas/backoffice/commit/5e14408c28052b20146e49be86ffe4b10aba6aad))

## [1.150.6](https://gitlab.com/fi-sas/backoffice/compare/v1.150.5...v1.150.6) (2022-03-16)


### Bug Fixes

* minor fix user pack view ([ab80535](https://gitlab.com/fi-sas/backoffice/commit/ab8053582ccda5e576a729756d56368ee3c7128d))

## [1.150.5](https://gitlab.com/fi-sas/backoffice/compare/v1.150.4...v1.150.5) (2022-03-14)


### Bug Fixes

* **social scholarchip:** fix iban percentage ([7b4fc3f](https://gitlab.com/fi-sas/backoffice/commit/7b4fc3f53069c67f799d732807dd532aac24ef08))

## [1.150.4](https://gitlab.com/fi-sas/backoffice/compare/v1.150.3...v1.150.4) (2022-03-11)


### Bug Fixes

* **social-support:** absence reject form add fields ([39a0748](https://gitlab.com/fi-sas/backoffice/commit/39a074824f2fdc7544f631b83b218b452cc5f7ab))

## [1.150.3](https://gitlab.com/fi-sas/backoffice/compare/v1.150.2...v1.150.3) (2022-03-11)


### Bug Fixes

* **alimentation:** remove produtcs from required on family form ([964e511](https://gitlab.com/fi-sas/backoffice/commit/964e51101f67fda6ffdbadb94d828f1765e15d90))

## [1.150.2](https://gitlab.com/fi-sas/backoffice/compare/v1.150.1...v1.150.2) (2022-03-10)


### Bug Fixes

* **food:** remove families from required ([e9936eb](https://gitlab.com/fi-sas/backoffice/commit/e9936ebb514b6d2eaa421ed619bc7e4ccd5029f0))

## [1.150.1](https://gitlab.com/fi-sas/backoffice/compare/v1.150.0...v1.150.1) (2022-03-07)


### Bug Fixes

* **packs:** reduce payload of user_meals request ([531587a](https://gitlab.com/fi-sas/backoffice/commit/531587a92e97149bf4571ee43448b2495361d535))

# [1.150.0](https://gitlab.com/fi-sas/backoffice/compare/v1.149.7...v1.150.0) (2022-03-03)


### Bug Fixes

* **accommodation:** add current regime field to applications ([e86dd3c](https://gitlab.com/fi-sas/backoffice/commit/e86dd3c6e5be618929c8b7122dd6027bbb56c5d9))
* **accommodation:** fix update translations service ([d1e23a1](https://gitlab.com/fi-sas/backoffice/commit/d1e23a1fd60f89843da9bd187c48e5d4d55f7334))
* **accommodation:** permute button loading ([0325a70](https://gitlab.com/fi-sas/backoffice/commit/0325a70df6f9d8498c396691d68bbe0cd776615b))
* **accommodation:** show available rooms ([176d894](https://gitlab.com/fi-sas/backoffice/commit/176d89492a46a36bf88f9d9dc5b1a998178a66d3))
* **current account:** show available cash account ([a25b1ee](https://gitlab.com/fi-sas/backoffice/commit/a25b1ee157c25b2373547f25c37d2ba3d842e7b7))
* **private accommodation:** fix complaint module routing ([70bc2df](https://gitlab.com/fi-sas/backoffice/commit/70bc2dfe6ca0ccbc8a9df364a0ea07bfa9e2d34b))


### Features

* **accommodation:** discount extra, regime, and typology ([d83689f](https://gitlab.com/fi-sas/backoffice/commit/d83689f738aaec28dd048c4d95ecb0436ee40da2))
* **accommodation:** discount fields regime, typology and extra ([ff797b4](https://gitlab.com/fi-sas/backoffice/commit/ff797b45dcfcd2bcde689299bfef5ab6825e6f63))
* **accommodation:** filter tariffs and periods ([0780494](https://gitlab.com/fi-sas/backoffice/commit/0780494bfa75a63e4d769decad13a076f7c3dce7))
* **monitoring:** show user email in equipments ([a6c4b72](https://gitlab.com/fi-sas/backoffice/commit/a6c4b7202bc2a41039f01e8ac1355c0a96256d18))

## [1.149.7](https://gitlab.com/fi-sas/backoffice/compare/v1.149.6...v1.149.7) (2022-03-03)


### Bug Fixes

* **social-support:** configurations allow empty fields ([eb815fe](https://gitlab.com/fi-sas/backoffice/commit/eb815fea85e773a50850c19d198d870e00f3fe74))

## [1.149.6](https://gitlab.com/fi-sas/backoffice/compare/v1.149.5...v1.149.6) (2022-02-28)


### Bug Fixes

* **social-support:** phone  external users form ([eff923e](https://gitlab.com/fi-sas/backoffice/commit/eff923ea4058cf78f09e56d29a242127cbbc213b))

## [1.149.5](https://gitlab.com/fi-sas/backoffice/compare/v1.149.4...v1.149.5) (2022-02-28)


### Bug Fixes

* **auth:** calculate expire_in date on login ([8224344](https://gitlab.com/fi-sas/backoffice/commit/822434489bb808850d4aa3f444d1f22afd783948))

## [1.149.4](https://gitlab.com/fi-sas/backoffice/compare/v1.149.3...v1.149.4) (2022-02-23)


### Bug Fixes

* **current-account:** add invoices to sales reports  filters ([9f4ceb7](https://gitlab.com/fi-sas/backoffice/commit/9f4ceb7205b042afcd8cca5776d6f7deb5218068))

## [1.149.3](https://gitlab.com/fi-sas/backoffice/compare/v1.149.2...v1.149.3) (2022-02-18)


### Bug Fixes

* **accommodation:** users load ([4dfb397](https://gitlab.com/fi-sas/backoffice/commit/4dfb397e634b327ca9ee225994847fb2075d6f14))
* add fixs to social support and volunteering ([416ed5f](https://gitlab.com/fi-sas/backoffice/commit/416ed5f8853c960df529ae6e47d2828d3e4466bc))
* **health:** filters and selects ([047dd9d](https://gitlab.com/fi-sas/backoffice/commit/047dd9defe84ce83155aa2df7a56da861dd64fca))
* **scholarship:** configurations page ([ffffc0c](https://gitlab.com/fi-sas/backoffice/commit/ffffc0cc9ce8207a6cf9348199171cb0a571d5f9))
* **social-support:** change application form ([d642f1b](https://gitlab.com/fi-sas/backoffice/commit/d642f1b0031d72c4cd24a8bc272d945982c3f6e5))

## [1.149.2](https://gitlab.com/fi-sas/backoffice/compare/v1.149.1...v1.149.2) (2022-02-11)


### Bug Fixes

* **volunteering:** remove change status modal on dispatch ([5a72ed2](https://gitlab.com/fi-sas/backoffice/commit/5a72ed271cdbb2c671f359ec3b10cc2c2fee1f1b))

## [1.149.1](https://gitlab.com/fi-sas/backoffice/compare/v1.149.0...v1.149.1) (2022-02-11)


### Bug Fixes

* **volunteering:** change dispatch endpoint ([a634c06](https://gitlab.com/fi-sas/backoffice/commit/a634c064e6cd336550465bdcef5462d60d3faea4))

# [1.149.0](https://gitlab.com/fi-sas/backoffice/compare/v1.148.0...v1.149.0) (2022-02-10)


### Features

* **food:** add families field to products form ([a213036](https://gitlab.com/fi-sas/backoffice/commit/a2130367be05cc51f1d45ddd4c881090de458814))

# [1.148.0](https://gitlab.com/fi-sas/backoffice/compare/v1.147.0...v1.148.0) (2022-02-10)


### Bug Fixes

* **current-account:** load paymentMethod ([60ff10c](https://gitlab.com/fi-sas/backoffice/commit/60ff10c88ff63d0e87908d5efc1309368e5b268c))


### Features

* **accommodation:** remove limit from observations text area ([0905149](https://gitlab.com/fi-sas/backoffice/commit/09051492395ffb2d0eeee6e7e62477ee4d680cfa))
* **current-acccount:** add new detailed charges report ([e057a1e](https://gitlab.com/fi-sas/backoffice/commit/e057a1ee830f6e05bff982afa7d9f2b8d4591e1f))
* **current-account:** add new reports fiters ([a2db059](https://gitlab.com/fi-sas/backoffice/commit/a2db059517ed1676523d522c6e10f542957c0567))
* **reports:** add new columns to detail sales reports ([d2aef06](https://gitlab.com/fi-sas/backoffice/commit/d2aef061b05cc2848bed61c1107caa0c22dd9cb6))

# [1.147.0](https://gitlab.com/fi-sas/backoffice/compare/v1.146.1...v1.147.0) (2022-02-07)


### Bug Fixes

* **health:** associating internal doctor ([4591a01](https://gitlab.com/fi-sas/backoffice/commit/4591a01e22c44f003bb6d6efa769018066f55dfe))
* **health:** fix module configuration problem ([79b0547](https://gitlab.com/fi-sas/backoffice/commit/79b0547f982059b8ae5040a169e58b462499055f))


### Features

* **accommodation:** show only dispatch counts ([2e19b00](https://gitlab.com/fi-sas/backoffice/commit/2e19b00022514af6d109255d26fd7161e9fe1e0f))

## [1.146.1](https://gitlab.com/fi-sas/backoffice/compare/v1.146.0...v1.146.1) (2022-02-03)


### Bug Fixes

* **target-posts:** get users on target groups update ([14db0da](https://gitlab.com/fi-sas/backoffice/commit/14db0da7b23bf244e16d1195e1c8a0af1fa0f92f))

# [1.146.0](https://gitlab.com/fi-sas/backoffice/compare/v1.145.0...v1.146.0) (2022-02-02)


### Bug Fixes

* **alimentation:** change the reports pages of orders and reserves ([17d8e80](https://gitlab.com/fi-sas/backoffice/commit/17d8e80e6a68bb05d1dff90f81c919e685db73e2))


### Features

* **token:** new token refresh system ([fb0c4e3](https://gitlab.com/fi-sas/backoffice/commit/fb0c4e39688fcf3c1b4ddb1819cfc30dd439b6e6))
* **update:** change new update system ([033ccab](https://gitlab.com/fi-sas/backoffice/commit/033ccab879729952261c6efdb6c700a77a1d9c52))

# [1.145.0](https://gitlab.com/fi-sas/backoffice/compare/v1.144.0...v1.145.0) (2022-02-01)


### Features

* **alimentation:** add new reports ([99778d6](https://gitlab.com/fi-sas/backoffice/commit/99778d6063510b0c162d18a89c4ce6a05dc8d10f))

# [1.144.0](https://gitlab.com/fi-sas/backoffice/compare/v1.143.1...v1.144.0) (2022-02-01)


### Bug Fixes

* **accommodation:** variable name ([bbf84dd](https://gitlab.com/fi-sas/backoffice/commit/bbf84ddf80b4bcf9bd51b3b22e68fa325b70ba1d))
* **modules:** export configuration modules names ([6292795](https://gitlab.com/fi-sas/backoffice/commit/6292795f10a2ae14dc977354b4e7766201791429))


### Features

* **accommodation:** label rename ([74c92c9](https://gitlab.com/fi-sas/backoffice/commit/74c92c9a8605203843d18be0d828ceec6ecc6e87))
* **accommodation:** show painel without any residence selected ([e647951](https://gitlab.com/fi-sas/backoffice/commit/e6479517ea3777bbeb725d23ce8c35b594ea183a))
* **accommodation:** show typologies without a selected residence ([84f3bba](https://gitlab.com/fi-sas/backoffice/commit/84f3bbad192c200ec8f6f2e33783dc938e1543eb))
* **accommodation:** withdrawal modal ([5fe669d](https://gitlab.com/fi-sas/backoffice/commit/5fe669dc3fc94994208369b3ff49685c5c684103))

## [1.143.1](https://gitlab.com/fi-sas/backoffice/compare/v1.143.0...v1.143.1) (2022-01-28)


### Bug Fixes

* **routing:** fix configuration routing ([04addc6](https://gitlab.com/fi-sas/backoffice/commit/04addc636ef9e44fa5f58e5a8d93ae13449ed9e6))

# [1.143.0](https://gitlab.com/fi-sas/backoffice/compare/v1.142.1...v1.143.0) (2022-01-28)


### Features

* **communication:** add users fields to target posts ([268adc0](https://gitlab.com/fi-sas/backoffice/commit/268adc0dff3a2aeacd1e73b7a060dc7773987cfd))

## [1.142.1](https://gitlab.com/fi-sas/backoffice/compare/v1.142.0...v1.142.1) (2022-01-26)


### Bug Fixes

* minor fixs ([4a3bb6b](https://gitlab.com/fi-sas/backoffice/commit/4a3bb6bd68e1c0c0ad521a668b0c0d78ecd0f44a))

# [1.142.0](https://gitlab.com/fi-sas/backoffice/compare/v1.141.0...v1.142.0) (2022-01-26)


### Features

* **cc:** add sales report ([614c322](https://gitlab.com/fi-sas/backoffice/commit/614c322c59bdcbff2dfcafaff3c7604df8614428))

# [1.141.0](https://gitlab.com/fi-sas/backoffice/compare/v1.140.1...v1.141.0) (2022-01-25)


### Bug Fixes

* **bolsa:** removing unused functions ([6a4abcc](https://gitlab.com/fi-sas/backoffice/commit/6a4abcc63fb6289c217c719e62f6d69f72d27481))
* **scholarship:** fixing variable issue ([d649bf3](https://gitlab.com/fi-sas/backoffice/commit/d649bf3e560c67b82fe1306359e26ea4bea49448))
* **scholarship:** variable type ([01ec603](https://gitlab.com/fi-sas/backoffice/commit/01ec603520202cfe573e250e35144e8eec97d845))


### Features

* **bolsa:** configurations improvment ([671a132](https://gitlab.com/fi-sas/backoffice/commit/671a1328c15feee32ccac3ac31cc612cfd72d68d))
* **bolsa:** configurations improvment, missing commit ([4557a44](https://gitlab.com/fi-sas/backoffice/commit/4557a443e14806078e1c2848b473a9f64afdfa30))
* **health:** configurations page ([5df1ef8](https://gitlab.com/fi-sas/backoffice/commit/5df1ef8c90eeca280710b3f93a66dc9c2ce933dd))
* **scholarship:** forms issue solving ([d95d8fb](https://gitlab.com/fi-sas/backoffice/commit/d95d8fb7781083229a6ffebd15445ba6dcce62d0))

## [1.140.1](https://gitlab.com/fi-sas/backoffice/compare/v1.140.0...v1.140.1) (2022-01-24)


### Bug Fixes

* **accommodation:** change start_date to expected end_date ([e3d323a](https://gitlab.com/fi-sas/backoffice/commit/e3d323a74aaba5982434068b53c1e7f21cc999ed))

# [1.140.0](https://gitlab.com/fi-sas/backoffice/compare/v1.139.0...v1.140.0) (2022-01-18)


### Bug Fixes

* **health:** removing font family from buy modal css ([6084b5c](https://gitlab.com/fi-sas/backoffice/commit/6084b5c3a75a5792891902a7939736f68a2a6cc5))


### Features

* **health:** buy modal box improvement ([3899293](https://gitlab.com/fi-sas/backoffice/commit/3899293ceb10bfd6a29a185c91493ec2c9bbd91e))
* **queue:** users notification action ([78c589c](https://gitlab.com/fi-sas/backoffice/commit/78c589c9973143f3c686a470ad45cace09e64461))

# [1.139.0](https://gitlab.com/fi-sas/backoffice/compare/v1.138.0...v1.139.0) (2022-01-14)


### Bug Fixes

* **health:** fix css modal and generate calendar request ([f69c47b](https://gitlab.com/fi-sas/backoffice/commit/f69c47bdf1b9170763548f2f82ee44cd454516e1))


### Features

* **calendar:** calendar export option ([e90f3c7](https://gitlab.com/fi-sas/backoffice/commit/e90f3c78019570eb626a120b287a224083d59e44))
* **pay:** pay box modal ([62f034e](https://gitlab.com/fi-sas/backoffice/commit/62f034e30c45df6dbffa2026699707d6d2b01223))

# [1.138.0](https://gitlab.com/fi-sas/backoffice/compare/v1.137.1...v1.138.0) (2022-01-14)


### Features

* **city:** select city ([47dc944](https://gitlab.com/fi-sas/backoffice/commit/47dc9448e57c26853d2de862ddb04e503a89fa40))
* **export:** option for exporting list of users enrolled ([98a8870](https://gitlab.com/fi-sas/backoffice/commit/98a88707c01d14be339a8d00143ad99ab53fd3ba))
* **various:** improvements ([557f48e](https://gitlab.com/fi-sas/backoffice/commit/557f48ee5082a6c2f3c1d8aa8b22f8a4dc07c57d))

## [1.137.1](https://gitlab.com/fi-sas/backoffice/compare/v1.137.0...v1.137.1) (2022-01-12)


### Bug Fixes

* **social-support:** rename canceled to cancelled status ([e3e7362](https://gitlab.com/fi-sas/backoffice/commit/e3e7362cb7581e2606c8b27739ca485d157d52c3))
* **volunteering:** remove imports from social support ([6e655fd](https://gitlab.com/fi-sas/backoffice/commit/6e655fd2335b1cfee6f7450b167bdeb33b548f8d))

# [1.137.0](https://gitlab.com/fi-sas/backoffice/compare/v1.136.3...v1.137.0) (2022-01-10)


### Features

* **packs:** update details pages of packs ([c8b2948](https://gitlab.com/fi-sas/backoffice/commit/c8b2948bfef62187556520a0f52740cd8d09606d))

## [1.136.3](https://gitlab.com/fi-sas/backoffice/compare/v1.136.2...v1.136.3) (2022-01-04)


### Bug Fixes

* **files:** add file name to files uploaded ([5713fc9](https://gitlab.com/fi-sas/backoffice/commit/5713fc9b28c64f0a3e1447239b2e48ff847ba770))

## [1.136.2](https://gitlab.com/fi-sas/backoffice/compare/v1.136.1...v1.136.2) (2022-01-03)


### Bug Fixes

* minor fixs ([0c9c60f](https://gitlab.com/fi-sas/backoffice/commit/0c9c60fc545dbc674b591e0991c2d9392a64548f))

## [1.136.1](https://gitlab.com/fi-sas/backoffice/compare/v1.136.0...v1.136.1) (2021-12-23)


### Bug Fixes

* **queue:** delete console log ([8bf5f6f](https://gitlab.com/fi-sas/backoffice/commit/8bf5f6fee059a3f99d1081f2da289f8cc7018a7a))

# [1.136.0](https://gitlab.com/fi-sas/backoffice/compare/v1.135.0...v1.136.0) (2021-12-23)


### Bug Fixes

* **configuration:** add icon form external service ([fc770f3](https://gitlab.com/fi-sas/backoffice/commit/fc770f39576f9aa718bac7a77d4a9857c3d217c2))
* **queue:** add history page ([c4bc6e0](https://gitlab.com/fi-sas/backoffice/commit/c4bc6e0eba58cf65430b8b6d72cc65cea78c5efd))
* **queue:** lazy loading ([c657dd0](https://gitlab.com/fi-sas/backoffice/commit/c657dd07cdd191319ecd9632b10a60ec15a5a64e))
* **queue:** recall ticket ([e2c183e](https://gitlab.com/fi-sas/backoffice/commit/e2c183ebbc08127e283712241c7382d1bef92a89))


### Features

* **queue:** new page history tickets ([24ff216](https://gitlab.com/fi-sas/backoffice/commit/24ff2160a506c0c14f6f2b27f5331b6d0aae1769))

# [1.135.0](https://gitlab.com/fi-sas/backoffice/compare/v1.134.3...v1.135.0) (2021-12-23)


### Bug Fixes

* **health:** fix health build prod ([8651dae](https://gitlab.com/fi-sas/backoffice/commit/8651daecc69ea9b7794bc5f1f28ad036eb703e2c))


### Features

* **core:** check version change on all environments ([24cfccc](https://gitlab.com/fi-sas/backoffice/commit/24cfccc71225e2161b691cca4a4dbf9e796a3e24))

## [1.134.3](https://gitlab.com/fi-sas/backoffice/compare/v1.134.2...v1.134.3) (2021-12-22)


### Bug Fixes

* **configuration:** change text ([7ab14f4](https://gitlab.com/fi-sas/backoffice/commit/7ab14f4f629774ff8a8f66185bf13667314fa1c5))

## [1.134.2](https://gitlab.com/fi-sas/backoffice/compare/v1.134.1...v1.134.2) (2021-12-22)


### Bug Fixes

* **calendar:** correction error validation ([a837f56](https://gitlab.com/fi-sas/backoffice/commit/a837f56286ed18adb175005bc49643071e7a43df))
* **configuration:** add calendar icon ([8e3c56e](https://gitlab.com/fi-sas/backoffice/commit/8e3c56e176f3b6989f2a4ccd4f692027babe6ce2))
* **queue:** add validation user ([b30a92b](https://gitlab.com/fi-sas/backoffice/commit/b30a92b98f5765a786a65a7259d33c422a3aa6c3))

## [1.134.1](https://gitlab.com/fi-sas/backoffice/compare/v1.134.0...v1.134.1) (2021-12-22)


### Bug Fixes

* **cc:** cacheAccountClose missing service ([3f87736](https://gitlab.com/fi-sas/backoffice/commit/3f877366de03063fc1d843113e3e0827f08174dd))

# [1.134.0](https://gitlab.com/fi-sas/backoffice/compare/v1.133.3...v1.134.0) (2021-12-21)


### Features

* **bus:** tickets list ([e906ba2](https://gitlab.com/fi-sas/backoffice/commit/e906ba2d4af353b36bff28fbd70b0596231b8613))

## [1.133.3](https://gitlab.com/fi-sas/backoffice/compare/v1.133.2...v1.133.3) (2021-12-21)


### Bug Fixes

* **core:** add scopes to all dashboard and sidebar items ([73865f5](https://gitlab.com/fi-sas/backoffice/commit/73865f54a5eba1b85a349d0e06c7a79da959c0d6))

## [1.133.2](https://gitlab.com/fi-sas/backoffice/compare/v1.133.1...v1.133.2) (2021-12-20)


### Bug Fixes

* **queue:** add/remove subject ([0b1a70a](https://gitlab.com/fi-sas/backoffice/commit/0b1a70ac746cb49636b117207ff8514241fa649a))

## [1.133.1](https://gitlab.com/fi-sas/backoffice/compare/v1.133.0...v1.133.1) (2021-12-17)


### Bug Fixes

* **events:** list calendar and recurrent ([2047d7c](https://gitlab.com/fi-sas/backoffice/commit/2047d7c86eb752e209dbd2fb39efc92871571629))

# [1.133.0](https://gitlab.com/fi-sas/backoffice/compare/v1.132.14...v1.133.0) (2021-12-16)


### Bug Fixes

* **appointments:** correçoes / melhorias ([873b12d](https://gitlab.com/fi-sas/backoffice/commit/873b12d349d3796762a8f0937d7226922a9809bf))
* **dashboar:** missing commit ([e4cce6c](https://gitlab.com/fi-sas/backoffice/commit/e4cce6ceaf0f84e7de9c5332ba62e5fbfdd5a66b))
* **holiday:** remove require from input ([4555cb3](https://gitlab.com/fi-sas/backoffice/commit/4555cb37b3f73583a252556de39d447e71d40805))
* **select:** allow clear select ([c8863cf](https://gitlab.com/fi-sas/backoffice/commit/c8863cf69839d29ea8094449e4e4f8de0dbd4fcf))


### Features

* **appointment:** appointment step one ([aa718c9](https://gitlab.com/fi-sas/backoffice/commit/aa718c9421162739c36322be26c2287c2616edc4))
* **appointment:** realizar uma marcação ([dce2686](https://gitlab.com/fi-sas/backoffice/commit/dce268644c8faae91338e4a7960517dfa8705f6e))
* **health:** health interface ([41d1e57](https://gitlab.com/fi-sas/backoffice/commit/41d1e57ebb79f533e97867a132dcfb3dfed84f38))
* **holiday:** allow multiple organic units ([fb41119](https://gitlab.com/fi-sas/backoffice/commit/fb41119e89e0412e67e4abb0a3d65d3c24c50e3d))
* **holiday:** holiday interface ([4cfd887](https://gitlab.com/fi-sas/backoffice/commit/4cfd8874c6b0717859ae2d1c8dacb02cf158a966))
* **translation:** added holiday translation ([91c17e4](https://gitlab.com/fi-sas/backoffice/commit/91c17e4a428628e2ddfb29e7ba080c50077fb777))
* **various:** appointment steps ([d19b78c](https://gitlab.com/fi-sas/backoffice/commit/d19b78cf58a4ed0c8a9cb917557549639f5a4d97))
* **various:** historic appointment and complaint ([e1976bb](https://gitlab.com/fi-sas/backoffice/commit/e1976bb22cd60effc9ec5892f57afe3e1e883c99))
* **various:** marcações reclamações e calendario ([45f19d6](https://gitlab.com/fi-sas/backoffice/commit/45f19d6a423741d82aa53c6c5803a68aaa5bb122))

## [1.132.14](https://gitlab.com/fi-sas/backoffice/compare/v1.132.13...v1.132.14) (2021-12-15)


### Bug Fixes

* **events:** view events, list subscriptions ([02a3b57](https://gitlab.com/fi-sas/backoffice/commit/02a3b577ce26e2551270be9458e9a39cadc60410))

## [1.132.13](https://gitlab.com/fi-sas/backoffice/compare/v1.132.12...v1.132.13) (2021-12-15)


### Bug Fixes

* **dashbaord:** color title section ([8bb703e](https://gitlab.com/fi-sas/backoffice/commit/8bb703e1d3125fdf5a65c4e367cc1683089767a5))

## [1.132.12](https://gitlab.com/fi-sas/backoffice/compare/v1.132.11...v1.132.12) (2021-12-15)


### Bug Fixes

* **calendar:** list and status events ([b301be9](https://gitlab.com/fi-sas/backoffice/commit/b301be989ed4fad495be9a919b0777adde0ee22c))

## [1.132.11](https://gitlab.com/fi-sas/backoffice/compare/v1.132.10...v1.132.11) (2021-12-15)


### Bug Fixes

* **bus:** change bus scopes ([c0fe96c](https://gitlab.com/fi-sas/backoffice/commit/c0fe96ced1ab7032473bdba1fd4beb1bb5c5b692))

## [1.132.10](https://gitlab.com/fi-sas/backoffice/compare/v1.132.9...v1.132.10) (2021-12-14)


### Bug Fixes

* **dashboard:** correction error ([1b6d2de](https://gitlab.com/fi-sas/backoffice/commit/1b6d2def1da3ed45fd30d5f1d3e3452a7298dc37))

## [1.132.9](https://gitlab.com/fi-sas/backoffice/compare/v1.132.8...v1.132.9) (2021-12-14)


### Bug Fixes

* **dashboard:** new style ([255c12b](https://gitlab.com/fi-sas/backoffice/commit/255c12b22e3af54424eeab2d86747d03379810fa))

## [1.132.8](https://gitlab.com/fi-sas/backoffice/compare/v1.132.7...v1.132.8) (2021-12-14)


### Bug Fixes

* **configuration:** add sections ([098b29c](https://gitlab.com/fi-sas/backoffice/commit/098b29cac4f1345504ce2f12870db2aa8aad2fb0))
* **configuration:** new fields external services ([8f1d755](https://gitlab.com/fi-sas/backoffice/commit/8f1d755b1d8aae1c4a409fe0e8d594a1871089ab))
* **configuration:** sections in dashboard ([e308cf1](https://gitlab.com/fi-sas/backoffice/commit/e308cf17e4a5fd3b72ac057e7ddbb229b5ddb6d7))

## [1.132.7](https://gitlab.com/fi-sas/backoffice/compare/v1.132.6...v1.132.7) (2021-12-13)


### Bug Fixes

* **calendar:** create events ([c086ff5](https://gitlab.com/fi-sas/backoffice/commit/c086ff526cfddaea095eef876281ee25556b8176))
* **events:** change form to steps ([91d7a3f](https://gitlab.com/fi-sas/backoffice/commit/91d7a3f4b21f8d57b89464763d51455adb642ac2))
* **events:** create event ([5feec78](https://gitlab.com/fi-sas/backoffice/commit/5feec780dfd1abec46f5eb645ace474ca73280e6))

## [1.132.6](https://gitlab.com/fi-sas/backoffice/compare/v1.132.5...v1.132.6) (2021-12-10)


### Bug Fixes

* **private_accommodation:** nzrequired field ([ad2da49](https://gitlab.com/fi-sas/backoffice/commit/ad2da498b3d67c17742f645f3bdd7336c6955bbe))

## [1.132.5](https://gitlab.com/fi-sas/backoffice/compare/v1.132.4...v1.132.5) (2021-12-10)


### Bug Fixes

* **comunnication:** wp and mobile file 16:9 ([3f69e36](https://gitlab.com/fi-sas/backoffice/commit/3f69e3684be256f6f5b1248a795004220c686f5b))

## [1.132.4](https://gitlab.com/fi-sas/backoffice/compare/v1.132.3...v1.132.4) (2021-12-10)


### Bug Fixes

* **social-support:** dispatch decision label and notes field ([46ed265](https://gitlab.com/fi-sas/backoffice/commit/46ed265ec7458d45c369ca2e936846b647afafd1))
* **volunteering:** dispatch decision label and notes field ([8a96980](https://gitlab.com/fi-sas/backoffice/commit/8a96980f27796781dbbf65c4b44d85d511787aa6))

## [1.132.3](https://gitlab.com/fi-sas/backoffice/compare/v1.132.2...v1.132.3) (2021-12-10)


### Bug Fixes

* **social-support:** interview list and register fixes ([e579c64](https://gitlab.com/fi-sas/backoffice/commit/e579c64bf73c98f948a9e231d9d22085995d58cf))

## [1.132.2](https://gitlab.com/fi-sas/backoffice/compare/v1.132.1...v1.132.2) (2021-12-10)


### Bug Fixes

* **private_accom:** changing mandatory fields ([5da3de9](https://gitlab.com/fi-sas/backoffice/commit/5da3de991361a3af22b368240b2625c181ffe6eb))
* **private_accommodation:** redirect map ([586334a](https://gitlab.com/fi-sas/backoffice/commit/586334ae6e16222d8eca038ef8c12b15e9532415))

## [1.132.1](https://gitlab.com/fi-sas/backoffice/compare/v1.132.0...v1.132.1) (2021-12-09)


### Bug Fixes

* **layout:** style menu ([a055439](https://gitlab.com/fi-sas/backoffice/commit/a0554397ed7c0b21f6f6939c5abfaa3f458b21dd))

# [1.132.0](https://gitlab.com/fi-sas/backoffice/compare/v1.131.3...v1.132.0) (2021-12-06)


### Bug Fixes

* **calendar:** remove style ([3021509](https://gitlab.com/fi-sas/backoffice/commit/3021509f221c9738edca2d26d6dd3c976b5b7b73))


### Features

* **calendar:** new part of categories ([07eec78](https://gitlab.com/fi-sas/backoffice/commit/07eec780165d50becbc30b0b0b0e3f65fe897397))

## [1.131.3](https://gitlab.com/fi-sas/backoffice/compare/v1.131.2...v1.131.3) (2021-12-03)


### Bug Fixes

* **geral:** revision merge ([af0cccd](https://gitlab.com/fi-sas/backoffice/commit/af0cccd0f71099a351a2e2b27f46c2c5ce39a95e))

## [1.131.2](https://gitlab.com/fi-sas/backoffice/compare/v1.131.1...v1.131.2) (2021-12-03)


### Bug Fixes

* **communication:** correction error ([96aead8](https://gitlab.com/fi-sas/backoffice/commit/96aead88b05daf607c15c0305d3e92690a038749))

## [1.131.1](https://gitlab.com/fi-sas/backoffice/compare/v1.131.0...v1.131.1) (2021-12-03)


### Bug Fixes

* **alimentation:** entity selected by user ([c533a81](https://gitlab.com/fi-sas/backoffice/commit/c533a819ee96a04090dab224d0e403b405dfc160))
* **communication:** add new filters fields ([8d8abde](https://gitlab.com/fi-sas/backoffice/commit/8d8abdec47ad82d605348cd50b7a500629eaf339))
* **communication:** error correction geral setting ([fbba0ce](https://gitlab.com/fi-sas/backoffice/commit/fbba0ce382582d347d262e8516258432687d23d2))
* **queue:** date field change ([7d29802](https://gitlab.com/fi-sas/backoffice/commit/7d29802454ed02a06bef8118832d64546bac8808))

# [1.131.0](https://gitlab.com/fi-sas/backoffice/compare/v1.130.1...v1.131.0) (2021-12-03)


### Bug Fixes

* **social-support:** dispatch workflow ([673cc35](https://gitlab.com/fi-sas/backoffice/commit/673cc35cb5d12ef137ee082410239af1e76d9e5b))
* **social-support:** dispatch workflow ([a18f697](https://gitlab.com/fi-sas/backoffice/commit/a18f69720dbc408703e0de7bc75231dd40ce71cf))
* **social-support:** fix to how dispatch state is handled ([798c211](https://gitlab.com/fi-sas/backoffice/commit/798c211d67a51255714bb7fd317e70111986c51f))
* **social-support:** status machine workflow refactor ([a439d34](https://gitlab.com/fi-sas/backoffice/commit/a439d34894ff7d37a0b183a749742c3d966e46d2))
* **volunteering:** dispatch workflow ([9146471](https://gitlab.com/fi-sas/backoffice/commit/91464710ead96f15025549d0a42d0ed9cfb52930))
* **volunteering:** status machine workflow refactor ([2192bbc](https://gitlab.com/fi-sas/backoffice/commit/2192bbc44331cdddc063bcd6c23566ac12bd036d))


### Features

* **volunteering:** added dispatch state workflow ([aa6d21a](https://gitlab.com/fi-sas/backoffice/commit/aa6d21aeaf8ac2da654739fc5cb151a53f713ebe))

## [1.130.1](https://gitlab.com/fi-sas/backoffice/compare/v1.130.0...v1.130.1) (2021-12-03)


### Bug Fixes

* **conf:** correction error internal services ([3b10ed8](https://gitlab.com/fi-sas/backoffice/commit/3b10ed88dd6a332e4cf3fd97bd0a249f87a4b606))

# [1.130.0](https://gitlab.com/fi-sas/backoffice/compare/v1.129.4...v1.130.0) (2021-12-03)


### Bug Fixes

* **social-support:** added has_current_account config ([e1508c3](https://gitlab.com/fi-sas/backoffice/commit/e1508c3cbb24ad00a983a60a9cea9694b19e1802))


### Features

* **social-support:** added has_current_account config ([6ba7eaa](https://gitlab.com/fi-sas/backoffice/commit/6ba7eaa08a88ffa917235a531cfa099841a71b09))
* **social-support:** added has_current_account config ([84f40b2](https://gitlab.com/fi-sas/backoffice/commit/84f40b206e622c4b60de6fba98274b4a05822613))

## [1.129.4](https://gitlab.com/fi-sas/backoffice/compare/v1.129.3...v1.129.4) (2021-12-03)


### Bug Fixes

* **billings:** change sort to application.full_name ([7670c19](https://gitlab.com/fi-sas/backoffice/commit/7670c1939f2a6b6ea8e685a85c463975d68ad2d0))

## [1.129.3](https://gitlab.com/fi-sas/backoffice/compare/v1.129.2...v1.129.3) (2021-11-29)


### Bug Fixes

* **accommodation:** add filters change contract ([1809ea2](https://gitlab.com/fi-sas/backoffice/commit/1809ea2fc452e1b0740d80d28257d759e1cabd37))
* **alimentation:** edit prices dish types ([1949930](https://gitlab.com/fi-sas/backoffice/commit/19499308d89b85433aacafc187f9bdfebc5b0c80))

## [1.129.2](https://gitlab.com/fi-sas/backoffice/compare/v1.129.1...v1.129.2) (2021-11-26)


### Bug Fixes

* **ubike:** add sort by bike identification ([0c818ed](https://gitlab.com/fi-sas/backoffice/commit/0c818ed2eaffec8d31279edf77e3e427a9d65ff1))

## [1.129.1](https://gitlab.com/fi-sas/backoffice/compare/v1.129.0...v1.129.1) (2021-11-26)


### Bug Fixes

* **accommodation:** style dashboard ([4112cec](https://gitlab.com/fi-sas/backoffice/commit/4112cec31578149546f1595d7735be1404e67fa5))
* **notification:** add date ([784e742](https://gitlab.com/fi-sas/backoffice/commit/784e74227c26be193b2e2547f870b548f21208e2))
* **ubike:** disabled date & filter in the begin ([c6af50a](https://gitlab.com/fi-sas/backoffice/commit/c6af50a63389da294e0d3118a9641134bc0fffa7))

# [1.129.0](https://gitlab.com/fi-sas/backoffice/compare/v1.128.0...v1.129.0) (2021-11-24)


### Features

* **social-support:** added assigned status auto change config ([e6853d6](https://gitlab.com/fi-sas/backoffice/commit/e6853d61618140c06c0b61241dced62fbefd6932))
* **social-support:** added assigned status auto change config ([1c41de4](https://gitlab.com/fi-sas/backoffice/commit/1c41de4a377a43d3e3946daa2cc5af2905cff0e6))

# [1.128.0](https://gitlab.com/fi-sas/backoffice/compare/v1.127.1...v1.128.0) (2021-11-23)


### Bug Fixes

* **alimentation:** add report stock ([8eacea2](https://gitlab.com/fi-sas/backoffice/commit/8eacea201536027f29819760f31914973fab5ea9))


### Features

* **food:** add endpoint to stock report ([69c67e3](https://gitlab.com/fi-sas/backoffice/commit/69c67e33578632ab3a7e5bc6e4d3524319557191))

## [1.127.1](https://gitlab.com/fi-sas/backoffice/compare/v1.127.0...v1.127.1) (2021-11-22)


### Bug Fixes

* **social_support:** variable change ([9a40b83](https://gitlab.com/fi-sas/backoffice/commit/9a40b83bbea3eedc63eb320a08c8ee2ba25a0455))

# [1.127.0](https://gitlab.com/fi-sas/backoffice/compare/v1.126.4...v1.127.0) (2021-11-22)


### Features

* **packs:** add bulk insert ([41cb732](https://gitlab.com/fi-sas/backoffice/commit/41cb732c2bec1569b4e794268b35ddaa362e17f0))
* **users:** adtivate rfid fiel on user form ([981af5d](https://gitlab.com/fi-sas/backoffice/commit/981af5d1104b4cce19e8e05709329eff126921a9))

## [1.126.4](https://gitlab.com/fi-sas/backoffice/compare/v1.126.3...v1.126.4) (2021-11-18)


### Bug Fixes

* **footer:** centralize information ([494df9e](https://gitlab.com/fi-sas/backoffice/commit/494df9e0d0effec17d91b2956d1573d4de820a21))

## [1.126.3](https://gitlab.com/fi-sas/backoffice/compare/v1.126.2...v1.126.3) (2021-11-18)


### Bug Fixes

* **accommodation:** clear filters ([a9578e1](https://gitlab.com/fi-sas/backoffice/commit/a9578e10765d348d17f94bf56f60d721a34adac3))

## [1.126.2](https://gitlab.com/fi-sas/backoffice/compare/v1.126.1...v1.126.2) (2021-11-18)


### Bug Fixes

* **footer:** initial page footer ([3fbc64a](https://gitlab.com/fi-sas/backoffice/commit/3fbc64ad6a8fc7000c026e2ad7fafafebb07c5cd))

## [1.126.1](https://gitlab.com/fi-sas/backoffice/compare/v1.126.0...v1.126.1) (2021-11-18)


### Bug Fixes

* **footer:** img footer ([2f880c7](https://gitlab.com/fi-sas/backoffice/commit/2f880c785c85047a1c84ecd73c1066dfa2a5e2bd))

# [1.126.0](https://gitlab.com/fi-sas/backoffice/compare/v1.125.5...v1.126.0) (2021-11-17)


### Bug Fixes

* **accommodation:** fix billings debit_direct filter ([2ee4cb6](https://gitlab.com/fi-sas/backoffice/commit/2ee4cb605def7d6803cbc19d4d726ad63d2f8dd5))


### Features

* **accommodation:** add sepa report ([940fed7](https://gitlab.com/fi-sas/backoffice/commit/940fed7437dee9f0d4d9bd09f2c3f2a4a66a4475))

## [1.125.5](https://gitlab.com/fi-sas/backoffice/compare/v1.125.4...v1.125.5) (2021-11-16)


### Bug Fixes

* **footer:** add img footes ([7de5a5c](https://gitlab.com/fi-sas/backoffice/commit/7de5a5cd621c9cdcbd3db7d1859f9e08e3c0d805))

## [1.125.4](https://gitlab.com/fi-sas/backoffice/compare/v1.125.3...v1.125.4) (2021-11-16)


### Bug Fixes

* **accomodation:** filter occurrences, modal user ([81300e6](https://gitlab.com/fi-sas/backoffice/commit/81300e60282949fa41627e7d910ca81512866726))

## [1.125.3](https://gitlab.com/fi-sas/backoffice/compare/v1.125.2...v1.125.3) (2021-11-16)


### Bug Fixes

* **accommodation:** add config billing ([2f7f762](https://gitlab.com/fi-sas/backoffice/commit/2f7f762f79267c21793559f0fd02e63fafe79dce))

## [1.125.2](https://gitlab.com/fi-sas/backoffice/compare/v1.125.1...v1.125.2) (2021-11-15)


### Bug Fixes

* **accommodation:** validation extras and regimes ([9c1cb82](https://gitlab.com/fi-sas/backoffice/commit/9c1cb82c4e69cc9282dbf55fb99caa21127ec339))
* **configurations:** add fields external services ([aa8371a](https://gitlab.com/fi-sas/backoffice/commit/aa8371a166a041c3af3bb69fb780373e7bd82d88))

## [1.125.1](https://gitlab.com/fi-sas/backoffice/compare/v1.125.0...v1.125.1) (2021-11-12)


### Bug Fixes

* **packs:** packs production error ([1908850](https://gitlab.com/fi-sas/backoffice/commit/19088508447f7d76471ecb6fe1782eb92267f28d))

# [1.125.0](https://gitlab.com/fi-sas/backoffice/compare/v1.124.4...v1.125.0) (2021-11-12)


### Features

* **users:** add blocke fields field ([9c78b8e](https://gitlab.com/fi-sas/backoffice/commit/9c78b8ef98749ab3b2dc24807f7daf40e21d70ad))

## [1.124.4](https://gitlab.com/fi-sas/backoffice/compare/v1.124.3...v1.124.4) (2021-11-12)


### Bug Fixes

* **accommodation:** add typologies dashboard ([c6df966](https://gitlab.com/fi-sas/backoffice/commit/c6df966d9accad48ae0545985008945f5a686382))
* **accommodation:** change cursor typology ([05c895b](https://gitlab.com/fi-sas/backoffice/commit/05c895b3d3ff84f3782fc5f68d3705469e9548c5))

## [1.124.3](https://gitlab.com/fi-sas/backoffice/compare/v1.124.2...v1.124.3) (2021-11-12)


### Bug Fixes

* **social-support:** fix how experience report is generated ([3634b6a](https://gitlab.com/fi-sas/backoffice/commit/3634b6a9cbb8c558eb3743f13cb9e1949313a19c))

## [1.124.2](https://gitlab.com/fi-sas/backoffice/compare/v1.124.1...v1.124.2) (2021-11-12)


### Bug Fixes

* **social-support:** change endpoint call generateReport ([b211fbc](https://gitlab.com/fi-sas/backoffice/commit/b211fbc4f3ab818978a54b9e3107fc58e4ff7b08))

## [1.124.1](https://gitlab.com/fi-sas/backoffice/compare/v1.124.0...v1.124.1) (2021-11-11)


### Bug Fixes

* **accommodation:** change extras ([f436121](https://gitlab.com/fi-sas/backoffice/commit/f436121011ab327f015f37fca0419a7115e213dd))
* **alimentation:** add filters bar, reservations ([cf864f4](https://gitlab.com/fi-sas/backoffice/commit/cf864f4ca00f3c16650e2c4739abd0c77d1ffd42))
* **geral:** change "aluno" to "estudante" ([cd8389a](https://gitlab.com/fi-sas/backoffice/commit/cd8389a9bc9614814d6458c583435a6154873e8b))
* **social_support:** add type column ([c3462df](https://gitlab.com/fi-sas/backoffice/commit/c3462df175df5306a38e79b8f48f8b798fc21bf2))
* **volunteering:** add type column ([e20ed72](https://gitlab.com/fi-sas/backoffice/commit/e20ed7270f9461fecee5a63f239b3e2dbf04d838))

# [1.124.0](https://gitlab.com/fi-sas/backoffice/compare/v1.123.3...v1.124.0) (2021-11-10)


### Features

* **social-support:** added config to mark responsible profile ids ([66ef012](https://gitlab.com/fi-sas/backoffice/commit/66ef012a2bd06fd828b87ab72ac7039eace5476d))
* **social-support:** fix payment generation ([5793547](https://gitlab.com/fi-sas/backoffice/commit/579354720b07fe6a6e891325796d0537b3225158))
* **social-support:** generate offer report ([58d3fff](https://gitlab.com/fi-sas/backoffice/commit/58d3fff51117a5a44ddf5358c6a41dc1952cbd7c))
* **social-support:** list users with profiles ([5c8b2bb](https://gitlab.com/fi-sas/backoffice/commit/5c8b2bba270d3f890e62a0dfbd0457925516c650))

## [1.123.3](https://gitlab.com/fi-sas/backoffice/compare/v1.123.2...v1.123.3) (2021-11-10)


### Bug Fixes

* **volunteering:** change text ([74c04eb](https://gitlab.com/fi-sas/backoffice/commit/74c04ebb1b6b982eda3a600ddcf4be3477c29d98))

## [1.123.2](https://gitlab.com/fi-sas/backoffice/compare/v1.123.1...v1.123.2) (2021-11-09)


### Bug Fixes

* **alimentation:** add sell_to_publi field to packs ([fa51cd5](https://gitlab.com/fi-sas/backoffice/commit/fa51cd52a0ac610d0279e5ee736d46fec6628b7e))

## [1.123.1](https://gitlab.com/fi-sas/backoffice/compare/v1.123.0...v1.123.1) (2021-11-09)


### Bug Fixes

* **alimentation:** change endpoint choose entity ([568b218](https://gitlab.com/fi-sas/backoffice/commit/568b218a0b100bd0e46ea3df35d4e69ad8d867aa))

# [1.123.0](https://gitlab.com/fi-sas/backoffice/compare/v1.122.0...v1.123.0) (2021-11-09)


### Features

* **volunteering:** new page list collaborations ([c79d0c1](https://gitlab.com/fi-sas/backoffice/commit/c79d0c1ff7fbbd1f0f3377e098790ef3189a1fe4))

# [1.122.0](https://gitlab.com/fi-sas/backoffice/compare/v1.121.0...v1.122.0) (2021-11-09)


### Bug Fixes

* **alimentation:** change field name on reservations view ([9980493](https://gitlab.com/fi-sas/backoffice/commit/9980493dc75e4f85804e2bab6e4d8ade7ef6118b))


### Features

* **alimentation:** add new view to reservations list ([eee28f9](https://gitlab.com/fi-sas/backoffice/commit/eee28f9c2f8e0205f01c0cbb5e6744d1a1311ecc))

# [1.121.0](https://gitlab.com/fi-sas/backoffice/compare/v1.120.1...v1.121.0) (2021-11-09)


### Features

* **social-support:** added dispatch state ([7659015](https://gitlab.com/fi-sas/backoffice/commit/7659015128f7b844bb891e5d8746c6bc94e5c1ee))
* **social-support:** added dispatch state ([fc30981](https://gitlab.com/fi-sas/backoffice/commit/fc309814e3b5db36a7b55d0a54193c1cfbecf06f))
* **social-support:** added dispatch state ([be4a441](https://gitlab.com/fi-sas/backoffice/commit/be4a44155ba7d659327cb943826f4f114a613a9a))
* **social-support:** added dispatch state ([104e623](https://gitlab.com/fi-sas/backoffice/commit/104e6233acbbf6a9d02843e92334bda0ed0eddc9))

## [1.120.1](https://gitlab.com/fi-sas/backoffice/compare/v1.120.0...v1.120.1) (2021-11-09)


### Bug Fixes

* **accommodation:** add external_validated to billing status ([f5a0265](https://gitlab.com/fi-sas/backoffice/commit/f5a0265a655a1829d947c822449a9c5599f2bf31))

# [1.120.0](https://gitlab.com/fi-sas/backoffice/compare/v1.119.0...v1.120.0) (2021-11-09)


### Features

* **packs:** add initial pack system ([8adea5a](https://gitlab.com/fi-sas/backoffice/commit/8adea5a161c9284200d98ca9c67a42f62990031b))

# [1.119.0](https://gitlab.com/fi-sas/backoffice/compare/v1.118.1...v1.119.0) (2021-11-08)


### Features

* **social_support:** new page list collaborations ([e8efaa4](https://gitlab.com/fi-sas/backoffice/commit/e8efaa4c36cb2c2ff768f15bdb4c70c675ec50e3))

## [1.118.1](https://gitlab.com/fi-sas/backoffice/compare/v1.118.0...v1.118.1) (2021-11-05)


### Bug Fixes

* **comunication:** fields residence filter ([1e84ef4](https://gitlab.com/fi-sas/backoffice/commit/1e84ef490729cd5d43e1f0ac8074321e9bc8e221))

# [1.118.0](https://gitlab.com/fi-sas/backoffice/compare/v1.117.1...v1.118.0) (2021-11-04)


### Features

* **cc:** add cc reports ([4e08a13](https://gitlab.com/fi-sas/backoffice/commit/4e08a137aaac94a54558726763f732863f06a35e))

## [1.117.1](https://gitlab.com/fi-sas/backoffice/compare/v1.117.0...v1.117.1) (2021-11-04)


### Bug Fixes

* **queue:** size text ([c6cbbe6](https://gitlab.com/fi-sas/backoffice/commit/c6cbbe6cbe49934abe6426ae4e2492c1abae6aa6))

# [1.117.0](https://gitlab.com/fi-sas/backoffice/compare/v1.116.0...v1.117.0) (2021-11-04)


### Features

* **social-support:** changed label ([b9ce108](https://gitlab.com/fi-sas/backoffice/commit/b9ce108113632c83ae5f8b7e88573277922fb2e9))
* **social-support:** show application attachments ([1cd5b71](https://gitlab.com/fi-sas/backoffice/commit/1cd5b71d0d34430d4101436c6c8630852dbb8a21))
* **social-support:** show application attachments ([6a8f3ed](https://gitlab.com/fi-sas/backoffice/commit/6a8f3ed10dbae3a70e21015fc076aa6adecaccc5))

# [1.116.0](https://gitlab.com/fi-sas/backoffice/compare/v1.115.14...v1.116.0) (2021-11-04)


### Features

* **social-support:** offer form copy PT fields to EN ([a6f9146](https://gitlab.com/fi-sas/backoffice/commit/a6f914691dfe3dd9810a01f9df0b91898db54744))

## [1.115.14](https://gitlab.com/fi-sas/backoffice/compare/v1.115.13...v1.115.14) (2021-11-03)


### Bug Fixes

* **geral:** new logos ([002ca9a](https://gitlab.com/fi-sas/backoffice/commit/002ca9a062afe7e0635f231fb083f7cbeb000166))

## [1.115.13](https://gitlab.com/fi-sas/backoffice/compare/v1.115.12...v1.115.13) (2021-11-03)


### Bug Fixes

* **configuration:** external services ([39e88ed](https://gitlab.com/fi-sas/backoffice/commit/39e88edcea9c1d9e0125716efe91cd9926e589d4))

## [1.115.12](https://gitlab.com/fi-sas/backoffice/compare/v1.115.11...v1.115.12) (2021-11-03)


### Bug Fixes

* **queue:** add seconds ([7ebf32d](https://gitlab.com/fi-sas/backoffice/commit/7ebf32dd06237b87a9c10a88a01de8b72ed22d5f))

## [1.115.11](https://gitlab.com/fi-sas/backoffice/compare/v1.115.10...v1.115.11) (2021-10-29)


### Bug Fixes

* **accommodation:** filter residence ([f27f43a](https://gitlab.com/fi-sas/backoffice/commit/f27f43a44e13c04c4da1e8a33806ed1a938ca001))
* **accommodation:** modal data perfil ([9d51440](https://gitlab.com/fi-sas/backoffice/commit/9d5144081336e03a6c8e8afe8fd903381c10ddb9))

## [1.115.10](https://gitlab.com/fi-sas/backoffice/compare/v1.115.9...v1.115.10) (2021-10-28)


### Bug Fixes

* **accommodation:** add fields review application ([3d76bac](https://gitlab.com/fi-sas/backoffice/commit/3d76bac5d991eb7e9b651cd33726ee81a13fd2cd))
* **accommodation:** call ms states change contract ([5c5e849](https://gitlab.com/fi-sas/backoffice/commit/5c5e849957bc9b6da0949eb83fd05157e82daf35))
* **accommodation:** valid equal regimes or extras ([dd8124f](https://gitlab.com/fi-sas/backoffice/commit/dd8124fa78607d6123eeddad73bb26af8e0f3ab2))

## [1.115.9](https://gitlab.com/fi-sas/backoffice/compare/v1.115.8...v1.115.9) (2021-10-28)


### Bug Fixes

* **current_account:** correction filter date ([9b452b0](https://gitlab.com/fi-sas/backoffice/commit/9b452b02a8f6e6ea587af5a1ca9380450f6567fd))

## [1.115.8](https://gitlab.com/fi-sas/backoffice/compare/v1.115.7...v1.115.8) (2021-10-27)


### Bug Fixes

* **accommodation:** correction error list ([37943f4](https://gitlab.com/fi-sas/backoffice/commit/37943f4dbbf39e9fee04fa4008ce1a3b1234aa3e))
* **accommodation:** price aligns right and format ([c200f6b](https://gitlab.com/fi-sas/backoffice/commit/c200f6bb477f666ca44951dfaa7a65e398438620))

## [1.115.7](https://gitlab.com/fi-sas/backoffice/compare/v1.115.6...v1.115.7) (2021-10-27)


### Bug Fixes

* **accommodation:** count contract changes ([1b41d5c](https://gitlab.com/fi-sas/backoffice/commit/1b41d5c65b97fce7a2cad10514a7a6b4c84d2dbf))

## [1.115.6](https://gitlab.com/fi-sas/backoffice/compare/v1.115.5...v1.115.6) (2021-10-26)


### Bug Fixes

* **users:** add is_final_consumer ([cb9b18c](https://gitlab.com/fi-sas/backoffice/commit/cb9b18c50496819b9a85cae39dc1bb835e812359))

## [1.115.5](https://gitlab.com/fi-sas/backoffice/compare/v1.115.4...v1.115.5) (2021-10-26)


### Bug Fixes

* **private_accommodation:** correct error required ([b4e5975](https://gitlab.com/fi-sas/backoffice/commit/b4e5975f008e517327e1b7e3204a88ddc11285da))

## [1.115.4](https://gitlab.com/fi-sas/backoffice/compare/v1.115.3...v1.115.4) (2021-10-26)


### Bug Fixes

* **private_accommodation:** add trim description ([3891df0](https://gitlab.com/fi-sas/backoffice/commit/3891df0aa6046f4d38e772de96d9df184951a293))

## [1.115.3](https://gitlab.com/fi-sas/backoffice/compare/v1.115.2...v1.115.3) (2021-10-26)


### Bug Fixes

* **private_accommodation:** property title in en ([13b5f57](https://gitlab.com/fi-sas/backoffice/commit/13b5f57d548126a30daa6c904056f976e6ff7b1f))

## [1.115.2](https://gitlab.com/fi-sas/backoffice/compare/v1.115.1...v1.115.2) (2021-10-26)


### Bug Fixes

* **accommodation:** include selected billings to sepa ([f29099a](https://gitlab.com/fi-sas/backoffice/commit/f29099a74512cb2587723dcf61f4efb3d0ed9d46))

## [1.115.1](https://gitlab.com/fi-sas/backoffice/compare/v1.115.0...v1.115.1) (2021-10-25)


### Bug Fixes

* **accommodation:** period ([ee90aab](https://gitlab.com/fi-sas/backoffice/commit/ee90aab6a89c101fcdded7f46fa9e6b59bbfa7fb))

# [1.115.0](https://gitlab.com/fi-sas/backoffice/compare/v1.114.5...v1.115.0) (2021-10-22)


### Bug Fixes

* revert proxy-config ([1c31474](https://gitlab.com/fi-sas/backoffice/commit/1c31474035d603b619f99caa684b101fda197460))


### Features

* **accomodation:** sepa generation ([789a2a6](https://gitlab.com/fi-sas/backoffice/commit/789a2a691d7bb840f2466940ed2a5397adcb0f5b))
* **sepa:** geração de ficheiro  débito directo ([8867d43](https://gitlab.com/fi-sas/backoffice/commit/8867d4336b4c1b730cc520ff1a008cc30d7f0dbb))

## [1.114.5](https://gitlab.com/fi-sas/backoffice/compare/v1.114.4...v1.114.5) (2021-10-21)


### Bug Fixes

* **accommodation:** change MS resume list ([fcdac52](https://gitlab.com/fi-sas/backoffice/commit/fcdac5227057003095262048932c3154258a8e78))

## [1.114.4](https://gitlab.com/fi-sas/backoffice/compare/v1.114.3...v1.114.4) (2021-10-21)


### Bug Fixes

* **queue:** services and subject active/ inative ([ecc25c3](https://gitlab.com/fi-sas/backoffice/commit/ecc25c365f1fd92bbbcf05b78238557d81f6036a))

## [1.114.3](https://gitlab.com/fi-sas/backoffice/compare/v1.114.2...v1.114.3) (2021-10-20)


### Bug Fixes

* **alimentation:** pack_available dish type ([242b20d](https://gitlab.com/fi-sas/backoffice/commit/242b20da7d4a390ae367f5004a96863c94dfa8c0))
* **mobility:** new column table declaration ([da56cee](https://gitlab.com/fi-sas/backoffice/commit/da56cee32ee8f10048852b38e7e3e78df6d109fe))
* **social_support:** change colores status ([184825b](https://gitlab.com/fi-sas/backoffice/commit/184825b3259bc9d5cb0b639ce973fa0c9f5a6e57))
* **volunteering:** change color status ([ff2a98a](https://gitlab.com/fi-sas/backoffice/commit/ff2a98a1fd827a3ccd315ed8dd1856735dcd2704))

## [1.114.2](https://gitlab.com/fi-sas/backoffice/compare/v1.114.1...v1.114.2) (2021-10-20)


### Bug Fixes

* **queue:** correction dates & notes ([17a8d3e](https://gitlab.com/fi-sas/backoffice/commit/17a8d3e67ce25d4142f8bedaea8356f28b4ec6d9))

## [1.114.1](https://gitlab.com/fi-sas/backoffice/compare/v1.114.0...v1.114.1) (2021-10-19)


### Bug Fixes

* **queue:** change ticket code ([dd59580](https://gitlab.com/fi-sas/backoffice/commit/dd59580355649d94421e08796a99bb549ee448c6))

# [1.114.0](https://gitlab.com/fi-sas/backoffice/compare/v1.113.2...v1.114.0) (2021-10-19)


### Bug Fixes

* **accommodation:** change modal add item billing ([2c661bf](https://gitlab.com/fi-sas/backoffice/commit/2c661bf43d88368202392b5fdab69db68bbc7c78))
* **accommodation:** correction error vat extras ([17471da](https://gitlab.com/fi-sas/backoffice/commit/17471dabf8ecdf91b4426b330312e48e1693c116))
* **accommodation:** correction problem doc img ([788ad3f](https://gitlab.com/fi-sas/backoffice/commit/788ad3fc928b126bb0cccff8676653969818754d))
* **accommodation:** new columns table ([119535f](https://gitlab.com/fi-sas/backoffice/commit/119535f636ae719e08c698eb7f4840be656b2a71))
* **accommodation:** new status withdrawal dashboar ([66b1ae4](https://gitlab.com/fi-sas/backoffice/commit/66b1ae4e8e198328d200c587401b13206f5a60ac))
* **accommodation:** save info page changes ([a02270b](https://gitlab.com/fi-sas/backoffice/commit/a02270b64a9244fcff218f25c007963c7874ba1b))
* **accommodation:** test ([2e71e88](https://gitlab.com/fi-sas/backoffice/commit/2e71e88e235d4aea7ad17e2b35642e5de7dc0613))


### Features

* **accommodation:** new list page ([4d5f0b3](https://gitlab.com/fi-sas/backoffice/commit/4d5f0b3b5d2538add555ffd8d919f9cc05db883a))

## [1.113.2](https://gitlab.com/fi-sas/backoffice/compare/v1.113.1...v1.113.2) (2021-10-18)


### Bug Fixes

* **alimentation:** change meal field name ([9e90351](https://gitlab.com/fi-sas/backoffice/commit/9e90351390441a5bcb996cbcb048b28230c47ba9))

## [1.113.1](https://gitlab.com/fi-sas/backoffice/compare/v1.113.0...v1.113.1) (2021-10-14)


### Bug Fixes

* **queue:** change texte ([d38b86d](https://gitlab.com/fi-sas/backoffice/commit/d38b86da7cdabee0821ae1218e6e57ca15ab2785))
* **social-support:** normalize hours format ([91b772a](https://gitlab.com/fi-sas/backoffice/commit/91b772ac4a1289738887dd51056678866909ed4b))
* **social-support:** normalize hours format ([8d69385](https://gitlab.com/fi-sas/backoffice/commit/8d693855e67a0d6c953a83289a8204f6f40bb654))
* **social-support:** show new fields on application ([8278b1e](https://gitlab.com/fi-sas/backoffice/commit/8278b1e8b756f68fb5479fa145dfe8f778353244))
* **social-support:** show new fields on application ([af5f295](https://gitlab.com/fi-sas/backoffice/commit/af5f29543dbd374fc0a4373e75753abac1814bc2))
* **social-support:** show new fields on application ([02992d2](https://gitlab.com/fi-sas/backoffice/commit/02992d21a287702dbd684e7ce9ea836ddf616cf8))
* **social-support:** show new fields on application ([8632a00](https://gitlab.com/fi-sas/backoffice/commit/8632a004b3ca8b1e2d1539e557988fa16d3e9025))
* **volunteering:** normalize hours format ([af4f585](https://gitlab.com/fi-sas/backoffice/commit/af4f5857a0f3fd8a8adbf46e26e554dfc64a7119))

# [1.113.0](https://gitlab.com/fi-sas/backoffice/compare/v1.112.13...v1.113.0) (2021-10-14)


### Features

* **configurations:** allow presentation text to be blank ([cc1ba5e](https://gitlab.com/fi-sas/backoffice/commit/cc1ba5e631f55dec31343a81d4ae30d8657d00c2))

## [1.112.13](https://gitlab.com/fi-sas/backoffice/compare/v1.112.12...v1.112.13) (2021-10-14)


### Bug Fixes

* **social-support:** fixed selected academic year filter from panel ([b6a3f99](https://gitlab.com/fi-sas/backoffice/commit/b6a3f99fe5e2e8eb6af7d7aefb6a2f6e079e2e2b))
* **social-support:** fixed selected academic year filter from panel ([9f0edc3](https://gitlab.com/fi-sas/backoffice/commit/9f0edc37d675e8e01571c3abbe2f84782cb734e3))
* **volunteering:** fixed selected academic year filter from panel ([aa22fc6](https://gitlab.com/fi-sas/backoffice/commit/aa22fc68b9ea2e8c519a075eddd049a17c6e6b4b))
* **volunteering:** fixed selected academic year filter from panel ([b5558d9](https://gitlab.com/fi-sas/backoffice/commit/b5558d958ccb82e8c53a47c2a71ab682df0412d3))

## [1.112.12](https://gitlab.com/fi-sas/backoffice/compare/v1.112.11...v1.112.12) (2021-10-13)


### Bug Fixes

* **alimentation:** filter stock ([a14b542](https://gitlab.com/fi-sas/backoffice/commit/a14b542774553e96a603b9b305f99f13ec56a007))
* **current_account:** delete space top page ([42d68e1](https://gitlab.com/fi-sas/backoffice/commit/42d68e1c713500e8027daf22070d88ae6628011a))

## [1.112.11](https://gitlab.com/fi-sas/backoffice/compare/v1.112.10...v1.112.11) (2021-10-13)


### Bug Fixes

* **ubike:** correction status error ([7c16ae0](https://gitlab.com/fi-sas/backoffice/commit/7c16ae03147e0be7e73f0d4084854ed3f63de134))

## [1.112.10](https://gitlab.com/fi-sas/backoffice/compare/v1.112.9...v1.112.10) (2021-10-12)


### Bug Fixes

* **ubike:** change status ([c182f30](https://gitlab.com/fi-sas/backoffice/commit/c182f30e00829a164f865963db456129b9280dfd))

## [1.112.9](https://gitlab.com/fi-sas/backoffice/compare/v1.112.8...v1.112.9) (2021-10-12)


### Bug Fixes

* **current_account:** img success ([69b6210](https://gitlab.com/fi-sas/backoffice/commit/69b6210cd2f4ef998c969592a444daa675d3ed9d))

## [1.112.8](https://gitlab.com/fi-sas/backoffice/compare/v1.112.7...v1.112.8) (2021-10-12)


### Bug Fixes

* **current_account:** correction of values ([15933c2](https://gitlab.com/fi-sas/backoffice/commit/15933c20eb9169f54629fec9d4f1e5c8cf1206d1))

## [1.112.7](https://gitlab.com/fi-sas/backoffice/compare/v1.112.6...v1.112.7) (2021-10-12)


### Bug Fixes

* **geral:** limit 255 caracters textarea ([8c4eec0](https://gitlab.com/fi-sas/backoffice/commit/8c4eec02e29efaa984350f06dbfe3868ed794476))

## [1.112.6](https://gitlab.com/fi-sas/backoffice/compare/v1.112.5...v1.112.6) (2021-10-11)


### Bug Fixes

* **alimentation:** product qt group by wharehouse ([d2da1b2](https://gitlab.com/fi-sas/backoffice/commit/d2da1b29fdaa00efeb5ed203626dcc25fe1c8214))

## [1.112.5](https://gitlab.com/fi-sas/backoffice/compare/v1.112.4...v1.112.5) (2021-10-08)


### Bug Fixes

* **dish-types:** add pos operations form patch ([2d294cd](https://gitlab.com/fi-sas/backoffice/commit/2d294cd9c95d79db9849fe09fdb3cbbbe0de4139))

## [1.112.4](https://gitlab.com/fi-sas/backoffice/compare/v1.112.3...v1.112.4) (2021-10-08)


### Bug Fixes

* **communication:** resolution error cropping img ([a9c2fc5](https://gitlab.com/fi-sas/backoffice/commit/a9c2fc5211b8187fd23df4ff1906e94cd6b092b1))

## [1.112.3](https://gitlab.com/fi-sas/backoffice/compare/v1.112.2...v1.112.3) (2021-10-07)


### Bug Fixes

* **current_account:** error loading ([bffb844](https://gitlab.com/fi-sas/backoffice/commit/bffb844547fff33d74384bc7239c4479f553022c))

## [1.112.2](https://gitlab.com/fi-sas/backoffice/compare/v1.112.1...v1.112.2) (2021-10-07)


### Bug Fixes

* **accommodation:** fix applications filter by residence ([09d53e9](https://gitlab.com/fi-sas/backoffice/commit/09d53e98f1b59356ada5b078e22407d1b0097b6b))

## [1.112.1](https://gitlab.com/fi-sas/backoffice/compare/v1.112.0...v1.112.1) (2021-10-07)


### Bug Fixes

* **accommodation:** processing report ([938aa2e](https://gitlab.com/fi-sas/backoffice/commit/938aa2ed3ea875c83f8ae27f4b9963a9ca762e20))

# [1.112.0](https://gitlab.com/fi-sas/backoffice/compare/v1.111.1...v1.112.0) (2021-10-06)


### Features

* **current_account:** new page current account ([9d7df9b](https://gitlab.com/fi-sas/backoffice/commit/9d7df9b04ec06ecf5415447a3bf53392bdda2e17))

## [1.111.1](https://gitlab.com/fi-sas/backoffice/compare/v1.111.0...v1.111.1) (2021-10-06)


### Bug Fixes

* **accommodation:** reprocess contracted applications ([e86811d](https://gitlab.com/fi-sas/backoffice/commit/e86811df8ce7d4ddd636f186beebe028a7b14a5e))

# [1.111.0](https://gitlab.com/fi-sas/backoffice/compare/v1.110.2...v1.111.0) (2021-10-01)


### Features

* **accommodation:** fix error of set residence on contract ([9cf367d](https://gitlab.com/fi-sas/backoffice/commit/9cf367dcb5679a1ab390605e6accc99a0858095f))

## [1.110.2](https://gitlab.com/fi-sas/backoffice/compare/v1.110.1...v1.110.2) (2021-09-30)


### Bug Fixes

* **accommodation:** change text ([d598409](https://gitlab.com/fi-sas/backoffice/commit/d598409503d0f82329091f3b7b8f9cccf72be589))
* **auth:** correction permission ([343d181](https://gitlab.com/fi-sas/backoffice/commit/343d1816694db478b5aab78ccfb059cecc49182e))

## [1.110.1](https://gitlab.com/fi-sas/backoffice/compare/v1.110.0...v1.110.1) (2021-09-29)


### Bug Fixes

* **alimentation:** add cancel to reservations ([a60e98b](https://gitlab.com/fi-sas/backoffice/commit/a60e98b0cfca621bbb528496c051b243edc45113))

# [1.110.0](https://gitlab.com/fi-sas/backoffice/compare/v1.109.1...v1.110.0) (2021-09-29)


### Features

* **alimentation:** add reservation counts ([f04c02f](https://gitlab.com/fi-sas/backoffice/commit/f04c02fb38db623bfc733bdc899ee5057b45e1ad))

## [1.109.1](https://gitlab.com/fi-sas/backoffice/compare/v1.109.0...v1.109.1) (2021-09-29)


### Bug Fixes

* **accommodation:** remove is_deposit on extras ([a24daf7](https://gitlab.com/fi-sas/backoffice/commit/a24daf7d328f08f08ca07d250c42766c127869f9))

# [1.109.0](https://gitlab.com/fi-sas/backoffice/compare/v1.108.5...v1.109.0) (2021-09-29)


### Bug Fixes

* **social-support:** show all experiences on filter ([0be1465](https://gitlab.com/fi-sas/backoffice/commit/0be1465ac735604723cc6adf40efc97d283001d7))


### Features

* **social-support:** allow register interview from actions ([6c8b726](https://gitlab.com/fi-sas/backoffice/commit/6c8b726d141666dcc509ec6f1bd2939695e0300d))
* **u-bike:** accept/decline bike assignement ([95cf776](https://gitlab.com/fi-sas/backoffice/commit/95cf77623d76d03b31c11d1811cddf189cc7ede8))
* **volunteering:** validate/reject attendances ([d2772b4](https://gitlab.com/fi-sas/backoffice/commit/d2772b4b5a4c8711caf2ba2de1b4a3b69c092fc6))

## [1.108.5](https://gitlab.com/fi-sas/backoffice/compare/v1.108.4...v1.108.5) (2021-09-27)


### Bug Fixes

* **alimentation:** info quantity ([ab86a0f](https://gitlab.com/fi-sas/backoffice/commit/ab86a0fded051e4653e8a5bd6169dfa1f798924b))

## [1.108.4](https://gitlab.com/fi-sas/backoffice/compare/v1.108.3...v1.108.4) (2021-09-27)


### Bug Fixes

* **alimentation:** correction error ([3071cf6](https://gitlab.com/fi-sas/backoffice/commit/3071cf68962f8de0c741a1ac912be56a68543f60))

## [1.108.3](https://gitlab.com/fi-sas/backoffice/compare/v1.108.2...v1.108.3) (2021-09-27)


### Bug Fixes

* **alimentations:** delete trasferwharehouse ([7d3bd0e](https://gitlab.com/fi-sas/backoffice/commit/7d3bd0e307e4a7dfddc3bbac0f91e50b9b821f10))

## [1.108.2](https://gitlab.com/fi-sas/backoffice/compare/v1.108.1...v1.108.2) (2021-09-27)


### Bug Fixes

* **alimnetation:** delete function ([2fc20cb](https://gitlab.com/fi-sas/backoffice/commit/2fc20cb398cf247af4dbdc57d246620f8b26393f))

## [1.108.1](https://gitlab.com/fi-sas/backoffice/compare/v1.108.0...v1.108.1) (2021-09-27)


### Bug Fixes

* **alimentation:** limit product ([0e1e02c](https://gitlab.com/fi-sas/backoffice/commit/0e1e02cb203e80995eaf7708ff9aa0ddf2142127))

# [1.108.0](https://gitlab.com/fi-sas/backoffice/compare/v1.107.2...v1.108.0) (2021-09-24)


### Features

* **bus:** add declaration field ([10bc35c](https://gitlab.com/fi-sas/backoffice/commit/10bc35c5c8ce98ec11531a09206b839be8cd6bde))

## [1.107.2](https://gitlab.com/fi-sas/backoffice/compare/v1.107.1...v1.107.2) (2021-09-24)


### Bug Fixes

* **accommodation:** allow zero value on patrimony categories ([9bcaa6b](https://gitlab.com/fi-sas/backoffice/commit/9bcaa6b04ac8fff16853b73dcf389437ab1632c1))
* **accommodation:** change allow_debit_direct and IBAN order ([38de606](https://gitlab.com/fi-sas/backoffice/commit/38de6064e8a31c56466bf45993aa394a219588e5))
* **accommodation:** minor fixs ([5cb335d](https://gitlab.com/fi-sas/backoffice/commit/5cb335d2364ff2c1f89baab7a225d2f5786ede12))

## [1.107.1](https://gitlab.com/fi-sas/backoffice/compare/v1.107.0...v1.107.1) (2021-09-23)


### Bug Fixes

* **alimentation:** dish form recipes select ([f470fb4](https://gitlab.com/fi-sas/backoffice/commit/f470fb43832508ecf303a87336dde71827316adf))

# [1.107.0](https://gitlab.com/fi-sas/backoffice/compare/v1.106.5...v1.107.0) (2021-09-23)


### Features

* **bus:** added 'observations' field ([021f414](https://gitlab.com/fi-sas/backoffice/commit/021f414709149f12b3fc28f8fcbf39045955e63b))
* **bus:** added 'observations' field ([4947cc6](https://gitlab.com/fi-sas/backoffice/commit/4947cc6c76a3e35106969f7d9e954b0aae12d71f))
* **bus:** added 'observations' field ([a0055ad](https://gitlab.com/fi-sas/backoffice/commit/a0055add3fa0567599b5a871f42d6bad6c151b2e))

## [1.106.5](https://gitlab.com/fi-sas/backoffice/compare/v1.106.4...v1.106.5) (2021-09-22)


### Bug Fixes

* **alimentation:** change endpoints of orders serve unserve ([43247f8](https://gitlab.com/fi-sas/backoffice/commit/43247f83bb0f3e5574714dddc42861e843390a2d))
* build production errors ([c6d2a2a](https://gitlab.com/fi-sas/backoffice/commit/c6d2a2ac24aaec6947fc7a22b66500b5a66c94df))

## [1.106.4](https://gitlab.com/fi-sas/backoffice/compare/v1.106.3...v1.106.4) (2021-09-22)


### Bug Fixes

* **accommodation:** add phase filter to application raw report ([4c8cb02](https://gitlab.com/fi-sas/backoffice/commit/4c8cb020c716b977512ae9767be1d0d2e45a3ce2))

## [1.106.3](https://gitlab.com/fi-sas/backoffice/compare/v1.106.2...v1.106.3) (2021-09-21)


### Bug Fixes

* **bus:** change reports page ([11ac498](https://gitlab.com/fi-sas/backoffice/commit/11ac498fbddb53e6cfa4791e8b65b1d7eea630ee))

## [1.106.2](https://gitlab.com/fi-sas/backoffice/compare/v1.106.1...v1.106.2) (2021-09-20)


### Bug Fixes

* **accommodation:** fix errors on application history ([f18b2d9](https://gitlab.com/fi-sas/backoffice/commit/f18b2d99e2353b0cdbea38915d09c41ae378bcbe))
* **application:** add withdrawal rollback status ([4e17267](https://gitlab.com/fi-sas/backoffice/commit/4e17267bf7480dd372a8ef4cceb70074c9e76780))

## [1.106.1](https://gitlab.com/fi-sas/backoffice/compare/v1.106.0...v1.106.1) (2021-09-20)


### Bug Fixes

* **alimentation:** change stock moviment list ([6618af4](https://gitlab.com/fi-sas/backoffice/commit/6618af4b62c396c8ad696194cc87d917d282c1f4))

# [1.106.0](https://gitlab.com/fi-sas/backoffice/compare/v1.105.2...v1.106.0) (2021-09-17)


### Bug Fixes

* **accommodation:** add filters on application changes ([c1a4aad](https://gitlab.com/fi-sas/backoffice/commit/c1a4aadef822af42354fce2d295efdf74b79f0a4))
* **applications-list-volunteering:** add query ([2a3e880](https://gitlab.com/fi-sas/backoffice/commit/2a3e8803415d55f342999cb843e94d8e94d2918b))
* **experiences-list-volunteering:** add query ([2b10bc1](https://gitlab.com/fi-sas/backoffice/commit/2b10bc1235e9c0596c3a00fa07935c38440ae480))
* **panel-stats-volunteering:** update panel ([d05d0c5](https://gitlab.com/fi-sas/backoffice/commit/d05d0c5772af7ad59af55e73dc0c86371cf7cfcd))
* **stats-volunteering:** update endpoints stats ([63ba3e9](https://gitlab.com/fi-sas/backoffice/commit/63ba3e9296bf9d8756bc94aea9c33995b5449c86))
* **volunteering-component:** remove call endpoints ([80613c3](https://gitlab.com/fi-sas/backoffice/commit/80613c34d183df88f99298bf6210813450233982))


### Features

* **accommodation:** add iban change request ([a997bd1](https://gitlab.com/fi-sas/backoffice/commit/a997bd162c11f6d11f1068a0743b5e26f8c33922))
* **app.config:** add new endpoints ([9986903](https://gitlab.com/fi-sas/backoffice/commit/998690374336e032dd4d7dae930a2be865920213))

## [1.105.2](https://gitlab.com/fi-sas/backoffice/compare/v1.105.1...v1.105.2) (2021-09-17)


### Bug Fixes

* **communication:** correction error categories ([af496e3](https://gitlab.com/fi-sas/backoffice/commit/af496e3bed57f9c15d031c31c1dfb4da999a5e1e))

## [1.105.1](https://gitlab.com/fi-sas/backoffice/compare/v1.105.0...v1.105.1) (2021-09-17)


### Bug Fixes

* **communication:** required kiosk, tv ([48f0925](https://gitlab.com/fi-sas/backoffice/commit/48f0925389da66b0fb2c76f25051aee6a1811a1d))

# [1.105.0](https://gitlab.com/fi-sas/backoffice/compare/v1.104.0...v1.105.0) (2021-09-17)


### Bug Fixes

* **accommodation:** fix permute room errror ([b810e52](https://gitlab.com/fi-sas/backoffice/commit/b810e52d63173228ea1b5f0d011edab0d4b008e3))
* **accommodation:** premute room error ([af591d6](https://gitlab.com/fi-sas/backoffice/commit/af591d63675f49ca703bd8f861ef4c440f9230cd))
* **communication:** change video required ([76fb4cc](https://gitlab.com/fi-sas/backoffice/commit/76fb4cc311893e80e822f153bd290a45ef3f50bb))


### Features

* **accommodation:** add change room request ([c2bd5ed](https://gitlab.com/fi-sas/backoffice/commit/c2bd5ed2c7fa4d2b861e43dc6a221ada911aa745))

# [1.104.0](https://gitlab.com/fi-sas/backoffice/compare/v1.103.0...v1.104.0) (2021-09-17)


### Bug Fixes

* **app.config:** add endpoint ([d4d1c4b](https://gitlab.com/fi-sas/backoffice/commit/d4d1c4b7619c619730e9f87e23b52934db7188fb))
* **bus-component:** update component bus ([e77091b](https://gitlab.com/fi-sas/backoffice/commit/e77091b78465d07ebfd9ad979f3f620e41064db5))


### Features

* **list-application-bus:** add academic year ([cd954f5](https://gitlab.com/fi-sas/backoffice/commit/cd954f532aebc34e2fd62a1cd638694db0dab282))
* **reports_bus:** add new report ([0a5a82d](https://gitlab.com/fi-sas/backoffice/commit/0a5a82da0b164402e9b77e97a9c7d41514fdc515))
* **view-declaration:** add cc_emittted_in ([bb3f961](https://gitlab.com/fi-sas/backoffice/commit/bb3f9619d7a4690c95eec5580b45646b9762a4f1))

# [1.103.0](https://gitlab.com/fi-sas/backoffice/compare/v1.102.2...v1.103.0) (2021-09-16)


### Bug Fixes

* **accommodation:** add manual processments to withdrawal applications ([912b35e](https://gitlab.com/fi-sas/backoffice/commit/912b35ea65117c9bbf01af268401e9295157505c))
* **volunteering:** course not required on volunteering ([f8b9c4d](https://gitlab.com/fi-sas/backoffice/commit/f8b9c4d99a235a5cdf2e61b4e8c3b5d094567357))


### Features

* **application-changes:** add filters on change requests ([ed8aa11](https://gitlab.com/fi-sas/backoffice/commit/ed8aa111eef4aadb21d71bd73b1bfec8e0caefc3))

## [1.102.2](https://gitlab.com/fi-sas/backoffice/compare/v1.102.1...v1.102.2) (2021-09-15)


### Bug Fixes

* **alimentation:** new part of stock ([d97077a](https://gitlab.com/fi-sas/backoffice/commit/d97077a5fcdd6ed8367b89fd76c75659705b2e76))

## [1.102.1](https://gitlab.com/fi-sas/backoffice/compare/v1.102.0...v1.102.1) (2021-09-15)


### Bug Fixes

* **infrastructure:** remove requiered fields on organic units ([fdea4a4](https://gitlab.com/fi-sas/backoffice/commit/fdea4a4b50655c0b19e5a73f0c657389a5c2c89b))
* **ubike:** fix application submit errors ([559bfc9](https://gitlab.com/fi-sas/backoffice/commit/559bfc90cdd817dd4cb4a4566c95e4c01936be69))
* **ubike:** fix application submit errors ([9ecc40a](https://gitlab.com/fi-sas/backoffice/commit/9ecc40a7967b2ea33478cf78c45ca1fa5ee92cb0))
* **ubike:** fix application submit errors ([2d98808](https://gitlab.com/fi-sas/backoffice/commit/2d9880873da5a3e20c9a3c59918b60e656fe381a))

# [1.102.0](https://gitlab.com/fi-sas/backoffice/compare/v1.101.0...v1.102.0) (2021-09-15)


### Bug Fixes

* **bus-sub23-declaration:** update service chamge status ([36265ae](https://gitlab.com/fi-sas/backoffice/commit/36265aec5108985a7390ee49d55510411cb9dbf1))
* **list-23-declarations:** update validations and infos ([244f8a0](https://gitlab.com/fi-sas/backoffice/commit/244f8a0f64b15112ec0453b266d9d938a610b0bf))
* **list-23-declarations-sub23:** update actions and add validations ([b160b12](https://gitlab.com/fi-sas/backoffice/commit/b160b12ce750a3ebeccf0bab33a0bf329394c921))


### Features

* **bus:** add new componet for generate reports ([90c65a2](https://gitlab.com/fi-sas/backoffice/commit/90c65a2466e63ec66388bfa83f043139f109757e))
* **declaration-sub-23-view:** add reject_reason ([0e25e11](https://gitlab.com/fi-sas/backoffice/commit/0e25e1138f4be54fe359c24373680a27dc986c00))
* **sub_declaration_mode:** add new status ([0e25596](https://gitlab.com/fi-sas/backoffice/commit/0e255963c7be5a9546ad55ed0103ead737c4fc22))

# [1.101.0](https://gitlab.com/fi-sas/backoffice/compare/v1.100.1...v1.101.0) (2021-09-14)


### Features

* **acommodation:** add change room application request ([df80a9d](https://gitlab.com/fi-sas/backoffice/commit/df80a9d43c9902f811e626f7d5898e1ed42b4d1c))
* **communication:** add optional to general configs ([ad3a499](https://gitlab.com/fi-sas/backoffice/commit/ad3a4992c4be72e83a03b2224c81a72c1fb1b8e7))

## [1.100.1](https://gitlab.com/fi-sas/backoffice/compare/v1.100.0...v1.100.1) (2021-09-14)


### Bug Fixes

* **social-support-experience-user-interest:** fix status error change ([36771c3](https://gitlab.com/fi-sas/backoffice/commit/36771c3ecb4208b0795a1fddeecf8e9481c8845b))

# [1.100.0](https://gitlab.com/fi-sas/backoffice/compare/v1.99.3...v1.100.0) (2021-09-13)


### Features

* **accommodation:** add show iban configuration ([93ed7ae](https://gitlab.com/fi-sas/backoffice/commit/93ed7aec32579be8630097a61276da76d516fc27))

## [1.99.3](https://gitlab.com/fi-sas/backoffice/compare/v1.99.2...v1.99.3) (2021-09-10)


### Bug Fixes

* **auth.guard:** change validation token to module check ([8f67563](https://gitlab.com/fi-sas/backoffice/commit/8f67563dc8324a9a72950b7de3f98eeee5aac1df))

## [1.99.2](https://gitlab.com/fi-sas/backoffice/compare/v1.99.1...v1.99.2) (2021-09-10)


### Bug Fixes

* **communication:** required kiosk, tv ([9c011fd](https://gitlab.com/fi-sas/backoffice/commit/9c011fd8fc20478274a81d722d38ae01f28ba8da))

## [1.99.1](https://gitlab.com/fi-sas/backoffice/compare/v1.99.0...v1.99.1) (2021-09-10)


### Bug Fixes

* **alimentation:** valid access wharehouses stocks ([0f89b7d](https://gitlab.com/fi-sas/backoffice/commit/0f89b7dd2ff8c46bd8e0d19ce3f3dc1910e1d886))

# [1.99.0](https://gitlab.com/fi-sas/backoffice/compare/v1.98.0...v1.99.0) (2021-09-10)


### Features

* **accommodation:** add application entry and exit communications ([5867a8c](https://gitlab.com/fi-sas/backoffice/commit/5867a8cb381a1d1ee63481789bd90a279429ade5))

# [1.98.0](https://gitlab.com/fi-sas/backoffice/compare/v1.97.0...v1.98.0) (2021-09-09)


### Features

* **ubike:** added configs for ubike notifications ([f617657](https://gitlab.com/fi-sas/backoffice/commit/f617657931dc9fb0882bf7526cdfb8b40221b046))
* **ubike:** added configs for ubike notifications ([5e9edd6](https://gitlab.com/fi-sas/backoffice/commit/5e9edd6d513f074260cae92c5e55562533d0d46f))
* **ubike:** added configs for ubike notifications ([6fbf94a](https://gitlab.com/fi-sas/backoffice/commit/6fbf94ae12e022daced90d84580affa08763e0a6))
* **ubike:** added configs for ubike notifications ([7baefc3](https://gitlab.com/fi-sas/backoffice/commit/7baefc3e4dae6581403c026751950d50c39aa18e))

# [1.97.0](https://gitlab.com/fi-sas/backoffice/compare/v1.96.1...v1.97.0) (2021-09-09)


### Features

* **panel-stats-social-support:** add filter academic year ([bd5908d](https://gitlab.com/fi-sas/backoffice/commit/bd5908d308aed21d83b04b99e0775aa45ab3ec85))

## [1.96.1](https://gitlab.com/fi-sas/backoffice/compare/v1.96.0...v1.96.1) (2021-09-09)


### Bug Fixes

* **list-view-applications-ubike:** optimize list and view ([915913e](https://gitlab.com/fi-sas/backoffice/commit/915913e071484ebbf877816a34d4fbba5f5ae0e6))

# [1.96.0](https://gitlab.com/fi-sas/backoffice/compare/v1.95.0...v1.96.0) (2021-09-08)


### Bug Fixes

* **social-support-applications:** remove load ([014288c](https://gitlab.com/fi-sas/backoffice/commit/014288c01c19f8d279f25b488f8d9151351e97ba))
* **social-support-list-applications:** update validation ([4f2b699](https://gitlab.com/fi-sas/backoffice/commit/4f2b69983c5cba4f6ac2b5a07c7ee3541fb68928))


### Features

* **dashboar:** sort menus ([ca8a8d7](https://gitlab.com/fi-sas/backoffice/commit/ca8a8d7204aeaec6a1d93a719aabb8ec5a539227))
* **social-suport-list-applications:** add filter current academic year ([e6cddc6](https://gitlab.com/fi-sas/backoffice/commit/e6cddc6788d02501076bbb5ea1ff5b8d9900aad4))
* **social-support:** fixes to errors reported ([b79ed99](https://gitlab.com/fi-sas/backoffice/commit/b79ed99e97221cb572f4fb75ba144a65a756d4ca))
* **social-support:** register interview from listing ([dddc306](https://gitlab.com/fi-sas/backoffice/commit/dddc306871a9a2cd14412fb56c3e8b9b980d7e06))

# [1.95.0](https://gitlab.com/fi-sas/backoffice/compare/v1.94.3...v1.95.0) (2021-09-08)


### Features

* **accommodation:** add list/create/edit occurrences ([ee929ae](https://gitlab.com/fi-sas/backoffice/commit/ee929ae9593726dfed1b60784db0afbe8b0f8001))
* **accommodation:** add missing application withrelated ([dd93128](https://gitlab.com/fi-sas/backoffice/commit/dd93128341546ecadb767803038a8145f04d255a))

## [1.94.3](https://gitlab.com/fi-sas/backoffice/compare/v1.94.2...v1.94.3) (2021-09-08)


### Bug Fixes

* **upload-file:** remove console log ([9a580d7](https://gitlab.com/fi-sas/backoffice/commit/9a580d75fd108a915ff7787f022ccac93c60d1f9))
* **upload-file-componet:** add validations ([88a6adf](https://gitlab.com/fi-sas/backoffice/commit/88a6adfc442c2a8dfd79d71189abdb5cf061fb78))

## [1.94.2](https://gitlab.com/fi-sas/backoffice/compare/v1.94.1...v1.94.2) (2021-09-07)


### Bug Fixes

* **private-accommodation:** correção erro nas configurações ([d25d437](https://gitlab.com/fi-sas/backoffice/commit/d25d437a7f738486d5b39628665061ee8c05072c))

## [1.94.1](https://gitlab.com/fi-sas/backoffice/compare/v1.94.0...v1.94.1) (2021-09-07)


### Bug Fixes

* **accommodation:** name change in view ([bccfd91](https://gitlab.com/fi-sas/backoffice/commit/bccfd91939494081c9b84e13f3c3a6079cca797c))

# [1.94.0](https://gitlab.com/fi-sas/backoffice/compare/v1.93.5...v1.94.0) (2021-09-06)


### Bug Fixes

* **private_accommodation:** fix problem on configurations ([25e8bfa](https://gitlab.com/fi-sas/backoffice/commit/25e8bfaf3580bd794b07f2d22b4f1684666b177d))


### Features

* **accommodation:** add new application reports ([8b2eb44](https://gitlab.com/fi-sas/backoffice/commit/8b2eb445c85bcd483e765f0690f4608d5143766e))

## [1.93.5](https://gitlab.com/fi-sas/backoffice/compare/v1.93.4...v1.93.5) (2021-09-06)


### Bug Fixes

* **accommodation:** error correction, wrong form ([75c3bc6](https://gitlab.com/fi-sas/backoffice/commit/75c3bc6c83e8dcb93de3f03afb1b8cd712dceada))

## [1.93.4](https://gitlab.com/fi-sas/backoffice/compare/v1.93.3...v1.93.4) (2021-09-06)


### Bug Fixes

* **alimentation:** clear dishes after select type ([3b641a9](https://gitlab.com/fi-sas/backoffice/commit/3b641a9d6581383516972e2873ebb92867962aa4))

## [1.93.3](https://gitlab.com/fi-sas/backoffice/compare/v1.93.2...v1.93.3) (2021-09-03)


### Bug Fixes

* **alimentation:** filter ([f7e393c](https://gitlab.com/fi-sas/backoffice/commit/f7e393c5546a65033a4a710aa552ae6fbd191e1e))

## [1.93.2](https://gitlab.com/fi-sas/backoffice/compare/v1.93.1...v1.93.2) (2021-09-03)


### Bug Fixes

* **alimentation:** remove expiration date required ([bfaf0c1](https://gitlab.com/fi-sas/backoffice/commit/bfaf0c1a647e3ad1441e9860ef713eec3c85f291))

## [1.93.1](https://gitlab.com/fi-sas/backoffice/compare/v1.93.0...v1.93.1) (2021-09-03)


### Bug Fixes

* **accommodation:** fields blocked from profile ([be96b96](https://gitlab.com/fi-sas/backoffice/commit/be96b9676f0c925aa73d4feb7c56ff5703daf7b3))

# [1.93.0](https://gitlab.com/fi-sas/backoffice/compare/v1.92.3...v1.93.0) (2021-09-03)


### Features

* **accommodation:** add revert application to previous status action ([44f0e6c](https://gitlab.com/fi-sas/backoffice/commit/44f0e6c6dc8ceb832b71847a3891d27ce0c3d84f))

## [1.92.3](https://gitlab.com/fi-sas/backoffice/compare/v1.92.2...v1.92.3) (2021-09-02)


### Bug Fixes

* **accommodation:** files form, add per capita ([ce64e31](https://gitlab.com/fi-sas/backoffice/commit/ce64e313726ed5fc179f8e2d730009de4e516109))

## [1.92.2](https://gitlab.com/fi-sas/backoffice/compare/v1.92.1...v1.92.2) (2021-09-02)


### Bug Fixes

* **accommodation:** change misspelled parameter name ([c221feb](https://gitlab.com/fi-sas/backoffice/commit/c221feb2d26009f608ca16a41fc8bf2dc154d635))
* **accommodation:** fix errors found in sprint review ([dddf40c](https://gitlab.com/fi-sas/backoffice/commit/dddf40c4075bed53e1483b0e79afbef68b34940d))
* **applications:** fix error on list applications on submitted status ([ff616a3](https://gitlab.com/fi-sas/backoffice/commit/ff616a333f73cad0f4736c6dfb037f40f9e20504))

## [1.92.1](https://gitlab.com/fi-sas/backoffice/compare/v1.92.0...v1.92.1) (2021-09-01)


### Bug Fixes

* **geral:** correction errors detected in revision ([1cdd9f5](https://gitlab.com/fi-sas/backoffice/commit/1cdd9f55f4570baa1b6d8c3fc2edfd9dd82bfec8))

# [1.92.0](https://gitlab.com/fi-sas/backoffice/compare/v1.91.3...v1.92.0) (2021-09-01)


### Features

* **social-support:** move register interview to dropdown ([0cfdf19](https://gitlab.com/fi-sas/backoffice/commit/0cfdf192cec378e54fa5ef5b0a05add667497ac2))
* **social-support:** register interview from listing ([a605e1c](https://gitlab.com/fi-sas/backoffice/commit/a605e1c86134573978729e09eaa4bcac4a1b8012))
* **social-support:** register interview from manifestations listing ([f66da2f](https://gitlab.com/fi-sas/backoffice/commit/f66da2f9286565943b3cf1a9c6671b1531b92f43))

## [1.91.3](https://gitlab.com/fi-sas/backoffice/compare/v1.91.2...v1.91.3) (2021-09-01)


### Bug Fixes

* **accommodation:** error correction ([e1a1143](https://gitlab.com/fi-sas/backoffice/commit/e1a11432063e62210f3ec6b532ae7ce0b369a0c9))

## [1.91.2](https://gitlab.com/fi-sas/backoffice/compare/v1.91.1...v1.91.2) (2021-09-01)


### Bug Fixes

* **accommodation:** error correction name residenc ([652948f](https://gitlab.com/fi-sas/backoffice/commit/652948fa91e30ad47f0ba10e4243fba3447fa80a))

## [1.91.1](https://gitlab.com/fi-sas/backoffice/compare/v1.91.0...v1.91.1) (2021-08-31)


### Bug Fixes

* **accommodation:** add application phase select on create application ([6f76f77](https://gitlab.com/fi-sas/backoffice/commit/6f76f77d625f65c09b60b68cb0f981ee730b11ff))
* **accommodation:** change application history display ([20a2000](https://gitlab.com/fi-sas/backoffice/commit/20a2000373624d6ac8fdbd22c5756f3d5957fc29))
* **accommodation:** changing the historical field ([9d38d18](https://gitlab.com/fi-sas/backoffice/commit/9d38d18185ca2d720d7663d5dc6186a8813158bf))
* **accommodation:** new settings requested ([d24fd41](https://gitlab.com/fi-sas/backoffice/commit/d24fd415d21377b5c4964d8c70c072140386841e))

# [1.91.0](https://gitlab.com/fi-sas/backoffice/compare/v1.90.0...v1.91.0) (2021-08-27)


### Features

* **accommodation:** create more that one billing by period ([5306210](https://gitlab.com/fi-sas/backoffice/commit/5306210c46d91e43e5dfaecd26cd22bc2e86d300))

# [1.90.0](https://gitlab.com/fi-sas/backoffice/compare/v1.89.5...v1.90.0) (2021-08-27)


### Bug Fixes

* **ub-application-model:** add typology ([94a614c](https://gitlab.com/fi-sas/backoffice/commit/94a614c234647448a2adee91bb1687a5e676f753))
* **ubike-applications-forms:** update module ([57d14cb](https://gitlab.com/fi-sas/backoffice/commit/57d14cbc399bf952565a2ffd7d1b950996ad741b))


### Features

* **ubike:** add create application on menu ([a6ae6b5](https://gitlab.com/fi-sas/backoffice/commit/a6ae6b5336f1e932ef6dd5d693386a22bff36e93))
* **ubike-application-form:** add component for create applications ([38bd658](https://gitlab.com/fi-sas/backoffice/commit/38bd658fdf576d90130afbaf58493160ace554b9))
* **ubike-application-service:** add start and end date applications ([91dbb39](https://gitlab.com/fi-sas/backoffice/commit/91dbb39fa4056c6ea73d319ee7df5cab779c38cd))
* **ubike-applications-form:** add endoint cerate application ([a3ac7d4](https://gitlab.com/fi-sas/backoffice/commit/a3ac7d479edf0a67903c1e50b68106fa354cd94e))

## [1.89.5](https://gitlab.com/fi-sas/backoffice/compare/v1.89.4...v1.89.5) (2021-08-26)


### Bug Fixes

* **alimentation:** valid can be deleted from menu ([fc08003](https://gitlab.com/fi-sas/backoffice/commit/fc0800338750d574d3143ba996876d9aa472b246))

## [1.89.4](https://gitlab.com/fi-sas/backoffice/compare/v1.89.3...v1.89.4) (2021-08-26)


### Bug Fixes

* **alimentation:** correction errors tests ([b08121d](https://gitlab.com/fi-sas/backoffice/commit/b08121d9b3a5110bf63a1fca243a426a68d3345c))

## [1.89.3](https://gitlab.com/fi-sas/backoffice/compare/v1.89.2...v1.89.3) (2021-08-25)


### Bug Fixes

* **notification:** id 4 payments ([210606b](https://gitlab.com/fi-sas/backoffice/commit/210606bcdeb880b5ed862266d1aeb6582193b029))

## [1.89.2](https://gitlab.com/fi-sas/backoffice/compare/v1.89.1...v1.89.2) (2021-08-25)


### Bug Fixes

* **alimentation:** error correction ([866996f](https://gitlab.com/fi-sas/backoffice/commit/866996f55cc693d1bd89bd7ad68860f6cad498f5))

## [1.89.1](https://gitlab.com/fi-sas/backoffice/compare/v1.89.0...v1.89.1) (2021-08-24)


### Bug Fixes

* **alimentation:** error correction ([352db7b](https://gitlab.com/fi-sas/backoffice/commit/352db7bd8a394a6c542053ec51fa693637eafc1e))

# [1.89.0](https://gitlab.com/fi-sas/backoffice/compare/v1.88.3...v1.89.0) (2021-08-24)


### Bug Fixes

* **bus_application:** update busapplicationstatus ([4c246b9](https://gitlab.com/fi-sas/backoffice/commit/4c246b9940f5efa6c6bd56ee58bfe383ae8bf940))
* **bus-sub-23-declarations:** update validation ([3e5d6bd](https://gitlab.com/fi-sas/backoffice/commit/3e5d6bd1d136bf6f1b84ed1535715b70113dfcf7))
* **html-bus-application:** update satus ([0c52b13](https://gitlab.com/fi-sas/backoffice/commit/0c52b13391efaba54d7a17acff22079ed094527d))
* **sub23declaration:** update status ([4b4538e](https://gitlab.com/fi-sas/backoffice/commit/4b4538e6f0f58b533f43d5c1f156e2003b7a4a8f))


### Features

* **app.config:** add endpoints ([c6a0bf5](https://gitlab.com/fi-sas/backoffice/commit/c6a0bf5d410aec37a5530812dd8a5be7186d908d))
* **bus_component:** add configurations on menu ([8424608](https://gitlab.com/fi-sas/backoffice/commit/8424608bd3c278f101783905dea72ed188e9f4fd))
* **configurations_bus:** add page and service ([118c611](https://gitlab.com/fi-sas/backoffice/commit/118c6119feacd87a93d628c829e87599f74cb7ec))
* **configurations_model_ bus:** add model ([158a4de](https://gitlab.com/fi-sas/backoffice/commit/158a4de47f2cd73d2007b97f1ae315e74f8d866a))

## [1.88.3](https://gitlab.com/fi-sas/backoffice/compare/v1.88.2...v1.88.3) (2021-08-23)


### Bug Fixes

* **auth:** get scopes after token refresh ([00c09e3](https://gitlab.com/fi-sas/backoffice/commit/00c09e3e1f7365ace1463c35c0e74d08c80eb51c))

## [1.88.2](https://gitlab.com/fi-sas/backoffice/compare/v1.88.1...v1.88.2) (2021-08-19)


### Bug Fixes

* **accommodation:** add room and typology details ([a820aa7](https://gitlab.com/fi-sas/backoffice/commit/a820aa7b6707c83033af3b569bae6c6156ea275f))

## [1.88.1](https://gitlab.com/fi-sas/backoffice/compare/v1.88.0...v1.88.1) (2021-08-19)


### Bug Fixes

* **current_account:** cash closure ([5f9e0ce](https://gitlab.com/fi-sas/backoffice/commit/5f9e0ce260312b712e1d208c1759561fc8d48e3e))

# [1.88.0](https://gitlab.com/fi-sas/backoffice/compare/v1.87.1...v1.88.0) (2021-08-19)


### Bug Fixes

* **accommodation:** add file_id to application change requests ([d3c1f88](https://gitlab.com/fi-sas/backoffice/commit/d3c1f88846e9c2e899ce9e79c9a0ee9be8e0ef0a))
* **accommodation:** fix pipeline error ([543e6b5](https://gitlab.com/fi-sas/backoffice/commit/543e6b53c294b2d55dc06764e2063702659b2bc1))


### Features

* **accommodation:** add tariffs list and actions ([1c448ce](https://gitlab.com/fi-sas/backoffice/commit/1c448ce42188368076435ed8ca421d729a170c06))
* **accommodation:** change tariff modal and date validations ([6afa883](https://gitlab.com/fi-sas/backoffice/commit/6afa883ce8985bdcde1098e9dc69a37fdc9ae643))
* **tariff-change-request:** list tariff change request ([909311d](https://gitlab.com/fi-sas/backoffice/commit/909311d578413d3d25270c6d103dca2cacd1744b))

## [1.87.1](https://gitlab.com/fi-sas/backoffice/compare/v1.87.0...v1.87.1) (2021-08-19)


### Bug Fixes

* **financial:** account validation empty docTypes middleware ([9e86cc0](https://gitlab.com/fi-sas/backoffice/commit/9e86cc0bf2baad7f8aa1525b4196807879c57888))

# [1.87.0](https://gitlab.com/fi-sas/backoffice/compare/v1.86.4...v1.87.0) (2021-08-18)


### Features

* **financial:** add configuration to account to set docs to send to erp ([f398969](https://gitlab.com/fi-sas/backoffice/commit/f39896940e678f986d5c0fa84f327fe9648687dd))

## [1.86.4](https://gitlab.com/fi-sas/backoffice/compare/v1.86.3...v1.86.4) (2021-08-18)


### Bug Fixes

* **accommodation:** table columns configuration ([f184cb3](https://gitlab.com/fi-sas/backoffice/commit/f184cb361650eb51fedf7c33e785710e0336f6e9))

## [1.86.3](https://gitlab.com/fi-sas/backoffice/compare/v1.86.2...v1.86.3) (2021-08-17)


### Bug Fixes

* **accommodation:** link panel ([39540d6](https://gitlab.com/fi-sas/backoffice/commit/39540d6cc9e5a6cfd13b2a879659d099927fd06d))

## [1.86.2](https://gitlab.com/fi-sas/backoffice/compare/v1.86.1...v1.86.2) (2021-08-17)


### Bug Fixes

* **monitoring:** standardization rules ([bac4966](https://gitlab.com/fi-sas/backoffice/commit/bac49666e7624b03d9396ea6b3d165e80e89473c))

## [1.86.1](https://gitlab.com/fi-sas/backoffice/compare/v1.86.0...v1.86.1) (2021-08-16)


### Bug Fixes

* **voluntering:** rules, error correction ([6349ecf](https://gitlab.com/fi-sas/backoffice/commit/6349ecfedd8285d503fef93b81d071fd59e2af59))

# [1.86.0](https://gitlab.com/fi-sas/backoffice/compare/v1.85.1...v1.86.0) (2021-08-12)


### Features

* **list-applicatons-ubike:** add update bike disponibility ([0b06544](https://gitlab.com/fi-sas/backoffice/commit/0b06544ffdaf9ad75e53afb257e2e98966c0eae2))

## [1.85.1](https://gitlab.com/fi-sas/backoffice/compare/v1.85.0...v1.85.1) (2021-08-11)


### Bug Fixes

* **ubike-list-applications:** remove sort bike_id ([e091b87](https://gitlab.com/fi-sas/backoffice/commit/e091b87f3b54af98ba69a56aea1d819dc08cfb3d))

# [1.85.0](https://gitlab.com/fi-sas/backoffice/compare/v1.84.0...v1.85.0) (2021-08-11)


### Bug Fixes

* **ubike-report-service:** bike_id to getstatus, refactor endpoint print ([aace0bc](https://gitlab.com/fi-sas/backoffice/commit/aace0bc5adf281fe81a55d107dbbc6b885976a94))
* **ubike-service-bike:** update list bikes endpoint ([f12af1a](https://gitlab.com/fi-sas/backoffice/commit/f12af1a0e9ad938e48b24e9a6db0c478b6e7cb8a))
* **view-applications-bike:** code ([f4182d1](https://gitlab.com/fi-sas/backoffice/commit/f4182d12ec464258c4730b45e42c6560b4e8f476))


### Features

* **app.configs:** add endpoint ([a8942e9](https://gitlab.com/fi-sas/backoffice/commit/a8942e93d670893feefb661d2455098610844298))
* **view-stats-ubike:** add bikeid search and fix endpoint print ([bd1b6af](https://gitlab.com/fi-sas/backoffice/commit/bd1b6af9e07155d8c5b92d50a15caa9ad48b8bfc))
* **view-stats-ubike:** add select box bikes ([c1a9881](https://gitlab.com/fi-sas/backoffice/commit/c1a988129cc86f3555e1b5d92830e3e1f8fa4c2d))

# [1.84.0](https://gitlab.com/fi-sas/backoffice/compare/v1.83.0...v1.84.0) (2021-08-11)


### Features

* **cc:** add url and errors into movements list ([3ba95ca](https://gitlab.com/fi-sas/backoffice/commit/3ba95ca1aee54f8cccf1f4dad70f21967e105456))

# [1.83.0](https://gitlab.com/fi-sas/backoffice/compare/v1.82.0...v1.83.0) (2021-08-10)


### Features

* **app.config:** add endpoints ([1f89d1b](https://gitlab.com/fi-sas/backoffice/commit/1f89d1b94b582fc569302569d2be53ae379b9abe))
* **list-applications-ubike:** add withrelated ([a895d56](https://gitlab.com/fi-sas/backoffice/commit/a895d56f7362b1ee6bbfa9a6f2b1a896c68bd60e))
* **model-application-ubike:** add field ([bb8ce21](https://gitlab.com/fi-sas/backoffice/commit/bb8ce21694762ebd9d57ed63b324bc50e7f6ace9))
* **service-ubike-applications:** add servicer for documents ([271131a](https://gitlab.com/fi-sas/backoffice/commit/271131add532975e7429097a5751d26da9d87472))
* **view-application-ubike:** add tab documents and content ([f96d35c](https://gitlab.com/fi-sas/backoffice/commit/f96d35cd338b63e99ad087aa890626b08644ab4f))
* **view-application-ubike-css:** add classes ([2de2ab2](https://gitlab.com/fi-sas/backoffice/commit/2de2ab2a88a301d016e603d25a03c26a50320998))
* **view-applications-ubike-component:** update componet for documents ([85e0a85](https://gitlab.com/fi-sas/backoffice/commit/85e0a85f24f5e9b25287a765ab4446d0af0d615c))

# [1.82.0](https://gitlab.com/fi-sas/backoffice/compare/v1.81.1...v1.82.0) (2021-08-09)


### Bug Fixes

* **accommdation:** add contract changes tab to application view ([f55ae67](https://gitlab.com/fi-sas/backoffice/commit/f55ae6712e1596562fef795b3067f04cc9cde985))
* **accommodation:** change update application without change requests ([0dabb7f](https://gitlab.com/fi-sas/backoffice/commit/0dabb7f70663b8f469efb02260c7a453b5c8a2bd))
* **accommodation:** fix build errors ([c04f8d7](https://gitlab.com/fi-sas/backoffice/commit/c04f8d7a79ecc94c3e2151c43b4230bd171a72f1))


### Features

* **accommodation:** add date to reprocess billings prices ([92f46fd](https://gitlab.com/fi-sas/backoffice/commit/92f46fdb2f3a0d978fe56ae053422d966d538d82))
* **accommodation:** application regime change requests ([83a565c](https://gitlab.com/fi-sas/backoffice/commit/83a565ce2d0040c4e2dacfff6f729d5d706f9601))
* **accommodation:** change extras requests ([688f5e1](https://gitlab.com/fi-sas/backoffice/commit/688f5e11ca8e7b150065ece81c0d105c4f060545))
* **accommodation:** change typology request ([69f80b5](https://gitlab.com/fi-sas/backoffice/commit/69f80b5ab2ad7295909f59253454c35a3a3951cd))
* **accommodation:** maintenance requests ([40a8cb7](https://gitlab.com/fi-sas/backoffice/commit/40a8cb70fe00b4a83155edc1cc23e9dfd039daa0))
* **various:** premute room and  residencechange requests ([b211600](https://gitlab.com/fi-sas/backoffice/commit/b211600fc70902c6452e46caefebdf94f6986121))

## [1.81.1](https://gitlab.com/fi-sas/backoffice/compare/v1.81.0...v1.81.1) (2021-08-09)


### Bug Fixes

* **ubike-form-bike:** error with assets ([f14c378](https://gitlab.com/fi-sas/backoffice/commit/f14c37838e86b7f7460ed88093e660395c9b052a))
* **ubike-list-applications-modal:** add validation open modal ([4740af3](https://gitlab.com/fi-sas/backoffice/commit/4740af3f0de982bb603fc829767b11aa6831fe75))

# [1.81.0](https://gitlab.com/fi-sas/backoffice/compare/v1.80.3...v1.81.0) (2021-08-05)


### Features

* **private-accommodation-ccs-form:** add new class ([7b3e18f](https://gitlab.com/fi-sas/backoffice/commit/7b3e18f045439b730d7cb7ad372897fe6960de0d))
* **private-accommodation-listing-form:** add message min one language ([c8f0ae2](https://gitlab.com/fi-sas/backoffice/commit/c8f0ae20af0ab8e053e609a8b6340f75fc50e902))
* **private-accomodation-form-step-one:** room translation required ([b245b48](https://gitlab.com/fi-sas/backoffice/commit/b245b4855ebf3da86896936a8a9e260f5c146a91))

## [1.80.3](https://gitlab.com/fi-sas/backoffice/compare/v1.80.2...v1.80.3) (2021-08-05)


### Bug Fixes

* **current-account:** chabnge type to number ([a44fd2a](https://gitlab.com/fi-sas/backoffice/commit/a44fd2a2746c8da9a91b3f94af946c34b5f7859f))

## [1.80.2](https://gitlab.com/fi-sas/backoffice/compare/v1.80.1...v1.80.2) (2021-08-05)


### Bug Fixes

* **alimentation:** minor fixs ([1562ede](https://gitlab.com/fi-sas/backoffice/commit/1562ede14b3bfd1ff8e1f95d46532900830b6f0c))

## [1.80.1](https://gitlab.com/fi-sas/backoffice/compare/v1.80.0...v1.80.1) (2021-08-04)


### Bug Fixes

* **private-accommodation-complain:** remove list all users ([e92e6f1](https://gitlab.com/fi-sas/backoffice/commit/e92e6f1e5dfc9a67e8817e6f3fdc5703ef384e77))
* **private-accommodation-pa-configurations:** get all user groups ([e52afbb](https://gitlab.com/fi-sas/backoffice/commit/e52afbb7ee95afc292cb152fd2a62aff68745bf6))

# [1.80.0](https://gitlab.com/fi-sas/backoffice/compare/v1.79.2...v1.80.0) (2021-08-03)


### Bug Fixes

* **accommodation:** small fix ([470dfb2](https://gitlab.com/fi-sas/backoffice/commit/470dfb21e677d6f70acd43c6e7969d2e87a13005))


### Features

* **accommocation:** rebuild acccommodation billing components ([9b3d723](https://gitlab.com/fi-sas/backoffice/commit/9b3d723a2dad17e31321380ddd9fef8137bcada7))

## [1.79.2](https://gitlab.com/fi-sas/backoffice/compare/v1.79.1...v1.79.2) (2021-08-02)


### Bug Fixes

* **accommodation:** change withdrawals state machines ([118c6c6](https://gitlab.com/fi-sas/backoffice/commit/118c6c6e99f13a3e2f7f63ba88a63d940fb2c60e))
* **accommodation:** extensions state machine ([fb1c1a6](https://gitlab.com/fi-sas/backoffice/commit/fb1c1a6381d80d0be5283ea9058ff37aeb4c9fd9))

## [1.79.1](https://gitlab.com/fi-sas/backoffice/compare/v1.79.0...v1.79.1) (2021-07-30)


### Bug Fixes

* **social_support:** normalization of rules ([b9fd388](https://gitlab.com/fi-sas/backoffice/commit/b9fd388898b4fbe2aa637f92dd7ebe63b3f46ef2))

# [1.79.0](https://gitlab.com/fi-sas/backoffice/compare/v1.78.2...v1.79.0) (2021-07-30)


### Features

* **social-support:** normalize names ([75618e3](https://gitlab.com/fi-sas/backoffice/commit/75618e326f27daf7e7f85a26e8f5bf0010e89f6b))
* **volunteering:** normalize names ([e954a2e](https://gitlab.com/fi-sas/backoffice/commit/e954a2e3b6104f9ec6ce8019035cac5682ccbed2))

## [1.78.2](https://gitlab.com/fi-sas/backoffice/compare/v1.78.1...v1.78.2) (2021-07-29)


### Bug Fixes

* **ubike:** standardization of rules ([e655895](https://gitlab.com/fi-sas/backoffice/commit/e655895a7001b2991b74574434e2552e325c0487))

## [1.78.1](https://gitlab.com/fi-sas/backoffice/compare/v1.78.0...v1.78.1) (2021-07-28)


### Bug Fixes

* **bus:** correction id error ([e80a2e9](https://gitlab.com/fi-sas/backoffice/commit/e80a2e96971622962ba2c2a3737e5c345e471f49))
* **private_accomodation:** correction typology ([288449e](https://gitlab.com/fi-sas/backoffice/commit/288449ecfcaf83bb65216b2acec581809d0512a4))
* **users:** correction color button ([b455a98](https://gitlab.com/fi-sas/backoffice/commit/b455a98735114ca752d4e27afe2515dd93f6e80b))

# [1.78.0](https://gitlab.com/fi-sas/backoffice/compare/v1.77.0...v1.78.0) (2021-07-28)


### Features

* **social-support:** enable external profile configuration ([a21e998](https://gitlab.com/fi-sas/backoffice/commit/a21e9988aaf3c05949a24b9952dc20af05fdee25))
* **social-support:** normalize manifest interest state names ([4226fa3](https://gitlab.com/fi-sas/backoffice/commit/4226fa3f7deefeae8ae5a00030ea865c15ee998d))
* **social-support:** normalize manifest interest state names ([b86131e](https://gitlab.com/fi-sas/backoffice/commit/b86131e4214afd70102c7ead1026e7e98c9f8b05))
* **volunteering:** enable external profile configuration ([dad3829](https://gitlab.com/fi-sas/backoffice/commit/dad3829c6720381f17901ed4597637baca21a7cb))
* **volunteering:** normalize manifest interest state names ([ab4ad8e](https://gitlab.com/fi-sas/backoffice/commit/ab4ad8e824b0d52350cfbd1a97f97259633f5c4e))
* **volunteering:** normalize states names ([233ff0e](https://gitlab.com/fi-sas/backoffice/commit/233ff0e209d5636cd458a8e959d25cfb7ccf267a))

# [1.77.0](https://gitlab.com/fi-sas/backoffice/compare/v1.76.1...v1.77.0) (2021-07-27)


### Bug Fixes

* **accommodaition:** add tariff option to contract popup ([f07a9bf](https://gitlab.com/fi-sas/backoffice/commit/f07a9bf8bf44a108786892cc9cc1a53b4eadea3b))
* **accommodation:** add current_account_id to residences ([7687e25](https://gitlab.com/fi-sas/backoffice/commit/7687e25e53c658918e66bcf8f49a1f1edc1b8e14))
* **accommodation:** change wrong merge conflit resolve ([f99756a](https://gitlab.com/fi-sas/backoffice/commit/f99756a839417cc55eddf354ee3b540a3bc59528))
* **accommodation:** fix misplaced buttons ([e8f56d3](https://gitlab.com/fi-sas/backoffice/commit/e8f56d34a403b4a3c8bb936a45753723c8d157fe))
* **alimentation:** correction of list of dishes ([15f2423](https://gitlab.com/fi-sas/backoffice/commit/15f24233ec2aacde1fdaec01552599360fcbc378))
* **various:** add tariff to regimes and typologies ([36bb9f5](https://gitlab.com/fi-sas/backoffice/commit/36bb9f5b2d26fdcb5a6a9228fd1c2d0a9faf242a))


### Features

* **accommodation:** add due date configurations params ([5287467](https://gitlab.com/fi-sas/backoffice/commit/528746784ef12a97893587acacc0c9371df9ffed))

## [1.76.1](https://gitlab.com/fi-sas/backoffice/compare/v1.76.0...v1.76.1) (2021-07-27)


### Bug Fixes

* **private_accommodation:** normalization rules ([87265a1](https://gitlab.com/fi-sas/backoffice/commit/87265a17a16f0976efb44e02be52bd18b90c5fc9))

# [1.76.0](https://gitlab.com/fi-sas/backoffice/compare/v1.75.0...v1.76.0) (2021-07-27)


### Features

* **configuration:** add CPUID field to devices ([7323b2c](https://gitlab.com/fi-sas/backoffice/commit/7323b2c580f88f174ff91259fadee6c5709851c7))

# [1.75.0](https://gitlab.com/fi-sas/backoffice/compare/v1.74.9...v1.75.0) (2021-07-27)


### Bug Fixes

* **social-support-form-experience:** remove current_account ([6cb1413](https://gitlab.com/fi-sas/backoffice/commit/6cb14131f6cc3aeadb1f32db5d22b0cda20fc529))


### Features

* **configs:** add endpoint config current_account ([a88fc1a](https://gitlab.com/fi-sas/backoffice/commit/a88fc1a0411f3aad249253484f24528018d777d2))
* **social-support-configurations:** add current_account config ([2e7fad4](https://gitlab.com/fi-sas/backoffice/commit/2e7fad4a160a6e4a30079cbbc6bdf2af21ee666f))

## [1.74.9](https://gitlab.com/fi-sas/backoffice/compare/v1.74.8...v1.74.9) (2021-07-26)


### Bug Fixes

* **bus:** standardization of rules ([e2ac15b](https://gitlab.com/fi-sas/backoffice/commit/e2ac15bcf8598e942dce4101b0e4415b3a2a2dbb))

## [1.74.8](https://gitlab.com/fi-sas/backoffice/compare/v1.74.7...v1.74.8) (2021-07-26)


### Bug Fixes

* **experiences-social-scholarchip:** adicionar filtro unidade organica ([b5f740e](https://gitlab.com/fi-sas/backoffice/commit/b5f740e43b6c60dd6d0f6eb81c66bbc7ea33d274))

## [1.74.7](https://gitlab.com/fi-sas/backoffice/compare/v1.74.6...v1.74.7) (2021-07-23)


### Bug Fixes

* **payments:** remove filter from list ([977c71d](https://gitlab.com/fi-sas/backoffice/commit/977c71d6330d5388cd533765968846f4e1fd8ea8))

## [1.74.6](https://gitlab.com/fi-sas/backoffice/compare/v1.74.5...v1.74.6) (2021-07-23)


### Bug Fixes

* **form-link:** add message ([a4b2690](https://gitlab.com/fi-sas/backoffice/commit/a4b2690219569d3face7571f2075b19bd410b966))
* **form-link:** fix problem on create link ([d6512a7](https://gitlab.com/fi-sas/backoffice/commit/d6512a72b7b7d431d0b1774e0b5976837503cef6))
* **form-zone:** correção de erro ao criar zona ([309452d](https://gitlab.com/fi-sas/backoffice/commit/309452d5377d877e12d004052137ac5d8f898db3))
* **list-links:** fix delete ([e387934](https://gitlab.com/fi-sas/backoffice/commit/e38793454568bdc150119bb7df46d57ebaabbcc7))
* **reports:** standardization rules ([4e48aad](https://gitlab.com/fi-sas/backoffice/commit/4e48aadf00bbaceb1d6b985d99ee003854c045b9))

## [1.74.5](https://gitlab.com/fi-sas/backoffice/compare/v1.74.4...v1.74.5) (2021-07-23)


### Bug Fixes

* **users:** order list ([3f52bf9](https://gitlab.com/fi-sas/backoffice/commit/3f52bf9f88adca092872b5e640ca8e9f6ffc96f2))
* **users:** standardization rules ([43a892f](https://gitlab.com/fi-sas/backoffice/commit/43a892f9cf0366e31bb77fa00eff692a783c76f9))

## [1.74.4](https://gitlab.com/fi-sas/backoffice/compare/v1.74.3...v1.74.4) (2021-07-22)


### Bug Fixes

* **notification:** standardization rules ([7a620d1](https://gitlab.com/fi-sas/backoffice/commit/7a620d1588c11ac3193379111d483f1504cebfb6))

## [1.74.3](https://gitlab.com/fi-sas/backoffice/compare/v1.74.2...v1.74.3) (2021-07-22)


### Bug Fixes

* **notification:** deleting the last notification ([e0e63f7](https://gitlab.com/fi-sas/backoffice/commit/e0e63f72b6328b964c36f96491206c965e7bf32b))

## [1.74.2](https://gitlab.com/fi-sas/backoffice/compare/v1.74.1...v1.74.2) (2021-07-21)


### Bug Fixes

* **comunnication:** wp & mobile video validation ([445e931](https://gitlab.com/fi-sas/backoffice/commit/445e931c4cabb3b585f3d700e499eff1405a60a6))

## [1.74.1](https://gitlab.com/fi-sas/backoffice/compare/v1.74.0...v1.74.1) (2021-07-21)


### Bug Fixes

* **current_account:** permissions ([285ed06](https://gitlab.com/fi-sas/backoffice/commit/285ed06c4db41566a5c43e3b8e263eb1b6ba2a7e))

# [1.74.0](https://gitlab.com/fi-sas/backoffice/compare/v1.73.2...v1.74.0) (2021-07-21)


### Bug Fixes

* **component.ts:** update code and add functions ([9dcb091](https://gitlab.com/fi-sas/backoffice/commit/9dcb091780bbd0998ec867455ba7378a88ed7131))
* **dashboard-component:** add dashboard component ([060ef83](https://gitlab.com/fi-sas/backoffice/commit/060ef83a0612e39bec5ce85b36c9222f4fb0774d))
* **dashboardpage:** update html ([9832af7](https://gitlab.com/fi-sas/backoffice/commit/9832af7e0930959af3688819997b4073d714a8ab))
* **monitoring:** update module ([e051ee3](https://gitlab.com/fi-sas/backoffice/commit/e051ee31bc758d82232dc79f8290d1880315ad3c))


### Features

* **app:** add endpoit dashboard ([3246c55](https://gitlab.com/fi-sas/backoffice/commit/3246c558bb6b742ea51d8edaf5c82fb0b462653d))
* **app.config:** add endpoints ([c256a45](https://gitlab.com/fi-sas/backoffice/commit/c256a45786874746ef56275910f122e725aabbc6))
* **appconfig:** add endpoint ([59e27d6](https://gitlab.com/fi-sas/backoffice/commit/59e27d6207b0b80d0ec6e45b254f6fcbdc9638d5))
* **dashboard-monitoring:** update dashboard ([82f1384](https://gitlab.com/fi-sas/backoffice/commit/82f13844c352faeb1e3366928e64ec73a6137570))
* **dashboard-service:** add service ([404784f](https://gitlab.com/fi-sas/backoffice/commit/404784f5df4b37e81d71c09fc2ce113bb073ab3d))
* **dashboardless:** add classes ([9a690e8](https://gitlab.com/fi-sas/backoffice/commit/9a690e858f3ec56ed50f8d7f07b908505320a367))
* **dashboardservice:** add services ([115e4a8](https://gitlab.com/fi-sas/backoffice/commit/115e4a8965dc99359042faec6bf6880cf24b63e9))

## [1.73.2](https://gitlab.com/fi-sas/backoffice/compare/v1.73.1...v1.73.2) (2021-07-21)


### Bug Fixes

* **alimentation:** name error correction on menus ([6aa298c](https://gitlab.com/fi-sas/backoffice/commit/6aa298c20470fa880b50fd8d1226117a298c38b5))

## [1.73.1](https://gitlab.com/fi-sas/backoffice/compare/v1.73.0...v1.73.1) (2021-07-20)


### Bug Fixes

* **current_account:** standardization of rules ([929fa5a](https://gitlab.com/fi-sas/backoffice/commit/929fa5a33c35c0000ef1f67c8c8b1090d1911654))

# [1.73.0](https://gitlab.com/fi-sas/backoffice/compare/v1.72.3...v1.73.0) (2021-07-20)


### Features

* **volunteering:** changed incorrect concepts ([6d38c5d](https://gitlab.com/fi-sas/backoffice/commit/6d38c5d8892192caef5b6f6e02d2636eae1b4f6e))

## [1.72.3](https://gitlab.com/fi-sas/backoffice/compare/v1.72.2...v1.72.3) (2021-07-19)


### Bug Fixes

* **accommodation:** renewal of pages ([4858521](https://gitlab.com/fi-sas/backoffice/commit/48585212b34e2deb418f795465fe1c7a0b455eaf))

## [1.72.2](https://gitlab.com/fi-sas/backoffice/compare/v1.72.1...v1.72.2) (2021-07-19)


### Bug Fixes

* **volunteering-experiences:** fix order language error and error update ([d9c7a86](https://gitlab.com/fi-sas/backoffice/commit/d9c7a86d25c8af050f42d84af85468c4b32a88ff))

## [1.72.1](https://gitlab.com/fi-sas/backoffice/compare/v1.72.0...v1.72.1) (2021-07-16)


### Bug Fixes

* **geral:** changing the style of table titles ([e7c8cb4](https://gitlab.com/fi-sas/backoffice/commit/e7c8cb44ad3f3a4b0a1d4b88e43d7bd18bc61817))

# [1.72.0](https://gitlab.com/fi-sas/backoffice/compare/v1.71.0...v1.72.0) (2021-07-16)


### Bug Fixes

* **infrastructure:** remove comments ([9a66306](https://gitlab.com/fi-sas/backoffice/commit/9a66306eb79406b6839401b2520b8e3ef3da4550))


### Features

* **infrastructure:** add address flieds to organic units ([bd06bea](https://gitlab.com/fi-sas/backoffice/commit/bd06bea50f41a2a14d75e6b9f5ece726b4b402e2))

# [1.71.0](https://gitlab.com/fi-sas/backoffice/compare/v1.70.11...v1.71.0) (2021-07-14)


### Features

* **core:** change html lang to pt ([f557ae8](https://gitlab.com/fi-sas/backoffice/commit/f557ae82c16f6bd8ff53c8d9022aa4699cc8b0c3))

## [1.70.11](https://gitlab.com/fi-sas/backoffice/compare/v1.70.10...v1.70.11) (2021-07-13)


### Bug Fixes

* **alimentation:** filter reservations by meal ([4e989c6](https://gitlab.com/fi-sas/backoffice/commit/4e989c6883d0d3721e594b10a669565b2b2d26f2))

## [1.70.10](https://gitlab.com/fi-sas/backoffice/compare/v1.70.9...v1.70.10) (2021-07-13)


### Bug Fixes

* **social-scholarship:** add missing scopes ([45a622c](https://gitlab.com/fi-sas/backoffice/commit/45a622c06aab5db61d2a215c15208e9d8d927e8e))
* **social-scholarship:** fix scope ([9c8d330](https://gitlab.com/fi-sas/backoffice/commit/9c8d3301bc38244aed5b6df5ceedf6e62853d2f7))

## [1.70.9](https://gitlab.com/fi-sas/backoffice/compare/v1.70.8...v1.70.9) (2021-07-13)


### Bug Fixes

* **measurement-units:** rename loading ([bf650b2](https://gitlab.com/fi-sas/backoffice/commit/bf650b2c3524cfbf6fc944912bcdc7d48ff0c5a8))
* **measurement-units:** rename loading ([9dfc12a](https://gitlab.com/fi-sas/backoffice/commit/9dfc12ae30e3501961a76f98d5a75a2bc34c5c03))
* **ubike-list-applications:** rename total to totalData ([e7b905d](https://gitlab.com/fi-sas/backoffice/commit/e7b905d9ae42472c4ff7f781ac3c82e9cbe4d9e7))

## [1.70.8](https://gitlab.com/fi-sas/backoffice/compare/v1.70.7...v1.70.8) (2021-07-12)


### Bug Fixes

* **cookie:** add expire date of 1 year to cookie acceptance ([1f7bfcc](https://gitlab.com/fi-sas/backoffice/commit/1f7bfcc007ad7e93b02a4f44d71a0e711fb78e73))

## [1.70.7](https://gitlab.com/fi-sas/backoffice/compare/v1.70.6...v1.70.7) (2021-07-12)


### Bug Fixes

* **accommodation:** extra error correction ([9427c87](https://gitlab.com/fi-sas/backoffice/commit/9427c872e197fbde75e60b7d7a1b2cdf6bf68d24))

## [1.70.6](https://gitlab.com/fi-sas/backoffice/compare/v1.70.5...v1.70.6) (2021-07-12)


### Bug Fixes

* **accommodation:** raw report now accepts all residences ([3b6fd2c](https://gitlab.com/fi-sas/backoffice/commit/3b6fd2c4a5decacfa2c6864d864d313188353cc5))

## [1.70.5](https://gitlab.com/fi-sas/backoffice/compare/v1.70.4...v1.70.5) (2021-07-09)


### Bug Fixes

* **ubike:** identification filter add ([b8c9e5e](https://gitlab.com/fi-sas/backoffice/commit/b8c9e5ea3fac4c3fe1be26ddb0b3026c58e0caf5))

## [1.70.4](https://gitlab.com/fi-sas/backoffice/compare/v1.70.3...v1.70.4) (2021-07-08)


### Bug Fixes

* **u_bike:** fix list applications filters ([93c6eec](https://gitlab.com/fi-sas/backoffice/commit/93c6eec731b0a22fe824de18dd05ff576d95f2db))
* **ubike:** remove unsed components ([3c63b6f](https://gitlab.com/fi-sas/backoffice/commit/3c63b6ff1c1a3e6664c8963aeb1e1d34d38e91ce))

## [1.70.3](https://gitlab.com/fi-sas/backoffice/compare/v1.70.2...v1.70.3) (2021-07-08)


### Bug Fixes

* **current_account:** remove repeated service ([ac6f0c4](https://gitlab.com/fi-sas/backoffice/commit/ac6f0c4d89aab1ef762bbdd4e236a61a87f872c6))

## [1.70.2](https://gitlab.com/fi-sas/backoffice/compare/v1.70.1...v1.70.2) (2021-07-08)


### Bug Fixes

* **accommodation:** start of standardization rules ([274ad59](https://gitlab.com/fi-sas/backoffice/commit/274ad596aa631974a612d023d2e7a682ceeec361))
* **geral:** add ordering to table ([ca63d79](https://gitlab.com/fi-sas/backoffice/commit/ca63d79212ca0d6c9aa57372b2dc5b289ed6ff7c))

## [1.70.1](https://gitlab.com/fi-sas/backoffice/compare/v1.70.0...v1.70.1) (2021-07-07)


### Bug Fixes

* **infrastructures:** add delete option ([213c985](https://gitlab.com/fi-sas/backoffice/commit/213c98589e07d7939febe7819723101ad85f79d2))
* **user:** add fields to view user information ([3cf8c26](https://gitlab.com/fi-sas/backoffice/commit/3cf8c26d29d70737edba514041f733901ff98393))

# [1.70.0](https://gitlab.com/fi-sas/backoffice/compare/v1.69.0...v1.70.0) (2021-07-07)


### Features

* **bus:** add absence requests to suspend application ([1e9d7eb](https://gitlab.com/fi-sas/backoffice/commit/1e9d7eb0c34e9f9203aef28c25d2bd4288a08ab2))

# [1.69.0](https://gitlab.com/fi-sas/backoffice/compare/v1.68.1...v1.69.0) (2021-07-07)


### Features

* **volunteering:** fixed filters layout ([0f62743](https://gitlab.com/fi-sas/backoffice/commit/0f627434dd2a43c43d7c90da8695f539c27106a7))

## [1.68.1](https://gitlab.com/fi-sas/backoffice/compare/v1.68.0...v1.68.1) (2021-07-07)


### Bug Fixes

* **communication:** clear recipient count variable ([daa8c2a](https://gitlab.com/fi-sas/backoffice/commit/daa8c2ae8ddcfbc613cdfcd51eb2466ad00680fc))

# [1.68.0](https://gitlab.com/fi-sas/backoffice/compare/v1.67.1...v1.68.0) (2021-07-07)


### Features

* **current_account:** max and min method page ([6f4ab1a](https://gitlab.com/fi-sas/backoffice/commit/6f4ab1ae87378b23a18ea1dd9bd1ab8969835e03))
* **current_account:** new max and min part ([29b5f4c](https://gitlab.com/fi-sas/backoffice/commit/29b5f4c7ed27bac9b36203b6b6feadf597738541))

## [1.67.1](https://gitlab.com/fi-sas/backoffice/compare/v1.67.0...v1.67.1) (2021-07-06)


### Bug Fixes

* **queue:** standardization of pages ([f8cf052](https://gitlab.com/fi-sas/backoffice/commit/f8cf052114b5f8336c642d4f8290bd9693b7b9a1))

# [1.67.0](https://gitlab.com/fi-sas/backoffice/compare/v1.66.2...v1.67.0) (2021-07-06)


### Bug Fixes

* **app_routing_module:** update modules ([a3d65f6](https://gitlab.com/fi-sas/backoffice/commit/a3d65f67532985fb0efb57115763068eb0d0660f))
* **configurations:** standardization of pages ([1eafd76](https://gitlab.com/fi-sas/backoffice/commit/1eafd76e86f8061dfe706d9d1c1f1f840e9a6eb1))
* **environments:** add url mesaurements_monitorization_api_url ([122a549](https://gitlab.com/fi-sas/backoffice/commit/122a54943090eb7332ec9e5c368ec03233c3a4f7))
* **environments:** fix endpoint monitoring ([cf59e0c](https://gitlab.com/fi-sas/backoffice/commit/cf59e0ccd8e7d51b5b1fb646d04bdbde6bc87b0f))
* **equipment_model:** update model ([9d33078](https://gitlab.com/fi-sas/backoffice/commit/9d330783b48930c5e78e631014c2cef4b6dc7a99))
* **form-equipments-monitoring:** update layout ([8f4003f](https://gitlab.com/fi-sas/backoffice/commit/8f4003f1671c4245731f6e0c3ff79316afe4cae3))
* **form-group-equipment-monitoring:** update layout ([49175db](https://gitlab.com/fi-sas/backoffice/commit/49175db0acebd9199e2de2e3aceaf65b3a2de16d))
* **form-sensor-monitoring:** update layout ([028d8db](https://gitlab.com/fi-sas/backoffice/commit/028d8db365bc8c038b04363aaf95fd3af4fcf809))
* **form-units-monitoring:**  update layout ([2492da1](https://gitlab.com/fi-sas/backoffice/commit/2492da144ef242da95a8b03f123906bbaf4cff7e))
* **general-config-modbus:**  update layout ([94ef815](https://gitlab.com/fi-sas/backoffice/commit/94ef8155f8646b153c1dbe85e8be9d238667c9ce))
* **list_group_equipment:** small fix ([66bf300](https://gitlab.com/fi-sas/backoffice/commit/66bf300502b4cc76c7fe420bb3ea7490efbc49f5))
* **list-equipements-monitoring:**  update layout ([d732180](https://gitlab.com/fi-sas/backoffice/commit/d732180107c0f7250fcd3c54ad73c12b1847ff7b))
* **list-group-equipements-monitoring:**  update layout ([a8cbe06](https://gitlab.com/fi-sas/backoffice/commit/a8cbe06a3749270d0cb9b793227f0461a13da58f))
* **list-sensor-monitoring:**  update layout ([b8800ae](https://gitlab.com/fi-sas/backoffice/commit/b8800ae127239683ed99b52d1cadb069f7159acb))
* **list-units-monitoring:** update layout ([e2aa309](https://gitlab.com/fi-sas/backoffice/commit/e2aa3097fb3c7cf35e4fe60366483f0753fef0c2))
* **monitoring_module:** update module ([b4ec66f](https://gitlab.com/fi-sas/backoffice/commit/b4ec66f8bae99c0baa2bc174033c2cf85a7aa157))
* **monitoring-component:** add new tab menu ([be891bd](https://gitlab.com/fi-sas/backoffice/commit/be891bd1ae9d793719b8c9a73aacf3d9c1b03386))
* **monitoring-form-equipments:** update form ([4bdc865](https://gitlab.com/fi-sas/backoffice/commit/4bdc86583108d3e47e05cbf2f0f7ef6c0c249b45))
* **monitoring-form-group:** update form group equipment ([a61b7d3](https://gitlab.com/fi-sas/backoffice/commit/a61b7d3a035be004386abc20feb51ae911021155))
* **monitoring-form-sensor:** update form ([633b7ec](https://gitlab.com/fi-sas/backoffice/commit/633b7ec7084a686ac034123f73ff45cc12e12628))
* **monitoring-form-units:** update form ([b4a871e](https://gitlab.com/fi-sas/backoffice/commit/b4a871e93f842e5f4029084e72a0c81c87175006))
* **monitoring-general-config:** update form ([e3a791e](https://gitlab.com/fi-sas/backoffice/commit/e3a791e95639268c0aeb7879ee553ad33e97d867))
* **monitoring-list-equipments:** update list ([d504d00](https://gitlab.com/fi-sas/backoffice/commit/d504d001072aac84ba5218bd9145f4b196d93e72))
* **monitoring-routing:** fix names ([429780e](https://gitlab.com/fi-sas/backoffice/commit/429780ea77833237ddbb7fe8a5e8eb77f3a10fc3))
* **package:** update node modules ([c403677](https://gitlab.com/fi-sas/backoffice/commit/c40367729df267ab132ff80fe1950be5125f80ca))


### Features

* **app.config:** add endpoints ([7a290ca](https://gitlab.com/fi-sas/backoffice/commit/7a290ca27ba28d3f289a143455820afc70ab0b9b))
* **configs:** add endpoints monitoring ([7257193](https://gitlab.com/fi-sas/backoffice/commit/7257193b2a47695fd46865826ba8f387ef561c34))
* **equipment_group:** add services ([59f5cf2](https://gitlab.com/fi-sas/backoffice/commit/59f5cf24dba571d47f9b75d8c6e0e3d670cc8e1b))
* **equipments:** add service ([4b83f62](https://gitlab.com/fi-sas/backoffice/commit/4b83f62ee5422c55fd79052922807c33fd8dc81c))
* **form_group_equipment:** add form group equipments ([49db82a](https://gitlab.com/fi-sas/backoffice/commit/49db82aa2a9791196e116a1e8fb729bf77c1004d))
* **form_sensor:** add component ([dd7ffb5](https://gitlab.com/fi-sas/backoffice/commit/dd7ffb500548f12eeb082f148b0ced3b7948b8be))
* **form_unit:** add component ([2fb6b3d](https://gitlab.com/fi-sas/backoffice/commit/2fb6b3de7d2480f5f915dacb88ecff1280f005b2))
* **form-equipments:** add pages ([e0a1bb9](https://gitlab.com/fi-sas/backoffice/commit/e0a1bb913c0fd098701646b58e31abf2cd647062))
* **general_config:** add page ([f20cb07](https://gitlab.com/fi-sas/backoffice/commit/f20cb0759fd3701a4711b75437b34960f8b61e0e))
* **list_equipments:** add page ([a21eb55](https://gitlab.com/fi-sas/backoffice/commit/a21eb5516135632705f66cd166f041ef1aacf2a3))
* **list_group_equipments:** add list of group equipments ([69092a7](https://gitlab.com/fi-sas/backoffice/commit/69092a7e558c2fd6f3d3b9b4cab2daa1f09d9246))
* **list_sensor:** add component ([bf1511c](https://gitlab.com/fi-sas/backoffice/commit/bf1511cb98ae547591d61eb1201eafc2526e6318))
* **list_units:** add component ([4414689](https://gitlab.com/fi-sas/backoffice/commit/441468982ad470941b039bd256a3545404f8504d))
* **list-equipments-view:** add page ([ee0b4b9](https://gitlab.com/fi-sas/backoffice/commit/ee0b4b920db28288d60e2af469df65ebbe36f1c9))
* **model:** add equipment_group model ([cb8432d](https://gitlab.com/fi-sas/backoffice/commit/cb8432db66ba8db711cf6c3ed20dce7a287f40ec))
* **model:** add source_connection models ([0a1d206](https://gitlab.com/fi-sas/backoffice/commit/0a1d206a20c27a58ecaf047d4050d0b449a0c89b))
* **model:** sensor model ([8eb820a](https://gitlab.com/fi-sas/backoffice/commit/8eb820a3ecf3aa79710edd9c9e80b18d0c6e5a5e))
* **models:** add equipment model ([b29657a](https://gitlab.com/fi-sas/backoffice/commit/b29657af55767cbf3ec352539c612389ff8732f1))
* **monitoring:** add unit service ([1a1d267](https://gitlab.com/fi-sas/backoffice/commit/1a1d2679297e6d563a0527616ea500f97e2dcede))
* **monitoring_module:** add module ([0587652](https://gitlab.com/fi-sas/backoffice/commit/058765293e90e5a9d48dcc4fce5c4ff750ebaddb))
* **sensor:** add service ([3ac38ae](https://gitlab.com/fi-sas/backoffice/commit/3ac38aeb981049bd48d38ddb208ea99486657a8a))
* **source-connection:** add service ([8000de4](https://gitlab.com/fi-sas/backoffice/commit/8000de460d43be0d4e1a7d040eb0cd0bd42f7801))
* **unit_model:** add unit model ([4ff1d73](https://gitlab.com/fi-sas/backoffice/commit/4ff1d7354fc7b9a1bef4491ca827b3cd69def274))

## [1.66.2](https://gitlab.com/fi-sas/backoffice/compare/v1.66.1...v1.66.2) (2021-07-06)


### Bug Fixes

* **social-scholarship:** fix typos ([fad3faf](https://gitlab.com/fi-sas/backoffice/commit/fad3faf8112a1791235f8e605623b52ea9ab157d))
* **volunteering:** fix scopes ([3220806](https://gitlab.com/fi-sas/backoffice/commit/32208061eef31dfce8d08d8bbfb81b12819a2269))

## [1.66.1](https://gitlab.com/fi-sas/backoffice/compare/v1.66.0...v1.66.1) (2021-07-06)


### Bug Fixes

* **infraestruture:** standardization of pages ([a1151d3](https://gitlab.com/fi-sas/backoffice/commit/a1151d383e6caea1b8d65c3d17f15d597a308a94))

# [1.66.0](https://gitlab.com/fi-sas/backoffice/compare/v1.65.3...v1.66.0) (2021-07-06)


### Features

* **volunteering:** remove advisor references ([64f9769](https://gitlab.com/fi-sas/backoffice/commit/64f9769e5c16da039b9cb2d24af8fd1c18e6b1e7))

## [1.65.3](https://gitlab.com/fi-sas/backoffice/compare/v1.65.2...v1.65.3) (2021-07-06)


### Bug Fixes

* **alimentation:** standardization of pages ([f1f2789](https://gitlab.com/fi-sas/backoffice/commit/f1f27894fa59093a5ac9b01132031bdbc3b8e1b8))

## [1.65.2](https://gitlab.com/fi-sas/backoffice/compare/v1.65.1...v1.65.2) (2021-07-06)


### Bug Fixes

* **communication:** standardization of pages ([346890f](https://gitlab.com/fi-sas/backoffice/commit/346890f5e6f1adad00d633ce21fc14ccb1b56498))

## [1.65.1](https://gitlab.com/fi-sas/backoffice/compare/v1.65.0...v1.65.1) (2021-07-06)


### Bug Fixes

* **geral:** add scroll to all listings ([3681181](https://gitlab.com/fi-sas/backoffice/commit/3681181a3cf800905687a6862b1368562f362aa1))

# [1.65.0](https://gitlab.com/fi-sas/backoffice/compare/v1.64.1...v1.65.0) (2021-07-06)


### Features

* **misc:** changed 'update' permission to 'status' ([855c2c8](https://gitlab.com/fi-sas/backoffice/commit/855c2c8ab10d18ff5621d48bacb179ca24e69204))
* **misc:** changed 'update' permission to 'status' ([85e061a](https://gitlab.com/fi-sas/backoffice/commit/85e061a0a1a6e29a8a4b25cbdbc013ad665ed55b))

## [1.64.1](https://gitlab.com/fi-sas/backoffice/compare/v1.64.0...v1.64.1) (2021-07-05)


### Bug Fixes

* **private-accommodation-complains:** remove list all users ([c05cb8a](https://gitlab.com/fi-sas/backoffice/commit/c05cb8a83724be0024af93907ab114174dab2618))
* **volunteering:** corrects issues detected in last review preparation ([0b95636](https://gitlab.com/fi-sas/backoffice/commit/0b95636bdc5f5959692af624ad3e1236f58d1603))

# [1.64.0](https://gitlab.com/fi-sas/backoffice/compare/v1.63.1...v1.64.0) (2021-07-01)


### Features

* add new version of logos ([85f9426](https://gitlab.com/fi-sas/backoffice/commit/85f9426a01477e703c7b8c3d8adb89f715b74e5a))
* new theme for IPViseu ([7246a34](https://gitlab.com/fi-sas/backoffice/commit/7246a341e02568e4dd3c7db63131e03065d8ea9b))

## [1.63.1](https://gitlab.com/fi-sas/backoffice/compare/v1.63.0...v1.63.1) (2021-07-01)


### Bug Fixes

* **accommodation:** add filter to raw report ([24ab9d6](https://gitlab.com/fi-sas/backoffice/commit/24ab9d6744e83be18a875312d43ac9cd7b48d4cb))
* **accommodation:** build error on view component ([bcb5acf](https://gitlab.com/fi-sas/backoffice/commit/bcb5acf8540aa520fb452a23fd9bd55e83e8a72a))
* **accommodation:** fix list application ([89b445b](https://gitlab.com/fi-sas/backoffice/commit/89b445b23d3886d2627a6fa004c47497b9b317a8))
* **accommodation:** removeu unused interface ([83de634](https://gitlab.com/fi-sas/backoffice/commit/83de63453d7b28dc975482d49fce28bf49998feb))

# [1.63.0](https://gitlab.com/fi-sas/backoffice/compare/v1.62.1...v1.63.0) (2021-07-01)


### Bug Fixes

* **sub23_declarations:** change sub23 applications to sub23 declarations ([e157fb8](https://gitlab.com/fi-sas/backoffice/commit/e157fb8e6bc7e83ebf32d9f6ee9e13e401c6adf1))


### Features

* **bus:** add sub 23 applications ([fc7bb4c](https://gitlab.com/fi-sas/backoffice/commit/fc7bb4ca17e0ae2037324b4b4d0935bc1beddf85))

## [1.62.1](https://gitlab.com/fi-sas/backoffice/compare/v1.62.0...v1.62.1) (2021-06-30)


### Bug Fixes

* **configurations:** add option to edit services ([5493764](https://gitlab.com/fi-sas/backoffice/commit/5493764d7147f96465e67e4b3e9d2da550bf9360))

# [1.62.0](https://gitlab.com/fi-sas/backoffice/compare/v1.61.3...v1.62.0) (2021-06-30)


### Bug Fixes

* **form_experience_componet:** fix patch to update ([0a2f8d2](https://gitlab.com/fi-sas/backoffice/commit/0a2f8d2eb9138d7214f2053e935b5d01fabd4767))


### Features

* **form-experience:** add info mail advisor ([e33c174](https://gitlab.com/fi-sas/backoffice/commit/e33c17498763f8230f7303a0caca933e863a1a82))
* **list_experience:** add advisor in chande status ([e259d17](https://gitlab.com/fi-sas/backoffice/commit/e259d1712ca5775e2a6799038b0f12149bd5ebe4))
* **list_experience_component:** add change status advisor ([eb7fc48](https://gitlab.com/fi-sas/backoffice/commit/eb7fc48f9f815ed711ade97614d3d738bb632b93))

## [1.61.3](https://gitlab.com/fi-sas/backoffice/compare/v1.61.2...v1.61.3) (2021-06-30)


### Bug Fixes

* **alimentation:** page renewal ([7fa9f24](https://gitlab.com/fi-sas/backoffice/commit/7fa9f2417ec66cd88645f7d0b0d9e52185577bdb))
* **alimentation:** renewing pages, add permissions ([37925d8](https://gitlab.com/fi-sas/backoffice/commit/37925d8557542723c8c1a15ea9a895ad631242c0))

## [1.61.2](https://gitlab.com/fi-sas/backoffice/compare/v1.61.1...v1.61.2) (2021-06-30)


### Bug Fixes

* **volunteering:** add withdrawal accepted state transition ([3f3f6ed](https://gitlab.com/fi-sas/backoffice/commit/3f3f6edc1e66696b33ce44e7c1daf78827093163))

## [1.61.1](https://gitlab.com/fi-sas/backoffice/compare/v1.61.0...v1.61.1) (2021-06-29)


### Bug Fixes

* **social-scholarship:** add missing scopes ([b5d2711](https://gitlab.com/fi-sas/backoffice/commit/b5d27116abb05558d876ee0253341bb3833de70c))
* **social-scholarship:** fix complaints change status scope ([562e680](https://gitlab.com/fi-sas/backoffice/commit/562e680048e58ae195b117a7100fb4c4d6eaa8c0))
* **social-scholarship:** fix model ([6b85013](https://gitlab.com/fi-sas/backoffice/commit/6b85013dc5b10300088408b7bcabc8be62c11317))
* **volunteering:** reorder experience list actions ([e2e4556](https://gitlab.com/fi-sas/backoffice/commit/e2e45564ec53c1a90623ac55ea482c32b6e77696))

# [1.61.0](https://gitlab.com/fi-sas/backoffice/compare/v1.60.2...v1.61.0) (2021-06-29)


### Bug Fixes

* **social-scholarship:** remove delete experience button ([83037e0](https://gitlab.com/fi-sas/backoffice/commit/83037e0d9d51619e3c941a63b792b9104f925829))
* **ubike:** change reports scopes ([19ed8f9](https://gitlab.com/fi-sas/backoffice/commit/19ed8f9bb3fe1d1fcda579b2c774cbbd84fca624))
* **ubike:** remove unused imports ([4fd5dad](https://gitlab.com/fi-sas/backoffice/commit/4fd5dad66733c9b17655d0934e767f046fb6675a))


### Features

* **volunteering:** add missing state transitions ([39942cb](https://gitlab.com/fi-sas/backoffice/commit/39942cb67e3a3a53d22762318ad198eff2e99d25))

## [1.60.2](https://gitlab.com/fi-sas/backoffice/compare/v1.60.1...v1.60.2) (2021-06-29)


### Bug Fixes

* **reports,notification:** delete ([a5c9d1a](https://gitlab.com/fi-sas/backoffice/commit/a5c9d1a786925888c2c453872a4bd26d0b7ea293))

## [1.60.1](https://gitlab.com/fi-sas/backoffice/compare/v1.60.0...v1.60.1) (2021-06-28)


### Bug Fixes

* **social-scholarship:** rename experience unpublish action ([5552168](https://gitlab.com/fi-sas/backoffice/commit/55521680d1f9926cbf158d9b423b820785996412))

# [1.60.0](https://gitlab.com/fi-sas/backoffice/compare/v1.59.6...v1.60.0) (2021-06-28)


### Features

* **logos:** add new logos to backoffice ([0dadcb5](https://gitlab.com/fi-sas/backoffice/commit/0dadcb5b39de5ff3ca451c9ea91604aa651db5e4))

## [1.59.6](https://gitlab.com/fi-sas/backoffice/compare/v1.59.5...v1.59.6) (2021-06-28)


### Bug Fixes

* **social-support-experience:** remove contract certifcate from view ([dee68f6](https://gitlab.com/fi-sas/backoffice/commit/dee68f630d1ba16b2029e31ae2d765443da14e0f))
* **social-support-experience:** remove from form ([6e01cda](https://gitlab.com/fi-sas/backoffice/commit/6e01cdaa738da99a7b2e592612475f3312c9c46d))
* **social-support-experiences:** remove contract and certificate html ([a37d00f](https://gitlab.com/fi-sas/backoffice/commit/a37d00ff55b8f6fda8b53ba51a036cf90b2dad19))

## [1.59.5](https://gitlab.com/fi-sas/backoffice/compare/v1.59.4...v1.59.5) (2021-06-28)


### Bug Fixes

* **social-support-activities:** fix activities ([ff0f926](https://gitlab.com/fi-sas/backoffice/commit/ff0f926ac214e42fcf721c8d7ffe2f3f2ff12bc4))

## [1.59.4](https://gitlab.com/fi-sas/backoffice/compare/v1.59.3...v1.59.4) (2021-06-25)


### Bug Fixes

* **experiences_list:** reduce withrelateds ([2e4b41c](https://gitlab.com/fi-sas/backoffice/commit/2e4b41cbf7501158de826f58ff5d94d5891a9b08))
* **list_applications:** update withrelateds ([56281c9](https://gitlab.com/fi-sas/backoffice/commit/56281c95b08f2165cb98f7ec6943a269ed56b5e8))
* **list_applications_volunteering:** update withrelateds ([57e8b52](https://gitlab.com/fi-sas/backoffice/commit/57e8b5228021e2400fc7fcd18d93663a9b9212be))
* **list_experiences_volunteering:** update withrelateds ([98c315a](https://gitlab.com/fi-sas/backoffice/commit/98c315a144e6ff7ec6405b10897790672de2dbea))
* **list_user_interest:** update call view ([bfb6859](https://gitlab.com/fi-sas/backoffice/commit/bfb68597fbe340d5d24a53a433e01595781833d2))
* **view_applications_scholarship:** update view ([be60275](https://gitlab.com/fi-sas/backoffice/commit/be60275b51fb76b4a74add48f8315827bc5b03e9))
* **view_applications_volunteering:** update view ([e09dd93](https://gitlab.com/fi-sas/backoffice/commit/e09dd9347370814a29416445f0017dccb87f68ae))
* **view_experience:** update view experience ([5442a5d](https://gitlab.com/fi-sas/backoffice/commit/5442a5d0da0906ddbbeb9317ed4c983bb7f2c29d))
* **view_experiences_volunteering:** update view ([fdc318e](https://gitlab.com/fi-sas/backoffice/commit/fdc318e2ad5acbe0e20092357281cbc24c1dd559))
* **view_user_intereste:** update view user interests ([b33b29e](https://gitlab.com/fi-sas/backoffice/commit/b33b29e1702cab5c767739b795072089f91731b4))

## [1.59.3](https://gitlab.com/fi-sas/backoffice/compare/v1.59.2...v1.59.3) (2021-06-24)


### Bug Fixes

* **shared:** cc saml request model name ([0acd723](https://gitlab.com/fi-sas/backoffice/commit/0acd723c056ff95120ed652ef8e9f13d1e84cbf9))

## [1.59.2](https://gitlab.com/fi-sas/backoffice/compare/v1.59.1...v1.59.2) (2021-06-24)


### Bug Fixes

* **bus:** add new application states ([89df8d9](https://gitlab.com/fi-sas/backoffice/commit/89df8d9b416f2a0ea6ff00252ecffe671226dd37))

## [1.59.1](https://gitlab.com/fi-sas/backoffice/compare/v1.59.0...v1.59.1) (2021-06-24)


### Bug Fixes

* **queue:** identify user ([de0198b](https://gitlab.com/fi-sas/backoffice/commit/de0198b56af3a3564dd362fa47caba3e89bd66d6))

# [1.59.0](https://gitlab.com/fi-sas/backoffice/compare/v1.58.0...v1.59.0) (2021-06-24)


### Bug Fixes

* **auth:** undefined auth on get permission ([1ed4cdc](https://gitlab.com/fi-sas/backoffice/commit/1ed4cdcf324caed0942756187ffe738260ab2457))
* **volunteering:** remove delete button from actions list ([2608a63](https://gitlab.com/fi-sas/backoffice/commit/2608a63f65102b19432ecbcc1e867936372bab11))


### Features

* **volunteering:** handle max collaborations ([ad14ffa](https://gitlab.com/fi-sas/backoffice/commit/ad14ffa6ea1457c36dd9b08d6d7344ff492af0e6))
* **volunteering:** remove academic year from action form ([67f1e24](https://gitlab.com/fi-sas/backoffice/commit/67f1e247e71d81abfd034e0dff8e1d6b6476c0e1))

# [1.58.0](https://gitlab.com/fi-sas/backoffice/compare/v1.57.6...v1.58.0) (2021-06-24)


### Bug Fixes

* **volunteering:** reload actions list after change status ([18e2b98](https://gitlab.com/fi-sas/backoffice/commit/18e2b98e58cc25f45cb22b093c756554c8f9f273))


### Features

* **volunteering:** invite students to manifest interest on actions ([4296748](https://gitlab.com/fi-sas/backoffice/commit/429674805d0c5e78d6bf60d12a4f8d6aed10f01d))

## [1.57.6](https://gitlab.com/fi-sas/backoffice/compare/v1.57.5...v1.57.6) (2021-06-24)


### Bug Fixes

* **queue:** timeOccurred, remove console log ([e0e6811](https://gitlab.com/fi-sas/backoffice/commit/e0e6811f3ba4663e1982306b83dad9a44beb5262))

## [1.57.5](https://gitlab.com/fi-sas/backoffice/compare/v1.57.4...v1.57.5) (2021-06-23)


### Bug Fixes

* **geral:** remove tinymce_api_key environment ([61d0a9e](https://gitlab.com/fi-sas/backoffice/commit/61d0a9ece67b889fb221c7acadc71da103532cd1))

## [1.57.4](https://gitlab.com/fi-sas/backoffice/compare/v1.57.3...v1.57.4) (2021-06-23)


### Bug Fixes

* **geral:** remove editor, required fields ([da05aed](https://gitlab.com/fi-sas/backoffice/commit/da05aed85e8b527c0591b6ef3f4e89fb8e345ee0))

## [1.57.3](https://gitlab.com/fi-sas/backoffice/compare/v1.57.2...v1.57.3) (2021-06-23)


### Bug Fixes

* **geral:** change editor ([4743d2f](https://gitlab.com/fi-sas/backoffice/commit/4743d2f3a12c500a2e0d19e38b6b0bdf63a76561))

## [1.57.2](https://gitlab.com/fi-sas/backoffice/compare/v1.57.1...v1.57.2) (2021-06-23)


### Bug Fixes

* **bus:** change withdrawals system ([394c4b1](https://gitlab.com/fi-sas/backoffice/commit/394c4b1c61b54cade2175b7c970e9c596c88a931))
* **communication:** correction of errors ([5137c7d](https://gitlab.com/fi-sas/backoffice/commit/5137c7d178a23d9198f7ecc83cc280bbc712e08a))
* **geral:** correction errors detect in the review ([e9c2ad7](https://gitlab.com/fi-sas/backoffice/commit/e9c2ad7969e9140b902cc9df1a4a997999f62343))

## [1.57.1](https://gitlab.com/fi-sas/backoffice/compare/v1.57.0...v1.57.1) (2021-06-23)


### Bug Fixes

* **communication:** lazy loading, facebook conf ([3bdd38a](https://gitlab.com/fi-sas/backoffice/commit/3bdd38a651bf1031be7f55c9e6394ac4ef54a9aa))

# [1.57.0](https://gitlab.com/fi-sas/backoffice/compare/v1.56.0...v1.57.0) (2021-06-22)


### Bug Fixes

* **social-scholarship:** define 0.01 euros as minimum for payment value ([e3b7a4b](https://gitlab.com/fi-sas/backoffice/commit/e3b7a4b7d01fb73b238f59390ffe56a941f3d9ed))


### Features

* **social-scholarship:** multiple changes ([234be9f](https://gitlab.com/fi-sas/backoffice/commit/234be9f802d5547be85efd8020111e72dbd23d36))

# [1.56.0](https://gitlab.com/fi-sas/backoffice/compare/v1.55.1...v1.56.0) (2021-06-21)


### Features

* **users:** add natiolnality field ([99a7fcd](https://gitlab.com/fi-sas/backoffice/commit/99a7fcdc220d1aec49eb4e7dab74ecb35f768b95))

## [1.55.1](https://gitlab.com/fi-sas/backoffice/compare/v1.55.0...v1.55.1) (2021-06-21)


### Bug Fixes

* **ubike:** correction of detected errors ([2d917f9](https://gitlab.com/fi-sas/backoffice/commit/2d917f974cc35ff72b9580ba622c7f3e590c854f))

# [1.55.0](https://gitlab.com/fi-sas/backoffice/compare/v1.54.0...v1.55.0) (2021-06-18)


### Features

* **social-scholarship:** handle max collaborations ([f6a156e](https://gitlab.com/fi-sas/backoffice/commit/f6a156edef3b3fd54874ba127e13ff5828c1ab77))

# [1.54.0](https://gitlab.com/fi-sas/backoffice/compare/v1.53.0...v1.54.0) (2021-06-18)


### Features

* **social-scholarship:** remove academic year from experience form ([fa3abe4](https://gitlab.com/fi-sas/backoffice/commit/fa3abe46cef80c4176d7b002b8c3f17f58ddb835))

# [1.53.0](https://gitlab.com/fi-sas/backoffice/compare/v1.52.7...v1.53.0) (2021-06-18)


### Features

* **volunteering:** list, details and create volunteering actions ([ed16270](https://gitlab.com/fi-sas/backoffice/commit/ed16270f46f29e6ce38cf9a3187c6ead6c1267c8))
* **volunteering:** list, details and create volunteering actions (wip) ([80b06e2](https://gitlab.com/fi-sas/backoffice/commit/80b06e2bf195bdc0fa9e198ab7793ade577cacf5))

## [1.52.7](https://gitlab.com/fi-sas/backoffice/compare/v1.52.6...v1.52.7) (2021-06-18)


### Bug Fixes

* **bus:** generate declaration ([94c8b1a](https://gitlab.com/fi-sas/backoffice/commit/94c8b1ae212e7882d71253d1dffed030c66beef0))

## [1.52.6](https://gitlab.com/fi-sas/backoffice/compare/v1.52.5...v1.52.6) (2021-06-18)


### Bug Fixes

* **accommodations:** changes misplaced nzloading ([fcf0055](https://gitlab.com/fi-sas/backoffice/commit/fcf0055056f93769930705a6924adb92ffb8cb8b))

## [1.52.5](https://gitlab.com/fi-sas/backoffice/compare/v1.52.4...v1.52.5) (2021-06-18)


### Bug Fixes

* **bus:** add map to locals ([c71decd](https://gitlab.com/fi-sas/backoffice/commit/c71decdec8f93e46d7ed386533266b0f3029903a))
* **bus:** add timetables validations ([34ca162](https://gitlab.com/fi-sas/backoffice/commit/34ca162482df286cfe47208723213262a485a702))
* **bus:** add title on timetables and prices ([8e94f03](https://gitlab.com/fi-sas/backoffice/commit/8e94f039fcea359fe21d35613213d88a8253a76e))
* **bus:** fix prices and zones errors ([90807bd](https://gitlab.com/fi-sas/backoffice/commit/90807bd3133672723c7f09d9cc024ea07cf4ad6e))

## [1.52.4](https://gitlab.com/fi-sas/backoffice/compare/v1.52.3...v1.52.4) (2021-06-17)


### Bug Fixes

* **accommodation:** add school filter on applications ([10e851f](https://gitlab.com/fi-sas/backoffice/commit/10e851f6c4e8cbdcd00c476e7b451ebcb5168e0d))

## [1.52.3](https://gitlab.com/fi-sas/backoffice/compare/v1.52.2...v1.52.3) (2021-06-17)


### Bug Fixes

* **queue:** counter operation process (continued) ([5264ef6](https://gitlab.com/fi-sas/backoffice/commit/5264ef6a64128e99e4c5ade62cf866cf8f2cfe83))
* **queue:** final fixes ([8f61d9e](https://gitlab.com/fi-sas/backoffice/commit/8f61d9e91b96f988195075852a2587a8434181f6))
* **queue:** refresh tickets ([d27254e](https://gitlab.com/fi-sas/backoffice/commit/d27254e0cf09ae0412f421f1c60012c59d8d87f2))

## [1.52.2](https://gitlab.com/fi-sas/backoffice/compare/v1.52.1...v1.52.2) (2021-06-17)


### Bug Fixes

* **volunteering:** multiple fixes ([3e87ddd](https://gitlab.com/fi-sas/backoffice/commit/3e87dddbb16c979d2bb927e1e6f5353a9da07ed7))

## [1.52.1](https://gitlab.com/fi-sas/backoffice/compare/v1.52.0...v1.52.1) (2021-06-16)


### Bug Fixes

* **volunteering:** multiple fixes ([44c6174](https://gitlab.com/fi-sas/backoffice/commit/44c6174cfbb8a3015788787d3d2b709b9cc777f3))

# [1.52.0](https://gitlab.com/fi-sas/backoffice/compare/v1.51.0...v1.52.0) (2021-06-16)


### Bug Fixes

* **listing_component:** update scope ([982d375](https://gitlab.com/fi-sas/backoffice/commit/982d375f43a5af88747d3408a572d3cef1983113))
* **listing_step_seven:** remove medias translations ([06ec5ba](https://gitlab.com/fi-sas/backoffice/commit/06ec5ba81e9b27597a9f15123f4749979e4c3f2d))
* **view_listing:** remove translations medias ([2595680](https://gitlab.com/fi-sas/backoffice/commit/259568063253f265441bfa9148052bf023ec469f))


### Features

* **volunteering:** implement participation interface ([96f794a](https://gitlab.com/fi-sas/backoffice/commit/96f794a6ea555a207970b7c1486747c53abf6198))

# [1.51.0](https://gitlab.com/fi-sas/backoffice/compare/v1.50.0...v1.51.0) (2021-06-16)


### Bug Fixes

* **social-scholarship:** add missing property ([f3585fe](https://gitlab.com/fi-sas/backoffice/commit/f3585fe8d35552c9dd59b26e87a37b0da530622e))
* **social-scholarship:** remove birth_date ([f0ed6a9](https://gitlab.com/fi-sas/backoffice/commit/f0ed6a985960a5efcfda085593790b4256037925))
* **social-scholarship:** remove birth_date ([6eea871](https://gitlab.com/fi-sas/backoffice/commit/6eea871d2c702bad9d9fe15a2f9586810c7e026c))
* **social-scholarship:** typo ([0fa89a7](https://gitlab.com/fi-sas/backoffice/commit/0fa89a7ac597388788a2967a74d441600245d760))
* **volunteering:** add missing property ([24e28aa](https://gitlab.com/fi-sas/backoffice/commit/24e28aa41d3e13689dfa36cc545914e278232229))
* **volunteering:** remove birth_date ([53555d6](https://gitlab.com/fi-sas/backoffice/commit/53555d647e3c3da438f8b6b47def31e6d56943e1))


### Features

* **social-scholarship:** add validation to external users ([f4afd13](https://gitlab.com/fi-sas/backoffice/commit/f4afd13ee2defce734316ea86cf8dc412a3c38a4))
* **volunteering:** add applications report by academic year ([e41a7ff](https://gitlab.com/fi-sas/backoffice/commit/e41a7ff913339f04ea145388c18293337de0e819))
* **volunteering:** add validation to external users ([48fa960](https://gitlab.com/fi-sas/backoffice/commit/48fa9606d3dc1984464cbb0fdeb84ad6fd9ce540))

# [1.50.0](https://gitlab.com/fi-sas/backoffice/compare/v1.49.1...v1.50.0) (2021-06-15)


### Bug Fixes

* **global:** fix proxy api target ([88b1fff](https://gitlab.com/fi-sas/backoffice/commit/88b1fff67f4c4496ad021fce760d60bcd3d61ee9))
* **social-scholarship:** fix endpoint ([60717f4](https://gitlab.com/fi-sas/backoffice/commit/60717f493bd377626774bc602c028bbbd5b061e8))
* **social-scholarship:** fix student report file ([a4319b5](https://gitlab.com/fi-sas/backoffice/commit/a4319b5307adfa074d168b957bb91c574481e1fb))


### Features

* **social-scholarship:** add contracts tab experience user interests ([5617328](https://gitlab.com/fi-sas/backoffice/commit/5617328087cad6baa4dacc8e6933e896afb4ee00))

## [1.49.1](https://gitlab.com/fi-sas/backoffice/compare/v1.49.0...v1.49.1) (2021-06-15)


### Bug Fixes

* **social-support-experience-list:** correcao lentidao da lista ([75fb300](https://gitlab.com/fi-sas/backoffice/commit/75fb3009308ffca2100839acc290ae73715cb2d7))

# [1.49.0](https://gitlab.com/fi-sas/backoffice/compare/v1.48.0...v1.49.0) (2021-06-15)


### Bug Fixes

* **social-scholarship:** fix module routing paths ([c385bbe](https://gitlab.com/fi-sas/backoffice/commit/c385bbef37c3a94533f247a0ad0bfae463252e6a))


### Features

* **social-scholarship:** add complain list details and answers ([bbe42b6](https://gitlab.com/fi-sas/backoffice/commit/bbe42b62850b0b546e57418dedb5ab0e75943698))
* **social-scholarship:** implement complains ([811744a](https://gitlab.com/fi-sas/backoffice/commit/811744a98656e610846d5331cdd0b82cf1c198b3))

# [1.48.0](https://gitlab.com/fi-sas/backoffice/compare/v1.47.0...v1.48.0) (2021-06-15)


### Bug Fixes

* **communication:** add return to list posts on success ([c86b881](https://gitlab.com/fi-sas/backoffice/commit/c86b8818311ef672dfa59670150023ca4b75ac04))
* **queue:** document correction ([5d62d96](https://gitlab.com/fi-sas/backoffice/commit/5d62d96866601d8d76792d5fb85276d002831f77))
* **several:** correction of various errors ([3d10193](https://gitlab.com/fi-sas/backoffice/commit/3d10193210774d07607b17683aee41f3286075c8))


### Features

* **volunteering:** add volunteering actions reports ([3ad5768](https://gitlab.com/fi-sas/backoffice/commit/3ad5768f2195b421fd946aa36445f77005b6162d))

# [1.47.0](https://gitlab.com/fi-sas/backoffice/compare/v1.46.5...v1.47.0) (2021-06-14)


### Features

* **media:** remove unused module MEDIA from dashboard ([ec7333f](https://gitlab.com/fi-sas/backoffice/commit/ec7333f93220d35e700183f0e83da123a5bef9a9))

## [1.46.5](https://gitlab.com/fi-sas/backoffice/compare/v1.46.4...v1.46.5) (2021-06-11)


### Bug Fixes

* **alimentation:** filters, edit prod, type dishes ([f1625b0](https://gitlab.com/fi-sas/backoffice/commit/f1625b03a6e7d9a7bc9cecf8d58ff7f534e74822))

## [1.46.4](https://gitlab.com/fi-sas/backoffice/compare/v1.46.3...v1.46.4) (2021-06-11)


### Bug Fixes

* **social-scholarship:** fix missing endpoint ([ea5ca85](https://gitlab.com/fi-sas/backoffice/commit/ea5ca85097c0e1d885767b950c6649e7e076c5d4))

## [1.46.3](https://gitlab.com/fi-sas/backoffice/compare/v1.46.2...v1.46.3) (2021-06-11)


### Bug Fixes

* **accommodation:** more changes requested ([e69b5fb](https://gitlab.com/fi-sas/backoffice/commit/e69b5fb2f410c65e7391dab4bbff46c6ef3fa57e))

## [1.46.2](https://gitlab.com/fi-sas/backoffice/compare/v1.46.1...v1.46.2) (2021-06-09)


### Bug Fixes

* **infraestruture:** filter edit ([72b00fa](https://gitlab.com/fi-sas/backoffice/commit/72b00fa132863ba5f482730ffb5d2d760ba8b725))
* **ubike:** permission ([95ab985](https://gitlab.com/fi-sas/backoffice/commit/95ab985edcc63b7f347be9d3d5ee3d225d7a55aa))

## [1.46.1](https://gitlab.com/fi-sas/backoffice/compare/v1.46.0...v1.46.1) (2021-06-09)


### Bug Fixes

* **ubike:** trim error ([b2f3a71](https://gitlab.com/fi-sas/backoffice/commit/b2f3a7104ede46ec7d5a0cf8607d9d979ba06fcb))

# [1.46.0](https://gitlab.com/fi-sas/backoffice/compare/v1.45.0...v1.46.0) (2021-06-09)


### Bug Fixes

* **queue:** correction of document errors ([9c91544](https://gitlab.com/fi-sas/backoffice/commit/9c91544a708ccfb71c39c3077d2636d914906c54))


### Features

* **volunteering:** add complaints system ([748aebc](https://gitlab.com/fi-sas/backoffice/commit/748aebc8afee4620d782d7a7e8751ba9a5469a61))
* **volunteering:** add scopes to complaints system ([0d42caf](https://gitlab.com/fi-sas/backoffice/commit/0d42caf6239e8bb941fc7b03788aa764b32d6ccb))

# [1.45.0](https://gitlab.com/fi-sas/backoffice/compare/v1.44.2...v1.45.0) (2021-06-09)


### Features

* **volunteering:** add close all applications ([55d1db9](https://gitlab.com/fi-sas/backoffice/commit/55d1db9389d79e9fbce24cd1342b70d10d0f0fb4))

## [1.44.2](https://gitlab.com/fi-sas/backoffice/compare/v1.44.1...v1.44.2) (2021-06-08)


### Bug Fixes

* **volunteering:** fix build error ([d7c00b0](https://gitlab.com/fi-sas/backoffice/commit/d7c00b0f989fc55162b86bbb2ac4603bfda15c9c))

## [1.44.1](https://gitlab.com/fi-sas/backoffice/compare/v1.44.0...v1.44.1) (2021-06-08)


### Bug Fixes

* **social-scholarship:** fix mandatory return reason ([eda8838](https://gitlab.com/fi-sas/backoffice/commit/eda88384cf879b633e72d0919cd2693c3db80fc2))

# [1.44.0](https://gitlab.com/fi-sas/backoffice/compare/v1.43.2...v1.44.0) (2021-06-08)


### Features

* **social-scholarship:** add payment grid permissions ([b3d1967](https://gitlab.com/fi-sas/backoffice/commit/b3d19677f596dd6a6e18cdd1e369d6844d05f181))

## [1.43.2](https://gitlab.com/fi-sas/backoffice/compare/v1.43.1...v1.43.2) (2021-06-02)


### Bug Fixes

* **social-scholarship:** add missing endpoint ([4e79194](https://gitlab.com/fi-sas/backoffice/commit/4e7919479d47ff1698de7a18c3b2881ff554b70e))

## [1.43.1](https://gitlab.com/fi-sas/backoffice/compare/v1.43.0...v1.43.1) (2021-06-02)


### Bug Fixes

* **alimentation:** dates error correction ([a4ec530](https://gitlab.com/fi-sas/backoffice/commit/a4ec530d1b2bbc53703aa73599245543ef981414))

# [1.43.0](https://gitlab.com/fi-sas/backoffice/compare/v1.42.0...v1.43.0) (2021-06-02)


### Bug Fixes

* **alimentation:** fixes ([bcfabd7](https://gitlab.com/fi-sas/backoffice/commit/bcfabd7024a966b809ef5cec3c9312699663bcd5))
* **social-scholarship:** fix financial email configuration ([e59d6ff](https://gitlab.com/fi-sas/backoffice/commit/e59d6ff39c1ba20e51d87049b308626000c80edd))


### Features

* **social-scholarship:** add student number search ([d395a4e](https://gitlab.com/fi-sas/backoffice/commit/d395a4eee21f5b56317717627bab55b277e13a5c))

# [1.42.0](https://gitlab.com/fi-sas/backoffice/compare/v1.41.0...v1.42.0) (2021-06-01)


### Bug Fixes

* **accommodation:** fixes ([1368530](https://gitlab.com/fi-sas/backoffice/commit/1368530b9a586b770f8bc52d0e40baed758f2557))
* **volunteering:** fix comment ([c2e0e79](https://gitlab.com/fi-sas/backoffice/commit/c2e0e79f441678c765593683f671bdf8f648a53b))
* **volunteering:** fix permissions ([c489393](https://gitlab.com/fi-sas/backoffice/commit/c4893936f99e33cb66095f385e59d3a83e69d95b))
* **volunteering:** remove duplicate routes ([63c7295](https://gitlab.com/fi-sas/backoffice/commit/63c7295f1b66deb186207addde73755c6fda4080))


### Features

* **volunteering:** state machine implementation ([816cc7e](https://gitlab.com/fi-sas/backoffice/commit/816cc7e259d510c3cba20abc279d9d0dfb33cbf6))
* **volunteering:** state machine, filters and application listing ([44057ee](https://gitlab.com/fi-sas/backoffice/commit/44057ee597af77fd6ca19a300530bcc52d535343))

# [1.41.0](https://gitlab.com/fi-sas/backoffice/compare/v1.40.0...v1.41.0) (2021-06-01)


### Bug Fixes

* **payment_grid:** update calculate values ([a364322](https://gitlab.com/fi-sas/backoffice/commit/a3643229c3407d79ca7a1502d9e54eb3898ff90d))
* **users:** add last step validation ([e7638fa](https://gitlab.com/fi-sas/backoffice/commit/e7638fa1bddef5690c2e70f7e903cd0b681a39aa))
* **users:** fix 2 click save errror ([885d4c2](https://gitlab.com/fi-sas/backoffice/commit/885d4c28c54e3df18227236812e289aa16f93023))


### Features

* **social-scholarship:** add financial view ([fc0c589](https://gitlab.com/fi-sas/backoffice/commit/fc0c5895bbff57265e47e41f4f7717adb56e5abb))
* **social-scholarship:** add permission scope ([cf46441](https://gitlab.com/fi-sas/backoffice/commit/cf46441c4f5f538cf5517286b04c27e0309554f7))

# [1.40.0](https://gitlab.com/fi-sas/backoffice/compare/v1.39.1...v1.40.0) (2021-06-01)


### Bug Fixes

* **comunication:** weight field in the categories ([93c949e](https://gitlab.com/fi-sas/backoffice/commit/93c949e527cff48056fd472f82828dc53b0e8e6f))


### Features

* **accommodation:** add automatic status change config ([b194bf3](https://gitlab.com/fi-sas/backoffice/commit/b194bf3222dfef26e72a6784c4ea484ba9f27bf7))

## [1.39.1](https://gitlab.com/fi-sas/backoffice/compare/v1.39.0...v1.39.1) (2021-05-31)


### Bug Fixes

* **accommodation:** unlock gender when u ([b152308](https://gitlab.com/fi-sas/backoffice/commit/b15230862fe1ad85b5aeb0a8ffce8897a1b493c7))

# [1.39.0](https://gitlab.com/fi-sas/backoffice/compare/v1.38.2...v1.39.0) (2021-05-31)


### Bug Fixes

* **volunteering:** fix module loading path ([78aaf2d](https://gitlab.com/fi-sas/backoffice/commit/78aaf2d9cfd36fa0984aa87976783023d9a0bb32))


### Features

* **volunteering:** add external entities ([855328f](https://gitlab.com/fi-sas/backoffice/commit/855328f3d62abbea64f13b15d3f494a9512da46c))

## [1.38.2](https://gitlab.com/fi-sas/backoffice/compare/v1.38.1...v1.38.2) (2021-05-31)


### Bug Fixes

* **accommodation:** new field typology ([d755a08](https://gitlab.com/fi-sas/backoffice/commit/d755a0878d29e8152c30753f8723c62dc47fb277))

## [1.38.1](https://gitlab.com/fi-sas/backoffice/compare/v1.38.0...v1.38.1) (2021-05-28)


### Bug Fixes

* **social-scholarship:** fix external user model ([6786e6e](https://gitlab.com/fi-sas/backoffice/commit/6786e6eb12f78c506a9a94a3b5a497abda58cbe5))

# [1.38.0](https://gitlab.com/fi-sas/backoffice/compare/v1.37.0...v1.38.0) (2021-05-28)


### Bug Fixes

* **social-scholarship:** remove unnecessary code ([fb067b1](https://gitlab.com/fi-sas/backoffice/commit/fb067b1ad4904d3829ec92e20c5d8e304d437d5f))


### Features

* **social-scholarship:** add external users ([9a64f60](https://gitlab.com/fi-sas/backoffice/commit/9a64f60c29267c6a7515091b85e9d4fd6f7b89e3))
* **social-scholarship:** add file upload ([2dca08c](https://gitlab.com/fi-sas/backoffice/commit/2dca08cc760c822d911e436ebd026d554fc91b08))
* **social-scholarship:** add permission scopes ([08d470f](https://gitlab.com/fi-sas/backoffice/commit/08d470f9e526848286c1dc358bfe8d961f60ef34))

# [1.37.0](https://gitlab.com/fi-sas/backoffice/compare/v1.36.1...v1.37.0) (2021-05-28)


### Bug Fixes

* **accommodation:** school filter ([b425fa5](https://gitlab.com/fi-sas/backoffice/commit/b425fa50f7626d4fc5d89d7330099b5f048fff9f))
* **social-scholarship:** fix typo on experience model attribute ([55e7cfb](https://gitlab.com/fi-sas/backoffice/commit/55e7cfb49b2f49db2e12d2ae9390e52e74873194))


### Features

* **infrastructure:** add permissions ([0eecf94](https://gitlab.com/fi-sas/backoffice/commit/0eecf94ec2d8cbe83bd9c41e4f36f199a1ceedd1))
* **social-scholarship:** change state tab/max colaborations ([9e86ab1](https://gitlab.com/fi-sas/backoffice/commit/9e86ab153372f0c5f63d5092fdb9eb97b3a3c260))
* **social-scholarship:** validate collaborations on user interests ([fc486c5](https://gitlab.com/fi-sas/backoffice/commit/fc486c5224f2a563bb2bdbaf9d44c8127a487694))

## [1.36.1](https://gitlab.com/fi-sas/backoffice/compare/v1.36.0...v1.36.1) (2021-05-27)


### Bug Fixes

* **social-scholarship:** fix missing endpoint ([fdd079a](https://gitlab.com/fi-sas/backoffice/commit/fdd079a09567c91e57c1b69e7b73d383ac49d709))

# [1.36.0](https://gitlab.com/fi-sas/backoffice/compare/v1.35.8...v1.36.0) (2021-05-27)


### Bug Fixes

* **social-scholarship:** remove console.log ([009f560](https://gitlab.com/fi-sas/backoffice/commit/009f5604edb55931571471914a37c19a7d4dbdef))


### Features

* **alert-types:** add simulation button ([c58d73a](https://gitlab.com/fi-sas/backoffice/commit/c58d73a8fcc1ae07449d699f59b32cc8165ba310))
* **social-scholarship:** add max hours alert ([57d8ee7](https://gitlab.com/fi-sas/backoffice/commit/57d8ee7046f871799e44e8e7713c4d7888bd2023))

## [1.35.8](https://gitlab.com/fi-sas/backoffice/compare/v1.35.7...v1.35.8) (2021-05-26)


### Bug Fixes

* **social-scholarship:** fix to adapt changes made in MS ([cb7c840](https://gitlab.com/fi-sas/backoffice/commit/cb7c8409268001aa1fbfa8fbd90fe301092a19d4))
* **social-scholarship:** fix to adapt changes made in MS ([db4d276](https://gitlab.com/fi-sas/backoffice/commit/db4d27650faeb798d2b42dadd75124b759c0eef7))

## [1.35.7](https://gitlab.com/fi-sas/backoffice/compare/v1.35.6...v1.35.7) (2021-05-26)


### Bug Fixes

* **accommodation:** permission ([28664fa](https://gitlab.com/fi-sas/backoffice/commit/28664fa7fb3ebbe34e4a20d983109e8cab75dcde))

## [1.35.6](https://gitlab.com/fi-sas/backoffice/compare/v1.35.5...v1.35.6) (2021-05-26)


### Bug Fixes

* **geral:** trim, filter room, canteen filter ([2e03af9](https://gitlab.com/fi-sas/backoffice/commit/2e03af9e2872733bd506e3b0d61ea0336df55075))

## [1.35.5](https://gitlab.com/fi-sas/backoffice/compare/v1.35.4...v1.35.5) (2021-05-26)


### Bug Fixes

* **accommodation:** error correction ([f0561c9](https://gitlab.com/fi-sas/backoffice/commit/f0561c95d59a4be691175ac99ccbad3c38eccf5d))
* **alimmentation:** correction of detected errors ([bfbcb3a](https://gitlab.com/fi-sas/backoffice/commit/bfbcb3ab5502a3f6cb0bf878b1fad48027c5ecef))

## [1.35.4](https://gitlab.com/fi-sas/backoffice/compare/v1.35.3...v1.35.4) (2021-05-25)


### Bug Fixes

* **accommodation:** correction of errors ([f12937d](https://gitlab.com/fi-sas/backoffice/commit/f12937d46f5a0cf838453050ac10e3fccb0a59b4))

## [1.35.3](https://gitlab.com/fi-sas/backoffice/compare/v1.35.2...v1.35.3) (2021-05-25)


### Bug Fixes

* **accommodation:** changing input text,trim ([75db344](https://gitlab.com/fi-sas/backoffice/commit/75db344e5dfe5f589e6ccf80c78633254240818b))

## [1.35.2](https://gitlab.com/fi-sas/backoffice/compare/v1.35.1...v1.35.2) (2021-05-25)


### Bug Fixes

* **alimentation:** change order status ([559e15e](https://gitlab.com/fi-sas/backoffice/commit/559e15e17ddce06ccdee0b473eb46be6e15fd4f5))

## [1.35.1](https://gitlab.com/fi-sas/backoffice/compare/v1.35.0...v1.35.1) (2021-05-25)


### Bug Fixes

* **alimentation:** changing orders - states, view ([68a4f20](https://gitlab.com/fi-sas/backoffice/commit/68a4f20faca4b50f5f366458c68dc03fa443e262))

# [1.35.0](https://gitlab.com/fi-sas/backoffice/compare/v1.34.2...v1.35.0) (2021-05-24)


### Bug Fixes

* **accommodation:** correction errors ([5e8361c](https://gitlab.com/fi-sas/backoffice/commit/5e8361cb961eb30f627a354b9579c05676a21c25))
* **geral:** change Fi@SAS ([04e85b9](https://gitlab.com/fi-sas/backoffice/commit/04e85b9a7e2a86ce0d956c661fa5f7854170fc86))


### Features

* **configurations:** Add regulations and text-presentation scopes ([510e55b](https://gitlab.com/fi-sas/backoffice/commit/510e55b345707c8f0b8ed71aecfe3f0f09969be1))
* **queue:** Add module scopes ([10926ec](https://gitlab.com/fi-sas/backoffice/commit/10926ec78c2f9d2a1cc6164ba1df3de7d737faa2))

## [1.34.2](https://gitlab.com/fi-sas/backoffice/compare/v1.34.1...v1.34.2) (2021-05-21)


### Bug Fixes

* **alimentation:** correction error access ([9276a49](https://gitlab.com/fi-sas/backoffice/commit/9276a4987e70a358450d836cacb4718e067775ca))

## [1.34.1](https://gitlab.com/fi-sas/backoffice/compare/v1.34.0...v1.34.1) (2021-05-21)


### Bug Fixes

* **geral:** service name change settings ([79fe3d2](https://gitlab.com/fi-sas/backoffice/commit/79fe3d2e8a6e3e9f467179368a377e8c203c5198))

# [1.34.0](https://gitlab.com/fi-sas/backoffice/compare/v1.33.1...v1.34.0) (2021-05-21)


### Bug Fixes

* **social-scholarship:** create missing history models ([daa318f](https://gitlab.com/fi-sas/backoffice/commit/daa318fd264160563c7ed389f4d6cf141499a1c9))


### Features

* **social-scholarship:** schedule and execute user interest interview ([d912c9b](https://gitlab.com/fi-sas/backoffice/commit/d912c9b7c7abcd1d67356a6a5dd2d107c8c567c3))

## [1.33.1](https://gitlab.com/fi-sas/backoffice/compare/v1.33.0...v1.33.1) (2021-05-21)


### Bug Fixes

* **infraestructure:** correction infrastructure ([f350677](https://gitlab.com/fi-sas/backoffice/commit/f3506770990baac60591e5df7bc13dc1084c67f8))

# [1.33.0](https://gitlab.com/fi-sas/backoffice/compare/v1.32.2...v1.33.0) (2021-05-20)


### Bug Fixes

* **mobility:** added removed fields from previous commit ([847571f](https://gitlab.com/fi-sas/backoffice/commit/847571f95f076b714b93bca5fd55ee233f5efc62))
* **mobility:** fix permissions ([b694a9c](https://gitlab.com/fi-sas/backoffice/commit/b694a9c995e7232bedc83884fa2fc85443d183f3))
* **mobility:** fix permissions ([4fc3e73](https://gitlab.com/fi-sas/backoffice/commit/4fc3e73b67ccae8450f8d99f861bb2226f188ae7))
* **private-accommodation:** fix duplicated identifier ([ba3edcc](https://gitlab.com/fi-sas/backoffice/commit/ba3edcca515eecbb352aa250a2af089922bf0c3e))
* **private-accommodation:** fix permissions ([2fc6888](https://gitlab.com/fi-sas/backoffice/commit/2fc6888123ff9e84bdae42b3deb101bbe7d94777))
* **social-scholarship:** fix merge problems ([63be4b1](https://gitlab.com/fi-sas/backoffice/commit/63be4b1fa54af5a3cc6da0a6591a60248c1b601f))
* **social-scholarship:** missing comma ([c1aa377](https://gitlab.com/fi-sas/backoffice/commit/c1aa3776c1363543d0bd335b5d71eb68b82b3692))
* **users:** fix scope name ([e970788](https://gitlab.com/fi-sas/backoffice/commit/e970788d6befd8d7df46a40ff1dc685c6c8b2f24))


### Features

* **social-scholarship:** add warning messages ([1c7fea7](https://gitlab.com/fi-sas/backoffice/commit/1c7fea79c8bdc1c2999820ede41d42114a281320))

## [1.32.2](https://gitlab.com/fi-sas/backoffice/compare/v1.32.1...v1.32.2) (2021-05-19)


### Bug Fixes

* **accommodation:** gender ([af6e16d](https://gitlab.com/fi-sas/backoffice/commit/af6e16dc36e973b939abc03161b807ede1403f8c))

## [1.32.1](https://gitlab.com/fi-sas/backoffice/compare/v1.32.0...v1.32.1) (2021-05-19)


### Bug Fixes

* **accommodation:** add gender profile ([72c6212](https://gitlab.com/fi-sas/backoffice/commit/72c6212413febb4d27ca691fb7d1b8b68c79610d))

# [1.32.0](https://gitlab.com/fi-sas/backoffice/compare/v1.31.0...v1.32.0) (2021-05-19)


### Bug Fixes

* **social-scholarship:** duplicate identifier ([d26a0f3](https://gitlab.com/fi-sas/backoffice/commit/d26a0f3dd62dd2f52bf57eb4469c9804fa64725a))
* **social-scholarship):** missing endpoints ([e399057](https://gitlab.com/fi-sas/backoffice/commit/e39905736259219c925dbbd577fb126809ad6f39))


### Features

* **social-scholarship:** add collaboration management ([f8adcf5](https://gitlab.com/fi-sas/backoffice/commit/f8adcf51c67139d3c2b299cefe4e57a914a01ad7))
* **social-scholarship:** add link do attendance file ([8a8c268](https://gitlab.com/fi-sas/backoffice/commit/8a8c2682382c7ebd9e9b15db95cc310e6ec895d1))
* **social-scholarship:** add max monthly hours management ([95c6a2b](https://gitlab.com/fi-sas/backoffice/commit/95c6a2bb77cea5eca70bc25a0f41b618351671f2))

# [1.31.0](https://gitlab.com/fi-sas/backoffice/compare/v1.30.1...v1.31.0) (2021-05-19)


### Bug Fixes

* **accommodation:** fix occupation map ([1292641](https://gitlab.com/fi-sas/backoffice/commit/129264175a2886cc81e887472cbb3d75170e08b6))
* **accommodation:** language service change ([379f9dd](https://gitlab.com/fi-sas/backoffice/commit/379f9dd3b0a7d12c3a272c46d8425e1595638a2d))
* **accommodation:** language service change ([0fb03c6](https://gitlab.com/fi-sas/backoffice/commit/0fb03c66afbe7798a6b36dbb9d6afb4c252f1056))
* **current_account:** fix errors and add iban ([ab27251](https://gitlab.com/fi-sas/backoffice/commit/ab272519890970242f7b096aac500a9bcb1ffcac))
* **social-scholarship:** fix wrong model ([024e1bd](https://gitlab.com/fi-sas/backoffice/commit/024e1bd0ca6c7705ae1f4eee0bea40740786a0d8))


### Features

* **social-scholarship:** add extra columns to experiences list ([8d27503](https://gitlab.com/fi-sas/backoffice/commit/8d275033b403b1da3ce956ba25b9f534549491c2))
* **social-scholarship:** add view application modal ([9ce4f19](https://gitlab.com/fi-sas/backoffice/commit/9ce4f1955738854fccc9ffa9b53e42420f80ccfc))
* **social-scholarship:** generate experience user interests report ([3174a68](https://gitlab.com/fi-sas/backoffice/commit/3174a688df98239d98e5b99fbf45be39eacea103))
* **social-scholarship:** reopen experience user interest ([08f6633](https://gitlab.com/fi-sas/backoffice/commit/08f66331d56a722ca92e6eef513e410fac89ee75))

## [1.30.1](https://gitlab.com/fi-sas/backoffice/compare/v1.30.0...v1.30.1) (2021-05-19)


### Bug Fixes

* **geral:** new home page permissions ([002ba21](https://gitlab.com/fi-sas/backoffice/commit/002ba2111ef47846c4ccfd76aba23951b9ea6649))

# [1.30.0](https://gitlab.com/fi-sas/backoffice/compare/v1.29.5...v1.30.0) (2021-05-18)


### Features

* **social-scholarship:** add navigation to panels ([5cc738b](https://gitlab.com/fi-sas/backoffice/commit/5cc738b3a6f784218724db350cf46e1c5cf165bf))

## [1.29.5](https://gitlab.com/fi-sas/backoffice/compare/v1.29.4...v1.29.5) (2021-05-18)


### Bug Fixes

* **alimentation:** more bug fixes ([feac391](https://gitlab.com/fi-sas/backoffice/commit/feac391a02842e1d5f2107b5f37192db5e89f6f8))

## [1.29.4](https://gitlab.com/fi-sas/backoffice/compare/v1.29.3...v1.29.4) (2021-05-18)


### Bug Fixes

* **geral:** error correction ([3fd5557](https://gitlab.com/fi-sas/backoffice/commit/3fd55579e9943f28755f3e7d708c3b3137125150))

## [1.29.3](https://gitlab.com/fi-sas/backoffice/compare/v1.29.2...v1.29.3) (2021-05-18)


### Bug Fixes

* **alimentation:** error correction ([7d1790d](https://gitlab.com/fi-sas/backoffice/commit/7d1790d358956714d45891c455d260aa258b26b9))

## [1.29.2](https://gitlab.com/fi-sas/backoffice/compare/v1.29.1...v1.29.2) (2021-05-18)


### Bug Fixes

* **alimentation:** error correction ([0e33562](https://gitlab.com/fi-sas/backoffice/commit/0e335622b73fded2323ec2ccc130488cf9d176dc))

## [1.29.1](https://gitlab.com/fi-sas/backoffice/compare/v1.29.0...v1.29.1) (2021-05-17)


### Bug Fixes

* **notifications:** add field data to form ([570adb0](https://gitlab.com/fi-sas/backoffice/commit/570adb0bfe8fab619c91c4ab8ac33641717da2ac))

# [1.29.0](https://gitlab.com/fi-sas/backoffice/compare/v1.28.0...v1.29.0) (2021-05-17)


### Bug Fixes

* **accommodation:** fix text error ([2ccf7e8](https://gitlab.com/fi-sas/backoffice/commit/2ccf7e85013f68513befc542f6edc11b16100351))


### Features

* **accommodation:** add field visible_for_users to residences ([0a4357a](https://gitlab.com/fi-sas/backoffice/commit/0a4357ad375f06b4f28e1e9e52ab90fa969e5550))

# [1.28.0](https://gitlab.com/fi-sas/backoffice/compare/v1.27.9...v1.28.0) (2021-05-17)


### Bug Fixes

* **current_account:** add cash account and operator to movements ([7e1fe7a](https://gitlab.com/fi-sas/backoffice/commit/7e1fe7a5bbd5fbe8e2da7184679d5d7cfee11a06))
* **current_account:** disable charge/uncharge button ([5b0f5c4](https://gitlab.com/fi-sas/backoffice/commit/5b0f5c4d99baec2ed221f8e04551143a3209d3e6))
* **current_account:** fix date pipe ([f403fdc](https://gitlab.com/fi-sas/backoffice/commit/f403fdc175fce54b04e9bac851d11e23441966b7))


### Features

* **cash_accounts:** manege cash_Accounts ([ddd604e](https://gitlab.com/fi-sas/backoffice/commit/ddd604ee6ddd43e3529ba564e513c52d0d211f22))
* **social-scholarship:** implement user interests state machine ([6952d50](https://gitlab.com/fi-sas/backoffice/commit/6952d5014f9382023e3d3f2f4535293bb5e07045))

## [1.27.9](https://gitlab.com/fi-sas/backoffice/compare/v1.27.8...v1.27.9) (2021-05-14)


### Bug Fixes

* **geral:** notifications, reports, buildings ([b5704db](https://gitlab.com/fi-sas/backoffice/commit/b5704dbb6744f3d2754718d6fbce3a224e5055ff))

## [1.27.8](https://gitlab.com/fi-sas/backoffice/compare/v1.27.7...v1.27.8) (2021-05-13)


### Bug Fixes

* **geral:** field external_ref ([9c2f2c9](https://gitlab.com/fi-sas/backoffice/commit/9c2f2c904b50b51b3bfb8ee1d7457e9b815ef20b))

## [1.27.7](https://gitlab.com/fi-sas/backoffice/compare/v1.27.6...v1.27.7) (2021-05-13)


### Bug Fixes

* **ipleiria:** change ipleiria logos filenames ([4ec7e2e](https://gitlab.com/fi-sas/backoffice/commit/4ec7e2e5a1f17c818775a5409b7c47930e8bab9e))

## [1.27.6](https://gitlab.com/fi-sas/backoffice/compare/v1.27.5...v1.27.6) (2021-05-13)


### Bug Fixes

* **accommodation:** correction of form errors ([2c6d94b](https://gitlab.com/fi-sas/backoffice/commit/2c6d94b158239474ba346206f6d9ed8e5692c291))

## [1.27.5](https://gitlab.com/fi-sas/backoffice/compare/v1.27.4...v1.27.5) (2021-05-13)


### Bug Fixes

* **alimentation:** document error correction ([9e020f4](https://gitlab.com/fi-sas/backoffice/commit/9e020f4fd73767ad396e0516fa02a3b6623140cb))

## [1.27.4](https://gitlab.com/fi-sas/backoffice/compare/v1.27.3...v1.27.4) (2021-05-12)


### Bug Fixes

* **post:** max length summary ([5307d01](https://gitlab.com/fi-sas/backoffice/commit/5307d01bb148003af49db8e5f35a8774533cf100))

## [1.27.3](https://gitlab.com/fi-sas/backoffice/compare/v1.27.2...v1.27.3) (2021-05-12)


### Bug Fixes

* **communication:** fix date range on create/edit post ([2b46b45](https://gitlab.com/fi-sas/backoffice/commit/2b46b452af134ebb6afbeea9f28453fd405def12))

## [1.27.2](https://gitlab.com/fi-sas/backoffice/compare/v1.27.1...v1.27.2) (2021-05-11)


### Bug Fixes

* **accommodation:** correction query ([6ce3efc](https://gitlab.com/fi-sas/backoffice/commit/6ce3efc8a39e5cb6d8d22730b491e54db51cdd26))

## [1.27.1](https://gitlab.com/fi-sas/backoffice/compare/v1.27.0...v1.27.1) (2021-05-10)


### Bug Fixes

* **users:** add document type to user form ([0861666](https://gitlab.com/fi-sas/backoffice/commit/0861666c3013b256728e631bbaaebb57181c1161))

# [1.27.0](https://gitlab.com/fi-sas/backoffice/compare/v1.26.1...v1.27.0) (2021-05-10)


### Bug Fixes

* **management-payments:** fix error ([3c6ed02](https://gitlab.com/fi-sas/backoffice/commit/3c6ed02054adf6342709f9b1378a609204baccb1))
* **payments_module:** add payment module and routing ([17f13b9](https://gitlab.com/fi-sas/backoffice/commit/17f13b98df024a3bf4afd9e6477451d2f6fd1062))
* **social-support:** add routing ([1a54f2f](https://gitlab.com/fi-sas/backoffice/commit/1a54f2f8dec9bfe3c4770d59d55130d6f21e131d))


### Features

* **configs:** add endpoints payment_grid ([5127cc1](https://gitlab.com/fi-sas/backoffice/commit/5127cc12a92c0b66f64ea67629e09da440e07294))
* **modules_payment:** add component payment-grid ([3f6615c](https://gitlab.com/fi-sas/backoffice/commit/3f6615c2fa47ac2f70e8f428670311e30f1da68b))
* **payment_services:** add user_interest_service ([fcd96f0](https://gitlab.com/fi-sas/backoffice/commit/fcd96f0910c0d11408b447dd7e42fde9b5e42d10))
* **payments_grid:** add model payment_grid ([9652f25](https://gitlab.com/fi-sas/backoffice/commit/9652f25ca730207592bdf9b35b0771f9a1a44426))
* **payments_services:** add experiences service ([688597d](https://gitlab.com/fi-sas/backoffice/commit/688597db7abea922fccd44b428fdd8ed8dbb3fa4))
* **payments_services:** add payment_grid ([15f7b9f](https://gitlab.com/fi-sas/backoffice/commit/15f7b9fd5f66a6dbf14ee480662c9ef9215cfd4a))
* **social-support:** add routing ([197c3f7](https://gitlab.com/fi-sas/backoffice/commit/197c3f757991d6c425c98ab78a1d81026119afc2))

## [1.26.1](https://gitlab.com/fi-sas/backoffice/compare/v1.26.0...v1.26.1) (2021-05-07)


### Bug Fixes

* **header:** add link to image ([32b116a](https://gitlab.com/fi-sas/backoffice/commit/32b116a21407ed77cb3e7896d76191c4496c934b))
* **infrastructures:** fix reported errors ([655b2bd](https://gitlab.com/fi-sas/backoffice/commit/655b2bd93385fb9363e30d9d36668f8fa84f25bf))

# [1.26.0](https://gitlab.com/fi-sas/backoffice/compare/v1.25.0...v1.26.0) (2021-05-07)


### Bug Fixes

* **accommodation:** select pre selected room ([e6e4ac1](https://gitlab.com/fi-sas/backoffice/commit/e6e4ac15c4f9273698f64796aa1db6694f997571))


### Features

* **core:** add new SASocial Logo ([90a5f0b](https://gitlab.com/fi-sas/backoffice/commit/90a5f0b304c75b67b4410c69f766bf737244debe))

# [1.25.0](https://gitlab.com/fi-sas/backoffice/compare/v1.24.2...v1.25.0) (2021-05-06)


### Features

* **social-scholarship:** invite students to manifest interest ([9273dbe](https://gitlab.com/fi-sas/backoffice/commit/9273dbe700319cb2b4a3d3c5e2225fe580ff72f5))

## [1.24.2](https://gitlab.com/fi-sas/backoffice/compare/v1.24.1...v1.24.2) (2021-05-05)


### Bug Fixes

* **bus:** fixed corrected and added filter in 2 pages of infrastrocture ([2ba9b1e](https://gitlab.com/fi-sas/backoffice/commit/2ba9b1eb3b026a57e9b4ac1a8c4eb8fd16c8b5eb))
* **mobility:** canceled status fixed ([9b2398f](https://gitlab.com/fi-sas/backoffice/commit/9b2398fd558cc8be1abc6edd6cd018364da91f21))
* **mobility:** small changes ([40baa74](https://gitlab.com/fi-sas/backoffice/commit/40baa74dd143d4a05088d8e6bd377e70de0a2d8a))

## [1.24.1](https://gitlab.com/fi-sas/backoffice/compare/v1.24.0...v1.24.1) (2021-05-05)


### Bug Fixes

* **social-scholarship:** fix schedule ([629b2b1](https://gitlab.com/fi-sas/backoffice/commit/629b2b12a22ffad42c3364ab96e60a47631e1a15))
* **social-scholarship:** fix statistics panel ([bb6683f](https://gitlab.com/fi-sas/backoffice/commit/bb6683fae1a6f162bdbe7ccd5581adae4c6ae582))
* **social-scolarship:** fix schedule and course_year ([e7a05ba](https://gitlab.com/fi-sas/backoffice/commit/e7a05bab0b38de423af48efbc7e1e945ac95f008))

# [1.24.0](https://gitlab.com/fi-sas/backoffice/compare/v1.23.1...v1.24.0) (2021-05-05)


### Features

* **social-scholarship:** implement experience user interests list ([3e9e771](https://gitlab.com/fi-sas/backoffice/commit/3e9e771dbd9625b893536ffdc8d41162abb6327d))
* **social-scholarship:** implement unpublish experience ([1402e12](https://gitlab.com/fi-sas/backoffice/commit/1402e1223829db3551dcf29136bfb2e6c4c5e9f0))

## [1.23.1](https://gitlab.com/fi-sas/backoffice/compare/v1.23.0...v1.23.1) (2021-05-05)


### Bug Fixes

* **social-scholarship:** fix missing models ([f6e9b9e](https://gitlab.com/fi-sas/backoffice/commit/f6e9b9ed016ccdaa5c45b92c5134e7d5877b60c1))

# [1.23.0](https://gitlab.com/fi-sas/backoffice/compare/v1.22.0...v1.23.0) (2021-05-05)


### Bug Fixes

* **social-scholarship:** duplicate identifier ([0214edf](https://gitlab.com/fi-sas/backoffice/commit/0214edf397eae9dae31f25e0d8103b2deaabb26b))


### Features

* **social-scholarship:** application list detail ([27aa447](https://gitlab.com/fi-sas/backoffice/commit/27aa447da84baf1e16a3d008bb64e9d71444bc41))

# [1.22.0](https://gitlab.com/fi-sas/backoffice/compare/v1.21.1...v1.22.0) (2021-05-05)


### Bug Fixes

* **social-scholarship:** fix interview model ([222ae4a](https://gitlab.com/fi-sas/backoffice/commit/222ae4a5e58bb5b93ecad6f923f3adaf1d195b03))
* **social-scholarship:** fix status ([655be42](https://gitlab.com/fi-sas/backoffice/commit/655be428deb011650ec636338142be2687501e1f))


### Features

* **social_scholarship:** application report ([9e0ac6b](https://gitlab.com/fi-sas/backoffice/commit/9e0ac6b4b78dbd4e9487ac553c0b5ddd49c4eaea))
* **social_scholarship:** send application to last status ([f48db41](https://gitlab.com/fi-sas/backoffice/commit/f48db4135de7ad1a79038e3e27c258d5bc3409f9))
* **social-scholarship:** application list ([e978236](https://gitlab.com/fi-sas/backoffice/commit/e9782365022c2a2eaca9a72e317f2964090e5ae3))
* **social-scholarship:** applications menu changes ([a483a89](https://gitlab.com/fi-sas/backoffice/commit/a483a895db597330a74519f00f1a3fb78a5fab77))
* **social-scholarship:** interview schedule ([1c27e99](https://gitlab.com/fi-sas/backoffice/commit/1c27e99a5c5ac25cb03c62d17852ca24f7436857))
* **social-scholarship:** interview schedule ([444c929](https://gitlab.com/fi-sas/backoffice/commit/444c92953114094b0fabe22392969427f3a77029))
* **social-scholarship:** schedule interview ([da2bf1e](https://gitlab.com/fi-sas/backoffice/commit/da2bf1e44749f9b0bf7349a1f3eb8a76538ba698))
* **social-scholarship:** state machine ([8414a73](https://gitlab.com/fi-sas/backoffice/commit/8414a7340eb83ce24cf907b8981000c8ed88e617))

## [1.21.1](https://gitlab.com/fi-sas/backoffice/compare/v1.21.0...v1.21.1) (2021-05-05)


### Bug Fixes

* **configurations:** info form loading addition ([4b06e35](https://gitlab.com/fi-sas/backoffice/commit/4b06e359f96ba310d80a7b816f3f7442e2509b89))

# [1.21.0](https://gitlab.com/fi-sas/backoffice/compare/v1.20.3...v1.21.0) (2021-05-04)


### Features

* **accommodation:** info form ([174629d](https://gitlab.com/fi-sas/backoffice/commit/174629dbaa386b68d3e39e76e3837bf651d11a62))

## [1.20.3](https://gitlab.com/fi-sas/backoffice/compare/v1.20.2...v1.20.3) (2021-05-03)


### Bug Fixes

* **private accommodation:** selet fixed ([4b81daa](https://gitlab.com/fi-sas/backoffice/commit/4b81daacff268402cb031a88021d79b1b2e9b19c))

## [1.20.2](https://gitlab.com/fi-sas/backoffice/compare/v1.20.1...v1.20.2) (2021-05-03)


### Bug Fixes

* **accommodation:** final part of accommodation ([6b43c90](https://gitlab.com/fi-sas/backoffice/commit/6b43c9055da44353b3d33935b86d139cdc635a38))

## [1.20.1](https://gitlab.com/fi-sas/backoffice/compare/v1.20.0...v1.20.1) (2021-05-03)


### Bug Fixes

* **infrastocture:** deleted logs ([dec7876](https://gitlab.com/fi-sas/backoffice/commit/dec78766b6b623bc4468fcb48a1710cf05f74c03))
* **infrastroctures:** clean file fix ([c100845](https://gitlab.com/fi-sas/backoffice/commit/c100845176b907e2982acb92180bb39e21ec605d))
* **infrastroctures:** pages rebuilt and fixed, also fix in U-bike ([196fc75](https://gitlab.com/fi-sas/backoffice/commit/196fc758fb35c395088bccf6546e7a1f5ee2cbcb))
* **infrastroctures:** shared module fixed ([41b7a38](https://gitlab.com/fi-sas/backoffice/commit/41b7a38b4a838bad5105099243cbd4c199a816d8))

# [1.20.0](https://gitlab.com/fi-sas/backoffice/compare/v1.19.0...v1.20.0) (2021-05-03)


### Bug Fixes

* **social-scholarship:** fix academic year offset ([687d119](https://gitlab.com/fi-sas/backoffice/commit/687d11939e51051a7b10496e5516ac912f61166b))


### Features

* **social-scholarship:** expire all applications ([867dd4d](https://gitlab.com/fi-sas/backoffice/commit/867dd4d7f7fbc50a122e8afe81b601ca7719f7de))

# [1.19.0](https://gitlab.com/fi-sas/backoffice/compare/v1.18.2...v1.19.0) (2021-05-03)


### Features

* **social-scholarship:** statistics panel ([306783f](https://gitlab.com/fi-sas/backoffice/commit/306783f752bd67852f0161f9a3ab42c28cc85bfa))

## [1.18.2](https://gitlab.com/fi-sas/backoffice/compare/v1.18.1...v1.18.2) (2021-04-30)


### Bug Fixes

* **volunteering:** fix scopes of volunteering module ([ee4c72a](https://gitlab.com/fi-sas/backoffice/commit/ee4c72a757b175c03e8dd2acee22524213a35bc8))

## [1.18.1](https://gitlab.com/fi-sas/backoffice/compare/v1.18.0...v1.18.1) (2021-04-30)


### Bug Fixes

* **accommodation:** permissions ([8d71285](https://gitlab.com/fi-sas/backoffice/commit/8d71285ed77cc94a6dd97eeb00f46d09dcfe993c))

# [1.18.0](https://gitlab.com/fi-sas/backoffice/compare/v1.17.2...v1.18.0) (2021-04-30)


### Features

* **social-scholarship:** add status to experience users interest list ([e41509e](https://gitlab.com/fi-sas/backoffice/commit/e41509ef5994589230981a8a90db2a5186a4dd1b))
* **social-scholarship:** implement experience state machine ([c509046](https://gitlab.com/fi-sas/backoffice/commit/c50904619e43f46043a5c4345d12b02122817d04))

## [1.17.2](https://gitlab.com/fi-sas/backoffice/compare/v1.17.1...v1.17.2) (2021-04-28)


### Bug Fixes

* **accommodation:** correction of errors ([dc6714c](https://gitlab.com/fi-sas/backoffice/commit/dc6714cf27607cc54b623c333d3995f5b9d1a7b1))

## [1.17.1](https://gitlab.com/fi-sas/backoffice/compare/v1.17.0...v1.17.1) (2021-04-28)


### Bug Fixes

* **accommodation:** final accommodation changes ([f3ea7ed](https://gitlab.com/fi-sas/backoffice/commit/f3ea7edbb7b21c4f4d8a46d2681347cf179bbf00))

# [1.17.0](https://gitlab.com/fi-sas/backoffice/compare/v1.16.2...v1.17.0) (2021-04-27)


### Bug Fixes

* **social-scholarship:** improved experience details ([be95c4e](https://gitlab.com/fi-sas/backoffice/commit/be95c4e50fd901706af7c259f62a0f72291c24ca))


### Features

* **social-scholarship:** add academic year filter to experience list ([ec1107d](https://gitlab.com/fi-sas/backoffice/commit/ec1107dd38bc747f6f2f5c224c7965c3aba40266))
* **social-scholarship:** review experiences create/edit form ([181a397](https://gitlab.com/fi-sas/backoffice/commit/181a39751eb9c8c94f5187f94d14fd0e7bc15162))

## [1.16.2](https://gitlab.com/fi-sas/backoffice/compare/v1.16.1...v1.16.2) (2021-04-27)


### Bug Fixes

* **bus:** fix bus applications list ([cd321f0](https://gitlab.com/fi-sas/backoffice/commit/cd321f0bf737b4bbab95a0a7dfb2c848c93e36aa))

## [1.16.1](https://gitlab.com/fi-sas/backoffice/compare/v1.16.0...v1.16.1) (2021-04-26)


### Bug Fixes

* **accommodation:** new change step 5 form ([5031bd1](https://gitlab.com/fi-sas/backoffice/commit/5031bd1a739316a4f530fc8dd28d83931b9fb2cc))

# [1.16.0](https://gitlab.com/fi-sas/backoffice/compare/v1.15.4...v1.16.0) (2021-04-26)


### Features

* **users:** remove passwornd pin from user form ([6cc6be5](https://gitlab.com/fi-sas/backoffice/commit/6cc6be5e5b01b60fa6a2fcc26a7a8a6faf8008aa))

## [1.15.4](https://gitlab.com/fi-sas/backoffice/compare/v1.15.3...v1.15.4) (2021-04-26)


### Bug Fixes

* **private-accommodation:** fix configurations form ([ea28a1a](https://gitlab.com/fi-sas/backoffice/commit/ea28a1af0329cb2a18cc4e1ec94a4b9d3f80c38b))

## [1.15.3](https://gitlab.com/fi-sas/backoffice/compare/v1.15.2...v1.15.3) (2021-04-23)


### Bug Fixes

* **accommodation:** review and more fields ([79c1c6c](https://gitlab.com/fi-sas/backoffice/commit/79c1c6cbc354e8f061caac36138ffcd136da611e))

## [1.15.2](https://gitlab.com/fi-sas/backoffice/compare/v1.15.1...v1.15.2) (2021-04-23)


### Bug Fixes

* **infra:** fixes done ([d80cef0](https://gitlab.com/fi-sas/backoffice/commit/d80cef03ed1978e1cf2048a9a4a1e467b9a89e51))
* **ubike:** all pages ajusted and fixed ([5b06567](https://gitlab.com/fi-sas/backoffice/commit/5b06567372cd454b4f5a8b8d6ff6608e996c5a1c))
* **ubike:** changes done ([4b2e711](https://gitlab.com/fi-sas/backoffice/commit/4b2e711e19153814c52a7b9e0e0fa32e254fa14c))
* **ubike:** validators fixed ([876d418](https://gitlab.com/fi-sas/backoffice/commit/876d4181d5b83cb2c814f06acfe453004d224b87))

## [1.15.1](https://gitlab.com/fi-sas/backoffice/compare/v1.15.0...v1.15.1) (2021-04-21)


### Bug Fixes

* **accommodation:** step 5 application ([bcc1598](https://gitlab.com/fi-sas/backoffice/commit/bcc1598f078f504c51287657b4e11985fb087348))

# [1.15.0](https://gitlab.com/fi-sas/backoffice/compare/v1.14.8...v1.15.0) (2021-04-21)


### Features

* **social-scholarship:** changed checkbox filters to yes/no select ([b7a99b1](https://gitlab.com/fi-sas/backoffice/commit/b7a99b157cac7289773848b2e082805648a7bb15))

## [1.14.8](https://gitlab.com/fi-sas/backoffice/compare/v1.14.7...v1.14.8) (2021-04-21)


### Bug Fixes

* **configurations:** change academic year model name ([e7eec3b](https://gitlab.com/fi-sas/backoffice/commit/e7eec3b27b94af2ad6a40cc6599ef56e470532a4))

## [1.14.7](https://gitlab.com/fi-sas/backoffice/compare/v1.14.6...v1.14.7) (2021-04-20)


### Bug Fixes

* **accommodation:** validations step 3, settings ([e8fb1f4](https://gitlab.com/fi-sas/backoffice/commit/e8fb1f407b221bdd4cc360a864dbd7be53523ff4))

## [1.14.6](https://gitlab.com/fi-sas/backoffice/compare/v1.14.5...v1.14.6) (2021-04-19)


### Bug Fixes

* **accommodation:** step 2 application ([0ffa34a](https://gitlab.com/fi-sas/backoffice/commit/0ffa34ab3a4524295a6f9604daccaa94e4589764))

## [1.14.5](https://gitlab.com/fi-sas/backoffice/compare/v1.14.4...v1.14.5) (2021-04-19)


### Bug Fixes

* **accommodation:** step 1 of the application ([af96206](https://gitlab.com/fi-sas/backoffice/commit/af962064fffaf660e99e13e438732dc4d0fe0b4c))

## [1.14.4](https://gitlab.com/fi-sas/backoffice/compare/v1.14.3...v1.14.4) (2021-04-16)


### Bug Fixes

* **accommodation:** step 4 application ([f2be878](https://gitlab.com/fi-sas/backoffice/commit/f2be8783a34da1ba2adf9e3f78c420d570679f9a))
* **accommodation:** step 4 application ([3829570](https://gitlab.com/fi-sas/backoffice/commit/382957005f7722ca32f97d8db939fbad1019f275))

## [1.14.3](https://gitlab.com/fi-sas/backoffice/compare/v1.14.2...v1.14.3) (2021-04-15)


### Bug Fixes

* **accommodation:** regime and extras ([68f2245](https://gitlab.com/fi-sas/backoffice/commit/68f2245f3652cd183b95130b3e1bfcfded5a0959))

## [1.14.2](https://gitlab.com/fi-sas/backoffice/compare/v1.14.1...v1.14.2) (2021-04-15)


### Bug Fixes

* **users:** fix user form ([34009ac](https://gitlab.com/fi-sas/backoffice/commit/34009ac4475edbe281d95f97059397c1c9245cc5))

## [1.14.1](https://gitlab.com/fi-sas/backoffice/compare/v1.14.0...v1.14.1) (2021-04-14)


### Bug Fixes

* **communication:** add feedback on post status chage ([9dcb369](https://gitlab.com/fi-sas/backoffice/commit/9dcb369c35c0b501b1c06facceabf8ed0200dfa9))

# [1.14.0](https://gitlab.com/fi-sas/backoffice/compare/v1.13.0...v1.14.0) (2021-04-13)


### Features

* **users:** add new filters to users list ([5008395](https://gitlab.com/fi-sas/backoffice/commit/50083956abee83aadf8c928c4770ed6f817a4634))

# [1.13.0](https://gitlab.com/fi-sas/backoffice/compare/v1.12.9...v1.13.0) (2021-04-12)


### Bug Fixes

* **users:** fix various users module issues ([47097d9](https://gitlab.com/fi-sas/backoffice/commit/47097d98dbb6bcb4c5501f881fae592bb5302e74))


### Features

* **users:** add new user form validations ([7ecced5](https://gitlab.com/fi-sas/backoffice/commit/7ecced56f96233a31534bf266ba08ddccbdd9b12))

## [1.12.9](https://gitlab.com/fi-sas/backoffice/compare/v1.12.8...v1.12.9) (2021-04-12)


### Bug Fixes

* **bulk_menus:** update dish type and list ([8c09f57](https://gitlab.com/fi-sas/backoffice/commit/8c09f576d5e5034ce86858e19ca3be51491e71ca))
* **dish_model:** update dish model ([19d75e7](https://gitlab.com/fi-sas/backoffice/commit/19d75e7d2568dfc8800421d8e3f002f846d0a87a))
* **form dishs:** update dishs ([9086395](https://gitlab.com/fi-sas/backoffice/commit/9086395ac5537f340a44e9cb369434b7d5bc4523))
* **form_menu:** update dish type ([1b66298](https://gitlab.com/fi-sas/backoffice/commit/1b6629878d0ff18610d840747c7d96259c351d59))
* **form_menus:** filter dish_type ([64dfc51](https://gitlab.com/fi-sas/backoffice/commit/64dfc518cfc46d4f4f50f06bc012d487cee98f7f))
* **list-dishs:** update list of dishs ([85f7ecd](https://gitlab.com/fi-sas/backoffice/commit/85f7ecd753a7d857440e161ae6cd19904321bed3))
* **show_menu:** fix html ([7584d10](https://gitlab.com/fi-sas/backoffice/commit/7584d10d49692e77d8ec1894c297e3359c952d16))
* **view_dishs:** update view dishs info ([574a501](https://gitlab.com/fi-sas/backoffice/commit/574a5019748eb76b30708e3410e2dea7bb4a6898))

## [1.12.8](https://gitlab.com/fi-sas/backoffice/compare/v1.12.7...v1.12.8) (2021-04-12)


### Bug Fixes

* **infrastrocture:** infrastrocture pages fixed ([152a70c](https://gitlab.com/fi-sas/backoffice/commit/152a70c4602ff4921d0bd344a290ef0dff635d29))
* **infrastroctures:** filter fixed waiting creation plan decision ([da5b727](https://gitlab.com/fi-sas/backoffice/commit/da5b7273c85fcbd1d5b62644ca0f6d18a2c26a72))
* **infrastroctures:** probems fixed added filters waiting one MS check ([0818aff](https://gitlab.com/fi-sas/backoffice/commit/0818aff11ca64673fb52a44c06cdb1639f22aebd))

## [1.12.7](https://gitlab.com/fi-sas/backoffice/compare/v1.12.6...v1.12.7) (2021-04-07)


### Bug Fixes

* **accommodation:** change misspelled descriptions ([299cc9f](https://gitlab.com/fi-sas/backoffice/commit/299cc9f459dcf18396fa8e06fba6d14274b0676a))
* **accommodation:** change range picker to two date pickers ([a0a5ada](https://gitlab.com/fi-sas/backoffice/commit/a0a5ada7b34a7fe17d1658edd834328dd4e73dd1))
* **alimentation:** correction error hour ([562c9b6](https://gitlab.com/fi-sas/backoffice/commit/562c9b687ff6147e2df6ee89fef6a8f805ebd23f))

## [1.12.6](https://gitlab.com/fi-sas/backoffice/compare/v1.12.5...v1.12.6) (2021-04-07)


### Bug Fixes

* **accommodation:** remove unused type of document on application reports ([c62f2cf](https://gitlab.com/fi-sas/backoffice/commit/c62f2cf07958ef6049a85ee80d086ec87ceb3484))
* **accomodation:** remove disabled previous dates on edit application ([52ab97e](https://gitlab.com/fi-sas/backoffice/commit/52ab97e715ba583e9198a9cc31a3a8ec81b62159))

## [1.12.5](https://gitlab.com/fi-sas/backoffice/compare/v1.12.4...v1.12.5) (2021-04-07)


### Bug Fixes

* **ubike:** multiple fixes on various pages ([60630fb](https://gitlab.com/fi-sas/backoffice/commit/60630fbb2ad1aaecd70ca7103f931a35e9700ae1))

## [1.12.4](https://gitlab.com/fi-sas/backoffice/compare/v1.12.3...v1.12.4) (2021-04-05)


### Bug Fixes

* **alimentation:** correction error type dish ([d139d16](https://gitlab.com/fi-sas/backoffice/commit/d139d16dd20057ee935faa321fe8de66d2950b9a))

## [1.12.3](https://gitlab.com/fi-sas/backoffice/compare/v1.12.2...v1.12.3) (2021-04-01)


### Bug Fixes

* **shared:** fix auto upload comonent on null file id ([1185fdc](https://gitlab.com/fi-sas/backoffice/commit/1185fdca7de8ca4c7a128b0e7fd290baba1be237))

## [1.12.2](https://gitlab.com/fi-sas/backoffice/compare/v1.12.1...v1.12.2) (2021-03-30)


### Bug Fixes

* **currentaccount:** charge modal fixed ([498b4a0](https://gitlab.com/fi-sas/backoffice/commit/498b4a0464c6d3e121c8a1cf439b55a130520ee6))

## [1.12.1](https://gitlab.com/fi-sas/backoffice/compare/v1.12.0...v1.12.1) (2021-03-30)


### Bug Fixes

* **core:** fix sider permssion pipe ([afcc4ef](https://gitlab.com/fi-sas/backoffice/commit/afcc4efbcad7c9c8ed65b80696700ff88295d0dd))

# [1.12.0](https://gitlab.com/fi-sas/backoffice/compare/v1.11.22...v1.12.0) (2021-03-30)


### Features

* **shared:** add has permission pipe ([3e81f16](https://gitlab.com/fi-sas/backoffice/commit/3e81f161cc884105e4e6d760511b0f08bb4d36bd))

## [1.11.22](https://gitlab.com/fi-sas/backoffice/compare/v1.11.21...v1.11.22) (2021-03-29)


### Bug Fixes

* **meal:** problem selection dish ([a5b08ac](https://gitlab.com/fi-sas/backoffice/commit/a5b08acc3e38e4716e940f4c4feb34ef48af8167))

## [1.11.21](https://gitlab.com/fi-sas/backoffice/compare/v1.11.20...v1.11.21) (2021-03-26)


### Bug Fixes

* **queue:** edition and creation fixed and added groups ([20ba9c8](https://gitlab.com/fi-sas/backoffice/commit/20ba9c80311ef9612f96b5e62a337fff37f208df))
* **queue:** small fixes, waiting for furder progress ([9f16f0d](https://gitlab.com/fi-sas/backoffice/commit/9f16f0d419007bcddd0b19d60b7dcd2fe45af03a))

## [1.11.20](https://gitlab.com/fi-sas/backoffice/compare/v1.11.19...v1.11.20) (2021-03-26)


### Bug Fixes

* **multiple pages:** orthography error of gender ([96d1c45](https://gitlab.com/fi-sas/backoffice/commit/96d1c45a8131a60059853586d7f784b034cfa91b))

## [1.11.19](https://gitlab.com/fi-sas/backoffice/compare/v1.11.18...v1.11.19) (2021-03-26)


### Bug Fixes

* **dahsboard:** update scopes for modules ([ce9a3c1](https://gitlab.com/fi-sas/backoffice/commit/ce9a3c1087ba2337323a057df4d07e1fce7954e8))

## [1.11.18](https://gitlab.com/fi-sas/backoffice/compare/v1.11.17...v1.11.18) (2021-03-24)


### Bug Fixes

* **ubike:** fix monitoring and reports errors ([dbd5b3e](https://gitlab.com/fi-sas/backoffice/commit/dbd5b3e2b56fe6f3595a9d6b9f9973e25a47e821))

## [1.11.17](https://gitlab.com/fi-sas/backoffice/compare/v1.11.16...v1.11.17) (2021-03-24)


### Bug Fixes

* **configurations:** add POS device type ([ea0a177](https://gitlab.com/fi-sas/backoffice/commit/ea0a177354d752a0050ee987b1c79a2d843a5144))

## [1.11.16](https://gitlab.com/fi-sas/backoffice/compare/v1.11.15...v1.11.16) (2021-03-24)


### Bug Fixes

* **finantil:** documents list and edition added ([2866c0b](https://gitlab.com/fi-sas/backoffice/commit/2866c0b43f8390911be3e9b7eb7446e67d2029da))

## [1.11.15](https://gitlab.com/fi-sas/backoffice/compare/v1.11.14...v1.11.15) (2021-03-24)


### Bug Fixes

* **private-accommodation:** fox map zoom on listing form ([d87faf5](https://gitlab.com/fi-sas/backoffice/commit/d87faf5c8a4611c0d70167b73eb6407322cd5602))

## [1.11.14](https://gitlab.com/fi-sas/backoffice/compare/v1.11.13...v1.11.14) (2021-03-24)


### Bug Fixes

* **communication:** add new posts filters ([ffc1607](https://gitlab.com/fi-sas/backoffice/commit/ffc1607b5e55cf73d58edb67bbefcdc341bcb0d8))

## [1.11.13](https://gitlab.com/fi-sas/backoffice/compare/v1.11.12...v1.11.13) (2021-03-23)


### Bug Fixes

* **ubike:** small errors fixed and extra layout implemented ([cccc5fa](https://gitlab.com/fi-sas/backoffice/commit/cccc5fa725ee2e735152de49adc4b40e2f7e2059))

## [1.11.12](https://gitlab.com/fi-sas/backoffice/compare/v1.11.11...v1.11.12) (2021-03-23)


### Bug Fixes

* **aliementation:** add text search on units and allergends ([49be0e5](https://gitlab.com/fi-sas/backoffice/commit/49be0e587c82913f3ae86b396afc8d7bb94b9a6c))

## [1.11.11](https://gitlab.com/fi-sas/backoffice/compare/v1.11.10...v1.11.11) (2021-03-17)


### Bug Fixes

* **queue:** service tradution name ([9ee3978](https://gitlab.com/fi-sas/backoffice/commit/9ee3978c6e0733ed6c62b59af441630b7033d671))

## [1.11.10](https://gitlab.com/fi-sas/backoffice/compare/v1.11.9...v1.11.10) (2021-03-17)


### Bug Fixes

* **queue:** correction error trad desk management ([284a577](https://gitlab.com/fi-sas/backoffice/commit/284a577c2359c95a01320f3bf5fa2bef93b98eae))

## [1.11.9](https://gitlab.com/fi-sas/backoffice/compare/v1.11.8...v1.11.9) (2021-03-16)


### Bug Fixes

* **queue:** correction of translations ([a6ebf22](https://gitlab.com/fi-sas/backoffice/commit/a6ebf226b764295778a43dd501cb9edef5dfa57e))

## [1.11.8](https://gitlab.com/fi-sas/backoffice/compare/v1.11.7...v1.11.8) (2021-03-16)


### Bug Fixes

* **queue:** maxlength 2 desk ([e3f6317](https://gitlab.com/fi-sas/backoffice/commit/e3f6317392786e39efa68d0e73486a736487d054))

## [1.11.7](https://gitlab.com/fi-sas/backoffice/compare/v1.11.6...v1.11.7) (2021-03-15)


### Bug Fixes

* **queue:** max lenght name ([5df4c96](https://gitlab.com/fi-sas/backoffice/commit/5df4c96cf4e48ee3a3cf320889016da53d75ae5a))

## [1.11.6](https://gitlab.com/fi-sas/backoffice/compare/v1.11.5...v1.11.6) (2021-03-12)


### Bug Fixes

* **queue:** improvements configurations ([e85099b](https://gitlab.com/fi-sas/backoffice/commit/e85099b6b56a9289662c1e3c84c7ac9d26bf8b96))

## [1.11.5](https://gitlab.com/fi-sas/backoffice/compare/v1.11.4...v1.11.5) (2021-03-12)


### Bug Fixes

* **queue:** add missing tickets pagination and some miner fixs ([7566e8b](https://gitlab.com/fi-sas/backoffice/commit/7566e8be127d07d001c9087aa0f13d3ce20404d7))

## [1.11.4](https://gitlab.com/fi-sas/backoffice/compare/v1.11.3...v1.11.4) (2021-03-11)


### Bug Fixes

* **ubike:** fixes in form and orthographic mistakes ([2ebde77](https://gitlab.com/fi-sas/backoffice/commit/2ebde775c53e04f478f3e527e9c2643e9d3fddad))

## [1.11.3](https://gitlab.com/fi-sas/backoffice/compare/v1.11.2...v1.11.3) (2021-03-11)


### Bug Fixes

* **indraestrotura:** build fixed ([af52bf0](https://gitlab.com/fi-sas/backoffice/commit/af52bf0de496c9b8d70d0392a77595a312e4d3bf))

## [1.11.2](https://gitlab.com/fi-sas/backoffice/compare/v1.11.1...v1.11.2) (2021-03-11)


### Bug Fixes

* **users-groups:** fix user grousp form withRelated ([ed15dba](https://gitlab.com/fi-sas/backoffice/commit/ed15dba467728cbf7c0c6c42dd51f30eba9ebc8b))

## [1.11.1](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0...v1.11.1) (2021-03-10)


### Bug Fixes

* **infrastrocures:** pages all almost completed, waiting for more info ([8b24576](https://gitlab.com/fi-sas/backoffice/commit/8b24576192a4c1d08ebf559201911773540a407c))
* **ubike:** different errors on pages fixed ([e2ad7f5](https://gitlab.com/fi-sas/backoffice/commit/e2ad7f5fc10a510ceef6ae57e3f124c0e80c0389))

# [1.11.0](https://gitlab.com/fi-sas/backoffice/compare/v1.10.3...v1.11.0) (2021-03-09)


### Bug Fixes

* **accommodation:** add rejected and cancel status to withdraws and ext ([84b19f9](https://gitlab.com/fi-sas/backoffice/commit/84b19f9e40c5f365ff7e42854fb4f290b5e9d89f))
* **accommodation:** many texts fixs ([b5d10b5](https://gitlab.com/fi-sas/backoffice/commit/b5d10b521acce82c8fe2e7d6bca8cfb301c765a8))
* **accommodation:** minor fix to all module ([16eccbb](https://gitlab.com/fi-sas/backoffice/commit/16eccbb3b80a3954b969d64e40acd83c9a656b36))
* **accommodation:** minor fixs ([19cb5a6](https://gitlab.com/fi-sas/backoffice/commit/19cb5a66d18545e7cdf0582da0e397d2e3f5b802))
* **alimentation:** add entity_id in header ([2e0eac6](https://gitlab.com/fi-sas/backoffice/commit/2e0eac6669df4c2862311dd98d1f7ed46238e525))
* **alimentation:** fix list and form dishs ([4ca378b](https://gitlab.com/fi-sas/backoffice/commit/4ca378be04c07b782a354b9c01cad99b5f5d5562))
* **alimentation:** fix list of reservations ([7dd3797](https://gitlab.com/fi-sas/backoffice/commit/7dd37972d9e808bf8c18f9571f1805f842da94e1))
* **alimentation:** list and action of orders ([3947366](https://gitlab.com/fi-sas/backoffice/commit/3947366985f5c9299e1a80f1e38e194c053ad4b3))
* **alimentation:** list and forms fro create meals ([ad25e13](https://gitlab.com/fi-sas/backoffice/commit/ad25e133e1496356eaf9e2a9dd2ff36917f71778))
* **alimentation:** prices list ([6a87242](https://gitlab.com/fi-sas/backoffice/commit/6a872424f59461a36c475b9fb384c461812ef939))
* **alimentation:** remover comments ([1b5b411](https://gitlab.com/fi-sas/backoffice/commit/1b5b411b7d6d0ada8581e0068d09dfac38e8603b))
* **alimentation:** small errors ([c81212e](https://gitlab.com/fi-sas/backoffice/commit/c81212e1b6a8f94096117d847e092266d1267da6))
* **alimentation:** update components ([6bc887b](https://gitlab.com/fi-sas/backoffice/commit/6bc887baa99b3a0a63c0cb69b5728a9ec3918cd2))
* **appconfig:** update endpoints of alimentation ([d9b5325](https://gitlab.com/fi-sas/backoffice/commit/d9b5325279d1ef8d675644c5979e4c8bc659851e))
* **communication:** fix add language and order language problems ([135e945](https://gitlab.com/fi-sas/backoffice/commit/135e94579f151fb95227d635d47e8bab9451e4e8))
* **communication:** remove required on date in posts form ([6fe64b6](https://gitlab.com/fi-sas/backoffice/commit/6fe64b665a3a76dd045a317dd019839664c9f40c))
* **configurations:** new generic device ([74a6625](https://gitlab.com/fi-sas/backoffice/commit/74a66256158b5d6bdd8448d00bd033a7ffe57573))
* **configurations:** resolve error length of undefined ([9f24759](https://gitlab.com/fi-sas/backoffice/commit/9f24759b679868663598b2528d95d9ae2db751d7))
* **private accommodation:** errors fixed, waiting for data to test ([4c3692e](https://gitlab.com/fi-sas/backoffice/commit/4c3692e682d8ae3004711288da3d437d2b3ea196))
* **queue:** fixed operar balcao, fixed data errors, waiting ms updates ([b02f42d](https://gitlab.com/fi-sas/backoffice/commit/b02f42dde6d93eff050ff9ce3d87b8c887ba2ebf))
* **u-bike:** list trips page fixed and functionalities added ([cb3bed9](https://gitlab.com/fi-sas/backoffice/commit/cb3bed95dd6859995c4b3bc0d90aabb9118eebe0))
* accommodation, regulations, queue ([39f7625](https://gitlab.com/fi-sas/backoffice/commit/39f76250b4554631e1e961393adbbdd1d70b4c67))
* accommodation,volunteering,current account ([edf98b1](https://gitlab.com/fi-sas/backoffice/commit/edf98b1aaffa42706232d03c8571acff46eb56dd))
* adding new file ([b9e7cd5](https://gitlab.com/fi-sas/backoffice/commit/b9e7cd5777409849f74f9298ea398723430398d5))
* change proxy to suport http only cookie ([5ac3744](https://gitlab.com/fi-sas/backoffice/commit/5ac3744d47298887f0e135b16fbd604dd960ec04))
* correction error pagination mobile ([eed78ea](https://gitlab.com/fi-sas/backoffice/commit/eed78eaed5df0df5d4ff2f34a0065d7b91f31124))
* current account fixed, waiting for final endpoint ([30936e1](https://gitlab.com/fi-sas/backoffice/commit/30936e150a39260284cedc4c791918a378843fdd))
* current account history confirmed and small changes ([011becb](https://gitlab.com/fi-sas/backoffice/commit/011becbf905e85d6f3c02987a5cbaef562952a2a))
* fixed canceling ([97a1758](https://gitlab.com/fi-sas/backoffice/commit/97a1758e7c94cbd7338b319141d02ea91de8da83))
* form allergens ([8066788](https://gitlab.com/fi-sas/backoffice/commit/80667889fae9bcedf52c048f05f88c6e89af0c23))
* form and list of product ([1412764](https://gitlab.com/fi-sas/backoffice/commit/1412764b8fee0b34c6b656edadd53f4884e34fd8))
* form nutrients ([de56795](https://gitlab.com/fi-sas/backoffice/commit/de5679516089dd7dbf955c7c246c3f9780f4ef58))
* form services ([adb5989](https://gitlab.com/fi-sas/backoffice/commit/adb59890c9071990f11f68cbc486d2b7c818d351))
* form unit ([28283a3](https://gitlab.com/fi-sas/backoffice/commit/28283a3ac8aa9684a77cfe8fe528697d75064f91))
* form units ([8393f93](https://gitlab.com/fi-sas/backoffice/commit/8393f93c6230c51d0b0b165effdf3e3027d07628))
* form wharehouse ([e2b96d5](https://gitlab.com/fi-sas/backoffice/commit/e2b96d5478fec01db0e1ad36d5f7345707093d6e))
* form, list and view families ([fd40d04](https://gitlab.com/fi-sas/backoffice/commit/fd40d0471bbfb34ea5805d373cd5422a823ecd24))
* form, list and view of entities ([b45f9fb](https://gitlab.com/fi-sas/backoffice/commit/b45f9fb0835ccc7069a9a29fef1af2e6adddbfb5))
* forms, list and view of complements ([2f9d5da](https://gitlab.com/fi-sas/backoffice/commit/2f9d5da02703791755da525df2127ad3bb94a6d4))
* important modules ([69e4f55](https://gitlab.com/fi-sas/backoffice/commit/69e4f551e7366b7f61487ba7ed2adc0e2ac81322))
* list allergens ([a7a3f9e](https://gitlab.com/fi-sas/backoffice/commit/a7a3f9ec428d0f65614be0264353707553b3c8bf))
* list and form of dish_type ([d650c23](https://gitlab.com/fi-sas/backoffice/commit/d650c2382ebc0946e913b1ba22043d162e598bd0))
* list and view of wharehouse ([109d33e](https://gitlab.com/fi-sas/backoffice/commit/109d33e5be800d4aed3380e1b63a0b80f8f20bcf))
* list nutrients ([771d7ad](https://gitlab.com/fi-sas/backoffice/commit/771d7ad2f7183b00a3282b590f86b0806429e671))
* list products ([9ca3a16](https://gitlab.com/fi-sas/backoffice/commit/9ca3a16fdc1e43471eb4a8d3b941aa04b95daba7))
* list recipes and form ([86c276b](https://gitlab.com/fi-sas/backoffice/commit/86c276bb54ce536377531181466e2f8bc578222d))
* list services ([6d326bd](https://gitlab.com/fi-sas/backoffice/commit/6d326bda63867dca05e5af4e9e74e38bf3ff25b6))
* list units ([718759b](https://gitlab.com/fi-sas/backoffice/commit/718759b703411d5fc651c0df94f5eb797307d393))
* payments completed, waiting for more UI ([6154571](https://gitlab.com/fi-sas/backoffice/commit/6154571f8b8212f65e49cd60b2f510151d52f067))
* small fix on request ([5cfb94f](https://gitlab.com/fi-sas/backoffice/commit/5cfb94f15870e6300f7b9a1810ebce0aaa18b8b3))
* start of cancelation (waiting for repairs on UI) ([e502323](https://gitlab.com/fi-sas/backoffice/commit/e5023234fe6b99618a9b65abf6185213c783270f))
* u-bike applications and date controls ([eba3021](https://gitlab.com/fi-sas/backoffice/commit/eba30219c9ae9ddce259ebf74e41b5e2e5d75957))
* **alojamentoprivado:** form property ([000bee1](https://gitlab.com/fi-sas/backoffice/commit/000bee12a82a8c4626ed88de3cd125ef18f9261d))
* **bolsa:** text apresentation configuration ([a382698](https://gitlab.com/fi-sas/backoffice/commit/a382698e13c9ad9600d550bbe3ff2c991eaac4f0))
* **configurations:** resolve build errors ([14f2282](https://gitlab.com/fi-sas/backoffice/commit/14f2282762a9f754ed30acb24313aaad29ab80b3))
* **geral:** remove school ([ed40bbb](https://gitlab.com/fi-sas/backoffice/commit/ed40bbb8dd3eefb4c0ca389daf76ac8215e3ddd1))
* new merge request ([bc8dc84](https://gitlab.com/fi-sas/backoffice/commit/bc8dc84e5b5da4a3d1b0274b5990ff603af3d910))
* update modules and services ([692f58e](https://gitlab.com/fi-sas/backoffice/commit/692f58e784ed2f872db7339aea50be94d9a2fba4))
* **accommodation:** cancel application form ([43d7cfa](https://gitlab.com/fi-sas/backoffice/commit/43d7cfaa151286d98f40c4264bc8be63f6df6d14))
* **accommodation:** fix scopes ([655357d](https://gitlab.com/fi-sas/backoffice/commit/655357dc31b33ef0d5bee79fd5c2c17f4223b080))
* **accommodation:** lateral menu texts ([4a6253f](https://gitlab.com/fi-sas/backoffice/commit/4a6253fae584fcb76219d2eeeafcf6e7aeefe162))
* **accommodation:** minor fixs ([9ef412d](https://gitlab.com/fi-sas/backoffice/commit/9ef412dbbf6fd6e0cdb9244c12f87ef063433421))
* **accommodationprivate:** change form required ([a4e0d94](https://gitlab.com/fi-sas/backoffice/commit/a4e0d94c86e574639cfcfb0e8ff9ab1ce284738e))
* **app:** minor fixs ([b8d743f](https://gitlab.com/fi-sas/backoffice/commit/b8d743faf3b3025ed8cc188940378cacf82c93a0))
* **build:** fix post-build.js paths ([c1abb9e](https://gitlab.com/fi-sas/backoffice/commit/c1abb9e4849be9fd133e95378863d262490941fb))
* **bus:** adaptation of the backoffice to the new ms ([e43087c](https://gitlab.com/fi-sas/backoffice/commit/e43087c40aad8339c4956b5cb4ebe24da54105c6))
* **bus:** application adaptation for new ms ([55ef5e1](https://gitlab.com/fi-sas/backoffice/commit/55ef5e15b60068971b577ecc9668258b21dbdd39))
* **bus:** enable load accounts ([47e31a6](https://gitlab.com/fi-sas/backoffice/commit/47e31a6b9981575730fbcb30dd243d33d3ba49ec))
* **bus:** fix date in timetable ([57914e4](https://gitlab.com/fi-sas/backoffice/commit/57914e4a56162b0544dbbcb46290787d071bb76a))
* **bus:** fix error when insert timetable ([2c5c3f3](https://gitlab.com/fi-sas/backoffice/commit/2c5c3f30af467ef443f49ec29b0205a3f67845ba))
* **bus:** fix minor issues in links ([0b85cfb](https://gitlab.com/fi-sas/backoffice/commit/0b85cfbb6a9011c09f0819fa19f4ad0a8fd9ea9c))
* **bus:** fix price table update list ([93b7e75](https://gitlab.com/fi-sas/backoffice/commit/93b7e7546695e7b44b546bbb635cf3a028fe8047))
* **bus:** fix timetable list ([ad0ea19](https://gitlab.com/fi-sas/backoffice/commit/ad0ea1954e8ff92c0d02a660617acedc2805506f))
* **bus:** set corrent accounts ([d046cbf](https://gitlab.com/fi-sas/backoffice/commit/d046cbf5c4af227af8211b5ef127a8964618cf61))
* **bus:** set paginations list zones ([5fa66c7](https://gitlab.com/fi-sas/backoffice/commit/5fa66c7ac062ced550c645f6073dc2a6c1988ed5))
* **communication:** fix feeds offset ([5a07742](https://gitlab.com/fi-sas/backoffice/commit/5a07742b65583b891df179a3fdff9300f2ff7dc2))
* **communication:** fix pagination of posts ([d75ae11](https://gitlab.com/fi-sas/backoffice/commit/d75ae1168f5c1f51550959e7e8c51c9e86502516))
* **enums:** change to uppercase ([8127a7b](https://gitlab.com/fi-sas/backoffice/commit/8127a7b1197a9a0f2640cc0be01cd47d9bca52fe))
* **envs:** fix angular ipportalegre env ([a822a95](https://gitlab.com/fi-sas/backoffice/commit/a822a955be74bac51489032b35d67461d7db0676))
* **envs:** fix ipportalegre envs ([8b6f39f](https://gitlab.com/fi-sas/backoffice/commit/8b6f39fb1060d677c6128daf5f2e8e782ca80cb6))
* **erros:** erros are now from api ([4b5d1b2](https://gitlab.com/fi-sas/backoffice/commit/4b5d1b22d923089db4ad86d987caeca747aa9be0))
* **financial:** add fixs to new MS ([53246ac](https://gitlab.com/fi-sas/backoffice/commit/53246acf21a61d8df6f38948ef547d06eec42446))
* **food:** add changes to new version of microservice ([ae053fa](https://gitlab.com/fi-sas/backoffice/commit/ae053fa577e581712899bc8b39a85b5e011a3d25))
* **geral:** services active dashboard; error image ([444aa96](https://gitlab.com/fi-sas/backoffice/commit/444aa9669aba5515ff61862ddf1f24e142bf5a6a))
* **media:** change url medias ([bcde485](https://gitlab.com/fi-sas/backoffice/commit/bcde485786f7a796bf055fa912f7ef75012c3a59))
* **medias:** fix search param  in files ([cd30dfe](https://gitlab.com/fi-sas/backoffice/commit/cd30dfe8ca3279fa63e43fda33ffd0eb1100f658))
* **model:** gender default pt ([31a2aa1](https://gitlab.com/fi-sas/backoffice/commit/31a2aa1b4993575043ff08675904b24d22105932))
* **notifications:** minoxr fixs ([1ffc3e4](https://gitlab.com/fi-sas/backoffice/commit/1ffc3e4768054f4755511b9384c035b45c9ff9af))
* **notifications:** update alert enums ([5d27840](https://gitlab.com/fi-sas/backoffice/commit/5d278405c5a5b1307097322c51ca145d68b6cd9c))
* **package:** update ngx-mask to 7.9.10 ([c58da20](https://gitlab.com/fi-sas/backoffice/commit/c58da2038dca4cf2f76232ca8d2d15c4da6a2b8c))
* **private accommodation:** fix view listing ([e9ee55f](https://gitlab.com/fi-sas/backoffice/commit/e9ee55f63d29fa6dab73eadce3aa0d5571068893))
* **private-accommodation:** corrections for the new boilerplate ([a11d900](https://gitlab.com/fi-sas/backoffice/commit/a11d9007f4ca42bde483b35373a4289ad725961a))
* **privateAccommodation:** housing title ([5e4730e](https://gitlab.com/fi-sas/backoffice/commit/5e4730e557152e34880beb6c69ce1730f0d37328))
* **queue:** minor fix ([5d6e1e1](https://gitlab.com/fi-sas/backoffice/commit/5d6e1e17776b6bb64888772e8c257c73ee9f15a0))
* **s-scholarship:** application status uppercase ([16bfb5b](https://gitlab.com/fi-sas/backoffice/commit/16bfb5b323c45c4438f187aca3822005c361988e))
* **socialscholarship:** fix translated fields ([59f3398](https://gitlab.com/fi-sas/backoffice/commit/59f3398a0c861fe1507fc981dc2b48bfe38e83e3))
* conta Corrente UI ([5164520](https://gitlab.com/fi-sas/backoffice/commit/5164520d86cf40f5ffe25debcf2b4e5f2cb25314))
* desk-start (still waiting for ms final alterations ([ec54d95](https://gitlab.com/fi-sas/backoffice/commit/ec54d958a05eb4b22e46f83e6ce78289ce2567e4))
* environment ([a496491](https://gitlab.com/fi-sas/backoffice/commit/a496491a37c7889e027420e6ff88c3a11d1a1090))
* environments localhost ([c424d83](https://gitlab.com/fi-sas/backoffice/commit/c424d83c6268c0621657490b2b6fbc8f4e416b00))
* minor fixs in multiple modules ([3a82829](https://gitlab.com/fi-sas/backoffice/commit/3a828297c6d6e19f94f33239b0af0db29b1a685d))
* small changes in colors and other small parts - operar balcao ([004ca51](https://gitlab.com/fi-sas/backoffice/commit/004ca51a6d4c026289eefc9c78d41457718ce518))
* **private configuration:** fix owner regulation parse ([67e5201](https://gitlab.com/fi-sas/backoffice/commit/67e5201e8a21db9b03a500a76d6f19e76d31917d))
* **privateaccommodation:** max length title ([49c0e21](https://gitlab.com/fi-sas/backoffice/commit/49c0e2118dc3587e63e4321ce16fabf06a8b1530))
* **privateaccomodation:** form property ([1ad1323](https://gitlab.com/fi-sas/backoffice/commit/1ad1323fafe890a52770dd4349ce58be04234fe4))
* **queue:**  organizations folders ([b546703](https://gitlab.com/fi-sas/backoffice/commit/b546703f1ab7a0d176c6f6978243d61134a2e81a))
* **reports:** add id to templates ([3e614e0](https://gitlab.com/fi-sas/backoffice/commit/3e614e01a8b7630b0ba8c9a5f67a9e703eb6db81))
* **shared:** add validation date ([7e4ac44](https://gitlab.com/fi-sas/backoffice/commit/7e4ac44f24af6c4bf808a3759a0d9a79b3275d54))
* **shared:** fix file upload url path ([24a92ac](https://gitlab.com/fi-sas/backoffice/commit/24a92ac720a0fe549b7cacea8d9347d853293700))
* **shared:** fix upload images endpoint ([5bf9202](https://gitlab.com/fi-sas/backoffice/commit/5bf92026850ccbf1f2bcc9b2883be300951d9287))
* **social_schollarship:** minor fix for migration ([d34f4f3](https://gitlab.com/fi-sas/backoffice/commit/d34f4f38ecbd5d279cca51f0924be3b2db6235cc))
* **social_support:** errors fix on social support module ([b86f3f6](https://gitlab.com/fi-sas/backoffice/commit/b86f3f6ccc554844d3cebbc9f11eadbcb4ee3472))
* **social-support:** remove duplicate translations ([16b98c2](https://gitlab.com/fi-sas/backoffice/commit/16b98c29d17cb5d9851276b90eac4a2ecab6bb5a))
* **u_bike:** fix application loading error ([7072314](https://gitlab.com/fi-sas/backoffice/commit/707231417e135f929426beaa531cf922406be977))
* volunteering pages fixed according to ms's, waiting for 1ms fixe ([fa1cf6e](https://gitlab.com/fi-sas/backoffice/commit/fa1cf6e763f8d725adbd68fb2dce7abe2bd88ad2))
* **users:** request users relateds ([2a90733](https://gitlab.com/fi-sas/backoffice/commit/2a90733cfe92339c67e465dd51872f1d24cac496))
* create couter and service ([60628f5](https://gitlab.com/fi-sas/backoffice/commit/60628f54b89af19a5e82e1e13a8a6182196ffc5e))
* endpoints wanted_ads ([7e73871](https://gitlab.com/fi-sas/backoffice/commit/7e73871a911e96a1d848dbcc85739f68393acdaa))
* minor issues ([2ca0845](https://gitlab.com/fi-sas/backoffice/commit/2ca0845977da2908c62c598c43dd5e332fad0afa))
* MS payment endpoints ([ce1c138](https://gitlab.com/fi-sas/backoffice/commit/ce1c138dbac2e069a1574ad2bfa25661f3eb3c77))


### Features

* **accommodation:** add chage_deposit flag to residences ([604e354](https://gitlab.com/fi-sas/backoffice/commit/604e354d5c2f913046b1a8ae7c23e52aed205225))
* **alimentation:** add form and list of stock ([778db5e](https://gitlab.com/fi-sas/backoffice/commit/778db5ec671633682377cfdbaf9a472974eba273))
* **auth:** add tree scopes component ([2e6f201](https://gitlab.com/fi-sas/backoffice/commit/2e6f201cad06a07fe69def0087a1e1e986a60dbf))
* **auth:** change auth method to refresh token ([ee2e745](https://gitlab.com/fi-sas/backoffice/commit/ee2e745c5501f72577437bb16e41fd53fb286a44))
* **communication:** add filters on list posts ([41a6e9e](https://gitlab.com/fi-sas/backoffice/commit/41a6e9e4752211ccb704dc96166fbc6966c435d2))
* **communication::** add button to update tickers ([a730177](https://gitlab.com/fi-sas/backoffice/commit/a730177ad292c1f3d9947e9fc5d2faef32ea786e))
* **configurations:** add files to apresentation texts ([3cf7695](https://gitlab.com/fi-sas/backoffice/commit/3cf769557ba3b0e53335bb25fd5bb2586e1f86eb))
* **configurations:** add files to terms, policies and informations ([a851d83](https://gitlab.com/fi-sas/backoffice/commit/a851d83cc1152f331a5e8894db991157fc1779a8))
* **configurations:** add language order; add reorder page; ([f34c214](https://gitlab.com/fi-sas/backoffice/commit/f34c21416532df0c090c65121055475b0a9278e4))
* **cookie_consent:** inital notification for cookie consent ([84cb02f](https://gitlab.com/fi-sas/backoffice/commit/84cb02fa0cdec9eff8cfa144a442acdacfe560a9))
* **core:** add notifications list on header ([3841865](https://gitlab.com/fi-sas/backoffice/commit/3841865b922df3abb10ea4fdb04111dfd349cfc7))
* **core:** add reports to header ([cf42da8](https://gitlab.com/fi-sas/backoffice/commit/cf42da8dfdd349a38be62baf0b427434078e31b3))
* **core:** add scrollToTop to ui service ([788acde](https://gitlab.com/fi-sas/backoffice/commit/788acde0bee00eb1617cd6add5da7947abd86da0))
* **finacial:** add payment methods account page ([49db6d6](https://gitlab.com/fi-sas/backoffice/commit/49db6d6c6d6393cc3762f731f59109e6009d14b4))
* **notifications:** add alert templates module ([d83049a](https://gitlab.com/fi-sas/backoffice/commit/d83049ab0ca657fdf1a3453d0ce41dc68ed80891))
* **notifications:** add internal method type ([7e77d81](https://gitlab.com/fi-sas/backoffice/commit/7e77d81d77fd69d0bbe9be815e0712579d8bf178))
* **notifications:** initial version of email code editor and preview ([7bd4419](https://gitlab.com/fi-sas/backoffice/commit/7bd4419fdad94fc6b556c6fde96afc5527d97f25))
* **social_support:** add requirement_number on confirm experience ([98c76ec](https://gitlab.com/fi-sas/backoffice/commit/98c76ecfdc560712ccc82ffd23549a6006410a36))
* **social_support:** change language order ([2ed8769](https://gitlab.com/fi-sas/backoffice/commit/2ed876931b6a8783596819b8bee33b36b48e1b2b))
* **social-scholarship:** fdorm for gerenete monthly report ([55641f9](https://gitlab.com/fi-sas/backoffice/commit/55641f96f6db3dab289471f47137eeed4422f0ec))
* **u_bike:** add dashboard panel ([7e6f7de](https://gitlab.com/fi-sas/backoffice/commit/7e6f7de8a8311645148fb7fbefb611774b3936df))

# [1.11.0-dev.62](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.61...v1.11.0-dev.62) (2021-03-08)


### Bug Fixes

* **queue:** fixed operar balcao, fixed data errors, waiting ms updates ([b02f42d](https://gitlab.com/fi-sas/backoffice/commit/b02f42dde6d93eff050ff9ce3d87b8c887ba2ebf))
* desk-start (still waiting for ms final alterations ([ec54d95](https://gitlab.com/fi-sas/backoffice/commit/ec54d958a05eb4b22e46f83e6ce78289ce2567e4))
* small changes in colors and other small parts - operar balcao ([004ca51](https://gitlab.com/fi-sas/backoffice/commit/004ca51a6d4c026289eefc9c78d41457718ce518))

# [1.11.0-dev.61](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.60...v1.11.0-dev.61) (2021-03-05)


### Bug Fixes

* **private accommodation:** errors fixed, waiting for data to test ([4c3692e](https://gitlab.com/fi-sas/backoffice/commit/4c3692e682d8ae3004711288da3d437d2b3ea196))

# [1.11.0-dev.60](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.59...v1.11.0-dev.60) (2021-03-04)


### Bug Fixes

* **accommodation:** add rejected and cancel status to withdraws and ext ([84b19f9](https://gitlab.com/fi-sas/backoffice/commit/84b19f9e40c5f365ff7e42854fb4f290b5e9d89f))

# [1.11.0-dev.59](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.58...v1.11.0-dev.59) (2021-03-04)


### Bug Fixes

* **u-bike:** list trips page fixed and functionalities added ([cb3bed9](https://gitlab.com/fi-sas/backoffice/commit/cb3bed95dd6859995c4b3bc0d90aabb9118eebe0))

# [1.11.0-dev.58](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.57...v1.11.0-dev.58) (2021-03-02)


### Bug Fixes

* current account fixed, waiting for final endpoint ([30936e1](https://gitlab.com/fi-sas/backoffice/commit/30936e150a39260284cedc4c791918a378843fdd))
* current account history confirmed and small changes ([011becb](https://gitlab.com/fi-sas/backoffice/commit/011becbf905e85d6f3c02987a5cbaef562952a2a))
* fixed canceling ([97a1758](https://gitlab.com/fi-sas/backoffice/commit/97a1758e7c94cbd7338b319141d02ea91de8da83))

# [1.11.0-dev.57](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.56...v1.11.0-dev.57) (2021-03-01)


### Bug Fixes

* **accommodation:** many texts fixs ([b5d10b5](https://gitlab.com/fi-sas/backoffice/commit/b5d10b521acce82c8fe2e7d6bca8cfb301c765a8))
* **accommodation:** minor fixs ([19cb5a6](https://gitlab.com/fi-sas/backoffice/commit/19cb5a66d18545e7cdf0582da0e397d2e3f5b802))
* **communication:** fix add language and order language problems ([135e945](https://gitlab.com/fi-sas/backoffice/commit/135e94579f151fb95227d635d47e8bab9451e4e8))

# [1.11.0-dev.56](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.55...v1.11.0-dev.56) (2021-03-01)


### Bug Fixes

* **alimentation:** add entity_id in header ([2e0eac6](https://gitlab.com/fi-sas/backoffice/commit/2e0eac6669df4c2862311dd98d1f7ed46238e525))
* **alimentation:** fix list and form dishs ([4ca378b](https://gitlab.com/fi-sas/backoffice/commit/4ca378be04c07b782a354b9c01cad99b5f5d5562))
* **alimentation:** fix list of reservations ([7dd3797](https://gitlab.com/fi-sas/backoffice/commit/7dd37972d9e808bf8c18f9571f1805f842da94e1))
* **alimentation:** list and action of orders ([3947366](https://gitlab.com/fi-sas/backoffice/commit/3947366985f5c9299e1a80f1e38e194c053ad4b3))
* **alimentation:** list and forms fro create meals ([ad25e13](https://gitlab.com/fi-sas/backoffice/commit/ad25e133e1496356eaf9e2a9dd2ff36917f71778))
* **alimentation:** prices list ([6a87242](https://gitlab.com/fi-sas/backoffice/commit/6a872424f59461a36c475b9fb384c461812ef939))
* **alimentation:** remover comments ([1b5b411](https://gitlab.com/fi-sas/backoffice/commit/1b5b411b7d6d0ada8581e0068d09dfac38e8603b))
* **alimentation:** small errors ([c81212e](https://gitlab.com/fi-sas/backoffice/commit/c81212e1b6a8f94096117d847e092266d1267da6))
* **alimentation:** update components ([6bc887b](https://gitlab.com/fi-sas/backoffice/commit/6bc887baa99b3a0a63c0cb69b5728a9ec3918cd2))
* **appconfig:** update endpoints of alimentation ([d9b5325](https://gitlab.com/fi-sas/backoffice/commit/d9b5325279d1ef8d675644c5979e4c8bc659851e))
* form allergens ([8066788](https://gitlab.com/fi-sas/backoffice/commit/80667889fae9bcedf52c048f05f88c6e89af0c23))
* form and list of product ([1412764](https://gitlab.com/fi-sas/backoffice/commit/1412764b8fee0b34c6b656edadd53f4884e34fd8))
* form nutrients ([de56795](https://gitlab.com/fi-sas/backoffice/commit/de5679516089dd7dbf955c7c246c3f9780f4ef58))
* form services ([adb5989](https://gitlab.com/fi-sas/backoffice/commit/adb59890c9071990f11f68cbc486d2b7c818d351))
* form unit ([28283a3](https://gitlab.com/fi-sas/backoffice/commit/28283a3ac8aa9684a77cfe8fe528697d75064f91))
* form units ([8393f93](https://gitlab.com/fi-sas/backoffice/commit/8393f93c6230c51d0b0b165effdf3e3027d07628))
* form wharehouse ([e2b96d5](https://gitlab.com/fi-sas/backoffice/commit/e2b96d5478fec01db0e1ad36d5f7345707093d6e))
* form, list and view families ([fd40d04](https://gitlab.com/fi-sas/backoffice/commit/fd40d0471bbfb34ea5805d373cd5422a823ecd24))
* form, list and view of entities ([b45f9fb](https://gitlab.com/fi-sas/backoffice/commit/b45f9fb0835ccc7069a9a29fef1af2e6adddbfb5))
* forms, list and view of complements ([2f9d5da](https://gitlab.com/fi-sas/backoffice/commit/2f9d5da02703791755da525df2127ad3bb94a6d4))
* important modules ([69e4f55](https://gitlab.com/fi-sas/backoffice/commit/69e4f551e7366b7f61487ba7ed2adc0e2ac81322))
* list allergens ([a7a3f9e](https://gitlab.com/fi-sas/backoffice/commit/a7a3f9ec428d0f65614be0264353707553b3c8bf))
* list and form of dish_type ([d650c23](https://gitlab.com/fi-sas/backoffice/commit/d650c2382ebc0946e913b1ba22043d162e598bd0))
* list and view of wharehouse ([109d33e](https://gitlab.com/fi-sas/backoffice/commit/109d33e5be800d4aed3380e1b63a0b80f8f20bcf))
* list nutrients ([771d7ad](https://gitlab.com/fi-sas/backoffice/commit/771d7ad2f7183b00a3282b590f86b0806429e671))
* list products ([9ca3a16](https://gitlab.com/fi-sas/backoffice/commit/9ca3a16fdc1e43471eb4a8d3b941aa04b95daba7))
* list recipes and form ([86c276b](https://gitlab.com/fi-sas/backoffice/commit/86c276bb54ce536377531181466e2f8bc578222d))
* list services ([6d326bd](https://gitlab.com/fi-sas/backoffice/commit/6d326bda63867dca05e5af4e9e74e38bf3ff25b6))
* list units ([718759b](https://gitlab.com/fi-sas/backoffice/commit/718759b703411d5fc651c0df94f5eb797307d393))
* update modules and services ([692f58e](https://gitlab.com/fi-sas/backoffice/commit/692f58e784ed2f872db7339aea50be94d9a2fba4))


### Features

* **alimentation:** add form and list of stock ([778db5e](https://gitlab.com/fi-sas/backoffice/commit/778db5ec671633682377cfdbaf9a472974eba273))

# [1.11.0-dev.55](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.54...v1.11.0-dev.55) (2021-03-01)


### Bug Fixes

* **configurations:** new generic device ([74a6625](https://gitlab.com/fi-sas/backoffice/commit/74a66256158b5d6bdd8448d00bd033a7ffe57573))

# [1.11.0-dev.54](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.53...v1.11.0-dev.54) (2021-02-24)


### Bug Fixes

* **configurations:** resolve error length of undefined ([9f24759](https://gitlab.com/fi-sas/backoffice/commit/9f24759b679868663598b2528d95d9ae2db751d7))

# [1.11.0-dev.53](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.52...v1.11.0-dev.53) (2021-02-24)


### Bug Fixes

* **accommodation:** minor fix to all module ([16eccbb](https://gitlab.com/fi-sas/backoffice/commit/16eccbb3b80a3954b969d64e40acd83c9a656b36))

# [1.11.0-dev.52](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.51...v1.11.0-dev.52) (2021-02-24)


### Bug Fixes

* payments completed, waiting for more UI ([6154571](https://gitlab.com/fi-sas/backoffice/commit/6154571f8b8212f65e49cd60b2f510151d52f067))
* start of cancelation (waiting for repairs on UI) ([e502323](https://gitlab.com/fi-sas/backoffice/commit/e5023234fe6b99618a9b65abf6185213c783270f))

# [1.11.0-dev.51](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.50...v1.11.0-dev.51) (2021-02-24)


### Bug Fixes

* small fix on request ([5cfb94f](https://gitlab.com/fi-sas/backoffice/commit/5cfb94f15870e6300f7b9a1810ebce0aaa18b8b3))
* u-bike applications and date controls ([eba3021](https://gitlab.com/fi-sas/backoffice/commit/eba30219c9ae9ddce259ebf74e41b5e2e5d75957))

# [1.11.0-dev.50](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.49...v1.11.0-dev.50) (2021-02-23)


### Features

* **communication:** add filters on list posts ([41a6e9e](https://gitlab.com/fi-sas/backoffice/commit/41a6e9e4752211ccb704dc96166fbc6966c435d2))

# [1.11.0-dev.49](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.48...v1.11.0-dev.49) (2021-02-23)


### Bug Fixes

* **configurations:** resolve build errors ([14f2282](https://gitlab.com/fi-sas/backoffice/commit/14f2282762a9f754ed30acb24313aaad29ab80b3))

# [1.11.0-dev.48](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.47...v1.11.0-dev.48) (2021-02-23)


### Bug Fixes

* **geral:** remove school ([ed40bbb](https://gitlab.com/fi-sas/backoffice/commit/ed40bbb8dd3eefb4c0ca389daf76ac8215e3ddd1))

# [1.11.0-dev.47](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.46...v1.11.0-dev.47) (2021-02-22)


### Bug Fixes

* **communication:** remove required on date in posts form ([6fe64b6](https://gitlab.com/fi-sas/backoffice/commit/6fe64b665a3a76dd045a317dd019839664c9f40c))

# [1.11.0-dev.46](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.45...v1.11.0-dev.46) (2021-02-19)


### Bug Fixes

* correction error pagination mobile ([eed78ea](https://gitlab.com/fi-sas/backoffice/commit/eed78eaed5df0df5d4ff2f34a0065d7b91f31124))

# [1.11.0-dev.45](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.44...v1.11.0-dev.45) (2021-02-19)


### Features

* **configurations:** add files to apresentation texts ([3cf7695](https://gitlab.com/fi-sas/backoffice/commit/3cf769557ba3b0e53335bb25fd5bb2586e1f86eb))
* **configurations:** add files to terms, policies and informations ([a851d83](https://gitlab.com/fi-sas/backoffice/commit/a851d83cc1152f331a5e8894db991157fc1779a8))

# [1.11.0-dev.44](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.43...v1.11.0-dev.44) (2021-02-17)


### Features

* **configurations:** add language order; add reorder page; ([f34c214](https://gitlab.com/fi-sas/backoffice/commit/f34c21416532df0c090c65121055475b0a9278e4))
* **social_support:** change language order ([2ed8769](https://gitlab.com/fi-sas/backoffice/commit/2ed876931b6a8783596819b8bee33b36b48e1b2b))

# [1.11.0-dev.43](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.42...v1.11.0-dev.43) (2021-02-15)


### Features

* **cookie_consent:** inital notification for cookie consent ([84cb02f](https://gitlab.com/fi-sas/backoffice/commit/84cb02fa0cdec9eff8cfa144a442acdacfe560a9))

# [1.11.0-dev.42](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.41...v1.11.0-dev.42) (2021-02-11)


### Bug Fixes

* accommodation,volunteering,current account ([edf98b1](https://gitlab.com/fi-sas/backoffice/commit/edf98b1aaffa42706232d03c8571acff46eb56dd))
* **u_bike:** fix application loading error ([7072314](https://gitlab.com/fi-sas/backoffice/commit/707231417e135f929426beaa531cf922406be977))


### Features

* **u_bike:** add dashboard panel ([7e6f7de](https://gitlab.com/fi-sas/backoffice/commit/7e6f7de8a8311645148fb7fbefb611774b3936df))

# [1.11.0-dev.41](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.40...v1.11.0-dev.41) (2021-02-11)


### Bug Fixes

* **social_support:** errors fix on social support module ([b86f3f6](https://gitlab.com/fi-sas/backoffice/commit/b86f3f6ccc554844d3cebbc9f11eadbcb4ee3472))

# [1.11.0-dev.40](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.39...v1.11.0-dev.40) (2021-02-11)


### Features

* **finacial:** add payment methods account page ([49db6d6](https://gitlab.com/fi-sas/backoffice/commit/49db6d6c6d6393cc3762f731f59109e6009d14b4))

# [1.11.0-dev.39](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.38...v1.11.0-dev.39) (2021-02-10)


### Bug Fixes

* volunteering pages fixed according to ms's, waiting for 1ms fixe ([fa1cf6e](https://gitlab.com/fi-sas/backoffice/commit/fa1cf6e763f8d725adbd68fb2dce7abe2bd88ad2))

# [1.11.0-dev.38](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.37...v1.11.0-dev.38) (2021-02-10)


### Bug Fixes

* **social-support:** remove duplicate translations ([16b98c2](https://gitlab.com/fi-sas/backoffice/commit/16b98c29d17cb5d9851276b90eac4a2ecab6bb5a))

# [1.11.0-dev.37](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.36...v1.11.0-dev.37) (2021-02-10)


### Features

* **social-scholarship:** fdorm for gerenete monthly report ([55641f9](https://gitlab.com/fi-sas/backoffice/commit/55641f96f6db3dab289471f47137eeed4422f0ec))

# [1.11.0-dev.36](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.35...v1.11.0-dev.36) (2021-02-09)


### Bug Fixes

* **alojamentoprivado:** form property ([000bee1](https://gitlab.com/fi-sas/backoffice/commit/000bee12a82a8c4626ed88de3cd125ef18f9261d))
* **bolsa:** text apresentation configuration ([a382698](https://gitlab.com/fi-sas/backoffice/commit/a382698e13c9ad9600d550bbe3ff2c991eaac4f0))

# [1.11.0-dev.35](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.34...v1.11.0-dev.35) (2021-02-07)


### Bug Fixes

* change proxy to suport http only cookie ([5ac3744](https://gitlab.com/fi-sas/backoffice/commit/5ac3744d47298887f0e135b16fbd604dd960ec04))

# [1.11.0-dev.34](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.33...v1.11.0-dev.34) (2021-02-05)


### Features

* **auth:** change auth method to refresh token ([ee2e745](https://gitlab.com/fi-sas/backoffice/commit/ee2e745c5501f72577437bb16e41fd53fb286a44))

# [1.11.0-dev.33](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.32...v1.11.0-dev.33) (2021-02-04)


### Bug Fixes

* adding new file ([b9e7cd5](https://gitlab.com/fi-sas/backoffice/commit/b9e7cd5777409849f74f9298ea398723430398d5))

# [1.11.0-dev.32](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.31...v1.11.0-dev.32) (2021-02-04)


### Bug Fixes

* accommodation, regulations, queue ([39f7625](https://gitlab.com/fi-sas/backoffice/commit/39f76250b4554631e1e961393adbbdd1d70b4c67))
* new merge request ([bc8dc84](https://gitlab.com/fi-sas/backoffice/commit/bc8dc84e5b5da4a3d1b0274b5990ff603af3d910))

# [1.11.0-dev.31](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.30...v1.11.0-dev.31) (2021-02-04)


### Bug Fixes

* **socialscholarship:** fix translated fields ([59f3398](https://gitlab.com/fi-sas/backoffice/commit/59f3398a0c861fe1507fc981dc2b48bfe38e83e3))


### Features

* **social_support:** add requirement_number on confirm experience ([98c76ec](https://gitlab.com/fi-sas/backoffice/commit/98c76ecfdc560712ccc82ffd23549a6006410a36))

# [1.11.0-dev.30](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.29...v1.11.0-dev.30) (2021-02-01)


### Bug Fixes

* **bus:** enable load accounts ([47e31a6](https://gitlab.com/fi-sas/backoffice/commit/47e31a6b9981575730fbcb30dd243d33d3ba49ec))

# [1.11.0-dev.29](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.28...v1.11.0-dev.29) (2021-02-01)


### Bug Fixes

* **bus:** set corrent accounts ([d046cbf](https://gitlab.com/fi-sas/backoffice/commit/d046cbf5c4af227af8211b5ef127a8964618cf61))

# [1.11.0-dev.28](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.27...v1.11.0-dev.28) (2021-01-29)


### Bug Fixes

* **geral:** services active dashboard; error image ([444aa96](https://gitlab.com/fi-sas/backoffice/commit/444aa9669aba5515ff61862ddf1f24e142bf5a6a))

# [1.11.0-dev.27](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.26...v1.11.0-dev.27) (2021-01-28)


### Bug Fixes

* **s-scholarship:** application status uppercase ([16bfb5b](https://gitlab.com/fi-sas/backoffice/commit/16bfb5b323c45c4438f187aca3822005c361988e))
* conta Corrente UI ([5164520](https://gitlab.com/fi-sas/backoffice/commit/5164520d86cf40f5ffe25debcf2b4e5f2cb25314))

# [1.11.0-dev.26](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.25...v1.11.0-dev.26) (2021-01-26)


### Bug Fixes

* environment ([a496491](https://gitlab.com/fi-sas/backoffice/commit/a496491a37c7889e027420e6ff88c3a11d1a1090))
* **accommodation:** cancel application form ([43d7cfa](https://gitlab.com/fi-sas/backoffice/commit/43d7cfaa151286d98f40c4264bc8be63f6df6d14))
* **financial:** add fixs to new MS ([53246ac](https://gitlab.com/fi-sas/backoffice/commit/53246acf21a61d8df6f38948ef547d06eec42446))
* **privateaccomodation:** form property ([1ad1323](https://gitlab.com/fi-sas/backoffice/commit/1ad1323fafe890a52770dd4349ce58be04234fe4))

# [1.11.0-dev.25](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.24...v1.11.0-dev.25) (2021-01-26)


### Bug Fixes

* **model:** gender default pt ([31a2aa1](https://gitlab.com/fi-sas/backoffice/commit/31a2aa1b4993575043ff08675904b24d22105932))

# [1.11.0-dev.24](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.23...v1.11.0-dev.24) (2021-01-20)


### Bug Fixes

* **medias:** fix search param  in files ([cd30dfe](https://gitlab.com/fi-sas/backoffice/commit/cd30dfe8ca3279fa63e43fda33ffd0eb1100f658))
* **private accommodation:** fix view listing ([e9ee55f](https://gitlab.com/fi-sas/backoffice/commit/e9ee55f63d29fa6dab73eadce3aa0d5571068893))
* **private configuration:** fix owner regulation parse ([67e5201](https://gitlab.com/fi-sas/backoffice/commit/67e5201e8a21db9b03a500a76d6f19e76d31917d))

# [1.11.0-dev.23](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.22...v1.11.0-dev.23) (2021-01-19)


### Bug Fixes

* **privateaccommodation:** max length title ([49c0e21](https://gitlab.com/fi-sas/backoffice/commit/49c0e2118dc3587e63e4321ce16fabf06a8b1530))

# [1.11.0-dev.22](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.21...v1.11.0-dev.22) (2021-01-19)


### Bug Fixes

* environments localhost ([c424d83](https://gitlab.com/fi-sas/backoffice/commit/c424d83c6268c0621657490b2b6fbc8f4e416b00))
* **accommodationprivate:** change form required ([a4e0d94](https://gitlab.com/fi-sas/backoffice/commit/a4e0d94c86e574639cfcfb0e8ff9ab1ce284738e))

# [1.11.0-dev.21](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.20...v1.11.0-dev.21) (2021-01-18)


### Bug Fixes

* **privateAccommodation:** housing title ([5e4730e](https://gitlab.com/fi-sas/backoffice/commit/5e4730e557152e34880beb6c69ce1730f0d37328))

# [1.11.0-dev.20](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.19...v1.11.0-dev.20) (2021-01-18)


### Bug Fixes

* **bus:** fix date in timetable ([57914e4](https://gitlab.com/fi-sas/backoffice/commit/57914e4a56162b0544dbbcb46290787d071bb76a))
* **bus:** fix price table update list ([93b7e75](https://gitlab.com/fi-sas/backoffice/commit/93b7e7546695e7b44b546bbb635cf3a028fe8047))
* **bus:** fix timetable list ([ad0ea19](https://gitlab.com/fi-sas/backoffice/commit/ad0ea1954e8ff92c0d02a660617acedc2805506f))
* **bus:** set paginations list zones ([5fa66c7](https://gitlab.com/fi-sas/backoffice/commit/5fa66c7ac062ced550c645f6073dc2a6c1988ed5))
* **private-accommodation:** corrections for the new boilerplate ([a11d900](https://gitlab.com/fi-sas/backoffice/commit/a11d9007f4ca42bde483b35373a4289ad725961a))

# [1.11.0-dev.19](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.18...v1.11.0-dev.19) (2021-01-18)


### Bug Fixes

* **social_schollarship:** minor fix for migration ([d34f4f3](https://gitlab.com/fi-sas/backoffice/commit/d34f4f38ecbd5d279cca51f0924be3b2db6235cc))

# [1.11.0-dev.18](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.17...v1.11.0-dev.18) (2021-01-13)


### Bug Fixes

* minor fixs in multiple modules ([3a82829](https://gitlab.com/fi-sas/backoffice/commit/3a828297c6d6e19f94f33239b0af0db29b1a685d))

# [1.11.0-dev.17](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.16...v1.11.0-dev.17) (2021-01-13)


### Bug Fixes

* **accommodation:** lateral menu texts ([4a6253f](https://gitlab.com/fi-sas/backoffice/commit/4a6253fae584fcb76219d2eeeafcf6e7aeefe162))
* **food:** add changes to new version of microservice ([ae053fa](https://gitlab.com/fi-sas/backoffice/commit/ae053fa577e581712899bc8b39a85b5e011a3d25))
* **shared:** add validation date ([7e4ac44](https://gitlab.com/fi-sas/backoffice/commit/7e4ac44f24af6c4bf808a3759a0d9a79b3275d54))

# [1.11.0-dev.16](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.15...v1.11.0-dev.16) (2021-01-06)


### Bug Fixes

* **queue:**  organizations folders ([b546703](https://gitlab.com/fi-sas/backoffice/commit/b546703f1ab7a0d176c6f6978243d61134a2e81a))

# [1.11.0-dev.15](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.14...v1.11.0-dev.15) (2021-01-05)


### Bug Fixes

* **users:** request users relateds ([2a90733](https://gitlab.com/fi-sas/backoffice/commit/2a90733cfe92339c67e465dd51872f1d24cac496))

# [1.11.0-dev.14](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.13...v1.11.0-dev.14) (2021-01-04)


### Features

* **notifications:** initial version of email code editor and preview ([7bd4419](https://gitlab.com/fi-sas/backoffice/commit/7bd4419fdad94fc6b556c6fde96afc5527d97f25))

# [1.11.0-dev.13](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.12...v1.11.0-dev.13) (2020-12-31)


### Bug Fixes

* **communication:** fix pagination of posts ([d75ae11](https://gitlab.com/fi-sas/backoffice/commit/d75ae1168f5c1f51550959e7e8c51c9e86502516))


### Features

* **notifications:** add internal method type ([7e77d81](https://gitlab.com/fi-sas/backoffice/commit/7e77d81d77fd69d0bbe9be815e0712579d8bf178))

# [1.11.0-dev.12](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.11...v1.11.0-dev.12) (2020-12-30)


### Bug Fixes

* **shared:** fix file upload url path ([24a92ac](https://gitlab.com/fi-sas/backoffice/commit/24a92ac720a0fe549b7cacea8d9347d853293700))


### Features

* **core:** add notifications list on header ([3841865](https://gitlab.com/fi-sas/backoffice/commit/3841865b922df3abb10ea4fdb04111dfd349cfc7))
* **core:** add reports to header ([cf42da8](https://gitlab.com/fi-sas/backoffice/commit/cf42da8dfdd349a38be62baf0b427434078e31b3))

# [1.11.0-dev.11](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.10...v1.11.0-dev.11) (2020-12-23)


### Bug Fixes

* create couter and service ([60628f5](https://gitlab.com/fi-sas/backoffice/commit/60628f54b89af19a5e82e1e13a8a6182196ffc5e))

# [1.11.0-dev.10](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.9...v1.11.0-dev.10) (2020-12-23)


### Features

* **core:** add scrollToTop to ui service ([788acde](https://gitlab.com/fi-sas/backoffice/commit/788acde0bee00eb1617cd6add5da7947abd86da0))

# [1.11.0-dev.9](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.8...v1.11.0-dev.9) (2020-12-21)


### Bug Fixes

* **envs:** fix angular ipportalegre env ([a822a95](https://gitlab.com/fi-sas/backoffice/commit/a822a955be74bac51489032b35d67461d7db0676))

# [1.11.0-dev.8](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.7...v1.11.0-dev.8) (2020-12-21)


### Bug Fixes

* **envs:** fix ipportalegre envs ([8b6f39f](https://gitlab.com/fi-sas/backoffice/commit/8b6f39fb1060d677c6128daf5f2e8e782ca80cb6))

# [1.11.0-dev.7](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.6...v1.11.0-dev.7) (2020-12-21)


### Bug Fixes

* **shared:** fix upload images endpoint ([5bf9202](https://gitlab.com/fi-sas/backoffice/commit/5bf92026850ccbf1f2bcc9b2883be300951d9287))

# [1.11.0-dev.6](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.5...v1.11.0-dev.6) (2020-12-21)


### Bug Fixes

* endpoints wanted_ads ([7e73871](https://gitlab.com/fi-sas/backoffice/commit/7e73871a911e96a1d848dbcc85739f68393acdaa))

# [1.11.0-dev.5](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.4...v1.11.0-dev.5) (2020-12-17)


### Bug Fixes

* **queue:** minor fix ([5d6e1e1](https://gitlab.com/fi-sas/backoffice/commit/5d6e1e17776b6bb64888772e8c257c73ee9f15a0))

# [1.11.0-dev.4](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.3...v1.11.0-dev.4) (2020-12-14)


### Bug Fixes

* **erros:** erros are now from api ([4b5d1b2](https://gitlab.com/fi-sas/backoffice/commit/4b5d1b22d923089db4ad86d987caeca747aa9be0))

# [1.11.0-dev.3](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.2...v1.11.0-dev.3) (2020-12-11)


### Bug Fixes

* **build:** fix post-build.js paths ([c1abb9e](https://gitlab.com/fi-sas/backoffice/commit/c1abb9e4849be9fd133e95378863d262490941fb))

# [1.11.0-dev.2](https://gitlab.com/fi-sas/backoffice/compare/v1.11.0-dev.1...v1.11.0-dev.2) (2020-12-11)


### Bug Fixes

* MS payment endpoints ([ce1c138](https://gitlab.com/fi-sas/backoffice/commit/ce1c138dbac2e069a1574ad2bfa25661f3eb3c77))

# [1.11.0-dev.1](https://gitlab.com/fi-sas/backoffice/compare/v1.10.3...v1.11.0-dev.1) (2020-12-09)


### Bug Fixes

* **accommodation:** fix scopes ([655357d](https://gitlab.com/fi-sas/backoffice/commit/655357dc31b33ef0d5bee79fd5c2c17f4223b080))
* **accommodation:** minor fixs ([9ef412d](https://gitlab.com/fi-sas/backoffice/commit/9ef412dbbf6fd6e0cdb9244c12f87ef063433421))
* **app:** minor fixs ([b8d743f](https://gitlab.com/fi-sas/backoffice/commit/b8d743faf3b3025ed8cc188940378cacf82c93a0))
* **bus:** adaptation of the backoffice to the new ms ([e43087c](https://gitlab.com/fi-sas/backoffice/commit/e43087c40aad8339c4956b5cb4ebe24da54105c6))
* **bus:** application adaptation for new ms ([55ef5e1](https://gitlab.com/fi-sas/backoffice/commit/55ef5e15b60068971b577ecc9668258b21dbdd39))
* **bus:** fix error when insert timetable ([2c5c3f3](https://gitlab.com/fi-sas/backoffice/commit/2c5c3f30af467ef443f49ec29b0205a3f67845ba))
* **bus:** fix minor issues in links ([0b85cfb](https://gitlab.com/fi-sas/backoffice/commit/0b85cfbb6a9011c09f0819fa19f4ad0a8fd9ea9c))
* **communication:** fix feeds offset ([5a07742](https://gitlab.com/fi-sas/backoffice/commit/5a07742b65583b891df179a3fdff9300f2ff7dc2))
* **enums:** change to uppercase ([8127a7b](https://gitlab.com/fi-sas/backoffice/commit/8127a7b1197a9a0f2640cc0be01cd47d9bca52fe))
* **reports:** add id to templates ([3e614e0](https://gitlab.com/fi-sas/backoffice/commit/3e614e01a8b7630b0ba8c9a5f67a9e703eb6db81))
* minor issues ([2ca0845](https://gitlab.com/fi-sas/backoffice/commit/2ca0845977da2908c62c598c43dd5e332fad0afa))
* **media:** change url medias ([bcde485](https://gitlab.com/fi-sas/backoffice/commit/bcde485786f7a796bf055fa912f7ef75012c3a59))
* **notifications:** minoxr fixs ([1ffc3e4](https://gitlab.com/fi-sas/backoffice/commit/1ffc3e4768054f4755511b9384c035b45c9ff9af))
* **notifications:** update alert enums ([5d27840](https://gitlab.com/fi-sas/backoffice/commit/5d278405c5a5b1307097322c51ca145d68b6cd9c))
* **package:** update ngx-mask to 7.9.10 ([c58da20](https://gitlab.com/fi-sas/backoffice/commit/c58da2038dca4cf2f76232ca8d2d15c4da6a2b8c))


### Features

* **accommodation:** add chage_deposit flag to residences ([604e354](https://gitlab.com/fi-sas/backoffice/commit/604e354d5c2f913046b1a8ae7c23e52aed205225))
* **auth:** add tree scopes component ([2e6f201](https://gitlab.com/fi-sas/backoffice/commit/2e6f201cad06a07fe69def0087a1e1e986a60dbf))
* **communication::** add button to update tickers ([a730177](https://gitlab.com/fi-sas/backoffice/commit/a730177ad292c1f3d9947e9fc5d2faef32ea786e))
* **notifications:** add alert templates module ([d83049a](https://gitlab.com/fi-sas/backoffice/commit/d83049ab0ca657fdf1a3453d0ce41dc68ed80891))

# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.10.3"></a>
## [1.10.3](https://gitlab.com/fi-sas/backoffice/compare/v1.10.2...v1.10.3) (2020-11-25)



<a name="1.10.2"></a>
## [1.10.2](https://gitlab.com/fi-sas/backoffice/compare/v1.10.1...v1.10.2) (2020-11-02)


### Bug Fixes

* **communication:** minor fixs ([9c82444](https://gitlab.com/fi-sas/backoffice/commit/9c82444))
* **private-accommodation:** fix configuration  inputs ([02b18c3](https://gitlab.com/fi-sas/backoffice/commit/02b18c3))



<a name="1.10.1"></a>
## [1.10.1](https://gitlab.com/fi-sas/backoffice/compare/v1.10.0...v1.10.1) (2020-10-29)


### Bug Fixes

* **accommodation:** add temporrary fix to billing processement ([8cf56a3](https://gitlab.com/fi-sas/backoffice/commit/8cf56a3))



<a name="1.10.0"></a>
# [1.10.0](https://gitlab.com/fi-sas/backoffice/compare/v1.9.1...v1.10.0) (2020-10-27)


### Bug Fixes

* **accommodation:** add opposition details to application ([754d768](https://gitlab.com/fi-sas/backoffice/commit/754d768))


### Features

* **bus:** add withdrawal and cancel status to applications ([28018aa](https://gitlab.com/fi-sas/backoffice/commit/28018aa))



<a name="1.9.1"></a>
## [1.9.1](https://gitlab.com/fi-sas/backoffice/compare/v1.9.0...v1.9.1) (2020-10-09)


### Bug Fixes

* **accommodation:** add fileds to enqeued applications table ([4821857](https://gitlab.com/fi-sas/backoffice/commit/4821857))
* **social-support:** fix empty properties ([38d5bf3](https://gitlab.com/fi-sas/backoffice/commit/38d5bf3))
* **volunteering:** fix urls ([a28e4ba](https://gitlab.com/fi-sas/backoffice/commit/a28e4ba))
* **volunteering:** set experience in accept status ([e73d00a](https://gitlab.com/fi-sas/backoffice/commit/e73d00a))



<a name="1.9.0"></a>
# [1.9.0](https://gitlab.com/fi-sas/backoffice/compare/v1.8.0...v1.9.0) (2020-09-29)


### Bug Fixes

* **accommodation:** minor fixs ([bb6a5e7](https://gitlab.com/fi-sas/backoffice/commit/bb6a5e7))
* **finacial:** fix minor issue ([9999793](https://gitlab.com/fi-sas/backoffice/commit/9999793))
* **pipe:** move filter month pipe to shared module ([bb574a5](https://gitlab.com/fi-sas/backoffice/commit/bb574a5))


### Features

* **financial:** add order list page ([859d704](https://gitlab.com/fi-sas/backoffice/commit/859d704))
* **social-support:** add colaborations page ([624482d](https://gitlab.com/fi-sas/backoffice/commit/624482d))



<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/fi-sas/backoffice/compare/v1.7.0...v1.8.0) (2020-09-23)


### Bug Fixes

* **accommodation:** fix tables scroll's ([4d4aadc](https://gitlab.com/fi-sas/backoffice/commit/4d4aadc))
* **alimentation:** fix recipes form default values ([d92b0a2](https://gitlab.com/fi-sas/backoffice/commit/d92b0a2))
* **private accomodation:** add issues receipt in owner ([90da541](https://gitlab.com/fi-sas/backoffice/commit/90da541))
* **u-bike:** fix default values in form trip ([1e1e4f5](https://gitlab.com/fi-sas/backoffice/commit/1e1e4f5))
* **u-bike:** fix orthographic error ([363d872](https://gitlab.com/fi-sas/backoffice/commit/363d872))
* **u-bike:** fix units in stats ([39d9b65](https://gitlab.com/fi-sas/backoffice/commit/39d9b65))
* **ubike:** fix trips users select ([7a1f2c2](https://gitlab.com/fi-sas/backoffice/commit/7a1f2c2))


### Features

* **u-bike:** add form for record trip km ([ca2969e](https://gitlab.com/fi-sas/backoffice/commit/ca2969e))



<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/fi-sas/backoffice/compare/v1.6.0...v1.7.0) (2020-09-17)


### Features

* **financial:** new layout to financial module ([7e53f11](https://gitlab.com/fi-sas/backoffice/commit/7e53f11))



<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/fi-sas/backoffice/compare/v1.5.0...v1.6.0) (2020-09-15)


### Bug Fixes

* **accommodation:** add proccess modal ([1ae790d](https://gitlab.com/fi-sas/backoffice/commit/1ae790d))
* **accommodation:** minor fixs ([a1634eb](https://gitlab.com/fi-sas/backoffice/commit/a1634eb))
* **accommodation:** remove import ([b9e881b](https://gitlab.com/fi-sas/backoffice/commit/b9e881b))
* **private-accommodation:** fix redirect after typology creation ([90ccb33](https://gitlab.com/fi-sas/backoffice/commit/90ccb33))
* **sso-auth:** fix nagivate link ([3366508](https://gitlab.com/fi-sas/backoffice/commit/3366508))


### Features

* **accommodation:** add billings analyse ([62807e3](https://gitlab.com/fi-sas/backoffice/commit/62807e3))



<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/fi-sas/backoffice/compare/v1.4.0...v1.5.0) (2020-09-10)


### Bug Fixes

* **auth:** add ngx-cookie for sso ([31499ca](https://gitlab.com/fi-sas/backoffice/commit/31499ca))
* **social-support:** set experience in accept status ([c0c36ae](https://gitlab.com/fi-sas/backoffice/commit/c0c36ae))


### Features

* **login:** add sso authentication ([f6aab5b](https://gitlab.com/fi-sas/backoffice/commit/f6aab5b))



<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/fi-sas/backoffice/compare/v1.3.6...v1.4.0) (2020-09-09)


### Bug Fixes

* **accommodation:** add is_deposit field to extras ([75bc2f4](https://gitlab.com/fi-sas/backoffice/commit/75bc2f4))
* **bus:** feature to update payment months ([6da612f](https://gitlab.com/fi-sas/backoffice/commit/6da612f))
* **private-accommodation:** set opcional inputs in create owner ([8ed7da1](https://gitlab.com/fi-sas/backoffice/commit/8ed7da1))


### Features

* **bus:** add imit bus declaration ([50ed8c4](https://gitlab.com/fi-sas/backoffice/commit/50ed8c4))



<a name="1.3.6"></a>
## [1.3.6](https://gitlab.com/fi-sas/backoffice/compare/v1.3.4...v1.3.6) (2020-09-03)


### Bug Fixes

* **accommodation:** fix reports application result modal ([87d7955](https://gitlab.com/fi-sas/backoffice/commit/87d7955))



<a name="1.3.5"></a>
## [1.3.5](https://gitlab.com/fi-sas/backoffice/compare/v1.3.4...v1.3.5) (2020-09-03)


### Bug Fixes

* **accommodation:** fix reports application result modal ([87d7955](https://gitlab.com/fi-sas/backoffice/commit/87d7955))



<a name="1.3.4"></a>
## [1.3.4](https://gitlab.com/fi-sas/backoffice/compare/v1.3.3...v1.3.4) (2020-09-03)



<a name="1.3.3"></a>
## [1.3.3](https://gitlab.com/fi-sas/backoffice/compare/v1.3.2...v1.3.3) (2020-09-03)


### Bug Fixes

* **accommodation:** change the application phase module ([ae81640](https://gitlab.com/fi-sas/backoffice/commit/ae81640))



<a name="1.3.2"></a>
## [1.3.2](https://gitlab.com/fi-sas/backoffice/compare/v1.3.1...v1.3.2) (2020-09-01)


### Bug Fixes

* **bus:** fix bus status modal ([5ff60de](https://gitlab.com/fi-sas/backoffice/commit/5ff60de))



<a name="1.3.1"></a>
## [1.3.1](https://gitlab.com/fi-sas/backoffice/compare/v1.3.0...v1.3.1) (2020-08-25)


### Bug Fixes

* **users:** add 'Other' gender ([96fdb19](https://gitlab.com/fi-sas/backoffice/commit/96fdb19))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/fi-sas/backoffice/compare/v1.2.1...v1.3.0) (2020-08-14)


### Bug Fixes

* **bus:** fix tag status ([58ce47a](https://gitlab.com/fi-sas/backoffice/commit/58ce47a))
* **social-support:** button disabled in change status ([58c6e18](https://gitlab.com/fi-sas/backoffice/commit/58c6e18))
* **volunteering:** button disabled in change status ([bdc576b](https://gitlab.com/fi-sas/backoffice/commit/bdc576b))


### Features

* **applications:** add list payments ([0445213](https://gitlab.com/fi-sas/backoffice/commit/0445213))
* **bus:** add application list ([cd93d25](https://gitlab.com/fi-sas/backoffice/commit/cd93d25))
* **u-bike:** print list applications ([18eef14](https://gitlab.com/fi-sas/backoffice/commit/18eef14))



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/fi-sas/backoffice/compare/v1.2.0...v1.2.1) (2020-08-07)



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/fi-sas/backoffice/compare/v1.1.8...v1.2.0) (2020-08-07)


### Bug Fixes

* **accommodation:** fix regime form ([45fe376](https://gitlab.com/fi-sas/backoffice/commit/45fe376))
* **private-accomodation:** module for complaints ([d4551d7](https://gitlab.com/fi-sas/backoffice/commit/d4551d7))
* **private-accomomodation:** create listing module ([8ac8b74](https://gitlab.com/fi-sas/backoffice/commit/8ac8b74))


### Features

* **private accomodation:** module for typologies and owners ([c307fac](https://gitlab.com/fi-sas/backoffice/commit/c307fac))
* **private-accommodation:** create form for owner ([c76855c](https://gitlab.com/fi-sas/backoffice/commit/c76855c))
* **private-accomodation:** form for create listing ([0a4d3c5](https://gitlab.com/fi-sas/backoffice/commit/0a4d3c5))
* **private-accomodation:** module for configurations ([55515b7](https://gitlab.com/fi-sas/backoffice/commit/55515b7))
* **sidebar:** add title and icon ([cc53b2d](https://gitlab.com/fi-sas/backoffice/commit/cc53b2d))
* **style:** change the global style system ([bd0a000](https://gitlab.com/fi-sas/backoffice/commit/bd0a000))
* **u-bike:** change bike in apllications ([165259c](https://gitlab.com/fi-sas/backoffice/commit/165259c))
* **users:** add new layout profiles module ([6f9cee7](https://gitlab.com/fi-sas/backoffice/commit/6f9cee7))
* **users:** add new layout scopes module ([f0dc069](https://gitlab.com/fi-sas/backoffice/commit/f0dc069))
* **users:** add new layout targets module ([e95e191](https://gitlab.com/fi-sas/backoffice/commit/e95e191))
* **users:** add new layout user-groups module ([14a803a](https://gitlab.com/fi-sas/backoffice/commit/14a803a))
* **users:** add new layout users module ([3f9b914](https://gitlab.com/fi-sas/backoffice/commit/3f9b914))
* **users:** initial structure of new users module ([081df82](https://gitlab.com/fi-sas/backoffice/commit/081df82))
* **users:** initial version of lists ([2fbd7bc](https://gitlab.com/fi-sas/backoffice/commit/2fbd7bc))



<a name="1.1.8"></a>
## [1.1.8](https://gitlab.com/fi-sas/backoffice/compare/v1.1.7...v1.1.8) (2020-07-22)


### Bug Fixes

* **u-bike:** remove asset when create bike ([f281a4b](https://gitlab.com/fi-sas/backoffice/commit/f281a4b))



<a name="1.1.7"></a>
## [1.1.7](https://gitlab.com/fi-sas/backoffice/compare/v1.1.6...v1.1.7) (2020-07-20)


### Bug Fixes

* **accommodation:** add fianacial reports ([d209365](https://gitlab.com/fi-sas/backoffice/commit/d209365))
* **accommodation:** minor fixs ([bb55b08](https://gitlab.com/fi-sas/backoffice/commit/bb55b08))
* **financial:** add provisory confirm button to payments ([a52fe1f](https://gitlab.com/fi-sas/backoffice/commit/a52fe1f))


### Features

* **accommodation:** add initial financial process ([e922d37](https://gitlab.com/fi-sas/backoffice/commit/e922d37))
* **ubike:** add module for application forms ([c544bf1](https://gitlab.com/fi-sas/backoffice/commit/c544bf1))



<a name="1.1.6"></a>
## [1.1.6](https://gitlab.com/fi-sas/backoffice/compare/v1.1.5...v1.1.6) (2020-07-14)


### Bug Fixes

* **auto-upload-file:** change function to upload ([c2df5e2](https://gitlab.com/fi-sas/backoffice/commit/c2df5e2))
* **social-support:** add auto update in files ([7b93df6](https://gitlab.com/fi-sas/backoffice/commit/7b93df6))
* **social-support:** add missing status ([9d7c750](https://gitlab.com/fi-sas/backoffice/commit/9d7c750))
* **social-support:** application status count fixed ([a7efd2a](https://gitlab.com/fi-sas/backoffice/commit/a7efd2a))
* **social-support:** change mentor to orintador ([9697104](https://gitlab.com/fi-sas/backoffice/commit/9697104))
* **social-support:** change modal  to change status in experience ([36f6e9f](https://gitlab.com/fi-sas/backoffice/commit/36f6e9f))
* **social-support:** change modal to change status ([a6c41ab](https://gitlab.com/fi-sas/backoffice/commit/a6c41ab))
* **social-support:** cnhage view application ans add schedule ([c4167b4](https://gitlab.com/fi-sas/backoffice/commit/c4167b4))
* **social-support:** limit number simultaneous candidates ([748ccff](https://gitlab.com/fi-sas/backoffice/commit/748ccff))
* **social-support:** modify view activity design ([88ff848](https://gitlab.com/fi-sas/backoffice/commit/88ff848))
* **social-support:** modify view experience design ([259d145](https://gitlab.com/fi-sas/backoffice/commit/259d145))
* **u-bike:** application of issues corrected in ms ([d53fb54](https://gitlab.com/fi-sas/backoffice/commit/d53fb54))
* **users:** add birth_date ([8bf93c1](https://gitlab.com/fi-sas/backoffice/commit/8bf93c1))
* **volunteering:** add auto update in files ([7f3f17c](https://gitlab.com/fi-sas/backoffice/commit/7f3f17c))
* **volunteering:** add missing status ([0b74c19](https://gitlab.com/fi-sas/backoffice/commit/0b74c19))
* **volunteering:** application status count fixed ([b9387d7](https://gitlab.com/fi-sas/backoffice/commit/b9387d7))
* **volunteering:** change mentor to orintador ([0c45ab8](https://gitlab.com/fi-sas/backoffice/commit/0c45ab8))
* **volunteering:** change modal  to change status in experience ([28a9b0e](https://gitlab.com/fi-sas/backoffice/commit/28a9b0e))
* **volunteering:** change modal to change status ([9469b4e](https://gitlab.com/fi-sas/backoffice/commit/9469b4e))
* **volunteering:** cnhage view application ans add schedule ([6783b56](https://gitlab.com/fi-sas/backoffice/commit/6783b56))
* **volunteering:** limit number simultaneous candidates ([880ec5a](https://gitlab.com/fi-sas/backoffice/commit/880ec5a))
* **volunteering:** modify view experience design ([6fb9340](https://gitlab.com/fi-sas/backoffice/commit/6fb9340))


### Features

* **auto-update:** add new input for udpate files ([64d5c3f](https://gitlab.com/fi-sas/backoffice/commit/64d5c3f))
* **infrastructure:** add documents types service ([8e2e8d3](https://gitlab.com/fi-sas/backoffice/commit/8e2e8d3))
* **styles:** add class disabled ([eb2daf8](https://gitlab.com/fi-sas/backoffice/commit/eb2daf8))



<a name="1.1.5"></a>
## [1.1.5](https://gitlab.com/fi-sas/backoffice/compare/v1.1.4...v1.1.5) (2020-07-07)


### Bug Fixes

* **accommodation:** fix reports url ([cdac5fa](https://gitlab.com/fi-sas/backoffice/commit/cdac5fa))
* **u-bike:** delete old files ([78c9d76](https://gitlab.com/fi-sas/backoffice/commit/78c9d76))
* **u-bike:** fix form bike ([805d11d](https://gitlab.com/fi-sas/backoffice/commit/805d11d))
* **u-bike:** fix inputs in reports page ([e1638ba](https://gitlab.com/fi-sas/backoffice/commit/e1638ba))
* **u-bike:** fix list and form ([f8083d6](https://gitlab.com/fi-sas/backoffice/commit/f8083d6))
* **u-bike:** moduele for training ([cff6073](https://gitlab.com/fi-sas/backoffice/commit/cff6073))
* **u-bike:** module for applications ([1f2aed2](https://gitlab.com/fi-sas/backoffice/commit/1f2aed2))
* **u-bike:** module for bike model ([6c005a1](https://gitlab.com/fi-sas/backoffice/commit/6c005a1))
* **u-bike:** module for bike typology ([3288a17](https://gitlab.com/fi-sas/backoffice/commit/3288a17))
* **u-bike:** module for configurarions ([6e85a3d](https://gitlab.com/fi-sas/backoffice/commit/6e85a3d))
* **u-bike:** module for resports ([26e64be](https://gitlab.com/fi-sas/backoffice/commit/26e64be))
* **u-bike:** module for trip ([33b3307](https://gitlab.com/fi-sas/backoffice/commit/33b3307))



<a name="1.1.4"></a>
## [1.1.4](https://gitlab.com/fi-sas/backoffice/compare/v1.1.3...v1.1.4) (2020-06-29)


### Bug Fixes

* **accommodation:** minor fix ([376dbe4](https://gitlab.com/fi-sas/backoffice/commit/376dbe4))


### Features

* **accommodation:** add out_of_date and household_distance fields ([97b86ef](https://gitlab.com/fi-sas/backoffice/commit/97b86ef))



<a name="1.1.3"></a>
## [1.1.3](https://gitlab.com/fi-sas/backoffice/compare/v1.1.2...v1.1.3) (2020-06-25)


### Bug Fixes

* **social-support:** applications divided per status ([349d551](https://gitlab.com/fi-sas/backoffice/commit/349d551))
* **volunteering:** application divided by status ([745a4c7](https://gitlab.com/fi-sas/backoffice/commit/745a4c7))


### Features

* **social-support:** add stats panel ([6a7087b](https://gitlab.com/fi-sas/backoffice/commit/6a7087b))
* **u-bike:** generate questionnaire file ([b3cfaf6](https://gitlab.com/fi-sas/backoffice/commit/b3cfaf6))



<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/fi-sas/backoffice/compare/v1.1.1...v1.1.2) (2020-06-22)


### Bug Fixes

* **notifications:** minor fixs ([ef4ea41](https://gitlab.com/fi-sas/backoffice/commit/ef4ea41))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/fi-sas/backoffice/compare/v1.1.0...v1.1.1) (2020-06-22)


### Bug Fixes

* **accommodation:** minor fixs ([6eba467](https://gitlab.com/fi-sas/backoffice/commit/6eba467))
* **u-bike:** add new data in application detail ([2bf8608](https://gitlab.com/fi-sas/backoffice/commit/2bf8608))


### Features

* **notifications:**  add alert-type module ([f3e331a](https://gitlab.com/fi-sas/backoffice/commit/f3e331a))
* **notifications:** add alert module ([5f9cc3f](https://gitlab.com/fi-sas/backoffice/commit/5f9cc3f))
* **notifications:** add notification-method module ([b3da483](https://gitlab.com/fi-sas/backoffice/commit/b3da483))
* **notifications:** change the layout of notifications mdule ([427169f](https://gitlab.com/fi-sas/backoffice/commit/427169f))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/fi-sas/backoffice/compare/v1.0.26...v1.1.0) (2020-06-17)


### Bug Fixes

* **accommodation:** minor fixs ([975be79](https://gitlab.com/fi-sas/backoffice/commit/975be79))
* **notifications:** change  alert retry button enable condition ([25bf6c4](https://gitlab.com/fi-sas/backoffice/commit/25bf6c4))
* **reports:** fix tempate model ([82409ee](https://gitlab.com/fi-sas/backoffice/commit/82409ee))


### Features

* **reports:** add reports form page ([88a5585](https://gitlab.com/fi-sas/backoffice/commit/88a5585))
* **reports:** add reports module to dashboard ([04a7730](https://gitlab.com/fi-sas/backoffice/commit/04a7730))
* **reports:** add templates list page ([984c47e](https://gitlab.com/fi-sas/backoffice/commit/984c47e))
* **reports:** creation of reports module ([f1ba498](https://gitlab.com/fi-sas/backoffice/commit/f1ba498))



<a name="1.0.26"></a>
## [1.0.26](https://gitlab.com/fi-sas/backoffice/compare/v1.0.25...v1.0.26) (2020-06-15)


### Features

* **notifications:** add retry button to failed notifications ([60bb54b](https://gitlab.com/fi-sas/backoffice/commit/60bb54b))



<a name="1.0.25"></a>
## [1.0.25](https://gitlab.com/fi-sas/backoffice/compare/v1.0.24...v1.0.25) (2020-06-15)


### Bug Fixes

* **social-support:** fix edit button experience ([15ea1a9](https://gitlab.com/fi-sas/backoffice/commit/15ea1a9))



<a name="1.0.24"></a>
## [1.0.24](https://gitlab.com/fi-sas/backoffice/compare/v1.0.23...v1.0.24) (2020-06-15)


### Features

* **notifications:** add test button for notifications templates ([ba76929](https://gitlab.com/fi-sas/backoffice/commit/ba76929))



<a name="1.0.23"></a>
## [1.0.23](https://gitlab.com/fi-sas/backoffice/compare/v1.0.22...v1.0.23) (2020-06-09)


### Bug Fixes

* **accommodation:** fiix list application persistent filter ([7225bd0](https://gitlab.com/fi-sas/backoffice/commit/7225bd0))



<a name="1.0.22"></a>
## [1.0.22](https://gitlab.com/fi-sas/backoffice/compare/v1.0.21...v1.0.22) (2020-06-09)


### Bug Fixes

* **accoommodation:** fix form booleans init ([23bb32a](https://gitlab.com/fi-sas/backoffice/commit/23bb32a))
* **notifications:** fix alert-type update form ([ff8d106](https://gitlab.com/fi-sas/backoffice/commit/ff8d106))
* **social-support:** add acknowledged status ([1cc349b](https://gitlab.com/fi-sas/backoffice/commit/1cc349b))


### Features

* **accommodation:** add global filters ([6997fea](https://gitlab.com/fi-sas/backoffice/commit/6997fea))
* **social-support:** list users interest in experience ([f070abc](https://gitlab.com/fi-sas/backoffice/commit/f070abc))



<a name="1.0.21"></a>
## [1.0.21](https://gitlab.com/fi-sas/backoffice/compare/v1.0.20...v1.0.21) (2020-06-06)


### Bug Fixes

* **accommodation:** minor fixes ([a8b46e7](https://gitlab.com/fi-sas/backoffice/commit/a8b46e7))



<a name="1.0.20"></a>
## [1.0.20](https://gitlab.com/fi-sas/backoffice/compare/v1.0.19...v1.0.20) (2020-06-06)


### Bug Fixes

* **accommodation:** add alert to fill all translations ([663be5e](https://gitlab.com/fi-sas/backoffice/commit/663be5e))
* **accommodation:** fix appllication phase form ([48e9eb4](https://gitlab.com/fi-sas/backoffice/commit/48e9eb4))


### Features

* **accommodation:** add opposotion status to applications ([2526098](https://gitlab.com/fi-sas/backoffice/commit/2526098))
* **accommodation:** add residence detail view ([a48d68d](https://gitlab.com/fi-sas/backoffice/commit/a48d68d))
* **accommodation:** update the sidebar total applications by status ([a6284d2](https://gitlab.com/fi-sas/backoffice/commit/a6284d2))
* **social-support:** add absence-reason accept ([1b96482](https://gitlab.com/fi-sas/backoffice/commit/1b96482))
* **volunteering:** create module for volunteering ([0184ada](https://gitlab.com/fi-sas/backoffice/commit/0184ada))



<a name="1.0.19"></a>
## [1.0.19](https://gitlab.com/fi-sas/backoffice/compare/v1.0.18...v1.0.19) (2020-05-22)


### Bug Fixes

* **social_support:** fix experiences form error ([6ec5011](https://gitlab.com/fi-sas/backoffice/commit/6ec5011))



<a name="1.0.18"></a>
## [1.0.18](https://gitlab.com/fi-sas/backoffice/compare/v1.0.17...v1.0.18) (2020-05-15)


### Bug Fixes

* **accommodation:** apllication show rooms available only ([1d3f114](https://gitlab.com/fi-sas/backoffice/commit/1d3f114))
* **accommodation:** fix the status machine of applications ([e7c2d9a](https://gitlab.com/fi-sas/backoffice/commit/e7c2d9a))
* **social-support:** change the status machine of applications ([723ed79](https://gitlab.com/fi-sas/backoffice/commit/723ed79))



<a name="1.0.17"></a>
## [1.0.17](https://gitlab.com/fi-sas/backoffice/compare/v1.0.16...v1.0.17) (2020-05-07)


### Bug Fixes

* **accommodation:** minor fixs ([cb9bf00](https://gitlab.com/fi-sas/backoffice/commit/cb9bf00))
* **social-support:** minor fixs ([1bf3c31](https://gitlab.com/fi-sas/backoffice/commit/1bf3c31))
* **social-support:** remove fiancial_email field ([b1f7e8a](https://gitlab.com/fi-sas/backoffice/commit/b1f7e8a))



<a name="1.0.16"></a>
## [1.0.16](https://gitlab.com/fi-sas/backoffice/compare/v1.0.15...v1.0.16) (2020-05-06)


### Bug Fixes

* **social-support:** minor fixes ([12dab65](https://gitlab.com/fi-sas/backoffice/commit/12dab65))



<a name="1.0.15"></a>
## [1.0.15](https://gitlab.com/fi-sas/backoffice/compare/v1.0.14...v1.0.15) (2020-05-06)


### Bug Fixes

* **accommodation:** add styling to billing dashboard ([de76406](https://gitlab.com/fi-sas/backoffice/commit/de76406))
* **social-support:** add more info to detail view ([426cf20](https://gitlab.com/fi-sas/backoffice/commit/426cf20))


### Features

* **accommodation:** add premute room ([fb106dd](https://gitlab.com/fi-sas/backoffice/commit/fb106dd))



<a name="1.0.14"></a>
## [1.0.14](https://gitlab.com/fi-sas/backoffice/compare/v1.0.13...v1.0.14) (2020-05-04)



<a name="1.0.13"></a>
## [1.0.13](https://gitlab.com/fi-sas/backoffice/compare/v1.0.12...v1.0.13) (2020-05-04)


### Bug Fixes

* **accommodation:** fix billing dashboard chart ([416184b](https://gitlab.com/fi-sas/backoffice/commit/416184b))



<a name="1.0.12"></a>
## [1.0.12](https://gitlab.com/fi-sas/backoffice/compare/v1.0.11...v1.0.12) (2020-05-04)


### Bug Fixes

* **accommodation:** fix rooms form validations ([71d3221](https://gitlab.com/fi-sas/backoffice/commit/71d3221))
* **financial:** remove shared changes from financial module ([f4b9a30](https://gitlab.com/fi-sas/backoffice/commit/f4b9a30))
* **private-accommodation:** minor fix user related ([5e82393](https://gitlab.com/fi-sas/backoffice/commit/5e82393))
* **sahred:** imga uploader prevent buttons to submit forms ([eb841a9](https://gitlab.com/fi-sas/backoffice/commit/eb841a9))
* **shared:** add showToolstrip option to list comp ([95d1207](https://gitlab.com/fi-sas/backoffice/commit/95d1207))
* **social-support:** add date validations ([6c7c5da](https://gitlab.com/fi-sas/backoffice/commit/6c7c5da))
* **social-support:** fix experience view fields ([c870da6](https://gitlab.com/fi-sas/backoffice/commit/c870da6))
* **social-support:** minor fixs on forms ([2de4145](https://gitlab.com/fi-sas/backoffice/commit/2de4145))


### Features

* **accommodation:** add billing metrics charts ([d1d8111](https://gitlab.com/fi-sas/backoffice/commit/d1d8111))
* **accommodation:** add extenions status changer ([2953b46](https://gitlab.com/fi-sas/backoffice/commit/2953b46))
* **accommodation:** add withdrawal status changer ([d7a9e7a](https://gitlab.com/fi-sas/backoffice/commit/d7a9e7a))



<a name="1.0.11"></a>
## [1.0.11](https://gitlab.com/fi-sas/backoffice/compare/v1.0.10...v1.0.11) (2020-04-23)


### Bug Fixes

* **themes:** fix ipvc theme ([e9c83af](https://gitlab.com/fi-sas/backoffice/commit/e9c83af))



<a name="1.0.10"></a>
## [1.0.10](https://gitlab.com/fi-sas/backoffice/compare/v1.0.9...v1.0.10) (2020-04-22)


### Bug Fixes

* **accommmodation:** minor fixes ([54f828f](https://gitlab.com/fi-sas/backoffice/commit/54f828f))
* **app:** minor fixes for release ([63d1ede](https://gitlab.com/fi-sas/backoffice/commit/63d1ede))
* **core:** add new theme envs ([25facfe](https://gitlab.com/fi-sas/backoffice/commit/25facfe))
* **financial:** changes user list ([8254114](https://gitlab.com/fi-sas/backoffice/commit/8254114))
* **financial:** changes user list ([3623442](https://gitlab.com/fi-sas/backoffice/commit/3623442))
* **financial:** correct rebase to corrent account movements ([9847d96](https://gitlab.com/fi-sas/backoffice/commit/9847d96))
* **financial:** correct rebase to corrent account movements ([c588326](https://gitlab.com/fi-sas/backoffice/commit/c588326))
* **financial:** css changes ([58be9ba](https://gitlab.com/fi-sas/backoffice/commit/58be9ba))
* **financial:** fix sider menu link ([4eeeb3f](https://gitlab.com/fi-sas/backoffice/commit/4eeeb3f))
* **medias:** add upload function ([e2f23ab](https://gitlab.com/fi-sas/backoffice/commit/e2f23ab))
* **private-accomodation:** fix list owners, approve and reject ([80deda5](https://gitlab.com/fi-sas/backoffice/commit/80deda5))
* **shared:** minor fixes ([474d5b3](https://gitlab.com/fi-sas/backoffice/commit/474d5b3))
* **social-support:** add fields to list experiences ([237d189](https://gitlab.com/fi-sas/backoffice/commit/237d189))
* **social-support:** add name of user to list applications ([6b356bc](https://gitlab.com/fi-sas/backoffice/commit/6b356bc))
* **spcoial-support:** temporary notifications tab on applications ([6725577](https://gitlab.com/fi-sas/backoffice/commit/6725577))
* **users:** fix user form ([3e84376](https://gitlab.com/fi-sas/backoffice/commit/3e84376))


### Features

* **accommdation:** adption to new layout ([0ae0f41](https://gitlab.com/fi-sas/backoffice/commit/0ae0f41))
* **core:** add new layout dashboard ([95862f4](https://gitlab.com/fi-sas/backoffice/commit/95862f4))
* **core:** add new layout to core components ([1703dd7](https://gitlab.com/fi-sas/backoffice/commit/1703dd7))
* **sahred:** flatObject pipe created ([fc82103](https://gitlab.com/fi-sas/backoffice/commit/fc82103))
* **shared:** add a user select component ([3741443](https://gitlab.com/fi-sas/backoffice/commit/3741443))
* **shared:** added table helper class ([03e6254](https://gitlab.com/fi-sas/backoffice/commit/03e6254))
* **social_support:** add applications list ([5534ee8](https://gitlab.com/fi-sas/backoffice/commit/5534ee8))
* **social_support:** add new layout experience form ([7c53a3f](https://gitlab.com/fi-sas/backoffice/commit/7c53a3f))
* **social-support:**  add applications filters ([10684f5](https://gitlab.com/fi-sas/backoffice/commit/10684f5))
* **social-support:**  add configurations page ([c3d0cea](https://gitlab.com/fi-sas/backoffice/commit/c3d0cea))
* **social-support:**  add filters to experiences ([608de65](https://gitlab.com/fi-sas/backoffice/commit/608de65))
* **social-support:** add activities table ([48c479f](https://gitlab.com/fi-sas/backoffice/commit/48c479f))
* **social-support:** add application view history ([0f55a85](https://gitlab.com/fi-sas/backoffice/commit/0f55a85))
* **social-support:** add attendaces list and form ([71bdde6](https://gitlab.com/fi-sas/backoffice/commit/71bdde6))
* **social-support:** add delete experience ([0a4f37e](https://gitlab.com/fi-sas/backoffice/commit/0a4f37e))
* **social-support:** add detail view of experience ([60744bb](https://gitlab.com/fi-sas/backoffice/commit/60744bb))
* **social-support:** add experience view history ([5cf754d](https://gitlab.com/fi-sas/backoffice/commit/5cf754d))
* **social-support:** add experiences list ([7465a1f](https://gitlab.com/fi-sas/backoffice/commit/7465a1f))
* **social-support:** add filters to activities ([6232c6d](https://gitlab.com/fi-sas/backoffice/commit/6232c6d))
* **social-support:** add monthly report list and form ([6abf20d](https://gitlab.com/fi-sas/backoffice/commit/6abf20d))
* **social-support:** add payment grid form and list ([ef82181](https://gitlab.com/fi-sas/backoffice/commit/ef82181))
* **social-support:** add view of activity ([0fe68d9](https://gitlab.com/fi-sas/backoffice/commit/0fe68d9))
* **social-support:** change structure of module ([d1b0fa4](https://gitlab.com/fi-sas/backoffice/commit/d1b0fa4))
* **social-support:** delete activity ([886a690](https://gitlab.com/fi-sas/backoffice/commit/886a690))
* **social-support:** form for creation of activities ([2f3a22c](https://gitlab.com/fi-sas/backoffice/commit/2f3a22c))
* **social-support:** update activites ([39bcc54](https://gitlab.com/fi-sas/backoffice/commit/39bcc54))
* **social-support:** view application details ([d128cb3](https://gitlab.com/fi-sas/backoffice/commit/d128cb3))



<a name="1.0.9"></a>
## [1.0.9](https://gitlab.com/fi-sas/backoffice/compare/v1.0.8...v1.0.9) (2020-03-17)


### Bug Fixes

* **u-bike:** fix start date in reports ([48a5d26](https://gitlab.com/fi-sas/backoffice/commit/48a5d26))


### Features

* **u-bike:** u-bike reports ([adf219e](https://gitlab.com/fi-sas/backoffice/commit/adf219e))



<a name="1.0.8"></a>
## [1.0.8](https://gitlab.com/fi-sas/backoffice/compare/v1.0.7...v1.0.8) (2020-03-05)


### Bug Fixes

* **.githnore:** ignore .env ([cf72df2](https://gitlab.com/fi-sas/backoffice/commit/cf72df2))
* **docker-compose:** add always restart in backoffice ([da82027](https://gitlab.com/fi-sas/backoffice/commit/da82027))
* **list-applicantions:** applicantions changes status ([0efaf65](https://gitlab.com/fi-sas/backoffice/commit/0efaf65))
* **u-bike:** fix the consumption array data ([2031d6e](https://gitlab.com/fi-sas/backoffice/commit/2031d6e))



<a name="1.0.7"></a>
## [1.0.7](https://gitlab.com/fi-sas/backoffice/compare/v1.0.6...v1.0.7) (2020-02-17)


### Bug Fixes

* **alimentation:** add fieldannulment_maximum_hour ([f04b3af](https://gitlab.com/fi-sas/backoffice/commit/f04b3af))


### Features

* **alimentation:** add meals to dish type prices ([18b26e9](https://gitlab.com/fi-sas/backoffice/commit/18b26e9))
* **alimentation:** add minimum hour  to disponibility of dishs types ([6e01d2e](https://gitlab.com/fi-sas/backoffice/commit/6e01d2e))



<a name="1.0.6"></a>
## [1.0.6](https://gitlab.com/fi-sas/backoffice/compare/v1.0.5...v1.0.6) (2020-01-20)


### Bug Fixes

* **u-bike:** fix issue and create homepage form ([30da79e](https://gitlab.com/fi-sas/backoffice/commit/30da79e))


### Features

* **alimentation:** add option of copy menu, day, week ([c5021ce](https://gitlab.com/fi-sas/backoffice/commit/c5021ce))
* **alimentation:** form multiple menu creation ([a5cb030](https://gitlab.com/fi-sas/backoffice/commit/a5cb030))



<a name="1.0.5"></a>
## [1.0.5](https://gitlab.com/fi-sas/backoffice/compare/v1.0.4...v1.0.5) (2020-01-13)


### Features

* **ubike:** add form bike model and list bike model ([d15ba71](https://gitlab.com/fi-sas/backoffice/commit/d15ba71))



<a name="1.0.4"></a>
## [1.0.4](https://gitlab.com/fi-sas/backoffice/compare/v1.0.3...v1.0.4) (2019-12-23)


### Bug Fixes

* **financial:** disable chager form ([2f83ff9](https://gitlab.com/fi-sas/backoffice/commit/2f83ff9))
* **themes:** add extra vars to themes ([74d9384](https://gitlab.com/fi-sas/backoffice/commit/74d9384))
* **ubike:** chage style ([b617ce3](https://gitlab.com/fi-sas/backoffice/commit/b617ce3))


### Features

* **backoffice:** add new applications logic ([a09509e](https://gitlab.com/fi-sas/backoffice/commit/a09509e))
* **ubike:** application with new states and translations ([ae183f5](https://gitlab.com/fi-sas/backoffice/commit/ae183f5))



<a name="1.0.3"></a>
## [1.0.3](https://gitlab.com/fi-sas/backoffice/compare/v1.0.2...v1.0.3) (2019-11-15)


### Features

* **accommodation:** add new report clothes map ([e76e0cd](https://gitlab.com/fi-sas/backoffice/commit/e76e0cd))



<a name="1.0.2"></a>
## [1.0.2](https://gitlab.com/fi-sas/backoffice/compare/v1.0.1...v1.0.2) (2019-11-08)


### Bug Fixes

* **private-accommodation:** fix complaints ([bfe7703](https://gitlab.com/fi-sas/backoffice/commit/bfe7703))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/fi-sas/backoffice/compare/v1.0.0...v1.0.1) (2019-11-07)


### Bug Fixes

* **calender:** fix bug ([3d3508d](https://gitlab.com/fi-sas/backoffice/commit/3d3508d))
* **social_support:** fix error ([41b60f1](https://gitlab.com/fi-sas/backoffice/commit/41b60f1))
* **u_bike:** fix trip error ([42225e6](https://gitlab.com/fi-sas/backoffice/commit/42225e6))
* **u-bike:** fix applications ([9fb054e](https://gitlab.com/fi-sas/backoffice/commit/9fb054e))


### Features

* **u_bike:** add form to trips ([3318371](https://gitlab.com/fi-sas/backoffice/commit/3318371))
* **u-bike:** create project stats ([76c1bf2](https://gitlab.com/fi-sas/backoffice/commit/76c1bf2))
* **u-bike:** create regulamentation form ([41da359](https://gitlab.com/fi-sas/backoffice/commit/41da359))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/fi-sas/backoffice/compare/v0.19.28...v1.0.0) (2019-11-07)


### Bug Fixes

* **accommodation:** allow to access all residences ([b139d42](https://gitlab.com/fi-sas/backoffice/commit/b139d42))


### Features

* **accommodation:** add occupants to the dashboard ([0a1551c](https://gitlab.com/fi-sas/backoffice/commit/0a1551c))



<a name="0.19.28"></a>
## [0.19.28](https://gitlab.com/fi-sas/backoffice/compare/v0.19.27...v0.19.28) (2019-10-31)



<a name="0.19.27"></a>
## [0.19.27](https://gitlab.com/fi-sas/backoffice/compare/v0.19.26...v0.19.27) (2019-10-29)


### Bug Fixes

* **configuration:** fix imports ([0341e7c](https://gitlab.com/fi-sas/backoffice/commit/0341e7c))



<a name="0.19.26"></a>
## [0.19.26](https://gitlab.com/fi-sas/backoffice/compare/v0.19.25...v0.19.26) (2019-10-29)


### Bug Fixes

* **accommodation:** add occupation of the residences ([354597a](https://gitlab.com/fi-sas/backoffice/commit/354597a))
* **accommodation:** fix issues on applications ([079b0d1](https://gitlab.com/fi-sas/backoffice/commit/079b0d1))
* **configurations:** fix import ([8703500](https://gitlab.com/fi-sas/backoffice/commit/8703500))


### Features

* **accommodation:** add field max_occupants_number to typologies ([af74cd5](https://gitlab.com/fi-sas/backoffice/commit/af74cd5))
* **accommodation:** add rooms map to dashboard ([ce1870b](https://gitlab.com/fi-sas/backoffice/commit/ce1870b))
* **configurations:** add schools courseDegrees and courses ([193fa4b](https://gitlab.com/fi-sas/backoffice/commit/193fa4b))



<a name="0.19.25"></a>
## [0.19.25](https://gitlab.com/fi-sas/backoffice/compare/v0.19.24...v0.19.25) (2019-10-29)


### Features

* **accommodation:** add gallery of images to residences ([3a77118](https://gitlab.com/fi-sas/backoffice/commit/3a77118))



<a name="0.19.24"></a>
## [0.19.24](https://gitlab.com/fi-sas/backoffice/compare/v0.19.23...v0.19.24) (2019-10-15)


### Bug Fixes

* **applications:** backoffice data update fix ([a296c11](https://gitlab.com/fi-sas/backoffice/commit/a296c11))



<a name="0.19.23"></a>
## [0.19.23](https://gitlab.com/fi-sas/backoffice/compare/v0.19.22...v0.19.23) (2019-10-11)


### Bug Fixes

* **accommodation:** add filters to rooms list ([bb1739d](https://gitlab.com/fi-sas/backoffice/commit/bb1739d))


### Features

* **accommodation:** add new new type of report to accommodation ([d755d84](https://gitlab.com/fi-sas/backoffice/commit/d755d84))



<a name="0.19.22"></a>
## [0.19.22](https://gitlab.com/fi-sas/backoffice/compare/v0.19.21...v0.19.22) (2019-10-11)


### Bug Fixes

* **communication:** fix location update ([c7bc00c](https://gitlab.com/fi-sas/backoffice/commit/c7bc00c))
* **communication:** location text area size ([0c4571f](https://gitlab.com/fi-sas/backoffice/commit/0c4571f))



<a name="0.19.21"></a>
## [0.19.21](https://gitlab.com/fi-sas/backoffice/compare/v0.19.20...v0.19.21) (2019-10-08)



<a name="0.19.20"></a>
## [0.19.20](https://gitlab.com/fi-sas/backoffice/compare/v0.19.19...v0.19.20) (2019-10-04)



<a name="0.19.19"></a>
## [0.19.19](https://gitlab.com/fi-sas/backoffice/compare/v0.19.18...v0.19.19) (2019-10-03)



<a name="0.19.18"></a>
## [0.19.18](https://gitlab.com/fi-sas/backoffice/compare/v0.19.17...v0.19.18) (2019-10-02)


### Bug Fixes

* **accommodation:** fix reports request ([5dee94a](https://gitlab.com/fi-sas/backoffice/commit/5dee94a))
* **accommodation:** minor fixes ([f3ef489](https://gitlab.com/fi-sas/backoffice/commit/f3ef489))
* **communication:** add max size of 50mb to videos on posts ([c67ae85](https://gitlab.com/fi-sas/backoffice/commit/c67ae85))
* **communication:** max of video upload to 100mb ([16d079c](https://gitlab.com/fi-sas/backoffice/commit/16d079c))


### Features

* **communication:** add location feild to posts ([b9e1696](https://gitlab.com/fi-sas/backoffice/commit/b9e1696))



<a name="0.19.17"></a>
## [0.19.17](https://gitlab.com/fi-sas/backoffice/compare/v0.19.16...v0.19.17) (2019-08-11)


### Bug Fixes

* **app:** version checker url fix ([bc4fda2](https://gitlab.com/fi-sas/backoffice/commit/bc4fda2))



<a name="0.19.16"></a>
## [0.19.16](https://gitlab.com/fi-sas/backoffice/compare/v0.19.15...v0.19.16) (2019-08-10)


### Bug Fixes

* **app:** correction of environment import ([736ba6c](https://gitlab.com/fi-sas/backoffice/commit/736ba6c))



<a name="0.19.15"></a>
## [0.19.15](https://gitlab.com/fi-sas/backoffice/compare/v0.19.14...v0.19.15) (2019-08-10)



<a name="0.19.14"></a>
## [0.19.14](https://gitlab.com/fi-sas/backoffice/compare/v0.19.13...v0.19.14) (2019-08-10)


### Features

* **accommodation:** add observations field to dispach ([7292095](https://gitlab.com/fi-sas/backoffice/commit/7292095))



<a name="0.19.13"></a>
## [0.19.13](https://gitlab.com/fi-sas/backoffice/compare/v0.19.12...v0.19.13) (2019-08-08)


### Bug Fixes

* **communication:** add date field to post ([d3b5838](https://gitlab.com/fi-sas/backoffice/commit/d3b5838))


### Features

* **communication:** add date field to post ([daf1055](https://gitlab.com/fi-sas/backoffice/commit/daf1055))
* **core:** add version checker to update client page ([d8b62f3](https://gitlab.com/fi-sas/backoffice/commit/d8b62f3))



<a name="0.19.12"></a>
## [0.19.12](https://gitlab.com/fi-sas/backoffice/compare/v0.19.11...v0.19.12) (2019-08-06)


### Features

* **accommodation:** add bulk to dispach page ([9b8c698](https://gitlab.com/fi-sas/backoffice/commit/9b8c698))



<a name="0.19.11"></a>
## [0.19.11](https://gitlab.com/fi-sas/backoffice/compare/v0.19.10...v0.19.11) (2019-08-05)


### Bug Fixes

* **reports:** add models to reports model ([7b073fe](https://gitlab.com/fi-sas/backoffice/commit/7b073fe))


### Features

* **accommodation:** add reports page ([7d067a7](https://gitlab.com/fi-sas/backoffice/commit/7d067a7))



<a name="0.19.10"></a>
## [0.19.10](https://gitlab.com/fi-sas/backoffice/compare/v0.19.9...v0.19.10) (2019-07-30)


### Bug Fixes

* **shared:** form patch condition fix ([375768f](https://gitlab.com/fi-sas/backoffice/commit/375768f))



<a name="0.19.9"></a>
## [0.19.9](https://gitlab.com/fi-sas/backoffice/compare/v0.19.8...v0.19.9) (2019-07-29)


### Bug Fixes

* **core:** check permission of subsubitems of sider ([293928a](https://gitlab.com/fi-sas/backoffice/commit/293928a))



<a name="0.19.8"></a>
## [0.19.8](https://gitlab.com/fi-sas/backoffice/compare/v0.19.7...v0.19.8) (2019-07-27)


### Bug Fixes

* **shared:** list update selected list when item deleted ([07f9de8](https://gitlab.com/fi-sas/backoffice/commit/07f9de8))



<a name="0.19.7"></a>
## [0.19.7](https://gitlab.com/fi-sas/backoffice/compare/v0.19.6...v0.19.7) (2019-07-27)


### Bug Fixes

* **shared:** list selected item update on delete ([14da132](https://gitlab.com/fi-sas/backoffice/commit/14da132))



<a name="0.19.6"></a>
## [0.19.6](https://gitlab.com/fi-sas/backoffice/compare/v0.19.5...v0.19.6) (2019-07-26)


### Bug Fixes

* **accommodation:** minor fixes accommodation ([2c1acd7](https://gitlab.com/fi-sas/backoffice/commit/2c1acd7))



<a name="0.19.5"></a>
## [0.19.5](https://gitlab.com/fi-sas/backoffice/compare/v0.19.4...v0.19.5) (2019-07-22)


### Bug Fixes

* **accommoation:** application viewer condition error ([092d173](https://gitlab.com/fi-sas/backoffice/commit/092d173))



<a name="0.19.4"></a>
## [0.19.4](https://gitlab.com/fi-sas/backoffice/compare/v0.19.3...v0.19.4) (2019-07-22)


### Bug Fixes

* **accommodation:** add scopes to lateral menu ([6952ef2](https://gitlab.com/fi-sas/backoffice/commit/6952ef2))
* **auth:** fix users form update ([6f3dc8b](https://gitlab.com/fi-sas/backoffice/commit/6f3dc8b))


### Features

* **shared:**  form component can patch entities ([bb19172](https://gitlab.com/fi-sas/backoffice/commit/bb19172))



<a name="0.19.3"></a>
## [0.19.3](https://gitlab.com/fi-sas/backoffice/compare/v0.19.2...v0.19.3) (2019-07-12)


### Features

* **accomodation:** add new application status machine ([ac78e48](https://gitlab.com/fi-sas/backoffice/commit/ac78e48))



<a name="0.19.2"></a>
## [0.19.2](https://gitlab.com/fi-sas/backoffice/compare/v0.19.1...v0.19.2) (2019-07-01)


### Bug Fixes

* **accommodation:** change status to multiple users ([94459e0](https://gitlab.com/fi-sas/backoffice/commit/94459e0))
* **alimentation:** minor fixes on meals page ([770c373](https://gitlab.com/fi-sas/backoffice/commit/770c373))
* **shared:**  list remove deselectAll on multiple selection  disabled ([58200b9](https://gitlab.com/fi-sas/backoffice/commit/58200b9))


### Features

* **backoffice:** add router and http loader ([ae819b7](https://gitlab.com/fi-sas/backoffice/commit/ae819b7))
* **shared:** add multi select to list component ([2835051](https://gitlab.com/fi-sas/backoffice/commit/2835051))



<a name="0.19.1"></a>
## [0.19.1](https://gitlab.com/fi-sas/backoffice/compare/v0.19.0...v0.19.1) (2019-06-19)


### Bug Fixes

* **accommodation:** add lective year field to list ([9f6d99b](https://gitlab.com/fi-sas/backoffice/commit/9f6d99b))



<a name="0.19.0"></a>
# [0.19.0](https://gitlab.com/fi-sas/backoffice/compare/v0.18.6...v0.19.0) (2019-06-18)


### Bug Fixes

* **accommodation:** add noew fields to application form ([07badc8](https://gitlab.com/fi-sas/backoffice/commit/07badc8))
* **auth:** fix auth messsage error on login ([c78b78d](https://gitlab.com/fi-sas/backoffice/commit/c78b78d))
* **shared:** fix muliselection on the list component and remove nz-list ([db5cd06](https://gitlab.com/fi-sas/backoffice/commit/db5cd06))
* **shared:** remove error 404 from error interceptor ([6934865](https://gitlab.com/fi-sas/backoffice/commit/6934865))


### Features

* **accommodation:** add application status processement ([8a8813d](https://gitlab.com/fi-sas/backoffice/commit/8a8813d))
* **accommodation:** landing page for accommodation module ([b1501b2](https://gitlab.com/fi-sas/backoffice/commit/b1501b2))
* **accommodation:** modal for patch applications ([0c24596](https://gitlab.com/fi-sas/backoffice/commit/0c24596))
* **core:** add badges to sider items ([30a7a14](https://gitlab.com/fi-sas/backoffice/commit/30a7a14))



<a name="0.18.6"></a>
## [0.18.6](https://gitlab.com/fi-sas/backoffice/compare/v0.18.5...v0.18.6) (2019-06-03)



<a name="0.18.5"></a>
## [0.18.5](https://gitlab.com/fi-sas/backoffice/compare/v0.18.4...v0.18.5) (2019-06-03)



<a name="0.18.4"></a>
## [0.18.4](https://gitlab.com/fi-sas/backoffice/compare/v0.18.3...v0.18.4) (2019-06-03)



<a name="0.18.3"></a>
## [0.18.3](https://gitlab.com/fi-sas/backoffice/compare/v0.18.2...v0.18.3) (2019-05-15)



<a name="0.18.2"></a>
## [0.18.2](https://gitlab.com/fi-sas/backoffice/compare/v0.18.1...v0.18.2) (2019-05-15)



<a name="0.18.1"></a>
## [0.18.1](https://gitlab.com/fi-sas/backoffice/compare/v0.18.0...v0.18.1) (2019-05-15)



<a name="0.18.0"></a>
# [0.18.0](https://gitlab.com/fi-sas/backoffice/compare/v0.17.4...v0.18.0) (2019-05-15)


### Bug Fixes

* **application:** fix view ([32c86b6](https://gitlab.com/fi-sas/backoffice/commit/32c86b6))
* **bus:** add iva and current account in price tables ([d4e6466](https://gitlab.com/fi-sas/backoffice/commit/d4e6466))
* **bus:** delete the user type component ([a8f7ebd](https://gitlab.com/fi-sas/backoffice/commit/a8f7ebd))
* **bus:** fix timetable form ([f88831e](https://gitlab.com/fi-sas/backoffice/commit/f88831e))
* **social_support:** add  a new method to change status ([6a194c9](https://gitlab.com/fi-sas/backoffice/commit/6a194c9))
* **u-bike:** change module ([150df76](https://gitlab.com/fi-sas/backoffice/commit/150df76))
* **u-bike:** change typologies code for new layout ([e74b150](https://gitlab.com/fi-sas/backoffice/commit/e74b150))
* **u-bike:** fix grammatical error in application ([4282b44](https://gitlab.com/fi-sas/backoffice/commit/4282b44))
* **u-bike:** update code for trips ([04c3fab](https://gitlab.com/fi-sas/backoffice/commit/04c3fab))


### Features

* **backoffice:** add multiple configurations options to build and dev ([0f5e16e](https://gitlab.com/fi-sas/backoffice/commit/0f5e16e))
* **configurations:** add components modal to devices form ([bc623b0](https://gitlab.com/fi-sas/backoffice/commit/bc623b0))
* **configurations:** add new layout to form and list taxes ([200d7c1](https://gitlab.com/fi-sas/backoffice/commit/200d7c1))
* **configurations:** add new layout to languages form and list ([b0d0160](https://gitlab.com/fi-sas/backoffice/commit/b0d0160))
* **configurations:** add new layout to list and form of footers ([155f1b9](https://gitlab.com/fi-sas/backoffice/commit/155f1b9))
* **configurations:** add services form ([2a97f49](https://gitlab.com/fi-sas/backoffice/commit/2a97f49))
* **configurations:** add services list ([bd0f205](https://gitlab.com/fi-sas/backoffice/commit/bd0f205))
* **configurations:** creation of form contacts page ([2439a5d](https://gitlab.com/fi-sas/backoffice/commit/2439a5d))
* **configurations:** creation of list contacts page ([9258f8c](https://gitlab.com/fi-sas/backoffice/commit/9258f8c))
* **configurations:** list and form of devices new layout ([49766a5](https://gitlab.com/fi-sas/backoffice/commit/49766a5))
* **configurations:** new list groups and form group layout ([b9c876f](https://gitlab.com/fi-sas/backoffice/commit/b9c876f))
* **shared:** add patch to repository ([e2cd2a5](https://gitlab.com/fi-sas/backoffice/commit/e2cd2a5))
* **social_support:** creation of experience view ([577f99d](https://gitlab.com/fi-sas/backoffice/commit/577f99d))
* **style:** adding variables to the css theme ([24e4e87](https://gitlab.com/fi-sas/backoffice/commit/24e4e87))
* **u-bike:** creation of bike form ([2a46c25](https://gitlab.com/fi-sas/backoffice/commit/2a46c25))
* **u-bike:** creation of bikes list ([f8bdab1](https://gitlab.com/fi-sas/backoffice/commit/f8bdab1))



<a name="0.17.4"></a>
## [0.17.4](https://gitlab.com/fi-sas/backoffice/compare/v0.17.3...v0.17.4) (2019-04-24)



<a name="0.17.3"></a>
## [0.17.3](https://gitlab.com/fi-sas/backoffice/compare/v0.17.2...v0.17.3) (2019-04-24)



<a name="0.17.2"></a>
## [0.17.2](https://gitlab.com/fi-sas/backoffice/compare/v0.17.1...v0.17.2) (2019-04-24)



<a name="0.17.1"></a>
## [0.17.1](https://gitlab.com/fi-sas/backoffice/compare/v0.16.5...v0.17.1) (2019-04-24)


### Bug Fixes

* **alimentation:** minor's fix of menu form and  list ([f474fa4](https://gitlab.com/fi-sas/backoffice/commit/f474fa4))
* **core:**  add padding to sider menu button on the header ([206c510](https://gitlab.com/fi-sas/backoffice/commit/206c510))
* **shared:**  add selectLabelKey when searchOnServer is true ([28b7c90](https://gitlab.com/fi-sas/backoffice/commit/28b7c90))
* **shared:** add picker month and year ([7256bc3](https://gitlab.com/fi-sas/backoffice/commit/7256bc3))
* **shared:** add select input searchTrigger ([56a304c](https://gitlab.com/fi-sas/backoffice/commit/56a304c))
* **shared:** fix error on delete item from list ([b106d0c](https://gitlab.com/fi-sas/backoffice/commit/b106d0c))
* **shared:** fix key of multilanguage field from form generator ([ce505d4](https://gitlab.com/fi-sas/backoffice/commit/ce505d4))
* **shared:** fix parser for input number ([2f58496](https://gitlab.com/fi-sas/backoffice/commit/2f58496))
* **shared:** fix repository ([eec4a60](https://gitlab.com/fi-sas/backoffice/commit/eec4a60))
* **shared:** form select input options is async now ([b683a35](https://gitlab.com/fi-sas/backoffice/commit/b683a35))
* **shared:** form show load indicator and disable form on request http ([c6d77a5](https://gitlab.com/fi-sas/backoffice/commit/c6d77a5))
* **shared:** form submit function is now a observable ([8e94860](https://gitlab.com/fi-sas/backoffice/commit/8e94860))
* **shared:** set new properties in input number ([7c0152b](https://gitlab.com/fi-sas/backoffice/commit/7c0152b))
* **shared:** upload-file name error ([45a6ed8](https://gitlab.com/fi-sas/backoffice/commit/45a6ed8))
* **social_support:** fix bug ([bdbf46b](https://gitlab.com/fi-sas/backoffice/commit/bdbf46b))


### Features

* **alimentation:** add family form new layout ([4ab3ab1](https://gitlab.com/fi-sas/backoffice/commit/4ab3ab1))
* **alimentation:** add form wharehouse new layout ([0065db7](https://gitlab.com/fi-sas/backoffice/commit/0065db7))
* **alimentation:** add new form to units ([6d1e17b](https://gitlab.com/fi-sas/backoffice/commit/6d1e17b))
* **alimentation:** add new layout allergens form ([23d1e58](https://gitlab.com/fi-sas/backoffice/commit/23d1e58))
* **alimentation:** add new layout to list wharehouses ([ea07001](https://gitlab.com/fi-sas/backoffice/commit/ea07001))
* **alimentation:** add new layout to services form ([256fd52](https://gitlab.com/fi-sas/backoffice/commit/256fd52))
* **alimentation:** add new layout to services list ([36bd3e6](https://gitlab.com/fi-sas/backoffice/commit/36bd3e6))
* **alimentation:** add new laytout to recipes list ([d179e86](https://gitlab.com/fi-sas/backoffice/commit/d179e86))
* **alimentation:** add new list to units ([0ff4edd](https://gitlab.com/fi-sas/backoffice/commit/0ff4edd))
* **alimentation:** add nutrients and allergens to form product ([094a305](https://gitlab.com/fi-sas/backoffice/commit/094a305))
* **alimentation:** add users modal to fiscal entities ([8e5d5d8](https://gitlab.com/fi-sas/backoffice/commit/8e5d5d8))
* **alimentation:** allergens list new layout ([0cae896](https://gitlab.com/fi-sas/backoffice/commit/0cae896))
* **alimentation:** complements form new layout ([6557e8d](https://gitlab.com/fi-sas/backoffice/commit/6557e8d))
* **alimentation:** creation of list complements on new layout ([6c02e6a](https://gitlab.com/fi-sas/backoffice/commit/6c02e6a))
* **alimentation:** creation wharehouse users access modal ([0815336](https://gitlab.com/fi-sas/backoffice/commit/0815336))
* **alimentation:** dish types list new layout ([12f57c5](https://gitlab.com/fi-sas/backoffice/commit/12f57c5))
* **alimentation:** dishtype form new layout ([138b472](https://gitlab.com/fi-sas/backoffice/commit/138b472))
* **alimentation:** form dish new layout ([c829b1e](https://gitlab.com/fi-sas/backoffice/commit/c829b1e))
* **alimentation:** form products new layout ([5b22635](https://gitlab.com/fi-sas/backoffice/commit/5b22635))
* **alimentation:** form products new layout ([dfac1b3](https://gitlab.com/fi-sas/backoffice/commit/dfac1b3))
* **alimentation:** list dish new layout ([5069f60](https://gitlab.com/fi-sas/backoffice/commit/5069f60))
* **alimentation:** list dish new layout ([8834015](https://gitlab.com/fi-sas/backoffice/commit/8834015))
* **alimentation:** list nutrients new layout ([2ddca30](https://gitlab.com/fi-sas/backoffice/commit/2ddca30))
* **alimentation:** new form entity ([b6154d2](https://gitlab.com/fi-sas/backoffice/commit/b6154d2))
* **alimentation:** new form nutrient layout ([96fd567](https://gitlab.com/fi-sas/backoffice/commit/96fd567))
* **alimentation:** new layout to families list ([916cb42](https://gitlab.com/fi-sas/backoffice/commit/916cb42))
* **alimentation:** new list entities layout ([f661bab](https://gitlab.com/fi-sas/backoffice/commit/f661bab))
* **alimentation:** new list schools layout ([518e97f](https://gitlab.com/fi-sas/backoffice/commit/518e97f))
* **alimentation:** schools form new layout ([d288323](https://gitlab.com/fi-sas/backoffice/commit/d288323))
* **gamification:**  creation of competitions list ([625cedc](https://gitlab.com/fi-sas/backoffice/commit/625cedc))
* **gamification:** creation of challenge form ([864bd53](https://gitlab.com/fi-sas/backoffice/commit/864bd53))
* **gamification:** creation of challenge list ([228132f](https://gitlab.com/fi-sas/backoffice/commit/228132f))
* **gamification:** creation of competition form ([0921051](https://gitlab.com/fi-sas/backoffice/commit/0921051))
* **gamification:** creation of players list ([0276f2a](https://gitlab.com/fi-sas/backoffice/commit/0276f2a))
* **gamification:** creation of rankings for challenges ([d61328e](https://gitlab.com/fi-sas/backoffice/commit/d61328e))
* **lib/core:** add patch to resources service ([3e57224](https://gitlab.com/fi-sas/backoffice/commit/3e57224))
* **shared:** add dynamic search to form generator ([a241790](https://gitlab.com/fi-sas/backoffice/commit/a241790))
* **shared:** add formPadding and formScroll options to form generator ([4e51e02](https://gitlab.com/fi-sas/backoffice/commit/4e51e02))
* **shared:** add hidden field feature to form generator ([8f0fce5](https://gitlab.com/fi-sas/backoffice/commit/8f0fce5))
* **shared:** add input type time to formGenerator ([6a61ca4](https://gitlab.com/fi-sas/backoffice/commit/6a61ca4))
* **shared:** add persistent url params property to repository ([a463e46](https://gitlab.com/fi-sas/backoffice/commit/a463e46))
* **shared:** add prefix and sufix to select component of form generator ([40319f2](https://gitlab.com/fi-sas/backoffice/commit/40319f2))
* **shared:** add translation field to list component ([6dc42d4](https://gitlab.com/fi-sas/backoffice/commit/6dc42d4))
* **shared:** condition params on form generator fields ([dc8f34c](https://gitlab.com/fi-sas/backoffice/commit/dc8f34c))
* **shared:** field-select add dynamic params ([9330521](https://gitlab.com/fi-sas/backoffice/commit/9330521))
* **shared:** input number formatter ([8bed3dc](https://gitlab.com/fi-sas/backoffice/commit/8bed3dc))
* **social_support:** bug fix ([bef8308](https://gitlab.com/fi-sas/backoffice/commit/bef8308))
* **social_support:** creation of activities list ([0f1fb1f](https://gitlab.com/fi-sas/backoffice/commit/0f1fb1f))
* **social_support:** creation of activity form ([f1b3b52](https://gitlab.com/fi-sas/backoffice/commit/f1b3b52))
* **social_support:** creation of application list for volunteering ([5d4a6e7](https://gitlab.com/fi-sas/backoffice/commit/5d4a6e7))
* **social_support:** creation of applications list for bc ([bddfc4d](https://gitlab.com/fi-sas/backoffice/commit/bddfc4d))
* **social_support:** creation of attendance form ([861dcfd](https://gitlab.com/fi-sas/backoffice/commit/861dcfd))
* **social_support:** creation of configurations form ([9305292](https://gitlab.com/fi-sas/backoffice/commit/9305292))
* **social_support:** creation of experience form ([97744fd](https://gitlab.com/fi-sas/backoffice/commit/97744fd))
* **social_support:** creation of experience list ([6b0a383](https://gitlab.com/fi-sas/backoffice/commit/6b0a383))
* **social_support:** creation of module ([8e267f4](https://gitlab.com/fi-sas/backoffice/commit/8e267f4))
* **social_support:** creation of monthly report form ([b2f4a8a](https://gitlab.com/fi-sas/backoffice/commit/b2f4a8a))
* **social_support:** creation of payment grid form ([a431b50](https://gitlab.com/fi-sas/backoffice/commit/a431b50))
* **social_support:** creation of payment-grid list ([13706f1](https://gitlab.com/fi-sas/backoffice/commit/13706f1))
* **social_support:** creation of services and models ([fb69590](https://gitlab.com/fi-sas/backoffice/commit/fb69590))



<a name="0.17.0"></a>
# [0.17.0](https://gitlab.com/fi-sas/backoffice/compare/v0.16.5...v0.17.0) (2019-04-24)


### Bug Fixes

* **alimentation:** minor's fix of menu form and  list ([f474fa4](https://gitlab.com/fi-sas/backoffice/commit/f474fa4))
* **core:**  add padding to sider menu button on the header ([206c510](https://gitlab.com/fi-sas/backoffice/commit/206c510))
* **shared:**  add selectLabelKey when searchOnServer is true ([28b7c90](https://gitlab.com/fi-sas/backoffice/commit/28b7c90))
* **shared:** add picker month and year ([7256bc3](https://gitlab.com/fi-sas/backoffice/commit/7256bc3))
* **shared:** add select input searchTrigger ([56a304c](https://gitlab.com/fi-sas/backoffice/commit/56a304c))
* **shared:** fix error on delete item from list ([b106d0c](https://gitlab.com/fi-sas/backoffice/commit/b106d0c))
* **shared:** fix key of multilanguage field from form generator ([ce505d4](https://gitlab.com/fi-sas/backoffice/commit/ce505d4))
* **shared:** fix parser for input number ([2f58496](https://gitlab.com/fi-sas/backoffice/commit/2f58496))
* **shared:** fix repository ([eec4a60](https://gitlab.com/fi-sas/backoffice/commit/eec4a60))
* **shared:** form select input options is async now ([b683a35](https://gitlab.com/fi-sas/backoffice/commit/b683a35))
* **shared:** form show load indicator and disable form on request http ([c6d77a5](https://gitlab.com/fi-sas/backoffice/commit/c6d77a5))
* **shared:** form submit function is now a observable ([8e94860](https://gitlab.com/fi-sas/backoffice/commit/8e94860))
* **shared:** set new properties in input number ([7c0152b](https://gitlab.com/fi-sas/backoffice/commit/7c0152b))
* **shared:** upload-file name error ([45a6ed8](https://gitlab.com/fi-sas/backoffice/commit/45a6ed8))
* **social_support:** fix bug ([bdbf46b](https://gitlab.com/fi-sas/backoffice/commit/bdbf46b))


### Features

* **alimentation:** add family form new layout ([4ab3ab1](https://gitlab.com/fi-sas/backoffice/commit/4ab3ab1))
* **alimentation:** add form wharehouse new layout ([0065db7](https://gitlab.com/fi-sas/backoffice/commit/0065db7))
* **alimentation:** add new form to units ([6d1e17b](https://gitlab.com/fi-sas/backoffice/commit/6d1e17b))
* **alimentation:** add new layout allergens form ([23d1e58](https://gitlab.com/fi-sas/backoffice/commit/23d1e58))
* **alimentation:** add new layout to list wharehouses ([ea07001](https://gitlab.com/fi-sas/backoffice/commit/ea07001))
* **alimentation:** add new layout to services form ([256fd52](https://gitlab.com/fi-sas/backoffice/commit/256fd52))
* **alimentation:** add new layout to services list ([36bd3e6](https://gitlab.com/fi-sas/backoffice/commit/36bd3e6))
* **alimentation:** add new laytout to recipes list ([d179e86](https://gitlab.com/fi-sas/backoffice/commit/d179e86))
* **alimentation:** add new list to units ([0ff4edd](https://gitlab.com/fi-sas/backoffice/commit/0ff4edd))
* **alimentation:** add nutrients and allergens to form product ([094a305](https://gitlab.com/fi-sas/backoffice/commit/094a305))
* **alimentation:** add users modal to fiscal entities ([8e5d5d8](https://gitlab.com/fi-sas/backoffice/commit/8e5d5d8))
* **alimentation:** allergens list new layout ([0cae896](https://gitlab.com/fi-sas/backoffice/commit/0cae896))
* **alimentation:** complements form new layout ([6557e8d](https://gitlab.com/fi-sas/backoffice/commit/6557e8d))
* **alimentation:** creation of list complements on new layout ([6c02e6a](https://gitlab.com/fi-sas/backoffice/commit/6c02e6a))
* **alimentation:** creation wharehouse users access modal ([0815336](https://gitlab.com/fi-sas/backoffice/commit/0815336))
* **alimentation:** dish types list new layout ([12f57c5](https://gitlab.com/fi-sas/backoffice/commit/12f57c5))
* **alimentation:** dishtype form new layout ([138b472](https://gitlab.com/fi-sas/backoffice/commit/138b472))
* **alimentation:** form dish new layout ([c829b1e](https://gitlab.com/fi-sas/backoffice/commit/c829b1e))
* **alimentation:** form products new layout ([dfac1b3](https://gitlab.com/fi-sas/backoffice/commit/dfac1b3))
* **alimentation:** form products new layout ([5b22635](https://gitlab.com/fi-sas/backoffice/commit/5b22635))
* **alimentation:** list dish new layout ([5069f60](https://gitlab.com/fi-sas/backoffice/commit/5069f60))
* **alimentation:** list dish new layout ([8834015](https://gitlab.com/fi-sas/backoffice/commit/8834015))
* **alimentation:** list nutrients new layout ([2ddca30](https://gitlab.com/fi-sas/backoffice/commit/2ddca30))
* **alimentation:** new form entity ([b6154d2](https://gitlab.com/fi-sas/backoffice/commit/b6154d2))
* **alimentation:** new form nutrient layout ([96fd567](https://gitlab.com/fi-sas/backoffice/commit/96fd567))
* **alimentation:** new layout to families list ([916cb42](https://gitlab.com/fi-sas/backoffice/commit/916cb42))
* **alimentation:** new list entities layout ([f661bab](https://gitlab.com/fi-sas/backoffice/commit/f661bab))
* **alimentation:** new list schools layout ([518e97f](https://gitlab.com/fi-sas/backoffice/commit/518e97f))
* **alimentation:** schools form new layout ([d288323](https://gitlab.com/fi-sas/backoffice/commit/d288323))
* **gamification:**  creation of competitions list ([625cedc](https://gitlab.com/fi-sas/backoffice/commit/625cedc))
* **gamification:** creation of challenge form ([864bd53](https://gitlab.com/fi-sas/backoffice/commit/864bd53))
* **gamification:** creation of challenge list ([228132f](https://gitlab.com/fi-sas/backoffice/commit/228132f))
* **gamification:** creation of competition form ([0921051](https://gitlab.com/fi-sas/backoffice/commit/0921051))
* **gamification:** creation of players list ([0276f2a](https://gitlab.com/fi-sas/backoffice/commit/0276f2a))
* **gamification:** creation of rankings for challenges ([d61328e](https://gitlab.com/fi-sas/backoffice/commit/d61328e))
* **lib/core:** add patch to resources service ([3e57224](https://gitlab.com/fi-sas/backoffice/commit/3e57224))
* **shared:** add dynamic search to form generator ([a241790](https://gitlab.com/fi-sas/backoffice/commit/a241790))
* **shared:** add formPadding and formScroll options to form generator ([4e51e02](https://gitlab.com/fi-sas/backoffice/commit/4e51e02))
* **shared:** add hidden field feature to form generator ([8f0fce5](https://gitlab.com/fi-sas/backoffice/commit/8f0fce5))
* **shared:** add input type time to formGenerator ([6a61ca4](https://gitlab.com/fi-sas/backoffice/commit/6a61ca4))
* **shared:** add persistent url params property to repository ([a463e46](https://gitlab.com/fi-sas/backoffice/commit/a463e46))
* **shared:** add prefix and sufix to select component of form generator ([40319f2](https://gitlab.com/fi-sas/backoffice/commit/40319f2))
* **shared:** add translation field to list component ([6dc42d4](https://gitlab.com/fi-sas/backoffice/commit/6dc42d4))
* **shared:** condition params on form generator fields ([dc8f34c](https://gitlab.com/fi-sas/backoffice/commit/dc8f34c))
* **shared:** field-select add dynamic params ([9330521](https://gitlab.com/fi-sas/backoffice/commit/9330521))
* **shared:** input number formatter ([8bed3dc](https://gitlab.com/fi-sas/backoffice/commit/8bed3dc))
* **social_support:** bug fix ([bef8308](https://gitlab.com/fi-sas/backoffice/commit/bef8308))
* **social_support:** creation of activities list ([0f1fb1f](https://gitlab.com/fi-sas/backoffice/commit/0f1fb1f))
* **social_support:** creation of activity form ([f1b3b52](https://gitlab.com/fi-sas/backoffice/commit/f1b3b52))
* **social_support:** creation of applications list for bc ([bddfc4d](https://gitlab.com/fi-sas/backoffice/commit/bddfc4d))
* **social_support:** creation of attendance form ([861dcfd](https://gitlab.com/fi-sas/backoffice/commit/861dcfd))
* **social_support:** creation of configurations form ([9305292](https://gitlab.com/fi-sas/backoffice/commit/9305292))
* **social_support:** creation of experience form ([97744fd](https://gitlab.com/fi-sas/backoffice/commit/97744fd))
* **social_support:** creation of experience list ([6b0a383](https://gitlab.com/fi-sas/backoffice/commit/6b0a383))
* **social_support:** creation of module ([8e267f4](https://gitlab.com/fi-sas/backoffice/commit/8e267f4))
* **social_support:** creation of monthly report form ([b2f4a8a](https://gitlab.com/fi-sas/backoffice/commit/b2f4a8a))
* **social_support:** creation of payment grid form ([a431b50](https://gitlab.com/fi-sas/backoffice/commit/a431b50))
* **social_support:** creation of payment-grid list ([13706f1](https://gitlab.com/fi-sas/backoffice/commit/13706f1))
* **social_support:** creation of services and models ([fb69590](https://gitlab.com/fi-sas/backoffice/commit/fb69590))



<a name="0.16.5"></a>
## [0.16.5](https://gitlab.com/fi-sas/backoffice/compare/v0.16.4...v0.16.5) (2019-04-01)


### Features

* **list:** creation of teams list ([14d38cc](https://gitlab.com/fi-sas/backoffice/commit/14d38cc))



<a name="0.16.4"></a>
## [0.16.4](https://gitlab.com/fi-sas/backoffice/compare/v0.16.3...v0.16.4) (2019-04-01)


### Features

* **core:** add version of backoffice to footer ([d188728](https://gitlab.com/fi-sas/backoffice/commit/d188728))



<a name="0.16.3"></a>
## [0.16.3](https://gitlab.com/fi-sas/backoffice/compare/v0.16.2...v0.16.3) (2019-04-01)



<a name="0.16.2"></a>
## [0.16.2](https://gitlab.com/fi-sas/backoffice/compare/v0.16.1...v0.16.2) (2019-04-01)



<a name="0.16.1"></a>
## [0.16.1](https://gitlab.com/fi-sas/backoffice/compare/v0.16.0...v0.16.1) (2019-04-01)


### Bug Fixes

* **calendar:** change forms service to public ([ebfd743](https://gitlab.com/fi-sas/backoffice/commit/ebfd743))


### Features

* **gamification:** creation of challenge type list ([7805543](https://gitlab.com/fi-sas/backoffice/commit/7805543))



<a name="0.16.0"></a>
# [0.16.0](https://gitlab.com/fi-sas/backoffice/compare/v0.15.0...v0.16.0) (2019-04-01)


### Bug Fixes

* **authorization:** fix target service ([76f722e](https://gitlab.com/fi-sas/backoffice/commit/76f722e))
* **backoffice:** add auth to new layout ([c5bd39d](https://gitlab.com/fi-sas/backoffice/commit/c5bd39d))
* **backoffice:** minor fixs ([be7e85d](https://gitlab.com/fi-sas/backoffice/commit/be7e85d))
* **bugs:** fix minor bugs ([848840f](https://gitlab.com/fi-sas/backoffice/commit/848840f))
* **calendar:** add alerts types ([4bb0d21](https://gitlab.com/fi-sas/backoffice/commit/4bb0d21))
* **calendar:** add fullcalendar libs ([a88b0db](https://gitlab.com/fi-sas/backoffice/commit/a88b0db))
* **calendar:** fix calendar libs imports ([4c480b8](https://gitlab.com/fi-sas/backoffice/commit/4c480b8))
* **calendar:** fix event model ([dd233b4](https://gitlab.com/fi-sas/backoffice/commit/dd233b4))
* **calendar:** fix form events selects not updating ([0e0d7dc](https://gitlab.com/fi-sas/backoffice/commit/0e0d7dc))
* **calendar:** set event data in form on calendar ([c65e3ad](https://gitlab.com/fi-sas/backoffice/commit/c65e3ad))
* **configurations:** change the service to new method ([4fa1201](https://gitlab.com/fi-sas/backoffice/commit/4fa1201))
* **configurations:** fix resolver of tax ([9bd8edb](https://gitlab.com/fi-sas/backoffice/commit/9bd8edb))
* **list:** deep objects fields on array's ([4dbbcf1](https://gitlab.com/fi-sas/backoffice/commit/4dbbcf1))
* **shared:** add number input ([d72882e](https://gitlab.com/fi-sas/backoffice/commit/d72882e))
* **shared:** add preloaded ([1ee60d6](https://gitlab.com/fi-sas/backoffice/commit/1ee60d6))
* **shared:** add toolstrip by default on list's ([83c64a8](https://gitlab.com/fi-sas/backoffice/commit/83c64a8))
* **shared:** add translations to form-select ([f8345e6](https://gitlab.com/fi-sas/backoffice/commit/f8345e6))
* **shared:** fix error on daterange input update ([92a43a1](https://gitlab.com/fi-sas/backoffice/commit/92a43a1))
* **shared:** fix image gallery name ([50ff257](https://gitlab.com/fi-sas/backoffice/commit/50ff257))
* **shared:** fix import gallery upload component ([2a5f5fe](https://gitlab.com/fi-sas/backoffice/commit/2a5f5fe))
* **shared:** fix list don't update class on item selection ([a16c215](https://gitlab.com/fi-sas/backoffice/commit/a16c215))
* **shared:** fix list requesting two times on search ([e7af655](https://gitlab.com/fi-sas/backoffice/commit/e7af655))
* **shared:** fix list select not updating correctely ([96488b7](https://gitlab.com/fi-sas/backoffice/commit/96488b7))
* **shared:** fix query params on array ([6472bf1](https://gitlab.com/fi-sas/backoffice/commit/6472bf1))
* **shared:** form array error on empty fields ([78c3435](https://gitlab.com/fi-sas/backoffice/commit/78c3435))
* **shared:** form update corlors values ([50bf0dc](https://gitlab.com/fi-sas/backoffice/commit/50bf0dc))
* **shared:** forms updating daterange value ([e00aafc](https://gitlab.com/fi-sas/backoffice/commit/e00aafc))
* **shared:** list lock's on error ([fe449ba](https://gitlab.com/fi-sas/backoffice/commit/fe449ba))
* **shared:** list select search emit event on change ([1f76458](https://gitlab.com/fi-sas/backoffice/commit/1f76458))
* **shared:** order row on form inputs ([e8f2c7e](https://gitlab.com/fi-sas/backoffice/commit/e8f2c7e))
* **shared:** repository minor bugs ([367494b](https://gitlab.com/fi-sas/backoffice/commit/367494b))
* **shared:** upload files not reloading files ([e8d9008](https://gitlab.com/fi-sas/backoffice/commit/e8d9008))
* **shared:** upload image error on image id not found ([d339d67](https://gitlab.com/fi-sas/backoffice/commit/d339d67))
* **sports:** fix view recurrence ([57be8dc](https://gitlab.com/fi-sas/backoffice/commit/57be8dc))
* **users:** add users_groups to scopes form ([b21479a](https://gitlab.com/fi-sas/backoffice/commit/b21479a))
* **users:** fix user_groups and scopes from users form ([94e1c6a](https://gitlab.com/fi-sas/backoffice/commit/94e1c6a))


### Features

* **app:** add fullcalendar ([e6b18be](https://gitlab.com/fi-sas/backoffice/commit/e6b18be))
* **calendar:** create approve e reject form events ([77889a0](https://gitlab.com/fi-sas/backoffice/commit/77889a0))
* **calendar:** create calendar ([33a24b0](https://gitlab.com/fi-sas/backoffice/commit/33a24b0))
* **calendar:** creation of event form ([bbb19df](https://gitlab.com/fi-sas/backoffice/commit/bbb19df))
* **calendar:** creation of event list ([28018a1](https://gitlab.com/fi-sas/backoffice/commit/28018a1))
* **calendar:** creation of registration form ([6064d93](https://gitlab.com/fi-sas/backoffice/commit/6064d93))
* **calendar:** creation of registration form list ([2bbaf81](https://gitlab.com/fi-sas/backoffice/commit/2bbaf81))
* **calendar:** list reservations of event ([7d7a09e](https://gitlab.com/fi-sas/backoffice/commit/7d7a09e))
* **configurations:** create service and model for servcies ([46da2d9](https://gitlab.com/fi-sas/backoffice/commit/46da2d9))
* **core:** auto closable sider menu ([3b129c1](https://gitlab.com/fi-sas/backoffice/commit/3b129c1))
* **current-account:** add confirm cancel pending movement ([70d1c26](https://gitlab.com/fi-sas/backoffice/commit/70d1c26))
* **current-account:** add movements service ([e4c5d2b](https://gitlab.com/fi-sas/backoffice/commit/e4c5d2b))
* **current-account:** add pending movements list ([15764c2](https://gitlab.com/fi-sas/backoffice/commit/15764c2))
* **current-account:** creation of accounts list page ([b6088ad](https://gitlab.com/fi-sas/backoffice/commit/b6088ad))
* **current-account:** creation of accounts service ([8a1cfa0](https://gitlab.com/fi-sas/backoffice/commit/8a1cfa0))
* **current-account:** creation of current-account module ([d795fa3](https://gitlab.com/fi-sas/backoffice/commit/d795fa3))
* **current-account:** creation of page form account ([602a3c8](https://gitlab.com/fi-sas/backoffice/commit/602a3c8))
* **current-accounts:** add pay movement option ([1373e36](https://gitlab.com/fi-sas/backoffice/commit/1373e36))
* **current-accounts:** create page for balances and movements of users ([ca76c6d](https://gitlab.com/fi-sas/backoffice/commit/ca76c6d))
* **financial:** add charge accounts to current accounts page ([fb3da57](https://gitlab.com/fi-sas/backoffice/commit/fb3da57))
* **financial:** creation of list orders page ([87e30f1](https://gitlab.com/fi-sas/backoffice/commit/87e30f1))
* **financial:** creation of orders service ([b371b09](https://gitlab.com/fi-sas/backoffice/commit/b371b09))
* **gamification:** creation module ([5494716](https://gitlab.com/fi-sas/backoffice/commit/5494716))
* **gamification:** creation of challenge type form ([7d31a50](https://gitlab.com/fi-sas/backoffice/commit/7d31a50))
* **gamification:** creation of models and services ([41845aa](https://gitlab.com/fi-sas/backoffice/commit/41845aa))
* **notifications:** add form alert type page ([1080966](https://gitlab.com/fi-sas/backoffice/commit/1080966))
* **notifications:** creation of alert types list ([b9c0b44](https://gitlab.com/fi-sas/backoffice/commit/b9c0b44))
* **notifications:** creation of list alerts page ([1bd6920](https://gitlab.com/fi-sas/backoffice/commit/1bd6920))
* **notifications:** creation of list notifications methods page ([d036f94](https://gitlab.com/fi-sas/backoffice/commit/d036f94))
* **notifications:** creation of notifications module ([1db54d8](https://gitlab.com/fi-sas/backoffice/commit/1db54d8))
* **notifications:** cretion form notifications method ([00468b1](https://gitlab.com/fi-sas/backoffice/commit/00468b1))
* **payments:** creation of charge service ([9b37914](https://gitlab.com/fi-sas/backoffice/commit/9b37914))
* **payments:** creation of payments methods service ([345eb93](https://gitlab.com/fi-sas/backoffice/commit/345eb93))
* **payments:** creation of payments module ([90505ce](https://gitlab.com/fi-sas/backoffice/commit/90505ce))
* **payments:** creation of payments service ([01f0967](https://gitlab.com/fi-sas/backoffice/commit/01f0967))
* **shared:** add array input to form generator ([8a27d13](https://gitlab.com/fi-sas/backoffice/commit/8a27d13))
* **shared:** add color fields to list ([7e43bbc](https://gitlab.com/fi-sas/backoffice/commit/7e43bbc))
* **shared:** add datepicker search to list component ([6a3aeaa](https://gitlab.com/fi-sas/backoffice/commit/6a3aeaa))
* **shared:** add gallery input component ([be41a08](https://gitlab.com/fi-sas/backoffice/commit/be41a08))
* **shared:** add gallery upload to form generator ([4c8eddc](https://gitlab.com/fi-sas/backoffice/commit/4c8eddc))
* **shared:** add group and json input types to form generator ([9befbb6](https://gitlab.com/fi-sas/backoffice/commit/9befbb6))
* **shared:** add radio group custom search to list component ([8d52b33](https://gitlab.com/fi-sas/backoffice/commit/8d52b33))
* **shared:** add tip option to form field input ([bcb65e4](https://gitlab.com/fi-sas/backoffice/commit/bcb65e4))
* **shared:** form can load data from another field of the model ([7fb8790](https://gitlab.com/fi-sas/backoffice/commit/7fb8790))
* **shared:** form generator add inputs errors of email and pattern ([215f4a0](https://gitlab.com/fi-sas/backoffice/commit/215f4a0))
* **shared:** list return values of deep objects ([af550da](https://gitlab.com/fi-sas/backoffice/commit/af550da))
* **shared:** list select searchOnServer ([d005e18](https://gitlab.com/fi-sas/backoffice/commit/d005e18))
* **shared:** refresh layout of upload file component ([f01c929](https://gitlab.com/fi-sas/backoffice/commit/f01c929))
* **users:** add balances and movements to user list ([7de7b45](https://gitlab.com/fi-sas/backoffice/commit/7de7b45))
* **users:** add new layout to scopes ([b054ac8](https://gitlab.com/fi-sas/backoffice/commit/b054ac8))
* **users:** add new layout to targets ([26c6dda](https://gitlab.com/fi-sas/backoffice/commit/26c6dda))



<a name="0.15.0"></a>
# [0.15.0](https://gitlab.com/fi-sas/backoffice/compare/v0.14.5...v0.15.0) (2019-03-11)


### Bug Fixes

* **accommodation:** add form residence to list of residences ([d53d9d6](https://gitlab.com/fi-sas/backoffice/commit/d53d9d6))
* **accommodation:** change padding on occupation map ([f423776](https://gitlab.com/fi-sas/backoffice/commit/f423776))
* **accommodation:** fix imports of form ([a251f7f](https://gitlab.com/fi-sas/backoffice/commit/a251f7f))
* **app:** fix app routing ([18b1348](https://gitlab.com/fi-sas/backoffice/commit/18b1348))
* **authorization:** change profile pages ([568d36c](https://gitlab.com/fi-sas/backoffice/commit/568d36c))
* **configurations:** update device scope error ([587c2f7](https://gitlab.com/fi-sas/backoffice/commit/587c2f7))
* **infrastructure:** add top toolstrip on lists ([cd282ec](https://gitlab.com/fi-sas/backoffice/commit/cd282ec))
* **private-accommodation:** add scroll to tipologie form ([80fefa8](https://gitlab.com/fi-sas/backoffice/commit/80fefa8))
* **private-accommodation:** change users request ([96ff74e](https://gitlab.com/fi-sas/backoffice/commit/96ff74e))
* **shared:** add # to color fields on model update ([b7edc18](https://gitlab.com/fi-sas/backoffice/commit/b7edc18))
* **shared:** add allow clear on select custom search on list ([4c4224e](https://gitlab.com/fi-sas/backoffice/commit/4c4224e))
* **shared:** add disable condition on hasPermission directive ([79676c3](https://gitlab.com/fi-sas/backoffice/commit/79676c3))
* **shared:** add moment locale ([f9095ce](https://gitlab.com/fi-sas/backoffice/commit/f9095ce))
* **shared:** add validation on detechChanges on list component ([47510bc](https://gitlab.com/fi-sas/backoffice/commit/47510bc))
* **shared:** fix color picked ([c0e189e](https://gitlab.com/fi-sas/backoffice/commit/c0e189e))
* **shared:** fix error on documents upload ([513da57](https://gitlab.com/fi-sas/backoffice/commit/513da57))
* **shared:** fix shared on null value ([57fc29c](https://gitlab.com/fi-sas/backoffice/commit/57fc29c))
* **shared:** fix upload file error on dumb files object ([71ee68e](https://gitlab.com/fi-sas/backoffice/commit/71ee68e))
* **shared:** placeholder is optional ([08220c8](https://gitlab.com/fi-sas/backoffice/commit/08220c8))
* **shared:** rearrange MultiLanguageFields structure ([79244a6](https://gitlab.com/fi-sas/backoffice/commit/79244a6))
* **shared:** tag component fix error on null result ([6553898](https://gitlab.com/fi-sas/backoffice/commit/6553898))
* **sport:** call services for selects boxes ([be63474](https://gitlab.com/fi-sas/backoffice/commit/be63474))
* **sport:** fix date range picker of activity form ([f66cba9](https://gitlab.com/fi-sas/backoffice/commit/f66cba9))
* **sport:** fix padding in venue form ([aacd22f](https://gitlab.com/fi-sas/backoffice/commit/aacd22f))
* **u-bike:** fix rounting ([bf7a859](https://gitlab.com/fi-sas/backoffice/commit/bf7a859))
* **users:** change users global variables to public ([b66a10d](https://gitlab.com/fi-sas/backoffice/commit/b66a10d))
* **users:** change users repository errors ([28dc87a](https://gitlab.com/fi-sas/backoffice/commit/28dc87a))
* **users:** change users service to repository ([a78d6fd](https://gitlab.com/fi-sas/backoffice/commit/a78d6fd))
* **users:** fix users-groups scopes service ([97356bc](https://gitlab.com/fi-sas/backoffice/commit/97356bc))
* **users:** users-groups change service to repository ([4600a35](https://gitlab.com/fi-sas/backoffice/commit/4600a35))


### Features

* **accommodation:** add accupation-map page ([46817d6](https://gitlab.com/fi-sas/backoffice/commit/46817d6))
* **accommodation:** add applications form page ([5abe665](https://gitlab.com/fi-sas/backoffice/commit/5abe665))
* **accommodation:** add applications service ([8e5c942](https://gitlab.com/fi-sas/backoffice/commit/8e5c942))
* **accommodation:** add form typology page ([0179f0a](https://gitlab.com/fi-sas/backoffice/commit/0179f0a))
* **accommodation:** add list applications ([cb0444f](https://gitlab.com/fi-sas/backoffice/commit/cb0444f))
* **accommodation:** add list rooms page ([370d774](https://gitlab.com/fi-sas/backoffice/commit/370d774))
* **accommodation:** add list typologies page ([c7caa97](https://gitlab.com/fi-sas/backoffice/commit/c7caa97))
* **accommodation:** add pricelines service ([d427135](https://gitlab.com/fi-sas/backoffice/commit/d427135))
* **accommodation:** add residence form ([4596fdb](https://gitlab.com/fi-sas/backoffice/commit/4596fdb))
* **accommodation:** add residences service ([c1dac7e](https://gitlab.com/fi-sas/backoffice/commit/c1dac7e))
* **accommodation:** add rooms service ([4750537](https://gitlab.com/fi-sas/backoffice/commit/4750537))
* **accommodation:** add service form page ([3103c63](https://gitlab.com/fi-sas/backoffice/commit/3103c63))
* **accommodation:** creation of accommodation module ([315fa10](https://gitlab.com/fi-sas/backoffice/commit/315fa10))
* **accommodation:** creation of list residences ([286e0a8](https://gitlab.com/fi-sas/backoffice/commit/286e0a8))
* **accommodation:** creation of list services page ([ac7ab0a](https://gitlab.com/fi-sas/backoffice/commit/ac7ab0a))
* **accommodation:** creation of room form ([38f798c](https://gitlab.com/fi-sas/backoffice/commit/38f798c))
* **accommodation:** creation of typologies service ([05d6932](https://gitlab.com/fi-sas/backoffice/commit/05d6932))
* **calendar:** creation of configuration form ([60166dc](https://gitlab.com/fi-sas/backoffice/commit/60166dc))
* **calendar:** creation of event categories list ([ccc692a](https://gitlab.com/fi-sas/backoffice/commit/ccc692a))
* **calendar:** creation of event category form ([38cd244](https://gitlab.com/fi-sas/backoffice/commit/38cd244))
* **calendar:** creation of module and configs ([dfaca08](https://gitlab.com/fi-sas/backoffice/commit/dfaca08))
* **calendar:** creation of services and models ([e262d31](https://gitlab.com/fi-sas/backoffice/commit/e262d31))
* **core:** add 3rd level feature on side menu ([1a220b7](https://gitlab.com/fi-sas/backoffice/commit/1a220b7))
* **shared:** add color picker to form generator ([76cf824](https://gitlab.com/fi-sas/backoffice/commit/76cf824))
* **shared:** add date range input to form generator ([d4e217c](https://gitlab.com/fi-sas/backoffice/commit/d4e217c))
* **shared:** add disabled input to toolstrip buttons ([f03628f](https://gitlab.com/fi-sas/backoffice/commit/f03628f))
* **shared:** add form component ([c3696e4](https://gitlab.com/fi-sas/backoffice/commit/c3696e4))
* **shared:** add masking feat ([e30c44e](https://gitlab.com/fi-sas/backoffice/commit/e30c44e))
* **shared:** add multiSelect option to select input on form generator ([2075f2b](https://gitlab.com/fi-sas/backoffice/commit/2075f2b))
* **shared:** add persistent filters to list component ([a04714c](https://gitlab.com/fi-sas/backoffice/commit/a04714c))
* **shared:** add sserverSearch to input-select ([e3edb42](https://gitlab.com/fi-sas/backoffice/commit/e3edb42))
* **shared:** add static var with yes / no results to tag component ([ad7b179](https://gitlab.com/fi-sas/backoffice/commit/ad7b179))
* **shared:** add upload to form generator ([06a994f](https://gitlab.com/fi-sas/backoffice/commit/06a994f))
* **shared:** creation of form multi language field ([dbd748c](https://gitlab.com/fi-sas/backoffice/commit/dbd748c))
* **shared:** creation of form-input component ([901e396](https://gitlab.com/fi-sas/backoffice/commit/901e396))
* **shared:** creation of pipe to translation ([d0dffb5](https://gitlab.com/fi-sas/backoffice/commit/d0dffb5))
* **sport:** creation module ([d03603b](https://gitlab.com/fi-sas/backoffice/commit/d03603b))
* **sport:** creation of  modality form ([655289e](https://gitlab.com/fi-sas/backoffice/commit/655289e))
* **sport:** creation of  reservations list ([53bddaf](https://gitlab.com/fi-sas/backoffice/commit/53bddaf))
* **sport:** creation of activity form ([607b197](https://gitlab.com/fi-sas/backoffice/commit/607b197))
* **sport:** creation of activity list ([f749c3d](https://gitlab.com/fi-sas/backoffice/commit/f749c3d))
* **sport:** creation of configurations form ([d15baa7](https://gitlab.com/fi-sas/backoffice/commit/d15baa7))
* **sport:** creation of modalities list ([d96ef6a](https://gitlab.com/fi-sas/backoffice/commit/d96ef6a))
* **sport:** creation of services and models ([35e9536](https://gitlab.com/fi-sas/backoffice/commit/35e9536))
* **sport:** creation of venue form ([5d8ff86](https://gitlab.com/fi-sas/backoffice/commit/5d8ff86))
* **sport:** creation of venues list ([4f6e22c](https://gitlab.com/fi-sas/backoffice/commit/4f6e22c))
* **u-bike:** creation of project stats ([6a725de](https://gitlab.com/fi-sas/backoffice/commit/6a725de))
* **users:** add new list layout to users ([b2bf483](https://gitlab.com/fi-sas/backoffice/commit/b2bf483))



<a name="0.14.5"></a>
## [0.14.5](https://gitlab.com/fi-sas/backoffice/compare/v0.14.4...v0.14.5) (2019-02-22)



<a name="0.14.4"></a>
## [0.14.4](https://gitlab.com/fi-sas/backoffice/compare/v0.14.3...v0.14.4) (2019-02-22)


### Bug Fixes

* **u-bike:** fix u-bike routing lazy load ([bb91375](https://gitlab.com/fi-sas/backoffice/commit/bb91375))



<a name="0.14.3"></a>
## [0.14.3](https://gitlab.com/fi-sas/backoffice/compare/v0.14.2...v0.14.3) (2019-02-22)


### Bug Fixes

* **u-bike:** change to public repository on absence list ([5dc0f7f](https://gitlab.com/fi-sas/backoffice/commit/5dc0f7f))



<a name="0.14.2"></a>
## [0.14.2](https://gitlab.com/fi-sas/backoffice/compare/v0.14.1...v0.14.2) (2019-02-22)


### Bug Fixes

* **u-bike:** change service to public on list's ([bafc9a6](https://gitlab.com/fi-sas/backoffice/commit/bafc9a6))



<a name="0.14.1"></a>
## [0.14.1](https://gitlab.com/fi-sas/backoffice/compare/v0.14.0...v0.14.1) (2019-02-22)


### Bug Fixes

* **u-bike:** remove nonexistent component import ([4bcdc94](https://gitlab.com/fi-sas/backoffice/commit/4bcdc94))



<a name="0.14.0"></a>
# [0.14.0](https://gitlab.com/fi-sas/backoffice/compare/v0.13.14...v0.14.0) (2019-02-22)


### Bug Fixes

* **app.config:** add u-bike endpoints ([01a096c](https://gitlab.com/fi-sas/backoffice/commit/01a096c))
* **dashboard:** change icon u-bike ([0c7759a](https://gitlab.com/fi-sas/backoffice/commit/0c7759a))
* **private-accommodation:** listing remove character ([5e34ba5](https://gitlab.com/fi-sas/backoffice/commit/5e34ba5))
* **u-bike:** add new rountes ([f947bd7](https://gitlab.com/fi-sas/backoffice/commit/f947bd7))
* **u-bike:** add new rounting ([639b555](https://gitlab.com/fi-sas/backoffice/commit/639b555))
* **u-bike:** add new routes ([bf4bdd4](https://gitlab.com/fi-sas/backoffice/commit/bf4bdd4))
* **u-bike:** change css to less ([2c0d7d0](https://gitlab.com/fi-sas/backoffice/commit/2c0d7d0))
* **u-bike:** change css to less ([32c8481](https://gitlab.com/fi-sas/backoffice/commit/32c8481))
* **u-bike:** delete resolver ([7f8de12](https://gitlab.com/fi-sas/backoffice/commit/7f8de12))
* **u-bike:** fix absence service ([55b9563](https://gitlab.com/fi-sas/backoffice/commit/55b9563))
* **u-bike:** fix application ([ce335ed](https://gitlab.com/fi-sas/backoffice/commit/ce335ed))
* **u-bike:** fix application ([93ac712](https://gitlab.com/fi-sas/backoffice/commit/93ac712))
* **u-bike:** fix list typologies ([16f99af](https://gitlab.com/fi-sas/backoffice/commit/16f99af))
* **u-bike:** fix model stats-bikes ([46b7cc1](https://gitlab.com/fi-sas/backoffice/commit/46b7cc1))
* **u-bike:** fix routing ([09fdc6b](https://gitlab.com/fi-sas/backoffice/commit/09fdc6b))
* **u-bike:** fix services ([8a41af5](https://gitlab.com/fi-sas/backoffice/commit/8a41af5))
* **u-bike:** fix typologies ([4ed56ea](https://gitlab.com/fi-sas/backoffice/commit/4ed56ea))
* **u-bike:** form typology ([a553e21](https://gitlab.com/fi-sas/backoffice/commit/a553e21))
* **u-bike:** view-application change format ([3a48904](https://gitlab.com/fi-sas/backoffice/commit/3a48904))


### Features

* **list-breakdowns:** creation of breakdowns list ([82d0338](https://gitlab.com/fi-sas/backoffice/commit/82d0338))
* **u-bike:** creation models ([c8507c3](https://gitlab.com/fi-sas/backoffice/commit/c8507c3))
* **u-bike:** creation of absence list ([ed454ac](https://gitlab.com/fi-sas/backoffice/commit/ed454ac))
* **u-bike:** creation of application form ([72d3fcf](https://gitlab.com/fi-sas/backoffice/commit/72d3fcf))
* **u-bike:** creation of challenge list ([f1210fc](https://gitlab.com/fi-sas/backoffice/commit/f1210fc))
* **u-bike:** creation of configuration form ([9d80629](https://gitlab.com/fi-sas/backoffice/commit/9d80629))
* **u-bike:** creation of maintenances list ([0e6c2b5](https://gitlab.com/fi-sas/backoffice/commit/0e6c2b5))
* **u-bike:** creation of theft list ([0cda1fc](https://gitlab.com/fi-sas/backoffice/commit/0cda1fc))
* **u-bike:** creation of trips list ([9fa5172](https://gitlab.com/fi-sas/backoffice/commit/9fa5172))
* **u-bike:** creation of withdrawal list ([2c77cfa](https://gitlab.com/fi-sas/backoffice/commit/2c77cfa))
* **u-bike:** creation services ([fdad94a](https://gitlab.com/fi-sas/backoffice/commit/fdad94a))
* **u-bike:** creation view for application ([60e9d4a](https://gitlab.com/fi-sas/backoffice/commit/60e9d4a))
* **u-bike:** init module ([24de48f](https://gitlab.com/fi-sas/backoffice/commit/24de48f))



<a name="0.13.14"></a>
## [0.13.14](https://gitlab.com/fi-sas/backoffice/compare/v0.13.13...v0.13.14) (2019-02-12)


### Bug Fixes

* **package:** fix error on angular 7 build --prod ([4786c96](https://gitlab.com/fi-sas/backoffice/commit/4786c96))



<a name="0.11.11"></a>
## [0.11.11](https://gitlab.com/fi-sas/frontend/compare/v0.11.10...v0.11.11) (2018-12-14)


### Bug Fixes

* **mobility:** more timeout ([efdda67](https://gitlab.com/fi-sas/frontend/commit/efdda67))



<a name="0.11.10"></a>
## [0.11.10](https://gitlab.com/fi-sas/frontend/compare/v0.10.10...v0.11.10) (2018-12-14)


### Bug Fixes

* **mobility:** change fields names ([dfc39ce](https://gitlab.com/fi-sas/frontend/commit/dfc39ce))



<a name="0.10.10"></a>
## [0.10.10](https://gitlab.com/fi-sas/frontend/compare/v0.10.9...v0.10.10) (2018-12-14)


### Bug Fixes

* **mobility:** remove replce from template ([7116702](https://gitlab.com/fi-sas/frontend/commit/7116702))



<a name="0.10.9"></a>
## [0.10.9](https://gitlab.com/fi-sas/frontend/compare/v0.9.9...v0.10.9) (2018-12-14)


### Bug Fixes

* **interceptor:** wrong return value ([eb6d842](https://gitlab.com/fi-sas/frontend/commit/eb6d842))
* **meals:** dish type is no more necessary ([fd8a625](https://gitlab.com/fi-sas/frontend/commit/fd8a625))
* **mobility:** changed model ([fb1d1cd](https://gitlab.com/fi-sas/frontend/commit/fb1d1cd))
* **mobility:** fix some bugs after testing ([8c8accf](https://gitlab.com/fi-sas/frontend/commit/8c8accf))
* **mobility:** wrong field name ([4d97349](https://gitlab.com/fi-sas/frontend/commit/4d97349))
* **pipe:** on init missing ([0dc0301](https://gitlab.com/fi-sas/frontend/commit/0dc0301))
* **pipeline:** fix pipeline fail ([e4d978e](https://gitlab.com/fi-sas/frontend/commit/e4d978e))


### Features

* **cart-list:** cart list total and translations ([968b006](https://gitlab.com/fi-sas/frontend/commit/968b006))
* **cart-list:** translations for cart scene ([5475070](https://gitlab.com/fi-sas/frontend/commit/5475070))
* **mobility:** add ticket to cart has a product ([df70abd](https://gitlab.com/fi-sas/frontend/commit/df70abd))
* **mobility:** mobility detail scene with steps ([f5bbb45](https://gitlab.com/fi-sas/frontend/commit/f5bbb45))
* **mobility:** navigate to route detail ([35dc835](https://gitlab.com/fi-sas/frontend/commit/35dc835))
* **mobility:** route detail summary and information ([4358d03](https://gitlab.com/fi-sas/frontend/commit/4358d03))
* **route-detail:** add title to route detail ([a18494f](https://gitlab.com/fi-sas/frontend/commit/a18494f))



<a name="0.9.9"></a>
## [0.9.9](https://gitlab.com/fi-sas/frontend/compare/v0.8.9...v0.9.9) (2018-12-10)


### Bug Fixes

* **alimentation:** fix product update errors ([669c497](https://gitlab.com/fi-sas/frontend/commit/669c497))
* **alimentation:** remove list and create menus ([e950a78](https://gitlab.com/fi-sas/frontend/commit/e950a78))
* **css:** css uniformisation ([11c2b35](https://gitlab.com/fi-sas/frontend/commit/11c2b35))
* **css:** remove theming files from authentication ([8b9ce6c](https://gitlab.com/fi-sas/frontend/commit/8b9ce6c))
* **css:** remove theming files from dashboard and pipe problems ([4008b10](https://gitlab.com/fi-sas/frontend/commit/4008b10))
* **css:** remove theming files from header ([af68be5](https://gitlab.com/fi-sas/frontend/commit/af68be5))
* **css:** remove theming files from keyboard ([63d1f09](https://gitlab.com/fi-sas/frontend/commit/63d1f09))
* **css:** remove theming files from main menu ([8d5e6e2](https://gitlab.com/fi-sas/frontend/commit/8d5e6e2))
* **css:** remove theming files from ticker ([d20afce](https://gitlab.com/fi-sas/frontend/commit/d20afce))
* **css:** remove theming files from unavailable ([df8ee2a](https://gitlab.com/fi-sas/frontend/commit/df8ee2a))
* **css:** removed theming files from footer ([79c8e66](https://gitlab.com/fi-sas/frontend/commit/79c8e66))
* **fifteen-day:** type declaration ([33eaef7](https://gitlab.com/fi-sas/frontend/commit/33eaef7))
* **progress-bar:** progress bar position ([6385270](https://gitlab.com/fi-sas/frontend/commit/6385270))
* **service:** pipe error ([2a0319a](https://gitlab.com/fi-sas/frontend/commit/2a0319a))
* **services:** no default school ([87543e3](https://gitlab.com/fi-sas/frontend/commit/87543e3))
* **shared:** fix token interceptor overlap headers ([f809116](https://gitlab.com/fi-sas/frontend/commit/f809116))


### Features

* **alimentation:** creation of components and routing ([8f4f5b0](https://gitlab.com/fi-sas/frontend/commit/8f4f5b0))
* **alimentation:** creation of menus list and form ([944504c](https://gitlab.com/fi-sas/frontend/commit/944504c))
* **mobility:** add ticket to cart ([c2a2bd7](https://gitlab.com/fi-sas/frontend/commit/c2a2bd7))
* **mobility:** list routes screen new design ([4e06c5f](https://gitlab.com/fi-sas/frontend/commit/4e06c5f))
* **mobility:** places design ([e9559db](https://gitlab.com/fi-sas/frontend/commit/e9559db))
* **mobility:** search place freely ([edaf1c3](https://gitlab.com/fi-sas/frontend/commit/edaf1c3))
* **timer:** refresh videos and media after 30 min ([bc42eb5](https://gitlab.com/fi-sas/frontend/commit/bc42eb5))



<a name="0.8.9"></a>
## [0.8.9](https://gitlab.com/fi-sas/frontend/compare/v0.8.8...v0.8.9) (2018-11-30)


### Bug Fixes

* **timer:** enable timer for screen saver ([28c8758](https://gitlab.com/fi-sas/frontend/commit/28c8758))



<a name="0.8.8"></a>
## [0.8.8](https://gitlab.com/fi-sas/frontend/compare/v0.8.7...v0.8.8) (2018-11-30)


### Bug Fixes

* **conflicts:** resolve conflicts ([35c6b56](https://gitlab.com/fi-sas/frontend/commit/35c6b56))
* **install:** add missing services address ([e750bac](https://gitlab.com/fi-sas/frontend/commit/e750bac))



<a name="0.8.6"></a>
## [0.8.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.6) (2018-11-30)


### Bug Fixes

* **meals:** added fixed date to have data ([c34bef7](https://gitlab.com/fi-sas/frontend/commit/c34bef7))



<a name="0.8.7"></a>
## [0.8.7](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.7) (2018-11-30)

### Bug Fixes

* **environment:** missing entry for mobility configuration ([079d51f](https://gitlab.com/fi-sas/frontend/commit/079d51f))

<a name="0.8.6"></a>
## [0.8.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.6) (2018-11-30)


### Bug Fixes

* **car:** product interface ([b0514c2](https://gitlab.com/fi-sas/frontend/commit/b0514c2))
* **communication:** fix ration of 9:16 image ([158f49e](https://gitlab.com/fi-sas/frontend/commit/158f49e))
* **meals:** added fixed date to have data ([c34bef7](https://gitlab.com/fi-sas/frontend/commit/c34bef7))
* **meals:** fix declaration ([ca6236d](https://gitlab.com/fi-sas/frontend/commit/ca6236d))
* **meals:** removed fixed date ([d39394e](https://gitlab.com/fi-sas/frontend/commit/d39394e))
* **merge:** config var ([1fd08e5](https://gitlab.com/fi-sas/frontend/commit/1fd08e5))
* **merge:** resolve conflicts ([8ded2c4](https://gitlab.com/fi-sas/frontend/commit/8ded2c4))
* **merge:** resolve conflicts ([527995f](https://gitlab.com/fi-sas/frontend/commit/527995f))
* **title:** title input ([087fdc1](https://gitlab.com/fi-sas/frontend/commit/087fdc1))


### Features

* **alimentation:** creation of complement form and list page ([6deab50](https://gitlab.com/fi-sas/frontend/commit/6deab50))
* **alimentation:** form products updated ([46376cb](https://gitlab.com/fi-sas/frontend/commit/46376cb))
* **alimentation:** partial creation of products form ([80186cc](https://gitlab.com/fi-sas/frontend/commit/80186cc))
* **cart:** add products to the top of the stack ([24bc66c](https://gitlab.com/fi-sas/frontend/commit/24bc66c))
* **cart:** add to cart in meal detail ([ae088d1](https://gitlab.com/fi-sas/frontend/commit/ae088d1))
* **cart:** add to cart in meals product ([0023e76](https://gitlab.com/fi-sas/frontend/commit/0023e76))
* **interceptor:** implemented http interceptor ([49e12d2](https://gitlab.com/fi-sas/frontend/commit/49e12d2))
* **loading:** loader in navigation ([51ebcbc](https://gitlab.com/fi-sas/frontend/commit/51ebcbc))
* **meal:** detail meal ([41a2fa6](https://gitlab.com/fi-sas/frontend/commit/41a2fa6))
* **meal:** meal detail ([436c00f](https://gitlab.com/fi-sas/frontend/commit/436c00f))
* **meals:** change categories language, set default school and service ([4f1b368](https://gitlab.com/fi-sas/frontend/commit/4f1b368))
* **meals:** connect with families service ([578942d](https://gitlab.com/fi-sas/frontend/commit/578942d))
* **meals:** list available meals ([18c10c6](https://gitlab.com/fi-sas/frontend/commit/18c10c6))
* **meals:** set meals component variables for service call ([93c4a14](https://gitlab.com/fi-sas/frontend/commit/93c4a14))
* **meals:** transform meals data ([c2fc275](https://gitlab.com/fi-sas/frontend/commit/c2fc275))
* **resolver:** food detail service and resolver ([6c87c78](https://gitlab.com/fi-sas/frontend/commit/6c87c78))
* **route:** scene detail init ([2fab06a](https://gitlab.com/fi-sas/frontend/commit/2fab06a))
* **scene:** route page for mobility ([2c312e8](https://gitlab.com/fi-sas/frontend/commit/2c312e8))
* **services:** save selected school and service in alimentation service ([d6ced1f](https://gitlab.com/fi-sas/frontend/commit/d6ced1f))



<a name="0.7.6"></a>
## [0.7.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.5...v0.7.6) (2018-11-26)


### Bug Fixes

* **environment:** missing options on production ([d3666b8](https://gitlab.com/fi-sas/frontend/commit/d3666b8))



<a name="0.7.5"></a>
## [0.7.5](https://gitlab.com/fi-sas/frontend/compare/v0.6.5...v0.7.5) (2018-11-23)


### Bug Fixes

* **alimenation:** add form family products tab ([772a3ad](https://gitlab.com/fi-sas/frontend/commit/772a3ad))
* **alimentation:** fix wharhouse service list operations ([dc96a66](https://gitlab.com/fi-sas/frontend/commit/dc96a66))
* **backoffice:** change page index of the resolvers ([e85ebf3](https://gitlab.com/fi-sas/frontend/commit/e85ebf3))
* **build:** fix problems for build ([8231d21](https://gitlab.com/fi-sas/frontend/commit/8231d21))
* **cart:** review problems ([2b539b6](https://gitlab.com/fi-sas/frontend/commit/2b539b6))
* **cart-summary:** link to cart list ([00df95a](https://gitlab.com/fi-sas/frontend/commit/00df95a))
* **communication:** add post state on every step of posts history ([0aa88b0](https://gitlab.com/fi-sas/frontend/commit/0aa88b0))
* **communication:** change text on Login form as posts form ([53a20fb](https://gitlab.com/fi-sas/frontend/commit/53a20fb))
* **communication:** minor bugs ([02c091f](https://gitlab.com/fi-sas/frontend/commit/02c091f))
* **communication:** remove active from posts lists ([fdccb57](https://gitlab.com/fi-sas/frontend/commit/fdccb57))
* **config:** remove unused configurations ([18238bd](https://gitlab.com/fi-sas/frontend/commit/18238bd))
* **dashboard:** enable mobility module ([5a47e4a](https://gitlab.com/fi-sas/frontend/commit/5a47e4a))
* **dashboard:** fix module bus routing ([06d5398](https://gitlab.com/fi-sas/frontend/commit/06d5398))
* **days:** use const instead of let ([37b9cd7](https://gitlab.com/fi-sas/frontend/commit/37b9cd7))
* **history:** prevent infinity back hsitory ([c6e24d7](https://gitlab.com/fi-sas/frontend/commit/c6e24d7))
* **icons:** svg icons were not showing ([bae8144](https://gitlab.com/fi-sas/frontend/commit/bae8144))
* **media:** validate missing properties ([40e0fef](https://gitlab.com/fi-sas/frontend/commit/40e0fef))
* **merge:** conflicts ([ae12f99](https://gitlab.com/fi-sas/frontend/commit/ae12f99))
* **merge:** fix merge conflicts ([fd1ca7f](https://gitlab.com/fi-sas/frontend/commit/fd1ca7f))
* **merge:** resolve conflicts ([ff468d6](https://gitlab.com/fi-sas/frontend/commit/ff468d6))
* **merge:** resolving conflicts ([d93c9cc](https://gitlab.com/fi-sas/frontend/commit/d93c9cc))
* **navigation:** add navigation flow ([ddf9a6b](https://gitlab.com/fi-sas/frontend/commit/ddf9a6b))
* **navigation:** better navigation flow ([098fef3](https://gitlab.com/fi-sas/frontend/commit/098fef3))
* **pipe:** broken pipe ([eda2e31](https://gitlab.com/fi-sas/frontend/commit/eda2e31))
* **service:** no data problems ([a80be3e](https://gitlab.com/fi-sas/frontend/commit/a80be3e))
* **service:** no data problems ([f0b89f3](https://gitlab.com/fi-sas/frontend/commit/f0b89f3))
* **services:** import missing ([fd4d886](https://gitlab.com/fi-sas/frontend/commit/fd4d886))
* **shared:** fix translator no message ([7a998d8](https://gitlab.com/fi-sas/frontend/commit/7a998d8))
* **users:** fix users requireds feilds ([e1ff883](https://gitlab.com/fi-sas/frontend/commit/e1ff883))
* **weather:** weather service config default value ([97fe16e](https://gitlab.com/fi-sas/frontend/commit/97fe16e))


### Features

* **alimenation:** creation of entity form ([756a820](https://gitlab.com/fi-sas/frontend/commit/756a820))
* **alimentation:** add allergens to routing ([c959279](https://gitlab.com/fi-sas/frontend/commit/c959279))
* **alimentation:** add units to routing ([400cf0d](https://gitlab.com/fi-sas/frontend/commit/400cf0d))
* **alimentation:** add users to the entities form ([bd336bc](https://gitlab.com/fi-sas/frontend/commit/bd336bc))
* **alimentation:** add wharehouses form and list components ([933a22a](https://gitlab.com/fi-sas/frontend/commit/933a22a))
* **alimentation:** adding schools to routing ([f569961](https://gitlab.com/fi-sas/frontend/commit/f569961))
* **alimentation:** cration of form and list families ([9afdd4f](https://gitlab.com/fi-sas/frontend/commit/9afdd4f))
* **alimentation:** creation of alimentation service ([937ebba](https://gitlab.com/fi-sas/frontend/commit/937ebba))
* **alimentation:** creation of allergen form ([20f258a](https://gitlab.com/fi-sas/frontend/commit/20f258a))
* **alimentation:** creation of allergens components ([e72b42d](https://gitlab.com/fi-sas/frontend/commit/e72b42d))
* **alimentation:** creation of allergens service ([8b2c5ab](https://gitlab.com/fi-sas/frontend/commit/8b2c5ab))
* **alimentation:** creation of entity guard ([27204ad](https://gitlab.com/fi-sas/frontend/commit/27204ad))
* **alimentation:** creation of fiscal entity service ([140b41f](https://gitlab.com/fi-sas/frontend/commit/140b41f))
* **alimentation:** creation of list allergens ([967fffc](https://gitlab.com/fi-sas/frontend/commit/967fffc))
* **alimentation:** creation of list and form nutrients component ([f5ebbb7](https://gitlab.com/fi-sas/frontend/commit/f5ebbb7))
* **alimentation:** creation of nutrients components and service ([b40b998](https://gitlab.com/fi-sas/frontend/commit/b40b998))
* **alimentation:** creation of school form ([4342ed2](https://gitlab.com/fi-sas/frontend/commit/4342ed2))
* **alimentation:** creation of schools components ([d7864ce](https://gitlab.com/fi-sas/frontend/commit/d7864ce))
* **alimentation:** creation of schools list ([50fc438](https://gitlab.com/fi-sas/frontend/commit/50fc438))
* **alimentation:** creation of schools service ([055a454](https://gitlab.com/fi-sas/frontend/commit/055a454))
* **alimentation:** creation of services form and list component ([85a4b13](https://gitlab.com/fi-sas/frontend/commit/85a4b13))
* **alimentation:** creation of units service ([0f26996](https://gitlab.com/fi-sas/frontend/commit/0f26996))
* **alimentation:** units list and form dev ([a0f256a](https://gitlab.com/fi-sas/frontend/commit/a0f256a))
* **app_config_bus:** endpoints for bus api ([0a95a49](https://gitlab.com/fi-sas/frontend/commit/0a95a49))
* **balance:** user menu account balance ([fe2d9cc](https://gitlab.com/fi-sas/frontend/commit/fe2d9cc))
* **bus api url:** bus link ([c7ceaa8](https://gitlab.com/fi-sas/frontend/commit/c7ceaa8))
* **bus button:** enable button on dashboard ([67d50d3](https://gitlab.com/fi-sas/frontend/commit/67d50d3))
* **bus_module_files:** module configs ([458e4b1](https://gitlab.com/fi-sas/frontend/commit/458e4b1))
* **bus_routing:** bus module route ([7b4e6c5](https://gitlab.com/fi-sas/frontend/commit/7b4e6c5))
* **cart:** add user decision menus ([b1b6c58](https://gitlab.com/fi-sas/frontend/commit/b1b6c58))
* **cart:** cart service ([53acdda](https://gitlab.com/fi-sas/frontend/commit/53acdda))
* **cart:** cart service ([4ee5cfe](https://gitlab.com/fi-sas/frontend/commit/4ee5cfe))
* **cart:** cart service ([0754543](https://gitlab.com/fi-sas/frontend/commit/0754543))
* **cart:** cart service ([3db1141](https://gitlab.com/fi-sas/frontend/commit/3db1141))
* **cart:** cart service ([35c09b0](https://gitlab.com/fi-sas/frontend/commit/35c09b0))
* **cart:** cart service ([a6ea085](https://gitlab.com/fi-sas/frontend/commit/a6ea085))
* **cart:** cart service ([f5b1ec4](https://gitlab.com/fi-sas/frontend/commit/f5b1ec4))
* **cart:** cart service ([468626a](https://gitlab.com/fi-sas/frontend/commit/468626a))
* **cart:** init cart scene ([71651a2](https://gitlab.com/fi-sas/frontend/commit/71651a2))
* **cart:** wip cart summary ([f4273a9](https://gitlab.com/fi-sas/frontend/commit/f4273a9))
* **cart-summary:** static html and css for cart summary preview ([6629ba1](https://gitlab.com/fi-sas/frontend/commit/6629ba1))
* **contacts:** creation of contacts ([18a948c](https://gitlab.com/fi-sas/frontend/commit/18a948c))
* **currencys:** creation of currencys ([38f18ff](https://gitlab.com/fi-sas/frontend/commit/38f18ff))
* **day-fifteen:** show disabled weekend days ([801b45a](https://gitlab.com/fi-sas/frontend/commit/801b45a))
* **day-fifteen:** show disabled weekend days ([25fc804](https://gitlab.com/fi-sas/frontend/commit/25fc804))
* **days:** automatic list next fifteen days ([04023a0](https://gitlab.com/fi-sas/frontend/commit/04023a0))
* **detail:** food route detail ([9fd9a57](https://gitlab.com/fi-sas/frontend/commit/9fd9a57))
* **detail:** product big shop styling ([013dfda](https://gitlab.com/fi-sas/frontend/commit/013dfda))
* **detail:** static page styled and implemented ([bb9e55a](https://gitlab.com/fi-sas/frontend/commit/bb9e55a))
* **detail:** template and initial styling ([2d6df2c](https://gitlab.com/fi-sas/frontend/commit/2d6df2c))
* **fifteen-day:** automatic fifteen days ([d3db54b](https://gitlab.com/fi-sas/frontend/commit/d3db54b))
* **item:** car item description ([b2c16ac](https://gitlab.com/fi-sas/frontend/commit/b2c16ac))
* **locals:** creation of locals and list ([caedb4c](https://gitlab.com/fi-sas/frontend/commit/caedb4c))
* **mobility:** init scene ([e6c7629](https://gitlab.com/fi-sas/frontend/commit/e6c7629))
* **mobility:** scene for list places ([3ccaeb2](https://gitlab.com/fi-sas/frontend/commit/3ccaeb2))
* **mobility:** translate scene mobility ([08f2731](https://gitlab.com/fi-sas/frontend/commit/08f2731))
* **mobility-list:** mobility route list ([94d3fbd](https://gitlab.com/fi-sas/frontend/commit/94d3fbd))
* **periods:** creation of periods and list ([2962983](https://gitlab.com/fi-sas/frontend/commit/2962983))
* **places:** origin and destiny with init swap ([a899a51](https://gitlab.com/fi-sas/frontend/commit/a899a51))
* **prices_tables:** creation of prices tables and list ([3b7c4f6](https://gitlab.com/fi-sas/frontend/commit/3b7c4f6))
* **routes:** creation of routes and list. Creation of links ([83aa02b](https://gitlab.com/fi-sas/frontend/commit/83aa02b))
* **seasons:** creation of seasons and list ([9b80740](https://gitlab.com/fi-sas/frontend/commit/9b80740))
* **services:** dynamic data for services screen ([03c892b](https://gitlab.com/fi-sas/frontend/commit/03c892b))
* **services:** in alimentation select school and get services ([48d834f](https://gitlab.com/fi-sas/frontend/commit/48d834f))
* **shop-big:** meal detail shop button ([23ac3d6](https://gitlab.com/fi-sas/frontend/commit/23ac3d6))
* **tickets_types:** creation of tickets types and list ([6ba5f70](https://gitlab.com/fi-sas/frontend/commit/6ba5f70))
* **timetables:** creation of timetables and list ([c9a131b](https://gitlab.com/fi-sas/frontend/commit/c9a131b))
* **types_days:** creation of types days and list ([1c0ccb7](https://gitlab.com/fi-sas/frontend/commit/1c0ccb7))
* **types_users:** creation of types users and list ([327d602](https://gitlab.com/fi-sas/frontend/commit/327d602))
* **weather:** weather for different places ([9c0c40f](https://gitlab.com/fi-sas/frontend/commit/9c0c40f))
* **zones:** creation of zones and list ([04dbe1e](https://gitlab.com/fi-sas/frontend/commit/04dbe1e))



<a name="0.6.5"></a>
## [0.6.5](https://gitlab.com/fi-sas/frontend/compare/v0.6.4...v0.6.5) (2018-11-09)


### Bug Fixes

* **menu:** menu condition ([88392e7](https://gitlab.com/fi-sas/frontend/commit/88392e7))



<a name="0.6.4"></a>
## [0.6.4](https://gitlab.com/fi-sas/frontend/compare/v0.5.4...v0.6.4) (2018-11-09)


### Bug Fixes

* **backoffice:** fix resolvers empty response ([7722592](https://gitlab.com/fi-sas/frontend/commit/7722592))
* **colors:** change the name of grey and red colors ([63732fe](https://gitlab.com/fi-sas/frontend/commit/63732fe))
* **communication:** add delete button to feed list and categories list ([b995f4d](https://gitlab.com/fi-sas/frontend/commit/b995f4d))
* **communication:** add video upload to posts ([695f490](https://gitlab.com/fi-sas/frontend/commit/695f490))
* **communication:** change languages conditions ([a5d4ab1](https://gitlab.com/fi-sas/frontend/commit/a5d4ab1))
* **communication:** fix error when posts dont have any translation title ([40e6914](https://gitlab.com/fi-sas/frontend/commit/40e6914))
* **config:** add feeds urls ([0bf1175](https://gitlab.com/fi-sas/frontend/commit/0bf1175))
* **configs:** delete buttons confirmation ([b91dcf3](https://gitlab.com/fi-sas/frontend/commit/b91dcf3))
* **css:** font family and 2px ([51f9318](https://gitlab.com/fi-sas/frontend/commit/51f9318))
* **date-day:** interval and theming ([5590f25](https://gitlab.com/fi-sas/frontend/commit/5590f25))
* **food:** removing unnecessary theming ([3fc6258](https://gitlab.com/fi-sas/frontend/commit/3fc6258))
* **icons:** current color and names ([8cf4952](https://gitlab.com/fi-sas/frontend/commit/8cf4952))
* **media:** change upload image component ([76e3185](https://gitlab.com/fi-sas/frontend/commit/76e3185))
* **merge:** conflicts ([db169bc](https://gitlab.com/fi-sas/frontend/commit/db169bc))
* **merge:** resolve conflicts ([a2d631b](https://gitlab.com/fi-sas/frontend/commit/a2d631b))
* **merge:** resolve conflicts ([b2a413e](https://gitlab.com/fi-sas/frontend/commit/b2a413e))
* **rfid:** fix read data ([82c08ee](https://gitlab.com/fi-sas/frontend/commit/82c08ee))
* **serial:** change from usb to serial ([2f8dc23](https://gitlab.com/fi-sas/frontend/commit/2f8dc23))
* **serial:** refactor class serial ([e521ca0](https://gitlab.com/fi-sas/frontend/commit/e521ca0))
* **service:** when initializing with invalid token ([4fd069a](https://gitlab.com/fi-sas/frontend/commit/4fd069a))
* **share:** change declaration on image componenet uploader ([2dc2bc3](https://gitlab.com/fi-sas/frontend/commit/2dc2bc3))
* **share:** fix upload componenets ([69423b6](https://gitlab.com/fi-sas/frontend/commit/69423b6))
* **share:** upload image component is now a form control ([4ebdbf8](https://gitlab.com/fi-sas/frontend/commit/4ebdbf8))
* **shared:** fix upload components test error ([8f2d735](https://gitlab.com/fi-sas/frontend/commit/8f2d735))
* **shared:** fix upload file missing language_id ([04d4a01](https://gitlab.com/fi-sas/frontend/commit/04d4a01))
* **space:** scroll space ([0b3e8d1](https://gitlab.com/fi-sas/frontend/commit/0b3e8d1))
* **svg:** svg fill color and theming ([7cbdb12](https://gitlab.com/fi-sas/frontend/commit/7cbdb12))
* **two-col:** theming ([7b731f4](https://gitlab.com/fi-sas/frontend/commit/7b731f4))
* **typings:** typescript lint errors ([03abedf](https://gitlab.com/fi-sas/frontend/commit/03abedf))
* **users:** change style from css to less ([184ee2f](https://gitlab.com/fi-sas/frontend/commit/184ee2f))
* **users:** update scope model, form and list ([c992764](https://gitlab.com/fi-sas/frontend/commit/c992764))
* **vertical-item:** interface typo ([169f549](https://gitlab.com/fi-sas/frontend/commit/169f549))


### Features

* **button:** styling antd button ([abd7221](https://gitlab.com/fi-sas/frontend/commit/abd7221))
* **button:** theme button ([0b26fc3](https://gitlab.com/fi-sas/frontend/commit/0b26fc3))
* **communication:** add categories service ([fd25249](https://gitlab.com/fi-sas/frontend/commit/fd25249))
* **communication:** add feed's service ([7fe4f5d](https://gitlab.com/fi-sas/frontend/commit/7fe4f5d))
* **communication:** add posts validation status ([c33308b](https://gitlab.com/fi-sas/frontend/commit/c33308b))
* **communication:** creation of feed's form ([d067020](https://gitlab.com/fi-sas/frontend/commit/d067020))
* **communication:** creation of form category ([1d52b9f](https://gitlab.com/fi-sas/frontend/commit/1d52b9f))
* **communication:** creation of list of feeds ([7dbb161](https://gitlab.com/fi-sas/frontend/commit/7dbb161))
* **communication:** creation of list of posts ([c173313](https://gitlab.com/fi-sas/frontend/commit/c173313))
* **communication:** creation of lists categories ([303f864](https://gitlab.com/fi-sas/frontend/commit/303f864))
* **communication:** creation of module comunication ([5a437d9](https://gitlab.com/fi-sas/frontend/commit/5a437d9))
* **communication:** creation of posts form ([1a1bdae](https://gitlab.com/fi-sas/frontend/commit/1a1bdae))
* **communication:** creation of posts service ([e56993a](https://gitlab.com/fi-sas/frontend/commit/e56993a))
* **communication:** creation of resolvers categories and channels ([958c49a](https://gitlab.com/fi-sas/frontend/commit/958c49a))
* **communication:** show history of posts status ([f9cdbd6](https://gitlab.com/fi-sas/frontend/commit/f9cdbd6))
* **configs:** add devices service ([d1b12ae](https://gitlab.com/fi-sas/frontend/commit/d1b12ae))
* **configs:** add footers service ([7754c5e](https://gitlab.com/fi-sas/frontend/commit/7754c5e))
* **configs:** add groups service ([434ad01](https://gitlab.com/fi-sas/frontend/commit/434ad01))
* **configs:** creation of languages service ([5c39a04](https://gitlab.com/fi-sas/frontend/commit/5c39a04))
* **context:** component new structure ([82c9990](https://gitlab.com/fi-sas/frontend/commit/82c9990))
* **context-large-block:** theming ([a75085e](https://gitlab.com/fi-sas/frontend/commit/a75085e))
* **core:** add confirm modal to ui service ([045edf3](https://gitlab.com/fi-sas/frontend/commit/045edf3))
* **core:** add return modal function ([f65b1d9](https://gitlab.com/fi-sas/frontend/commit/f65b1d9))
* **core:** set side menu always open when activated ([9793ade](https://gitlab.com/fi-sas/frontend/commit/9793ade))
* **electron:** init preferences ([d6d1057](https://gitlab.com/fi-sas/frontend/commit/d6d1057))
* **fifteen-days:** new component structure ([a50918e](https://gitlab.com/fi-sas/frontend/commit/a50918e))
* **food:** product list detail icons ([e3bf5dc](https://gitlab.com/fi-sas/frontend/commit/e3bf5dc))
* **food:** removing theming ([86c8b55](https://gitlab.com/fi-sas/frontend/commit/86c8b55))
* **icons:** icons reviewed ([af143c2](https://gitlab.com/fi-sas/frontend/commit/af143c2))
* **install:** add serial port to install config ([265edf5](https://gitlab.com/fi-sas/frontend/commit/265edf5))
* **large-block:** passing content ([249568b](https://gitlab.com/fi-sas/frontend/commit/249568b))
* **main-menu:** new component structure ([d57a18e](https://gitlab.com/fi-sas/frontend/commit/d57a18e))
* **medias:** add category service ([6e80f9f](https://gitlab.com/fi-sas/frontend/commit/6e80f9f))
* **medias:** add files service ([7e051c4](https://gitlab.com/fi-sas/frontend/commit/7e051c4))
* **menu-vertical-item:** new component structure ([5708a26](https://gitlab.com/fi-sas/frontend/commit/5708a26))
* **preferences:** init serial preferences ([9f4e08f](https://gitlab.com/fi-sas/frontend/commit/9f4e08f))
* **product:** new component structure ([c78dac3](https://gitlab.com/fi-sas/frontend/commit/c78dac3))
* **rfid:** implement rfid authentication ([87ac004](https://gitlab.com/fi-sas/frontend/commit/87ac004))
* **rfid:** read serial port ([4d6cc90](https://gitlab.com/fi-sas/frontend/commit/4d6cc90))
* **serial:** async serial resolver ([8c48fb7](https://gitlab.com/fi-sas/frontend/commit/8c48fb7))
* **services:** services list design ([02d5416](https://gitlab.com/fi-sas/frontend/commit/02d5416))
* **services:** services screen design ([c995203](https://gitlab.com/fi-sas/frontend/commit/c995203))
* **services:** translations ([bfc3020](https://gitlab.com/fi-sas/frontend/commit/bfc3020))
* **share:** add http error interceptor ([14f0c3d](https://gitlab.com/fi-sas/frontend/commit/14f0c3d))
* **share:** creation of resolvers all groups ([ff9ecf0](https://gitlab.com/fi-sas/frontend/commit/ff9ecf0))
* **timezone:** daylight saving time ([3f122e0](https://gitlab.com/fi-sas/frontend/commit/3f122e0))
* **users:** add scopes service ([27dfc7b](https://gitlab.com/fi-sas/frontend/commit/27dfc7b))
* **users:** add users groups service ([89097b8](https://gitlab.com/fi-sas/frontend/commit/89097b8))
* **users:** add users service ([01e7ac4](https://gitlab.com/fi-sas/frontend/commit/01e7ac4))
* **vertical-item:** vertical menu item component with data ([9752607](https://gitlab.com/fi-sas/frontend/commit/9752607))



<a name="0.5.4"></a>
## [0.5.4](https://gitlab.com/fi-sas/frontend/compare/v0.5.3...v0.5.4) (2018-10-29)


### Bug Fixes

* **video:** trying to avoid error cache operation not supported ([8601c27](https://gitlab.com/fi-sas/frontend/commit/8601c27))


### Features

* **config:** connect config with service ([6dedf49](https://gitlab.com/fi-sas/frontend/commit/6dedf49))
* **ticker:** change tickers language ([7280456](https://gitlab.com/fi-sas/frontend/commit/7280456))



<a name="0.5.3"></a>
## [0.5.3](https://gitlab.com/fi-sas/frontend/compare/v0.5.2...v0.5.3) (2018-10-26)


### Bug Fixes

* **electron:** environment was true always ([efe33ff](https://gitlab.com/fi-sas/frontend/commit/efe33ff))



<a name="0.5.2"></a>
## [0.5.2](https://gitlab.com/fi-sas/frontend/compare/v0.5.1...v0.5.2) (2018-10-26)


### Bug Fixes

* **options:** options for electron missing ([fd37c76](https://gitlab.com/fi-sas/frontend/commit/fd37c76))



<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/fi-sas/frontend/compare/v0.5.0...v0.5.1) (2018-10-26)


### Bug Fixes

* **zip:** deploy zip empty was fixed ([5546a1e](https://gitlab.com/fi-sas/frontend/commit/5546a1e))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/fi-sas/frontend/compare/v0.4.0...v0.5.0) (2018-10-26)


### Bug Fixes

* **backoffice:** add messages from ui service ([84332b7](https://gitlab.com/fi-sas/frontend/commit/84332b7))
* **backoffice:** add resolvers to all features of backoffice ([f540884](https://gitlab.com/fi-sas/frontend/commit/f540884))
* **backoffice:** change controls error detection ([a1bd5c8](https://gitlab.com/fi-sas/frontend/commit/a1bd5c8))
* **core:** create interface for the api errors on resource interface ([848002d](https://gitlab.com/fi-sas/frontend/commit/848002d))
* **core:** export resource error interface ([9740ee9](https://gitlab.com/fi-sas/frontend/commit/9740ee9))
* **css:** days position ([b016340](https://gitlab.com/fi-sas/frontend/commit/b016340))
* **css:** design adjustments ([263cd72](https://gitlab.com/fi-sas/frontend/commit/263cd72))
* **css:** remove text selection and elements outline ([dac292f](https://gitlab.com/fi-sas/frontend/commit/dac292f))
* **font:** add missing fonts and load them locally ([44f6d62](https://gitlab.com/fi-sas/frontend/commit/44f6d62))
* **import:** routing imports clean ([c5c4e3d](https://gitlab.com/fi-sas/frontend/commit/c5c4e3d))
* **keyboard:** add output to changes on keyboard values ([8dc6fff](https://gitlab.com/fi-sas/frontend/commit/8dc6fff))
* **keyboard:** get form control injection ([eff9223](https://gitlab.com/fi-sas/frontend/commit/eff9223))
* **load:** remove loading text ([b19a464](https://gitlab.com/fi-sas/frontend/commit/b19a464))
* **loading:** loading page removed ([b3f5f0a](https://gitlab.com/fi-sas/frontend/commit/b3f5f0a))
* **loading:** removed loader ([195e349](https://gitlab.com/fi-sas/frontend/commit/195e349))
* **media:** add header to media service ([74c8bb5](https://gitlab.com/fi-sas/frontend/commit/74c8bb5))
* **media:** empty media ([a3501af](https://gitlab.com/fi-sas/frontend/commit/a3501af))
* **media:** muted audio ([3ce6daf](https://gitlab.com/fi-sas/frontend/commit/3ce6daf))
* **media:** public file could be false ([98d3efd](https://gitlab.com/fi-sas/frontend/commit/98d3efd))
* **media:** remove media with empty path file ([2bd931c](https://gitlab.com/fi-sas/frontend/commit/2bd931c))
* **media:** wrong name for key ([772c644](https://gitlab.com/fi-sas/frontend/commit/772c644))
* **medias:** medias and tickers empty arrays ([6052c88](https://gitlab.com/fi-sas/frontend/commit/6052c88))
* **merge:** conflicts ([0479012](https://gitlab.com/fi-sas/frontend/commit/0479012))
* **merge:** resolve conflicts ([a630dd8](https://gitlab.com/fi-sas/frontend/commit/a630dd8))
* **merge:** resolve conflicts ([810926a](https://gitlab.com/fi-sas/frontend/commit/810926a))
* **merge:** resolve merge conflicts ([6e7b339](https://gitlab.com/fi-sas/frontend/commit/6e7b339))
* **mock:** remove mock tokens ([c3ecb2c](https://gitlab.com/fi-sas/frontend/commit/c3ecb2c))
* **nx:** upgrade nx to version 6.1.1 ([eff9e1f](https://gitlab.com/fi-sas/frontend/commit/eff9e1f))
* **provider:** app base ref token provider ([4eead2c](https://gitlab.com/fi-sas/frontend/commit/4eead2c))
* **release:** only for tags ([ccdd372](https://gitlab.com/fi-sas/frontend/commit/ccdd372))
* **style:** icons style ([868ee70](https://gitlab.com/fi-sas/frontend/commit/868ee70))
* **test:** add ticker service to providers ([ef18a9e](https://gitlab.com/fi-sas/frontend/commit/ef18a9e))
* **test:** failed pipeline ([c61650a](https://gitlab.com/fi-sas/frontend/commit/c61650a))
* **test:** play component test ([8eeda69](https://gitlab.com/fi-sas/frontend/commit/8eeda69))
* **ticker:** text color opacity ([c6f498f](https://gitlab.com/fi-sas/frontend/commit/c6f498f))
* **users:** delete repeat_password from object send on post ([e567feb](https://gitlab.com/fi-sas/frontend/commit/e567feb))
* **video:** autoplay false ([9ceee9a](https://gitlab.com/fi-sas/frontend/commit/9ceee9a))


### Features

* **account:** menu account styling ([acc7797](https://gitlab.com/fi-sas/frontend/commit/acc7797))
* **auth:** add permissions verification and token validation ([e0e11da](https://gitlab.com/fi-sas/frontend/commit/e0e11da))
* **auth:** auth menu when user logged in ([8457624](https://gitlab.com/fi-sas/frontend/commit/8457624))
* **auth:** device auth ([912a0f5](https://gitlab.com/fi-sas/frontend/commit/912a0f5))
* **auth:** init separation of concepts ([a6085f8](https://gitlab.com/fi-sas/frontend/commit/a6085f8))
* **auth:** theming ([0530d4b](https://gitlab.com/fi-sas/frontend/commit/0530d4b))
* **cart:** empty cart ([ef74bfa](https://gitlab.com/fi-sas/frontend/commit/ef74bfa))
* **context:** new context component ([9803866](https://gitlab.com/fi-sas/frontend/commit/9803866))
* **dashboard:** creating applications grid ([73eb70a](https://gitlab.com/fi-sas/frontend/commit/73eb70a))
* **dashboard:** implementing icons and style ([3e4d123](https://gitlab.com/fi-sas/frontend/commit/3e4d123))
* **device:** save token device authorize ([13e001c](https://gitlab.com/fi-sas/frontend/commit/13e001c))
* **food:** food and meal component init ([ba713c7](https://gitlab.com/fi-sas/frontend/commit/ba713c7))
* **food:** meals design ([65292bd](https://gitlab.com/fi-sas/frontend/commit/65292bd))
* **food:** meals design ([806aeba](https://gitlab.com/fi-sas/frontend/commit/806aeba))
* **food:** meals overflows ([c3f947d](https://gitlab.com/fi-sas/frontend/commit/c3f947d))
* **food:** meals static design ([ef9f961](https://gitlab.com/fi-sas/frontend/commit/ef9f961))
* **food:** static design for meals ([81da9a7](https://gitlab.com/fi-sas/frontend/commit/81da9a7))
* **history:** routing history and guards ([e434431](https://gitlab.com/fi-sas/frontend/commit/e434431))
* **intializer:** hook angular initialization ([fea5d16](https://gitlab.com/fi-sas/frontend/commit/fea5d16))
* **login:** authenticate user in service ([72d3d96](https://gitlab.com/fi-sas/frontend/commit/72d3d96))
* **meal:** simple bar horizontal ([6acfa83](https://gitlab.com/fi-sas/frontend/commit/6acfa83))
* **meals:** theming ([e72eefc](https://gitlab.com/fi-sas/frontend/commit/e72eefc))
* **media:** add category form page ([0d94a6f](https://gitlab.com/fi-sas/frontend/commit/0d94a6f))
* **media:** add form file page to upload content ([7b25721](https://gitlab.com/fi-sas/frontend/commit/7b25721))
* **media:** add list categories page ([fafc511](https://gitlab.com/fi-sas/frontend/commit/fafc511))
* **media:** add list files page ([552ed7c](https://gitlab.com/fi-sas/frontend/commit/552ed7c))
* **media:** add media module to backoffice ([6afdc10](https://gitlab.com/fi-sas/frontend/commit/6afdc10))
* **media:** join component with microservice of communication ([b9d5151](https://gitlab.com/fi-sas/frontend/commit/b9d5151))
* **media:** media with priority applied ([0a1d717](https://gitlab.com/fi-sas/frontend/commit/0a1d717))
* **media:** use token that is in storage for media service ([c55482d](https://gitlab.com/fi-sas/frontend/commit/c55482d))
* **pwa:** implementing capability to work offline ([3a2346f](https://gitlab.com/fi-sas/frontend/commit/3a2346f))
* **scroll:** horizontal drag content ([ee23c93](https://gitlab.com/fi-sas/frontend/commit/ee23c93))
* **share:** add file upload component ([bfbb6f9](https://gitlab.com/fi-sas/frontend/commit/bfbb6f9))
* **share:** add image upload component with cropperjs ([2d546c3](https://gitlab.com/fi-sas/frontend/commit/2d546c3))
* **share:** add messages to ui service ([7fca565](https://gitlab.com/fi-sas/frontend/commit/7fca565))
* **share:** error translator service for error validation from api ([785d16b](https://gitlab.com/fi-sas/frontend/commit/785d16b))
* **storage:** implementing service for storage ([6fc2fac](https://gitlab.com/fi-sas/frontend/commit/6fc2fac))
* **ticker:** connect ticker to service ([a74bbfd](https://gitlab.com/fi-sas/frontend/commit/a74bbfd))
* **tickers:** added tickers coming from service on authentication ([bf8315e](https://gitlab.com/fi-sas/frontend/commit/bf8315e))
* **timer:** tickers timer caching ([f96adc1](https://gitlab.com/fi-sas/frontend/commit/f96adc1))
* **vertical-menu:** design of vertical menu of items ([16c8ab3](https://gitlab.com/fi-sas/frontend/commit/16c8ab3))
* **weather:** connect to open weather map ([aa2c999](https://gitlab.com/fi-sas/frontend/commit/aa2c999))
* **week-day:** theming ([fa5fd16](https://gitlab.com/fi-sas/frontend/commit/fa5fd16))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/fi-sas/frontend/compare/v0.3.0...v0.4.0) (2018-08-31)


### Bug Fixes

* **auth:** change to work with new resource service ([2633921](https://gitlab.com/fi-sas/frontend/commit/2633921))
* **auth:** correct user mode load after session closed ([21e20bb](https://gitlab.com/fi-sas/frontend/commit/21e20bb))
* **auth:** fix user is send null to observable ([acf8c70](https://gitlab.com/fi-sas/frontend/commit/acf8c70))
* **config:** change config to work with new resource service ([c02e582](https://gitlab.com/fi-sas/frontend/commit/c02e582))
* **core:** change http data signature ([42191fd](https://gitlab.com/fi-sas/frontend/commit/42191fd))
* **dashboard:** add alimentation link to dashboard ([1b06242](https://gitlab.com/fi-sas/frontend/commit/1b06242))
* **header-user:** add observable of user object ([078db84](https://gitlab.com/fi-sas/frontend/commit/078db84))
* **header-user:** add the data of user ([18ac73e](https://gitlab.com/fi-sas/frontend/commit/18ac73e))
* **json:** wrong limit in footer mock ([3410b59](https://gitlab.com/fi-sas/frontend/commit/3410b59))
* **keyboard:** domain dot suffix cursor position ([cb257d8](https://gitlab.com/fi-sas/frontend/commit/cb257d8))
* **keyboard:** set font size ([0cb9720](https://gitlab.com/fi-sas/frontend/commit/0cb9720))
* **login:** fix no message in case of exception ([b71c1be](https://gitlab.com/fi-sas/frontend/commit/b71c1be))
* **login:** show different error messages ([abe8c99](https://gitlab.com/fi-sas/frontend/commit/abe8c99))
* **resource:** fix create and update methods ([e1d5cfb](https://gitlab.com/fi-sas/frontend/commit/e1d5cfb))
* **resource:** resource service now return all resource data ([21dbb2a](https://gitlab.com/fi-sas/frontend/commit/21dbb2a))
* **service:** fix when config endpoints object is empty ([53fb8f2](https://gitlab.com/fi-sas/frontend/commit/53fb8f2))
* **shared:** move page not fount and test component to shared module ([58b09de](https://gitlab.com/fi-sas/frontend/commit/58b09de))
* **test:** install tests dependencies ([7d05671](https://gitlab.com/fi-sas/frontend/commit/7d05671))
* **theme:** include paths missing for tests ([ba2abae](https://gitlab.com/fi-sas/frontend/commit/ba2abae))
* **tickers:** change to work with new resource service ([851a46d](https://gitlab.com/fi-sas/frontend/commit/851a46d))
* **users:** change the url to get scope by id ([fa0ed07](https://gitlab.com/fi-sas/frontend/commit/fa0ed07))
* **users:** fix to work with new resource service ([feba891](https://gitlab.com/fi-sas/frontend/commit/feba891))


### Features

* **alimentation:** creation of alimentation module ([452d2d7](https://gitlab.com/fi-sas/frontend/commit/452d2d7))
* **auth:** added authentication layout to the backoffice ([99c26f4](https://gitlab.com/fi-sas/frontend/commit/99c26f4))
* **auth:** creation of authentication interceptor ([dd5fe65](https://gitlab.com/fi-sas/frontend/commit/dd5fe65))
* **auth:** creation of authentication service ([c000e89](https://gitlab.com/fi-sas/frontend/commit/c000e89))
* **auth-models:** creation of user module models ([1e92fab](https://gitlab.com/fi-sas/frontend/commit/1e92fab))
* **authentication:** authentication route ([2692973](https://gitlab.com/fi-sas/frontend/commit/2692973))
* **authentication:** english translation ([a25cae1](https://gitlab.com/fi-sas/frontend/commit/a25cae1))
* **authentication:** initial theming ([2727c62](https://gitlab.com/fi-sas/frontend/commit/2727c62))
* **authentication:** reactive form ([5fc57ad](https://gitlab.com/fi-sas/frontend/commit/5fc57ad))
* **authentication:** reajust response signature ([8dc22e4](https://gitlab.com/fi-sas/frontend/commit/8dc22e4))
* **authentication:** validator on form ([dc87c1a](https://gitlab.com/fi-sas/frontend/commit/dc87c1a))
* **config:** add configuration module ([2b61f3d](https://gitlab.com/fi-sas/frontend/commit/2b61f3d))
* **config:** add footers list and form component ([76e88d2](https://gitlab.com/fi-sas/frontend/commit/76e88d2))
* **config:** add group form ([7f938bf](https://gitlab.com/fi-sas/frontend/commit/7f938bf))
* **config:** add language form ([3df6ff2](https://gitlab.com/fi-sas/frontend/commit/3df6ff2))
* **config:** add libraries core to the backoffice ([fa28506](https://gitlab.com/fi-sas/frontend/commit/fa28506))
* **config:** add list of devices ([046a4a1](https://gitlab.com/fi-sas/frontend/commit/046a4a1))
* **config:** add list of groups ([ee41269](https://gitlab.com/fi-sas/frontend/commit/ee41269))
* **config:** add list of language ([ee3b79b](https://gitlab.com/fi-sas/frontend/commit/ee3b79b))
* **config:** added form device ([b833219](https://gitlab.com/fi-sas/frontend/commit/b833219))
* **docker:** adjust release for backoffice ([71dc9fc](https://gitlab.com/fi-sas/frontend/commit/71dc9fc))
* **docker:** build only on master ([b257262](https://gitlab.com/fi-sas/frontend/commit/b257262))
* **docker:** initial test for automated deploys ([98c2751](https://gitlab.com/fi-sas/frontend/commit/98c2751))
* **guards:** add public and private guard to backoffice ([72f881b](https://gitlab.com/fi-sas/frontend/commit/72f881b))
* **guards:** added public and private guard ([8069ca5](https://gitlab.com/fi-sas/frontend/commit/8069ca5))
* **install:** initial screen for config ([6621b2a](https://gitlab.com/fi-sas/frontend/commit/6621b2a))
* **install:** save local config ([62ecd9c](https://gitlab.com/fi-sas/frontend/commit/62ecd9c))
* **keyboard:** write in the middle, accents and hide/show ([a107498](https://gitlab.com/fi-sas/frontend/commit/a107498))
* **login:** added login page to backoffice ([994e8ec](https://gitlab.com/fi-sas/frontend/commit/994e8ec))
* **scopes:** add scope form and list of scopes ([f72b67e](https://gitlab.com/fi-sas/frontend/commit/f72b67e))
* **theme:** color for ticker ([ff8f535](https://gitlab.com/fi-sas/frontend/commit/ff8f535))
* **theme:** footer and header components ([13e1344](https://gitlab.com/fi-sas/frontend/commit/13e1344))
* **theme:** initial theme aproach ([0985894](https://gitlab.com/fi-sas/frontend/commit/0985894))
* **theme:** menu theming process ([7707793](https://gitlab.com/fi-sas/frontend/commit/7707793))
* **theme:** reajust definitions ([e0c19c1](https://gitlab.com/fi-sas/frontend/commit/e0c19c1))
* **theme:** theme colors for all available components ([1c67a71](https://gitlab.com/fi-sas/frontend/commit/1c67a71))
* **user-groups:** add user-group form and list of user-groups ([7701f9e](https://gitlab.com/fi-sas/frontend/commit/7701f9e))
* **users:** add user form and list of users ([e56300f](https://gitlab.com/fi-sas/frontend/commit/e56300f))
* **users:** add users module to backoffice ([2b2a63c](https://gitlab.com/fi-sas/frontend/commit/2b2a63c))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/fi-sas/frontend/compare/v0.2.0...v0.3.0) (2018-07-30)


### Bug Fixes

* **antd:** antd does not support module split ([f53a6eb](https://gitlab.com/fi-sas/frontend/commit/f53a6eb))
* **bugs:** minor bugs ([1f0ad40](https://gitlab.com/fi-sas/frontend/commit/1f0ad40))
* **clock:** clock with date pipe ([f25686f](https://gitlab.com/fi-sas/frontend/commit/f25686f))
* **dashboard:** change declaration of DashboardModule class ([32d0b04](https://gitlab.com/fi-sas/frontend/commit/32d0b04))
* **dashboard:** set the type of variable ([85d8569](https://gitlab.com/fi-sas/frontend/commit/85d8569))
* **imports:** remove unwanted imports ([a358eaa](https://gitlab.com/fi-sas/frontend/commit/a358eaa))
* **media:** change equal comparison ([0fdd34c](https://gitlab.com/fi-sas/frontend/commit/0fdd34c))
* **media:** fix to run in electron ([8c793ed](https://gitlab.com/fi-sas/frontend/commit/8c793ed))
* **media:** media width strange behaviour ([1ebfa7e](https://gitlab.com/fi-sas/frontend/commit/1ebfa7e))
* **media:** scroll-y influence on media content ([31c4397](https://gitlab.com/fi-sas/frontend/commit/31c4397))
* **merge:** fix merge changes ([8dd4539](https://gitlab.com/fi-sas/frontend/commit/8dd4539))
* **merge:** merge branch 'develop' into FE-Translations ([1c23101](https://gitlab.com/fi-sas/frontend/commit/1c23101))
* **sidenav:** fix sub menu items link's ([5f5fcd7](https://gitlab.com/fi-sas/frontend/commit/5f5fcd7))
* **styling:** move layout css to main style file ([558f316](https://gitlab.com/fi-sas/frontend/commit/558f316))
* **test:** app component state provider ([63f2544](https://gitlab.com/fi-sas/frontend/commit/63f2544))
* **test:** refactor some tests ([9fd75fe](https://gitlab.com/fi-sas/frontend/commit/9fd75fe))


### Features

* **breadcrumb:** add breadcrumb component ([6621533](https://gitlab.com/fi-sas/frontend/commit/6621533))
* **dashboard:** add dashboard component ([8ecd8e6](https://gitlab.com/fi-sas/frontend/commit/8ecd8e6))
* **footer:** add footer component ([8a6ee72](https://gitlab.com/fi-sas/frontend/commit/8a6ee72))
* **header:** add component header to full layout ([983ae0d](https://gitlab.com/fi-sas/frontend/commit/983ae0d))
* **keyboard:** component initial for keyboard ([4534daa](https://gitlab.com/fi-sas/frontend/commit/4534daa))
* **layout:** add full layout component ([5300954](https://gitlab.com/fi-sas/frontend/commit/5300954))
* **layout:** add ui-service to components  ([d546ccd](https://gitlab.com/fi-sas/frontend/commit/d546ccd))
* **layout:** create core module ([8c68255](https://gitlab.com/fi-sas/frontend/commit/8c68255))
* **layout:** creation of ui-service for controlling the layout ([c7c68f2](https://gitlab.com/fi-sas/frontend/commit/c7c68f2))
* **media:** media slide with videos ([3a87421](https://gitlab.com/fi-sas/frontend/commit/3a87421))
* **not-found:** add page not found component ([6f78419](https://gitlab.com/fi-sas/frontend/commit/6f78419))
* **shared:** shared module ([62dd689](https://gitlab.com/fi-sas/frontend/commit/62dd689))
* **sidenav:** add side nav component ([08a3ae1](https://gitlab.com/fi-sas/frontend/commit/08a3ae1))
* **state:** redux state ([97593a3](https://gitlab.com/fi-sas/frontend/commit/97593a3))
* **structure:** create structure of backoffice ([9944462](https://gitlab.com/fi-sas/frontend/commit/9944462))
* **ticker:** async ticker data ([446ef61](https://gitlab.com/fi-sas/frontend/commit/446ef61))
* **timer:** deactivate app to screen saver ([b104181](https://gitlab.com/fi-sas/frontend/commit/b104181))
* **translation:** translations in home scene ([f4bfd09](https://gitlab.com/fi-sas/frontend/commit/f4bfd09))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/fi-sas/frontend/compare/v0.0.1...v0.2.0) (2018-07-20)


### Bug Fixes

* **clock:** added import component to spec ([4e30247](https://gitlab.com/fi-sas/frontend/commit/4e30247))
* **clock:** invalid character ([d41ff7b](https://gitlab.com/fi-sas/frontend/commit/d41ff7b))
* **core:** wrong import in spec module ([9528893](https://gitlab.com/fi-sas/frontend/commit/9528893))
* **import:** fix test import class ([0412b89](https://gitlab.com/fi-sas/frontend/commit/0412b89))
* **media:** component media name ([69caba5](https://gitlab.com/fi-sas/frontend/commit/69caba5))
* **media:** import antd module in spec ([c5ae6c8](https://gitlab.com/fi-sas/frontend/commit/c5ae6c8))
* **merge:** fix merge conflicts ([dab1bfa](https://gitlab.com/fi-sas/frontend/commit/dab1bfa))
* **merge:** merge branch 'develop' into FE-Ticker ([ab12f7b](https://gitlab.com/fi-sas/frontend/commit/ab12f7b))
* **nx:** fix nx.json with backoffice ([d7c8da6](https://gitlab.com/fi-sas/frontend/commit/d7c8da6))
* **selector:** fix prefix selectors ([c131929](https://gitlab.com/fi-sas/frontend/commit/c131929))
* **test:** fix home spec ([00ebc4a](https://gitlab.com/fi-sas/frontend/commit/00ebc4a))
* **test:** fix wrong imports ([21a5889](https://gitlab.com/fi-sas/frontend/commit/21a5889))
* **test:** forgotten import on component date ([3569e8c](https://gitlab.com/fi-sas/frontend/commit/3569e8c))
* **test:** test fixed for initial release ([9f7d344](https://gitlab.com/fi-sas/frontend/commit/9f7d344))
* **test:** test problem in ticker interval ([80dc1dc](https://gitlab.com/fi-sas/frontend/commit/80dc1dc))
* **theme:** refactor directories for theming ([41d2fca](https://gitlab.com/fi-sas/frontend/commit/41d2fca))
* **ticker:** lint problems ([8b2bc56](https://gitlab.com/fi-sas/frontend/commit/8b2bc56))


### Features

* **backoffice:** add app backoffice ([18e5751](https://gitlab.com/fi-sas/frontend/commit/18e5751))
* **clock:** clock component for header ([75e67b3](https://gitlab.com/fi-sas/frontend/commit/75e67b3))
* **configurator:** add lib configurator ([87c0857](https://gitlab.com/fi-sas/frontend/commit/87c0857))
* **language:** lazy load of languages ([080c870](https://gitlab.com/fi-sas/frontend/commit/080c870))
* **libs:** add lib utils and share ([b23bd7c](https://gitlab.com/fi-sas/frontend/commit/b23bd7c))
* **media:** media dummy ([fb47205](https://gitlab.com/fi-sas/frontend/commit/fb47205))
* **media:** media slide show example ([1a66998](https://gitlab.com/fi-sas/frontend/commit/1a66998))
* **menu:** dummy menu component ([dba3970](https://gitlab.com/fi-sas/frontend/commit/dba3970))
* **services:** resource service and related core services ([2823ad8](https://gitlab.com/fi-sas/frontend/commit/2823ad8))
* **theme:** initial theming rules ([6c15219](https://gitlab.com/fi-sas/frontend/commit/6c15219))
* **ticker:** applying design ([2340702](https://gitlab.com/fi-sas/frontend/commit/2340702))
* **ticker:** ticker version with out design ([f29b061](https://gitlab.com/fi-sas/frontend/commit/f29b061))
* **weather:** added weather component ([872f8f0](https://gitlab.com/fi-sas/frontend/commit/872f8f0))



<a name="0.0.1"></a>
## 0.0.1 (2018-07-13)


### Bug Fixes

* **merge:** merge conflict changes ([478005d](https://gitlab.com/fi-sas/frontend/commit/478005d))
* **mock:** only a record should be on footer ([ebd6ad1](https://gitlab.com/fi-sas/frontend/commit/ebd6ad1))


### Features

* **antd:** add antd ui project ([93c8b9c](https://gitlab.com/fi-sas/frontend/commit/93c8b9c))
* **bundle:** Compile and generate electron executable bundle. ([002f804](https://gitlab.com/fi-sas/frontend/commit/002f804))
* **commitlint:** add lint on commits ([a1afaf3](https://gitlab.com/fi-sas/frontend/commit/a1afaf3))
* **mock:** json rest data mock ([4befcae](https://gitlab.com/fi-sas/frontend/commit/4befcae))
* **schematic:** Added schemati ti generate files and scripts for electron. ([bba8a26](https://gitlab.com/fi-sas/frontend/commit/bba8a26))
* **translate:** add ngx-translate lib ([f0b426c](https://gitlab.com/fi-sas/frontend/commit/f0b426c))
