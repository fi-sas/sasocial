# @FIS-SASOCIAL

Repositorio do backoffice do projeto FI@SAS

## Iniciação

Estas instruções servem para obter uma cópia do projecto de forma a correr localmente na máquina para desenvolvimento e testes.

### Pré-Requisitos

* Nodejs
* Git
* Angular Cli

Este projecto usa como estrutura base [Nrwl](https://github.com/nrwl/nx) e [Angular Cli](https://github.com/angular/angular-cli).

## Instalação

Inicie por clonar o projecto para a sua máquina local.

```
git clone git@gitlab.com:fi-sas/backoffice.git
```

De seguida proceda à instalação de todas as dependências.

```
cd backoffice
npm i
```

Depois da instalação estará pronto para iniciar o backoffice.

## Indice de aplicações e comandos


| Aplicação | Comando                           | Descritivo                                                                            | Ambiente        |
| --------- | --------------------------------- | ------------------------------------------------------------------------------------- | --------------- |
| Backoffice| ng serve backoffice               | Inicia o backoffice para desenvolvimento com watch mode.                              | Desenvolvimento |
|           | npm run test:bo                   | Corre os testes do backoffice                                                         | Desenvolvimento      |
|           | npm run doc:bo                    | Gera documentação sobre o backoffice                                                  | Desenvolvimento      |
|           | npm run build:bo                  | Compilação do angular com o header AOT                                                | Produtivo       |

**TODO**: Colocar comandos para cada aplicação quando forem criadas.

## Passos para criação de novo modulo no backoffice

**TODO**


## Processo de desenvolvimento e regras

Para contribuir para o projecto é necessário ter em conta determinadas regras a cumprir. O processo utilizado é Scrum. Para cada novo device é estipulado uma sprint design na qual se fazem interações de como a feature vai ser implementada de forma a atingir o objectivo colocado pelo owner do projecto. Nesta sprint design são reunidas ideias, discussão aberta sobre elas definindo as aproachs a tomar bem como produção rápida de mocks e wireframes. No final é apresentado ao owner do projecto a solução e dá-se inicio a nivel de design a concepção final do wireframe.

No diário de uma sprint é convenção é sprints de uma semana com as cerimónias de apresentação e reunião da equipa no ultimo dia da sprint para que se discuta e apresente-se o produzido, o que correu bem ou mal, definição da próxima sprint com a recolha de tarefas do backlog e definição das mesmas para a area de sprint.

Como tools é usado uma board do [Gitlab](https://gitlab.com/fi-sas/backoffice/boards) onde se encontra todas as tarefas e processo de scrum.



### Design

**TODO**: @alves

### Desenvolvimento

As nomenclaturas usadas a nivel de git são: [github flow](https://guides.github.com/introduction/flow/). Documento ilustrativo e resumido [aqui](./docs/githubflow-online.pdf).

![github flow](./docs/git-flow.jpg)

A nomenclatura apenas tem uma pequena derivação sendo o ramo para desenvolvimento `develop`. A partir deste ramo é criado uma nova branch que irá reflectir uma feature/fix etc... Sobre esta nova branch são comitadas todas as acções de desenvolvimento. As acções devem reflectir a seguinte _Definiton of Done_ :

* Mensagens de commits mediante [nomenclatura angular](https://gist.github.com/stephenparish/9941e89d80e2bc58a153)
* Código formatado pelo prettier
* Nomenclatura de nomes regidas pelo tslint
* Criação de unit tests
* Code review

Estando já em conclusão, o developer abre um _pull request_ da branch em causa para a branch `develop`. Nesta fase inicia-se o code review e discussão/partilha de conhecimento entre os intervenientes. Também aqui de uma forma automatizada o repositório irá proceder com o automatismo de execução de testes verificando se a branch tem a possibilidade de ser _merged_ no pedido efectuado.

No final, e depois de concordância entre os intervenientes o developer responsável/assignado deverá proceder ao merge do código para a branch de desenvolvimento (develop).

#### Exemplo

Vamos supor que existe a necessidade de se criar um slider de imagens. O processo inicia-se por definir as tarefas no trello mediante os requisitos apresentados pelo owner do projecto. Dito isto imaginesmo que o card da task é o numero: 25. Por convenção de boa prática ao criares um branch deves adicionar um prefixo, como estamos sobre frontend vamos usar FE como prefixo, isto é uma ideia, poderiamos usar por device, feature etc.

Antes de criar a branch é importante actualizar o nosso branch base de desenvolvimento, no caso `develop` para evitarmos conflitos na altura do merge.

```
git pull
```

De seguida vamos criar a branch:

```
git checkout -b [BO S(NumeroDaSrint)] - [NomeDoModulo]: Tarefa Realizar
```

Depois disto estamos aptos para inciar todo o desenvolvimento do nosso slider na respectiva branch. Seguindo a [convenção de mensagens](https://conventionalcommits.org/) de commit do angular, imaginemos que estamos a finalizar o slider
e commitamos da seguinte forma:

```
git commit -m "feat(slider): Slider implementation and functionality."
```

Pós isto é necessário abrir o [pull request no gitlab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) para aprecissão/discussão/partilha de conhecimento. Não esquecer nos passos apenas de referenciar e assignar as pessoas para code review. Para mantermos o repo o mais limpo possivel selecione a opção de _Remove source branch when merge request is accepted_.

Após devidamente validade e aceite o developer assignado deve proceder ao merge e pós esta acção voltamos ao nosso branch de `develop` e voltamos a actualizar para recebermos todas as alterações que foram mesclados no acto o pull request. E com este ciclo terminado voltamos ao ponto inicial de criar uma nova branch para a feture seguinte.

### Nomenclatura de commits

A nomenclatura de commits obedece a regras, o commit segue o seguinte formato:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Tipos definidos:

* feat(feature)
* fix(bug fix)
* style(formatting)
* refactor(code change)
* test(add tests)
* build(build)
* ci(continuos integration)
* perf(performance)
* revert(revert code/feature)

`<subject>`: Texto a aplicar na mensagem.

* usar presente imperativo: "adição de feature ...."
* sem maisculas ao iniciar a frase
* não finalizar frase com ponto

`<body>`: Corpo da mensage

`<footer>`: Alterações drásticas que implicam quebrar a aplicação.

```
BREAKING CHANGES: Foi retirado a funcionalidade de input....
```

Referenciar card/issues (trello/gitlab):

```
Closes #25, #26
```

# Micro Serviços


# Deploy

Para release é usado uma tooling que trata de incrementar a verãos mediante a definição SemVer, cria um chancelou com a nomenclatura usada no standard de mensagens e adiciona uma label ao ramo (master) para definir pontos de release na nossa linha produtiva.

Para proceder deve correr o seguinte script:

```
npm run release
```

Para mais informação sobre a tooling siga o [link](https://github.com/conventional-changelog/standard-version).

# Licenças

Todos os direitos de autoria estão reservados a...

# Ferramentas 
* **Livraria UI** - https://ng.ant.design/docs/introduce/en 
* **Animações** - https://github.com/mygu/ngx-animate 
* **Gráficos** - https://swimlane.gitbook.io/ngx-charts
* **Pipes** - https://github.com/fknop/angular-pipes
* **Scrollbar** - https://github.com/zefoy/ngx-perfect-scrollbar
* **Recorte de Imagens** - https://fengyuanchen.github.io/cropperjs/
* **Mascaras** - https://github.com/JsDaddy/ngx-mask
* **Datas** - https://github.com/iamkun/dayjs
* **Datas** - https://github.com/moment/moment 

# Serviços e Componentes do Backoffice

## Core
O modulo Core contem todos os componentes que estão presente no layout do backoffice.

### UiService

UiService é o serviço responsavel pelos comportamentos do layout do backoffice, como o menu lateral, mensagens, e apresentação da área do conteudo.

| Método | Descrição  | Tipo |
|---|---|---|
| setContentWrapperActive |   |   |
| getContentWrapperActive |   |   |
| getContentWrapperActiveObservable |   |  |
| setSiderActive| | |
| getSiderActive| | |
| getSiderActiveObservable| | |
| setSiderCollapsed| | |
| getSiderCollapsed|||
| getSiderCollapsedObservable|||
| setSiderItems|||
| addSiderItem|||
| removeSiderItems|||
| removeSiderItem|||
| getSiderItems|||
| getSiderItemsObservable|||
| showMessage|||
| removeMessage|||
| showConfirm|||
| showReturnModal|||

##Shared

O moduleo Shared contem todos os componentes, serviços, guards, pipes etc... que possam ser partilhaveis e reutilizado entre modulos.

### AuthService

| Método | Descrição  | Tipo |
|---|---|---|
| login  |   |   |
| logout  |   |   |
| getAuth  |   |   |
| getUser  |   |   |
| getScopes  |   |   |
| accessToMicroservice  |   |   |
| hasPermission  |   |   |
| getUserObservable  |   |   |
| getToken  |   |   |
| validateToken  |   |   |

### Image

| Propriadade | Descrição  | Default |
|---|---|---|
|   |   |   |
|   |   |   |
|   |   |   |

### Tag


| Propriadade | Descrição  | Default |
|---|---|---|
|   |   |   |
|   |   |   |
|   |   |   |

### List

*Exemplo:*
```html
<fi-sas-list>
  <div list></div>
  <div form></div>
</fi-sas-list>
```

*Possiveis valores que a entrar no campos `fieldsKeys`:*

Normal:
```typescript
 { nome_do_campo: 'Titulo do campo'}
```

Traduções:
```typescript
  {
    'translations.name': {
      translation: 'Nome'
    }
  }
```

Tag ou Enum:
```typescript
  {
    'active': {
      label: 'Ativo',
      results: {
        true: { color: 'green', label: 'Sim'},
        false: { color: 'red', label: 'Não'}
      }
      /* No caso de sim, não podemos usar um results do TagComponent
      results: TagComponent.YesNoTag
      */
    }
  }
```

| Propriadade | Descrição  | Default |
|---|---|---|
| repoService |   |   |
| fieldKeys  |   |   |
| sortKeys  |   |   |

####ListService

| Método | Descrição | Params | Tipo |
|---|---|---|---|
| selectedItemObservable  |   |   |   |
| updateSelectedItem  |   |   |   |
| itemUpdatedObservable  |   |   |   |
| itemUpdated  |   |   |   |
| itemDeletedObservable  |   |   |   |
| deleteItem  |   |   |   |

### Form

*Exemplo:*
```html
<fi-sas-form>
  <!-- Botões para a toolstrip -->
</fi-sas-form>
```

*Tipos de fields:*

Campode normal de texto: 
```typescript
        {
          key: 'name',
          row: 1,
          type: InputType.input,
          templateOptions: {
            name: 'nome',
            label: 'Nome',
            validators: [Validators.required]
          }
        }
```

Array de traduções :
```typescript
    {
      key: 'translation',
      row: 1,
      type: InputType.multilanguage,
      templateOptions: {
        label: 'Traduções'
      },
      fields: [
      ]
    }
```

Select com dados do servidor:
```typescript
{
  key: 'user',
  type: InputType.select,
  row: 1,
  templateOptions: {
    name: 'utilizador',
    label: 'Utilizador',
    validators: [Validators.required],
    repository: this.usersService,
    }
  }
```

Select com pesquisa no servidor:
```typescript
{
  key: 'user',
  type: InputType.select,
  row: 1,
  templateOptions: {
    name: 'utilizador',
    label: 'Utilizador',
    validators: [Validators.required],
    repository: this.usersService,
    serverSearch: true,
    searchKey: 'name'
    }
  }
```

| Propriadade | Descrição  | Default |
|---|---|---|
| entityName  |   |   |
| fields |   |   |
| service |   |   |
| isListUpdate |   |   |
| formPadding | Padding do form  |  true |
| formScroll |  Scroll do form |  true |

### Toolstrip

| Propriadade | Descrição  | Default |
|---|---|---|
|   |   |   |
|   |   |   |
|   |   |   |

### ToolstripButton

```html
<fi-sas-toolstrip-button></fi-sas-toolstrip-button>
```

| Propriadade | Descrição  | Default |
|---|---|---|
| iconType |   |   |
| tooltip |   |   |
| onClick |   |   |

###UploadFile

| Propriadade | Descrição  | Default |
|---|---|---|
|   |   |   |
|   |   |   |
|   |   |   |

###UploadImage

| Propriadade | Descrição  | Default |
|---|---|---|
|   |   |   |
|   |   |   |
|   |   |   |
