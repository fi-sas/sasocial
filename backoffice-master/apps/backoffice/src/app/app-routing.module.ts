import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullLayoutComponent } from '@fi-sas/backoffice/core/layouts/full-layout/full-layout.component';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { DashboardComponent } from '@fi-sas/backoffice/components/dashboard/dashboard.component';
import { AuthLayoutComponent } from '@fi-sas/backoffice/core/layouts/auth-layout/auth-layout.component';
import { LoginComponent } from '@fi-sas/backoffice/components/login/login.component';
import { PublicGuard } from '@fi-sas/backoffice/shared/guards/public.guard';
import { PrivateGuard } from '@fi-sas/backoffice/shared/guards/private.guard';
import { UnauthorizedComponent } from '@fi-sas/backoffice/components/unauthorized/unauthorized.component';
import { CompleteLayoutComponent } from '@fi-sas/backoffice/core/layouts/complete-layout/complete-layout.component';
import { DashboardResolver } from './shared/resolvers/dashboard.resolver';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '', redirectTo: 'login', pathMatch: 'full' },
    ],
    canActivate: [PublicGuard],
  },
  {
    path: '',
    component: FullLayoutComponent,
    resolve: [DashboardResolver],
    children: [
      {
        path: 'unauthorized',
        component: UnauthorizedComponent,
        data: { breadcrumb: 'Sem autorização', title: 'Sem autorização' },
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { breadcrumb: 'Dashboard', title: 'Dashboard' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'system',
        loadChildren: './modules/system/system.module#SystemModule',
        data: { breadcrumb: 'Sistema', title: 'Sistema' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'configurations',
        loadChildren:
          './modules/configurations/configurations.module#ConfigurationModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'alimentation',
        loadChildren:
          './modules/alimentation/alimentation.module#AlimentationModule',
        data: { breadcrumb: 'Alimentação', title: 'Alimentação' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'communication',
        loadChildren:
          './modules/communication/communication.module#CommunicationModule',
        data: { breadcrumb: 'Comunicação', title: 'Comunicação' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'bus',
        loadChildren: './modules/bus/bus.module#BusModule',
        data: { breadcrumb: 'Mobilidade', title: 'Mobilidade', scope: 'bus' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'u-bike',
        loadChildren: './modules/u-bike/u-bike.module#UBikeModule',
        data: { breadcrumb: 'U-Bike', title: 'U-Bike' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'accommodation',
        loadChildren:
          './modules/accommodation/accommodation.module#AccommodationModule',
        data: { breadcrumb: 'Alojamento', title: 'Alojamento' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'emergency-fund',
        loadChildren:
          './modules/emergency-fund/emergency-fund.module#EmergencyFundModule',
        data: { breadcrumb: 'Fundo de Emergencia', title: 'Fundo de Emergencia' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'notifications',
        loadChildren:
          './modules/notifications/notifications.module#NotificationsModule',
        data: {
          breadcrumb: 'Notificações',
          title: 'Notificações',
          scope: 'notifications',
        },
        canActivate: [PrivateGuard],
      },
      {
        path: 'reports',
        loadChildren: './modules/reports/reports.module#ReportsModule',
        data: { breadcrumb: 'Relatórios', title: 'Relatórios' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'social-support',
        loadChildren:
          './modules/social-support/social-support.module#SocialSupportModule',
        data: {
          breadcrumb: 'Bolsa de Colaboradores',
          title: 'Bolsa de Colaboradores',
        },
        canActivate: [PrivateGuard],
      },
      {
        path: 'private-accommodation',
        loadChildren:
          './modules/private-accommodation/private-accommodation.module#PrivateAccommodationModule',
        data: {
          breadcrumb: 'Alojamento Privado',
          title: 'Alojamento Privado',
          scope: 'private_accommodation',
        },
        canActivate: [PrivateGuard],
      },
      {
        path: 'volunteering',
        loadChildren:
          './modules/volunteering/volunteering.module#VolunteeringModule',
        data: { breadcrumb: 'Voluntariado', title: 'Voluntariado' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'users',
        loadChildren: './modules/users/users.module#UsersModule',
        data: {
          breadcrumb: 'Utilizadores',
          title: 'Utilizadores',
          scope: 'authorization',
        },
        canActivate: [PrivateGuard],
      },
      {
        path: 'financial',
        loadChildren: './modules/financial/financial.module#FinancialModule',
        data: { breadcrumb: 'Finanças', title: 'Finanças' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'queue',
        loadChildren: './modules/queue/queue.module#QueueModule',
        data: { breadcrumb: 'Fila de Espera', title: 'Fila de Espera' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'consumption',
        loadChildren: './modules/monitoring/monitoring.module#MonitoringModule',
        data: { breadcrumb: 'Monitorização', title: 'Monitorização' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'infrastructure',
        loadChildren:
          './modules/infrastructure/infrastructure.module#InfrastructureModule',
        data: { breadcrumb: 'Infraestrutura', title: 'Infraestrutura' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'health',
        loadChildren: './modules/health/health.module#HealthModule',
        data: { breadcrumb: 'Saúde', title: 'Saúde' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'calendar',
        loadChildren: './modules/calendar/calendar.module#CalendarModule',
        data: { breadcrumb: 'Agenda', title: 'Agenda' },
        canActivate: [PrivateGuard],
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    ],
    canActivate: [PrivateGuard],
    canActivateChild: [PrivateGuard],
    data: { breadcrumb: null, title: null },
  },
  {
    path: '',
    component: CompleteLayoutComponent,
    resolve: [DashboardResolver],
    children: [
      {
        path: 'sport',
        loadChildren: './modules/sport/sport.module#SportModule',
        data: { breadcrumb: 'Desporto', title: 'Desporto' },
        canActivate: [PrivateGuard],
      },

      {
        path: 'gamification',
        loadChildren:
          './modules/gamification/gamification.module#GamificationModule',
        data: { breadcrumb: 'Gamificação', title: 'Gamificação' },
        canActivate: [PrivateGuard],
      },
      {
        path: 'unauthorized',
        component: UnauthorizedComponent,
        data: { breadcrumb: 'Sem autorização', title: 'Sem autorização' },
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada',
        },
      },
    ],
    canActivate: [PrivateGuard],
    canActivateChild: [PrivateGuard],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
