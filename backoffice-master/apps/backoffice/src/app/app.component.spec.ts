import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NxModule } from '@nrwl/nx';
import { FiCoreModule } from '@fi-sas/core';
import { CoreModule } from '@fi-sas/backoffice/core/core.module';
import { DashboardComponent } from '@fi-sas/backoffice/components/dashboard/dashboard.component';
import { LoginComponent } from '@fi-sas/backoffice/components/login/login.component';
import { RouterTestingModule } from '@angular/router/testing';
describe('AppComponent', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [AppComponent]
      }).compileComponents();
    })
  );
  it(
    'should create the app',
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    })
  );
  it(
    `should have as title 'fi-sas'`,
    async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app.title).toEqual('fi-sas');
    })
  );
});
