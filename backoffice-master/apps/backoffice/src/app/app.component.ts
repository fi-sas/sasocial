import { Component, OnInit, isDevMode, ViewChild, TemplateRef } from '@angular/core';
import { environment } from '../environments/environment';
import { VersionCheckService } from './core/services/version-check.service';
import { AuthService } from './shared/services/auth.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { first } from 'rxjs/operators';
import { NzNotificationComponent, NzNotificationService } from 'ng-zorro-antd';
import * as moment from 'moment';

@Component({
  selector: 'fi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'fi-sas';
  @ViewChild("cookieMsg", { static: true }) cookieMsg: TemplateRef<{}>;

  constructor(
    versionCheckService: VersionCheckService,
    private cookieService: CookieService,
    private authService: AuthService,
    private router: Router,
    private notification: NzNotificationService
  ) {

    versionCheckService.initVersionCheck(environment.version_check_url);

    // CHECK IF IS LOGIN FROM SSO
    if (environment.hasSSO && !this.authService.isLogged) {
      const authCookie = this.cookieService.get('authToken');
      const authFirstLogin = this.cookieService.get('authFirstLogin');

      if (authCookie) {
        this.authService.createAuthObject({ token: authCookie, expires_in: new Date(Date.now() + 1000 * 60), expires_in_ms: 1000 * 60  }).pipe(first()).subscribe(() => {
          this.router.navigate(['/']);
        });
      } else {
        // this.authService.ssoLoginUrl().pipe(first()).subscribe(res => window.location.replace(res.data[0].context));
      }
    }

  }

  ngOnInit(): void {
    if (!this.cookieService.get('cookieAcceptanceBO')) {
      this.notification.template(this.cookieMsg, {
        nzDuration: 0,
      });
    }
  }

  cookieConsent(notification: NzNotificationComponent) {
    notification.close();
    this.cookieService.put('cookieAcceptanceBO', "true", {
      expires: moment().add(1, 'year').toDate(),
    });
  }
}
