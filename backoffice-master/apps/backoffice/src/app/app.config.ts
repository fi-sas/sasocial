import { environment } from '../environments/environment';

export const BO_CONFIGURATION = {
  DEFAULT_LANG_ID: 3,
  DEFAULT_CATEGORY_ID: 1,
  DOMAINS_API: [
    { HOST: environment.sasocial_api_url, KEY: '@api_sasocial' },
    { HOST: environment.auth_api_url, KEY: '@api_auth' },
    { HOST: environment.conf_api_url, KEY: '@api_conf' },
    { HOST: environment.media_api_url, KEY: '@api_media' },
    { HOST: environment.media_url, KEY: '@api_media_url' },
    { HOST: environment.communication_api_url, KEY: '@api_communication' },
    { HOST: environment.bus_api_url, KEY: '@api_bus' },
    { HOST: environment.alimentation_api_url, KEY: '@api_alimentation' },
    {
      HOST: environment.private_accommodation_api_url,
      KEY: '@api_private_accommodation',
    },
    { HOST: environment.accommodation_api_url, KEY: '@api_accommodation' },
    { HOST: environment.infrastructure_api_url, KEY: '@api_infrastructure' },
    { HOST: environment.u_bike_api_url, KEY: '@api_u_bike' },
    { HOST: environment.sport_api_url, KEY: '@api_sport' },
    { HOST: environment.calendar_api_url, KEY: '@api_calendar' },
    { HOST: environment.current_account_api_url, KEY: '@api_current_account' },
    { HOST: environment.notifications_api_url, KEY: '@api_notifications' },
    { HOST: environment.gamification_api_url, KEY: '@api_gamification' },
    { HOST: environment.social_support_api_url, KEY: '@api_social_support' },
    { HOST: environment.payments_api_url, KEY: '@api_payments' },
    { HOST: environment.reports_api_url, KEY: '@api_reports' },
    { HOST: environment.reports_url, KEY: '@api_reports_url' },
    { HOST: environment.queue_api_url, KEY: '@api_queue' },
    { HOST: environment.volunteering_api_url, KEY: '@api_volunteering' },
    { HOST: environment.emergency_fund_api_url, KEY: '@api_emergency_fund' },
    {
      HOST: environment.measurements_monitorization_api_url,
      KEY: '@api_monitoring',
    },
    { HOST: environment.health_api_url, KEY: '@api_health' },
    { HOST: '/assets/data', KEY: '@local' },
  ],
  ENDPOINTS: {
    LOGIN: '@api_auth:/authorize/device-type/BO',
    LOGIN_USER: '@api_auth:/authorize/user',
    VALIDATE_TOKEN: '@api_auth:/authorize/validate-token',
    REFRESH_TOKEN: '@api_auth:/authorize/refresh-token/:type',
    LOGOUT: '@api_auth:/authorize/logout/:type',
    SSO_URL: '@api_auth:/authorize/sso/url?device=BO',
    SSO_CC_URL: '@api_auth:/authorize/sso-cc/url',

    SASOCIAL: {
      CACHE_INFO: '@api_sasocial:/cache/info',
      CACHE: '@api_sasocial:/cache/',
      CACHE_DELETE: '@api_sasocial:/cache/delete',
      CACHE_DELETE_ALL: '@api_sasocial:/cache/delete_all',
      BACKUPS: '@api_sasocial:/backups',
      BACKUPS_ID: '@api_sasocial:/backups/:id',
      BACKUPS_RESTORE: '@api_sasocial:/backups/restore',
      BACKUPS_ALL: '@api_sasocial:/backups/all',
      GATEWAY_REGENERATE: '@api_sasocial:/gateway/regenerate',
      GATEWAY_LIST: '@api_reports_url:/api/api/list-aliases',
    },

    USERS: {
      USERS: '@api_auth:/users',
      USERS_ID: '@api_auth:/users/:id',
      ACCOUNT_VERIFY: '@api_auth:/users/create_account_verify',
      CHANGE_STATUS: '@api_auth:/users/:user_id/status',
      USERS_GROUPS: '@api_auth:/user-groups',
      USERS_GROUPS_ID: '@api_auth:/user-groups/:id',
      USERS_USER_GROUP: '@api_auth:/users-user-groups',
      USERS_USER_GROUP_DELETE:
        '@api_auth:/users-user-groups/:user_id/:user_group_id',
      SCOPES: '@api_auth:/scopes',
      SCOPES_TREE: '@api_auth:/scopes/tree',
      SCOPES_ID: '@api_auth:/scopes/:id',
      USERS_SCOPES: '@api_auth:/users-scopes',
      USERS_SCOPES_DELETE: '@api_auth:/users-scopes/:user_id/:scope_id',
      USER_GROUPS_SCOPES: '@api_auth:/user-groups-scopes',
      USER_GROUPS_SCOPES_DELETE:
        '@api_auth:/user-groups-scopes/:user_group_id/:scope_id',
      PROFILES: '@api_auth:/profiles',
      PROFILES_ID: '@api_auth:/profiles/:id',
      TARGETS: '@api_auth:/targets',
      TARGETS_ID: '@api_auth:/targets/:id',
      TARGETS_COUNT: '@api_auth:/targets/count',
      DOCUMENT_TYPES: '@api_auth:/document-types',
      DOCUMENT_TYPES_ID: '@api_auth:/document-types/:id',
      DEPARTMENTS: '@api_auth:/departments',
      DEPARTMENTS_ID: '@api_auth:/departments/:id',
      SECTIONS: '@api_auth:/sections',
      SECTIONS_ID: '@api_auth:/sections/:id',
    },

    CONFIGURATIONS: {
      GERAL: '@api_conf:/configuration',
      TERMS: '@api_conf:/terms',
      PRIVACY_POLICIES: '@api_conf:/privacy_policies',
      INFORMATIONS: '@api_conf:/informations',
      TERMS_ID: '@api_conf:/terms/:id',
      PRIVACY_POLICIES_ID: '@api_conf:/privacy_policies/:id',
      INFORMATIONS_ID: '@api_conf:/informations/:id',
      FORM_INFORMATION: '@api_conf:/form_informations',
      FORM_INFORMATION_ID: '@api_conf:/form_informations/:id',
      CONTACTS: '@api_conf:/contacts',
      CONTACTS_ID: '@api_conf:/contacts/:id',
      COMPONENTS: '@api_conf:/components',
      COMPONENTS_ID: '@api_conf:/components/:id',
      DEVICES: '@api_conf:/devices',
      DEVICES_ID: '@api_conf:/devices/:id',
      DEVICES_TYPES: '@api_conf:/devices/types',
      DEVICES_ORIENTATIONS: '@api_conf:/devices/orientations',
      DEVICES_GROUPS: '@api_conf:/devices/:device_id/groups',
      DEVICES_COMPONENTS: '@api_conf:/devices/:device_id/components',
      DEVICES_DELETE: '@api_conf:/devices/:device_id/components/:component_id',
      GROUPS: '@api_conf:/groups',
      GROUPS_ID: '@api_conf:/groups/:id',
      GROUPS_DEVICES: '@api_conf:/group-devices',
      GROUPS_DEVICES_DELETE: '@api_conf:/group-devices/:group_id/:device_id',
      FOOTERS: '@api_conf:/footers',
      FOOTERS_ID: '@api_conf:/footers/:id',
      GROUPS_FOOTERS: '@api_conf:/group-footers',
      GROUPS_FOOTERS_DELETE: '@api_conf:/group-footers/:group_id/:footer_id',
      LANGUAGES: '@api_conf:/languages',
      LANGUAGES_ID: '@api_conf:/languages/:id',
      REORDER_LANGUAGES: '@api_conf:/languages/reorder',
      TAXES: '@api_conf:/taxes',
      TAXES_ID: '@api_conf:/taxes/:id',
      COURSE_DEGREES: '@api_conf:/course-degrees',
      COURSE_DEGREES_ID: '@api_conf:/course-degrees/:id',
      COURSES: '@api_conf:/courses',
      COURSES_ID: '@api_conf:/courses/:id',
      SERVICES: '@api_conf:/services',
      SERVICES_ID: '@api_conf:/services/:id',
      TEXTAPRESENTATION: '@api_conf:/apresentation_texts',
      TEXTAPRESENTATION_ID: '@api_conf:/apresentation_texts/:id',
      ACADEMIC_YEARS: '@api_conf:/academic_years',
      ACADEMIC_YEARS_ID: '@api_conf:/academic_years/:id',
      CURRENT_ACADEMIC_YEAR: '@api_conf:/academic_years/current',
      HOLIDAYS: '@api_conf:/holidays',
      HOLIDAYS_ID: '@api_conf:/holidays/:id',
      SECTIONS: '@api_conf:/sections_bo',
      SECTIONS_ID: '@api_conf:/sections_bo/:id',
    },

    CATEGORIES: '@api_media:/categories',
    CATEGORIES_ID: '@api_media:/categories/:id',
    MEDIAS_PATH: '@api_media_url:/:path',
    FILES: '@api_media:/files',
    FILES_ID: '@api_media:/files/:id',
    FILES_UPLOAD: '@api_media:/files/upload',
    COMMUNI_CATEGORIES: '@api_communication:/categories',
    COMMUNI_CATEGORIES_ID: '@api_communication:/categories/:id',
    COMMUNI_TARGET_POSTS: '@api_communication:/target_posts',
    COMMUNI_TARGET_POSTS_ID: '@api_communication:/target_posts/:id',
    COMMUNI_TARGET_POSTS_STATUS: '@api_communication:/target_posts/:id/status',
    COMMUNI_CONFIG: '@api_communication:/configurations',
    FEEDS: '@api_communication:/feeds',
    FEEDS_ID: '@api_communication:/feeds/:id',
    FEEDS_FETCH_RSS: '@api_communication:/feeds/fetch-rss-feeds',
    POSTS: '@api_communication:/posts',
    POSTS_ID: '@api_communication:/posts/:id',
    POSTS_MEDIAS: '@api_communication:/posts/medias',
    POSTS_TICKERS: '@api_communication:/posts/tickers',
    POSTS_CHANNELS: '@api_communication:/posts/channels',
    POSTS_APPROVE: '@api_communication:/posts/:id/approve',
    POSTS_REJECT: '@api_communication:/posts/:id/reject',
    POSTS_GROUPS: '@api_communication:/posts-groups',
    POSTS_GROUPS_DELETE: '@api_communication:/posts-groups/:post_id/:group_id',

    FOOD: {
      ENTITIES: '@api_alimentation:/entities',
      ENTITIES_ID: '@api_alimentation:/entities/:id',
      ENTITIES_USERS: '@api_alimentation:/entities/:id/users',
      ENTITIES_USERS_ID:
        '@api_alimentation:/entities/:entity_id/users/:user_id',
      ENTITIES_OF_USER: '@api_alimentation:/entities/user',
      ALLERGENS: '@api_alimentation:/allergens',
      ALLERGENS_ID: '@api_alimentation:/allergens/:id',
      DISHS: '@api_alimentation:/dishs',
      DISHS_ID: '@api_alimentation:/dishs/:id',
      DISH_TYPES: '@api_alimentation:/dishs-types',
      DISH_TYPES_ID: '@api_alimentation:/dishs-types/:id',
      DISH_TYPES_ID_PRICES: '@api_alimentation:/dishs-types/:id/prices',
      RECIPES: '@api_alimentation:/recipes',
      RECIPES_ID: '@api_alimentation:/recipes/:id',
      PRODUCTS: '@api_alimentation:/products',
      PRODUCTS_ID: '@api_alimentation:/products/:id',
      PRODUCTS_ID_DISPONIBILITY:
        '@api_alimentation:/products/:id/disponibility',
      PRODUCTS_ID_ALLERGENS: '@api_alimentation:/products/:id/allergens',
      PRODUCTS_ID_NUTRIENTS: '@api_alimentation:/products/:id/nutrients',
      PRODUCTS_ID_COMPLEMENTS: '@api_alimentation:/products/:id/complements',
      PRODUCTS_ID_COMPOSITION: '@api_alimentation:/products/:id/composition',
      PRODUCTS_ID_PRICES: '@api_alimentation:/products/:id/prices',
      PRODUCTS_ID_STOCKS: '@api_alimentation:/products/:id/stocks',
      HISTORY_STOCK: '@api_alimentation:/stocks-operations',
      LOTES:
        '@api_alimentation:/stocks/lotes?wharehouse_id=:id_whare&product_id=:id_prod',
      PRODUCTS_ID_DERIVATIVES: '@api_alimentation:/products/:id/derivatives',
      COMPLEMENTS: '@api_alimentation:/complements',
      COMPLEMENTS_ID: '@api_alimentation:/complements/:id',
      NUTRIENTS: '@api_alimentation:/nutrients',
      NUTRIENTS_ID: '@api_alimentation:/nutrients/:id',
      UNITS: '@api_alimentation:/units',
      UNITS_ID: '@api_alimentation:/units/:id',
      FAMILIES: '@api_alimentation:/families',
      FAMILIES_ID: '@api_alimentation:/families/:id',
      WHAREHOUSES: '@api_alimentation:/wharehouses',
      WHAREHOUSES_ACCESS: '@api_alimentation:/wharehouses/user_access',
      WHAREHOUSES_ID: '@api_alimentation:/wharehouses/:id',
      WHAREHOUSES_ID_OPERATIONS:
        '@api_alimentation:/wharehouses/:wharehouse_id/operations',
      WHAREHOUSES_ID_USERS: '@api_alimentation:/wharehouses/:id/users',
      WHAREHOUSES_ID_USERS_ID:
        '@api_alimentation:/wharehouses/:id/users/:user_id',
      STOCK_GROUP: '@api_alimentation:/stocks/grouped',
      STOCKS: '@api_alimentation:/stocks',
      STOCKS_ID: '@api_alimentation:/stocks/:id',
      STOCKS_ID_OPERATION: '@api_alimentation:/stocks/:id/operations',
      STOCKS_LOTE: '@api_alimentation:/stocks/lote/:lote',
      STOCKS_INVENTORY_REPORT: '@api_alimentation:/stocks/inventoryReport',
      STOCKS_EXPIRED: '@api_alimentation:/stocks/stocks/expired',
      STOCKS_QUANTITY: '@api_alimentation:/stocks/:id/quantity',
      MEALS: '@api_alimentation:/menus',
      MEALS_BULK: '@api_alimentation:/menus/bulk',
      MEALS_ID: '@api_alimentation:/menus/:id',
      MEALS_ID_COPY: '@api_alimentation:/menus/:id/copy',
      MEALS_COPY_DAY: '@api_alimentation:/menus/copy/day',
      MEALS_COPY_WEEK: '@api_alimentation:/menus/copy/week',
      MEALS_ID_ACCEPT: '@api_alimentation:/menus/:id/accept',
      MEALS_ID_REJECT: '@api_alimentation:/menus/:id/reject',
      SCHOOLS: '@api_alimentation:/schools',
      SCHOOLS_ID: '@api_alimentation:/schools/:id',
      SERVICES: '@api_alimentation:/services',
      SERVICES_ID: '@api_alimentation:/services/:id',
      ALI_RESERVATIONS: '@api_alimentation:/reservations',
      ALI_RESERVATIONS_ID: '@api_alimentation:/reservations/:id',
      RESERVATION_SERVED: '@api_alimentation:/reservations/:id/serve',
      RESERVATION_UNSERVED: '@api_alimentation:/reservations/:id/unserved',
      RESERVATION_CANCEL: '@api_alimentation:/reservations/:id/cancel',
      RESERVATION_REPORT: '@api_alimentation:/reservations/reservationsReport',
      RESERVATION_REVENUE_REPORT:
        '@api_alimentation:/reservations/revenueReport',
      ALI_RESERVATIONS_COUNT: '@api_alimentation:/reservations/count',
      ORDERS: '@api_alimentation:/orders',
      ORDERS_ID: '@api_alimentation:/orders/:id',
      ORDERS_ID_NOT_SERVED: '@api_alimentation:/orders/:id/not_served',
      ORDERS_ID_SERVED: '@api_alimentation:/orders/:id/serve',
      RESERVATIONS_COUNT_ALL: '@api_alimentation:/reservations/countAll',
      ORDERS_REVENUE_REPORT: '@api_alimentation:/orders/revenueReport',
      ORDERS_PRODUCTS_REVENUE_REPORT:
        '@api_alimentation:/orders/revenueByProductReport',

      CONFIGURATIONS: '@api_alimentation:/configurations',

      // PACKS
      PACKS: '@api_alimentation:/packs',
      PACKS_ID: '@api_alimentation:/packs/:id',
      USER_DEFAULTS: '@api_alimentation:/users_defaults',
      USER_DEFAULTS_ID: '@api_alimentation:/users_defaults/:id',
      USER_DEFAULTS_ID_UPDATE:
        '@api_alimentation:/users_defaults/:user_id/defaults',

      USERS_MEALS: '@api_alimentation:/users_meals',
      USERS_PACKS: '@api_alimentation:/users_packs',
      USERS_PACKS_ID: '@api_alimentation:/users_packs/:id',
      USERS_PACKS_ID_DESACTIVATE: '@api_alimentation:/users_packs/:id/desactivate',
      USERS_PACKS_ID_ACTIVATE: '@api_alimentation:/users_packs/:id/activate',
      USERS_PACKS_BULK: '@api_alimentation:/users_packs/bulk',

      //HACCP
      EQUIPMENTS: '@api_alimentation:/equipments',
      EQUIPMENTS_ID: '@api_alimentation:/equipments/:id',
      EQUIPMENTS_TEMPERATURE: '@api_alimentation:/equipment_temperatures',
      EQUIPMENTS_TEMPERATURE_ID:
        '@api_alimentation:/equipment_temperatures/:id',
      ANOMALIES: '@api_alimentation:/anomalies',
      ANOMALIES_ID: '@api_alimentation:/anomalies/:id',
      FOOD_TEMPERATURE: '@api_alimentation:/food_temperatures',
      FOOD_TEMPERATURE_ID: '@api_alimentation:/food_temperatures/:id',
      FOOD_TEMPERATURES_CLASSES: '@api_alimentation:/food_temperature_classes',
      FOOD_TEMPERATURES_CLASSES_ID:
        '@api_alimentation:/food_temperature_classes/:id',
      FOOD_TRANSPORT_TEMPERATURE:
        '@api_alimentation:/food_transport_temperatures',
      FOOD_TRANSPORT_TEMPERATURE_ID:
        '@api_alimentation:/food_transport_temperatures/:id',
      SANITIZE_INGRADIENTS: '@api_alimentation:/sanitize_ingredients',
      SANITIZE_INGRADIENTS_ID: '@api_alimentation:/sanitize_ingredients/:id',

      PRODUCTION_SHEETS: '@api_alimentation:/production_sheet',
      PRODUCTION_SHEETS_ID: '@api_alimentation:/production_sheet/:id',
      PRODUCTION_SHEETS_SUGESTED_RECIPES: '@api_alimentation:/production_sheet/sugested_recipes',
      PRODUCTION_SHEETS_SUGESTED_PRODUCTS: '@api_alimentation:/production_sheet/sugested_products',

      STATE_FRYING_OIL: '@api_alimentation:/state_frying_oil',
      STATE_FRYING_OIL_ID: '@api_alimentation:/state_frying_oil/:id',
      TESTEMONY_SAMPLE_COLLECTIONS: '@api_alimentation:/testimony_sample_collections',
      TESTEMONY_SAMPLE_COLLECTIONS_ID: '@api_alimentation:/testimony_sample_collections/:id',
      EQUIPMENT_SANITIZATIONS: '@api_alimentation:/equipment_sanitizations',
      EQUIPMENT_SANITIZATIONS_ID: '@api_alimentation:/equipment_sanitizations/:id',

    },

    BUS: {
      APPLICATIONS: '@api_bus:/applications',
      APPLICATIONS_ID: '@api_bus:/applications/:id',
      APPLICATIONS_STATUS: '@api_bus:/applications/:id/status',
      APPLICATIONS_WITHDRAWAL: '@api_bus:/applications/withdrawal',
      APPLICATIONS_WITHDRAWAL_ID: '@api_bus:/applications/withdrawal/:id',
      APPLICATIONS_WITHDRAWAL_STATUS:
        '@api_bus:/applications/withdrawal/:id/status',
      APPLICATIONS_LIST_REPORTS: '@api_bus:/applications/reports-list',

      APPLICATIONS_ACCEPT: '@api_bus:/applications/accept/:id',
      APPLICATIONS_REJECT: '@api_bus:/applications/reject/:id',
      APPLICATION_DECLARATION: '@api_bus:/applications/:id/declaration',

      PAYMENT_MONTHS: '@api_bus:/payment_months',
      PAYMENT_MONTHS_ID: '@api_bus:/payment_months/:id',
      TIMETABLES: '@api_bus:/timetables',
      TIMETABLES_ID: '@api_bus:/timetables/:id',
      TIMETABLE_UPDATE_HOUR: '@api_bus:/timetables/update/hour',
      TIMETABLE_DELETE_LINE:
        '@api_bus:/timetables/delete/line/:timetable_id/:line_number',
      TIMETABLE_INSERT_LINE: '@api_bus:/timetable/insert/line',
      TYPES_DAYS: '@api_bus:/types_days',
      TYPES_DAYS_ID: '@api_bus:/types_days/:id',
      TYPES_DAYS_ADD_DAY: '@api_bus:/types_days/addDay',
      TYPES_DAYS_REMOVE_DAY: '@api_bus:/types_days/removeDay/:day_group_id',
      DAYS_WEEKS: '@api_bus:/days_weeks',
      DAYS_WEEKS_ID: '@api_bus:/days_weeks/:id',
      PRICE_TABLES: '@api_bus:/price_tables',
      PRICE_TABLES_ID: '@api_bus:/price_tables/:id',
      PRICES: '@api_bus:/prices',
      PRICES_ID: '@api_bus:/prices/:id',
      SEASONS: '@api_bus:/seasons',
      SEASONS_ID: '@api_bus:/seasons/:id',
      LOCALS: '@api_bus:/locals',
      LOCALS_ID: '@api_bus:/locals/:id',
      ROUTES: '@api_bus:/routes',
      ROUTES_ID: '@api_bus:/routes/:id',
      ZONES: '@api_bus:/zones',
      ZONES_ID: '@api_bus:/zones/:id',
      TICKET_CONFIG: '@api_bus:/ticket_config',
      TICKET_CONFIG_ID: '@api_bus:/ticket_config/:id',
      LINKS: '@api_bus:/links',
      LINKS_ID: '@api_bus:/links/:id',
      TICKETS_BOUGHT: '@api_bus:/tickets_bought',
      TICKETS_BOUGHT_ID: '@api_bus:/tickets_bought/:id',
      TICKETS_BOUGHT_ID_CONFIRM: '@api_bus:/tickets_bought/:id/confirm',

      WITHDRAWALS: '@api_bus:/withdrawals',
      WITHDRAWALS_ID: '@api_bus:/withdrawals/:id',
      WITHDRAWALS_ID_APPROVE: '@api_bus:/withdrawals/:id/approve',
      WITHDRAWALS_ID_REJECT: '@api_bus:/withdrawals/:id/reject',

      SUB23_DECLARATIONS: '@api_bus:/sub23_declarations',
      SUB23_DECLARATIONS_ID: '@api_bus:/sub23_declarations/:id',
      SUB23_DECLARATIONS_STATUS: '@api_bus:/sub23_declarations/:id/status',
      SUB23_DECLARATIONS_PRINT: '@api_bus:/sub23_declarations/:id/declaration',
      SUB23_DECLARATIONS_PRINT_LIST: '@api_bus:/sub23_declarations/report-list',

      ABSENCES: '@api_bus:/absences',
      ABSENCES_ID: '@api_bus:/absences/:id',
      ABSENCES_STATUS: '@api_bus:/absences/:id/status',

      CONFIGURATIONS: '@api_bus:/configurations',
      CONFIGURATIONS_ID: '@api_bus:/configurations/:id',
      CONFIGURATION_KEY: '@api_bus:/configurations/:key',
    },

    PRIVATE_ACCOMMODATION: {
      TYPOLOGIES: '@api_private_accommodation:/typologies',
      TYPOLOGIES_ID: '@api_private_accommodation:/typologies/:id',

      TYPOLOGIES_WANTED_ADS:
        '@api_private_accommodation:/typologies/:id/wanted_ads',
      WANTED_ADS: '@api_private_accommodation:/wanted_ads',
      WANTED_ADS_ID: '@api_private_accommodation:/wanted_ads/:id',
      WANTED_ADS_APPROVE: '@api_private_accommodation:/wanted_ads/:id/approve',
      WANTED_ADS_REJECT: '@api_private_accommodation:/wanted_ads/:id/reject',

      OWNERS_APPLICATIONS: '@api_private_accommodation:/owners/application',
      OWNERS_APPLICATIONS_ID:
        '@api_private_accommodation:/owners/application/:id',
      OWNERS_APPLICATIONS_APPROVE:
        '@api_private_accommodation:/owners/application/:id/approve',
      OWNERS_APPLICATIONS_REJECT:
        '@api_private_accommodation:/owners/application/:id/reject',
      OWNERS: '@api_private_accommodation:/owners',
      OWNERS_ID: '@api_private_accommodation:/owners/:id',
      OWNERS_CHANGE_ACTIVE:
        '@api_private_accommodation:/owners/:id/change-active',
      OWNERS_LISTINGS: '@api_private_accommodation:/owners/:id/listings',

      LISTINGS: '@api_private_accommodation:/listings',
      LISTINGS_ID: '@api_private_accommodation:/listings/:id',
      LISTINGS_APPROVE: '@api_private_accommodation:/listings/:id/approve',
      LISTINGS_REJECT: '@api_private_accommodation:/listings/:id/reject',
      TYPOLOGIES_LISTINGS:
        '@api_private_accommodation:/typologies/:id/listings',

      COMPLAINTS: '@api_private_accommodation:/complaints',
      COMPLAINTS_ID: '@api_private_accommodation:/complaints/:id',
      COMPLAINTS_LISTINGS:
        '@api_private_accommodation:/complaints/:id/listings',
      COMPLAINTS_RESPONSE:
        '@api_private_accommodation:/complaints/:id/response',

      CONFIGURATIONS: '@api_private_accommodation:/configurations',
      CONFIGURATIONS_ID: '@api_private_accommodation:/configurations/:id',
      CONFIGURATIONS_MAX_MEDIAS_PER_LISTING:
        '@api_private_accommodation:/configurations/max_medias_per_listing',
      CONFIGURATIONS_MAX_LISTING_PUBLISH_DAYS:
        '@api_private_accommodation:/configurations/max_listing_publish_days',
      CONFIGURATIONS_MAX_WANTEDADS_PUBLISH_DAYS:
        '@api_private_accommodation:/configurations/max_wantedads_publish_days',
      CONFIGURATIONS_ADMIN_NOTIFICATION_EMAIL:
        '@api_private_accommodation:/configurations/admin_notification_email',
      CONFIGURATIONS_OWNER_PROFILE_ID:
        '@api_private_accommodation:/configurations/owner_profile_id',
      CONFIGURATIONS_OWNER_USER_GROUP_IDS:
        '@api_private_accommodation:/configurations/owner_user_group_ids',
      CONFIGURATIONS_OWNER_REGULATION:
        '@api_private_accommodation:/configurations/owner_regulation',
    },

    QUEUE: {
      SERVICES: '@api_queue:/services',
      SERVICES_DESACTIVE: '@api_queue:/services/:id/inactivate',
      SERVICES_ID: '@api_queue:/services/:id',
      OPERATORS: '@api_queue:/desk_operators',
      OPERATORS_ID: '@api_queue:/desk_operators/:id',
      SUBJECTS: '@api_queue:/subjects',
      SUBJECTS_OPEN: '@api_queue:/subjects/get_open_subjects_by_service',
      SUBJECT_DESACTIVE: '@api_queue:/subjects/:id/inactivate',
      UPDATE_SUBJECT: '@api_queue:/subjects/:id',
      DESKS: '@api_queue:/desks',
      DESKS_ID: '@api_queue:/desks/:id',
      DESACTIVE_SUBJECT_DESK_OPERATORS:
        '@api_queue:/subject_desk_operators/:id/desactive',
      ACTIVE_SUBJECT_DESK_OPERATORS:
        '@api_queue:/subject_desk_operators/:id/active',
      UPDATE_SUBJECT_DESK_OPERATORS: '@api_queue:/subject_desk_operators/:id',
      GET_SUBJECT_DESK_OPERATORS: '@api_queue:/subject_desk_operators',
      UPDATE_START_SUBJECT_DESK_OPERATORS:
        '@api_queue:/subject_desk_operators/createAndInitAssociation',
      START_DESK:
        '@api_queue:/desks/starService/desk/:desk_id/operator/:operator_id',
      GET_MANAGEMENT_SUBJECT_DESK_OPERATORS:
        '@api_queue:/subject_desk_operators/sameUserDesk',
      GET_TICKETS: '@api_queue:/subject_desk_operators/information/tickets',
      SUBJECT_DESK_OPERATORS: '@api_queue:/subject_desk_operators/subject',
      TICKET_DESK_GET: '@api_queue:/tickets/queue',
      TICKET_DESK: '@api_queue:/tickets/',
      END_DESK:
        '@api_queue:/desks/stopService/desk/:desk_id/operator/:operator_id',
      START_PAUSE_DESK: '@api_queue:/pauses/starpause/',
      END_PAUSE_DESK: '@api_queue:/pauses/stoppause/',
      GET_USER_LOGGED_SUBJECTS:
        '@api_queue:/subject_desk_operators/association/operating/:id',
      GET_STUDENT_TICKET_HISTORY: '@api_queue:/tickets_history/user',
      TICKET_TRANSFER: '@api_queue:/tickets/',
      SAVE_TICKET_NOTE: '@api_queue:/tickets/',
      TOTAL_TICKETS: '@api_queue:/tickets_history/percentagetickets/ALL',
      AVERAGE_RUNNING_TIME:
        '@api_queue:/hours_attendance_desks/avgtimeoperating',
      AVG_PAUSES: '@api_queue:/pauses/avgpauses',
      AVERAGE_WAITING_TIME: '@api_queue:/tickets_history/avgwaitingtime',
      TICKET_DIGITAL_PAPER: '@api_queue:/tickets_history/ticketpaperanddigital',
      TICKET_ATTENDANCE: '@api_queue:/tickets_history/totalnumberattended',
      TICKET_CANCELEDNOTAPPEAR:
        '@api_queue:/tickets_history/ticketscancelednotappear',
      AVGATTENDANCETIME: '@api_queue:/tickets_history/avgattendancetime',
      AVGBETWEENCALLATTEDENCE:
        '@api_queue:/tickets_history/avgbetweencallattendence',
      AVERAGERECOVEREDTICKETS:
        '@api_queue:/tickets_history/averagerecoveredtickets',
      TICKETSOUTOFGOAL: '@api_queue:/tickets_history/ticketsoutofgoal',
      TOTALTICKETSTRANSFERED:
        '@api_queue:/tickets_history/totalticketstransfered',
      TOTALTICKETSACTUALWAITING:
        '@api_queue:/tickets_history/totalticketsactualwaiting',
      TOTALTICKETSATTENDANCE:
        '@api_queue:/tickets_history/totalticketsattendance',
      ATTENDANCEABOVEAVG: '@api_queue:/tickets_history/attendanceaboveavg',
      TOTALDESKSSTATUS: '@api_queue:/tickets_history/totaldesksstatus',
      TOTALSERVICESSTATUS: '@api_queue:/tickets_history/totalservicesstatus',
      HISTORY: '@api_queue:/tickets_history/tickets_history_subjects',
      TICKETS_SUBJECTS: '@api_queue:/tickets/tickets_subjects',
      RECALL_TICKETS: '@api_queue:/tickets_history/tickects_history_to_recall',
      RECALL: '@api_queue:/tickets_history/recall_ticket_history',
      BROADCAST_NOTIFICATION: '@api_queue:/tickets/broadcastDeskNotification',
    },

    INFRASTRUCTURE: {
      ASSETS: '@api_infrastructure:/assets',
      ASSETS_ID: '@api_infrastructure:/assets/:id',
      BUILDINGS: '@api_infrastructure:/buildings',
      BUILDINGS_ID: '@api_infrastructure:/buildings/:id',
      FLOORS: '@api_infrastructure:/floors',
      FLOORS_ID: '@api_infrastructure:/floors/:id',
      LOCATIONS: '@api_infrastructure:/locations',
      LOCATIONS_ID: '@api_infrastructure:/locations/:id',
      ORGANIC_UNITS: '@api_infrastructure:/organic-units',
      ORGANIC_UNITS_ID: '@api_infrastructure:/organic-units/:id',
      ROOMS: '@api_infrastructure:/rooms',
      ROOMS_ID: '@api_infrastructure:/rooms/:id',
      SERVICES_INFRA: '@api_infrastructure:/services',
      SERVICES_INFRA_ID: '@api_infrastructure:/services/:id',
      WINGS: '@api_infrastructure:/wings',
      WINGS_ID: '@api_infrastructure:/wings/:id',
      DOCUMENT_TYPES: '@api_infrastructure:/document-types',
      DOCUMENT_TYPES_ID: '@api_infrastructure:/document-types/:id',
      AREAS: '@api_infrastructure:/areas',
      AREAS_ID: '@api_infrastructure:/areas/:id',
      CATEGORIES: '@api_infrastructure:/categories',
      CATEGORY_ID: '@api_infrastructure:/categories/:id',
    },

    U_BIKE: {
      /*APPLICATION_START_DATE: '@api_u_bike:/configs/application-start-date',
      APPLICATION_END_DATE: '@api_u_bike:/configs/application-end-date',
      APPLICATION_MISSING_TRIPS:
        '@api_u_bike:/configs/application-missing-trips',
      APPLICATION_NOTIFICATION_HOUR:
        '@api_u_bike:/configs/application-notification-hour',
      APPLICATION_CONTRACT_NOTIFICATIONS:
        '@api_u_bike:/configs/application-contract-notifications',*/

      CONFIGURATIONS: '@api_u_bike:/configs',
      CONFIGURATIONS_BY_KEY: '@api_u_bike:/configs/:key',

      APPLICATIONS: '@api_u_bike:/applications',
      APPLICATIONS_ID: '@api_u_bike:/applications/:id',
      APPLICATIONS_ID_STATUS: '@api_u_bike:/applications/:id/status',
      APPLICATIONS_APPROVE: '@api_u_bike:/applications/admin/:id/accept',
      APPLICATIONS_REJECT: '@api_u_bike:/applications/admin/:id/reject',
      APPLICATIONS_STATS: '@api_u_bike:/applications/stats',

      BIKE_TYPOLOGIES: '@api_u_bike:/typologies',
      BIKE_TYPOLOGIES_ID: '@api_u_bike:/typologies/:id',

      TRIPS: '@api_u_bike:/trips',
      TRIPS_ID: '@api_u_bike:/trips/:id',

      STATS_PROJECT: '@api_u_bike:/stats/project',
      STATS_BIKE: '@api_u_bike:/stats/bikes',

      BIKE: '@api_u_bike:/bikes',
      BIKE_ID: '@api_u_bike:/bikes/:id',

      FREE_BIKES: '@api_u_bike:/bikes/disponible',

      BIKE_MODEL: '@api_u_bike:/bike_models',
      BIKE_MODEL_ID: '@api_u_bike:/bike_models/:id',

      TRAININGS: '@api_u_bike:/trainings',
      TRAININGS_ID: '@api_u_bike:/trainings/:id',

      HOMEPAGE: '@api_u_bike:/configs/application-homepage',

      REGULAMENTATIONS: '@api_u_bike:/configs/regulamentations',

      APPLICATION_FORMS: '@api_u_bike:/application-forms',
      APPLICATION_FORMS_ID: '@api_u_bike:/application-forms/:id',
      APPLICATION_FORMS_ID_STATUS: '@api_u_bike:/application-forms/:id/status',
      APPLICATION_FORMS_APPROVE:
        '@api_u_bike:/application-forms/admin/:id/approve',
      APPLICATION_FORMS_REJECT:
        '@api_u_bike:/application-forms/admin/:id/reject',
      PRINT_QUEST: '@api_u_bike:/applications/print-quest',
      PRINT: '@api_u_bike:/applications/print',

      APPLICATION_REMOVE_DOCUMENTS: '@api_u_bike:/application_files/:id',
      APPLICATION_SAVE_DOCUMENTS: '@api_u_bike:/application_files',
      APPLICATION_DOCUMENTS: '@api_u_bike:/application_files',
      APPLICATION_STATS_PRINT: '@api_u_bike:/stats/bikes/print',
      APPLICATIONS_REPORT: '@api_u_bike:/applications/applications-report',
      KMS_TRAVELED_REPORT: '@api_u_bike:/trips/kms-report',
      GET_CONSENT_TERMS_FILE: '@api_u_bike:/applications/:id/consent-terms',
      GET_DELIVERY_RECEPTION_FILE: '@api_u_bike:/applications/:id/delivery-reception',
      SUBMIT_CONSENT_DELIVERY_REPORTS: '@api_u_bike:/applications/:id/submit-reports',
      LIST_BIKES_REPORT: '@api_u_bike:/bikes/bike-kms-report',
    },

    ACCOMMODATION: {
      REPORTS: {
        APPLICATION_REPORT: '@api_accommodation:/reports/application',
        APPLICATIONS_RESULTS:
          '@api_accommodation:/reports/applications-results',
        APPLICATIONS_RAW: '@api_accommodation:/reports/application-raw',
        CLOTHING_MAP: '@api_accommodation:/reports/clothing-map',
        SCHOLARSHIP_MAP:
          '@api_accommodation:/reports/schollarship-applications-map',
        REGIME_MAP: '@api_accommodation:/reports/regime-applications-map',
        EXTRAS_MAP: '@api_accommodation:/reports/extras-applications-map',
        PROCESS: '@api_accommodation:/billings/report',
        GENERATE_SEPA_FILE: '@api_accommodation:/reports/generate-sepa-file',
        SEPA: '@api_accommodation:/billings/sepa-map',
        TARIFFS_MAP: '@api_accommodation:/reports/tariffs-applications-map',
      },
      CAN_APLY: '@api_accommodation:/applications/can-apply',
      APPLICATION_PHASE_OPEN:
        '@api_accommodation:/application-phases/openPhase',
      APPLICATION_PHASE_FROM_TODAY: '@api_accommodation:/application-phases/getPhasesAfterToday',
      PERIODS: '@api_accommodation:/accommodation-periods',
      PERIODS_ID: '@api_accommodation:/accommodation-periods/:id',
      CONFIGURATIONS: '@api_accommodation:/configurations',
      CONFIGURATIONS_PROCESS_UNPROCESSED_APPLICATIONS:
        '@api_accommodation:/configurations/applications/process',
      PATRIMONY_CATEGORIES: '@api_accommodation:/patrimony-categories',
      PATRIMONY_CATEGORIES_ID: '@api_accommodation:/patrimony-categories/:id',
      APPLICATIONS_STATS: '@api_accommodation:/applications/stats',
      CONTRACT_CHANGE_STATS:
        '@api_accommodation:/applications/contract-changes-stats',
      APPLICANT_INFO: '@api_accommodation:/applications/applicant-info',
      APPLICATIONS: '@api_accommodation:/applications',
      APPLICATIONS_ID: '@api_accommodation:/applications/:id',
      APPLICATIONS_ID_STATUS: '@api_accommodation:/applications/:id/status',
      APPLICATIONS_ID_STATUS_ROLLBACK:
        '@api_accommodation:/applications/:id/status/rollback',
      APPLICATIONS_STATUS: '@api_accommodation:/applications/status',
      APPLICATIONS_ID_APPROVE_STATUS:
        '@api_accommodation:/applications/:id/admin/approve-status',
      APPLICATIONS_ID_REJECT_STATUS:
        '@api_accommodation:/applications/:id/admin/reject-status',
      APPLICATIONS_PREMUTE_ROOM:
        '@api_accommodation:/applications/:id/premute-room',
      BACKOFFICE_DATA: '@api_accommodation:/applications/:id/backoffice-data',
      RESIDENCES: '@api_accommodation:/residences',
      RESIDENCES_ID: '@api_accommodation:/residences/:id',
      RESIDENCES_OCCUPATION: '@api_accommodation:/residences/occupation',
      // RESIDENCES_TYPOLOGY_MAP_ID: '@api_accommodation:/residences/:id/typology-map',
      RESIDENCES_TYPOLOGY_MAP: '@api_accommodation:/residences/typology-map',
      FREE_ROOMS_RESIDENCE_ID: '@api_accommodation:/residences/:id/free-rooms',
      OCCUPIED_ROOMS_RESIDENCE_ID:
        '@api_accommodation:/residences/:id/occupied-rooms',
      ROOMS_MAP_RESIDENCE: '@api_accommodation:/residences/rooms-map',
      ROOMS_OCCUPANTS_RESIDENCE: '@api_accommodation:/residences/rooms-map/occupants',
      APPLICATION_PHASE: '@api_accommodation:/application-phases',
      APPLICATION_PHASE_ID: '@api_accommodation:/application-phases/:id',
      RESIDENCES_OCCUPATION_MAP:
        '@api_accommodation:/residences/occupation-map/:id',
      ROOMS: '@api_accommodation:/rooms',
      ROOMS_ID: '@api_accommodation:/rooms/:id',
      TYPOLOGIES: '@api_accommodation:/typologies',
      TYPOLOGIES_ID: '@api_accommodation:/typologies/:id',
      SERVICES: '@api_accommodation:/services',
      SERVICES_ID: '@api_accommodation:/services/:id',
      EXTRAS: '@api_accommodation:/extras',
      EXTRAS_ID: '@api_accommodation:/extras/:id',
      REGIMES: '@api_accommodation:/regimes',
      REGIMES_ID: '@api_accommodation:/regimes/:id',
      PRICES: '@api_accommodation:/price-lines',
      PRICES_ID: '@api_accommodation:/price-lines/:id',
      WITHDRAWALS: '@api_accommodation:/withdrawals',
      WITHDRAWALS_ID: '@api_accommodation:/withdrawals/:id',
      WITHDRAWALS_ID_APPROVE:
        '@api_accommodation:/withdrawals/:id/admin/approve',
      WITHDRAWALS_ID_REJECT: '@api_accommodation:/withdrawals/:id/admin/reject',
      WITHDRAWALS_ID_STATUS: '@api_accommodation:/withdrawals/:id/status',
      WITHDRAWALS_ID_CANCEL: '@api_accommodation:/withdrawals/:id/cancel',
      ABSENCES: '@api_accommodation:/absences',
      ABSENCES_ID: '@api_accommodation:/absences/:id',
      ABSENCES_ID_STATUS: '@api_accommodation:/absences/:id/status',
      ABSENCES_ID_APPROVE: '@api_accommodation:/absences/:id/admin/approve',
      ABSENCES_ID_REJECT: '@api_accommodation:/absences/:id/admin/approve',
      EXTENSIONS: '@api_accommodation:/extensions',
      EXTENSIONS_ID: '@api_accommodation:/extensions/:id',
      EXTENSIONS_ID_APPROVE: '@api_accommodation:/extensions/:id/admin/approve',
      EXTENSIONS_ID_REJECT: '@api_accommodation:/extensions/:id/admin/reject',
      EXTENSIONS_ID_STATUS: '@api_accommodation:/extensions/:id/status',
      EXTENSIONS_ID_CANCEL: '@api_accommodation:/extensions/:id/cancel',
      BILLINGS: '@api_accommodation:/billings',
      BILLINGS_ID: '@api_accommodation:/billings/:id',
      BILLINGS_STATUS: '@api_accommodation:/billings/status',
      BILLINGS_REPROCESS: '@api_accommodation:/billings/:id/reprocess',
      BILLING_ITEMS: '@api_accommodation:/billing_items',
      BILLING_ITEMS_ID: '@api_accommodation:/billing_items/:id',


      TARIFFS: '@api_accommodation:/tariffs',
      TARIFFS_ID: '@api_accommodation:/tariffs/:id',

      EXTRA_CHANGE_REQUEST: '@api_accommodation:/application-extra-changes',
      EXTRA_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-extra-changes/:id',
      EXTRA_CHANGE_REQUEST_STATUS:
        '@api_accommodation:/application-extra-changes/:id/status',
      EXTRA_CHANGE_REQUEST_APPROVE:
        '@api_accommodation:/application-extra-changes/:id/admin/approve',
      EXTRA_CHANGE_REQUEST_REJECT:
        '@api_accommodation:/application-extra-changes/:id/admin/reject',

      REGIME_CHANGE_REQUEST: '@api_accommodation:/application-regime-changes',
      REGIME_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-regime-changes/:id',
      REGIME_CHANGE_REQUEST_STATUS:
        '@api_accommodation:/application-regime-changes/:id/status',
      REGIME_CHANGE_REQUEST_APPROVE:
        '@api_accommodation:/application-regime-changes/:id/admin/approve',
      REGIME_CHANGE_REQUEST_REJECT:
        '@api_accommodation:/application-regime-changes/:id/admin/reject',

      ROOM_CHANGE_REQUEST: '@api_accommodation:/application-room-changes',
      ROOM_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-room-changes/:id',
      ROOM_CHANGE_REQUEST_STATUS:
        '@api_accommodation:/application-room-changes/:id/status',
      ROOM_CHANGE_REQUEST_APPROVE:
        '@api_accommodation:/application-room-changes/:id/admin/approve',
      ROOM_CHANGE_REQUEST_REJECT:
        '@api_accommodation:/application-room-changes/:id/admin/reject',

      MAINTENANCE_REQUEST: '@api_accommodation:/maintenance-requests',
      MAINTENANCE_REQUEST_ID: '@api_accommodation:/maintenance-requests/:id',
      MAINTENANCE_REQUEST_STATUS:
        '@api_accommodation:/maintenance-requests/:id/status',

      TARIFF_CHANGE_REQUEST: '@api_accommodation:/application-tariff-changes',
      TARIFF_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-tariff-changes/:id',
      TARIFF_CHANGE_REQUEST_STATUS:
        '@api_accommodation:/application-tariff-changes/:id/status',
      TARIFF_CHANGE_REQUEST_APPROVE:
        '@api_accommodation:/application-tariff-changes/:id/admin/approve',
      TARIFF_CHANGE_REQUEST_REJECT:
        '@api_accommodation:/application-tariff-changes/:id/admin/reject',

      OCCURRENCE: '@api_accommodation:/occurrences',
      OCCURRENCE_ID: '@api_accommodation:/occurrences/:id',

      APPLICATION_COMMUNICATION:
        '@api_accommodation:/application-communications',
      APPLICATION_RESUME_LIST: '@api_accommodation:/applications/resume-list',
      APPLICATION_COMMUNICATION_ID:
        '@api_accommodation:/application-communications/:id',
      APPLICATION_COMMUNICATION_REPLY:
        '@api_accommodation:/application-communications/:id/reply',
      APPLICATION_COMMUNICATION_CLOSE:
        '@api_accommodation:/application-communications/:id/close',

      IBAN_CHANGE_REQUEST: '@api_accommodation:/application-iban-changes',
      IBAN_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-iban-changes/:id',

      PERIOD_CHANGE_REQUEST: '@api_accommodation:/application-period-changes',
      PERIOD_CHANGE_REQUEST_ID:
        '@api_accommodation:/application-period-changes/:id',
      PERIOD_CHANGE_REQUEST_STATUS:
        '@api_accommodation:/application-period-changes/:id/status',
      PERIOD_CHANGE_REQUEST_APPROVE:
        '@api_accommodation:/application-period-changes/:id/admin/approve',
      PERIOD_CHANGE_REQUEST_REJECT:
        '@api_accommodation:/application-period-changes/:id/admin/reject',
    },

    REPORTS: {
      REPORTS: '@api_reports:/reports',
      REPORTS_ID: '@api_reports:/reports/:id',
      TEMPLATES: '@api_reports:/templates',
      TEMPLATES_ID: '@api_reports:/templates/:id',
      FILE: '@api_reports_url:/:filename',
      PRINT: '@api_reports:/templates/:key/print',
      REPORTS_READ: '@api_reports:/reports/:id/read',
    },

    MODALITIES: '@api_sport:/modalities',
    MODALITIES_ID: '@api_sport:/modalities/:id',

    VENUES: '@api_sport:/venues',
    VENUES_ID: '@api_sport:/venues/:id',

    ACTIVITIES: '@api_sport:/activities',
    ACTIVITIES_ID: '@api_sport:/activities/:id',

    RESERVATIONS: '@api_sport:/reservations',
    RESERVATIONS_ID: '@api_sport:/reservations/:id',
    RESERVATIONS_APPROVE: '@api_sport:/reservations/:id/approve',
    RESERVATIONS_REJECT: '@api_sport:/reservations/:id/reject',

    CONFIGURATIONS_SPORT: '@api_sport:/configurations',
    CONFIGURATIONS_REGULAR_TRAINING_EVENT_CATEGORY_ID:
      '@api_sport:/configurations/regular_training_event_category_id',
    CONFIGURATIONS_COMPETITION_EVENT_CATEGORY_ID:
      '@api_sport:/configurations/competition_event_category_id',
    CONFIGURATIONS_GENERAL_ACTIVITY_EVENT_CATEGORY_ID:
      '@api_sport:/configurations/general_activity_event_category_id',
    CONFIGURATIONS_ACTIVITY_REGISTRATION_FORM_ID:
      '@api_sport:/configurations/activity_registration_form_id',

    EVENTS: {
      CATEGORIES: '@api_calendar:/categorys',
      CATEGORIES_ID: '@api_calendar:/categorys/:id',
      EVENTS: '@api_calendar:/events',
      EVENTS_ID: '@api_calendar:/events/:id',
      CHANGE_STATUS: '@api_calendar:/events/status/:id',
      REPORT: '@api_calendar:/events/report/:id',
    },

    CURRENT_ACCOUNTS: {
      ACCOUNTS: '@api_current_account:/accounts',
      MOVEMENTS_USER_REPORT: '@api_current_account:/movements/user-report',
      ACCOUNTS_ID: '@api_current_account:/accounts/:id',
      ACCOUNTS_ID_PAYMENT_METHODS:
        '@api_current_account:/accounts/:account_id/payment-methods',
      MOVEMENTS: '@api_current_account:/movements',
      MOVEMENTS_HISTORY: '@api_current_account:/movements/chargesHistory',
      MOVEMENTS_HISTORY_REPORT:
        '@api_current_account:/movements/user-report-history-charges',
      MOVEMENTS_PAYMENT: '@api_current_account:/movements/pay',
      MOVEMENTS_CANCEL: '@api_current_account:/movements/cancel',
      MOVEMENTS_ID: '@api_current_account:/movements/:id',
      UNCHARGE_ACCOUNT: '@api_current_account:/movements/unchargeAccount',
      CHARGE_ACCOUNT: '@api_current_account:/movements/chargeAccount',
      MOVEMENTS_BALANCES: '@api_current_account:/movements/balances',
      PENDING_MOVEMENTS: '@api_current_account:/pending-movements',
      PENDING_MOVEMENTS_ID: '@api_current_account:/pending-movements/:id',
      PENDING_MOVEMENTS_ID_CONFIRM:
        '@api_current_account:/pending-movements/:id/confirm',
      PENDING_MOVEMENTS_ID_CANCEL:
        '@api_current_account:/pending-movements/:id/cancel',
      DEFAULT_ITEM_CONFIGURATION:
        '@api_current_account:/default_item_configuration',
      DEFAULT_ITEM_CONFIGURATION_ID:
        '@api_current_account:/default_item_configuration/:id',
      DOCUMENTS: '@api_current_account:/document_types',
      DOCUMENTS_ID: '@api_current_account:/document_types/:id',
      CASH_ACCOUNTS: '@api_current_account:/cash_account',
      CASH_ACCOUNTS_ID: '@api_current_account:/cash_account/:id',
      CASH_ACCOUNTS_AVAILABLE_ME:
        '@api_current_account:/cash_account/available-me',
      ACCOUNTS_PAYMENTS_METHODS:
        '@api_current_account:/payment_method_settings',
      ACCOUNTS_PAYMENTS_METHODS_ID:
        '@api_current_account:/payment_method_settings/:id',
      CASH_ACCOUNT_CLOSE: '@api_current_account:/cash_account/cashAccountClose',
      CASH_ACCOUNT_CLOSE_REPORT:
        '@api_current_account:/cash_account/cashAccountCloseReport',

      CHARGES_PERIOD_REPORT:
        '@api_current_account:/movements/charges-period-report',
      CHARGES_DETAILED_PERIOD_REPORT:
        '@api_current_account:/movements/charges-detailed-period-report',
      MOVEMENTS_PERIOD_REPORT:
        '@api_current_account:/movements/movements-period-report',
      SALES_PERIOD_RESUME_REPORT:
        '@api_current_account:/movements/sales-resume-period-report',
      SALES_PERIOD_DETAILED_REPORT:
        '@api_current_account:/movements/sales-detailed-period-report',
    },

    PAYMENTS: {
      PAYMENTS: '@api_payments:/payment',
      PAYMENTS_ID: '@api_payments:/payment/:id',
      PAYMENTS_PAY_MOVEMENTS: '@api_payments:/payment/pay-movements',
      PAYMENTS_CONFIRM: '@api_payments:/payment/:id/confirm',
      PAYMENTS_CANCEL: '@api_payments:/payment/:id/cancel',
      PAYMENTS_METHODS: '@api_payments:/payment-methods',
      PAYMENTS_METHODS_ID: '@api_payments:/payment-methods/:id',
      CHARGES: '@api_payments:/charges/:id',
      CHARGES_ID: '@api_payments:/charges/:id',
      CHARGES_CHARGE: '@api_payments:/charges/chargeAccount',
      UNCHARGES: '@api_payments:/uncharges/:id',
      UNCHARGES_ID: '@api_payments:/uncharges/:id',
      UNCHARGES_UNCHARGE: '@api_payments:/uncharges/unchargeAccount',
    },

    NOTIFICATIONS: {
      ALERT_TYPES: '@api_notifications:/alert-types',
      ALERT_TYPES_ID: '@api_notifications:/alert-types/:id',
      ALERT_TYPE_SIMULATE_ID: '@api_notifications:/alert-types/:id/simulate',
      ALERT_TEMPLATES: '@api_notifications:/alert-templates',
      ALERT_TEMPLATES_ID: '@api_notifications:/alert-templates/:id',
      ALERTS: '@api_notifications:/alerts',
      ALERTS_CREATE: '@api_notifications:/alerts/:alert_key',
      ALERTS_ID: '@api_notifications:/alerts/:id',
      ALERTS_ID_RETRY: '@api_notifications:/alerts/:id/retry',
      ALERTS_ID_REPLY: '@api_notifications:/alerts/:id/reply',
      ALERTS_ID_MARK_AS_READ: '@api_notifications:/alerts/:id/mask-as-read',
      NOTIFICATIONS_METHODS: '@api_notifications:/notification-methods',
      NOTIFICATIONS_METHODS_ID: '@api_notifications:/notification-methods/:id',
      NOTIFICATIONS: '@api_notifications:/notifications',
      NOTIFICATIONS_ID: '@api_notifications:/notifications/:id',
      NOTIFICATIONS_INTERNAL: '@api_notifications:/notifications/internal',
      NOTIFICATIONS_READ: '@api_notifications:/notifications/:id/read',
    },

    CHALLENGESTYPES: '@api_gamification:/challenges/types',
    CHALLENGESTYPES_ID: '@api_gamification:/challenges/types/:id',

    CHALLENGES_GAMI: '@api_gamification:/challenges',
    CHALLENGES_GAMI_ID: '@api_gamification:/challenges/:id',
    CHALLENGES_PLAYER: '@api_gamification:/challenges/:id/player/:player_id',
    CHALLENGES_PLAYER_POINTS: '@api_gamification:/challenges/point/:id',
    CHALLENGES_SCORE: '@api_gamification:/challenges/:id/score',
    CHALLENGES_POINTS: '@api_gamification:/challenges/:id/points',
    CHALLENGES_RANKING_TEAMS: '@api_gamification:/challenges/:id/ranking/teams',
    CHALLENGES_RANKING_PLAYERS:
      '@api_gamification:/challenges/:id/ranking/players',

    COMPETITIONS: '@api_gamification:/competitions',
    COMPETITIONS_ID: '@api_gamification:/competitions/:id',
    COMPETITIONS_RANKING_TEAMS:
      '@api_gamification:/competitions/:id/ranking/teams',
    COMPETITIONS_RANKING_PLAYERS:
      '@api_gamification:/competitions/:id/ranking/players',

    PLAYERS: '@api_gamification:/players',
    PLAYERS_ID: '@api_gamification:/players/:id',

    TEAMS: '@api_gamification:/teams',
    TEAMS_ID: '@api_gamification:/teams/:id',

    VOLUNTEERING: {
      ACTIVIFTIES: '@api_volunteering:/activities',
      ACTIVITIES_ID: '@api_volunteering:/activities/:id',

      APPLICATIONS: '@api_volunteering:/applications',
      APPLICATIONS_STATS: '@api_volunteering:/applications/stats',
      APPLICATIONS_ID: '@api_volunteering:/applications/:id',
      APPLICATIONS_STATUS: '@api_volunteering:/applications/status',
      APPLICATIONS_STATUS_ID: '@api_volunteering:/applications/:id/status',
      APPLICATIONS_WITHDRAW_ID:
        '@api_volunteering:/applications/:id/status/applicant-withdraw',
      APPLICATIONS_CANCEL_ID:
        '@api_volunteering:/applications/:id/status/applicant-cancel',
      APPLICATIONS_LAST_STATUS:
        '@api_volunteering:/applications/:id/last-status',
      APPLICATIONS_LIST_REPORTS: '@api_volunteering:/applications/list-report',

      GENERATE_COLLABORATION_CERTIFICATE:
        '@api_volunteering:/experience-user-interests/:id/generate-certificate',
      EXPERIENCES: '@api_volunteering:/experiences',
      EXPERIENCES_ID: '@api_volunteering:/experiences/:id',
      EXPERIENCES_USER_INTEREST: '@api_volunteering:/experiences/user-interest',
      EXPERIENCES_USER_INTEREST_STATUS:
        '@api_volunteering:/experience-user-interests/:id/status',
      EXPERIENCES_ID_STATUS: '@api_volunteering:/experiences/:id/status',
      EXPERIENCE_USER_MANIFEST:
        '@api_volunteering:/experiences/:id/users-manifest',
      EXPERIENCE_USER_MANIFEST_SELECTION:
        '@api_volunteering:/experiences/:id/users-selection',
      EXPERIENCES_TARGET_USERS:
        '@api_volunteering:/experiences/:id/target-users',
      EXPERIENCES_SEND_NOTIFICATIONS:
        '@api_volunteering:/experiences/:id/send-notifications',
      EXPERIENCE_LAST_STATUS: '@api_volunteering:/experiences/:id/last-status',
      EXPERIENCE_STATS: '@api_volunteering:/experiences/stats',

      EXPERIENCE_USER_INTERESTS: '@api_volunteering:/experience-user-interests',
      EXPERIENCE_USER_INTERESTS_ID:
        '@api_volunteering:/experience-user-interests/:id',
      EXPERIENCE_USER_INTERESTS_STATUS:
        '@api_volunteering:/experience-user-interests/:id/status',
      EXPERIENCE_USER_INTEREST_LAST_STATUS:
        '@api_volunteering:/experience-user-interests/:id/last-status',
      EXPERIENCE_USER_INTEREST_STATS:
        '@api_volunteering:/experience-user-interests/stats',

      ABSENCE_REASON: '@api_volunteering:/absence_reason',
      ABSENCE_REASON_ID: '@api_volunteering:/absence_reason/:id',

      ABSENCE_ATTENDANCES: '@api_volunteering:/attendances',
      ABSENCE_ATTENDANCES_ID: '@api_volunteering:/attendances/:id',

      ABSENCE_REASON_ACCEPT:
        '@api_volunteering:/attendances/absence-reason/:id/accept',
      ATTENDANCES_ID_CHANGE_STATUS:
        '@api_volunteering:/attendances/:id/change-status',
      VALIDATE_ATTENDANCE_ID_JUSTIFICATION:
        '@api_volunteering:/absence_reason/:id/accept',

      GENERAL_REPORTS_ID: '@api_volunteering:/generalreports/user/:id',

      SCHEDULES: '@api_volunteering:/schedules',
      SCHEDULES_ID: '@api_volunteering:/schedules/:id',

      CONFIGURATIONS_EXTERNAL_PROFILE_ID:
        '@api_volunteering:/configurations/external-entity-profile-id',
      CONFIGURATIONS_CLOSE_APPLICATIONS:
        '@api_volunteering:/applications/close-applications',

      EXTERNAL_ENTITIES: '@api_volunteering:/external_entities',

      EXTERNAL_ENTITIES_CONFIG: '@api_volunteering:/external_entities',

      GENERAL_REPORTS_USER_INTEREST:
        '@api_volunteering:/general-reports/user-interest/:id',

      USER_INTEREST: '@api_volunteering:/experience-user-interests',
      USER_INTEREST_ID: '@api_volunteering:/experience-user-interests/:id',
      USER_INTEREST_ADD_CERTIFICATE:
        '@api_volunteering:/experience-user-interests/:id/add-certificate',
      USER_INTEREST_GENERATE_CERTIFICATE:
        '@api_volunteering:/experience-user-interests/:id/generate-certificate',
      USER_INTEREST_ADD_FINAL_REPORT:
        '@api_volunteering:/experience-user-interests/:id/add-report-avaliation',

      EXPERIENCES_USER_COLLABORATION:
        '@api_volunteering:/experience-user-interests/users-colaboration',

      EXTERNAL_USERS: '@api_volunteering:/external_entities',
      EXTERNAL_USERS_ID: '@api_volunteering:/external_entities/:id',
      EXTERNAL_USERS_DEACTIVATE:
        '@api_volunteering:/external_entities/:id/inactive',
      EXTERNAL_USERS_ACTIVATE:
        '@api_volunteering:/external_entities/:id/active',
      EXTERNAL_USERS_VALIDATE:
        '@api_volunteering:/external_entities/:id/validate',

      COMPLAINS_GENERAL: '@api_volunteering:/general-complains',
      COMPLAINS_GENERAL_RESPONSE:
        '@api_volunteering:/general-complains/:id/response',
      COMPLAINS_GENERAL_STATUS:
        '@api_volunteering:/general-complains/:id/analyse',
      COMPLAINS_EXPERIENCES: '@api_volunteering:/complain-experiences',
      COMPLAINS_EXPERIENCES_RESPONSE:
        '@api_volunteering:/complain-experiences/:id/response',
      COMPLAINS_EXPERIENCES_STATUS:
        '@api_volunteering:/complain-experiences/:id/analyse',
      COMPLAINS_USER_INTERESTS: '@api_volunteering:/complain-user-interests',
      COMPLAINS_USER_INTERESTS_RESPONSE:
        '@api_volunteering:/complain-user-interests/:id/response',
      COMPLAINS_USER_INTERESTS_STATUS:
        '@api_volunteering:/complain-user-interests/:id/analyse',
      COMPLAINS_APPLICATIONS: '@api_volunteering:/complain-applications',
      COMPLAINS_APPLICATIONS_RESPONSE:
        '@api_volunteering:/complain-applications/:id/response',
      COMPLAINS_APPLICATIONS_STATUS:
        '@api_volunteering:/complain-applications/:id/analyse',
    },

    SOCIAL_SUPPORT: {
      APPLICATIONS: '@api_social_support:/applications',

      APPLICATIONS_STATS: '@api_social_support:/applications/stats',
      GENERATE_COLLABORATION_CERTIFICATE:
        '@api_social_support:/experience-user-interest/:id/generate-certificate',
      APPLICATIONS_ID: '@api_social_support:/applications/:id',
      APPLICATIONS_CERTIFICATE:
        '@api_social_support:/applications/:id/certificate',
      APPLICATIONS_STATUS: '@api_social_support:/applications/:id/status',
      APPLICATIONS_LAST_STATUS:
        '@api_social_support:/applications/:id/last-status',
      APPLICATIONS_SAVE_INTERVIEW:
        '@api_social_support:/applications/:id/add-interview-report',
      APPLICATION_SCHEDULE_INTERVIEW:
        '@api_social_support:/applications/:id/status/applicant-interview',
      APPLICATIONS_REPORT: '@api_social_support:/applications/:id/report',
      CONFIGURATIONS_CLOSE_APPLICATIONS:
        '@api_social_support:/applications/close-applications',
      EXPERIENCES: '@api_social_support:/experiences',
      EXPERIENCES_ID: '@api_social_support:/experiences/:id',
      EXPERIENCES_STATUS: '@api_social_support:/experiences/:id/status',
      EXPERIENCE_USER_INTEREST_STATS:
        '@api_social_support:/experience-user-interest/stats',
      EXPERIENCE_STATS: '@api_social_support:/experiences/stats',
      EXPERIENCES_CHANGE_PUBLISH:
        '@api_social_support:/experiences/:id/change-publish',
      EXPERIENCES_CHANGE_CLOSING_DATE:
        '@api_social_support:/experiences/:id/change-closing-date',
      EXPERIENCES_ADVISOR: '@api_social_support:/experiences/advisor',
      EXPERIENCES_TARGET_USERS:
        '@api_social_support:/experiences/:id/target-users',
      EXPERIENCES_SEND_NOTIFICATIONS:
        '@api_social_support:/experiences/:id/send-notifications',
      EXPERIENCES_USER_INTERESTS_REPORT:
        '@api_social_support:/experiences/:id/users-manifest',
      EXPERIENCE_CANDIDATE_REPORT:
        '@api_social_support:/experiences/:id/user-manifest-selection',
      EXPERIENCES_GENERATE_REPORT:
        '@api_social_support:/experiences/:id/generate-report-experience-offer',
      EXPERIENCE_USER_INTERESTS:
        '@api_social_support:/experience-user-interest',
      EXPERIENCE_USER_INTERESTS_ID:
        '@api_social_support:/experience-user-interest/:id',
      EXPERIENCE_USER_INTERESTS_STATUS:
        '@api_social_support:/experience-user-interest/:id/status',
      EXPERIENCE_USER_INTEREST_LAST_STATUS:
        '@api_social_support:/experience-user-interest/:id/last-status',
      EXPERIENCE_USER_INTERESTS_SCHEDULE_INTERVIEW:
        '@api_social_support:/experience-user-interest/:id/status/user-manifest-interview',
      EXPERIENCE_USER_INTEREST_SAVE_INTERVIEW:
        '@api_social_support:/experience-user-interest/:id/add-interview-report',
      USER_INTEREST: '@api_social_support:/experience-user-interest',
      USER_INTEREST_ID: '@api_social_support:/experience-user-interest/:id',
      USER_INTEREST_ADD_CERTIFICATE:
        '@api_social_support:/experience-user-interest/:id/add-certificate',
      USER_INTEREST_GENERATE_CERTIFICATE:
        '@api_social_support:/experience-user-interest/:id/generate-certificate',
      USER_INTEREST_ADD_FINAL_REPORT:
        '@api_social_support:/experience-user-interest/:id/add-report-avaliation',
      EXPERIENCES_USER_COLLABORATION:
        '@api_social_support:/experience-user-interest/users-colaboration',

      COMPLAINS_GENERAL: '@api_social_support:/general-complains',
      COMPLAINS_GENERAL_RESPONSE:
        '@api_social_support:/general-complains/:id/response',
      COMPLAINS_GENERAL_STATUS:
        '@api_social_support:/general-complains/:id/analyse',
      COMPLAINS_EXPERIENCES: '@api_social_support:/complain-experiences',
      COMPLAINS_EXPERIENCES_RESPONSE:
        '@api_social_support:/complain-experiences/:id/response',
      COMPLAINS_EXPERIENCES_STATUS:
        '@api_social_support:/complain-experiences/:id/analyse',
      COMPLAINS_USER_INTERESTS: '@api_social_support:/complain_user_interests',
      COMPLAINS_USER_INTERESTS_RESPONSE:
        '@api_social_support:/complain_user_interests/:id/response',
      COMPLAINS_USER_INTERESTS_STATUS:
        '@api_social_support:/complain_user_interests/:id/analyse',
      COMPLAINS_APPLICATIONS: '@api_social_support:/complain-applications',
      COMPLAINS_APPLICATIONS_RESPONSE:
        '@api_social_support:/complain-applications/:id/response',
      COMPLAINS_APPLICATIONS_STATUS:
        '@api_social_support:/complain-applications/:id/analyse',

      MONTHLY_REPORTS: '@api_social_support:/monthly-reports',
      MONTHLY_REPORTS_ID: '@api_social_support:/monthly-reports/:id/',
      MONTHLY_REPORTS_GENERATE:
        '@api_social_support:/monthly-reports/:id/generate-report',
      MONTHLY_HOURS: '@api_social_support:/monthly-reports/:id/alerts',

      ATTENDANCES: '@api_social_support:/attendances',
      ATTENDANCES_ID: '@api_social_support:/attendances/:id',
      ATTENDANCES_ID_CHAGE_STATUS:
        '@api_social_support:/attendances/:id/change-status',

      ABSENCE_REASON_ACCEPT:
        '@api_social_support:/attendances/absence-reason/:id/accept',

      ACTIVITIES: '@api_social_support:/activities',
      ACTIVITIES_ID: '@api_social_support:/activities/:id',

      PAYMENT_GRID: '@api_social_support:/payment-grid',
      PAYMENT_GRID_ID: '@api_social_support:/payment-grid/:id',
      PAYMENT_GRID_APPROVE: '@api_social_support:/payment-grid/:id/approve',
      PAYMENT_GRID_REJECT: '@api_social_support:/payment-grid/:id/reject',
      PAYMENT_GRID_GENERATE:
        '@api_social_support:/payment-grid/generate/:year/:month',
      PAYMENT_GRID_CREATE:
        '@api_social_support:/payment-grid/:month/:year/:user_interest_id',
      PAYMENT_GRID_APPROVED: '@api_social_support:/payment-grid/:id/approve',
      PAYMENT_GRID_PAY: '@api_social_support:/payment-grid/:id/pay',
      PAYMENT_GRID_REOPEN: '@api_social_support:/payment-grid/:id/reopen',
      PAYMENT_GRID_REPORT:
        '@api_social_support:/payment-grid/:id/generate/report',

      CONFIGURATIONS: '@api_social_support:/configurations',
      CONFIGURATIONS_ID: '@api_social_support:/configurations/:id',

      CONFIGURATIONS_EXTERNAL_ENTITY_PROFILE_ID:
        '@api_social_support:/configurations/external-entity-profile-id',
      /*  CONFIGURATIONS_FINANCIAL_NOTIFICATION_EMAIL: '@api_social_support:/configurations/financial_notification_email',
       */
      CONFIGURATIONS_RESPONSIBLES_PROFILES_IDS:
        '@api_social_support:/configurations/responsable-profile',
      /*
      CONFIGURATIONS_APPLICATION_EMAILS_USERS_IDS: '@api_social_support:/configurations/application_emails',
      CONFIGURATIONS_EXPERIENCE_EMAILS_USERS_IDS: '@api_social_support:/configurations/experience_emails',
      CONFIGURATIONS_INTEREST_EMAILS_USERS_IDS: '@api_social_support:/configurations/user_interest_emails',
*/

      //CONFIGURATIONS_MAX_APPLICATION_MONTHLY_HOURS: '@api_social_support:/configurations/max-hours-work',

      // CURRENT_ACCOUNT: '@api_social_support:/configurations/current-account',

      GENERAL_REPORTS_APPLICATIONS:
        '@api_social_support:/general-reports/applications/:id',
      GENERAL_REPORTS_USER_INTEREST:
        '@api_social_support:/general-reports/user-interest/:id',

      EXTERNAL_USERS: '@api_social_support:/external_entities',
      EXTERNAL_USERS_ID: '@api_social_support:/external_entities/:id',
      EXTERNAL_USERS_DEACTIVATE:
        '@api_social_support:/external_entities/:id/inactive',
      EXTERNAL_USERS_ACTIVATE:
        '@api_social_support:/external_entities/:id/active',
      EXTERNAL_USERS_VALIDATE:
        '@api_social_support:/external_entities/:id/validate',
    },

    MONITORING: {
      GROUP_EQUIPMENTS: '@api_monitoring:/measurement_group',
      GROUP_EQUIPMENTS_ID: '@api_monitoring:/measurement_group/:id',
      EQUIPMENTS: '@api_monitoring:/measurement_equipment',
      EQUIPMENTS_ID: '@api_monitoring:/measurement_equipment/:id',
      SOURCE_CONNECTION: '@api_monitoring:/measurement_source',
      SOURCE_CONNECTION_ID: '@api_monitoring:/measurement_source/:id',
      UNITS: '@api_monitoring:/measurement_unit',
      UNITS_ID: '@api_monitoring:/measurement_unit/:id',
      SENSOR: '@api_monitoring:/measurement_configuration',
      SENSOR_ID: '@api_monitoring:/measurement_configuration/:id',
      DASHBOARD_LAST_MEASUREMENT: '@api_monitoring:/measurements/equipment/:id',
      MAX_MIN_BY_SENSOR: '@api_monitoring:/measurements/sensor-max-min/:id',
      LIST_ALERTS_SENSOR:
        '@api_monitoring:/notification_measurement_configuration',
      LIST_MEASUREMENTS_BY_SENSOR: '@api_monitoring:/measurements/values/:id',
      LIST_SENSORS:
        '@api_monitoring:/measurement_configuration/user-responsable',
    },

    HEALTH: {
      TARIFFS: '@api_health:/tariff',
      TARIFFS_ID: '@api_health:/tariff/:id',
      PLACE: '@api_health:/place',
      PLACE_ID: '@api_health:/place/:id',
      PLACE_STATUS: '@api_health:/place/:id/status',
      APPOINTMENT_TYPE: '@api_health:/appointment-type',
      APPOINTMENT_TYPE_ID: '@api_health:/appointment-type/:id',
      DOCTOR: '@api_health:/doctor',
      DOCTOR_ID: '@api_health:/doctor/:id',
      DOCTOR_EXTERNAL: '@api_health:/doctor-external',
      DOCTOR_EXTERNAL_ID: '@api_health:/doctor-external/:id',
      CREATE_DOCTOR_EXTERNAL: '@api_health:/doctor-external/create',
      VALIDATE_DOCTOR_EXTERNAL: '@api_health:/doctor-external/:id/validate',
      DOCTOR_GET_SPECIALTIES: '@api_health:/doctor/:id/specialties',
      DOCTOR_GET_CALENDAR_BY_SPECIALTY: '@api_health:/doctor/calendar',
      CREATE_DOCTOR_SCHEDULE: '@api_health:/doctor/schedule/create',
      GET_DOCTOR_PERIOD: '@api_health:/doctor/schedule/get/:id',
      DELETE_DOCTOR_SCHEDULE: '@api_health:/doctor/schedule/remove/:id',
      UPDATE_DOCTOR_SCHEDULE: '@api_health:/doctor/schedule/update/:id',
      SPECIALTY: '@api_health:/specialty',
      SPECIALTY_ID: '@api_health:/specialty/:id',
      SPECIALTY_STATUS: '@api_health:/specialty/:id/status',
      SPECIALTY_DOCTORS: '@api_health:/specialty/:id/doctors',
      SPECIALTY_SCHEDULE_DOCTOR: '@api_health:/specialty/:id/schedule',
      PERIOD: '@api_health:/period/periods',
      APPOINTMENT: '@api_health:/appointment',
      APPOINTMENT_ID: '@api_health:/appointment/:id',
      ABSENCE_APPOINTMENT: '@api_health:/appointment/:id/absence',
      HISTORIC_APPOINTMENT: '@api_health:/appointment/:id/historic',
      APPROVE_REQUEST_APPOINTMENT:
        '@api_health:/appointment/:id/approve-request',
      CANCEL_APPOINTMENT: '@api_health:/appointment/:id/cancel',
      CHANGE_APPOINTMENT: '@api_health:/appointment/:id/change',
      APPOINTMENT_ATTENDANCE_CERTIFICATE:
        '@api_health:/appointment/:id/certificate',
      COMPLAINT: '@api_health:/complaint',
      ANSWER_COMPLAINT: '@api_health:/appointment/:id/answer-complaint',
      CONFIGURATIONS: '@api_health:/configurations',
    },

    EMERGENCY_FUND: {
      VERIFY_ACTIVE: '@api_emergency_fund:/applications/user-application',
    },
  },

  FILE_TYPES: [
    { label: 'PDF', value: 'pdf' },
    { label: 'Excel', value: 'xlsx' },
    { label: 'Word', value: 'docx' },
    { label: 'OpenDocument Text', value: 'odt' },
    { label: 'OpenDocument Spreadsheet', value: 'ods' },
    { label: 'CSV', value: 'csv' },
    { label: 'TXT', value: 'txt' },
  ],

  ERROR_TRANSLATOR: {
    NOT_FOUND_MESSAGE: 'Occoreu um erro desconhecido',
    REFRESH_PAGE: 'Ocorreu um erro, por favor actualize a página',
    SERVICE_DOWN: 'Serviço temporariamente indisponível',
  },
};
