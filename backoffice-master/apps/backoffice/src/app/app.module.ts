import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AppRoutingModule } from '@fi-sas/backoffice/app-routing.module';
import { CoreModule } from '@fi-sas/backoffice/core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FiCoreModule } from '@fi-sas/core';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { PrivateGuard } from '@fi-sas/backoffice/shared/guards/private.guard';
import { PublicGuard } from '@fi-sas/backoffice/shared/guards/public.guard';
import { HTTP_INTERCEPTORS } from '../../../../node_modules/@angular/common/http';
import { TokenInterceptor } from '@fi-sas/backoffice/shared/interceptors/token.interceptor';
import { ApisErrorsInterceptor } from '@fi-sas/backoffice/shared/interceptors/apis-errors.interceptor';
import { BackofficeErrorHandle } from '@fi-sas/backoffice/shared/errors/error-handle';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { NgxMaskModule } from 'ngx-mask';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { CookieModule } from 'ngx-cookie';
import { MonacoEditorModule } from 'ngx-monaco-editor';

export function initializeApp(authService: AuthService) {
  return (): Promise<any> => {
    return authService.refreshToken().toPromise();
  }
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UnauthorizedComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    FiCoreModule,
    SharedModule,
    CoreModule.forRoot(),
    AppRoutingModule,
    NgxMaskModule.forRoot(),
    CookieModule.forRoot(),
    MonacoEditorModule.forRoot(),
    LoadingBarRouterModule,
    LoadingBarHttpClientModule,
  ],
  providers: [
    AuthService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AuthService], multi: true },
    { provide: ErrorHandler, useClass: BackofficeErrorHandle },
    PrivateGuard,
    PublicGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ApisErrorsInterceptor, multi: true }
  ],
  exports: [
    CoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { }
}
