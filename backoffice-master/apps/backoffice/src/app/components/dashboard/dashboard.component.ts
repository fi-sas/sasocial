import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router } from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { first } from 'rxjs/operators';
import { DashboardModule, ServiceModel } from './model/dashboard.model';

@Component({
  selector: 'fi-sas-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  modules: DashboardModule[] = [];
  dataConfiguration: any;

  constructor(
    private uiService: UiService,
    private router: Router,
    private authService: AuthService,
    private configurationsService: ConfigurationGeralService
  ) {
    this.uiService.setContentWrapperActive(false);
    this.uiService.setSiderActive(false);
    this.uiService.setBreadcrumbVisible(false);
    this.uiService.removeSiderItems();
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    this.getServices();
  }

  getServices() {
    this.configurationsService
      .getSectionsOrder()
      .pipe(first())
      .subscribe((data) => {
        if (data.data.length > 0) {
          data.data.forEach((section) => {
            section.services.forEach((service) => {
              service.icon = this.getIcon(service.id);
              service.link = this.getLink(service.id);
              service.active = this.validPermission(service.id);
            });
            section.services = section.services.filter(
              (service) => service.active
            );
            if (section.services.length > 0) {
              const aux: DashboardModule = new DashboardModule(
                section.name,
                section.priority,
                section.services
              );
              this.modules.push(aux);
            }
          });
          let conf;
          this.modules.forEach((module) => {
            conf = module.services.find((service) => service.id == 6);
          });
          if (!conf) {
            this.addConfigurationModuleInit();
          }
        } else {
          this.addConfigurationModuleInit();
        }
      });
  }

  addConfigurationModuleInit() {
    if (this.authService.hasPermission('configuration')) {
      const service: ServiceModel[] = [];
      service.push(
        new ServiceModel(
          this.validTitleTraductions(6),
          0,
          '/configurations',
          'setting',
          this.authService.hasPermission('configuration')
        )
      );
      const aux: DashboardModule = new DashboardModule('', 0, service);
      this.modules.push(aux);
    }
  }

  getIcon(idService: number): string {
    let icon;
    switch (idService) {
      case 12:
        icon = 'message';
        break;
      case 2:
        icon = 'icons:icons-food';
        break;
      case 1:
        icon = 'home';
        break;
      case 15:
        icon = 'environment';
        break;
      case 8:
        icon = 'user';
        break;
      case 17:
        icon = 'icons:icons-scholarship';
        break;
      case 29:
        icon = 'team';
        break;
      case 28:
        icon = 'team';
        break;
      case 18:
        icon = 'schedule';
        break;
      case 19:
        icon = 'mail';
        break;
      case 6:
        icon = 'setting';
        break;
      case 11:
        icon = 'icons:walltet_v3';
        break;
      case 24:
        icon = 'dribbble';
        break;
      case 21:
        icon = 'ordered-list';
        break;
      case 25:
        icon = 'trophy';
        break;
      case 26:
        icon = 'dashboard';
        break;
      case 9:
        icon = 'bank';
        break;
      case 20:
        icon = 'tool';
        break;
      case 3:
        icon = 'icons:icons-mobility';
        break;
      case 7:
        icon = 'heart';
        break;
      case 23:
        icon = 'file-text';
        break;
      case 13:
        icon = 'bold';
        break;
      case 31:
        icon = 'code';
        break;
      case 30:
        icon = 'icons:icons-emergency-fund';
        break;
    }
    return icon;
  }

  getLink(idService: number): string {
    let link;
    switch (idService) {
      case 12:
        link = '/notifications';
        break;
      case 2:
        link = '/alimentation';
        break;
      case 1:
        link = '/accommodation';
        break;
      case 15:
        link = '/private-accommodation';
        break;
      case 8:
        link = '/users';
        break;
      case 17:
        link = '/social-support';
        break;
      case 29:
        link = '/mentoring';
        break;
      case 28:
        link = '/volunteering';
        break;
      case 18:
        link = '/calendar';
        break;
      case 19:
        link = '/communication';
        break;
      case 6:
        link = '/configurations';
        break;
      case 11:
        link = '/financial';
        break;
      case 24:
        link = '/sport';
        break;
      case 21:
        link = '/queue';
        break;
      case 25:
        link = '/gamification';
        break;
      case 26:
        link = '/consumption';
        break;
      case 9:
        link = '/infrastructure';
        break;
      case 20:
        link = '/maintenance';
        break;
      case 3:
        link = '/bus';
        break;
      case 7:
        link = '/health';
        break;
      case 23:
        link = '/reports';
        break;
      case 13:
        link = '/u-bike';
        break;
      case 31:
        link = '/system';
        break;
      case 30:
        link = '/emergency-fund';
        break;
    }
    return link;
  }

  validPermission(idService: number): boolean {
    let permission;
    switch (idService) {
      case 12:
        permission = 'notifications';
        break;
      case 2:
        permission = 'alimentation';
        break;
      case 1:
        permission = 'accommodation';
        break;
      case 15:
        permission = 'private_accommodation';
        break;
      case 8:
        permission = 'authorization';
        break;
      case 17:
        permission = 'social_scholarship';
        break;
      case 29:
        permission = '';
        break;
      case 28:
        permission = 'volunteering';
        break;
      case 18:
        permission = 'calendar';
        break;
      case 19:
        permission = 'communication';
        break;
      case 6:
        permission = 'configuration';
        break;
      case 11:
        permission = 'current_account';
        break;
      case 24:
        permission = 'sport';
        break;
      case 21:
        permission = 'queue';
        break;
      case 25:
        permission = 'gamification';
        break;
      case 26:
        permission = 'infrastructure';
        break;
      case 9:
        permission = 'infrastructure';
        break;
      case 20:
        permission = 'maintenance';
        break;
      case 3:
        permission = 'bus';
        break;
      case 7:
        permission = 'health';
        break;
      case 23:
        permission = 'reports';
        break;
      case 13:
        permission = 'u_bike';
        break;
      case 31:
        permission = 'sasocial:system';
        break;
      case 30:
        permission = 'emergency_fund';
        break;
    }
    if (permission != '') {
      return this.authService.hasPermission(permission);
    }
    return false;
  }

  ngOnDestroy() {
    this.uiService.setBreadcrumbVisible(true);
  }

  selectModule(module) {
    this.router.navigate([module.link]);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find((t) => t.id === id) != null &&
      this.dataConfiguration.find((t) => t.id === id) != undefined
      ? this.dataConfiguration.find((t) => t.id === id).translations
      : [];
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans)
      ? translation.find((tran) => tran.language_id == id_trans).name
      : '';
  }
}
