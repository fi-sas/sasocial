import { TranslationModel } from "@fi-sas/backoffice/modules/configurations/models/translations.model";

export class DashboardModule {
    id: number;
    name?: string;
    priority?: number;
    services: ServiceModel[] = [];

    constructor(name?: string, priority?: number, services?: ServiceModel[]) {
        this.name = name;
        this.priority = priority;
        this.services = services;
    }
}

export class ServiceModel {
    id: number;
    translations?: TranslationModel[];
    priority?: number;
    link?: string;
    icon?: string;
    active?: boolean;

    constructor(translations?: TranslationModel[], priority?: number, link?: string, icon?: string, active?: boolean) {
        this.translations = translations;
        this.priority = priority;
        this.link = link;
        this.icon = icon;
        this.active = active;
    }
}