import { environment } from 'apps/backoffice/src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isLoading = false;
  showError = false;
  error: string;
  returnUrl: string;
  hasSSO = environment.hasSSO;
  remaining_attempts = null;
  too_many_attempts = false;

  constructor(
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  getEmailInputError() {
    return this.loginForm.controls.email.errors.required
      ? 'Introduza o email'
      : this.loginForm.controls.email.errors.email
      ? 'Introduza um email válido'
      : '';
  }

  getPasswordInputError() {
    return this.loginForm.controls.password.errors.required
      ? 'Introduza a password'
      : '';
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(formValue: any, valid: boolean) {
    if (!valid) {
      this.loginForm.markAsDirty();
      return;
    }

    this.showError = false;
    this.isLoading = true;

    this.authService
      .login(formValue.email, formValue.password)
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe(
        (value) => {
          this.isLoading = false;

          this.router.navigate([this.returnUrl]);
        },
        (err) => {
          if (
            err.error.errors[0] &&
            err.error.errors[0].data.remaining_attempts
          ) {
            this.remaining_attempts =
              err.error.errors[0].data.remaining_attempts;
          } else {
            this.remaining_attempts = null;
          }

          if (
            err.error.errors[0] &&
            err.error.errors[0].type === 'ERR_TOO_MANY_LOGIN_ATTEMPTS'
          ) {
            this.too_many_attempts = true;
          } else {
            this.too_many_attempts = false;
          }
        }
      );
  }

  redirectSSOUrl() {
    this.authService.ssoLoginUrl().subscribe((data) => {
      location.replace(data.data[0].context);
    });
  }
}
