import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotFoundComponent } from './page-not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CommonModule, Location } from '@angular/common';

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;
  let location: Location;
  let router: Router;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [CommonModule, RouterTestingModule, NgZorroAntdModule],
        providers: [],
        declarations: [PageNotFoundComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router.initialNavigation();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
