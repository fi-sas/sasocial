import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.less']
})
export class PageNotFoundComponent implements OnInit {
  constructor(private location: Location, private uiService: UiService) {

  }

  ngOnInit() {
    this.uiService.setContentWrapperActive(true);
  }

  voltar() {
    this.location.back();
  }
}
