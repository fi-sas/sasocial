import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.less']
})
export class UnauthorizedComponent implements OnInit {

  constructor(private location: Location, private uiService: UiService) {

  }

  ngOnInit() {
    this.uiService.setContentWrapperActive(true);
  }

  voltar() {
    this.location.back();
  }

}
