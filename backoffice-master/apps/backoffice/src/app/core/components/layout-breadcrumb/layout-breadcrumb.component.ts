import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  PRIMARY_OUTLET,
  Router
} from '@angular/router';
import { Breadcrumb } from '@fi-sas/backoffice/core/models/breadcrumb';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'fi-sas-layout-breadcrumb',
  templateUrl: './layout-breadcrumb.component.html',
  styleUrls: ['./layout-breadcrumb.component.less']
})
export class LayoutBreadcrumbComponent implements OnInit {
  static readonly ROUTE_DATA_BREADCRUMB = 'breadcrumb';
  static readonly ROUTE_DATA_TITLE = 'title';

  title = '';
  breadcrumbs: Breadcrumb[] = [];
  data;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private configurationsService: ConfigurationGeralService) {
    this.data = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    // subscribe to the NavigationEnd event
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        this.updateData();
      });

    this.updateData();
  }

  private updateData() {
    const root: ActivatedRoute = this.activatedRoute.root;

    // set title
    this.title = this.getTitle(root);

    // set breadcrumbs
    this.breadcrumbs = this.getBreadcrumbs(root);
  }

  private getBreadcrumbs(
    route: ActivatedRoute,
    url: string = '',
    breadcrumbs: Breadcrumb[] = []
  ): Breadcrumb[] {
    // get the child routes
    const children: ActivatedRoute[] = route.children;

    // return if there are no more children
    if (children.length === 0) {
      return breadcrumbs;
    }

    // iterate over each children
    for (const child of children) {
      //verify primary route
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      // verify the custom data property "breadcrumb" is specified on the route
      if (
        !child.snapshot.data.hasOwnProperty(
          LayoutBreadcrumbComponent.ROUTE_DATA_BREADCRUMB
        )
      ) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      // get the route's URL segment
      const routeURL: string = child.snapshot.url
        .map(segment => segment.path)
        .join('/');

      // append route URL to URL
      url += `/${routeURL}`;

      if (
        child.snapshot.data[LayoutBreadcrumbComponent.ROUTE_DATA_BREADCRUMB] !==
        null
      ) {
        // add breadcrumb
        const breadcrumb: Breadcrumb = {
          label:
            child.snapshot.data[
            LayoutBreadcrumbComponent.ROUTE_DATA_BREADCRUMB
            ],
          params: child.snapshot.params,
          url: url
        };
        breadcrumbs.push(breadcrumb);
        breadcrumbs.forEach(element => {
          switch (element.label) {
            case 'Fila de Espera':
              element.label = this.validTitleTraductions(21);
              break;
            case 'Alimentação':
              element.label = this.validTitleTraductions(2);
              break;
            case 'Medias':
              element.label = this.validTitleTraductions(10);
              break;
            case 'Comunicação':
              element.label = this.validTitleTraductions(19);
              break;
            case 'Mobilidade':
              element.label = this.validTitleTraductions(3);
              break;
            case 'U-Bike':
              element.label = this.validTitleTraductions(13);
              break;
            case 'Alojamento':
              element.label = this.validTitleTraductions(1);
              break;
            case 'Notificações':
              element.label = this.validTitleTraductions(12);
              break;
            case 'Relatórios':
              element.label = this.validTitleTraductions(23);
              break;
            case 'Bolsa de Colaboradores':
              element.label = this.validTitleTraductions(17);
              break;
            case 'Alojamento Privado':
              element.label = this.validTitleTraductions(15);
              break;
            case 'Voluntariado':
              element.label = this.validTitleTraductions(28);
              break;
            case 'Utilizadores':
              element.label = this.validTitleTraductions(8);
              break;
            case 'Finanças':
              element.label = this.validTitleTraductions(11);
              break;
            case 'Infraestrutura':
              element.label = this.validTitleTraductions(9);
              break;
            case 'Configurações':
              element.label = this.validTitleTraductions(6);
              break;
            case 'Desporto':
              element.label = this.validTitleTraductions(24);
              break;
            case 'Agenda':
              element.label = this.validTitleTraductions(18);
              break;
            case 'Gamificação':
              element.label = this.validTitleTraductions(25);
              break;

          }
        });
      }

      // recursive
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }

    // should never get here, but just in case
    return breadcrumbs;
  }

  changeLabel() {

  }
  
  getTitle(root: ActivatedRoute): string {
    // check if is last segment
    if (root.children.length === 0) {
      // check if as title propriety
      if (
        root.snapshot.data.hasOwnProperty(
          LayoutBreadcrumbComponent.ROUTE_DATA_TITLE
        )
      ) {
        return root.snapshot.data[LayoutBreadcrumbComponent.ROUTE_DATA_TITLE];
      } else {
        return '';
      }
    }

    // recursive
    for (const child of root.children) {
      return this.getTitle(child);
    }
  }

  validTitleTraductions(id: number) {
    return this.data.find(t => t.id === id) != null && this.data.find(t => t.id === id) != undefined ? this.data.find(t => t.id === id).translations : '';
  }
}
