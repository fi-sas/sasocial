import { environment } from '@fi-sas/backoffice/../environments/environment';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fi-sas-layout-footer',
  templateUrl: './layout-footer.component.html',
  styleUrls: ['./layout-footer.component.less']
})
export class LayoutFooterComponent implements OnInit {

  version = environment.version;
  date = new Date();

  constructor() {}

  ngOnInit() {}
}
