import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderNotificationsComponent } from './layout-header-notifications.component';

describe('LayoutHeaderNotificationsComponent', () => {
  let component: LayoutHeaderNotificationsComponent;
  let fixture: ComponentFixture<LayoutHeaderNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutHeaderNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
