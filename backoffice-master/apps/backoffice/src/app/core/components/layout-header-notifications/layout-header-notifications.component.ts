import { Component, OnInit } from '@angular/core';
import { NotificationModel } from '@fi-sas/backoffice/modules/notifications/modules/notifications/models/notification.model';
import { finalize, first } from 'rxjs/operators';
import { NotificationsHearderService } from '../../services/notification.service';

@Component({
  selector: 'fi-sas-layout-header-notifications',
  templateUrl: './layout-header-notifications.component.html',
  styleUrls: ['./layout-header-notifications.component.less']
})
export class LayoutHeaderNotificationsComponent implements OnInit {
  visibleNotifications: boolean;
  $notifications = null;
  $notificationsLength = null;
  loading = true;
  notificationSelect: NotificationModel = new NotificationModel();
  total = 0;

  constructor(private notificationsService: NotificationsHearderService) {
    this.notificationSelect = new NotificationModel();
    this.getNotifications();
    this.getNotificationsLength();
    this.notificationsService.total$.subscribe(
      (total) => {
        this.total = total;
      }
    );
  }


  ngOnInit() {

  }
  getNotifications() {
    this.$notifications = null;
    this.$notifications = this.notificationsService
      .getNotificationsObservable();
    this.loading = false;
  }

  getNotificationsLength() {
    this.$notificationsLength = null;
    this.$notificationsLength = this.notificationsService
      .getNotificationsLengthObservable();
  }

  refresh() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.notificationSelect = new NotificationModel();
      this.notificationsService.loadData();
    }, 200);
  }

  clickMe(): void {
    this.visibleNotifications = false;
  }

  change(value: boolean): void {

  }

  showMore() {
    this.notificationsService.loadData(true);
  }

  selectNotification(notification: NotificationModel) {
    this.notificationSelect = notification;
    if (notification.unread == true) {
      this.notificationsService.updateReadNotification(notification.id).pipe(first()).subscribe(() => {
        notification.unread = false;
        this.notificationsService.decrementTotal();
      });
    }
  }

  changeIcon(service_id: number): string {
    switch (service_id) {
      case 1: {
        return 'home';
      }
      case 2: {
        return 'coffee';
      }
      case 3: {
        return 'car';
      }
      case 4: {
        return 'wallet';
      }
      case 6: {
        return 'setting';
      }
      case 7: {
        return 'heart';
      }
      case 8: {
        return 'user';
      }
      case 9: {
        return 'bank';
      }
      case 10: {
        return 'picture';
      }
      case 11: {
        return 'wallet';
      }
      case 12: {
        return 'message';
      }
      case 13: {
        return 'bold';
      }
      case 15: {
        return 'environment';
      }
      case 17: {
        return 'team';
      }
      case 18: {
        return 'schedule';
      }
      case 19: {
        return 'mail';
      }
      case 20: {
        return 'tool';
      }
      case 21: {
        return 'ordered-list';
      }
      case 23: {
        return 'file-text';
      }
      case 24: {
        return 'dribbble';
      }
      case 25: {
        return 'trophy';
      }
      case 26: {
        return 'dashboard';
      }
      case 28: {
        return 'team';
      }
      default: {
        return 'file-text';
      }
    }
  }

  delete(id) {
    this.notificationsService.delete(id).pipe(first()).subscribe((data) => {
      this.refresh();
    })
  }
}
