import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderReportsComponent } from './layout-header-reports.component';

describe('LayoutHeaderReportsComponent', () => {
  let component: LayoutHeaderReportsComponent;
  let fixture: ComponentFixture<LayoutHeaderReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutHeaderReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
