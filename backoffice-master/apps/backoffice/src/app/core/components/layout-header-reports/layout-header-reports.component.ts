import { Component, OnInit } from "@angular/core";
import { ReportModel } from "@fi-sas/backoffice/modules/reports/models/report.model";
import { first } from "rxjs/operators";
import { ReportsHeaderService } from "../../services/reports.service";

@Component({
  selector: 'fi-sas-layout-header-reports',
  templateUrl: './layout-header-reports.component.html',
  styleUrls: ['./layout-header-reports.component.less']
})

export class LayoutReportsComponent implements OnInit {

  visibleReports: boolean;
  $reports = null;
  $reportsLength = null;
  loading = true;
  total = 0;

  constructor(
    private reportsHeaderService: ReportsHeaderService
    ) {
    this.getReports();
    this.getReportsLength();
    this.reportsHeaderService.total$.subscribe(
      (total) => {
        this.total = total;
      }
    );
  }

  ngOnInit() {

  }

  getReports() {
    this.$reports = null;
    this.$reports = this.reportsHeaderService
      .getReportsObservable();
    this.loading = false;
  }

  getReportsLength() {
    this.$reportsLength = null;
    this.$reportsLength = this.reportsHeaderService
      .getReportsLengthObservable();
  }

  refresh() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.reportsHeaderService.loadData();
    }, 200);
  }

  clickMe(): void {
    this.visibleReports = false;
  }

  change(value: boolean): void {

  }

  showMore() {
    this.reportsHeaderService.loadData(true);
  }

  download(report: ReportModel) {
    if (report.unread == true) {
      this.reportsHeaderService.updateReadReport(report.id).pipe(first()).subscribe(() => {
        report.unread = false;
        this.reportsHeaderService.decrementTotal();
      });
    }
    window.open(report.file.url);
  }

  delete(id) {
    this.reportsHeaderService.delete(id).pipe(first()).subscribe((data)=> {
        this.refresh();
    })
}
}
