import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderUserComponent } from './layout-header-user.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('LayoutHeaderUserComponent', () => {
  let component: LayoutHeaderUserComponent;
  let fixture: ComponentFixture<LayoutHeaderUserComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, RouterTestingModule],
        providers: [FiResourceService, FiUrlService],
        declarations: [LayoutHeaderUserComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
