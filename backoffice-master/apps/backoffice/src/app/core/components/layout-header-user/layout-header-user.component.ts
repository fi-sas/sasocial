import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { Router } from '@angular/router';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

@Component({
  selector: 'fi-sas-layout-header-user',
  templateUrl: './layout-header-user.component.html',
  styleUrls: ['./layout-header-user.component.less']
})
export class LayoutHeaderUserComponent implements OnInit {
  userModel: UserModel;
  nameUser: string;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authService.getUserObservable().subscribe(user => {
      this.userModel = user;
    });

    this.userModel = this.authService.getUser();

    if(this.userModel) {
      this.nameUser = this.userModel.name.split(' ')[0];
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['auth', 'login']);
  }
}
