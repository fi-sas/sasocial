import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderComponent } from './layout-header.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { LayoutHeaderUserComponent } from '@fi-sas/backoffice/core/components/layout-header-user/layout-header-user.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('LayoutHeaderComponent', () => {
  let component: LayoutHeaderComponent;
  let fixture: ComponentFixture<LayoutHeaderComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SharedModule, RouterTestingModule],
        providers: [FiResourceService, FiUrlService],
        declarations: [LayoutHeaderComponent, LayoutHeaderUserComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
