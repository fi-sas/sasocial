import { environment } from './../../../../environments/environment';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.less']
})
export class LayoutHeaderComponent implements OnInit, OnDestroy {
  isSiderCollapsed = false;
  isSiderActive = false;
  institute = '';
  constructor(private uiService: UiService) {
    this.institute = environment.institute;
  }

  ngOnInit() {
    this.uiService.getSiderCollapsedObservable().subscribe(value => {
      this.isSiderCollapsed = value;
    });

    this.uiService.getSiderActiveObservable().subscribe(value => {
      this.isSiderActive = value;
    });
  }

  ngOnDestroy() {}

  siderButtonClick() {
    this.uiService.toggleSiderCollapsed();
  }
}
