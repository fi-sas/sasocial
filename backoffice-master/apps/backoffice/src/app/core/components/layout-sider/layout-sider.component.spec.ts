import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutSiderComponent } from './layout-sider.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';

describe('LayoutSiderComponent', () => {
  let component: LayoutSiderComponent;
  let fixture: ComponentFixture<LayoutSiderComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, NgZorroAntdModule],
        declarations: [LayoutSiderComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutSiderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
