import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-layout-sider',
  templateUrl: './layout-sider.component.html',
  styleUrls: ['./layout-sider.component.less']
})
export class LayoutSiderComponent implements OnInit, OnDestroy {
  isCollapsed = false;
  siderItems: SiderItem[] = [];

  icon = null;
  title = null;

  subscriptions: Subscription[] = [];

  constructor(private uiService: UiService) {}

  ngOnInit() {
    this.subscriptions.push(this.uiService.getSiderCollapsedObservable().subscribe(value => {
      this.isCollapsed = value;
    }));

    this.subscriptions.push(this.uiService.getSiderItemsObservable().subscribe(value => {
      this.siderItems = value;
    }));

    this.subscriptions.push(this.uiService.getSiderTitleObservable().subscribe(value => {
      this.title = value.title;
      this.icon = value.icon;
    }));
  }

  ngOnDestroy() {
    this.siderItems.map( items => items.destroy())
    this.subscriptions.map(f => {
      f.unsubscribe();
    });
  }

  closeSider() {
    // this.uiService.setSiderCollapsed(true);
  }
}
