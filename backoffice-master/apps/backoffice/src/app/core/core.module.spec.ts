import { CoreModule } from './core.module';
import { async, TestBed } from '@angular/core/testing';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

describe('CoreModule', () => {
  beforeEach(() => {
    async(() => {
      TestBed.configureTestingModule({
        imports: [CommonModule, SharedModule, CoreModule],
        providers: [UiService]
      }).compileComponents();
    });
  });

  it('should create an instance', () => {
    expect(CoreModule).toBeDefined();
  });
});
