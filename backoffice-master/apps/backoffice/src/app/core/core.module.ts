import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
  APP_BOOTSTRAP_LISTENER
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';
import { LayoutHeaderComponent } from './components/layout-header/layout-header.component';
import { LayoutHeaderUserComponent } from './components/layout-header-user/layout-header-user.component';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LayoutSiderComponent } from './components/layout-sider/layout-sider.component';
import { LayoutFooterComponent } from './components/layout-footer/layout-footer.component';
import { LayoutBreadcrumbComponent } from './components/layout-breadcrumb/layout-breadcrumb.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AccessibleSiderItemPipe } from './pipes/accessible-sider-item.pipe';
import { CompleteLayoutComponent } from './layouts/complete-layout/complete-layout.component';
import { LayoutHeaderNotificationsComponent } from './components/layout-header-notifications/layout-header-notifications.component';
import { ReportsService } from '../modules/reports/services/reports.service';
import { NotificationsService } from '../modules/notifications/modules/notifications/services/notifications.service';
import { LayoutReportsComponent } from './components/layout-header-reports/layout-header-reports.component';
import { ReportsHeaderService } from './services/reports.service';
import { NotificationsHearderService } from './services/notification.service';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [
    FullLayoutComponent,
    LayoutHeaderComponent,
    LayoutHeaderUserComponent,
    LayoutSiderComponent,
    LayoutFooterComponent,
    LayoutBreadcrumbComponent,
    AuthLayoutComponent,
    AccessibleSiderItemPipe,
    CompleteLayoutComponent,
    LayoutHeaderNotificationsComponent,
    LayoutReportsComponent
  ],
  providers: [
    UiService,
    // FOR HEADER COMPONENTS
    ReportsService,
    ReportsHeaderService,
    NotificationsHearderService,
    NotificationsService,
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('Core Module is already loaded use only on Root Module');
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [UiService]
    };
  }
}
