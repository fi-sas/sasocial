import { Component, OnInit } from '@angular/core';
import { environment } from 'apps/backoffice/src/environments/environment';

@Component({
  selector: 'fi-sas-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.less']
})
export class AuthLayoutComponent implements OnInit {

  institute = environment.institute;

  constructor() {}

  ngOnInit() {}
}
