import { AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fi-sas-complete-layout',
  templateUrl: './complete-layout.component.html',
  styleUrls: ['./complete-layout.component.less']
})
export class CompleteLayoutComponent implements OnInit, OnDestroy, AfterViewChecked {

  isSiderCollapsed = false;
  isSiderActive = false;
  contentWrapperActive = true;
  isBreadcrumbVisible = true;

  subscriptions: Subscription[] = [];
  
  constructor(private uiService: UiService, private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.subscriptions.push(this.uiService.getSiderCollapsedObservable().subscribe(value => {
      this.isSiderCollapsed = value;
    }));

    this.subscriptions.push(this.uiService.getSiderActiveObservable().subscribe(value => {
      this.isSiderActive = value;
    }));

    this.subscriptions.push(this.uiService.getContentWrapperActiveObservable().subscribe(value => {
      this.contentWrapperActive = value;
    }));

    this.subscriptions.push(this.uiService.getBreadcrumbVisible().subscribe(value => {
      this.isBreadcrumbVisible = value;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.map(s => {
      s.unsubscribe();
    });
  }

  ngAfterViewChecked() {
    // FIX THE ERROR FOR VALUE CHANGED AFTER VIEW
    this.cdRef.detectChanges();
  }
}
