import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullLayoutComponent } from './full-layout.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { LayoutHeaderComponent } from '@fi-sas/backoffice/core/components/layout-header/layout-header.component';
import { LayoutSiderComponent } from '@fi-sas/backoffice/core/components/layout-sider/layout-sider.component';
import { LayoutBreadcrumbComponent } from '@fi-sas/backoffice/core/components/layout-breadcrumb/layout-breadcrumb.component';
import { LayoutFooterComponent } from '@fi-sas/backoffice/core/components/layout-footer/layout-footer.component';
import { LayoutHeaderUserComponent } from '@fi-sas/backoffice/core/components/layout-header-user/layout-header-user.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

describe('FullLayoutComponent', () => {
  let component: FullLayoutComponent;
  let fixture: ComponentFixture<FullLayoutComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, SharedModule],
        providers: [FiResourceService, FiUrlService],
        declarations: [
          FullLayoutComponent,
          LayoutHeaderComponent,
          LayoutHeaderUserComponent,
          LayoutSiderComponent,
          LayoutBreadcrumbComponent,
          LayoutFooterComponent
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FullLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
