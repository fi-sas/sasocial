import { Subscription } from 'rxjs';
import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'fi-sas-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.less']
})
export class FullLayoutComponent
  implements OnInit, OnDestroy, AfterViewChecked {

  @ViewChild(PerfectScrollbarComponent, { static: false }) scroll?: PerfectScrollbarComponent;

  isSiderCollapsed = false;
  isSiderActive = false;
  contentWrapperActive = true;
  isBreadcrumbVisible = true;

  subscriptions: Subscription[] = [];

  constructor(private uiService: UiService, private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.subscriptions.push(this.uiService.getSiderCollapsedObservable().subscribe(value => {
      this.isSiderCollapsed = value;
    }));

    this.subscriptions.push(this.uiService.getSiderActiveObservable().subscribe(value => {
      this.isSiderActive = value;
    }));

    this.subscriptions.push(this.uiService.getContentWrapperActiveObservable().subscribe(value => {
      this.contentWrapperActive = value;
    }));

    this.subscriptions.push(this.uiService.getBreadcrumbVisible().subscribe(value => {
      this.isBreadcrumbVisible = value;
    }));

    this.subscriptions.push(this.uiService.scrollToTopObservable().subscribe(signal => {
      if(this.scroll)
      this.scroll.directiveRef.scrollToTop();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.map(s => {
      s.unsubscribe();
    });
  }

  ngAfterViewChecked() {
    // FIX THE ERROR FOR VALUE CHANGED AFTER VIEW
    this.cdRef.detectChanges();
  }

}
