export interface SiderItemBadge {
  value: number | string;
  color: string;
}
