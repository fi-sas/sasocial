import { Observable, Subscription } from 'rxjs';
import { SiderItemBadge } from "./sider-item-badge";

export class SiderItem {
  name: string;
  icon = null;
  link?: string;
  permission?: string;
  hasPermission: boolean;
  childs: SiderItem[] = [];
  parent: SiderItem;
  badgeObservable: Observable<SiderItemBadge> = null;
  $badgeSubs: Subscription = null;
  badge : SiderItemBadge = null;

  constructor(
    name: string,
    icon?: string,
    link?: string,
    permission?: string,
    childs?: SiderItem[],
    badgeObservable?: Observable<SiderItemBadge>
  ) {
    this.name = name;

    if (icon) this.icon = icon;

    if (link) this.link = link;

    if (permission) this.permission = permission;

    if (childs) {
      childs.map(child => {
        this.addChild(child);
      });
    }

    if(badgeObservable) {
      this.badgeObservable = badgeObservable;

      this.$badgeSubs = this.badgeObservable.subscribe(badge => this.badge = badge);
    }
  }


  destroy() {
    if (this.$badgeSubs) {
    this.$badgeSubs.unsubscribe();
    }

    if(this.childs) {
      this.childs.map(child => child.destroy());
    }
  }

  /***
   * Return if the item has childs
   * @returns {boolean}
   */
  isSubMenu(): boolean {
    return this.childs.length > 0 ? true : false;
  }

  /***
   * Adds a new child to the item
   * @param {SiderItem} item
   * @returns {SiderItem}
   */
  addChild(item: SiderItem): SiderItem {
    item.parent = this;
    this.childs.push(item);
    return item;
  }

  /***
   * Remove the child of item
   * @param {SiderItem} item
   */
  removeChild(item: SiderItem) {
    const index = this.childs.indexOf(item, 0);
    if (index > -1) {
      this.childs.slice(index, 1);
    }
  }

  /***
   * Change the hasAccess value
   * @param access
   */
  setHasPermission(access: boolean) {
    this.hasPermission = access;
  }

}

