import { Pipe, PipeTransform } from '@angular/core';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';

@Pipe({
  name: 'accessibleSiderItem'
})
export class AccessibleSiderItemPipe implements PipeTransform {

  transform(items: SiderItem[], args?: any): any {
    const result: SiderItem[] = [];

    for(const item of items) {
      if (item.hasPermission || !item.permission ) result.push(item);
    }

    return result;
  }

}
