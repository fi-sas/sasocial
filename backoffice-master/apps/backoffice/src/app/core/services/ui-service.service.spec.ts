import { TestBed, inject } from '@angular/core/testing';
import { UiService } from './ui-service.service';
import { OverlayModule } from '@angular/cdk/overlay';
import {
  NgZorroAntdModule,
  NzMessageComponent,
  NzMessageContainerComponent
} from 'ng-zorro-antd';

describe('UiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, NgZorroAntdModule],
      providers: [UiService],
      declarations: []
    });
  });

  it(
    'should be created',
    inject([UiService], (service: UiService) => {
      expect(service).toBeTruthy();
    })
  );
});
