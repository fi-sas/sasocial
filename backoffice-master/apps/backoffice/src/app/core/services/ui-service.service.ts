import { Injectable, TemplateRef, Type } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import {
  NzMessageConfig,
  NzMessageDataFilled,
  NzMessageService,
  NzModalService
} from 'ng-zorro-antd';
import { NavigationExtras, Router, ChildActivationStart } from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

export enum MessageType {
  success,
  info,
  warning,
  error,
  loading
}

@Injectable({
  providedIn: 'root'
})
export class UiService {
  isBreadcrumbVisible = true;
  isBreadcrumbVisibleSubject: BehaviorSubject<boolean> = new BehaviorSubject(
    this.isBreadcrumbVisible
  );

  isContentWrapperActive = true;
  isContentWrapperActiveSubject: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(this.isContentWrapperActive);

  isSiderActive = false;
  isSiderActiveSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    this.isSiderActive
  );

  isSiderCollapsed = false;
  isSiderCollapsedSubject: BehaviorSubject<boolean> = new BehaviorSubject(
    this.isSiderCollapsed
  );

  siderItems: SiderItem[] = [];
  siderItemsSubject: BehaviorSubject<SiderItem[]> = new BehaviorSubject<
    SiderItem[]
  >(this.siderItems);

  siderTitle: string = '';
  siderIcon: string = '';
  siderTitleSubject: BehaviorSubject<{ title: string; icon: string;}> = new BehaviorSubject<{ title: string; icon: string;}>({
    title: null,
    icon: null
  });

  scrollToTopSignal: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  /***
   * Create a instance of the service
   *
   */
  constructor(
    private messageService: NzMessageService,
    private modalService: NzModalService,
    private router: Router,
    private authService: AuthService
  ) { }

  /***
   * Change the flag for activate the content wrapper on the template
   * @param {boolean} active
   * @returns {boolean}
   */
  setContentWrapperActive(active: boolean = true): boolean {
    this.isContentWrapperActive = active;
    this.isContentWrapperActiveSubject.next(this.isContentWrapperActive);
    return this.isContentWrapperActive;
  }

  /***
   * Returns the flag of the state of content wrapper of the template
   * @returns {boolean}
   */
  getContentWrapperActive(): boolean {
    return this.isContentWrapperActive;
  }

  /***
   * Returns the observable of the content wrapper state
   * @returns {Observable<boolean>}
   */
  getContentWrapperActiveObservable(): Observable<boolean> {
    return this.isContentWrapperActiveSubject.asObservable();
  }

  /***
   * Change the flag for activate the side nav menu
   * @param {boolean} active
   * @returns {boolean}
   */
  setSiderActive(active: boolean = true): boolean {
    // collpase the menu first
    this.isSiderActive = active;
    this.setSiderCollapsed(false);
    this.isSiderActiveSubject.next(this.isSiderActive);
    return this.isSiderActive;
  }

  /***
   * Returns the flag if the side nav menu is active
   * @returns {boolean}
   */
  getSiderActive(): boolean {
    return this.isSiderActive;
  }

  /***
   * Returns the observable of the side nav menu active status
   * @returns {Observable<boolean>}
   */
  getSiderActiveObservable(): Observable<boolean> {
    return this.isSiderActiveSubject.asObservable();
  }

  /***
   * Set the flag for sider menu status, collapsed (true) or expanded (false)
   * @param {boolean} collapsed
   * @returns {boolean}
   */
  setSiderCollapsed(collapsed = true): boolean {
    this.isSiderCollapsed = collapsed;
    this.isSiderCollapsedSubject.next(this.isSiderCollapsed);
    return this.isSiderCollapsed;
  }

  /***
   * Toggles this sider menu collapse or expended state
   * @returns {boolean}
   */
  toggleSiderCollapsed(): boolean {
    this.isSiderCollapsed = !this.isSiderCollapsed;
    this.isSiderCollapsedSubject.next(this.isSiderCollapsed);
    return this.isSiderCollapsed;
  }

  /***
   * Returns the value of the sider menu status for collapsed (true) or expanded (false)
   * @returns {boolean}
   */
  getSiderCollapsed(): boolean {
    return this.isSiderCollapsed;
  }

  /***
   * Return the observable of the slider menu status collapsed (true) or expanded (false)
   * @returns {Observable<boolean>}
   */
  getSiderCollapsedObservable(): Observable<boolean> {
    return this.isSiderCollapsedSubject.asObservable();
  }

  /***
   * Adds a array of items de de side nav menu
   * @param {SiderItem[]} items
   * @returns {SiderItem[]}
   */
  setSiderItems(items: SiderItem[]): SiderItem[] {

    this.siderItems.map(item => item.destroy());

    items.map(item => {
      item.setHasPermission(this.authService.hasPermission(item.permission));
      item.childs.map(subItem => {
        subItem.setHasPermission(this.authService.hasPermission(subItem.permission))
        subItem.childs.map(subSubItem => {
          subSubItem.setHasPermission(this.authService.hasPermission(subSubItem.permission))
        });
      });
    });

    this.siderItems = items;
    this.siderItemsSubject.next(this.siderItems);
    return this.siderItems;
  }

  /***
   * Add a item to the side menu
   * @param {SiderItem} item
   * @returns {SiderItem[]}
   */
  addSiderItem(item: SiderItem): SiderItem[] {

    item.setHasPermission(item.permission ? this.authService.hasPermission(item.permission) : true );
    item.childs.map(subItem => {
      subItem.setHasPermission(subItem.permission ? this.authService.hasPermission(subItem.permission) : true );
      subItem.childs.map(subSubItem => {
        subSubItem.setHasPermission(subSubItem.permission ? this.authService.hasPermission(subSubItem.permission) :  true )
      });
    });

    this.siderItems.push(item);
    this.siderItemsSubject.next(this.siderItems);
    return this.siderItems;
  }

  /***
   * Remove all the items from the side nav menu
   * @returns {SiderItem[]}
   */
  removeSiderItems(): SiderItem[] {
    this.siderItems.map(item => item.destroy());
    this.siderItems = [];
    this.siderItemsSubject.next(this.siderItems);
    return this.siderItems;
  }

  /***
   * Remove the item send by parameter from the side nav menu
   * @param {SiderItem} item
   * @returns {SiderItem[]}
   */
  removeSiderItem(item: SiderItem): SiderItem[] {
    const index = this.siderItems.indexOf(item, 0);

    if (index > -1) {
      this.siderItems[index].destroy();
      this.siderItems.splice(index, 1);
    }
    this.siderItemsSubject.next(this.siderItems);
    return this.siderItems;
  }

  /***
   * Retunrs de the items on the side nav menu
   * @returns {SiderItem[]}
   */
  getSiderItems(): SiderItem[] {
    return this.siderItems;
  }

  /***
   * Returns the observable of the side nav menu items
   * @returns {Observable<SiderItem[]>}
   */
  getSiderItemsObservable(): Observable<SiderItem[]> {
    return this.siderItemsSubject.asObservable();
  }

  getSiderTitleObservable(): Observable<{ title: string; icon: string;}> {
    return this.siderTitleSubject.asObservable();
  }

  /***
   * Set the sider top tile and icon
   */
  setSiderTitle(title: string, icon: string): { title: string; icon: string;} {
    this.siderTitle = title;
    this.siderIcon = icon;
    this.siderTitleSubject.next({
      title: this.siderTitle,
      icon: this.siderIcon
    });

    return {
      title: this.siderTitle,
      icon: this.siderIcon
    };
  }

  /***
   * Clears the sider top tile and icon
   */
  clearSiderTitle() {
    this.setSiderTitle(null, null);
  }

  /***
   * Show a message on top of screen
   * @param type
   * @param message
   * @param config
   */
  showMessage(
    type: MessageType,
    message: string,
    config?: NzMessageConfig
  ): NzMessageDataFilled {
    switch (type) {
      case MessageType.success:
        return this.messageService.success(message);
      case MessageType.info:
        return this.messageService.info(message);
      case MessageType.warning:
        return this.messageService.warning(message);
      case MessageType.error:
        return this.messageService.error(message);
      case MessageType.loading:
        return this.messageService.loading(message);
    }
  }

  /***
   * Remove a message from the stack
   * @param message
   */
  removeMessage(message: NzMessageDataFilled) {
    this.messageService.remove(message.messageId);
  }

  /***
   * Show a modal windows for confirmation, returns a promise of true if OkButton is pressed or false if cancelButton is pressed
   * @param title
   * @param content
   * @param okText
   * @param cancelText
   * @param okType
   */
  showConfirm(
    title: string,
    content: string | TemplateRef<{}>,
    okText: string = 'Ok',
    cancelText: string = 'Cancelar',
    okType: string = 'danger'
  ): Observable<boolean> {
    return Observable.create(observer => {
      this.modalService.confirm({
        nzTitle: title,
        nzContent: content,
        nzOkText: okText,
        nzOkType: okType,
        nzOnOk: () => {
          observer.next(true);
          observer.complete();
        },
        nzCancelText: cancelText,
        nzOnCancel: () => {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }

  /***
   * Is used to the return button on modals shows a confirmation modal if true redirect the user to the path
   * @param commands
   * @param extras
   * @param title
   * @param content
   */
  showReturnModal(
    commands: string[],
    extras?: NavigationExtras,
    title?: string,
    content?: string
  ): Observable<boolean> {
    return Observable.create(observer => {
      this.modalService.confirm({
        nzTitle: title ? title : 'Cancelar',
        nzContent: content ? content : 'Tem a certeza que pretende cancelar?',
        nzOkText: 'Sim',
        nzOkType: 'danger',
        nzOnOk: () => {
          this.router.navigate(commands, extras);
          observer.next(true);
          observer.complete();
        },
        nzCancelText: 'Não',
        nzOnCancel: () => {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }

  /***
   * Is used to set the breadcrumb visible or invisible
   * @param {boolean} visible
   * @returns {boolean}
   */
  setBreadcrumbVisible(visible: boolean) {
    this.isBreadcrumbVisible = visible;
    this.isBreadcrumbVisibleSubject.next(this.isBreadcrumbVisible);
    return this.isBreadcrumbVisible;
  }

   /***
   * Returns the observable of the side breadcrumb visible
   * @returns {Observable<boolean>}
   */
  getBreadcrumbVisible(): Observable<boolean> {
    return this.isBreadcrumbVisibleSubject.asObservable();
  }

  /**
   * Returns Observable scroll to top signal
   */
  scrollToTopObservable(): Observable<boolean> {
    return this.scrollToTopSignal.asObservable();
  }

  /**
   * Scroll content to top
   */
  scrollToTop() {
    this.scrollToTopSignal.next(true);
  }
}
