import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { AccommodationComponent } from '@fi-sas/backoffice/modules/accommodation/accommodation.component';
import { ResidenceSelectionComponent } from './pages/residence-selection/residence-selection.component';
import { ResidenceGuard } from './guards/residence.guard';
import { GeralSettingsComponent } from './pages/geral-settings/geral-settings.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { ConsultApplicationComponent } from './pages/consult-application/consult-application.component';

const routes: Routes = [
  {
    path: '',
    component: AccommodationComponent,
    children: [
      {
        path: 'residence-selection',
        component: ResidenceSelectionComponent,
        data: { breadcrumb: 'Selecionar Residência', title: 'Selecionar Residência' }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { breadcrumb: 'Painel', title: 'Painel', scope: 'accommodation:applications:read' },
      },
      {
        path: 'applications',
        loadChildren: '../accommodation/modules/aco_applications/applications.module#ApplicationsModule',
        data: { breadcrumb: 'Candidaturas', title: 'Candidaturas' },
      },
      {
        path: 'rooms',
        loadChildren: './modules/rooms/rooms.module#RoomsModule',
        data: { breadcrumb: 'Quartos', title: 'Quartos' },
        canActivate: [ResidenceGuard]
      },
      {
        path: 'services',
        loadChildren: './modules/services/services.module#ServicesModule',
        data: { breadcrumb: 'Serviços', title: 'Serviços' },
      },
      {
        path: 'period',
        loadChildren: './modules/period/period.module#PeriodModule',
        data: { breadcrumb: 'Período Alojamento', title: 'Período Alojamento' },
      },
      {
        path: 'movable-heritage',
        loadChildren: './modules/movable-heritage/movable-heritage.module#MovableHeritageModule',
        data: { breadcrumb: 'Património Mobiliário', title: 'Património Mobiliário' },
      },
      {
        path: 'typologies',
        loadChildren: './modules/typologies/typologies.module#TypologiesModule',
        data: { breadcrumb: 'Tipologias', title: 'Tipologias' },
      },
      {
        path: 'regimes',
        loadChildren: './modules/regimes/regimes.module#RegimesModule',
        data: { breadcrumb: 'Regimes', title: 'Regimes' },
      },
      {
        path: 'phases',
        loadChildren: './modules/phases/phases.module#PhasesModule',
        data: { breadcrumb: 'Fases candidaturas', title: 'Fases candidaturas' }
      },
      {
        path: 'residences',
        loadChildren: './modules/residences/residences.module#ResidencesModule',
        data: { breadcrumb: 'Residências', title: 'Residências' },
      },
      {
        path: 'extras',
        loadChildren: './modules/extras/extras.module#ExtrasModule',
        data: { breadcrumb: 'Extras', title: 'Extras' },
      },
      {
        path: 'withdrawals',
        loadChildren: './modules/withdrawals/withdrawals.module#WithdrawalsModule',
        data: { breadcrumb: 'Desistências', title: 'Desistências' },
      },
      {
        path: 'extensions',
        loadChildren: './modules/extensions/extensions.module#ExtensionsModule',
        data: { breadcrumb: 'Extensões', title: 'Extensões' },
      },
      {
        path: 'absences',
        loadChildren: './modules/absences/absences.module#AbsencesModule',
        data: { breadcrumb: 'Ausências', title: 'Ausências' },
      },
      {
        path: 'billing',
        loadChildren: './modules/billing/billing.module#BillingModule',
        data: { breadcrumb: 'Faturação', title: 'Faturação' },
      },
      {
        path: 'extras-change',
        loadChildren: './modules/extra-change-requests/extra-change-requests.module#ExtraChangeRequestsModule',
        data: { breadcrumb: 'Alteração de extras', title: 'Alteração de extras' },
      },
      {
        path: 'regime-change',
        loadChildren: './modules/regime-change-requests/regime-change-requests.module#RegimeChangeRequestsModule',
        data: { breadcrumb: 'Alteração de regime', title: 'Alteração de regime' },
      },
      {
        path: 'typology-change',
        loadChildren: './modules/typology-change-request/typology-change-request.module#TypologyChangeRequestModule',
        data: { breadcrumb: 'Alteração de tipologia', title: 'Alteração de tipologia' },
      },
      {
        path: 'residence-change',
        loadChildren: './modules/residence-change-requests/residence-change-requests.module#ResidenceChangeRequestsModule',
        data: { breadcrumb: 'Alteração de residência', title: 'Alteração de residência' },
      },
      {
        path: 'permute-room',
        loadChildren: './modules/permute-room-requests/permute-room-requests.module#PermuteRoomRequestsModule',
        data: { breadcrumb: 'Permuta de quarto', title: 'Permuta de quarto' },
      },
      {
        path: 'maintenance-requests',
        loadChildren: './modules/maintenance-requests/maintenance-requests.module#MaintenanceRequestsModule',
        data: { breadcrumb: 'Pedidos de manutenção', title: 'Pedidos de manutenção' },
      },
      {
        path: 'tariff-change',
        loadChildren: './modules/tariff-change-requests/tariff-change-requests.module#TariffChangeRequestsModule',
        data: { breadcrumb: 'Alteração de tarifário', title: 'Alteração de tarifário' },
      },
      {
        path: 'tariffs',
        loadChildren: './modules/tariffs/tariffs.module#TariffsModule',
        data: { breadcrumb: 'Tarifários', title: 'Tarifários' },
      },
      {
        path: 'occurrences',
        loadChildren: './modules/occurrences/occurrences.module#OccurrencesModule',
        data: { breadcrumb: 'Ocorrências', title: 'Ocorrências' },
      },
      {
        path: 'application-entry-communications',
        loadChildren: './modules/application-entry-communications/application-entry-communications.module#ApplicationEntryCommunicationsModule',
        data: { breadcrumb: 'Comunicação de entrada', title: 'Comunicação de entrada' },
      },
      {
        path: 'application-exit-communications',
        loadChildren: './modules/application-exit-communications/application-exit-communications.module#ApplicationExitCommunicationsModule',
        data: { breadcrumb: 'Comunicação de saída', title: 'Comunicação de saída' },
      },
      {
        path: 'room-changes',
        loadChildren: './modules/room-change-requests/room-change-requests.module#RoomChangeRequestsModule',
        data: { breadcrumb: 'Alteração de quarto', title: 'Alteração de quarto' },
      },
      {
        path: 'iban-changes',
        loadChildren: './modules/iban-change-requests/iban-change-requests.module#IbanChangeRequestsModule',
        data: { breadcrumb: 'Alteração de IBAN', title: 'Alteração de IBAN' },
      },
      {
        path: 'period-change',
        loadChildren: './modules/period-change-requests/period-change-requests.module#PeriodChangeRequestsModule',
        data: { breadcrumb: 'Alteração de Período', title: 'Alteração de Período' }
      },
      {
        path: 'geralsetting',
        component: GeralSettingsComponent,
        data: { breadcrumb: 'Configurações Gerais', title: 'Configurações Gerais', scope: 'accommodation:configurations:read' },
      },
      {
        path: 'consult-application',
        component: ConsultApplicationComponent,
        data: { breadcrumb: 'Consulta de candidaturas', title: 'Consulta de candidaturas', scope: 'accommodation:applications:resume-list' },
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccommodationRoutingModule { }
