import { SiderItemBadge } from './../../core/models/sider-item-badge';

import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { BehaviorSubject } from 'rxjs';
import { AccommodationService } from './services/accommodation.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-accommodation',
  template: '<router-outlet></router-outlet>',
})
export class AccommodationComponent implements OnInit {
  dataConfiguration: any;
  totalWithdrawal = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalAbsences = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalCommEntry = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalCommExit = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalExtraChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalIbanChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalRegimeChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalRoomPerm = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalRoomRes = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalRoomChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalTypolChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalTarifChange = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalExtension = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalMaintenance = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });
  totalPeriodChanges = new BehaviorSubject<SiderItemBadge>({ value: 0, color: '' });

  constructor(
    private uiService: UiService,
    private accommodationService: AccommodationService,
    private authservice: AuthService,
    private configurationsService: ConfigurationGeralService
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(1), 'home');


    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const resiChoose = new SiderItem('Escolher residência', '', '/accommodation/residence-selection');
    this.uiService.addSiderItem(resiChoose);

    // INITIALIZE GLOBAL FILTERS
    const data = this.accommodationService.getAccommodationData();
    if (data) {

      let name = '';

      if (data.academicYear) {
        name += '[' + data.academicYear + '] ';
      }

      if (data.residence) {
        name += '[' + data.residence.name + '] ';
      }

      if (data.applicationPhase) {
        name += '[ Fase ' + data.applicationPhase.application_phase + ']';
      }

      if (!data.academicYear && !data.residence) {
        name = 'Escolher Residência';
      }

      resiChoose.name = name;
      this.accommodationService.updateApplicationsStats();
      this.accommodationService.updateContractChangeStats();
    }

    this.accommodationService.selectedAccommodationDataObservable().subscribe(data => {
      let name = '';

      if (data.academicYear) {
        name += '[' + data.academicYear + '] ';
      }

      if (data.residence) {
        name += '[' + data.residence.name + '] ';
      }

      if (data.applicationPhase) {
        name += '[ Fase ' + data.applicationPhase.application_phase + ']';
      }

      if (!data.academicYear && !data.residence) {
        name = 'Escolher residência';
      }

      resiChoose.name = name;
    });

    this.accommodationService.contractChangeStatsObservable().subscribe(stats => {
      this.updateBadges(stats);
    });
    // END GLOBAL FILTERS
    this.uiService.setSiderActive(true);
    const dashboard = new SiderItem('Painel', '', '/accommodation/dashboard', 'accommodation:applications:read');
    this.uiService.addSiderItem(dashboard);

    const applications = new SiderItem('Candidaturas',null,null, 'accommodation:applications');
    applications.addChild(new SiderItem('Relatórios', '', '/accommodation/applications/reports', 'accommodation:applications:reports'));
    applications.addChild(new SiderItem('Nova', '', '/accommodation/applications/form-application', 'accommodation:applications:create'));
    applications.addChild(new SiderItem('Listar', '', '/accommodation/applications/all-applications', 'accommodation:applications:all-applications', null, null));

    applications.addChild(new SiderItem('Consulta de candidaturas', '', '/accommodation/consult-application', 'accommodation:applications:resume-list', null, null));

    this.uiService.addSiderItem(applications);

    /*const consultApplication = new SiderItem('Consulta de candidaturas', '', '/accommodation/consult-application','accommodation:applications:resume-list');
    this.uiService.addSiderItem(consultApplication);*/


    if (this.authservice.hasPermission('accommodation:billings:read')) {
      const billings = new SiderItem('Processamento');
      billings.addChild(new SiderItem('Listagem', '', '/accommodation/billing/list-billings', 'accommodation:billings:read'));
      this.uiService.addSiderItem(billings);
    }

    if (this.authservice.hasPermission('accommodation:occupation-map') || this.authservice.hasPermission('accommodation:rooms:create') || this.authservice.hasPermission('accommodation:rooms:read')) {
      const rooms = new SiderItem('Quartos');
      rooms.addChild(new SiderItem('Mapa de Ocupações', '', '/accommodation/rooms/occupation-map', 'accommodation:occupation-map'));
      rooms.addChild(new SiderItem('Novo', '', '/accommodation/rooms/create', 'accommodation:rooms:create'));
      rooms.addChild(new SiderItem('Listar', '', '/accommodation/rooms/list', 'accommodation:rooms:read'));
      this.uiService.addSiderItem(rooms);
    }

    const contract_changes = new SiderItem('Alterações de contrato', null, null, 'accommodation:contract-changes');
    contract_changes.addChild(new SiderItem('Desistências', '', '/accommodation/withdrawals', 'accommodation:contract-changes:withdrawals',null,this.totalWithdrawal));
    contract_changes.addChild(new SiderItem('Ausências', '', '/accommodation/absences', 'accommodation:contract-changes:absences',null,this.totalAbsences));
    contract_changes.addChild(new SiderItem('Extensões', '', '/accommodation/extensions', 'accommodation:contract-changes:extensions',null,this.totalExtension));
    contract_changes.addChild(new SiderItem('Alteração de período', '', '/accommodation/period-change', 'accommodation:contract-changes:application-period-changes',null,this.totalPeriodChanges));
    contract_changes.addChild(new SiderItem('Alteração de extras', '', '/accommodation/extras-change', 'accommodation:contract-changes:application-extra-changes',null,this.totalExtraChanges));
    contract_changes.addChild(new SiderItem('Alteração de regime', '', '/accommodation/regime-change', 'accommodation:contract-changes:application-regime-changes',null,this.totalRegimeChanges));
    contract_changes.addChild(new SiderItem('Alteração de tipologia', '', '/accommodation/typology-change', 'accommodation:contract-changes:application-typology-change',null,this.totalTypolChanges));
    contract_changes.addChild(new SiderItem('Alteração de residência', '', '/accommodation/residence-change', 'accommodation:contract-changes:application-residence-change',null,this.totalRoomRes));
    contract_changes.addChild(new SiderItem('Permuta de quarto', '', '/accommodation/permute-room', 'accommodation:contract-changes:permute-room',null,this.totalRoomPerm));
    contract_changes.addChild(new SiderItem('Alteração regime de bolsa', '', '/accommodation/tariff-change', 'accommodation:contract-changes:application-tariff-changes',null,this.totalTarifChange));
    contract_changes.addChild(new SiderItem('Pedidos de manutenção', '', '/accommodation/maintenance-requests', 'accommodation:contract-changes:maintenance-requests',null,this.totalMaintenance));
    contract_changes.addChild(new SiderItem('Comunicação de entrada', '', '/accommodation/application-entry-communications', 'accommodation:contract-changes:application-entry-communications',null,this.totalCommEntry));
    contract_changes.addChild(new SiderItem('Comunicação de saída', '', '/accommodation/application-exit-communications', 'accommodation:contract-changes:application-exit-communications',null,this.totalCommExit));
    contract_changes.addChild(new SiderItem('Alteração de quarto', '', '/accommodation/room-changes', 'accommodation:contract-changes:application-room-changes',null,this.totalRoomChanges));
    contract_changes.addChild(new SiderItem('Alteração de iban', '', '/accommodation/iban-changes', 'accommodation:contract-changes:application-iban-changes',null,this.totalIbanChanges));

    this.uiService.addSiderItem(contract_changes);

    const occorrences = new SiderItem('Ocorrências', null, null, 'accommodation:occurrences');
    if (this.authservice.hasPermission('accommodation:occurrences')) {
      occorrences.addChild(new SiderItem('Criar', '', '/accommodation/occurrences/create', 'accommodation:occurrences:create'));
      occorrences.addChild(new SiderItem('Listar', '', '/accommodation/occurrences/list', 'accommodation:occurrences:read'));
    }
    this.uiService.addSiderItem(occorrences);


    const configurations = new SiderItem('Configurações', null, null, 'accommodation:configurations');
    if (this.authservice.hasPermission('accommodation:regimes:create') || this.authservice.hasPermission('accommodation:regimes:read')) {
      const regimes = configurations.addChild(new SiderItem('Regimes'));
      regimes.addChild(new SiderItem('Criar', '', '/accommodation/regimes/create', 'accommodation:regimes:create'));
      regimes.addChild(new SiderItem('Listar', '', '/accommodation/regimes/list', 'accommodation:regimes:read'));
    }

    if (this.authservice.hasPermission('accommodation:extras:create') || this.authservice.hasPermission('accommodation:extras:read')) {
      const extras = configurations.addChild(new SiderItem('Extras'));
      extras.addChild(new SiderItem('Criar', '', '/accommodation/extras/create', 'accommodation:extras:create'));
      extras.addChild(new SiderItem('Listar', '', '/accommodation/extras/list', 'accommodation:extras:read'));
    }

    if (this.authservice.hasPermission('accommodation:residences:create') || this.authservice.hasPermission('accommodation:residences:read')) {
      const residences = configurations.addChild(new SiderItem('Residências'));
      residences.addChild(new SiderItem('Criar', '', '/accommodation/residences/create', 'accommodation:residences:create'));
      residences.addChild(new SiderItem('Listar', '', '/accommodation/residences/list', 'accommodation:residences:read'));
    }

    if (this.authservice.hasPermission('accommodation:application-phases:create') || this.authservice.hasPermission('accommodation:application-phases:read')) {
      const applicationPahses = configurations.addChild(new SiderItem('Fases de Candidatura'));
      applicationPahses.addChild(new SiderItem('Criar', '', '/accommodation/phases/create', 'accommodation:application-phases:create'));
      applicationPahses.addChild(new SiderItem('Listar', '', '/accommodation/phases/list', 'accommodation:application-phases:read'));
    }

    if (this.authservice.hasPermission('accommodation:typologies:create') || this.authservice.hasPermission('accommodation:typologies:read')) {
      const tipologies = configurations.addChild(new SiderItem('Tipologias'));
      tipologies.addChild(new SiderItem('Criar', '', '/accommodation/typologies/create', 'accommodation:typologies:create'));
      tipologies.addChild(new SiderItem('Listar', '', '/accommodation/typologies/list', 'accommodation:typologies:read'));
    }

    if (this.authservice.hasPermission('accommodation:services:create') || this.authservice.hasPermission('accommodation:services:read')) {
      const services = configurations.addChild(new SiderItem('Serviços'));
      services.addChild(new SiderItem('Criar', '', '/accommodation/services/create', 'accommodation:services:create'));
      services.addChild(new SiderItem('Listar', '', '/accommodation/services/list', 'accommodation:services:read'));
    }

    if (this.authservice.hasPermission('accommodation:accommodation-periods:create') || this.authservice.hasPermission('accommodation:accommodation-periods:read')) {
      const dates = configurations.addChild(new SiderItem('Período Alojamento'));
      dates.addChild(new SiderItem('Criar', '', '/accommodation/period/create', 'accommodation:accommodation-periods:create'));
      dates.addChild(new SiderItem('Listar', '', '/accommodation/period/list', 'accommodation:accommodation-periods:read'));
    }

    if (this.authservice.hasPermission('accommodation:patrimony-categories:create') || this.authservice.hasPermission('accommodation:patrimony-categories:read')) {
      const valueMovable = configurations.addChild(new SiderItem('Património Mobiliário'));
      valueMovable.addChild(new SiderItem('Criar', '', '/accommodation/movable-heritage/create', 'accommodation:patrimony-categories:create'));
      valueMovable.addChild(new SiderItem('Listar', '', '/accommodation/movable-heritage/list', 'accommodation:patrimony-categories:read'));
    }

    if (this.authservice.hasPermission('accommodation:tariffs:create') || this.authservice.hasPermission('accommodation:tariffs:read')) {
      const tariffs = configurations.addChild(new SiderItem('Tarifários'));
      tariffs.addChild(new SiderItem('Criar', '', '/accommodation/tariffs/create', 'accommodation:tariffs:create'));
      tariffs.addChild(new SiderItem('Listar', '', '/accommodation/tariffs/list', 'accommodation:tariffs:read'));
    }

    if (this.authservice.hasPermission('accommodation:configurations:read') || this.authservice.hasPermission('accommodation:configurations:create')) {
      configurations.addChild(new SiderItem('Configurações Gerais', '', '/accommodation/geralsetting', 'accommodation:configurations:read'));
    }

    this.uiService.addSiderItem(configurations);

    this.uiService.setSiderActive(true);
  }


  updateBadges(stats: any) {
    this.totalWithdrawal.next({ value: stats.withdrawals ? stats.withdrawals.value: '-', color: stats.withdrawals.color });
    this.totalAbsences.next({ value: stats.absences ? stats.absences.value : '-', color: stats.withdrawals.color });
    this.totalCommEntry.next({ value: stats.application_communications_entry ? stats.application_communications_entry.value : '-', color: stats.withdrawals.color });
    this.totalCommExit.next({ value: stats.application_communications_exit ? stats.application_communications_exit.value : '-', color: stats.withdrawals.color });
    this.totalExtraChanges.next({ value: stats.application_extra_changes ? stats.application_extra_changes.value : '-', color: stats.withdrawals.color });
    this.totalIbanChanges.next({ value: stats.application_iban_changes ? stats.application_iban_changes.value : '-', color: stats.withdrawals.color });
    this.totalRegimeChanges.next({ value: stats.application_regime_changes ? stats.application_regime_changes.value : '-', color: stats.withdrawals.color });
    this.totalRoomPerm.next({ value: stats.application_room_permute_changes ? stats.application_room_permute_changes.value : '-', color: stats.withdrawals.color });
    this.totalRoomRes.next({ value: stats.application_room_residence_changes ? stats.application_room_residence_changes.value : '-', color: stats.withdrawals.color });
    this.totalRoomChanges.next({ value: stats.application_room_room_changes ? stats.application_room_room_changes.value : '-', color: stats.withdrawals.color });
    this.totalTypolChanges.next({ value: stats.application_room_typology_changes ? stats.application_room_typology_changes.value : '-', color: stats.withdrawals.color });
    this.totalTarifChange.next({ value: stats.application_tariff_changes ? stats.application_tariff_changes.value : '-', color: stats.withdrawals.color });
    this.totalExtension.next({ value: stats.extensions ? stats.extensions.value : '-', color: stats.withdrawals.color });
    this.totalMaintenance.next({ value: stats.maintenance_requests_resolving ? stats.maintenance_requests_resolving.value : '-', color: stats.withdrawals.color });
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
