import { RoomOccupantsComponent } from './components/room-occupants/room-occupants.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccommodationRoutingModule } from './accommodation-routing.module';
import { AccommodationComponent } from './accommodation.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ResidenceSelectionComponent } from './pages/residence-selection/residence-selection.component';
import { AccommodationService } from './services/accommodation.service';
import { PremuteRoomComponent } from './components/premute-room/premute-room.component';
import { PremuteRoomModalService } from './services/premute-room-modal.service';
import { UsersService } from '../users/modules/users_users/services/users.service';
import { GeralSettingsComponent } from './pages/geral-settings/geral-settings.component';
import { ConsultApplicationComponent } from './pages/consult-application/consult-application.component';
import { ViewApplicationDetailComponent } from './pages/consult-application/components/view-application-detail/view-application-detail.component';

@NgModule({
  declarations: [
    AccommodationComponent,
    DashboardComponent,
    RoomOccupantsComponent,
    ResidenceSelectionComponent,
    PremuteRoomComponent,
    GeralSettingsComponent,
    ConsultApplicationComponent,
    ViewApplicationDetailComponent
  ],
  imports: [
    CommonModule,
    AccommodationRoutingModule,
    SharedModule,
  ],
  entryComponents: [
    PremuteRoomComponent
  ],
  providers: [
    AccommodationService,
    PremuteRoomModalService,
    UsersService
  ],
})
export class AccommodationModule { }
