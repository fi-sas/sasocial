import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremuteRoomComponent } from './premute-room.component';

describe('PremuteRoomComponent', () => {
  let component: PremuteRoomComponent;
  let fixture: ComponentFixture<PremuteRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremuteRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremuteRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
