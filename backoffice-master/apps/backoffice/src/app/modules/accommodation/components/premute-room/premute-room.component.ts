import { Component, OnInit } from '@angular/core';
import { ApplicationModel } from '../../modules/aco_applications/models/application.model';
import { RoomsService } from '../../modules/rooms/services/rooms.service';
import { RoomModel } from '../../modules/rooms/models/room.model';
import { first, finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-premute-room',
  templateUrl: './premute-room.component.html',
  styleUrls: ['./premute-room.component.less']
})
export class PremuteRoomComponent implements OnInit {

  application: ApplicationModel = null;
  residenceId: number;

  rooms: RoomModel[] = [];
  rooms_loading = false;

  selected_room :RoomModel;
  premute_with_id= null;

  constructor(
    private roomsService: RoomsService,
  ) { }

  ngOnInit() {
    this.loadRooms();
  }

  loadRooms() {
    this.rooms_loading = true;
    this.roomsService.list(1, -1, null,  null, {
      withRelated: 'occupants',
      residence_id: this.residenceId ? this.residenceId : this.application.residence_id
    }).pipe(
      first(),
      finalize(() => this.rooms_loading = false)
    ).subscribe(results => {
      this.rooms = results.data.filter(room => room.id !== this.application.room_id);
    });
  }

  roomChanged() {
    
  }

}
