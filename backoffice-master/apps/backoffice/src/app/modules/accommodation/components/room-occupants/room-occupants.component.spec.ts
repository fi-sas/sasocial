import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomOccupantsComponent } from './room-occupants.component';

describe('RoomOccupantsComponent', () => {
  let component: RoomOccupantsComponent;
  let fixture: ComponentFixture<RoomOccupantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomOccupantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomOccupantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
