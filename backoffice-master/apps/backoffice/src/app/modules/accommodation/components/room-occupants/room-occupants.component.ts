import { Component, OnInit, Input } from '@angular/core';
import { RoomsService } from '../../modules/rooms/services/rooms.service';
import { first, finalize } from 'rxjs/operators';
import { RoomModel } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/models/room.model';
import { ApplicationModel } from '../../modules/aco_applications/models/application.model';
import { PremuteRoomModalService } from '../../services/premute-room-modal.service';

@Component({
  selector: 'fi-sas-room-occupants',
  templateUrl: './room-occupants.component.html',
  styleUrls: ['./room-occupants.component.less']
})
export class RoomOccupantsComponent implements OnInit {

  @Input('roomId') roomId: number;
  @Input('academicYear') academicYear: string;

  room: RoomModel = null;
  occupants : ApplicationModel[] = [];
  application;
  loading = false;

  constructor(
    private roomsService: RoomsService,
    private premuteRoomService: PremuteRoomModalService
  ) { }

  ngOnInit() {
    this.loadOccupants();
  }
  
  loadOccupants() {
    this.loading = true;

    this.roomsService.getOccupants(this.roomId, this.academicYear).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(res => {
      this.occupants = res.data[0].occupants;
      this.application = res.data[0];
    });
  }

  openModal(application: ApplicationModel, id_residence: number)  {

   return this.premuteRoomService.openModal(application, id_residence).subscribe(changed => {
      if (changed)
        this.loadOccupants();
    });
  }

}
