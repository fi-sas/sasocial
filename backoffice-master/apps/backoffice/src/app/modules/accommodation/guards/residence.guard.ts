import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccommodationService } from '../services/accommodation.service';

@Injectable({
  providedIn: 'root'
})
export class ResidenceGuard implements CanActivate {

  constructor(
    private accommodationService: AccommodationService,
    private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return true;

    /*if (this.accommodationService.selectedResidence()) {
        return true;
    } else {
      this.router.navigate(['accommodation', 'residence-selection'], {
        queryParams: { returnUrl: state.url }
      });
      return false;
    }*/
  }
}
