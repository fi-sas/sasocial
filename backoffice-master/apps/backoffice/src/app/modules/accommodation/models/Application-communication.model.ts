import { ApplicationModel } from "../modules/aco_applications/models/application.model";

export const ApplicationCommunicationStatusTranslations = {
    SUBMITTED: { label: 'Submetido', color: 'blue' },
    CLOSED: { label: 'Fechado', color: 'gray' }
};

export class ApplicationCommunicationModel {
    id: number;
    application_id: number;
    application: ApplicationModel;
    start_date: Date;
    end_date: Date;
    observations: string;
    response: string;
    status: string;
    type: string;
    created_at: Date;
    updated_at: Date;
    entry_hour: string;
    exit_hour: string;
}
