import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../modules/aco_applications/models/application.model';
import { ResidenceModel } from '../modules/residences/models/residence.model';
import { RoomModel } from '../modules/rooms/models/room.model';
import { TypologyModel } from '../modules/typologies/models/typology.model';
import { TypologiesModule } from '../modules/typologies/typologies.module';

// export const ExtensionDecision = {
//   APPROVE: { label: 'Aprovar', color: 'green' },
//   REJECT: { label: 'Rejeitar', color: 'red' },
// };
export enum RoomChangeRequestStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}
export enum RoomChangeRequestDecision {
  APPROVE = 'APPROVE',
  REJECT = 'REJECT',
}
export const RoomChangeRequestDecisionTranslations = {
  APPROVE: { label: 'Aprovada', color: 'green' },
  REJECT: { label: 'Rejeitada', color: 'red' },
};
export const RoomChangeRequestStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export class RoomChangeRequestHistoryModel {
  id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class RoomChangeRequestModel {
  id: number;
  reason: string;
  application_id: number;
  application: ApplicationModel;
  start_date: Date;
  observations: string;
  decision: RoomChangeRequestDecision;
  status: RoomChangeRequestStatus;
  created_at: Date;
  update_at: Date;
  history: RoomChangeRequestHistoryModel[];
  residence_id: number
  residence: ResidenceModel;
  room_id: number
  room: RoomModel;
  permute_user_tin: string;
  type: string;
  typology_id: number;
  typology: TypologyModel;
  file_id: number;
  file: File;
}
