export class OccupationModel {
  date: string;
  applications: number;
  bookings: number;
  absences: number;
  withdawals: number;
  total_occupation: number;
  occupation_rate: number;
}

export class OccupationMapModel {
  occupation: OccupationModel[];
  applications: number;
  bookings: number;
  absences: number;
  withdrawals: number;
  rooms: number;
}
