import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAbsencesComponent } from './pages/list-absences/list-absences.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAbsencesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:absences:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbsencesRoutingModule { }
