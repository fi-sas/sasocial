import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListAbsencesComponent } from './pages/list-absences/list-absences.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbsencesRoutingModule } from './absences-routing.module';
import { ViewAbsenceComponent } from './components/view-absence/view-absence.component';

@NgModule({
  declarations: [
    ListAbsencesComponent,
    ViewAbsenceComponent
  ],
  imports: [
    CommonModule,
    AbsencesRoutingModule,
    SharedModule
  ]
})
export class AbsencesModule { }
