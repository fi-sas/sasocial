import { Component, OnInit, Input } from '@angular/core';
import { AbsenceModel, AbsenceStatusTranslations } from '../../models/absence.model';

@Component({
  selector: 'fi-sas-view-absence',
  templateUrl: './view-absence.component.html',
  styleUrls: ['./view-absence.component.less']
})
export class ViewAbsenceComponent implements OnInit {

  @Input() data: AbsenceModel = null;
  AbsenceStatusTranslations = AbsenceStatusTranslations;
  constructor() { }

  ngOnInit() {
  }

}
