import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';


export const AbsenceStatusTranslations = {
    SUBMITTED: { label: 'Submetido', color: 'blue' },
    ANALYSIS: { label: 'Analise', color: 'yellow' },
    DISPATCH: { label: 'Despacho', color: 'orange' },
    APPROVED_WITH_SUSPENSION: { label: 'Aprovada c/suspensão de pagamento', color: 'green' },
    APPROVED_WITHOUT_SUSPENSION: { label: 'Aprovada s/suspensão de pagamento', color: 'red' },
    CANCELLED: { label: 'Cancelada', color: 'gray' },
};

export const AbsenceDecisionTranslations = {
    APPROVE_WITH_SUSPENSION: { label: 'Aprovar c/suspensão', color: 'green' },
    APPROVE_WITHOUT_SUSPENSION: { label: 'Aprovar s/suspensão', color: 'red' },
};

export class AbsenceModel {
    id: number;
    start_date: Date;
    end_date: Date;
    exit_date: Date;
    reason: string;
    application: ApplicationModel;
    application_id: number;
    file: FileModel
    file_id: number;
    allow_booking: number;
    status: string;
    decision: string;
    updated_at: Date;
    created_at: Date;
    history: AbsenceHistoryModel[];
}

export class AbsenceHistoryModel {
    id: number;
    absence_id: number;
    user_id: number;
    status: string;
    notes: string;
    decision: string;
    updated_at: Date;
    created_at: Date;
}