import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { AbsencesService } from '../../services/absences.service';
import * as moment from 'moment';
import { AbsenceDecisionTranslations, AbsenceModel, AbsenceStatusTranslations } from '../../models/absence.model';
import { finalize, first } from 'rxjs/operators';
@Component({
  selector: 'fi-sas-list-absences',
  templateUrl: './list-absences.component.html',
  styleUrls: ['./list-absences.component.less']
})
export class ListAbsencesComponent extends TableHelper implements OnInit {
  AbsenceStatusTranslations = AbsenceStatusTranslations;
  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalAbsenceSelected: AbsenceModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE_WITH_SUSPENSION";
  residence: ResidenceModel = null;
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public accommodationService: AccommodationService,
    public absenceService: AbsencesService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'end_date',
        label: 'Data Fim',
        template: (data) => {
          return moment(new Date(data.end_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'application.room.name',
        label: 'Quarto',
        sortable: false
      },
      {
        key: 'application.assignedResidence.name',
        label: 'Residência',
        sortable: false
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: AbsenceDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: AbsenceStatusTranslations
      },
    );

    this.persistentFilters['withRelated'] = 'application,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.absenceService);
  }

  openModalWithdrawal(absence: AbsenceModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalAbsenceSelected = absence;
    this.changeStatusModalAction = action;
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {
    if (this.changeStatusModalAction === 'analyse') {
      this.changeStatusModalLoading = true;
      this.absenceService.status(this.changeStatusModalAbsenceSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.absenceService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.absenceService.status(this.changeStatusModalAbsenceSelected.id, "DISPATCH", {
        decision: this.changeStatusModalDecision
      }, this.changeStatusModalNotes
      ).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.absenceService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.absenceService.admin_approve(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.absenceService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.absenceService.admin_reject(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.absenceService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }
  }

  handleChangeStatusCancel() {
    this.changeStatusModalAbsenceSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
