import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { AbsenceModel } from '../models/absence.model';

@Injectable({
  providedIn: 'root'
})
export class AbsencesService extends Repository<AbsenceModel> {

  entities_url = 'ACCOMMODATION.ABSENCES';
  entity_url = 'ACCOMMODATION.ABSENCES_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, absence, notes) {
    return this.resourceService.create<AbsenceModel>(
      this.urlService.get('ACCOMMODATION.ABSENCES_ID_STATUS', { id }), { event, absence, notes });
  }

  admin_approve(id: number, absence, notes) {
    return this.resourceService.create<AbsenceModel>(
      this.urlService.get('ACCOMMODATION.ABSENCES_ID_APPROVE', { id }), { absence, notes });
  }

  admin_reject(id: number, absence, notes) {
    return this.resourceService.create<AbsenceModel>(
      this.urlService.get('ACCOMMODATION.ABSENCES_ID_REJECT', { id }), { absence, notes });
  }

}
