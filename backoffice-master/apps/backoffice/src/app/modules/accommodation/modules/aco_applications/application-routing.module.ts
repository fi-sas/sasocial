import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { FormApplicationComponent } from "./pages/form-application/form-application.component";
import { ListApplicationsComponent } from "./pages/list-applications/list-applications.component";
import { ResidenceGuard } from "../../guards/residence.guard";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { ApplicationsReportsComponent } from "./pages/applications-reports/applications-reports.component";


const routes: Routes = [
  { path: '', redirectTo: 'all-applications', pathMatch: 'full' }, 
  {
    path: 'reports',
    component: ApplicationsReportsComponent,
    data: { breadcrumb: 'Relatórios de candidaturas', title: 'Relatórios de candidaturas' }
  },
  {
    path: 'form-application',
    component: FormApplicationComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:applications:create' }
  },
  {
    path: 'all-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', applicationsStatus: '', scope: 'accommodation:applications:all-applications' },
  },
/*  {
    path: 'selection-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas submetidas', title: 'Candidaturas submetidas', applicationsStatus: 'submitted', scope: 'accommodation:applications:applications-selection' }
    ,
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'analyse-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Analise de candidaturas', title: 'Analise de candidaturas', applicationsStatus: 'analysed', scope: 'accommodation:applications:applications-analyse' }
    ,
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'dispach-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas para despacho', title: 'Candidaturas para despacho', applicationsStatus: 'pending', scope: 'accommodation:applications:applications-dispatches' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'queued-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Lista de espera', title: 'Lista de espera', applicationsStatus: 'queued', scope: 'accommodation:applications:applications-queued' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'assigned-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas colocadas', title: 'Candidaturas colocadas', applicationsStatus: 'assigned', scope: 'accommodation:applications:applications-assigned' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'unassigned-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas não colocadas', title: 'Candidaturas não colocadas', applicationsStatus: 'unassigned', scope: 'accommodation:applications:applications-unassigned' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'confirmeds-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas confirmadas', title: 'Candidaturas confirmadas', applicationsStatus: 'confirmed', scope: 'accommodation:applications:applications-confirmeds' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'rejects-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas rejeitadas', title: 'Candidaturas rejeitadas', applicationsStatus: 'rejected', scope: 'accommodation:applications:applications-rejected' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'oppositions-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas em oposição', title: 'Candidaturas em oposição', applicationsStatus: 'opposition', scope: 'accommodation:applications:applications-oppositions' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'contracted-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas contratadas', title: 'Candidaturas contratadas', applicationsStatus: 'contracted', scope: 'accommodation:applications:applications-contracteds' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'closed-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas fechadas', title: 'Candidaturas fechadas', applicationsStatus: 'closed', scope: 'accommodation:applications:applications-closed' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'cancelled-applications',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Candidaturas canceladas', title: 'Candidaturas canceladas', applicationsStatus: 'cancelled',  scope: 'accommodation:applications:applications-cancelled'},
    canActivate: [ ResidenceGuard]
  },*/
  {
    path: '**',
    component: PageNotFoundComponent,
    data: {
      breadcrumb: 'Página não encontrada',
      title: 'Página não encontrada'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
