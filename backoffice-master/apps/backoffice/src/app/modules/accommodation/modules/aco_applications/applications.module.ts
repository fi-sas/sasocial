import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationDataModalComponent } from './components/application-data-modal/application-data-modal.component';
import { ApplicationStatusPromptComponent } from './components/application-status-prompt/application-status-prompt.component';
import { ApplicationDecisionConfirmComponent } from './components/application-decision-confirm/application-decision-confirm.component';
import { ApplicationObservationsSaveComponent } from './components/application-observations-save/application-observations-save.component';
import { ViewApplicationComponent } from './components/view-application/view-application.component';
import { FormApplicationComponent } from "./pages/form-application/form-application.component";
import { ListApplicationsComponent } from "./pages/list-applications/list-applications.component";
import { ApplicationsRoutingModule } from './application-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsReportsComponent } from './pages/applications-reports/applications-reports.component';
import { PremuteRoomModalService } from '../../services/premute-room-modal.service';
import { ResumeApplicationComponent } from './components/resume-application/resume-application.component';
import { RoomChangeModalComponent } from './components/room-change-modal/room-change-modal.component';
import { RegimeExtrasChangeModelComponent } from './components/regime-extras-change-model/regime-extras-change-model.component';
import { ApplicationTariffChangeComponent } from './components/application-tariff-change/application-tariff-change.component';
import { ApplicationRollbackStatusModalComponent } from './components/application-rollback-status-modal/application-rollback-status-modal.component';
import { WithdrawalModalComponent } from './components/withdrawal-modal/withdrawal-modal.component';
import { ApplicationPeriodChangeComponent } from './components/application-period-change/application-period-change.component';
import { ApplicationRenewComponent } from './components/application-renew/application-renew.component';

@NgModule({
  declarations: [
    ListApplicationsComponent,
    FormApplicationComponent,
    ViewApplicationComponent,
    ApplicationsReportsComponent,
    ApplicationDataModalComponent,
    ApplicationStatusPromptComponent,
    ApplicationDecisionConfirmComponent,
    ApplicationObservationsSaveComponent,
    ResumeApplicationComponent,
    RoomChangeModalComponent,
    RegimeExtrasChangeModelComponent,
    ApplicationTariffChangeComponent,
    ApplicationRollbackStatusModalComponent,
    WithdrawalModalComponent,
    ApplicationPeriodChangeComponent,
    ApplicationRenewComponent
  ],
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    SharedModule
  ],
  providers: [
    PremuteRoomModalService
  ],
  entryComponents: [
    ViewApplicationComponent,
    ApplicationDataModalComponent,
    ApplicationStatusPromptComponent,
    RoomChangeModalComponent,
    RegimeExtrasChangeModelComponent,
    ApplicationTariffChangeComponent,
    ApplicationRollbackStatusModalComponent,
    WithdrawalModalComponent,
    ApplicationPeriodChangeComponent,
    ApplicationRenewComponent
  ]
})
export class ApplicationsModule { }
