import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationDataModalComponent } from './application-data-modal.component';

describe('ApplicationDataModalComponent', () => {
  let component: ApplicationDataModalComponent;
  let fixture: ComponentFixture<ApplicationDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
