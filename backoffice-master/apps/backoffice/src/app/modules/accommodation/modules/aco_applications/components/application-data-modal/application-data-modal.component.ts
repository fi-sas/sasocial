import { Validators } from '@angular/forms';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { ApplicationData } from '../../models/application-data.model';
import { ApplicationModel } from '../../models/application.model';
import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { first, finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-application-data-modal',
  templateUrl: './application-data-modal.component.html',
  styleUrls: ['./application-data-modal.component.less']
})
export class ApplicationDataModalComponent implements OnInit {

  application: ApplicationModel = null;
  applicationData: ApplicationData = new ApplicationData();
  residenceSelect = '';


  formData = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    end_date: new FormControl(null, [Validators.required]),
    signed_contract_file_id: new FormControl(null, []),
    liability_term_file_id: new FormControl(null, []),
    international_student: new FormControl(false, [Validators.required]),
  });

  formDataAnaysed = new FormGroup({
    per_capita_income: new FormControl(null, []),
  });


  constructor(
    private modalRef: NzModalRef,
    private applicationsservice: ApplicationsService,
  ) {
  }

  ngOnInit() {
    if (this.application && this.application.status == 'contracted') {
      this.formData.patchValue({
        start_date: this.application.start_date,
        end_date: this.application.end_date,
        signed_contract_file_id: this.application.signed_contract_file_id ? this.application.signed_contract_file_id : null,
        liability_term_file_id: this.application.liability_term_file_id ? this.application.liability_term_file_id : null,
        international_student: this.application.international_student
      });
    }else if (this.application && this.application.status == 'analysed') {
      this.formDataAnaysed.patchValue({
        per_capita_income: this.application.per_capita_income,
      });
    }

  }

  public save() {
    if (this.application.status == 'contracted') {
      for (const i in this.formData.controls) {
        if (this.formData.controls[i]) {
          this.formData.controls[i].markAsDirty();
          this.formData.controls[i].updateValueAndValidity();
        }
      }
      if (this.formData.valid) {
        let data = {
          start_date: this.formData.value.start_date,
          end_date: this.formData.value.end_date,
          signed_contract_file_id: this.formData.value.signed_contract_file_id,
          liability_term_file_id: this.formData.value.liability_term_file_id,
          international_student: this.formData.value.international_student,
        }

        return new Promise((resolve, reject) => {
          this.applicationsservice.applicationBackofficeData(this.application.id, data)
            .pipe(first(), finalize(() => reject())).subscribe(application => {
              this.close();
              resolve(true);
            }, () => resolve(false));
        });
      } else {
        return Promise.resolve(false);
      }
    } else {
      if (this.formDataAnaysed.valid) {
        let data = {
          per_capita_income: this.formDataAnaysed.value.per_capita_income,
        }

        return new Promise((resolve, reject) => {
          this.applicationsservice.applicationBackofficeData(this.application.id, data)
            .pipe(first(), finalize(() => reject())).subscribe(application => {
              this.close();
              resolve(true);
            }, () => resolve(false));
        });
      } else {
        return Promise.resolve(false);
      }

    }

  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }

}
