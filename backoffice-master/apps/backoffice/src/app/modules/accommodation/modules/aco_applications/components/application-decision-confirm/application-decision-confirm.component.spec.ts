import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationDecisionConfirmComponent } from './application-decision-confirm.component';

describe('ApplicationDecisionConfirmComponent', () => {
  let component: ApplicationDecisionConfirmComponent;
  let fixture: ComponentFixture<ApplicationDecisionConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationDecisionConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationDecisionConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
