import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationModel, ApplicationDecisionList } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';
import { first, finalize } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ListService } from '@fi-sas/backoffice/shared/services/list.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';

@Component({
  selector: 'fi-sas-application-decision-confirm',
  templateUrl: './application-decision-confirm.component.html',
  styleUrls: ['./application-decision-confirm.component.less']
})
export class ApplicationDecisionConfirmComponent implements OnInit {

  @Input() application: ApplicationModel = null;
  @Output() applicationChanged: EventEmitter<boolean> = new EventEmitter<boolean>(); 

  ApplicationDecisionList = ApplicationDecisionList;
  loading = false;

  constructor(
    private accommodationService: AccommodationService,
    private applicationSevice: ApplicationsService,
    private listService: ListService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
  }


  reject() {
    this.loading = true;
    this.applicationSevice.rejectDecision(this.application.id)
    .pipe(first(), finalize(() => this.loading = false))
    .subscribe(resulst => {
      this.listService.deleteItem(this.application);
      this.uiService.showMessage(MessageType.success, 'Decisão rejeitada');
      this.applicationChanged.emit(true);
      this.accommodationService.updateApplicationsStats();
    });
  }

  approve() {
    this.loading = true;
    this.applicationSevice.acceptDecision(this.application.id)
    .pipe(first(), finalize(() => this.loading = false))
    .subscribe(resulst => {
      this.listService.deleteItem(this.application);
      this.uiService.showMessage(MessageType.success, 'Decisão aprovada');
      this.applicationChanged.emit(true);
      this.accommodationService.updateApplicationsStats();
    });
  }
}
