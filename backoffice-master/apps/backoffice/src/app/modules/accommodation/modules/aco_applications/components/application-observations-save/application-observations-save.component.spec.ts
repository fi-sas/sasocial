import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationObservationsSaveComponent } from './application-observations-save.component';

describe('ApplicationObservationsSaveComponent', () => {
  let component: ApplicationObservationsSaveComponent;
  let fixture: ComponentFixture<ApplicationObservationsSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationObservationsSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationObservationsSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
