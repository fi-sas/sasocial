import { Component, OnInit, Input } from '@angular/core';
import { ApplicationsService } from '../../services/applications.service';
import { first, tap, finalize } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';

@Component({
  selector: 'fi-sas-application-observations-save',
  templateUrl: './application-observations-save.component.html',
  styleUrls: ['./application-observations-save.component.less']
})
export class ApplicationObservationsSaveComponent implements OnInit {

  @Input('application')
  application: ApplicationModel = null;
  observations = '';
  loading = false;

  constructor(
    private applicationService: ApplicationsService
  ) { }

  ngOnInit() {
    this.observations = this.application.observations;
  }

  save() {
    this.loading = true;
    this.applicationService.patch(this.application.id, {
      observations: this.observations
    }).pipe(first(), finalize(() => this.loading = false))
    .subscribe(r => {
        this.application = r.data[0];
    });
  }
}
