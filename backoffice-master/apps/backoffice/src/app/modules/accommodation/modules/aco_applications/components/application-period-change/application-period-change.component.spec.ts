import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationPeriodChangeComponent } from './application-period-change.component';

describe('ApplicationPeriodChangeComponent', () => {
  let component: ApplicationPeriodChangeComponent;
  let fixture: ComponentFixture<ApplicationPeriodChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationPeriodChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationPeriodChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
