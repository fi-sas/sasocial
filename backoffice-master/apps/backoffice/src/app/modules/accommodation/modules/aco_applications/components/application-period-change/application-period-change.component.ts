import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd';
import { ApplicationModel } from '../../models/application.model';
import * as moment from 'moment';
import { ApplicationsService } from '../../services/applications.service';
import { finalize, first } from 'rxjs/operators';
import { PeriodChangeRequestsService } from '../../../period-change-requests/services/period-change-requests.service';

@Component({
  selector: 'fi-sas-application-period-change',
  templateUrl: './application-period-change.component.html',
  styleUrls: ['./application-period-change.component.less']
})
export class ApplicationPeriodChangeComponent implements OnInit {

  application: ApplicationModel = null;
  
  formData = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    end_date: new FormControl(null, [Validators.required]),
    application_id: new FormControl(null, [Validators.required]),
    reason: new FormControl(null, [Validators.required]),
  });

  constructor(
    private modalRef: NzModalRef,
    private periodChangeRequest: PeriodChangeRequestsService
  ) { }

  ngOnInit() {
    this.formData.patchValue({
      application_id: this.application.id,
      start_date: moment(this.application.start_date).format("YYYY-MM-DD"),
      end_date: moment(this.application.end_date).format("YYYY-MM-DD")
    })
  }

  disableStartDate = (date: Date) => {
    const endDate = moment(this.formData.controls.end_date.value).format("YYYY-MM-DD")
    return moment(moment(date).format("YYYY-MM-DD")).isSameOrAfter(endDate);
  }

  disableEndDate = (date: Date) => {
    const startDate = moment(this.formData.controls.start_date.value).format("YYYY-MM-DD");
    return  moment(moment(date).format("YYYY-MM-DD")).isSameOrBefore(startDate);
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }

  public save() {
    if (this.application.status == 'contracted') {
      for (const i in this.formData.controls) {
        if (this.formData.controls[i]) {
          this.formData.controls[i].markAsDirty();
          this.formData.controls[i].updateValueAndValidity();
        }
      }
      if (this.formData.valid) {
        return new Promise((resolve, reject) => {
          this.periodChangeRequest.create(this.formData.value)
            .pipe(first(), finalize(() => reject())).subscribe(application => {
              this.close();
              resolve(true);
            }, () => resolve(false));
        });
      } else {
        return Promise.resolve(false);
      }
    }
  }
}
