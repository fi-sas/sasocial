import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationRenewComponent } from './application-renew.component';

describe('ApplicationRenewComponent', () => {
  let component: ApplicationRenewComponent;
  let fixture: ComponentFixture<ApplicationRenewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationRenewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationRenewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
