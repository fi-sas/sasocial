import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { NzModalRef } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { ApplicationPhaseModel } from '../../../phases/models/application-phase.model';
import { ApplicationPhaseService } from '../../../phases/services/application-phase.service';
import { ApplicationModel } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';

@Component({
  selector: 'fi-sas-application-renew',
  templateUrl: './application-renew.component.html',
  styleUrls: ['./application-renew.component.less']
})
export class ApplicationRenewComponent implements OnInit {

  @Input() application: ApplicationModel = null;
  applicationPhases: ApplicationPhaseModel[] = [];
  applicationToRenewID: number = null;
  applicationPhase: ApplicationPhaseModel = null;

  constructor(
    private modalRef: NzModalRef,
    private applicationPhasesService: ApplicationPhaseService,
    private router: Router,
    private applicationService: ApplicationsService,
    private uiService: UiService,
    ) { }

  ngOnInit() {
    this.getApplicationPhases();
  }

  getApplicationPhases() {
    this.applicationPhasesService.openApplication().pipe(
      first()
    ).subscribe(result => {
      this.applicationPhases = result.data;
    });
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }


  submit() {
    if(this.applicationPhase){
      this.applicationService.canAply(this.application.user_id, this.applicationPhase.id).pipe(first()).subscribe((response) => {
        if(response.data.length){
          if(response.data[0].renew){
            this.close();
            this.router.navigate(['accommodation', 'applications','form-application'], {
              queryParams: {
                residence_id: this.application.residence_id,
                application_id: response.data[0].renew_application_id,
                application_phase_id: this.applicationPhase.id
              }
            });
          }else{
            this.uiService.showMessage(
              MessageType.error,
              'O utilizador tem uma renovação para o ano letivo selecionado.'
            );
          }
        }
      });
    }else{
      this.uiService.showMessage(
        MessageType.error,
        'É necessário selecionar um ano letivo.'
      );
    }
  }

  getPhase(phase) {
    return "["+ phase.academic_year + "] " + " Fase: " + phase.application_phase + ' (' + phase.start_date.split('T')[0] + '/' + phase.end_date.split('T')[0] + ')';
  }

}
