import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationRollbackStatusModalComponent } from './application-rollback-status-modal.component';

describe('ApplicationRollbackStatusModalComponent', () => {
  let component: ApplicationRollbackStatusModalComponent;
  let fixture: ComponentFixture<ApplicationRollbackStatusModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationRollbackStatusModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationRollbackStatusModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
