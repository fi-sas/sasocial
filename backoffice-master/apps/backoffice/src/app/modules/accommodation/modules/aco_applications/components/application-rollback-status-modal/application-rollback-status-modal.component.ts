import { Component, OnInit } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';

@Component({
  selector: 'fi-sas-application-rollback-status-modal',
  templateUrl: './application-rollback-status-modal.component.html',
  styleUrls: ['./application-rollback-status-modal.component.less']
})
export class ApplicationRollbackStatusModalComponent implements OnInit {
  application: ApplicationModel = null;

  possible_rollbacks = {
    analysed: [
      { label: 'Submetida', event: 'ROLLBACK_TO_SUBMITTED' }
    ],
    cancelled: [
      { label: 'Submetida', event: 'ROLLBACK_TO_SUBMITTED' },
      { label: 'Em Analise', event: 'ROLLBACK_TO_ANALYSED' },
      { label: 'Despacho', event: 'ROLLBACK_TO_PENDING' }
    ],
    pending: [
      { label: 'Em Analise', event: 'ROLLBACK_TO_ANALYSED' },
    ],
    assigned: [
      { label: 'Despacho', event: 'ROLLBACK_TO_PENDING' }
    ],
    unassigned: [
      { label: 'Despacho', event: 'ROLLBACK_TO_PENDING' }
    ],
    confirmed: [
      { label: 'Colocado', event: 'ROLLBACK_TO_ASSIGNED' }
    ],
    rejected: [
      { label: 'Colocado', event: 'ROLLBACK_TO_ASSIGNED' }
    ],
    withdrawal: [
      { label: 'Em análise', event: 'ROLLBACK_TO_ANALYSED' }
    ],

  }
  selected_rollback = null;
  notes = null;
  submitted = false;
  constructor(
    private modalRef: NzModalRef,
    private applicationService: ApplicationsService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }


  submit() {
    this.submitted = true;
    if (!this.selected_rollback) { return };

    return new Promise((resolve, reject) => {
      this.applicationService.rollbackStatus(this.application.id, this.selected_rollback, this.notes)
        .pipe(first(), finalize(() => reject())).subscribe(application => {
          this.uiService.showMessage(MessageType.success, 'Estado revertido com sucesso');
          this.close();
          resolve(true);
        }, () => resolve(false));
    });
  }

}
