import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationStatusPromptComponent } from './application-status-prompt.component';

describe('ApplicationStatusPromptComponent', () => {
  let component: ApplicationStatusPromptComponent;
  let fixture: ComponentFixture<ApplicationStatusPromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationStatusPromptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationStatusPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
