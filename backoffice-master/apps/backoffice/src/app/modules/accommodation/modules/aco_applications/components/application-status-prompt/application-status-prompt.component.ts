import { RoomModel } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/models/room.model';
import { ApplicationStatus, ApplicationDecisionList, ApplicationDecision, ApplicationModel } from '../../models/application.model';
import { first, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { Component, OnInit } from '@angular/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import { ApplicationsService } from '../../services/applications.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { ResidencesService } from '../../../residences/services/residences.service';
import { ResidenceOccupationModel } from '../../../residences/models/residence-occupation.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '../../../tariffs/services/tariffs.service';


@Component({
  selector: 'fi-sas-application-status-prompt',
  templateUrl: './application-status-prompt.component.html',
  styleUrls: ['./application-status-prompt.component.less']
})
export class ApplicationStatusPromptComponent implements OnInit {
  ApplicationStatus = ApplicationStatus;
  application: ApplicationModel = null;
  applications: ApplicationModel[] = null;
  state: any = null;
  notes = '';
  available: boolean = true;
  residence_id = null;
  residences: ResidenceModel[] = [];
  residenceOccupation: ResidenceOccupationModel = { totalOccupied: 0, totalAvailable: 0, maximumcapacity: 0, occupationRate: 0 };
  residencesLoad = false;
  residenceSelect = '';
  ApplicationDecision = ApplicationDecision;
  ApplicationDecisionList = ApplicationDecisionList
  decision: ApplicationDecision = null;
  decisionLoad = false;

  tariff_id: number = null;
  tariffs: TariffModel[] = [];
  tariffLoad = false;

  room_id = null;
  rooms: RoomModel[] = [];
  roomsLoad

  isMultiple = false;
  totalSelected = 0;

  opposition_answer = '';

  constructor(
    private accommodationService: AccommodationService,
    private uiService: UiService,
    private modalRef: NzModalRef,
    private residencesService: ResidencesService,
    private roomsService: RoomsService,
    private applicationService: ApplicationsService,
    private tariffsService: TariffsService
  ) { }

  ngOnInit() {
    this.isMultiple = !!this.applications;
    this.totalSelected = this.isMultiple ? this.applications.length : 0;
    if (!this.application && this.isMultiple) {
      this.application = this.applications[0];
    }

    if ((this.application.status === ApplicationStatus.ANALYSED || this.application.status === ApplicationStatus.QUEUED || this.application.status === ApplicationStatus.OPPOSITION) && this.state.event === 'PENDING') {
      this.residencesLoad = true;
      this.residencesService
        .listAvailableForAssign()
        .pipe(first(), finalize(() => (this.residencesLoad = false)))
        .subscribe(residences => {
          this.residences = residences.data;
          this.residenceSelect = this.application.residence.name;
          const residence = this.residences.find(data =>
            data.id == (this.application.assigned_residence_id ? this.application.assigned_residence_id : this.application.residence_id)
          );
          if (residence && residence.available_for_assign) {
            this.residence_id = residence.id;
          } else {
            this.residence_id = null;
          }
          this.decision = this.application.decision;
          this.room_id = this.application.room_id;
          this.residenceChanged(this.residence_id);
          this.getRoom(this.residence_id);
        });
      this.getTariffs();
    }

    if (this.application.status === ApplicationStatus.CLOSED && this.state.event === 'CONTRACT') {
      this.getRoom(this.application.residence_id);
      this.getTariffs();
    }

    if (this.application.status === ApplicationStatus.CONFIRMED) {
      this.getRoom(this.application.assigned_residence_id);
      this.getTariffs();
    }
  }

  getTariffs() {
    this.tariffsService.list(0, -1).pipe(first(), finalize(() => (this.tariffLoad = false)))
      .subscribe(tariffs => {
        this.tariffs = tariffs.data;
      });
  }

  getRoom(idResidence) {
    if (idResidence) {
      this.roomsLoad = true;
      this.roomsService
        .freeRooms(idResidence, this.application.id)
        .pipe(first(), finalize(() => (this.roomsLoad = false)))
        .subscribe(rooms => {
          this.rooms = rooms.data;
          if (this.rooms.find(x => x.id === this.application.room_id)) {
            this.room_id = this.application.room_id;
          }
        });
    }

  }

  save() {

    let changes = {};

    return new Promise((resolve, reject) => {

      if ([ApplicationStatus.ANALYSED, ApplicationStatus.QUEUED, ApplicationStatus.OPPOSITION].includes(this.application.status) && this.state.event === "PENDING") {
        if (this.application.status === ApplicationStatus.OPPOSITION && this.opposition_answer === "") {
          reject();
          this.uiService.showMessage(
            MessageType.info,
            'Preencha a resposta a oposição'
          );
          return;
        } else {
          changes = { ...changes, opposition_answer: this.opposition_answer };
        }
        if (this.residence_id === null) {
          reject();
          this.uiService.showMessage(
            MessageType.info,
            'Selecione uma residência'
          );

          return;
        } else if (this.decision === null) {
          reject();
          this.uiService.showMessage(
            MessageType.info,
            'Selecione uma decisão'
          );

          return;
        } else {
          changes = {
            ...changes,
            assigned_residence_id: this.residence_id,
            room_id: this.room_id,
            decision: this.decision
          };
        }
      }
      if (this.application.status === ApplicationStatus.CONFIRMED || (this.application.status === ApplicationStatus.CLOSED && this.state.event === "CONTRACT")) {
        if (this.room_id === null) {
          reject();
          this.uiService.showMessage(MessageType.info, 'Selecione um quarto');
          return;
        }
        if (this.tariff_id === null) {
          reject();
          this.uiService.showMessage(MessageType.info, 'Selecione um tarifário');
          return;
        }
        else {

          changes = { ...changes, assigned_residence_id: this.application.assignedResidence.id, room_id: this.room_id, tariff_id: this.tariff_id };
        }
      }

      if (this.application.status === ApplicationStatus.PENDING) {

        if (!this.isMultiple) {
          if (this.state.event === 'DISPACH_APPPROVE') {
            this.applicationService.acceptDecision(this.application.id)
              .pipe(first(), finalize(() => reject()))
              .subscribe(application => {
                this.close();
                this.accommodationService.updateApplicationsStats();
                resolve(application);
                this.uiService.showMessage(
                  MessageType.success,
                  'Candidatura aceite com sucesso'
                );
              });
          }
          if (this.state.event === 'DISPACH_REJECT') {
            this.applicationService.rejectDecision(this.application.id)
              .pipe(first(), finalize(() => reject()))
              .subscribe(application => {
                this.close();
                this.accommodationService.updateApplicationsStats();
                resolve(application);
                this.uiService.showMessage(
                  MessageType.success,
                  'Candidatura rejeitada com sucesso'
                );
              });
          }
        } else {
          const itemsSelected = this.applications;
          const ids = itemsSelected.map(is => is.id);
          ids.map(id => {
            if (this.state.event === 'DISPACH_APPPROVE') {
              this.applicationService.acceptDecision(id)
                .pipe(first())
                .subscribe(application => {
                  this.accommodationService.updateApplicationsStats();
                });
            }
            if (this.state.event === 'DISPACH_REJECT') {
              this.applicationService.rejectDecision(id)
                .pipe(first())
                .subscribe(application => {
                  this.accommodationService.updateApplicationsStats();
                });
            }
          });
          resolve(this.application);
          if (this.state.event === 'DISPACH_REJECT') {
            this.uiService.showMessage(
              MessageType.success,
              'Candidatura rejeitada com sucesso'
            );
          } else {
            this.uiService.showMessage(
              MessageType.success,
              'Candidatura aceite com sucesso'
            );
          }
          this.close();
        }
        return;
      }



      if (!this.isMultiple) {

        if (this.state.event === 'PENDING' && (changes['decision'] === 'ASSIGN')) {
          if (this.residenceOccupation.totalAvailable < 1) {
            this.uiService.showMessage(
              MessageType.error,
              'Não existe vagas livres para esta residência'
            );
            reject();
            return;
          }
        }
        this.applicationService
          .changeStatus(this.application.id, {
            event: this.state.event,
            notes: this.notes,
            application: changes
          })
          .pipe(first(), finalize(() => reject()))
          .subscribe(application => {
            this.close();
            this.accommodationService.updateApplicationsStats();
            resolve(application);
            this.uiService.showMessage(
              MessageType.success,
              'Estado alterado com sucesso'
            );
          });
      } else {
        const itemsSelected = this.applications;
        const ids = itemsSelected.map(is => is.id);

        if (this.state.event === 'PENDING' && (changes['decision'] === 'ASSIGN')) {
          if (this.residenceOccupation.totalAvailable < this.totalSelected) {
            this.uiService.showMessage(
              MessageType.error,
              'Não existe vagas livres para esta residência'
            );
            reject();
            return;
          }
        }

        this.applicationService.changeStatusBulk({
          event: this.state.event,
          notes: this.notes,
          ids,
          extra_data: changes
        }).pipe(first(), finalize(() => reject()))
          .subscribe(applications => {
            this.close();
            this.accommodationService.updateApplicationsStats();
            resolve(applications);
            this.uiService.showMessage(
              MessageType.success,
              'Estados alterados com sucesso'
            );
          });
      }
    });
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }

  residenceChanged(residence_id) {
    if (residence_id && this.application) {
      this.residencesService.occupation(residence_id, this.application.academic_year).pipe(first()).subscribe(occupation => {
        this.residenceOccupation = occupation.data[0];
      });
    }

  }

}
