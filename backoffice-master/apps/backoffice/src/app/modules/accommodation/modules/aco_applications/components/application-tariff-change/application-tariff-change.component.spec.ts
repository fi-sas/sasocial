import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationTariffChangeComponent } from './application-tariff-change.component';

describe('ApplicationTariffChangeComponent', () => {
  let component: ApplicationTariffChangeComponent;
  let fixture: ComponentFixture<ApplicationTariffChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationTariffChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationTariffChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
