import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { TariffChangeRequestsService } from '../../../tariff-change-requests/services/tariff-change-requests.service';
import { ApplicationModel } from '../../models/application.model';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import * as moment from 'moment';
import { TariffsService } from '../../../tariffs/services/tariffs.service';

@Component({
  selector: 'fi-sas-application-tariff-change',
  templateUrl: './application-tariff-change.component.html',
  styleUrls: ['./application-tariff-change.component.less']
})
export class ApplicationTariffChangeComponent implements OnInit {

  application: ApplicationModel = null;

  tariffChangeForm = new FormGroup({
    tariff_id: new FormControl(null, []),
    start_date: new FormControl('', [Validators.required]),
    has_scholarship: new FormControl(false, [Validators.required]),
    application_id: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
  });
  tariffs: TariffModel[] = [];
  tariffsLoading = false;

  get f() { return this.tariffChangeForm.controls; }

  constructor(
    private uiService: UiService,
    private modalRef: NzModalRef,
    private tariffChangeRequestsService: TariffChangeRequestsService,
    private tariffsService: TariffsService
  ) { }

  ngOnInit() {
    this.loadTariffs();
    this.tariffChangeForm.get('application_id').setValue(this.application.id);
  }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }

  submit() {
    for (const i in this.tariffChangeForm.controls) {
      this.tariffChangeForm.controls[i].markAsDirty();
      this.tariffChangeForm.controls[i].updateValueAndValidity();
    }

    if (this.tariffChangeForm.invalid) {
      this.uiService.showMessage(MessageType.error, "Erro no formulário");
      return;
    }
    this.tariffChangeRequestsService.create(this.tariffChangeForm.value).pipe(first(),
      finalize(() => this.tariffsLoading = false))
      .subscribe(results => {
        this.uiService.showMessage(MessageType.success, 'Pedido de alteração de regime de bolsa criado com sucesso');
        this.close();
      });
  }

  loadTariffs() {
    this.tariffsLoading = true;
    this.tariffsService.list(0, -1, null, null, { active: true })
      .pipe(first(),
        finalize(() => this.tariffsLoading = false))
      .subscribe(results => {
        this.tariffs = results.data;
      });
  }
}
