import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegimeExtrasChangeModelComponent } from './regime-extras-change-model.component';

describe('RegimeExtrasChangeModelComponent', () => {
  let component: RegimeExtrasChangeModelComponent;
  let fixture: ComponentFixture<RegimeExtrasChangeModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegimeExtrasChangeModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegimeExtrasChangeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
