import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ExtraChangeRequestsService } from '../../../extra-change-requests/services/extra-change-requests.service';
import { ExtraModel } from '../../../extras/models/extra.model';
import { RegimeChangeRequestsService } from '../../../regime-change-requests/services/regime-change-requests.service';
import { RegimeModel } from '../../../regimes/models/regime.model';
import { ResidencesService } from '../../../residences/services/residences.service';
import { ApplicationModel } from '../../models/application.model';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-regime-extras-change-model',
  templateUrl: './regime-extras-change-model.component.html',
  styleUrls: ['./regime-extras-change-model.component.less']
})
export class RegimeExtrasChangeModelComponent implements OnInit {

  application: ApplicationModel = null;
  type: string = null;

  regimes: RegimeModel[] = [];
  extras: ExtraModel[] = [];

  regimeExtrasChangeForm = new FormGroup({
    extra_ids: new FormControl(null, []),
    regime_id: new FormControl(null, []),
    start_date: new FormControl('', [Validators.required]),
    application_id: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
  });


  constructor(
    private uiService: UiService,
    private modalRef: NzModalRef,
    private residencesService: ResidencesService,
    private regimeChangeRequest: RegimeChangeRequestsService,
    private extrasChangeRequest: ExtraChangeRequestsService,

  ) {
  }

  ngOnInit() {
    this.loadResidence(this.application.assigned_residence_id);
    if (this.type == 'REGIME')
      this.regimeExtrasChangeForm.patchValue({ application_id: this.application.id, regime_id: this.application.regime_id });
    else
      this.regimeExtrasChangeForm.patchValue({ application_id: this.application.id, extra_ids: this.application.extras.map(x => x.id) });
  }

  get f() { return this.regimeExtrasChangeForm.controls; }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }

  submit() {
    for (const i in this.regimeExtrasChangeForm.controls) {
      this.regimeExtrasChangeForm.controls[i].markAsDirty();
      this.regimeExtrasChangeForm.controls[i].updateValueAndValidity();
    }
    if (this.regimeExtrasChangeForm.invalid) {
      this.uiService.showMessage(MessageType.error, "Erro no formulário");
      return;
    }
    let title = '';
    switch (this.type) {
      case 'REGIME':
        title = 'Pedido de alteração de regime submetido com sucesso';
        break;
      case 'EXTRA':
        title = 'Pedido de alteração de extras submetido com sucesso';
        break;
      default:
        title = '';
    }
    if (this.type == "REGIME") {
      this.regimeChangeRequest.create(this.regimeExtrasChangeForm.value)
        .pipe(first())
        .subscribe(response => {
          this.uiService.showMessage(MessageType.success, title);
          this.close();
        });
    } else {
      let error = false;
      if(this.application.extras.length>0 && this.application.extras.length == this.regimeExtrasChangeForm.get('extra_ids').value.length) {
        let aux = [];
        this.application.extras.forEach((appl)=>{
          this.regimeExtrasChangeForm.get('extra_ids').value.forEach(form => {
            if(appl.id == form) {
              aux.push(form);
            }
          });
        })
        if(aux.length>0 && aux.length == this.application.extras.length){
          error = true;
        }

      }
      if(error) {
        this.uiService.showMessage(MessageType.error, 'Não pode inserir os mesmos extras');
        return;
      }
      this.extrasChangeRequest.create(this.regimeExtrasChangeForm.value)
        .pipe(first())
        .subscribe(response => {
          this.uiService.showMessage(MessageType.success, title);
          this.close();
        });
    }
  }

  loadResidence(residence_id: number) {
    this.residencesService.read(residence_id)
      .pipe(first())
      .subscribe(residence => {
        this.regimes = residence.data[0].regimes.filter((data)=> {
          return data.regime_id != this.application.current_regime_id;
        });

        this.extras = residence.data[0].extras;
      });
  }
}
