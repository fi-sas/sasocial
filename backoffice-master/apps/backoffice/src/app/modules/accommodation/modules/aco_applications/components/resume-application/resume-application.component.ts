import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FilesService } from "@fi-sas/backoffice/modules/medias/services/files.service";
import { DocumentTypeModel } from "@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model";
import { first } from "rxjs/operators";
import { ExtraModel } from "../../../extras/models/extra.model";
import { RegimeModel } from "../../../regimes/models/regime.model";
import { TypologyModel } from "../../../typologies/models/typology.model";
import { ApplicationModel } from "../../models/application.model";

@Component({
    selector: 'app-resume-application',
    templateUrl: './resume-application.component.html',
    styleUrls: ['./resume-application.component.less']
})
export class ResumeApplicationComponent implements OnInit {
    @Input() application: ApplicationModel = new ApplicationModel();
    @Input() nameResidenceSelected: string = '';
    @Input() nameUsedResidenceSelected: string = '';
    @Output() currentValue = new EventEmitter;
    @Input() nameRoom: string = '';
    @Input() nameOrganic: string = ''
    @Input() nameCourseDegree: string = '';
    @Input() nameCourse: string = '';
    @Input() descPatrimony: string = '';
    @Input() sendRegime: RegimeModel;
    @Input() residenceName: string = '';
    @Input() extrasSend: ExtraModel[] = [];
    @Input() documentType: DocumentTypeModel;
    @Input() year: number;
    @Input() sendTypologies: TypologyModel;

    loadingFiles: boolean = false;
    filesIncomeStatement = [];
    filesOthers = [];
    constructor(private fileService: FilesService) { }

    ngOnInit() {
        if(this.application.income_statement_files_ids && this.application.income_statement_files_ids.length>0) {
            this.loadMedias();
        }
        if(this.application.file_ids && this.application.file_ids.length>0) {
            this.loadFiles();
       }
    }

    loadMedias() {
        this.fileService
            .listFilesByIDS(this.convertGalleryToFilesIDS())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.filesIncomeStatement = files.data;
                    this.loadingFiles = false;
                },
                () => {
                    this.loadingFiles = false;
                }
            );
    }

    loadFiles() {
        this.fileService
            .listFilesByIDS(this.convertGalleryToFilesIDSOthers())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.filesOthers = files.data;
                    this.loadingFiles = false;
                },
                () => {
                    this.loadingFiles = false;
                }
            );
    }

    convertGalleryToFilesIDS() {
        let filesIDS = [];
        this.application.income_statement_files_ids.forEach((fileID) => {
            filesIDS.push(fileID);
        });

        return filesIDS;
    }

    convertGalleryToFilesIDSOthers() {
        let filesIDS = [];
        this.application.file_ids.forEach((fileID) => {
            filesIDS.push(fileID);
        });

        return filesIDS;
    }


    changeValue(value: number) {
        this.currentValue.emit(value);
    }
}