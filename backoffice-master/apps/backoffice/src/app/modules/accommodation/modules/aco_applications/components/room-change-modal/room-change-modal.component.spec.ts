import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomChangeModalComponent } from './room-change-modal.component';

describe('RoomChangeModalComponent', () => {
  let component: RoomChangeModalComponent;
  let fixture: ComponentFixture<RoomChangeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomChangeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomChangeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
