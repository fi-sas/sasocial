import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RoomChangeRequestsService } from '@fi-sas/backoffice/modules/accommodation/services/room-change-requests.service';
import { NzModalRef } from 'ng-zorro-antd';
import { debounceTime, distinctUntilChanged, finalize, first, switchMap } from 'rxjs/operators';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { ResidencesService } from '../../../residences/services/residences.service';
import { TypologyModel } from '../../../typologies/models/typology.model';
import { ApplicationModel, ApplicationStatus, ApplicationStatusTranslations } from '../../models/application.model';
import * as moment from 'moment';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Resource } from '@fi-sas/core';
import { ApplicationsService } from '../../services/applications.service';
import { RoomsService } from '../../../rooms/services/rooms.service';


@Component({
  selector: 'fi-sas-room-change-modal',
  templateUrl: './room-change-modal.component.html',
  styleUrls: ['./room-change-modal.component.less']
})
export class RoomChangeModalComponent implements OnInit {

  application: ApplicationModel = null;
  type: string = null;

  typologies: TypologyModel[] = [];
  typologiesLoading = false;

  residences: ResidenceModel[] = [];
  residencesLoading = false;

  roomChangeForm = new FormGroup({
    typology_id: new FormControl(null, []),
    residence_id: new FormControl(null, []),
    permute_user_tin: new FormControl(null, []),
    room_id: new FormControl(null, []),
    start_date: new FormControl('', [Validators.required]),
    application_id: new FormControl(null, [Validators.required]),
    type: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
  });
  get f() { return this.roomChangeForm.controls; }
  optionListSubscrition: Subscription = null;
  searchChange$ = new BehaviorSubject('');
  applications: ApplicationModel[] = [];
  applications_loading = false;
  ApplicationStatus = ApplicationStatus;
  ApplicationStatusTranslations = ApplicationStatusTranslations;

  typology_id: number = null;

  free_rooms = [];
  roomsLoading = true;
  constructor(
    private uiService: UiService,
    private modalRef: NzModalRef,
    private residencesService: ResidencesService,
    private roomChangeRequestsService: RoomChangeRequestsService,
    private applicationsServices: ApplicationsService,
    private ref: ChangeDetectorRef,
    private roomsService: RoomsService
  ) { }


  ngOnInit() {
    this.roomChangeForm.patchValue({ application_id: this.application.id, type: this.type });
    if (this.type == 'TYPOLOGY') {
      this.loadResidenceTypologies(this.application.assigned_residence_id);
      this.roomChangeForm.controls.typology_id.setValidators(Validators.required);
    } else if (this.type == 'RESIDENCE') {
      this.roomChangeForm.controls.residence_id.setValidators(Validators.required);
      this.loadResidences();
    } else if (this.type == 'ROOM') {
      this.roomChangeForm.controls.room_id.setValidators(Validators.required);
      this.loadFreeRooms();
    } else {
      this.roomChangeForm.controls.permute_user_tin.setValidators(Validators.required);
      const optionList$: Observable<Resource<ApplicationModel>> = this.searchChange$
        .asObservable()
        .pipe(
          // tap(() => (this.applications_loading = true)),
          debounceTime(500),
          distinctUntilChanged(),
          switchMap(search =>
            this.applicationsServices.list(0, 10, null, null, {
              withRelated: "room,assignedResidence",
              status: 'contracted',
              search,
              searchFields: 'full_name,tin,student_number',
              sort: 'full_name'
            }).pipe(first(), finalize(() => {
              if (!this.ref['destroyed']) {
                this.ref.detectChanges();
              }
            }))
          ),
        );
      this.optionListSubscrition = optionList$.subscribe(results => {
        this.applications = [...results.data];
        this.applications_loading = false;
      });
    }

  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }
  // disabledDate = (current: Date) => {
  //   return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  // }

  loadResidenceTypologies(residence_id: number) {
    this.typologiesLoading = true;
    this.residencesService.read(residence_id)
      .pipe(first(), finalize(() => { this.typologiesLoading = false; }))
      .subscribe(residence => {
        this.typologies = residence.data[0].typologies;
      });
  }

  loadResidences() {
    this.residencesLoading = true;
    this.residencesService.list(0, -1, null, null, { active: true })
      .pipe(first(), finalize(() => { this.residencesLoading = false; }))
      .subscribe(residences => {
        this.residences = residences.data.filter((data)=> {
          return data.id != this.application.assignedResidence.id;
        });
      });
  }

  loadFreeRooms() {
    this.roomsService.freeRooms(this.application.assigned_residence_id, this.application.id).pipe(
      first(),
      finalize(() => this.roomsLoading = false)
    ).subscribe(result => {
      this.free_rooms = result.data;
    });
  }


  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }
  submit() {
    for (const i in this.roomChangeForm.controls) {
      this.roomChangeForm.controls[i].markAsDirty();
      this.roomChangeForm.controls[i].updateValueAndValidity();
    }
    if (this.roomChangeForm.invalid) {
      this.uiService.showMessage(MessageType.error, "Erro no formulário");
      return;
    }

    this.roomChangeRequestsService.create(this.roomChangeForm.value)
      .pipe(first(), finalize(() => { this.residencesLoading = false; }))
      .subscribe(response => {
        let title = '';
        switch (this.type) {
          case 'TYPOLOGY':
            title = 'Pedido de alteração de tipologia submetido com sucesso';
            break;
          case 'RESIDENCE':
            title = 'Pedido de alteração de residência submetido com sucesso';
            break;
          case 'PERMUTE':
            title = 'Pedido de permuta de quarto submetido com sucesso';
            break;
          case 'ROOM':
            title = 'Pedido de alteração de quarto submetido com sucesso';
            break;
          default:
            title = '';
        }
        this.uiService.showMessage(MessageType.success, title);
        this.close();
      });
  }
}
