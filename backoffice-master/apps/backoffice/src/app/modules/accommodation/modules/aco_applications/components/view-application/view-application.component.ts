import { ApplicationsService } from '../../services/applications.service';
import {
  ApplicationStatusMachine,
  ApplicationStatusTranslations,
  ApplicationDecision,
  ApplicationDecisionList,
  ApplicationStatus,
  ApplicationModel,
} from '../../models/application.model';
import {
  Component,
  OnInit,
  ChangeDetectorRef,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { first, finalize } from 'rxjs/operators';
import { ResidencesService } from '../../../residences/services/residences.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { RoomModel } from '../../../rooms/models/room.model';
import { RoomsService } from '../../../rooms/services/rooms.service';
import { ExtraChangeRequestsService } from '../../../extra-change-requests/services/extra-change-requests.service';
import { RegimeChangeRequestsService } from '../../../regime-change-requests/services/regime-change-requests.service';
import { ExtraChangeRequestDecisionTranslations, ExtraChangeRequestModel, ExtraChangeRequestStatusTranslations } from '../../../extra-change-requests/models/Extra-change-request.model';
import { RegimeChangeRequestModel } from '../../../regime-change-requests/models/Regime-change-request.model';
import { RoomChangeRequestsService } from '@fi-sas/backoffice/modules/accommodation/services/room-change-requests.service';
import { RoomChangeRequestModel } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';
import { WithdrawalModel } from '../../../withdrawals/models/withdrawal.model';
import { WithdrawalsService } from '../../../withdrawals/services/withdrawals.service';
import { ExtensionModel } from '../../../extensions/models/extension.model';
import { ExtensionsService } from '../../../extensions/services/extensions.service';
import { AbsencesService } from '../../../absences/services/absences.service';
import { AbsenceDecisionTranslations, AbsenceModel, AbsenceStatusTranslations } from '../../../absences/models/absence.model';
@Component({
  selector: 'fi-sas-view-application',
  templateUrl: './view-application.component.html',
  styleUrls: ['./view-application.component.less'],
})
export class ViewApplicationComponent implements OnInit {
  ApplicationStatus = ApplicationStatus;
  DEFAULT_LANG_ID = null;
  filesIncomeStatement = [];
  @Input() application: ApplicationModel = null;
  @Output() applicationChange = new EventEmitter();
  @Output() applicationRemove = new EventEmitter();

  editMode = false;
  ApplicationStatusTranslations = ApplicationStatusTranslations;
  statusMachine = ApplicationStatusMachine;

  ApplicationDecision = ApplicationDecision;
  ApplicationDecisionList = ApplicationDecisionList;
  decision: ApplicationDecision = null;
  decisionLoad = false;
  residenceSelect: string = '';

  @Input() residences: ResidenceModel[] = [];
  residencesLoad = false;
  residence_id: number = null;
  filesOthers = [];
  room_id = null;
  rooms: RoomModel[] = [];
  roomsLoad = false;
  modalPerfil = false;
  extraChangeResquests: ExtraChangeRequestModel[] = null;
  regimeChangeResquests: RegimeChangeRequestModel[] = null;
  typologyChangeRequests: RoomChangeRequestModel[] = null;
  residenceChangeRequests: RoomChangeRequestModel[] = null;
  permuteChangeRequests: RoomChangeRequestModel[] = null;
  ExtrasStatusTranslations = ExtraChangeRequestStatusTranslations;
  ExtrasDecisionTranslations = ExtraChangeRequestDecisionTranslations;
  withdrawalRequests: WithdrawalModel[] = null;
  extensionRequests: ExtensionModel[] = null;
  absenceRequests: AbsenceModel[] = null;
  AbsenceStatusTranslations = AbsenceStatusTranslations;
  AbsenceDecisionTranslations = AbsenceDecisionTranslations;


  constructor(
    private configurator: FiConfigurator,
    private residencesService: ResidencesService,
    private applicationService: ApplicationsService,
    private ref: ChangeDetectorRef,
    private roomsService: RoomsService,
    private extraChangesService: ExtraChangeRequestsService,
    private regimeChangesService: RegimeChangeRequestsService,
    private roomChangeRequestsService: RoomChangeRequestsService,
    private withdrawalsService: WithdrawalsService,
    private extensionsService: ExtensionsService,
    private absencesService: AbsencesService
  ) {
    this.DEFAULT_LANG_ID = this.configurator.getOption('DEFAULT_LANG_ID');
  }

  ngOnInit() {
    if (this.application.income_statement_files.length > 0) {
      this.application.income_statement_files.forEach((element) => {
        this.filesIncomeStatement.push(element.file);
      });
    }
    if (this.application.files && this.application.files.length > 0) {
      this.application.files.forEach(element => {
          this.filesOthers.push(element);
      });
  }
    if (this.residences.length > 0) {
      this.residences = this.residences.filter((data) => {
        return data.available_for_assign == true;
      });
    }

    this.changeApplicationData(this.application);
  }

  changeApplicationData(application: ApplicationModel) {
    this.application = application;

    if (
      this.application.status === 'analysed' ||
      this.application.status === 'queued'
    ) {
      this.loadResidences();
    }

    if (this.application) {
      this.decision = this.application.decision;
      this.residenceSelect = this.application.residence.name;
      const residence = this.residences.find(
        (data) => data.id == this.application.assigned_residence_id
      );
      if (residence && residence.available_for_assign) {
        this.residence_id = residence.id;
      } else {
        this.residence_id = null;
      }
      this.room_id = this.application.room_id;

      if (
        this.application.status === 'analysed' ||
        this.application.status === 'queued'
      ) {
        this.getRoom(this.residence_id);
      }
    }

    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }
  }

  getRoom(idResidence) {
    if (idResidence) {
      this.roomsLoad = true;
      this.roomsService
        .freeRooms(idResidence, this.application.id)
        .pipe(
          first(),
          finalize(() => (this.roomsLoad = false))
        )
        .subscribe((rooms) => {
          this.rooms = rooms.data;
        });
    }
  }

  loadResidences() {
    if (this.residences.length === 0 && !this.residencesLoad) {
      this.residencesLoad = true;
      this.residencesService
        .listAvailableForAssign()
        .pipe(
          first(),
          finalize(() => (this.residencesLoad = false))
        )
        .subscribe((results) => {
          this.residences = results.data;
        });
    }
  }

  saveDecision() {
    if (this.residence_id && this.decision) {
      this.decisionLoad = true;
      this.applicationService
        .patch(this.application.id, {
          decision: this.decision,
          assigned_residence_id: this.residence_id,
          room_id: this.room_id,
        })
        .pipe(
          first(),
          finalize(() => (this.decisionLoad = false))
        )
        .subscribe((result) => {
          this.application = result.data[0];
          this.applicationChange.emit(this.application);
        });
    }
  }

  resetList() {
    this.applicationRemove.emit(true);
  }

  loadContractChanges() {
    if (this.extraChangeResquests == null)
      this.extraChangesService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: "extras" }).pipe(
        first(),
      ).subscribe((response) => {
        this.extraChangeResquests = response.data;
      });
    if (!this.regimeChangeResquests)
      this.regimeChangesService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: "regime" }).pipe(
        first(),
      ).subscribe((response) => {
        this.regimeChangeResquests = response.data;
      });
    if (!this.typologyChangeRequests)
      this.roomChangeRequestsService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: "residence,typology" }).pipe(
        first(),
      ).subscribe((response) => {
        this.typologyChangeRequests = response.data.filter(x => x.type == 'TYPOLOGY');
        this.residenceChangeRequests = response.data.filter(x => x.type == 'RESIDENCE');
        this.permuteChangeRequests = response.data.filter(x => x.type == 'RESIDENCE');
      });
    if (!this.withdrawalRequests)
      this.withdrawalsService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: false }).pipe(
        first(),
      ).subscribe((response) => {
        this.withdrawalRequests = response.data;
      });
    if (!this.extensionRequests)
      this.extensionsService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: false }).pipe(
        first(),
      ).subscribe((response) => {
        this.extensionRequests = response.data;
      });
    if (!this.absenceRequests)
      this.absencesService.list(0, 5, "created_at", "DESC", { application_id: this.application.id, withRelated: false }).pipe(
        first(),
      ).subscribe((response) => {
        this.absenceRequests = response.data;
      });

  }

}
