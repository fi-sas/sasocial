import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { WithdrawalsService } from '../../../withdrawals/services/withdrawals.service';
import { ApplicationModel } from '../../models/application.model';

@Component({
  selector: 'fi-sas-withdrawal-modal',
  templateUrl: './withdrawal-modal.component.html',
  styleUrls: ['./withdrawal-modal.component.less']
})
export class WithdrawalModalComponent implements OnInit {
  
  application: ApplicationModel = null;

  withdrawalForm = new FormGroup({
    application_id: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
    end_date: new FormControl(null, [Validators.required]),
  });

  submitted = false;
  isLoading = false;

  constructor(
    private modalRef: NzModalRef,
    private withdrawalsService : WithdrawalsService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.withdrawalForm.patchValue({ application_id: this.application.id });
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }
  
  submit() {
    this.submitted = true;
    if (this.withdrawalForm.valid) {
      this.isLoading = true;
      this.withdrawalsService.create(this.withdrawalForm.value).pipe(first(), finalize(()=> this.isLoading = false)).subscribe(()=> {
        this.uiService.showMessage(MessageType.success, 'Pedido de desistência de alojamento submetido com sucesso');
        this.close();
      })
    }else{
      this.submitted = false;
    }
  }
}
