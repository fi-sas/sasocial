export class ApplicationData {
  user_id?: number;
  start_date?: Date;
  end_date?: Date;
  assigned_residence_id?: number;
  room_id?: number;
  // has_scholarship: boolean;
  signed_contract_file_id?: number;
  liability_term_file_id?: number;
  extra_ids?: number[];
  regime_id?: number;
  per_capita_income?: number;
  // tariff_id?: number;
  reprocess_since?: Date;
  international_student?: boolean;
  constructor() { }
}
