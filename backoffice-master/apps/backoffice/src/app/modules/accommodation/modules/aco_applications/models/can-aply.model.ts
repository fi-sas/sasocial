export class CanAplyApplicationModel {
    can: boolean;
    reason: string;
    renew: boolean;
    renew_application_id: number;
}