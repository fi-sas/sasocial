import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsReportsComponent } from './applications-reports.component';

describe('ApplicationsReportsComponent', () => {
  let component: ApplicationsReportsComponent;
  let fixture: ComponentFixture<ApplicationsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
