import { Component, OnInit } from '@angular/core';
import { ResidencesService } from '../../../residences/services/residences.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { tap, first, finalize } from 'rxjs/operators';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { FiConfigurator } from '@fi-sas/configurator';
import { ApplicationPhaseModel } from '../../../phases/models/application-phase.model';
import { ApplicationPhaseService } from '../../../phases/services/application-phase.service';
import { ReportsService } from '../../services/reports.service';
import { NzMessageService } from 'ng-zorro-antd';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';

@Component({
  selector: 'fi-sas-applications-reports',
  templateUrl: './applications-reports.component.html',
  styleUrls: ['./applications-reports.component.less'],
})
export class ApplicationsReportsComponent implements OnInit {
  file_types = [];
  file_type = null;

  residences: ResidenceModel[] = [];
  selectedResidence_report1: ResidenceModel = null;
  selectedResidence_report2: ResidenceModel = null;
  selectedResidence_report3: ResidenceModel = null;
  selectedResidence_report4: ResidenceModel = null;

  phases: ApplicationPhaseModel[] = [];

  raw_phases_loading = false;
  raw_phases: ApplicationPhaseModel[] = [];
  selectedPhase: ApplicationPhaseModel = null;
  raw_selected_phase: ApplicationPhaseModel = null;

  selected_residence_sholarship_map: ResidenceModel = null;
  selected_phase_scholarship_map: ApplicationPhaseModel = null;
  scolarship_map_loading = false;

  selected_residence_regime_map: ResidenceModel = null;
  selected_phase_regime_map: ApplicationPhaseModel = null;
  regime_map_loading = false;

  selected_residence_extras_map: ResidenceModel = null;
  selected_phase_extras_map: ApplicationPhaseModel = null;
  extras_map_loading = false;

  selected_residence_process: ResidenceModel = null;
  academic_year_process = null;
  month_year_process = null;
  process_loading = false;

  alocanres_loading = false;

  alocanbruto_loading = false;
  academic_years = [];
  academic_year = null;
  aloclothesmap_loading = false;

  selected_residence_sepa: ResidenceModel = null;
  month_year_sepa = null;
  generateSepaFile_loading = false;

  selected_residence_tariff: ResidenceModel = null;
  academic_year_tariff = null;
  tariffs: TariffModel[] = [];
  selected_tariff: TariffModel = null;
  month_year_tariff = null;
  alotariffsmap_loading = false;

  constructor(
    private reportsService: ReportsService,
    private residencesService: ResidencesService,
    private phasesService: ApplicationPhaseService,
    private messageService: NzMessageService,
    private tariffsService: TariffsService,
    private accommodationService: AccommodationService
  ) {
    for (let i = -1; i < 5; i++) {
      this.academic_years.push(
        moment()
          .subtract(i + 1, 'year')
          .year() +
          '-' +
          moment().subtract(i, 'year').year()
      );
    }
    this.academic_year =
      this.academic_years.length > 0 ? this.academic_years[0] : null;
  }

  ngOnInit() {
    // this.file_types = this.configurator.getOptionTree('FILE_TYPES', false);
    // this.file_type = this.file_types.length > 0 ? this.file_types[0] : null;
    const data = this.accommodationService.getAccommodationData();
    this.residencesService.list(1, -1).subscribe((residences) => {
      this.residences = residences.data;
      if (residences.data.length > 0) {
        this.selectedResidence_report1 = residences.data[0];
        this.selectedResidence_report2 = residences.data[0];
        this.selectedResidence_report3 = residences.data[0];
        this.selectedResidence_report4 = residences.data[0];
        this.selected_residence_process = residences.data[0];
        this.selected_residence_sepa = residences.data[0];
      }
    });
    this.loadPhases(data.academicYear);
    this.onChangeAcademicYearRaw();
    this.loadTariffs();
  }

  loadPhases(academicYear) {
    this.selectedPhase = null;
    this.alocanres_loading = true;
    this.phasesService.getPhasesByAcademicYear(academicYear).pipe(first(), finalize(() => this.alocanres_loading = false))
      .subscribe(response => this.phases = response.data);
  }

  loadTariffs() {
    this.tariffsService
      .list(1, -1)
      .pipe(first())
      .subscribe((response) => {
        this.tariffs = response.data;
      });
  }

  applications_results() {
    if (!this.selectedPhase) {
      return this.messageService.warning('Selecione a fase');
    }

    this.reportsService
      .getApplicationsResultsReport(
        this.selectedResidence_report1.id,
        this.selectedPhase.id
      )
      .pipe(
        first(),
        tap(() => (this.alocanres_loading = true)),
        finalize(() => (this.alocanres_loading = false))
      )
      .subscribe((result) => {
        return this.messageService.success('Relatório gerado com sucesso');
      });
  }

  onChangeAcademicYearRaw(){
    if(this.academic_year){
      this.raw_phases_loading = true;
      this.phasesService.getPhasesByAcademicYear(this.academic_year).pipe(first(), finalize(() => this.raw_phases_loading = false))
      .subscribe(response => this.raw_phases = response.data);
    }
  }

  applications_raw() {
    if (this.academic_year) {
      this.reportsService
        .getApplicationsRawReport(
          this.academic_year,
          this.selectedResidence_report2
            ? this.selectedResidence_report2.id
            : null,
          this.raw_selected_phase ? this.raw_selected_phase.id : null
        )
        .pipe(
          first(),
          tap(() => (this.alocanbruto_loading = true)),
          finalize(() => (this.alocanbruto_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar um ano letivo para gerar o relatório.'
      );
    }
  }

  clothes_map() {
    if (this.selectedResidence_report3) {
      this.reportsService
        .getClothingMap(this.selectedResidence_report3.id)
        .pipe(
          first(),
          tap(() => (this.aloclothesmap_loading = true)),
          finalize(() => (this.aloclothesmap_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar uma residência para gerar o relatório.'
      );
    }
  }

  applications_scholarship_map() {
    if (this.selected_phase_scholarship_map) {
      this.reportsService
        .getScholarshipMap(
          this.selected_residence_sholarship_map
            ? this.selected_residence_sholarship_map.id
            : null,
          this.selected_phase_scholarship_map.id
        )
        .pipe(
          first(),
          tap(() => (this.scolarship_map_loading = true)),
          finalize(() => (this.scolarship_map_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar uma fase para gerar o relatório.'
      );
    }
  }

  applications_regime_map() {
    if (this.selected_phase_regime_map) {
      this.reportsService
        .getRegimeMap(
          this.selected_residence_regime_map
            ? this.selected_residence_regime_map.id
            : null,
          this.selected_phase_regime_map.id
        )
        .pipe(
          first(),
          tap(() => (this.regime_map_loading = true)),
          finalize(() => (this.regime_map_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar uma fase para gerar o relatório.'
      );
    }
  }

  applications_extras_map() {
    if (this.selected_phase_extras_map) {
      this.reportsService
        .getExtrasMap(
          this.selected_residence_extras_map
            ? this.selected_residence_extras_map.id
            : null,
          this.selected_phase_extras_map.id
        )
        .pipe(
          first(),
          tap(() => (this.extras_map_loading = true)),
          finalize(() => (this.extras_map_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar uma fase para gerar o relatório.'
      );
    }
  }

  reportProcess() {
    if (this.academic_year_process) {
      let month = moment(this.month_year_process).month();
      this.process_loading = true;
      this.reportsService
        .getProcess(
          this.selected_residence_process
            ? this.selected_residence_process.id
            : null,
          this.academic_year_process,
          this.month_year_process
            ? moment(this.month_year_process).year().toString()
            : null,
          this.month_year_process
            ? Number(moment().month(month).format('M'))
            : null
        )
        .pipe(
          first(),
          tap(() => (this.process_loading = true)),
          finalize(() => (this.process_loading = false))
        )
        .subscribe((result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error(
        'É necessário selecionar uma ano letivo para gerar o relatório.'
      );
    }
  }

  generateSepaFile() {
    let month = moment(this.month_year_sepa).month();
    this.generateSepaFile_loading = true;
    this.reportsService
      .generateURLSepaFile(
        null,
        this.month_year_sepa
          ? moment(this.month_year_sepa).year().toString()
          : null,
        this.month_year_sepa ? Number(moment().month(month).format('M')) : null
      )
      .pipe(
        first(),
        tap(() => (this.generateSepaFile_loading = true)),
        finalize(() => (this.generateSepaFile_loading = false))
      )
      .subscribe(
        (result) => {
          return this.messageService.success('Ficheiro gerado com sucesso');
        },
        (err) => {
          console.error(err);

          if (err.error) {
            return this.messageService.warning(JSON.stringify(err.error));
          }
        }
      );
  }
  reportSepa() {
    let month = moment(this.month_year_sepa).month();
    this.generateSepaFile_loading = true;
    this.reportsService
      .getReportSepa(
        this.selected_residence_sepa ? this.selected_residence_sepa.id : null,
        this.month_year_sepa
          ? moment(this.month_year_sepa).year().toString()
          : null,
        this.month_year_sepa ? Number(moment().month(month).format('M')) : null
      )
      .pipe(
        first(),
        tap(() => (this.generateSepaFile_loading = true)),
        finalize(() => (this.generateSepaFile_loading = false))
      )
      .subscribe(
        (result) => {
          return this.messageService.success('Relatório gerado com sucesso');
        },
        (err) => {
          console.error(err);

          if (err.error) {
            return this.messageService.warning(JSON.stringify(err.error));
          }
        }
      );
  }

  tariffs_map() {
    if (this.academic_year_tariff) {
      let month = moment(this.month_year_tariff).month();
      this.reportsService
        .getTariffsMap(
          this.selected_residence_tariff
            ? this.selected_residence_tariff.id
            : null,
          this.academic_year_tariff,
          this.selected_tariff ? this.selected_tariff.id : null,
          this.month_year_tariff
            ? moment(this.month_year_tariff).year().toString()
            : null,
          this.month_year_tariff
            ? Number(moment().month(month).format('M'))
            : null
        )
        .pipe(
          first(),
          tap(() => (this.alotariffsmap_loading = true)),
          finalize(() => (this.alotariffsmap_loading = false))
        )
        .subscribe(() => {
          return this.messageService.success('Relatório gerado com sucesso');
        });
    } else {
      return this.messageService.error('Ano Lectivo é obrigatório');
    }
  }
}
