import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ResidencesService } from '../../../residences/services/residences.service';
import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { first, finalize } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { CoursesDegreesServices } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/courses-degrees.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { NzModalService } from 'ng-zorro-antd';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { RoomMapModel } from '../../../rooms/models/room-map.model';
import { compareTwoFieldsValidation, trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { GeralSettingsService } from '@fi-sas/backoffice/modules/accommodation/services/geral-settings.service';
import { RegimeModel } from '../../../regimes/models/regime.model';
import { ExtraModel } from '../../../extras/models/extra.model';
import { PatrimonyCategoriesModel } from '../../models/patrimony-categories.model';
import { nationalities } from '@fi-sas/backoffice/shared/common/nationalities';
import { PeriodsService } from '../../../period/services/period.service';
import { CanAplyApplicationModel } from '../../models/can-aply.model';
import { DocumentTypeService } from '@fi-sas/backoffice/modules/users/modules/document-type/services/document-type.service';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';
import { TypologyModel } from '../../../typologies/models/typology.model';
import { RoomModel } from '../../../rooms/models/room.model';
import { ApplicationPhaseService } from '../../../phases/services/application-phase.service';
import { ApplicationPhaseModel } from '../../../phases/models/application-phase.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { Location } from '@angular/common';


@Component({
  selector: 'fi-sas-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less']
})
export class FormApplicationComponent implements OnInit {
  fixed: boolean = false;
  index = 'identification';
  canAply: CanAplyApplicationModel;
  other_income: boolean = false;
  studentSelectedFlg = false;
  studentSelected: UserModel;
  year: number = 0;
  isSubmited = false;
  typologies: TypologyModel[] = [];
  sendTypologies: TypologyModel;
  applicationPhase: ApplicationPhaseModel = null;
  disabledIdentification;
  fileListIncomeStatement = [];
  fileListOthers = [];
  residenceId: number = 0;
  current = 0;
  first = true;
  get f() { return this.aplicationForm.controls; }

  residenceApplicationModel = new ApplicationModel();
  nameResidenceSelected: string;
  nameUsedResidenceSelected: string;
  nameRoom: string;
  nameOrganic: string;
  nameCourseDegree: string;
  nameCourse: string;
  descPatrimony: string;
  sendRegime: RegimeModel;
  extrasSend: ExtraModel[] = [];
  residenceName: string;
  documentType: DocumentTypeModel;

  loading: boolean = true;
  loadingResidence: boolean = false;

  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  aplicationForm: FormGroup;

  residences: ResidenceModel[] = [];
  residencesAssign: ResidenceModel[] = [];
  residenceSelected: ResidenceModel[] = [];
  residencesExceptSelect: ResidenceModel[] = [];
  residencesApplication: ResidenceModel[] = [];
  organics: OrganicUnitsModel[] = [];
  regimes: RegimeModel[] = [];;
  roomsList: RoomModel[] = [];
  extrasList: ExtraModel[] = [];;
  allow_optional_residence = false;
  show_iban_on_application_form = false;
  loadingButton = false;
  loadingRoom = false;
  user: UserModel;
  listPatrimony: PatrimonyCategoriesModel[] = [];
  courseDegreesList: any;
  coursesList: any;
  documentTypes: DocumentTypeModel[] = [];
  nationalities = nationalities;
  courseLoading = false;
  previousNumberElements = 1;
  academic_year: string = '';
  startDate: Date;
  endDate: Date;
  applicationPhases: ApplicationPhaseModel[] = [];
  isRenew: boolean = false;
  applicantInfo: ApplicationModel;

  constructor(private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private applicationService: ApplicationsService,
    private residencesService: ResidencesService,
    private coursesDegreesServices: CoursesDegreesServices,
    private roomsService: RoomsService,
    private modal: NzModalService,
    private accommodationService: AccommodationService,
    private uiService: UiService,
    private organicUnitsService: OrganicUnitsService,
    private documentTypeService: DocumentTypeService,
    private settings: GeralSettingsService,
    private periodService: PeriodsService,
    private applicationPhasesService: ApplicationPhaseService,
    private academicYearsService: AcademicYearsService,
    private location: Location
    ) {
  }

  newForm() {
    return this.fb.group({
      // 1 step
      full_name: new FormControl('', [Validators.required, trimValidation]),
      applicant_birth_date: new FormControl(null, [Validators.required]),
      identification: ['', [Validators.required, trimValidation]],
      document_type_id: [null, [Validators.required]],
      tin: ['', [Validators.required, Validators.minLength(9)]],
      gender: [null, [Validators.required]],
      father_name: ['', [Validators.required, trimValidation]],
      mother_name: ['', [Validators.required, trimValidation]],
      incapacity: [false, [Validators.required]],
      incapacity_type: ["", [trimValidation]],
      incapacity_degree: ["", [trimValidation]],
      nationality: [null, [Validators.required]],
      iban: ["", []],
      allow_direct_debit: [false, []],
      address: ['', [Validators.required, trimValidation]],
      city: ['', [Validators.required, trimValidation]],
      postal_code: ['', [Validators.required]],
      country: ['', [Validators.required, trimValidation]],
      household_distance: ['', [Validators.required]],
      // 2 step
      email: ['', [Validators.pattern(this.emailRegex), Validators.required, trimValidation]],
      secundary_email: ['', [Validators.pattern(this.emailRegex), trimValidation]],
      phone_1: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(18)]],
      phone_2: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(18)]],
      // 3 step
      residence_id: [null, [Validators.required]],
      second_option_residence_id: [],
      dateRange: new FormControl([], Validators.required),
      regime_id: [null, [Validators.required]],
      extra_ids: [],
      preferred_typology_id: [null],
      has_used_residences_before: [false],
      which_residence: [null],
      which_room: [null],
      // 4 step
      international_student: new FormControl(false, [Validators.required]),
      organic_unit_id: new FormControl('', [Validators.required]),
      course_degree_id: new FormControl('', [Validators.required]),
      course_id: new FormControl('', [Validators.required]),
      course_year: new FormControl('', [Validators.required]),
      student_number: new FormControl('', [Validators.required, trimValidation]),
      admission_date: new FormControl('', [Validators.required]),
      // 5 step
      income_statement_files_ids: [],
      patrimony_category_id: [null, [Validators.required]],
      income_source: ["", [Validators.required]],
      other_income_source: [""],
      household_elements_number: ['', [Validators.min(0.01)]],
      household_total_income: ['', [Validators.required, Validators.min(1)]],
      income_statement_delivered: [false, [Validators.required]],
      house_hould_elements: this.fb.array([]),
      househould_elements_in_university: [
        "",
        [Validators.required, Validators.min(1)],
      ],
      // 7 step
      applied_scholarship: [false],
      scholarship_value: [''],
      // 9 step
      observations: ['', [trimValidation]],
      file_ids: [],
      // 10 step
      applicant_consent: new FormControl(false, [Validators.requiredTrue])
    }, {
      validator:
        [
          compareTwoFieldsValidation('phone_1', 'phone_2'),
          compareTwoFieldsValidation('email', 'secundary_email')
        ]
    });
  }

  ngOnInit() {
    const data = this.accommodationService.getAccommodationData();
    if (!data.residence && !this.route.snapshot.queryParamMap.get("residence_id")) {
      this.modal.error({
        nzTitle: 'Residência não selecionada',
        nzContent: 'Não existe nenhuma residência selecionada. Por favor selecione uma residência e tente novamente.',
        nzOnOk: () => this.router.navigateByUrl('accommodation/residence-selection')
      });
    }else if(this.route.snapshot.queryParamMap.get("application_id") && this.route.snapshot.queryParamMap.get("residence_id")){
      this.isRenew = true;
      this.applicationService
      .read(parseInt(this.route.snapshot.queryParamMap.get("application_id")))
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        if(result.data.length){
          this.applicantInfo = result.data[0];
          this.aplicationForm = this.newForm();
          this.fillUserInfo(this.applicantInfo);
          this.fillFormOldApplication(this.applicantInfo);
          this.applicationPhasesService.read(parseInt(this.route.snapshot.queryParamMap.get("application_phase_id"))).pipe(first())
          .subscribe(response =>  this.applicationPhase = response.data.length ? response.data[0] : null );
          this.residenceId = parseInt(this.route.snapshot.queryParamMap.get("residence_id"));
          this.getResidenceSelected(this.residenceId);
          this.studentSelectedFlg = true;
          this.fileListIncomeStatement = [];
          this.fileListOthers = [];
          this.first = true;
          this.getDocumentTypes();
          this.addNewGroup();
          this.getPhases();
          this.getConfigurations();
          this.getPatrimonyCategories();
        }
      });
    }else {
      this.loading = false
      this.residenceId = data.residence.id;
      this.getPatrimonyCategories();
      this.getApplicationPhases();
    }

  }

  fillUserInfo(application: ApplicationModel){
    this.studentSelected = new UserModel();
    this.studentSelected.name = application.user && application.user.name ? application.user.name : application.full_name;
    this.studentSelected.birth_date = application.user && application.user.birth_date ? application.user.birth_date : application.applicant_birth_date
    this.studentSelected.nationality = application.user && application.user.nationality ? application.user.nationality : application.nationality
    this.studentSelected.email = application.user && application.user.email ? application.user.email : application.email
    this.studentSelected.identification = application.user && application.user.identification ? application.user.identification : application.identification ? String(application.identification) : null;
    this.studentSelected.document_type_id = application.user && application.user.document_type_id ? application.user.document_type_id : application.document_type_id;
    this.studentSelected.phone = application.user && application.user.phone_1 ? application.user.phone_1 : application.phone_1;
    this.studentSelected.gender = application.user && application.user.gender ? application.user.gender : application.gender;
    this.studentSelected.tin = application.user && application.user.tin ? application.user.tin : application.tin? String(application.tin) : null;
    this.studentSelected.student_number = application.user && application.user.student_number ? application.user.student_number : application.student_number;
    this.studentSelected.organic_unit_id = application.user && application.user.organic_unit_id ? application.user.organic_unit_id : application.organic_unit_id;
    this.studentSelected.course_id =   application.user && application.user.course_id ? application.user.course_id :application.course_id ? Number(application.course_id) : null;
    this.studentSelected.course_year =  application.user && application.user.course_year ? application.user.course_year : application.course_year;
    this.studentSelected.city =  application.user && application.user.city ? application.user.city : application.city;
    this.studentSelected.address =  application.user && application.user.address ? application.user.address : application.address;
    this.studentSelected.postal_code =  application.user && application.user.postal_code ? application.user.postal_code : application.postal_code;
    this.studentSelected.country =  application.user && application.user.country ? application.user.country : application.country;
    this.studentSelected.id = application.user_id;
    this.studentSelected.course_degree_id = application.user && application.user.course_degree_id ? application.user.course_degree_id : application.course_degree_id;
    this.newApplicaionStartingValues();
  }

  fillFormOldApplication(application: ApplicationModel){
    this.onSelectValueRoom(application.residence_id);

    if(application.father_name){
      this.aplicationForm.get('father_name').patchValue(application.father_name);
      this.aplicationForm.get('father_name').updateValueAndValidity();
    }
    if(application.mother_name){
      this.aplicationForm.get('mother_name').patchValue(application.mother_name);
      this.aplicationForm.get('mother_name').updateValueAndValidity();
    }
    if(application.allow_direct_debit){
      this.aplicationForm.get('allow_direct_debit').patchValue(application.allow_direct_debit);
      this.aplicationForm.get('allow_direct_debit').updateValueAndValidity();
    }
    if(application.iban){
      this.aplicationForm.get('iban').patchValue(application.iban);
      this.aplicationForm.get('iban').updateValueAndValidity();
    }
    if(application.household_distance){
      this.aplicationForm.get('household_distance').patchValue(application.household_distance);
      this.aplicationForm.get('household_distance').updateValueAndValidity();
    }

    if(application.phone_2){
      this.aplicationForm.get('phone_2').patchValue(application.phone_2);
      this.aplicationForm.get('phone_2').updateValueAndValidity();
    }

    if(application.secundary_email){
      this.aplicationForm.get('secundary_email').patchValue(application.secundary_email);
      this.aplicationForm.get('secundary_email').updateValueAndValidity();
    }
    if(application.international_student){
      this.aplicationForm.get('international_student').patchValue(application.international_student);
      this.aplicationForm.get('international_student').updateValueAndValidity();
    }

    if(application.admission_date){
      this.aplicationForm.get('admission_date').patchValue(application.admission_date);
      this.aplicationForm.get('admission_date').updateValueAndValidity();
    }
    this.aplicationForm.get('has_used_residences_before').patchValue(true);
    this.aplicationForm.get('has_used_residences_before').updateValueAndValidity();

  if(application.residence_id){
      this.aplicationForm.get('which_residence').patchValue(application.residence_id);
      this.aplicationForm.get('which_residence').updateValueAndValidity();
    }

    if(application.room_id){
      this.aplicationForm.get('which_room').patchValue(application.room_id);
      this.aplicationForm.get('which_room').updateValueAndValidity();
      /*this.aplicationForm.get('room_id').patchValue(application.room_id);
      this.aplicationForm.get('room_id').updateValueAndValidity();*/
    }
  }

  getApplicationPhases() {
    this.applicationPhasesService.getPhasesSinceToday().pipe(first()).subscribe(response => this.applicationPhases = response.data);
  }

  getConfigurations() {
    this.settings.list(1, -1).pipe(first()).subscribe((data) => {
      this.allow_optional_residence = data.data[0].ALLOW_OPTIONAL_RESIDENCE;
      this.show_iban_on_application_form = data.data[0].SHOW_IBAN_ON_APPLICATION_FORM;
      if (!this.show_iban_on_application_form) {
        this.aplicationForm.get("iban").setValue(null);
        this.aplicationForm.get("allow_direct_debit").setValue(null);
      }
    })
  }

  allowDebitDirectChange(event) {
    if (event) {
      this.f.iban.setValidators(Validators.required);
    } else {
      this.f.iban.setValidators(null);
    }
    this.f.iban.updateValueAndValidity();
  }
  getPatrimonyCategories() {
    this.applicationService.getPatrimonyCategories().pipe(first()).subscribe((data) => {
      this.listPatrimony = data.data;
    })
  }

  getResidenceSelected(idResidence: number) {
    this.residencesService.read(idResidence, {
      withRelated: 'extras,regimes,typologies'
    }).subscribe(
      data => {
        this.residenceSelected = data.data;
        this.regimes = this.residenceSelected[0].regimes;
        this.extrasList = this.residenceSelected[0].extras;
        this.typologies = this.residenceSelected[0].typologies;
        this.aplicationForm.controls.residence_id.setValue(idResidence);
        this.getResidences();
        this.getResidencesAvailableForApplication();
        this.getOrganicUnits();
        this.getDegrees();
        if(this.isRenew){
          this.setResidenceAndRoomInfo();
        }
      }
    );
  }

  setResidenceAndRoomInfo(){
    this.aplicationForm.patchValue({
      room_id: this.roomsList.filter(room => room.room_id === this.applicantInfo.room_id).length ? this.applicantInfo.room_id : null,
      extra_ids: this.extrasList.filter(el => this.applicantInfo.extras.find(e => el.extra_id === e.id)).length ? this.extrasList.filter(el => this.applicantInfo.extras.find(e => el.extra_id === e.id)).map(e => e.extra_id) : [] ,
      regime_id: this.regimes.filter(regime => regime.regime_id === this.applicantInfo.regime_id).length ? this.applicantInfo.regime_id : null,
      preferred_typology_id: this.typologies.filter(typology => typology.id === this.applicantInfo.preferred_typology_id).length ? this.applicantInfo.preferred_typology_id: null
    });
  }

  setSelectedResidence(event: any){
    this.loadingResidence = true;
    this.aplicationForm.get("extra_ids").setValue([]);
    this.aplicationForm.get("regime_id").setValue(null);
    this.aplicationForm.get("preferred_typology_id").setValue(null);

    this.residencesService.read(event, {
      withRelated: 'extras,regimes,typologies'
    }).pipe(first(), finalize(() => this.loadingResidence = false)).subscribe((result) => {
      this.residenceSelected = result.data;
      this.regimes = this.residenceSelected[0].regimes;
      this.extrasList = this.residenceSelected[0].extras;
      this.typologies = this.residenceSelected[0].typologies;
    });
  }


  getResidencesAvailableForApplication() {
    this.residencesService.listAvailableForApplication().pipe(first()).subscribe((result) => {
      this.residencesExceptSelect = result.data.filter((residence) => {
        return residence.id != this.residenceSelected[0].id;
      })
    });
  }

  getResidences() {
    this.residencesService.list(1, -1, null, null, { withRelated: false }).pipe(first()).subscribe((result) => {
      this.residences = result.data;
      this.residencesAssign = this.residences.filter((res) => {
        return res.available_for_assign == true;
      })
      this.residencesApplication = result.data.filter((res) => res.available_for_application);
    });
  }

  getOrganicUnits() {
    this.organicUnitsService.list(1, -1, null, null, { active: true, is_school: true }).pipe(
      first()
    ).subscribe(results => {
      this.organics = results.data;
    });
  }

  getDocumentTypes() {
    let idCC = false;
    this.documentTypeService.list(1, -1, null, null, { active: true }).pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
      this.documentTypes.forEach((data) => {
        if (data.id == 2) {
          idCC = true;
        }
      })
      if (idCC && !this.aplicationForm.get('document_type_id').value) {
        this.aplicationForm.get('document_type_id').setValue(2);
      }

    })
  }


  changeTypeDoc() {
    if (this.first) {
      this.first = false;
      return;
    }
    this.disabledIdentification = false;
  }

  goToForm() {
    this.applicationService.canAply(this.studentSelected.id, this.applicationPhase.id).pipe(first()).subscribe((data) => {
      this.canAply = data.data[0];
      if (this.canAply.can) {
        this.aplicationForm = this.newForm();
        this.studentSelectedFlg = true;
        this.fileListIncomeStatement = [];
        this.fileListOthers = [];
        this.first = true;
        this.newApplicaionStartingValues();
        this.getResidenceSelected(this.residenceId);
        this.getDocumentTypes();
        this.addNewGroup();
        this.getPhases();
        this.getConfigurations();
      } else {
        this.uiService.showMessage(MessageType.error, "Este estudante já tem uma candidatura");
      }
    })
  }

  cancelApplicationForm() {
    if(this.isRenew){
      this.location.back();
    }else{
      this.addNewGroup()
      this.current = 0;
      this.studentSelected = null;
      this.aplicationForm.reset();
      this.studentSelectedFlg = false;
    }
  }

  getPhases() {
    this.applicationService.openPhases().pipe(first()).subscribe((data) => {
      if (data.data.length > 0) {
        this.academic_year = data.data[0].academic_year;
        this.fixed = data.data[0].fixed_accommodation_period;
        this.year = Number(data.data[0].academic_year.split('-')[0]);
      }
      if (this.academic_year) {
        this.getPeriodsByAcademicYear(this.academic_year);
      }
    })
  }

  getPeriodsByAcademicYear(academic_year: string) {
    this.periodService.getPeriodsByAcademicYear(academic_year).pipe(first()).subscribe((data) => {
      if (data.data.length > 0) {
        this.startDate = data.data[0].start_date;
        this.endDate = data.data[0].end_date;
        this.aplicationForm.get('dateRange').setValue([this.startDate, this.endDate]);
      }
    })
  }

  disabledDatePeriod = (current: Date) => {
    const end = new Date(this.endDate);
    end.setDate(end.getDate());
    return moment(current).isAfter(end) || moment(current).isBefore(this.startDate);
  }

  incapacityValueChange() {
    if (this.aplicationForm.get("incapacity").value) {
      this.aplicationForm.controls.incapacity_type.setValidators([Validators.required]);
      this.aplicationForm.controls.incapacity_degree.setValidators([Validators.required]);
    } else {
      this.aplicationForm.controls.incapacity_type.clearValidators();
      this.aplicationForm.controls.incapacity_degree.clearValidators();
    }
    this.aplicationForm.controls.incapacity_type.updateValueAndValidity();
    this.aplicationForm.controls.incapacity_degree.updateValueAndValidity();
  }

  createItem() {
    return this.fb.group({
      kinship: ['', [Validators.required, trimValidation]],
      profession: ['', [Validators.required, trimValidation]]
    });
  }

  addNewGroup() {
    let add: FormArray;
    add = this.aplicationForm.get('house_hould_elements') as FormArray;
    add.push(this.createItem());
  }

  deleteGroup(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get('house_hould_elements') as FormArray;
    add.removeAt(index);
  }

  newApplicaionStartingValues() {
    if (this.studentSelected) {
      this.aplicationForm.get('full_name').patchValue(this.studentSelected.name);
      this.aplicationForm.get('full_name').updateValueAndValidity();
    }
    if (this.studentSelected.birth_date) {
      this.aplicationForm.get('applicant_birth_date').patchValue(this.studentSelected.birth_date);
      this.aplicationForm.get('applicant_birth_date').updateValueAndValidity();
    }
    if (this.studentSelected.nationality) {
      this.aplicationForm.get('nationality').patchValue(this.studentSelected.nationality);
      this.aplicationForm.get('nationality').updateValueAndValidity();
    }
    if (this.studentSelected.email) {
      this.aplicationForm.get('email').patchValue(this.studentSelected.email);
      this.aplicationForm.get('email').updateValueAndValidity();
    }
    if (this.studentSelected.identification) {
      this.aplicationForm.get('identification').patchValue(this.studentSelected.identification);
      this.disabledIdentification = true;
      this.aplicationForm.get('identification').updateValueAndValidity();
    }
    if (this.studentSelected.document_type_id) {
      this.aplicationForm.get('document_type_id').patchValue(this.studentSelected.document_type_id);
      this.aplicationForm.get('document_type_id').updateValueAndValidity();
    }
    if (this.studentSelected.phone) {
      this.aplicationForm.get('phone_1').patchValue(this.studentSelected.phone);
      this.aplicationForm.get('phone_1').updateValueAndValidity();
    }
    if (this.studentSelected.gender) {
      this.aplicationForm.get('gender').patchValue(this.studentSelected.gender);
      this.aplicationForm.get('gender').updateValueAndValidity();
    }
    if (this.studentSelected.tin) {
      this.aplicationForm.get('tin').patchValue(this.studentSelected.tin);
      this.aplicationForm.get('tin').updateValueAndValidity();
    }
    if (this.studentSelected.student_number) {
      this.aplicationForm.get('student_number').patchValue(this.studentSelected.student_number);
      this.aplicationForm.get('student_number').updateValueAndValidity();
    }
    if (this.studentSelected.organic_unit_id) {
      this.aplicationForm.get('organic_unit_id').patchValue(this.studentSelected.organic_unit_id);
      this.aplicationForm.get('organic_unit_id').updateValueAndValidity();
    }
    if (this.studentSelected.course_degree_id) {
      this.aplicationForm.get('course_degree_id').patchValue(this.studentSelected.course_degree_id);
      this.aplicationForm.get('course_degree_id').updateValueAndValidity();
      this.clickDegree();
    }
    if (this.studentSelected.course_id) {
      this.aplicationForm.get('course_id').patchValue(this.studentSelected.course_id);
      this.aplicationForm.get('course_id').updateValueAndValidity();
    }
    if (this.studentSelected.course_year) {
      this.aplicationForm.get('course_year').patchValue(this.studentSelected.course_year);
      this.aplicationForm.get('course_year').updateValueAndValidity();
    }
    if (this.studentSelected.city) {
      this.aplicationForm.get('city').patchValue(this.studentSelected.city);
      this.aplicationForm.get('city').updateValueAndValidity();
    }
    if (this.studentSelected.address) {
      this.aplicationForm.get('address').patchValue(this.studentSelected.address);
      this.aplicationForm.get('address').updateValueAndValidity();
    }
    if (this.studentSelected.postal_code) {
      this.aplicationForm.get('postal_code').patchValue(this.studentSelected.postal_code);
      this.aplicationForm.get('postal_code').updateValueAndValidity();
    }
    if (this.studentSelected.country) {
      this.aplicationForm.get('country').patchValue(this.studentSelected.country);
      this.aplicationForm.get('country').updateValueAndValidity();
    }
    if (this.studentSelected.admission_date) {
      this.aplicationForm.get('admission_date').patchValue(this.studentSelected.admission_date);
      this.aplicationForm.get('admission_date').updateValueAndValidity();
    }

  }

  getDegrees() {
    this.coursesDegreesServices.courseDegrees()
      .pipe(
        first(),
      )
      .subscribe(arg => {
        this.courseDegreesList = arg.data;

      });

  }

  getPhase(phase) {
    return "["+ phase.academic_year + "] " + " Fase: " + phase.application_phase + ' (' + phase.start_date.split('T')[0] + '/' + phase.end_date.split('T')[0] + ')';
  }

  changeValue() {
    this.f.course_id.setValue(null);
    this.clickDegree();
  }


  clickDegree() {
    if (this.aplicationForm.get('course_degree_id').value && this.aplicationForm.get('organic_unit_id').value) {
      this.courseLoading = true;
      this.coursesDegreesServices
        .courses(this.aplicationForm.get('course_degree_id').value, this.aplicationForm.get('organic_unit_id').value)
        .pipe(
          first(),
          finalize(() => {
            this.courseLoading = false;
          })
        )
        .subscribe((arg) => {
          this.coursesList = arg.data;
        });
    }

  }
  disableEndDate = (endDate: Date) => {
    if(!this.isRenew){
      const currentDate = new Date();
      currentDate.setDate(currentDate.getDate());
      return moment(endDate).isAfter(currentDate);
    }else{
      return !moment(endDate).isBetween(this.startDate, this.endDate);
    }
  }


  disableStartDate = (startDate: Date) => {
    if(!this.isRenew){
      const currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - 1);
      return moment(startDate).isBefore(currentDate);
    }else{
      return !moment(startDate).isBetween(this.startDate, this.endDate);
    }
  }

  onChangeValueResidence() {

    if (!this.aplicationForm.controls.has_used_residences_before.value) {
      this.aplicationForm.controls.which_residence.clearValidators();
      this.aplicationForm.controls.which_room.clearValidators();
    } else {
      this.aplicationForm.controls.which_residence.setValidators([Validators.required]);
      this.aplicationForm.controls.which_room.setValidators([Validators.required]);
    }

    this.aplicationForm.controls.which_residence.updateValueAndValidity();
    this.aplicationForm.controls.which_room.updateValueAndValidity();
  }

  onSelectValueRoom(id: any) {
    this.loadingRoom = true;
    this.aplicationForm.patchValue({ which_room_id: null });
    this.roomsService.list(1, -1, null, null, {
      residence_id: id,
      active: true,
      sort: 'name'
    }).pipe((first(), finalize(() => this.loadingRoom = false))).subscribe(
      arg => {
        console.log(arg.data)
        this.roomsList = arg.data;
      });
  }

  next(): void {
    this.isSubmited = true;
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('full_name'),
          this.aplicationForm.get('applicant_birth_date'),
          this.aplicationForm.get('gender'),
          this.aplicationForm.get('father_name'),
          this.aplicationForm.get('mother_name'),
          this.aplicationForm.get('incapacity'),
          this.aplicationForm.get('incapacity_type'),
          this.aplicationForm.get('incapacity_degree'),
          this.aplicationForm.get('nationality'),
          this.aplicationForm.get('identification'),
          this.aplicationForm.get('document_type_id'),
          this.aplicationForm.get('tin'),
          this.aplicationForm.get('iban'),
          this.aplicationForm.get('address'),
          this.aplicationForm.get('city'),
          this.aplicationForm.get('postal_code'),
          this.aplicationForm.get('country'),
          this.aplicationForm.get('household_distance')
        ]);
        break;
      }
      case 1: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('email'),
          this.aplicationForm.get('secundary_email'),
          this.aplicationForm.get('phone_1'),
          this.aplicationForm.get('phone_2')
        ]);
        break;
      }
      case 2: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('residence_id'),
          this.aplicationForm.get('second_option_residence_id'),
          this.aplicationForm.get('dateRange'),
          this.aplicationForm.get('regime_id'),
          this.aplicationForm.get('extra_ids'),
          this.aplicationForm.get('has_used_residences_before'),
          this.aplicationForm.get('which_residence'),
          this.aplicationForm.get('which_room'),
        ]);
        break;
      }
      case 3: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('international_student'),
          this.aplicationForm.get('organic_unit_id'),
          this.aplicationForm.get('course_degree_id'),
          this.aplicationForm.get('course_id'),
          this.aplicationForm.get('course_year'),
          this.aplicationForm.get('student_number'),
          this.aplicationForm.get('admission_date')
        ]);
        break;
      }
      case 4: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('house_hould_elements'),
          this.aplicationForm.get('household_elements_number'),
          this.aplicationForm.get('household_total_income'),
          this.aplicationForm.get("income_source"),
          this.aplicationForm.get("other_income_source"),
          this.aplicationForm.get('patrimony_category_id'),
          this.aplicationForm.get('income_statement_delivered'),
          this.aplicationForm.get('househould_elements_in_university')
        ]) && this.validIncomeStatementDelivered();
        break;
      }
      case 5: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('applied_scholarship'),
          this.aplicationForm.get('scholarship_value')
        ]);
        break;
      }
      case 6:
        valid = true;
        break;
    }

    if (valid) {
      this.isSubmited = false;
      this.current += 1;
      this.changeContent();
    }
  }

  validIncomeStatementDelivered(): boolean {
    if (this.aplicationForm.get("income_statement_delivered").value) {
      if (this.fileListIncomeStatement.length == 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  onChangeIncomeSource(event) {

    this.other_income = false;
    if (event.length > 0) {
      event.forEach(income => {
        if (income == "OTHER") {
          this.other_income = true;
        }
      });
      this.aplicationForm.get('income_source').setValue(event);
    } else {
      this.aplicationForm.get('income_source').setValue('');
    }

    if (this.other_income) {
      this.aplicationForm.controls.other_income_source.setValidators([Validators.required]);
    } else {
      this.aplicationForm.controls.other_income_source.clearValidators();
    }
    this.aplicationForm.controls.other_income_source.updateValueAndValidity();
  }

  check(value): boolean {
    if (this.aplicationForm.get('income_source').value) {
      return this.aplicationForm.get('income_source').value.includes(value);
    } else {
      return false;
    }
  }

  previous(): void {
    this.current -= 1;
    this.changeContent();
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'identification';
        break;
      }
      case 1: {
        this.index = 'contacts';
        break;
      }
      case 2: {
        this.index = 'residence';
        break;
      }
      case 3: {
        this.index = 'academic_data';
        break;
      }
      case 4: {
        this.index = 'family';
        break;
      }
      case 5: {
        this.index = 'social_benefits';
        break;
      }
      case 6: {
        this.index = 'observations';
        break;
      }
      case 7: {
        this.index = 'revision';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }

  valuesForm() {
    this.isSubmited = true;
    let valid = this.checkValidityOfControls([
      this.aplicationForm.get("applicant_consent"),
    ]);
    if (valid) {
      this.residenceApplicationModel = new ApplicationModel();
      this.extrasSend = [];
      this.getNameStep1();
      this.getNamesStep2();
      this.getNamesStep3();
      this.getNamesStep4();
      this.isSubmited = false;
      this.current += 1;
      this.changeContent();
      // 0 step
      this.residenceApplicationModel.full_name = this.aplicationForm.controls.full_name.value;
      this.residenceApplicationModel.applicant_birth_date = this.aplicationForm.controls.applicant_birth_date.value;
      this.residenceApplicationModel.gender = this.aplicationForm.controls.gender.value;
      this.residenceApplicationModel.father_name = this.aplicationForm.controls.father_name.value;
      this.residenceApplicationModel.mother_name = this.aplicationForm.controls.mother_name.value;
      this.residenceApplicationModel.incapacity = this.aplicationForm.controls.incapacity.value;
      if (this.residenceApplicationModel.incapacity) {
        this.residenceApplicationModel.incapacity_type = this.aplicationForm.controls.incapacity_type.value;
        this.residenceApplicationModel.incapacity_degree = this.aplicationForm.controls.incapacity_degree.value;
      } else {
        this.residenceApplicationModel.incapacity_type = null;
        this.residenceApplicationModel.incapacity_degree = null;
      }
      this.residenceApplicationModel.nationality = this.aplicationForm.controls.nationality.value;
      this.residenceApplicationModel.identification = this.aplicationForm.controls.identification.value;
      this.residenceApplicationModel.document_type_id = this.aplicationForm.controls.document_type_id.value;
      this.residenceApplicationModel.tin = this.aplicationForm.controls.tin.value;
      this.residenceApplicationModel.iban = this.aplicationForm.controls.iban.value;
      this.residenceApplicationModel.allow_direct_debit = this.aplicationForm.controls.allow_direct_debit.value;
      this.residenceApplicationModel.user_id = this.studentSelected.id;
      this.residenceApplicationModel.application_phase_id = this.applicationPhase.id ;
      this.residenceApplicationModel.address = this.aplicationForm.controls.address.value;
      this.residenceApplicationModel.city = this.aplicationForm.controls.city.value;
      this.residenceApplicationModel.postal_code = this.aplicationForm.controls.postal_code.value;
      this.residenceApplicationModel.country = this.aplicationForm.controls.country.value;
      this.residenceApplicationModel.household_distance = +this.aplicationForm.controls.household_distance.value;
      // 1 step
      this.residenceApplicationModel.email = this.aplicationForm.controls.email.value;
      this.residenceApplicationModel.secundary_email = this.aplicationForm.controls.secundary_email.value ? this.aplicationForm.controls.secundary_email.value : null;
      this.residenceApplicationModel.phone_1 = this.aplicationForm.controls.phone_1.value;
      this.residenceApplicationModel.phone_2 = this.aplicationForm.controls.phone_2.value;
      // 2 step
      this.residenceApplicationModel.residence_id = this.aplicationForm.controls.residence_id.value;
      if (this.aplicationForm.controls.second_option_residence_id.value && this.allow_optional_residence) {
        this.residenceApplicationModel.second_option_residence_id = this.aplicationForm.controls.second_option_residence_id.value;
      } else {
        this.residenceApplicationModel.second_option_residence_id = null;
      }
      this.residenceApplicationModel.start_date = this.aplicationForm.controls.dateRange.value[0];
      this.residenceApplicationModel.end_date = this.aplicationForm.controls.dateRange.value[1];
      this.residenceApplicationModel.regime_id = this.aplicationForm.controls.regime_id.value;
      if (this.aplicationForm.controls.preferred_typology_id.value) {
        this.residenceApplicationModel.preferred_typology_id = this.aplicationForm.controls.preferred_typology_id.value;
      }
      if (this.aplicationForm.controls.extra_ids.value) {
        this.residenceApplicationModel.extra_ids = this.aplicationForm.controls.extra_ids.value;
      } else {
        this.residenceApplicationModel.extra_ids = [];
      }

      if(this.isRenew && this.aplicationForm.value.which_room && this.applicantInfo
        && this.aplicationForm.value.residence_id === this.applicantInfo.residence_id){
        this.residenceApplicationModel.room_id = this.aplicationForm.value.which_room
      }

      if (this.aplicationForm.controls.has_used_residences_before.value) {
        this.residenceApplicationModel.has_used_residences_before = true;
        this.residenceApplicationModel.which_residence_id = this.aplicationForm.controls.which_residence.value;
        this.residenceApplicationModel.which_room_id = this.aplicationForm.controls.which_room.value;
      } else {
        this.residenceApplicationModel.has_used_residences_before = false;
        this.residenceApplicationModel.which_residence_id = null;
        this.residenceApplicationModel.which_room_id = null;
      }
      // 3 step
      this.residenceApplicationModel.international_student = this.aplicationForm.controls.international_student.value;
      this.residenceApplicationModel.organic_unit_id = this.aplicationForm.controls.organic_unit_id.value;
      this.residenceApplicationModel.course_id = this.aplicationForm.controls.course_id.value;
      this.residenceApplicationModel.course_year = this.aplicationForm.controls.course_year.value;
      this.residenceApplicationModel.student_number = this.aplicationForm.controls.student_number.value;
      this.residenceApplicationModel.admission_date = this.aplicationForm.controls.admission_date.value;
      // 4 step
      this.residenceApplicationModel.house_hould_elements = this.aplicationForm.controls.house_hould_elements.value;
      this.residenceApplicationModel.household_elements_number = this.aplicationForm.controls.house_hould_elements.value.length;
      this.residenceApplicationModel.income_source = this.aplicationForm.controls.income_source.value ? this.aplicationForm.controls.income_source.value : null;
      if (this.aplicationForm.controls.income_source.value && this.aplicationForm.controls.income_source.value.includes("OTHER")) {
        this.residenceApplicationModel.other_income_source = this.aplicationForm.controls.other_income_source.value;
      } else {
        this.residenceApplicationModel.other_income_source = null;
      }

      this.residenceApplicationModel.househould_elements_in_university = Number(this.aplicationForm.controls.househould_elements_in_university.value);
      this.residenceApplicationModel.income_statement_delivered = this.aplicationForm.controls.income_statement_delivered.value;
      this.residenceApplicationModel.patrimony_category_id = this.aplicationForm.controls.patrimony_category_id.value;
      this.residenceApplicationModel.household_total_income = parseFloat(
        Number(this.aplicationForm.controls.household_total_income.value.replace(',', '.')).toFixed(2));
      if (this.aplicationForm.controls.income_statement_delivered.value) {
        this.aplicationForm.get("income_statement_files_ids").setValue(this.fileListIncomeStatement);
        this.residenceApplicationModel.income_statement_files_ids = this.aplicationForm.controls.income_statement_files_ids.value;
      } else {
        this.residenceApplicationModel.income_statement_files_ids = [];
      }

      // 5 step
      if (this.aplicationForm.controls.applied_scholarship.value === true) {
        this.residenceApplicationModel.applied_scholarship = this.aplicationForm.controls.applied_scholarship.value;
        this.residenceApplicationModel.scholarship_value = this.aplicationForm.controls.scholarship_value.value;
      } else {
        this.residenceApplicationModel.applied_scholarship = false;
        this.residenceApplicationModel.scholarship_value = 0;
      }
      // 6 step
      if (this.aplicationForm.controls.observations.value) {
        this.residenceApplicationModel.observations = this.aplicationForm.controls.observations.value;
      } else {
        this.residenceApplicationModel.observations = '';
      }

      this.aplicationForm.get("file_ids").setValue(this.fileListOthers);
      this.residenceApplicationModel.file_ids = this.aplicationForm.controls.file_ids.value;
      // 7 step
      this.residenceApplicationModel.applicant_consent = this.aplicationForm.controls.applicant_consent.value;
      this.residenceApplicationModel.consent_date = new Date().toDateString();


      this.residenceApplicationModel.created_at = new Date();
    }
  }

  onFileDeletedOther(fileId) {
    this.fileListOthers = this.fileListOthers.filter((f) => f !== fileId);
  }

  onFileAddedOther(fileId) {
    this.fileListOthers.push(fileId);
  }

  getNameStep1() {
    this.documentTypes.forEach((data) => {
      if (data.id == this.aplicationForm.get('document_type_id').value)
        this.documentType = data;
    })
  }

  getNamesStep2() {
    this.residenceName = this.residenceSelected[0].name;
    this.residenceSelected[0].regimes.forEach((regime) => {
      if (regime.regime_id == this.aplicationForm.get('regime_id').value) {
        this.sendRegime = regime;
      }
    })
    this.residenceSelected[0].typologies.forEach((typ) => {
      if (typ.id == this.aplicationForm.get('preferred_typology_id').value) {
        this.sendTypologies = typ;
      }
    })
    if (this.residenceSelected[0].extras.length > 0 && this.aplicationForm.get('extra_ids').value) {
      this.residenceSelected[0].extras.forEach((extra) => {
        this.aplicationForm.get('extra_ids').value.forEach(extraApp => {
          if (extra.extra_id == extraApp) {
            this.extrasSend.push(extra);
          }
        })
      })
    }

    if (this.aplicationForm.get('second_option_residence_id').value) {
      this.residencesExceptSelect.forEach(res => {
        if (res.id == this.aplicationForm.get('second_option_residence_id').value) {
          this.nameResidenceSelected = res.name;
        }
      })
    }
    if (this.aplicationForm.get('has_used_residences_before').value) {
      this.residences.forEach(res => {
        if (res.id == this.aplicationForm.get('which_residence').value) {
          this.nameUsedResidenceSelected = res.name;
        }
      })
      this.roomsList.forEach(room => {
        if (room.id == this.aplicationForm.get('which_room').value) {
          this.nameRoom = room.name;
        }
      })
    }
  }

  getNamesStep3() {
    this.organics.forEach((organic) => {
      if (organic.id == this.aplicationForm.get('organic_unit_id').value) {
        this.nameOrganic = organic.name;
      }
    })
    this.courseDegreesList.forEach(course_degree => {
      if (course_degree.id == this.aplicationForm.get('course_degree_id').value) {
        this.nameCourseDegree = course_degree.name;
      }
    });

    this.coursesList.forEach(course => {
      if (course.id == this.aplicationForm.get('course_id').value) {
        this.nameCourse = course.name;
      }
    });
  }

  getNamesStep4() {
    if (this.aplicationForm.get('patrimony_category_id').value) {
      this.listPatrimony.forEach(patrimony => {
        if (patrimony.id == this.aplicationForm.get('patrimony_category_id').value) {
          this.descPatrimony = patrimony.description;
        }
      })
    }

  }

  changeCurrent(event) {
    this.current = Number(event);
    this.changeContent();
  }



  cancelApplication(): void {
    window.location.reload();
    this.router.navigateByUrl('accommodation/dashboard');
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find(c => c.status !== 'VALID');
  }

  submitForm() {

    if (this.aplicationForm.valid) {
      this.loadingButton = true;
      this.applicationService.create(this.residenceApplicationModel).pipe(
        first(),
        finalize(() => this.loadingButton = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, "Candidatura submetida com sucesso");
        this.router.navigateByUrl('accommodation/dashboard');
      });
    }
  }

  onFileDeleted(fileId) {
    this.fileListIncomeStatement = this.fileListIncomeStatement.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.fileListIncomeStatement.push(fileId);
  }

  internationalValueChange() {
    let add: FormArray;
    add = this.aplicationForm.get('house_hould_elements') as FormArray;
    if (this.aplicationForm.value.international_student == true) {
      this.aplicationForm.controls.househould_elements_in_university.clearValidators();
      this.aplicationForm.controls.household_total_income.clearValidators();
      this.aplicationForm.controls.income_source.clearValidators();
      this.aplicationForm.controls.patrimony_category_id.clearValidators();
      this.aplicationForm.controls.income_statement_delivered.clearValidators();
      add.controls.forEach(control => {
        control.get('kinship').clearValidators();
        control.get('profession').clearValidators();
      });
    } else {
      this.aplicationForm.controls.househould_elements_in_university.setValidators([Validators.required]);
      this.aplicationForm.controls.household_total_income.setValidators([Validators.required]);
      this.aplicationForm.controls.income_source.setValidators([Validators.required]);
      this.aplicationForm.controls.patrimony_category_id.setValidators([Validators.required]);
      this.aplicationForm.controls.income_statement_delivered.setValidators([Validators.required]);
      add.controls.forEach(control => {
        control.get('kinship').setValidators([Validators.required]);
        control.get('profession').setValidators([Validators.required]);
      });
    }
    this.aplicationForm.controls.househould_elements_in_university.updateValueAndValidity();
    this.aplicationForm.controls.income_source.updateValueAndValidity();
    this.aplicationForm.controls.household_total_income.updateValueAndValidity();
    this.aplicationForm.controls.patrimony_category_id.updateValueAndValidity();
    this.aplicationForm.controls.income_statement_delivered.updateValueAndValidity();
    add.controls.forEach(control => {
      control.get('kinship').updateValueAndValidity();
      control.get('profession').updateValueAndValidity();
    });
  }

}
