import { ApplicationModel } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/models/application.model';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { UiService } from '../../../../../../core/services/ui-service.service';
import { ApplicationStatusTranslations, ApplicationStatusMachine } from '../../models/application.model';
import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccommodationService } from '../../../../services/accommodation.service';
import { TableHelper, Column } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ApplicationDataModalComponent } from '../../components/application-data-modal/application-data-modal.component';
import { ApplicationStatusPromptComponent } from '../../components/application-status-prompt/application-status-prompt.component';
import { ResidencesService } from '../../../residences/services/residences.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { first, finalize } from 'rxjs/operators';
import { PremuteRoomModalService } from '@fi-sas/backoffice/modules/accommodation/services/premute-room-modal.service';
import { ReportsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/reports.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { RoomChangeModalComponent } from '../../components/room-change-modal/room-change-modal.component';
import { RegimeExtrasChangeModelComponent } from '../../components/regime-extras-change-model/regime-extras-change-model.component';
import { ApplicationTariffChangeComponent } from '../../components/application-tariff-change/application-tariff-change.component';
import * as moment from 'moment';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { ApplicationRollbackStatusModalComponent } from '../../components/application-rollback-status-modal/application-rollback-status-modal.component';
import { WithdrawalModalComponent } from '../../components/withdrawal-modal/withdrawal-modal.component';
import { ApplicationPeriodChangeComponent } from '../../components/application-period-change/application-period-change.component';
import { ApplicationRenewComponent } from '../../components/application-renew/application-renew.component';

@Component({
  selector: 'fi-sas-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less']
})
export class ListApplicationsComponent extends TableHelper implements OnInit {

  statusMachine = ApplicationStatusMachine;

  columns: Column[] = [];
  decisionFilter = [
    { value: 'ASSIGN', label: 'Colocado' },
    { value: 'UNASSIGN', label: 'Não colocado' },
    { value: 'ENQUEUE', label: 'Lista de espera' }
  ];
  statusFilter = [
    { value: null, label: 'Todas', scope: this.validPermissionFilter('accommodation:applications:all-applications') },
    { value: 'cancelled', label: 'Cancelada', scope: this.validPermissionFilter('accommodation:applications:applications-cancelled') },
    { value: 'assigned', label: 'Colocado', scope: this.validPermissionFilter('accommodation:applications:applications-assigned') },
    { value: 'confirmed', label: 'Confirmado', scope: this.validPermissionFilter('accommodation:applications:applications-confirmeds') },
    { value: 'contracted', label: 'Contratado', scope: this.validPermissionFilter('accommodation:applications:applications-contracteds') },
    { value: 'withdrawal', label: 'Desistência', scope: this.validPermissionFilter('accommodation:applications:applications-withdrals') },
    { value: 'analysed', label: 'Em analise', scope: this.validPermissionFilter('accommodation:applications:applications-analyse') },
    { value: 'pending', label: 'Em despacho', scope: this.validPermissionFilter('accommodation:applications:applications-dispatches') },
    { value: 'closed', label: 'Fechada', scope: this.validPermissionFilter('accommodation:applications:applications-closed') },
    { value: 'queued', label: 'Lista de espera', scope: this.validPermissionFilter('accommodation:applications:applications-queued') },
    { value: 'unassigned', label: 'Não colocado', scope: this.validPermissionFilter('accommodation:applications:applications-unassigned') },
    { value: 'opposition', label: 'Oposição', scope: this.validPermissionFilter('accommodation:applications:applications-oppositions') },
    { value: 'rejected', label: 'Rejeitado', scope: this.validPermissionFilter('accommodation:applications:applications-rejected') },
    { value: 'submitted', label: 'Submetida', scope: this.validPermissionFilter('accommodation:applications:applications-selection') },
  ];
  persistentFilters = {
    searchFields: 'full_name,email,student_number,city,tin,identification'
  }
  allChecked = true;
  checkOptionsOne = [];

  residencesSelect = {};
  status = '';

  residences: ResidenceModel[] = [];
  schools: OrganicUnitsModel[] = [];
  schoolsLoading = true;
  accommodationData;
  canRenew: boolean = false;

  constructor(
    accommodationService: AccommodationService,
    public service: ApplicationsService,
    private authService: AuthService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private messageService: NzMessageService,
    private modalService: NzModalService,
    private residencesService: ResidencesService,
    private premuteRoomService: PremuteRoomModalService,
    private reportsService: ReportsService,
    private organicUnitService: OrganicUnitsService
  ) {
    super(uiService, router, activatedRoute);
    this.loadResidences();
    this.accommodationData = accommodationService.getAccommodationData();

    if (this.accommodationData.academicYear) {
      this.persistentFilters['academic_year'] = this.accommodationData.academicYear;
    }

    if (this.accommodationData.applicationPhase) {
      this.persistentFilters['application_phase_id'] = this.accommodationData.applicationPhase.id;
    }

    if (this.accommodationData.residence) {
      this.persistentFilters['application_residence_id'] = this.accommodationData.residence.id;
    }

    this.persistentFilters['withRelated'] = 'tariff,preferred_typology,history,residence,currentRegime,regime,regime.translations,paymentMethod,room,course,house_hould_elements,extras,assignedResidence,second_option_residence,organic_unit,income_statement_files,whichRoom,whichResidence,user_previous_applications,document_type,files,occurrences,user'
    let statusAppli = localStorage.getItem('status_application');
    if (statusAppli) {
      this.filters.status = statusAppli;
      setTimeout(() => {
        this.updateFilters();
      }, 500);

    }
    localStorage.removeItem('status_application');
  }

  ngOnInit() {
    this.initTableData(this.service);
    this.initOrganicUnitList();

    const checkValid = localStorage.getItem('check_column_table_accommodation' + '_' + this.authService.getUser().id.toString());
    if (checkValid) {
      this.initCheck();
      const cookie = JSON.parse(checkValid);
      this.checkOptionsOne.forEach((checkInit)=> {
        cookie.forEach(checkCookie => {
            if(checkInit.value == checkCookie.value) {
              checkInit.checked = checkCookie.checked;
            }
        });
      })
      this.check();
    } else {
      this.initCheck();
    }

  }

  initCheck() {
    this.checkOptionsOne = [
      { label: 'Nº Estudante', value: 'student_number', checked: true },
      { label: 'Nome', value: 'full_name', checked: true },
      { label: 'Estado', value: 'status', checked: true },
      { label: 'Ano Académico', value: 'academic_year', checked: true },
      { label: 'Residência candidatada', value: 'residence_id', checked: true },
      { label: 'Residencia atribuída ', value: 'assigned_residence_id', checked: true },
      { label: 'Quarto', value: 'room_id', checked: true },
      { label: 'Regime', value: 'regime_id', checked: false },
      { label: 'Regime Atual', value: 'current_regime_id', checked: true },
      { label: 'Data de Submissão', value: 'submission_date', checked: false },
      { label: 'Fora de Prazo', value: 'submitted_out_of_date', checked: false },
      { label: 'Candidatou-se a Bolsa de Estudo', value: 'applied_scholarship', checked: false },
      { label: 'Incapacidade', value: 'incapacity', checked: false },
      { label: 'Nacionalidade', value: 'nationality', checked: false },
      { label: 'Género', value: 'gender', checked: false },
      { label: 'Internacional', value: 'international_student', checked: false },
      { label: 'Escola', value: 'organic_unit', checked: false },
      { label: 'Grau', value: 'courseDegree', checked: false },
      { label: 'Curso', value: 'course', checked: false },
      { label: 'Escalão Património', value: 'patrimony_category', checked: false },
      { label: 'Nº Elementos Agregado', value: 'household_elements_number', checked: false },
      { label: 'Total de Rendimentos', value: 'household_total_income', checked: false },
      { label: 'Rendimento per Capita', value: 'per_capita_income', checked: false },
      { label: 'Período de Alojamento', value: 'start_date', checked: false },
      { label: 'Distância da Residência', value: 'household_distance', checked: false },
    ];
    this.check();
  }

  updateFilters() {
    this.searchData(true);
  }

  initOrganicUnitList() {
    this.organicUnitService.list(1, -1, null, null, { active: true, is_school: true, withRelated: false }).pipe(
      first()
    ).subscribe(
      results => {
        this.schools = results.data;
        this.schoolsLoading = false;
      },
      err => {
        this.schoolsLoading = false;
      });
  }


  validPermissionFilter(permission) {
    return this.authService.hasPermission(permission);
  }

  validStatusFilter() {
    this.filters.status = this.statusFilter.find((perm) => perm.scope) ? this.statusFilter.find((perm) => this.authService.hasPermission(perm.value)).value : '';
  }

  listComplete() {
    this.persistentFilters['residence_id'] = null;
    this.persistentFilters['assigned_residence_id'] = null;
    this.filters.status = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.decision = null;
    this.filters.submission_date = null;
    if (this.accommodationData.residence) {
      this.persistentFilters['application_residence_id'] = this.accommodationData.residence.id;
    }
    this.searchData(true)
  }


  loadResidences() {
    this.residencesService.list(1, -1, null, null, { withRelated: false })
      .pipe(first())
      .subscribe(results => {
        this.residences = results.data;
      });
  }

  // CHANGES STATUS FUNCTIONS

  showStatusModal(state: any, application: ApplicationModel) {
    switch (state.status) {
      case 'ASSIGNED':
        this.showApplicationDataModal(application);
        break;
      default:
        this.showPromptStatusModal(state, application);
    }
  }

  showStatusModalBulk(state: any) {
    switch (state.status) {
      case 'ASSIGNED':
        //this.showApplicationDataModal(application);
        break;
      default:
        this.showPromptStatusModal(state, null, this.getSelectedData());
    }
  }

  showPromptStatusModal(state: any, application?: ApplicationModel, applications?: ApplicationModel[]) {
    const modal = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: ApplicationStatusPromptComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application,
        applications: applications,
        state: state,
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Gravar',
          type: state.type === 'success' ? 'primary' : 'danger',
          onClick: (componentInstance) => {
            componentInstance.save().then(() => {
              this.searchData(true);
            }).catch(() => {
            });
          }
        },
      ],
    });
  }

  showApplicationDataModal(application: ApplicationModel) {
    const modal = this.modalService.create({
      nzTitle: 'Dados Candidatura',
      nzContent: ApplicationDataModalComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Cancelar',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.save().then(result => {
              if (result) {
                this.searchData();
              }
            });
          }
        },
      ]
    });
  }

  showApplicationPeriodModal(application: ApplicationModel) {
    const modal = this.modalService.create({
      nzTitle: 'Alterar Período',
      nzContent: ApplicationPeriodChangeComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Cancelar',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.save().then(result => {
              if (result) {
                this.searchData();
              }
            });
          }
        },
      ]
    });
  }

  getActiveBulkOptions() {
    if ((this.isIndeterminate || this.isAllDisplayDataChecked) && !this.loading) {
      let checkedAppsStatus = this.getSelectedData();
      checkedAppsStatus = checkedAppsStatus.map(app => app.status);

      let statusOptions = checkedAppsStatus.filter((f, i) => checkedAppsStatus.indexOf(f) === i)
      statusOptions = [].concat(...statusOptions.map(s => this.statusMachine[s]));
      return statusOptions;
    } else {
      return [];
    }
  }

  openPermuteRoomModal(application: ApplicationModel) {
    return this.premuteRoomService.openModal(application).subscribe(changed => {
      if (changed) {
        this.searchData();
      }
    });
  }

  tableXSize(): string {
    return (this.columns.length * 125).toString().concat('px');
  }

  downloadApplicationReport(application_id: number) {
    this.loading = true;
    this.reportsService.getApplicationReport(application_id).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      return this.messageService.success("Relatório gerado com sucesso");
    });
  }

  openRoomChangeModal(application: ApplicationModel, type: string) {
    let title = '';
    switch (type) {
      case 'TYPOLOGY':
        title = 'Alterar tipologia';
        break;
      case 'RESIDENCE':
        title = 'Alterar residência';
        break;
      case 'PERMUTE':
        title = 'Permuta de quarto';
        break;
      case 'ROOM':
        title = 'Alterar quarto';
        break;
      default:
        title = '';
    }
    const modal = this.modalService.create({
      nzTitle: title,
      nzContent: RoomChangeModalComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application,
        type: type
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });
  }

  openRegimeExtrasChangeModal(application: ApplicationModel, type: string) {
    let title = '';
    switch (type) {
      case 'REGIME':
        title = 'Alterar regime';
        break;
      case 'EXTRA':
        title = 'Alterar extras';
        break;
      default:
        title = '';
    }
    const modal = this.modalService.create({
      nzTitle: title,
      nzContent: RegimeExtrasChangeModelComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application,
        type: type
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });
  }

  openWithdrawalModal(application: ApplicationModel){
    const modal = this.modalService.create({
      nzTitle: 'Desistência do alojamento',
      nzContent: WithdrawalModalComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });
  }

  validExtras(listExtrasResidence){
    const aux = listExtrasResidence.filter((extra)=>{
      return extra.extra.active;
    })
    if(aux.length>0){
      return true;
    }
    return false;
  }

  validRegime(listRegimesResidence, idRegime){
    const aux = listRegimesResidence.filter((data)=> {
      return data.regime_id != idRegime && data.regime.active;
    });
    if(aux.length>0){
      return true;
    }
    return false;
  }

  openTariffChangeModal(application: ApplicationModel) {
    const modal = this.modalService.create({
      nzTitle: 'Alteração de regime de bolsa',
      nzContent: ApplicationTariffChangeComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });

  }

  openRenewProcess(application: ApplicationModel){
    this.modalService.create({
      nzTitle: 'Renovar Candidatura',
      nzContent: ApplicationRenewComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });
  }

  showRevertStatus(status: string) {
    return ['analysed', 'pending', 'cancelled', 'assigned', 'unassigned', 'rejected', 'confirmed', 'withdrawal'].includes(status);
  }

  openRollbackStatusModal(application: ApplicationModel) {
    const modal = this.modalService.create({
      nzTitle: 'Reverter estado',
      nzContent: ApplicationRollbackStatusModalComponent,
      nzWidth: 650,
      nzComponentParams: {
        application: application
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Gravar',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit().then(result => {
              if (result) {
                this.searchData();
              }
            });
          }
        },
      ],
    });
  }

  check() {
    this.columns = [];
    this.loading = true;
    this.checkOptionsOne.filter((check) => check.checked == true).length == 20 ? this.allChecked = true : this.allChecked = false;
    localStorage.setItem('check_column_table_accommodation' + '_' + this.authService.getUser().id.toString(), JSON.stringify(this.checkOptionsOne));
    this.checkOptionsOne.forEach((check) => {
      if (check.checked == true) {
        switch (check.value) {
          case 'full_name':
            this.columns.push({
              key: 'full_name',
              label: 'Nome',
              sortable: true,
            })
            break;
          case 'student_number':
            this.columns.push({
              key: 'student_number',
              label: 'Nº Estudante',
              sortable: true,
            })
            break;
          case 'status':
            this.columns.push({
              key: 'status',
              label: 'Estado',
              sortable: true,
              tag: ApplicationStatusTranslations
            })
            break;
          case 'academic_year':
            this.columns.push({
              key: 'academic_year',
              label: 'Ano Académico',
              sortable: true,
            })
            break;
          case 'submission_date':
            this.columns.push({
              key: '',
              label: 'Data de Submissão',
              template: (data) => {
                return moment(new Date(data.submission_date)).format('DD/MM/YYYY');
              },
              sortable: true,
            })
            break;
          case 'submitted_out_of_date':
            this.columns.push({
              key: 'submitted_out_of_date',
              label: 'Fora de Prazo',
              sortable: true,
              tag: TagComponent.YesNoTag,
            })
            break;
          case 'residence_id':
            this.columns.push({
              key: '',
              label: 'Residência Candidatada',
              template: (residence) => {
                // return residence.status == 'queue' || residence.status == 'analysed' || residence.status == 'cancelled' ? residence.residence.name : residence.residence ? residence.residence.name : residence.assignedResidence ? residence.assignedResidence.name : '';
                return residence.residence.name;
              },
              sortable: false,
            })
            break;
          case 'assigned_residence_id':
            this.columns.push({
              key: '',
              label: 'Residência Atribuída',
              template: (residence) => {
                return residence.assignedResidence ? residence.assignedResidence.name : ''
              },
              sortable: false,
            })
            break;
          case 'room_id':
            this.columns.push({
              key: '',
              label: 'Quarto',
              template: (residence) => {
                return residence.room ? residence.room.name : '';
              },
              sortable: false,
            })
            break;
          case 'current_regime_id':
            this.columns.push({
              key: '',
              label: 'Regime Atual',
              template: (residence) => {
                return residence.currentRegime ? residence.currentRegime.translations.find(x => x.language_id == 3).name : '';
              },
              sortable: false,
            })
            break;
          case 'regime_id':
            this.columns.push({
              key: '',
              label: 'Regime',
              template: (residence) => {
                return residence.regime ? residence.regime.translations.find(x => x.language_id == 3).name : '';
              },
              sortable: false,
            })
            break;
          case 'applied_scholarship':
            this.columns.push({
              key: 'applied_scholarship',
              label: 'Candidatou-se a Bolsa de Estudo',
              sortable: true,
              tag: TagComponent.YesNoTag,
            })
            break;
          case 'incapacity':
            this.columns.push({
              key: 'incapacity',
              label: 'Incapacidade',
              sortable: true,
              tag: TagComponent.YesNoTag,
            })
            break;
          case 'nationality':
            this.columns.push({
              key: 'nationality',
              label: 'Nacionalidade',
              sortable: true,
            })
            break;
          case 'gender':
            this.columns.push({
              key: '',
              label: 'Género',
              template: (residence) => {
                return residence.gender == 'M' ? 'Masculino' : residence.gender == 'F' ? 'Feminino' : 'Outro';
              },
              sortable: true,
            })
            break;
          case 'international_student':
            this.columns.push({
              key: 'international_student',
              label: 'Internacional',
              sortable: true,
              tag: TagComponent.YesNoTag,
            })
            break;
          case 'organic_unit':
            this.columns.push({
              key: '',
              label: 'Escola',
              template: (residence) => {
                return residence.organic_unit ? residence.organic_unit.name : '';
              },
              sortable: false,
            })
            break;
          case 'courseDegree':
            this.columns.push({
              key: '',
              label: 'Grau',
              template: (residence) => {
                return residence.course.courseDegree ? residence.course.courseDegree.name : '';
              },
              sortable: false,
            })
            break;
          case 'course':
            this.columns.push({
              key: '',
              label: 'Curso',
              template: (residence) => {
                return residence.course ? residence.course.name : '';
              },
              sortable: false,
            })
            break;
          case 'patrimony_category':
            this.columns.push({
              key: 'patrimony_category',
              label: 'Escalão Património',
              sortable: true,
            })
            break;
          case 'household_elements_number':
            this.columns.push({
              key: 'household_elements_number',
              label: 'Nº Elementos Agregado',
              sortable: true,
            })
            break;
          case 'household_total_income':
            this.columns.push({
              key: 'household_total_income',
              label: 'Total de Rendimentos',
              sortable: true,
            })
            break;
          case 'per_capita_income':
            this.columns.push({
              key: 'per_capita_income',
              label: 'Rendimento per Capita',
              sortable: true,
            })
            break;
          case 'start_date':
            this.columns.push({
              key: '',
              label: 'Período Alojamento',
              template: (data) => {
                return moment(new Date(data.start_date)).format('DD/MM/YYYY') + ' - ' +  moment(new Date(data.end_date)).format('DD/MM/YYYY');

              },
              sortable: true,
            })
            break;
          case 'household_distance':
            this.columns.push({
              key: '',
              label: 'Distância da Residência',
              template: (data) => {
                return data.household_distance ? data.household_distance + ' km' : '';
              },
              sortable: true,
            })
            break;
        }

      }
    })
    this.loading = false;
  }

  updateAllChecked(): void {
    if (this.allChecked) {
      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
        ...item,
        checked: true
      }));
      this.check();
    } else {
      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
        ...item,
        checked: false
      }));
      this.check();
    }
  }
}
