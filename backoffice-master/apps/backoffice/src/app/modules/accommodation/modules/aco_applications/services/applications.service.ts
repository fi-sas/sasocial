import { ApplicationStatusBulkModel } from '../models/application-status-bulk.model';
import { ApplicationStatusModel } from '../models/application-status.model';
import { first } from 'rxjs/operators';
import { ApplicationData } from '../models/application-data.model';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ApplicationModel } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/models/application.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { PatrimonyCategoriesModel } from '../models/patrimony-categories.model';
import { ApplicationPhaseModel } from '../../phases/models/application-phase.model';
import { CanAplyApplicationModel } from '../models/can-aply.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService extends Repository<ApplicationModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.APPLICATIONS';
    this.entity_url = 'ACCOMMODATION.APPLICATIONS_ID';
  }

  getApplicationsStats(residence_id?: number, academic_year?: string, application_phase_id?: number): Observable<Resource<any>> {
    let params = new HttpParams();

    if (residence_id) {
      params = params.append('residence_id', residence_id.toString());
    }

    if (academic_year) {
      params = params.append('academic_year', academic_year);
    }

    if (application_phase_id) {
      params = params.append('application_phase_id', application_phase_id.toString());
    }

    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_STATS');
    return this.resourceService.read<any>(url, {
      params
    }).pipe(first());
  }

  getContractChangeStats(residence_id?: number, academic_year?: string, application_phase_id?: number): Observable<Resource<any>> {
    let params = new HttpParams();

    if (residence_id) {
      params = params.append('residence_id', residence_id.toString());
    }

    if (academic_year) {
      params = params.append('academic_year', academic_year);
    }

    if (application_phase_id) {
      params = params.append('application_phase_id', application_phase_id.toString());
    }

    const url = this.urlService.get('ACCOMMODATION.CONTRACT_CHANGE_STATS');
    return this.resourceService.read<any>(url, {
      params
    }).pipe(first());
  }

  openPhases(): Observable<Resource<ApplicationPhaseModel>> {
    return this.resourceService.read<ApplicationPhaseModel>(
      this.urlService.get('ACCOMMODATION.APPLICATION_PHASE_OPEN')
    );
  }

  canAply(user_id: number, application_phase_id: number): Observable<Resource<CanAplyApplicationModel>> {
    let params = new HttpParams();
    params = params.append('user_id', user_id.toString());
    params = params.append('application_phase_id', application_phase_id.toString());
    return this.resourceService.read<CanAplyApplicationModel>(
      this.urlService.get('ACCOMMODATION.CAN_APLY'), {
      params
    }
    );
  }



  getPatrimonyCategories(): Observable<Resource<PatrimonyCategoriesModel>> {
    let params = new HttpParams();
    params = params.set('query[active]', 'true');
    params = params.set('sort', 'description');
    return this.resourceService.list<PatrimonyCategoriesModel>(
      this.urlService.get('ACCOMMODATION.PATRIMONY_CATEGORIES'),
      {
        params
      }
    );
  }

  applicationBackofficeData(id: number, application: ApplicationData): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.BACKOFFICE_DATA', { id });
    return this.resourceService.patch<ApplicationModel>(url, application).pipe(first());
  }

  changeStatus(id: number, statusData: ApplicationStatusModel): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_ID_STATUS', { id });
    return this.resourceService.create<ApplicationModel>(url, statusData).pipe(first());
  }

  rollbackStatus(id: number, event: string, notes: string): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_ID_STATUS_ROLLBACK', { id });
    return this.resourceService.create<ApplicationModel>(url, { event, notes }).pipe(first());
  }


  changeStatusBulk(statusData: ApplicationStatusBulkModel): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_STATUS');
    return this.resourceService.create<ApplicationModel>(url, statusData).pipe(first());
  }

  acceptDecision(id: number): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_ID_APPROVE_STATUS', { id });
    return this.resourceService.create<ApplicationModel>(url, {}).pipe(first());
  }

  rejectDecision(id: number): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_ID_REJECT_STATUS', { id });
    return this.resourceService.create<ApplicationModel>(url, {}).pipe(first());
  }

  premuteRoom(id: number, premute_with_id: number): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS_PREMUTE_ROOM', { id });
    return this.resourceService.create<ApplicationModel>(url, {
      premute_with_id
    }).pipe(first());
  }


  searchApplicationsToBilling(search: string, residence_id?: number, academic_year?: string): Observable<Resource<ApplicationModel>> {
    const url = this.urlService.get('ACCOMMODATION.APPLICATIONS');
    let params = new HttpParams()
      .append('withRelated', 'room,assignedResidence')
      .append('searchFields', 'full_name,tin,student_number')
      .append('search', search)
      .append('query[status]', 'contracted')
      .append('query[status]', 'withdrawal')
      .append('sort', 'full_name');
    if (residence_id)
      params = params.append('query[assigned_residence_id]', residence_id.toString())
    if (academic_year)
      params = params.append('query[academic_year]', academic_year);
    return this.resourceService.list<ApplicationModel>(url, { params }).pipe(first());
  }
}
