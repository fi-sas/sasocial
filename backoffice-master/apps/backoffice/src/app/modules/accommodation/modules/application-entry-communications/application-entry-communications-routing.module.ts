import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListApplicationEntryCommunicationsComponent } from './pages/list-application-entry-communications/list-application-entry-communications.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListApplicationEntryCommunicationsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationEntryCommunicationsRoutingModule { }
