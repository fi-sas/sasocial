import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationEntryCommunicationsRoutingModule } from './application-entry-communications-routing.module';
import { ListApplicationEntryCommunicationsComponent } from './pages/list-application-entry-communications/list-application-entry-communications.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewApplicationEntryCommunicationComponent } from './components/view-application-entry-communication/view-application-entry-communication.component';

@NgModule({
  declarations: [ListApplicationEntryCommunicationsComponent, ViewApplicationEntryCommunicationComponent],
  imports: [
    CommonModule,
    ApplicationEntryCommunicationsRoutingModule,
    SharedModule
  ]
})
export class ApplicationEntryCommunicationsModule { }
