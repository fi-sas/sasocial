import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewApplicationEntryCommunicationComponent } from './view-application-entry-communication.component';

describe('ViewApplicationEntryCommunicationComponent', () => {
  let component: ViewApplicationEntryCommunicationComponent;
  let fixture: ComponentFixture<ViewApplicationEntryCommunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewApplicationEntryCommunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewApplicationEntryCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
