import { Component, Input, OnInit } from '@angular/core';
import { ApplicationCommunicationModel } from '@fi-sas/backoffice/modules/accommodation/models/Application-communication.model';

@Component({
  selector: 'fi-sas-view-application-entry-communication',
  templateUrl: './view-application-entry-communication.component.html',
  styleUrls: ['./view-application-entry-communication.component.less']
})
export class ViewApplicationEntryCommunicationComponent implements OnInit {

  @Input() data: ApplicationCommunicationModel = null;

  constructor() { }

  ngOnInit() {
  }

}
