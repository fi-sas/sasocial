import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApplicationEntryCommunicationsComponent } from './list-application-entry-communications.component';

describe('ListApplicationEntryCommunicationsComponent', () => {
  let component: ListApplicationEntryCommunicationsComponent;
  let fixture: ComponentFixture<ListApplicationEntryCommunicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApplicationEntryCommunicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationEntryCommunicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
