import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationExitCommunicationsRoutingModule } from './application-exit-communications-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListApplicationExitCommunicationsComponent } from './pages/list-application-exit-communications/list-application-exit-communications.component';
import { ViewApplicationExitCommunicationComponent } from './components/view-application-exit-communication/view-application-exit-communication.component';

@NgModule({
  declarations: [ListApplicationExitCommunicationsComponent, ViewApplicationExitCommunicationComponent],
  imports: [
    CommonModule,
    ApplicationExitCommunicationsRoutingModule,
    SharedModule,
  ],
})
export class ApplicationExitCommunicationsModule { }
