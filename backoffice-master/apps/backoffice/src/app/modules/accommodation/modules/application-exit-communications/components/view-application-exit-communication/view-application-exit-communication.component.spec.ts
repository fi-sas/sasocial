import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewApplicationExitCommunicationComponent } from './view-application-exit-communication.component';

describe('ViewApplicationExitCommunicationComponent', () => {
  let component: ViewApplicationExitCommunicationComponent;
  let fixture: ComponentFixture<ViewApplicationExitCommunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewApplicationExitCommunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewApplicationExitCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
