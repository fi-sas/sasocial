import { Component, Input, OnInit } from '@angular/core';
import { ApplicationCommunicationModel } from '@fi-sas/backoffice/modules/accommodation/models/Application-communication.model';

@Component({
  selector: 'fi-sas-view-application-exit-communication',
  templateUrl: './view-application-exit-communication.component.html',
  styleUrls: ['./view-application-exit-communication.component.less']
})
export class ViewApplicationExitCommunicationComponent implements OnInit {

  @Input() data: ApplicationCommunicationModel = null;

  constructor() { }

  ngOnInit() {
  }

}
