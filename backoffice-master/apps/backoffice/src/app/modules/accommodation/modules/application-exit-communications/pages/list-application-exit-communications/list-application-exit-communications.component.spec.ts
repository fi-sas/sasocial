import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApplicationExitCommunicationsComponent } from './list-application-exit-communications.component';

describe('ListApplicationExitCommunicationsComponent', () => {
  let component: ListApplicationExitCommunicationsComponent;
  let fixture: ComponentFixture<ListApplicationExitCommunicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApplicationExitCommunicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationExitCommunicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
