import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ApplicationCommunicationStatusTranslations, ApplicationCommunicationModel } from '@fi-sas/backoffice/modules/accommodation/models/Application-communication.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { ApplicationCommunicationsService } from '@fi-sas/backoffice/modules/accommodation/services/application-communications.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-application-exit-communications',
  templateUrl: './list-application-exit-communications.component.html',
  styleUrls: ['./list-application-exit-communications.component.less']
})
export class ListApplicationExitCommunicationsComponent extends TableHelper implements OnInit {

  ApplicationCommunicationStatusTranslations = ApplicationCommunicationStatusTranslations;

  //MODAL
  changeStatusModalResponse = null;
  changeStatusModalObservations = null;
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalLoading = false;
  changeStatusModalCommunicationSelected: ApplicationCommunicationModel = null;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public applicationCommunicationsService: ApplicationCommunicationsService,
    private accommodationService: AccommodationService,
  ) {
    super(uiService, router, activatedRoute);

    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'application.assignedResidence.name',
        label: 'Residência',
        sortable: false,
      },
      {
        key: 'application.room.name',
        label: 'Quarto',
        sortable: false,
      },
      {
        key: 'end_date',
        label: 'Data saída',
        template: (data) => {
          return moment(new Date(data.end_date)).format('DD/MM/YYYY');
        },
        sortable: true,
      },
      {
        key: 'exit_hour',
        label: 'Hora saída',
        sortable: true,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ApplicationCommunicationStatusTranslations
      },
    );

    this.persistentFilters['withRelated'] = 'application';
    this.persistentFilters['searchFields'] = 'full_name,email,student_number,tin,identification';
    this.persistentFilters['type'] = 'EXIT';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.applicationCommunicationsService);
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true);
  }

  openModal(communication: ApplicationCommunicationModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalCommunicationSelected = communication;
    this.changeStatusModalAction = action;
  }
  handleChangeStatus() {
    if (this.changeStatusModalAction === 'reply') {
      this.changeStatusModalLoading = true;
      this.applicationCommunicationsService.reply(this.changeStatusModalCommunicationSelected.id, this.changeStatusModalResponse).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.applicationCommunicationsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'close') {
      this.changeStatusModalLoading = true;
      this.applicationCommunicationsService.close(this.changeStatusModalCommunicationSelected.id, this.changeStatusModalObservations).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.applicationCommunicationsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }
  }

  handleChangeStatusCancel() {
    this.changeStatusModalCommunicationSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalResponse = '';
    this.changeStatusModalObservations = '';
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "reply") {
      return "Responder á comunicação de saída";
    }
    if (this.changeStatusModalAction == "close") {
      return "Fechar comunicação de saída";
    }
    return "";
  }
  modalOkButton(): string {
    if (this.changeStatusModalAction == "reply") {
      return "Responder";
    }
    if (this.changeStatusModalAction == "close") {
      return "Fechar";
    }
    return "";

  }

}
