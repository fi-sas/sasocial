import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingsListComponent } from './components/billings-list/billings-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  // {
  //   path: 'dashboard',
  //   component: DashboardComponent,
  //   data: { breadcrumb: 'Painel', title: 'Painel', scope: 'accommodation:billings:read' }
  // },
  {
    path: 'list-billings',
    component: BillingsListComponent,
    data: { breadcrumb: 'Listagem', title: 'Faturação', scope: 'accommodation:billings:read' }
  },
  /* {
     path: 'analyze-contractors',
     component: AnalyzeContractorsComponent,
     data: { breadcrumb: 'Analíse de contratos', title: 'Analíse de contratos', scope: 'accommodation:billings:read' }
   },
    {
      path: 'analyze-items',
      component: AnalyzeItemsComponent,
      data: { breadcrumb: 'Analíse de movimentos', title: 'Analíse de movimentos' }
    },*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule { }
