import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ReportModalComponent } from './components/report-modal/report-modal.component';
import { BillintgItemFormModalComponent } from './components/billintg-item-form-modal/billintg-item-form-modal.component';
import { AnalyzeItemsComponent } from './pages/analyze-items/analyze-items.component';
import { ProccessModalComponent } from './components/proccess-modal/proccess-modal.component';
import { BillingsListComponent } from './components/billings-list/billings-list.component';
import { ViewBillingComponent } from './components/view-billing/view-billing.component';
import { FormBillingModalComponent } from './components/form-billing-modal/form-billing-modal.component';
import { AlertRecordBillingsComponent } from './components/alert-record-billings/alert-record-billings.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ReportModalComponent,
    BillintgItemFormModalComponent,
    AnalyzeItemsComponent,
    ProccessModalComponent,
    BillingsListComponent,
    ViewBillingComponent,
    FormBillingModalComponent,
    AlertRecordBillingsComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    BillingRoutingModule,
    SharedModule,
  ],
  entryComponents: [
    BillintgItemFormModalComponent,
    ProccessModalComponent,
    ReportModalComponent,
    FormBillingModalComponent
  ]
})
export class BillingModule { }
