import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertRecordBillingsComponent } from './alert-record-billings.component';

describe('AlertRecordBillingsComponent', () => {
  let component: AlertRecordBillingsComponent;
  let fixture: ComponentFixture<AlertRecordBillingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertRecordBillingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertRecordBillingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
