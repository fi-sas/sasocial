import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertModel } from '../../models/alert.model';

@Component({
  selector: 'fi-sas-alert-record-billings',
  templateUrl: './alert-record-billings.component.html',
  styleUrls: ['./alert-record-billings.component.less']
})
export class AlertRecordBillingsComponent implements OnInit {

  @Input() alertInfo: AlertModel = null;
  @Output() closeAlert = new EventEmitter();
  @Output() selectAllRecords = new EventEmitter();

  textDescription: string = null;

  constructor() { }

  ngOnInit() {
  }

  afterClose(){
    this.closeAlert.emit(true);
  }

  selectAction(){
      this.selectAllRecords.emit(true);
  }

}
