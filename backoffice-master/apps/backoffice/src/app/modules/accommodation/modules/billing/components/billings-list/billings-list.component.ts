import { AfterContentChecked, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as  moment from 'moment';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { finalize, first, tap } from 'rxjs/operators';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { RegimeModel } from '../../../regimes/models/regime.model';
import { RegimesService } from '../../../regimes/services/regimes.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { ResidencesService } from '../../../residences/services/residences.service';
import { BillingModel, BillingStatusTranslations } from '../../models/billing.model';
import { BillingService } from '../../services/billing.service';
import { ProccessModalComponent } from '../proccess-modal/proccess-modal.component';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import { FormBillingModalComponent } from '../form-billing-modal/form-billing-modal.component';

import { ReportsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/reports.service';
import { GeralSettingsService } from '@fi-sas/backoffice/modules/accommodation/services/geral-settings.service';
import { AlertModel } from '../../models/alert.model';

@Component({
  selector: 'fi-sas-billings-list',
  templateUrl: './billings-list.component.html',
  styleUrls: ['./billings-list.component.less']
})
export class BillingsListComponent extends TableHelper implements OnInit, AfterContentChecked{

  loadingTariffs = false;
  tariffs: TariffModel[] = [];

  loadingResidences = false;
  residences: ResidenceModel[] = [];

  BillingStatusTranslations = BillingStatusTranslations;
  enable_status_external_validate_or_paid = false;
  checked = false;
  indeterminate = false;
  setOfCheckedId = new Set<number>();

  accommodationData;
  loadingRegimes = false;
  regimes: RegimeModel[] = [];
  monthPicker = new Date();

  allSelected: BillingModel[] = [];
  showAllRecordsAlert: boolean = false;

  alertModel: AlertModel = new AlertModel();

  constructor(
    accommodationService: AccommodationService,
    uiService: UiService,
    activateRoute: ActivatedRoute,
    activatedRoute: ActivatedRoute,
    public router: Router,
    private reportsService: ReportsService,
    private messageService: NzMessageService,
    private tariffsService: TariffsService,
    private residencesService: ResidencesService,
    private billingsService: BillingService,
    private regimesService: RegimesService,
    private nzModalSerice: NzModalService,
    private settings: GeralSettingsService,
  ) {
    super(uiService, router, activatedRoute);
    this.filters = {};
    this.filters.application = {};


    this.persistentFilters = {
      searchFields: 'full_name,tin,student_number',
      sort: "application.full_name,billing_start_date"
    };
    this.accommodationData = accommodationService.getAccommodationData();
    if (this.accommodationData.residence) {
      this.filters.application.assigned_residence_id = this.accommodationData.residence.id;
    }
    const today = moment();
    this.filters.month = (today.month() + 1) + '';
    this.filters.year = today.year().toString();
  }
  ngAfterContentChecked(): void {
    this.alertModel.total = this.totalData;
    this.alertModel.n_selected = this.setOfCheckedId.size;
  }

  ngOnInit() {
    this.settingsGet();
    this.loadTariffs();
    this.loadRegimes();
    this.loadResidences();
    this.initTableData(this.billingsService);

  }

  settingsGet(){
    this.settings.list(1, -1).pipe(first()).subscribe((data) => {
      this.enable_status_external_validate_or_paid = data.data[0].ENABLE_STATUS_EXTERNAL_VALIDATE_OR_PAID;
    })
  }

  loadRegimes() {
    this.loadingRegimes = true;
    this.regimesService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingRegimes = false))
      )
      .subscribe((results) => {
        this.regimes = results.data;
      });
  }


  loadResidences() {
    this.loadingResidences = true;
    this.residencesService
      .list(1, -1, null, null, { active: true, available_for_assign: true })
      .pipe(
        first(),
        finalize(() => (this.loadingResidences = false))
      )
      .subscribe((results) => {
        this.residences = results.data;
      });
  }

  refreshList(event) {
    if(event){
      this.initTableData(this.billingsService);
    }
  }

  loadTariffs() {
    this.loadingTariffs = true;
    this.tariffsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingTariffs = false))
      )
      .subscribe((results) => {
        this.tariffs = results.data;
      });
  }

  listComplete() {
    this.monthPicker = null;
    this.filters = {};
    this.filters.application = {};
    this.filters.search = null;
    this.filters.status = null;
    /*this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId = new Set<number>();*/
    this.search();
  }

  getTotalPrice(i) {
    let sum_price = 0;
    for (const item of this.data[i].billing_items) {
      sum_price += item.price;
    }
    // return sum_price;
    return +sum_price.toFixed(2);
  }
  getTotalExtrasPrice(i) {
    let sum_price = 0;
    for (const item of this.data[i].billing_items.filter(x => x.extra_id != null)) {
      sum_price += item.price;
    }
    return sum_price.toFixed(2);
  }
  getTotalRegimePrice(i) {
    let sum_price = 0;
    for (const item of this.data[i].billing_items.filter(x => x.regime_id != null)) {
      sum_price += item.price;
    }
    return sum_price.toFixed(2);
  }
  getTotalTypologyPrice(i) {
    let sum_price = 0;
    for (const item of this.data[i].billing_items.filter(x => x.typology_id != null)) {
      sum_price += item.price;
    }
    return sum_price.toFixed(2);
  }

  validateBilling(id: number) {
    this.uiService
      .showConfirm(
        'Validar movimento',
        'Pretende validar o movimento?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.status({ action: 'REVIEW', billing_ids: [id] })
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });

  }

  cancelBilling(id: number) {
    this.uiService
      .showConfirm(
        'Cancelar movimento',
        'Pretende cancelar este movimento?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.status({ action: 'CANCEL', billing_ids: [id] })
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });
  }

  changeStatusToExternalBilled(id: number) {
    this.uiService
      .showConfirm(
        'Alterar estado do movimento',
        'Pretende alterar o estado do para "Validado Externo"?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.status({ action: 'EXTERNAL_VALIDATE', billing_ids: [id] })
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });
  }

  changeStatusToExternalPaid(id: number) {
    this.uiService
      .showConfirm(
        'Alterar estado do movimento',
        'Pretende alterar o estado do para "Pago Externo"?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.status({ action: 'EXTERNAL_PAID', billing_ids: [id] })
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });
  }

  createInvoice(id: number) {
    this.uiService
      .showConfirm(
        'Faturar este movimento',
        'Pretende enviar este mvimento para conta corrente e ser faturado ?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.status({ action: 'REVIEW', billing_ids: [id] })
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });
  }

  reprocess(id: number) {
    this.uiService
      .showConfirm(
        'Reprocessar movimento',
        'Pretende reprocessar este movimento?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.reprocess(id)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
            });
        }
      });
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData): void {
    this.data = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.data.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
    this.checkInfoModal();

  }

  onAllChecked(checked: boolean): void {
    this.data
      .filter(({ disabled }) => !disabled)
      .forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
    this.checkInfoModal();
  }

  checkInfoModal(){
    if(this.setOfCheckedId.size !== this.totalData && (this.totalData/this.pageSize) > 1  && this.setOfCheckedId.size !== 0){
      this.alertModel.total = this.totalData;
      this.alertModel.n_selected = this.setOfCheckedId.size;
      this.showAllRecordsAlert = true
    }else if(this.setOfCheckedId.size === 0 || this.setOfCheckedId.size === this.totalData){
      this.closeAlert();
    }
  }

  selectAllRecords(){
    this.loading = true;
    this.billingsService.list(1, -1, this.sortKey, this.sortValue,
      { ...this.filterObject(), withRelated: false, fields: "id"}, this.extraFiltersObject()).pipe(first(), finalize(() => {
      this.loading = false;
    })).subscribe(response => {
      console.log(response)
      response.data.forEach((billing) => this.updateCheckedSet(billing.id, true));
      this.refreshCheckedStatus();
    });
  }

  closeAlert(){
    this.showAllRecordsAlert = false;
  }

  bulkOperation(action: string) {
    if (action == 'CANCEL') {
      this.uiService
        .showConfirm(
          'Cancelar movimentos',
          'Pretende cancelar os movimentos?',
          'Sim',
          'Não',
          'warning'
        )
        .subscribe((confirmed) => {
          if (confirmed) {
            this.loading = true;
            this.billingsService.status({ action, billing_ids: Array.from(this.setOfCheckedId) })
              .pipe(
                first(),
                finalize(() => (this.loading = false))
              )
              .subscribe((results) => {
                this.initTableData(this.billingsService);
                this.setOfCheckedId = new Set<number>();
              });
          }
        });
    } else {
      this.uiService
        .showConfirm(
          'Validar movimentos',
          'Pretende validar os movimentos?',
          'Sim',
          'Não',
          'warning'
        )
        .subscribe((confirmed) => {
          if (confirmed) {
            this.loading = true;
            this.billingsService.status({ action, billing_ids: Array.from(this.setOfCheckedId) })
              .pipe(
                first(),
                finalize(() => (this.loading = false))
              )
              .subscribe((results) => {
                this.initTableData(this.billingsService);
                this.setOfCheckedId = new Set<number>();
              });
          }
        });
    }


  }
  search() {
    if (this.monthPicker) {
      const date = moment(this.monthPicker);
      const month = date.month() + 1;
      this.filters.month = month.toString();
      this.filters.year = date.year().toString();
    } else {
      this.filters.month = null;
      this.filters.year = null;
    }
    this.checked = false;
    this.indeterminate = false;
    this.showAllRecordsAlert = false;
    this.setOfCheckedId = new Set<number>();
    this.searchData(true);
  }

  changeByResidence() {
    const modal = this.nzModalSerice.create({
      nzContent: ProccessModalComponent,
      nzTitle: "Operações por residência",
      nzComponentParams: {
        residences: this.residences,
        residence_id: this.filters.application.assigned_residence_id
      },
      nzOnOk: data => {
        return data.submit();
      },
      nzOnCancel: data => {
        return data.close();
      }
    });
    modal.afterClose.subscribe(data => {
      if (data)
        this.search();
    });
  }


  addNewBilling() {
    const modal = this.nzModalSerice.create({
      nzContent: FormBillingModalComponent,
      nzTitle: "Adicionar movimento",
      nzComponentParams: {
      },
      nzOnOk: data => {
        return data.submit();
      },
      nzOnCancel: data => {
        return data.close();
      }, nzWidth: 650
    });
    modal.afterClose.subscribe(data => {
      if (data) {
        this.search();
        return;
      }
    });
  }

  remove(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar movimento',
        'Pretende eliminar o movimento?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.loading = true;
          this.billingsService.delete(id)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((results) => {
              this.initTableData(this.billingsService);
              this.setOfCheckedId = new Set<number>();
            });
        }
      });
  }

  generateSepaFile_loading = false;
  generateSepaFile(){

    let year_sepa = this.filters.year;
    let month_sepa = this.filters.month;
    let billing_ids = Array.from(this.setOfCheckedId);

    this.generateSepaFile_loading = true;
    this.reportsService.generateURLSepaFile(
      billing_ids,
       year_sepa,
       month_sepa
     ).pipe(tap(() => this.generateSepaFile_loading = true), finalize(() => this.generateSepaFile_loading = false))
       .subscribe(result => {
         return this.messageService.success("Ficheiro gerado com sucesso!");
       },err => {
 
         console.error(err);
 
         if(err.error)
         {
           return this.messageService.warning(JSON.stringify(err.error));
         }
       });
   }
}
