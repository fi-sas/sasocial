import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillintgItemFormModalComponent } from './billintg-item-form-modal.component';

describe('BillintgItemFormModalComponent', () => {
  let component: BillintgItemFormModalComponent;
  let fixture: ComponentFixture<BillintgItemFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillintgItemFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillintgItemFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
