import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ExtrasService } from '../../../extras/services/extras.service';
import { ExtraModel } from '../../../extras/models/extra.model';
import { first, finalize, tap } from 'rxjs/operators';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd';
import { TypologiesService } from '../../../typologies/services/typologies.service';
import { RegimesService } from '../../../regimes/services/regimes.service';
import { TypologyModel } from '../../../typologies/models/typology.model';
import { RegimeModel } from '../../../regimes/models/regime.model';
import { PaymentMethodModel } from '@fi-sas/backoffice/modules/financial/models/payment-method.model';
import { BillingItemService } from '../../services/billing-item.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-billintg-item-form-modal',
  templateUrl: './billintg-item-form-modal.component.html',
  styleUrls: ['./billintg-item-form-modal.component.less']
})
export class BillintgItemFormModalComponent implements OnInit, OnDestroy {
  billing_id = 0;
  billing_item_id = null;

  extraChangeSubs: Subscription = null;
  regimeChangeSubs: Subscription = null;
  typologyChangeSubs: Subscription = null;

  itemGroup: FormGroup = new FormGroup({
    product_code: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    typology_id: new FormControl(null),
    regime_id: new FormControl(null),
    extra_id: new FormControl(null),
    quantity: new FormControl(1, Validators.required),
    unit_price: new FormControl(0, Validators.required),
    period: new FormControl('MONTH', Validators.required),
    vat_id: new FormControl(null, Validators.required),
    option: new FormControl('extra'),
    discount: new FormControl(null, Validators.max(100)),
    discount_value: new FormControl(null),
    processed_manually: new FormControl(null),
  });

  loading_extras = false;
  extras: ExtraModel[] = [];

  loading_typologies = false;
  typologies: TypologyModel[] = [];

  loading_regimes = false;
  regimes: RegimeModel[] = [];

  loading_taxes = false;
  taxes: TaxModel[] = [];
  descriptionInit: string;
  loading_paymentMethods = false;
  paymentMethods: PaymentMethodModel[] = [];



  constructor(
    private nzRef: NzModalRef,
    private billingItemService: BillingItemService,
    private taxesService: TaxesService,
    private extrasService: ExtrasService,
    private typologiesService: TypologiesService,
    private regimesService: RegimesService,
    private uiService: UiService
  ) {
  }
  startSubscriptions(): void {
    this.extraChangeSubs = this.itemGroup.get('extra_id').valueChanges.subscribe(e => {
      if(this.itemGroup.value.option == 'extra' && e != this.itemGroup.value.extra_id) {
        if (e instanceof ExtraModel) {
          this.extraChanged(e);
        } else {
          this.extraChanged(this.extras.find(x => x.id == e));
        }
      }

    });
    this.regimeChangeSubs = this.itemGroup.get('regime_id').valueChanges.subscribe(e => {
      if(this.itemGroup.value.option == 'regime' && e != this.itemGroup.value.regime_id) {
        if (e instanceof RegimeModel) {
          this.regimeChanged(e);
        } else {
          this.regimeChanged(this.regimes.find(x => x.id == e));
        }
      }

    });
    this.typologyChangeSubs = this.itemGroup.get('typology_id').valueChanges.subscribe(e => {
      if(this.itemGroup.value.option == 'typology' && e != this.itemGroup.value.typology_id) {
        if (e instanceof TypologyModel) {
          this.typologyChanged(e);
        } else {
          this.typologyChanged(this.typologies.find(x => x.id == e));
        }
      }
    });
  }

  ngOnInit() {
    this.loadTaxes();
    this.loadExtras();
    this.loadTypologies();
    this.loadRegimes();

    if (this.billing_item_id) {
      this.billingItemService.read(this.billing_item_id).pipe(
        first(),
      ).subscribe(result => {
        this.itemGroup.patchValue({
          product_code: result.data[0].product_code,
          description: result.data[0].description,
          name: result.data[0].name,
          quantity: result.data[0].quantity,
          unit_price: result.data[0].unit_price,
          period: result.data[0].period,
          vat_id: result.data[0].vat_id,
          extra_id: result.data[0].extra_id,
          typology_id: result.data[0].typology_id,
          regime_id: result.data[0].regime_id,
          price: result.data[0].price,
          discount: result.data[0].discount,
          discount_value: result.data[0].discount_value,
          processed_manually: result.data[0].processed_manually,
        });
        this.descriptionInit = result.data[0].description;
        if (result.data[0].typology_id) {
          this.itemGroup.patchValue({ option: 'typology' });
        } else if (result.data[0].regime_id) {
          this.itemGroup.patchValue({ option: 'regime' });
        } else if (result.data[0].extra_id) {
          this.itemGroup.patchValue({ option: 'extra' });
        }
        this.startSubscriptions();
      })
    } else {
      this.startSubscriptions();
      this.resetForm();

    }
  }

  priceCalculation(){
    if(this.itemGroup.value.unit_price && this.itemGroup.value.quantity && this.itemGroup.value.discount){
      const price = this.itemGroup.value.unit_price * this.itemGroup.value.quantity
      this.itemGroup.patchValue({
          discount_value: price ?  ( price * this.itemGroup.value.discount)/100 : 0,
      })
    }else{
      this.itemGroup.controls.discount_value.reset()
    }
  }

  validItem() {
    if (this.itemGroup.value.option == 'extra') {
      this.itemGroup.controls.extra_id.setValidators([Validators.required]);
      this.itemGroup.controls.regime_id.clearValidators();
      this.itemGroup.controls.typology_id.clearValidators();
    } else if (this.itemGroup.value.option == 'typology') {
      this.itemGroup.controls.typology_id.setValidators([Validators.required]);
      this.itemGroup.controls.regime_id.clearValidators();
      this.itemGroup.controls.extra_id.clearValidators();
    } else if (this.itemGroup.value.option == 'regime') {
      this.itemGroup.controls.regime_id.setValidators([Validators.required]);
      this.itemGroup.controls.typology_id.clearValidators();
      this.itemGroup.controls.extra_id.clearValidators();
    }
    this.itemGroup.controls.regime_id.updateValueAndValidity();
    this.itemGroup.controls.typology_id.updateValueAndValidity();
    this.itemGroup.controls.extra_id.updateValueAndValidity();
  }

  ngOnDestroy() {
    this.extraChangeSubs.unsubscribe();
    this.regimeChangeSubs.unsubscribe();
    this.typologyChangeSubs.unsubscribe();
  }

  loadExtras() {
    this.loading_extras = true;
    this.extrasService.list(1, -1).pipe(
      first(),
      finalize(() => this.loading_extras = false)
    ).subscribe(results => {
      this.extras = results.data;
    });
  }

  loadTypologies() {
    this.loading_typologies = true;
    this.typologiesService.list(1, -1).pipe(
      first(),
      finalize(() => this.loading_typologies = false)
    ).subscribe(results => {
      this.typologies = results.data;
    });
  }

  loadRegimes() {
    this.loading_regimes = true;
    this.regimesService.list(1, -1).pipe(
      first(),
      finalize(() => this.loading_regimes = false)
    ).subscribe(results => {
      this.regimes = results.data;
    });
  }

  loadTaxes() {
    this.loading_taxes = true;
    this.taxesService.list(1, -1).pipe(
      first(),
      finalize(() => this.loading_taxes = false)
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  extraChanged(extra: ExtraModel) {
    if (extra) {
      this.itemGroup.patchValue({ regime_id: null, typology_id: null });
      this.itemGroup.patchValue({
        product_code: extra.product_code,
        name: extra.translations.find(trans => trans.language_id == 3) ? extra.translations.find(trans => trans.language_id == 3).name : 'Sem Nome',
        unit_price: extra.price,
        description: this.billing_item_id ? this.descriptionInit : ((extra.translations.find(trans => trans.language_id == 3) ? extra.translations.find(trans => trans.language_id == 3).name : 'Sem Nome') + '_' +
          (this.itemGroup.get('period').value == 'MONTH' ? 'Mês' : this.itemGroup.get('period').value == 'WEEK' ? 'Semana' : this.itemGroup.get('period').value == 'DAY' ? 'Dia' : '')),
        vat_id: this.taxes.find(t => t.id === extra.vat_id) ? this.taxes.find(t => t.id === extra.vat_id).id : null,
      });
    } else {
      this.itemGroup.patchValue({ product_code: null, unit_price: null, vat: null, name: null, description: null });
    }
  }

  changePeriod() {
    if(!this.billing_item_id) {
      this.itemGroup.patchValue({
        description: this.itemGroup.get('name').value + '_' +
          (this.itemGroup.get('period').value == 'MONTH' ? 'Mês' : this.itemGroup.get('period').value == 'WEEK' ? 'Semana' : this.itemGroup.get('period').value == 'DAY' ? 'Dia' : ''),
      });
    }
  }

  typologyChanged(typology: TypologyModel) {
    if (typology) {
      this.itemGroup.patchValue({ regime_id: null, extra_id: null });
      this.itemGroup.patchValue({
        product_code: typology.product_code,
        name: typology.translations.find(trans => trans.language_id == 3) ? typology.translations.find(trans => trans.language_id == 3).name : 'Sem Nome',
        description: this.billing_item_id ? this.descriptionInit : ((typology.translations.find(trans => trans.language_id == 3) ? typology.translations.find(trans => trans.language_id == 3).name : 'Sem Nome') + '_' +
          (this.itemGroup.get('period').value == 'MONTH' ? 'Mês' : this.itemGroup.get('period').value == 'WEEK' ? 'Semana' : this.itemGroup.get('period').value == 'DAY' ? 'Dia' : '')),
        unit_price: 0,
        vat_id: null,
      });

    } else {
      this.itemGroup.patchValue({ product_code: null, unit_price: null, vat: null, name: null, description: null });
    }
  }

  regimeChanged(regime: RegimeModel) {
    if (regime) {
      this.itemGroup.patchValue({ typology_id: null, extra_id: null });
      this.itemGroup.patchValue({
        product_code: regime.product_code,
        description: this.billing_item_id ? this.descriptionInit : ((regime.translations.find(trans => trans.language_id == 3) ? regime.translations.find(trans => trans.language_id == 3).name : 'Sem Nome') + '_' +
          (this.itemGroup.get('period').value == 'MONTH' ? 'Mês' : this.itemGroup.get('period').value == 'WEEK' ? 'Semana' : this.itemGroup.get('period').value == 'DAY' ? 'Dia' : '')),
        name: regime.translations.find(trans => trans.language_id == 3) ? regime.translations.find(trans => trans.language_id == 3).name : 'Sem Nome',
        unit_price: 0,
        vat_id: null,
      });
    } else {
      this.itemGroup.patchValue({ product_code: null, unit_price: null, vat: null, name: null, description: null });
    }
  }

  submit(): Promise<boolean> {

    this.checkValidityOfControls([
      this.itemGroup.get('product_code'),
      this.itemGroup.get('name'),
      this.itemGroup.get('description'),
      this.itemGroup.get('quantity'),
      this.itemGroup.get('unit_price'),
      this.itemGroup.get('vat_id'),
      this.itemGroup.get('typology_id'),
      this.itemGroup.get('regime_id'),
      this.itemGroup.get('extra_id'),
      this.itemGroup.get('discount'),
      this.itemGroup.get('discount_value'),
    ]);
    if (!this.itemGroup.valid) {
      return Promise.resolve(false);
    }

    this.nzRef.getInstance().nzOkLoading = true;

    return new Promise(resolve => {
      if (!this.billing_item_id) {
        this.billingItemService.create({
          billing_id: this.billing_id,
          product_code: this.itemGroup.value.product_code,
          name: this.itemGroup.value.name,
          description: this.itemGroup.value.description,
          extra_id: this.itemGroup.value.extra_id,
          regime_id: this.itemGroup.value.regime_id,
          typology_id: this.itemGroup.value.typology_id,
          quantity: this.itemGroup.value.quantity,
          unit_price: this.itemGroup.value.unit_price,
          price:  this.itemGroup.value.discount ? ((this.itemGroup.value.unit_price * this.itemGroup.value.quantity) -  this.itemGroup.value.discount_value) : this.itemGroup.value.unit_price * this.itemGroup.value.quantity,
          vat_id: this.itemGroup.value.vat_id,
          vat_value: this.taxes.find(t => t.id === this.itemGroup.value.vat_id).tax_value,
          period: this.itemGroup.value.period,
          possible_retroactive: 0,
          discount: this.itemGroup.value.discount,
          discount_value: this.itemGroup.value.discount_value,
          processed_manually: true,
        }).pipe(
          first(),
          finalize(() => this.nzRef.getInstance().nzOkLoading = false)
        ).subscribe(result => {
          this.uiService.showMessage(MessageType.success, "Movimento criado com sucesso");
          resolve(true);
        }, err => {
          this.uiService.showMessage(MessageType.error, "Erro ao criar movimento");
          resolve(false);
        });
      }
      else {

        this.billingItemService.patch(this.billing_item_id, {
          billing_id: this.billing_id,
          product_code: this.itemGroup.value.product_code,
          name: this.itemGroup.value.name,
          description: this.itemGroup.value.description,
          extra_id: this.itemGroup.value.extra_id,
          regime_id: this.itemGroup.value.regime_id,
          typology_id: this.itemGroup.value.typology_id,
          quantity: this.itemGroup.value.quantity,
          unit_price: this.itemGroup.value.unit_price,
          price:  this.itemGroup.value.discount ? ((this.itemGroup.value.unit_price * this.itemGroup.value.quantity) -  this.itemGroup.value.discount_value) : this.itemGroup.value.unit_price * this.itemGroup.value.quantity,
          vat_id: this.itemGroup.value.vat_id,
          vat_value: this.taxes.find(t => t.id === this.itemGroup.value.vat_id).tax_value,
          period: this.itemGroup.value.period,
          possible_retroactive: 0,
          discount: this.itemGroup.value.discount,
          discount_value: this.itemGroup.value.discount_value,
          processed_manually: this.itemGroup.value.processed_manually,
        }).pipe(
          first(),
          finalize(() => this.nzRef.getInstance().nzOkLoading = false)
        ).subscribe(result => {
          this.uiService.showMessage(MessageType.success, "Movimento editado com sucesso");
          resolve(true);
        }, err => {
          this.uiService.showMessage(MessageType.error, "Erro ao editado movimento");
          resolve(false);
        });
      }
    })
  };

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  close() {
    this.nzRef.close();
    return Promise.resolve(true);
  }
  resetForm() {
    this.itemGroup.patchValue({
      product_code: '',
      description: '',
      name: '',
      quantity: 1,
      unit_price: 0,
      period: 'MONTH',
      vat_id: null,
      extra_id: null,
      typology_id: null,
      regime_id: null,
      price: 0,
    })
    this.validItem();
  }
}
