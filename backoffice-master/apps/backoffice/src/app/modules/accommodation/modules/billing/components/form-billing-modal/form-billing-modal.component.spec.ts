import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBillingModalComponent } from './form-billing-modal.component';

describe('FormBillingModalComponent', () => {
  let component: FormBillingModalComponent;
  let fixture: ComponentFixture<FormBillingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBillingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBillingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
