import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AccommodationData, AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { Resource } from '@fi-sas/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, first, switchMap } from 'rxjs/operators';
import { ApplicationModel, ApplicationStatusTranslations } from '../../../aco_applications/models/application.model';
import { ApplicationsService } from '../../../aco_applications/services/applications.service';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import { BillingService } from '../../services/billing.service';

@Component({
  selector: 'fi-sas-form-billing-modal',
  templateUrl: './form-billing-modal.component.html',
  styleUrls: ['./form-billing-modal.component.less']
})
export class FormBillingModalComponent implements OnInit {

  billingGroup = new FormGroup({
    application_id: new FormControl(null, Validators.required),
    tariff_id: new FormControl(null, Validators.required),
    month: new FormControl(moment().month() + 1, Validators.required),
    year: new FormControl(moment().year(), Validators.required),
    billing_start_date: new FormControl(null, Validators.required),
    billing_end_date: new FormControl(null, Validators.required),
    status: new FormControl('PROCESSED', Validators.required),
  });

  optionListSubscrition: Subscription = null;
  searchChange$ = new BehaviorSubject('');
  onChanged: any = () => { }
  onTouched: any = () => { }
  loading = false;
  applications: ApplicationModel[] = [];
  monthPicker = new Date();
  tariffs: TariffModel[] = [];
  ApplicationStatusTranslations = ApplicationStatusTranslations;
  constructor(
    private applicationsServices: ApplicationsService,
    private ref: ChangeDetectorRef,
    private tariffsService: TariffsService,
    private billingsService: BillingService,
    private uiService: UiService,
    private nzRef: NzModalRef,
    private accommodationService: AccommodationService,
  ) { }
  accommodationData: AccommodationData = null;
  ngOnInit() {
    this.accommodationData = this.accommodationService.getAccommodationData();
    const optionList$: Observable<Resource<ApplicationModel>> = this.searchChange$
      .asObservable()
      .pipe(
        // tap(() => (this.loading = true)),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(search =>
          this.applicationsServices.searchApplicationsToBilling(
            search,
            this.accommodationData.residence ? this.accommodationData.residence.id : null,
            this.accommodationData.academicYear ? this.accommodationData.academicYear : null,
          ).pipe(first(), finalize(() => {
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
          }))
        ),
      );

    this.optionListSubscrition = optionList$.subscribe(results => {
      this.applications = [...results.data];
      this.loading = false;
    });
    this.loadTariffs();
    this.loading = false;

    this.billingGroup.get('billing_start_date').setValue(moment().startOf('month').format('YYYY-MM-DD'));
    this.billingGroup.get('billing_end_date').setValue(moment().endOf('month').format('YYYY-MM-DD'));

  }
  loadTariffs() {
    this.tariffsService.list(0, -1, null, null).subscribe(response => {
      this.tariffs = response.data;
    });
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  updateFormValues() {
    this.billingGroup.get('month').setValue(moment(this.monthPicker).month() + 1);
    this.billingGroup.get('year').setValue(moment(this.monthPicker).year());
  }
  selectTariff() {
    const application = this.applications.find(x => x.id == this.billingGroup.get('application_id').value);
    if (application) {
      this.billingGroup.get('tariff_id').setValue(application.tariff_id);
    }
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  submit() {
    this.checkValidityOfControls([
      this.billingGroup.get('application_id'),
      this.billingGroup.get('tariff_id'),
      this.billingGroup.get('month'),
      this.billingGroup.get('year'),
      this.billingGroup.get('billing_start_date'),
      this.billingGroup.get('billing_end_date'),
    ]);
    if (!this.billingGroup.valid) {
      return Promise.resolve(false);
    }
    this.nzRef.getInstance().nzOkLoading = true;
    return new Promise(resolve => {
      this.billingsService.create(this.billingGroup.value).pipe(
        first(),
        finalize(() => this.nzRef.getInstance().nzOkLoading = false)
      ).subscribe(result => {
        this.uiService.showMessage(MessageType.success, "Movimento criado com sucesso");
        resolve(true);
      }, err => {
        this.uiService.showMessage(MessageType.error, "Erro ao criar movimento");
        resolve(false);
      });
    });

  }

  close() {
    this.nzRef.close();
    return Promise.resolve(true);
  }

}
