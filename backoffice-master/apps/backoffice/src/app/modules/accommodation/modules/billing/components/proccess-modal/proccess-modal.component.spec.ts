import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProccessModalComponent } from './proccess-modal.component';

describe('ProccessModalComponent', () => {
  let component: ProccessModalComponent;
  let fixture: ComponentFixture<ProccessModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProccessModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProccessModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
