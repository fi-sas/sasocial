import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ResidenceModel } from '../../../residences/models/residence.model';
import * as  moment from 'moment';
import { BillingService } from '../../services/billing.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
@Component({
  selector: 'fi-sas-proccess-modal',
  templateUrl: './proccess-modal.component.html',
  styleUrls: ['./proccess-modal.component.less']
})
export class ProccessModalComponent implements OnInit {
  optionsGroup = new FormGroup({
    date: new FormControl(new Date(), Validators.required),
    residence_id: new FormControl(null, Validators.required),
    operation: new FormControl("REVIEW", Validators.required),
  });
  residences: ResidenceModel[] = [];
  residence_id: number = null;
  constructor(
    private nzRef: NzModalRef,
    private billingService: BillingService,
    private uiService: UiService
  ) { }

  ngOnInit() {
    if (this.residence_id) {
      this.optionsGroup.patchValue({ residence_id: this.residence_id });
    }
  }

  validateForm() {
    if (this.optionsGroup.valid) { return; }
    this.submit();
  }
  submit() {
    return new Promise(resolve => {
      this.billingService.status({
        action: this.optionsGroup.value.operation,
        residence_id: this.optionsGroup.value.residence_id,
        month: moment(this.optionsGroup.value.date).month() + 1,
        year: moment(this.optionsGroup.value.date).year(),
      }).pipe(
        first(),
        finalize(() => this.nzRef.getInstance().nzOkLoading = false)
      ).subscribe(result => {
        this.uiService.showMessage(MessageType.success, "Operações submetidas com sucesso");
        resolve(true);
      }, err => {
        this.uiService.showMessage(MessageType.error, "Erro ao processar alterações movimento");
        resolve(false);
      });
    });
  }

  close() {
    this.nzRef.close();
    return Promise.resolve(true);
  }
}
