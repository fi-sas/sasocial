import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fi-sas-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.less']
})
export class ReportModalComponent implements OnInit {

  date: Date = new Date();
  
  constructor() { }

  ngOnInit() {
  }

}
