import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LeftPadPipe } from 'angular-pipes';
import { NzModalService } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { BillingItemModel } from '../../models/billing-item.model';
import { BillingModel } from '../../models/billing.model';
import { BillingItemService } from '../../services/billing-item.service';
import { BillintgItemFormModalComponent } from '../billintg-item-form-modal/billintg-item-form-modal.component';

@Component({
  selector: 'fi-sas-view-billing',
  templateUrl: './view-billing.component.html',
  styleUrls: ['./view-billing.component.less']
})
export class ViewBillingComponent implements OnInit {

  @Input() billing_items: BillingItemModel[];
  @Input() billing_status: string;
  @Input() billing_id: number;
  @Output() refresh = new EventEmitter();
  typologies_items: BillingItemModel[];
  regimes_items: BillingItemModel[];
  extras_items: BillingItemModel[];

  constructor(
    private billingItemService: BillingItemService,
    private uiService: UiService,
    private ref: ChangeDetectorRef,
    private nzModalSerice: NzModalService,
  ) { }

  ngOnInit() {
    this.typologies_items = this.billing_items.filter(x => x.typology_id != null);
    this.regimes_items = this.billing_items.filter(x => x.regime_id != null);
    this.extras_items = this.billing_items.filter(x => x.extra_id != null);
  }

  getTotalPrice() {
    let sum_price = 0;
    for (const item of this.billing_items) {
      sum_price += item.price;
    }
    return sum_price;
  }

  removeBillingItem(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar movimento',
        'Pretende eliminar este movimento?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.billingItemService.delete(id)
            .pipe(
              first(),
            ).subscribe((results) => {
              this.billing_items.splice(this.billing_items.findIndex(x => x.id == id), 1);
              this.typologies_items = this.billing_items.filter(x => x.typology_id != null);
              this.regimes_items = this.billing_items.filter(x => x.regime_id != null);
              this.extras_items = this.billing_items.filter(x => x.extra_id != null);
              this.uiService.showMessage(MessageType.success, "Movimento editado com sucesso");
            });
        }
      });


  }
  editBillingItem(billing_id: number, item_id: number) {
    const modal = this.nzModalSerice.create({
      nzContent: BillintgItemFormModalComponent,
      nzTitle: "Adicionar item",
      nzComponentParams: {
        billing_item_id: item_id,
        billing_id: billing_id
      },
      nzOnOk: data => {
        return data.submit();
      },
      nzOnCancel: data => {
        return data.close();
      }
    });
    modal.afterClose.subscribe(data => {
      if (data) {
        this.billingItemService.list(0, -1, null, null, { billing_id: billing_id }).subscribe(
          response => {
            this.billing_items = response.data;
            this.typologies_items = this.billing_items.filter(x => x.typology_id != null);
            this.regimes_items = this.billing_items.filter(x => x.regime_id != null);
            this.extras_items = this.billing_items.filter(x => x.extra_id != null);
            this.refresh.emit(true);
            this.ref.detectChanges();
          }
        );
      }
    });
  }
  createBillingItem(billing_id: number) {
    const modal = this.nzModalSerice.create({
      nzContent: BillintgItemFormModalComponent,
      nzTitle: "Adicionar item",
      nzComponentParams: {
        billing_id: billing_id,
      },
      nzOnOk: data => {
        return data.submit();
        
      },
      nzOnCancel: data => {
        return data.close();
      }
    });
    modal.afterClose.subscribe(data => {
      if (data) {
        this.billingItemService.list(0, -1, null, null, { billing_id: billing_id }).subscribe(
          response => {
            this.billing_items = response.data;
            this.typologies_items = this.billing_items.filter(x => x.typology_id != null);
            this.regimes_items = this.billing_items.filter(x => x.regime_id != null);
            this.extras_items = this.billing_items.filter(x => x.extra_id != null);
            this.refresh.emit(true);
            this.ref.detectChanges();
          }
        );
      }
    });
  }

}
