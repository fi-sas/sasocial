import { TypologyModel } from './../../typologies/models/typology.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { TypologyPriceLinePeriod } from '../../typologies/models/typology.model';
import { RoomModel } from '../../rooms/models/room.model';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { OrderModel } from '@fi-sas/backoffice/modules/financial/models/order.model';
import { ExtraModel } from '../../extras/models/extra.model';
import { RegimeModel } from '../../regimes/models/regime.model';

export class BillingItemModel {
    id?: number;
    billing_id: number;
    product_code: string;
    name: string;
    description: string;
    typology_id: number;
    typology?: TypologyModel;
    regime_id: number;
    regime?: RegimeModel;
    extra_id: number;
    extra?: ExtraModel;
    vat_id: number;
    vat?: TaxModel;
    vat_value: number;
    quantity: number;
    unit_price: number;
    price: number;
    period: TypologyPriceLinePeriod;
    possible_retroactive?: number;
    discount?: number;
    discount_value?: number;
    processed_manually: boolean;
    created_at?: Date;
    updated_at?: Date;
}
