import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzeItemsComponent } from './analyze-items.component';

describe('AnalyzeItemsComponent', () => {
  let component: AnalyzeItemsComponent;
  let fixture: ComponentFixture<AnalyzeItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyzeItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzeItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
