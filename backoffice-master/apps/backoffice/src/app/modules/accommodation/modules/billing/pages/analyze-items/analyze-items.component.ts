import { Component, OnInit } from '@angular/core';
import { ApplicationStatusMachine, BillingStatusResult } from '../../../aco_applications/models/application.model';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ResidencesService } from '../../../residences/services/residences.service';
import { first, finalize } from 'rxjs/operators';
import { BillingService } from '../../services/billing.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { TagResult } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { ApplicationsService } from '../../../aco_applications/services/applications.service';

@Component({
  selector: 'fi-sas-analyze-items',
  templateUrl: './analyze-items.component.html',
  styleUrls: ['./analyze-items.component.less']
})
export class AnalyzeItemsComponent extends TableHelper implements OnInit {
  BillingStatusResult = BillingStatusResult;

  validateLoading = false;

  showResidence = true;
  statusMachine = ApplicationStatusMachine;
  residencesSelect = {};

  residences: ResidenceModel[] = [];
  residencesLoading = false;

  persistentFilters = {
    searchFields: 'full_name,email,student_number,city,tin,identification,submission_date',
    withRelated: 'regime,assignedResidence,room,billingItems'
  }

  constructor(
    accommodationService: AccommodationService,
    public service: ApplicationsService,
    public billingService: BillingService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private residencesService: ResidencesService,
  ) {
    super(uiService, router, activatedRoute);
    // this.loadResidences();
    // const accommodationData = accommodationService.getAccommodationData();

    // if (accommodationData.academicYear) {
    //   this.persistentFilters['academic_year'] = accommodationData.academicYear;
    // }

    // if (accommodationData.applicationPhase) {
    //   this.persistentFilters['application_phase_id'] = accommodationData.applicationPhase.id;
    // }

    // this.persistentFilters['status'] = 'contracted';
    // if (accommodationData.residence) {
    //   this.persistentFilters['residence_id'] = accommodationData.residence.id;
    //   this.persistentFilters['assigned_residence_id'] = accommodationData.residence.id;
    // }
  }

  ngOnInit() {
    // this.initTableData(this.service)

    // if (this.persistentFilters['assigned_residence_id']) {
    //   const index = this.columns.findIndex(x => x.key === 'assignedResidence.name');
    //   if (index > -1) {
    //     this.showResidence = false;
    //   }
    // }
  }

  // loadResidences() {
  //   this.residencesLoading = true;
  //   this.residencesService.list(1, -1, null, null, { withRelated: false })
  //     .pipe(first(),
  //       finalize(() => this.residencesLoading = false))
  //     .subscribe(results => {
  //       this.residences = results.data;
  //     });
  // }

  // tableXSize(): string {
  //   return (this.columns.length * 125).toString().concat('px');
  // }

  // validate(application_id?: number) {
  //   this.validateLoading = true;
  //   this.billingService.validate({
  //     year: '2020',
  //     month: '10',
  //     application_ids: application_id ? [application_id] : this.getSelectedData().map(app => app.id)
  //   }).pipe(first(), finalize(() => this.validateLoading = false)).subscribe(results => {
  //     this.searchData();
  //     this.clearSelected();
  //   });
  // }


}
