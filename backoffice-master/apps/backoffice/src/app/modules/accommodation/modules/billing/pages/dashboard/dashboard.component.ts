import { first, finalize } from 'rxjs/operators';
import { BillingService } from './../../services/billing.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { ResidencesService } from '../../../residences/services/residences.service';

@Component({
  selector: 'fi-sas-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {
  dataRange = [
    moment().subtract(10, 'd').toISOString(),
    moment().toISOString(),
  ];

  operation = null;
  period = null;
  residencesSelect = null;

  residences: ResidenceModel[] = [];
  residencesLoading = false;

  loading_metrics = false;
  metrics_financial_dataSet = [
    {
      name: 'ADR',
      series: [],
    },
    {
      name: 'REVPAB',
      series: [],
    },
    {
      name: 'TREVPAB',
      series: [],
    },
    {
      name: 'Receita diária',
      series: [],
    },
  ];

  metrics_occupation_rate_dataSet = [
    {
      name: 'Ocupação',
      series: [],
    },
  ];

  metrics_occupation_dataSet = [
    {
      name: 'Cama ocupadas',
      series: [],
    },
    {
      name: 'Camas disponiveis',
      series: [],
    },
  ];

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'],
  };

  formatDate = (data) => {
    if(!this.period || this.period === 'day') {
      return moment(data).format('l');
    } else if (this.period === 'week') {
      return moment(data).format('YYYY/WW');
    } else if (this.period === 'month') {
      return moment(data).format('YYYY-MM');
    }
  };
  formatPercentage = (data) => {
    return data.toString().concat(' %');
  };
  formatMoney = (data) => {
    return data.toString().concat(' €');
  };

  constructor(
    private billingService: BillingService,
    private residencesService: ResidencesService
  ) {}

  ngOnInit() {
    // this.loadResidences();
    // this.loadMetrics();
  }

  // loadResidences() {
  //   this.residencesLoading = true;
  //   this.residencesService
  //     .list(1, -1, null, null, { withRelated: false })
  //     .pipe(
  //       first(),
  //       finalize(() => (this.residencesLoading = false))
  //     )
  //     .subscribe((results) => {
  //       this.residences = results.data;
  //     });
  // }

  // loadMetrics() {
  //   let query = {
  //     start_date: moment(this.dataRange[0]).format('YYYY-MM-DD'),
  //     end_date: moment(this.dataRange[1]).format('YYYY-MM-DD'),

  //     operation: 'avg',
  //   };

  //   if (this.residencesSelect) {
  //     query['residence_id'] = this.residencesSelect;
  //   }

  //   if (this.operation) {
  //     query['operation'] = this.operation;
  //   }

  //   if (this.period) {
  //     query['period'] = this.period;
  //   }

  //   this.loading_metrics = true;
  //   this.billingService
  //     .metrics(query)
  //     .pipe(
  //       first(),
  //       finalize(() => (this.loading_metrics = false))
  //     )
  //     .subscribe((results) => {
  //       this.metrics_financial_dataSet[0].series = [];
  //       this.metrics_financial_dataSet[1].series = [];
  //       this.metrics_financial_dataSet[2].series = [];
  //       this.metrics_financial_dataSet[3].series = [];

  //       this.metrics_occupation_dataSet[0].series = [];
  //       this.metrics_occupation_dataSet[1].series = [];

  //       this.metrics_occupation_rate_dataSet[0].series = [];

  //       results.data.map((metric) => {
  //         let date = new Date();
  //         if (metric.date) {
  //           date = metric.date;
  //         } else if (metric.year && metric.week) {
  //           date = moment(metric.year + '/' + metric.week, "YYYY-WW").toDate();
  //         } else if (metric.year && metric.month) {
  //           date = moment(metric.year + '/' + metric.month, "YYYY-MM").toDate();
  //         }

  //           metric.date ||
  //           this.metrics_financial_dataSet[0].series.push({
  //             name: date,
  //             value: metric.adr,
  //           });
  //         this.metrics_financial_dataSet[1].series.push({
  //           name: date,
  //           value: metric.revpab,
  //         });
  //         this.metrics_financial_dataSet[2].series.push({
  //           name: date,
  //           value: metric.trevpab,
  //         });
  //         this.metrics_financial_dataSet[3].series.push({
  //           name: date,
  //           value: metric.diary_receipt,
  //         });
  //         this.metrics_financial_dataSet = [...this.metrics_financial_dataSet];

  //         this.metrics_occupation_dataSet[0].series.push({
  //           name: date,
  //           value: metric.beds_occupation,
  //         });
  //         this.metrics_occupation_dataSet[1].series.push({
  //           name: date,
  //           value: metric.maximum_capacity,
  //         });
  //         this.metrics_occupation_dataSet = [
  //           ...this.metrics_occupation_dataSet,
  //         ];

  //         this.metrics_occupation_rate_dataSet[0].series.push({
  //           name: date,
  //           value: metric.occupation_rate,
  //         });
  //         this.metrics_occupation_rate_dataSet = [
  //           ...this.metrics_occupation_rate_dataSet,
  //         ];
  //       });
  //     });
  // }

  // onSelect(data): void {

  // }

  // onActivate(data): void {

  // }

  // onDeactivate(data): void {

  // }
}
