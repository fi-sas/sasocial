import { TestBed } from '@angular/core/testing';

import { BillingItemService } from './billing-item.service';

describe('BillingItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BillingItemService = TestBed.get(BillingItemService);
    expect(service).toBeTruthy();
  });
});
