import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { BillingItemModel } from '../models/billing-item.model';

@Injectable({
  providedIn: 'root'
})
export class BillingItemService extends Repository<BillingItemModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.BILLING_ITEMS';
    this.entity_url = 'ACCOMMODATION.BILLING_ITEMS_ID';
  }

}
