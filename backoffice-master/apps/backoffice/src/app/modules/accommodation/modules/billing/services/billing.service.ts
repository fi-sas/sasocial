import { first } from 'rxjs/operators';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { BillingModel } from '../models/billing.model';
import { FiResourceService, FiUrlService, HttpOptions } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class BillingService extends Repository<BillingModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.BILLINGS';
    this.entity_url = 'ACCOMMODATION.BILLINGS_ID';
  }
  status(body) {
    return this.resourceService.create(this.urlService.get('ACCOMMODATION.BILLINGS_STATUS'), body).pipe(first());
  }

  reprocess(id: number) {
    return this.resourceService.create(this.urlService.get('ACCOMMODATION.BILLINGS_REPROCESS', { id }), {}).pipe(first());
  }

}
