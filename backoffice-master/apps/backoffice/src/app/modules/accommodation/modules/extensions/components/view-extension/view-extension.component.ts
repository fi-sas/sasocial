import { ExtensionModel, ExtensionStatusTranslations } from '../../models/extension.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-extension',
  templateUrl: './view-extension.component.html',
  styleUrls: ['./view-extension.component.less']
})
export class ViewExtensionComponent implements OnInit {

  @Input() data: ExtensionModel = null;
  ExtensionStatusTranslations = ExtensionStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
