import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExtensionsComponent } from './pages/list-extensions/list-extensions.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListExtensionsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:extensions:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtensionsRoutingModule { }
