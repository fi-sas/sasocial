import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtensionsRoutingModule } from './extensions-routing.module';
import { ListExtensionsComponent } from './pages/list-extensions/list-extensions.component';
import { ViewExtensionComponent } from './components/view-extension/view-extension.component';

@NgModule({
  declarations: [
    ListExtensionsComponent,
    ViewExtensionComponent
  ],
  imports: [
    CommonModule,
    ExtensionsRoutingModule,
    SharedModule
  ]
})
export class ExtensionsModule { }
