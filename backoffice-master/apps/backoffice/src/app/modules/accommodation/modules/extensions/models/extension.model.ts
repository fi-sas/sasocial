import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';


export const ExtensionDecision = {
  APPROVE: { label: 'Aprovar', color: 'green' },
  REJECT: { label: 'Rejeitar', color: 'red' },
};
export enum ExtensionStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}
export const ExtensionStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' }
};

export class ExtensionHistoryModel {
  id: number;
  withdrawal_id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class ExtensionModel {
  id: number;
  decision: string;
  reason: string;
  application_id: number;
  start_date: Date;
  end_date: Date;
  file_id: number;
  file: FileModel;
  user_id: number;
  application: ApplicationModel;
  status: ExtensionStatus;
  history: ExtensionHistoryModel[];
  created_at: Date;
  update_at: Date;
}
