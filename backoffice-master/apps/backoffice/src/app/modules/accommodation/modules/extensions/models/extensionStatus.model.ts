import { ExtensionModel } from "./extension.model";


export class ExtensionStatusModel {
    notes: string;
    extension: ExtensionModel | any;
}
