import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExtensionsComponent } from './list-extensions.component';

describe('ListExtensionsComponent', () => {
  let component: ListExtensionsComponent;
  let fixture: ComponentFixture<ListExtensionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExtensionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExtensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
