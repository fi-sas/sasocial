import {
  ExtensionModel,
  ExtensionStatus,
  ExtensionDecision,
  ExtensionStatusTranslations,
} from './../../models/extension.model';
import { TableHelper } from './../../../../../../shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { ExtensionsService } from '../../services/extensions.service';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-list-extensions',
  templateUrl: './list-extensions.component.html',
  styleUrls: ['./list-extensions.component.less'],
})
export class ListExtensionsComponent extends TableHelper implements OnInit {
  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalExtensionSelected: ExtensionModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";
  ExtensionStatus = ExtensionStatus;
  ExtensionDecision = ExtensionDecision;
  residence: ResidenceModel = null;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public accommodationService: AccommodationService,
    public extensionsService: ExtensionsService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'end_date',
        label: 'Data saída',
        template: (data) => {
          return moment(new Date(data.end_date)).format('DD/MM/YYYY');
        },
        sortable: true,
      },
      {
        key: 'application.room.name',
        label: 'Quarto',
        sortable: false,
      },
      {
        key: 'application.assignedResidence.name',
        label: 'Residência',
        sortable: false,
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: ExtensionDecision
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ExtensionStatusTranslations
      },
    );

    this.persistentFilters['withRelated'] =
      'application,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.extensionsService);
  }

  openModalExtension(extension: ExtensionModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalExtensionSelected = extension;
    this.changeStatusModalAction = action;
  }

  handleChangeStatus() {
    if (this.changeStatusModalAction === 'analyse') {
      this.changeStatusModalLoading = true;
      this.extensionsService.status(this.changeStatusModalExtensionSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extensionsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.extensionsService.status(this.changeStatusModalExtensionSelected.id, "DISPATCH", {
        decision: this.changeStatusModalDecision
      }, this.changeStatusModalNotes
      ).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extensionsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.extensionsService.admin_aprove(this.changeStatusModalExtensionSelected.id, {
        notes: this.changeStatusModalNotes,
        extension: {}
      }).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extensionsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.extensionsService.admin_reject(this.changeStatusModalExtensionSelected.id, {
        notes: this.changeStatusModalNotes,
        extension: {}
      }).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extensionsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }
  }

  handleChangeStatusCancel() {
    this.changeStatusModalExtensionSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }


  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";

  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar pedido de extensão";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar pedido de extensão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar pedido de extensão";
    }
    return "";

  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }
}
