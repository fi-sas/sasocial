import { Injectable } from '@angular/core';
import { ExtensionModel } from '../models/extension.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ExtensionStatusModel } from '../models/extensionStatus.model';

@Injectable({
  providedIn: 'root',
})
export class ExtensionsService extends Repository<ExtensionModel> {
  entities_url = 'ACCOMMODATION.EXTENSIONS';
  entity_url = 'ACCOMMODATION.EXTENSIONS_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
  }

  status(id: number, event: string, extension, notes) {
    return this.resourceService.create<ExtensionModel>(
      this.urlService.get('ACCOMMODATION.EXTENSIONS_ID_STATUS', { id }), { event, extension, notes });
  }

  admin_aprove(
    id: number,
    withdrawalNotes: ExtensionStatusModel
  ): Observable<Resource<ExtensionModel>> {
    return this.resourceService.create<ExtensionModel>(
      this.urlService.get('ACCOMMODATION.EXTENSIONS_ID_APPROVE', { id }),
      withdrawalNotes
    );
  }

  admin_reject(
    id: number,
    withdrawalNotes: ExtensionStatusModel
  ): Observable<Resource<ExtensionModel>> {
    return this.resourceService.create<ExtensionModel>(
      this.urlService.get('ACCOMMODATION.EXTENSIONS_ID_REJECT', { id }),
      withdrawalNotes
    );
  }


}
