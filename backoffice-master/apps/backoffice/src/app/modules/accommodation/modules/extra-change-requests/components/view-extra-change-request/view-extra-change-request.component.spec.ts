import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExtraChangeRequestComponent } from './view-extra-change-request.component';

describe('ViewExtraChangeRequestComponent', () => {
  let component: ViewExtraChangeRequestComponent;
  let fixture: ComponentFixture<ViewExtraChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewExtraChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExtraChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
