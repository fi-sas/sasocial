import { Component, Input, OnInit } from '@angular/core';
import { ExtraChangeRequestModel, ExtraChangeRequestStatusTranslations } from '../../models/Extra-change-request.model';

@Component({
  selector: 'fi-sas-view-extra-change-request',
  templateUrl: './view-extra-change-request.component.html',
  styleUrls: ['./view-extra-change-request.component.less']
})
export class ViewExtraChangeRequestComponent implements OnInit {

  @Input() data: ExtraChangeRequestModel = null;
  ExtraChangeRequestStatusTranslations = ExtraChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }
  getNewExtras(): string[] {
    const list = [];
    for (const e of this.data.extras) {
      const pt = e.extra.translations.find(x => x.language_id == 3);
      if (pt) {
        list.push(pt.name);
      }
    }
    return list;
  }
  getApplicationExtras(): string[] {
    const list = [];
    for (const e of this.data.application.extras) {
      const pt = e.translations.find(x => x.language_id == 3);
      if (pt) {
        list.push(pt.name);
      }
    }
    return list;
  }
}
