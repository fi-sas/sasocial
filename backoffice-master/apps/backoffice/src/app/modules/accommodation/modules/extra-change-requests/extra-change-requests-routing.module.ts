import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExtraChangeRequestsComponent } from './pages/list-extra-change-requests/list-extra-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListExtraChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtraChangeRequestsRoutingModule { }
