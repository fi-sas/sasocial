import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtraChangeRequestsRoutingModule } from './extra-change-requests-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListExtraChangeRequestsComponent } from './pages/list-extra-change-requests/list-extra-change-requests.component';
import { ViewExtraChangeRequestComponent } from './components/view-extra-change-request/view-extra-change-request.component';


@NgModule({
  declarations: [
    ListExtraChangeRequestsComponent,
    ViewExtraChangeRequestComponent],
  imports: [
    CommonModule,
    ExtraChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class ExtraChangeRequestsModule { }
