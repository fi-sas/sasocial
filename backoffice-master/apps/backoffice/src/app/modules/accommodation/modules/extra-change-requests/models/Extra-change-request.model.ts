import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { ExtraModel } from '../../extras/models/extra.model';


// export const ExtensionDecision = {
//   APPROVE: { label: 'Aprovar', color: 'green' },
//   REJECT: { label: 'Rejeitar', color: 'red' },
// };
export enum ExtraChangeRequestStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}
export enum ExtraChangeRequestDecision {
  APPROVE = 'APPROVE',
  REJECT = 'REJECT',
}
export const ExtraChangeRequestDecisionTranslations = {
  APPROVE: { label: 'Aprovada', color: 'green' },
  REJECT: { label: 'Rejeitada', color: 'red' },
};
export const ExtraChangeRequestStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export class ExtraChangeRequestHistoryModel {
  id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class ExtrasDTO {
  application_extra_change_id: number;
  extra: ExtraModel
  extra_id: number;
}

export class ExtraChangeRequestModel {
  id: number;
  reason: string;
  application_id: number;
  application: ApplicationModel;
  start_date: Date;
  observations: string;
  decision: ExtraChangeRequestDecision;
  status: ExtraChangeRequestStatus;
  created_at: Date;
  update_at: Date;
  history: ExtraChangeRequestHistoryModel[];
  extras: ExtrasDTO[];
  file_id: number;
  file: FileModel;
}
