import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExtraChangeRequestsComponent } from './list-extra-change-requests.component';

describe('ListExtraChangeRequestsComponent', () => {
  let component: ListExtraChangeRequestsComponent;
  let fixture: ComponentFixture<ListExtraChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExtraChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExtraChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
