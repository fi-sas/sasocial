import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AccommodationData, AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { ExtraChangeRequestDecisionTranslations, ExtraChangeRequestModel, ExtraChangeRequestStatusTranslations } from '../../models/Extra-change-request.model';
import { ExtraChangeRequestsService } from '../../services/extra-change-requests.service';

@Component({
  selector: 'fi-sas-list-extra-change-requests',
  templateUrl: './list-extra-change-requests.component.html',
  styleUrls: ['./list-extra-change-requests.component.less']
})
export class ListExtraChangeRequestsComponent extends TableHelper implements OnInit {


  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalAbsenceSelected: ExtraChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";


  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private extraChangeRequestsService: ExtraChangeRequestsService,
    private accommodationService: AccommodationService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: '',
        label: 'Novos extras',
        template: (data) => {
          const extras = [];
          for (const e of data.extras) {
            const pt = e.extra.translations.find(x => x.language_id == 3);
            if (pt)
              extras.push(pt.name);
          }
          return extras.length > 0 ? extras.join(",") : '-';
        },
        sortable: true
      },

      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: ExtraChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ExtraChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'extras,application,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.extraChangeRequestsService);
  }


  openModalWithdrawal(absence: ExtraChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalAbsenceSelected = absence;
    this.changeStatusModalAction = action;
  }
  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {

    if (this.changeStatusModalAction === 'analyse') {

      this.changeStatusModalLoading = true;
      this.extraChangeRequestsService.status(this.changeStatusModalAbsenceSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extraChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.extraChangeRequestsService.status(this.changeStatusModalAbsenceSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extraChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.extraChangeRequestsService.admin_approve(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extraChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.extraChangeRequestsService.admin_reject(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.extraChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  handleChangeStatusCancel() {
    this.changeStatusModalAbsenceSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }
}


