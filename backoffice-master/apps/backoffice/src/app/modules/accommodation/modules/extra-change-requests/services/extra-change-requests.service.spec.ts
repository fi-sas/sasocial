import { TestBed } from '@angular/core/testing';

import { ExtraChangeRequestsService } from './extra-change-requests.service';

describe('ExtraChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExtraChangeRequestsService = TestBed.get(ExtraChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
