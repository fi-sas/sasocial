import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { ExtraChangeRequestModel } from '../models/Extra-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class ExtraChangeRequestsService extends Repository<ExtraChangeRequestModel> {
  entities_url = 'ACCOMMODATION.EXTRA_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.EXTRA_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, application_extra_change, notes) {
    return this.resourceService.create<ExtraChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST_STATUS', { id }), { event, application_extra_change, notes });
  }

  admin_approve(id: number, application_extra_change, notes) {
    return this.resourceService.create<ExtraChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST_APPROVE', { id }), { application_extra_change, notes });
  }

  admin_reject(id: number, application_extra_change, notes) {
    return this.resourceService.create<ExtraChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST_REJECT', { id }), { application_extra_change, notes });
  }

}
