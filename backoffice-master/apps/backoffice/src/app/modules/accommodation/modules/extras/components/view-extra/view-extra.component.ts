

import { Component, OnInit, Input } from '@angular/core';
import { ExtraModel } from '../../models/extra.model';

@Component({
  selector: 'fi-sas-view-extra',
  templateUrl: './view-extra.component.html',
  styleUrls: ['./view-extra.component.less']
})
export class ViewExtraComponent implements OnInit {

  @Input() data: ExtraModel = null;
  
  constructor(
   
  ) {     
    
  }

  ngOnInit() {}


}
