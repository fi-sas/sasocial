import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormExtraComponent } from './pages/form-extra/form-extra.component';
import { ListExtrasComponent } from './pages/list-extras/list-extras.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormExtraComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:extras:create' }
  },
  {
    path: 'edit/:id',
    component: FormExtraComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:extras:create' }
  },
  {
    path: 'list',
    component: ListExtrasComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:extras:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtrasRoutingModule { }
