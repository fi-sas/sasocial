import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtrasRoutingModule } from './extras-routing.module';
import { FormExtraComponent } from './pages/form-extra/form-extra.component';
import { ListExtrasComponent } from './pages/list-extras/list-extras.component';
import { ViewExtraComponent } from './components/view-extra/view-extra.component';

@NgModule({
  declarations: [
    FormExtraComponent,
    ListExtrasComponent,
    ViewExtraComponent
  ],
  imports: [
    CommonModule,
    ExtrasRoutingModule,
    SharedModule
  ]
})
export class ExtrasModule { }
