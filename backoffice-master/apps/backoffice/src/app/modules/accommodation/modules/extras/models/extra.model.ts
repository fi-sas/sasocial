import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
export class ExtraTranslationModel  {
    language_id: number;
    name: string;
}

export class ExtraDiscount{
    month: number;
    year: number;
    discount_value: number;
}

export class ExtraModel {
    id: number;
    translations: ExtraTranslationModel[];
    product_code: string;
    billing_name: string;
    vat_id: number;
    vat?: TaxModel;
    price: number;
    visible: boolean;
    active: boolean;
    is_deposit: boolean;
    extra_id: number;
    extra: ExtraModel;
    discounts: ExtraDiscount[];
    reprocess_since: Date;
}
