import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormExtraComponent } from './form-extra.component';

describe('FormExtraComponent', () => {
  let component: FormExtraComponent;
  let fixture: ComponentFixture<FormExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormExtraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
