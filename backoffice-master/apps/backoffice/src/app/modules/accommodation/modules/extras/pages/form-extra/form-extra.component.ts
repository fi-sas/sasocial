import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ExtrasService } from '../../services/extras.service';
import { FormField, InputType } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';
import { Validators, FormGroup, FormArray, FormControl } from '@angular/forms';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-form-extra',
  templateUrl: './form-extra.component.html',
  styleUrls: ['./form-extra.component.less']
})
export class FormExtraComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  submit = false;
  loadingLanguages = false;
  languages: LanguageModel[] = [];

  loadingTaxes = false;
  taxes: TaxModel[] = [];

  idToUpdate = null;
  loading = false;

  extraGroup = new FormGroup({
    translations: new FormArray([]),
    billing_name: new FormControl('', [Validators.required, trimValidation]),
    product_code: new FormControl('', [Validators.required, trimValidation]),
    vat_id: new FormControl(null, Validators.required),
    price: new FormControl(0, [Validators.required]),
    visible: new FormControl(true, Validators.required),
    active: new FormControl(true, Validators.required),
    // is_deposit: new FormControl(false, Validators.required),
    reprocess_since: new FormControl(null),
    discounts: new FormArray([]),
  });
  translations = this.extraGroup.get('translations') as FormArray;
  discounts = this.extraGroup.get('discounts') as FormArray;
  get f() { return this.extraGroup.controls; }
  constructor(
    private languagesService: LanguagesService,
    private uiService: UiService,
    public activateRoute: ActivatedRoute,
    public router: Router,
    public extrasService: ExtrasService,
    private taxesService: TaxesService,
  ) { }

  ngOnInit() {
    this.loadLanguages();
    this.loadTaxes();
  }

  loadTaxes() {
    this.loadingTaxes = true;
    this.taxesService.list(1, -1).pipe(
      first(),
      finalize(() => this.loadingTaxes = false)
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  loadExtra() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.extrasService.read(parseInt(data.get('id'), 10)).subscribe(result => {
          this.extraGroup.patchValue({
            billing_name: result.data[0].billing_name,
            product_code: result.data[0].product_code,
            vat_id: result.data[0].vat_id,
            price: result.data[0].price,
            visible: result.data[0].visible,
            active: result.data[0].active,
            reprocess_since: result.data[0].reprocess_since,
            // is_deposit: result.data[0].is_deposit,
          })

          if (result.data[0].translations) {
            result.data[0].translations.map(t => {
              this.addTranslation(t.language_id, t.name);
            });
          }

          if (result.data[0].discounts) {
            result.data[0].discounts.map(t => {
              this.addDiscountLine(new Date(t.year, t.month, null), t.discount_value);
            });
          }

          this.languages.map(l => {
            const foundLnaguage = this.translations.value.find(t => t.language_id === l.id);
            if (!foundLnaguage) {
              this.addTranslation(l.id, '');
            }
          });

        });
      } else {
        this.languages.map(l => {
          this.addTranslation(l.id, '');
        });
      }
    });
  }

  disableDate = (date: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(date).isBefore(moment(currentDate).format("YYYY-MM-DD"));
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.extraGroup.controls.translations as FormArray;
    translations.push(new FormGroup({
      language_id: new FormControl(language_id, Validators.required),
      name: new FormControl(name, [Validators.required, trimValidation]),
    }));
  }

  addDiscountLine(
    monthAndYear?: Date,
    discount_value?: number,
  ) {
    const discounts = this.extraGroup.controls.discounts as FormArray;
    discounts.push(
      new FormGroup({
        date: new FormControl(monthAndYear ? monthAndYear : null , Validators.required),
        discount_value: new FormControl(discount_value ? discount_value : 0, Validators.required),
      })
    );
  }

  deleteDiscountLine(discountLineIndex: any) {
    if (this.discounts.at(discountLineIndex)) {
      this.discounts.removeAt(discountLineIndex);
    }
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService.list(1, -1).pipe(first(), finalize(() => this.loadingLanguages = false)).subscribe(results => {
      this.languages = results.data;

      this.loadExtra();
    });
  }

  submitForm(value: any) {
    this.submit = true;

    let translationsError = false;

    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
        if (!tt.valid) {
          translationsError = true;
        }
      }
    }

    for (const ds in this.discounts.controls) {
      if (ds) {
        const disc = this.discounts.get(ds) as FormGroup;
        for (const i in disc.controls) {
          if (disc) {
            disc.controls[i].markAsDirty();
            disc.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    if (this.extraGroup.valid) {
      this.loading = true;
      this.submit = false;
      if (this.idToUpdate) {
        this.extrasService.update(this.idToUpdate, value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['/accommodation', 'extras', 'list']);
            this.uiService.showMessage(MessageType.success, 'Extra alterado com sucesso');
          });
      } else {
        this.extrasService.create(value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['/accommodation', 'extras', 'list']);
            this.uiService.showMessage(MessageType.success, 'Extra criado com sucesso');
          });
      }
    }

  }
  backList() {
    this.router.navigate(['/accommodation', 'extras', 'list']);
  }

}
