import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExtrasComponent } from './list-extras.component';

describe('ListExtrasComponent', () => {
  let component: ListExtrasComponent;
  let fixture: ComponentFixture<ListExtrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExtrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExtrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
