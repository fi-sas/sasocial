import { first } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { ExtrasService } from '../../services/extras.service';

@Component({
  selector: 'fi-sas-list-extras',
  templateUrl: './list-extras.component.html',
  styleUrls: ['./list-extras.component.less']
})
export class ListExtrasComponent extends TableHelper implements OnInit {
  status = [];
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public extrasService: ExtrasService
  ) {
    super(uiService, router, activatedRoute);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.persistentFilters = {
      searchFields: 'name',
    };
  }

  ngOnInit() {
    this.initTableData(this.extrasService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  deleteExtra(id: number) {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este extra?', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.extrasService.delete(id).pipe(first()).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Extra eliminado com sucesso');
          this.searchData();
        });
      }
    });
  }

     
  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
