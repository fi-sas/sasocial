import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ExtraModel } from '../models/extra.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class ExtrasService extends Repository<ExtraModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.EXTRAS';
    this.entity_url = 'ACCOMMODATION.EXTRAS_ID';
  }
}
