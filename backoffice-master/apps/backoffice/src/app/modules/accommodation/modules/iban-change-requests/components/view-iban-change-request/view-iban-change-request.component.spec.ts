import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewIbanChangeRequestComponent } from './view-iban-change-request.component';

describe('ViewIbanChangeRequestComponent', () => {
  let component: ViewIbanChangeRequestComponent;
  let fixture: ComponentFixture<ViewIbanChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewIbanChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewIbanChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
