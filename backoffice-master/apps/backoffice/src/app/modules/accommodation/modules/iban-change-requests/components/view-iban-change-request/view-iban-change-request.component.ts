import { Component, Input, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { IbanChangeRequestModel } from '../../models/iban-change-request.model';

@Component({
  selector: 'fi-sas-view-iban-change-request',
  templateUrl: './view-iban-change-request.component.html',
  styleUrls: ['./view-iban-change-request.component.less']
})
export class ViewIbanChangeRequestComponent implements OnInit {

  @Input() data: IbanChangeRequestModel = null;
  YesNoTag = TagComponent.YesNoTag;

  constructor() { }

  ngOnInit() {
  }

}
