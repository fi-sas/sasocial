import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListIbanChangeRequestsComponent } from './pages/list-iban-change-requests/list-iban-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListIbanChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IbanChangeRequestsRoutingModule { }
