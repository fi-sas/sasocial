import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IbanChangeRequestsRoutingModule } from './iban-change-requests-routing.module';
import { ListIbanChangeRequestsComponent } from './pages/list-iban-change-requests/list-iban-change-requests.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewIbanChangeRequestComponent } from './components/view-iban-change-request/view-iban-change-request.component';

@NgModule({
  declarations: [ListIbanChangeRequestsComponent, ViewIbanChangeRequestComponent],
  imports: [
    CommonModule,
    IbanChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class IbanChangeRequestsModule { }
