import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { TariffModel } from '../../tariffs/models/tariff.model';
import { ExtraModel } from '../../extras/models/extra.model';
import { RegimeModel } from '../../regimes/models/regime.model';


// export const ExtensionDecision = {
//   APPROVE: { label: 'Aprovar', color: 'green' },
//   REJECT: { label: 'Rejeitar', color: 'red' },
// };
export enum IbanChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    EXPIRED = 'EXPIRED'
}
export const IbanChangeRequestStatusTranslations = {
    SUBMITTED: { label: 'Submetido', color: 'blue' },
    EXPIRED: { label: 'Expirado', color: 'red' },
};


export class IbanChangeRequestModel {
    id: number;
    application_id: number;
    application: ApplicationModel;
    file_id: number;
    file: FileModel;
    iban: string;
    status: IbanChangeRequestStatus;
    allow_direct_debit: boolean;
    created_at: Date;
    update_at: Date;
}
