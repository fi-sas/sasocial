import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListIbanChangeRequestsComponent } from './list-iban-change-requests.component';

describe('ListIbanChangeRequestsComponent', () => {
  let component: ListIbanChangeRequestsComponent;
  let fixture: ComponentFixture<ListIbanChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListIbanChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIbanChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
