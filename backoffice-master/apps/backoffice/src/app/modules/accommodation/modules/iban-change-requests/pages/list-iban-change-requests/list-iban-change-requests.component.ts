import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { TariffChangeRequestsService } from '../../../tariff-change-requests/services/tariff-change-requests.service';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import * as moment from 'moment';
import { IbanChangeRequestStatusTranslations } from '../../models/iban-change-request.model';
import { IbanChangeRequestsService } from '../../services/iban-change-requests.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';

@Component({
  selector: 'fi-sas-list-iban-change-requests',
  templateUrl: './list-iban-change-requests.component.html',
  styleUrls: ['./list-iban-change-requests.component.less']
})
export class ListIbanChangeRequestsComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private ibanChangeRequestsService: IbanChangeRequestsService,
    private accommodationService: AccommodationService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'created_at',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.created_at)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'iban',
        label: 'IBAN',
        sortable: false
      },
      {
        key: 'allow_direct_debit',
        label: 'Autorização débito direto',
        tag: TagComponent.YesNoTag
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: IbanChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'application,tariff,old_tariff,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.ibanChangeRequestsService);
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
