import { TestBed } from '@angular/core/testing';

import { IbanChangeRequestsService } from './iban-change-requests.service';

describe('IbanChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IbanChangeRequestsService = TestBed.get(IbanChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
