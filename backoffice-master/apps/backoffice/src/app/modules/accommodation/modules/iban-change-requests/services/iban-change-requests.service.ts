import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { IbanChangeRequestModel } from '../models/iban-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class IbanChangeRequestsService extends Repository<IbanChangeRequestModel> {
  entities_url = 'ACCOMMODATION.IBAN_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.IBAN_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }
}