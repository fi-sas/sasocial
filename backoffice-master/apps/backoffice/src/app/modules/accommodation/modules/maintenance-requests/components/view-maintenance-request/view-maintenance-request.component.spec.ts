import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMaintenanceRequestComponent } from './view-maintenance-request.component';

describe('ViewMaintenanceRequestComponent', () => {
  let component: ViewMaintenanceRequestComponent;
  let fixture: ComponentFixture<ViewMaintenanceRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMaintenanceRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMaintenanceRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
