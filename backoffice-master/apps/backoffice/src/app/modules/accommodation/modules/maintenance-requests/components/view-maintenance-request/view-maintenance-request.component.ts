import { Component, Input, OnInit } from '@angular/core';
import { MaintenanceRequestModel, MaintenanceRequestStatusTranslations } from '../../models/Maintenande-request-model.model';

@Component({
  selector: 'fi-sas-view-maintenance-request',
  templateUrl: './view-maintenance-request.component.html',
  styleUrls: ['./view-maintenance-request.component.less']
})
export class ViewMaintenanceRequestComponent implements OnInit {

  MaintenanceStatusTranslations = MaintenanceRequestStatusTranslations;
  @Input() data: MaintenanceRequestModel = null;


  constructor() { }

  ngOnInit() {
  }

}
