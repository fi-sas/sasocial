import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMaintenanceRequestsComponent } from './pages/list-maintenance-requests/list-maintenance-requests.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListMaintenanceRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRequestsRoutingModule { }
