import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceRequestsRoutingModule } from './maintenance-requests-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListMaintenanceRequestsComponent } from './pages/list-maintenance-requests/list-maintenance-requests.component';
import { ViewMaintenanceRequestComponent } from './components/view-maintenance-request/view-maintenance-request.component';

@NgModule({
  declarations: [ListMaintenanceRequestsComponent, ViewMaintenanceRequestComponent],
  imports: [
    CommonModule,
    MaintenanceRequestsRoutingModule,
    SharedModule
  ]
})
export class MaintenanceRequestsModule { }
