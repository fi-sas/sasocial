import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { ExtraModel } from '../../extras/models/extra.model';


export enum ExtraChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    RESOLVING = 'RESOLVING',
    RESOLVED = 'RESOLVED',
    REJECTED = 'REJECTED',
    CANCELLED = 'CANCELLED',
}

export const MaintenanceRequestStatusTranslations = {
    SUBMITTED: { label: 'Submetido', color: 'blue' },
    RESOLVING: { label: 'Em resolução', color: 'yellow' },
    RESOLVED: { label: 'Resolvido', color: 'green' },
    REJECTED: { label: 'Rejeitado', color: 'red' },
    CANCELLED: { label: 'Cancelado', color: 'gray' },
};

export class MaintenanceRequestHistoryModel {
    id: number;
    status: string;
    user_id: number;
    notes: string;
    created_at: Date;
    update_at: Date;
}
export class MaintenanceRequestModel {
    id: number;
    description: string;
    local: string;
    application_id: number;
    application: ApplicationModel;
    status: ExtraChangeRequestStatus;
    file_id: number;
    file: File;
    created_at: Date;
    update_at: Date;
    history: MaintenanceRequestHistoryModel[];
}
