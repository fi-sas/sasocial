import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMaintenanceRequestsComponent } from './list-maintenance-requests.component';

describe('ListMaintenanceRequestsComponent', () => {
  let component: ListMaintenanceRequestsComponent;
  let fixture: ComponentFixture<ListMaintenanceRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMaintenanceRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMaintenanceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
