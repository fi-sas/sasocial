import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { MaintenanceRequestModel, MaintenanceRequestStatusTranslations } from '../../models/Maintenande-request-model.model';
import { MaintenanceRequestsService } from '../../services/maintenance-requests.service';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';

@Component({
  selector: 'fi-sas-list-maintenance-requests',
  templateUrl: './list-maintenance-requests.component.html',
  styleUrls: ['./list-maintenance-requests.component.less']
})
export class ListMaintenanceRequestsComponent extends TableHelper implements OnInit {

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalMaintenanceSelected: MaintenanceRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";





  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private maintenanceRequestsService: MaintenanceRequestsService,
    private accommodationService: AccommodationService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'created_at',
        label: 'Data',
        template: (data) => {
          return moment(new Date(data.created_at)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'local',
        label: 'Local',
        sortable: false
      },
      {
        key: 'description',
        label: 'Descrição',
        sortable: true
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: MaintenanceRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'application,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.maintenanceRequestsService);
  }

  openModalWithdrawal(maintenance: MaintenanceRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalMaintenanceSelected = maintenance;
    this.changeStatusModalAction = action;
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "resolving") {
      return "Em resolução";
    }
    if (this.changeStatusModalAction == "resolve") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }
  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Pedido em resolução";
    }
    if (this.changeStatusModalAction == "resolve") {
      return "Resolvido";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  handleChangeStatus() {

    this.changeStatusModalLoading = true;
    this.maintenanceRequestsService.status(this.changeStatusModalMaintenanceSelected.id, this.changeStatusModalAction, {}, this.changeStatusModalNotes).pipe(
      first(),
      finalize(() => this.changeStatusModalLoading = false)
    ).subscribe(result => {
      this.initTableData(this.maintenanceRequestsService);
      this.handleChangeStatusCancel();
      this.accommodationService.updateContractChangeStats();
    });
  }
  handleChangeStatusCancel() {
    this.changeStatusModalMaintenanceSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }
}
