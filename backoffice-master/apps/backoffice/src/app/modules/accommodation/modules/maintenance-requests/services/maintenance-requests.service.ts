import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { ExtraChangeRequestModel } from '../../extra-change-requests/models/Extra-change-request.model';
import { MaintenanceRequestModel } from '../models/Maintenande-request-model.model';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceRequestsService extends Repository<MaintenanceRequestModel> {
  entities_url = 'ACCOMMODATION.MAINTENANCE_REQUEST';
  entity_url = 'ACCOMMODATION.MAINTENANCE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, maintenance_request, notes: string) {
    return this.resourceService.create<MaintenanceRequestModel>(
      this.urlService.get('ACCOMMODATION.MAINTENANCE_REQUEST_STATUS', { id }), { event, maintenance_request, notes });
  }
}
