export class MovableHeritageModel {
    code: string;
    description: string;
    active: boolean;
    reference_value: number;
    id: number;
}