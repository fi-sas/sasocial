import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormMovableHeritageComponent } from './pages/form-movable-heritage/form-movable-heritage.component';
import { ListMovableHeritageComponent } from './pages/list-movable-heritage/list-movable-heritage.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormMovableHeritageComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:patrimony-categories:create' }
  },
  {
    path: 'edit/:id',
    component: FormMovableHeritageComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:patrimony-categories:create' }
  },
  {
    path: 'list',
    component: ListMovableHeritageComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:patrimony-categories:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovableHeritageRoutingModule { }
