import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovableHeritageRoutingModule } from './movable-heritage-routing.module';
import { ListMovableHeritageComponent } from './pages/list-movable-heritage/list-movable-heritage.component';
import { FormMovableHeritageComponent } from './pages/form-movable-heritage/form-movable-heritage.component';

@NgModule({
  declarations: [
    FormMovableHeritageComponent,
    ListMovableHeritageComponent,
  ],
  imports: [
    CommonModule,
    MovableHeritageRoutingModule,
    SharedModule
  ]
})
export class MovableHeritageModule { }
