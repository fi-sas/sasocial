import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormMovableHeritageComponent } from './form-movable-heritage.component';


describe('FormMovableHeritageComponent', () => {
  let component: FormMovableHeritageComponent;
  let fixture: ComponentFixture<FormMovableHeritageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMovableHeritageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMovableHeritageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
