import { MessageType, UiService } from './../../../../../../core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MovableHeritageService } from '../../services/movable-heritage.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-movable-heritage',
  templateUrl: './form-movable-heritage.component.html',
  styleUrls: ['./form-movable-heritage.component.less']
})
export class FormMovableHeritageComponent implements OnInit {
  loading = false;
  movableHeritageId: number;
  formMovableHeritage = new FormGroup({
    code: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required, trimValidation]),
    reference_value: new FormControl(null, [Validators.required, Validators.min(0)]),
    active: new FormControl(false, Validators.required),
  });
  submit = false;

  constructor(
    private uiService: UiService,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    private movableHeritageService: MovableHeritageService) {

    activatedRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.movableHeritageId = parseInt(data.get('id'), 10);
        this.loadMovableHeritage();
      }
    });
  }

  get f() { return this.formMovableHeritage.controls; }

  ngOnInit() {
  }


  loadMovableHeritage() {
    this.movableHeritageService.read(this.movableHeritageId).pipe(
      first(),
      finalize(() => this.loading = false))
      .subscribe(result => {
        this.formMovableHeritage.patchValue({
          description: result.data[0].description,
          code: result.data[0].code,
          reference_value: result.data[0].reference_value,
          active: result.data[0].active
        });
      });
  }

  submitForm() {
    const value = { ...this.formMovableHeritage.value };
    this.submit = true;

    if (this.formMovableHeritage.valid) {
      this.submit = false;
      this.loading = true;
      if (this.movableHeritageId) {
        this.movableHeritageService.update(this.movableHeritageId, value).pipe(
          first(),
          finalize(() => this.loading = false))
          .subscribe(result => {
            this.router.navigate(['/accommodation', 'movable-heritage', 'list']);
            this.uiService.showMessage(MessageType.success, 'Património Mobiliário alterado com sucesso');
          });
      } else {
        this.movableHeritageService.create(value).pipe(
          first(),
          finalize(() => this.loading = false))
          .subscribe(result => {
            this.router.navigate(['/accommodation', 'movable-heritage', 'list']);
            this.uiService.showMessage(MessageType.success, 'Património Mobiliário criado com sucesso');
          });
      }
    }
  }

  backList() {
    this.router.navigate(['/accommodation', 'movable-heritage', 'list']);
  }
}
