import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListMovableHeritageComponent } from './list-movable-heritage.component';

describe('ListMovableHeritageComponent', () => {
  let component: ListMovableHeritageComponent;
  let fixture: ComponentFixture<ListMovableHeritageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMovableHeritageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMovableHeritageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
