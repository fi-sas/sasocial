import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

import { first, finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovableHeritageModel } from '../../model/movable-heritage.model';
import { MovableHeritageService } from '../../services/movable-heritage.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';


@Component({
  selector: 'fi-sas-movable-heritage-list',
  templateUrl: './list-movable-heritage.component.html',
  styleUrls: ['./list-movable-heritage.component.less']
})
export class ListMovableHeritageComponent extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private movableHeritageService: MovableHeritageService,
  ) {
    super(uiService, router, activatedRoute);
    this.initTableData(this.movableHeritageService);

  }

  ngOnInit() {

  }

  newMovableHeritage() {
    this.router.navigate(['/accommodation', 'movable-heritage', 'create']);
  }

  deleteMovableHeritage(id: number) {
    this.uiService.showConfirm('Eliminar património mobiliário',
      'Pretende eliminar este património mobiliário?',
      'Eliminar',
      'Cancelar')
      .pipe(first()).subscribe(confirm => {
        if (confirm) {
          this.movableHeritageService.delete(id).pipe(first()).subscribe(result => {
            this.uiService.showMessage(MessageType.success, 'Património mobiliário eliminado com successo');
            this.initTableData(this.movableHeritageService);
          });
        }
      });
  }

}
