

import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { MovableHeritageModel } from '../model/movable-heritage.model';


@Injectable({
  providedIn: 'root'
})
export class MovableHeritageService extends Repository<MovableHeritageModel>{

  constructor(resource: FiResourceService, url: FiUrlService) {
    super(resource, url);
    this.entities_url = 'ACCOMMODATION.PATRIMONY_CATEGORIES';
    this.entity_url = 'ACCOMMODATION.PATRIMONY_CATEGORIES_ID';
  }
}
