import { Component, Input, OnInit } from '@angular/core';
import { OccurrenceModel } from '../../models/Occurrence.model';

@Component({
  selector: 'fi-sas-view-occurrence',
  templateUrl: './view-occurrence.component.html',
  styleUrls: ['./view-occurrence.component.less']
})
export class ViewOccurrenceComponent implements OnInit {

  @Input() data: OccurrenceModel = null;

  constructor() { }

  ngOnInit() {
  }

}
