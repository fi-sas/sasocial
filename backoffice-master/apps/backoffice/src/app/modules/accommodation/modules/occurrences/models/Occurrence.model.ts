import { FileModel } from "@fi-sas/backoffice/modules/medias/models/file.model";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { ApplicationModel } from "../../aco_applications/models/application.model";

export class OccurrenceModel {
    id: number;
    application_id: number;
    date: Date;
    subject: string;
    description: string;
    file_id: number;
    created_by_id: number;
    created_at: Date;
    update_at: Date;
    application: ApplicationModel;
    created_by: UserModel;
    file: FileModel;
}
