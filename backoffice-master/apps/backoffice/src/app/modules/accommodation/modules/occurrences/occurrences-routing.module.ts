import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResidenceGuard } from '../../guards/residence.guard';
import { FormOccurrencesComponent } from './pages/form-occurrences/form-occurrences.component';
import { ListOccurrencesComponent } from './pages/list-occurrences/list-occurrences.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListOccurrencesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' },
  },
  {
    path: 'create',
    component: FormOccurrencesComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:applications:update' }
  },
  {
    path: 'edit/:id',
    component: FormOccurrencesComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:applications:update' }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccurrencesRoutingModule { }
