import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OccurrencesRoutingModule } from './occurrences-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListOccurrencesComponent } from './pages/list-occurrences/list-occurrences.component';
import { ViewOccurrenceComponent } from './components/view-occurrence/view-occurrence.component';
import { FormOccurrencesComponent } from './pages/form-occurrences/form-occurrences.component';

@NgModule({
  declarations: [ListOccurrencesComponent, ViewOccurrenceComponent, FormOccurrencesComponent],
  imports: [
    CommonModule,
    OccurrencesRoutingModule,
    SharedModule,
  ]
})
export class OccurrencesModule { }
