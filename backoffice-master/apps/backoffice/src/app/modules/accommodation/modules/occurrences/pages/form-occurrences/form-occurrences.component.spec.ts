import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormOccurrencesComponent } from './form-occurrences.component';

describe('FormOccurrencesComponent', () => {
  let component: FormOccurrencesComponent;
  let fixture: ComponentFixture<FormOccurrencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormOccurrencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormOccurrencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
