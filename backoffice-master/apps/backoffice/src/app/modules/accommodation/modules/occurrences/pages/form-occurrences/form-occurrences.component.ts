import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Resource } from '@fi-sas/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, first, switchMap } from 'rxjs/operators';
import { ApplicationModel, ApplicationStatus, ApplicationStatusTranslations } from '../../../aco_applications/models/application.model';
import { ApplicationsService } from '../../../aco_applications/services/applications.service';
import { OccurrencesService } from '../../services/occurrences.service';

@Component({
  selector: 'fi-sas-form-occurrences',
  templateUrl: './form-occurrences.component.html',
  styleUrls: ['./form-occurrences.component.less']
})
export class FormOccurrencesComponent implements OnInit {
  ApplicationStatus = ApplicationStatus;
  ApplicationStatusTranslations = ApplicationStatusTranslations;
  occurrenceGroup = new FormGroup({
    application_id: new FormControl(null, Validators.required),
    subject: new FormControl(null, Validators.required),
    description: new FormControl(null, Validators.required),
    date: new FormControl(new Date(), Validators.required),
    file_id: new FormControl(null),
  });

  optionListSubscrition: Subscription = null;
  searchChange$ = new BehaviorSubject('');
  applications: ApplicationModel[] = [];
  applications_loading = false;
  submit = false;
  idToUpdate = null;
  loading = false;
  get f() { return this.occurrenceGroup.controls; }

  constructor(
    private applicationsServices: ApplicationsService,
    private ref: ChangeDetectorRef,
    public activateRoute: ActivatedRoute,
    private occurencesService: OccurrencesService,
    private router: Router,
    private uiService: UiService
  ) { }

  ngOnInit() {
    const optionList$: Observable<Resource<ApplicationModel>> = this.searchChange$
      .asObservable()
      .pipe(
        // tap(() => (this.applications_loading = true)),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(search =>
          this.applicationsServices.list(0, 10, null, null, {
            withRelated: false,
            // status: 'contracted',
            search,
            searchFields: 'full_name,tin,student_number',
            sort: 'full_name'
          }).pipe(first(), finalize(() => {
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
          }))
        ),
      );

    this.optionListSubscrition = optionList$.subscribe(results => {
      this.applications = [...results.data];
      this.applications_loading = false;
    });
    this.loadOccurrence();
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  loadOccurrence() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.occurencesService.read(parseInt(data.get('id'), 10), {
          withRelated: 'file,created_by,application',
        })
          .subscribe((result) => {
            this.occurrenceGroup.patchValue(result.data[0]);
            this.applicationsServices.read(result.data[0].application_id)
              .subscribe(app => {
                this.applications = app.data;
                this.occurrenceGroup.controls.application_id.patchValue(app.data[0].id);
              });
          });
      }
    });
  }
  submitForm(value: any) {
    this.submit = true;
    this.updateValueAndVaidity();
    if (this.occurrenceGroup.invalid) {
      return;
    }
    if (this.idToUpdate) {
      this.loading = true;
      this.occurencesService.patch(this.idToUpdate, value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(occurrence => {
        this.router.navigate(['accommodation', 'occurrences', 'list']);
        this.uiService.showMessage(MessageType.success, 'Ocorrência alterada com sucesso');
      });
    }
    else {
      this.occurencesService.create(value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(occurrence => {
        this.router.navigate(['accommodation', 'occurrences', 'list']);
        this.uiService.showMessage(MessageType.success, 'Ocorrência alterada com sucesso');
      });
    }
  }
  updateValueAndVaidity() {
    for (const i in this.occurrenceGroup.controls) {
      this.occurrenceGroup.controls[i].updateValueAndValidity();
    }
  }
}
