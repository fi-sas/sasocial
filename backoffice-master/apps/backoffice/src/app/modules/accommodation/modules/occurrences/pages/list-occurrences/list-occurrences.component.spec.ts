import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOccurrencesComponent } from './list-occurrences.component';

describe('ListOccurrencesComponent', () => {
  let component: ListOccurrencesComponent;
  let fixture: ComponentFixture<ListOccurrencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOccurrencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOccurrencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
