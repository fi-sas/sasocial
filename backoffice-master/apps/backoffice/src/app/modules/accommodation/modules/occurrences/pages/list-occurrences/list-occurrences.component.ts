import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { first } from 'rxjs/operators';
import { OccurrenceModel } from '../../models/Occurrence.model';
import { OccurrencesService } from '../../services/occurrences.service';

@Component({
  selector: 'fi-sas-list-occurrences',
  templateUrl: './list-occurrences.component.html',
  styleUrls: ['./list-occurrences.component.less']
})
export class ListOccurrencesComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private occurrencesService: OccurrencesService
  ) {
    super(uiService, router, activatedRoute);


    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: '',
        label: 'Data',
        template: (data: OccurrenceModel) => {
          return moment(new Date(data.date)).format('DD/MM/YYYY');
        },
        sortable: true,
      },
      {
        key: 'subject',
        label: 'Assunto',
        sortable: false,
      },
      {
        key: 'created_by.name',
        label: 'Criado por',
        sortable: false,
      },
    );
    this.persistentFilters['withRelated'] =
      'file,created_by,application';
    this.persistentFilters['searchFields'] = 'description,subject,full_name,email,student_number,tin,identification';
  }

  ngOnInit() {
    this.initTableData(this.occurrencesService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  deleteOccurrence(id: number) {
    this.uiService.showConfirm('Eliminar ocorrência',
      'Pretende eliminar a ocorrência?',
      'Eliminar',
      'Cancelar')
      .pipe(first()).subscribe(confirm => {
        if (confirm) {
          this.occurrencesService.delete(id).pipe(first()).subscribe(result => {
            this.uiService.showMessage(MessageType.success, 'Ocorrência eliminada com successo');
            this.searchData();
          });
        }
      });
  }


}
