import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { OccurrenceModel } from '../models/Occurrence.model';

@Injectable({
  providedIn: 'root'
})
export class OccurrencesService extends Repository<OccurrenceModel> {
  entities_url = 'ACCOMMODATION.OCCURRENCE';
  entity_url = 'ACCOMMODATION.OCCURRENCE_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
  }

}
