import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPeriodChangeRequestComponent } from './view-period-change-request.component';

describe('ViewPeriodChangeRequestComponent', () => {
  let component: ViewPeriodChangeRequestComponent;
  let fixture: ComponentFixture<ViewPeriodChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPeriodChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPeriodChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
