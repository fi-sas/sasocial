import { Component, Input, OnInit } from '@angular/core';
import { PeriodChangeRequestModel, PeriodChangeRequestStatusTranslations } from '../../models/Period-change-request.model';

@Component({
  selector: 'fi-sas-view-period-change-request',
  templateUrl: './view-period-change-request.component.html',
  styleUrls: ['./view-period-change-request.component.less']
})
export class ViewPeriodChangeRequestComponent implements OnInit {

  @Input() data: PeriodChangeRequestModel = null;
  PeriodChangeRequestStatusTranslations = PeriodChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
