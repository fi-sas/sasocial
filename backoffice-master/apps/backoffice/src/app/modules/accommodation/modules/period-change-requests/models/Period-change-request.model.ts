import { ApplicationModel } from '../../aco_applications/models/application.model';

export enum PeriodChangeRequestStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}

export enum PeriodChangeRequestDecision {
  APPROVE = 'APPROVE',
  REJECT = 'REJECT',
}

export const PeriodChangeRequestDecisionTranslations = {
  APPROVE: { label: 'Aprovada', color: 'green' },
  REJECT: { label: 'Rejeitada', color: 'red' },
};

export const PeriodChangeRequestStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export class PeriodChangeRequestHistoryModel {
  id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class PeriodChangeRequestModel {
  id: number;
  reason: string;
  application_id: number;
  application?: ApplicationModel;
  start_date: Date;
  end_date: Date;
  observations: string;
  decision: PeriodChangeRequestDecision;
  status: PeriodChangeRequestStatus;
  created_at: Date;
  update_at: Date;
  history: PeriodChangeRequestHistoryModel[];
}
