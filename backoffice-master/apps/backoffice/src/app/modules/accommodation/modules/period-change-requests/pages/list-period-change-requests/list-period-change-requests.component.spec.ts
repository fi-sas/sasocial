import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPeriodChangeRequestsComponent } from './list-period-change-requests.component';

describe('ListPeriodChangeRequestsComponent', () => {
  let component: ListPeriodChangeRequestsComponent;
  let fixture: ComponentFixture<ListPeriodChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPeriodChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPeriodChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
