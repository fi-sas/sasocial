import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { PeriodChangeRequestDecisionTranslations, PeriodChangeRequestModel, PeriodChangeRequestStatusTranslations } from '../../models/Period-change-request.model';
import { PeriodChangeRequestsService } from '../../services/period-change-requests.service';
import * as moment from 'moment';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-period-change-requests',
  templateUrl: './list-period-change-requests.component.html',
  styleUrls: ['./list-period-change-requests.component.less']
})
export class ListPeriodChangeRequestsComponent extends TableHelper implements OnInit {

  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalAbsenceSelected: PeriodChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";
  accommodationData;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private periodChangeRequestsService: PeriodChangeRequestsService,
    private accommodationService: AccommodationService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.accommodationData = accommodationService.getAccommodationData();
    if (this.accommodationData.academicYear) {
      this.persistentFilters['academic_year'] = this.accommodationData.academicYear;
    }
    if (this.accommodationData.applicationPhase) {
      this.persistentFilters['application_phase_id'] = this.accommodationData.applicationPhase.id;
    }
    if (this.accommodationData.residence) {
      this.persistentFilters['residence_id'] = this.accommodationData.residence.id;
    }

    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'end_date',
        label: 'Data Fim',
        template: (data) => {
          return moment(new Date(data.end_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: PeriodChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: PeriodChangeRequestStatusTranslations
      },
    );
   }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.periodChangeRequestsService);
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {

    if (this.changeStatusModalAction === 'analyse') {

      this.changeStatusModalLoading = true;
      this.periodChangeRequestsService.status(this.changeStatusModalAbsenceSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.periodChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.periodChangeRequestsService.status(this.changeStatusModalAbsenceSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.periodChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.periodChangeRequestsService.admin_approve(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.periodChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.periodChangeRequestsService.admin_reject(this.changeStatusModalAbsenceSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.periodChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  openModalStatus(periodRequest: PeriodChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalAbsenceSelected = periodRequest;
    this.changeStatusModalAction = action;
  }

  handleChangeStatusCancel() {
    this.changeStatusModalAbsenceSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }
}
