import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPeriodChangeRequestsComponent } from './pages/list-period-change-requests/list-period-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListPeriodChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodChangeRequestsRoutingModule { }
