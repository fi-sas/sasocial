import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { PeriodChangeRequestsRoutingModule } from './period-change-requests-routing.module';
import { ListPeriodChangeRequestsComponent } from './pages/list-period-change-requests/list-period-change-requests.component';
import { ViewPeriodChangeRequestComponent } from './components/view-period-change-request/view-period-change-request.component';

@NgModule({
    declarations: [

    ListPeriodChangeRequestsComponent,

    ViewPeriodChangeRequestComponent],
    imports: [
        CommonModule,
        SharedModule,
        PeriodChangeRequestsRoutingModule
    ]
})

export class PeriodChangeRequestsModule { }