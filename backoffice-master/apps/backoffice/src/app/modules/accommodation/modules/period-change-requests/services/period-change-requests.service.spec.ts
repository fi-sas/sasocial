import { TestBed } from '@angular/core/testing';

import { PeriodChangeRequestsService } from './period-change-requests.service';

describe('PeriodChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeriodChangeRequestsService = TestBed.get(PeriodChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
