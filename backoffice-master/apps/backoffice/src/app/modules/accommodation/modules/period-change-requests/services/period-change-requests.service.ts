import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { PeriodChangeRequestModel } from '../models/Period-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class PeriodChangeRequestsService extends Repository<PeriodChangeRequestModel> {
  entities_url = 'ACCOMMODATION.PERIOD_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.PERIOD_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }
  
  status(id: number, event: string, application_period_change, notes) {
    return this.resourceService.create<PeriodChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.PERIOD_CHANGE_REQUEST_STATUS', { id }), { event, application_period_change, notes });
  }

  admin_approve(id: number, application_period_change, notes) {
    return this.resourceService.create<PeriodChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.PERIOD_CHANGE_REQUEST_APPROVE', { id }), { application_period_change, notes });
  }

  admin_reject(id: number, application_period_change, notes) {
    return this.resourceService.create<PeriodChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.PERIOD_CHANGE_REQUEST_REJECT', { id }), { application_period_change, notes });
  }
}
