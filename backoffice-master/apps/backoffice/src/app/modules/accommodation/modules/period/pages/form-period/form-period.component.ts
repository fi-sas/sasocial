import { MessageType, UiService } from './../../../../../../core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PeriodsService } from '../../services/period.service';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-form-period',
  templateUrl: './form-period.component.html',
  styleUrls: ['./form-period.component.less']
})
export class FormPeriodComponent implements OnInit {
  loading = false;
  load_academic_years = false;
  periodId: number;
  date = [];
  academic_years: AcademicYearModel[] = [];
  startDate: Date;
  endDate: Date;
  academic_year_first_change: boolean;
  formPeriods = new FormGroup({
    academic_year: new FormControl(null, Validators.required),
    dateRange: new FormControl([], Validators.required),
  });
  submit = false;
  

  constructor(
    private uiService: UiService,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    private academicYearsService: AcademicYearsService,
    private periodsService: PeriodsService) {
    this.loadAcademicYears();
    activatedRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.academic_year_first_change = true;
        this.periodId = parseInt(data.get('id'), 10);
        this.loadPeriod();
      } else {
        this.academic_year_first_change = false;
      }
    });
  }

  get f() { return this.formPeriods.controls; }

  ngOnInit() {
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }


  loadPeriod() {
    this.periodsService.read(this.periodId).pipe(
      first(),
      finalize(() => this.loading = false))
      .subscribe(result => {
        this.date = [result.data[0].start_date, result.data[0].end_date];
        this.formPeriods.patchValue({
          dateRange: [result.data[0].start_date, result.data[0].end_date],
          academic_year: result.data[0].academic_year
        });
      });
  }


  changeAcademicYear(event) {
    if (this.academic_year_first_change) {
      this.academic_year_first_change = false;
      if (event) {
        this.startDate = (this.academic_years.find(year => year.academic_year === event)) ? (this.academic_years.find(year => year.academic_year === event).start_date) : null;
        this.endDate = (this.academic_years.find(year => year.academic_year === event)) ? (this.academic_years.find(year => year.academic_year === event).end_date) : null;
      }
      return;
    }
    this.formPeriods.get('dateRange').setValue(null);
    if (event) {
      this.startDate = this.academic_years.find(year => year.academic_year === event).start_date;
      this.endDate = this.academic_years.find(year => year.academic_year === event).end_date;
    }

  }

  disableEndDate = (current: Date) => {
    const end = new Date(this.endDate);
    end.setDate(end.getDate() + 1);
    return moment(current).isAfter(end) || moment(current).isBefore(this.startDate);
  };

  submitForm() {
    const value = { ...this.formPeriods.value };
    value.start_date = value.dateRange[0];
    value.end_date = value.dateRange[1];
    delete value.dateRange;
    this.submit = true;

    if (this.formPeriods.valid) {
      this.submit = false;
      this.loading = true;
      if (this.periodId) {
        this.periodsService.update(this.periodId, value).pipe(
          first(),
          finalize(() => this.loading = false))
          .subscribe(result => {
            this.router.navigate(['/accommodation', 'period', 'list']);
            this.uiService.showMessage(MessageType.success, 'Período alterado com sucesso');
          });
      } else {
        this.periodsService.create(value).pipe(
          first(),
          finalize(() => this.loading = false))
          .subscribe(result => {
            this.router.navigate(['/accommodation', 'period', 'list']);
            this.uiService.showMessage(MessageType.success, 'Período criado com sucesso');
          });
      }
    }


  }

  backList() {
    this.router.navigate(['/accommodation', 'period', 'list']);
  }
}
