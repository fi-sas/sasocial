import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

import { first, finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeriodsService } from '../../services/period.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';


@Component({
  selector: 'fi-sas-period-list',
  templateUrl: './list-period.component.html',
  styleUrls: ['./list-period.component.less']
})
export class PeriodListComponent extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private periodService: PeriodsService,
  ) {
    super(uiService, router, activatedRoute);
    this.initTableData(this.periodService);

  }

  ngOnInit() {

  }

  newPeriod() {
    this.router.navigate(['/accommodation', 'period', 'create']);
  }

  deletePeriod(id: number) {
    this.uiService.showConfirm('Eliminar período de alojamento',
      'Pretende eliminar este período?',
      'Eliminar',
      'Cancelar')
      .pipe(first()).subscribe(confirm => {
        if (confirm) {
          this.periodService.delete(id).pipe(first()).subscribe(result => {
            this.uiService.showMessage(MessageType.success, 'Período eliminado com successo');
            this.initTableData(this.periodService);
          });
        }
      });
  }

}
