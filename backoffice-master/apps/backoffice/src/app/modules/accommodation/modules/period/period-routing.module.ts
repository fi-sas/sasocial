import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPeriodComponent } from './pages/form-period/form-period.component';
import { PeriodListComponent } from './pages/list-period/list-period.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormPeriodComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:accommodation-periods:create' }
  },
  {
    path: 'edit/:id',
    component: FormPeriodComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:accommodation-periods:create' }
  },
  {
    path: 'list',
    component: PeriodListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:accommodation-periods:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodRoutingModule { }
