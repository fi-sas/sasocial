import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeriodRoutingModule } from './period-routing.module';
import { FormPeriodComponent } from './pages/form-period/form-period.component';
import { PeriodListComponent } from './pages/list-period/list-period.component';

@NgModule({
  declarations: [
    FormPeriodComponent,
    PeriodListComponent,
  ],
  imports: [
    CommonModule,
    PeriodRoutingModule,
    SharedModule
  ]
})
export class PeriodModule { }
