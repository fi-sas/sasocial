
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { PeriodModel } from '../model/period.model';


@Injectable({
  providedIn: 'root'
})
export class PeriodsService extends Repository<PeriodModel>{

  constructor(resource: FiResourceService, url: FiUrlService) {
    super(resource, url);
    this.entities_url = 'ACCOMMODATION.PERIODS';
    this.entity_url = 'ACCOMMODATION.PERIODS_ID';
  }

  getPeriodsByAcademicYear(academic_year: string) {
    let params = new HttpParams();
    params = params.set('query[academic_year]', academic_year);
    return this.resourceService.read<PeriodModel>(
      this.urlService.get('ACCOMMODATION.PERIODS'),
      {params}
    );
  }
}
