import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPermuteRoomRequestComponent } from './view-permute-room-request.component';

describe('ViewPermuteRoomRequestComponent', () => {
  let component: ViewPermuteRoomRequestComponent;
  let fixture: ComponentFixture<ViewPermuteRoomRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPermuteRoomRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPermuteRoomRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
