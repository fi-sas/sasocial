import { Component, Input, OnInit } from '@angular/core';
import { RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';

@Component({
  selector: 'fi-sas-view-permute-room-request',
  templateUrl: './view-permute-room-request.component.html',
  styleUrls: ['./view-permute-room-request.component.less']
})
export class ViewPermuteRoomRequestComponent implements OnInit {

  @Input() data: RoomChangeRequestModel = null;
  RoomChangeRequestStatusTranslations = RoomChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
