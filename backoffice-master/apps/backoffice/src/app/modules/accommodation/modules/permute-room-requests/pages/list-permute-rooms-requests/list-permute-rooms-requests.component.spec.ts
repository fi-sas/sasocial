import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPermuteRoomsRequestsComponent } from './list-permute-rooms-requests.component';

describe('ListPermuteRoomsRequestsComponent', () => {
  let component: ListPermuteRoomsRequestsComponent;
  let fixture: ComponentFixture<ListPermuteRoomsRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPermuteRoomsRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPermuteRoomsRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
