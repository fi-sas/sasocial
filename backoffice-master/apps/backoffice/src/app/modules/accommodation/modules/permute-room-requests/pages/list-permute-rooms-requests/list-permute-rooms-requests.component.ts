import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RoomChangeRequestDecisionTranslations, RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { RoomChangeRequestsService } from '@fi-sas/backoffice/modules/accommodation/services/room-change-requests.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-permute-rooms-requests',
  templateUrl: './list-permute-rooms-requests.component.html',
  styleUrls: ['./list-permute-rooms-requests.component.less']
})
export class ListPermuteRoomsRequestsComponent extends TableHelper implements OnInit {

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalPermuteSelected: RoomChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";


  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private roomChangeRequestsService: RoomChangeRequestsService,
    private accommodationService: AccommodationService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: 'room.name',
        label: 'Novo quarto',
        sortable: true
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: RoomChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: RoomChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'residence,room,application,history,file';
    this.persistentFilters['type'] = "PERMUTE";
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }


  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.roomChangeRequestsService);
  }

  openModalWithdrawal(typologyChange: RoomChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalPermuteSelected = typologyChange;
    this.changeStatusModalAction = action;
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {
    if (this.changeStatusModalAction === 'analyse') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.status(this.changeStatusModalPermuteSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.status(this.changeStatusModalPermuteSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.admin_approve(this.changeStatusModalPermuteSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.admin_reject(this.changeStatusModalPermuteSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  handleChangeStatusCancel() {
    this.changeStatusModalPermuteSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
