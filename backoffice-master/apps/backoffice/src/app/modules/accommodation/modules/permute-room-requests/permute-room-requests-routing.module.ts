import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPermuteRoomsRequestsComponent } from './pages/list-permute-rooms-requests/list-permute-rooms-requests.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListPermuteRoomsRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermuteRoomRequestsRoutingModule { }
