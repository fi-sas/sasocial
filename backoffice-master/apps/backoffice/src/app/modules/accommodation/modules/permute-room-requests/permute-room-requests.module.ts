import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermuteRoomRequestsRoutingModule } from './permute-room-requests-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListPermuteRoomsRequestsComponent } from './pages/list-permute-rooms-requests/list-permute-rooms-requests.component';
import { ViewPermuteRoomRequestComponent } from './components/view-permute-room-request/view-permute-room-request.component';

@NgModule({
  declarations: [ListPermuteRoomsRequestsComponent, ViewPermuteRoomRequestComponent],
  imports: [
    CommonModule,
    PermuteRoomRequestsRoutingModule,
    SharedModule
  ]
})
export class PermuteRoomRequestsModule { }
