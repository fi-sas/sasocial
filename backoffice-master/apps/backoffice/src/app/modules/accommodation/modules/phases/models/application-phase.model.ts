export class ApplicationPhaseModel {
    id: number;
    residence_id: number;
    start_date: Date;
    end_date: Date;
    out_of_date_from: Date;
    academic_year: string;
    out_of_date_validation: boolean;
    out_of_date_validation_days: number; 
    application_phase: number;
    fixed_accommodation_period: boolean;
}