import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ApplicationPhaseService } from '../../services/application-phase.service';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';

function betweenDatesValidator(form: FormGroup): { [key: string]: boolean } {
  if (
    form.controls['out_of_date_from'].value !== null &&
    moment(form.controls['out_of_date_from'].value).isValid
  ) {
    if (form.controls['dateRange'].value) {
      if (
        !moment(form.controls['out_of_date_from'].value).isBetween(
          form.controls['dateRange'].value[0],
          form.controls['dateRange'].value[1]
        )
      ) {
        return { notBetween: true };
      }
    }
  }

  return null;
}

@Component({
  selector: 'fi-sas-phase-form',
  templateUrl: './phase-form.component.html',
  styleUrls: ['./phase-form.component.less'],
})
export class PhaseFormComponent implements OnInit {
  academic_years: AcademicYearModel[] = [];
  load_academic_years = false;
  submit = false;
  idToUpdate = false;
  phaseGroup = new FormGroup(
    {
      dateRange: new FormControl([], Validators.required),
      out_of_date_from: new FormControl(null, [Validators.required]),
      academic_year: new FormControl(null, Validators.required),
      application_phase: new FormControl(1, Validators.required),
      out_of_date_validation: new FormControl(false, Validators.required),
      out_of_date_validation_days: new FormControl(null),
      fixed_accommodation_period: new FormControl(false, Validators.required),
    },
    betweenDatesValidator
  );

  loading = false;
  phase_id: number;

  constructor(
    private academicYearsService: AcademicYearsService,
    public activatedRoute: ActivatedRoute,
    private uiService: UiService,
    private router: Router,
    private phasesService: ApplicationPhaseService
  ) {
    this.loadAcademicYears();
    activatedRoute.paramMap.subscribe((data) => {
      if (data.get('phase_id')) {
        this.phase_id = parseInt(data.get('phase_id'), 10);
        this.idToUpdate = true;
        this.loadPhase();
      }
    });
  }

  ngOnInit() {}

  get f() {
    return this.phaseGroup.controls;
  }

  loadPhase() {
    this.phasesService
      .read(this.phase_id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.phaseGroup.patchValue({
          dateRange: [result.data[0].start_date, result.data[0].end_date],
          academic_year: result.data[0].academic_year,
          application_phase: result.data[0].application_phase,
          out_of_date_validation: result.data[0].out_of_date_validation,
          out_of_date_validation_days:
            result.data[0].out_of_date_validation_days,
          fixed_accommodation_period: result.data[0].fixed_accommodation_period,
        });
        this.phaseGroup.patchValue({
          out_of_date_from: result.data[0].out_of_date_from,
        });
      });
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  changeOutDate(event) {
    if (event) {
      this.phaseGroup.controls.out_of_date_validation_days.setValidators([
        Validators.required,
      ]);
      this.phaseGroup.controls.out_of_date_from.setValue(null);
      this.phaseGroup.controls.out_of_date_from.disable();
      this.phaseGroup.controls.out_of_date_from.clearValidators();
    } else {
      this.phaseGroup.controls.out_of_date_validation_days.clearValidators();
      this.phaseGroup.controls.out_of_date_from.enable();
      this.phaseGroup.controls.out_of_date_from.setValue(new Date());
      this.phaseGroup.controls.out_of_date_from.setValidators([
        Validators.required,
      ]);
      this.phaseGroup.controls.out_of_date_validation_days.setValue(null);
    }
    this.phaseGroup.controls.out_of_date_validation_days.updateValueAndValidity();
    this.phaseGroup.controls.out_of_date_from.updateValueAndValidity();
  }

  submitForm(formValue: any) {
    const value = { ...formValue };
    value.start_date = value.dateRange[0];
    value.end_date = value.dateRange[1];
    delete value.dateRange;
    value.out_of_date_from = this.phaseGroup.get('out_of_date_from').value
      ? this.phaseGroup.get('out_of_date_from').value
      : null;
    this.submit = true;

    if (this.phaseGroup.valid) {
      this.submit = false;

      this.loading = true;
      if (this.phase_id) {
        this.phasesService
          .update(this.phase_id, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.router.navigate(['/accommodation', 'phases', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Fase alterada com sucesso'
            );
          });
      } else {
        this.phasesService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.router.navigate(['/accommodation', 'phases', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Fase criada com sucesso'
            );
          });
      }
    }
  }

  backList() {
    this.router.navigate(['/accommodation', 'phases', 'list']);
  }
}
