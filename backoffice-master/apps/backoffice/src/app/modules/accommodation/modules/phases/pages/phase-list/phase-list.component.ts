import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

import { first, finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ApplicationPhaseModel } from '../../models/application-phase.model';
import { ApplicationPhaseService } from '../../services/application-phase.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';


@Component({
  selector: 'fi-sas-phase-list',
  templateUrl: './phase-list.component.html',
  styleUrls: ['./phase-list.component.less']
})
export class PhaseListComponent extends TableHelper implements OnInit {
  YesNoTag = TagComponent.YesNoTag;
  loading = false;
  phases: ApplicationPhaseModel[] = [];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private applicationPhasesService: ApplicationPhaseService,
  ) {
    super(uiService, router, activatedRoute);
    
  }

  ngOnInit() {
    this.initTableData(this.applicationPhasesService);
  }

  newPhase() {
    this.router.navigate(['/accommodation', 'phases', 'create']);
  }


  phaseValue(phase: ApplicationPhaseModel) {
    const result = moment().isBetween(phase.start_date, phase.end_date) ? 'open' : 'closed';

    if(result === 'open' && phase.out_of_date_from) {
      return moment().isAfter(phase.out_of_date_from) ? 'out_of_date' : 'open';
    }
    return result;
  }


  phaseResult(phase: ApplicationPhaseModel) {
    return {
      open: {
        label: 'Aberta',
        color: 'green'
      },
      out_of_date: {
        label: 'Fora de prazo',
        color: 'orange'
      },
      closed: {
        label: 'Fechado',
        color: 'red'
      }
    };

  }


  deletePhase(id: number) {
    this.uiService.showConfirm('Eliminar fase de candidatura',
      'Pretende eliminar a fase de candidatura?',
      'Eliminar',
      'Cancelar')
      .pipe(first()).subscribe(confirm => {
        if (confirm) {
          this.applicationPhasesService.delete(id).pipe(first()).subscribe(result => {
            this.uiService.showMessage(MessageType.success, 'Fase de candidatura eliminada com successo');
            this.initTableData(this.applicationPhasesService);
          });
        }
      });
  }

}
