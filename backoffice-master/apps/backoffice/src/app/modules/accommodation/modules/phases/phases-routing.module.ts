import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhaseListComponent } from './pages/phase-list/phase-list.component';
import { PhaseFormComponent } from './pages/phase-form/phase-form.component';

const routes: Routes = [

  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: PhaseFormComponent,
    data: { breadcrumb: 'Nova', title: 'Nova', scope: 'accommodation:application-phases:create' }
  },
  {
    path: 'edit/:phase_id',
    component: PhaseFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:application-phases:create' }
  },
  {
    path: 'list',
    component: PhaseListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:application-phases:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhasesRoutingModule { }
