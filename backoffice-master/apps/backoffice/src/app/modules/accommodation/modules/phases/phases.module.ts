import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhasesRoutingModule } from './phases-routing.module';
import { PhaseFormComponent } from './pages/phase-form/phase-form.component';
import { PhaseListComponent } from './pages/phase-list/phase-list.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    PhaseListComponent,
    PhaseFormComponent
  ],
  imports: [
    CommonModule,
    PhasesRoutingModule,
    SharedModule
  ]
})
export class PhasesModule { }
