import { TestBed } from '@angular/core/testing';

import { ApplicationPhaseService } from './application-phase.service';

describe('ApplicationPhaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationPhaseService = TestBed.get(ApplicationPhaseService);
    expect(service).toBeTruthy();
  });
});
