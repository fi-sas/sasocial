import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ApplicationPhaseModel } from '../models/application-phase.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApplicationPhaseService extends Repository<ApplicationPhaseModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.APPLICATION_PHASE';
    this.entity_url = 'ACCOMMODATION.APPLICATION_PHASE_ID';
  }

  openApplication(): Observable<Resource<ApplicationPhaseModel>> {
    return this.resourceService.read<ApplicationPhaseModel>(
      this.urlService.get('ACCOMMODATION.APPLICATION_PHASE_OPEN')
    );
  }

  getPhasesByAcademicYear(academic_year: string): Observable<Resource<ApplicationPhaseModel>> {
    let params = new HttpParams();
    if(academic_year){
      params = params.set('query[academic_year]', academic_year);
    }
    return this.resourceService.list<ApplicationPhaseModel>(
      this.urlService.get('ACCOMMODATION.APPLICATION_PHASE'),
      {
        params,
      }
    );
  }

  getPhasesSinceToday(): Observable<Resource<ApplicationPhaseModel>>{
    return this.resourceService.list<ApplicationPhaseModel>(this.urlService.get('ACCOMMODATION.APPLICATION_PHASE_FROM_TODAY'));
  }

}
