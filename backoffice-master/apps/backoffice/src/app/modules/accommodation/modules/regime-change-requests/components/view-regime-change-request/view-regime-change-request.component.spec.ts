import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRegimeChangeRequestComponent } from './view-regime-change-request.component';

describe('ViewRegimeChangeRequestComponent', () => {
  let component: ViewRegimeChangeRequestComponent;
  let fixture: ComponentFixture<ViewRegimeChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRegimeChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRegimeChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
