import { Component, Input, OnInit } from '@angular/core';
import { RegimeChangeRequestModel, RegimeChangeRequestStatusTranslations } from '../../models/Regime-change-request.model';

@Component({
  selector: 'fi-sas-view-regime-change-request',
  templateUrl: './view-regime-change-request.component.html',
  styleUrls: ['./view-regime-change-request.component.less']
})
export class ViewRegimeChangeRequestComponent implements OnInit {

  @Input() data: RegimeChangeRequestModel = null;
  RegimeChangeRequestStatusTranslations = RegimeChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
