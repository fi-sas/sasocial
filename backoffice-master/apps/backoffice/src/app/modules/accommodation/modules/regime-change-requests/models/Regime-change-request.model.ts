import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { ExtraModel } from '../../extras/models/extra.model';
import { RegimeModel } from '../../regimes/models/regime.model';


// export const ExtensionDecision = {
//   APPROVE: { label: 'Aprovar', color: 'green' },
//   REJECT: { label: 'Rejeitar', color: 'red' },
// };
export enum RegimeChangeRequestStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}
export enum RegimeChangeRequestDecision {
  APPROVE = 'APPROVE',
  REJECT = 'REJECT',
}
export const RegimeChangeRequestDecisionTranslations = {
  APPROVE: { label: 'Aprovada', color: 'green' },
  REJECT: { label: 'Rejeitada', color: 'red' },
};
export const RegimeChangeRequestStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export class RegimeChangeRequestHistoryModel {
  id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class RegimeChangeRequestModel {
  id: number;
  reason: string;
  application_id: number;
  application: ApplicationModel;
  start_date: Date;
  observations: string;
  decision: RegimeChangeRequestDecision;
  status: RegimeChangeRequestStatus;
  created_at: Date;
  update_at: Date;
  history: RegimeChangeRequestHistoryModel[];
  regime_id: number;
  regime: RegimeModel;
  file_id: number;
  file: FileModel;
}
