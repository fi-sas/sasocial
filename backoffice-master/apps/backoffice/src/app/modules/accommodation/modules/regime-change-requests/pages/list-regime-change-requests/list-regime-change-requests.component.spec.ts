import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRegimeChangeRequestsComponent } from './list-regime-change-requests.component';

describe('ListRegimeChangeRequestsComponent', () => {
  let component: ListRegimeChangeRequestsComponent;
  let fixture: ComponentFixture<ListRegimeChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRegimeChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRegimeChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
