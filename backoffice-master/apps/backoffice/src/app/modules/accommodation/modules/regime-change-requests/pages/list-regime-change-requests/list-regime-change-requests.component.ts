import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { RegimeChangeRequestDecisionTranslations, RegimeChangeRequestModel, RegimeChangeRequestStatusTranslations } from '../../models/Regime-change-request.model';
import { RegimeChangeRequestsService } from '../../services/regime-change-requests.service';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';

@Component({
  selector: 'fi-sas-list-regime-change-requests',
  templateUrl: './list-regime-change-requests.component.html',
  styleUrls: ['./list-regime-change-requests.component.less']
})
export class ListRegimeChangeRequestsComponent extends TableHelper implements OnInit {

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalRegimeChangeSelected: RegimeChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";



  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private regimeChangeRequestsService: RegimeChangeRequestsService,
    private accommodationService: AccommodationService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: '',
        label: 'Novo Regime',
        template: (data: RegimeChangeRequestModel) => {
          const pt = data.regime.translations.find(x => x.language_id == 3);
          return pt ? pt.name : '-';
        },
        sortable: true
      },

      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: RegimeChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: RegimeChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'regime,application,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.regimeChangeRequestsService);
  }

  openModalWithdrawal(absence: RegimeChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalRegimeChangeSelected = absence;
    this.changeStatusModalAction = action;
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }


  handleChangeStatus() {

    if (this.changeStatusModalAction === 'analyse') {

      this.changeStatusModalLoading = true;
      this.regimeChangeRequestsService.status(this.changeStatusModalRegimeChangeSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.regimeChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.regimeChangeRequestsService.status(this.changeStatusModalRegimeChangeSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.regimeChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.regimeChangeRequestsService.admin_approve(this.changeStatusModalRegimeChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.regimeChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.regimeChangeRequestsService.admin_reject(this.changeStatusModalRegimeChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.regimeChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  handleChangeStatusCancel() {
    this.changeStatusModalRegimeChangeSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }


}
