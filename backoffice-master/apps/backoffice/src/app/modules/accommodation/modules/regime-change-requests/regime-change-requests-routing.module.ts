import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRegimeChangeRequestsComponent } from './pages/list-regime-change-requests/list-regime-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListRegimeChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegimeChangeRequestsRoutingModule { }
