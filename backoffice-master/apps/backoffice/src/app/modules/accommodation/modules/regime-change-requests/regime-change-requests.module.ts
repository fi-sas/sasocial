import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegimeChangeRequestsRoutingModule } from './regime-change-requests-routing.module';
import { ListRegimeChangeRequestsComponent } from './pages/list-regime-change-requests/list-regime-change-requests.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewRegimeChangeRequestComponent } from './components/view-regime-change-request/view-regime-change-request.component';

@NgModule({
  declarations: [ListRegimeChangeRequestsComponent, ViewRegimeChangeRequestComponent],
  imports: [
    CommonModule,
    RegimeChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class RegimeChangeRequestsModule { }
