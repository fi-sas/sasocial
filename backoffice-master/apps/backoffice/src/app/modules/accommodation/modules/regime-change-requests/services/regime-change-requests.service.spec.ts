import { TestBed } from '@angular/core/testing';

import { RegimeChangeRequestsService } from './regime-change-requests.service';

describe('RegimeChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegimeChangeRequestsService = TestBed.get(RegimeChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
