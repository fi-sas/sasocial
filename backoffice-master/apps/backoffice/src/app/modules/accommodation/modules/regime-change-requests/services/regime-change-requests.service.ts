import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { RegimeChangeRequestModel } from '../models/Regime-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class RegimeChangeRequestsService extends Repository<RegimeChangeRequestModel> {
  entities_url = 'ACCOMMODATION.REGIME_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.REGIME_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, application_regime_change, notes) {
    return this.resourceService.create<RegimeChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST_STATUS', { id }), { event, application_regime_change, notes });
  }

  admin_approve(id: number, application_regime_change, notes) {
    return this.resourceService.create<RegimeChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST_APPROVE', { id }), { application_regime_change, notes });
  }

  admin_reject(id: number, application_regime_change, notes) {
    return this.resourceService.create<RegimeChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST_REJECT', { id }), { application_regime_change, notes });
  }


}
