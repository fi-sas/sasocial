import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRegimeComponent } from './view-regime.component';

describe('ViewRegimeComponent', () => {
  let component: ViewRegimeComponent;
  let fixture: ComponentFixture<ViewRegimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRegimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRegimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
