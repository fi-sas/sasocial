import { PriceLinePeriod } from '../../models/regime.model';
import { LanguageModel } from '../../../../../configurations/models/language.model';
import { Component, OnInit, Input } from '@angular/core';
import { RegimeModel } from '../../models/regime.model';

@Component({
  selector: 'fi-sas-view-regime',
  templateUrl: './view-regime.component.html',
  styleUrls: ['./view-regime.component.less']
})
export class ViewRegimeComponent implements OnInit {

  @Input() data: RegimeModel = null;
  @Input() languages: LanguageModel[] = [];

  loading = false; 
  constructor(
    
  ) { 
    
  }

  ngOnInit() {
   
  }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  getPeriodTranslations(period: PriceLinePeriod) {
      switch(period) {
        case PriceLinePeriod.DAY:
          return 'Dia';
          break;
        case PriceLinePeriod.WEEK:
          return 'Semana';
          break;
        case PriceLinePeriod.MONTH:
          return 'Mês';
          break;
      }
  }
}
