import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
export class RegimeModel {
  id?: number;
  translations: RegimeTranslationModel[];
  priceLines: RegimePriceLineModel[];
  active: boolean;
  product_code: string;
  billing_name: string;
  appear_billing: boolean;
  created_at: string;
  update_at: string;
  regime_id: number;
  regime: RegimeModel;
  discounts: RegimeDiscount[];
  reprocess_since: Date;
}

export class RegimeTranslationModel {
  language_id: number;
  name: string;
  description: string;
}

export class RegimePriceLineModel {
  price: number;
  vat?: TaxModel;
  vat_id: number;
  period: PriceLinePeriod;
  tariff_id: number;
}

export class RegimeDiscount{
  month: number;
  year: number;
  discount_value: number;
}

export enum PriceLinePeriod {
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
}