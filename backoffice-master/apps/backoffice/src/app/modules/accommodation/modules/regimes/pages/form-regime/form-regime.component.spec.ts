import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegimeComponent } from './form-regime.component';

describe('FormRegimeComponent', () => {
  let component: FormRegimeComponent;
  let fixture: ComponentFixture<FormRegimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRegimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRegimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
