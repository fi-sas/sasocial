import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from './../../../../../configurations/services/languages.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { RegimesService } from '../../services/regimes.service';
import { Validators, FormGroup, FormArray, FormControl, Form } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { first, finalize } from 'rxjs/operators';
import { PriceLinePeriod } from '../../models/regime.model';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-form-regime',
  templateUrl: './form-regime.component.html',
  styleUrls: ['./form-regime.component.less'],
})
export class FormRegimeComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  loadingTaxes = false;
  taxes: TaxModel[] = [];
  submit = false;

  loadingTariffs = false;
  tariffs: TariffModel[] = [];

  loadingLanguages = false;
  languages: LanguageModel[] = [];
  loading = false;
  regimeGroup = new FormGroup({
    translations: new FormArray([]),
    priceLines: new FormArray([]),
    product_code: new FormControl('', [Validators.required, trimValidation]),
    billing_name: new FormControl('', [Validators.required, trimValidation]),
    appear_billing: new FormControl(true, Validators.required),
    active: new FormControl(true, Validators.required),
    reprocess_since: new FormControl(null),
    discounts: new FormArray([]),
  });
  translations = this.regimeGroup.get('translations') as FormArray;
  priceLines = this.regimeGroup.get('priceLines') as FormArray;
  discounts = this.regimeGroup.get('discounts') as FormArray;
  get f() { return this.regimeGroup.controls; }
  idToUpdate = null;

  constructor(
    private languagesService: LanguagesService,
    public regimesService: RegimesService,
    private taxesService: TaxesService,
    private uiService: UiService,
    public activateRoute: ActivatedRoute,
    public router: Router,
    private tariffsService: TariffsService
  ) { }

  ngOnInit() {
    this.loadLanguages();
    this.loadTaxes();
    this.loadTariffs();
  }

  loadTaxes() {
    this.loadingTaxes = true;
    this.taxesService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingTaxes = false))
      )
      .subscribe((results) => {
        this.taxes = results.data;
      });
  }
  loadTariffs() {
    this.loadingTariffs = true;
    this.tariffsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingTariffs = false))
      )
      .subscribe((results) => {
        this.tariffs = results.data;
      });
  }
  
  disableDate = (date: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(date).isBefore(moment(currentDate).format("YYYY-MM-DD"));
  }

  addPriceLine(
    price?: number,
    vat_id?: number,
    period?: PriceLinePeriod,
    tariff_id?: number,
  ) {
    const priceLines = this.regimeGroup.controls.priceLines as FormArray;
    priceLines.push(
      new FormGroup({
        price: new FormControl(price ? price : 0, Validators.required),
        vat_id: new FormControl(
          vat_id ? vat_id : this.taxes.length > 0 ? this.taxes[0].id : null,
          Validators.required
        ),
        period: new FormControl(period ? period : null, Validators.required),
        tariff_id: new FormControl(tariff_id ? tariff_id : null, Validators.required),
      })
    );
  }

  deletePriceLine(priceLineIndex: any) {
    if (this.priceLines.at(priceLineIndex)) {
      this.priceLines.removeAt(priceLineIndex);
    }
  }

  addDiscountLine( monthAndYear?: Date, discount_value?: number) {
    const discounts = this.regimeGroup.controls.discounts as FormArray;
    discounts.push(
      new FormGroup({
        date: new FormControl(monthAndYear? monthAndYear : null , Validators.required),
        discount_value: new FormControl(discount_value ? discount_value : 0, Validators.required),
      })
    );
  }

  deleteDiscountLine(discountLineIndex: any) {
    if (this.discounts.at(discountLineIndex)) {
      this.discounts.removeAt(discountLineIndex);
    }
  }


  loadRegime() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.regimesService
          .read(parseInt(data.get('id'), 10))
          .subscribe((result) => {
            this.regimeGroup.patchValue({
              product_code: result.data[0].product_code,
              billing_name: result.data[0].billing_name,
              appear_billing: result.data[0].appear_billing,
              active: result.data[0].active,
              reprocess_since: result.data[0].reprocess_since,
            });

            if (result.data[0].translations) {
              result.data[0].translations.map((t) => {
                this.addTranslation(t.language_id, t.name, t.description);
              });
            }

            if (result.data[0].discounts) {
              result.data[0].discounts.map(t => {
                this.addDiscountLine(new Date(t.year, t.month, null), t.discount_value);
              });
            }

            this.languages.map((l) => {
              const foundLnaguage = this.translations.value.find(
                (t) => t.language_id === l.id
              );
              if (!foundLnaguage) {
                this.addTranslation(l.id, '', '');
              }
            });

            if (result.data[0].priceLines) {
              result.data[0].priceLines.map((pl) => {
                this.addPriceLine(
                  pl.price,
                  pl.vat_id,
                  pl.period,
                  pl.tariff_id
                );
              });
            }
          });
      } else {
        this.languages.map((l) => {
          this.addTranslation(l.id, '', '');
        });
      }
    });
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.regimeGroup.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
        description: new FormControl(description, trimValidation),
      })
    );
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loadingLanguages = false))
      )
      .subscribe((results) => {
        this.languages = results.data;

        this.loadRegime();
      });
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  submitForm(value: any) {
    this.submit = true;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    for (const pl in this.priceLines.controls) {
      if (pl) {
        const plt = this.priceLines.get(pl) as FormGroup;
        for (const i in plt.controls) {
          if (plt) {
            plt.controls[i].markAsDirty();
            plt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    for (const ds in this.discounts.controls) {
      if (ds) {
        const disc = this.discounts.get(ds) as FormGroup;
        for (const i in disc.controls) {
          if (disc) {
            disc.controls[i].markAsDirty();
            disc.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    if (this.regimeGroup.valid) {
      this.loading = true;
      this.submit = false;
      if (this.idToUpdate) {
        this.regimesService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            this.router.navigate(['accommodation', 'regimes', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Regime alterado com sucesso'
            );
          });
      } else {
        this.regimesService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            this.router.navigate(['accommodation', 'regimes', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Regime criado com sucesso'
            );
          });
      }
    }
  }

  backList() {
    this.router.navigate(['accommodation', 'regimes', 'list']);
  }

  isDisabledPeriod(value:any , i){
    const tariffs = this.regimeGroup.get('priceLines')['controls'][i].value;
    return this.priceLines.controls.find(p => p.value.tariff_id === tariffs.tariff_id && p.value.period === value);
  }

  isTariffDisabled(tariff){
    const tariffs = this.priceLines.controls.filter(p => p.value.tariff_id === tariff.id);
    return tariff.active ? tariffs.find(t => t.value.period === "DAY") && tariffs.find(t => t.value.period === "WEEK") && tariffs.find(t => t.value.period === "MONTH") : false;
  }

  filterDaySelection(i){
    const tariffs = this.regimeGroup.get('priceLines')['controls'][i];
    tariffs.controls.period.setValue(null)
  }
}
