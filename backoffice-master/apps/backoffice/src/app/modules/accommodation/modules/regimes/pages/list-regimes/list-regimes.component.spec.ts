import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRegimesComponent } from './list-regimes.component';

describe('ListRegimesComponent', () => {
  let component: ListRegimesComponent;
  let fixture: ComponentFixture<ListRegimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRegimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRegimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
