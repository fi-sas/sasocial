import { first } from 'rxjs/operators';
import { TagComponent } from './../../../../../../shared/components/tag/tag.component';
import { Component, OnInit } from '@angular/core';
import { RegimesService } from '../../services/regimes.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';

@Component({
  selector: 'fi-sas-list-regimes',
  templateUrl: './list-regimes.component.html',
  styleUrls: ['./list-regimes.component.less']
})
export class ListRegimesComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    public regimesService: RegimesService
  ) {
    super(uiService, router, activatedRoute);
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.persistentFilters = {
      searchFields: 'name',
    };

  }


  ngOnInit() {
    //this.persistentFilters['withRelated'] = 'priceLines,translations';
    this.initTableData(this.regimesService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  deleteRegime(id: number) {
    this.uiService.showConfirm('Eliminar regime', 'Pretende eliminar este regime', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.regimesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Regime eliminado com sucesso');
        });
      }
    });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
