import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormRegimeComponent } from './pages/form-regime/form-regime.component';
import { ListRegimesComponent } from './pages/list-regimes/list-regimes.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormRegimeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:regimes:create' }
  },
  {
    path: 'edit/:id',
    component: FormRegimeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:regimes:create' }
  },
  {
    path: 'list',
    component: ListRegimesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:regimes:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegimesRoutingModule { }
