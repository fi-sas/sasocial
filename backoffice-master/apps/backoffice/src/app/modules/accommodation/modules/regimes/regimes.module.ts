import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegimesRoutingModule } from './regimes-routing.module';
import { FormRegimeComponent } from './pages/form-regime/form-regime.component';
import { ListRegimesComponent } from './pages/list-regimes/list-regimes.component';
import { ViewRegimeComponent } from './components/view-regime/view-regime.component';

@NgModule({
  declarations: [
    FormRegimeComponent,
    ListRegimesComponent,
    ViewRegimeComponent
  ],
  imports: [
    CommonModule,
    RegimesRoutingModule,
    SharedModule
  ]
})
export class RegimesModule { }
