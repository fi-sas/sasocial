import { TestBed } from '@angular/core/testing';

import { RegimesService } from './regimes.service';

describe('RegimesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegimesService = TestBed.get(RegimesService);
    expect(service).toBeTruthy();
  });
});
