import { FiResourceService } from '@fi-sas/core';
import { FiUrlService } from '@fi-sas/core';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { RegimeModel } from '../models/regime.model';

@Injectable({
  providedIn: 'root'
})
export class RegimesService extends Repository<RegimeModel> {

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);

    this.entity_url = 'ACCOMMODATION.REGIMES_ID';
    this.entities_url = 'ACCOMMODATION.REGIMES';
  }
}
