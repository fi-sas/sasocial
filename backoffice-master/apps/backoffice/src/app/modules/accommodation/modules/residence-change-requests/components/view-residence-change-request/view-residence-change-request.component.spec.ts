import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewResidenceChangeRequestComponent } from './view-residence-change-request.component';

describe('ViewResidenceChangeRequestComponent', () => {
  let component: ViewResidenceChangeRequestComponent;
  let fixture: ComponentFixture<ViewResidenceChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewResidenceChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewResidenceChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
