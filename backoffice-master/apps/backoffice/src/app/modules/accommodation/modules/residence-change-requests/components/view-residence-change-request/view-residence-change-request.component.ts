import { Component, Input, OnInit } from '@angular/core';
import { RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';

@Component({
  selector: 'fi-sas-view-residence-change-request',
  templateUrl: './view-residence-change-request.component.html',
  styleUrls: ['./view-residence-change-request.component.less']
})
export class ViewResidenceChangeRequestComponent implements OnInit {

  @Input() data: RoomChangeRequestModel = null;
  RoomChangeRequestStatusTranslations = RoomChangeRequestStatusTranslations;


  constructor() { }

  ngOnInit() {
  }

}
