import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListResidenceChangeRequestsComponent } from './list-residence-change-requests.component';

describe('ListResidenceChangeRequestsComponent', () => {
  let component: ListResidenceChangeRequestsComponent;
  let fixture: ComponentFixture<ListResidenceChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListResidenceChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListResidenceChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
