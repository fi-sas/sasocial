import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RoomChangeRequestDecisionTranslations, RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { RoomChangeRequestsService } from '@fi-sas/backoffice/modules/accommodation/services/room-change-requests.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { RoomModel } from '../../../rooms/models/room.model';
import { RoomsService } from '../../../rooms/services/rooms.service';

@Component({
  selector: 'fi-sas-list-residence-change-requests',
  templateUrl: './list-residence-change-requests.component.html',
  styleUrls: ['./list-residence-change-requests.component.less']
})
export class ListResidenceChangeRequestsComponent extends TableHelper implements OnInit {


  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalResidenceChangeSelected: RoomChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";
  rooms: RoomModel[] = [];
  roomsLoading = false;
  newRoomId = null;


  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private roomsChangeRequestsService: RoomChangeRequestsService,
    private roomsService: RoomsService,
    private accommodationService: AccommodationService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: 'residence.name',
        label: 'Nova residência',
        sortable: true
      },

      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: RoomChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: RoomChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'residence,room,application,history,file';
    this.persistentFilters['type'] = "RESIDENCE";
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.roomsChangeRequestsService);
  }


  openModalWithdrawal(typologyChange: RoomChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalResidenceChangeSelected = typologyChange;
    this.changeStatusModalAction = action;
    this.roomsLoading = true;
    this.roomsService.freeRooms(typologyChange.residence_id, typologyChange.application.id).pipe(
      first(),
      finalize(() => this.roomsLoading = false)
    ).subscribe(result => {
      this.rooms = result.data;
    });
  }
  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }


  handleChangeStatus() {

    if (this.changeStatusModalAction === 'analyse') {

      this.changeStatusModalLoading = true;
      this.roomsChangeRequestsService.status(this.changeStatusModalResidenceChangeSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomsChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      if (!this.newRoomId && this.changeStatusModalDecision == "APPROVE") {
        this.uiService.showMessage(MessageType.info, "Seleção de novo quarto obrigatória")
        return;
      }
      this.changeStatusModalLoading = true;
      this.roomsChangeRequestsService.status(this.changeStatusModalResidenceChangeSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision, room_id: this.newRoomId }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomsChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.roomsChangeRequestsService.admin_approve(this.changeStatusModalResidenceChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomsChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.roomsChangeRequestsService.admin_reject(this.changeStatusModalResidenceChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomsChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  handleChangeStatusCancel() {
    this.changeStatusModalResidenceChangeSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
    this.newRoomId = null;
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
