import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListResidenceChangeRequestsComponent } from './pages/list-residence-change-requests/list-residence-change-requests.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListResidenceChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidenceChangeRequestsRoutingModule { }
