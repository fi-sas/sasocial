import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidenceChangeRequestsRoutingModule } from './residence-change-requests-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListResidenceChangeRequestsComponent } from './pages/list-residence-change-requests/list-residence-change-requests.component';
import { ViewResidenceChangeRequestComponent } from './components/view-residence-change-request/view-residence-change-request.component';

@NgModule({
  declarations: [ListResidenceChangeRequestsComponent, ViewResidenceChangeRequestComponent],
  imports: [
    CommonModule,
    ResidenceChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class ResidenceChangeRequestsModule { }
