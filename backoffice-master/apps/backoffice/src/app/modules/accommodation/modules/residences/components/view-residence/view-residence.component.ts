import { Component, OnInit, Input } from '@angular/core';
import { ResidenceModel } from '../../models/residence.model';

@Component({
  selector: 'fi-sas-view-residence',
  templateUrl: './view-residence.component.html',
  styleUrls: ['./view-residence.component.less']
})
export class ViewResidenceComponent implements OnInit {

  @Input() data: ResidenceModel;

  constructor() { }

  ngOnInit() {
  }

}
