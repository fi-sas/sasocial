export class ResidenceOccupationModel {
  maximumcapacity: number;
  totalOccupied: number;
  totalAvailable: number;
  occupationRate: number;
}
