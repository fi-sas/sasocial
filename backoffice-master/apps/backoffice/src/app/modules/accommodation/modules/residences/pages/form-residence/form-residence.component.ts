import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { ServiceModel } from './../../../services/models/service.model';
import { TypologyModel } from './../../../typologies/models/typology.model';

import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ResidencesService } from '../../../residences/services/residences.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { TypologiesService } from '../../../typologies/services/typologies.service';
import { ServicesService } from '../../../services/services/services.service';
import { NzModalService, UploadFile, UploadXHRArgs } from 'ng-zorro-antd';
import { first, finalize, debounceTime, switchMap, map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { RegimeModel } from '../../../regimes/models/regime.model';
import { RegimesService } from '../../../regimes/services/regimes.service';
import { ExtraModel } from '../../../extras/models/extra.model';
import { ExtrasService } from '../../../extras/services/extras.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-form-residence',
  templateUrl: './form-residence.component.html',
  styleUrls: ['./form-residence.component.less']
})
export class FormResidenceComponent implements OnInit {
  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  usersSearchChange$ = new BehaviorSubject('');
  users: UserModel[] = [];
  selectedUser: UserModel;
  isLoadingUsers = false;

  typologies: TypologyModel[] = [];
  typologiesLoading = false;

  services: ServiceModel[] = [];
  servicesLoading = false;

  buildings: BuildingModel[] = [];
  buildingsLoading = false;

  regimes: RegimeModel[] = [];
  regimeLoading = false;

  extras: ExtraModel[] = [];
  extrasLoading = false;

  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;

  idToUpdate = null;
  loading = false;

  residenceGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    address: new FormControl('', [Validators.required, trimValidation]),
    city: new FormControl('', [Validators.required, trimValidation]),
    postal_code: new FormControl('', [Validators.required, Validators.pattern('\\d{4}-\\d{3}$')]),
    phone_1: new FormControl('', [Validators.required, trimValidation]),
    phone_2: new FormControl('', [Validators.required, trimValidation]),
    email: new FormControl('', [Validators.required, trimValidation, Validators.pattern(this.emailRegex)]),
    building_id: new FormControl(null, Validators.required),
    typology_ids: new FormControl([]),
    service_ids: new FormControl([]),
    payment_method_ids: new FormControl([]),
    residence_responsible_ids: new FormControl([]),
    current_account_id: new FormControl([], Validators.required),
    wing_responsible_ids: new FormControl([]),
    kitchen_responsible_ids: new FormControl([]),
    regulation_file_id: new FormControl(null, Validators.required),
    regime_ids: new FormControl(null, Validators.required),
    extra_ids: new FormControl([]),
    contract_file_id: new FormControl(null, Validators.required),
    media_ids: new FormControl([]),
    charge_deposit: new FormControl(true, Validators.required),
    available_for_assign: new FormControl(true, Validators.required),
    available_for_application: new FormControl(true, Validators.required),
    visible_for_users: new FormControl(true, Validators.required),
    active: new FormControl(true, Validators.required)
  });

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private uiService: UiService,
    private filesService: FilesService,
    public residencesService: ResidencesService,
    public typologysService: TypologiesService,
    private usersService: UsersService,
    public servicesService: ServicesService,
    public buildingsService: BuildingService,
    private regimesService: RegimesService,
    private extrasService: ExtrasService,
    private currentAccountsService: CurrentAccountsService
  ) {

  }

  ngOnInit() {
    this.loadRegimes();
    this.loadExtras();
    this.loadBuildings();
    this.loadServices();
    this.loadTypologies();
    this.loadCurrentAccounts();

    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.loading = true;
        this.residencesService.read(this.idToUpdate, {
          withRelated: 'mediaIds,kitchenResponsibleIds,wingResponsibleIds,residenceResponsibleIds,typologies,services,extras,regimes'
        }).pipe(
          first(),
          finalize(() => this.loading = false)
        ).subscribe(result => {
          this.residenceGroup.patchValue({
            name: result.data[0].name,
            address: result.data[0].address,
            city: result.data[0].city,
            postal_code: result.data[0].postal_code,
            email: result.data[0].email,
            phone_1: result.data[0].phone_1,
            phone_2: result.data[0].phone_2,
            building_id: result.data[0].building_id,
            regulation_file_id: result.data[0].regulation_file_id,
            contract_file_id: result.data[0].contract_file_id,
            active: result.data[0].active,
            charge_deposit: result.data[0].charge_deposit,
            available_for_application: result.data[0].available_for_application,
            available_for_assign: result.data[0].available_for_assign,
            visible_for_users: result.data[0].visible_for_users,
            typology_ids: result.data[0].typologies.map(t => t.id),
            service_ids: result.data[0].services.map(s => s.id),
            regime_ids: result.data[0].regimes.map(t => t.regime_id),
            extra_ids: result.data[0].extras.map(t => t.extra_id),
            media_ids: result.data[0].mediaIds.map(m => m.id),
            current_account_id: result.data[0].current_account_id
          });

          if(result.data[0].residenceResponsibleIds){
            let usersIds =  result.data[0].residenceResponsibleIds.map(u => u.id);
            this.getLoadUpdateUsers(usersIds);
            this.residenceGroup.get('residence_responsible_ids').setValue(usersIds);
          }

          if(result.data[0].wingResponsibleIds){
            let usersIds =  result.data[0].wingResponsibleIds.map(u => u.id);
            this.getLoadUpdateUsers(usersIds);
            this.residenceGroup.get('wing_responsible_ids').setValue(usersIds);
          }

          if(result.data[0].kitchenResponsibleIds){
            let usersIds =  result.data[0].kitchenResponsibleIds.map(u => u.id);
            this.getLoadUpdateUsers(usersIds);
            this.residenceGroup.get('kitchen_responsible_ids').setValue(usersIds);
          }


          if (result.data[0].mediaIds) {
            result.data[0].mediaIds.map<FileModel>(m => {
              this.fileList.push({
                uid: m.id,
                name: m.filename,
                status: 'done',
                url: m.url
              });
              return m;
            });
            this.fileList = [...this.fileList];
          }
        });
      }
    });

    // tslint:disable-next-line:no-any
    const loadUsers = (search: string) =>
      this.usersService.list(1, 20, null, null, {
        withRelated: false,
        search,
        searchFields: 'name,user_name,student_number,email,identification'
      }).pipe(first(), map((results) => results.data));


    const optionUsers$: Observable<UserModel[]> = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe(users => {
      this.users = users;
      this.isLoadingUsers = false;
    });
  }
  currentAccountsLoading = false;
  currentAccounts: any[] = [];
  loadCurrentAccounts() {
    this.currentAccountsLoading = true;
    this.currentAccountsService.list(0, -1).pipe(
      first(),
      finalize(() => this.currentAccountsLoading = false)).subscribe(results => {
        this.currentAccounts = results.data;
      });
  }

  loadTypologies() {
    this.typologiesLoading = true;
    this.typologysService.list(1, -1).pipe(
      first(),
      finalize(() => this.typologiesLoading = false)).subscribe(results => {
        this.typologies = results.data;
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService.list(1, -1).pipe(
      first(),
      finalize(() => this.servicesLoading = false)).subscribe(results => {
        this.services = results.data;
      });
  }

  loadBuildings() {
    this.buildingsLoading = true;
    this.buildingsService.list(1, -1, null, null, {
      sort: 'name'
    }).pipe(
      first(),
      finalize(() => this.buildingsLoading = false)).subscribe(results => {
        this.buildings = results.data;
      });
  }

  loadRegimes() {
    this.regimeLoading = true;
    this.regimesService.list(1, -1).pipe(
      first(),
      finalize(() => this.regimeLoading = false)).subscribe(results => {
        this.regimes = results.data;
      });
  }

  loadExtras() {
    this.extrasLoading = true;
    this.extrasService.list(1, -1).pipe(
      first(),
      finalize(() => this.extrasLoading = false)).subscribe(results => {
        this.extras = results.data;
      });
  }

  submitForm(value: any) {

    for (const i in this.residenceGroup.controls) {
      if (i) {
        this.residenceGroup.controls[i].markAsDirty();
        this.residenceGroup.controls[i].updateValueAndValidity();
      }
    }

    if (this.residenceGroup.valid) {
      this.loading = true;
      value.visible_for_users = value.available_for_application ? true : value.visible_for_users;
      if (this.idToUpdate) {
        this.residencesService.update(this.idToUpdate, value).pipe(
          first(),
          finalize(() => this.loading = false)
        ).subscribe(results => {
          this.router.navigate(['accommodation', 'residences', 'list']);
          this.uiService.showMessage(MessageType.success, 'Residência alterada com sucesso');
        });
      } else {
        this.residencesService.create(value).pipe(
          first(),
          finalize(() => this.loading = false)
        ).subscribe(results => {
          this.router.navigate(['accommodation', 'residences', 'list']);
          this.uiService.showMessage(MessageType.success, 'Residência criada com sucesso');
        });
      }
    }
  }

  removeImage = (image: any): Observable<boolean> => {
    return Observable.create(subscriver => {
      this.uiService.showConfirm('Remover imagem', 'Pretende remover a imagem', 'Remover', 'Cancelar')
        .pipe(first()).subscribe(confirm => {
          if (confirm) {
            const index = this.fileList.indexOf(image);
            if (index > -1) {
              this.filesService.delete(this.fileList[index].uid).pipe(first()).subscribe(result => {
                this.fileList = [...this.fileList];
                this.residenceGroup.patchValue({
                  media_ids: this.fileList.map(fl => fl.uid)
                });
                this.uiService.showMessage(MessageType.success, 'Imagem eliminada com sucesso');
                subscriver.next(true);
                subscriver.complete();
              });
            } else {
              subscriver.next(false);
              subscriver.complete();
            }
          } else {
            subscriver.next(false);
            subscriver.complete();
          }
        });
    });
  }

  uploadFile = (item: UploadXHRArgs) => {
    const formData = new FormData();
    formData.append('name', item.file.name);
    formData.append('weight', '1');
    formData.append('public', 'true');
    formData.append('file_category_id', '1');
    formData.append('language_id', '3');
    formData.append('file', item.file as any, item.file.name);

    const req = this.filesService.createFile(formData);
    return req.subscribe(
      // tslint:disable-next-line no-any
      (event: HttpEvent<any>) => {

        if (event.type === HttpEventType.UploadProgress) {
          if (event.total && event.total > 0) {
            // tslint:disable-next-line:no-any
            (event as any).percent = (event.loaded / event.total) * 100;
          }
          item.onProgress(event, item.file);
        } else if (event instanceof HttpResponse) {
          const file = event.body.data[0];

          item.onSuccess({
            uid: file.id,
            name: file.filename,
            status: 'done',
            url: file.url
          }, item.file, event);

          this.fileList.pop();
          this.fileList.push({
            uid: file.id,
            name: file.filename,
            status: 'done',
            url: file.url
          });
          this.fileList = [...this.fileList];
          this.residenceGroup.patchValue({
            media_ids: this.fileList.map(fl => fl.uid)
          });

          this.uiService.showMessage(
            MessageType.success,
            'Imagem carregada com sucesso'
          );
        }
      },
      err => {
        item.onError(err, item.file);
        this.uiService.showMessage(
          MessageType.error,
          'Ocorreu um erro durente o upload da imagem'
        );
      });
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };

  onSearchUsers(event) {
    this.isLoadingUsers = true;
    this.usersSearchChange$.next(event);
  }

  getLoadUpdateUsers(users_ids) {
    this.isLoadingUsers = true;
    this.usersService
      .list(0, 100, null, null, {
        id: users_ids,
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.isLoadingUsers = false))
      )
      .subscribe((res) => {
        this.users.push(...res.data);
      });
  }

  backList() {
    this.router.navigate(['accommodation', 'residences', 'list']);
  }
}
