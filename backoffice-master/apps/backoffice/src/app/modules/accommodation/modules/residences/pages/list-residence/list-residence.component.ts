import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { ResidencesService } from '../../services/residences.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'fi-sas-list-residence',
  templateUrl: './list-residence.component.html',
  styleUrls: ['./list-residence.component.less']
})
export class ListResidenceComponent extends TableHelper implements OnInit {
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public residencesService: ResidencesService) {
    super(uiService, router, activatedRoute);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.persistentFilters = {
      searchFields: 'name',
    };
  }

  ngOnInit() {
    this.persistentFilters['withRelated'] = 'regulationFile,contractFile,extras,regimes,current_account';
    this.initTableData(this.residencesService);

  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  deleteResidence(id: number) {
    this.uiService.showConfirm('Eliminar residência',
      'Pretende eliminar a residência?',
      'Eliminar',
      'Cancelar')
      .pipe(first()).subscribe(confirm => {
        if (confirm) {
          this.residencesService.delete(id).pipe(first()).subscribe(result => {
            this.uiService.showMessage(MessageType.success, 'Residência eliminada com successo');
            this.searchData();
          });
        }
      });
  }

}
