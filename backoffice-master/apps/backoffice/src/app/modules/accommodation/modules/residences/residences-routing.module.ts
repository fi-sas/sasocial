import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormResidenceComponent } from './pages/form-residence/form-residence.component';
import { ListResidenceComponent } from './pages/list-residence/list-residence.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormResidenceComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:regimes:create' }
  },
  {
    path: 'edit/:id',
    component: FormResidenceComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:regimes:create' }
  },
  {
    path: 'list',
    component: ListResidenceComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:regimes:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidencesRoutingModule { }
