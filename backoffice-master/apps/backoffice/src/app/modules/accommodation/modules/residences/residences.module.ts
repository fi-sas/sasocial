import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidencesRoutingModule } from './residences-routing.module';
import { FormResidenceComponent } from './pages/form-residence/form-residence.component';
import { ListResidenceComponent } from './pages/list-residence/list-residence.component';
import { ViewResidenceComponent } from './components/view-residence/view-residence.component';
@NgModule({
  declarations: [
    FormResidenceComponent,
    ListResidenceComponent,
    ViewResidenceComponent,
  ],
  imports: [
    CommonModule,
    ResidencesRoutingModule,
    SharedModule
  ]
})
export class ResidencesModule { }
