import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ResidenceOccupationModel } from '../models/residence-occupation.model';
import { ResidenceModel } from '../models/residence.model';

@Injectable({
  providedIn: 'root'
})
export class ResidencesService extends Repository<ResidenceModel> {

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.RESIDENCES';
    this.entity_url = 'ACCOMMODATION.RESIDENCES_ID';
  }


  /*occupation(residence_id: number): Observable<Resource<ResidenceOccupationModel>> {
    return this.resourceService.list(this.urlService.get('ACCOMMODATION.RESIDENCES_OCCUPATION_ID',
      {
        id: residence_id,
      }));
  }*/

  occupation(residence_id: number, academic_year: string): Observable<Resource<ResidenceOccupationModel>> {
    let params = new HttpParams();
    params = params.append('sort', '-name');
    if (residence_id) {
      params = params.append('id', residence_id.toString());
    }
    if(academic_year){
      params = params.append('academic_year', academic_year.toString());
    }
    const url = this.urlService.get('ACCOMMODATION.RESIDENCES_OCCUPATION');
    return this.resourceService.read<any>(url, {
      params
    }).pipe(first());
  }

  typologiesMap(residence_id: number, academic_year: string): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.append('sort', '-name');
    if (residence_id) {
      params = params.append('residence_id', residence_id.toString());
    }
    if(academic_year){
      params = params.append('academic_year', academic_year.toString());
    }
    const url = this.urlService.get('ACCOMMODATION.RESIDENCES_TYPOLOGY_MAP');
    return this.resourceService.list<any>(url, {
      params
    }).pipe(first());
  }

  listAvailableForApplication(withRelated: string = 'regulationFile,mediaIds,extras,regimes'): Observable<Resource<ResidenceModel>> {
    let params = new HttpParams();
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('active', 'true');
    params = params.set('query[available_for_application]', 'true');

    return this.resourceService.list<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES'), { params });
  }

  
  listAvailableForAssign(withRelated: string = 'regulationFile,mediaIds,extras,regimes'): Observable<Resource<ResidenceModel>> {
    let params = new HttpParams();
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    params = params.set('active', 'true');
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[available_for_assign]', 'true');

    return this.resourceService.list<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES'), { params });
  }
}
