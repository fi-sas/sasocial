import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRoomChangeRequestComponent } from './view-room-change-request.component';

describe('ViewRoomChangeRequestComponent', () => {
  let component: ViewRoomChangeRequestComponent;
  let fixture: ComponentFixture<ViewRoomChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRoomChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRoomChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
