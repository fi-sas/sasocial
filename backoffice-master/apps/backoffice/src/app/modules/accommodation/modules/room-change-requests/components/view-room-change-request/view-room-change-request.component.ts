import { Component, Input, OnInit } from '@angular/core';
import { RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';

@Component({
  selector: 'fi-sas-view-room-change-request',
  templateUrl: './view-room-change-request.component.html',
  styleUrls: ['./view-room-change-request.component.less']
})
export class ViewRoomChangeRequestComponent implements OnInit {

  @Input() data: RoomChangeRequestModel = null;
  RoomChangeRequestStatusTranslations = RoomChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
