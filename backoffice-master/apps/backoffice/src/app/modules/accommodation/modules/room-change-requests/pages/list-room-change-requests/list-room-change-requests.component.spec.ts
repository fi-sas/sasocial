import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRoomChangeRequestsComponent } from './list-room-change-requests.component';

describe('ListRoomChangeRequestsComponent', () => {
  let component: ListRoomChangeRequestsComponent;
  let fixture: ComponentFixture<ListRoomChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRoomChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRoomChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
