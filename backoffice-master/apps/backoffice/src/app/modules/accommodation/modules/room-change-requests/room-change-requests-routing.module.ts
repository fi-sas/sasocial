import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRoomChangeRequestsComponent } from './pages/list-room-change-requests/list-room-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListRoomChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomChangeRequestsRoutingModule { }
