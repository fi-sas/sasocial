import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomChangeRequestsRoutingModule } from './room-change-requests-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListRoomChangeRequestsComponent } from './pages/list-room-change-requests/list-room-change-requests.component';
import { ViewRoomChangeRequestComponent } from './components/view-room-change-request/view-room-change-request.component';

@NgModule({
  declarations: [ListRoomChangeRequestsComponent, ViewRoomChangeRequestComponent],
  imports: [
    CommonModule,
    RoomChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class RoomChangeRequestsModule { }
