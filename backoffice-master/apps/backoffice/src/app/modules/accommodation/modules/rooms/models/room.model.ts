import { ApplicationModel } from '../../aco_applications/models/application.model';
import { TypologyModel } from '../../typologies/models/typology.model';
import { ResidenceModel } from '../../residences/models/residence.model';

export class RoomModel {
  id: number;
  name: string;
  room_id: number;
  active: boolean;
  updated_at: Date | string;
  created_at: Date | string;
  allow_booking: boolean;
  residence?: ResidenceModel;
  residence_id: number;
  typology?: TypologyModel;
  typology_id: number;
  occupants: ApplicationModel[];
}
