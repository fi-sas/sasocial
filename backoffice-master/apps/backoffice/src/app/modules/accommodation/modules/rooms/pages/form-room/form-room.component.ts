import { Location } from '@angular/common';
import { hasOwnProperty } from 'tslint/lib/utils';
import { FiConfigurator } from '@fi-sas/configurator';
import { RoomModel } from '@fi-sas/backoffice/modules/infrastructure/models/room.model';
import { Component, OnInit, Input } from '@angular/core';
import { RoomsService as RoomsINFService } from '@fi-sas/backoffice/modules/infrastructure/services/rooms.service';
import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { TypologyModel } from '../../../typologies/models/typology.model';
import { ResidencesService } from '../../../residences/services/residences.service';
import { TypologiesService } from '../../../typologies/services/typologies.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-room',
  templateUrl: './form-room.component.html',
  styleUrls: ['./form-room.component.less']
})
export class FormRoomComponent implements OnInit {
  language_id = null;
  roomsLoad = false;
  buildId: number;
  roomForm = new FormGroup({
    name: new FormControl('', [Validators.required,trimValidation]),
    residence_id: new FormControl(null, [Validators.required]),
    room_id: new FormControl(null, [Validators.required]),
    typology_id: new FormControl(null, [Validators.required]),
    is_booked: new FormControl(false, [Validators.required]),
    allow_booking: new FormControl(false, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
  });
  submit = false;
  residences: ResidenceModel[] = [];
  typologies: TypologyModel[] = [];
  roomsInf: RoomModel[] = [];

  loading = false;
  load_residences = false;
  load_typologies = false;

  idToUpdate = null;
  get f() { return this.roomForm.controls; }
  constructor(
    public roomsService: RoomsService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private configurator: FiConfigurator,
    private residenceService: ResidencesService,
    private roomsINFService: RoomsINFService,
  ) {
    this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
  }

  ngOnInit() {

    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id') ? this.activatedRoute.snapshot.params.id : null;
    if(this.idToUpdate) {
      this.roomsService.read(this.idToUpdate).pipe(first()).subscribe(result => {
        this.roomForm.patchValue({
          name: result.data[0].name,
          residence_id: result.data[0].residence_id,
          room_id: result.data[0].room_id,
          typology_id: result.data[0].typology_id,
          allow_booking: result.data[0].allow_booking,
          active: result.data[0].active,
        });
        this.getResidence(result.data[0].residence_id, result.data[0].room_id)
      });
    }

   this.loadResidences();
   //this.loadTypologies();
  }


  submitForm() {
    this.submit = true;
    if(this.roomForm.valid) {
      this.submit = false;
      this.loading = true;
      if(this.idToUpdate) {
        this.roomsService.update(this.idToUpdate, this.roomForm.value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Quarto atualizado com sucesso.');
          this.location.back();
        });
      } else {
        this.roomsService.create(this.roomForm.value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Quarto inserido com sucesso.');
          this.router.navigate(['accommodation', 'rooms', 'list']);
        });
      }
    }

  }


  loadResidences() {
    this.load_residences = true;
    this.residenceService.list(1, -1, null, null, {
      withRelated: false,
      sort: 'name'
    }).pipe(first(), finalize(() => this.load_residences = false)).subscribe(data => {
      this.residences = data.data;
    });
  }

  getResidence(id, idRoom) {
    this.roomsLoad = true;

    this.residenceService.read(id).pipe(first(), finalize(() => this.load_residences = false)).subscribe(data => {
      this.buildId = data.data[0].building_id;
      if (this.buildId) {
        this.typologies = data.data[0].typologies.length ? data.data[0].typologies : [] ;
        if(!this.typologies.filter(t => t.id === this.roomForm.value.typology_id).length){
          this.roomForm.get('typology_id').setValue(null);
        }

        this.roomForm.get('room_id').setValue(null);
        this.roomsINFService
          .listRoomsBuild(this.buildId)
          .pipe(first())
          .subscribe(rooms => {
            this.roomsInf = rooms.data;
            if(idRoom) {
              if (this.roomsInf.find(x => x.id === idRoom)) {
                this.roomForm.get('room_id').setValue(idRoom);
                this.roomsLoad = false;
              }
            }else {
              this.roomsLoad = false;
            }

          }, (error)=> {
            this.roomsLoad = false;
          });
      }
    }, (error)=> {
      this.roomsLoad = false;
    });
  }

  /*loadTypologies() {
    this.load_typologies = true;
    this.typologiesService.list(1, -1).pipe(first(), finalize(() => this.load_typologies = false)).subscribe(data => {
      this.typologies = data.data;
    });
  }*/

  backList() {
    this.router.navigate(['accommodation', 'rooms', 'list']);
  }
}
