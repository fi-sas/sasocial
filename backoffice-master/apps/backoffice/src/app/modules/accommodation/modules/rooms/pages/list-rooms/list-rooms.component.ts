import { FiConfigurator } from '@fi-sas/configurator';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { TypologyModel } from '../../../typologies/models/typology.model';
import { TypologiesService } from '../../../typologies/services/typologies.service';
import { TagComponent } from './../../../../../../shared/components/tag/tag.component';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Component, OnInit } from '@angular/core';
import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import { ResidencesService } from '../../../residences/services/residences.service';
import { CustomControl, CustomControlOption } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first, finalize } from 'rxjs/operators';


@Component({
  selector: 'fi-sas-list-rooms',
  templateUrl: './list-rooms.component.html',
  styleUrls: ['./list-rooms.component.less']
})
export class ListRoomsComponent extends TableHelper implements OnInit {
  status = [];
  language_id: number = 3;
  residences: ResidenceModel[] = [];
  typologies: TypologyModel[] = [];
  load_residences = false;
  load_typologies = false;

  constructor(public roomsService: RoomsService,
    private residencesService: ResidencesService,
    private configurator: FiConfigurator,
    uiService: UiService,
    router: Router,
    private residenceService: ResidencesService,
    private typologiesService: TypologiesService,
    activatedRoute: ActivatedRoute) { 
      super(uiService, router, activatedRoute);
      this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
      this.loadResidences();
      this.loadTypologies();
      this.filters.searchFields = "name";
      this.status = [
        {
          description: "Ativo",
          value: true
        },
        {
          description: "Desativo",
          value: false
        }
      ];
      this.columns.push(
        {
          label: 'Nome',
          key: 'name',
          sortable: true
        },
        {
          label: 'Tipologia',
          sortKey: 'typology_id',
          key: 'typology',
          template: (data) => {
            return data.typology.translations.find(t => t.language_id === 3).name
          },
          sortable: true
        },
        {
          label: 'Residência',
          key: 'residence.name',
          sortKey: 'residence_id',
          sortable: true
        },
        {
          label: 'Permite reservas',
          key: 'allow_booking',
          tag: TagComponent.YesNoTag,
          sortable: true
        },
        {
          label: 'Ativo',
          key: 'active',
          tag: TagComponent.YesNoTag,
          sortable: true
        }
      );
    }

  ngOnInit() {
    this.initTableData(this.roomsService);
  }

  deleteRoom(id: number) {
    this.uiService.showConfirm('Eliminar quarto', 'Pretende eliminar este quarto ?', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.roomsService.delete(id).pipe(first()).subscribe(res => {
          this.uiService.showMessage(MessageType.success, "Quarto eliminado com sucesso");
          this.searchData();
        });
      }
    });
  }


  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.filters.residence_id = null;
    this.filters.typology_id = null;
    this.searchData(true)
  }


  loadResidences() {
    this.load_residences = true;
    this.residenceService.list(1, -1, null, null, {
      withRelated: false,
      sort: 'name'
    }).pipe(first(), finalize(() => this.load_residences = false)).subscribe(data => {
      this.residences = data.data;
    });
  }

  loadTypologies() {
    this.load_typologies = true;
    this.typologiesService.list(1, -1).pipe(first(), finalize(() => this.load_typologies = false)).subscribe(data => {
      this.typologies = data.data;
    });
  }

}
