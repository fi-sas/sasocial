import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OccupationMapModel } from '@fi-sas/backoffice/modules/accommodation/models/occupation.model';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexYAxis,
  ApexLegend,
  ApexFill,
  ApexMarkers,
  ApexTitleSubtitle,
  ApexTooltip
} from "ng-apexcharts";

import * as moment from 'moment';
import { first, finalize } from 'rxjs/operators';
import { ResidencesService } from '../../../residences/services/residences.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  yaxis: ApexYAxis;
  legend: ApexLegend;
  fill: ApexFill;
  markers: ApexMarkers;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
};

@Component({
  selector: 'fi-sas-occupation-map',
  templateUrl: './occupation-map.component.html',
  styleUrls: ['./occupation-map.component.less']
})
export class OccupationMapComponent implements OnInit, OnDestroy {
  @ViewChild("chart", { static: true }) cookieMsg: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  totalSubmitted = 0;
  totalAnalyzed = 0;
  totalPending = 0;
  totalQueued = 0;
  totalConfirmed = 0;
  selectedResidence: number;
  occupationMap: OccupationMapModel = null;
  residence_loading = false;
  residences: ResidenceModel[] = [];

  dates: Date[] = [];
  academicYear = null;

  constructor(
    private uiService: UiService,
    private roomsService: RoomsService,
    private residenceService: ResidencesService,
    private accommodationService: AccommodationService,
    private academicYearService: AcademicYearsService) {
  }

  ngOnInit() {
    const data = this.accommodationService.getAccommodationData();
    if(!data.academicYear){
      this.academicYearService.getCurrentAcademicYear().pipe(first()).subscribe(response => {
        if(response.data.length ){
          this.academicYear = response.data[0].academic_year;
        }
      });
    }else{
      this.academicYear = data.academicYear;
    }

    this.uiService.setContentWrapperActive(false);
    this.dates[0] = moment().subtract(30, 'days').toDate();
    this.dates[1] = new Date();
    this.loadResidences();
    /*this.chartOptions = {
      series: [
        {
          name: "Alojados",
          data: this.totalSubmitted
        },
        {
          name: "Ausências",
          data: this.totalAnalyzed
        },
        {
          name: "Reservas",
          data: this.totalPending
        },
        {
          name: "Desistências",
          data: this.totalQueued
        },
        {
          name: "Quartos",
          data: this.totalConfirmed
        }
      ],
      chart: {
        type: "area",
        stacked: false,
        height: 350,
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      markers: {
        size: 0
      },
      fill: {
        type: "gradient",
        gradient: {
          shadeIntensity: 1,
          inverseColors: false,
          opacityFrom: 0.45,
          opacityTo: 0.05,
          stops: [20, 100, 100, 100]
        }
      },
      yaxis: {
        labels: {
          style: {
            color: "#8e8da4"
          },
          offsetX: 0,
          formatter: function(val) {
            return (val / 1000000).toFixed(2);
          }
        },
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        }
      },
      xaxis: {
        type: "datetime",
        tickAmount: 8,
        min: new Date("01/01/2014").getTime(),
        max: new Date("01/20/2014").getTime(),
        labels: {
          rotate: -15,
          rotateAlways: true,
          formatter: function(val, timestamp) {
            return moment(new Date(timestamp)).format("DD MMM YYYY");
          }
        }
      },
      title: {
        text: "Mapa Ocupação",
        align: "left",
        offsetX: 14
      },
      tooltip: {
        shared: true
      },
      legend: {
        position: "top",
        horizontalAlign: "right",
        offsetX: -10
      }
    };*/
  }

  ngOnDestroy() {
    this.uiService.setContentWrapperActive(true);
  }

  loadResidences() {
    this.residence_loading = true;
    this.residenceService.list(1, -1, null, null, {
      withRelated: false
    })
      .pipe(first(), finalize(() => this.residence_loading = false))
      .subscribe(resis => {
        this.residences = resis.data;
        if (this.residences.length > 0) {
          this.selectedResidence = this.residences[0].id;
          this.loadData();
        }
      });
  }

  onResidenceChange(id: number) {
    this.loadData();
  }

  // onIntervalChange(event: Date[]) {
  //   this.loadData();
  // }

  drawChartMonthAllocated(data: any) {

  }

  loadData() {
    this.roomsService.occupationMap(this.selectedResidence, this.academicYear).pipe(first()).subscribe(value => {
      this.totalSubmitted = value.data[0].applications;
      this.totalAnalyzed = value.data[0].absences;
      this.totalPending = value.data[0].bookings;
      this.totalQueued = value.data[0].withdrawals;
      this.totalConfirmed = value.data[0].rooms;
    });
  }

  reduceXAxis(tick: any) {
    return moment(tick).format('ll')
  }

  occupationRate() {
    if (this.occupationMap) {
      if (this.occupationMap.occupation.length > 0) {
        return this.occupationMap.occupation[0].occupation_rate;
      }
    }
    return 0;
  }

  numberOfRooms() {
    /*
       if (this.occupationMap) {
       this.cardsNumberData =  [
          {
            value: this.occupationMap.occupation[0].applications,
            name: 'Alojados'
          },
          {
            value: this.occupationMap.occupation[0].absences,
            name: 'Ausências'
          },
          {
            value: this.occupationMap.occupation[0].bookings,
            name: 'Reservas'
          },
          {
            value: this.occupationMap.occupation[0].withdawals,
            name: 'Desistências'
          },
          {
            value: this.occupationMap.total_rooms,
            name: 'Total Quartos'
          },
        ]
       } else {
         this.cardsNumberData = [];
       }
       */
  }
}
