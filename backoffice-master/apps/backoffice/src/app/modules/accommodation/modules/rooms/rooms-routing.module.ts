import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { ResidenceGuard } from "../../guards/residence.guard";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { FormRoomComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/form-room/form-room.component';
import { ListRoomsComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/list-rooms/list-rooms.component';
import { OccupationMapComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/occupation-map/occupation-map.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' }, 
  {
    path: 'create',
    component: FormRoomComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:rooms:create' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'edit/:id',
    component: FormRoomComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:rooms:create' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'list',
    component: ListRoomsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:rooms:read' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: 'occupation-map',
    component: OccupationMapComponent,
    data: { breadcrumb: 'Mapa de Ocupações', title: 'Mapa de Ocupações', scope: 'accommodation:occupation-map' },
    canActivate: [ ResidenceGuard]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    data: {
      breadcrumb: 'Página não encontrada',
      title: 'Página não encontrada'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomsRoutingModule { }
