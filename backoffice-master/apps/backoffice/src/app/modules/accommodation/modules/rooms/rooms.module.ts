import { RoomsService } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/services/rooms.service';
import { SharedModule } from './../../../../shared/shared.module';
import { OccupationMapComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/occupation-map/occupation-map.component';
import { ListRoomsComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/list-rooms/list-rooms.component';
import { FormRoomComponent } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/pages/form-room/form-room.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsRoutingModule } from './rooms-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    FormRoomComponent,
    ListRoomsComponent,
    OccupationMapComponent
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    RoomsRoutingModule,
    SharedModule,
  ],
  providers: [
    RoomsService
  ]
})
export class RoomsModule { }
