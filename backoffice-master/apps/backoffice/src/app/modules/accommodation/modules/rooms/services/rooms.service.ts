import { RoomMapModel } from './../models/room-map.model';
import { OccupationMapModel } from '@fi-sas/backoffice/modules/accommodation/models/occupation.model';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { RoomModel } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/models/room.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class RoomsService extends Repository<RoomModel> {

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
    this.entity_url = 'ACCOMMODATION.ROOMS_ID';
    this.entities_url = 'ACCOMMODATION.ROOMS';
  }


  freeRooms(residence_id: number, application_id): Observable<Resource<RoomModel>> {
    let params = new HttpParams();
    params = params.append('application_id', application_id);
    if (residence_id) {
      params = params.append('residence_id', residence_id.toString());
    }
    return this.resourceService.list(this.urlService.get('ACCOMMODATION.FREE_ROOMS_RESIDENCE_ID',
      {
        id: residence_id
      }
    ), {
      params
    });
  }


  occupiedRooms(residence_id: number): Observable<Resource<RoomModel>> {
    return this.resourceService.list(this.urlService.get('ACCOMMODATION.OCCUPIED_ROOMS_RESIDENCE_ID',
      {
        id: residence_id,
      }));
  }

  /*roomsMap(residence_id: number): Observable<Resource<RoomMapModel>> {
    let params = new HttpParams();
    params = params.append('sort', '-name');
    return this.resourceService.list(this.urlService.get('ACCOMMODATION.ROOMS_MAP_RESIDENCE_ID',
      {
        id: residence_id,
      }), {
      params
    });
  }*/

  roomsMap(residence_id: number, academic_year: string): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.append('sort', '-name');
    if (residence_id) {
      params = params.append('residence_id', residence_id.toString());
    }
    if(academic_year){
      params = params.append('academic_year', academic_year.toString());
    }
    const url = this.urlService.get('ACCOMMODATION.ROOMS_MAP_RESIDENCE');
    return this.resourceService.list<any>(url, {
      params
    }).pipe(first());
  }


  occupationMap(residence_id: number, academic_year: string): Observable<Resource<OccupationMapModel>> {
    let params = new HttpParams();
    if(academic_year){
      params = params.append('academic_year', academic_year.toString());
    }

    const url = this.urlService.get('ACCOMMODATION.RESIDENCES_OCCUPATION_MAP', { id: residence_id });
    return this.resourceService.list<any>(url, {
      params
    }).pipe(first());
  }

  getOccupants(roomId: number, academic_year: string){
    let params = new HttpParams();
    params = params.set('withRelated', 'occupants');
    if(academic_year){
      params = params.append('academic_year', academic_year.toString());
    }
    const url = this.urlService.get('ACCOMMODATION.ROOMS_ID', { id: roomId });
    return this.resourceService.read<any>(url, {
      params
    }).pipe(first());
  }

}
