
export class ServiceTranslation {
  name: string;
  language_id: number;
}

export class ServiceModel {
  id: number;
  translations: ServiceTranslation[];
  active: boolean;
}
