import { LanguagesService } from './../../../../../configurations/services/languages.service';
import { MessageType, UiService } from './../../../../../../core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { LanguageModel } from './../../../../../configurations/models/language.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';

@Component({
  selector: 'fi-sas-form-service',
  templateUrl: './form-service.component.html',
  styleUrls: ['./form-service.component.less']
})
export class FormServiceComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  idToUpdate = null;
  submit = false;
  loadingLanguages = false;
  languages: LanguageModel[] = [];
  loading = false;
  serviceGroup = new FormGroup({
    translations: new FormArray([]),
    active: new FormControl(true, Validators.required),
  });
  translations = this.serviceGroup.get('translations') as FormArray;

  constructor(
    private languagesService: LanguagesService,
    private uiService: UiService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public servicesService: ServicesService) { }

  ngOnInit() {
    this.loadLanguages();
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.serviceGroup.controls.translations as FormArray;
    translations.push(new FormGroup({
      language_id: new FormControl(language_id, Validators.required),
      name: new FormControl(name, [Validators.required,trimValidation]),
    }));
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService.list(1, -1).pipe(first(), finalize(() => this.loadingLanguages = false)).subscribe(results => {
      this.languages = results.data;

      this.loadService();
    });
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  loadService() {
    this.activatedRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.servicesService.read(parseInt(data.get('id'), 10)).subscribe(result => {
          this.serviceGroup.patchValue({
            active: result.data[0].active,
          })

          if (result.data[0].translations) {
            result.data[0].translations.map(t => {
              this.addTranslation(t.language_id, t.name);
            });
          }

          this.languages.map(l => {
            const foundLnaguage = this.translations.value.find(t => t.language_id === l.id);
            if (!foundLnaguage) {
              this.addTranslation(l.id, '');
            }
          });
        });
      } else {
        this.languages.map(l => {
          this.addTranslation(l.id, '');
        });
      }
    });
  }

  submitForm() {
    let translationsError = false;
    this.submit = true;
    let tabIndex = 0;
    
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          translationsError = true;
          break;
        }
      }
      tabIndex++;
    }


    if (this.serviceGroup.valid && !translationsError) {
      this.loading = true;

      if (this.idToUpdate) {
        this.servicesService.update(this.idToUpdate, this.serviceGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['accommodation', 'services', 'list']);
            this.uiService.showMessage(MessageType.success, 'Serviço alterado com sucesso');
          });
      } else {
        this.servicesService.create(this.serviceGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['accommodation', 'services', 'list']);
            this.uiService.showMessage(MessageType.success, 'Serviço criado com sucesso');
          });
      }
    }

  }

  backList() {
    this.router.navigate(['accommodation', 'services', 'list']);
  }
}
