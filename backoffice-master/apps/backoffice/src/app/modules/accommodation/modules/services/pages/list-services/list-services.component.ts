import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from './../../../../../../core/services/ui-service.service';
import { TableHelper } from './../../../../../../shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';

@Component({
  selector: 'fi-sas-list-services',
  templateUrl: './list-services.component.html',
  styleUrls: ['./list-services.component.less']
})
export class ListServicesComponent extends TableHelper implements OnInit {
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    public servicesService: ServicesService
    ) {
      super(uiService, router, activatedRoute);
      this.status = [
        {
          description: "Ativo",
          value: true
        },
        {
          description: "Desativo",
          value: false
        }
      ];
      this.persistentFilters = {
        withRelated: 'translations',
        searchFields: 'name',
      };
    }

  ngOnInit() {

    this.initTableData(this.servicesService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }
  
  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  deleteService(id: number) {
    this.uiService.showConfirm('Eliminar serviço', 'Pretende eliminar este serviço', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.servicesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Serviço eliminado com sucesso');
        });
      }
    });
  }
}
