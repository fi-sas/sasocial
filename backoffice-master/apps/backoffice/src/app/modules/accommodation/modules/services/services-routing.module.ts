import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListServicesComponent } from './pages/list-services/list-services.component';
import { FormServiceComponent } from './pages/form-service/form-service.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormServiceComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:services:create' }
  },
  {
    path: 'edit/:id',
    component: FormServiceComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:services:create' }
  },
  {
    path: 'list',
    component: ListServicesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:services:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
