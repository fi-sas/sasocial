import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { FormServiceComponent } from './pages/form-service/form-service.component';
import { ListServicesComponent } from './pages/list-services/list-services.component';

@NgModule({
  declarations: [
    FormServiceComponent,
    ListServicesComponent
  ],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    SharedModule
  ]
})
export class ServicesModule { }
