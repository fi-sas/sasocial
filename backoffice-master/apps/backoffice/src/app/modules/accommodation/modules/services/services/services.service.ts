import { ServiceModel } from './../models/service.model';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';


@Injectable({
  providedIn: 'root'
})
export class ServicesService extends Repository<ServiceModel>{

  constructor(resource: FiResourceService, url: FiUrlService) {
    super(resource, url);
    this.entities_url = 'ACCOMMODATION.SERVICES';
    this.entity_url = 'ACCOMMODATION.SERVICES_ID';
  }
}
