import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTariffChangeRequestComponent } from './view-tariff-change-request.component';

describe('ViewTariffChangeRequestComponent', () => {
  let component: ViewTariffChangeRequestComponent;
  let fixture: ComponentFixture<ViewTariffChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTariffChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTariffChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
