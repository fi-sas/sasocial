import { Component, Input, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TariffChangeRequestModel, TariffChangeRequestStatusTranslations } from '../../models/Tariff-change-request.model';

@Component({
  selector: 'fi-sas-view-tariff-change-request',
  templateUrl: './view-tariff-change-request.component.html',
  styleUrls: ['./view-tariff-change-request.component.less']
})
export class ViewTariffChangeRequestComponent implements OnInit {

  @Input() data: TariffChangeRequestModel = null;
  TariffChangeRequestStatusTranslations = TariffChangeRequestStatusTranslations;
  YesNoTag = TagComponent.YesNoTag;

  constructor() { }

  ngOnInit() {
  }

}
