import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
import { TariffModel } from '../../tariffs/models/tariff.model';
import { ExtraModel } from '../../extras/models/extra.model';
import { RegimeModel } from '../../regimes/models/regime.model';


// export const ExtensionDecision = {
//   APPROVE: { label: 'Aprovar', color: 'green' },
//   REJECT: { label: 'Rejeitar', color: 'red' },
// };
export enum TariffChangeRequestStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}
export enum TariffChangeRequestDecision {
  APPROVE = 'APPROVE',
  REJECT = 'REJECT',
}
export const TariffChangeRequestDecisionTranslations = {
  APPROVE: { label: 'Aprovada', color: 'green' },
  REJECT: { label: 'Rejeitada', color: 'red' },
};
export const TariffChangeRequestStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export class TariffChangeRequestHistoryModel {
  id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class TariffChangeRequestModel {
  id: number;
  reason: string;
  application_id: number;
  application: ApplicationModel;
  start_date: Date;
  observations: string;
  decision: TariffChangeRequestDecision;
  status: TariffChangeRequestStatus;
  created_at: Date;
  update_at: Date;
  history: TariffChangeRequestHistoryModel[];
  file_id: number;
  file: FileModel;
  tariff_id: number;
  tariff: TariffModel;
  old_tariff_id: number;
  old_tariff: TariffModel;
  has_scholarship: boolean;

}
