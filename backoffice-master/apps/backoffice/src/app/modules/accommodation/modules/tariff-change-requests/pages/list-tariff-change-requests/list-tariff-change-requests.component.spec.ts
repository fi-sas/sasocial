import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTariffChangeRequestsComponent } from './list-tariff-change-requests.component';

describe('ListTariffChangeRequestsComponent', () => {
  let component: ListTariffChangeRequestsComponent;
  let fixture: ComponentFixture<ListTariffChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTariffChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTariffChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
