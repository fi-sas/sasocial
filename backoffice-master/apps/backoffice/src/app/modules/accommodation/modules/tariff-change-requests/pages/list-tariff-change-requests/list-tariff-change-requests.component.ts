import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { TariffChangeRequestDecisionTranslations, TariffChangeRequestModel, TariffChangeRequestStatusTranslations } from '../../models/Tariff-change-request.model';
import { TariffChangeRequestsService } from '../../services/tariff-change-requests.service';
import * as moment from 'moment';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { finalize, first } from 'rxjs/operators';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '@fi-sas/backoffice/modules/accommodation/modules/tariffs/services/tariffs.service';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';

@Component({
  selector: 'fi-sas-list-tariff-change-requests',
  templateUrl: './list-tariff-change-requests.component.html',
  styleUrls: ['./list-tariff-change-requests.component.less']
})
export class ListTariffChangeRequestsComponent extends TableHelper implements OnInit {

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalTariffChangeSelected: TariffChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";
  newTariffId = null;
  tariffs: TariffModel[] = [];
  tariffsLoading = false;
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private tariffChangeRequestsService: TariffChangeRequestsService,
    private tariffsService: TariffsService,
    private accommodationService: AccommodationService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: 'has_scholarship',
        label: 'Possuí bolsa?',
        sortable: false,
        tag: TagComponent.YesNoTag
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: TariffChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: TariffChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'application,tariff,old_tariff,history,file';
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.tariffChangeRequestsService);
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

  openModalWithdrawal(tariff_change: TariffChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalTariffChangeSelected = tariff_change;
    this.changeStatusModalAction = action;
    this.tariffsLoading = true;
    this.tariffsService.list(0, -1, null, null, { active: true }).pipe(
      first(),
      finalize(() => this.tariffsLoading = false)
    ).subscribe(result => {
      this.tariffs = result.data;
    });
  }
  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {
    if (this.changeStatusModalAction === 'analyse') {
      this.changeStatusModalLoading = true;
      this.tariffChangeRequestsService.status(this.changeStatusModalTariffChangeSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.tariffChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      if (!this.newTariffId && this.changeStatusModalDecision == "APPROVE") {
        this.uiService.showMessage(MessageType.info, "Seleção de novo quarto obrigatória")
        return;
      }
      this.changeStatusModalLoading = true;
      this.tariffChangeRequestsService.status(this.changeStatusModalTariffChangeSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision, tariff_id: this.newTariffId }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.tariffChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.tariffChangeRequestsService.admin_approve(this.changeStatusModalTariffChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.tariffChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.tariffChangeRequestsService.admin_reject(this.changeStatusModalTariffChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.tariffChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }
  }

  handleChangeStatusCancel() {
    this.changeStatusModalTariffChangeSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
    this.newTariffId = null;
  }

}
