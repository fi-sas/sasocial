import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { TariffChangeRequestModel } from '../models/Tariff-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class TariffChangeRequestsService extends Repository<TariffChangeRequestModel> {
  entities_url = 'ACCOMMODATION.TARIFF_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.TARIFF_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, application_tariff_change, notes) {
    return this.resourceService.create<TariffChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.TARIFF_CHANGE_REQUEST_STATUS', { id }), { event, application_tariff_change, notes });
  }

  admin_approve(id: number, application_tariff_change, notes) {
    return this.resourceService.create<TariffChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.TARIFF_CHANGE_REQUEST_APPROVE', { id }), { application_tariff_change, notes });
  }

  admin_reject(id: number, application_tariff_change, notes) {
    return this.resourceService.create<TariffChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.TARIFF_CHANGE_REQUEST_REJECT', { id }), { application_tariff_change, notes });
  }
}