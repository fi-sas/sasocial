import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTariffChangeRequestsComponent } from './pages/list-tariff-change-requests/list-tariff-change-requests.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTariffChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TariffChangeRequestsRoutingModule { }
