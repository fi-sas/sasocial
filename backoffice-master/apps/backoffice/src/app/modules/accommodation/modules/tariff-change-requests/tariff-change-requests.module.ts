import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffChangeRequestsRoutingModule } from './tariff-change-requests-routing.module';
import { ListTariffChangeRequestsComponent } from './pages/list-tariff-change-requests/list-tariff-change-requests.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewTariffChangeRequestComponent } from './components/view-tariff-change-request/view-tariff-change-request.component';

@NgModule({
  declarations: [
    ListTariffChangeRequestsComponent,
    ViewTariffChangeRequestComponent
  ],
  imports: [
    CommonModule,
    TariffChangeRequestsRoutingModule,
    SharedModule
  ]
})
export class TariffChangeRequestsModule { }
