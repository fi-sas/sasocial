import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { TariffsService } from '../../services/tariffs.service';

@Component({
  selector: 'fi-sas-form-tariff',
  templateUrl: './form-tariff.component.html',
  styleUrls: ['./form-tariff.component.less']
})
export class FormTariffComponent implements OnInit {

  idToUpdate = null;
  submit = false;
  tariffGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    active: new FormControl(false, [Validators.required]),
  });
  loading = false;
  get f() { return this.tariffGroup.controls; }

  constructor(
    private uiService: UiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tariffsService: TariffsService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.loading = true;
        this.idToUpdate = data.get('id');
        this.tariffsService.read(parseInt(data.get('id'), 10)).pipe(finalize(() => this.loading = false)).subscribe(result => {
          this.tariffGroup.patchValue({
            name: result.data[0].name,
            active: result.data[0].active,
          });
        })
      }
    });
  }

  submitForm() {
    this.submit = false;
    if (this.tariffGroup.invalid) { 
      this.submit = true;
      return; 
    }
    if (this.idToUpdate) {
      this.tariffsService.update(this.idToUpdate, this.tariffGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)).subscribe(results => {
          this.router.navigate(['accommodation', 'tariffs', 'list']);
          this.uiService.showMessage(MessageType.success, 'Tarifário editado com sucesso');
        });
    } else {
      this.tariffsService.create(this.tariffGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)).subscribe(results => {
          this.router.navigate(['accommodation', 'tariffs', 'list']);
          this.uiService.showMessage(MessageType.success, 'Serviço criado com sucesso');
        });
    }
  }


  backList() {
    this.router.navigate(['accommodation', 'tariffs', 'list']);
  }

}
