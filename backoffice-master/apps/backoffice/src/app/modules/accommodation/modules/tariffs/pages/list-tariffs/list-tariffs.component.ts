import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { finalize, first } from 'rxjs/operators';
import { TariffsService } from '../../services/tariffs.service';

@Component({
  selector: 'fi-sas-list-tariffs',
  templateUrl: './list-tariffs.component.html',
  styleUrls: ['./list-tariffs.component.less']
})
export class ListTariffsComponent extends TableHelper implements OnInit {
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    public tariffsService: TariffsService
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: false
      },
      {
        key: 'active',
        label: 'Activo',
        tag: TagComponent.YesNoTag
      },
    );
  }

  ngOnInit() {
    this.persistentFilters['searchFields'] = 'name';
    this.initTableData(this.tariffsService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  changeActive(id: number, new_value: boolean) {

    this.uiService.showConfirm(
      new_value ? 'Activar tarifário' : 'Desativar comentário',
      new_value ? 'Pretende activar este tarifário' : 'Pretende desativar este tarifário',
      new_value ? 'Activar' : 'Desativar',
      'Cancelar',
      new_value ? 'primary' : 'danger',
    ).pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.tariffsService.patch(id, { active: new_value }).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.searchData(true);
            this.uiService.showMessage(MessageType.success, 'Serviço ativado com sucesso');
          });
      }
    });
  }

}
