import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTariffComponent } from './pages/form-tariff/form-tariff.component';
import { ListTariffsComponent } from './pages/list-tariffs/list-tariffs.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormTariffComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'accommodation:tariffs:create' }
  },
  {
    path: 'edit/:id',
    component: FormTariffComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'accommodation:tariffs:create' }
  },
  {
    path: 'list',
    component: ListTariffsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:tariffs:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TariffsRoutingModule { }
