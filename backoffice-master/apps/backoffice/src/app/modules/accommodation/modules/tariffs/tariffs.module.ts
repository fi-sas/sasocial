import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsRoutingModule } from './tariffs-routing.module';
import { ListTariffsComponent } from './pages/list-tariffs/list-tariffs.component';
import { FormTariffComponent } from './pages/form-tariff/form-tariff.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [ListTariffsComponent, FormTariffComponent],
  imports: [
    CommonModule,
    TariffsRoutingModule,
    SharedModule
  ]
})
export class TariffsModule { }
