import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTypologyComponent } from './view-typology.component';

describe('ViewTypologyComponent', () => {
  let component: ViewTypologyComponent;
  let fixture: ComponentFixture<ViewTypologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTypologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTypologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
