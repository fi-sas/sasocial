import { LanguageModel } from '../../../../../configurations/models/language.model';
import { TypologyModel, TypologyPriceLinePeriod } from '../../models/typology.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-typology',
  templateUrl: './view-typology.component.html',
  styleUrls: ['./view-typology.component.less']
})
export class ViewTypologyComponent implements OnInit {

  @Input() data: TypologyModel = null;
  constructor(
    
  ) { 
    
  }

  ngOnInit() {
   
  }

  getPeriodTranslations(period: TypologyPriceLinePeriod) {
      switch(period) {
        case TypologyPriceLinePeriod.DAY:
          return 'Dia';
          break;
        case TypologyPriceLinePeriod.WEEK:
          return 'Semana';
          break;
        case TypologyPriceLinePeriod.MONTH:
          return 'Mês';
          break;
      }
  }
  

}
