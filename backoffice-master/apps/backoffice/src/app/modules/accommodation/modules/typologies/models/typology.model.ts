import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { TariffModel } from '../../tariffs/models/tariff.model';
import { ResidenceModel } from "../../residences/models/residence.model";

export enum TypologyPriceLinePeriod {
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH'
}

export class TypologyPriceLine {
  price: number;
  vat?: TaxModel;
  vat_id: number;
  period: TypologyPriceLinePeriod;
  tariff: TariffModel;
  tariff_id: number;
}

export class TypologyTranslation {
  name: string;
  language_id: number;
}

export class TypologyDiscount{
  month: number;
  year: number;
  discount_value: number;
}

export class TypologyModel {
  id: number;
  max_occupants_number: number;
  product_code: string;
  translations: TypologyTranslation[];
  active: boolean;
  updated_at: Date | string;
  created_at: Date | string;
  residences: ResidenceModel[];
  priceLines: TypologyPriceLine[];
  discounts: TypologyDiscount[];
  reprocess_since: Date;
}
