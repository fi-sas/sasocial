import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTypologyComponent } from './form-typology.component';

describe('FormTripComponent', () => {
  let component: FormTypologyComponent;
  let fixture: ComponentFixture<FormTypologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTypologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTypologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
