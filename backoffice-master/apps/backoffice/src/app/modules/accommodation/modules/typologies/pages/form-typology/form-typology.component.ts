import { TypologyPriceLinePeriod } from './../../models/typology.model';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { TaxModel } from './../../../../../configurations/models/tax.model';
import { first, finalize } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FormControl } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { TypologiesService } from '../../services/typologies.service';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { TariffModel } from '../../../tariffs/models/tariff.model';
import { TariffsService } from '../../../tariffs/services/tariffs.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-form-typology',
  templateUrl: './form-typology.component.html',
  styleUrls: ['./form-typology.component.less']
})
export class FormTypologyComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  loadingLanguages = false;
  languages: LanguageModel[] = [];

  loadingTaxes = false;
  taxes: TaxModel[] = [];

  loadingTariffs = false;
  tariffs: TariffModel[] = [];


  idToUpdate = null;
  loading = false;

  typologyGroup = new FormGroup({
    translations: new FormArray([]),
    priceLines: new FormArray([]),
    product_code: new FormControl('', [Validators.required, trimValidation]),
    max_occupants_number: new FormControl(0, [Validators.required]),
    active: new FormControl(true, Validators.required),
    reprocess_since: new FormControl(null),
    discounts: new FormArray([]),
  });
  translations = this.typologyGroup.get('translations') as FormArray;
  priceLines = this.typologyGroup.get('priceLines') as FormArray;
  discounts = this.typologyGroup.get('discounts') as FormArray;

  constructor(
    private languagesService: LanguagesService,
    private uiService: UiService,
    public router: Router,
    public typologiesService: TypologiesService,
    public taxesService: TaxesService,
    public activateRoute: ActivatedRoute,
    private tariffsService: TariffsService
  ) {
  }

  ngOnInit() {
    this.loadLanguages();
    this.loadTaxes();
    this.loadTariffs();
  }

  loadTypology() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.typologiesService.read(parseInt(data.get('id'), 10)).subscribe(result => {
          this.typologyGroup.patchValue({
            product_code: result.data[0].product_code,
            max_occupants_number: result.data[0].max_occupants_number,
            active: result.data[0].active,
            reprocess_since: result.data[0].reprocess_since,
          })

          if (result.data[0].translations) {
            result.data[0].translations.map(t => {
              this.addTranslation(t.language_id, t.name);
            });
          }

          if (result.data[0].discounts) {
            result.data[0].discounts.map(t => {
              this.addDiscountLine(new Date(t.year, t.month, null), t.discount_value);
            });
          }

          this.languages.map(l => {
            const foundLnaguage = this.translations.value.find(t => t.language_id === l.id);
            if (!foundLnaguage) {
              this.addTranslation(l.id, '');
            }
          });

          if (result.data[0].priceLines) {
            result.data[0].priceLines.map(pl => {
              this.addPriceLine(pl.price, pl.vat_id, pl.period, pl.tariff_id);
            });
          }
        });
      } else {
        this.languages.map(l => {
          this.addTranslation(l.id, '');
        });
      }
    });
  }

  disableDate = (date: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(date).isBefore(moment(currentDate).format("YYYY-MM-DD"));
  }

  loadTariffs() {
    this.loadingTariffs = true;
    this.tariffsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingTariffs = false))
      )
      .subscribe((results) => {
        this.tariffs = results.data;
      });
  }

  addPriceLine(price?: number, vat_id?: number, period?: TypologyPriceLinePeriod, tariff_id?: number) {
    const priceLines = this.typologyGroup.controls.priceLines as FormArray;
    priceLines.push(new FormGroup({
      price: new FormControl(price ? price : 0, [Validators.required]),
      vat_id: new FormControl(vat_id ? vat_id : (this.taxes.length > 0 ? this.taxes[0].id : null), Validators.required),
      tariff_id: new FormControl(tariff_id ? tariff_id : null, Validators.required),
      period: new FormControl(period ? period : null, Validators.required)
    }));
  }

  deletePriceLine(priceLineIndex: any) {
    if (this.priceLines.at(priceLineIndex)) {
      this.priceLines.removeAt(priceLineIndex);
    }
  }

  addDiscountLine( monthAndYear?: Date, discount_value?: number) {
    const discounts = this.typologyGroup.controls.discounts as FormArray;
    discounts.push(
      new FormGroup({
        date: new FormControl(monthAndYear? monthAndYear : null , Validators.required),
        discount_value: new FormControl(discount_value ? discount_value : 0, Validators.required),
      })
    );
  }

  deleteDiscountLine(discountLineIndex: any) {
    if (this.discounts.at(discountLineIndex)) {
      this.discounts.removeAt(discountLineIndex);
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.typologyGroup.controls.translations as FormArray;
    translations.push(new FormGroup({
      language_id: new FormControl(language_id, Validators.required),
      name: new FormControl(name, [Validators.required, trimValidation]),
    }));
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService.list(1, -1).pipe(first(), finalize(() => this.loadingLanguages = false)).subscribe(results => {
      this.languages = results.data;

      this.loadTypology();
    });
  }

  loadTaxes() {
    this.loadingTaxes = true;
    this.taxesService.list(1, -1).pipe(
      first(),
      finalize(() => this.loadingTaxes = false)
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  submitForm() {
    for (const i in this.typologyGroup.controls) {
      if (i) {
        this.typologyGroup.controls[i].markAsDirty();
        this.typologyGroup.controls[i].updateValueAndValidity();
      }
    }
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    for (const ds in this.discounts.controls) {
      if (ds) {
        const disc = this.discounts.get(ds) as FormGroup;
        for (const i in disc.controls) {
          if (disc) {
            disc.controls[i].markAsDirty();
            disc.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    for (const pl in this.priceLines.controls) {
      if (pl) {
        const plt = this.priceLines.get(pl) as FormGroup;
        for (const i in plt.controls) {
          if (plt) {
            plt.controls[i].markAsDirty();
            plt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    if (this.typologyGroup.valid) {
      this.loading = true;
      if (this.idToUpdate) {
        this.typologiesService.update(this.idToUpdate, this.typologyGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['accommodation', 'typologies', 'list']);
            this.uiService.showMessage(MessageType.success, 'Tipologia alterado com sucesso');
          });
      } else {
        this.typologiesService.create(this.typologyGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['accommodation', 'typologies', 'list']);
            this.uiService.showMessage(MessageType.success, 'Tipologia criado com sucesso');
          });
      }
    }

  }

  backList() {
    this.router.navigate(['accommodation', 'typologies', 'list']);
  }

  isDisabledPeriod(value:any , i){
    const tariffs = this.typologyGroup.get('priceLines')['controls'][i].value;
    return this.priceLines.controls.find(p => p.value.tariff_id === tariffs.tariff_id && p.value.period === value);
  }

  isTariffDisabled(tariff){
    const tariffs = this.priceLines.controls.filter(p => p.value.tariff_id === tariff.id);
    return tariff.active ? tariffs.find(t => t.value.period === "DAY") && tariffs.find(t => t.value.period === "WEEK") && tariffs.find(t => t.value.period === "MONTH") : false;
  }

  filterDaySelection(i){
    const tariffs = this.typologyGroup.get('priceLines')['controls'][i];
    tariffs.controls.period.setValue(null)
  }
}
