import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypologiesComponent } from './list-typologies.component';

describe('ListTypologiesComponent', () => {
  let component: ListTypologiesComponent;
  let fixture: ComponentFixture<ListTypologiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTypologiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypologiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
