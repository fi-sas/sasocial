import { first } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TypologiesService } from '../../services/typologies.service';

@Component({
  selector: 'fi-sas-list-typologies',
  templateUrl: './list-typologies.component.html',
  styleUrls: ['./list-typologies.component.less']
})
export class ListTypologiesComponent extends TableHelper implements OnInit {
  status = [];
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public typologiesService: TypologiesService) {
      super(uiService, router, activatedRoute);
     }

  ngOnInit() {
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.persistentFilters = {
      searchFields: 'name,product_code',
    };
    this.initTableData(this.typologiesService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.filters.max_occupants_number = null;
    this.searchData(true)
  }
   
  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  deleteTypology(id: number) {
    this.uiService.showConfirm('Eliminar tipologia', 'Pretende eliminar esta tipologia', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.typologiesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Tipologia eliminada com sucesso');
        });
      }
    });
  }

  
}
