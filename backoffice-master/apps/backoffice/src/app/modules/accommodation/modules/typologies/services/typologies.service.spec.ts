import { TestBed } from '@angular/core/testing';

import { TypologiesService } from './typologies.service';

describe('TypologiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypologiesService = TestBed.get(TypologiesService);
    expect(service).toBeTruthy();
  });
});
