import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { TypologyModel } from '../models/typology.model';

@Injectable({
  providedIn: 'root'
})
export class TypologiesService extends Repository<TypologyModel> {

    entities_url = 'ACCOMMODATION.TYPOLOGIES';
    entity_url = 'ACCOMMODATION.TYPOLOGIES_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
      super(resourceService, urlService)
   }
}
