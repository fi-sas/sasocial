import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Criar tipologia', title: 'Criar tipologia', scope: 'accommodation:typologies:create' }
  },
  {
    path: 'edit/:id',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Editar tipologia', title: 'Editar tipologia', scope: 'accommodation:typologies:create' }
  },
  {
    path: 'list',
    component: ListTypologiesComponent,
    data: { breadcrumb: 'Listar tipologias', title: 'Listar tipologias', scope: 'accommodation:typologies:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypologiesRoutingModule { }
