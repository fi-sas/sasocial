import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypologiesRoutingModule } from './typologies-routing.module';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';
import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';
import { ViewTypologyComponent } from './components/view-typology/view-typology.component';

@NgModule({
  declarations: [
    FormTypologyComponent,
    ListTypologiesComponent,
    ViewTypologyComponent
  ],
  imports: [
    CommonModule,
    TypologiesRoutingModule,
    SharedModule
  ]
})
export class TypologiesModule { }
