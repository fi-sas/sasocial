import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTypologyChangeRequestComponent } from './view-typology-change-request.component';

describe('ViewTypologyChangeRequestComponent', () => {
  let component: ViewTypologyChangeRequestComponent;
  let fixture: ComponentFixture<ViewTypologyChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTypologyChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTypologyChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
