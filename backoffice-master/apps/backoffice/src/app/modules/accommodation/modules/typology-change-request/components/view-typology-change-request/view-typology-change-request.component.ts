import { Component, Input, OnInit } from '@angular/core';
import { RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';

@Component({
  selector: 'fi-sas-view-typology-change-request',
  templateUrl: './view-typology-change-request.component.html',
  styleUrls: ['./view-typology-change-request.component.less']
})
export class ViewTypologyChangeRequestComponent implements OnInit {

  @Input() data: RoomChangeRequestModel = null;
  RoomChangeRequestStatusTranslations = RoomChangeRequestStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
