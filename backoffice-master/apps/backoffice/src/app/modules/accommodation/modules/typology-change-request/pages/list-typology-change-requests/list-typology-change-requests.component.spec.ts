import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypologyChangeRequestsComponent } from './list-typology-change-requests.component';

describe('ListTypologyChangeRequestsComponent', () => {
  let component: ListTypologyChangeRequestsComponent;
  let fixture: ComponentFixture<ListTypologyChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTypologyChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypologyChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
