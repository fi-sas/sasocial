import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RoomChangeRequestDecisionTranslations, RoomChangeRequestModel, RoomChangeRequestStatusTranslations } from '@fi-sas/backoffice/modules/accommodation/models/Room-change-request.model';
import { AccommodationService } from '@fi-sas/backoffice/modules/accommodation/services/accommodation.service';
import { RoomChangeRequestsService } from '@fi-sas/backoffice/modules/accommodation/services/room-change-requests.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { RoomModel } from '../../../rooms/models/room.model';
import { RoomsService } from '../../../rooms/services/rooms.service';

@Component({
  selector: 'fi-sas-list-typology-change-requests',
  templateUrl: './list-typology-change-requests.component.html',
  styleUrls: ['./list-typology-change-requests.component.less']
})
export class ListTypologyChangeRequestsComponent extends TableHelper implements OnInit {


  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalTypologyChangeSelected: RoomChangeRequestModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";
  rooms: RoomModel[] = [];
  roomsLoading = false;
  newRoomId = null;




  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private roomChangeRequestsService: RoomChangeRequestsService,
    private roomsService: RoomsService,
    private accommodationService: AccommodationService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'start_date',
        label: 'Data Início',
        template: (data) => {
          return moment(new Date(data.start_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'reason',
        label: 'Justificação',
        sortable: false
      },
      {
        key: '',
        label: 'Nova tipologia',
        template: (data) => {
          const pt = data.typology.translations.find(x => x.language_id == 3);
          return pt ? pt.name : '-';
        },
        sortable: true
      },

      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: RoomChangeRequestDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: RoomChangeRequestStatusTranslations
      },
    );
    this.persistentFilters['withRelated'] = 'typology,application,history,room,file';
    // this.persistentFilters.query = "TYPOLOGY";
    this.filters.type = "TYPOLOGY";
    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }
  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.roomChangeRequestsService);
  }


  openModalWithdrawal(typologyChange: RoomChangeRequestModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalTypologyChangeSelected = typologyChange;
    this.changeStatusModalAction = action;
    if (action == "dispatch") {
      this.roomsLoading = true;
      this.roomsService.freeRooms(typologyChange.application.assigned_residence_id, typologyChange.application.id).pipe(
        first(),
        finalize(() => this.roomsLoading = false)
      ).subscribe(result => {
        this.rooms = result.data.filter(x => x.typology_id == typologyChange.typology_id);
      });
    }
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";
  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar ausência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  handleChangeStatus() {

    if (this.changeStatusModalAction === 'analyse') {

      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.status(this.changeStatusModalTypologyChangeSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      if (!this.newRoomId && this.changeStatusModalDecision == "APPROVE") {
        this.uiService.showMessage(MessageType.info, "Seleção de novo quarto obrigatória")
        return;
      }
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.status(this.changeStatusModalTypologyChangeSelected.id, "DISPATCH", { decision: this.changeStatusModalDecision, room_id: this.newRoomId }, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.admin_approve(this.changeStatusModalTypologyChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.roomChangeRequestsService.admin_reject(this.changeStatusModalTypologyChangeSelected.id, {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.roomChangeRequestsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

  }

  handleChangeStatusCancel() {
    this.changeStatusModalTypologyChangeSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
    this.newRoomId = null;
  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
