import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTypologyChangeRequestsComponent } from './pages/list-typology-change-requests/list-typology-change-requests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTypologyChangeRequestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:applications:read' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypologyChangeRequestRoutingModule { }
