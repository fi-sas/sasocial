import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypologyChangeRequestRoutingModule } from './typology-change-request-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListTypologyChangeRequestsComponent } from './pages/list-typology-change-requests/list-typology-change-requests.component';
import { ViewTypologyChangeRequestComponent } from './components/view-typology-change-request/view-typology-change-request.component';

@NgModule({
  declarations: [ListTypologyChangeRequestsComponent, ViewTypologyChangeRequestComponent],
  imports: [
    CommonModule,
    TypologyChangeRequestRoutingModule,
    SharedModule
  ]
})
export class TypologyChangeRequestModule { }
