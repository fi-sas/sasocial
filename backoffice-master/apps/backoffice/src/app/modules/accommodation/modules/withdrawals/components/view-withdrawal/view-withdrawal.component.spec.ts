import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWithdrawalComponent } from './view-withdrawal.component';

describe('ViewWithdrawalComponent', () => {
  let component: ViewWithdrawalComponent;
  let fixture: ComponentFixture<ViewWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
