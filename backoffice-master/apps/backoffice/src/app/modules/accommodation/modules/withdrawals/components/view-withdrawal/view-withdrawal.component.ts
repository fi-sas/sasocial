import { WithdrawalModel, WithdrawalStatusTranslations } from '../../models/withdrawal.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-withdrawal',
  templateUrl: './view-withdrawal.component.html',
  styleUrls: ['./view-withdrawal.component.less']
})
export class ViewWithdrawalComponent implements OnInit {

  @Input() data: WithdrawalModel = null;
  WithdrawalStatusTranslations = WithdrawalStatusTranslations;

  constructor() { }

  ngOnInit() {
  }

}
