import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '../../aco_applications/models/application.model';
export const WithdrawalStatusTranslations = {
  SUBMITTED: { label: 'Submetido', color: 'blue' },
  ANALYSIS: { label: 'Analise', color: 'yellow' },
  DISPATCH: { label: 'Despacho', color: 'orange' },
  APPROVED: { label: 'Aprovada', color: 'green' },
  CANCELLED: { label: 'Cancelada', color: 'gray' },
  REJECTED: { label: 'Rejeitada', color: 'red' },
};

export const WithdrawalDecisionTranslations = {
  APPROVE: { label: 'Aprovar', color: 'green' },
  REJECT: { label: 'Rejeitar', color: 'red' },
};

export enum WithdrawalStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSIS = 'ANALYSIS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',

}
export class WithdrawalHistoryModel {
  id: number;
  withdrawal_id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: Date;
  update_at: Date;
}

export class WithdrawalModel {
  id: number;
  reason: string;
  application_id: number;
  application: ApplicationModel;
  end_date: Date;
  file_id: number;
  file: FileModel;
  user_id: number;
  status: WithdrawalStatus;
  decision: string;
  history: WithdrawalHistoryModel[];
  created_at: Date;
  update_at: Date;
}
