import { WithdrawalModel } from "./withdrawal.model";

export class WithdrawalStatusModel {
    notes: string;
    withdrawal: WithdrawalModel | any;
}
