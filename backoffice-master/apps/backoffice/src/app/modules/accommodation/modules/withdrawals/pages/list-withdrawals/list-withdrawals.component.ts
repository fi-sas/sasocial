import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Component, OnInit } from '@angular/core';
import { WithdrawalsService } from '../../services/withdrawals.service';
import { AccommodationService } from '../../../../services/accommodation.service';
import { ResidenceModel } from '../../../residences/models/residence.model';
import { WithdrawalStatusTranslations, WithdrawalModel, WithdrawalStatus, WithdrawalDecisionTranslations } from '../../models/withdrawal.model';
import { first, finalize } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-list-withdrawals',
  templateUrl: './list-withdrawals.component.html',
  styleUrls: ['./list-withdrawals.component.less']
})
export class ListWithdrawalsComponent extends TableHelper implements OnInit {

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalWithdrawalSelected: WithdrawalModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';
  changeStatusModalDecision = "APPROVE";

  WithdrawalStatus = WithdrawalStatus;
  residence: ResidenceModel = null;
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public accommodationService: AccommodationService,
    public withdrawalsService: WithdrawalsService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'full_name,email,student_number,tin,identification'
    }
    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'end_date',
        label: 'Data Saída',
        template: (data) => {
          return moment(new Date(data.end_date)).format('DD/MM/YYYY');
        },
        sortable: true
      },
      {
        key: 'application.room.name',
        label: 'Quarto',
        sortable: false
      },
      {
        key: 'application.assignedResidence.name',
        label: 'Residência',
        sortable: false
      },
      {
        key: 'decision',
        label: 'Decisão',
        sortable: true,
        tag: WithdrawalDecisionTranslations
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: WithdrawalStatusTranslations
      },
    );

    this.persistentFilters['withRelated'] = 'application,history,file';

    const data = this.accommodationService.getAccommodationData();
    if (data.residence) {
      this.filters.residence_id = data.residence.id;
    }
    if (data.academicYear) {
      this.filters.academic_year = data.academicYear;
    }
  }

  ngOnInit() {
    this.accommodationService.updateContractChangeStats();
    this.initTableData(this.withdrawalsService);
  }

  openModalWithdrawal(withdrawal: WithdrawalModel, action: string) {
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalWithdrawalSelected = withdrawal;
    this.changeStatusModalAction = action;
  }


  handleChangeStatus() {
    if (this.changeStatusModalAction === 'analyse') {
      this.changeStatusModalLoading = true;
      this.withdrawalsService.status(this.changeStatusModalWithdrawalSelected.id, "ANALYSE", {}, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.withdrawalsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'dispatch') {
      this.changeStatusModalLoading = true;
      this.withdrawalsService.status(this.changeStatusModalWithdrawalSelected.id, "DISPATCH", {
        decision: this.changeStatusModalDecision
      }, this.changeStatusModalNotes
      ).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.withdrawalsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.withdrawalsService.admin_aprove(this.changeStatusModalWithdrawalSelected.id, {
        notes: this.changeStatusModalNotes,
        withdrawal: {}
      }).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.withdrawalsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    } else if (this.changeStatusModalAction === 'reject') {
      this.changeStatusModalLoading = true;
      this.withdrawalsService.admin_reject(this.changeStatusModalWithdrawalSelected.id, {
        notes: this.changeStatusModalNotes,
        withdrawal: {}
      }).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        this.initTableData(this.withdrawalsService);
        this.handleChangeStatusCancel();
        this.accommodationService.updateContractChangeStats();
      });
    }

    //  if (this.changeStatusModalAction === 'approve') {
    //   this.changeStatusModalLoading = true;
    //   this.withdrawalsService.aprove(this.changeStatusModalWithdrawalSelected.id, {
    //     notes: this.changeStatusModalNotes,
    //     withdrawal: {}
    //   }).pipe(
    //     first(),
    //     finalize(() => this.changeStatusModalLoading = false)
    //   ).subscribe(result => {
    //     const withdrawalIndex = this.data.findIndex(w => w.id === this.changeStatusModalWithdrawalSelected.id);
    //     if (withdrawalIndex >= 0) {
    //       this.data[withdrawalIndex] = result.data[0];
    //       this.data = [...this.data];
    //     }
    //     this.handleChangeStatusCancel();
    //   });
    // } else {
    //   this.changeStatusModalLoading = true;
    //   this.withdrawalsService.reject(this.changeStatusModalWithdrawalSelected.id, {
    //     notes: this.changeStatusModalNotes,
    //     withdrawal: {}
    //   }).pipe(
    //     first(),
    //     finalize(() => this.changeStatusModalLoading = false)
    //   ).subscribe(result => {
    //     const withdrawalIndex = this.data.findIndex(w => w.id === this.changeStatusModalWithdrawalSelected.id);
    //     if (withdrawalIndex >= 0) {
    //       this.data[withdrawalIndex] = result.data[0];
    //       this.data = [...this.data];
    //     }
    //     this.handleChangeStatusCancel();
    //   });
    // }
  }

  handleChangeStatusCancel() {
    this.changeStatusModalWithdrawalSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  cancelModal(id: number) {
    this.uiService
      .showConfirm(
        'Cancelar pedido',
        'Pretende cancelar este pedido?',
        'Sim',
        'Não',
        'warning'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          this.withdrawalsService
            .cancel(id, {
              notes: '',
              withdrawal: {},
            })
            .pipe(first())
            .subscribe(() => {
              this.searchData();
            });
        }
      });
  }

  modalOkButton(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar";
    }
    return "";

  }

  modalTitle(): string {
    if (this.changeStatusModalAction == "analyse") {
      return "Analisar desistência";
    }
    if (this.changeStatusModalAction == "dispatch") {
      return "Enviar para despacho";
    }
    if (this.changeStatusModalAction == "approve") {
      return "Aprovar decisão";
    }
    if (this.changeStatusModalAction == "reject") {
      return "Rejeitar decisão";
    }
    return "";

  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
