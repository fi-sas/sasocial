import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { WithdrawalModel } from '../models/withdrawal.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { WithdrawalStatusModel } from '../models/withdrawalStatus.model';

@Injectable({
  providedIn: 'root',
})
export class WithdrawalsService extends Repository<WithdrawalModel> {
  entities_url = 'ACCOMMODATION.WITHDRAWALS';
  entity_url = 'ACCOMMODATION.WITHDRAWALS_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
  }

  status(id: number, event: string, withdrawal, notes) {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('ACCOMMODATION.WITHDRAWALS_ID_STATUS', { id }), { event, withdrawal, notes });
  }

  admin_aprove(
    id: number,
    withdrawalNotes: WithdrawalStatusModel
  ): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('ACCOMMODATION.WITHDRAWALS_ID_APPROVE', { id }),
      withdrawalNotes
    );
  }

  admin_reject(
    id: number,
    withdrawalNotes: WithdrawalStatusModel
  ): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('ACCOMMODATION.WITHDRAWALS_ID_REJECT', { id }),
      withdrawalNotes
    );
  }

  cancel(
    id: number,
    withdrawalNotes: WithdrawalStatusModel
  ): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('ACCOMMODATION.WITHDRAWALS_ID_CANCEL', { id }),
      withdrawalNotes
    );
  }
}
