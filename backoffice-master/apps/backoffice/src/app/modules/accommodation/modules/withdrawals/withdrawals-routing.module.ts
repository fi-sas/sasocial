import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListWithdrawalsComponent } from './pages/list-withdrawals/list-withdrawals.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListWithdrawalsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'accommodation:withdrawals:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithdrawalsRoutingModule { }
