import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WithdrawalsRoutingModule } from './withdrawals-routing.module';
import { ListWithdrawalsComponent } from './pages/list-withdrawals/list-withdrawals.component';
import { ViewWithdrawalComponent } from './components/view-withdrawal/view-withdrawal.component';

@NgModule({
  declarations: [
    ListWithdrawalsComponent,
    ViewWithdrawalComponent
  ],
  imports: [
    CommonModule,
    WithdrawalsRoutingModule,
    SharedModule
  ]
})
export class WithdrawalsModule { }
