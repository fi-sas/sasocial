import { Component, Input, OnInit } from "@angular/core";
import { ApplicationModel } from "@fi-sas/backoffice/modules/accommodation/modules/aco_applications/models/application.model";
import { FiConfigurator } from "@fi-sas/configurator";
import { first } from "rxjs/operators";

@Component({
    selector: 'fi-sas-view-application-detail',
    templateUrl: './view-application-detail.component.html',
    styleUrls: ['./view-application-detail.component.less']
})

export class ViewApplicationDetailComponent implements OnInit {

    @Input() application: ApplicationModel = null;
    modalPerfil = false;
    DEFAULT_LANG_ID = null;

    constructor(private configurator: FiConfigurator){
        this.DEFAULT_LANG_ID = this.configurator.getOption('DEFAULT_LANG_ID');
    }

    ngOnInit(){
       
    }
}

