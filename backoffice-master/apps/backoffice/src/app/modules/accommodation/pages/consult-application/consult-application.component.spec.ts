import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsultApplicationComponent } from './consult-application.component';


describe('ConsultApplicationComponent', () => {
  let component: ConsultApplicationComponent;
  let fixture: ComponentFixture<ConsultApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
