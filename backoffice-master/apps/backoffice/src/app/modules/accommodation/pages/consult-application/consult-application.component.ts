import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { Column, TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { ApplicationStatusTranslations } from "../../modules/aco_applications/models/application.model";
import * as moment from 'moment';
import { AccommodationService } from "../../services/accommodation.service";
import { ApplicationListResumeService } from "../../services/application-list-resume.service";

@Component({
    selector: 'fi-sas-consult-application',
    templateUrl: './consult-application.component.html',
    styleUrls: ['./consult-application.component.less']
})

export class ConsultApplicationComponent extends TableHelper implements OnInit {

    columns: Column[] = [];
    accommodationData;
    statusFilter = [
        { value: null, label: 'Todas', scope: this.validPermissionFilter('accommodation:applications:all-applications') },
        { value: 'cancelled', label: 'Cancelada', scope: this.validPermissionFilter('accommodation:applications:applications-cancelled') },
        { value: 'assigned', label: 'Colocado', scope: this.validPermissionFilter('accommodation:applications:applications-assigned') },
        { value: 'confirmed', label: 'Confirmado', scope: this.validPermissionFilter('accommodation:applications:applications-confirmeds') },
        { value: 'contracted', label: 'Contratado', scope: this.validPermissionFilter('accommodation:applications:applications-contracteds') },
        { value: 'withdrawal', label: 'Desistência', scope: this.validPermissionFilter('accommodation:applications:applications-withdrals') },
        { value: 'analysed', label: 'Em analise', scope: this.validPermissionFilter('accommodation:applications:applications-analyse') },
        { value: 'pending', label: 'Em despacho', scope: this.validPermissionFilter('accommodation:applications:applications-dispatches') },
        { value: 'closed', label: 'Fechada', scope: this.validPermissionFilter('accommodation:applications:applications-closed') },
        { value: 'queued', label: 'Lista de espera', scope: this.validPermissionFilter('accommodation:applications:applications-queued') },
        { value: 'unassigned', label: 'Não colocado', scope: this.validPermissionFilter('accommodation:applications:applications-unassigned') },
        { value: 'opposition', label: 'Oposição', scope: this.validPermissionFilter('accommodation:applications:applications-oppositions') },
        { value: 'rejected', label: 'Rejeitado', scope: this.validPermissionFilter('accommodation:applications:applications-rejected') },
        { value: 'submitted', label: 'Submetida', scope: this.validPermissionFilter('accommodation:applications:applications-selection') },
    ];

    constructor(router: Router,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        public accommodationService: AccommodationService,
        public applicationResumeService: ApplicationListResumeService, private authService: AuthService) {
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            searchFields: 'full_name,email,student_number,city,tin,identification'
        }
        this.persistentFilters['withRelated'] = 'tariff,preferred_typology,residence,regime,regime.translations,room,course,extras,assignedResidence,whichRoom,whichResidence,document_type,occurrences,user'
        this.accommodationData = accommodationService.getAccommodationData();
        if (this.accommodationData.academicYear) {
            this.persistentFilters['academic_year'] = this.accommodationData.academicYear;
        }

        if (this.accommodationData.applicationPhase) {
            this.persistentFilters['application_phase_id'] = this.accommodationData.applicationPhase.id;
        }

        if (this.accommodationData.residence) {
            this.persistentFilters['application_residence_id'] = this.accommodationData.residence.id;
        }
    }

    ngOnInit() {
        this.columns.push(
            {
                key: 'student_number',
                label: 'Nº Estudante',
                sortable: true,
            },
            {
                key: 'full_name',
                label: 'Nome',
                sortable: true,
            },
            {
                key: '',
                label: 'Residência Candidata',
                template: (residence) => {
                    return residence.residence.name;
                },
                sortable: false,
            },
            {
                key: '',
                label: 'Residência Atribuída',
                template: (residence) => {
                    return residence.assignedResidence ? residence.assignedResidence.name : ''
                },
                sortable: false,
            },
            {
                key: '',
                label: 'Quarto',
                template: (residence) => {
                    return residence.room ? residence.room.name : '';
                },
                sortable: false,
            },
            {
                key: '',
                label: 'Data de Entrada',
                template: (data) => {
                    return moment(new Date(data.start_date)).format('DD/MM/YYYY');
                },
                sortable: true,
            },
            {
                key: 'phone_1',
                label: 'Contacto Estudante',
                sortable: true,
            },
            {
                key: 'phone_2',
                label: 'Contacto Emergência',
                sortable: true,
            },
            {
                key: 'status',
                label: 'Estado',
                sortable: true,
                tag: ApplicationStatusTranslations
            },

        );
        this.initTableData(this.applicationResumeService);
    }

    validPermissionFilter(permission) {
        return this.authService.hasPermission(permission);
    }

    listComplete() {
        this.filters.status = null;
        this.filters.search = null;
        this.searchData(true)
    }
}