import { RoomMapModel } from './../../modules/rooms/models/room-map.model';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { RoomsService } from '../../modules/rooms/services/rooms.service';
import { AccommodationService } from '../../services/accommodation.service';
import { ResidenceOccupationModel } from '../../modules/residences/models/residence-occupation.model';
import { ResidencesService } from '../../modules/residences/services/residences.service';
import { ResidenceModel } from '../../modules/residences/models/residence.model';
import { ApplicationPhaseModel } from '../../modules/phases/models/application-phase.model';
import { Router } from '@angular/router';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';

@Component({
  selector: 'fi-sas-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  residence: ResidenceModel = null;
  academicYear = null;
  applicationPhase: ApplicationPhaseModel = null;

  totalSubmitted = 0;
  totalAnalysed= 0;
  totalPending = 0;
  totalQueued = 0;
  totalAssigned = 0;
  totalUnassigned = 0;
  totalConfirmed = 0;
  totalRejected = 0;
  totalWithdrawal = 0;
  totalOpposition = 0;
  totalContracted = 0;
  totalClosed = 0;
  totalCancelled = 0;

  loadingStats = false;

  roomsMap: RoomMapModel[] = [];
  typlogogiesMap: RoomMapModel[] = [];
  residenceOccupation: ResidenceOccupationModel = { maximumcapacity: 0, totalAvailable: 0, totalOccupied: 0, occupationRate: 0};
  showInfoAcademicYear: boolean = false;

  constructor(
    private accommodationService: AccommodationService,
    private applicationsService: ApplicationsService,
    private residenceService: ResidencesService,
    private roomsService: RoomsService,
    private router: Router,
    private academicYearService: AcademicYearsService
  ) {
    const data = this.accommodationService.getAccommodationData();
    this.residence = data.residence;
    this.academicYear = data.academicYear;
    this.applicationPhase = data.applicationPhase;
  }

  ngOnInit() {

    let residence_id = {};
    let assigned_residence_id = {};

    if(this.residence) {
      residence_id = {
        residence_id: this.residence.id
      };
      assigned_residence_id = {
        assigned_residence_id: this.residence.id
      };
    }
    if(!this.academicYear){
      this.academicYearService.getCurrentAcademicYear().pipe(first()).subscribe(response => {
        if(response.data.length ){
          this.academicYear = response.data[0].academic_year;
          this.showInfoAcademicYear = true
          this.getInfos();
        }
      });
    }else{
      this.getInfos();
    }
  }

  goApplication(value: string) {
    localStorage.setItem('status_application', value);
    this.router.navigate(['/accommodation', 'applications', 'all-applications']);
  }

  getInfos(){
    this.residenceService.typologiesMap( this.residence ? this.residence.id : null, this.academicYear).pipe(first()).subscribe(typ => {
      this.typlogogiesMap = typ.data;
    });

    this.residenceService.occupation( this.residence ? this.residence.id : null, this.academicYear).pipe(first()).subscribe(occupation => {
      this.residenceOccupation = occupation.data[0];
    });

    this.roomsService.roomsMap( this.residence ? this.residence.id : null, this.academicYear).pipe(first()).subscribe(rooms => {
      this.roomsMap = rooms.data;
    });

    this.loadingStats = true;
    this.applicationsService.getApplicationsStats(
      this.residence ? this.residence.id : null,
      this.academicYear,
      this.applicationPhase ? this.applicationPhase.id : null
    ).pipe(
      first(),
      finalize(() => this.loadingStats = false)
    ).subscribe(results => {
      this.totalSubmitted = results.data[0].submitted;
      this.totalAnalysed = results.data[0].analysed;
      this.totalPending = results.data[0].pending;
      this.totalQueued = results.data[0].queued;
      this.totalAssigned = results.data[0].assigned;
      this.totalUnassigned = results.data[0].unassigned;
      this.totalConfirmed = results.data[0].confirmed;
      this.totalRejected = results.data[0].rejected;
      this.totalWithdrawal = results.data[0].withdrawal;
      this.totalOpposition = results.data[0].opposition;
      this.totalContracted = results.data[0].contracted;
      this.totalClosed = results.data[0].closed;
      this.totalCancelled = results.data[0].cancelled;
    });
  }

}
