import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GeralSettingsComponent } from './geral-settings.component';


describe('GeralSettingsComponent', () => {
  let component: GeralSettingsComponent;
  let fixture: ComponentFixture<GeralSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeralSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeralSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
