import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { GeralSettingsModel } from '../../models/geral-settings.model';
import { GeralSettingsService } from '../../services/geral-settings.service';

@Component({
  selector: 'fi-sas-geral-settings',
  templateUrl: './geral-settings.component.html',
  styleUrls: ['./geral-settings.component.less'],
})
export class GeralSettingsComponent implements OnInit {
  formData: FormGroup;
  loadingConfiguration = true;
  loading = false;
  submit = false;
  constructor(
    private fb: FormBuilder,
    private settings: GeralSettingsService,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.formData = this.fb.group({
      allow_optional_residence: new FormControl(false, [Validators.required]),
      allow_application_renew: new FormControl(false, [Validators.required]),
      assigned_status_auto_change: new FormControl(false, [
        Validators.required,
      ]),
      change_status_on_create: new FormControl(false, [Validators.required]),
      enable_status_external_validate_or_paid: new FormControl(false, [
        Validators.required,
      ]),
      change_status_on_create_days: new FormControl(''),
      assigned_status_auto_change_day: new FormControl(''),
      assigned_status_auto_change_state: new FormControl(''),
      billing_out_of_date_type: new FormControl('MONTH_DAY', [
        Validators.required,
      ]),
      billing_out_of_date_day: new FormControl('', [Validators.required]),
      show_iban_on_application_form: new FormControl(false, [
        Validators.required,
      ]),
      unassigned_opposition_limit: new FormControl('', [Validators.required]),
      institute_iban: new FormControl(''),
      institute_swift: new FormControl(''),
      institute_sepa_name: new FormControl(''),
      institute_sepa_private_id: new FormControl(''),
    });
    this.getConfigurations();
  }
  get f() {
    return this.formData.controls;
  }

  getConfigurations() {
    this.settings
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.loadingConfiguration = false))
      )
      .subscribe((data) => {
        this.formData
          .get('allow_optional_residence')
          .setValue(data.data[0].ALLOW_OPTIONAL_RESIDENCE);
        this.formData
          .get('allow_application_renew')
          .setValue(data.data[0].ALLOW_APPLICATION_RENEW);
        this.formData
          .get('assigned_status_auto_change')
          .setValue(data.data[0].ASSIGNED_STATUS_AUTO_CHANGE);
        this.formData
          .get('assigned_status_auto_change_day')
          .setValue(data.data[0].STATUS_AUTO_CHANGE_DAYS);
        this.formData
          .get('assigned_status_auto_change_state')
          .setValue(data.data[0].EVENT_FOR_AUTO_CHANGE);
        this.formData
          .get('billing_out_of_date_type')
          .setValue(data.data[0].BILLING_OUT_OF_DATE_TYPE);
        this.formData
          .get('billing_out_of_date_day')
          .setValue(data.data[0].BILLING_OUT_OF_DATE_DAY);
        this.formData
          .get('change_status_on_create')
          .setValue(data.data[0].CHANGE_STATUS_ON_CREATE);
        this.formData
          .get('change_status_on_create_days')
          .setValue(data.data[0].CHANGE_STATUS_ON_CREATE_DAYS);
        this.formData
          .get('show_iban_on_application_form')
          .setValue(data.data[0].SHOW_IBAN_ON_APPLICATION_FORM);
        this.formData
          .get('unassigned_opposition_limit')
          .setValue(data.data[0].UNASSIGNED_OPPOSITION_LIMIT);
        this.formData
          .get('enable_status_external_validate_or_paid')
          .setValue(data.data[0].ENABLE_STATUS_EXTERNAL_VALIDATE_OR_PAID);
        this.formData
          .get('institute_iban')
          .setValue(data.data[0].INSTITUTE_IBAN);
        this.formData
          .get('institute_swift')
          .setValue(data.data[0].INSTITUTE_SWIFT);
        this.formData
          .get('institute_sepa_name')
          .setValue(data.data[0].INSTITUTE_SEPA_NAME);
        this.formData
          .get('institute_sepa_private_id')
          .setValue(data.data[0].INSTITUTE_SEPA_PRIVATE_ID);
      });
  }

  submitForm() {
    this.loading = true;
    this.submit = true;
    let sendValue: GeralSettingsModel = new GeralSettingsModel();
    if (this.formData.valid) {
      this.submit = false;
      sendValue.ALLOW_OPTIONAL_RESIDENCE = this.formData.get(
        'allow_optional_residence'
      ).value;
      sendValue.ALLOW_APPLICATION_RENEW = this.formData.get(
        'allow_application_renew'
      ).value;
      sendValue.ASSIGNED_STATUS_AUTO_CHANGE = this.formData.get(
        'assigned_status_auto_change'
      ).value;
      sendValue.STATUS_AUTO_CHANGE_DAYS = this.formData.get(
        'assigned_status_auto_change_day'
      ).value;
      sendValue.EVENT_FOR_AUTO_CHANGE = this.formData.get(
        'assigned_status_auto_change_state'
      ).value;
      sendValue.BILLING_OUT_OF_DATE_TYPE = this.formData.get(
        'billing_out_of_date_type'
      ).value;
      sendValue.BILLING_OUT_OF_DATE_DAY = this.formData.get(
        'billing_out_of_date_day'
      ).value;
      sendValue.CHANGE_STATUS_ON_CREATE = this.formData.get(
        'change_status_on_create'
      ).value;
      sendValue.CHANGE_STATUS_ON_CREATE_DAYS = this.formData.get(
        'change_status_on_create_days'
      ).value;
      sendValue.SHOW_IBAN_ON_APPLICATION_FORM = this.formData.get(
        'show_iban_on_application_form'
      ).value;
      sendValue.UNASSIGNED_OPPOSITION_LIMIT = this.formData.get(
        'unassigned_opposition_limit'
      ).value;
      sendValue.ENABLE_STATUS_EXTERNAL_VALIDATE_OR_PAID = this.formData.get(
        'enable_status_external_validate_or_paid'
      ).value;
      sendValue.INSTITUTE_IBAN = this.formData.get('institute_iban').value;
      sendValue.INSTITUTE_SWIFT = this.formData.get('institute_swift').value;
      sendValue.INSTITUTE_SEPA_NAME = this.formData.get(
        'institute_sepa_name'
      ).value;
      sendValue.INSTITUTE_SEPA_PRIVATE_ID = this.formData.get(
        'institute_sepa_private_id'
      ).value;

      this.settings
        .create(sendValue)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((data) => {
          this.uiService.showMessage(
            MessageType.success,
            'Configurações alteradas com sucesso'
          );
        });
    } else {
      this.loading = false;
    }
  }

  changeStatus(event) {
    this.formData.get('assigned_status_auto_change_day').setValue(null);
    this.formData.get('assigned_status_auto_change_state').setValue(null);
    if (event == true) {
      this.formData.controls.assigned_status_auto_change_day.setValidators([
        Validators.required,
      ]);
      this.formData.controls.assigned_status_auto_change_state.setValidators([
        Validators.required,
      ]);
    } else {
      this.formData.controls.assigned_status_auto_change_day.clearValidators();
      this.formData.controls.assigned_status_auto_change_state.clearValidators();
    }
    this.formData.controls.assigned_status_auto_change_day.updateValueAndValidity();
    this.formData.controls.assigned_status_auto_change_state.updateValueAndValidity();
  }

  changeStatusValid(event) {
    this.formData.get('change_status_on_create_days').setValue(null);
    if (event == true) {
      this.formData.controls.change_status_on_create_days.setValidators([
        Validators.required,
      ]);
    } else {
      this.formData.controls.change_status_on_create_days.clearValidators();
    }
    this.formData.controls.change_status_on_create_days.updateValueAndValidity();
  }

  processUnprocessedContractedApplications() {
    this.uiService
      .showConfirm(
        'Processar candidaturas',
        'Pretende processar as candidaturas em estado <b>contratado</b> que ainda não foram processadas? <b>Atenção:</b> O processo pode levar algum tempo até que esteja concluído ',
        'Processar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.settings
            .processUnprocessedContractedApplications()
            .subscribe((data) => {
              this.uiService.showMessage(
                MessageType.success,
                'Processamento submetido com sucesso'
              );
            });
        }
      });
  }
}
