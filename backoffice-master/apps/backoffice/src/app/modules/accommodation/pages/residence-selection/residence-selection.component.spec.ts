import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenceSelectionComponent } from './residence-selection.component';

describe('ResidenceSelectionComponent', () => {
  let component: ResidenceSelectionComponent;
  let fixture: ComponentFixture<ResidenceSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidenceSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenceSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
