import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { AccommodationService } from '../../services/accommodation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ResidenceModel } from '../../modules/residences/models/residence.model';
import { ResidencesService } from '../../modules/residences/services/residences.service';
import { ApplicationPhaseModel } from '../../modules/phases/models/application-phase.model';
import { ApplicationPhaseService } from '../../modules/phases/services/application-phase.service';

@Component({
  selector: 'fi-sas-residence-selection',
  templateUrl: './residence-selection.component.html',
  styleUrls: ['./residence-selection.component.less']
})
export class ResidenceSelectionComponent implements OnInit {

  loadingResidences = true;
  loadingPhases = true;

  residences: ResidenceModel[] = [];
  applicationPhases: ApplicationPhaseModel[] = [];
  academicYears: string[] = [];

  selectedApplicationPhase: ApplicationPhaseModel = null;
  selectedAcademicYear: string = null;
  selectedResidence: ResidenceModel = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accommodationService: AccommodationService,
    private residencesService: ResidencesService,
    private applicationPhasesService: ApplicationPhaseService
  ) { }

  ngOnInit() {
    this.loadResidences();
    this.loadApplicationPhases();
  }


  loadResidences() {
    this.residencesService.list(1, -1, null, null, {
      withRelated: false,
      active: true
    }).pipe(
      first(),
      finalize(() => this.loadingResidences = false)
      ).subscribe(residences => {
      this.residences = residences.data;
      if(this.accommodationService.selectedResidence()) {
        this.selectedResidence = this.residences.find((res)=>res.id == this.accommodationService.selectedResidence().id);
      }
    });
  }

  loadApplicationPhases() {
    this.applicationPhasesService.list(1, -1, null, null, {
      withRelated: false
    }).pipe(
      first(),
      finalize(() => this.loadingPhases = false)
    ).subscribe(results => {
      this.applicationPhases = results.data;

      this.academicYears = Array.from(new Set(
        this.applicationPhases.map(a => a.academic_year)
      )).reverse();
      if(this.accommodationService.selectedApplicationPhase()) {
        this.selectedApplicationPhase = this.applicationPhases.find((phase)=> phase.id == this.accommodationService.selectedApplicationPhase().id);
      }
      if(this.accommodationService.selectedAcademicYear()) {
        this.selectedAcademicYear = this.academicYears.find((year)=>year == this.accommodationService.selectedAcademicYear());
      }
    });
  }

  continueButton() {
    this.accommodationService.select(
      this.selectedResidence,
      this.selectedAcademicYear,
      this.selectedApplicationPhase
    );
    this.router.navigate(['/accommodation','dashboard'])
  }

  getApplicationPhases() {
    return this.applicationPhases.filter( ap => {
      if(this.selectedAcademicYear)
        return this.selectedAcademicYear === ap.academic_year;

      return true
    });
  }
}
