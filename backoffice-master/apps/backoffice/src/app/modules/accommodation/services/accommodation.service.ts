import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ResidenceModel } from '../modules/residences/models/residence.model';
import { ApplicationsService } from '../modules/aco_applications/services/applications.service';
import { first, finalize } from 'rxjs/operators';
import { ApplicationPhaseModel } from '../modules/phases/models/application-phase.model';

export interface AccommodationData {
  residence: ResidenceModel,
  academicYear: string,
  applicationPhase: ApplicationPhaseModel
}

@Injectable({
  providedIn: 'root'
})
export class AccommodationService {


  loading = false;
  private _applicationStats: Subject<any> = new Subject<any>();
  private _contractChangeStats: Subject<any> = new Subject<any>();
  private _accommodationData: Subject<AccommodationData> = new Subject<AccommodationData>();

  constructor(
    private applicationService: ApplicationsService
  ) { }


  select(
    residence: ResidenceModel,
    academicYear: string,
    applicationPhase: ApplicationPhaseModel
  ) {
    this.selectResidence(residence);
    this.selectAcademicYear(academicYear);
    this.selectApplicationPhase(applicationPhase);
    this.updateApplicationsStats();
    this.updateContractChangeStats();
    this.emitAccommodationData();
  }

  selectResidence(residence: ResidenceModel) {
    localStorage.setItem('accommodation_residence', JSON.stringify(residence));
  }

  selectAcademicYear(academicYear: string) {
    localStorage.setItem('accommodation_academicYear', JSON.stringify(academicYear));
  }

  selectApplicationPhase(applicationPhase: ApplicationPhaseModel) {
    localStorage.setItem('accommodation_applicationPhase', JSON.stringify(applicationPhase));
  }

  selectedAcademicYear() {
    return JSON.parse(localStorage.getItem('accommodation_academicYear')) as string;
  }

  selectedApplicationPhase() {
    return JSON.parse(localStorage.getItem('accommodation_applicationPhase')) as ApplicationPhaseModel;
  }

  selectedResidence() {
    return JSON.parse(localStorage.getItem('accommodation_residence')) as ResidenceModel;
  }

  getAccommodationData(): AccommodationData {
    return {
      academicYear: this.selectedAcademicYear(),
      applicationPhase: this.selectedApplicationPhase(),
      residence: this.selectedResidence()
    }
  }

  emitAccommodationData() {
    this._accommodationData.next({
      academicYear: this.selectedAcademicYear(),
      applicationPhase: this.selectedApplicationPhase(),
      residence: this.selectedResidence()
    })
  }

  selectedAccommodationDataObservable() {
    return this._accommodationData.asObservable();
  }

  applicationStatsObservable() {
    return this._applicationStats.asObservable();
  }

  contractChangeStatsObservable() {
    return this._contractChangeStats.asObservable();
  }

  updateApplicationsStats() {

    if (this.loading) {
      return;
    }
    this.loading = true;


    this.applicationService.getApplicationsStats(
      this.selectedResidence() ? this.selectedResidence().id : null,
      this.selectedAcademicYear() ? this.selectedAcademicYear() : null,
      this.selectedApplicationPhase() ? this.selectedApplicationPhase().id : null
    )
      .pipe(
        first(),
        finalize(() => this.loading = false)
      )
      .subscribe(result => {
        this._applicationStats.next(result.data[0]);
      });
  }

  updateContractChangeStats() {

    this.applicationService.getContractChangeStats(
      this.selectedResidence() ? this.selectedResidence().id : null,
      this.selectedAcademicYear() ? this.selectedAcademicYear() : null,
      this.selectedApplicationPhase() ? this.selectedApplicationPhase().id : null
    )
      .pipe(
        first())
      .subscribe(result => {
        this._contractChangeStats.next(result.data[0]);
      });
  }

}
