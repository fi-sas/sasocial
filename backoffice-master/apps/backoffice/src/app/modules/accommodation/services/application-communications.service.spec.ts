import { TestBed } from '@angular/core/testing';

import { ApplicationCommunicationsService } from './application-communications.service';

describe('ApplicationCommunicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationCommunicationsService = TestBed.get(ApplicationCommunicationsService);
    expect(service).toBeTruthy();
  });
});
