import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { ApplicationCommunicationModel } from '../models/Application-communication.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationCommunicationsService extends Repository<ApplicationCommunicationModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.APPLICATION_COMMUNICATION';
    this.entity_url = 'ACCOMMODATION.APPLICATION_COMMUNICATION_ID';
  }

  reply(id: number, response: string) {
    return this.resourceService.create<ApplicationCommunicationModel>(
      this.urlService.get('ACCOMMODATION.APPLICATION_COMMUNICATION_REPLY', { id }), { response });
  }

  close(id: number, observations: string) {
    return this.resourceService.create<ApplicationCommunicationModel>(
      this.urlService.get('ACCOMMODATION.APPLICATION_COMMUNICATION_CLOSE', { id }), { observations });
  }
}
