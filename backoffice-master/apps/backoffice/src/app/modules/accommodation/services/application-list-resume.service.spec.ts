import { TestBed } from '@angular/core/testing';
import { ApplicationListResumeService } from './application-list-resume.service';

describe('ApplicationListResumeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationListResumeService = TestBed.get(ApplicationListResumeService);
    expect(service).toBeTruthy();
  });
});
