import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { ApplicationModel } from '../modules/aco_applications/models/application.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationListResumeService extends Repository<ApplicationModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.APPLICATION_RESUME_LIST';
  }

}
