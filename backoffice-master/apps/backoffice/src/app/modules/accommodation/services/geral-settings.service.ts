import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { GeralSettingsModel } from '../models/geral-settings.model';

@Injectable({
  providedIn: 'root'
})
export class GeralSettingsService extends Repository<GeralSettingsModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.CONFIGURATIONS';
  }

  processUnprocessedContractedApplications() {
    return this.resourceService.create<boolean>(
      this.urlService.get('ACCOMMODATION.CONFIGURATIONS_PROCESS_UNPROCESSED_APPLICATIONS', {}), {});
  }


}
