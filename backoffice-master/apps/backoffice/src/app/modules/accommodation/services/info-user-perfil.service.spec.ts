import { TestBed } from '@angular/core/testing';

import { InfoUserPerfilService } from './info-user-perfil.service';

describe('InfoUserPerfilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfoUserPerfilService = TestBed.get(InfoUserPerfilService);
    expect(service).toBeTruthy();
  });
});
