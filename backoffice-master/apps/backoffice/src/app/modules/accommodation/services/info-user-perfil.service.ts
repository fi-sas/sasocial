
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class InfoUserPerfilService extends Repository<UserModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.USERS_ID';
  }
}