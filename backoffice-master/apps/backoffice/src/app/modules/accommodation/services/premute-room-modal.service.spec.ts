import { TestBed } from '@angular/core/testing';

import { PremuteRoomModalService } from './premute-room-modal.service';

describe('PremuteRoomModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PremuteRoomModalService = TestBed.get(PremuteRoomModalService);
    expect(service).toBeTruthy();
  });
});
