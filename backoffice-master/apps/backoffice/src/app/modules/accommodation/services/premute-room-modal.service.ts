import { Injectable } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { PremuteRoomComponent } from '../components/premute-room/premute-room.component';
import { ApplicationModel } from '../modules/aco_applications/models/application.model';
import { ApplicationsService } from '../modules/aco_applications/services/applications.service';
import { catchError, finalize, first } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PremuteRoomModalService {
  loading = false;

  constructor(
    private modalService: NzModalService,
    private uiService: UiService,
    private applicationsService: ApplicationsService
  ) { }

  openModal(application: ApplicationModel, residenceId?: number): Observable<boolean> {
    return new Observable(observer => {
      this.modalService.create({
        nzComponentParams: {
          application,
          residenceId
        },
        nzContent: PremuteRoomComponent,
        nzTitle: 'Permutar quarto',
        nzOkText: 'Permutar',
        nzOkLoading: this.loading,
        nzOnOk: (comp) => {
          return new Promise((resolve) => {
  
            if (!comp.application.id) {
              this.uiService.showMessage(MessageType.warning, 'Sem candidatura selecionada');
              return resolve(false);
            }
            
            if (!comp.premute_with_id) {
              this.uiService.showMessage(MessageType.warning, 'Selecione um candidato para a troca');
              return resolve(false);
            }
  
            this.loading = true;
            this.applicationsService.premuteRoom(comp.application.id, comp.premute_with_id)
              .pipe(
                first(),
                finalize(() => this.loading = false),
                catchError(() => of(this.loading = false))
              ).subscribe(result => {
                observer.next(true);
                observer.complete();
                return resolve();
              });
          });
        },
        nzOnCancel: () => {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }
}
