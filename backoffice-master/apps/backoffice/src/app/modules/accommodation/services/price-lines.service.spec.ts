import { TestBed } from '@angular/core/testing';

import { PriceLinesService } from './price-lines.service';

describe('PriceLinesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PriceLinesService = TestBed.get(PriceLinesService);
    expect(service).toBeTruthy();
  });
});
