import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { PriceLineModel } from '@fi-sas/backoffice/modules/accommodation/models/price-line.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class PriceLinesService extends Repository<PriceLineModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACCOMMODATION.PRICES';
    this.entity_url = 'ACCOMMODATION.PRICES_ID';
  }
}
