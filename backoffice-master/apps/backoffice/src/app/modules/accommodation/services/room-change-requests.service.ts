import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { RoomChangeRequestModel } from '../models/Room-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class RoomChangeRequestsService extends Repository<RoomChangeRequestModel> {
  entities_url = 'ACCOMMODATION.ROOM_CHANGE_REQUEST';
  entity_url = 'ACCOMMODATION.ROOM_CHANGE_REQUEST_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
  }

  status(id: number, event: string, application_room_change, notes) {
    return this.resourceService.create<RoomChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST_STATUS', { id }), { event, application_room_change, notes });
  }

  admin_approve(id: number, application_room_change, notes) {
    return this.resourceService.create<RoomChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST_APPROVE', { id }), { application_room_change, notes });
  }

  admin_reject(id: number, application_room_change, notes) {
    return this.resourceService.create<RoomChangeRequestModel>(
      this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST_REJECT', { id }), { application_room_change, notes });
  }
}
