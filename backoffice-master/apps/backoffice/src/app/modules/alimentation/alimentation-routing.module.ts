import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { AlimentationComponent } from '@fi-sas/backoffice/modules/alimentation/alimentation.component';
import { ChooseEntityComponent } from '@fi-sas/backoffice/modules/alimentation/pages/choose-entity/choose-entity.component';
import { EntityGuard } from '@fi-sas/backoffice/modules/alimentation/guards/entity.guard';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: AlimentationComponent,
    children: [
      {
        path: 'choose-entity',
        component: ChooseEntityComponent,
        data: { breadcrumb: 'Entidade', title: 'Escolher entidade' },
      },
      {
        path: 'unit',
        loadChildren: './pages/units/units.module#UnitsModule',
        data: { breadcrumb: 'Unidades', title: 'Unidades' },
        canActivate: [EntityGuard],
      },
      {
        path: 'entity',
        loadChildren: './pages/entity/entity.module#EntityModule',
        data: { breadcrumb: 'Entidades', title: 'Entidades' },
        canActivate: [],
      },
      {
        path: 'nutrient',
        loadChildren: './pages/nutrient/nutrient.module#NutrientModule',
        data: { breadcrumb: 'Nutrientes', title: 'Nutrientes' },
        canActivate: [EntityGuard],
      },
      {
        path: 'allergen',
        loadChildren: './pages/allergen/allergen.module#AllergenModule',
        data: { breadcrumb: 'Alergénios', title: 'Alergénios' },
        canActivate: [EntityGuard],
      },
      {
        path: 'wharehouse',
        loadChildren: './pages/wharehouse/wharehouse.module#WharehouseModule',
        data: { breadcrumb: 'Armazéns', title: 'Armazéns' },
        canActivate: [EntityGuard],
      },
      {
        path: 'family',
        loadChildren: './pages/family/family.module#FamilyModule',
        data: { breadcrumb: 'Famílias', title: 'Famílias' },
        canActivate: [EntityGuard],
      },
      {
        path: 'service',
        loadChildren: './pages/service/service.module#ServiceModule',
        data: { breadcrumb: 'Serviços', title: 'Serviços' },
        canActivate: [EntityGuard],
      },
      {
        path: 'complement',
        loadChildren: './pages/complement/complement.module#ComplementModule',
        data: { breadcrumb: 'Complementos', title: 'Complementos' },
        canActivate: [EntityGuard],
      },
      {
        path: 'product',
        loadChildren: './pages/product/product.module#ProductModule',
        data: { breadcrumb: 'Produtos', title: 'Produtos' },
        canActivate: [EntityGuard],
      },
      {
        path: 'recipe',
        loadChildren: './pages/recipe/recipe.module#RecipeModule',
        data: { breadcrumb: 'Receitas', title: 'Receitas' },
        canActivate: [EntityGuard],
      },
      {
        path: 'dishtype',
        loadChildren: './pages/dishtype/dishtype.module#DishtypeModule',
        data: { breadcrumb: 'Tipos Pratos', title: 'Tipos Pratos' },
        canActivate: [EntityGuard],
      },
      {
        path: 'dish',
        loadChildren: './pages/dish/dish.module#DishModule',
        data: { breadcrumb: 'Pratos', title: 'Pratos' },
        canActivate: [EntityGuard],
      },
      {
        path: 'menus',
        loadChildren: './pages/menus/menus.module#MenusModule',
        data: { breadcrumb: 'Ementas', title: 'Ementas' },
        canActivate: [EntityGuard],
      },
      {
        path: 'stock',
        loadChildren: './pages/stock/stock.module#StockModule',
        data: { breadcrumb: 'Stocks', title: 'Stocks' },
        canActivate: [EntityGuard],
      },
      {
        path: 'reservation',
        loadChildren:
          './pages/reservation/reservation.module#ReservationModule',
        data: { breadcrumb: 'Reservas Cantina', title: 'Reservas Cantina' },
        canActivate: [EntityGuard],
      },
      {
        path: 'order',
        loadChildren: './pages/order/order.module#OrderModule',
        data: { breadcrumb: 'Pedidos Bar', title: 'Pedidos Bar' },
        canActivate: [EntityGuard],
      },
      {
        path: 'packs',
        loadChildren: './pages/packs/packs.module#PacksModule',
        data: { breadcrumb: 'Packs', title: 'Packs' },
        canActivate: [EntityGuard],
      },
      {
        path: 'configurations',
        loadChildren:
          './pages/food-configurations/food-configurations.module#FoodConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações' },
        canActivate: [EntityGuard],
      },
      {
        path: 'haccp',
        loadChildren: './pages/haccp/haccp.module#HaccpModule',
        data: { breadcrumb: 'HACCP', title: 'HACCP' },
        canActivate: [EntityGuard],
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada',
        },
      },
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlimentationRoutingModule {}
