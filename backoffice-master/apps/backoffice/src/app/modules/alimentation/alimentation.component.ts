import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-alimentation',
  template: '<router-outlet></router-outlet>',
})
export class AlimentationComponent implements OnInit {
  dataConfiguration: any;
  constructor(
    private uiService: UiService,
    private alimentationService: AlimentationService,
    private configurationsService: ConfigurationGeralService,
    private authService: AuthService
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    if (this.authService.hasPermission('alimentation:entities:read')) {
      this.alimentationService.getSelectedEntity();
    }
    this.uiService.setSiderTitle(this.validTitleTraductions(2), 'coffee');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const entChoose = new SiderItem(
      'Escolher entidade',
      '',
      '/alimentation/choose-entity',
      'alimentation:entities:read'
    );
    this.uiService.addSiderItem(entChoose);
    this.alimentationService.seletedEntityObservable().subscribe((entity) => {
      entChoose.name = entity
        ? 'Entidade [' + entity.name + ']'
        : 'Escolher entidade';
    });

    if (this.authService.hasPermission('alimentation:menus')) {
      const menus = new SiderItem('Ementas');
      menus.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/menus/list',
          'alimentation:menus:read'
        )
      );
      menus.addChild(
        new SiderItem(
          'Criar várias',
          '',
          '/alimentation/menus/bulk',
          'alimentation:menus:create'
        )
      );
      this.uiService.addSiderItem(menus);
    }

    if (this.authService.hasPermission('alimentation:stocks')) {
      const stocks = new SiderItem('Stocks');
      stocks.addChild(
        new SiderItem(
          'Criar movimento',
          '',
          '/alimentation/stock/create',
          'alimentation:stocks:create'
        )
      );
      stocks.addChild(
        new SiderItem(
          'Análise armazéns',
          '',
          '/alimentation/stock/list-wharehouses',
          'alimentation:stocks:read'
        )
      );
      stocks.addChild(
        new SiderItem(
          'Análise lotes',
          '',
          '/alimentation/stock/list-lotes',
          'alimentation:stocks:read'
        )
      );
      stocks.addChild(
        new SiderItem(
          'Análise movimentos',
          '',
          '/alimentation/stock/list-movements',
          'alimentation:stocks:read'
        )
      );
      this.uiService.addSiderItem(stocks);
    }

    if (this.authService.hasPermission('alimentation:reservations')) {
      const reservations = new SiderItem('Reservas de cantina');
      reservations.addChild(
        new SiderItem(
          'Contagens',
          '',
          '/alimentation/reservation/counts',
          'alimentation:reservations:read'
        )
      );
      reservations.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/reservation/list',
          'alimentation:reservations:read'
        )
      );
      reservations.addChild(
        new SiderItem(
          'Relatórios',
          '',
          '/alimentation/reservation/reports',
          'alimentation:reservations:read'
        )
      );
      this.uiService.addSiderItem(reservations);
    }

    const orders = new SiderItem('Pedidos de bar');
    orders.addChild(
      new SiderItem(
        'Listar',
        '',
        '/alimentation/order/list',
        'alimentation:orders:read'
      )
    );
    orders.addChild(
      new SiderItem(
        'Relatórios',
        '',
        '/alimentation/order/reports',
        'alimentation:orders:read'
      )
    );
    this.uiService.addSiderItem(orders);

    const user_packs = new SiderItem(
      'Packs',
      '',
      '/alimentation/packs/users',
      'alimentation:packs:read'
    );
    this.uiService.addSiderItem(user_packs);

    if (this.authService.hasPermission('haccp')) {
      const haccp = new SiderItem('HACCP');

      const option1 = haccp.addChild(
        new SiderItem('Temperaturas de equipamentos frio')
      );
      option1.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/equipment_temperatures/create',
          'haccp:equipment_temperatures:create'
        )
      );
      option1.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/equipment_temperatures/list',
          'haccp:equipment_temperatures:read'
        )
      );

      const option2 = haccp.addChild(
        new SiderItem('Temperaturas de alimentos')
      );
      option2.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/food_temperatures/create',
          'haccp:food_temperatures:create'
        )
      );
      option2.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/food_temperatures/list',
          'haccp:food_temperatures:read'
        )
      );

      const option3 = haccp.addChild(
        new SiderItem('Temp. de alimentos no transporte')
      );
      option3.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/food_transport_temperatures/create',
          'haccp:food_transport_temperatures:create'
        )
      );
      option3.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/food_transport_temperatures/list',
          'haccp:food_transport_temperatures:read'
        )
      );

      const option4 = haccp.addChild(new SiderItem('Higienizar ingredientes'));
      option4.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/sanitize_ingredients/create',
          'haccp:sanitize_ingredients:create'
        )
      );
      option4.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/sanitize_ingredients/list',
          'haccp:sanitize_ingredients:read'
        )
      );

      const option5 = haccp.addChild(
        new SiderItem('Colheita de amostra testemunho')
      );
      option5.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/testimony_sample_collections/create',
          'haccp:testimony_sample_collections:create'
        )
      );
      option5.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/testimony_sample_collections/list',
          'haccp:testimony_sample_collections:read'
        )
      );

      const option6 = haccp.addChild(
        new SiderItem('Higienização de equipamentos')
      );
      option6.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/equipment_sanitization/create',
          'haccp:equipment_sanitizations:create'
        )
      );
      option6.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/equipment_sanitization/list',
          'haccp:equipment_sanitizations:read'
        )
      );

      const option7 = haccp.addChild(
        new SiderItem('Estado óleos de fritura')
      );
      option7.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/state_frying_oil/create',
          'haccp:state_frying_oil:create'
        )
      );
      option7.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/state_frying_oil/list',
          'haccp:state_frying_oil:read'
        )
      );

      const option8 = haccp.addChild(new SiderItem('Fichas de produção'));
      option8.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/production_sheet/create',
          'haccp:production_sheet:create'
        )
      );
      option8.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/production_sheet/list',
          'haccp:production_sheet:read'
        )
      );

      const option9 = haccp.addChild(new SiderItem('Custo de refeição'));
      option9.addChild(
        new SiderItem(
          'Novo registo',
          '',
          '/alimentation/haccp/meal_cost/',
          'haccp:production_sheet:read'
        )
      );
      option9.addChild(
        new SiderItem(
          'Listagem',
          '',
          '/alimentation/haccp/packs/list',
          'haccp:packs:read'
        )
      );

      const haccpConf = new SiderItem('Configurações HACCP');
      const equipments = haccpConf.addChild(new SiderItem('Equipamentos', ''));
      equipments.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/haccp/equipments/list',
          'haccp:equipments:read'
        )
      );
      equipments.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/haccp/equipments/create',
          'haccp:equipments:create'
        )
      );

      const anomalies = haccpConf.addChild(new SiderItem('Anomalias', ''));
      anomalies.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/haccp/anomalies/list',
          'haccp:anomalies:read'
        )
      );
      anomalies.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/haccp/anomalies/create',
          'haccp:anomalies:create'
        )
      );

      const food_temperature_classes = haccpConf.addChild(
        new SiderItem('Class. de temp. alim.', '')
      );
      food_temperature_classes.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/haccp/food_temperature_classes/list',
          'haccp:food_temperature_classes:read'
        )
      );
      food_temperature_classes.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/haccp/food_temperature_classes/create',
          'haccp:food_temperature_classes:create'
        )
      );
      this.uiService.addSiderItem(haccp);
      this.uiService.addSiderItem(haccpConf);
    }

    const configurations = new SiderItem('Configurações');
    if (this.authService.hasPermission('alimentation:units')) {
      const units = configurations.addChild(new SiderItem('Unidades'));
      units.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/unit/create',
          'alimentation:units:create'
        )
      );
      units.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/unit/list',
          'alimentation:units:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:entities')) {
      const entities = configurations.addChild(new SiderItem('Entidades'));
      entities.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/entity/create',
          'alimentation:entities:create'
        )
      );
      entities.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/entity/list',
          'alimentation:entities:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:nutrients')) {
      const nutrients = configurations.addChild(new SiderItem('Nutrientes'));
      nutrients.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/nutrient/create',
          'alimentation:nutrients:create'
        )
      );
      nutrients.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/nutrient/list',
          'alimentation:nutrients:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:allergens')) {
      const allergens = configurations.addChild(new SiderItem('Alergénios'));
      allergens.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/allergen/create',
          'alimentation:allergens:create'
        )
      );
      allergens.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/allergen/list',
          'alimentation:allergens:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:wharehouses')) {
      const wharehouses = configurations.addChild(new SiderItem('Armazéns'));
      wharehouses.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/wharehouse/create',
          'alimentation:wharehouses:create'
        )
      );
      wharehouses.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/wharehouse/list',
          'alimentation:wharehouses:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:families')) {
      const families = configurations.addChild(new SiderItem('Famílias'));
      families.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/family/create',
          'alimentation:families:create'
        )
      );
      families.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/family/list',
          'alimentation:families:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:services')) {
      const services = configurations.addChild(new SiderItem('Serviços'));
      services.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/service/create',
          'alimentation:services:create'
        )
      );
      services.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/service/list',
          'alimentation:services:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:complements')) {
      const complements = configurations.addChild(
        new SiderItem('Complementos')
      );
      complements.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/complement/create',
          'alimentation:complements:create'
        )
      );
      complements.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/complement/list',
          'alimentation:complements:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:products')) {
      const products = configurations.addChild(new SiderItem('Produtos'));
      products.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/product/create',
          'alimentation:products:create'
        )
      );
      products.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/product/list',
          'alimentation:products:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:recipes')) {
      const recipes = configurations.addChild(new SiderItem('Receitas'));
      recipes.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/recipe/create',
          'alimentation:recipes:create'
        )
      );
      recipes.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/recipe/list',
          'alimentation:recipes:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:dishs-types')) {
      const dishTypes = configurations.addChild(new SiderItem('Tipos Pratos'));
      dishTypes.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/dishtype/create',
          'alimentation:dishs-types:create'
        )
      );
      dishTypes.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/dishtype/list',
          'alimentation:dishs-types:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:dishs')) {
      const dishs = configurations.addChild(new SiderItem('Pratos'));
      dishs.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/dish/create',
          'alimentation:dishs:create'
        )
      );
      dishs.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/dish/list',
          'alimentation:dishs:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:packs')) {
      const packs = configurations.addChild(new SiderItem('Packs'));
      packs.addChild(
        new SiderItem(
          'Criar',
          '',
          '/alimentation/packs/create',
          'alimentation:packs:create'
        )
      );
      packs.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/packs/list',
          'alimentation:packs:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:packs')) {
      const userDefaults = configurations.addChild(
        new SiderItem('Configs. utilizadores')
      );
      userDefaults.addChild(
        new SiderItem(
          'Listar',
          '',
          '/alimentation/packs/user_defaults',
          'alimentation:packs:read'
        )
      );
    }

    if (this.authService.hasPermission('alimentation:configurations')) {
      configurations.addChild(
        new SiderItem(
          'Gerais',
          '',
          '/alimentation/configurations/general',
          'alimentation:configurations:create'
        )
      );
    }

    if ((configurations.childs || []).length) {
      this.uiService.addSiderItem(configurations);
    }

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find((t) => t.id === id) != null &&
      this.dataConfiguration.find((t) => t.id === id) != undefined
      ? this.dataConfiguration.find((t) => t.id === id).translations
      : [];
  }
}
