import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlimentationRoutingModule } from '@fi-sas/backoffice/modules/alimentation/alimentation-routing.module';
import { AlimentationComponent } from './alimentation.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ChooseEntityComponent } from './pages/choose-entity/choose-entity.component';
import { EntityService } from '@fi-sas/backoffice/modules/alimentation/services/entity.service';
import { DevicesService } from '@fi-sas/backoffice/modules/configurations/services/devices.service';
import { AllergensService } from '@fi-sas/backoffice/modules/alimentation/services/allergens.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { UnitsService } from '@fi-sas/backoffice/modules/alimentation/services/units.service';
import { NutrientsService } from '@fi-sas/backoffice/modules/alimentation/services/nutrients.service';
import { WharehousesService } from '@fi-sas/backoffice/modules/alimentation/services/wharehouses.service';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { ComplementsService } from '@fi-sas/backoffice/modules/alimentation/services/complements.service';
import { MenusService } from '@fi-sas/backoffice/modules/alimentation/services/menus.service';
import { DishsService } from '@fi-sas/backoffice/modules/alimentation/services/dishs.service';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { UsersService } from '../users/modules/users_users/services/users.service';
import { ProfilesService } from '../users/modules/profiles/services/profiles.service';
import { OrdersService } from './services/orders.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AlimentationRoutingModule
  ],
  declarations: [
    AlimentationComponent,
    ChooseEntityComponent,
  ],
  providers: [
    UsersService,
    ProfilesService,
    DevicesService,
    LanguagesService,
    EntityService,
    AllergensService,
    UnitsService,
    NutrientsService,
    WharehousesService,
    FamiliesService,
    ServicesService,
    ComplementsService,
    ProductsService,
    TaxesService,
    MenusService,
    DishsService,
    DishTypesService,
    RecipesService,
    OrdersService
  ],
})
export class AlimentationModule {}
