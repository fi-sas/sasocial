import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyMenuDayComponent } from './copy-menu-day.component';

describe('CopyMenuDayComponent', () => {
  let component: CopyMenuDayComponent;
  let fixture: ComponentFixture<CopyMenuDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyMenuDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyMenuDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
