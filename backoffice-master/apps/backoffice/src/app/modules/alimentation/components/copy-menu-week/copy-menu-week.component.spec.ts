import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyMenuWeekComponent } from './copy-menu-week.component';

describe('CopyMenuWeekComponent', () => {
  let component: CopyMenuWeekComponent;
  let fixture: ComponentFixture<CopyMenuWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyMenuWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyMenuWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
