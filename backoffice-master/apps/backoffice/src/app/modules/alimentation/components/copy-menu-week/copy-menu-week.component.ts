import { MenusService } from './../../services/menus.service';
import { MenuModel } from './../../models/menu.model';
import { ServiceModel } from './../../models/service.model';
import { first, finalize } from 'rxjs/operators';
import { ServicesService } from './../../services/services.service';
import { NzModalRef } from 'ng-zorro-antd';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-copy-menu-week',
  templateUrl: './copy-menu-week.component.html',
  styleUrls: ['./copy-menu-week.component.less']
})
export class CopyMenuWeekComponent implements OnInit {
  submitted = false;
  sourceWeek: Date = null;
  sourceServiceId = null;

  isLoading = false;

  selectedWeek: Date = null;

  servicesLoading = false;
  services: ServiceModel[] = [];
  selectedServices: number[] = [];

  constructor(
    private servicesService: ServicesService,
    private menusService: MenusService,
    private modal: NzModalRef,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.loadServices();
  }

  loadServices() {
    this.servicesLoading = true;

    this.servicesService.list(0, -1, null, null, {
      type: 'canteen'
    }).pipe(first(), finalize(() => this.servicesLoading = false)).subscribe(result => {
      this.services = result.data;
    });
  }

  submit(): Promise<MenuModel[]> {
    this.submitted = true;
    return new Promise((resolve, reject) => {
    if (this.selectedWeek && this.selectedServices.length>0) {
      this.submitted = false;
      this.menusService.copyMenuWeek({
        from_day_of_week: moment(this.sourceWeek).format('YYYY-MM-DD'),
        from_service_id: this.sourceServiceId,
        to_day_of_week: moment(this.selectedWeek).format('YYYY-MM-DD'),
        service_ids: this.selectedServices,
      }).subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Ementa criada com sucesso'
        );
        resolve(result.data);
        this.isLoading = false;
        this.close();
      }, err => {
        reject(err);
        this.isLoading = false;
      });
    } else {
      this.isLoading = false;
      reject();
    }
  });
  }

  reset() {
    this.selectedWeek = null;
    this.selectedServices = [];
  }

  close() {
    this.modal.close();
  }

}
