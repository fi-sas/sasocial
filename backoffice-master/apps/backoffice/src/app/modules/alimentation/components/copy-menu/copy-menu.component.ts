import { NzModalRef } from 'ng-zorro-antd';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { first, finalize } from 'rxjs/operators';
import { MenusService } from '@fi-sas/backoffice/modules/alimentation/services/menus.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { Meal, MenuModel } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-copy-menu',
  templateUrl: './copy-menu.component.html',
  styleUrls: ['./copy-menu.component.less']
})
export class CopyMenuComponent implements OnInit {

  sourceMenu: MenuModel = null;
  submitted = false;
  Meal = Meal;
  selectedMeals: Meal[] = [];

  isLoading = false;
  
  selectedDates: Date[] = [
    moment().add(1, 'day').toDate()
  ];
  disabledDate = (current: Date): boolean => {
    return moment(current).isBefore(moment(), 'day');
  };


  servicesLoading = false;
  services: ServiceModel[] = [];
  selectedServices: number[] = [];

  constructor(
    private servicesService: ServicesService,
    private menusService: MenusService,
    private uiService: UiService,
    private modal: NzModalRef,
  ) { }

  ngOnInit() {
    this.loadServices();
  }

  loadServices() {
    this.servicesLoading = true;

    this.servicesService.list(0, -1, null, null, {
      type: 'canteen'
    }).pipe(first(), finalize(() => this.servicesLoading = false)).subscribe(result => {
      this.services = result.data;
    });
  }

  addDate(value: { nativeDate: Date }) {
    if (value && value.nativeDate) {
      const nd = moment(value.nativeDate);
      if (!this.selectedDates.find(d => moment(d).isSame(nd, 'day'))) {
        this.selectedDates.push(value.nativeDate);
      }
    }
  }

  submit(): Promise<MenuModel[]> {
    this.submitted = true;
    return new Promise((resolve, reject) => {
     if (this.sourceMenu && this.selectedMeals.length>0 && this.selectedServices.length>0 && this.selectedDates.length>0) {
      this.submitted = false;
      this.menusService.copyMenu(this.sourceMenu.id, {
        dates: this.selectedDates.map(d => moment(d).format('YYYY-MM-DD')),
        meals: this.selectedMeals,
        service_ids: this.selectedServices
      }).subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Ementa copiado com sucesso'
        );
        resolve(result.data);
        this.isLoading = false;
        this.close();
      }, err => {
        reject(err);
        this.isLoading = false;
      });
    } else {
      this.isLoading = false;
      reject();
    }
  });
  }

  reset() {
    this.selectedDates = [];
    this.selectedMeals = [];
    this.selectedServices = [];
  }

  close() {
    this.modal.close();
  }
}
