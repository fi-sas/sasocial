import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import {
  Meal,
  MenuDish,
  MenuModel,
} from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { MenusService } from '@fi-sas/backoffice/modules/alimentation/services/menus.service';
import { DishsService } from '@fi-sas/backoffice/modules/alimentation/services/dishs.service';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { NzModalRef } from 'ng-zorro-antd';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { FiConfigurator } from '@fi-sas/configurator';
import { cookieServiceFactory } from 'ngx-cookie';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'fi-sas-form-menu',
  templateUrl: './form-menu.component.html',
  styleUrls: ['./form-menu.component.less'],
})
export class FormMenuComponent implements OnInit {
  Meal = Meal;

  @Input()
  menu: MenuModel = null;

  @Input()
  meal: Meal = null;

  @Input()
  day: Date = null;

  @Input()
  service: ServiceModel = null;

  isLoading = false;
  isLoadingSelects = false;
  isUpdate = false;
  updateMenu: MenuModel = new MenuModel();
  menuDishCancel: MenuModel = new MenuModel();
  selectedDish: DishModel = null;
  selectedDishType: DishTypeModel = null;
  selectedAvailable = true;
  selectedDoses_Number = 0;

  dishList: DishModel[];
  dishTypeList: DishTypeModel[];

  searchChange$ = new BehaviorSubject('');

  defaultLang = 1;

  constructor(
    private menusService: MenusService,
    private dishsService: DishsService,
    private dishTypesService: DishTypesService,
    private uiService: UiService,
    private configurator: FiConfigurator,
    private modal: NzModalRef
  ) {
    this.defaultLang = configurator.getOption('DEFAULT_LANG_ID', 1);
  }

  ngOnInit() {
    this.updateMenu.dishs = [];

    this.isLoadingSelects = true;
    this.dishTypesService
      .list(1, -1, null, null, {
        withRelated: 'translations',
        active: true,
      })
      .subscribe((res) => {
        this.dishTypeList = res.data;
        this.isLoadingSelects = false;
      });

    if (this.menu) {
      this.updateMenu = this.menu;
      this.menuDishCancel = _.cloneDeep(this.menu);

      this.isUpdate = true;
    }

    const dishsList$: Observable<DishModel[]> = this.searchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isLoadingSelects = true;
          return this.dishsService.list(1, 10, null, null, {
            withRelated: 'translations',
            types: this.selectedDishType ? [this.selectedDishType.id] : [],
            search: event,
            active: true,
            searchFields: 'description,name',
            dish_type_id: this.selectedDishType ? this.selectedDishType.id : [],
          });
        })
      )
      .pipe(map((res) => res.data));

    dishsList$.subscribe((data) => {
      this.dishList = data;
      this.isLoadingSelects = false;
    });
  }

  removeDish(dish) {
    const index = this.updateMenu.dishs.indexOf(dish);
    if (index > -1) {
      this.updateMenu.dishs.splice(index, 1);
    }
  }

  submit(): Observable<MenuModel> {
    const obsMenu = new BehaviorSubject(this.updateMenu);
    this.isLoading = true;
    /*if (this.updateMenu.dishs.length === 0) {
      this.uiService.showMessage(
        MessageType.warning,
        'Deve adicionar pratos a lista'
      );
      this.isLoading = false;
    } else {*/
    let error = false;
    this.updateMenu.dishs.forEach((dish) => {
      if (dish.doses_available < 0) {
        error = true;
      }
    });
    if (error) {
      this.uiService.showMessage(
        MessageType.error,
        'Não pode haver quantidades negativas'
      );
      this.isLoading = false;
    } else {
      const value: any = {};
      value.meal = this.meal;
      value.service_id = this.service.id;
      value.date = moment(this.day).format('YYYY-MM-DD');
      value.dishs = this.updateMenu.dishs.map((dm) => ({
        dishType_id: dm.type.id,
        dish_id: dm.dish.id,
        doses_available: dm.doses_available,
        available: dm.available,
      }));
      if (this.isUpdate) {
        this.menusService.update(this.updateMenu.id, value).subscribe(
          (result) => {
            this.isLoading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Ementa alterada com sucesso'
            );
            this.close();
            this.menusService.get(this.updateMenu.id).subscribe((result) => {
              this.updateMenu = result.data[0];
              obsMenu.next(this.updateMenu);
              obsMenu.complete();
            });
          },
          (err) => {
            this.isLoading = false;
            obsMenu.error(err);
          }
        );
      } else {
        this.menusService.create(value).subscribe(
          (result) => {
            this.isLoading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Ementa criada com sucesso'
            );
            this.close();
            this.menusService.get(result.data[0].id).subscribe((result) => {
              this.updateMenu = result.data[0];
              obsMenu.next(this.updateMenu);
              obsMenu.complete();
            });
          },
          (err) => {
            this.isLoading = false;
            obsMenu.error(err);
          }
        );
      }
    }

    //}
    return obsMenu.asObservable();
  }

  close() {
    this.modal.destroy();
  }

  addDish() {
    if (!this.selectedDishType) {
      this.uiService.showMessage(
        MessageType.warning,
        'Selecione um tipo de prato'
      );
      return;
    }

    if (!this.selectedDish) {
      this.uiService.showMessage(MessageType.warning, 'Selecione um prato');
      return;
    }

    if (!this.selectedDoses_Number) {
      this.selectedDoses_Number = 0;
    }

    if (
      this.updateMenu.dishs.findIndex(
        (dishM) =>
          dishM.dish.id === this.selectedDish.id &&
          dishM.type.id === this.selectedDishType.id
      ) !== -1
    ) {
      this.uiService.showMessage(
        MessageType.warning,
        'Já exite esse prato e tipo de prato na lista'
      );
      return;
    }

    const tmd = new MenuDish();
    tmd.type = this.selectedDishType;
    tmd.dish = this.selectedDish;
    tmd.doses_available = this.selectedDoses_Number;
    tmd.available = this.selectedAvailable;
    this.updateMenu.dishs = [tmd, ...this.updateMenu.dishs];
    this.selectedDish = null;
    this.selectedDoses_Number = 0;
    this.selectedAvailable = true;
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  onTypeChanged(value: number) {
    this.dishList = [];
    this.searchChange$.next('');
    this.selectedDish = null;
  }
}
