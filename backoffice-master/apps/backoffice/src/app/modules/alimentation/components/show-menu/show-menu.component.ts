import { CopyMenuComponent } from './../copy-menu/copy-menu.component';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Meal, MenuModel } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { NzModalService } from 'ng-zorro-antd';
import { FormMenuComponent } from '@fi-sas/backoffice/modules/alimentation/components/form-menu/form-menu.component';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { MenusService } from '@fi-sas/backoffice/modules/alimentation/services/menus.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-show-menu',
  templateUrl: './show-menu.component.html',
  styleUrls: ['./show-menu.component.less']
})
export class ShowMenuComponent implements OnInit {
  Meal = Meal;

  @Output()
  update: EventEmitter<MenuModel> = new EventEmitter();

  @Input()
  menu: MenuModel = null;

  @Input()
  meal: Meal = null;

  @Input()
  day: Date = null;

  @Input()
  service: ServiceModel = null;

  constructor(
    private uiService: UiService,
    private modalService: NzModalService,
    private menusService: MenusService,
  ) { }

  ngOnInit() {
  }

  showModal() {
    const modal = this.modalService.create({
      nzTitle: 'Ementa',
      nzClosable: false,
      nzWidth: 1000,
      nzBodyStyle: { padding: '0px' },
      nzContent: FormMenuComponent,
      nzComponentParams: {
        meal: this.meal,
        day: this.day,
        service: this.service,
        menu: this.menu
      },
      nzOnOk: instance => {
        instance.submit().subscribe(mm => {
          if (mm.dishs.length > 0) {
            this.menu = mm;
            this.update.emit(this.menu);
          }
        });
        return false;
      },
      nzOnCancel: instance => {
        this.update.emit(instance.menuDishCancel);
      }
    });

    modal.open();
  }

  showValidationModal() {
    this.modalService.confirm({
      nzContent: this.menu.validated ? 'Pretende rejeitar esta ementa?' : 'Pretende aceitar esta ementa?',
      nzOkText: this.menu.validated ? 'Rejeitar' : 'Aceitar',
      nzOnOk: instance => {
        if (this.menu.validated) {
          this.menusService.reject(this.menu.id).subscribe(value => {
            this.menusService.get(this.menu.id).subscribe(
              result => {
                this.menu = result.data[0];
              });
            this.uiService.showMessage(MessageType.success, 'Ementa rejeita com sucesso');
          });
        } else {
          this.menusService.accept(this.menu.id).subscribe(value => {
            this.menusService.get(this.menu.id).subscribe(
              result => {
                this.menu = result.data[0];
              });
            this.uiService.showMessage(MessageType.success, 'Ementa aceite com sucesso');
          });
        }
      }
    })
  }

  canCreateMeal() {
    if (this.day) {
      return moment(this.day).isBefore(moment(), 'day');
    } else {
      return false;
    }
  }

  copyMeal(menu: MenuModel) {
    if (menu) {
      this.modalService.create({
        nzTitle: 'Copiar ementa',
        nzContent: CopyMenuComponent,
        nzComponentParams: {
          sourceMenu: menu
        },
        nzClosable: false,
        nzCancelText: 'Limpar',
        nzFooter: [
          {
            label: 'Limpar',
            onClick: componentInstance => {
              componentInstance.reset();
            }
          },
          {
            label: 'Cancelar',
            type: 'danger',
            onClick: componentInstance => {
              componentInstance.close();
            }
          },
          {
            label: 'Copiar',
            type: 'primary',
            onClick: componentInstance => {
              return componentInstance.submit().then(data => {
                data.map(m => this.update.emit(m));
              });
            }
          }
        ]
      });
    }
  }

  copyToDinner() {
    if (this.menu && this.service) {
      this.menusService.copyMenu(this.menu.id, {
        dates: [moment(this.menu.date).format('YYYY-MM-DD')],
        meals: [Meal.DINNER],
        service_ids: [this.service.id]
      }).subscribe(result => {
        result.data.map(m => {
          this.update.emit(m);
        });
        this.uiService.showMessage(MessageType.success, 'Ementa copiada com sucesso para jantar');
      })
    }
  }
}
