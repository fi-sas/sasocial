
export interface CopyDayDto {
    from_day: string;
    from_service_id: number;
    service_ids: number[];
    dates: string[];
    meals: string[];
      
}