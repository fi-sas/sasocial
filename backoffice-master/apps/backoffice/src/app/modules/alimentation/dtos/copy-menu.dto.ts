import { Meal } from "../models/menu.model";

export interface CopyMenuDto{
    service_ids: number[];
    meals: Meal[];
    dates: string[];
      
}