
export interface CopyWeekDto{
    from_day_of_week: string;
    from_service_id: number;
    service_ids: number[];
    to_day_of_week: string;
}