import { MenuDishDto } from './menu-dish.dto';
import { Meal } from "../models/menu.model";

export interface MenuBulkDto{
    service_ids: number[];
    meals: Meal[];
    dishs: MenuDishDto[];
    dates: string[];
      
}