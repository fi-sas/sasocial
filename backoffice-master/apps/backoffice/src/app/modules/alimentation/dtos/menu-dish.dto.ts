export interface MenuDishDto {
    dishType_id: number,
    dish_id: number,
    doses_available: number,
    available: boolean
}