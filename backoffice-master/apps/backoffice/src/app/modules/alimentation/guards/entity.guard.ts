import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';

@Injectable({
  providedIn: 'root'
})
export class EntityGuard implements CanActivate {

  constructor(
    private alimentationService: AlimentationService,
    private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.alimentationService.getSelectedEntity())
      return true;
    else
      this.router.navigate(['alimentation', 'choose-entity'], {
        queryParams: { returnUrl: state.url }
      });
      return false;
  }
}
