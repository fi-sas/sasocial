export class AllergenTranslationModel {
  language_id: number;
  name: string;
  description: string;
}

export class AllergenModel {
  id: number;
  translations: AllergenTranslationModel[];
  active: boolean;
}
