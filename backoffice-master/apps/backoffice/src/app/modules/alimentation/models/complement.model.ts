

export class ComplementTranslationModel {
  language_id: number;
  name: string;
  description: string;
}

export class ComplementModel {
  id: number;
  translations: ComplementTranslationModel[];
  active: boolean;
}
