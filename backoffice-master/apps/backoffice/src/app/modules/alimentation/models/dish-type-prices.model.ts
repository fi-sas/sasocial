import { TaxModel } from "../../configurations/models/tax.model";
import { ProfileModel } from "../../users/modules/profiles/models/profile.model";

export class DishTypePriceModel {
    id: number;
    active: boolean;
    description: string;
    tax_id: number;
    profile_id: number;
    meal: string;
    time: string;
    price: number;
    dish_type_id: number
    tax: TaxModel;
    profile: ProfileModel;
  }