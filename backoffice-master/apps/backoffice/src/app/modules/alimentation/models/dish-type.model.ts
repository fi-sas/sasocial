import { PriceVariationModel } from '@fi-sas/backoffice/modules/alimentation/models/price-variation.model';
import { DisponibilityModel } from '@fi-sas/backoffice/modules/alimentation/models/disponibility.model';

export class DishTypeTranslationModel {
  language_id: number;
  name: string;
}


export class DishTypeModel {
  id: number;
  translations: DishTypeTranslationModel[];
  tax_id: number;
  code: string;
  price: number;
  prices: PriceVariationModel[];
  disponibility_lunch: DisponibilityModel;
  disponibility_dinner: DisponibilityModel;
  pos_operations: boolean;
  active: boolean;
  pack_available: boolean;

  excluded_user_profile_ids: number[];
}
