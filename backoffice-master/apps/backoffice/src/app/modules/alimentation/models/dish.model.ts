import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';

export class DishTranslationModel {
  language_id: number;
  name: string;
  description: string;
}

export class DishModel {
  id: number;
  translations: DishTranslationModel[];
  recipes: RecipeModel[];
  type: DishTypeModel[];
  types_ids: number[];
  file: FileModel;
  active: boolean;
  file_id: number;
}
