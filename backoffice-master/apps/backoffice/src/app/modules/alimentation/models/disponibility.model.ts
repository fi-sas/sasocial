export class DisponibilityModel {
 begin_date: Date;
 end_date: Date;
 monday: boolean;
 tuesday: boolean;
 wednesday: boolean;
 thursday: boolean;
 friday: boolean;
 saturday: boolean;
 sunday: boolean;
 minimum_hour: Date;
 maximum_hour: Date;
 annulment_maximum_hour: Date;
}
