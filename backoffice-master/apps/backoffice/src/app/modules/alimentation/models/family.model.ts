import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ProductModel } from '@fi-sas/backoffice/modules/alimentation/models/product.model';

export class FamilyTranslationModel {
  language_id: number;
  name: string;
}

export class FamilyModel {
  id: number;
  translations: FamilyTranslationModel[];
  file_id: number;
  file: FileModel;
  products: ProductModel[];
  active: boolean;
}
