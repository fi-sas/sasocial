import { UsersModule } from '@fi-sas/backoffice/modules/users/users.module';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export class FiscalEntityModel {
  id: number;
  name: string;
  tin: number;
  street: string;
  door: number;
  city: string;
  cep: string;
  phone: string;
  email: string;
  website: string;
  logo_file_id: number;
  active: boolean;
  users: UserModel[];
  account_id: number;
}
