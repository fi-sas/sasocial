export class HistoryStockModel {
    id: number;
    created_at: Date;
    lote: string;
    operation: string;
    order_id: number;
    stock_reason: string;
    quantity: number;
    quantity_after: number;
    quantity_before: number;
    stock_id: number;
    user_id: number;
}