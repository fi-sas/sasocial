import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';

export enum Meal {
  LUNCH = 'lunch',
  DINNER = 'dinner',
  BREAKFAST = 'breakfast'
}

export class MenuDish {
  dish: DishModel;
  type: DishTypeModel;
  available: boolean;
  doses_available: number;
}

export class MenuModel {
  id: number;
  meal: Meal;
  service_id: number;
  service: ServiceModel;
  dishs: MenuDish[];
  date: string;
  validated: boolean;
}
