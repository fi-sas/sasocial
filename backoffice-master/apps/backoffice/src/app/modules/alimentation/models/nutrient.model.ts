import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';

export class NutrientTranslationModel {
  language_id: number;
  name: string;
}

export class NutrientModel {
  id: number;
  translations: NutrientTranslationModel[];
  unit: UnitModel;
  ddr: number;
  active: boolean;
  unit_id: number;
}
