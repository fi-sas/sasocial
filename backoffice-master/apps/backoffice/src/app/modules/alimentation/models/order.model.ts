import { ProductModel } from "./product.model";

export class OrderLineModel {
    id: number;
    quantity: number;
    order_id: number;
    product_id: Date;
    product: ProductModel;
    service_id: number;
    order_lines:[]

  }

export class OrdersModel {
    id: number;
    served: boolean;
    user_id: number;
    served_at: Date;
    served_by_user_id: number;
    service_id: number;
    order_lines: OrderLineModel[]
    status: string;
  }