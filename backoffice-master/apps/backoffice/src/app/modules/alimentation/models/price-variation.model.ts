import { Meal } from "./menu.model";

export class PriceVariationModel {
  id: number;
  description: string;
  tax_id: number;
  profile_id: number;
  price: number;
  time: Date;
  meal: Meal;
}
