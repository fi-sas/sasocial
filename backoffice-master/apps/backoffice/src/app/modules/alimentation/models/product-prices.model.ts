import { TaxModel } from "../../configurations/models/tax.model";
import { ProfileModel } from "../../users/modules/profiles/models/profile.model";

export class ProductPriceModel {
    id: number;
    active: boolean;
    description: string;
    tax_id: number;
    profile_id: number;
    meal: string;
    time: string;
    price: number;
    product_id: number;
    tax: TaxModel;
    profile: ProfileModel;
  }
  