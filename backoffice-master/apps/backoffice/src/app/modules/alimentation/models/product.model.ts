import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { AllergenModel } from '@fi-sas/backoffice/modules/alimentation/models/allergen.model';
import { NutrientModel } from '@fi-sas/backoffice/modules/alimentation/models/nutrient.model';
import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';
import { DisponibilityModel } from '@fi-sas/backoffice/modules/alimentation/models/disponibility.model';
import { ServiceType } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { PriceVariationModel } from '@fi-sas/backoffice/modules/alimentation/models/price-variation.model';
import { ComplementModel } from '@fi-sas/backoffice/modules/alimentation/models/complement.model';
import { FamilyModel } from './family.model';

export enum TrackStock {
  PRODUCT = 'product',
  COMPOSITION = 'composition',
  NONE = 'none',
  PRODUCT_COMPOSITION = 'product_composition'
}

export class ProductTranslationModel {
  language_id: number;
  name: string;
  description: string;
}

export class CompoundModel {
  compound: ProductModel;
  quantity: number;
}

export class NutrientProductModel {
  nutrient: NutrientModel;
  quantity: number;
}

export class ProductModel {
  id: number;
  translations: ProductTranslationModel[];
  allergens: AllergenModel[];
  nutrients: NutrientProductModel[];
  composition: CompoundModel[];
  prices: PriceVariationModel[];
  complements: ComplementModel[];
  unit: UnitModel;
  code: string;
  cost_price: number;
  tax_cost_price_id: number;
  price: number;
  tax_price_id: number;
  disponibility: DisponibilityModel;
  file: FileModel;
  available: boolean;
  visible: boolean;
  show_derivatives: boolean;
  minimal_stock: number;
  track_stock: TrackStock;
  type: ServiceType;
  active: boolean;
  file_id: number;
  unit_id: number;
  families?: FamilyModel[];
  // TODO STOCKS
}
