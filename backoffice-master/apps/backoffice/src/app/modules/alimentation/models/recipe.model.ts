import { ProductModel } from '@fi-sas/backoffice/modules/alimentation/models/product.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { TranslationModel } from '../../configurations/models/translations.model';

export class ProductRecipeModel {
  ingredient: ProductModel;
  liquid_quantity: number;
  gross_quantity: number;

  constructor(ingredient?: ProductModel, liquid_quantity?: number, gross_quantity?: number) {
    this.gross_quantity = gross_quantity;
    this.liquid_quantity = liquid_quantity;
    this.ingredient = ingredient;
  }
}

export class ProductsRecipeModel {
  products: ProductModel;
  ingredient_id: number;
  liquid_quantity: number;
  gross_quantity: number;
  translations: TranslationModel;
  recipes_quantitys: any;
}

export class RecipeStepModel {
  file_id: number;
  file: FileModel;
  step: number;
  description: string;
}

export class RecipeModel {
  id: number;
  name:	string;
  description:	string;
  ingredients:	ProductRecipeModel[];
  products: ProductsRecipeModel[];
  steps: RecipeStepModel[];
  preparation_steps: RecipeStepModel[];
  number_doses:	number;
  preparation: string;
  preparation_mode:	string;
  microbiological_criteria:	string;
  packaging:	string;
  use:	string;
  distribution:	string;
  comment:	string;
  associated_docs:	string;
  active: boolean;

}
