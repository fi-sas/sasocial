import { WharehouseModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse.model';
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';

import { FamilyModel } from '@fi-sas/backoffice/modules/alimentation/models/family.model';
import { SchoolModel } from '../../configurations/models/school.model';

export enum ServiceType {
  BAR = 'bar',
  CANTEEN = 'canteen',
  MAINTENANCE = 'maintenance',
}

export class ServiceModel {
  id: number;
  wharehouse_id: number;
  wharehouse: WharehouseModel;
  name: string;
  maintenance: boolean;
  devices: DeviceModel[];
  organic_unit_id: number;
  school: SchoolModel;
  families: FamilyModel[];
  type: ServiceType;
  service_id: number;
  active: boolean;
}
