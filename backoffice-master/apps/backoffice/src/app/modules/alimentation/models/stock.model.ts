export class StockModel {
    id: number;
    product_id: number;
    quantity: number;
    expired: string;
    wharehouse_id: number;
    lote: string;
    active: boolean;
    operation: string;
    stock_reason: string;
    to_wharehouse_id: number;
  }
  