export class UnitTranslationModel {
  language_id: number;
  name: string;
}

export class UnitModel {
  id: number;
  translations: UnitTranslationModel[];
  acronym: string;
  active: boolean;
}
