export enum OperationType {
  IN = 'in',
  OUT = 'out',
  DISCARD = 'discard'
}

export class WharehouseOperationModel {
  id: number;
  order_id: number;
  operation: OperationType;
  quantity: number;
  quantity_before: number;
  quantity_after: number;
  date: Date;
  lote: string;
  user_id: number;
// TODO stock: StockModel
}
