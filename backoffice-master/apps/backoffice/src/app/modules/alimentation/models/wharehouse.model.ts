import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export class WharehouseModel {
  id: number;
  name: string;
  users: UserModel[];
  active: boolean;
}
