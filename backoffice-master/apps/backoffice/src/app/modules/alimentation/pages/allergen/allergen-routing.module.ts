import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAllergenComponent } from './form-allergen/form-allergen.component';
import { ListAllergensComponent } from './list-allergens/list-allergens.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAllergensComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:allergens:read'},

  },
  {
    path: 'create',
    component: FormAllergenComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:allergens:create'},
  },
  {
    path: 'update/:id',
    component: FormAllergenComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:allergens:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllergenRoutingModule { }
