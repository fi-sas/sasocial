import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListAllergensComponent } from './list-allergens/list-allergens.component';
import { FormAllergenComponent } from './form-allergen/form-allergen.component';
import { AllergenRoutingModule } from './allergen-routing.module';

@NgModule({
  declarations: [
    ListAllergensComponent,
    FormAllergenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AllergenRoutingModule
  ],

})
export class AllergenModule { }
