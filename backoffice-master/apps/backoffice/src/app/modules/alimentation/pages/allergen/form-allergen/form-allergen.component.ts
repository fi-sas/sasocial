import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AllergenModel } from '@fi-sas/backoffice/modules/alimentation/models/allergen.model';
import { AllergensService } from '@fi-sas/backoffice/modules/alimentation/services/allergens.service';
import { hasOwnProperty } from 'tslint/lib/utils';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { finalize, first } from 'rxjs/operators';
import * as _ from 'lodash';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';


@Component({
  selector: 'fi-sas-form-allergen',
  templateUrl: './form-allergen.component.html',
  styleUrls: ['./form-allergen.component.less']
})
export class FormAllergenComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;

  id: number;
  submitted: boolean = false;
  errorTrans = false;
  allergenForm = new FormGroup({
    translations: new FormArray([]),
    active: new FormControl(true, [Validators.required])
  });


  translations = this.allergenForm.get('translations') as FormArray;

  constructor(
    public allergensService: AllergensService,
    private route: ActivatedRoute,
    private languagesService: LanguagesService,
    private router: Router,
    private uiService: UiService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDataAllergenById(this.id);
    } else {
      this.loadLanguages('');
    }
  }

  getDataAllergenById(id: number){
    let allergen: AllergenModel = new AllergenModel();
    this.allergensService.getAllergenById(id).pipe(
      first()
    ).subscribe(results => {
      allergen = results.data[0];
      this.allergenForm.patchValue({
        ...allergen,
      });
      this.listOfSelectedLanguages = [];
      this.loadLanguages(allergen);
    });
  }

  loadLanguages(allergen: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (allergen) {
          this.startTranslation(allergen);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  submitAllergen(edit: boolean) {
    this.submitted = true;
    let sendValues: AllergenModel = new AllergenModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if(this.allergenForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.allergenForm.valid && !this.errorTrans) {
      this.loading = true;
      sendValues = this.allergenForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.allergensService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Alergénico alterada com sucesso'
            );
            this.backList();
          });

      } else {
        this.allergensService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Alergénico registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/allergen/list');
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.allergenForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.allergenForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required,trimValidation]),
        description: new FormControl(description, [Validators.required,trimValidation]),
      })
    );
  }

  get f() { return this.allergenForm.controls; }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name,
          translation.description
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }


}
