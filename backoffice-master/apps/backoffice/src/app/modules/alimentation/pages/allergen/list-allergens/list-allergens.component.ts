import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AllergensService } from '@fi-sas/backoffice/modules/alimentation/services/allergens.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { first } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { AllergenModel } from '../../../models/allergen.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-allergens',
  templateUrl: './list-allergens.component.html',
  styleUrls: ['./list-allergens.component.less']
})
export class ListAllergensComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];

  constructor(
    public allergensService: AllergensService,
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private languageService: LanguagesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  status = [];

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
   
    this.initTableData(this.allergensService);
    this.persistentFilters = {
      searchFields: 'name,description',
    };
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  editAllergen(id: number) {
    if(!this.authService.hasPermission('alimentation:allergens:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/allergen/update/' + id);
  }

  desactive(data: AllergenModel) {
    if(!this.authService.hasPermission('alimentation:allergens:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Alergénio. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: AllergenModel) {
    data.active = false;
    this.allergensService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Alergénio desativado com sucesso'
        );
        this.initTableData(this.allergensService);
      });
  }

  active(data: AllergenModel) {
    if(!this.authService.hasPermission('alimentation:allergens:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Alergénio. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: AllergenModel) {
    data.active = true;
    this.allergensService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Alergénio ativado com sucesso'
        );
        this.initTableData(this.allergensService);
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }
}
