import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { EntityService } from '@fi-sas/backoffice/modules/alimentation/services/entity.service';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-choose-entity',
  templateUrl: './choose-entity.component.html',
  styleUrls: ['./choose-entity.component.less']
})
export class ChooseEntityComponent implements OnInit, OnDestroy {

  entities = [];
  loading = true;


  constructor(
    private alimentationService: AlimentationService,
    private entitiesServices: EntityService,
    private activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    private router: Router) {

  }

  ngOnInit() {
    this.entitiesServices.entitiesOfUser().pipe(first()).subscribe(entities => {
      this.entities = entities.data;
    });
    this.loading = false;
  }

  ngOnDestroy() {
  }

  selectEntity(entity: FiscalEntityModel) {
    const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/alimentation/initial-page';
    this.alimentationService.setSelectedEntity(entity);
    this.router.navigate([returnUrl]);
  }
}
