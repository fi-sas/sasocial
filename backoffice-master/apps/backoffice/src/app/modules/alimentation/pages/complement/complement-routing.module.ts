import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComplementComponent } from './form-complement/form-complement.component';
import { ListComplementsComponent } from './list-complement/list-complements.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListComplementsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:complements:read'},

  },
  {
    path: 'create',
    component: FormComplementComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:complements:create'},
  },
  {
    path: 'update/:id',
    component: FormComplementComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:complements:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplementRoutingModule { }
