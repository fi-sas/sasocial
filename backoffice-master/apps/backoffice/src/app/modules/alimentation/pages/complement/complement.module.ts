import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListComplementsComponent } from './list-complement/list-complements.component';
import { FormComplementComponent } from './form-complement/form-complement.component';
import { ComplementRoutingModule } from './complement-routing.module';



@NgModule({
  declarations: [
    ListComplementsComponent,
    FormComplementComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComplementRoutingModule
  ],

})
export class ComplementModule { }
