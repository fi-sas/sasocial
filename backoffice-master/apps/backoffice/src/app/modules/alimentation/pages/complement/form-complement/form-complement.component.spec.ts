import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComplementComponent } from './form-complement.component';

describe('FormComplementComponent', () => {
  let component: FormComplementComponent;
  let fixture: ComponentFixture<FormComplementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormComplementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComplementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
