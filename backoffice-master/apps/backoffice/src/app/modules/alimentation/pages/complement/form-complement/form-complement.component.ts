import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ComplementModel } from '@fi-sas/backoffice/modules/alimentation/models/complement.model';
import { ComplementsService } from '@fi-sas/backoffice/modules/alimentation/services/complements.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-complement',
  templateUrl: './form-complement.component.html',
  styleUrls: ['./form-complement.component.less']
})
export class FormComplementComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  errorTrans = false;
  id: number;
  submitted: boolean = false;

  complementForm = new FormGroup({
    translations: new FormArray([]),
    active: new FormControl(true, [Validators.required])
  });

  translations = this.complementForm.get('translations') as FormArray;

  constructor(
    public complementsService: ComplementsService,
    private route: ActivatedRoute,
    private router: Router,
    private languagesService: LanguagesService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.languages_loading = true;
      this.getDataComplementById(this.id);

    } else {
      this.loadLanguages('');
    }
  }

  get f() { return this.complementForm.controls; }

  getDataComplementById(id: number) {
    this.loading = true;
    let complement: ComplementModel = new ComplementModel();
    this.languages_loading = false;
    this.complementsService
      .read(this.id)
      .pipe(
        first()
      )
      .subscribe((results) => {
        complement = results.data[0];
        this.complementForm.patchValue({
          ...complement
        });
        this.loading = false
        this.listOfSelectedLanguages = [];
        this.loadLanguages(complement);
      });
  }

  submitComplement(edit: boolean) {
    this.submitted = true;
    let sendValues: ComplementModel = new ComplementModel();
    if(this.complementForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.complementForm.valid && !this.errorTrans) {
      sendValues = this.complementForm.value;
      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.complementsService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Complemento alterada com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.complementsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Complemento registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }


  backList() {
    this.router.navigateByUrl('/alimentation/complement/list');
  }

  loadLanguages(complement: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        if (complement) {
          this.startTranslation(complement);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.complementForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.complementForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
        description: new FormControl(description, [Validators.required,trimValidation]),
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name,
          translation.description
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
