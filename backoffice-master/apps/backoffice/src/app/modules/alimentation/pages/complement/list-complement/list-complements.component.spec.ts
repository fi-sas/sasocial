import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComplementsComponent } from './list-complements.component';

describe('ListComplementsComponent', () => {
  let component: ListComplementsComponent;
  let fixture: ComponentFixture<ListComplementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComplementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComplementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
