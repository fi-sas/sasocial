import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ComplementsService } from '@fi-sas/backoffice/modules/alimentation/services/complements.service';
import { NzModalService } from "ng-zorro-antd";
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { first } from 'rxjs/operators';
import { ComplementModel } from '../../../models/complement.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-complements',
  templateUrl: './list-complements.component.html',
  styleUrls: ['./list-complements.component.less']
})
export class ListComplementsComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];
  constructor(
    public complementsService: ComplementsService,
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  status = [];

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    
    this.initTableData(this.complementsService);
    this.persistentFilters = {
      searchFields: 'name,description',
    };
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  editComplement(id: number) {
    if(!this.authService.hasPermission('alimentation:complements:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/complement/update/' + id);
  }

  desactive(data: ComplementModel) {
    if(!this.authService.hasPermission('alimentation:complements:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Complemento. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: ComplementModel) {
    data.active = false;
    this.complementsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Complemento desativada com sucesso'
        );
        this.initTableData(this.complementsService);
      });
  }

  active(data: ComplementModel) {
    if(!this.authService.hasPermission('alimentation:complements:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Complemento. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: ComplementModel) {
    data.active = true;
    this.complementsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Complemento ativada com sucesso'
        );
        this.initTableData(this.complementsService);
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }
}
