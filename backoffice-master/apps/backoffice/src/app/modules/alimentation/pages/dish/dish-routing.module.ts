import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDishComponent } from './form-dish/form-dish.component';
import { ListDishsComponent } from './list-dish/list-dishs.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListDishsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:dishs:read'},

  },
  {
    path: 'create',
    component: FormDishComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:dishs:create'},
  },
  {
    path: 'update/:id',
    component: FormDishComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:dishs:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishRoutingModule { }
