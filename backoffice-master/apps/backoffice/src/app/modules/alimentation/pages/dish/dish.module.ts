import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListDishsComponent } from './list-dish/list-dishs.component';
import { ViewDishsComponent } from './list-dish/view-dishs/view-dishs.component';
import { FormDishComponent } from './form-dish/form-dish.component';
import { DishRoutingModule } from './dish-routing.module';


@NgModule({
  declarations: [
    ListDishsComponent,
    ViewDishsComponent,
    FormDishComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DishRoutingModule
  ],

})
export class DishModule { }
