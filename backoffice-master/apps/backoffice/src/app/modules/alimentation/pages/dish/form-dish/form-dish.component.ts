import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { DishsService } from '@fi-sas/backoffice/modules/alimentation/services/dishs.service';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { debounceTime, finalize, first, switchMap, map } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'fi-sas-form-dish',
  templateUrl: './form-dish.component.html',
  styleUrls: ['./form-dish.component.less'],
})
export class FormDishComponent implements OnInit, OnDestroy {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  errorTrans = false;

  id: number = null;
  duplicate_id: number = null;

  submitted: boolean = false;
  filterTypes = ['image/png', 'image/jpeg'];
  dishForm = new FormGroup({
    translations: new FormArray([]),
    file_id: new FormControl(null, [Validators.required]),
    types_ids: new FormControl(null, [Validators.required]),
    recipes_ids: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
  });

  translations = this.dishForm.get('translations') as FormArray;

  dishTypes: DishTypeModel[];

  recipesList$: Subscription;
  recipes: RecipeModel[];
  searchRecipesChange$ = new BehaviorSubject('');
  isRecipesLoading = false;

  constructor(
    public dishsService: DishsService,
    private recipesService: RecipesService,
    private dishTypesService: DishTypesService,
    private route: ActivatedRoute,
    private router: Router,
    private languagesService: LanguagesService,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.loadDishTypes();
    this.loadRecipes();
    this.route.params.subscribe((params) => {
      this.id = params['id'] || null;
    });

    this.route.queryParams.subscribe((queryParams) => {
      this.duplicate_id = queryParams['duplicate_id'] || null;
    });
    if (this.id || this.duplicate_id) {
      this.languages_loading = true;
      this.getDataDishById(this.id || this.duplicate_id);
    } else {
      this.loadLanguages('');
    }
  }

  ngOnDestroy(): void {
    if (this.recipesList$) this.recipesList$.unsubscribe();
  }

  get f() {
    return this.dishForm.controls;
  }

  getDataDishById(id: number) {
    let dish: DishModel = new DishModel();
    this.dishsService
      .read(id, { withRelated: 'translations,recipes,type' })
      .pipe(first())
      .subscribe((results) => {
        dish = results.data[0];

        // PREFILL THE SELECT BOX
        this.recipes = dish.recipes;

        this.dishForm.patchValue({
          file_id: dish.file_id && !this.duplicate_id ? dish.file_id : null,
          types_ids: dish.type.map((a) => a.id),
          recipes_ids: dish.recipes ? dish.recipes.map((x) => x.id) : null,
          active: dish.active ? dish.active : false,
        });
        this.listOfSelectedLanguages = [];
        this.loadLanguages(dish);
      });
  }

  loadDishTypes() {
    this.dishTypesService
      .list(1, -1, null, null, { active: true })
      .pipe(first())
      .subscribe((results) => {
        this.dishTypes = results.data;
      });
  }

  loadRecipes() {
    this.recipesList$ = this.searchRecipesChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isRecipesLoading = true;
          return this.recipesService
            .list(1, 10, null, null, {
              active: true,
              sort: 'name',
              searchFields: 'description,name',
              search: event,
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.recipes = data;
        this.isRecipesLoading = false;
      });
  }

  submitDish(edit: boolean) {
    this.submitted = true;
    let sendValues: DishModel = new DishModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.dishForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.dishForm.valid && !this.errorTrans) {
      sendValues = this.dishForm.value;
      this.loading = true;
      sendValues.types_ids = this.f.types_ids.value;
      if (edit) {
        sendValues.id = this.id;
        this.dishsService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Prato alterado com sucesso'
            );
            this.backList();
          });
      } else {
        this.dishsService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Prato registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/dish/list');
  }

  loadLanguages(dish: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (dish) {
          this.startTranslation(dish);
        } else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.dishForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.dishForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
        description: new FormControl(description, [
          Validators.required,
          trimValidation,
        ]),
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name,
          translation.description
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  onRecipesSearch(value: string): void {
    this.searchRecipesChange$.next(value);
  }
}
