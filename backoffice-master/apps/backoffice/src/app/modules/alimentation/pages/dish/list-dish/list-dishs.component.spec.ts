import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDishsComponent } from './list-dishs.component';

describe('ListDishsComponent', () => {
  let component: ListDishsComponent;
  let fixture: ComponentFixture<ListDishsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDishsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDishsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
