import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { DishsService } from '@fi-sas/backoffice/modules/alimentation/services/dishs.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { first } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { DishModel } from '../../../models/dish.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-dishs',
  templateUrl: './list-dishs.component.html',
  styleUrls: ['./list-dishs.component.less']
})
export class ListDishsComponent extends TableHelper implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;
  languages: LanguageModel[] = [];
  status = [];

  constructor(
    public dishsService: DishsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private languageService: LanguagesService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'translations,recipes,type',
      searchFields: 'name,description'
    }
  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.initTableData(this.dishsService);
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
      ).subscribe(result => {
      this.languages = result.data;
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  editDish(id){
    if(!this.authService.hasPermission('alimentation:dishs:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/dish/update/' + id);
  }

  duplicateDish(id){
    if(!this.authService.hasPermission('alimentation:dishs:create')){
      return;
    }
    this.router.navigate(['alimentation', 'dish', 'create'], {
      queryParams: {
        duplicate_id: id,
      }
    });
  }

  desactive(data: DishModel) {
    if(!this.authService.hasPermission('alimentation:dishs:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este prato. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: DishModel) {
    data.active = false;
    this.dishsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Prato desativado com sucesso'
        );
        this.initTableData(this.dishsService);
      });

  }

  active(data: DishModel) {
    if(!this.authService.hasPermission('alimentation:dishs:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este prato. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: DishModel) {
    data.active = true;
    this.dishsService.active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Prato ativado com sucesso'
        );
        this.initTableData(this.dishsService);
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }
}
