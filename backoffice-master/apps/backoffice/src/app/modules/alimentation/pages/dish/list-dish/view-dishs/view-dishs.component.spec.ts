import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDishsComponent } from './view-dishs.component';

describe('ViewDishsComponent', () => {
  let component: ViewDishsComponent;
  let fixture: ComponentFixture<ViewDishsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDishsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDishsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
