import { Component, Input, OnInit } from '@angular/core';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { DishModel } from '../../../../models/dish.model';

@Component({
  selector: 'fi-sas-view-dishs',
  templateUrl: './view-dishs.component.html',
  styleUrls: ['./view-dishs.component.less']
})
export class ViewDishsComponent implements OnInit {

  @Input() data: DishModel = null;

  @Input() languages: LanguageModel[] = [];

  constructor(
    
  ) {     
    
  }

  ngOnInit() {
  }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
