import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDishtypeComponent } from './form-dishtype/form-dishtype.component';
import { ListDishtypesComponent } from './list-dishtype/list-dishtypes.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListDishtypesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:dishs-types:read'},

  },
  {
    path: 'create',
    component: FormDishtypeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:dishs-types:create'},
  },
  {
    path: 'update/:id',
    component: FormDishtypeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:dishs-types:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishtypesRoutingModule { }
