import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListDishtypesComponent } from './list-dishtype/list-dishtypes.component';
import { ViewDishtypesComponent } from './list-dishtype/view-dishtypes/view-dishtypes.component';
import { FormDishtypeComponent } from './form-dishtype/form-dishtype.component';
import { DishtypesRoutingModule } from './dishtype-routing.module';

@NgModule({
  declarations: [
    ListDishtypesComponent,
    ViewDishtypesComponent,
    FormDishtypeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DishtypesRoutingModule
  ],

})
export class DishtypeModule { }
