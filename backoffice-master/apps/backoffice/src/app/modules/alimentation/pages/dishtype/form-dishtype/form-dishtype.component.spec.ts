import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDishtypeComponent } from './form-dishtype.component';

describe('FormDishtypeComponent', () => {
  let component: FormDishtypeComponent;
  let fixture: ComponentFixture<FormDishtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDishtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDishtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
