import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import * as moment from 'moment';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { DisponibilityModel } from '../../../models/disponibility.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';

@Component({
  selector: 'fi-sas-form-dishtype',
  templateUrl: './form-dishtype.component.html',
  styleUrls: ['./form-dishtype.component.less']
})
export class FormDishtypeComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  errorTrans = false;
  id: number;
  submitted: boolean = false;
  taxes: TaxModel[];

  profiles: ProfileModel[] = [];

  dishTypeForm = new FormGroup({
    translations: new FormArray([]),
    code: new FormControl('', [Validators.required, trimValidation]),
    tax_id: new FormControl(null, [Validators.required]),
    price: new FormControl('', [Validators.required]),
    begin_date_dinner: new FormControl(null),
    end_date_dinner: new FormControl(null),
    monday_dinner: new FormControl(true, [Validators.required]),
    tuesday_dinner: new FormControl(true, [Validators.required]),
    wednesday_dinner: new FormControl(true, [Validators.required]),
    thursday_dinner: new FormControl(true, [Validators.required]),
    friday_dinner: new FormControl(true, [Validators.required]),
    saturday_dinner: new FormControl(true, [Validators.required]),
    sunday_dinner: new FormControl(true, [Validators.required]),
    minimum_hour_dinner: new FormControl(null),
    maximum_hour_dinner: new FormControl(null),
    annulment_maximum_hour_dinner: new FormControl(null),


    begin_date_lunch: new FormControl(null),
    end_date_lunch: new FormControl(null),
    monday_lunch: new FormControl(true, [Validators.required]),
    tuesday_lunch: new FormControl(true, [Validators.required]),
    wednesday_lunch: new FormControl(true, [Validators.required]),
    thursday_lunch: new FormControl(true, [Validators.required]),
    friday_lunch: new FormControl(true, [Validators.required]),
    saturday_lunch: new FormControl(true, [Validators.required]),
    sunday_lunch: new FormControl(true, [Validators.required]),
    minimum_hour_lunch: new FormControl(null),
    maximum_hour_lunch: new FormControl(null),
    annulment_maximum_hour_lunch: new FormControl(null),
    pos_operations: new FormControl(false, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
    pack_available: new FormControl(false, [Validators.required]),

    excluded_user_profile_ids: new FormControl([], []),
  });

  translations = this.dishTypeForm.get('translations') as FormArray;
  ;
  constructor(
    public dishTypesService: DishTypesService,
    private taxesService: TaxesService,
    private languagesService: LanguagesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    private profilesService: ProfilesService,
  ) { }

  ngOnInit() {
    this.loadTaxes();
    this.loadProfiles();

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.languages_loading = true;
      this.loading = true;
      this.getDataDishTypeById(this.id);

    } else {
      this.loadLanguages('');
    }
  }

  get f() { return this.dishTypeForm.controls; }


  backList() {
    this.router.navigateByUrl('/alimentation/dishtype/list');
  }

  loadTaxes() {
    this.taxesService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
      first()
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  getDataDishTypeById(id: number) {
    let dishType: DishTypeModel = new DishTypeModel();
    this.dishTypesService
      .read(this.id, { withRelated: "translations,disponibility_lunch,disponibility_dinner" })
      .pipe(
        first()
      )
      .subscribe((results) => {
        dishType = results.data[0];

        let min_hours_dinner;
        let max_hours_dinner;
        let max_annul_dinner;

        if (dishType.disponibility_dinner.minimum_hour) {
          min_hours_dinner = dishType.disponibility_dinner.minimum_hour;
          min_hours_dinner = min_hours_dinner == '23:00:00+00' ? (moment(dishType.disponibility_dinner.minimum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_dinner.minimum_hour, "HH:mm:ssZ"));
        }
        if (dishType.disponibility_dinner.maximum_hour) {
          max_hours_dinner = dishType.disponibility_dinner.maximum_hour;
          max_hours_dinner = max_hours_dinner == '23:00:00+00' ? (moment(dishType.disponibility_dinner.maximum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_dinner.maximum_hour, "HH:mm:ssZ"));
        }
        if (dishType.disponibility_dinner.annulment_maximum_hour) {
          max_annul_dinner = dishType.disponibility_dinner.annulment_maximum_hour;
          max_annul_dinner = max_annul_dinner == '23:00:00+00' ? (moment(dishType.disponibility_dinner.annulment_maximum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_dinner.annulment_maximum_hour, "HH:mm:ssZ"));
        }

        let min_hours_lunch;
        let max_hours_lunch;
        let max_annul_lunch;
        if (dishType.disponibility_lunch.minimum_hour) {
          min_hours_lunch = dishType.disponibility_lunch.minimum_hour;
          min_hours_lunch = min_hours_lunch == '23:00:00+00' ? (moment(dishType.disponibility_lunch.minimum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_lunch.minimum_hour, "HH:mm:ssZ"));
        }
        if (dishType.disponibility_lunch.maximum_hour) {
          max_hours_lunch = dishType.disponibility_lunch.maximum_hour;
          max_hours_lunch = max_hours_lunch == '23:00:00+00' ? (moment(dishType.disponibility_lunch.maximum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_lunch.maximum_hour, "HH:mm:ssZ"));
        }
        if (dishType.disponibility_lunch.annulment_maximum_hour) {
          max_annul_lunch = dishType.disponibility_lunch.annulment_maximum_hour;
          max_annul_lunch = max_annul_lunch == '23:00:00+00' ? (moment(dishType.disponibility_lunch.annulment_maximum_hour, "HH:mm:ssZ").subtract(1, 'day')) : (moment(dishType.disponibility_lunch.annulment_maximum_hour, "HH:mm:ssZ"));
        }


        this.dishTypeForm.patchValue({
          translation: dishType.translations ? dishType.translations : null,
          price: dishType.price ? dishType.price : null,
          code: dishType.code ? dishType.code : null,
          tax_id: dishType.tax_id ? dishType.tax_id : null,
          begin_date_dinner: dishType.disponibility_dinner.begin_date ? dishType.disponibility_dinner.begin_date : null,
          end_date_dinner: dishType.disponibility_dinner.end_date ? dishType.disponibility_dinner.end_date : null,
          monday_dinner: dishType.disponibility_dinner.monday,
          tuesday_dinner: dishType.disponibility_dinner.tuesday,
          wednesday_dinner: dishType.disponibility_dinner.wednesday,
          thursday_dinner: dishType.disponibility_dinner.thursday,
          friday_dinner: dishType.disponibility_dinner.friday,
          saturday_dinner: dishType.disponibility_dinner.saturday,
          sunday_dinner: dishType.disponibility_dinner.sunday,
          minimum_hour_dinner: dishType.disponibility_dinner.minimum_hour !== null ? min_hours_dinner.toDate() : null,
          maximum_hour_dinner: dishType.disponibility_dinner.maximum_hour !== null ? max_hours_dinner.toDate() : null,
          annulment_maximum_hour_dinner: dishType.disponibility_dinner.annulment_maximum_hour !== null ? max_annul_dinner.toDate() : null,

          begin_date_lunch: dishType.disponibility_lunch.begin_date ? dishType.disponibility_lunch.begin_date : null,
          end_date_lunch: dishType.disponibility_lunch.end_date ? dishType.disponibility_lunch.end_date : null,
          monday_lunch: dishType.disponibility_lunch.monday,
          tuesday_lunch: dishType.disponibility_lunch.tuesday,
          wednesday_lunch: dishType.disponibility_lunch.wednesday,
          thursday_lunch: dishType.disponibility_lunch.thursday,
          friday_lunch: dishType.disponibility_lunch.friday,
          saturday_lunch: dishType.disponibility_lunch.saturday,
          sunday_lunch: dishType.disponibility_lunch.sunday,
          minimum_hour_lunch: dishType.disponibility_lunch.minimum_hour !== null ? min_hours_lunch.toDate() : null,
          maximum_hour_lunch: dishType.disponibility_lunch.maximum_hour !== null ? max_hours_lunch.toDate() : null,
          annulment_maximum_hour_lunch: dishType.disponibility_lunch.annulment_maximum_hour !== null ? max_annul_lunch.toDate() : null,
          active: dishType.active,
          pos_operations: dishType.pos_operations,
          pack_available: dishType.pack_available,

          excluded_user_profile_ids: dishType.excluded_user_profile_ids,
        });
        this.loading = false;
        this.listOfSelectedLanguages = [];
        this.loadLanguages(dishType);
      });
  }

  loadProfiles() {
    this.profilesService.list(1, -1, null, null, {
      withRelated: false
    }).pipe(first()).subscribe(result => {
      this.profiles = result.data;
    });
  }

  submitDishType(edit: boolean) {
    this.submitted = true;
    let sendValues: DishTypeModel = new DishTypeModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if(this.dishTypeForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }

    if (this.dishTypeForm.valid  && !this.errorTrans && !this.validTimeMinLunch() && !this.validTimeMaxLunch() && !this.validTimeLunch()
    && !this.validTimeMinDinner() && !this.validTimeMaxDinner() && !this.validTimeDinner()) {
      sendValues = this.dishTypeForm.value;
      let disponibility_dinner = new DisponibilityModel();
      if (this.f.begin_date_dinner.value) {
        disponibility_dinner.begin_date = this.f.begin_date_dinner.value;
      }
      if (this.f.end_date_dinner.value) {
        disponibility_dinner.end_date = this.f.end_date_dinner.value;
      }
      disponibility_dinner.monday = this.f.monday_dinner.value;
      disponibility_dinner.tuesday = this.f.tuesday_dinner.value;
      disponibility_dinner.thursday = this.f.thursday_dinner.value;
      disponibility_dinner.wednesday = this.f.wednesday_dinner.value;
      disponibility_dinner.friday = this.f.friday_dinner.value;
      disponibility_dinner.sunday = this.f.sunday_dinner.value;
      disponibility_dinner.saturday = this.f.saturday_dinner.value;
      if (this.f.maximum_hour_dinner.value) {
        disponibility_dinner.maximum_hour = moment(this.f.maximum_hour_dinner.value).toDate();
      }
      if (this.f.minimum_hour_dinner.value) {
        disponibility_dinner.minimum_hour = moment(this.f.minimum_hour_dinner.value).toDate();
      }
      if (this.f.annulment_maximum_hour_dinner.value) {

        disponibility_dinner.annulment_maximum_hour = moment(this.f.annulment_maximum_hour_dinner.value).toDate();
      }

      let disponibility_lunch = new DisponibilityModel();
      if (this.f.begin_date_lunch.value) {
        disponibility_lunch.begin_date = this.f.begin_date_lunch.value;
      }
      if (this.f.end_date_lunch.value) {
        disponibility_lunch.end_date = this.f.end_date_lunch.value;
      }

      disponibility_lunch.monday = this.f.monday_lunch.value;
      disponibility_lunch.tuesday = this.f.tuesday_lunch.value;
      disponibility_lunch.thursday = this.f.thursday_lunch.value;
      disponibility_lunch.wednesday = this.f.wednesday_lunch.value;
      disponibility_lunch.friday = this.f.friday_lunch.value;
      disponibility_lunch.sunday = this.f.sunday_lunch.value;
      disponibility_lunch.saturday = this.f.saturday_lunch.value;
      if (this.f.maximum_hour_lunch.value) {
        disponibility_lunch.maximum_hour = moment(this.f.maximum_hour_lunch.value).toDate();
      }
      if (this.f.minimum_hour_lunch.value) {
        disponibility_lunch.minimum_hour = moment(this.f.minimum_hour_lunch.value).toDate();
      }
      if (this.f.annulment_maximum_hour_lunch.value) {
        disponibility_lunch.annulment_maximum_hour = moment(this.f.annulment_maximum_hour_lunch.value).toDate();
      }

      sendValues.disponibility_dinner = disponibility_dinner;
      sendValues.disponibility_lunch = disponibility_lunch;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.dishTypesService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Tipo de prato alterada com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.dishTypesService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Tipo de prato registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  loadLanguages(dishType: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (dishType) {
          this.startTranslation(dishType);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.dishTypeForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.dishTypeForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required,trimValidation])
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  disableStartDateLunch = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(startDate).isBefore(currentDate) || moment(this.dishTypeForm.get('end_date_lunch').value).isBefore(startDate);
  }

  disableEndDateLunch = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(endDate).isBefore(currentDate) || moment(this.dishTypeForm.get('begin_date_lunch').value).isAfter(endDate);
  }

  disableStartDateDinner = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(startDate).isBefore(currentDate) || moment(this.dishTypeForm.get('end_date_dinner').value).isBefore(startDate);
  }

  disableEndDateDinner = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(endDate).isBefore(currentDate) || moment(this.dishTypeForm.get('begin_date_dinner').value).isAfter(endDate);
  }

  validTimeMinLunch() {
    if(this.f.minimum_hour_lunch.value && this.f.maximum_hour_lunch.value) {
      return moment(this.f.minimum_hour_lunch.value).isAfter(this.f.maximum_hour_lunch.value);
    }
    return false;
  }

  validTimeMaxLunch() {
    if(this.f.minimum_hour_lunch.value && this.f.maximum_hour_lunch.value) {
      return moment(this.f.maximum_hour_lunch.value).isBefore(this.f.minimum_hour_lunch.value);
    }
    return false;
  }

  validTimeLunch(){
    if(this.f.minimum_hour_lunch.value && this.f.annulment_maximum_hour_lunch.value) {
      return moment(this.f.annulment_maximum_hour_lunch.value).isBefore(this.f.minimum_hour_lunch.value);
    }
    return false;
  }

  validTimeMinDinner() {
    if(this.f.minimum_hour_dinner.value && this.f.maximum_hour_dinner.value) {
      return moment(this.f.minimum_hour_dinner.value).isAfter(this.f.maximum_hour_dinner.value);
    }
    return false;
  }

  validTimeMaxDinner() {
    if(this.f.minimum_hour_dinner.value && this.f.maximum_hour_dinner.value) {
      return moment(this.f.maximum_hour_dinner.value).isBefore(this.f.minimum_hour_dinner.value);
    }
    return false;
  }

  validTimeDinner(){
    if(this.f.minimum_hour_dinner.value && this.f.annulment_maximum_hour_dinner.value) {
      return moment(this.f.annulment_maximum_hour_dinner.value).isBefore(this.f.minimum_hour_dinner.value);
    }
    return false;
  }
}
