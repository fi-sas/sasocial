import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDishtypesComponent } from './list-dishtypes.component';

describe('ListDishtypesComponent', () => {
  let component: ListDishtypesComponent;
  let fixture: ComponentFixture<ListDishtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDishtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDishtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
