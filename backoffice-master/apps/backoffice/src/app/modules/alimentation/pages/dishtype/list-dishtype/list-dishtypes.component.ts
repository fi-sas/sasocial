import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { DishTypeModel } from '../../../models/dish-type.model';
import { DishTypePriceModel } from '../../../models/dish-type-prices.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-dishtypes',
  templateUrl: './list-dishtypes.component.html',
  styleUrls: ['./list-dishtypes.component.less']
})
export class ListDishtypesComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];
  public YesNoTag = TagComponent.YesNoTag;
  time1;

  status = [];
  idEdit = -1;
  edit = false;
  constructor(
    public dishTypesService: DishTypesService,
    private profilesService: ProfilesService,
    private taxesService: TaxesService,
    private languageService: LanguagesService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'translations,disponibility_lunch,disponibility_dinner',
      searchFields: 'name'
    }
  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.initTableData(this.dishTypesService);
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  editDishType(id: number) {
    if(!this.authService.hasPermission('alimentation:dishs-types:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/dishtype/update/' + id);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  desactive(data: DishTypeModel) {
    if(!this.authService.hasPermission('alimentation:dishs-types:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este tipo de prato. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: DishTypeModel) {
    data.active = false;
    this.dishTypesService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Tipo de prato desativada com sucesso'
        );
        this.initTableData(this.dishTypesService);
      });

  }

  active(data: DishTypeModel) {
    if(!this.authService.hasPermission('alimentation:dishs-types:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Tipo de Prato. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: DishTypeModel) {
    data.active = true;
    this.dishTypesService.active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Tipo de prato ativado com sucesso'
        );
        this.initTableData(this.dishTypesService);
      });
  }


  dish_type_id: number;
  typePrices: DishTypePriceModel[]
  taxes: TaxModel[];
  profiles: ProfileModel[];
  mealList;
  submmitedPrice = false;
  defaultOpenValue = new Date(0, 0, 0, 0, 0, 0);
  accessModalPrices = false;
  loading_prices = false;

  productPricesForm = new FormGroup({
    price: new FormControl('', [Validators.required, Validators.min(1)]),
    description: new FormControl('', [Validators.required, trimValidation]),
    tax_id: new FormControl('', [Validators.required]),
    meal: new FormControl('', [Validators.required]),
    time: new FormControl('', [Validators.required]),
    profile_id: new FormControl('', [Validators.required]),
  });

  get f() { return this.productPricesForm.controls; }

  toggleAccessPrices(product_id) {
    if(!this.authService.hasPermission('alimentation:dishs-types:read')){
      return;
    }
    this.mealList = [
      { label: "Jantar", value: "dinner" },
      { label: "Almoço", value: "lunch" }
    ]
    this.loadTaxes();
    this.loadProfiles();
    this.accessModalPrices = !this.accessModalPrices;
    this.productPricesForm.reset();
    this.edit = false;
    this.idEdit = -1;
    this.submmitedPrice = false;
    if (this.accessModalPrices === true) {
      if (product_id !== null) {
        this.dish_type_id = product_id;
        this.updateListPrices();
      }
    }
  }

  updateListPrices() {
    this.typePrices = [];
    this.loading_prices = true;
    this.dishTypesService.getPrices(this.dish_type_id).subscribe(prices => {
      this.typePrices = prices.data;
      this.loading_prices = false;
    });
  }

  loadTaxes() {
    this.taxesService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  loadProfiles() {
    this.profilesService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.profiles = results.data;
    });
  }

  addPrices() {
    this.submmitedPrice = true;
    const findData = this.typePrices.find((data) => data.profile_id === this.f.profile_id.value && data.meal === this.f.meal.value && data.time === moment(this.f.time.value).format("HH:mm:ss"));
    if(findData) {
      this.uiService.showMessage(MessageType.error, 'Já existe essa informação na lista de preços');
      return;
    }
    if (!this.productPricesForm.invalid) {
      this.loading_prices = true;
      let price = new DishTypePriceModel();
      price.active = true;
      price.description = this.f.description.value,
        price.meal = this.f.meal.value,
        price.dish_type_id = this.dish_type_id;
      price.time = moment(this.f.time.value).format("HH:mm:ss");
      price.tax_id = this.f.tax_id.value;
      price.profile_id = this.f.profile_id.value;
      price.price = this.f.price.value;
      this.typePrices.push(price);

      this.dishTypesService.patchPrices(this.dish_type_id, this.typePrices).subscribe(res => {
        this.updateListPrices();
        this.loading_prices = false;
        this.submmitedPrice = false;
        this.uiService.showMessage(MessageType.success, 'Preço adicionado com sucesso.');
        this.productPricesForm.reset();
      });
    }
  }

  editClose(){
    this.edit = false;
    this.productPricesForm.reset();
    this.idEdit = -1;
    this.submmitedPrice = false;
  }

  removePrices(price) {
    const index = this.typePrices.indexOf(price);
    if (index > -1) {
      this.typePrices.splice(index, 1);
    }
    this.dishTypesService.patchPrices(this.dish_type_id, this.typePrices).subscribe(res => {
      this.updateListPrices();
      this.uiService.showMessage(MessageType.success, 'Preço removido com sucesso.');
    });
  }

  setValuePrices(price) {
    this.edit = true;
    this.idEdit = price.id;
    const date = new Date("1990-01-01 "+ price.time);
    this.productPricesForm.get('description').setValue(price.description);
    this.productPricesForm.get('price').setValue(price.price);
    this.productPricesForm.get('tax_id').setValue(price.tax_id);
    this.productPricesForm.get('meal').setValue(price.meal);
    this.productPricesForm.get('time').setValue(date);
    this.productPricesForm.get('profile_id').setValue(price.profile_id);
  }

  editPrice(){
    this.submmitedPrice = true;
    const listAux = this.typePrices.filter((data)=>data.id != this.idEdit);
    const findData = listAux.find((data) => data.profile_id === this.f.profile_id.value && data.meal === this.f.meal.value && data.time === moment(this.f.time.value).format("HH:mm:ss"));
    if(findData) {
      this.uiService.showMessage(MessageType.error, 'Já existe essa informação na lista de preços');
      return;
    }
    
    if (!this.productPricesForm.invalid) {
      this.loading_prices = true;
      this.typePrices.forEach((data)=>{
        if(data.id == this.idEdit) {
          data.description = this.f.description.value;
          data.meal = this.f.meal.value;
          data.time = moment(this.f.time.value).format("HH:mm:ss");
          data.tax_id = this.f.tax_id.value;
          data.profile_id = this.f.profile_id.value;
          data.price = this.f.price.value;
          data.active = true;
          data.dish_type_id = this.dish_type_id;
        }
      })
      this.dishTypesService.patchPrices(this.dish_type_id,this.typePrices).subscribe(res => {
        this.updateListPrices();
        this.loading_prices = false;
        this.editClose();
        this.uiService.showMessage(MessageType.success, 'Preço editado com sucesso.');
      });
    }
   
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

}
