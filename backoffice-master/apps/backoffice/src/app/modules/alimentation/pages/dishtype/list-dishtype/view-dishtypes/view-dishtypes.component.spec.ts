import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDishtypesComponent } from './view-dishtypes.component';

describe('ViewDishtypesComponent', () => {
  let component: ViewDishtypesComponent;
  let fixture: ComponentFixture<ViewDishtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDishtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDishtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
