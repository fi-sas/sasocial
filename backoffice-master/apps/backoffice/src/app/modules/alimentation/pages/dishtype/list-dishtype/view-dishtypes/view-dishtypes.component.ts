import { Component, Input, OnInit } from '@angular/core';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { DishTypeModel } from '../../../../models/dish-type.model';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-view-dishtypes',
  templateUrl: './view-dishtypes.component.html',
  styleUrls: ['./view-dishtypes.component.less']
})
export class ViewDishtypesComponent implements OnInit {

  @Input() data: DishTypeModel = null;
  @Input() languages: LanguageModel[] = [];
  annulment_maximum_hour_dinner;
  minimum_hour_dinner;
  maximum_hour_dinner;
  annulment_maximum_hour_lunch;
  minimum_hour_lunch;
  maximum_hour_lunch;
  constructor(
    
  ) {
   
  }
  ngOnInit() { 
    if(this.data.disponibility_dinner.maximum_hour) {
      this.maximum_hour_dinner = moment(this.data.disponibility_dinner.maximum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility_dinner.minimum_hour) {
      this.minimum_hour_dinner = moment(this.data.disponibility_dinner.minimum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility_dinner.annulment_maximum_hour) {
      this.annulment_maximum_hour_dinner = moment(this.data.disponibility_dinner.annulment_maximum_hour, "HH:mm:ssZ").toDate();
    }

    if(this.data.disponibility_lunch.maximum_hour) {
      this.maximum_hour_lunch = moment(this.data.disponibility_lunch.maximum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility_lunch.minimum_hour) {
      this.minimum_hour_lunch = moment(this.data.disponibility_lunch.minimum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility_lunch.annulment_maximum_hour) {
      this.annulment_maximum_hour_lunch = moment(this.data.disponibility_lunch.annulment_maximum_hour, "HH:mm:ssZ").toDate();
    }
  }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
