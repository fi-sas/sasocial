import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEntityComponent } from './form-entity/form-entity.component';
import { ListEntitiesComponent } from './list-entity/list-entities.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListEntitiesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:entities:read'},

  },
  {
    path: 'create',
    component: FormEntityComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:entities:create'},
  },
  {
    path: 'update/:id',
    component: FormEntityComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:entities:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EntityRoutingModule { }
