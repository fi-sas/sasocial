import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { EntityRoutingModule } from './entity-routing.module';
import { ListEntitiesComponent } from './list-entity/list-entities.component';
import { ViewEntitiesComponent } from './list-entity/view-entities/view-entities.component';
import { FormEntityComponent } from './form-entity/form-entity.component';

@NgModule({
  declarations: [
    FormEntityComponent,
    ViewEntitiesComponent,
    ListEntitiesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EntityRoutingModule
  ],

})
export class EntityModule { }
