import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityService } from '@fi-sas/backoffice/modules/alimentation/services/entity.service';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { finalize, first } from 'rxjs/operators';
import {
  trimValidation,
  validNIF,
  validNumber,
} from '@fi-sas/backoffice/shared/validators/validators.validator';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-form-entity',
  templateUrl: './form-entity.component.html',
  styleUrls: ['./form-entity.component.less'],
})
export class FormEntityComponent implements OnInit {
  accessModalVisible = false;

  fiscalEntityForm = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    tin: new FormControl('', [
      Validators.required,
      validNIF,
      Validators.maxLength(20),
    ]),
    street: new FormControl('', trimValidation),
    door: new FormControl(''),
    city: new FormControl('', trimValidation),
    cep: new FormControl(''),
    phone: new FormControl('', [validNumber]),
    email: new FormControl('', [Validators.email]),
    website: new FormControl('', trimValidation),
    account_id: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
  });

  correntAccounts: AccountModel[] = [];

  id: number;
  submitted: boolean = false;
  loading = false;

  constructor(
    public entityService: EntityService,
    private currentAccountsService: CurrentAccountsService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadAccounts();
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.loading = true;
      this.getDataEntityById();
    }
  }

  get f() {
    return this.fiscalEntityForm.controls;
  }

  getTinError() {
    return this.f.tin.errors.required
      ? 'Campo Obrigatório'
      : this.f.tin.errors.maxlength
      ? 'Tamanho máximo de 20'
      : this.f.tin.errors.invalidNIF
      ? 'NIF Inválido'
      : '';
  }

  getPhoneError() {
    return this.f.phone.errors.invalidPhone ? 'Numero inválido' : '';
  }

  loadAccounts() {
    this.currentAccountsService
      .list(1, -1, null, null)
      .pipe(first())
      .subscribe((results) => {
        this.correntAccounts = results.data;
      });
  }

  getDataEntityById() {
    this.loading = true;
    let entity: FiscalEntityModel = new FiscalEntityModel();
    this.entityService
      .read(this.id)
      .pipe(first())
      .subscribe((results) => {
        entity = results.data[0];
        this.fiscalEntityForm.patchValue({
          ...entity,
        });
        this.loading = false;
      });
  }

  submitEntity(edit: boolean) {
    this.submitted = true;
    let sendValues: FiscalEntityModel = new FiscalEntityModel();
    if (this.fiscalEntityForm.valid) {
      sendValues.name = this.fiscalEntityForm.get('name').value;
      sendValues.tin = this.fiscalEntityForm.get('tin').value;
      sendValues.street = this.fiscalEntityForm.get('street').value;
      sendValues.city = this.fiscalEntityForm.get('city').value;
      if (this.fiscalEntityForm.get('door').value) {
        sendValues.door = this.fiscalEntityForm.get('door').value;
      }
      sendValues.cep = this.fiscalEntityForm.get('cep').value;
      sendValues.phone = this.fiscalEntityForm.get('phone').value;
      sendValues.email = this.fiscalEntityForm.get('email').value;
      sendValues.website = this.fiscalEntityForm.get('website').value;
      sendValues.account_id = this.fiscalEntityForm.get('account_id').value;
      sendValues.active = this.fiscalEntityForm.get('active').value;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.entityService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Entidade alterada com sucesso'
            );
            this.backList();
          });
      } else {
        this.entityService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Entidade registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/entity/list');
  }
}
