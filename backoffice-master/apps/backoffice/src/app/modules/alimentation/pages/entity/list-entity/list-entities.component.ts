import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { EntityService } from '@fi-sas/backoffice/modules/alimentation/services/entity.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { NzModalService } from "ng-zorro-antd";
import { FiscalEntityModel } from '../../../models/fiscal-entity.model';

@Component({
  selector: 'fi-sas-list-entities',
  templateUrl: './list-entities.component.html',
  styleUrls: ['./list-entities.component.less']
})
export class ListEntitiesComponent extends TableHelper implements OnInit {

  status = [];
  userModel: UserModel;
  accessModalVisible = false;
  loading_users = false;
  users: UserModel[] = [];


  usersEntityForm = new FormGroup({
    user_id: new FormControl(null, [Validators.required]),
    manager: new FormControl(false, [Validators.required])
  });

  constructor(
    public entitiesService: EntityService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name,phone,tin,email"
    } 
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'tin',
        label: 'Nif',
        sortable: true,
      },
      {
        key: 'phone',
        label: 'Telefone',
        sortable: true,
      },
      {
        key: 'email',
        label: 'Email',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      }
    );
    this.persistentFilters['withRelated'] = 'users';
  }


  ngOnInit() {  
    this.initTableData(this.entitiesService);
    this.getUserLogged();
  }

  get f() { return this.usersEntityForm.controls; }

  editEntity(id: number) {
    if(!this.authService.hasPermission('alimentation:entities:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/entity/update/' + id);
  }

  getUserLogged() {
    this.authService.getUserObservable().subscribe(user => {
      this.userModel = user;
    });
    this.userModel = this.authService.getUser();
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  desactive(data: FiscalEntityModel) {
    if(!this.authService.hasPermission('alimentation:entities:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Entidade. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: FiscalEntityModel) {
    data.active = false;
    this.entitiesService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Entidade desativada com sucesso'
        );
        this.initTableData(this.entitiesService);
      });
  }

  active(data: FiscalEntityModel) {
    if(!this.authService.hasPermission('alimentation:entities:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Entidade. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: FiscalEntityModel) {
    data.active = true;
    this.entitiesService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Entidade ativada com sucesso'
        );
        this.initTableData(this.entitiesService);
      });
  }

  entity_id: number;
  toggleAccessModal(entity_id: number) {
    if(!this.authService.hasPermission('alimentation:entities:read')){
      return;
    }
    if (entity_id !== null) {
      this.entity_id = entity_id;
      this.updateUsersList()
    }
    this.accessModalVisible = !this.accessModalVisible;
    if (this.accessModalVisible === false) {
      this.initTableData(this.entitiesService);
    }
  }

  updateUsersList() {
    this.loading_users = true;
    this.entitiesService.listUsers(this.entity_id).subscribe(users => {
      this.users = users.data;
      this.loading_users = false;
    });
  }

  addUserToWharehouse() {
    let error = false;
    this.users.forEach((user) => {
      if (user.id == this.usersEntityForm.controls.user_id.value) {
        error = true;
      }
    })
    if (error) {
      this.uiService.showMessage(MessageType.error, 'Esse utilizador já foi adicionado');
    } else {
      if (!this.usersEntityForm.invalid) {
        this.entitiesService.addUser(this.entity_id, this.usersEntityForm.controls.user_id.value, this.usersEntityForm.controls.manager.value).subscribe(res => {
          this.updateUsersList();
          this.uiService.showMessage(MessageType.success, 'Utilizador adicionado com sucesso');
        });
      }
    }
  }

  removeUserFromWharehouse(id: number) {
    this.entitiesService.removeUser(this.entity_id, id).subscribe(res => {
      this.updateUsersList();
      this.uiService.showMessage(MessageType.success, 'Utilizador removido com sucesso');
    });
  }

  validUserId(idUser: number): boolean {
    if(idUser == this.userModel.id) {
      return false;
    }
    return true;
  }
}
