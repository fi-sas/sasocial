import { Component, Input, OnInit } from '@angular/core';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FiscalEntityModel } from '../../../../models/fiscal-entity.model';
import { EntityService } from '../../../../services/entity.service';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-view-entities',
  templateUrl: './view-entities.component.html',
  styleUrls: ['./view-entities.component.less']
})
export class ViewEntitiesComponent implements OnInit {

  @Input() data: FiscalEntityModel = null;

  constructor(public entitiesService: EntityService) { }


  ngOnInit() {
    
  }

}
