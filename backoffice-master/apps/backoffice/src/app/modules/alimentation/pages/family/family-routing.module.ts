import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFamilyComponent } from './form-family/form-family.component';
import { ListFamiliesComponent } from './list-family/list-families.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFamiliesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:families:read'},

  },
  {
    path: 'create',
    component: FormFamilyComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:families:create'},
  },
  {
    path: 'update/:id',
    component: FormFamilyComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:families:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyRoutingModule { }
