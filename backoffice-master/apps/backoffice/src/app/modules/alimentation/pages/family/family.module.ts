import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListFamiliesComponent } from './list-family/list-families.component';
import { FormFamilyComponent } from './form-family/form-family.component';
import { FamilyRoutingModule } from './family-routing.module';


@NgModule({
  declarations: [
    ListFamiliesComponent,
    FormFamilyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FamilyRoutingModule
  ],

})
export class FamilyModule { }
