import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, finalize, first, switchMap, map } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { ProductModel } from '../../../models/product.model';
import { FamilyModel } from '../../../models/family.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-form-family',
  templateUrl: './form-family.component.html',
  styleUrls: ['./form-family.component.less']
})
export class FormFamilyComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  errorTrans = false;
  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  filterTypes = ['image/png', 'image/jpeg'];
  id: number;
  submitted: boolean = false;
  isLoadingSelects = false;
  products: ProductModel[] = [];
  searchChange$ = new BehaviorSubject('');

  familyForm = new FormGroup({
    translations: new FormArray([]),
    file_id: new FormControl(null),
    products: new FormControl(null, []),
    active: new FormControl(true, [Validators.required])
  });

  translations = this.familyForm.get('translations') as FormArray;

  constructor(
    public familiesService: FamiliesService,
    private productsService: ProductsService,
    private languagesService: LanguagesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.languages_loading = true;
      this.loading = true;
      this.getDataFamilyById(this.id);

    } else {
      this.loadProducts();
      this.loadLanguages('');

    }
  }

  get f() { return this.familyForm.controls; }

  getDataFamilyById(id: number) {
    this.loading = true;
    let family: FamilyModel = new FamilyModel();
    this.familiesService
      .read(this.id, { withRelated: "translations,products" })
      .pipe(
        first()
      )
      .subscribe((results) => {
        family = results.data[0];
        this.familyForm.patchValue({
          translations: family.translations ? family.translations : null,
          file_id: family.file_id ? family.file_id : null,
          products: family.products ? family.products.map((p) => p.id) : null,
          active: family.active ? family.active : false,
        });
        this.products = family.products;
        this.loadProducts();
        this.languages_loading = false;
        this.loading = false
        this.listOfSelectedLanguages = [];
        this.loadLanguages(family);
      });
  }

  submitFamily(edit: boolean) {
    this.submitted = true;
    let sendValues: FamilyModel = new FamilyModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.familyForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.familyForm.valid && !this.errorTrans) {
      sendValues = this.familyForm.value;
      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.familiesService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Família alterada com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.familiesService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Família registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/family/list');
  }

  loadProducts() {
    const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
      .pipe(debounceTime(500)).pipe(
        switchMap(event => {
          this.isLoadingSelects = true;
          return this.productsService.list(1, 10, null, null,
            {
              withRelated: "translations",
              type: 'bar',
              search: event,
              active: true,
              searchFields: 'description,name'
            }
          )
        }
        )).pipe(map(res => res.data));

    productList$.subscribe(data => {
      this.products = data;
      this.isLoadingSelects = false;
    });
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  loadLanguages(complement: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (complement) {
          this.startTranslation(complement);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.familyForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.familyForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation])
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
