import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { first } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { FamilyModel } from '../../../models/family.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-families',
  templateUrl: './list-families.component.html',
  styleUrls: ['./list-families.component.less']
})
export class ListFamiliesComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];

  public YesNoTag = TagComponent.YesNoTag;

  status = [];

  constructor(
    public familiesService: FamiliesService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private languageService: LanguagesService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'translations,products',
      searchFields: 'name',
    }
  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.initTableData(this.familiesService);
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  editFamily(id: number) {
    if(!this.authService.hasPermission('alimentation:families:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/family/update/' + id);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

  desactive(data: FamilyModel) {
    if(!this.authService.hasPermission('alimentation:families:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Família. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: FamilyModel) {
    data.active = false;
    this.familiesService
      .desactive(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Família desativada com sucesso'
        );
        this.initTableData(this.familiesService);
      });

  }

  active(data: FamilyModel) {
    if(!this.authService.hasPermission('alimentation:families:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Família. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: FamilyModel) {
    data.active = true;
    this.familiesService
      .active(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Família ativado com sucesso'
        );
        this.initTableData(this.familiesService);
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

}
