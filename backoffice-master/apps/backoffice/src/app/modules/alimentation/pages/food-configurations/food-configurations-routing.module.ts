import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodConfigurationsComponent } from './food-configurations.component';

const routes: Routes = [
  { path: '', redirectTo: 'general', pathMatch: 'full' },
  {
    path: 'general',
    component: FoodConfigurationsComponent,
    data: { breadcrumb: 'Gerais', title: 'Gerais', scope: 'alimentation:configurations:read'},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodConfigurationsRoutingModule { }
