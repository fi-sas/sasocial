import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { FoodConfigrationsModel } from '../../models/food-configurations.model';
import { FoodConfigurationsService } from '../../services/food-configurations.service';

@Component({
  selector: 'fi-sas-food-configurations',
  templateUrl: './food-configurations.component.html',
  styleUrls: ['./food-configurations.component.less'],
})
export class FoodConfigurationsComponent implements OnInit {
  formData: FormGroup;
  loadingConfiguration = true;

  showAllergensOnMenuData = null;

  loading = false;
  submit = false;
  constructor(
    private fb: FormBuilder,
    private settings: FoodConfigurationsService,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.formData = this.fb.group({
      SHOW_ALLERGENS_ON_MENU: new FormControl(false, [Validators.required]),
    });
    this.getConfigurations();
  }
  get f() {
    return this.formData.controls;
  }

  getConfigurations() {
    this.settings
      .list(1, -1, null, null, {
        withRelated: 'info',
      })
      .pipe(
        first(),
        finalize(() => (this.loadingConfiguration = false))
      )
      .subscribe((data) => {
        this.formData
          .get('SHOW_ALLERGENS_ON_MENU')
          .setValue(data.data[0].SHOW_ALLERGENS_ON_MENU);
        this.showAllergensOnMenuData = data.data[1].SHOW_ALLERGENS_ON_MENU;
      });
  }

  submitForm() {
    this.loading = true;
    this.submit = true;
    let sendValue: FoodConfigrationsModel = new FoodConfigrationsModel();
    if (this.formData.valid) {
      this.submit = false;
      sendValue.SHOW_ALLERGENS_ON_MENU = this.formData.get(
        'SHOW_ALLERGENS_ON_MENU'
      ).value;

      this.settings
        .create(sendValue)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((data) => {
          this.uiService.showMessage(
            MessageType.success,
            'Configurações alteradas com sucesso'
          );
          this.getConfigurations();
        });
    } else {
      this.loading = false;
    }
  }
}
