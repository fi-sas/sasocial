import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodConfigurationsRoutingModule } from './food-configurations-routing.module';
import { FoodConfigurationsComponent } from './food-configurations.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [FoodConfigurationsComponent],
  imports: [
    CommonModule,
    FoodConfigurationsRoutingModule,
    SharedModule,
  ]
})
export class FoodConfigurationsModule { }
