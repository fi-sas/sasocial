import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAnomaliesComponent } from './form-anomalies/form-anomalies.component';
import { ListAnomaliesComponent } from './list-anomalies/list-anomalies.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAnomaliesComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar anomalias',
      scope: 'haccp:anomalies:read',
    },
  },
  {
    path: 'create',
    component: FormAnomaliesComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar anomalias',
      scope: 'haccp:anomalies:create',
    },
  },
  {
    path: 'update/:id',
    component: FormAnomaliesComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar anomalias',
      scope: 'haccp:anomalies:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnomaliesRoutingModule {}
