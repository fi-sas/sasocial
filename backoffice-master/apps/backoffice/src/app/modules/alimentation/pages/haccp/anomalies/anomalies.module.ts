import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AnomaliesRoutingModule } from './anomalies-routing.module';
import { FormAnomaliesComponent } from './form-anomalies/form-anomalies.component';
import { ListAnomaliesComponent } from './list-anomalies/list-anomalies.component';

@NgModule({
  declarations: [ListAnomaliesComponent, FormAnomaliesComponent],
  imports: [CommonModule, AnomaliesRoutingModule, SharedModule],
})
export class AnomaliesModule {}
