import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { AnomalyModel, AnomalyType } from '../../models/anomaly.model';
import { AnomaliesService } from '../../services/anomalies.service';

@Component({
  templateUrl: './form-anomalies.component.html',
  styleUrls: ['./form-anomalies.component.less'],
})
export class FormAnomaliesComponent implements OnInit {
  AnomalyType = AnomalyType;
  equipmentForm = new FormGroup({
    type: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    code: new FormControl('', [Validators.required]),
  });

  id: number;
  submitted: boolean = false;
  loading = false;

  constructor(
    public equipmentsAnomaliesService: AnomaliesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.loading = true;
      this.getDataEquipmentAnomalyById();
    }
  }

  get f() {
    return this.equipmentForm.controls;
  }

  getDataEquipmentAnomalyById() {
    this.loading = true;
    let equipmentAnomaly: AnomalyModel = new AnomalyModel();
    this.equipmentsAnomaliesService
      .read(this.id)
      .pipe(first())
      .subscribe((results) => {
        equipmentAnomaly = results.data[0];
        this.equipmentForm.patchValue({
          ...equipmentAnomaly,
        });
        this.loading = false;
      });
  }

  submitEquipment(edit: boolean) {
    this.submitted = true;
    let sendValues: AnomalyModel = new AnomalyModel();

    console.log(this.equipmentForm.errors);
    console.log(this.equipmentForm.valid);
    if (!this.equipmentForm.valid) {
      Object.keys(this.equipmentForm.controls).forEach((key) => {
        this.equipmentForm.controls[key].markAsDirty();
        console.log(key);
        console.log(this.equipmentForm.controls[key].errors);
      });
      return;
    } else {
      sendValues = this.equipmentForm.value;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.equipmentsAnomaliesService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Anomalia de equipamento alterada com sucesso'
            );
            this.backList();
          });
      } else {
        this.equipmentsAnomaliesService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Anomalia de equipamento registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/anomalies/list');
  }
}
