import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';

import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { AnomaliesService } from '../../services/anomalies.service';

@Component({
  templateUrl: './list-anomalies.component.html',
  styleUrls: ['./list-anomalies.component.less'],
})
export class ListAnomaliesComponent extends TableHelper implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: AnomaliesService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.columns.push(
      {
        key: 'code',
        label: 'Código',
        sortable: true,
      },
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      }
    );
    this.persistentFilters = {};

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:anomalies:delete')) {
      return;
    }
    this.router.navigateByUrl('/alimentation/haccp/anomalies/update/' + id);
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar anomalia de equipamento',
        'Pretende eliminar esta  anomalia de equipamento',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Anomalia de equipamento eliminada com sucesso'
              );
            });
        }
      });
  }
}
