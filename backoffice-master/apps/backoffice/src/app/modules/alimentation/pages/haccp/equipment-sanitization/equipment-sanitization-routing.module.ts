import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEquipmentSanitizationComponent } from './form-equipment-sanitization/form-equipment-sanitization.component';
import { ListEquipmentSanitizationComponent } from './list-equipment-sanitization/list-equipment-sanitization.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListEquipmentSanitizationComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar higienização de equipamentos',
      scope: 'haccp:equipment_sanitization:read',
    },
  },
  {
    path: 'create',
    component: FormEquipmentSanitizationComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar registo higienização de equipamentos',
      scope: 'haccp:equipment_sanitization:create',
    },
  },
  {
    path: 'update/:id',
    component: FormEquipmentSanitizationComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar registo higienização de equipamentos',
      scope: 'haccp:equipment_sanitization:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipmentSanitizationRoutingModule { }
