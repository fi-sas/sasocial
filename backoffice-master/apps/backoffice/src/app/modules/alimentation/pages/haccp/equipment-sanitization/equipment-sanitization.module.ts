import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EquipmentSanitizationRoutingModule } from './equipment-sanitization-routing.module';
import { FormEquipmentSanitizationComponent } from './form-equipment-sanitization/form-equipment-sanitization.component';
import { ListEquipmentSanitizationComponent } from './list-equipment-sanitization/list-equipment-sanitization.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [FormEquipmentSanitizationComponent, ListEquipmentSanitizationComponent],
  imports: [
    CommonModule,
    SharedModule,
    EquipmentSanitizationRoutingModule
  ]
})
export class EquipmentSanitizationModule { }
