import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEquipmentSanitizationComponent } from './form-equipment-sanitization.component';

describe('FormEquipmentSanitizationComponent', () => {
  let component: FormEquipmentSanitizationComponent;
  let fixture: ComponentFixture<FormEquipmentSanitizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEquipmentSanitizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEquipmentSanitizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
