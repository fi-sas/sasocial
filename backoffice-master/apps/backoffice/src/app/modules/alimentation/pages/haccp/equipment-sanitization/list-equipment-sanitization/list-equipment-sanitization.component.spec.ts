import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEquipmentSanitizationComponent } from './list-equipment-sanitization.component';

describe('ListEquipmentSanitizationComponent', () => {
  let component: ListEquipmentSanitizationComponent;
  let fixture: ComponentFixture<ListEquipmentSanitizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEquipmentSanitizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEquipmentSanitizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
