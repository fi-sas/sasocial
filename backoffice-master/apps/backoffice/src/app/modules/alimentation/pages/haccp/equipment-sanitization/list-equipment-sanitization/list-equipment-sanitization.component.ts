import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { EquipmentSanitizationsService } from '../../services/equipment-sanitizations.service';

@Component({
  selector: 'fi-sas-list-equipment-sanitization',
  templateUrl: './list-equipment-sanitization.component.html',
  styleUrls: ['./list-equipment-sanitization.component.less']
})
export class ListEquipmentSanitizationComponent extends TableHelper implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: EquipmentSanitizationsService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'service,lines',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:equipment_sanitizations:update')) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/equipment_sanitization/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }

}
