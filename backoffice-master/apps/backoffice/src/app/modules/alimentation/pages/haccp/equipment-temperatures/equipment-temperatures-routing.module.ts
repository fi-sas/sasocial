import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEquipmentTemperaturesComponent } from './form-equipment-temperatures/form-equipment-temperatures.component';
import { ListEquipmentTemperaturesComponent } from './list-equipment-temperatures/list-equipment-temperatures.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListEquipmentTemperaturesComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar temperatura equipamentos',
      scope: 'haccp:equipments:read',
    },
  },
  {
    path: 'create',
    component: FormEquipmentTemperaturesComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Registar temperatura equipamento',
      scope: 'haccp:equipments:create',
    },
  },
  {
    path: 'update/:id',
    component: FormEquipmentTemperaturesComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Registar temperatura equipamento',
      scope: 'haccp:equipments:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipmentTemperaturesRoutingModule {}
