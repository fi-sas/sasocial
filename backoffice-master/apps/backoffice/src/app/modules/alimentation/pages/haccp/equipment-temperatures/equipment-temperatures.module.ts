import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EquipmentTemperaturesRoutingModule } from './equipment-temperatures-routing.module';
import { ListEquipmentTemperaturesComponent } from './list-equipment-temperatures/list-equipment-temperatures.component';
import { FormEquipmentTemperaturesComponent } from './form-equipment-temperatures/form-equipment-temperatures.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    ListEquipmentTemperaturesComponent,
    FormEquipmentTemperaturesComponent,
  ],
  imports: [CommonModule, SharedModule, EquipmentTemperaturesRoutingModule],
})
export class EquipmentTemperaturesModule {}
