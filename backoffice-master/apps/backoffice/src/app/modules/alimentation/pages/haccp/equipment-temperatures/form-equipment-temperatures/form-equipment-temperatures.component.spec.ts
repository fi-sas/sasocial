import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEquipmentTemperaturesComponent } from './form-equipment-temperatures.component';

describe('FormEquipmentTemperaturesComponent', () => {
  let component: FormEquipmentTemperaturesComponent;
  let fixture: ComponentFixture<FormEquipmentTemperaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEquipmentTemperaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEquipmentTemperaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
