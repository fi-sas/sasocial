import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { eq } from 'lodash';
import { finalize, first } from 'rxjs/operators';
import { AnomalyModel } from '../../models/anomaly.model';
import { EquipmentTemperatureModel } from '../../models/equipment-temperature.model';
import { EquipmentModel } from '../../models/equipment.model';
import { AnomaliesService } from '../../services/anomalies.service';
import { EquipmentsTemperatureService } from '../../services/equipments-temperature.service';
import { EquipmentsService } from '../../services/equipments.service';

@Component({
  selector: 'fi-sas-form-equipment-temperatures',
  templateUrl: './form-equipment-temperatures.component.html',
  styleUrls: ['./form-equipment-temperatures.component.less'],
})
export class FormEquipmentTemperaturesComponent implements OnInit {
  equipmentTemperatureForm = new FormGroup({
    temperature: new FormControl(0, [Validators.required]),
    date: new FormControl(new Date(), [Validators.required]),
    equipment_id: new FormControl(null, [Validators.required]),
    observations: new FormControl('', []),
    anomaly_id: new FormControl(null, []),
    justification: new FormControl('', []),
  });

  id: number;
  submitted: boolean = false;
  loading = false;

  equipmentsLoading = false;
  equipments: EquipmentModel[] = [];

  equipmentsAnomaliesLoading = false;
  equipmentsAnomalies: AnomalyModel[] = [];

  selectedEquipment: EquipmentModel = null;
  isGoodTemperature = false;

  constructor(
    private equipmentsTemperatureService: EquipmentsTemperatureService,
    private equipmentsService: EquipmentsService,
    private anomaliesService: AnomaliesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadEquipments();
    this.loadEquipmentsAnomalies();

    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.loading = true;
      this.getDataEquipmentTemperatureById();
    }
  }

  loadEquipmentsAnomalies() {
    this.equipmentsAnomaliesLoading = true;
    this.anomaliesService
      .list(1, -1, null, null, {
        type: 'EQUIPMENT',
      })
      .pipe(
        first(),
        finalize(() => (this.equipmentsAnomaliesLoading = false))
      )
      .subscribe((result) => {
        this.equipmentsAnomalies = result.data;
      });
  }

  loadEquipments() {
    this.equipmentsLoading = true;
    this.equipmentsService
      .list(1, -1, null, null, {
        type: 'REFRIGERATION',
      })
      .pipe(
        first(),
        finalize(() => (this.equipmentsLoading = false))
      )
      .subscribe((result) => {
        this.equipments = result.data;
      });
  }

  get f() {
    return this.equipmentTemperatureForm.controls;
  }

  getDataEquipmentTemperatureById() {
    this.loading = true;
    let equipmentTemperature: EquipmentTemperatureModel = new EquipmentTemperatureModel();
    this.equipmentsTemperatureService
      .read(this.id)
      .pipe(first())
      .subscribe((results) => {
        equipmentTemperature = results.data[0];
        this.equipmentTemperatureForm.patchValue({
          ...equipmentTemperature,
        });
        this.loading = false;
      });
  }

  validateTemperature() {
    const formValue = this.equipmentTemperatureForm.value;
    if (formValue.equipment_id) {
      this.selectedEquipment = this.equipments.find(
        (e) => e.id === formValue.equipment_id
      );

      if (this.selectedEquipment && formValue.temperature) {
        this.isGoodTemperature =
          formValue.temperature >= this.selectedEquipment.minimum_temperature &&
          formValue.temperature <= this.selectedEquipment.maximum_temperature;

        if (this.isGoodTemperature) {
          this.equipmentTemperatureForm.controls[
            'anomaly_id'
          ].clearValidators();
          this.equipmentTemperatureForm.controls['anomaly_id'].setValidators(
            []
          );
          this.equipmentTemperatureForm.controls[
            'justification'
          ].clearValidators();
          this.equipmentTemperatureForm.controls['justification'].setValidators(
            []
          );
        } else {
          this.equipmentTemperatureForm.controls[
            'anomaly_id'
          ].clearValidators();
          this.equipmentTemperatureForm.controls['anomaly_id'].setValidators([
            Validators.required,
          ]);
          this.equipmentTemperatureForm.controls[
            'justification'
          ].clearValidators();
          this.equipmentTemperatureForm.controls[
            'justification'
          ].setValidators([Validators.required]);
        }
      }
    }
  }

  submitEquipment(edit: boolean) {
    this.submitted = true;
    let sendValues: EquipmentTemperatureModel = new EquipmentTemperatureModel();

    if (!this.equipmentTemperatureForm.valid) {
      Object.keys(this.equipmentTemperatureForm.controls).forEach((key) => {
        this.equipmentTemperatureForm.controls[key].markAsDirty();
      });
      return;
    } else {
      sendValues = this.equipmentTemperatureForm.value;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.equipmentsTemperatureService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Registo alterado com sucesso'
            );
            this.backList();
          });
      } else {
        this.equipmentsTemperatureService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Registado com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl(
      '/alimentation/haccp/equipment_temperatures/list'
    );
  }
}
