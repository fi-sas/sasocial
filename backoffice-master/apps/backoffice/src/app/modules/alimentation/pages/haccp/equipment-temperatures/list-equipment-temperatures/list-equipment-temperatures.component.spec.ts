import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEquipmentTemperaturesComponent } from './list-equipment-temperatures.component';

describe('ListEquipmentTemperaturesComponent', () => {
  let component: ListEquipmentTemperaturesComponent;
  let fixture: ComponentFixture<ListEquipmentTemperaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEquipmentTemperaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEquipmentTemperaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
