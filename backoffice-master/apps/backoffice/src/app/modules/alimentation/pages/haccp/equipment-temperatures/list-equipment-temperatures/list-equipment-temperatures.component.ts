import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { EquipmentModel } from '../../models/equipment.model';
import { EquipmentsTemperatureService } from '../../services/equipments-temperature.service';
import { EquipmentsService } from '../../services/equipments.service';

@Component({
  selector: 'fi-sas-list-equipment-temperatures',
  templateUrl: './list-equipment-temperatures.component.html',
  styleUrls: ['./list-equipment-temperatures.component.less'],
})
export class ListEquipmentTemperaturesComponent
  extends TableHelper
  implements OnInit {


  equipmentsLoading = false;
  equipments: EquipmentModel[] = [];

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: EquipmentsTemperatureService,
    private equipmentsService: EquipmentsService,
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.columns.push(
      {
        key: 'date',
        label: 'Data',
        sortable: true,
      },
      {
        key: 'equipment.name',
        label: 'Equipamento',
        sortable: true,
      },
      {
        key: 'temperature',
        label: 'Temperatura',
        sortable: true,
      }
    );
    this.persistentFilters = {
      withRelated: 'equipment',
    };

    this.loadEquipments();
    this.initTableData(this.templateService);
  }

  listComplete() { }

  loadEquipments() {
    this.equipmentsLoading = true;
    this.equipmentsService
      .list(1, -1, null, null, {
        type: 'REFRIGERATION',
      })
      .pipe(
        first(),
        finalize(() => (this.equipmentsLoading = false))
      )
      .subscribe((result) => {
        this.equipments = result.data;
      });
  }

  editEquip(id: number) {
    if (
      !this.authService.hasPermission('haccp:equipment_temperatures:delete')
    ) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/equipment_temperatures/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar equipamento',
        'Pretende eliminar este equipamento',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Equipamento eliminado com sucesso'
              );
            });
        }
      });
  }
}
