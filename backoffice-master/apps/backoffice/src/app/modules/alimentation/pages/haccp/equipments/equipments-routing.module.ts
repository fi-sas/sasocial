import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEquipmentsComponent } from './form-equipments/form-equipments.component';
import { ListEquipmentsComponent } from './list-equipments/list-equipments.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListEquipmentsComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar equipamentos',
      scope: 'haccp:equipments:read',
    },
  },
  {
    path: 'create',
    component: FormEquipmentsComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar equipamento',
      scope: 'haccp:equipments:create',
    },
  },
  {
    path: 'update/:id',
    component: FormEquipmentsComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar equipamento',
      scope: 'haccp:equipments:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipmentsRoutingModule {}
