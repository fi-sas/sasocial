import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EquipmentsRoutingModule } from './equipments-routing.module';
import { ListEquipmentsComponent } from './list-equipments/list-equipments.component';
import { FormEquipmentsComponent } from './form-equipments/form-equipments.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [ListEquipmentsComponent, FormEquipmentsComponent],
  imports: [CommonModule, EquipmentsRoutingModule, SharedModule],
})
export class EquipmentsModule {}
