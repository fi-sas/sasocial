import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { finalize, first } from 'rxjs/operators';
import { EquipmentModel, EquipmentType } from '../../models/equipment.model';
import { EquipmentsService } from '../../services/equipments.service';

@Component({
  selector: 'fi-sas-form-equipments',
  templateUrl: './form-equipments.component.html',
  styleUrls: ['./form-equipments.component.less'],
})
export class FormEquipmentsComponent implements OnInit {
  EquipmentType = EquipmentType;

  equipmentForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    minimum_temperature: new FormControl('', []),
    maximum_temperature: new FormControl('', []),
    polar_compounds_limit: new FormControl('', []),
    service_id: new FormControl('', [Validators.required]),
    sanatize: new FormControl(false, [Validators.required]),
    type: new FormControl('', [Validators.required]),
    active: new FormControl(true, [Validators.required]),
  });

  id: number;
  submitted: boolean = false;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];
  constructor(
    public equipmentsService: EquipmentsService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    private servicesService: ServicesService
  ) {}

  ngOnInit() {
    this.loadServices();

    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.loading = true;
      this.getDataEquipmentById();
    }
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {})
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }

  onTypeChange(type) {
    this.equipmentForm.controls['minimum_temperature'].setValidators([]);
    this.equipmentForm.controls['maximum_temperature'].setValidators([]);
    this.equipmentForm.controls['polar_compounds_limit'].setValidators([]);

    if (type === EquipmentType.DEEP_FRYER) {
      this.equipmentForm.controls['maximum_temperature'].setValidators([
        Validators.required,
      ]);
      this.equipmentForm.controls['polar_compounds_limit'].setValidators([
        Validators.required,
      ]);
    }

    if (type === EquipmentType.REFRIGERATION) {
      this.equipmentForm.controls['minimum_temperature'].setValidators([
        Validators.required,
      ]);
      this.equipmentForm.controls['maximum_temperature'].setValidators([
        Validators.required,
      ]);
    }
  }

  get f() {
    return this.equipmentForm.controls;
  }

  getDataEquipmentById() {
    this.loading = true;
    let equipment: EquipmentModel = new EquipmentModel();
    this.equipmentsService
      .read(this.id)
      .pipe(first())
      .subscribe((results) => {
        equipment = results.data[0];
        this.equipmentForm.patchValue({
          ...equipment,
        });
        this.loading = false;
      });
  }

  submitEquipment(edit: boolean) {
    this.submitted = true;
    let sendValues: EquipmentModel = new EquipmentModel();

    console.log(this.equipmentForm.errors);
    console.log(this.equipmentForm.valid);
    if (!this.equipmentForm.valid) {
      Object.keys(this.equipmentForm.controls).forEach((key) => {
        this.equipmentForm.controls[key].markAsDirty();
        console.log(key);
        console.log(this.equipmentForm.controls[key].errors);
      });
      return;
    } else {
      sendValues = this.equipmentForm.value;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.equipmentsService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Equipamento alterado com sucesso'
            );
            this.backList();
          });
      } else {
        this.equipmentsService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Equipamento registado com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/equipments/list');
  }
}
