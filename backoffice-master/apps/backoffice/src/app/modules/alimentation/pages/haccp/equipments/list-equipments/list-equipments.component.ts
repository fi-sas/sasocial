import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';

import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { EquipmentsService } from '../../services/equipments.service';

@Component({
  selector: 'fi-sas-list-equipments',
  templateUrl: './list-equipments.component.html',
  styleUrls: ['./list-equipments.component.less'],
})
export class ListEquipmentsComponent extends TableHelper implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: EquipmentsService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'service.name',
        label: 'Serviço',
        sortable: true,
        sortKey: 'service_id',
      }
    );
    this.persistentFilters = {
      withRelated: 'service',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:equipments:delete')) {
      return;
    }
    this.router.navigateByUrl('/alimentation/haccp/equipments/update/' + id);
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar equipamento',
        'Pretende eliminar este equipamento',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Equipamento eliminado com sucesso'
              );
            });
        }
      });
  }
}
