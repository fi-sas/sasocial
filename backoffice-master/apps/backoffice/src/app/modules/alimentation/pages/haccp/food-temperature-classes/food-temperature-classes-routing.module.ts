import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFoodTemperatureClassesComponent } from './form-food-temperature-classes/form-food-temperature-classes.component';
import { ListFoodTemperatureClassesComponent } from './list-food-temperature-classes/list-food-temperature-classes.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFoodTemperatureClassesComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar classes de temperatura de alimentos',
      scope: 'haccp:food_temperature_classes:read',
    },
  },
  {
    path: 'create',
    component: FormFoodTemperatureClassesComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar classe de temperatura de alimentos',
      scope: 'haccp:food_temperature_classes:create',
    },
  },
  {
    path: 'update/:id',
    component: FormFoodTemperatureClassesComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar classe de temperatura de alimentos',
      scope: 'haccp:food_temperature_classes:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodTemperatureClassesRoutingModule {}
