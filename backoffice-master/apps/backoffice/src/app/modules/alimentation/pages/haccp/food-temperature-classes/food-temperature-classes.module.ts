import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodTemperatureClassesRoutingModule } from './food-temperature-classes-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListFoodTemperatureClassesComponent } from './list-food-temperature-classes/list-food-temperature-classes.component';
import { FormFoodTemperatureClassesComponent } from './form-food-temperature-classes/form-food-temperature-classes.component';

@NgModule({
  declarations: [
    ListFoodTemperatureClassesComponent,
    FormFoodTemperatureClassesComponent,
  ],
  imports: [CommonModule, SharedModule, FoodTemperatureClassesRoutingModule],
})
export class FoodTemperatureClassesModule {}
