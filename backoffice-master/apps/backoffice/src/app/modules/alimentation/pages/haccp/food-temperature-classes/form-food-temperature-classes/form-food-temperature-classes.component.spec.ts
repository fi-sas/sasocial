import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFoodTemperatureClassesComponent } from './form-food-temperature-classes.component';

describe('FormFoodTemperatureClassesComponent', () => {
  let component: FormFoodTemperatureClassesComponent;
  let fixture: ComponentFixture<FormFoodTemperatureClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFoodTemperatureClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFoodTemperatureClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
