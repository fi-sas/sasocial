import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { FoodTemperatureClassModel } from '../../models/food-temperature-class.model';
import { FoodsTemperatureClassesService } from '../../services/foods-temperature-classes.service';

@Component({
  selector: 'fi-sas-form-food-temperature-classes',
  templateUrl: './form-food-temperature-classes.component.html',
  styleUrls: ['./form-food-temperature-classes.component.less'],
})
export class FormFoodTemperatureClassesComponent implements OnInit {
  resourceForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    minimum: new FormControl('', [Validators.required]),
    maximum: new FormControl('', [Validators.required]),
  });

  id: number;
  submitted: boolean = false;
  loading = false;

  constructor(
    public resourceService: FoodsTemperatureClassesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.loading = true;
      this.getDataEquipmentById();
    }
  }

  get f() {
    return this.resourceForm.controls;
  }

  getDataEquipmentById() {
    this.loading = true;
    this.resourceService
      .read(this.id)
      .pipe(first())
      .subscribe((results) => {
        this.resourceForm.patchValue({
          ...results.data[0],
        });
        this.loading = false;
      });
  }

  submitEquipment(edit: boolean) {
    this.submitted = true;
    let sendValues: FoodTemperatureClassModel = new FoodTemperatureClassModel();

    if (!this.resourceForm.valid) {
      Object.keys(this.resourceForm.controls).forEach((key) => {
        this.resourceForm.controls[key].markAsDirty();
        this.resourceForm.controls[key].updateValueAndValidity();
      });
      return;
    } else {
      sendValues = this.resourceForm.value;

      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.resourceService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Classe alterada com sucesso'
            );
            this.backList();
          });
      } else {
        this.resourceService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Classe registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl(
      '/alimentation/haccp/food_temperature_classes/list'
    );
  }
}
