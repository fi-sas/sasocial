import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFoodTemperatureClassesComponent } from './list-food-temperature-classes.component';

describe('ListFoodTemperatureClassesComponent', () => {
  let component: ListFoodTemperatureClassesComponent;
  let fixture: ComponentFixture<ListFoodTemperatureClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFoodTemperatureClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFoodTemperatureClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
