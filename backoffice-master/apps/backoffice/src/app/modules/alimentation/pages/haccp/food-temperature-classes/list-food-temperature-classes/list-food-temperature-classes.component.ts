import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { FoodsTemperatureClassesService } from '../../services/foods-temperature-classes.service';

@Component({
  selector: 'fi-sas-list-food-temperature-classes',
  templateUrl: './list-food-temperature-classes.component.html',
  styleUrls: ['./list-food-temperature-classes.component.less'],
})
export class ListFoodTemperatureClassesComponent
  extends TableHelper
  implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: FoodsTemperatureClassesService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'minimum',
        label: 'Minimo',
        sortable: true,
      },
      {
        key: 'maximum',
        label: 'Máximo',
        sortable: true,
      }
    );
    this.persistentFilters = {
      withRelated: 'false',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editResource(id: number) {
    if (
      !this.authService.hasPermission('haccp:food_temperature_classes:delete')
    ) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/food_temperature_classes/update/' + id
    );
  }

  deleteResource(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }
}
