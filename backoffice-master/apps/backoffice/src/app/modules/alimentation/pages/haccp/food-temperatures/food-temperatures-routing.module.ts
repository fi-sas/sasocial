import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFoodTemperaturesComponent } from './form-food-temperatures/form-food-temperatures.component';
import { ListFoodTemperaturesComponent } from './list-food-temperatures/list-food-temperatures.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFoodTemperaturesComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar temperatura de alimentos',
      scope: 'haccp:food_temperatures:read',
    },
  },
  {
    path: 'create',
    component: FormFoodTemperaturesComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar temperatura de alimentos',
      scope: 'haccp:food_temperatures:create',
    },
  },
  {
    path: 'update/:id',
    component: FormFoodTemperaturesComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar temperatura de alimentos',
      scope: 'haccp:food_temperatures:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodTemperaturesRoutingModule {}
