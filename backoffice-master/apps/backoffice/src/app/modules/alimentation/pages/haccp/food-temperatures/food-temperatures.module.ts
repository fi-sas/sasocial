import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FoodTemperaturesRoutingModule } from './food-temperatures-routing.module';
import { FormFoodTemperaturesComponent } from './form-food-temperatures/form-food-temperatures.component';
import { ListFoodTemperaturesComponent } from './list-food-temperatures/list-food-temperatures.component';

@NgModule({
  declarations: [FormFoodTemperaturesComponent, ListFoodTemperaturesComponent],
  imports: [CommonModule, FoodTemperaturesRoutingModule, SharedModule],
})
export class FoodTemperaturesModule {}
