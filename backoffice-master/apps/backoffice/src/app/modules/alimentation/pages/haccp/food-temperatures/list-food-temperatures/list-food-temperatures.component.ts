import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { FoodsTemperaturesService } from '../../services/foods-temperatures.service';

@Component({
  selector: 'fi-sas-list-food-temperatures',
  templateUrl: './list-food-temperatures.component.html',
  styleUrls: ['./list-food-temperatures.component.less'],
})
export class ListFoodTemperaturesComponent
  extends TableHelper
  implements OnInit {


  servicesLoading = false;
  services: ServiceModel[] = [];


  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: FoodsTemperaturesService,
    private servicesService: ServicesService,
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'service,lines',
    };

    this.loadServices();
    this.initTableData(this.templateService);
  }


  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }


  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:food_temperatures:delete')) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/food_temperatures/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }
}
