import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFoodTransportTemperaturesComponent } from './form-food-transport-temperatures/form-food-transport-temperatures.component';
import { ListFoodTransportTemperaturesComponent } from './list-food-transport-temperatures/list-food-transport-temperatures.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFoodTransportTemperaturesComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar temperatura transporte de alimentos',
      scope: 'haccp:food_transport_temperatures:read',
    },
  },
  {
    path: 'create',
    component: FormFoodTransportTemperaturesComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar temperatura transporte de alimentos',
      scope: 'haccp:food_transport_temperatures:create',
    },
  },
  {
    path: 'update/:id',
    component: FormFoodTransportTemperaturesComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar temperatura transporte de alimentos',
      scope: 'haccp:food_transport_temperatures:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodTransportTemperaturesRoutingModule {}
