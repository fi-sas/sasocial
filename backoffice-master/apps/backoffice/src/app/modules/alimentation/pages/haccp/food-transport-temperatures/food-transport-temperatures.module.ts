import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FoodTransportTemperaturesRoutingModule } from './food-transport-temperatures-routing.module';
import { FormFoodTransportTemperaturesComponent } from './form-food-transport-temperatures/form-food-transport-temperatures.component';
import { ListFoodTransportTemperaturesComponent } from './list-food-transport-temperatures/list-food-transport-temperatures.component';

@NgModule({
  declarations: [
    FormFoodTransportTemperaturesComponent,
    ListFoodTransportTemperaturesComponent,
  ],
  imports: [CommonModule, FoodTransportTemperaturesRoutingModule, SharedModule],
})
export class FoodTransportTemperaturesModule {}
