import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { AnomalyModel } from '../../models/anomaly.model';
import { FoodTemperatureClassModel } from '../../models/food-temperature-class.model';
import { AnomaliesService } from '../../services/anomalies.service';
import { FoodsTemperatureClassesService } from '../../services/foods-temperature-classes.service';
import { FoodsTemperaturesService } from '../../services/foods-temperatures.service';
import { FoodTransportTemperaturesService } from '../../services/foods-transport-temperatures.service';

@Component({
  selector: 'fi-sas-form-food-temperatures',
  templateUrl: './form-food-transport-temperatures.component.html',
  styleUrls: ['./form-food-transport-temperatures.component.less'],
})
export class FormFoodTransportTemperaturesComponent implements OnInit {
  id = null;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];

  recipes_first_run = true;
  recipes_first_run_ids = [];
  recipesList$: Subscription;
  recipes: RecipeModel[];
  searchRecipesChange$ = new BehaviorSubject('');
  isRecipesLoading = false;

  anomaliesLoading = false;
  anomalies: AnomalyModel[] = [];

  currentForm = new FormGroup({
    start_date: new FormControl(new Date(), [Validators.required]),
    end_date: new FormControl(new Date(), []),
    origin_service_id: new FormControl(null, [Validators.required]),
    destiny_service_id: new FormControl(null, [Validators.required]),
    lines: new FormArray([]),
  });

  isClassesLoading = false;
  classes: FoodTemperatureClassModel[] = [];

  constructor(
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private servicesService: ServicesService,
    private resourceService: FoodTransportTemperaturesService,
    private recipesService: RecipesService,
    private anomaliesService: AnomaliesService,
    private foodClassesTemperaturesService: FoodsTemperatureClassesService
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id != undefined) {
        this.loading = true;
        this.getDataById();
      } else {
        this.loadServices();
        this.loadRecipes();
        this.loadAnomalies();
        this.loadClasses();
      }
    });
  }

  ngOnInit() {}

  getDataById() {
    this.loading = true;
    this.resourceService
      .read(this.id, {
        withRelated: 'lines',
      })
      .pipe(first())
      .subscribe((results) => {
        this.currentForm.patchValue({
          ...results.data[0],
        });
        results.data[0].lines.forEach((l) => {
          this.addLine();
        });
        this.lines.setValue(results.data[0].lines);

        this.loading = false;
        this.recipes_first_run_ids = results.data[0].lines.map(
          (l) => l.recipe_id
        );

        this.loadServices();
        this.loadRecipes();
        this.loadAnomalies();
        this.loadClasses();
      });
  }

  loadClasses() {
    this.foodClassesTemperaturesService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.isClassesLoading = false))
      )
      .subscribe((result) => {
        this.classes = result.data;
      });
  }

  loadAnomalies() {
    this.anomaliesLoading = true;
    this.anomaliesService
      .list(1, -1, null, null, {
        type: 'FOOD_TEMPERATURE',
      })
      .pipe(
        first(),
        finalize(() => (this.anomaliesLoading = false))
      )
      .subscribe((result) => {
        this.anomalies = result.data;
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }

  loadSelectedRecipes() {
    return this.recipesService
      .list(1, -1, null, null, {
        id: this.recipes_first_run_ids,
        withRelated: 'translations',
      })
      .pipe(first())
      .subscribe((result) => {
        this.recipes = [...this.recipes, ...result.data];
      });
  }

  loadRecipes() {
    this.recipesList$ = this.searchRecipesChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isRecipesLoading = true;
          return this.recipesService
            .list(1, 10, null, null, {
              active: true,
              sort: 'name',
              searchFields: 'description,name',
              search: event,
              withRelated: 'translations',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.recipes = data;
        this.isRecipesLoading = false;

        if (this.recipes_first_run && this.recipes_first_run_ids.length > 0) {
          this.loadSelectedRecipes();
        }
      });
  }

  onRecipesSearch(value: string): void {
    this.searchRecipesChange$.next(value);
  }

  get lines() {
    return this.currentForm.controls['lines'] as FormArray;
  }

  addLine() {
    const lineForm = new FormGroup({
      recipe_id: new FormControl(null, [Validators.required]),
      food_temperature_class_id: new FormControl(false, [Validators.required]),
      begin_temperature: new FormControl(0, [Validators.required]),
      end_temperature: new FormControl(null, []),
      anomaly_id: new FormControl(null, []),
      observations: new FormControl('', []),
    });

    this.lines.push(lineForm);
  }

  deleteLine(lineIndex: number) {
    this.lines.removeAt(lineIndex);
  }

  selectedClasses: FoodTemperatureClassModel[] = [];

  classChanged(i, id) {
    this.selectedClasses[i] = this.classes.find((c) => c.id === id);
  }

  temperatureChanged(i, formGroup: FormGroup) {
    const begin_temperature = formGroup.value.begin_temperature;
    const end_temperature = formGroup.value.end_temperature;
    if (this.selectedClasses[i]) {
      if (begin_temperature !== null && end_temperature !== null) {
        if (
          begin_temperature >= this.selectedClasses[i].minimum &&
          begin_temperature <= this.selectedClasses[i].maximum &&
          end_temperature >= this.selectedClasses[i].minimum &&
          end_temperature <= this.selectedClasses[i].maximum
        ) {
          formGroup.controls['anomaly_id'].clearValidators();
          formGroup.controls['anomaly_id'].setValidators([]);
        } else {
          formGroup.controls['anomaly_id'].clearValidators();
          formGroup.controls['anomaly_id'].setValidators([Validators.required]);
        }
      } else {
        formGroup.controls['anomaly_id'].clearValidators();
        formGroup.controls['anomaly_id'].setValidators([]);
      }
    }
  }

  submit() {
    if (!this.currentForm.valid || !this.lines.valid) {
      Object.keys(this.currentForm.controls).forEach((key) => {
        this.currentForm.controls[key].markAsDirty();
        this.currentForm.controls[key].updateValueAndValidity();
      });

      this.lines.controls.forEach((element) => {
        const elementFg = element as FormGroup;
        Object.keys(elementFg.controls).forEach((key) => {
          elementFg.controls[key].markAsDirty();
          elementFg.controls[key].updateValueAndValidity();
        });
      });

      return;
    } else {
      if (this.id) {
        this.loading = true;
        this.resourceService
          .update(this.id, this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Temperatura de alimentos alterada com sucesso.'
            );
            this.backList();
          });
      } else {
        this.loading = true;
        this.resourceService
          .create(this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Temperatura de alimentos registada com sucesso.'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl(
      '/alimentation/haccp/food_transport_temperatures/list'
    );
  }
}
