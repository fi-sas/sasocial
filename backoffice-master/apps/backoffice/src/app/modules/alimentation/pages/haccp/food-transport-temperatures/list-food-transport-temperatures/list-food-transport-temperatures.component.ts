import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { FoodsTemperaturesService } from '../../services/foods-temperatures.service';
import { FoodTransportTemperaturesService } from '../../services/foods-transport-temperatures.service';

@Component({
  selector: 'fi-sas-list-food-temperatures',
  templateUrl: './list-food-transport-temperatures.component.html',
  styleUrls: ['./list-food-transport-temperatures.component.less'],
})
export class ListFoodTransportTemperaturesComponent
  extends TableHelper
  implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: FoodTransportTemperaturesService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'origin_service,destiny_service,lines',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (
      !this.authService.hasPermission(
        'haccp:food_transport_temperatures:delete'
      )
    ) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/food_transport_temperatures/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }
}
