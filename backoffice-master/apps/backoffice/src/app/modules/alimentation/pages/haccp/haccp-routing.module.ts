import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'equipments',
    loadChildren: './equipments/equipments.module#EquipmentsModule',
    data: { breadcrumb: 'Equipamentos', title: 'Equipamentos' },
  },
  {
    path: 'equipment_temperatures',
    loadChildren:
      './equipment-temperatures/equipment-temperatures.module#EquipmentTemperaturesModule',
    data: {
      breadcrumb: 'Temperatura equipamentos',
      title: 'Temperatura equipamentos',
    },
  },
  {
    path: 'anomalies',
    loadChildren: './anomalies/anomalies.module#AnomaliesModule',
    data: {
      breadcrumb: 'Anomalias',
      title: 'Anomalias',
    },
  },
  {
    path: 'food_temperatures',
    loadChildren:
      './food-temperatures/food-temperatures.module#FoodTemperaturesModule',
    data: {
      breadcrumb: 'Temperatura alimentos',
      title: 'Temperatura alimentos',
    },
  },
  {
    path: 'food_temperature_classes',
    loadChildren:
      './food-temperature-classes/food-temperature-classes.module#FoodTemperatureClassesModule',
    data: {
      breadcrumb: 'Classes de temperatura de alimentos',
      title: 'Classes de temperatura de alimentos',
    },
  },
  {
    path: 'food_transport_temperatures',
    loadChildren:
      './food-transport-temperatures/food-transport-temperatures.module#FoodTransportTemperaturesModule',
    data: {
      breadcrumb: 'Classes de temperatura de alimentos',
      title: 'Classes de temperatura de alimentos',
    },
  },
  {
    path: 'sanitize_ingredients',
    loadChildren:
      './sanitize-ingredients/sanitize-ingredients.module#SanitizeIngredientsModule',
    data: {
      breadcrumb: 'Higienização de ingredientes',
      title: 'Higienização de ingredientes',
    },
  },
  {
    path: 'testimony_sample_collections',
    loadChildren:
      './testimony-sample-collections/testimony-sample-collections.module#TestimonySampleCollectionsModule',
    data: {
      breadcrumb: 'Colheita de amostra testemunho',
      title: 'Colheita de amostra testemunho',
    },
  },
  {
    path: 'equipment_sanitization',
    loadChildren:
    './equipment-sanitization/equipment-sanitization.module#EquipmentSanitizationModule',
    data: {
      breadcrumb: 'Higienização de equipamentos',
      title: 'Higienização de equipamentos',
    },
  },
  {
    path: 'state_frying_oil',
    loadChildren:
    './state-frying-oil/state-frying-oil.module#StateFryingOilModule',
    data: {
      breadcrumb: 'Estado óleos de fritura',
      title: 'Estado óleos de fritura',
    },
  },
  {
    path: 'production_sheet',
    loadChildren:
    './production-sheet/production-sheet.module#ProductionSheetModule',
    data: {
      breadcrumb: 'Fichas de produção',
      title: 'Fichas de produção',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HaccpRoutingModule {}
