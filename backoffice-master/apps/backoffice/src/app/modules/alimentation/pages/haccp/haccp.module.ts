import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HaccpRoutingModule } from './haccp-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HaccpRoutingModule
  ]
})
export class HaccpModule { }
