export enum AnomalyType {
  EQUIPMENT = 'EQUIPMENT',
  FOOD_TEMPERATURE = 'FOOD_TEMPERATURE',
  FOOD_TEMPERATURE_TRANSPORT = 'FOOD_TEMPERATURE_TRANSPORT',
  TESTIMONY_SAMPLE = 'TESTIMONY_SAMPLE',
}

export class AnomalyModel {
  id: number;
  name: string;
  code: string;
  created_at: Date;
  updated_at: Date;
}
