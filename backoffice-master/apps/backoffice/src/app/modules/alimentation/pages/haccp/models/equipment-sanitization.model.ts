
export enum EquipmentSanitizationStatus {
    REGISTERED = "REGISTERED",
    VALIDATED = "VALIDATED",
    NOT_VALID = "NOT_VALID"

}

export class EquipmentSanitizationLineModel {
    equipment_id: number;
    disinfectant_product_id: number;
    lote: string;
}

export class EquipmentSanitizationModel {
 date: Date;
 user_id: number;
 service_id: number;
 status : EquipmentSanitizationStatus  
 created_at: Date;
 updated_at: Date;
 fentity_id: number;
 lines: EquipmentSanitizationLineModel[]; 
}