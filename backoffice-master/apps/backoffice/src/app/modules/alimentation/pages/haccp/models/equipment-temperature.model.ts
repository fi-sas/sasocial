export class EquipmentTemperatureModel {
  id: number;
  temperature: number;
  date: Date;
  equipment_id: number;
  user_id: number;
  anomaly_id: number;
  observations: string;
  justification: string;

  created_at: Date;
  updated_at: Date;
}
