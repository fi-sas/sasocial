export enum EquipmentType {
  REFRIGERATION = 'REFRIGERATION',
  DEEP_FRYER = 'DEEP_FRYER',
}

export class EquipmentModel {
  id: number;
  name: string;
  minimum_temperature: number;
  maximum_temperature: number;
  polar_compounds_limit: number;
  service_id: number;
  sanatize: boolean;
  type: EquipmentType;
  active: boolean;
  created_at: Date;
  updated_at: Date;
}
