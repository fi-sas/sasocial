export class FoodTemperatureClassModel {
  id: number;
  name: string;
  minimum: number;
  maximum: number;
  created_at: Date;
  updated_at: Date;
}
