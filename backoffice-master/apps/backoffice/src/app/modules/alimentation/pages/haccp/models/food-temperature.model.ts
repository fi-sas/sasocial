export class FoodTemperatureLineModel {
  food_temperature_id: number;
  recipe_id: number;
  food_temperature_class_id: number;
  temperature: number;
  food_temperature_anomaly_id: number;
  observations: string;
}

export class FoodTemperatureModel {
  id: number;
  date: Date;
  service_id: number;
  user_id: number;
  lines: FoodTemperatureLineModel[];

  created_at: Date;
  updated_at: Date;
  fentity_id: number;
}
