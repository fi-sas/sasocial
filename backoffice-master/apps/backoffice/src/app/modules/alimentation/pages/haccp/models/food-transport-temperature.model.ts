export class FoodTransportTemperatureLineModel {
  food_transport_temperature_id: number;
  recipe_id: number;
  begin_temperature: number;
  end_temperature: number;
  food_temperature_class_id: number;
  anomaly_id: number;
  observations: string;
}

export class FoodTransportTemperatureModel {
  id: number;
  start_date: Date;
  end_date: Date;
  origin_service_id: number;
  destiny_service_id: number;
  lines: FoodTransportTemperatureLineModel[];

  user_id: number;
  created_at: Date;
  updated_at: Date;
  fentity_id: number;
}
