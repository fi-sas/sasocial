import { Meal } from "../../../models/menu.model";
import { ProductModel } from "../../../models/product.model";

export interface SugestedProduct {
    product: ProductModel;
    product_id: number;
    quantity: number;
    unit_id: number;
    price: number;
}

export class ProductionSheetRecipeModel {
    recipe_id: number;
}
export class ProductionSheetLineModel {
    product_id: number;
    name: string;
    unit_id: number;
    price: number;
    quantity: number;
    lote: string;
}

export class ProductionSheetModel {
 
    date: Date;
    user_id: number;
    meal: Meal;
    service_id: number;
    doses: number;
    created_at: Date;
    updated_at: Date;
    fentity_id: number;
    recipe_ids?: number[];
    recipes: ProductionSheetRecipeModel [];
    lines: ProductionSheetLineModel[];  
}