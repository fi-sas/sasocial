export class SanitizeIngredientLineModel {
  sanitize_ingredients_id: number;
  disinfectant_product_id: number;
  product_id: number;
  lote: string;
  start_date: Date;
  end_date: Date;
  start_user_id: number;
  end_user_id: number;
  quantity: number;
  water_quantity: number;
  created_at: Date;
  updated_at: Date;
}

export class SanitizeIngredientsModel {
  id: number;
  user_id: number;
  service_id: number;
  date: Date;
  created_at: Date;
  updated_at: Date;
  fentity_id: number;
  lines: SanitizeIngredientLineModel[];
}
