import { Meal } from "../../../models/menu.model";

export class StateFryingOilLineModel {
    equipment_id: number;
    recipe_id: number;
    temperature: number;
    polar_compounds: number;
    oil_change: boolean;
}

export class StateFryingOilModel {
    date: Date;
    user_id: number;
    service_id: number;
    menu_id: number;
    meal: Meal;
    created_at: Date;
    updated_at: Date;
    fentity_id: number;
    lines: StateFryingOilLineModel[];   
}