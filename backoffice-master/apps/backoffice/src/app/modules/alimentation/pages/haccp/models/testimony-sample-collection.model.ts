import { Meal } from "../../../models/menu.model";

export class TestimonySampleCollectionLineModel {
    dish_id: number;
    collected: boolean;
    anomaly_id: number;
    observations: string;
}

export class TestimonySampleCollectionModel {
    meal: Meal;
    date: Date;
    user_id: number;
    service_id: number;
    created_at: Date;
    updated_at: Date;
    fentity_id: number;
    lines: TestimonySampleCollectionLineModel[];
}