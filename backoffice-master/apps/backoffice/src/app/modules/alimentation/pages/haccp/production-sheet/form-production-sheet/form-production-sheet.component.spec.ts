import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProductionSheetComponent } from './form-production-sheet.component';

describe('FormProductionSheetComponent', () => {
  let component: FormProductionSheetComponent;
  let fixture: ComponentFixture<FormProductionSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProductionSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProductionSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
