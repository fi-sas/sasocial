import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProductModel } from '@fi-sas/backoffice/modules/alimentation/models/product.model';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { UnitsService } from '@fi-sas/backoffice/modules/alimentation/services/units.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { ProductionSheetService } from '../../services/production-sheet.service';

@Component({
  selector: 'fi-sas-form-production-sheet',
  templateUrl: './form-production-sheet.component.html',
  styleUrls: ['./form-production-sheet.component.less']
})
export class FormProductionSheetComponent implements OnInit {
  id = null;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];

  products_first_run = true;
  products_first_run_ids = [];
  productsList$: Subscription;
  products: ProductModel[];
  searchProductsChange$ = new BehaviorSubject('');
  isProductsLoading = false;

  recipes_first_run = true;
  recipes_first_run_ids = [];
  recipesList$: Subscription;
  recipes: RecipeModel[];
  searchRecipesChange$ = new BehaviorSubject('');
  isRecipesLoading = false;

  sugested_recipes: RecipeModel[];
  isSugestedRecipesLoading = false;

  units: UnitModel[] = [];
  unitsLoading = false;

  currentForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    meal: new FormControl(null, [Validators.required]),
    service_id: new FormControl(null, [Validators.required]),
    recipe_ids: new FormControl(null, [Validators.required]),
    doses: new FormControl(0, [Validators.required]),
    lines: new FormArray([]),
  });


  constructor(
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private servicesService: ServicesService,
    private resourceService: ProductionSheetService,
    private productsService: ProductsService,
    private recipesService: RecipesService,
    private unitsService: UnitsService,
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id != undefined) {
        this.loading = true;
        this.getDataById();
      } else {
        this.loadUnits();
        this.loadServices();
        this.loadProducts();
        this.loadRecipes();
      }
    });
  }

  ngOnInit() {}

  getDataById() {
    this.loading = true;
    this.resourceService
      .read(this.id, {
        withRelated: 'lines,recipes',
      })
      .pipe(first())
      .subscribe((results) => {
        this.currentForm.patchValue({
          service_id: results.data[0].service_id,
          date: results.data[0].date,
          meal: results.data[0].meal,
        });

        results.data[0].lines.forEach((l) => {
          this.addLine();
        });
        this.lines.setValue(
          results.data[0].lines.map((l) => ({
            product_id: l.product_id,
            unit_id: l.unit_id,
            lote: l.lote,
            price: l.price,
            quantity: l.quantity,
          }))
        );

        this.loading = false;
        this.products_first_run_ids = [
          ...results.data[0].lines.map((l) => l.product_id),
        ];
        this.recipes_first_run_ids = results.data[0].recipe_ids;

        this.loadUnits();
        this.loadServices();
        this.loadProducts();
        this.loadRecipes();
      });
  }

  loadUnits() {
    this.unitsLoading = true;
    this.unitsService
      .list(1, -1, null, null, {
      })
      .pipe(
        first(),
        finalize(() => (this.unitsLoading = false))
      )
      .subscribe((result) => {
        this.units = result.data;
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }


  loadSugestedProducts() {

    if(!this.currentForm.value.recipe_ids) {
      return;
    }

    this.loading = true;
    this.resourceService.sugestedProducts(this.currentForm.value.recipe_ids, this.currentForm.value.doses).pipe(
      first(),
      finalize(() =>this.loading = false)
    ).subscribe(result => {
      this.products = [...this.products, ...result.data.map(d => d.product)];
      result.data.forEach(l => {
        if(!this.lines.value.find(af => af.product_id === l.product_id)) {
          this.addLine({
            product_id: l.product_id,
            price: l.price,
            unit_id: l.unit_id,
            quantity: l.quantity,
            lote: ''
          })
        }

      })
      console.log(result.data);
    })
  }

  loadSugestedRecipes() {

    if(
    !(this.currentForm.value.date &&
    this.currentForm.value.service_id &&
    this.currentForm.value.meal)) {
      this.sugested_recipes = [];
      return;
    }

    this.isSugestedRecipesLoading = true;
    this.resourceService
      .sugestedRecipes(
        this.currentForm.value.date,
        this.currentForm.value.service_id,
        this.currentForm.value.meal,
      )
      .pipe(
        first(),
        finalize(() => (this.isSugestedRecipesLoading = false))
      )
      .subscribe((result) => {
        this.sugested_recipes = result.data;
      });
  }

  loadSelectedRecipes() {
    return this.recipesService
      .list(1, -1, null, null, {
        id: this.recipes_first_run_ids,
        withRelated: false,
      })
      .pipe(first())
      .subscribe((result) => {
        this.recipes = [
          ...this.recipes,
          ...result.data,
        ];
      });
  }

  loadSelectedProducts() {
    return this.productsService
      .list(1, -1, null, null, {
        id: this.products_first_run_ids,
        withRelated: 'translations',
      })
      .pipe(first())
      .subscribe((result) => {
        this.products = [...this.products, ...result.data];
      });
  }

  loadProducts() {
    this.productsList$ = this.searchProductsChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isProductsLoading = true;
          return this.productsService
            .list(1, 10, null, null, {
              active: true,
              type: 'canteen',
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'translations',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.products = data;
        this.isProductsLoading = false;

        if (this.products_first_run && this.products_first_run_ids.length > 0) {
          this.loadSelectedProducts();
        }
      });
  }

  loadRecipes() {
    this.recipesList$ = this.searchRecipesChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isRecipesLoading = true;
          return this.recipesService
            .list(1, 10, null, null, {
              active: true,
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'false',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.recipes = data;
        this.isRecipesLoading = false;

        if (
          this.recipes_first_run &&
          this.recipes_first_run_ids.length > 0
        ) {
          this.loadSelectedRecipes();
        }
      });
  }

  onProductsSearch(value: string): void {
    this.searchProductsChange$.next(value);
  }

  onRecipesSearch(value: string): void {
    this.searchRecipesChange$.next(value);
  }

  get lines() {
    return this.currentForm.controls['lines'] as FormArray;
  }

  addLine(values? : {
    product_id: number,
    unit_id: number,
    price: number,
    quantity: number,
    lote: string,
  }) {
    const lineForm = new FormGroup({
      product_id: new FormControl(values && values.product_id ? values.product_id : null, [Validators.required]),
      unit_id: new FormControl(values && values.unit_id ? values.unit_id : 0, [Validators.required]),
      price: new FormControl(values && values.price ? values.price : 0, [Validators.required]),
      quantity: new FormControl(values && values.quantity ? values.quantity : 0, [Validators.required]),
      lote: new FormControl(values && values.lote ? values.lote : '', []),
    });

    this.lines.push(lineForm);
  }

  deleteLine(lineIndex: number) {
    this.lines.removeAt(lineIndex);
  }

  productSelected(product_id: number, linesForm) {
    const product = this.products.find(p => p.id === product_id);
    console.log(linesForm.patchValue({
      price: product.cost_price,
      unit_id: product.unit_id,
    }))
  }

  submit() {
    if (!this.currentForm.valid || !this.lines.valid) {
      Object.keys(this.currentForm.controls).forEach((key) => {
        this.currentForm.controls[key].markAsDirty();
        this.currentForm.controls[key].updateValueAndValidity();
      });

      this.lines.controls.forEach((element) => {
        const elementFg = element as FormGroup;
        Object.keys(elementFg.controls).forEach((key) => {
          elementFg.controls[key].markAsDirty();
          elementFg.controls[key].updateValueAndValidity();
        });
      });

      return;
    } else {
      if (this.id) {
        this.loading = true;
        this.resourceService
          .update(this.id, this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Ficha de produção alterada com sucesso.'
            );
            this.backList();
          });
      } else {
        this.loading = true;
        this.resourceService
          .create(this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Ficha de produção registada com sucesso.'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/production_sheet/list');
  }
}
