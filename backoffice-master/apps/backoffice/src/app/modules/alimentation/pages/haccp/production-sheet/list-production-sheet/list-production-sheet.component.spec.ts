import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductionSheetComponent } from './list-production-sheet.component';

describe('ListProductionSheetComponent', () => {
  let component: ListProductionSheetComponent;
  let fixture: ComponentFixture<ListProductionSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProductionSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductionSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
