import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { ProductionSheetService } from '../../services/production-sheet.service';

@Component({
  selector: 'fi-sas-list-production-sheet',
  templateUrl: './list-production-sheet.component.html',
  styleUrls: ['./list-production-sheet.component.less']
})
export class ListProductionSheetComponent  extends TableHelper implements OnInit {
constructor(
  router: Router,
  activatedRoute: ActivatedRoute,
  uiService: UiService,
  private authService: AuthService,
  private templateService: ProductionSheetService
) {
  super(uiService, router, activatedRoute);
}
ngOnInit() {
  this.persistentFilters = {
    withRelated: 'service,lines,recipes',
  };

  this.initTableData(this.templateService);
}

listComplete() {}

editSheet(id: number) {
  if (!this.authService.hasPermission('haccp:production_sheet:update')) {
    return;
  }
  this.router.navigateByUrl(
    '/alimentation/haccp/production_sheet/update/' + id
  );
}

deleteSheet(id: number) {
  this.uiService
    .showConfirm(
      'Eliminar registo',
      'Pretende eliminar este registo',
      'Eliminar'
    )
    .pipe(first())
    .subscribe((confirm) => {
      if (confirm) {
        this.templateService
          .delete(id)
          .pipe(first())
          .subscribe((result) => {
            this.searchData();
            this.uiService.showMessage(
              MessageType.success,
              'Registo eliminado com sucesso'
            );
          });
      }
    });
}
}
