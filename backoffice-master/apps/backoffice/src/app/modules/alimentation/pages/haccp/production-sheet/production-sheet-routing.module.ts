import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormProductionSheetComponent } from './form-production-sheet/form-production-sheet.component';
import { ListProductionSheetComponent } from './list-production-sheet/list-production-sheet.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListProductionSheetComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar fichas de produção',
      scope: 'haccp:production_sheet:read',
    },
  },
  {
    path: 'create',
    component: FormProductionSheetComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar registo fichas de produção',
      scope: 'haccp:production_sheet:create',
    },
  },
  {
    path: 'update/:id',
    component: FormProductionSheetComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar registo fichas de produção',
      scope: 'haccp:production_sheet:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionSheetRoutingModule { }
