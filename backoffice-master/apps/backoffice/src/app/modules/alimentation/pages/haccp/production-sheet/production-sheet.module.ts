import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionSheetRoutingModule } from './production-sheet-routing.module';
import { FormProductionSheetComponent } from './form-production-sheet/form-production-sheet.component';
import { ListProductionSheetComponent } from './list-production-sheet/list-production-sheet.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [FormProductionSheetComponent, ListProductionSheetComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProductionSheetRoutingModule
  ]
})
export class ProductionSheetModule { }
