import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProductModel } from '@fi-sas/backoffice/modules/alimentation/models/product.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { FoodTemperatureClassModel } from '../../models/food-temperature-class.model';
import { SanitizeIngredientsService } from '../../services/sanitize-ingredients.service';

@Component({
  templateUrl: './form-sanitize-ingredients.component.html',
  styleUrls: ['./form-sanitize-ingredients.component.less'],
})
export class FormSanitizeIngredientsComponent implements OnInit {
  id = null;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];

  products_first_run = true;
  products_first_run_ids = [];
  productsList$: Subscription;
  products: ProductModel[];
  searchProductsChange$ = new BehaviorSubject('');
  isProductsLoading = false;

  disinfectant_products_first_run = true;
  disinfectant_products_first_run_ids = [];
  disinfectant_productsList$: Subscription;
  disinfectant_products: ProductModel[];
  searchDisinfectantProductsChange$ = new BehaviorSubject('');
  isDisinfectantProductsLoading = false;

  currentForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    service_id: new FormControl(null, [Validators.required]),
    lines: new FormArray([]),
  });

  isClassesLoading = false;
  classes: FoodTemperatureClassModel[] = [];

  constructor(
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private servicesService: ServicesService,
    private resourceService: SanitizeIngredientsService,
    private productsService: ProductsService
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id != undefined) {
        this.loading = true;
        this.getDataById();
      } else {
        this.loadServices();
        this.loadProducts();
        this.loadDesinfectantProducts();
      }
    });
  }

  ngOnInit() {}

  getDataById() {
    this.loading = true;
    this.resourceService
      .read(this.id, {
        withRelated: 'lines',
      })
      .pipe(first())
      .subscribe((results) => {
        this.currentForm.patchValue({
          service_id: results.data[0].service_id,
          date: results.data[0].date,
        });

        results.data[0].lines.forEach((l) => {
          this.addLine();
        });
        this.lines.setValue(
          results.data[0].lines.map((l) => ({
            product_id: l.product_id,
            disinfectant_product_id: l.disinfectant_product_id,
            lote: l.lote,
            start_date: l.start_date,
            end_date: l.end_date,
            quantity: l.quantity,
            water_quantity: l.water_quantity,
            start_user_id: l.start_user_id,
            end_user_id: l.end_user_id,
          }))
        );

        this.loading = false;
        this.products_first_run_ids = [
          ...results.data[0].lines.map((l) => l.product_id),
        ];
        this.disinfectant_products_first_run_ids = [
          ...results.data[0].lines.map((l) => l.disinfectant_product_id),
        ];

        this.loadServices();
        this.loadProducts();
        this.loadDesinfectantProducts();
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }

  loadSelectedDisinfectantProducts() {
    return this.productsService
      .list(1, -1, null, null, {
        id: this.disinfectant_products_first_run_ids,
        withRelated: 'translations',
      })
      .pipe(first())
      .subscribe((result) => {
        this.disinfectant_products = [
          ...this.disinfectant_products,
          ...result.data,
        ];
      });
  }

  loadSelectedProducts() {
    return this.productsService
      .list(1, -1, null, null, {
        id: this.products_first_run_ids,
        withRelated: 'translations',
      })
      .pipe(first())
      .subscribe((result) => {
        this.products = [...this.products, ...result.data];
      });
  }

  loadProducts() {
    this.productsList$ = this.searchProductsChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isProductsLoading = true;
          return this.productsService
            .list(1, 10, null, null, {
              active: true,
              type: 'canteen',
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'translations',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.products = data;
        this.isProductsLoading = false;

        if (this.products_first_run && this.products_first_run_ids.length > 0) {
          this.loadSelectedProducts();
        }
      });
  }

  loadDesinfectantProducts() {
    this.disinfectant_productsList$ = this.searchDisinfectantProductsChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isDisinfectantProductsLoading = true;
          return this.productsService
            .list(1, 10, null, null, {
              active: true,
              type: 'maintenance',
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'translations',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.disinfectant_products = data;
        this.isDisinfectantProductsLoading = false;

        if (
          this.disinfectant_products_first_run &&
          this.disinfectant_products_first_run_ids.length > 0
        ) {
          this.loadSelectedDisinfectantProducts();
        }
      });
  }

  onProductsSearch(value: string): void {
    this.searchProductsChange$.next(value);
  }

  onDisinfectantProductsSearch(value: string): void {
    this.searchDisinfectantProductsChange$.next(value);
  }

  get lines() {
    return this.currentForm.controls['lines'] as FormArray;
  }

  addLine() {
    const lineForm = new FormGroup({
      product_id: new FormControl(null, [Validators.required]),
      disinfectant_product_id: new FormControl(null, [Validators.required]),
      lote: new FormControl('', []),
      start_date: new FormControl(new Date(), [Validators.required]),
      end_date: new FormControl(new Date(), []),
      quantity: new FormControl(0, [Validators.required]),
      water_quantity: new FormControl(0, [Validators.required]),
      start_user_id: new FormControl(null, []),
      end_user_id: new FormControl(null, []),
    });

    this.lines.push(lineForm);
  }

  deleteLine(lineIndex: number) {
    this.lines.removeAt(lineIndex);
  }

  submit() {
    if (!this.currentForm.valid || !this.lines.valid) {
      Object.keys(this.currentForm.controls).forEach((key) => {
        this.currentForm.controls[key].markAsDirty();
        this.currentForm.controls[key].updateValueAndValidity();
      });

      this.lines.controls.forEach((element) => {
        const elementFg = element as FormGroup;
        Object.keys(elementFg.controls).forEach((key) => {
          elementFg.controls[key].markAsDirty();
          elementFg.controls[key].updateValueAndValidity();
        });
      });

      return;
    } else {
      if (this.id) {
        this.loading = true;
        this.resourceService
          .update(this.id, this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'higienização de ingredientes alterado com sucesso.'
            );
            this.backList();
          });
      } else {
        this.loading = true;
        this.resourceService
          .create(this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'higienização de ingredientesregistado com sucesso.'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/sanitize_ingredients/list');
  }
}
