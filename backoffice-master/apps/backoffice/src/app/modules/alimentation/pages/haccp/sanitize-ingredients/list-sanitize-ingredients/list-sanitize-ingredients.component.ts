import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { SanitizeIngredientsService } from '../../services/sanitize-ingredients.service';

@Component({
  templateUrl: './list-sanitize-ingredients.component.html',
  styleUrls: ['./list-sanitize-ingredients.component.less'],
})
export class ListSanitizeIngredientsComponent
  extends TableHelper
  implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: SanitizeIngredientsService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'service,lines',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:sanitize_ingredients:delete')) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/sanitize_ingredients/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }
}
