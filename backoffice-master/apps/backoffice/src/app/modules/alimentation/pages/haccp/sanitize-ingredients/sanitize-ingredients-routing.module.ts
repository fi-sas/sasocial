import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSanitizeIngredientsComponent } from './form-sanitize-ingredients/form-sanitize-ingredients.component';
import { ListSanitizeIngredientsComponent } from './list-sanitize-ingredients/list-sanitize-ingredients.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSanitizeIngredientsComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar higienização de ingredientes',
      scope: 'haccp:sanitize_ingredients:read',
    },
  },
  {
    path: 'create',
    component: FormSanitizeIngredientsComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar registo  higienização de ingredientes',
      scope: 'haccp:sanitize_ingredients:create',
    },
  },
  {
    path: 'update/:id',
    component: FormSanitizeIngredientsComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar registo  higienização de ingredientes',
      scope: 'haccp:sanitize_ingredients:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SanitizeIngredientsRoutingModule {}
