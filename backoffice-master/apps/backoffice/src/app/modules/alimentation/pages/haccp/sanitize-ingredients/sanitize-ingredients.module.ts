import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SanitizeIngredientsRoutingModule } from './sanitize-ingredients-routing.module';
import { FormSanitizeIngredientsComponent } from './form-sanitize-ingredients/form-sanitize-ingredients.component';
import { ListSanitizeIngredientsComponent } from './list-sanitize-ingredients/list-sanitize-ingredients.component';

@NgModule({
  declarations: [
    FormSanitizeIngredientsComponent,
    ListSanitizeIngredientsComponent,
  ],
  imports: [CommonModule, SanitizeIngredientsRoutingModule, SharedModule],
})
export class SanitizeIngredientsModule {}
