import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { FiscalEntityModel } from '../../../models/fiscal-entity.model';
import { AlimentationService } from '../../../services/alimentation.service';
import { FoodTransportTemperatureModel } from '../models/food-transport-temperature.model';

@Injectable({
  providedIn: 'root',
})
export class FoodTransportTemperaturesService extends Repository<FoodTransportTemperatureModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.FOOD_TRANSPORT_TEMPERATURE';
    this.entity_url = 'FOOD.FOOD_TRANSPORT_TEMPERATURE_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe((entity) => {
      this.selectedEntity = entity;
      if (this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id,
        };
      }
    });
  }
}
