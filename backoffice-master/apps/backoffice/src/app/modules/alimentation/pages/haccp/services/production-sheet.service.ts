import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { FiscalEntityModel } from '../../../models/fiscal-entity.model';
import { Meal } from '../../../models/menu.model';
import { RecipeModel } from '../../../models/recipe.model';
import { AlimentationService } from '../../../services/alimentation.service';
import { ProductionSheetModel, SugestedProduct } from '../models/production-sheet.model';

@Injectable({
  providedIn: 'root'
})
export class ProductionSheetService extends Repository<ProductionSheetModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.PRODUCTION_SHEETS';
    this.entity_url = 'FOOD.PRODUCTION_SHEETS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe((entity) => {
      this.selectedEntity = entity;
      if (this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id,
        };
      }
    });
  }

  sugestedRecipes(date: Date, service_id: number, meal: Meal): Observable<Resource<RecipeModel>> {
    return this.resourceService.list<RecipeModel>(
      this.urlService.get('FOOD.PRODUCTION_SHEETS_SUGESTED_RECIPES', {}),
      { 
        params: new HttpParams().append('service_id', service_id.toString()).append('meal', meal.toString()).append('date', date.toISOString()),
        headers: this.getHeaders()
      });
  }

  sugestedProducts(recipe_ids: number[], doses: number): Observable<Resource<SugestedProduct>> {
    let params = new HttpParams();

    recipe_ids.forEach(recipe_id => params = params.append('recipe_ids', recipe_id.toString()));

    return this.resourceService.list<SugestedProduct>(
      this.urlService.get('FOOD.PRODUCTION_SHEETS_SUGESTED_PRODUCTS', {}),
      { 
        params: params.append('doses', doses.toString()),
        headers: this.getHeaders()
      });
  }
  
}
