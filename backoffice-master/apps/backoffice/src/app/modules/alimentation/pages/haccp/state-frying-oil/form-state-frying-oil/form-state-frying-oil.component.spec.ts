import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormStateFryingOilComponent } from './form-state-frying-oil.component';

describe('FormStateFryingOilComponent', () => {
  let component: FormStateFryingOilComponent;
  let fixture: ComponentFixture<FormStateFryingOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormStateFryingOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStateFryingOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
