import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Meal } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { EquipmentModel } from '../../models/equipment.model';
import { EquipmentsService } from '../../services/equipments.service';
import { StateFryingOilService } from '../../services/state-frying-oil.service';

@Component({
  selector: 'fi-sas-form-state-frying-oil',
  templateUrl: './form-state-frying-oil.component.html',
  styleUrls: ['./form-state-frying-oil.component.less']
})
export class FormStateFryingOilComponent implements OnInit {

  id = null;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];

  equipments_first_run = true;
  equipments_first_run_ids = [];
  equipmentsList$: Subscription;
  equipments: EquipmentModel[];
  searchEquipmentsChange$ = new BehaviorSubject('');
  isEquipmentsLoading = false;

  recipes_first_run = true;
  recipes_first_run_ids = [];
  recipesList$: Subscription;
  recipes: RecipeModel[];
  searchRecipesChange$ = new BehaviorSubject('');
  isRecipesLoading = false;

  currentForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    service_id: new FormControl(null, [Validators.required]),
    meal: new FormControl(Meal.LUNCH, [Validators.required]),
    lines: new FormArray([]),
  });

  constructor(
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private servicesService: ServicesService,
    private resourceService: StateFryingOilService,
    private recipesService: RecipesService,
    private equipmentsService: EquipmentsService,
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id != undefined) {
        this.loading = true;
        this.getDataById();
      } else {
        this.loadServices();
        this.loadEquipments();
        this.loadRecipes();
      }
    });
  }

  ngOnInit() {}

  getDataById() {
    this.loading = true;
    this.resourceService
      .read(this.id, {
        withRelated: 'lines',
      })
      .pipe(first())
      .subscribe((results) => {
        this.currentForm.patchValue({
          service_id: results.data[0].service_id,
          date: results.data[0].date,
        });

        results.data[0].lines.forEach((l) => {
          this.addLine();
        });
        this.lines.setValue(
          results.data[0].lines.map((l) => ({
            equipment_id: l.equipment_id,
            recipe_id: l.recipe_id,
            temperature: l.temperature,
            polar_compounds: l.polar_compounds,
            oil_change: l.oil_change,
          }))
        );

        this.loading = false;
        this.equipments_first_run_ids = [
          ...results.data[0].lines.map((l) => l.equipment_id),
        ];
        this.recipes_first_run_ids = [
          ...results.data[0].lines.map((l) => l.recipe_id),
        ];

        this.loadServices();
        this.loadEquipments();
        this.loadRecipes();
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }

  loadSelectedRecipes() {
    return this.recipesService
      .list(1, -1, null, null, {
        id: this.recipes_first_run_ids,
        withRelated: 'false',
      })
      .pipe(first())
      .subscribe((result) => {
        this.recipes = [
          ...this.recipes,
          ...result.data,
        ];
      });
  }

  loadSelectedEquipments() {
    return this.equipmentsService
      .list(1, -1, null, null, {
        id: this.equipments_first_run_ids,
        withRelated: 'false',
      })
      .pipe(first())
      .subscribe((result) => {
        this.equipments = [...this.equipments, ...result.data];
      });
  }

  loadEquipments() {
    this.equipmentsList$ = this.searchEquipmentsChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isEquipmentsLoading = true;
          return this.equipmentsService
            .list(1, 10, null, null, {
              active: true,
              sort: '-id',
              searchFields: 'name',
              search: event,
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.equipments = data;
        this.isEquipmentsLoading = false;

        if (this.equipments_first_run && this.equipments_first_run_ids.length > 0) {
          this.loadSelectedEquipments();
        }
      });
  }

  loadRecipes() {
    this.recipesList$ = this.searchRecipesChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isRecipesLoading = true;
          return this.recipesService
            .list(1, 10, null, null, {
              active: true,
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'false',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.recipes = data;
        this.isRecipesLoading = false;

        if (
          this.recipes_first_run &&
          this.recipes_first_run_ids.length > 0
        ) {
          this.loadSelectedRecipes();
        }
      });
  }

  onEquipmentsSearch(value: string): void {
    this.searchEquipmentsChange$.next(value);
  }

  onRecipeSearch(value: string): void {
    this.searchRecipesChange$.next(value);
  }

  get lines() {
    return this.currentForm.controls['lines'] as FormArray;
  }

  addLine() {
    const lineForm = new FormGroup({
      equipment_id: new FormControl(null, [Validators.required]),
      recipe_id: new FormControl(null, [Validators.required]),
      temperature: new FormControl(null, [Validators.required]),
      polar_compounds: new FormControl(null, [Validators.required]),
      oil_change: new FormControl(false, []),
    });

    this.lines.push(lineForm);
  }

  deleteLine(lineIndex: number) {
    this.lines.removeAt(lineIndex);
  }

  submit() {
    if (!this.currentForm.valid || !this.lines.valid) {
      Object.keys(this.currentForm.controls).forEach((key) => {
        this.currentForm.controls[key].markAsDirty();
        this.currentForm.controls[key].updateValueAndValidity();
      });

      this.lines.controls.forEach((element) => {
        const elementFg = element as FormGroup;
        Object.keys(elementFg.controls).forEach((key) => {
          elementFg.controls[key].markAsDirty();
          elementFg.controls[key].updateValueAndValidity();
        });
      });

      return;
    } else {
      if (this.id) {
        this.loading = true;
        this.resourceService
          .update(this.id, this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Registo alterado com sucesso.'
            );
            this.backList();
          });
      } else {
        this.loading = true;
        this.resourceService
          .create(this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Registo guardado com sucesso.'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/state_frying_oil/list');
  }
}
