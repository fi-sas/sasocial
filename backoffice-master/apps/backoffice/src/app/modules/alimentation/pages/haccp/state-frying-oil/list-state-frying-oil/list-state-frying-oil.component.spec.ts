import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListStateFryingOilComponent } from './list-state-frying-oil.component';

describe('ListStateFryingOilComponent', () => {
  let component: ListStateFryingOilComponent;
  let fixture: ComponentFixture<ListStateFryingOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListStateFryingOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStateFryingOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
