import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { StateFryingOilService } from '../../services/state-frying-oil.service';

@Component({
  selector: 'fi-sas-list-state-frying-oil',
  templateUrl: './list-state-frying-oil.component.html',
  styleUrls: ['./list-state-frying-oil.component.less']
})
export class ListStateFryingOilComponent extends TableHelper implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private templateService: StateFryingOilService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'service',
    };

    this.initTableData(this.templateService);
  }

  listComplete() {}

  editEquip(id: number) {
    if (!this.authService.hasPermission('haccp:state_frying_oil:update')) {
      return;
    }
    this.router.navigateByUrl(
      '/alimentation/haccp/state_frying_oil/update/' + id
    );
  }

  deleteEquip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar registo',
        'Pretende eliminar este registo',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.templateService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.searchData();
              this.uiService.showMessage(
                MessageType.success,
                'Registo eliminado com sucesso'
              );
            });
        }
      });
  }

}
