import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormStateFryingOilComponent } from './form-state-frying-oil/form-state-frying-oil.component';
import { ListStateFryingOilComponent } from './list-state-frying-oil/list-state-frying-oil.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListStateFryingOilComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar estado óleos de fritura',
      scope: 'haccp:state_frying_oil:read',
    },
  },
  {
    path: 'create',
    component: FormStateFryingOilComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar registo estado óleos de fritura',
      scope: 'haccp:state_frying_oil:create',
    },
  },
  {
    path: 'update/:id',
    component: FormStateFryingOilComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar registo estado óleos de fritura',
      scope: 'haccp:state_frying_oil:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StateFryingOilRoutingModule { }
