import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StateFryingOilRoutingModule } from './state-frying-oil-routing.module';
import { FormStateFryingOilComponent } from './form-state-frying-oil/form-state-frying-oil.component';
import { ListStateFryingOilComponent } from './list-state-frying-oil/list-state-frying-oil.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [FormStateFryingOilComponent, ListStateFryingOilComponent],
  imports: [
    CommonModule,
    SharedModule,
    StateFryingOilRoutingModule
  ]
})
export class StateFryingOilModule { }
