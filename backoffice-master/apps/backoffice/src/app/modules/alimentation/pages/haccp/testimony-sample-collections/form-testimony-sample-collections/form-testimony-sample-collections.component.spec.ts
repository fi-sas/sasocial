import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTestimonySampleCollectionsComponent } from './form-testimony-sample-collections.component';

describe('FormTestimonySampleCollectionsComponent', () => {
  let component: FormTestimonySampleCollectionsComponent;
  let fixture: ComponentFixture<FormTestimonySampleCollectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTestimonySampleCollectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTestimonySampleCollectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
