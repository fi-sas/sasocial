import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { DishsService } from '@fi-sas/backoffice/modules/alimentation/services/dishs.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { AnomalyModel } from '../../models/anomaly.model';
import { FoodTemperatureClassModel } from '../../models/food-temperature-class.model';
import { AnomaliesService } from '../../services/anomalies.service';
import { TestimonySampleCollectionsService } from '../../services/testimony-sample-collections.service';

@Component({
  selector: 'fi-sas-form-testimony-sample-collections',
  templateUrl: './form-testimony-sample-collections.component.html',
  styleUrls: ['./form-testimony-sample-collections.component.less']
})
export class FormTestimonySampleCollectionsComponent implements OnInit {
  id = null;
  loading = false;

  servicesLoading = false;
  services: ServiceModel[] = [];

  dishs_first_run = true;
  dishs_first_run_ids = [];
  dishsList$: Subscription;
  dishs: DishModel[];
  searchDishsChange$ = new BehaviorSubject('');
  isDishsLoading = false;

  anomaliesLoading = false;
  anomalies: AnomalyModel[] = [];

  currentForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    service_id: new FormControl(null, [Validators.required]),
    meal: new FormControl(null, [Validators.required]),
    lines: new FormArray([]),
  });

  isClassesLoading = false;
  classes: FoodTemperatureClassModel[] = [];

  constructor(
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private servicesService: ServicesService,
    private resourceService: TestimonySampleCollectionsService,
    private dishsService: DishsService,
    private anomaliesService: AnomaliesService,
  ) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id != undefined) {
        this.loading = true;
        this.getDataById();
      } else {
        this.loadServices();
        this.loadDishs();
        this.loadAnomalies();
      }
    });
  }

  ngOnInit() {}

  getDataById() {
    this.loading = true;
    this.resourceService
      .read(this.id, {
        withRelated: 'lines',
      })
      .pipe(first())
      .subscribe((results) => {
        this.currentForm.patchValue({
          service_id: results.data[0].service_id,
          date: results.data[0].date,
          meal: results.data[0].meal,
        });

        results.data[0].lines.forEach((l) => {
          this.addLine();
        });
        this.lines.setValue(
          results.data[0].lines.map((l) => ({
            ...l
          }))
        );

        this.loading = false;
        this.dishs_first_run_ids = [
          ...results.data[0].lines.map((l) => l.dish_id),
        ];

        this.loadServices();
        this.loadDishs();
        this.loadAnomalies();
      });
  }


  loadAnomalies() {
    this.anomaliesLoading = true;
    this.anomaliesService
      .list(1, -1, null, null, {
        type: 'FOOD_TEMPERATURE',
      })
      .pipe(
        first(),
        finalize(() => (this.anomaliesLoading = false))
      )
      .subscribe((result) => {
        this.anomalies = result.data;
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(1, -1, null, null, {
        type: 'canteen',
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((result) => {
        this.services = result.data;
      });
  }

  loadSelectedDishs() {
    return this.dishsService
      .list(1, -1, null, null, {
        id: this.dishs_first_run_ids,
        withRelated: 'translations',
      })
      .pipe(first())
      .subscribe((result) => {
        this.dishs = [...this.dishs, ...result.data];
      });
  }

  loadDishs() {
    this.dishsList$ = this.searchDishsChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(
        switchMap((event) => {
          this.isDishsLoading = true;
          return this.dishsService
            .list(1, 10, null, null, {
              active: true,
              sort: '-id',
              searchFields: 'description,name',
              search: event,
              withRelated: 'translations',
            })
            .pipe(first());
        })
      )
      .pipe(map((res) => res.data))
      .subscribe((data) => {
        this.dishs = data;
        this.isDishsLoading = false;

        if (this.dishs_first_run && this.dishs_first_run_ids.length > 0) {
          this.loadSelectedDishs();
        }
      });
  }

  onDishsSearch(value: string): void {
    this.searchDishsChange$.next(value);
  }

  get lines() {
    return this.currentForm.controls['lines'] as FormArray;
  }

  addLine() {
    const lineForm = new FormGroup({
      dish_id: new FormControl(null, [Validators.required]),
      collected: new FormControl(false, [Validators.required]),
      anomaly_id: new FormControl(null, []),
      observations: new FormControl('', []),
    });

    this.lines.push(lineForm);
  }

  deleteLine(lineIndex: number) {
    this.lines.removeAt(lineIndex);
  }

  submit() {
    if (!this.currentForm.valid || !this.lines.valid) {
      Object.keys(this.currentForm.controls).forEach((key) => {
        this.currentForm.controls[key].markAsDirty();
        this.currentForm.controls[key].updateValueAndValidity();
      });

      this.lines.controls.forEach((element) => {
        const elementFg = element as FormGroup;
        Object.keys(elementFg.controls).forEach((key) => {
          elementFg.controls[key].markAsDirty();
          elementFg.controls[key].updateValueAndValidity();
        });
      });

      return;
    } else {
      if (this.id) {
        this.loading = true;
        this.resourceService
          .update(this.id, this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Registo alterado com sucesso.'
            );
            this.backList();
          });
      } else {
        this.loading = true;
        this.resourceService
          .create(this.currentForm.value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Registo guardado com sucesso.'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/haccp/testimony_sample_collections/list');
  }

}
