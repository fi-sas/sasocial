import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTestimonySampleCollectionsComponent } from './list-testimony-sample-collections.component';

describe('ListTestimonySampleCollectionsComponent', () => {
  let component: ListTestimonySampleCollectionsComponent;
  let fixture: ComponentFixture<ListTestimonySampleCollectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTestimonySampleCollectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTestimonySampleCollectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
