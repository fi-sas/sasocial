import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTestimonySampleCollectionsComponent } from './form-testimony-sample-collections/form-testimony-sample-collections.component';
import { ListTestimonySampleCollectionsComponent } from './list-testimony-sample-collections/list-testimony-sample-collections.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTestimonySampleCollectionsComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar colheita de amostra testemunho',
      scope: 'haccp:testimony_sample_collections:read',
    },
  },
  {
    path: 'create',
    component: FormTestimonySampleCollectionsComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar registo  colheita de amostra testemunho',
      scope: 'haccp:testimony_sample_collections:create',
    },
  },
  {
    path: 'update/:id',
    component: FormTestimonySampleCollectionsComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar registo  colheita de amostra testemunho',
      scope: 'haccp:testimony_sample_collections:update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestimonySampleCollectionsRoutingModule {}
