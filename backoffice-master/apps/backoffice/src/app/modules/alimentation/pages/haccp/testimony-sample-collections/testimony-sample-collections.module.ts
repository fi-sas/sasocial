import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonySampleCollectionsRoutingModule } from './testimony-sample-collections-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormTestimonySampleCollectionsComponent } from './form-testimony-sample-collections/form-testimony-sample-collections.component';
import { ListTestimonySampleCollectionsComponent } from './list-testimony-sample-collections/list-testimony-sample-collections.component';

@NgModule({
  declarations: [FormTestimonySampleCollectionsComponent, ListTestimonySampleCollectionsComponent],
  imports: [
    CommonModule,
    SharedModule,
    TestimonySampleCollectionsRoutingModule,
  ],
})
export class TestimonySampleCollectionsModule {}
