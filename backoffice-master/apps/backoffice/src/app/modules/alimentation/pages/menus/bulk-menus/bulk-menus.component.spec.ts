import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkMenusComponent } from './bulk-menus.component';

describe('BulkMenusComponent', () => {
  let component: BulkMenusComponent;
  let fixture: ComponentFixture<BulkMenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkMenusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
