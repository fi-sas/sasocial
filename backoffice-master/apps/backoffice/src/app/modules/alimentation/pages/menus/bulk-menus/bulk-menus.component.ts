import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';

import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';
import { Meal, MenuModel, MenuDish } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { Component, OnInit } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FiConfigurator } from '@fi-sas/configurator';
import { Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, switchMap, map, first, finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { MenusService } from '../../../services/menus.service';
import { DishsService } from '../../../services/dishs.service';
import { DishTypesService } from '../../../services/dish-types.service';
import { MenuBulkDto } from '../../../dtos/menu-bulk.dto';

@Component({
  selector: 'fi-sas-bulk-menus',
  templateUrl: './bulk-menus.component.html',
  styleUrls: ['./bulk-menus.component.less']
})
export class BulkMenusComponent implements OnInit {

  Meal = Meal;
  selectedMeals: Meal[] = [];


  selectedDates: Date[] = [
    moment().add(1, 'day').toDate()
  ];

  servicesLoading = false;
  services: ServiceModel[] = [];
  selectedServices: number[] = [];
  submitted = false;

  isLoading = false;
  isLoadingSelects = false;
  isUpdate = false;
  updateMenu: MenuModel = new MenuModel();
  selectedDish: DishModel = null;
  selectedDishType: DishTypeModel = null;
  selectedAvailable = true;
  selectedDoses_Number = 0;

  dishList: DishModel[];
  dishTypeList: DishTypeModel[];

  searchChange$ = new BehaviorSubject('');

  defaultLang = 1;

  constructor(
    private servicesService: ServicesService,
    private menusService: MenusService,
    private dishsService: DishsService,
    private dishTypesService: DishTypesService,
    private uiService: UiService,
    private configurator: FiConfigurator
  ) {
    this.defaultLang = configurator.getOption('DEFAULT_LANG_ID', 1);
  }

  ngOnInit() {

    this.loadServices();

    this.updateMenu.dishs = [];

    this.isLoadingSelects = true;
    this.dishTypesService.list(1, -1, null, null, {
      withRelated: "translations",
    }).subscribe(res => {
      this.dishTypeList = res.data;
      this.isLoadingSelects = false;
    });


    const dishsList$: Observable<DishModel[]> = this.searchChange$.asObservable()
      .pipe(debounceTime(500)).pipe(
        switchMap(event => {
          this.isLoadingSelects = true;
          return this.dishsService.list(1, 10, null, null,
            {
              withRelated: "translations",
              types: this.selectedDishType ? [this.selectedDishType.id] : [],
              search: event,
              active: true,
              searchFields: 'description,name',
              dish_type_id: this.selectedDishType ? this.selectedDishType.id : []
            }
          )
        }
        )).pipe(map(res => res.data));

    dishsList$.subscribe(data => {
      this.dishList = data;
      this.isLoadingSelects = false;
    });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService.list(0, -1, null, null, {
      type: 'canteen'
    }).pipe(first(), finalize(() => this.servicesLoading = false)).subscribe(result => {
      this.services = result.data;
    });
  }

  addDate(value: { nativeDate: Date }) {
    if (value && value.nativeDate) {
      const nd = moment(value.nativeDate);
      if (!this.selectedDates.find(d => moment(d).isSame(nd, 'day'))) {
        this.selectedDates.push(value.nativeDate);
      }
    }
  }

  reset() {
    this.selectedDates = [];
    this.selectedMeals = [];
    this.selectedServices = [];
    this.updateMenu.dishs = [];
  }

  submit() {
    this.submitted = true;
    if (this.selectedServices.length > 0 && this.selectedMeals.length > 0 && this.selectedDates.length > 0) {
      this.submitted = false;
      let error = false;
      this.updateMenu.dishs.forEach((dish) => {
        if (dish.doses_available < 0) {
          error = true;
        }
      })
      if (error) {
        this.uiService.showMessage(
          MessageType.error,
          'Não pode haver quantidades negativas'
        );
        this.isLoading = false;
      } else {

        const value: MenuBulkDto = {
          meals: this.selectedMeals,
          service_ids: this.selectedServices,
          dates: this.selectedDates.map(d => moment(d).format('YYYY-MM-DD')),
          dishs: this.updateMenu.dishs.map(dm => ({
            dishType_id: dm.type.id,
            dish_id: dm.dish.id,
            doses_available: dm.doses_available,
            available: dm.available
          }))
        };

        if (this.updateMenu.dishs.length === 0) {
          this.uiService.showMessage(
            MessageType.warning,
            'Deve adicionar pratos a lista'
          );
          this.isLoading = false;
          return;
        }

        if (this.updateMenu.dishs.length !== 0) {
          this.isLoading = true;
          this.menusService.createBulk(value).subscribe(
            result => {
              this.isLoading = false;
              this.uiService.showMessage(
                MessageType.success,
                'Ementas criadas com sucesso'
              );
              this.reset();
            },
            err => {
              this.isLoading = false;
            });
        }
      }
    }


  }

  addDish() {
    if (!this.selectedDishType) {
      this.uiService.showMessage(MessageType.warning, 'Selecione um tipo de prato');
      return;
    }

    if (!this.selectedDish) {
      this.uiService.showMessage(MessageType.warning, 'Selecione um prato');
      return;
    }

    if (!this.selectedDoses_Number) {
      this.selectedDoses_Number = 0;
    }

    if (this.updateMenu.dishs.findIndex(dishM => dishM.dish.id === this.selectedDish.id &&
      dishM.type.id === this.selectedDishType.id) !== -1) {
      this.uiService.showMessage(MessageType.warning, 'Já existe esse prato e tipo de prato na lista');
      return;
    }

    const tmd = new MenuDish();
    tmd.type = this.selectedDishType;
    tmd.dish = this.selectedDish;
    tmd.doses_available = this.selectedDoses_Number;
    tmd.available = this.selectedAvailable;
    this.updateMenu.dishs = [tmd, ...this.updateMenu.dishs];
    this.selectedDish = null;
    this.selectedDoses_Number = 0;
    this.selectedAvailable = true;
  }

  removeDish(dish) {
    const index = this.updateMenu.dishs.indexOf(dish);
    if (index > -1) {
      this.updateMenu.dishs.splice(index, 1);
    }
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  onTypeChanged(value: number) {
    this.dishList = [];
    this.searchChange$.next('');
    this.selectedDish = null;
  }

  disabledDate = (current: Date): boolean => {
    return moment(current).isBefore(moment(), 'day');
  };
}
