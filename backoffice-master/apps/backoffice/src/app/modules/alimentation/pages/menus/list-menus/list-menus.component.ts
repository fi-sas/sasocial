import { NzModalService } from 'ng-zorro-antd';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { Meal, MenuModel } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { MenusService } from '@fi-sas/backoffice/modules/alimentation/services/menus.service';
import { getISOWeek } from 'date-fns';
import * as moment from 'moment';
import { ServicesService } from '../../../services/services.service';
import { CopyMenuDayComponent } from '../../../components/copy-menu-day/copy-menu-day.component';
import { CopyMenuWeekComponent } from '../../../components/copy-menu-week/copy-menu-week.component';

@Component({
  selector: 'fi-sas-list-menus',
  templateUrl: './list-menus.component.html',
  styleUrls: ['./list-menus.component.less']
})
export class ListMenusComponent implements OnInit {
  Meal = Meal;
  services: ServiceModel[] = [];
  selectedService: ServiceModel;

  loading = true;
  total = 0;
  selectedWeek: Date = new Date();
  week: Date[] = [];
  menus: MenuModel[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService,
    private menusService: MenusService,
    private modalService: NzModalService
  ) {
  }

  ngOnInit() {

    this.servicesService.list(1,-1,null,null, { type: 'canteen', active: true }).subscribe(ser => {
      this.services = ser.data;
      this.serviceChanged(this.services.length > 0 ? this.services[0] : null);
    });

    this.selectedWeek = new Date();
    this.getDateOfISOWeek(getISOWeek(this.selectedWeek), this.selectedWeek.getFullYear());
  }


  getDateOfISOWeek(w, y) {
    this.week = [];
    let ISOweekStart = moment().week(w).year(y);
    ISOweekStart = ISOweekStart.startOf('week');
    ISOweekStart = ISOweekStart.subtract(1, 'day');
    for (let i = 0; i < 7; i++) {
      this.week.push(
        new Date(ISOweekStart.toDate())
      );
      ISOweekStart.add(1,'day');
      
    }
    return ISOweekStart;
  }

  getMenuOfDay(day: Date, type: Meal) {

    if(this.menus) {
      return this.menus.find(m => moment(m.date).isSame(day,'day') && m.meal === type);
    } else {
      return null;
    }
  }


  updateMenuIfCan(data: MenuModel) {
      const menu = this.menus.find(m => moment(m.date).isSame(data.date, 'day') && m.meal === data.meal &&  m.service_id === data.service_id);
      
      if(menu) {
        const indexOf = this.menus.indexOf(menu);
        this.menus[indexOf] = data;
      } else {
        this.menus = [... this.menus, data];
      }
  }

  weekChanged(date: Date) {
    this.loading = true;
    this.selectedWeek = date;
    if(date) {
      let iSOWeek = getISOWeek(date);
      if(moment(date).weekday() == 6) { //if it's sunday
        iSOWeek++;
      }
      this.getDateOfISOWeek(iSOWeek, date.getFullYear());
    }
    
    this.loadMenus();
  }

  loadMenus() {
    if(this.selectedService) {
      this.menusService.list(1, 14,null, null,
        null, this.week[0],this.week[6], this.selectedService.id).subscribe(val => {
        this.menus = val.data;
        this.loading = false;
      });
    }else {
      this.loading = false;
    }
   
  }

  serviceChanged(value: ServiceModel) {
    this.selectedService = value;
    this.loadMenus();
  }

  copyDay(date: Date) {
    this.modalService.create({
      nzTitle: 'Copiar dia',
      nzContent: CopyMenuDayComponent,
      nzComponentParams: {
        sourceDate: date,
        sourceServiceId: this.selectedService.id,
      },
      nzClosable: false,
      nzCancelText: 'Limpar',
      nzFooter: [
        {
          label: 'Limpar',
          onClick: componentInstance => {
            componentInstance.reset();
          }
        },
        {
          label: 'Cancelar',
          type: 'danger',
          onClick: componentInstance => {
             componentInstance.close();
          }
        },
        {
          label: 'Copiar',
          type: 'primary',
          onClick: componentInstance => {
            return componentInstance.submit().then(data => {
              for (const  m of data) {
                 this.updateMenuIfCan(m);
              }
            });
          }
        }
      ]
    });
  }

  copyWeek() {
    this.modalService.create({
      nzTitle: 'Copiar semana',
      nzContent: CopyMenuWeekComponent,
      nzComponentParams: {
        sourceWeek: this.selectedWeek,
        sourceServiceId: this.selectedService.id,
      },
      nzClosable: false,
      nzCancelText: 'Limpar',
      nzFooter: [
        {
          label: 'Limpar',
          onClick: componentInstance => {
            componentInstance.reset();
          }
        },
        {
          label: 'Cancelar',
          type: 'danger',
          onClick: componentInstance => {
             componentInstance.close();
          }
        },
        {
          label: 'Copiar',
          type: 'primary',
          onClick: componentInstance => {
            return componentInstance.submit().then(data => {
              for (const  m of data) {
                 this.updateMenuIfCan(m);
              }
            });
          }
        }
      ]
    });
  }
}
