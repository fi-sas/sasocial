import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkMenusComponent } from './bulk-menus/bulk-menus.component';
import { ListMenusComponent } from './list-menus/list-menus.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListMenusComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:menus:read'},

  },
  {
    path: 'bulk',
    component: BulkMenusComponent,
    data: { breadcrumb: 'Criar várias', title: 'Criar várias', scope: 'alimentation:menus:create'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenusRoutingModule { }
