import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListMenusComponent } from './list-menus/list-menus.component';
import { BulkMenusComponent } from './bulk-menus/bulk-menus.component';
import { MenusRoutingModule } from './menus-routing.module';
import { ShowMenuComponent } from '../../components/show-menu/show-menu.component';
import { FormMenuComponent } from '../../components/form-menu/form-menu.component';
import { CopyMenuComponent } from '../../components/copy-menu/copy-menu.component';
import { CopyMenuDayComponent } from '../../components/copy-menu-day/copy-menu-day.component';
import { CopyMenuWeekComponent } from '../../components/copy-menu-week/copy-menu-week.component';


@NgModule({
  declarations: [
    ListMenusComponent,
    BulkMenusComponent,    
    ShowMenuComponent,
    FormMenuComponent,
    CopyMenuComponent,
    CopyMenuDayComponent,
    CopyMenuWeekComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MenusRoutingModule
  ],
  entryComponents: [
    FormMenuComponent,
    CopyMenuComponent,
    CopyMenuDayComponent,
    CopyMenuWeekComponent
  ]
})
export class MenusModule { }
