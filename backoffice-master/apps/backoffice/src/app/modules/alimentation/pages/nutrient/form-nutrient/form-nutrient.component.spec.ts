import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNutrientComponent } from './form-nutrient.component';

describe('FormNutrientComponent', () => {
  let component: FormNutrientComponent;
  let fixture: ComponentFixture<FormNutrientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNutrientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNutrientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
