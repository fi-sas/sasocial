import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NutrientModel } from '@fi-sas/backoffice/modules/alimentation/models/nutrient.model';
import { NutrientsService } from '@fi-sas/backoffice/modules/alimentation/services/nutrients.service';
import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { UnitsService } from '../../../services/units.service';

@Component({
  selector: 'fi-sas-form-nutrient',
  templateUrl: './form-nutrient.component.html',
  styleUrls: ['./form-nutrient.component.less']
})
export class FormNutrientComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent; 

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  errorTrans = false;
  id: number;
  submitted: boolean = false;

  units: UnitModel[];
  nutrientForm = new FormGroup({
    ddr: new FormControl(null, [Validators.required]),
    translations: new FormArray([]),
    unit_id: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required])
  });

  translations = this.nutrientForm.get('translations') as FormArray;

  constructor(
    public nutrientsService: NutrientsService,
    public unitsService: UnitsService,
    private route: ActivatedRoute,
    private router: Router,
    private languagesService: LanguagesService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.loadUnits();
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDataNutrientById(this.id);
      this.languages_loading = true;
    } else {
      this.loadLanguages('');
    }
  }


  get f() { return this.nutrientForm.controls; }


  getDataNutrientById(id: number) {
      let nutrient: NutrientModel = new NutrientModel();
      this.nutrientsService.getNutrientById(id).pipe(
        first()
      ).subscribe(results => {
        nutrient = results.data[0];
        this.nutrientForm.patchValue({
            translations: nutrient.translations ? nutrient.translations : null,
            ddr: nutrient.ddr ? nutrient.ddr : null,
            unit_id: nutrient.unit_id ? nutrient.unit_id : null,
            active: nutrient.active ? nutrient.active : false,
        });
        this.listOfSelectedLanguages = [];
        this.loadLanguages(nutrient);
      });
  }

  submitNutrient(edit: boolean) {
    this.submitted = true;
    let sendValues: NutrientModel = new NutrientModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if(this.nutrientForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.nutrientForm.valid && !this.errorTrans) {
      this.loading = true;
      sendValues = this.nutrientForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.nutrientsService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Nutriente alterada com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.nutrientsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Nutriente registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/nutrient/list');
  }

  loadUnits() {
    this.unitsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.units = results.data;
    });
  }

  loadLanguages(nutrient: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (nutrient) {
          this.startTranslation(nutrient);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.nutrientForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.nutrientForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
