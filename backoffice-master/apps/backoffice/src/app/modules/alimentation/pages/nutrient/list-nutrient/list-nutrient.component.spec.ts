import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNutrientComponent } from './list-nutrient.component';

describe('ListNutrientComponent', () => {
  let component: ListNutrientComponent;
  let fixture: ComponentFixture<ListNutrientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListNutrientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNutrientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
