import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NutrientsService } from '@fi-sas/backoffice/modules/alimentation/services/nutrients.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { UnitModel } from '../../../models/unit.model';
import { UnitsService } from '../../../services/units.service';
import { NutrientModel } from '../../../models/nutrient.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';


@Component({
  selector: 'fi-sas-list-nutrient',
  templateUrl: './list-nutrient.component.html',
  styleUrls: ['./list-nutrient.component.less']
})
export class ListNutrientComponent extends TableHelper implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;
  languages: LanguageModel[] = [];
  units: UnitModel[];
  status = [];
  constructor(
    public nutrientsService: NutrientsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public unitsService: UnitsService,
    private languageService: LanguagesService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.loadUnits();
    this.initTableData(this.nutrientsService);
    this.persistentFilters = {
      searchFields: 'name',
    };
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
      ).subscribe(result => {
      this.languages = result.data;
    });
  }

  loadUnits() {
    this.unitsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.units = results.data;
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
}

  editNutrient(id: number) {
    if(!this.authService.hasPermission('alimentation:nutrients:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/nutrient/update/' + id);
  }


  desactive(data: NutrientModel) {
    if(!this.authService.hasPermission('alimentation:nutrients:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Nutriente. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: NutrientModel) {
    data.active = false;
    this.nutrientsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Nutriente desativada com sucesso'
        );
        this.initTableData(this.nutrientsService);
      });

  }

  active(data: NutrientModel) {
    if(!this.authService.hasPermission('alimentation:nutrients:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Nutriente. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: NutrientModel) {
    data.active = true;
    this.nutrientsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Nutriente ativado com sucesso'
        );
        this.initTableData(this.nutrientsService);
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
