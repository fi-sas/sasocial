import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormNutrientComponent } from './form-nutrient/form-nutrient.component';
import { ListNutrientComponent } from './list-nutrient/list-nutrient.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListNutrientComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:nutrients:read'},

  },
  {
    path: 'create',
    component: FormNutrientComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:nutrients:create'},
  },
  {
    path: 'update/:id',
    component: FormNutrientComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:nutrients:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NutrientRoutingModule { }
