import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NutrientRoutingModule } from './nutrient-routing.module';
import { ListNutrientComponent } from './list-nutrient/list-nutrient.component';
import { FormNutrientComponent } from './form-nutrient/form-nutrient.component';

@NgModule({
  declarations: [
    ListNutrientComponent,
    FormNutrientComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NutrientRoutingModule
  ],

})
export class NutrientModule { }
