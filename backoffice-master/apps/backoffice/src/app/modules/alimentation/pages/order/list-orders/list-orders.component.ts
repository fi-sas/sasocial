import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { NzModalService } from "ng-zorro-antd";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { ServiceModel } from '../../../models/service.model';
import { ServicesService } from '../../../services/services.service';
import { OrdersService } from '../../../services/orders.service';
import { first } from 'rxjs/operators';
import * as moment  from 'moment';
@Component({
  selector: 'fi-sas-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.less']
})
export class ListOrdersComponent extends TableHelper implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;

  services: ServiceModel[] = [];
  userModel: UserModel;
  filter_date = null;
  currentDate = moment().startOf('day').toDate();

  constructor(
    private servicesServices: ServicesService,
    private ordersServices: OrdersService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    private modalService: NzModalService
  ) { 
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'user,order_lines,user_served'
    }
  }

  ngOnInit() {
    this.loadServices();
    this.getUserLogged();
    const dayCurrent = moment(this.currentDate).format('YYYY-MM-DD');
    const day2 = moment(this.currentDate.setDate(this.currentDate.getDate() - 2)).format('YYYY-MM-DD');
    this.filter_date = [day2,dayCurrent];
    this.filters['created_at'] = {};
    this.filters['created_at']['gte'] = moment(this.filter_date[0]).format('YYYY-MM-DD');
    this.filters['created_at']['lte'] = moment(this.filter_date[1]).format('YYYY-MM-DD');
    this.initTableData(this.ordersServices);
  }

  loadServices(){
    this.servicesServices.list(1, -1, null, null, { type: 'bar'}).subscribe(services => {
      this.services = services.data;
    });

  }

 
  listComplete() {
    this.filters.service_id = null;
    this.filters.status = null;
    this.filters.user_id = null;
    this.filter_date = null;
    this.filters.created_at = null;
    this.searchData(true)
  }

  updateFilters() {
    if (this.filter_date) {
        if(this.filter_date.length>0) {
            const temp = this.filter_date;
            this.filters['created_at'] = {};
            this.filters['created_at']['gte'] = moment(temp[0]).format('YYYY-MM-DD');
            this.filters['created_at']['lte'] = moment(temp[1]).format('YYYY-MM-DD');
        }else{
            this.filters['created_at'] = {};
            this.filter_date = null;
        }
        
    }
    this.searchData(true);
}


  getUserLogged() {
    this.authService.getUserObservable().subscribe(user => {
        this.userModel = user;
    });
    this.userModel = this.authService.getUser();
}

  markUnserved(id: number) {
    if(!this.authService.hasPermission('alimentation:orders:status')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai marcar este pedido como não servido. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markUnservedSubmit(id)
    });

  }

  markUnservedSubmit(id: number) {
    
    this.ordersServices
      .unServed(id)
      .pipe(first())
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Pedido marcado como não servido com sucesso'
        );
        this.initTableData(this.ordersServices);
      });

  }

  markServed(id: number) {
    if(!this.authService.hasPermission('alimentation:orders:status')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai marcar este pedido como servido. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markServedSubmit(id)
    });
  }

  markServedSubmit(id: number) {
    this.ordersServices.served(id, this.userModel.id)
    .pipe(first())
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Pedido marcado como servido com sucesso'
        );
        this.initTableData(this.ordersServices);
      });
  }


  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

}
