import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { OrdersReportsComponent } from './orders-reports/orders-reports.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListOrdersComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:orders:read'},
  },
  {
    path: 'reports',
    component: OrdersReportsComponent,
    data: { breadcrumb: 'Relatórios', title: 'Relatórios', scope: 'alimentation:orders:read'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
