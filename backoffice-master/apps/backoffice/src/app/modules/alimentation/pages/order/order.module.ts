import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { OrderRoutingModule } from './order-routing.module';
import { OrdersReportsComponent } from './orders-reports/orders-reports.component';

@NgModule({
  declarations: [
    ListOrdersComponent,
    OrdersReportsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    OrderRoutingModule
  ],

})
export class OrderModule { }
