import { Component, OnInit } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { ServiceModel } from '../../../models/service.model';
import { OrdersService } from '../../../services/orders.service';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'fi-sas-orders-reports',
  templateUrl: './orders-reports.component.html',
  styleUrls: ['./orders-reports.component.less']
})
export class OrdersReportsComponent implements OnInit {

  loading = false;
  selectedDate = [new Date(), new Date()];

  servicesLoading = false;
  selectedService = [];
  services : ServiceModel[] = [];

  selectedService1 = [];
  selectedDate1 = [new Date(), new Date()];

  constructor(
    private ordersService: OrdersService,
    private servicesService: ServicesService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.loadServices();
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService.list(0, -1, null, null, {
      type: 'bar',
      withRelated: false
    }).pipe(first(), finalize(() => this.servicesLoading = false)).subscribe((res) => {
      this.services = res.data;
    });
  }

  ordersProductsRevenue() {
    this.loading = true;
    this.ordersService.generateProductRevenueReport({
      start_date: this.selectedDate1[0],
      end_date: this.selectedDate1[1],
      service_id: this.selectedService1,
    }).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
      this.uiService.showMessage(MessageType.success, "Relatório criado com sucesso.")
    });
  }

  ordersRevenue() {
    this.loading = true;
    this.ordersService.generateRevenueReport({
      start_date: this.selectedDate[0],
      end_date: this.selectedDate[1],
      service_id: this.selectedService,
    }).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
      this.uiService.showMessage(MessageType.success, "Relatório criado com sucesso.")
    });
  }
}
