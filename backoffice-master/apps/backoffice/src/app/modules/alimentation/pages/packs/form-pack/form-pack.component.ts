import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import * as _ from 'lodash';
import * as moment from 'moment';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import { PackModel, PackPeriodEnum } from '../models/pack.model';
import { PacksService } from '../services/packs.service';

@Component({
  selector: 'fi-sas-form-pack',
  templateUrl: './form-pack.component.html',
  styleUrls: ['./form-pack.component.less']
})
export class FormPackComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  PackPeriodEnum = PackPeriodEnum;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;

  id: number;
  submitted: boolean = false;
  errorTrans = false;
  packForm = new FormGroup({
    translations: new FormArray([]),
    active: new FormControl(true, [Validators.required]),
    sell_to_public: new FormControl(false, [Validators.required]),
    period: new FormControl('week', [Validators.required]),
    number_lunchs: new FormControl(0, [Validators.required]),
    number_dinners: new FormControl(0, [Validators.required]),
    blocking_lunch_at: new FormControl(new Date(0,0,0,0,0), [Validators.required]),
    blocking_dinner_at: new FormControl(new Date(0,0,0,0,0), [Validators.required]),
    price: new FormControl(0, [Validators.required]),
    vat_id: new FormControl(null, [Validators.required]),
  });


  translations = this.packForm.get('translations') as FormArray;

  taxes: TaxModel[];

  constructor(
    public packsService: PacksService,
    private taxesService: TaxesService,
    private route: ActivatedRoute,
    private languagesService: LanguagesService,
    private router: Router,
    private uiService: UiService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDataPackById(this.id);
    } else {
      this.loadLanguages('');
    }

    this.loadTaxes();
  }

  loadTaxes() {
    this.taxesService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
      first()
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  getDataPackById(id: number){
    let pack: PackModel = new PackModel();
    this.packsService.read(id).pipe(
      first()
    ).subscribe(results => {
      pack = results.data[0];
      this.packForm.patchValue({
        //translations
        price: pack.price,
        vat_id: pack.vat_id,
        active: pack.active,
        sell_to_public: pack.sell_to_public,
        period: pack.period,
        number_lunchs: pack.number_lunchs,
        number_dinners: pack.number_dinners,
        blocking_lunch_at: moment(pack.blocking_lunch_at, 'HH:mm:ssZZ').toDate(),
        blocking_dinner_at: moment(pack.blocking_dinner_at, 'HH:mm:ssZZ').toDate(),
      });
      this.listOfSelectedLanguages = [];
      this.loadLanguages(pack);
    });
  }

  loadLanguages(pack: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (pack) {
          this.startTranslation(pack);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  submitAllergen(edit: boolean) {
    this.submitted = true;
    let sendValues: PackModel = new PackModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if(this.packForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.packForm.valid && !this.errorTrans) {
      this.loading = true;
      sendValues = this.packForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.packsService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Pack alterado com sucesso'
            );
            this.backList();
          });

      } else {
        this.packsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Pack registado com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/packs/list');
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.packForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.packForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required,trimValidation]),
        description: new FormControl(description, [Validators.required,trimValidation]),
      })
    );
  }

  get f() { return this.packForm.controls; }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name,
          translation.description
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
