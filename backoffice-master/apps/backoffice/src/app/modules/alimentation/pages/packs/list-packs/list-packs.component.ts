import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { NzModalService } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { PacksService } from '../services/packs.service';

@Component({
  selector: 'fi-sas-list-packs',
  templateUrl: './list-packs.component.html',
  styleUrls: ['./list-packs.component.less']
})
export class ListPacksComponent extends TableHelper implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;
  weekPeriodResults = {
    week: { label: 'Semana', color: 'gray' },
    month: { label: 'Mês', color: 'gray' },
  }

  languages: LanguageModel[] = [];
  status = [];


  constructor(
    public packsService: PacksService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private languageService: LanguagesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'translations,vat',
      searchFields: 'name,description'
    }
  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.initTableData(this.packsService);
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  editDish(id) {
    if (!this.authService.hasPermission('alimentation:packs:update')) {
      return;
    }
    this.router.navigateByUrl('/alimentation/packs/update/' + id);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }


}
