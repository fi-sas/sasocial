import { Component, Input, OnInit } from '@angular/core';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { PackModel } from '../../models/pack.model';

@Component({
  selector: 'fi-sas-view-pack',
  templateUrl: './view-pack.component.html',
  styleUrls: ['./view-pack.component.less']
})
export class ViewPackComponent implements OnInit {

  weekPeriodResults = {
    week: { label: 'Semana', color: 'gray' },
    month: { label: 'Mês', color: 'gray' },
  }

  @Input() data: PackModel = null;

  @Input() languages: LanguageModel[] = [];

  constructor(

  ) {

  }

  ngOnInit() {
  }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }

}
