import { TaxModel } from "@fi-sas/backoffice/modules/configurations/models/tax.model";

export enum PackPeriodEnum {
    WEEK = "week",
    MONTH = "month"
}

export class PackTranslationModel {
    language_id: number;
    name: string;
    description: string;
  }


export class PackModel {
    id: number;
    translations?: PackTranslationModel[];
    price: number;
    vat_id: number;
    vat?: TaxModel;
    sell_to_public: boolean;
    active: boolean;
    period: PackPeriodEnum;
    number_lunchs: number
    number_dinners: number
    blocking_lunch_at: Date;
    blocking_dinner_at: Date;
    fentity_id: number;
    created_at: Date;
    updated_at: Date;
}
