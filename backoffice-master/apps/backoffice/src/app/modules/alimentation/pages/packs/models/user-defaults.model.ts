export class FoodUserDefaultModel {
    user_id: number;
    canteen_lunch_id: number;
    dish_type_lunch_id: number;
    canteen_dinner_id: number;
    dish_type_dinner_id: number;
}