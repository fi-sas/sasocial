import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { PackModel } from "./pack.model";
import { UserMealModel } from "./user-meal.model";

export class UserPackModel {
    id: number;
    accommodation_application_id: number;
    movement_id: string;
    movement_item_id: string;
    user_id: number;
    pack_id: number;
    begin_at: Date;
    end_at: Date;
    number_lunchs: number;
    number_dinners: number;
    user?: UserModel;
    pack?: PackModel;
    user_meals?: UserMealModel[];
}
