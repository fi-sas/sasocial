import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPackComponent } from './form-pack/form-pack.component';
import { ListPacksComponent } from './list-packs/list-packs.component';
import { UserDefaultsComponent } from './user-defaults/user-defaults.component';
import { UserPacksComponent } from './user-packs/user-packs.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'users',
    component: UserPacksComponent,
    data: { breadcrumb: 'Packs', title: 'Packs', scope: 'alimentation:packs:read'},

  },
  {
    path: 'list',
    component: ListPacksComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:packs:read'},

  },
  {
    path: 'create',
    component: FormPackComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:packs:create'},

  },
  {
    path: 'update/:id',
    component: FormPackComponent,
    data: { breadcrumb: 'Alterar', title: 'Alterar', scope: 'alimentation:packs:update'},

  },
  {
    path: 'user_defaults',
    component: UserDefaultsComponent,
    data: { breadcrumb: 'Configs. utilizadores', title: 'Configurações utilizadores', scope: 'alimentation:packs:update'},

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacksRoutingModule { }
