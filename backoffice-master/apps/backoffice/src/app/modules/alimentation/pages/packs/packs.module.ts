import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacksRoutingModule } from './packs-routing.module';
import { ListPacksComponent } from './list-packs/list-packs.component';
import { FormPackComponent } from './form-pack/form-pack.component';
import { UserPacksComponent } from './user-packs/user-packs.component';
import { ViewPackComponent } from './list-packs/view-pack/view-pack.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AddUserPackComponent } from './user-packs/add-user-pack/add-user-pack.component';
import { UserPackViewComponent } from './user-packs/user-pack-view/user-pack-view.component';
import { UserDefaultsComponent } from './user-defaults/user-defaults.component';

@NgModule({
  declarations: [ListPacksComponent, FormPackComponent, UserPacksComponent, ViewPackComponent, AddUserPackComponent, UserPackViewComponent, UserDefaultsComponent],
  imports: [
    CommonModule,
    SharedModule,
    PacksRoutingModule
  ],
  entryComponents: [
    AddUserPackComponent
  ]
})
export class PacksModule { }
