import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { FiscalEntityModel } from '../../../models/fiscal-entity.model';
import { AlimentationService } from '../../../services/alimentation.service';
import { FoodUserDefaultModel } from '../models/user-defaults.model';

@Injectable({
  providedIn: 'root'
})
export class UserDefaultsService  extends Repository<FoodUserDefaultModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.USER_DEFAULTS';
    this.entity_url = 'FOOD.USER_DEFAULTS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    }); 
  }

  updateDefaultsUser(
    user_id: number,
    params: {
      canteen_lunch_id: number,
      dish_type_lunch_id: number,
      canteen_dinner_id: number,
      dish_type_dinner_id: number,
    }
  ): Observable<Resource<any>> {
    return this.resourceService
      .update<any>(
        this.urlService.get('FOOD.USER_DEFAULTS_ID_UPDATE', { user_id, ...this.persistentUrlParams }),
        params,
        {
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

  
}
