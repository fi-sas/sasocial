import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';

import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { FiscalEntityModel } from '../../../models/fiscal-entity.model';
import { AlimentationService } from '../../../services/alimentation.service';
import { UserMealModel } from '../models/user-meal.model';
import { UserPackModel } from '../models/user-pack.model';

@Injectable({
  providedIn: 'root'
})
export class UserPacksService extends Repository<UserPackModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.USERS_PACKS';
    this.entity_url = 'FOOD.USERS_PACKS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }


  listUserMeals(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    params?: {},
    extraParams?: {},
  ): Observable<Resource<UserMealModel>> {
    return this.resourceService
      .list<UserMealModel>(
        this.urlService.get('FOOD.USERS_MEALS', { ...this.persistentUrlParams }),
        {
          params: this.getQuery(
            pageIndex,
            pageSize,
            sortKey,
            sortValue,
            params,
            extraParams,
          ),
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

}
