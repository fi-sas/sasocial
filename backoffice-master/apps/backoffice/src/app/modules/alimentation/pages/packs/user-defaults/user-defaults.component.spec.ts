import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDefaultsComponent } from './user-defaults.component';

describe('UserDefaultsComponent', () => {
  let component: UserDefaultsComponent;
  let fixture: ComponentFixture<UserDefaultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDefaultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDefaultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
