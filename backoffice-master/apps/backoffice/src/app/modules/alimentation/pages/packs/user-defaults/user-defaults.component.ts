import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { DishTypesService } from '../../../services/dish-types.service';
import { ServicesService } from '../../../services/services.service';
import { UserDefaultsService } from '../services/user-defaults.service';

@Component({
  selector: 'fi-sas-user-defaults',
  templateUrl: './user-defaults.component.html',
  styleUrls: ['./user-defaults.component.less']
})
export class UserDefaultsComponent extends TableHelper implements OnInit {

  isModalVisible = false;
  dishTypeList = [];
  services = [];
  
  selectedUserId = null;
  selectedDishTypeLunch = null;
  selectedServiceLunch = null;
  selectedDishTypeDinner = null;
  selectedServiceDinner = null;

  constructor(
    public userDefaultsService: UserDefaultsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private dishTypesService: DishTypesService,
    private servicesService: ServicesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'user,lunch_canteen,dinner_canteen,lunch_dish_type,dinner_dish_type',
      searchFields: 'name,description'
    }
  }
  
  ngOnInit() {
    this.initTableData(this.userDefaultsService);
    this.loadDishTypes();
    this.loadServices();
  }

  loadDishTypes() {
    this.dishTypesService.list(1, -1, null, null, {
      withRelated: 'translations',
      pack_available: true,
      active: true,
    }).pipe(first()).subscribe((result) => {
      this.dishTypeList = result.data;
    });
  }

  loadServices() {
    this.servicesService.list(1, -1, null, null, {
      withRelated: 'false',
      type: 'canteen',
      active: true,
    }).pipe(first()).subscribe((result) => {
      this.services = result.data;
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  editConfigs(data: any){
    if(!this.authService.hasPermission('alimentation:packs:update')){
      return;
    }
   
    this.selectedUserId = data.user_id;
    this.selectedDishTypeLunch = data.dish_type_lunch_id;
    this.selectedServiceLunch = data.canteen_lunch_id;
    this.selectedDishTypeDinner = data.dish_type_dinner_id;
    this.selectedServiceDinner = data.canteen_dinner_id;

    this.isModalVisible = true;
  }

  handleModalCancel() {
    this.isModalVisible = false;
    this.selectedUserId = null;
    this.selectedDishTypeLunch = null;
    this.selectedServiceLunch = null;
    this.selectedDishTypeDinner = null;
    this.selectedServiceDinner = null;
  }

  handleModalOk() {
    this.isModalVisible = false;

    this.userDefaultsService.updateDefaultsUser(this.selectedUserId, {
      canteen_dinner_id: this.selectedServiceDinner,
      canteen_lunch_id: this.selectedServiceLunch,
      dish_type_dinner_id: this.selectedDishTypeDinner,
      dish_type_lunch_id: this.selectedDishTypeLunch,
    }).pipe(first()).subscribe((result) => {
      this.handleModalCancel();
      this.searchData();
    })
  }


}
