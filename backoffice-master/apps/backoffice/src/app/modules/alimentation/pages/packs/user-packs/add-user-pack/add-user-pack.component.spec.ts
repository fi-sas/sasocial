import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserPackComponent } from './add-user-pack.component';

describe('AddUserPackComponent', () => {
  let component: AddUserPackComponent;
  let fixture: ComponentFixture<AddUserPackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserPackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
