import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DishTypesService } from '@fi-sas/backoffice/modules/alimentation/services/dish-types.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { UserPacksService } from '@fi-sas/backoffice/modules/alimentation/services/user-packs.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { PackModel } from '../../models/pack.model';
import { PacksService } from '../../services/packs.service';
import { UserDefaultsService } from '../../services/user-defaults.service';

@Component({
  selector: 'fi-sas-add-user-pack',
  templateUrl: './add-user-pack.component.html',
  styleUrls: ['./add-user-pack.component.less']
})
export class AddUserPackComponent implements OnInit {

  public loading = false;

  packs: PackModel[] = [];
  packs_loading = false;

  dishTypeList = [];
  services = [];
  
  selectedUserId = null;
  selectedDishTypeLunch = null;
  selectedServiceLunch = null;
  selectedDishTypeDinner = null;
  selectedServiceDinner = null;

  submitted = false;

  public userPackForm = new FormGroup({
    user_id: new FormControl(null, [Validators.required]),
    pack_id: new FormControl(null, [Validators.required]),
    begin_at: new FormControl(null, [Validators.required]),

    canteen_lunch_id: new FormControl(null, [Validators.required]),
    dish_type_lunch_id: new FormControl(null, [Validators.required]),
    canteen_dinner_id: new FormControl(null, [Validators.required]),
    dish_type_dinner_id: new FormControl(null, [Validators.required]),

    end_at: new FormControl(null, []),
  });

  constructor(
    private userPacksService: UserPacksService,
    private userDefaultsService: UserDefaultsService,
    private packsService: PacksService,
    private dishTypesService: DishTypesService,
    private servicesService: ServicesService,
    private nzModalRef: NzModalRef,
  ) { }

  ngOnInit() {
    this.loadPacks();
    this.loadDishTypes();
    this.loadServices();
  }

  loadDishTypes() {
    this.dishTypesService.list(1, -1, null, null, {
      withRelated: 'translations',
      pack_available: true,
      active: true,
    }).pipe(first()).subscribe((result) => {
      this.dishTypeList = result.data;
    });
  }

  loadServices() {
    this.servicesService.list(1, -1, null, null, {
      withRelated: 'false',
      type: 'canteen',
      active: true,
    }).pipe(first()).subscribe((result) => {
      this.services = result.data;
    });
  }

  loadPacks() {
    this.packs_loading = true;

    this.packsService.list(1, -1, null ,null, {
      withRelated: 'translations',
      active: true
    }).pipe(
      first(),
      finalize(() => this.packs_loading = false)
    ).subscribe(result => this.packs =  result.data);
  }

  userChanged(user_id: number)  {
    this.userDefaultsService.list(1,1,null,null, {
      user_id,
    }).pipe(first()).subscribe((result) => {
      if(result.data.length > 0) {
        this.userPackForm.patchValue({
          canteen_lunch_id: result.data[0].canteen_lunch_id,
          dish_type_lunch_id: result.data[0].dish_type_lunch_id,
          canteen_dinner_id: result.data[0].canteen_dinner_id,
          dish_type_dinner_id: result.data[0].dish_type_dinner_id,
        });
      } else {
        this.userPackForm.patchValue({
          canteen_lunch_id: null,
          dish_type_lunch_id: null,
          canteen_dinner_id: null,
          dish_type_dinner_id: null,
        });
      }
      
    });
  }


  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  getPackPeriod() {
    if(this.userPackForm.get('pack_id').value) {
      return this.packs.find(p => p.id === this.userPackForm.get('pack_id').value).period;
    }

    return null;
  }

  close() {
    this.nzModalRef.close();
  }

  submit(): Promise<boolean> {
    this.submitted = true;
    if(!this.userPackForm.valid) {
      
      (<any>Object).values(this.userPackForm.controls).forEach(control => {
        control.markAsTouched();
      });


      return Promise.resolve(false);
    }
    return new Promise<boolean>((resolve) => {
      this.loading = true;
      this.userPacksService.bulk(this.userPackForm.value)
      .pipe(
        first(),
        finalize(() => this.loading = false))
        .subscribe(res => {
          resolve(true);
        }, (err) => {
          resolve(false);
        });
    });
  }
}
