import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPackViewComponent } from './user-pack-view.component';

describe('UserPackViewComponent', () => {
  let component: UserPackViewComponent;
  let fixture: ComponentFixture<UserPackViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPackViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPackViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
