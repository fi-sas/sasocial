import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { finalize, first } from 'rxjs/operators';
import { UserPackModel } from '../../models/user-pack.model';
import { UserPacksService } from '../../services/users-packs.service';

@Component({
  selector: 'fi-sas-user-pack-view',
  templateUrl: './user-pack-view.component.html',
  styleUrls: ['./user-pack-view.component.less']
})
export class UserPackViewComponent implements OnInit, OnChanges {

  @Input() data: UserPackModel = null;
  user_meals = [];
  public YesNoTag = TagComponent.YesNoTag;
  loading = false;

  constructor(
    private packsService: UserPacksService
  ) {

  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.user_meals = [];
    if (this.data.user_meals) {
      this.user_meals = this.data.user_meals;
      this.user_meals.map(um => um.date = new Date(um.date));
      this.data.user_meals.sort((a, b) => {
        return a.date.getTime() - b.date.getTime()
      });
    }
  }

  validPackTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validPackTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }

  loadMeals(id: number) {
    this.loading = true;

    this.packsService.listUserMeals(1, -1, 'date', 'asc', {
      user_pack_id: id
    }).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(res => {
      this.user_meals = res.data;
    });
  }
}
