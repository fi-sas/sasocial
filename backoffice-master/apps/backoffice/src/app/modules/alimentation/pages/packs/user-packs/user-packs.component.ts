import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { UserPacksService } from '../../../services/user-packs.service';
import { AddUserPackComponent } from './add-user-pack/add-user-pack.component';

@Component({
  selector: 'fi-sas-user-packs',
  templateUrl: './user-packs.component.html',
  styleUrls: ['./user-packs.component.less']
})
export class UserPacksComponent extends TableHelper implements OnInit {

  constructor(
    private userPacksService: UserPacksService,
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private nzModaService: NzModalService,
  ) {
    super(uiService, router, activatedRoute);
    //this.pageSize = 10;
    this.persistentFilters = {
      withRelated: 'pack,user'
    }
  }

  ngOnInit() {
    this.initTableData(this.userPacksService);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  addUserPack() {
    this.nzModaService.create({
      nzContent: AddUserPackComponent,
      nzWidth: '60%',
      nzOnOk: (instance) => {
        return instance.submit();
      },
      nzOnCancel: (instance) => {
        instance.close();
      }
    });
  }

  desactivateUserPack(id: number) {
    this.userPacksService.desactivate(id).pipe(first()).subscribe(() => {
      this.searchData();
    });
  }

  activateUserPack(id: number) {
    this.userPacksService.activate(id).pipe(first()).subscribe(() => {
      this.searchData();
    });
  }
}
