import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  ProductModel,
  TrackStock,
} from '@fi-sas/backoffice/modules/alimentation/models/product.model';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { ServiceType } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import * as moment from 'moment';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { UnitModel } from '../../../models/unit.model';
import { UnitsService } from '../../../services/units.service';
import { DisponibilityModel } from '../../../models/disponibility.model';
import { FamilyModel } from '../../../models/family.model';
import { FamiliesService } from '../../../services/families.service';
import { XAxisComponent } from '@swimlane/ngx-charts';

@Component({
  selector: 'fi-sas-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.less'],
})
export class FormProductComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  ServiceType = ServiceType;
  TrackStock = TrackStock;
  errorTrans = false;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;

  id: number = null;
  duplicate_id: number = null;

  submitted: boolean = false;

  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();
  listServices;
  listTracking;
  filterTypes = ['image/png', 'image/jpeg'];
  productForm = new FormGroup({
    code: new FormControl('', [Validators.required, trimValidation]),
    translations: new FormArray([]),
    unit_id: new FormControl(null, [Validators.required]),
    price: new FormControl('', [Validators.required]),
    tax_price_id: new FormControl('', [Validators.required]),
    cost_price: new FormControl('', [Validators.required]),
    tax_cost_price_id: new FormControl('', [Validators.required]),
    minimal_stock: new FormControl(0, [Validators.required]),
    track_stock: new FormControl(null, [Validators.required]),
    type: new FormControl('', [Validators.required]),
    families: new FormControl([], []),
    show_derivatives: new FormControl(false, [Validators.required]),
    begin_date: new FormControl(''),
    end_date: new FormControl(''),
    annulment_maximum_hour: new FormControl(null),
    minimum_hour: new FormControl(null),
    maximum_hour: new FormControl(null),
    file_id: new FormControl(''),
    monday: new FormControl(true, [Validators.required]),
    tuesday: new FormControl(true, [Validators.required]),
    wednesday: new FormControl(true, [Validators.required]),
    thursday: new FormControl(true, [Validators.required]),
    friday: new FormControl(true, [Validators.required]),
    saturday: new FormControl(true, [Validators.required]),
    sunday: new FormControl(true, [Validators.required]),
    available: new FormControl(true, [Validators.required]),
    visible: new FormControl(true, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
  });

  translations = this.productForm.get('translations') as FormArray;

  units: UnitModel[];
  taxes: TaxModel[];
  families: FamilyModel[];

  stock_tracker;

  constructor(
    public productsService: ProductsService,
    private unitsService: UnitsService,
    private taxesService: TaxesService,
    private familiesService: FamiliesService,
    private route: ActivatedRoute,
    private router: Router,
    private languagesService: LanguagesService,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.listServices = [
      { value: ServiceType.BAR, label: 'Bar' },
      { value: ServiceType.CANTEEN, label: 'Cantina' },
      { value: ServiceType.MAINTENANCE, label: 'Manutenção' },
    ];
    this.listTracking = [
      { value: TrackStock.COMPOSITION, label: 'Composição' },
      { value: TrackStock.NONE, label: 'Nenhum' },
      { value: TrackStock.PRODUCT, label: 'Produto' },
      { value: TrackStock.PRODUCT_COMPOSITION, label: 'Produto / Composição' },
    ];
    this.loadUnits();
    this.loadTaxes();
    this.loadFamilies();

    this.route.params.subscribe((params) => {
      this.id = params['id'] || null;
    });

    this.route.queryParams.subscribe((params) => {
      this.duplicate_id = params['duplicate_id'] || null;
    });

    if (this.id || this.duplicate_id) {
      this.getDataProductById(this.id || this.duplicate_id);
      this.languages_loading = true;
    } else {
      this.loadLanguages('');
    }
  }
  get f() {
    return this.productForm.controls;
  }

  getDataProductById(id: number) {
    this.loading = true;
    let product: ProductModel = new ProductModel();
    this.productsService
      .read(id, { withRelated: 'translations,disponibility,families' })
      .pipe(first())
      .subscribe((results) => {
        product = results.data[0];
        let min_hours;
        let max_hours;
        let max_annul;
        if (product.disponibility.minimum_hour) {
          min_hours = product.disponibility.minimum_hour;
          min_hours =
            min_hours == '23:00:00+00'
              ? moment(
                  product.disponibility.minimum_hour,
                  'HH:mm:ssZ'
                ).subtract(1, 'day')
              : moment(product.disponibility.minimum_hour, 'HH:mm:ssZ');
        }
        if (product.disponibility.maximum_hour) {
          max_hours = product.disponibility.maximum_hour;
          max_hours =
            max_hours == '23:00:00+00'
              ? moment(
                  product.disponibility.maximum_hour,
                  'HH:mm:ssZ'
                ).subtract(1, 'day')
              : moment(product.disponibility.maximum_hour, 'HH:mm:ssZ');
        }
        if (product.disponibility.annulment_maximum_hour) {
          max_annul = product.disponibility.annulment_maximum_hour;
          max_annul =
            max_annul == '23:00:00+00'
              ? moment(
                  product.disponibility.annulment_maximum_hour,
                  'HH:mm:ssZ'
                ).subtract(1, 'day')
              : moment(
                  product.disponibility.annulment_maximum_hour,
                  'HH:mm:ssZ'
                );
        }

        this.productForm.patchValue({
          translations: product.translations ? product.translations : null,
          code: product.code && !this.duplicate_id ? product.code : null,
          unit_id: product.unit_id ? product.unit_id : null,
          price: product.price ? product.price : null,
          tax_price_id: product.tax_price_id ? product.tax_price_id : null,
          cost_price: product.cost_price ? product.cost_price : null,
          tax_cost_price_id: product.tax_cost_price_id
            ? product.tax_cost_price_id
            : null,
          minimal_stock: product.minimal_stock ? product.minimal_stock : 0,
          track_stock: product.track_stock ? product.track_stock : null,
          type: product.type ? product.type : null,
          families: product.families ? product.families.map((f) => f.id) : null,
          show_derivatives: product.show_derivatives
            ? product.show_derivatives
            : false,
          begin_date: product.disponibility.begin_date
            ? product.disponibility.begin_date
            : null,
          end_date: product.disponibility.end_date
            ? product.disponibility.end_date
            : null,
          annulment_maximum_hour: product.disponibility.annulment_maximum_hour
            ? max_annul.toDate()
            : null,
          minimum_hour: product.disponibility.minimum_hour
            ? min_hours.toDate()
            : null,
          maximum_hour: product.disponibility.maximum_hour
            ? max_hours.toDate()
            : null,
          file_id:
            product.file_id && !this.duplicate_id ? product.file_id : null,
          monday: product.disponibility.monday,
          tuesday: product.disponibility.tuesday,
          wednesday: product.disponibility.wednesday,
          thursday: product.disponibility.thursday,
          friday: product.disponibility.friday,
          saturday: product.disponibility.saturday,
          sunday: product.disponibility.sunday,

          available: product.available,
          visible: product.visible,
          active: product.active,
        });
        this.listOfSelectedLanguages = [];
        this.loadLanguages(product);
        this.loading = false;
        this.languages_loading = false;
      });
  }

  loadUnits() {
    this.unitsService
      .list(1, -1, null, null, { active: true })
      .pipe(first())
      .subscribe((results) => {
        this.units = results.data;
      });
  }

  loadTaxes() {
    this.taxesService
      .list(1, -1, null, null, { active: true, sort: 'name' })
      .pipe(first())
      .subscribe((results) => {
        this.taxes = results.data;
      });
  }

  loadFamilies() {
    this.familiesService
      .list(1, -1, null, null, { withRelated: 'translations' })
      .pipe(first())
      .subscribe((results) => {
        this.families = results.data;
      });
  }

  submitProduct(edit: boolean) {
    this.submitted = true;
    let sendValues: ProductModel = new ProductModel();
    let disponibility: DisponibilityModel = new DisponibilityModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.productForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (
      this.productForm.valid &&
      !this.errorTrans &&
      !this.validTimeMax() &&
      !this.validTimeMin() &&
      !this.validTime()
    ) {
      this.loading = true;
      sendValues = this.productForm.value;
      if (!sendValues.price) {
        sendValues.price = null;
      }
      if (!sendValues.tax_price_id) {
        sendValues.tax_price_id = null;
      }
      if (this.f.begin_date.value) {
        disponibility.begin_date = this.f.begin_date.value;
      }
      if (this.f.end_date.value) {
        disponibility.end_date = this.f.end_date.value;
      }
      if (this.f.annulment_maximum_hour.value) {
        disponibility.annulment_maximum_hour = moment(
          this.f.annulment_maximum_hour.value
        ).toDate();
      }
      if (this.f.minimum_hour.value) {
        disponibility.minimum_hour = moment(this.f.minimum_hour.value).toDate();
      }
      if (this.f.maximum_hour.value) {
        disponibility.maximum_hour = moment(this.f.maximum_hour.value).toDate();
      }
      disponibility.monday = this.f.monday.value;
      disponibility.tuesday = this.f.tuesday.value;
      disponibility.thursday = this.f.thursday.value;
      disponibility.wednesday = this.f.wednesday.value;
      disponibility.friday = this.f.friday.value;
      disponibility.saturday = this.f.saturday.value;
      disponibility.sunday = this.f.sunday.value;
      sendValues.disponibility = disponibility;
      if (this.f.file_id.value === null || this.f.file_id.value === '') {
        sendValues.file_id = null;
      }

      if (edit) {
        sendValues.id = this.id;
        this.productsService
          .update(this.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Produto alterada com sucesso'
            );
            this.backList();
          });
      } else {
        this.productsService
          .create(sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Produto registada com sucesso'
            );

            // IF IS DUPLICATE FINISH DUPLICATING THE REST OF RESOURCES
            if (this.duplicate_id) {
              this.duplicateResources(result.data[0].id);
              this.backList();
            } else {
              this.backList();
            }
          });
      }
    }
  }

  changeTypeProduct(type) {
    if (type == 'bar') {
      this.productForm.controls.price.setValidators([Validators.required]);
      this.productForm.controls.tax_price_id.setValidators([
        Validators.required,
      ]);
    } else {
      this.productForm.controls.price.clearValidators();
      this.productForm.controls.tax_price_id.clearValidators();
    }
    this.productForm.controls.price.updateValueAndValidity();
    this.productForm.controls.tax_price_id.updateValueAndValidity();
  }

  backList() {
    this.router.navigateByUrl('/alimentation/product/list');
  }

  loadLanguages(nutrient: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (nutrient) {
          this.startTranslation(nutrient);
        } else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.productForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, description?: string) {
    const translations = this.productForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
        description: new FormControl(description, [
          Validators.required,
          trimValidation,
        ]),
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name,
          translation.description
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  disableStartDate = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return (
      moment(startDate).isBefore(currentDate) ||
      moment(this.productForm.get('end_date').value).isBefore(startDate)
    );
  };

  disableEndDate = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return (
      moment(endDate).isBefore(currentDate) ||
      moment(this.productForm.get('begin_date').value).isAfter(endDate)
    );
  };

  validTimeMin() {
    if (this.f.minimum_hour.value && this.f.maximum_hour.value) {
      return moment(this.f.minimum_hour.value).isAfter(
        this.f.maximum_hour.value
      );
    }
    return false;
  }

  validTimeMax() {
    if (this.f.minimum_hour.value && this.f.maximum_hour.value) {
      return moment(this.f.maximum_hour.value).isBefore(
        this.f.minimum_hour.value
      );
    }
    return false;
  }

  validTime() {
    if (this.f.minimum_hour.value && this.f.annulment_maximum_hour.value) {
      return moment(this.f.annulment_maximum_hour.value).isBefore(
        this.f.minimum_hour.value
      );
    }
    return false;
  }

  /**
   * Duplicate the rest of the resources of the product
   */
  duplicateResources(new_product_id: number) {
    this.productsService
      .getAllergens(this.duplicate_id)
      .pipe(first())
      .subscribe((result) => {
        console.log('getAllergens');
        console.log(result.data.map((a) => a.id));
        this.productsService
          .updateAllergens(
            new_product_id,
            result.data.map((a) => a.id)
          )
          .pipe(first())
          .subscribe((res) => {});
      });

    this.productsService
      .getNutrients(this.duplicate_id)
      .pipe(first())
      .subscribe((result) => {
        console.log('getNutrients');
        console.log(
          result.data.map((n) => ({
            quantity: n.quantity,
            nutrient_id: n.nutrient.id,
          }))
        );
        this.productsService
          .updateNutrients(
            new_product_id,
            result.data.map((n) => ({
              quantity: n.quantity,
              nutrient_id: n.nutrient.id,
            }))
          )
          .pipe(first())
          .subscribe((res) => {});
      });

    this.productsService
      .getComplements(this.duplicate_id)
      .pipe(first())
      .subscribe((result) => {
        console.log('getComplements');
        console.log(result.data.map((a) => a.id));
        this.productsService
          .updateComplements(
            new_product_id,
            result.data.map((a) => a.id)
          )
          .pipe(first())
          .subscribe((res) => {});
      });

    this.productsService
      .getComposition(this.duplicate_id)
      .pipe(first())
      .subscribe((result) => {
        console.log('getComposition');
        console.log(
          result.data.map((c) => ({
            compound_id: c.compound.id,
            quantity: c.quantity,
          }))
        );
        this.productsService
          .updateComposition(
            new_product_id,
            result.data.map((c) => ({
              compound_id: c.compound.id,
              quantity: c.quantity,
            }))
          )
          .pipe(first())
          .subscribe((res) => {});
      });

    this.productsService
      .getPrices(this.duplicate_id)
      .pipe(first())
      .subscribe((result) => {
        console.log('getPrices');
        console.log(
          result.data.map((p) => ({
            description: p.description,
            tax_id: p.tax_id,
            profile_id: p.profile_id,
            price: p.price,
            meal: p.meal,
            time: p.time,
          }))
        );
        this.productsService
          .updatePrices(
            new_product_id,
            result.data.map((p) => ({
              description: p.description,
              tax_id: p.tax_id,
              profile_id: p.profile_id,
              price: p.price,
              meal: p.meal,
              time: p.time,
            }))
          )
          .pipe(first())
          .subscribe((res) => {});
      });
  }
}
