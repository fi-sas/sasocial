import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import * as moment from 'moment';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { AllergensService } from '../../../services/allergens.service';
import { ComplementsService } from '../../../services/complements.service';
import { NutrientsService } from '../../../services/nutrients.service';
import { ProductModel } from '../../../models/product.model';
import { ProductPriceModel } from '../../../models/product-prices.model';
import { NutrientModel } from '../../../models/nutrient.model';
import { AllergenModel } from '../../../models/allergen.model';
import { ComplementModel } from '../../../models/complement.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.less']
})
export class ListProductsComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];
  typeProduct = '';
  public YesNoTag = TagComponent.YesNoTag;
  time1;

  status = [];

  accessModalPrices = false;
  loading_prices =  false;


  constructor(
    private languageService: LanguagesService,
    public productsService: ProductsService,
    private profilesService: ProfilesService,
    public allergensService: AllergensService,
    public complementsService: ComplementsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private taxesService: TaxesService,
    public nutrientsService: NutrientsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'translations,disponibility',
      searchFields: 'name,description'
    }

  }

  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.initTableData(this.productsService);
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  listComplete(){
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }


  desactive(data: ProductModel) {
    if(!this.authService.hasPermission('alimentation:products:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Produto. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: ProductModel) {
    data.active = false;
    this.productsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Produto desativado com sucesso'
        );
        this.initTableData(this.productsService);
      });
  }

  active(data: ProductModel) {
    if(!this.authService.hasPermission('alimentation:products:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Produto. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: ProductModel) {
    data.active = true;
    this.productsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Produto ativado com sucesso'
        );
        this.initTableData(this.productsService);
      });
  }

  // Prices
  product_id : number;
  productPrices: ProductPriceModel[]
  taxes: TaxModel[];
  profiles : ProfileModel[];
  mealList;
  submmitedPrice = false;
  defaultOpenValue = new Date(0, 0, 0, 0, 0, 0);

  productPricesForm = new FormGroup({
    price: new FormControl('', [Validators.required, Validators.min(1)]),
    description: new FormControl('', [Validators.required, trimValidation]),
    tax_id : new FormControl('', [Validators.required]),
    meal : new FormControl('', [Validators.required]),
    time : new FormControl('', [Validators.required]),
    profile_id: new FormControl('', [Validators.required]),
  });

  get f() { return this.productPricesForm.controls; }

  toggleAccessPrices(product_id, type: string) {
    if(!this.authService.hasPermission('alimentation:products:read')){
      return;
    }
    this.typeProduct = type;
    if(this.typeProduct == 'canteen') {
      this.productPricesForm.controls.meal.setValidators([Validators.required]);
    }else {
      this.productPricesForm.controls.meal.clearValidators();
    }
    this.productPricesForm.controls.meal.updateValueAndValidity();
    this.mealList = [
      { label : "Jantar", value: "dinner"},
      { label : "Almoço", value: "lunch"}
    ]
    this.loadTaxes();
    this.loadProfiles();
    this.accessModalPrices = !this.accessModalPrices;
    if(this.accessModalPrices === true) {
      if(product_id !== null){
        this.product_id = product_id;
        this.updateListPrices();
      }
    }
  }

  updateListPrices() {
    this.productPrices = [];
    this.loading_prices = true;
    this.productsService.getProductPrices(this.product_id).subscribe(prices => {
      this.productPrices = prices.data;
      this.loading_prices = false;
    });
  }

  loadTaxes() {
    this.taxesService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.taxes = results.data;
    });
  }

  loadProfiles() {
    this.profilesService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.profiles = results.data;
    });
  }

  addPrices() {
    this.submmitedPrice = true;
    const findData = this.productPrices.find((data) => data.profile_id === this.f.profile_id.value && data.meal === this.f.meal.value && data.time === moment(this.f.time.value).format("HH:mm:ss"));
    if(findData) {
      this.uiService.showMessage(MessageType.error, 'Já existe essa informação na lista de preços');
      return;
    }
    if(!this.productPricesForm.invalid){
      this.loading_prices = true;
      let price = new ProductPriceModel();
      price.active = true;
      price.description = this.f.description.value,
      price.meal = this.f.meal.value,
      price.product_id = this.product_id;
      price.time = moment(this.f.time.value).format("HH:mm:ss"); 
      price.tax_id = this.f.tax_id.value;
      price.profile_id = this.f.profile_id.value;
      price.price =  this.f.price.value;
      this.productPrices.push(price)
      this.productsService.updatePrices(this.product_id, this.productPrices).subscribe(res => {
        this.updateListPrices();
        this.loading_prices = false;
        this.submmitedPrice = false;
        this.uiService.showMessage(MessageType.success, 'Preço adicionado com sucesso');
        this.productPricesForm.reset();
      });
    }
  }

  removePrices(price) {
    const index = this.productPrices.indexOf(price);
    if (index > -1) {
      this.productPrices.splice(index, 1);
    }
    this.productsService.updatePrices(this.product_id, this.productPrices).subscribe(res => {
      this.updateListPrices();
      this.uiService.showMessage(MessageType.success, 'Preço removido com sucesso');
    });
  }

  // Nutrients
  productNutrientForm = new FormGroup({
    quantity: new FormControl('', [Validators.required, Validators.min(0.01)]),
    nutrient_id: new FormControl('', [Validators.required]),
  });

  get ff() { return this.productNutrientForm.controls; }

  accessModalNutrients = false;
  listNutrients : NutrientModel[];
  loading_nutrients = false;
  productNutrients;
  submmitedNutrient = false;

  toggleAccesNutrients(product_id) {
    if(!this.authService.hasPermission('alimentation:nutrients:read')){
      return;
    }
    this.loadNutrients();
    this.accessModalNutrients = !this.accessModalNutrients;
    if(this.accessModalNutrients === true) {
      if(product_id !== null){
        this.product_id = product_id;
        this.updateListNutrients();
      }
    }
  }

  updateListNutrients(){
    this.productNutrients = [];
    this.loading_nutrients = true;
    this.productsService.getNutrients(this.product_id).subscribe(nutrients => {
      this.productNutrients = nutrients.data;
      this.loading_nutrients = false;
    });
  }

  loadNutrients(){
    this.nutrientsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.listNutrients = results.data;
    });
  }

  addNutrient() {
    let sendList = [];
    this.submmitedNutrient = true;
    for(let nutri of this.productNutrients){
      const nt = {
        quantity: nutri.quantity,
        nutrient_id:  nutri.nutrient.id
      }
      sendList.push(nt);
    }
    const findNutrient = sendList.find((nutrient) => nutrient.nutrient_id === this.ff.nutrient_id.value);
    if(findNutrient) {
      this.uiService.showMessage(MessageType.error, 'Nutriente já presente no produto');
      return;
    }
    if(!this.productNutrientForm.invalid){
      this.loading_nutrients = true;
      const nt = {
        quantity: this.ff.quantity.value ,
        nutrient_id:  this.ff.nutrient_id.value
      }
      sendList.push(nt);
      this.productsService.updateNutrients(this.product_id, sendList).subscribe(res => {
        this.updateListNutrients();
        this.loading_nutrients = false;
        this.submmitedNutrient = false;
        this.uiService.showMessage(MessageType.success, 'Nutriente adicionado com sucesso');
        this.productNutrientForm.reset();
      });
    }
  }

  removeNutrient(nutrient){
    this.loading_nutrients = true;
    const index = this.productNutrients.indexOf(nutrient);
    if (index > -1) {
      this.productNutrients.splice(index, 1);
    }
    let sendList = [];
    for(let nutri of this.productNutrients){
      const nt = {
        quantity: nutri.quantity,
        nutrient_id:  nutri.nutrient.id
      }
      sendList.push(nt);
    }
    this.productsService.updateNutrients(this.product_id, sendList).subscribe(res => {
      this.updateListNutrients();
      this.uiService.showMessage(MessageType.success, 'Nutriente registado com sucesso');
    });
  }


  //Allergens

  productAllergenForm = new FormGroup({
    allergen_id: new FormControl('', [Validators.required]),
  });

  get fa() { return this.productAllergenForm.controls; }

  accessModalAllergens = false;
  listAllergens : AllergenModel[];
  loading_allergens = false;
  productAllergens;
  submmitedAllergen = false;

  toggleAccesAllergens(product_id) {
    if(!this.authService.hasPermission('alimentation:allergens:read')){
      return;
    }
    this.loadAllergens();
    this.accessModalAllergens = !this.accessModalAllergens;
    if(this.accessModalAllergens === true) {
      if(product_id !== null){
        this.product_id = product_id;
        this.updateListAllergens();
      }
    }
  }

  updateListAllergens(){
    this.productAllergens = [];
    this.loading_allergens = true;
    this.productsService.getAllergens(this.product_id).subscribe(nutrients => {
      this.productAllergens = nutrients.data;
      this.loading_allergens = false;
    });
  }

  loadAllergens(){
    this.allergensService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.listAllergens = results.data;
    });

  }

  addAllergens() {
    this.submmitedAllergen = true;
    let sendList = [];
    for(let allergen of this.productAllergens){
      sendList.push(allergen.id);
    }
    const findAllergen = sendList.find((allergen) => allergen === this.fa.allergen_id.value);
    if(findAllergen) {
      this.uiService.showMessage(MessageType.error, 'Alergénio já registado no produto');
      return;
    }
    if(!this.productAllergenForm.invalid){
      this.loading_allergens = true;
      sendList.push(this.fa.allergen_id.value);
      this.productsService.updateAllergens(this.product_id, sendList).subscribe(res => {
        this.updateListAllergens();
        this.loading_allergens = false;
        this.submmitedAllergen = false;
        this.uiService.showMessage(MessageType.success, 'Alergenio adicionado com sucesso');
        this.productAllergenForm.reset();
      });
    }
  }

  removeAllergen(nutrient){
    this.loading_allergens = true;
    const index = this.productAllergens.indexOf(nutrient);
    if (index > -1) {
      this.productAllergens.splice(index, 1);
    }
    let sendList = [];
    for(let allergen of this.productAllergens){
      sendList.push(allergen.id);
    }
    this.productsService.updateAllergens(this.product_id, sendList).subscribe(res => {
      this.updateListAllergens();
      this.uiService.showMessage(MessageType.success, 'Alergenio removido com sucesso');
    });
  }

  //Composição

  productCompositionForm = new FormGroup({
    compound_id: new FormControl('', [Validators.required]),
    quantity: new FormControl('', [Validators.required,Validators.min(0.001)]),
  });

  get fc() { return this.productCompositionForm.controls; }

  accessModalCompositions = false;
  isListCompositionsLoading = false;
  listCompositions : ProductModel[];
  searchProductsChange$ = new BehaviorSubject('');

  loading_compositions = false;
  
  productCompositions;
  submmitedComposition = false;

  toggleAccesCompositions(product_id) {
    if(!this.authService.hasPermission('alimentation:products:read')){
      return;
    }
    this.loadProducts();
    this.accessModalCompositions = !this.accessModalCompositions;
    if(this.accessModalCompositions === true) {
      if(product_id !== null){
        this.product_id = product_id;
        this.updateListCompositions();
      }
    }
  }

  loadProducts(){
    /*this.productsService.list(1, 10, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.listCompositions = results.data;
    });*/

    const productsList$: Observable<ProductModel[]> = this.searchProductsChange$.asObservable()
      .pipe(debounceTime(500)).pipe(
        switchMap(event => {
          this.isListCompositionsLoading = true;
          return this.productsService.list(1, 10, null, null, { active: true, withRelated: 'translations,unit', searchFields: 'description,name', search: event}).pipe(
            first()
          );
        })).pipe(map(res => res.data));
      
        productsList$.subscribe(data => {
          this.listCompositions = data;
          this.isListCompositionsLoading = false;
    });

  }
  
  onProductSearch(value: string): void {
    this.searchProductsChange$.next(value);
  }

  updateListCompositions() {
    this.productCompositions = [];
    this.loading_compositions = true;
    this.productsService.getComposition(this.product_id).subscribe(composition => {
      this.productCompositions = composition.data;
      this.loading_compositions = false;
    });
  }

  addComposition() {
    this.submmitedComposition = true;
    let sendList = [];
    for(let compound of this.productCompositions){
      const cmp = {
        compound_id : compound.compound.id,
        quantity: compound.quantity
      }
      sendList.push(cmp);
    }
    const findCompount = sendList.find((compound) => compound.compound_id === this.fc.compound_id.value);
    if(findCompount) {
      this.uiService.showMessage(MessageType.error, 'Composição já registado no produto');
      return;
    }
    if(this.fc.compound_id.value === this.product_id){
      this.uiService.showMessage(MessageType.error, 'Um produto não pode ser composto por ele mesmo');
      return;
    }
    if(!this.productCompositionForm.invalid){
      this.loading_compositions = true;
      const new_cmp = {
        compound_id : this.fc.compound_id.value,
        quantity: this.fc.quantity.value
      }
      sendList.push(new_cmp);
      this.productsService.updateComposition(this.product_id, sendList).subscribe(res => {
        this.updateListCompositions();
        this.loading_compositions = false;
        this.submmitedComposition = false;
        this.uiService.showMessage(MessageType.success, 'Composição adicionado com sucesso');
        this.productCompositionForm.reset();
      });
    }
  }

  removeComposition(composition){
    this.loading_compositions = true;
    const index = this.productCompositions.indexOf(composition);
    if (index > -1) {
      this.productCompositions.splice(index, 1);
    }
    let sendList = [];
    for(let compound of this.productCompositions){
      const nt = {
        quantity: compound.quantity,
        compound_id:  compound.compound.id
      }
      sendList.push(nt);
    }
    this.productsService.updateComposition(this.product_id, sendList).subscribe(res => {
      this.updateListCompositions();
      this.uiService.showMessage(MessageType.success, 'Composição removida com sucesso');
    });
  }

  //Complements

  productComplementForm = new FormGroup({
    complement_id: new FormControl('', [Validators.required]),
  });

  get fcc() { return this.productComplementForm.controls; }

  accessModalComplements = false;
  listComplements : ComplementModel[];
  loading_complements = false;
  productComplements;
  submmitedComplement = false;

  toggleAccesComplements(product_id) {
    if(!this.authService.hasPermission('alimentation:complements:update')){
      return;
    }
    this.loadComplements();
    this.accessModalComplements = !this.accessModalComplements;
    if(this.accessModalComplements === true) {
      if(product_id !== null){
        this.product_id = product_id;
        this.updateListComplements();
      }
    }
  }

  loadComplements(){
    this.complementsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.listComplements = results.data;
    });
  }

  updateListComplements(){
    this.productComplements = [];
    this.loading_complements = true;
    this.productsService.getComplements(this.product_id).subscribe(complement => {
      this.productComplements = complement.data;
      this.loading_complements = false;
    });
  }

  addComplements() {
    this.submmitedComplement = true;
    let sendList = [];
    for(let complement of this.productComplements){
      sendList.push(complement.id);
    }
    const findComplement = sendList.find((complement) => complement === this.fcc.complement_id.value);
    if(findComplement) {
      this.uiService.showMessage(MessageType.error, 'Complemento já registado no produto');
      return;
    }
    if(!this.productComplementForm.invalid){
      this.loading_complements = true;
      sendList.push(this.fcc.complement_id.value);
      this.productsService.updateComplements(this.product_id, sendList).subscribe(res => {
        this.updateListComplements();
        this.loading_complements = false;
        this.submmitedComplement = false;
        this.uiService.showMessage(MessageType.success, 'Complemento adicionado com sucesso');
        this.productComplementForm.reset();
      });
    }
  }

  removeComplement(nutrient){
    this.loading_complements = true;
    const index = this.productComplements.indexOf(nutrient);
    if (index > -1) {
      this.productComplements.splice(index, 1);
    }
    let sendList = [];
    for(let complement of this.productComplements){
      sendList.push(complement.id);
    }
    this.productsService.updateComplements(this.product_id, sendList).subscribe(res => {
      this.updateListComplements();
      this.uiService.showMessage(MessageType.success, 'Complemento removido com sucesso');
    });
  }

  editProduct(id: number){
    if(!this.authService.hasPermission('alimentation:products:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/product/update/' + id);
  }

  duplicateProduct(id: number){
    if(!this.authService.hasPermission('alimentation:products:create')){
      return;
    }
    this.router.navigate(['alimentation', 'product', 'create'], {
      queryParams: {
        duplicate_id: id,
      }
    });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
  
  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }


}
