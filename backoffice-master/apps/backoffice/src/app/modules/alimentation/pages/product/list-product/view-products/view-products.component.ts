import { Component, Input, OnInit } from '@angular/core';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { ProductModel } from '../../../../models/product.model';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.less']
})
export class ViewProductsComponent implements OnInit {

  @Input() data: ProductModel = null;
  @Input() languages: LanguageModel[] = [];
  maximum_hour;
  minimum_hour;
  annulment_maximum_hour;
  constructor(
    
  ) {
    
  }
  ngOnInit() {
    if(this.data.disponibility.maximum_hour) {
      this.maximum_hour = moment(this.data.disponibility.maximum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility.minimum_hour) {
      this.minimum_hour = moment(this.data.disponibility.minimum_hour, "HH:mm:ssZ").toDate();
    }
    if(this.data.disponibility.annulment_maximum_hour) {
      this.annulment_maximum_hour = moment(this.data.disponibility.annulment_maximum_hour, "HH:mm:ssZ").toDate();
    }
   }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

}
