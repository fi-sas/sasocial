import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormProductComponent } from './form-product/form-product.component';
import { ListProductsComponent } from './list-product/list-products.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListProductsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:products:read'},

  },
  {
    path: 'create',
    component: FormProductComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:products:create'},
  },
  {
    path: 'update/:id',
    component: FormProductComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:products:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
