import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewProductsComponent } from './list-product/view-products/view-products.component';
import { ListProductsComponent } from './list-product/list-products.component';
import { FormProductComponent } from './form-product/form-product.component';
import { ProductsRoutingModule } from './product-routing.module';

@NgModule({
  declarations: [
    ListProductsComponent,
    ViewProductsComponent,
    FormProductComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProductsRoutingModule
  ],

})
export class ProductModule { }
