import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from '@fi-sas/backoffice/modules/alimentation/services/products.service';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { ProductModel } from '../../../models/product.model';
import { ProductsRecipeModel, RecipeModel } from '../../../models/recipe.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-form-recipe',
  templateUrl: './form-recipe.component.html',
  styleUrls: [
    './form-recipe.component.less'
  ]
})
export class FormRecipeComponent implements OnInit {
  filterTypes = ['image/png', 'image/jpeg'];
  recipeForm = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    description: new FormControl('', [Validators.required, trimValidation]),
    number_doses: new FormControl('', [Validators.required, Validators.min(0)]),
    preparation: new FormControl(null, trimValidation),
    preparation_mode: new FormControl(null, trimValidation),
    microbiological_criteria: new FormControl(null, trimValidation),
    packaging: new FormControl('', trimValidation),
    use: new FormControl(null, trimValidation),
    distribution: new FormControl(null, trimValidation),
    comment: new FormControl(null, trimValidation),
    associated_docs: new FormControl(null, trimValidation),
    product: new FormControl(null),
    quantity_liquid: new FormControl('', [Validators.min(0)]),
    quantity_gross: new FormControl('', [Validators.min(0)]),
    steps: new FormArray([]),
    active: new FormControl(true, [Validators.required]),
  });

  loading = false;
  loading_info = false;

  id: number = null;
  duplicate_id: number = null;
  submitted: boolean = false;

  products: ProductModel[] = [];
  listReciceProducts = [];

  steps = this.recipeForm.get('steps') as FormArray;
  isLoadingSelects = false;
  searchChange$ = new BehaviorSubject('');

  constructor(
    public recipesService: RecipesService,
    private productsService: ProductsService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }



  ngOnInit() {
   
    this.route.params.subscribe(params => {
      this.id = params['id'] || null;
    });

    this.route.queryParams.subscribe(params => {
      this.duplicate_id = params['duplicate_id'] || null;
    });

    if (this.id || this.duplicate_id) {
      this.loading_info = true;
      this.loading = true;
      this.getDataRecipeById(this.id || this.duplicate_id);
    }else {
      this.loadProducts();
    }
  }


  loadProducts() {
    const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
      .pipe(debounceTime(500)).pipe(
        switchMap(event => {
          this.isLoadingSelects = true;
          return this.productsService.list(1, 10, null, null,
            {
              withRelated: "translations,unit",
              search: event,
              active: true,
              searchFields: 'description,name'
            }
          )
        }
        )).pipe(map(res => res.data));

    productList$.subscribe(data => {
      this.products = data;
      this.isLoadingSelects = false;
    });
  }

  getDataRecipeById(id: number) {
    this.loading = true;
    this.loading_info = true;
    let recipe;
    this.recipesService
      .read(id, { withRelated: "steps,products" })
      .pipe(
        first()
      )
      .subscribe((results) => {
        recipe = results.data[0];
        this.recipeForm.patchValue({
          name: recipe.name ? recipe.name : null,
          description: recipe.description ? recipe.description : null,
          number_doses: recipe.number_doses ? recipe.number_doses : null,
          preparation: recipe.preparation ? recipe.preparation : null,
          preparation_mode: recipe.preparation_mode ? recipe.preparation_mode : null,
          microbiological_criteria: recipe.microbiological_criteria ? recipe.microbiological_criteria : null,
          packaging: recipe.packaging ? recipe.packaging : null,
          use: recipe.use ? recipe.use : null,
          distribution: recipe.distribution ? recipe.description : null,
          comment: recipe.comment ? recipe.comment : null,
          associated_docs: recipe.associated_docs ? recipe.associated_docs : null,
          active: recipe.active ? recipe.active : false,
        });
      
        this.startSteps(recipe);
        if (recipe.products.length !== 0) {
          for (const prod of recipe.products) {
            const product = {
              product: prod,
              quantity_liquid: prod.recipes_quantitys.liquid_quantity,
              quantity_gross: prod.recipes_quantitys.gross_quantity,
            };
            this.listReciceProducts.push(product);
            this.productsService.filterById(recipe.products.map(u => u.id)).subscribe(prod => { this.products = prod.data; });
           
          }
        }
        this.loadProducts();
        this.loading = false;
        this.loading_info = false;
      });
  }

  get f() { return this.recipeForm.controls; }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }


  submitRecipe(edit: boolean) {
    this.submitted = true;
    let sendValues: RecipeModel = new RecipeModel();
    if (this.recipeForm.valid) {
      sendValues.description = this.f.description.value;
      sendValues.distribution = this.f.distribution.value;
      sendValues.microbiological_criteria = this.f.microbiological_criteria.value;
      sendValues.name = this.f.name.value;
      sendValues.number_doses = this.f.number_doses.value;
      sendValues.packaging = this.f.packaging.value;
      sendValues.use = this.f.use.value;
      sendValues.active = this.f.active.value;
      sendValues.associated_docs = this.f.associated_docs.value;
      sendValues.comment = this.f.comment.value;
      sendValues.preparation = this.f.preparation.value;
      sendValues.preparation_mode = this.f.preparation_mode.value;
      let listProducts: ProductsRecipeModel[] = [];
      if (this.listReciceProducts.length === 0) {
        this.uiService.showMessage(
          MessageType.error,
          'Não existem produtos adicionados a receita'
        );
        return;
      }
      for (const product of this.listReciceProducts) {
        let productrecipe = new ProductsRecipeModel();
        productrecipe.gross_quantity = product.quantity_gross;
        productrecipe.liquid_quantity = product.quantity_liquid;
        productrecipe.ingredient_id = product.product.id;
        listProducts.push(productrecipe);
      }
      sendValues.products = listProducts;
      if (this.f.steps.value.length !== 0) {
        sendValues.preparation_steps = this.f.steps.value;
      }
      this.loading = true;
      if (edit) {
        sendValues.id = this.id;
        this.recipesService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Receita alterada com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.recipesService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Receita registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/recipe/list');
  }


  addProducts() {
    if (!this.f.product.value || !this.f.quantity_liquid.value || !this.f.quantity_gross.value) {
      this.uiService.showMessage(
        MessageType.error,
        'Deve preencher todos os campos referentes a produto'
      );
      return;
    }
    const product = {
      product: this.f.product.value,
      quantity_liquid: this.f.quantity_liquid.value,
      quantity_gross: this.f.quantity_gross.value,
    };

    let findProduct = this.listReciceProducts.find((elem) => elem.product.id === product.product.id)
    if (findProduct) {
      this.uiService.showMessage(
        MessageType.error,
        'Produto ja existente na lista'
      );
      return;
    }

    this.listReciceProducts.push(product);
    this.recipeForm.patchValue({
      product: null,
      quantity_gross: null,
      quantity_liquid: null
    });
  }

  removeProduct(product) {
    const index = this.listReciceProducts.indexOf(product);
    if (index > -1) {
      this.listReciceProducts.splice(index, 1);
    }
  }


  addSteps(file_id?: number, description?: string, step?: number) {
    const steps = this.recipeForm.controls.steps as FormArray;
    steps.push(
      new FormGroup({
        file_id: new FormControl(file_id, Validators.required),
        description: new FormControl(description, [Validators.required, trimValidation]),
        step: new FormControl(step, Validators.required)
      })
    );
  }

  startSteps(value) {
    if (value !== '') {
      value.steps.map((steps) => {
        this.addSteps(
          !this.duplicate_id ? steps.file_id : null,
          steps.description,
          steps.step,
        );
      });
    } else {
      this.addSteps(null, '', null);
    }
  }

  removeStep(i) {
    this.steps.removeAt(i);
  }

}
