import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipesService } from '@fi-sas/backoffice/modules/alimentation/services/recipes.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service'
import { NzModalService } from "ng-zorro-antd";
import { RecipeModel } from '../../../models/recipe.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-recipes',
  templateUrl: './list-recipes.component.html',
  styleUrls: ['./list-recipes.component.less']
})
export class ListRecipesComponent extends TableHelper implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;

  status = [];
  
  constructor(
    public recipesService: RecipesService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'description',
        label: 'Descrição',
        sortable: true,
      },
      {
        key: 'number_doses',
        label: 'Doses',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        sortable: true,
      }
    );
    this.initTableData(this.recipesService);
    this.persistentFilters = {
      searchFields: "name,description"
    }
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  editRecipe(id){
    if(!this.authService.hasPermission('alimentation:recipes:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/recipe/update/' + id);
  }

  duplicateRecipe(id){
    if(!this.authService.hasPermission('alimentation:recipes:create')){
      return;
    }
    this.router.navigate(['alimentation', 'recipe', 'create'], {
      queryParams: {
        duplicate_id: id,
      }
    });
  }

  desactive(data: RecipeModel) {
    if(!this.authService.hasPermission('alimentation:recipes:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta receita. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: RecipeModel) {
    this.recipesService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Receita desativada com sucesso'
        );
        this.initTableData(this.recipesService);
      });

  }

  active(data: RecipeModel) {
    if(!this.authService.hasPermission('alimentation:recipes:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta receita. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: RecipeModel) {
    this.recipesService.active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Receita ativado com sucesso'
        );
        this.initTableData(this.recipesService);
      });
  }

}
