import { Component, Input, OnInit } from '@angular/core';
import { RecipeModel } from '../../../../models/recipe.model';

@Component({
  selector: 'fi-sas-view-recipes',
  templateUrl: './view-recipes.component.html',
  styleUrls: ['./view-recipes.component.less']
})
export class ViewRecipesComponent implements OnInit {

  @Input() data: RecipeModel = null;
  loading = false;

  constructor() { }

  ngOnInit() {
  }

}
