import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormRecipeComponent } from './form-recipe/form-recipe.component';
import { ListRecipesComponent } from './list-recipe/list-recipes.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListRecipesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:recipes:read'},

  },
  {
    path: 'create',
    component: FormRecipeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:recipes:create'},
  },
  {
    path: 'update/:id',
    component: FormRecipeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:recipes:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule { }
