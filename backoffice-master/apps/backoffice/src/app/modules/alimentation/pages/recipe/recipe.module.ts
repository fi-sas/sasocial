import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListRecipesComponent } from './list-recipe/list-recipes.component';
import { ViewRecipesComponent } from './list-recipe/view-recipes/view-recipes.component';
import { FormRecipeComponent } from './form-recipe/form-recipe.component';
import { RecipeRoutingModule } from './recipe-routing.module';

@NgModule({
  declarations: [
    ListRecipesComponent,
    FormRecipeComponent,
    ViewRecipesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RecipeRoutingModule
  ],

})
export class RecipeModule { }
