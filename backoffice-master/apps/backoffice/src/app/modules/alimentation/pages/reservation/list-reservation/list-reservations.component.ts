import { Component, OnInit } from '@angular/core';
import * as moment  from 'moment';
import { ActivatedRoute, Router } from '@angular/router'
import { NzModalService } from "ng-zorro-antd";
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { ServiceModel } from '../../../models/service.model';
import { ServicesService } from '../../../services/services.service';
import { ReservationsService } from '../../../services/reservations.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-reservations',
  templateUrl: './list-reservations.component.html',
  styleUrls: ['./list-reservations.component.less']
})
export class ListReservationsComponent extends TableHelper implements OnInit {
  services: ServiceModel[] = [];
  selectedService = null;
  currentDate = moment().startOf('day').toDate();
  filter_date = null;
  res = [];
  stats = [];

  public YesNoTag = TagComponent.YesNoTag;
  
  constructor(
    private servicesServices: ServicesService,
    private reservationsService: ReservationsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService
    ) {
      super(uiService, router, activatedRoute);
      this.persistentFilters = {
        withRelated: 'user,menu_dish,served_by_user'
      }
  }

  ngOnInit() {
    this.loadServices();
    this.columns.push(  
      {
        key: 'date',
        label: 'Data',
        sortable: true
      },
      {
        key: 'name',
        label: 'Localização',
        sortable: false
      },
      {
        key: 'meal',
        label: 'Refeição',
        sortable: true
      },
      {
        key: 'type',
        label: 'Tipo',
        sortable: false
      },
      {
        key: 'dish',
        label: 'Prato',
        sortable: false
      },
      {
        key: 'is_available',
        label: 'Disponível',
        sortable: true
      },
      {
        key: 'has_canceled',
        label: 'Cancelada',
        sortable: true
      },
      {
        key: 'is_served',
        label: 'Servida',
        sortable: true
      },
      {
        key: 'is_from_pack',
        label: 'Pack',
        sortable: true
      },
      {
        key: 'served_at',
        label: 'Servida em',
        sortable: true
        
      },
      
    );
    const dayCurrent = moment(this.currentDate).format('YYYY-MM-DD');
    const day2 = moment(this.currentDate.setDate(this.currentDate.getDate() - 2)).format('YYYY-MM-DD');
    this.filter_date = [day2,dayCurrent];
    this.filters['date'] = {};
    this.filters['date']['gte'] = moment(this.filter_date[0]).format('YYYY-MM-DD');
    this.filters['date']['lte'] = moment(this.filter_date[1]).format('YYYY-MM-DD');
    this.initTableData(this.reservationsService);
  }


  loadServices(){
    this.servicesServices.list(1, -1, null, null, { type: 'canteen'}).subscribe(services => {
      this.services = services.data;
    });

  }

  listComplete() {
    this.filters.service_id = null;
    this.filters.date = null;
    this.filters.user_id = null;
    this.filters.date = null;
    this.filters['meal'] = null;
    this.filter_date = null;
    this.searchData(true)
  }

  updateFilters() {
    if (this.filter_date) {
        if(this.filter_date.length>0) {
            const temp = this.filter_date;
            this.filters['date'] = {};
            this.filters['date']['gte'] = moment(temp[0]).format('YYYY-MM-DD');
            this.filters['date']['lte'] = moment(temp[1]).format('YYYY-MM-DD');
        }else{
            this.filters['date'] = {};
            this.filter_date = null;
        }
        
    }
    this.searchData(true);
}


  markUnserved(id: number) {
    if(!this.authService.hasPermission('alimentation:reservations:status')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai marcar esta reserva como não servida. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markUnservedSubmit(id)
    });

  }

  markUnservedSubmit(id: number) {
    this.reservationsService
      .unServed(id)
      .pipe(first())
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Reserva marcada como não servida com sucesso'
        );
        this.initTableData(this.reservationsService);
      });

  }

  markServed(id: number) {
    if(!this.authService.hasPermission('alimentation:reservations:status')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai marcar esta reserva como servida. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markServedSubmit(id)
    });
  }

  cancelReservation(id: number) {
    if(!this.authService.hasPermission('alimentation:reservations:status')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai anular esta comprar, o dinheiro será devolvido ao utilizador. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.CancelReservationSubmit(id)
    });
  }

  markServedSubmit(id: number) {
    this.reservationsService.served(id)
    .pipe(first())
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Reserva marcada como servida com sucesso'
        );
        this.initTableData(this.reservationsService);
      });
  }

  CancelReservationSubmit(id: number) {
    this.reservationsService.cancel(id)
      .pipe(first())
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Reserva cancelada com com sucesso'
        );
        this.initTableData(this.reservationsService);
      });
  }

}
