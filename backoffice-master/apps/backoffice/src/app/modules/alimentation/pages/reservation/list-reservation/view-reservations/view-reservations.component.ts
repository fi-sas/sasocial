import { Component, Input, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';

@Component({
  selector: 'fi-sas-view-reservations',
  templateUrl: './view-reservations.component.html',
  styleUrls: ['./view-reservations.component.less']
})
export class ViewReservationsComponent implements OnInit {

  public YesNoTag = TagComponent.YesNoTag;
  
  @Input() data: any = null;

  constructor() { }

  ngOnInit() {
  }

}
