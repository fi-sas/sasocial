import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListReservationsComponent } from './list-reservation/list-reservations.component';
import { ReservationsReportsComponent } from './reservations-reports/reservations-reports.component';
import { StatsReservationsComponent } from './stats-reservations/stats-reservations.component';


const routes: Routes = [
  { path: '', redirectTo: 'counts', pathMatch: 'full' },
  {
    path: 'counts',
    component: StatsReservationsComponent,
    data: { breadcrumb: 'Contagens', title: 'Contagens', scope: 'alimentation:reservations:read'},

  },
  {
    path: 'list',
    component: ListReservationsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:reservations:read'},
  },
  {
    path: 'reports',
    component: ReservationsReportsComponent,
    data: { breadcrumb: 'Relatórios', title: 'Relatórios', scope: 'alimentation:reservations:read'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule { }
