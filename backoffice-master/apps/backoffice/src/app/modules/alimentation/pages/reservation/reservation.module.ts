import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListReservationsComponent } from './list-reservation/list-reservations.component';
import { ReservationRoutingModule } from './reservation-routing.module';
import { ViewReservationsComponent } from './list-reservation/view-reservations/view-reservations.component';
import { StatsReservationsComponent } from './stats-reservations/stats-reservations.component';
import { ReservationsReportsComponent } from './reservations-reports/reservations-reports.component';

@NgModule({
  declarations: [
    ListReservationsComponent,
    ViewReservationsComponent,
    StatsReservationsComponent,
    ReservationsReportsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReservationRoutingModule
  ],

})
export class ReservationModule { }
