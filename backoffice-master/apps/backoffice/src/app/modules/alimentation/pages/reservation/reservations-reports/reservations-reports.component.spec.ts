import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationsReportsComponent } from './reservations-reports.component';

describe('ReservationsReportsComponent', () => {
  let component: ReservationsReportsComponent;
  let fixture: ComponentFixture<ReservationsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
