import { Component, OnInit } from '@angular/core';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { DishTypeModel } from '../../../models/dish-type.model';
import { Meal } from '../../../models/menu.model';
import { ServiceModel } from '../../../models/service.model';
import { DishTypesService } from '../../../services/dish-types.service';
import { ReservationsService } from '../../../services/reservations.service';
import { ServicesService } from '../../../services/services.service';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-reservations-reports',
  templateUrl: './reservations-reports.component.html',
  styleUrls: ['./reservations-reports.component.less'],
})
export class ReservationsReportsComponent implements OnInit {
  loading = false;
  selectedDate = [
    moment().startOf('day').toDate(),
    moment().endOf('day').toDate(),
  ];
  selectedDate1 = [
    moment().startOf('day').toDate(),
    moment().endOf('day').toDate(),
  ];
  selectedMeal: Meal = null;
  selectedDishTypes = [];
  dishsTypesLoading = false;
  dishTypes: DishTypeModel[] = [];
  servicesLoading = false;
  selectedServices = [];
  selectedServices1 = [];
  services: ServiceModel[] = [];

  loadingReport = false;
  loadingReport1 = false;

  constructor(
    private uiService: UiService,
    private dishTypesService: DishTypesService,
    private servicesService: ServicesService,
    private reservationsService: ReservationsService
  ) {}

  ngOnInit() {
    this.loadServices();
    this.loadDishTypes();
  }

  loadDishTypes() {
    this.dishsTypesLoading = true;
    this.dishTypesService
      .list(0, -1, null, null, { withRelated: 'translations' })
      .pipe(
        first(),
        finalize(() => (this.dishsTypesLoading = false))
      )
      .subscribe((res) => {
        this.dishTypes = res.data;
      });
  }

  loadServices() {
    this.servicesLoading = true;
    this.servicesService
      .list(0, -1, null, null, {
        type: 'canteen',
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.servicesLoading = false))
      )
      .subscribe((res) => {
        this.services = res.data;
      });
  }

  reservationsReport() {
    this.loadingReport = true;
    this.reservationsService
      .reservationsReport({
        start_date: this.selectedDate[0],
        end_date: this.selectedDate[1],
        service_id: this.selectedServices,
        dish_type_id: this.selectedDishTypes,
        meal: this.selectedMeal,
      })
      .pipe(
        first(),
        finalize(() => (this.loadingReport = false))
      )
      .subscribe((data) => {
        this.uiService.showMessage(
          MessageType.success,
          'Relatório criado com sucesso.'
        );
      });
  }

  reservationsRevenueReport() {
    this.loadingReport1 = true;
    this.reservationsService
      .reservationsRevenueReport({
        start_date: this.selectedDate1[0],
        end_date: this.selectedDate1[1],
        service_id: this.selectedServices1,
      })
      .pipe(
        first(),
        finalize(() => (this.loadingReport1 = false))
      )
      .subscribe((data) => {
        this.uiService.showMessage(
          MessageType.success,
          'Relatório criado com sucesso.'
        );
      });
  }
}
