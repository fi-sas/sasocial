import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsReservationsComponent } from './stats-reservations.component';

describe('StatsReservationsComponent', () => {
  let component: StatsReservationsComponent;
  let fixture: ComponentFixture<StatsReservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsReservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsReservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
