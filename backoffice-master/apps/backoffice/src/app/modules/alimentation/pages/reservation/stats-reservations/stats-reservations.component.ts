import { Component, OnInit } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { ReservationsService } from '../../../services/reservations.service';

@Component({
  selector: 'fi-sas-stats-reservations',
  templateUrl: './stats-reservations.component.html',
  styleUrls: ['./stats-reservations.component.less']
})
export class StatsReservationsComponent implements OnInit {

  loading = false;
  data = null;
  loadingReport = false;

  selectedDate: Date = new Date();

  constructor(
    private reservationsService: ReservationsService,
    private uiService: UiService,
  ) {
    this.uiService.setContentWrapperActive(false);
  }

  ngOnInit() {
    this.loadStats();
  }

  loadStats() {

    if(!this.selectedDate) {
      return;
    }

    this.loading = true;
    this.reservationsService.getAllCount(this.selectedDate).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.data = result.data;
    });
  }

  getMeals(meal: string, meals : any[]) {
    return meals.filter(m => m.meal == meal);
  }

}
