
import { Component, OnInit } from '@angular/core';
import { DevicesService } from '@fi-sas/backoffice/modules/configurations/services/devices.service';
import { ServiceModel, ServiceType } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { ServicesService as confService } from '@fi-sas/backoffice/modules/configurations/services/services.service';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ServiceModel as ServiceServiceModel } from '@fi-sas/backoffice/modules/configurations/models/service.model';
import { finalize, first } from 'rxjs/operators';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { FamilyModel } from '../../../models/family.model';
import { ServicesService } from '../../../services/services.service';
import { WharehousesService } from '../../../services/wharehouses.service';

@Component({
  selector: 'fi-sas-form-service',
  templateUrl: './form-service.component.html',
  styleUrls: ['./form-service.component.less']
})
export class FormServiceComponent implements OnInit {
  ServiceType = ServiceType;

  serviceForm = new FormGroup({
    name: new FormControl(null, [Validators.required, trimValidation]),
    wharehouse_id: new FormControl(null, [Validators.required]),
    service_id: new FormControl(null, [Validators.required]),
    organic_unit_id: new FormControl(null, [Validators.required]),
    devices: new FormControl(null, [Validators.required]),
    families: new FormControl(null),
    type: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required])
  });

  services: ServiceServiceModel[] = [];
  organicUnits: OrganicUnitsModel[] = [];
  wharehouses: WharehouseModel[] = [];
  devices: DeviceModel[] = [];
  families: FamilyModel[] = [];
  types = [
    { name: 'Bar', value: ServiceType.BAR },
    { name: 'Cantina', value: ServiceType.CANTEEN },
  ];

  id: number;
  submitted: boolean = false;
  loading = false;

  constructor(
    private wharehousesService: WharehousesService,
    private devicesService: DevicesService,
    private familiesService: FamiliesService,
    public confService: confService,
    public serviceService: ServicesService,
    public organicUnitService: OrganicUnitsService,
    private route: ActivatedRoute,
    private uiService: UiService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.loadServices();
    this.loadOrganicUnits();
    this.loadWharehouse();
    this.loadDivices();
    this.loadFamilies();
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.getDataServiceById(this.id);
    }
  }

  get f() { return this.serviceForm.controls; }


  getDataServiceById(id: number) {
    this.loading = true;
    let service: ServiceModel = new ServiceModel();
    this.serviceService
      .read(this.id, {
        withRelated:
          'devices,families',
      })
      .pipe(
        first()
      )
      .subscribe((results) => {
        service = results.data[0];
        this.serviceForm.patchValue({
          name: service.name ? service.name : null,
          wharehouse_id: service.wharehouse_id ? service.wharehouse_id : null,
          service_id: service.service_id ? service.service_id : null,
          organic_unit_id: service.organic_unit_id ? service.organic_unit_id : null,
          devices: service.devices ? service.devices.map( x => x.id ) : null,
          families: service.families ? service.families.map( x => x.id ) : null,
          type: service.type ? service.type : null,
          active: service.active ? service.active : false,
        });
        this.loading = false
      });

  }

  changeType(event) {
    if(event == 'bar') {
      this.serviceForm.controls.families.setValidators([Validators.required]);
    }else {
      this.serviceForm.controls.families.clearValidators();
    }
    this.serviceForm.controls.families.updateValueAndValidity();
  }

  loadServices() {
    this.confService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.services = results.data;
    });
  }

  loadOrganicUnits() {
    this.organicUnitService.list(1, -1, null, null, { active: true, sort: 'name' }).pipe(
      first()
    ).subscribe(results => {
      this.organicUnits = results.data;
    });
  }

  loadWharehouse() {
    this.wharehousesService.list(1, -1, null, null, { active: true, sort: 'name' }).pipe(
      first()
    ).subscribe(results => {
      this.wharehouses = results.data;
    });
  }

  loadDivices() {
    this.devicesService.list(1, -1, null, null, { active: true, sort: 'name' }).pipe(
      first()
    ).subscribe(results => {
      this.devices = results.data;
    });
  }

  loadFamilies() {
    this.familiesService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.families = results.data;
    });
  }


  submitService(edit: boolean) {
    this.submitted = true;
    let sendValues: ServiceModel = new ServiceModel();
    sendValues = this.serviceForm.value;
    if (this.serviceForm.valid) {
      this.loading = true;
      sendValues = this.serviceForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.serviceService.update(this.id, sendValues).pipe(
          first(), finalize(() =>  this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Serviço alterada com sucesso'
            );
            this.backList();
          });
      }
      else {

        this.serviceService.create(sendValues).pipe(
          first(), finalize(() =>  this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Serviço registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }


  backList() {
    this.router.navigateByUrl('/alimentation/service/list');
  }


}
