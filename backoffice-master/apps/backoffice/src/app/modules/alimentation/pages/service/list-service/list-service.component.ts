import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';
import { ServiceModel, ServiceType } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { finalize, first } from 'rxjs/operators';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { WharehousesService } from '../../../services/wharehouses.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';


@Component({
  selector: 'fi-sas-list-service',
  templateUrl: './list-service.component.html',
  styleUrls: ['./list-service.component.less']
})
export class ListServiceComponent extends TableHelper implements OnInit {
  ServiceType = ServiceType;

  public YesNoTag = TagComponent.YesNoTag;

  schools: OrganicUnitsModel[] = [];
  wharehouses: WharehouseModel[] = [];

  constructor(
    public servicesService: ServicesService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private wharehousesService: WharehousesService,
    public organicUnitService: OrganicUnitsService,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name",
      withRelated: 'wharehouse,devices,school,families'
    }
  }

  status = [];
  ngOnInit() {
    this.loadOrganicUnits();
    this.loadWharehouse();
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: 'wharehouse',
        label: 'Armazém',
        sortable: false,
      },
      {
        key: 'school',
        label: 'Escola',
        sortable: false,
      },
      {
        key: 'active',
        label: 'Ativo',
        sortable: true,
      }
    );
    this.initTableData(this.servicesService);

  }

  loadOrganicUnits() {
    this.organicUnitService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
      first()
    ).subscribe(results => {
      this.schools = results.data;
    });
  }

  loadWharehouse() {
    this.wharehousesService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
      first()
    ).subscribe(results => {
      this.wharehouses = results.data;
    });
  }


  editService(id: number) {
    if(!this.authService.hasPermission('alimentation:services:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/service/update/' + id);
  }

  desactive(data: ServiceModel) {
    if(!this.authService.hasPermission('alimentation:services:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Serviço. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: ServiceModel) {
    this.servicesService
      .desactive(data.id, {active: false})
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Serviço desativada com sucesso'
        );
        this.initTableData(this.servicesService);
      });

  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.filters.wharehouse_id = null;
    this.filters.organic_unit_id = null;
    this.searchData(true)
}

  active(data: ServiceModel) {
    if(!this.authService.hasPermission('alimentation:services:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Serviço. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: ServiceModel) {
    this.servicesService
      .active(data.id, { active: true })
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Serviço ativado com sucesso'
        );
        this.initTableData(this.servicesService);
      });
  }


}
