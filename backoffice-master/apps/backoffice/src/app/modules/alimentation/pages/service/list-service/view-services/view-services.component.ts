import { Component, Input, OnInit } from '@angular/core';
import { ServiceModel } from '../../../../models/service.model';

@Component({
  selector: 'fi-sas-view-services',
  templateUrl: './view-services.component.html',
  styleUrls: ['./view-services.component.less']
})
export class ViewServicesComponent implements OnInit {

  @Input() data: ServiceModel = null;

  constructor() { }

  ngOnInit() {
  }

}
