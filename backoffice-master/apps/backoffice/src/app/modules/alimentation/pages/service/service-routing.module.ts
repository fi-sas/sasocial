import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormServiceComponent } from './form-service/form-service.component';
import { ListServiceComponent } from './list-service/list-service.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListServiceComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:services:read'},

  },
  {
    path: 'create',
    component: FormServiceComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:services:create'},
  },
  {
    path: 'update/:id',
    component: FormServiceComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:services:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
