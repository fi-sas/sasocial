import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListServiceComponent } from './list-service/list-service.component';
import { ViewServicesComponent } from './list-service/view-services/view-services.component';
import { FormServiceComponent } from './form-service/form-service.component';
import { ServiceRoutingModule } from './service-routing.module';

@NgModule({
  declarations: [
    ListServiceComponent,
    ViewServicesComponent,
    FormServiceComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ServiceRoutingModule
  ],

})
export class ServiceModule { }
