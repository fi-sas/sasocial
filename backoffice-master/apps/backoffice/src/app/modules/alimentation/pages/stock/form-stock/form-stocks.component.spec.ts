import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormStocksComponent } from './form-stocks.component';

describe('FormStocksComponent', () => {
  let component: FormStocksComponent;
  let fixture: ComponentFixture<FormStocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormStocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
