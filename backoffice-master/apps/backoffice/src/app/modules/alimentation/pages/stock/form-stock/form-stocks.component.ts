import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router } from '@angular/router';
import { ProductModel } from '../../../models/product.model';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { ProductsService } from '../../../services/products.service';
import { WharehousesService } from '../../../services/wharehouses.service';
import { StocksService } from '../../../services/stocks.service';
import { StockModel } from '../../../models/stock.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-form-stocks',
  templateUrl: './form-stocks.component.html',
  styleUrls: ['./form-stocks.component.less']
})
export class FormStocksComponent implements OnInit {
  typeMovements = [];
  reasonsList = [];
  stocksForm = new FormGroup({
    operation: new FormControl(null, [Validators.required]),
    wharehouse_id: new FormControl(null, [Validators.required]),
    product_id: new FormControl(null, [Validators.required]),
    quantity: new FormControl('', [Validators.required, Validators.min(0.01)]),
    lote: new FormControl(null, [Validators.required]),
    stock_reason: new FormControl(null, [Validators.required]),
    to_wharehouse_id: new FormControl(null, []),
    expired: new FormControl(null)
  });

  quantityExist: number = 0;
  isLoadingSelects = false;
  products: ProductModel[] = [];
  searchChange$ = new BehaviorSubject('');
  wharehouses: WharehouseModel[];
  unit: string = 'Kg';
  loading = false;
  lotes: StockModel[] = [];
  submitted: boolean = false;

  constructor(
    private productsService: ProductsService,
    private wharehouseService: WharehousesService,
    private stockService: StocksService,
    private router: Router,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.typeMovements = [
      {
        description: "Entrada",
        value: 'in'
      },
      {
        description: "Saída",
        value: 'out'
      },
      {
        description: "Transferência",
        value: 'transfer'
      }
    ];
    this.reasonsList = [
      {
        description: "Abate",
      },
      {
        description: "Compra",
      },
      {
        description: "Consumo interno",
      },
      {
        description: "Perdas",
      },
      {
        description: "Venda",
      },
    ]
    this.loadWharehouses();
    this.loadProducts();
  }


  get f() { return this.stocksForm.controls; }


  loadProducts() {
    const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
    .pipe(debounceTime(500)).pipe(
      switchMap(event => {
        this.isLoadingSelects = true;
        return this.productsService.list(1, 10, null, null,
          {
            withRelated: "translations,unit",
            search: event,
            active: true,
            searchFields: 'description,name'
          }
        )
      }
      )).pipe(map(res => res.data));

  productList$.subscribe(data => {
    this.products = data;
    this.isLoadingSelects = false;
  });
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }


  loadWharehouses() {
    this.wharehouseService.getAccessUser().pipe(first()).subscribe((data) => {
      this.wharehouses = data.data;
    })
  }

  changeLote(event) {
    this.lotes.forEach((data) => {
      if (data.lote == event) {
        this.quantityExist = data.quantity;
      }
    })
  }

  changeType() {
    this.stocksForm.get('lote').setValue(null);
    if (this.stocksForm.get('operation').value == 'transfer') {
      this.stocksForm.get('to_wharehouse_id').setValidators([Validators.required]);
    } else {
      this.stocksForm.get('to_wharehouse_id').clearValidators();
      this.stocksForm.get('to_wharehouse_id').setValue(null);
    }
    this.stocksForm.get('to_wharehouse_id').updateValueAndValidity();
  }

  getUnit(event) {
    this.unit = 'Kg';
    this.products.forEach((data) => {
      if (data.id == event) {
        this.unit = data.unit.acronym;
      }
    })
  }

  submitStock() {
    this.submitted = true;
    let error = false;
    let sendValues: StockModel = new StockModel();
    if (this.stocksForm.get('operation').value == 'out' || this.stocksForm.get('operation').value == 'transfer') {
      if (this.stocksForm.get('quantity').value > this.quantityExist) {
        error = true;
      }
    }

    if (this.stocksForm.valid && (this.stocksForm.get('to_wharehouse_id').value != this.stocksForm.get('wharehouse_id').value)
      && !error) {
      this.loading = true;
      sendValues = this.stocksForm.value;
      sendValues.stock_reason = this.stocksForm.get('stock_reason').value[0];
      sendValues.lote = this.stocksForm.get('lote').value.length==1 ? this.stocksForm.get('lote').value[0] : this.stocksForm.get('lote').value ;
      this.stockService.create(sendValues).pipe(
        first(), finalize(() => this.loading = false)).subscribe(() => {
          this.loading = false;
          this.uiService.showMessage(
            MessageType.success,
            'Movimento registado com sucesso'
          );
          this.backList();
        });
    }
  }

  getLote() {
    if (this.stocksForm.get('product_id').value && this.stocksForm.get('wharehouse_id').value) {
      this.stocksForm.get('lote').setValue(null);
      this.getLoteByproductWharehouse(this.stocksForm.get('product_id').value, this.stocksForm.get('wharehouse_id').value);
    }
  }

  getLoteByproductWharehouse(id_prod, id_whare) {
    this.stockService.getLotesByProductWharehouse(id_prod, id_whare).pipe(first()).subscribe((data) => {
      this.lotes = data.data;
    })
  }

  backList() {
    this.router.navigateByUrl('/alimentation/stock/list-lotes');
  }


}
