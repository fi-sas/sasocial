import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service'
import { StocksService } from '../../../services/stocks.service';
import { debounceTime, first, switchMap, map, finalize } from 'rxjs/operators';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { ProductModel } from '../../../models/product.model';
import { ProductsService } from '../../../services/products.service';
import { WharehousesService } from '../../../services/wharehouses.service';
import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
@Component({
  selector: 'fi-sas-list-lotes',
  templateUrl: './list-lotes.component.html',
  styleUrls: ['./list-lotes.component.less']
})
export class ListLotesComponent extends TableHelper implements OnInit {
  isLoadingSelects = false;
  products: ProductModel[] = [];
  searchChange$ = new BehaviorSubject('');
  wharehouses: WharehouseModel[];
  dateFilter = null;
  openGenerateModalReport = false;
  wharehouseId;
  loadingReport = false;
  constructor(
    private stockService: StocksService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private message: NzMessageService,
    private productsService: ProductsService,
    private wharehouseService:WharehousesService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "lote"
    }
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'lote',
        label: 'Lote',
        sortable: true
      },
      {
        key: 'product',
        label: 'Produto',
        sortable: false
      },

      {
        key: 'wharehouse',
        label: 'Armazém',
        sortable: false
      },
      {
        key: 'date',
        label: 'Data de expiração',
        sortable: false
      },
      {
        key: 'quantity',
        label: 'Quant. Existente',
        sortable: true
      },

    );
    this.initTableData(this.stockService);
    this.loadProducts();
    this.loadWharehouses();
  }



  loadProducts() {
    const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
      .pipe(debounceTime(500)).pipe(
        switchMap(event => {
          this.isLoadingSelects = true;
          return this.productsService.list(1, 10, null, null,
            {
              withRelated: "translations",
              search: event,
              active: true,
              searchFields: 'description,name'
            }
          )
        }
        )).pipe(map(res => res.data));

    productList$.subscribe(data => {
      this.products = data;
      this.isLoadingSelects = false;
    });
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }
  

  loadWharehouses() {
    this.wharehouseService.getAccessUser().pipe(first()).subscribe((data) => {
      this.wharehouses = data.data;
    })
  }

  listComplete() {
    this.filters.search = null;
    this.filters.wharehouse_id = null;
    this.filters.product_id = null;
    this.filters.expired = null;
    this.filters.quantity = null;
    this.filters.stock_lower = null
    this.dateFilter = null;
    this.searchData(true);
  }

  filter() {
    if(this.dateFilter) {
      this.filters['expired'] = {};
      this.filters['expired']['lt'] = moment(this.dateFilter).format('YYYY-MM-DD');
    }
    this.searchData(true);
  }

  generateReportStock(id) {
    this.loadingReport = true;
      this.stockService.inventoryReport(id, false).pipe(first(), finalize(()=>this.loadingReport = false)).subscribe((data) => {
        this.message.create('success', 'Relatório criado com sucesso');
        this.openGenerateModalReport = false;
      });
    }
}
