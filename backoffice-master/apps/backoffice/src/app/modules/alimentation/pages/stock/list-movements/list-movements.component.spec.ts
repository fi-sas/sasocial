import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListMovementsStockComponent } from './list-movements.component';


describe('ListMovementsStockComponent', () => {
  let component: ListMovementsStockComponent;
  let fixture: ComponentFixture<ListMovementsStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMovementsStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMovementsStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
