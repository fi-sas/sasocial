import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service'
import { StocksHistoryService } from '../../../services/history-stock.service';
import * as moment from 'moment';
import { debounceTime, first, switchMap, map } from 'rxjs/operators';
import { WharehousesService } from '../../../services/wharehouses.service';
import { ProductsService } from '../../../services/products.service';
import { ProductModel } from '../../../models/product.model';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { DevicesService } from '@fi-sas/backoffice/modules/configurations/services/devices.service';
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { BehaviorSubject, Observable } from 'rxjs';
@Component({
    selector: 'fi-sas-list-movements-stock',
    templateUrl: './list-movements.component.html',
    styleUrls: ['./list-movements.component.less']
})
export class ListMovementsStockComponent extends TableHelper implements OnInit {
    isLoadingSelects = false;
    products: ProductModel[] = [];
    searchChange$ = new BehaviorSubject('');
    wharehouses: WharehouseModel[];
    typeMovements = [];
    startDate = null;
    endDate = null;
    devices: DeviceModel[] = [];
    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        private productsService: ProductsService,
        private deviceService: DevicesService,
        private wharehouseService: WharehousesService,
        private stocksHistoryService: StocksHistoryService
    ) {
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            withRelated: "stock,user,device,transferWharehouse",
            searchFields: "lote"
        }

    }

    ngOnInit() {
        this.typeMovements = [
            {
                description: "Entrada",
                value: 'in'
            },
            {
                description: "Saída",
                value: 'out'
            }
        ];
        this.columns.push(
            {
                key: 'created_at',
                label: 'Data',
                sortable: true
            },
            {
                key: 'product',
                label: 'Produto',
                sortable: false
            },
            {
                key: 'expired',
                label: 'Data de expiração',
                sortable: false
            },
            {
                key: 'wharehouse',
                label: 'Armazém',
                sortable: false
            },
            {
                key: 'operation',
                label: 'Movimento',
                sortable: false
            },
            {
                key: 'stock_reason',
                label: 'Motivo',
                sortable: true
            },
            {
                key: 'lote',
                label: 'Lote',
                sortable: true
            },
            {
                key: 'quantity_after',
                label: 'Quantidade',
                sortable: true
            },
            {
                key: 'device',
                label: 'Origem',
                sortable: false
            },
            {
                key: 'user',
                label: 'Utilizador',
                sortable: false
            },

        );
        this.initTableData(this.stocksHistoryService);
        this.loadProducts();
        this.loadWharehouses();
        this.getDevices();
    }


    loadProducts() {
        const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
            .pipe(debounceTime(500)).pipe(
                switchMap(event => {
                    this.isLoadingSelects = true;
                    return this.productsService.list(1, 10, null, null,
                        {
                            withRelated: "translations",
                            search: event,
                            active: true,
                            searchFields: 'description,name'
                        }
                    )
                }
                )).pipe(map(res => res.data));

        productList$.subscribe(data => {
            this.products = data;
            this.isLoadingSelects = false;
        });
    }

    onSearch(value: string): void {
        this.searchChange$.next(value);
    }

    loadWharehouses() {
        this.wharehouseService.getAccessUser().pipe(first()).subscribe((data) => {
            this.wharehouses = data.data;
        })
    }


    getDevices() {
        this.deviceService.list(1, -1, null, null, {
            sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.devices = data.data;
        })
    }

    listComplete() {
        this.filters.search = null;
        this.filters.wharehouse_id = null;
        this.filters.product_id = null;
        this.filters.created_at = null;
        this.filters.operation = null;
        this.startDate = null;
        this.endDate = null;
        this.filters.user_id = null;
        this.filters.device_id = null;
        this.searchData(true);
    }

    filter() {
        if (this.startDate && this.endDate) {
            this.filters['created_at'] = {};
            this.filters['created_at']['lte'] = moment(this.endDate).format('YYYY-MM-DD');
            this.filters['created_at']['gte'] = moment(this.startDate).format('YYYY-MM-DD');
        }
        this.searchData(true);
    }


}
