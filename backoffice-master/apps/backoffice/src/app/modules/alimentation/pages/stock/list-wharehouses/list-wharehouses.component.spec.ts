import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWharehousesStockComponent } from './list-wharehouses.component';

describe('ListWharehousesStockComponent', () => {
  let component: ListWharehousesStockComponent;
  let fixture: ComponentFixture<ListWharehousesStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWharehousesStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWharehousesStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
