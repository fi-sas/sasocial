import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service'
import { StocksService } from '../../../services/stocks.service';
import { debounceTime, first, switchMap, map, finalize } from 'rxjs/operators';
import { ProductsService } from '../../../services/products.service';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { ProductModel } from '../../../models/product.model';
import { WharehousesService } from '../../../services/wharehouses.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { StocksGroupService } from '../../../services/stock-group.service';
import { NzMessageService } from 'ng-zorro-antd';
@Component({
    selector: 'fi-sas-list-wharehouses-stock',
    templateUrl: './list-wharehouses.component.html',
    styleUrls: ['./list-wharehouses.component.less']
})
export class ListWharehousesStockComponent extends TableHelper implements OnInit {
    isLoadingSelects = false;
    products: ProductModel[] = [];
    searchChange$ = new BehaviorSubject('');
    wharehouses: WharehouseModel[];
    openGenerateModalReport = false;
    wharehouseId;
    loadingReport = false;
    constructor(
        private stocksGroupService: StocksGroupService,
        private stocksService: StocksService,
        private productsService: ProductsService,
        private wharehouseService: WharehousesService,
        router: Router,
        private message: NzMessageService,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
    ) {
        super(uiService, router, activatedRoute);
    }

    ngOnInit() {
        this.columns.push(
            {
                key: 'wharehouse',
                label: 'Armazém',
                sortable: false
            },
            {
                key: 'product',
                label: 'Produto',
                sortable: false
            },
            {
                key: 'quantity',
                label: 'Quant. Existente',
                sortable: true
            },
            {
                key: 'minimal_stock',
                label: 'Stock mínimo',
                sortable: false
            },

        );
        this.initTableData(this.stocksGroupService);
        this.loadWharehouses();
        this.loadProducts();
    }


    loadProducts() {
        const productList$: Observable<ProductModel[]> = this.searchChange$.asObservable()
            .pipe(debounceTime(500)).pipe(
                switchMap(event => {
                    this.isLoadingSelects = true;
                    return this.productsService.list(1, 10, null, null,
                        {
                            withRelated: "translations",
                            search: event,
                            active: true,
                            searchFields: 'description,name'
                        }
                    )
                }
                )).pipe(map(res => res.data));

        productList$.subscribe(data => {
            this.products = data;
            this.isLoadingSelects = false;
        });
    }

    onSearch(value: string): void {
        this.searchChange$.next(value);
    }
    loadWharehouses() {
        this.wharehouseService.getAccessUser().pipe(first()).subscribe((data) => {
            this.wharehouses = data.data;
        })
    }

    listComplete() {
        this.filters.wharehouse_id = null;
        this.filters.product_id = null;
        this.filters.stock_lower = null
        this.searchData(true);
    }

    generateReportStock(id) {
    this.loadingReport = true;
      this.stocksService.inventoryReport(id, true).pipe(first(), finalize(()=>this.loadingReport = false)).subscribe((data) => {
        this.message.create('success', 'Relatório criado com sucesso');
        this.openGenerateModalReport = false;
      });
    }

}
