import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormStocksComponent } from './form-stock/form-stocks.component';
import { ListLotesComponent } from './list-lotes/list-lotes.component';
import { ListMovementsStockComponent } from './list-movements/list-movements.component';
import { ListWharehousesStockComponent } from './list-wharehouses/list-wharehouses.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list-lotes',
    component: ListLotesComponent,
    data: { breadcrumb: 'Análise lotes', title: 'Análise lotes', scope: 'alimentation:stocks:read'},

  },
  {
    path: 'list-wharehouses',
    component: ListWharehousesStockComponent,
    data: { breadcrumb: 'Análise armazéns', title: 'Análise armazéns', scope: 'alimentation:stocks:read'},

  },
  {
    path: 'list-movements',
    component: ListMovementsStockComponent,
    data: { breadcrumb: 'Análise movimentos', title: 'Análise movimentos', scope: 'alimentation:stocks:read'},

  },
  {
    path: 'create',
    component: FormStocksComponent,
    data: { breadcrumb: 'Criar movimento', title: 'Criar movimento', scope: 'alimentation:stocks:create'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
