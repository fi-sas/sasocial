import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormStocksComponent } from './form-stock/form-stocks.component';
import { StockRoutingModule } from './stock-routing.module';
import { ListLotesComponent } from './list-lotes/list-lotes.component';
import { ListWharehousesStockComponent } from './list-wharehouses/list-wharehouses.component';
import { ListMovementsStockComponent } from './list-movements/list-movements.component';

@NgModule({
  declarations: [
    ListLotesComponent,
    FormStocksComponent,
    ListWharehousesStockComponent,
    ListMovementsStockComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StockRoutingModule
  ],

})
export class StockModule { }
