import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { UnitsService } from '@fi-sas/backoffice/modules/alimentation/services/units.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { finalize, first } from 'rxjs/operators';
import * as _ from 'lodash';
import { hasOwnProperty } from 'tslint/lib/utils';
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { UnitModel } from '../../../models/unit.model';

@Component({
  selector: 'fi-sas-form-unit',
  templateUrl: './units-form.component.html',
  styleUrls: ['./units-form.component.less']
})
export class FormUnitComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  loading = false;
  errorTrans = false;

  id: number;
  submitted: boolean = false;


  unitForm = new FormGroup({
    acronym: new FormControl(null, [Validators.required, Validators.maxLength(5), trimValidation]),
    translations: new FormArray([]),
    active: new FormControl(true, [Validators.required])
  });

  translations = this.unitForm.get('translations') as FormArray;

  constructor(
    public unitsService: UnitsService,
    private languagesService: LanguagesService,
    private route: ActivatedRoute,
    private router: Router,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDataUnitById(this.id);
    } else {
      this.loadLanguages('');
    }
  }

  getDataUnitById(id: number) {
    let unit: UnitModel = new UnitModel();
    this.unitsService.getUnitById(id).pipe(
      first()
    ).subscribe(results => {
      unit = results.data[0];
      this.unitForm.patchValue({
        ...unit,
      });
      this.listOfSelectedLanguages = [];
      this.loadLanguages(unit);
    });

  }

  getAcronymInputError() {
    return this.f.acronym.errors.required
      ? 'Campo obrigatório'
      : this.f.acronym.errors.maxlength ? 'Tamanho máximo é de 5' : '';
  }

  submitUnit(edit: boolean) {
    this.submitted = true;
    let sendValues: UnitModel = new UnitModel();
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if(this.unitForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if (this.unitForm.valid && !this.errorTrans) {
      this.loading = true;
      sendValues = this.unitForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.unitsService.update(this.id, sendValues).pipe(
          first(), finalize( () =>  this.loading = false )).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Unidade alterada com sucesso'
            );
            this.backList();
          });

      } else {
        this.unitsService.create(sendValues).pipe(
          first(), finalize(() =>  this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Unidade registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/unit/list');
  }

  get f() { return this.unitForm.controls; }

  resetForm() {
    this.unitForm.reset();
  }

  loadLanguages(unit: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
        if (unit) {
          this.startTranslation(unit);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.unitForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.unitForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }
}
