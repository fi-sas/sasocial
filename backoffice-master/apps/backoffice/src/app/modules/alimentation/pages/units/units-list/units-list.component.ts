import { Component, OnInit } from '@angular/core';
import { UnitsService } from '@fi-sas/backoffice/modules/alimentation/services/units.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { UnitModel } from '../../../models/unit.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-units',
  templateUrl: './units-list.component.html',
  styleUrls: ['./units-list.component.less'],
})
export class ListUnitsComponent extends TableHelper implements OnInit {
  fields = {
    'translations.name': { translation: 'Nome' },
    acronym: 'Acrónimo',
  };
  languages: LanguageModel[] = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public unitsService: UnitsService,
    private modalService: NzModalService,
    private languageService: LanguagesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  status = [];
  ngOnInit() {
    this.loadLanguages();
    this.status = [
      {
        description: 'Ativo',
        value: true,
      },
      {
        description: 'Desativo',
        value: false,
      },
    ];

    this.initTableData(this.unitsService);
    this.persistentFilters = {
      searchFields: 'acronym,name',
    };
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
      ).subscribe(result => {
      this.languages = result.data;
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true);
  }

  editUnit(id: number) {
    if(!this.authService.hasPermission('alimentation:units:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/unit/update/' + id);
  }

  desactive(data: UnitModel) {
    if(!this.authService.hasPermission('alimentation:units:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle:
        'Vai desativar esta Unidade. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data),
    });
  }

  desactiveDataSubmit(data: UnitModel) {
    data.active = false;
    this.unitsService.desactive(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Unidade desativada com sucesso'
      );
      this.initTableData(this.unitsService);
    });
  }

  active(data: UnitModel) {
    if(!this.authService.hasPermission('alimentation:units:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Unidade. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data),
    });
  }

  activeDataSubmit(data: UnitModel) {
    data.active = true;
    this.unitsService.active(data.id, data).pipe(first()).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Unidade ativada com sucesso'
      );
      this.initTableData(this.unitsService);
    });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
