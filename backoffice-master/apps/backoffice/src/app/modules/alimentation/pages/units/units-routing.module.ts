import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormUnitComponent } from './units-form/units-form.component';
import { ListUnitsComponent } from './units-list/units-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListUnitsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:units:read'},

  },
  {
    path: 'create',
    component: FormUnitComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:units:create'},
  },
  {
    path: 'update/:id',
    component: FormUnitComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:units:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitsRoutingModule { }
