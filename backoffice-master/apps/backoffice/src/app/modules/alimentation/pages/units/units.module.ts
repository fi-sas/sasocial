import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UnitsRoutingModule } from './units-routing.module';
import { FormUnitComponent } from './units-form/units-form.component';
import { ListUnitsComponent } from './units-list/units-list.component';

@NgModule({
  declarations: [
    ListUnitsComponent,
    FormUnitComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UnitsRoutingModule
  ],

})
export class UnitsModule { }
