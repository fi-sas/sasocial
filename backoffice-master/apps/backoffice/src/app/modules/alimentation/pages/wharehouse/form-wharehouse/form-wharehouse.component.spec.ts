import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormWharehouseComponent } from './form-wharehouse.component';

describe('FormWharehouseComponent', () => {
  let component: FormWharehouseComponent;
  let fixture: ComponentFixture<FormWharehouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormWharehouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormWharehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
