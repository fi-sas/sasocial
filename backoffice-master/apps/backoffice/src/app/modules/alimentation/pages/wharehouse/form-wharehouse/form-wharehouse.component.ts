import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { WharehouseModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse.model';
import { WharehousesService } from '@fi-sas/backoffice/modules/alimentation/services/wharehouses.service';
import { FormField, InputType } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { finalize, first } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-wharehouse',
  templateUrl: './form-wharehouse.component.html',
  styleUrls: ['./form-wharehouse.component.less']
})
export class FormWharehouseComponent implements OnInit {

  accessModalVisible = false;

 
  updateWharehouse: WharehouseModel = null;

  wharehouseForm = new FormGroup({
    name: new FormControl(null, [Validators.required, trimValidation]),
    active: new FormControl(true, [Validators.required])
  });

  loading = false;

  id: number;
  submitted: boolean = false;

  constructor(
    public wharehousesService: WharehousesService,
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router,
    private uiService: UiService,
  ) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDataWharehouseById(this.id);
    }
  }

  get f() { return this.wharehouseForm.controls; }

  getDataWharehouseById(id: number) {
    this.loading = true;
    let wahrehouse: WharehouseModel = new WharehouseModel();
    this.wharehousesService
      .read(this.id, {
        withRelated: false,
      })
      .pipe(
        first()
      )
      .subscribe((results) => {
        wahrehouse = results.data[0];
        this.wharehouseForm.patchValue({
          ...wahrehouse
        });
        this.loading = false
      });
  }

  submitWharehouse(edit: boolean) {
    this.submitted = true;
    let sendValues: WharehouseModel = new WharehouseModel();
    if (this.wharehouseForm.valid) {
      this.loading = true;
      sendValues = this.wharehouseForm.value;
      if (edit) {
        sendValues.id = this.id;
        this.wharehousesService.update(this.id, sendValues).pipe(
          first(), finalize(() =>  this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Armazém alterada com sucesso'
            );
            this.backList();
          });

      } else {
        this.wharehousesService.create(sendValues).pipe(
          first(), finalize(() =>  this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Armazém registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/alimentation/wharehouse/list');
  }
}
