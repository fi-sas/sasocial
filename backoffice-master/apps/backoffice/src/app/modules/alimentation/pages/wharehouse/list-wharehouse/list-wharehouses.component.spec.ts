import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWharehousesComponent } from './list-wharehouses.component';

describe('ListWharehousesComponent', () => {
  let component: ListWharehousesComponent;
  let fixture: ComponentFixture<ListWharehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWharehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWharehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
