import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { WharehousesService } from '@fi-sas/backoffice/modules/alimentation/services/wharehouses.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from "ng-zorro-antd";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { WharehouseModel } from '../../../models/wharehouse.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';



@Component({
  selector: 'fi-sas-list-wharehouses',
  templateUrl: './list-wharehouses.component.html',
  styleUrls: ['./list-wharehouses.component.less']
})
export class ListWharehousesComponent extends TableHelper implements OnInit {


  wharehouses: WharehouseModel[];
  status = [];

  public YesNoTag = TagComponent.YesNoTag;

  accessModalVisible = false;
  loading_users = false;

  wharehouseUsers: UserModel[] = [];


  usersWharehouseForm = new FormGroup({
    user_id: new FormControl(null, [Validators.required])
  });

  constructor(
    public wharehousesService: WharehousesService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private modalService: NzModalService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name"
    }
  }

  ngOnInit() {
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
   
    this.initTableData(this.wharehousesService);
  }

  get f() { return this.usersWharehouseForm.controls; }


  editWharehouse(id: number) {
    if(!this.authService.hasPermission('alimentation:wharehouses:update')){
      return;
    }
    this.router.navigateByUrl('/alimentation/wharehouse/update/' + id);
  }

  desactive(data: WharehouseModel) {
    if(!this.authService.hasPermission('alimentation:wharehouses:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Armazém. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });

  }

  desactiveDataSubmit(data: WharehouseModel) {
    data.active = false;
    this.wharehousesService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Armazém desativada com sucesso'
        );
        this.initTableData(this.wharehousesService);
      });

  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }

  active(data: WharehouseModel) {
    if(!this.authService.hasPermission('alimentation:wharehouses:update')){
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Armazém. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: WharehouseModel) {
    data.active = true;
    this.wharehousesService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Armazém ativado com sucesso'
        );
        this.initTableData(this.wharehousesService);
      });
  }

  wharehouse_id: number;

  toggleAccessModal(wharehouse_id: number) {
    if(!this.authService.hasPermission('alimentation:wharehouses:read')){
      return;
    }
    if (wharehouse_id !== null) {
      this.wharehouse_id = wharehouse_id;
      this.updateUsersList()
    }
    this.accessModalVisible = !this.accessModalVisible;
    if (this.accessModalVisible === false) {
      this.initTableData(this.wharehousesService);
    }
  }


  addUserToWharehouse() {
    let error = false;
    this.wharehouseUsers.forEach((user) => {
      if (user.id == this.usersWharehouseForm.controls.user_id.value) {
        error = true;
      }
    })
    if (error) {
      this.uiService.showMessage(MessageType.error, 'Esse utilizador já foi adicionado');
    } else {
      if (!this.usersWharehouseForm.invalid) {
        this.wharehousesService.addUser(this.wharehouse_id, this.usersWharehouseForm.controls.user_id.value).subscribe(res => {
          this.updateUsersList();
          this.uiService.showMessage(MessageType.success, 'Utilizador adicionado com sucesso');
        });

      }
    }
  }

  removeUserFromWharehouse(id: number) {
    this.wharehousesService.removeUser(this.wharehouse_id, id).subscribe(res => {
      this.updateUsersList();
      this.uiService.showMessage(MessageType.success, 'Utilizador removido com sucesso');
    });
  }

  updateUsersList() {
    this.loading_users = true;
    this.wharehousesService.listUsers(this.wharehouse_id).subscribe(users => {
      this.wharehouseUsers = users.data;
      this.loading_users = false;
    });
  }



}
