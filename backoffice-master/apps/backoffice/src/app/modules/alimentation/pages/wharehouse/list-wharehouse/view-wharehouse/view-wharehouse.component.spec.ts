import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewWharehousesComponent } from './view-wharehouse.component';


describe('ViewWharehousesComponent', () => {
  let component: ViewWharehousesComponent;
  let fixture: ComponentFixture<ViewWharehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewWharehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWharehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
