import { Component, Input, OnInit } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { WharehousesService } from '@fi-sas/backoffice/modules/alimentation/services/wharehouses.service';
import { WharehouseModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse.model';

@Component({
  selector: 'fi-sas-view-wharehouse',
  templateUrl: './view-wharehouse.component.html',
  styleUrls: ['./view-wharehouse.component.less']
})
export class ViewWharehousesComponent implements OnInit {

  @Input() data: WharehouseModel = null;
  constructor(public wharehousesService: WharehousesService) { }

  users: UserModel[] = [];
  ngOnInit() {
    this.wharehousesService.listUsers(this.data.id).pipe(
      first()
    ).subscribe(results => {
      this.users = results.data;
    });
  }

}
