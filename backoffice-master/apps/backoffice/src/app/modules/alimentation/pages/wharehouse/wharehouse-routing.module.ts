import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormWharehouseComponent } from './form-wharehouse/form-wharehouse.component';
import { ListWharehousesComponent } from './list-wharehouse/list-wharehouses.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListWharehousesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'alimentation:wharehouses:read'},

  },
  {
    path: 'create',
    component: FormWharehouseComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'alimentation:wharehouses:create'},
  },
  {
    path: 'update/:id',
    component: FormWharehouseComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'alimentation:wharehouses:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WharehouseRoutingModule { }
