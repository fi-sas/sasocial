import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListWharehousesComponent } from './list-wharehouse/list-wharehouses.component';
import { FormWharehouseComponent } from './form-wharehouse/form-wharehouse.component';
import { WharehouseRoutingModule } from './wharehouse-routing.module';
import { ViewWharehousesComponent } from './list-wharehouse/view-wharehouse/view-wharehouse.component';

@NgModule({
  declarations: [
    ListWharehousesComponent,
    FormWharehouseComponent,
    ViewWharehousesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    WharehouseRoutingModule
  ],

})
export class WharehouseModule { }
