import { Injectable } from '@angular/core';
import {  Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { ComplementModel } from '@fi-sas/backoffice/modules/alimentation/models/complement.model';
import { ComplementsService } from '@fi-sas/backoffice/modules/alimentation/services/complements.service';

@Injectable()
export class ListAllComplementsResolver implements Resolve<Observable<Resource<ComplementModel>>> {
  constructor(private complementsService: ComplementsService) {}

  resolve() {
    return this.complementsService.list(1, -1).pipe(first()).catch(() => {
      return EMPTY;
    });
  }
}
