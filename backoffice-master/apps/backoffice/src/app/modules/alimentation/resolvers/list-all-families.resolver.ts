import { Injectable } from '@angular/core';
import {  Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { FamilyModel } from '@fi-sas/backoffice/modules/alimentation/models/family.model';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';

@Injectable()
export class ListAllFamiliesResolver implements Resolve<Observable<Resource<FamilyModel>>> {
  constructor(private familiesService: FamiliesService) {}

  resolve() {
    return this.familiesService.list(1, -1).pipe(first()).catch(() => {
      return EMPTY;
    });
  }
}
