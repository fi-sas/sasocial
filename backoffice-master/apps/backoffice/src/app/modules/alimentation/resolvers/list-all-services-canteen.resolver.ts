import { Injectable } from '@angular/core';
import {  Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { ServiceModel, ServiceType } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { ServicesService } from '@fi-sas/backoffice/modules/alimentation/services/services.service';

@Injectable()
export class ListAllServicesCanteenResolver implements Resolve<Observable<Resource<ServiceModel>>> {
  constructor(private servicesService: ServicesService) {}

  resolve() {
    return this.servicesService.list(1, -1, null,null, ServiceType.CANTEEN).pipe(first()).catch(() => {
      return EMPTY;
    });
  }
}
