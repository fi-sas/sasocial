import { Injectable } from '@angular/core';
import {  Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';
import { UnitsService } from '@fi-sas/backoffice/modules/alimentation/services/units.service';

@Injectable()
export class ListAllUnitsResolver implements Resolve<Observable<Resource<UnitModel>>> {
  constructor(private unitsService: UnitsService) {}

  resolve() {
    return this.unitsService.list(1, -1).pipe(first()).catch(() => {
      return EMPTY;
    });
  }
}
