import { Injectable } from '@angular/core';
import {  Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { WharehouseModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse.model';
import { WharehousesService } from '@fi-sas/backoffice/modules/alimentation/services/wharehouses.service';

@Injectable()
export class ListAllWharehousesResolver implements Resolve<Observable<Resource<WharehouseModel>>> {
  constructor(private wharehousesService: WharehousesService) {}

  resolve() {
    return this.wharehousesService.list(1, -1).pipe(first()).catch(() => {
      return EMPTY;
    });
  }
}
