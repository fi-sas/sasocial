import { TestBed, inject } from '@angular/core/testing';

import { AlimentationService } from './alimentation.service';

describe('AlimentationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlimentationService]
    });
  });

  it('should be created', inject([AlimentationService], (service: AlimentationService) => {
    expect(service).toBeTruthy();
  }));
});
