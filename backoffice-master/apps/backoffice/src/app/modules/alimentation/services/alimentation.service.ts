import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AlimentationService {


  private selectedEntity: FiscalEntityModel;
  private selectedEntitySubject: BehaviorSubject<FiscalEntityModel> = new BehaviorSubject<FiscalEntityModel>(
    this.selectedEntity
  );

  constructor(
    private uiService: UiService,private authService: AuthService
  ) {}

  /***
   * Set a fiscal entity selected to use on the requests of the alimentation module
   * @param entity
   */
  setSelectedEntity(entity: FiscalEntityModel): FiscalEntityModel {
    this.selectedEntity = entity;
    this.selectedEntitySubject.next(entity);
    localStorage.setItem('alimentation_entity' + '_' + this.authService.getUser().id.toString(), JSON.stringify(this.selectedEntity));
    return this.selectedEntity;
  }

  /***
   * Returns the selected fiscal entity
   */
  getSelectedEntity() {
      const storedEnt = localStorage.getItem('alimentation_entity'+ '_' + this.authService.getUser().id.toString());
      const tempEnt: FiscalEntityModel = JSON.parse(storedEnt);
      this.selectedEntity = tempEnt;
      this.selectedEntitySubject.next(this.selectedEntity);
      return this.selectedEntity;
  }

  /***
   * Returns the observble os the selected fiscal entity
   */
  seletedEntityObservable(): Observable<FiscalEntityModel> {
    return this.selectedEntitySubject.asObservable();
  }
}
