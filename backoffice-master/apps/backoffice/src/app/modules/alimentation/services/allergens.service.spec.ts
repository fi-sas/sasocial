import { TestBed, inject } from '@angular/core/testing';

import { AllergensService } from './allergens.service';

describe('AllergensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllergensService]
    });
  });

  it('should be created', inject([AllergensService], (service: AllergensService) => {
    expect(service).toBeTruthy();
  }));
});
