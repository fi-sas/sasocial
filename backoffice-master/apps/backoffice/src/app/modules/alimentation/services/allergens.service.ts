import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { AllergenModel } from '@fi-sas/backoffice/modules/alimentation/models/allergen.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class AllergensService extends Repository<AllergenModel> {

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.ALLERGENS';
    this.entity_url = 'FOOD.ALLERGENS_ID';
  }


  create(data: AllergenModel): Observable<Resource<AllergenModel>> {
    const url = this.urlService.get(this.entities_url);
    return this.resourceService.create(url, data);
  }

  getAllergenById(id: number): Observable<Resource<AllergenModel>> {
    return this.resourceService.list(this.urlService.get(this.entity_url, { id }));
  }

  update(id: number, data: AllergenModel): Observable<Resource<AllergenModel>> {
    const url = this.urlService.get(this.entity_url, { id });
    return this.resourceService.update(url, data);
  }

  desactive(id: number, data: AllergenModel): Observable<Resource<AllergenModel>> {
    return this.resourceService.patch<AllergenModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: AllergenModel): Observable<Resource<AllergenModel>> {
    return this.resourceService.patch<AllergenModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
