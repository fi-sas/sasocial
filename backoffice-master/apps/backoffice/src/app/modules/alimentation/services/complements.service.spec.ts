import { TestBed, inject } from '@angular/core/testing';

import { ComplementsService } from './complements.service';

describe('ComplementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComplementsService]
    });
  });

  it('should be created', inject([ComplementsService], (service: ComplementsService) => {
    expect(service).toBeTruthy();
  }));
});
