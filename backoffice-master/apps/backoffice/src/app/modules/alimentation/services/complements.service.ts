import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { ComplementModel } from '@fi-sas/backoffice/modules/alimentation/models/complement.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class ComplementsService extends Repository<ComplementModel> {


  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.COMPLEMENTS';
    this.entity_url = 'FOOD.COMPLEMENTS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
      
    });
  }

  desactive(id: number, data: any): Observable<Resource<ComplementModel>> {
    return this.resourceService.patch<ComplementModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: any): Observable<Resource<ComplementModel>> {
    return this.resourceService.patch<ComplementModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

}
