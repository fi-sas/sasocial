import { TestBed, inject } from '@angular/core/testing';

import { DishTypesService } from './dish-types.service';

describe('DishTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DishTypesService]
    });
  });

  it('should be created', inject([DishTypesService], (service: DishTypesService) => {
    expect(service).toBeTruthy();
  }));
});
