import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { DishTypeModel } from '@fi-sas/backoffice/modules/alimentation/models/dish-type.model';
import { PriceVariationModel } from '../models/price-variation.model';
import { DishTypePriceModel } from '../models/dish-type-prices.model';

@Injectable({
  providedIn: 'root'
})
export class DishTypesService extends Repository<DishTypeModel> {


  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.DISH_TYPES';
    this.entity_url = 'FOOD.DISH_TYPES_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
      
    });
  }

  getPrices(dishType_id: number): Observable<Resource<DishTypePriceModel>> {
    const url = this.urlService.get('FOOD.DISH_TYPES_ID_PRICES', {id: dishType_id});
    return this.resourceService.list<DishTypePriceModel>(url, { headers: this.getHeaders()}).pipe(first());
  }

  patchPrices(dishType_id: number, prices: any): Observable<Resource<DishTypePriceModel>> {
    const url = this.urlService.get('FOOD.DISH_TYPES_ID_PRICES', { id: dishType_id});
    return this.resourceService.patch<DishTypePriceModel>(url, { prices : prices }, { headers: this.getHeaders()}).pipe(first());
  }
  

  desactive(id: number, data: DishTypeModel): Observable<Resource<DishTypeModel>> {
    return this.resourceService.patch<DishTypeModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: DishTypeModel): Observable<Resource<DishTypeModel>> {
    return this.resourceService.patch<DishTypeModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
