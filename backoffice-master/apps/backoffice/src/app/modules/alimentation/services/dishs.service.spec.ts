import { TestBed, inject } from '@angular/core/testing';

import { DishsService } from './dishs.service';

describe('DishsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DishsService]
    });
  });

  it('should be created', inject([DishsService], (service: DishsService) => {
    expect(service).toBeTruthy();
  }));
});
