import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { DishModel } from '@fi-sas/backoffice/modules/alimentation/models/dish.model';
import { DishTypeModel } from '../models/dish-type.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DishsService extends Repository<DishModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.DISHS';
    this.entity_url = 'FOOD.DISHS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });

    
  }

  desactive(id: number, data: DishModel): Observable<Resource<DishModel>> {
    return this.resourceService.patch<DishModel>(this.urlService.get(this.entity_url, { id }), { active: false}, { headers: this.getHeaders()});
  }

  active(id: number, data: DishModel): Observable<Resource<DishModel>> {
    return this.resourceService.patch<DishModel>(this.urlService.get(this.entity_url, { id }), { active : true}, { headers: this.getHeaders()});
  }
}
