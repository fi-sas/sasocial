import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AlimentationService } from './alimentation.service';

@Injectable({
  providedIn: 'root'
})
export class EntityService extends Repository<FiscalEntityModel> {

  selectedEntity: FiscalEntityModel;

  constructor(
    private alimentationService: AlimentationService,
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.ENTITIES';
    this.entity_url = 'FOOD.ENTITIES_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  entitiesOfUser() : Observable<Resource<FiscalEntityModel>> {
    return this.resourceService.list<FiscalEntityModel>(this.urlService.get('FOOD.ENTITIES_OF_USER', {})).pipe(first());
   }

  listUsers(id: number): Observable<Resource<UserModel>> {
   return this.resourceService.list<UserModel>(this.urlService.get('FOOD.ENTITIES_USERS', { id }), { headers: this.getHeaders()}).pipe(first());
  }

  addUser(entity_id: number, user_id: number, manager: boolean): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(this.urlService.get('FOOD.ENTITIES_USERS',{ id: entity_id  }), { user_id , manager }).pipe(first());
  }

  removeUser(entity_id: number, user_id: number): Observable<any> {
    return this.resourceService.delete(this.urlService.get('FOOD.ENTITIES_USERS_ID', { entity_id, user_id }), { headers: this.getHeaders()}).pipe(first());
  }

  desactive(id: number, data: FiscalEntityModel): Observable<Resource<FiscalEntityModel>> {
    return this.resourceService.patch<FiscalEntityModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: FiscalEntityModel): Observable<Resource<FiscalEntityModel>> {
    return this.resourceService.patch<FiscalEntityModel>(this.urlService.get(this.entity_url, { id }), data);
  }
}
