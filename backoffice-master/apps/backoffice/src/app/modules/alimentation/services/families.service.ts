import { Repository } from './../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { FamilyModel } from '@fi-sas/backoffice/modules/alimentation/models/family.model';

@Injectable({
  providedIn: 'root'
})
export class FamiliesService extends Repository<FamilyModel> {


  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.FAMILIES';
    this.entity_url = 'FOOD.FAMILIES_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  desactive(id: number): Observable<Resource<FamilyModel>> {
    return this.resourceService.patch<FamilyModel>(this.urlService.get(this.entity_url, { id }), {active: false});
  }

  active(id: number): Observable<Resource<FamilyModel>> {
    return this.resourceService.patch<FamilyModel>(this.urlService.get(this.entity_url, { id }), {active: true});
  }

}
