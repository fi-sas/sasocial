import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { FoodConfigrationsModel } from '../models/food-configurations.model';

@Injectable({
  providedIn: 'root'
})
export class FoodConfigurationsService extends Repository<FoodConfigrationsModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.CONFIGURATIONS';
  }
}
