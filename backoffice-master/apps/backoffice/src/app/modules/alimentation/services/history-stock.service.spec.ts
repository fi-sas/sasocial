import { TestBed, inject } from '@angular/core/testing';
import { StocksHistoryService } from './history-stock.service';


describe('StocksHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StocksHistoryService]
    });
  });

  it('should be created', inject([StocksHistoryService], (service: StocksHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
