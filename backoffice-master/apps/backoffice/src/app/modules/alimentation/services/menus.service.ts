import { CopyWeekDto } from './../dtos/copy-week.dto';
import { CopyDayDto } from './../dtos/copy-day.dto';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { MenuModel } from '@fi-sas/backoffice/modules/alimentation/models/menu.model';
import { MenuBulkDto } from '../dtos/menu-bulk.dto';
import { CopyMenuDto } from '../dtos/copy-menu.dto';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MenusService extends Repository<MenuModel> {

  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.MENUS';
    this.entity_url = 'FOOD.MENUS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  list(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string,
       date?: Date, date_begin?: Date, date_end?: Date, service_id?: number): Observable<Resource<MenuModel>> {
    let params = new HttpParams();
    params = params.set('offset', Math.abs((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());

    params = params.set('withRelated', 'dishs,dishstype');

    if(date) {
      params = params.set('query[date]', moment(date).format('YYYY-MM-DD'))
    }

    if(date_begin) {
      params = params.set('query[date][gte]', moment(date_begin).format('YYYY-MM-DD'))
    }

    if(date_end) {
      params = params.set('query[date][lte]', moment(date_end).format('YYYY-MM-DD'))
    }

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    } else {
      params = params.set('sort', '-id');
    }

    return this.resourceService.list<MenuModel>(this.urlService.get('FOOD.MEALS') + '?query[service_id]=' + service_id,
      { params, headers: this.getHeaders() }).pipe(first());
  }

  get(id: number): Observable<Resource<MenuModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'dishs,dishstype');
    return this.resourceService.list<MenuModel>(this.urlService.get('FOOD.MEALS_ID', {id}), { params, headers: this.getHeaders()}).pipe(first());
  }

  create(value: any): Observable<Resource<MenuModel>> {
    return this.resourceService
      .create<MenuModel>(this.urlService.get('FOOD.MEALS', { entity_id : this.selectedEntity.id }), value, { headers: this.getHeaders()})
      .pipe(first());
  }

  update(id: number, value: any): Observable<Resource<MenuModel>> {
    return this.resourceService
      .update<MenuModel>(
        this.urlService.get('FOOD.MEALS_ID', { id }),
        value, { headers: this.getHeaders()}
      )
      .pipe(first());
  }

  delete(id: number): Observable<any> {
    return this.resourceService
      .delete(this.urlService.get('MEALS_ID', { entity_id : this.selectedEntity.id, id }), { headers: this.getHeaders()})
      .pipe(first());
  }

  accept(id: number): Observable<Resource<MenuModel>> {
    return this.resourceService
      .create<MenuModel>(this.urlService.get('FOOD.MEALS_ID_ACCEPT', { id, entity_id : this.selectedEntity.id }), {}, { headers: this.getHeaders()})
      .pipe(first());
  }

  reject(id: number): Observable<Resource<MenuModel>> {
    return this.resourceService
      .create<MenuModel>(this.urlService.get('FOOD.MEALS_ID_REJECT', { id, entity_id : this.selectedEntity.id }), {}, { headers: this.getHeaders()})
      .pipe(first());
  }

  createBulk(value: MenuBulkDto): Observable<Resource<MenuModel>> {
      return this.resourceService
        .create<MenuModel>(this.urlService.get('FOOD.MEALS_BULK', { entity_id : this.selectedEntity.id }), value, { headers: this.getHeaders()})
        .pipe(first());
  }

  copyMenu(id: number, value: CopyMenuDto): Observable<Resource<MenuModel>> {
    return this.resourceService
        .create<MenuModel>(this.urlService.get('FOOD.MEALS_ID_COPY', { id, entity_id : this.selectedEntity.id }), value, { headers: this.getHeaders()})
        .pipe(first());
  }

  copyMenuDay( value: CopyDayDto): Observable<Resource<MenuModel>> {
    return this.resourceService
        .create<MenuModel>(this.urlService.get('FOOD.MEALS_COPY_DAY', { entity_id : this.selectedEntity.id }), value, { headers: this.getHeaders()})
        .pipe(first());
  }

  copyMenuWeek( value: CopyWeekDto): Observable<Resource<MenuModel>> {
    return this.resourceService
        .create<MenuModel>(this.urlService.get('FOOD.MEALS_COPY_WEEK', { entity_id : this.selectedEntity.id }), value, { headers: this.getHeaders()})
        .pipe(first());
  }
}
