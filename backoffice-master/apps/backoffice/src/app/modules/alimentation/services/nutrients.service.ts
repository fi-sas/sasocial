import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { NutrientModel } from '@fi-sas/backoffice/modules/alimentation/models/nutrient.model';

@Injectable({
  providedIn: 'root'
})
export class NutrientsService extends Repository<NutrientModel> {


  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.NUTRIENTS';
    this.entity_url = 'FOOD.NUTRIENTS_ID';
  }


  create(data: NutrientModel): Observable<Resource<NutrientModel>> {
    const url = this.urlService.get(this.entities_url);
    return this.resourceService.create(url, data);
  }

  update(id: number, data: NutrientModel): Observable<Resource<NutrientModel>> {
    const url = this.urlService.get(this.entity_url, { id });
    return this.resourceService.update(url, data);
  }

  getNutrientById(id: number): Observable<Resource<NutrientModel>> {
    return this.resourceService.list(this.urlService.get(this.entity_url, { id }));
  }

  desactive(id: number, data: NutrientModel): Observable<Resource<NutrientModel>> {
    return this.resourceService.patch<NutrientModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: NutrientModel): Observable<Resource<NutrientModel>> {
    return this.resourceService.patch<NutrientModel>(this.urlService.get(this.entity_url, { id }), data);
  }


}
