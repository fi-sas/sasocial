import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Repository } from './../../../shared/repository/repository.class';
import { Observable, of } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { OrdersModel } from '../models/order.model';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';

export class OrdersService extends Repository<OrdersModel> {
  selectedEntity: FiscalEntityModel;

  constructor(resourceService: FiResourceService, urlService: FiUrlService,
    private alimentationService: AlimentationService) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.ORDERS';
    this.entity_url = 'FOOD.ORDERS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  served(id: number, user_id: number): Observable<Resource<OrdersModel>> {
    return this.resourceService.create<OrdersModel>(this.urlService.get('FOOD.ORDERS_ID_SERVED', { id }), {});
  }

  unServed(id: number): Observable<Resource<OrdersModel>>{
    return this.resourceService.create<OrdersModel>(this.urlService.get('FOOD.ORDERS_ID_NOT_SERVED', { id }), {});
  }

  generateProductRevenueReport(query: {
    start_date: Date,
    end_date: Date,
    service_id: number[]
  }) {
    let params = new HttpParams().append('start_date', query.start_date.toISOString()).append('end_date', query.end_date.toISOString());

    if(query.service_id)
      query.service_id.forEach(id => {
        params = params.append('service_id[]', id.toString())
      })

    return this.resourceService.read<OrdersModel>(this.urlService.get('FOOD.ORDERS_PRODUCTS_REVENUE_REPORT', {}), {
      params,
      headers: this.getHeaders(),
    });
  }

  generateRevenueReport(query: {
    start_date: Date,
    end_date: Date,
    service_id: number[]
  }) {
    let params = new HttpParams().append('start_date', query.start_date.toISOString()).append('end_date', query.end_date.toISOString());

    if(query.service_id)
      query.service_id.forEach(id => {
        params = params.append('service_id[]', id.toString())
      })


    return this.resourceService.read<OrdersModel>(this.urlService.get('FOOD.ORDERS_REVENUE_REPORT', {}), {
      params,
      headers: this.getHeaders(),
    });
  }

}
