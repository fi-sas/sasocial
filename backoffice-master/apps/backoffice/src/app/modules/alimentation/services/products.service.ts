import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProductModel } from '@fi-sas/backoffice/modules/alimentation/models/product.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends Repository<ProductModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.PRODUCTS';
    this.entity_url = 'FOOD.PRODUCTS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }


  getAllergens(product_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_ALLERGENS', { entity_id: this.selectedEntity.id, id: product_id });
    return this.resourceService.list(url, { headers: this.getHeaders()}).pipe(first());
  }

  updateAllergens(product_id: number, allergens: any): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_ALLERGENS', { entity_id: this.selectedEntity.id, id: product_id });
    return this.resourceService.patch(url, {allergens: allergens}, { headers: this.getHeaders()}).pipe(first());
  }

  filterById(ids: number[]): Observable<any> {
    let params = new HttpParams();
    for (const id of ids) {
      params = params.append('query[id]', id + '');
    }
    return this.resourceService
      .list<ProductModel[]>(this.urlService.get('FOOD.PRODUCTS'), { params, headers: this.getHeaders() });
  }

  getNutrients(product_id: number, params?: {} ): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_NUTRIENTS', { id: product_id });
    return this.resourceService.list(url, { headers: this.getHeaders()} ).pipe(first());
  }

  updateNutrients(product_id: number, nutrients: any): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_NUTRIENTS', {id: product_id });
    return this.resourceService.patch(url, { nutrients : nutrients}, { headers: this.getHeaders()}).pipe(first());
  }

  getComplements(product_id: number, params?: {} ): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_COMPLEMENTS', { id: product_id });
    return this.resourceService.list(url,{ headers: this.getHeaders()}).pipe(first());
  }

  updateComplements(product_id: number, complements: any): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_COMPLEMENTS', { id: product_id });
    return this.resourceService.patch(url, {complements :complements } , { headers: this.getHeaders()}).pipe(first());
  }

  getComposition(product_id: number, params?: {} ): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_COMPOSITION', { id: product_id });
    return this.resourceService.list(url, { headers: this.getHeaders()} ).pipe(first());
  }

  updateComposition(product_id: number, composition: any): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_COMPOSITION', {id: product_id });
    return this.resourceService.patch(url, {composition : composition}, { headers: this.getHeaders()}).pipe(first());
  }

  getPrices(product_id: number, params?: {} ): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_PRICES', { entity_id: this.selectedEntity.id, id: product_id });
    return this.resourceService.list(url, { headers: this.getHeaders(), params: this.getQuery(null,null,null,null,params) }).pipe(first());
  }

  getProductPrices(product_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_PRICES', { entity_id: this.selectedEntity.id, id: product_id });
    return this.resourceService.list(url, { headers: this.getHeaders()} ).pipe(first());
  }

  updatePrices(product_id: number, prices: any): Observable<Resource<any>> {
    const url = this.urlService.get('FOOD.PRODUCTS_ID_PRICES', { id: product_id });
    return this.resourceService.patch(url, {prices : prices }, { headers: this.getHeaders()}).pipe(first());
  }

  desactive(id: number, data: ProductModel): Observable<Resource<ProductModel>> {
    return this.resourceService.patch<ProductModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: ProductModel): Observable<Resource<ProductModel>> {
    return this.resourceService.patch<ProductModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

}
