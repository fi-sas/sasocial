import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { RecipeModel } from '@fi-sas/backoffice/modules/alimentation/models/recipe.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class RecipesService extends Repository<RecipeModel> {

  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.RECIPES';
    this.entity_url = 'FOOD.RECIPES_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  desactive(id: number, data: RecipeModel): Observable<Resource<RecipeModel>> {
    return this.resourceService.patch<RecipeModel>(this.urlService.get(this.entity_url, { id }), { active: false, number_doses: data.number_doses},{ headers: this.getHeaders()});
  }

  active(id: number, data: RecipeModel): Observable<Resource<RecipeModel>> {
    return this.resourceService.patch<RecipeModel>(this.urlService.get(this.entity_url, { id }), { active : true, number_doses: data.number_doses}, { headers: this.getHeaders()});
  }

}
