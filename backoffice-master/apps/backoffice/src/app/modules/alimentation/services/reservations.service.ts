import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { Injectable } from '@angular/core';
import { ReservationModel } from '../models/reservation.model';
import { Repository } from './../../../shared/repository/repository.class';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { Meal } from '../models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService extends Repository<ReservationModel> {


  selectedEntity: FiscalEntityModel;

  constructor(resourceService: FiResourceService, urlService: FiUrlService,
    private alimentationService: AlimentationService) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.ALI_RESERVATIONS';
    this.entity_url = 'FOOD.ALI_RESERVATIONS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  count(params: {}) {
    const url = this.urlService.get('ALI_RESERVATIONS_COUNT', { ...this.persistentUrlParams });
    return this.resourceService.list(url, { params });
  }

  served(id: number): Observable<Resource<ReservationModel>> {
    return this.resourceService.create<ReservationModel>(this.urlService.get('FOOD.RESERVATION_SERVED', { id }), {}, { headers: this.getHeaders()} );
  }

  unServed(id: number): Observable<Resource<ReservationModel>>{
    return this.resourceService.delete(this.urlService.get('FOOD.RESERVATION_UNSERVED', { id }) );
  }

  cancel(id: number): Observable<Resource<ReservationModel>>{
    return this.resourceService.create(this.urlService.get('FOOD.RESERVATION_CANCEL', { id }), {});
  }

  getAllCount(date: Date) {
    const url = this.urlService.get('FOOD.RESERVATIONS_COUNT_ALL', {});
    return this.resourceService.list(url, {
      headers: this.getHeaders(),
      params: new HttpParams().append('date', date.toISOString())
    });
  }

  reservationsReport(query: {
    start_date: Date,
    end_date: Date,
    service_id: number[],
    dish_type_id: number[],
    meal: Meal,
  }) {
    let params = new HttpParams().append('start_date', moment(query.start_date).format('YYYY-MM-DD')).append('end_date', moment(query.end_date).format('YYYY-MM-DD'));

    if(query.service_id) {
      query.service_id.map(id => {
        params = params.append('service_id[]', id.toString());
      });
    }

    if(query.dish_type_id) {
      query.dish_type_id.map(id => {
        params = params.append('dish_type_id[]', id.toString());
      });
    }

    if(query.meal) {
      params = params.append('meal', query.meal);
    }

    return this.resourceService.read(this.urlService.get('FOOD.RESERVATION_REPORT'), {
      params,
      headers: this.getHeaders()
    });
  }

  reservationsRevenueReport(query: {
    start_date: Date,
    end_date: Date,
    service_id: number[],
  }) {
    let params = new HttpParams().append('start_date', query.start_date.toISOString()).append('end_date', query.end_date.toISOString());

    if(query.service_id) {
      query.service_id.map(id => {
        params = params.append('service_id[]', id.toString());
      });
    }

    return this.resourceService.read(this.urlService.get('FOOD.RESERVATION_REVENUE_REPORT'), {
      params,
      headers: this.getHeaders()
    });
  }

}
