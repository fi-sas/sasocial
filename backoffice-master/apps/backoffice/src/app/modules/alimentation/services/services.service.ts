import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/alimentation/models/service.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService extends Repository<ServiceModel> {


  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.SERVICES';
    this.entity_url = 'FOOD.SERVICES_ID';
    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  // getServiceById(id: number): Observable<Resource<ServiceModel>> {
  //   return this.resourceService.list(this.urlService.get(this.entity_url, { id }), { headers: this.getHeaders()});
  // }

  desactive(id: number, data: any): Observable<Resource<ServiceModel>> {
    return this.resourceService.patch<ServiceModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: any): Observable<Resource<ServiceModel>> {
    return this.resourceService.patch<ServiceModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

}
