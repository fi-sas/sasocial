import { TestBed } from '@angular/core/testing';
import { StocksGroupService } from './stock-group.service';


describe('StocksGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StocksGroupService = TestBed.get(StocksGroupService);
    expect(service).toBeTruthy();
  });
});
