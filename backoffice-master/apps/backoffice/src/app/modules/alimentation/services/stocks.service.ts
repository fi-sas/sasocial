import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { StockModel } from '../models/stock.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StocksService extends Repository<StockModel> {


  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.STOCKS';
    this.entity_url = 'FOOD.STOCKS_ID';
    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  getLotesByProductWharehouse(id_prod, id_whare) {
    return this.resourceService.list<StockModel>(this.urlService.get('FOOD.LOTES',{ id_prod: id_prod, id_whare: id_whare }),  { headers: this.getHeaders()});
  }

  inventoryReport(wharehouseId: number, stock): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.set('wharehouse_id', wharehouseId.toString());
    if(stock) {
      params = params.set('minimal_stock','true');
    }
   
    return this.resourceService.list<any>(this.urlService.get('FOOD.STOCKS_INVENTORY_REPORT'),  { headers: this.getHeaders(), params});
  }
}
