import { Repository } from './../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { UnitModel } from '@fi-sas/backoffice/modules/alimentation/models/unit.model';

@Injectable({
  providedIn: 'root'
})
export class UnitsService extends Repository<UnitModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.UNITS';
    this.entity_url = 'FOOD.UNITS_ID';
  }


  create(data: UnitModel): Observable<Resource<UnitModel>> {
    const url = this.urlService.get(this.entities_url);
    return this.resourceService.create(url, data);
  }

  getUnitById(id: number): Observable<Resource<UnitModel>> {
    return this.resourceService.list(this.urlService.get(this.entity_url, { id }));
  }

  update(id: number, data: UnitModel): Observable<Resource<UnitModel>> {
    const url = this.urlService.get(this.entity_url, { id });
    return this.resourceService.update(url, data);
  }

  desactive(id: number, data: UnitModel): Observable<Resource<UnitModel>> {
    return this.resourceService.patch<UnitModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: UnitModel): Observable<Resource<UnitModel>> {
    return this.resourceService.patch<UnitModel>(this.urlService.get(this.entity_url, { id }), data);
  }
  
}
