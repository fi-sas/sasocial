import { TestBed } from '@angular/core/testing';

import { UserPacksService } from './user-packs.service';

describe('UserPacksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserPacksService = TestBed.get(UserPacksService);
    expect(service).toBeTruthy();
  });
});
