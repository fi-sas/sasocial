import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { FiscalEntityModel } from '../models/fiscal-entity.model';
import { UserPackModel } from '../pages/packs/models/user-pack.model';
import { AlimentationService } from './alimentation.service';

@Injectable({
  providedIn: 'root'
})
export class UserPacksService extends Repository<UserPackModel> {

  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.USERS_PACKS';
    this.entity_url = 'FOOD.USERS_PACKS_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if (this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  bulk(entity: UserPackModel): Observable<Resource<UserPackModel>> {
    return this.resourceService
      .create<UserPackModel>(
        this.urlService.get("FOOD.USERS_PACKS_BULK", { ...this.persistentUrlParams }),
        entity,
        {
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

  desactivate(id: number): Observable<Resource<UserPackModel>> {
    return this.resourceService
      .create<UserPackModel>(
        this.urlService.get("FOOD.USERS_PACKS_ID_DESACTIVATE", { id }),
        {},
        {
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

  activate(id: number): Observable<Resource<UserPackModel>> {
    return this.resourceService
      .create<UserPackModel>(
        this.urlService.get("FOOD.USERS_PACKS_ID_ACTIVATE", { id }),
        {},
        {
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

}
