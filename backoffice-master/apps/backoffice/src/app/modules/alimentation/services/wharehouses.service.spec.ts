import { TestBed, inject } from '@angular/core/testing';

import { WharehousesService } from './wharehouses.service';

describe('WharehousesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WharehousesService]
    });
  });

  it('should be created', inject([WharehousesService], (service: WharehousesService) => {
    expect(service).toBeTruthy();
  }));
});
