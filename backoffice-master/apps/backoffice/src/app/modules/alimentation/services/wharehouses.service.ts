import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AlimentationService } from '@fi-sas/backoffice/modules/alimentation/services/alimentation.service';
import { FiscalEntityModel } from '@fi-sas/backoffice/modules/alimentation/models/fiscal-entity.model';
import { WharehouseModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { WharehouseOperationModel } from '@fi-sas/backoffice/modules/alimentation/models/wharehouse-operation.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class WharehousesService extends Repository<WharehouseModel> {
  selectedEntity: FiscalEntityModel;

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService,
    private alimentationService: AlimentationService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'FOOD.WHAREHOUSES';
    this.entity_url = 'FOOD.WHAREHOUSES_ID';

    this.selectedEntity = this.alimentationService.getSelectedEntity();
    this.alimentationService.seletedEntityObservable().subscribe(entity => {
      this.selectedEntity = entity;
      if(this.selectedEntity) {
        this.persistentHeaders = {
          'x-alimentation-entity-id': entity.id
        }
      }
    });
  }

  listOperations(wharehouse_id: number, pageIndex: number, pageSize: number): Observable<Resource<WharehouseOperationModel>> {
    let params = new HttpParams();
    params = params.set('offset', Math.abs((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    return this.resourceService.list<WharehouseOperationModel>(this.urlService.get('FOOD.GETUSERSINFO', { entity_id : this.selectedEntity.id,  wharehouse_id }), { params }).pipe(first());
  }

  listUsers(wharehouse_id: number): Observable<Resource<UserModel>> {
    return this.resourceService.list<UserModel>(this.urlService.get('FOOD.WHAREHOUSES_ID_USERS', { id : wharehouse_id }), { headers: this.getHeaders()} ).pipe(first());
  }

  addUser(wharehouse_id: number, user_id: number): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(this.urlService.get('FOOD.WHAREHOUSES_ID_USERS_ID', { id : wharehouse_id , user_id : user_id  }), {}, { headers: this.getHeaders()}).pipe(first());
  }

  removeUser(wharehouse_id: number, user_id: number): Observable<any> {
    return this.resourceService.delete(this.urlService.get('FOOD.WHAREHOUSES_ID_USERS_ID', { id : wharehouse_id , user_id : user_id  }), { headers: this.getHeaders()} ).pipe(first());
  }

  desactive(id: number, data: any): Observable<Resource<WharehouseModel>> {
    return this.resourceService.patch<WharehouseModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: any): Observable<Resource<WharehouseModel>> {
    return this.resourceService.patch<WharehouseModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  getAccessUser(): Observable<Resource<WharehouseModel>> {
    let params = new HttpParams();
    params = params.set('sort', 'name');
    params = params.set('active', 'true');
    return this.resourceService.list<WharehouseModel>(this.urlService.get('FOOD.WHAREHOUSES_ACCESS'),  { headers: this.getHeaders(), params});
  }
}
