import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { BusComponent } from '@fi-sas/backoffice/modules/bus/bus.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { ConfigurationsComponent } from './pages/configurations/configurations.component';




const routes: Routes = [
  {
    path: '',
    component: BusComponent,
    children: [
      {
        path: 'locals',
        loadChildren: './pages/local/local.module#LocalBusModule',
        data: { breadcrumb: 'Locais', title: 'Locais' },
      },
      {
        path: 'list',
        loadChildren: './pages/bus-applications/bus-applications.module#BusApplicationModule',
        data: { breadcrumb: 'Listar', title: 'Listar' },
      },
      {
        path: 'bus-sub23-declarations',
        loadChildren: './pages/bus-sub23-declarations/bus-sub23-declarations.module#BusSub23DeclarationsModule',
        data: { breadcrumb: 'Declarações Sub-23', title: 'Declarações Sub-23' },
      },
      {
        path: 'reports',
        loadChildren: './pages/reports/reports-bus.module#ReportsBusModule',
        data: { breadcrumb: 'Relatórios' , title: 'Relatórios' },
      },
      {
        path: 'routes',
        loadChildren: './pages/route/route.module#RouteBusModule',
        data: { breadcrumb: 'Routes', title: 'Routes' },
      },
      {
        path: 'season',
        loadChildren: './pages/season/season.module#SeasonBusModule',
        data: { breadcrumb: 'Épocas', title: 'Épocas' },
      },
      {
        path: 'type-day',
        loadChildren: './pages/type-day/type-day.module#TypeDayBusModule',
        data: { breadcrumb: 'Tipo de dias', title: 'Tipo de dias' },
      },
      {
        path: 'tickets',
        loadChildren: './pages/tickets/tickets.module#TicketsBusModule',
        data: { breadcrumb: 'Bilhetes', title: 'Bilhetes' },
      },

      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'configurations',
        component: ConfigurationsComponent,
        data: { breadcrumb: 'Configurações Gerais', title: 'Configurações Gerais', scope: 'bus:configurations:read' },
      },

      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusRoutingModule { }
