import { Component, OnInit } from '@angular/core';

import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.less']
})
export class BusComponent implements OnInit {
  dataConfiguration: any;
  constructor(private configurationsService: ConfigurationGeralService, private uiService: UiService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(3), 'car');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const locals = new SiderItem('Locais', '', '', 'bus:locals');
    locals.addChild(new SiderItem('Criar', '', '/bus/locals/create', 'bus:locals:create'));
    locals.addChild(new SiderItem('Listar', '', '/bus/locals/list', 'bus:locals:read'));
    this.uiService.addSiderItem(locals);

    const applications = new SiderItem('Candidaturas', '', '/bus/list/applications', 'bus:applications:read');
    this.uiService.addSiderItem(applications);

    const withdrawals = new SiderItem('Desistencias', '', '/bus/list/withdrawals', 'bus:applications:read');
    this.uiService.addSiderItem(withdrawals);

    const absences = new SiderItem('Pedidos de suspensão', '', '/bus/list/absences', 'bus:applications:read');
    this.uiService.addSiderItem(absences);

    const sub23Applications = new SiderItem('Declarações Sub-23', '', '', 'bus:sub23_declarations');
    sub23Applications.addChild(new SiderItem('Listar', '', '/bus/bus-sub23-declarations', 'bus:sub23_declarations:read'));
    this.uiService.addSiderItem(sub23Applications);


    const routes = new SiderItem('Rotas', '', '', 'bus:routes');
    routes.addChild(new SiderItem('Criar', '', '/bus/routes/create', 'bus:routes:create'));
    routes.addChild(new SiderItem('Listar', '', '/bus/routes/list', 'bus:routes:read'));
    routes.addChild(new SiderItem('Criar Ligação', '', '/bus/routes/link/create', 'bus:links:create'));
    this.uiService.addSiderItem(routes);

    const types_days = new SiderItem('Tipos de dias', '', '', 'bus:types_days');
    types_days.addChild(new SiderItem('Criar', '', '/bus/type-day/create', 'bus:types_days:create'));
    types_days.addChild(new SiderItem('Listar', '', '/bus/type-day/list', 'bus:types_days:read'));
    this.uiService.addSiderItem(types_days);

    const seasons = new SiderItem('Épocas', '', '', 'bus:seasons');
    seasons.addChild(new SiderItem('Criar', '', '/bus/season/create', 'bus:seasons:create'));
    seasons.addChild(new SiderItem('Listar', '', '/bus/season/list', 'bus:seasons:read'));
    this.uiService.addSiderItem(seasons);

    const tickets = new SiderItem('Bilhetes', '', '', 'bus:tickets_bought');
    tickets.addChild(new SiderItem('Listar', '', '/bus/tickets/list', 'bus:tickets_bought:read'));
    this.uiService.addSiderItem(tickets);

    const configurations = new SiderItem('Configurações gerais', '', '/bus/configurations', 'bus:configurations');
    this.uiService.addSiderItem(configurations);
    this.uiService.setSiderActive(true);


    const reports = new SiderItem('Relatórios', "", "/bus/reports", "bus:applications");
    this.uiService.addSiderItem(reports);


  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
