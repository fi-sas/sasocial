import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusComponent } from './bus.component';
import { BusRoutingModule } from '@fi-sas/backoffice/modules/bus/bus-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { ConfigurationsComponent } from './pages/configurations/configurations.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BusRoutingModule,
    AngularOpenlayersModule,
  ],
  declarations: [BusComponent, ConfigurationsComponent],

})
export class BusModule { }
