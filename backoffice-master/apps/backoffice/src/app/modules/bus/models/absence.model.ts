import { FileModel } from "../../medias/models/file.model";
import { BusApplicationModel } from "./bus_application.model";

export class AbsenceModel {
    id?: number;
    application_id?: number;
    user_id?: number;
    start_date: string;
    end_date: string;
    reason: string;
    file_id: number;
    status: string;
    updated_at?: string;
    created_at?: string;
    application?: BusApplicationModel;
    file?: FileModel;
    justification: string;
}

export const AbsenceStatusTranslations = {
    submitted: { label: 'Submetido', color: 'blue' },
    approved: { label: 'Aprovado', color: 'green' },
    rejected: { label: 'Rejeitado', color: 'red' },
    ongoing: { label: 'A decorrer', color: 'yellow' },
    completed: { label: 'Concluído', color: 'green' },
};
