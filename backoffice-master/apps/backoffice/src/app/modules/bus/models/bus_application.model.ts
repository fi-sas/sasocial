import { LocalModel } from './locals.model';
import { CourseModel } from './../../configurations/models/course.model';
import { WithdrawalModel } from '../../accommodation/modules/withdrawals/models/withdrawal.model';
import { SchoolModel } from '../../configurations/models/school.model';
import { FileModel } from '../../medias/models/file.model';

export class BusApplicationModel {
  'name': string;
  'student_number': string;
  'birth_date': string;
  'tin': string;
  'identification': string;
  'nationality': string;
  'date': string;
  'email': string;
  'phone_1': string;
  'phone_2': string;
  'course_year': number;
  'course_id': number;
  'renovation': boolean;
  'organic_unit_id': number;
  'observations': string;
  'accept_procedure': boolean;
  'morning_local_id_from': number;
  'morning_local_id_to': number;
  'afternoon_local_id_from': number;
  'afternoon_local_id_to': number;
  'user_id': number;
  'updated_at': string;
  'created_at': string;
  'status': string;
  'id': number;
  'course': CourseModel;
  'school': SchoolModel;
  'morning_local_from': LocalModel;
  'morning_local_to': LocalModel;
  'afternoon_local_from': LocalModel;
  'afternoon_local_to': LocalModel;
  withdrawal?: WithdrawalModel;
  declaration_file_id: number;
  declaration?: FileModel;
}

export const BusApplicationTagResult = {
  submitted: { color: 'blue', label: 'Submetida' },
  approved: { color: 'green', label: 'Aprovada' },
  not_approved: { color: 'red', label: 'Rejeitada' },
  accepted: { color: 'green', label: 'Aceite pelo utilizador'},
  rejected: { color: 'red', label: 'Rejeitada pelo utilizador'},
  canceled: { color: 'gray', label: 'Cancelada' },
  withdrawal: { color: 'red', label: 'Desistiu' },
  quiting: { color: 'red', label: 'Em desistência' },
  closed: { color: 'gray', label: 'Fechada' },
  suspended: { color: 'orange', label: 'Suspensa' },
};
