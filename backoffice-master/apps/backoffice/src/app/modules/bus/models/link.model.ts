export class LinkModel {
  local_route1_id: number;
  local_route2_id: number;
  route1_id: number;
  route2_id: number;
  active: boolean;
  updated_at: string;
  created_at: string;
}


