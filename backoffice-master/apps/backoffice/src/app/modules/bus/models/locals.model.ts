
export class LocalModel {
  id: number;
  latitude: number;
  longitude: number;
  local: string;
  region: string;
  active: boolean;
  institute: boolean;
  created_at: string;
  updated_at: string;
}
