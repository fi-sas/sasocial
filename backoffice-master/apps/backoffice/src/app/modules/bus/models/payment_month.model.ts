export class PaymentMonthModel {
  "id": number;
  "application_id": number;
  "month": string;
  "year": number;
  "value": number;
  "paid": boolean;
  "created_at": string;
  "updated_at": string;
}

