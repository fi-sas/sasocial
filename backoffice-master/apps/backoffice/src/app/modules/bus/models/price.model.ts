export class PriceModel {
  id: number;
  value
  price_table_id: number;
  description: string;
  number_stop: number;
}
