import { PriceModel } from '@fi-sas/backoffice/modules/bus/models/price.model';

export class PricesTablesModel {
  route_id: number;
  ticket_type: string;
  tax_id: number;
  profile_id: number;
  account_id: number;
  currency_id: number;
  active: boolean;
  prices: PriceModel[];
}

export class ValueModel {
  value: number;
}


