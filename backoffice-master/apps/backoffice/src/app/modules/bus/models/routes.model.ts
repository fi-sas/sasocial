import { LinkModel } from "./link.model";

export class RouteModel {

  id: number;
  name: string;
  external: boolean;
  represents_connection: boolean;
  active: boolean;
  contact_id: number;
  locals: Local[];
  links?: LinkModel[]
}

class Local {

  id: number;
  local: string;
  region: string;
  instruction: string;
  distance: number
}

export class LinkInfoModel {

  id: number;
  route1_id: number;
  local_route1_id: number;
  route2_id: number;
  local_route2_id: number;
  route1: string;
  region1: string;
  local1: string;
  route2: string;
  region2: string;
  local2: string;
}

