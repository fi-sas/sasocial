
export class SeasonModel {
  id: number;
  name: string;
  active: boolean;
  date_begin : string;
  date_end: string;
  updated_at: string;
  created_at: string;
}

