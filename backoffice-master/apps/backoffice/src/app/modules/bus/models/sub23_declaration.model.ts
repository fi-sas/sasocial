import { CourseModel } from "../../configurations/models/course.model";
import { OrganicUnitsModel } from "../../infrastructure/models/organic-units.model";
import { FileModel } from "../../medias/models/file.model";
import { DocumentTypeModel } from "../../users/modules/document-type/models/document-type.model";
import { UserModel } from "../../users/modules/users_users/models/user.model";

export class Sub23BusDeclarationModel {
  id: number;
  academic_year: string;
  name: string;
  address: string;
  postal_code: string;
  locality: string;
  country: string;
  document_type_id: number;
  identification: string;
  cc_emitted_in: string;
  birth_date: boolean;
  organic_unit_id: number;
  course_id: number;
  course_year: number;
  has_social_scholarship: boolean;
  applied_social_scholarship: boolean;
  status: string;
  user_id: number;
  created_at: string;
  updated_at: string;
  course: CourseModel;
  user: UserModel;
  organic_unit: OrganicUnitsModel;
  document_type: DocumentTypeModel;
  declaration_id: number;
  declaration: FileModel;
  observations: string;
}

export const Sub23BusDeclarationTagResult = {
  submitted: { color: 'blue', label: 'Submetida' },
  in_validation: { color: 'blue', label: "Em Validação" },
  validated: { color: 'gray', label: "Validada" },
  emitted: { color: 'green', label: 'Emitida' },
  rejected: { color: 'red', label: 'Rejeitada' }
};
