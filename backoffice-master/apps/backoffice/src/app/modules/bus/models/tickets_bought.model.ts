import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { LocalModel } from '@fi-sas/backoffice/modules/bus/models/locals.model';
import { RouteModel } from '@fi-sas/backoffice/modules/bus/models/routes.model';
import { PriceModel } from '@fi-sas/backoffice/modules/bus/models/price.model';

export class TicketsBoughtModel {
  id: number;
  route_id: number;
  validate_date: string;
  used: boolean;
  price_id: number;
  departure_local_id: number;
  arrival_local_id: number;
  user_id: number;
  route?: RouteModel;
  price?: PriceModel;
  departure_local?: LocalModel;
  arrival_local?: LocalModel;
  user?: UserModel;
  updated_at: string;
  created_at: string;
}


