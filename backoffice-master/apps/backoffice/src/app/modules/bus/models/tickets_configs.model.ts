export class TicketsConfigsModel {
  id?: number;
  route_id?: number;
  max_to_buy : number;
  active: boolean;
  created_at?: string;
  updated_at?: string;
}
