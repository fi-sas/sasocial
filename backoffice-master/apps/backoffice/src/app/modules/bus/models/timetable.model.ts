import { LocalModel } from '@fi-sas/backoffice/modules/bus/models/locals.model';

export class TimetableModel {
  id: number;
  route_id: number;
  active: boolean;
  type_day: string;
  type_day_id: number;
  season_id: number;
  hours: Array<Hours[]>;
}

class Hours {
  id: number;
  hour: string;
  local_id: number;
  timetable_id: number;
  route_order_id: number;
  line_number: number;
  created_at: string;
  updated_at: string;
  local: LocalModel
}

class Hour {
  hour_id: number;
  hour: string;
  line_number: number;
}

export class TimetableInsertModel {
  route_id: number;
  type_day_id: number;
  season_id: number;
  active: boolean;
  hours: string[];
}

export class HourUpdate {
  id: number;
  hour: string;
}



