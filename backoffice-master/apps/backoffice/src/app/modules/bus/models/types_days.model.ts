
export class TypesDaysModel {
  id: number;
  name: string;
  days_week_ids: number[];
  days_weeks: DaysWeeks[];
  updated_at: string;
  created_at: string;
}

class DaysWeeks {
  id: number;
  day: string;
}

export class DaysWeeksModel {
  id: number;
  day: string;
}

export class DayGroup {
   type_day_id: number;
   day_id: number;
}
