
export enum WithdrawalStatus {
    PENDING = "pending",
    ACCEPTED = "accepted",
    REJECTED = "rejected"
};

export const WithdrawalStatusTranslations = {
    pending: { label: 'Espera de aprovação', color: 'blue' },
    accepted: { label: 'Aprovada', color: 'green' },
    rejected: { label: 'Rejeitada', color: 'red' },
};

export class WithdrawalModel {
    application_id: number;
    date_withdrawal: Date;
    id: number;
    justification: string;
    status: WithdrawalStatus;
    created_at: Date;
    updated_at: Date;
}