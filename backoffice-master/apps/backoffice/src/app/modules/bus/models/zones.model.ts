import { LocalModel } from '@fi-sas/backoffice/modules/bus/models/locals.model';
export class ZoneModel {
  id: number;
  name: string;
  route_id: number;
  active: boolean;
  locals: LocalModel[];
  updated_at: string;
  created_at: string;
  locals_ids: number[];
}
export class ZoneInsert {
  name: string;
  route_id: number;
  active: boolean;
  locals_ids: number[]
}
