import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAbsencesComponent } from './pages/list-absences/list-absences.component';
import { ListBusApplicationsComponent } from './pages/list-bus-applications/list-bus-applications.component';
import { ListWithdrawalsComponent } from './pages/list-withdrawals/list-withdrawals.component';

const routes: Routes = [
  { path: '', redirectTo: 'applications', pathMatch: 'full' },
  {
    path: 'applications',
    component: ListBusApplicationsComponent,
    data: { breadcrumb: 'Candidaturas', title: 'Candidaturas', scope: 'bus:applications:read'},
  },
  {
    path: 'withdrawals',
    component: ListWithdrawalsComponent,
    data: { breadcrumb: 'Desistências', title: 'Desistências', scope: 'bus:applications:read'},
  },
  {
    path: 'absences',
    component: ListAbsencesComponent,
    data: { breadcrumb: 'Pedidos de suspensão', title: 'Pedidos de suspensão', scope: 'bus:applications:read'},
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListApplicationsBusRoutingModule { }