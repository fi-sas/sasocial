

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { ListApplicationsBusRoutingModule } from './bus-applications-routing.module';
import { ListBusApplicationsComponent } from './pages/list-bus-applications/list-bus-applications.component';
import { ViewBusApplicationComponent } from './components/view-bus-application/view-bus-application.component';
import { ModelPaymentMonthComponent } from './components/model-payment-month/model-payment-month.component';
import { ListWithdrawalsComponent } from './pages/list-withdrawals/list-withdrawals.component';
import { ListAbsencesComponent } from './pages/list-absences/list-absences.component';

@NgModule({
  declarations: [
    ListBusApplicationsComponent,
    ViewBusApplicationComponent,
    ModelPaymentMonthComponent,
    ListWithdrawalsComponent,
    ListAbsencesComponent
  ],
  imports: [
    CommonModule,
    AngularOpenlayersModule,
    SharedModule,
    ListApplicationsBusRoutingModule
  ]

})
export class BusApplicationModule { }
