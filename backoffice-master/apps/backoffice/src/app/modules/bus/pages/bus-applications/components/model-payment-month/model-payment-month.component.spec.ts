import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelPaymentMonthComponent } from './model-payment-month.component';

describe('ModelPaymentMonthComponent', () => {
  let component: ModelPaymentMonthComponent;
  let fixture: ComponentFixture<ModelPaymentMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelPaymentMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelPaymentMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
