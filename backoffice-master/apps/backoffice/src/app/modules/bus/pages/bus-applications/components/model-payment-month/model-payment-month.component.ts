import { PaymentMonthModel } from '../../../../models/payment_month.model';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { BusApplicationModel } from '../../../../models/bus_application.model';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { PaymentMonthService } from '../../services/payment-month.service';

@Component({
  selector: 'fi-sas-model-payment-month',
  templateUrl: './model-payment-month.component.html',
  styleUrls: ['./model-payment-month.component.less'],
})
export class ModelPaymentMonthComponent implements OnInit {
  @Input() paymentMonthModalApplication: BusApplicationModel = null;
  @Input() set paymentMonth(payment: PaymentMonthModel) {
    if (payment) {
      const date = moment()
        .set(
          'month',
          parseInt(moment().month(payment.month).format('M'), 10) - 1
        )
        .set('year', payment.year);
      this.value = payment.value;
      this.date = date.format();
      this.paid = payment.paid;
      this.paymentID = payment.id;
    }
  }
  @Output() paymentSubmitted = new EventEmitter<boolean>();

  isPaymentMonthModalVisible = false;
  paymentMonthModalLoading = false;
  date = '';
  value = 0;
  paid = false;
  paymentID = null;

  constructor(
    private uiService: UiService,
    private paymentMonthService: PaymentMonthService
  ) {}

  ngOnInit() {}

  handlePaymentMonthToOpen() {
    this.isPaymentMonthModalVisible = true;
  }

  handlePaymentMonthCancel() {
    this.isPaymentMonthModalVisible = false;
  }

  onChangeDate(date) {
    this.date = date;
  }

  handlePaymentMonthOk() {
    const paymentMonth = new PaymentMonthModel();

    paymentMonth.application_id = this.paymentMonthModalApplication.id;
    paymentMonth.value = this.value;
    paymentMonth.paid = this.paid;
    paymentMonth.month = moment(this.date).format('MMMM');
    paymentMonth.year = parseInt(moment(this.date).format('YYYY'), 10);

    this.paymentMonthModalLoading = true;

    if (this.paymentID === null) {
      this.paymentMonthService
        .create(paymentMonth)
        .pipe(
          first(),
          finalize(() => (this.paymentMonthModalLoading = false))
        )
        .subscribe(
          () => {
            this.uiService.showMessage(
              MessageType.success,
              'Pagamento registado com sucesso.'
            );
            this.paymentSubmitted.emit(true);
            this.handlePaymentMonthCancel();
          },
          () => {
            this.paymentSubmitted.emit(false);
          }
        );
    } else {
      this.paymentMonthService
        .update(this.paymentID, paymentMonth)
        .pipe(
          first(),
          finalize(() => (this.paymentMonthModalLoading = false))
        )
        .subscribe(
          () => {
            this.uiService.showMessage(
              MessageType.success,
              'Pagamento atualizado com sucesso.'
            );
            this.paymentSubmitted.emit(true);
            this.handlePaymentMonthCancel();
          },
          () => {
            this.paymentSubmitted.emit(false);
          }
        );
    }
  }

  disabledPaymentMonthButton(): boolean {
    if (this.value !== null && this.date !== '') {
      return false;
    }
    return true;
  }
}
