import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBusApplicationComponent } from './view-bus-application.component';

describe('ViewBusApplicationComponent', () => {
  let component: ViewBusApplicationComponent;
  let fixture: ComponentFixture<ViewBusApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBusApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBusApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
