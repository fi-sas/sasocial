
import { PaymentMonthModel } from '../../../../models/payment_month.model';
import { BusApplicationModel } from '../../../../models/bus_application.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { AbsenceModel, AbsenceStatusTranslations } from '../../../../models/absence.model';

import { WithdrawalModel, WithdrawalStatusTranslations } from '../../../../models/withdrawal.model';
import { PaymentMonthService } from '../../services/payment-month.service';
import { AbsencesService } from '../../services/absences.service';
import { WithdrawalsService } from '../../services/withdrawals.service';


@Component({
  selector: 'fi-sas-view-bus-application',
  templateUrl: './view-bus-application.component.html',
  styleUrls: ['./view-bus-application.component.less'],
})
export class ViewBusApplicationComponent implements OnInit {
  paymentsMonths: PaymentMonthModel[] = [];
  paymentMonth: PaymentMonthModel = null;
  payments_months_loading = false;


  @ViewChild('paymentModel', null) paymentModel: any;
  @Input() application: BusApplicationModel = null;

  absences: AbsenceModel[] = null;
  absenceStatusLabels = AbsenceStatusTranslations;
  absencesLoading = false;

  withdrawals: WithdrawalModel[] = null;
  withdrawalStatusLabels = WithdrawalStatusTranslations;
  withdrawalLoading = false;

  constructor(
    private paymentMonthService: PaymentMonthService,
    private absencesService: AbsencesService,
    private withdrawalsService: WithdrawalsService
  ) { }

  ngOnInit() { }

  loadPaymentMonths() {
    this.payments_months_loading = true;
    this.paymentMonthService
      .list(-1, 0, 'id', null, {
        'application_id': this.application.id
      })
      .pipe(
        first(),
        finalize(() => (this.payments_months_loading = false))
      )
      .subscribe((results) => {
        this.paymentsMonths = results.data;
      });
  }

  paymentMonthModal(paymentMonth: PaymentMonthModel) {
    this.paymentMonth = paymentMonth;
    this.paymentModel.handlePaymentMonthToOpen();
  }

  paymentSubmitted(submitted: boolean) {
    if (submitted) {
      this.loadPaymentMonths();
    }
  }
  loadAbsences() {
    if (this.absences) return;

    this.absencesLoading = true;
    this.absencesService.list(0, -1, null, null, { application_id: this.application.id }).pipe(first(), finalize(() => this.absencesLoading = false)).subscribe(result => {
      this.absences = result.data;
    });

  }

  loadWithdrawals() {
    if (this.withdrawals) return;

    this.withdrawalLoading = true;
    this.withdrawalsService.list(0, -1, null, null, { application_id: this.application.id }).pipe(first(), finalize(() => this.withdrawalLoading = false)).subscribe(result => {
      this.withdrawals = result.data;
    });

  }
}
