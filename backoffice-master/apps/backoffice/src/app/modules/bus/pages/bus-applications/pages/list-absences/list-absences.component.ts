import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AbsenceModel, AbsenceStatusTranslations } from '../../../../models/absence.model';

import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { AbsencesService } from '../../services/absences.service';

@Component({
  selector: 'fi-sas-list-absences',
  templateUrl: './list-absences.component.html',
  styleUrls: ['./list-absences.component.less']
})
export class ListAbsencesComponent extends TableHelper implements OnInit {

  statusFilter = [
    { label: "Submetido", value: "submitted" },
    { label: "Aprovado", value: "approved" },
    { label: "Rejeitado", value: "rejected" },
    { label: "A decorrer", value: "ongoing" },
    { label: "Concluído", value: "completed" }
  ];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public absencesService: AbsencesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);

    this.columns.push(
      {
        key: 'application.name',
        label: 'Nome',
        sortable: false
      },
      {
        key: 'start_date',
        label: 'Data',
        template: (data: AbsenceModel) => {
          return moment(new Date(data.start_date)).format('DD-MM-YYYY') + " / " + moment(new Date(data.end_date)).format('DD-MM-YYYY');
        },
      },
      {
        key: 'justification',
        label: 'Justificação',
        sortable: true
      },
      {
        key: 'file',
        label: 'Anexo',
        template: (data: AbsenceModel) => {
          return '';
        },
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: AbsenceStatusTranslations
      },
    )
    this.persistentFilters['withRelated'] = 'application,file';
  }
  ngOnInit() {
    this.initTableData(this.absencesService); 
  }

  isChangeStatusModalVisible = false;
  changeStatusModalAbsenceSelected: AbsenceModel = null;
  changeStatusModalAction: string = null;
  changeStatusModalLoading = false;
  openChangeStatusModal(absence: AbsenceModel) {

    if (absence.status != 'submitted' || !this.authService.hasPermission('bus:absences:status')) { return; }
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalAbsenceSelected = absence;
  }

  handleChangeStatusCancel() {
    this.changeStatusModalAbsenceSelected = null;
    this.changeStatusModalAction = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
  }
  handleChangeStatus() {
    if (!this.changeStatusModalAction) { return; }
    this.changeStatusModalLoading = true;
    this.absencesService.status(this.changeStatusModalAbsenceSelected.id, this.changeStatusModalAction).pipe(
      first(),
      finalize(() => this.changeStatusModalLoading = false)
    ).subscribe(result => {
      this.initTableData(this.absencesService);
      this.uiService.showMessage(MessageType.success, "Mudança de estado aprovada com sucesso");
      this.handleChangeStatusCancel();
    });

  }

  listComplete() {
    this.filters.status = null;
    this.filters.search = null;
    this.searchData(true);
  }

}
