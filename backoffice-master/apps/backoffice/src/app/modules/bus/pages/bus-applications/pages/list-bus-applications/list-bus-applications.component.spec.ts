import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBusApplicationsComponent } from './list-bus-applications.component';

describe('ListBusApplicationsComponent', () => {
  let component: ListBusApplicationsComponent;
  let fixture: ComponentFixture<ListBusApplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBusApplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBusApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
