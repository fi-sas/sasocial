import {
  BusApplicationModel,
  BusApplicationTagResult,
} from '../../../../models/bus_application.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { BusApplicationService } from '../../services/bus-application.service';
import * as moment from 'moment';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';

@Component({
  selector: 'fi-sas-list-bus-applications',
  templateUrl: './list-bus-applications.component.html',
  styleUrls: ['./list-bus-applications.component.less'],
})
export class ListBusApplicationsComponent
  extends TableHelper
  implements OnInit {
  @ViewChild('paymentModel', null) paymentModel: any;

  withdrawalDecision = '';
  isChangeStatusModalVisible = false;
  changeStatusModalApplication: BusApplicationModel = null;
  changeStatusModalApplicationAction = null;
  changeStatusModalLoading = false;
  academic_years = [];

  isPaymentMonthModalVisible = false;

  paymentMonthModalApplication: BusApplicationModel = null;
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public busApplicationService: BusApplicationService,
    private authService: AuthService,
    private academicYearsService: AcademicYearsService,
  ) {
    super(uiService, router, activatedRoute);
    this.status = [
      {
        description: "Aprovada",
        value: 'accepted'
      },
      {
        description: "Cancelada",
        value: 'canceled'
      },
      {
        description: "Desistiu",
        value: 'withdrawal'
      },
      {
        description: "Em desistência",
        value: 'quiting'
      },
      {
        description: "Fechada",
        value: 'closed'
      },
      {
        description: "Rejeitada",
        value: 'rejected'
      },

      {
        description: "Submtida",
        value: 'submitted'
      },
      {
        description: "Suspensa",
        value: 'suspended'
      },

      
    ];
  }

  ngOnInit() {

    this.persistentFilters = {
      searchFields: 'name'
    };

    this.columns.push(
      {
        key: 'academic_year',
        label: 'Ano Académico',
        sortable: true,
      },
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'identification',
        label: 'B.I./C.C.:',
        sortable: true,
      },
      {
        key: '',
        label: 'Data início',
        template: (data) => {
          return moment(new Date(data.date)).format('DD/MM/YYYY');
        },
        sortable: true,
      },
      {
        key: 'application.declaration',
        label: 'Declaração',
        sortable: true,
        tag: TagComponent.YesNoTag,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: BusApplicationTagResult,
      }
    );
      this.loadAcademicYears();
    this.initTableData(this.busApplicationService);
  }

  deleteBusApplication(id: number) {
    if (!this.authService.hasPermission('bus:applications:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar candidatura para o bus',
        'Pretende eliminar a candidatura para o bus?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.busApplicationService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Candidatura para o bus eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.status = null;
    this.filters.academic_year = null;
    this.filters.search = null;
    this.searchData(true);
  }

  changeStateModal(application: BusApplicationModel, disabled: boolean) {

    if (!this.authService.hasPermission('bus:applications:status') || (application.status === 'canceled' || application.status === 'closed' || application.status === 'withdrawal' || application.status === 'quiting')) {
      return;
    }

    this.withdrawalDecision = '';

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalApplication = application;
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalApplication = null;
    this.changeStatusModalApplicationAction = null;
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalApplicationAction = action;
  }

  handleChangeStatusOk() {
    this.changeStatusModalLoading = true;
    this.busApplicationService
      .status(this.changeStatusModalApplication.id, this.changeStatusModalApplicationAction)
      .pipe(
        first(),
        finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          'Estado da candidatura para o bus alterada com sucesso'
        );
        this.searchData();
        this.handleChangeStatusCancel();
        this.isChangeStatusModalVisible = false
      });
  }

  paymentMonthModal(application: BusApplicationModel, disabled: boolean) {
    if (!this.authService.hasPermission('bus:payment_months:create') || application.status !== 'accepted') {
      return;
    }

    if (!disabled) {
      this.paymentMonthModalApplication = application;
      this.paymentModel.handlePaymentMonthToOpen();
    }
  }

  paymentSubmitted(submitted: boolean) {
    if (submitted) {
      this.searchData();
    }
  }

  imitBusDeclaration(application: BusApplicationModel) {
    if (!this.authService.hasPermission('bus:applications:declaration') || application.status !== 'accepted') {
      return;
    }
    this.loading = true;
    this.busApplicationService.generateApplicationDeclaration(application.id).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(()=>{
      this.uiService.showMessage(MessageType.success, 'Declaração emitida com sucesso');
    });
  }

  loadAcademicYears() {
    this.academicYearsService
      .list(1, -1, 'start_date', 'ascend')
      .pipe(
        first()
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }
}
