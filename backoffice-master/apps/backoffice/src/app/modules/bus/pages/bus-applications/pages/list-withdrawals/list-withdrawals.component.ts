import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { finalize, first } from 'rxjs/operators';
import { WithdrawalModel, WithdrawalStatus, WithdrawalStatusTranslations } from '../../../../models/withdrawal.model';
import { WithdrawalsService } from '../../services/withdrawals.service';


@Component({
  selector: 'fi-sas-list-withdrawals',
  templateUrl: './list-withdrawals.component.html',
  styleUrls: ['./list-withdrawals.component.less']
})
export class ListWithdrawalsComponent extends TableHelper implements OnInit {
  WithdrawalStatus = WithdrawalStatus;

  //MODAL
  changeStatusModalAction = null;
  isChangeStatusModalVisible = false;
  changeStatusModalWithdrawalSelected: WithdrawalModel = null;
  changeStatusModalLoading = false;
  changeStatusModalNotes = '';

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public withdrawalsService: WithdrawalsService,
  ) {
    super(uiService, router, activatedRoute);

    this.columns.push(
      {
        key: 'application.name',
        label: 'Nome',
        sortable: false
      },
      {
        key: 'justification',
        label: 'Justificação',
        sortable: true
      },

      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: WithdrawalStatusTranslations
      },

    )

    this.persistentFilters['withRelated'] = 'application,application.room,application.assignedResidence,history'
  }
  ngOnInit() {
    this.initTableData(this.withdrawalsService);
  }


  openModalWithdrawal(withdrawal: WithdrawalModel, action: string) {
    if (withdrawal.status != 'pending') return;
    this.isChangeStatusModalVisible = true;
    this.changeStatusModalWithdrawalSelected = withdrawal;
    this.changeStatusModalAction = action;
  }

  handleChangeStatusCancel() {
    this.changeStatusModalWithdrawalSelected = null;
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalLoading = false;
    this.changeStatusModalNotes = '';
  }

  handleChangeStatus() {
    if (this.changeStatusModalAction === 'approve') {
      this.changeStatusModalLoading = true;
      this.withdrawalsService.aprove(this.changeStatusModalWithdrawalSelected.id, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        const withdrawalIndex = this.data.findIndex(w => w.id === this.changeStatusModalWithdrawalSelected.id);
        if (withdrawalIndex >= 0) {
          this.data[withdrawalIndex] = result.data[0];
          this.data = [...this.data];
        }
        this.handleChangeStatusCancel();
      });
    } else {
      // this.changeStatusModalLoading = true;
      this.withdrawalsService.reject(this.changeStatusModalWithdrawalSelected.id, this.changeStatusModalNotes).pipe(
        first(),
        finalize(() => this.changeStatusModalLoading = false)
      ).subscribe(result => {
        const withdrawalIndex = this.data.findIndex(w => w.id === this.changeStatusModalWithdrawalSelected.id);
        if (withdrawalIndex >= 0) {
          this.data[withdrawalIndex] = result.data[0];
          this.data = [...this.data];
        }
        this.handleChangeStatusCancel();
      });
    }
  }

}
