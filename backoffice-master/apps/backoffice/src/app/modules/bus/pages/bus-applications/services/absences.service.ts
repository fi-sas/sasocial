
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { AbsenceModel } from '../../../models/absence.model';

@Injectable({
  providedIn: 'root'
})
export class AbsencesService extends Repository<AbsenceModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'BUS.ABSENCES';
    this.entity_url = 'BUS.ABSENCES_ID';
  }

  status(id: number, event: string) {
    return this.resourceService.create<AbsenceModel>(
      this.urlService.get('BUS.ABSENCES_STATUS', { id }),
      { event }
    );
  }
}