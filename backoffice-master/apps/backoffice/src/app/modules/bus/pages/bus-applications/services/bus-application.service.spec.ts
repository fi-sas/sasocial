import { TestBed } from '@angular/core/testing';

import { BusApplicationService } from './bus-application.service';

describe('BusApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusApplicationService = TestBed.get(BusApplicationService);
    expect(service).toBeTruthy();
  });
});
