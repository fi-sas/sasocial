
import { Injectable } from '@angular/core';
import { ReportModel } from '@fi-sas/backoffice/modules/reports/models/report.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { BusApplicationModel } from '../../../models/bus_application.model';
import { PaymentMonthModel } from '../../../models/payment_month.model';

@Injectable({
  providedIn: 'root',
})
export class BusApplicationService extends Repository<BusApplicationModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'BUS.APPLICATIONS';
    this.entity_url = 'BUS.APPLICATIONS_ID';
  }

  status(id: number, event: string): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(this.urlService.get('BUS.APPLICATIONS_STATUS', { id }), { event: event });
  }

  accept(id: number): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_ACCEPT', { id }),
      {}
    );
  }

  widthdrawApplication(id: number, justification: string) {

    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_WITHDRAWAL_ID', { id }),
      {
        justification
      }
    );
  }

  reject(id: number): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_REJECT', { id }),
      {}
    );
  }

  cancel(id: number): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_CANCEL', { id }),
      {}
    );
  }

  withdrawal(id: number, justification: string): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_WITHDRAWAL', { id }),
      {
        justification
      }
    );
  }

  withdrawalStatus(id: number, accept: boolean): Observable<Resource<BusApplicationModel>> {
    return this.resourceService.create<BusApplicationModel>(
      this.urlService.get('BUS.APPLICATIONS_WITHDRAWAL_STATUS', { id }),
      {
        "event": accept ? "accept" : "reject"
      }
    );
  }

  paymentMonths(id: number): Observable<Resource<PaymentMonthModel>> {
    return this.resourceService.list<PaymentMonthModel>(
      this.urlService.get('BUS.APPLICATIONS_PAYMENT_MONTHS', { id }),
      {}
    );
  }

  generateApplicationDeclaration(id: number): Observable<Resource<ReportModel>> {
    return this.resourceService.create(this.urlService.get('BUS.APPLICATION_DECLARATION', { id }),
      {});
  }


}
