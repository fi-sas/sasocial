import { TestBed } from '@angular/core/testing';

import { PaymentMonthService } from './payment-month.service';

describe('PaymentMonthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentMonthService = TestBed.get(PaymentMonthService);
    expect(service).toBeTruthy();
  });
});
