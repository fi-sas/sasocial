
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { PaymentMonthModel } from '../../../models/payment_month.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentMonthService extends Repository<PaymentMonthModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'BUS.PAYMENT_MONTHS';
    this.entity_url = 'BUS.PAYMENT_MONTHS_ID';
  }
}
