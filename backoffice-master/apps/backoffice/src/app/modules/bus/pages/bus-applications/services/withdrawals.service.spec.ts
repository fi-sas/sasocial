import { TestBed } from '@angular/core/testing';

import { WithdrawalsService } from './withdrawals.service';

describe('WithdrawalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WithdrawalsService = TestBed.get(WithdrawalsService);
    expect(service).toBeTruthy();
  });
});
