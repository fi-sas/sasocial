import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { WithdrawalModel } from '../../../models/withdrawal.model';

@Injectable({
  providedIn: 'root'
})
export class WithdrawalsService extends Repository<WithdrawalModel> {
  entities_url = 'BUS.WITHDRAWALS';
  entity_url = 'BUS.WITHDRAWALS_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
  }

  aprove(
    id: number,
    withdrawalNotes: string
  ): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('BUS.WITHDRAWALS_ID_APPROVE', { id }),
      { notes: withdrawalNotes }
    );
  }

  reject(
    id: number,
    withdrawalNotes: string
  ): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(
      this.urlService.get('BUS.WITHDRAWALS_ID_REJECT', { id }),
      { notes: withdrawalNotes }
    );
  }


}
