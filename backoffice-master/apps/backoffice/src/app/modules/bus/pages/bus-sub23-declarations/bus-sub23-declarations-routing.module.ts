import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBusSub23DeclarationsComponent } from './pages/list-bus-sub23-declarations/list-bus-sub23-declarations.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListBusSub23DeclarationsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:sub23_declarations:create'},
  },

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListBusSub23DeclarationsRoutingModule { }