

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListBusSub23DeclarationsRoutingModule } from './bus-sub23-declarations-routing.module';
import { ViewBusSub23DeclarationComponent } from './components/view-bus-sub23-declaration/view-bus-sub23-declaration.component';
import { ListBusSub23DeclarationsComponent } from './pages/list-bus-sub23-declarations/list-bus-sub23-declarations.component';

@NgModule({
  declarations: [
    ViewBusSub23DeclarationComponent,
    ListBusSub23DeclarationsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ListBusSub23DeclarationsRoutingModule
  ]

})
export class BusSub23DeclarationsModule { }
