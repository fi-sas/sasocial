import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBusSub23DeclarationComponent } from './view-bus-sub23-declaration.component';

describe('ViewBusSub23DeclarationComponent', () => {
  let component: ViewBusSub23DeclarationComponent;
  let fixture: ComponentFixture<ViewBusSub23DeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBusSub23DeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBusSub23DeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
