import { Component, Input } from '@angular/core';

import { Sub23BusDeclarationModel } from '@fi-sas/backoffice/modules/bus/models/sub23_declaration.model';

@Component({
  selector: 'fi-sas-view-bus-sub23-declaration',
  templateUrl: './view-bus-sub23-declaration.component.html',
  styleUrls: ['./view-bus-sub23-declaration.component.less']
})
export class ViewBusSub23DeclarationComponent {
  @Input() declaration: Sub23BusDeclarationModel = null;

  constructor() { }

  getDocumentTypeDescription() {
    return this.declaration.document_type.translations.find(x => x.language_id == 3).description;
  }
}
