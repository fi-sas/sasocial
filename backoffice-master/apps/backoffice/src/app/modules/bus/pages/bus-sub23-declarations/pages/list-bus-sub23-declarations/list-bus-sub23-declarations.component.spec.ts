import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBusSub23DeclarationsComponent } from './list-bus-sub23-declarations.component';

describe('ListBusSub23DeclarationsComponent', () => {
  let component: ListBusSub23DeclarationsComponent;
  let fixture: ComponentFixture<ListBusSub23DeclarationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBusSub23DeclarationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBusSub23DeclarationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
