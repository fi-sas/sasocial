import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Sub23BusDeclarationModel, Sub23BusDeclarationTagResult } from '@fi-sas/backoffice/modules/bus/models/sub23_declaration.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { Sub23BusDeclarationsService } from '../../services/sub23-bus-declarations.service';

@Component({
  selector: 'fi-sas-list-bus-sub23-declarations',
  templateUrl: './list-bus-sub23-declarations.component.html',
  styleUrls: ['./list-bus-sub23-declarations.component.less']
})
export class ListBusSub23DeclarationsComponent extends TableHelper implements OnInit {

  withdrawalDecision = '';
  isChangeStatusModalVisible = false;
  changeStatusModalDeclaration: Sub23BusDeclarationModel = null;
  changeStatusModalDeclarationAction = null;
  changeStatusModalLoading = false;

  declaration_file_id = null;
  declaration_required = false;
  rejectReason = null;
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private sub23BusDeclarationservice: Sub23BusDeclarationsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.persistentFilters = {
      searchFields: 'name'
    };

    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        label: 'Tipo de documento',
        key: 'document_type',
        template: (data) => {
          return data.document_type.translations.find(x => x.language_id == 3).description;
        },
      },
      {
        key: 'identification',
        label: 'Nº identificação',
        sortable: true,
      },
      {
        key: 'has_social_scholarship',
        label: 'Bolsa de Estudo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      },
      {
        label: 'Submetida',
        key: 'created_at',
        template: (data) => {
          return moment(new Date(data.created_at)).format('DD/MM/YYYY');
        },
      },
      {
        key: 'status',
        label: 'Estado',
        tag: Sub23BusDeclarationTagResult,
      }
    );
    this.initTableData(this.sub23BusDeclarationservice);
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalDeclarationAction = action;
  }

  changeStateModal(declaration: Sub23BusDeclarationModel, disabled: boolean) {
    if(!this.authService.hasPermission('bus:sub23_declarations:status') || declaration.status == 'emitted' || declaration.status == 'rejected') {
      return;
    }

    this.withdrawalDecision = '';

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalDeclaration = declaration;
    }
  }
  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalDeclaration = null;
    this.changeStatusModalDeclarationAction = null;
  }


  handleChangeStatusOk() {
    if (this.changeStatusModalDeclarationAction == 'accept' && !this.declaration_file_id) {
      this.declaration_required = true;
      return;
    }
    if(this.changeStatusModalDeclarationAction == 'reject' && !this.rejectReason) {
      this.uiService.showMessage(
        MessageType.warning,
        'Deve indicar o motivo de rejeição'
      );
      return;

    }
    this.changeStatusModalLoading = true;
    this.sub23BusDeclarationservice
      .status(this.changeStatusModalDeclaration.id, this.changeStatusModalDeclarationAction, this.declaration_file_id, this.rejectReason)
      .pipe(
        first(),
        finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          'Estado do pedido de declaração sub 23 alterada com sucesso'
        );
        this.searchData();
        this.handleChangeStatusCancel();
        this.isChangeStatusModalVisible = false
      });
  }


  onFileDeleted() {
    this.declaration_file_id = null;
    this.declaration_required = true;
  }
  onFileAdded(id) {
    this.declaration_required = false;
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true);
  }


  printDeclaration(declaration: Sub23BusDeclarationModel) {
    if (!this.authService.hasPermission('bus:sub23_declarations:status')) { return; }
    this.sub23BusDeclarationservice.printDeclaration(declaration.id).subscribe(response => {
      this.uiService.showMessage(MessageType.success, 'Declaração emitida com sucesso');
    })
  }

}
