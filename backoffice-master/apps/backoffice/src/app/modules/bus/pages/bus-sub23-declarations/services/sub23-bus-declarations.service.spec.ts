import { TestBed } from '@angular/core/testing';
import { Sub23BusDeclarationsService } from './sub23-bus-declarations.service';


describe('Sub23BusDeclarationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Sub23BusDeclarationsService = TestBed.get(Sub23BusDeclarationsService);
    expect(service).toBeTruthy();
  });
});
