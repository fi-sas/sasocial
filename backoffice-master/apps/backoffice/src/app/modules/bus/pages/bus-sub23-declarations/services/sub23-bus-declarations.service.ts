import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { BusApplicationModel } from '../../../models/bus_application.model';
import { Sub23BusDeclarationModel } from '../../../models/sub23_declaration.model';

@Injectable({
  providedIn: 'root'
})
export class Sub23BusDeclarationsService extends Repository<Sub23BusDeclarationModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'BUS.SUB23_DECLARATIONS';
    this.entity_url = 'BUS.SUB23_DECLARATIONS_ID';
  }

  status(id: number, event: string, declaration_id?: number, reject_reason?: string): Observable<Resource<Sub23BusDeclarationModel>> {
    return this.resourceService.create<Sub23BusDeclarationModel>(this.urlService.get('BUS.SUB23_DECLARATIONS_STATUS', { id }), { event: event, declaration_id, reject_reason });
  }

  printDeclaration(id: number): Observable<Resource<Sub23BusDeclarationModel>> {
    return this.resourceService.create<Sub23BusDeclarationModel>(this.urlService.get('BUS.SUB23_DECLARATIONS_PRINT', { id }), {});
  }
  printListDeclarations(academic_year: string): Observable<Resource<Sub23BusDeclarationModel>> {
    let params = new HttpParams();
    params = params.set('academic_year', academic_year);
    return this.resourceService.read<Sub23BusDeclarationModel>(this.urlService.get('BUS.SUB23_DECLARATIONS_PRINT_LIST', { }), {params});
  }
  printApplications(academic_year?: string, organic_unit?: number): Observable<Resource<BusApplicationModel>> {
    let params = new HttpParams();
    if(academic_year && academic_year !== null) {
      params = params.set('academic_year', academic_year);
    }
    if(organic_unit && organic_unit !== null) {
      params = params.set('organic_unit', organic_unit.toString());
    }
    return this.resourceService.read<BusApplicationModel>(this.urlService.get('BUS.APPLICATIONS_LIST_REPORTS', { }), {params});
  }
  } 

