import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ConfigurationsService } from '../../services/configurations.service';
import { finalize, first } from "rxjs/operators";
import { ConfigurationsModel } from '../../models/configurations.model';

@Component({
  selector: 'fi-sas-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.less']
})
export class ConfigurationsComponent implements OnInit {

  formData: FormGroup;
  loadingConfiguration = true;
  loading = false;
  submit = false;
  constructor(private fb: FormBuilder, private settings: ConfigurationsService, private uiService: UiService) { }

  ngOnInit() {
      this.formData = this.fb.group({
          assigned_status_auto_change: new FormControl(false, [Validators.required]),
          assigned_status_auto_change_day: new FormControl(''),
          assigned_status_auto_change_state: new FormControl(''),
      })
      this.getConfigurations();
  }
  get f() { return this.formData.controls; }

  getConfigurations() {
      this.settings.list(1, -1).pipe(first(), finalize(() => this.loadingConfiguration = false)).subscribe((data) => {
        if(data.data[0].ASSIGNED_STATUS_AUTO_CHANGE === "false"){
          this.formData.get('assigned_status_auto_change').setValue(false);
        }
        if(data.data[0].ASSIGNED_STATUS_AUTO_CHANGE === "true"){
          this.formData.get('assigned_status_auto_change').setValue(true);
        }
         
          this.formData.get('assigned_status_auto_change_day').setValue(data.data[0].NUNBER_DAYS_FOR_CHANGE);
          this.formData.get('assigned_status_auto_change_state').setValue(data.data[0].EVENT_FOR_AUTO_CHANGE);
      })
  }

  submitForm() {
    this.loading = true;
    this.submit = true;
    let sendValue: ConfigurationsModel = new ConfigurationsModel();
    if (this.formData.valid) {
      this.submit = false;
      this.settings.update_createkey("ASSIGNED_STATUS_AUTO_CHANGE", this.formData.get('assigned_status_auto_change').value.toString()).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
      });
      this.settings.update_createkey("NUNBER_DAYS_FOR_CHANGE", this.formData.get('assigned_status_auto_change_day').value).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
      });
      this.settings.update_createkey("EVENT_FOR_AUTO_CHANGE", this.formData.get('assigned_status_auto_change_state').value).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
        this.uiService.showMessage(MessageType.success, 'Configurações alteradas com sucesso');
      });
    } else {
      this.loading = false;
    }

  }

  changeStatus(event) {
      this.formData.get('assigned_status_auto_change_day').setValue(null);
      this.formData.get('assigned_status_auto_change_state').setValue(null);
      if (event == true) {
          this.formData.controls.assigned_status_auto_change_day.setValidators([Validators.required]);
          this.formData.controls.assigned_status_auto_change_state.setValidators([Validators.required]);
      } else {
          this.formData.controls.assigned_status_auto_change_day.clearValidators();
          this.formData.controls.assigned_status_auto_change_state.clearValidators();
      }
      this.formData.controls.assigned_status_auto_change_day.updateValueAndValidity();
      this.formData.controls.assigned_status_auto_change_state.updateValueAndValidity();
  }
}
