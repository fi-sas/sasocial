import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormLocalComponent } from './pages/form-local/form-local.component';
import { ListLocalsComponent } from './pages/list-locals/list-locals.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListLocalsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:locals:read'},
  },
  {
    path: 'create',
    component: FormLocalComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'bus:locals:create'},
  },
  {
    path: 'update/:id',
    component: FormLocalComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'bus:locals:update'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalBusRoutingModule { }
