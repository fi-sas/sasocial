import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { LocalBusRoutingModule } from './local-routing.module';
import { FormLocalComponent } from './pages/form-local/form-local.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { ListLocalsComponent } from './pages/list-locals/list-locals.component';

@NgModule({
  declarations: [
    FormLocalComponent,
    ListLocalsComponent
  ],
  imports: [
    CommonModule,
    AngularOpenlayersModule,
    SharedModule,
    LocalBusRoutingModule
  ]

})
export class LocalBusModule { }
