import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LocalModel } from '@fi-sas/backoffice/modules/bus/models/locals.model';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { proj } from 'openlayers';
import { LocalsService } from '../../services/locals.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
@Component({
  selector: 'fi-sas-create-local',
  templateUrl: './form-local.component.html',
  styleUrls: ['./form-local.component.less']
})
export class FormLocalComponent implements OnInit {

  locals;
  localForm: FormGroup;
  isLoading = false;
  isUpdate = false;
  idUpdate;
  updateLocal: LocalModel = null;
  isSubmited = false;
  latitudeInit: number = 39.3999;
  longitudeInit: number = -8.2245;
  latitude: number = 39.3999;
  longitude: number = -8.2245;
  mapZoom = 8;

  getLatLong(event) {
    var lonlat = proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
    this.longitude = lonlat[0];
    this.latitude = lonlat[1];
    this.localForm.controls.latitude.setValue(this.latitude);
    this.localForm.controls.longitude.setValue(this.longitude);
  }


  constructor(
    private route: ActivatedRoute,
    private uiService: UiService,
    private localsService: LocalsService,
    private router: Router,
    private authService: AuthService
  ) {
    this.localForm = new FormGroup({
      local: new FormControl('', [Validators.required, trimValidation]),
      region: new FormControl('', [Validators.required, trimValidation]),
      latitude: new FormControl('', [
        Validators.required,
        //Validators.pattern('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}')
      ]),
      longitude: new FormControl('', [
        Validators.required,
        //Validators.pattern('^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\\.{1}\\d{1,6}')
      ]),
      active: new FormControl(true, [Validators.required]),
    });
  }


  get f() { return this.localForm.controls; }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
        this.getLocalId(id);
        this.idUpdate = id;
        this.isUpdate = true;
    }


  }

  getLocalId(id) {
    this.localsService
        .read(id)
        .pipe(
            first()
        )
        .subscribe((results) => {
          this.updateLocal = results.data[0];
          this.localForm.patchValue({
            local: this.updateLocal.local,
            region: this.updateLocal.region,
            latitude: this.updateLocal.latitude,
            longitude: this.updateLocal.longitude,
            active: this.updateLocal.active
          });
          this.latitudeInit = this.updateLocal.latitude;
          this.longitudeInit = this.updateLocal.longitude;
          this.longitude = this.updateLocal.longitude;
          this.latitude = this.updateLocal.latitude;
          this.mapZoom = 15;
        });
  }

  submit(value: any, valid: boolean) {

    this.isSubmited = true;

    if (!valid) {
      this.localForm.updateValueAndValidity();
      return;
    }

    this.localForm.disable();
    this.isLoading = true;


    if (this.isUpdate && this.authService.hasPermission('bus:locals:update')) {
      this.localsService.update(this.idUpdate, value)
        .pipe(
          first(),
          finalize(
            () => {
              this.isLoading = false;
            }
          )
        )
        .subscribe(local => {
          this.locals = local.data;
          this.router.navigate(['bus', 'locals','list']);
        });
    } else {

      this.localsService.create(value)
        .pipe(
          first(),
          finalize(
            () => {
              this.isLoading = false;
            }
          )
        )
        .subscribe(local => {
          this.locals = local.data;
          this.router.navigate(['bus', 'locals','list']);
        });
    }
  }

  returnButton() {
    this.router.navigate(['bus', 'locals','list']);
  }

  

}
