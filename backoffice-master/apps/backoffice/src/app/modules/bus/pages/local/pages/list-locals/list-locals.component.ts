import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { LocalsService } from '../../services/locals.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-locals',
  templateUrl: './list-locals.component.html',
  styleUrls: ['./list-locals.component.less']
})
export class ListLocalsComponent extends TableHelper implements OnInit {

  status = [];
  constructor(private localsService: LocalsService,
    private authService: AuthService,
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,) {
    super(uiService, router, activatedRoute);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
    this.persistentFilters = {
      searchFields: 'local,region',
    };
  }

  ngOnInit() {
    this.initTableData(this.localsService);
  }

  deleteLocal(id: number) {
    if (!this.authService.hasPermission('bus:locals:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este local?', 'Eliminar').subscribe(result => {
      if (result) {
        this.localsService.delete(id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, "Local eliminado com sucesso");
          this.initTableData(this.localsService);
        });
      }
    });
  }

  
  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true);
  }
}
