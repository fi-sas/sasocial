import { TestBed, inject } from '@angular/core/testing';

import { LocalsService } from './locals.service';

describe('LocalsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalsService]
    });
  });

  it('should be created', inject([LocalsService], (service: LocalsService) => {
    expect(service).toBeTruthy();
  }));
});
