

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { LocalModel } from '../../../models/locals.model';


@Injectable({
  providedIn: 'root'
})
export class LocalsService extends Repository<LocalModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.LOCALS_ID';
    this.entities_url = 'BUS.LOCALS';
  }

}
