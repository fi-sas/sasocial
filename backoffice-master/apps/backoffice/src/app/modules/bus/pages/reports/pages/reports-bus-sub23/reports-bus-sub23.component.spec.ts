import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsBusSub23Component } from './reports-bus-sub23.component';

describe('ReportsBusSub23Component', () => {
  let component: ReportsBusSub23Component;
  let fixture: ComponentFixture<ReportsBusSub23Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsBusSub23Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsBusSub23Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
