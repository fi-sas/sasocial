import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { tap, first, finalize } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { Sub23BusDeclarationsService } from '../../../bus-sub23-declarations/services/sub23-bus-declarations.service';

@Component({
  selector: 'fi-sas-reports-bus-sub23',
  templateUrl: './reports-bus-sub23.component.html',
  styleUrls: ['./reports-bus-sub23.component.less']
})
export class ReportsBusSub23Component implements OnInit {

  academic_years = [];
  academic_year = null;
  academic_year_applications = null;
  organic_unit = null;
  loading = false;
  organic_units = [];

  constructor(private declartionService: Sub23BusDeclarationsService, 
    private messageService: NzMessageService,  public organicUnitService: OrganicUnitsService) {
    for (let i = -1; i < 5; i++) {
      this.academic_years.push(moment().subtract(i + 1, 'year').year() + '-' + moment().subtract(i, 'year').year());
    }
    this.academic_year = this.academic_years.length > 0 ? this.academic_years[0] : null;
   }

  ngOnInit() {
    this.loadOrganicUnits();
  }


  declarations_raw() {

    this.declartionService.printListDeclarations(
      this.academic_year,
    ).pipe(tap(() => this.loading = true), finalize(() => this.loading = false))
      .subscribe(result => {
        return this.messageService.success("Relatório gerado com sucesso");
      });
  }

  applications_raw() {
    this.declartionService.printApplications(
      this.academic_year_applications, this.organic_unit
    ).pipe(tap(() => this.loading = true), finalize(() => this.loading = false))
      .subscribe(result => {
        return this.messageService.success("Relatório gerado com sucesso");
      });
  }

  loadOrganicUnits() {
    this.organicUnitService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
      first()
    ).subscribe(results => {
      this.organic_units = results.data;
    });
  }


}
