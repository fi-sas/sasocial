import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsBusSub23Component } from './pages/reports-bus-sub23/reports-bus-sub23.component';

import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    ReportsBusSub23Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingModule
  ]
})
export class ReportsBusModule { }
