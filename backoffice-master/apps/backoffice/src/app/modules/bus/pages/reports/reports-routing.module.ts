import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsBusSub23Component } from './pages/reports-bus-sub23/reports-bus-sub23.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ReportsBusSub23Component,
    data: { breadcrumb: 'Relatorios', title: 'Relatórios' },
  },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
