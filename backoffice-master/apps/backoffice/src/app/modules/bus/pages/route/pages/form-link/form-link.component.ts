import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { LinkModel } from '../../../../models/link.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { LinksService } from '../../services/links.service';
import { RoutesService } from '../../services/routes.service';


@Component({
  selector: 'fi-sas-form-link',
  templateUrl: './form-link.component.html',
  styleUrls: ['./form-link.component.less']
})
export class FormLinkComponent implements OnInit {

  isLoading = false;
  link;
  submitted = false;
  routes;
  routeSelected1 = null;
  localSelected1 = null;
  routeSelected2 = null;
  localSelected2 = null;
  linkActive = true;
  disableOne = true;
  disableTwo = true;
  locals1;
  locals2;
  id;

  constructor(private routesServices: RoutesService,
    private linksServices: LinksService,
    private activatedRoute: ActivatedRoute,
    private uiService: UiService,
    private routeService:RoutesService,
    private router: Router,
    private authService: AuthService) { }


  ngOnInit() {
    this.getRoutes();

  }

  getRoutes() {
    this.routeService.list(1,-1,null,null, {
      withRelated:'locals'
    }).pipe(first()).subscribe((data)=> {
      this.routes = data.data;
    })
  }


  getLocalsOne(route: { id: number, name: string }): void {
    this.localSelected1 = null;
    this.routesServices.read(route.id, {
      withRelated: 'locals'
    }).pipe(first()).subscribe(route_r => {
      this.disableOne = false;

      this.locals1 = route_r.data[0].locals;
    });
  }

  getLocalsTwo(route: { id: number, name: string }): void {
    this.localSelected2 = null;
    this.routesServices.read(route.id, {
      withRelated: 'locals'
    }).pipe(first()).subscribe(route_r => {
      this.disableTwo = false;
      this.locals2 = route_r.data[0].locals;
    });
  }

  getLocal1(local) {
    this.localSelected1 = local;
  }

  getLocal2(local) {
    this.localSelected2 = local;
  }

  submit() {
    this.submitted = true;
    if (this.authService.hasPermission('bus:links:create')) {
      if (this.localSelected1 !== null && this.localSelected2 !== null && this.locals1 !== null && this.locals2 !== null) {

        this.isLoading = true;

        const link_model = new LinkModel();

        link_model.route1_id = this.routeSelected1['id'];
        link_model.local_route1_id = this.localSelected1['id'];

        link_model.route2_id = this.routeSelected2['id'];
        link_model.local_route2_id = this.localSelected2['id'];
        link_model.local_route2_id = this.localSelected2['id'];
        link_model.active = this.linkActive;

        this.linksServices.create(link_model).pipe(
          first(),
          finalize(() => this.isLoading = false)
        )
          .subscribe((results) => {
            this.uiService.showMessage(MessageType.success, 'Ligação criada com sucesso');
            this.isLoading = false;
            this.submitted = false;
            this.router.navigate(["/bus", "routes","list"]);
          });

      } else {
        return;
      }
    }
  }

  returnButton() {
    this.router.navigate(["/bus", "routes","list"]);
  }
}
