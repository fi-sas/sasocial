import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPriceTableComponent } from './form-price-table.component';

describe('FormPriceTableComponent', () => {
  let component: FormPriceTableComponent;
  let fixture: ComponentFixture<FormPriceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPriceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPriceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
