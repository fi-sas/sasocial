import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import { PricesTablesModel } from '@fi-sas/backoffice/modules/bus/models/prices-tables.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxesService } from "@fi-sas/backoffice/modules/configurations/services/taxes.service";
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { PricesTablesService } from '../../services/prices-tables.service';
import { RoutesService } from '../../services/routes.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-form-price-table',
  templateUrl: './form-price-table.component.html',
  styleUrls: ['./form-price-table.component.less']
})
export class FormPriceTableComponent implements OnInit {

  route_id;
  price_table;
  totalcolumns = 2;
  formNumber = 0;
  validateForm: FormGroup;
  controlArrayGroup = [];
  controlArray: Array<{ line: number, row: number, controlInstance: string }> = [];
  isLoading = false;
  submitted = false;
  types_users;
  tickets_types;
  currentAccounts;
  taxes;

  ticket_types = [
    {
      value: "TICKET",
      label: "Bilhete",
    },
    {
      value: "PASS",
      label: "Passe",
    }
  ]
  id;


  formatterValue = value => `${value} `;

  constructor(private fb: FormBuilder,
    private typesUserSeervice: ProfilesService,
    private uiService: UiService,
    private routeService:RoutesService,
    private activatedRoute: ActivatedRoute,
    private pricesTablesService: PricesTablesService,
    private taxesService: TaxesService,
    private currentAccountService: CurrentAccountsService,
    private authService: AuthService,
    private router: Router) { }


  ngOnInit() {

    this.validateForm = this.fb.group({});
    this.validateForm.addControl('ticket_type', new FormControl('', Validators.required));
    this.validateForm.addControl('tax_id', new FormControl('', Validators.required));
    this.validateForm.addControl('profile_id', new FormControl('', Validators.required));
    this.validateForm.addControl('account_id', new FormControl('', []));
    this.validateForm.addControl('active', new FormControl(true, Validators.required));

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getRoutesById();
  }

  getRoutesById() {
    this.routeService.read(this.id, {
      withRelated:'locals'
    }).pipe(first()).subscribe((data)=> {
      this.route_id = this.id;
      this.getTypeUsers();
      this.getCurrentAccounts();
      this.getTaxes();
      this.addField();
    })
  }


  addField(e?: MouseEvent): void {

    if (e) {
      e.preventDefault();
    }

    const line = (this.controlArrayGroup.length > 0) ? this.controlArrayGroup[this.controlArrayGroup.length - 1][0].line + 1 : 0;

    for (let i = 0; i < this.totalcolumns; i++) {

      const row = (i > 0) ? this.controlArray[this.controlArray.length - 1].row + 1 : 0;

      this.formNumber = this.formNumber + 1;

      const control = {
        line,
        row,
        controlInstance: `price${this.formNumber}`
      };

      const index = this.controlArray.push(control);

      if (i !== 0) {

        this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl(null, [Validators.required,trimValidation]));

      } else {

        this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl(0, Validators.required));
      }
    }

    this.controlArrayGroup.push(this.controlArray);
    this.controlArray = [];

  }

  removeField(line: number, e: MouseEvent): void {
    e.preventDefault();

    if (this.controlArrayGroup.length > 1) {

      let index_remove: number;

      let index: { line: number, row: number, controlInstance: string };
      for (let i = 0; i < this.controlArrayGroup.length; i++) {
        for (let j = 0; j < this.controlArrayGroup[i].length; j++) {
          if (this.controlArrayGroup[i][j].line === line) {

            index = this.controlArrayGroup[i][j];
            index_remove = i;

            this.validateForm.removeControl(index.controlInstance);
          }
        }
      }
      this.controlArrayGroup.splice(index_remove, 1);

    }
  }



  submit(): void {
    if (this.authService.hasPermission('bus:price_tables:create')) {
      let tax_id: number, account_id: number;
      this.submitted = true;

      tax_id = parseInt(this.validateForm.value.tax_id, 10);
      if (this.validateForm.value.account_id)
        account_id = parseInt(this.validateForm.value.account_id, 10);

      for (const i in this.validateForm.controls) {
        if (i) {
          this.validateForm.controls[i].markAsDirty();
          this.validateForm.controls[i].updateValueAndValidity();
        }
      }

      if (!this.validateForm.valid) {
        this.validateForm.updateValueAndValidity();
        return;
      }

      const price_table = new PricesTablesModel();

      let index = 0;
      const prices = [];

      price_table.route_id = parseInt(this.route_id, 10);
      if (this.validateForm.value.account_id)
        price_table.account_id = parseInt(this.validateForm.value.account_id, 10);
      price_table.tax_id = parseInt(this.validateForm.value.tax_id, 10);
      price_table.profile_id = parseInt(this.validateForm.value.profile_id, 10);
      price_table.active = this.validateForm.value.active;
      price_table.ticket_type = this.validateForm.value.ticket_type;


      const values = [];

      for (const price in this.validateForm.controls) {

        if (price === "ticket_type" || price === "profile_id" || price === "tax_id" || price === "account_id" || price === "active") {

        } else {
          values.push(this.validateForm.controls[price].value)
        }

      }

      let price_model: { value: number, description: string, number_stop: number } = { value: 0, description: "", number_stop: 0 };

      let number_stop = 0;

      for (let i = 0; i < values.length; i++) {

        if (index < this.totalcolumns - 1) {

          if (index === 0) {

            price_model.value = Number(values[i]);
          }
          index++;
        } else {
          price_model.description = values[i];
          price_model.number_stop = number_stop;
          prices.push(price_model);
          price_model = { value: 0, description: "", number_stop: 0 };
          index = 0;
          number_stop++;
        }

      }

      price_table.prices = prices;

      this.validateForm.controls.ticket_type.disable();
      this.validateForm.controls.profile_id.disable();
      this.validateForm.controls.tax_id.disable();
      this.validateForm.controls.active.disable();
      this.validateForm.controls.account_id.disable();
      this.validateForm.disable();

      this.isLoading = true;

      this.pricesTablesService.create(price_table).subscribe(price_table_r => {
        this.price_table = price_table_r.data;
        this.isLoading = false;
        this.submitted = false;
        this.uiService.showMessage(
          MessageType.success,
          'Tabela de preço criada com sucesso'
        );
        this.router.navigate(["/bus", "routes", "list"]);
      });
    }
  }

  getTypeUsers(): void {

    this.typesUserSeervice.list(1, -1, null,null,{
      sort:'name'
    }).pipe(first()).subscribe(types_users => {
      this.types_users = types_users.data;
    });
  }

  getCurrentAccounts(): void {

    this.currentAccountService.list(1, -1, null,null,{
      sort:'name'
    }).pipe(first()).subscribe(currentAccounts => {
      this.currentAccounts = currentAccounts.data;
    });
  }

  getTaxes(): void {

    this.taxesService.list(1, -1, null,null,{
      sort:'name'
    }).pipe(first()).subscribe(taxes => {
      this.taxes = taxes.data;
    });
  }


  returnButton() {
    this.router.navigate(["/bus", "routes", "list"]);
  }




}
