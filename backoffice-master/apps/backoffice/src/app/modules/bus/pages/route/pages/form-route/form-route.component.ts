import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize, first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RouteModel } from '@fi-sas/backoffice/modules/bus/models/routes.model';
import { ContactsService } from '@fi-sas/backoffice/modules/configurations/services/contacts.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { Router } from '@angular/router';
import { LocalsService } from '../../../local/services/locals.service';
import { RoutesService } from '../../services/routes.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';


@Component({
  selector: 'fi-sas-form-route',
  templateUrl: './form-route.component.html',
  styleUrls: ['./form-route.component.less']
})
export class FormRouteComponent implements OnInit {

  route;
  totalcolumns = 3;
  formNumber = 0;
  validateForm: FormGroup;
  controlArrayGroup = [];
  controlArray: Array<{ line: number, row: number, controlInstance: string }> = [];
  isLoading = false;
  submited = false;
  contacts;
  locals;

  formatterDistance = value => `${value} KM`;
  parserDistance = value => value.replace('KM ', '');

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private contactsService: ContactsService,
    private localsServices: LocalsService,
    private routeService: RoutesService,
    private uiService: UiService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.validateForm = this.fb.group({});
    this.getContacts();
    this.getLocals();


    for (let i = 0; i < 2; i++) {
      this.addField();
    }

    this.validateForm.addControl('name', new FormControl('', [Validators.required, trimValidation]));
    this.validateForm.addControl('external', new FormControl(false, Validators.required));
    this.validateForm.addControl('active', new FormControl(true, Validators.required));
    this.validateForm.addControl('contact_id', new FormControl('', Validators.required));
    this.validateForm.addControl('represents_connection', new FormControl(false, Validators.required));

  }

  addField(e?: MouseEvent): void {

    if (e) {
      e.preventDefault();
    }

    const line = (this.controlArrayGroup.length > 0) ? this.controlArrayGroup[this.controlArrayGroup.length - 1][0].line + 1 : 0;

    for (let i = 0; i < this.totalcolumns; i++) {

      const row = (i > 0) ? this.controlArray[this.controlArray.length - 1].row + 1 : 0;

      this.formNumber = this.formNumber + 1;

      const control = {
        line,
        row,
        controlInstance: `local${this.formNumber}`
      };

      const index = this.controlArray.push(control);


      if (i === 0) {
        this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl(null, Validators.required));
      }

      if (i === 1) {
        this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl('', trimValidation));
      }

      if (i === 2) {

        this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl(0, Validators.required));
      }

    }

    this.controlArrayGroup.push(this.controlArray);
    this.controlArray = [];

  }

  removeField(line: number, e: MouseEvent): void {
    e.preventDefault();

    if (this.controlArrayGroup.length > 2) {

      let index_remove: number;

      let index: { line: number, row: number, controlInstance: string };
      for (let i = 0; i < this.controlArrayGroup.length; i++) {
        for (let j = 0; j < this.controlArrayGroup[i].length; j++) {
          if (this.controlArrayGroup[i][j].line === line) {

            index = this.controlArrayGroup[i][j];
            index_remove = i;

            this.validateForm.removeControl(index.controlInstance);
          }
        }
      }
      this.controlArrayGroup.splice(index_remove, 1);

    }
  }


  submit(): void {
    if (!this.authService.hasPermission('bus:routes:create')) {
      return
    }
    this.submited = true;
    for (const i in this.validateForm.controls) {
      if (i) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }

    if (!this.validateForm.valid) {
      this.validateForm.updateValueAndValidity();
      return;
    }

    const route = new RouteModel();

    let index = 0;
    const locals = [];

    route.name = this.validateForm.value.name;
    route.external = this.validateForm.value.external;
    route.active = this.validateForm.value.active;
    route.represents_connection = this.validateForm.value.represents_connection;
    route.contact_id = parseInt(this.validateForm.value.contact_id, 10);


    this.validateForm.controls.name.disable();
    this.validateForm.controls.contact_id.disable();


    const values = [];

    for (const local in this.validateForm.controls) {

      if (local === "contact_id" || local === "name" || local === "external" || local === "active" || local === "represents_connection") {

      } else {
        values.push(this.validateForm.controls[local].value)
      }

    }

    let local_model: { id: number, instruction: string, distance: number } = { id: 0, instruction: "", distance: 0 };

    for (let i = 0; i < values.length; i++) {

      if (index < this.totalcolumns - 1) {

        if (index === 0) {
          local_model.id = parseInt(values[i], 10);
        }

        if (index === 1) {
          local_model.instruction = values[i];
        }
        index++;
      } else {
        local_model.distance = values[i];
        locals.push(local_model);
        local_model = { id: 0, instruction: "", distance: 0 };
        index = 0;
      }
    }

    route.locals = locals;
    this.validateForm.disable();
    this.isLoading = true;

    this.routeService.create(route)
      .pipe(
        first(),
        finalize(
          () => { this.isLoading = false; }
        )
      )
      .subscribe(route_r => {
        this.route = route_r.data;
        this.router.navigate(['bus', 'routes', 'list']);
        this.uiService.showMessage(
          MessageType.success,
          'Rota criada com sucesso'
        );
      });
  }

  getContacts(): void {

    this.contactsService.list(1, -1).pipe(first()).subscribe(contacts => {
      this.contacts = contacts.data;
    });
  }

  getLocals(): void {

    this.localsServices.list(1, -1, null,null, {
      sort: 'local'
    }).pipe(first()).subscribe(locals => {
      this.locals = locals.data;
    });
  }


  returnButton() {
    this.uiService.showReturnModal(['bus', 'routes', 'list']).subscribe();
  }




  get f() { return this.validateForm.controls; }


}
