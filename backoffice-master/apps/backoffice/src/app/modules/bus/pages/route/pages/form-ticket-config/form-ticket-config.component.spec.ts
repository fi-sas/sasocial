import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTicketConfigComponent } from './form-ticket-config.component';

describe('FormTicketConfigComponent', () => {
  let component: FormTicketConfigComponent;
  let fixture: ComponentFixture<FormTicketConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTicketConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTicketConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
