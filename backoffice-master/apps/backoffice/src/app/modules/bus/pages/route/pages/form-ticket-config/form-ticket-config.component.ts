import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TicketsConfigsModel } from "@fi-sas/backoffice/modules/bus/models/tickets_configs.model";
import { FormControl, FormGroup, Validators} from "@angular/forms";
import { MessageType, UiService} from "@fi-sas/backoffice/core/services/ui-service.service";
import { TicketsConfigsService } from '../../services/tickets-configs.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-form-ticket-config',
  templateUrl: './form-ticket-config.component.html',
  styleUrls: ['./form-ticket-config.component.less']
})
export class FormTicketConfigComponent implements OnInit {
  submitted = false;
  route_id;
  isLoading = false;
  ticketconfig : TicketsConfigsModel = null;
  ticketconfigForm: FormGroup;

  constructor( private route: ActivatedRoute,
               private ticketconfigService: TicketsConfigsService,
               private uiService:UiService,
               private router:Router) {

    this.ticketconfigForm = new FormGroup({
      max_to_buy: new FormControl('',Validators.required),
      active: new FormControl(false, Validators.required)
    });

  }

  ngOnInit() {
    this.route_id = this.route.snapshot.paramMap.get('id');
    this.getValues();
  }

  getValues() {
    this.ticketconfigService.readByRoute(this.route_id).pipe(first()).subscribe((data)=> {
      this.ticketconfig = data.data[0];
      this.ticketconfigForm.patchValue({
        max_to_buy: this.ticketconfig.max_to_buy,
        active: Boolean(this.ticketconfig.active)
      })
    })
  }

  submit(value: any, valid: boolean) {
    this.submitted = true;
    if (!valid) {
      this.ticketconfigForm.updateValueAndValidity();
      return;
    }

    const ticketsConfig = new TicketsConfigsModel();
    ticketsConfig.route_id = parseInt(this.route_id, 10);
    ticketsConfig.max_to_buy = parseInt(value.max_to_buy,10);
    ticketsConfig.active = value.active;

    this.ticketconfigForm.disable();
    this.isLoading = true;

    this.ticketconfigService.update(this.ticketconfig.id, ticketsConfig).subscribe(() => {
         this.isLoading = false;
         this.submitted = false;
         this.uiService.showMessage(
          MessageType.success,
          'Configuração alterada com sucesso'
        );
         this.router.navigate(["/bus", "routes", 'list']);
    });

  }

  returnButton() {
    this.router.navigate(["/bus", "routes", 'list']);
  }
}
