import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromTimetableComponent } from './from-timetable.component';

describe('FromTimetableComponent', () => {
  let component: FromTimetableComponent;
  let fixture: ComponentFixture<FromTimetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromTimetableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
