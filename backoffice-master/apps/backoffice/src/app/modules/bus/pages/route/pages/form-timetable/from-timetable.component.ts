import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TimetableInsertModel, TimetableModel } from '@fi-sas/backoffice/modules/bus/models/timetable.model';
import { first } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { TimetableService } from '../../services/timetable.service';
import { RoutesService } from '../../services/routes.service';
import { TypesDaysService } from '@fi-sas/backoffice/modules/bus/pages/type-day/services/types-days.service';
import { SeasonsService } from '../../../season/services/seasons.service';

@Component({
  selector: 'fi-sas-from-timetable',
  templateUrl: './from-timetable.component.html',
  styleUrls: ['./from-timetable.component.less']
})
export class FromTimetableComponent implements OnInit {
  id;
  totalcolumns: number;
  formNumber = 0;
  validateForm: FormGroup;
  controlArrayGroup = [];
  route = [];
  controlArray: Array<{ line: number, row: number, controlInstance: string }> = [];
  isLoading = false;
  submitted = false;
  timetable;
  typesDays = [];
  seasons = [];
  defaultValue = new Date(0, 0, 0, 0, 0, 0);
  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private typeDaysService: TypesDaysService,
    private seasonService: SeasonsService,
    private timetableService: TimetableService,
    private uiService: UiService,
    private router:Router,
    private routeService: RoutesService,
    private authService: AuthService) {
  }

  ngOnInit(): void {
    
    this.validateForm = this.fb.group({});
    this.validateForm.addControl('type_day_id', new FormControl('', Validators.required));
      this.validateForm.addControl('season_id', new FormControl('', Validators.required));
      this.validateForm.addControl('active', new FormControl(true));
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getRoutesById();

  }

  getRoutesById() {
    this.routeService.read(this.id, {
      withRelated:'locals'
    }).pipe(first()).subscribe((data)=> {
      this.route = data.data;

      this.totalcolumns = this.route[0].locals.length;
  
      this.getTypesDays();
      this.getSeasons();
      this.addField();
    })
  }

  getDefaultValue(line, index) {
    const values = [];
    for (const hour in this.validateForm.controls) {
      if (!["type_day_id", "season_id", "active"].includes(hour)) {
        values.push(this.validateForm.controls[hour].value)
      }
    }
    const pos = (line * this.totalcolumns) + index - 1;
    return values[pos] && index != 0 ? values[pos] : this.defaultValue;
  }

  calculateIntevals(firstValue, line, collumn) {
    if (line == 0 || !firstValue || collumn != 0) { return; }

    const values = [];
    const controls = [];
    for (const hour in this.validateForm.controls) {
      if (!["type_day_id", "season_id", "active"].includes(hour)) {
        values.push(this.validateForm.controls[hour].value);
        controls.push(hour);
      }
    }
    const intervals = []
    for (let index = 0; index < this.totalcolumns; index++) {
      intervals.push(values[index + 1] - values[index]);
    }
    let count = 0;
    for (let index = controls.length - this.totalcolumns + 1; index < controls.length; index++) {
      if (count == 0) {
        this.validateForm.controls[controls[index]].setValue(new Date(new Date(firstValue).getTime() + intervals[count]));
      } else {
        this.validateForm.controls[controls[index]].setValue(new Date(new Date(this.validateForm.controls[controls[index - 1]].value).getTime() + intervals[count]));
      }
      count += 1;
    }

  }


  addField(e?: MouseEvent): void {

    if (e) {
      e.preventDefault();
    }

    const line = (this.controlArrayGroup.length > 0) ? this.controlArrayGroup[this.controlArrayGroup.length - 1][0].line + 1 : 0;

    for (let i = 0; i < this.totalcolumns; i++) {

      const row = (i > 0) ? this.controlArray[this.controlArray.length - 1].row + 1 : 0;

      this.formNumber = this.formNumber + 1;

      const control = {
        line,
        row,
        controlInstance: `hour${this.formNumber}`
      };

      const index = this.controlArray.push(control);

      this.validateForm.addControl(this.controlArray[index - 1].controlInstance, new FormControl(null, [Validators.required]));

    }

    this.controlArrayGroup.push(this.controlArray);
    this.controlArray = [];

  }

  removeField(line: number, e: MouseEvent): void {
    e.preventDefault();

    if (this.controlArrayGroup.length > 1) {

      let index_remove: number;

      let index: { line: number, row: number, controlInstance: string };
      for (let i = 0; i < this.controlArrayGroup.length; i++) {
        for (let j = 0; j < this.controlArrayGroup[i].length; j++) {
          if (this.controlArrayGroup[i][j].line === line) {

            index = this.controlArrayGroup[i][j];
            index_remove = i;

            this.validateForm.removeControl(index.controlInstance);
          }
        }
      }
      this.controlArrayGroup.splice(index_remove, 1);

    }
  }


  submit(): void {
    this.submitted = true;
    if (this.authService.hasPermission('bus:timetables:create')) {
      for (const i in this.validateForm.controls) {
        if (i) {
          this.validateForm.controls[i].markAsDirty();
          this.validateForm.controls[i].updateValueAndValidity();
        }
      }
      if (!this.validateForm.valid) {
        this.validateForm.updateValueAndValidity();
        return;
      }
      const timetable = new TimetableModel();

      let index = 0;
      let hourLine = [];
      const hours = [];


      timetable.route_id = Number(this.id);
      timetable.active = this.validateForm.value.active;
      timetable.type_day_id = parseInt(this.validateForm.value.type_day_id, 10);
      timetable.season_id = parseInt(this.validateForm.value.season_id, 10);

      const values = [];

      for (const hour in this.validateForm.controls) {

        if (hour === "type_day_id" || hour === "season_id" || hour === "active") {

        } else {
          values.push(this.validateForm.controls[hour].value)
        }

      }

      this.validateForm.controls.type_day_id.disable();
      this.validateForm.controls.season_id.disable();
      this.validateForm.controls.active.disable();

      for (let i = 0; i < values.length; i++) {

        if (index < this.totalcolumns - 1) {

          values[i] !== null ? hourLine.push(formatDate((values[i]), 'HH:mm:ss', 'pt-PT')) : hourLine.push(null);
          index++;

        } else {
          values[i] !== null ? hourLine.push(formatDate(values[i], 'HH:mm:ss', 'pt-PT')) : hourLine.push(null);
          hours.push(hourLine);
          hourLine = [];
          index = 0;
        }

      }

      const locals = [];

      for (const local in this.route[0].locals) {

        if (local) {
          locals.push(this.route[0].locals[local].id);
        }
      }

      timetable.hours = hours;

      this.validateForm.disable();
      this.isLoading = true;
      this.submitted = false;
      this.timetableService.create(timetable).subscribe(timtable => {
        this.timetable = timtable.data;
        this.isLoading = false;
        this.validateForm.enable();
        this.uiService.showMessage(
          MessageType.success,
          'Horário criado com sucesso'
        );
        this.router.navigate(["/bus", "routes", 'list']);
      },
        err => {
          this.isLoading = false;
          this.validateForm.enable();
        });
    }
  }

  getTypesDays(): void {

    this.typeDaysService.list(1, -1, null,null, {
      sort:'name'
    }).pipe(first()).subscribe(types_days => {
      this.typesDays = types_days.data;
    });

  }

  getSeasons(): void {
    this.seasonService.list(1, -1, null,null, {
      sort:'name'
    }).pipe(first()).subscribe(seasons => {
      this.seasons = seasons.data;
    });
  }


  returnButton() {
    this.router.navigate(["/bus", "routes", 'list']);
  }

  
}


