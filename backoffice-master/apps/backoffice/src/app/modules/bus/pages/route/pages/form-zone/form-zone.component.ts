import { Component, OnInit } from '@angular/core';
import { ZoneModel, ZoneInsert } from '@fi-sas/backoffice/modules/bus/models/zones.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { RoutesService } from '../../services/routes.service';
import { ZonesService } from '../../services/zones.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-zone',
  templateUrl: './form-zone.component.html',
  styleUrls: ['./form-zone.component.less']
})
export class FormZoneComponent implements OnInit {
  submitted = false;
  zone: ZoneModel[];
  locals = [];
  zonesForm: FormGroup;
  isLoading = false;
  route_id;
  zones;
  id;
  constructor(private zonesService: ZonesService,
    private routeServices: RoutesService,
    private routeService: RoutesService,
    private activatedRoute: ActivatedRoute,
    private uiService: UiService,
    private authService: AuthService,
    private router: Router) {

    this.zonesForm = new FormGroup({
      name: new FormControl('', [Validators.required,trimValidation]),
      active: new FormControl(true),
      locals_ids: new FormControl([],Validators.required)
    });
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getRoutesById();
  }

  getRoutesById() {
    this.routeService.read(this.id, {
      withRelated:'locals'
    }).pipe(first()).subscribe((data)=> {
      this.zones = data.data;
      this.route_id = this.id;
      this.locals = data.data[0].locals;

    })
  }

  submit(value: any, valid: boolean) {
    this.submitted = true;
    if (this.authService.hasPermission('bus:zones:create')) {
      if (!valid) {
        this.zonesForm.updateValueAndValidity();
        return;
      }
      this.isLoading = true;

      const zone_model = new ZoneModel();

      zone_model.name = value.name;
      zone_model.active = value.active;
      zone_model.route_id = parseInt(this.route_id, 10);
      zone_model.locals_ids = value.locals_ids;

      this.zonesService.create(zone_model).subscribe(zone => {
        this.isLoading = false;
        this.submitted = false;
        this.uiService.showMessage(
          MessageType.success,
          'Zona criada com sucesso'
        );
        this.router.navigate(["/bus", "routes","list"]);
      });
    } 
  }

  returnButton() {
    this.router.navigate(["/bus", "routes","list"]);
  }

  
}
