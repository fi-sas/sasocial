
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { LinksService } from '../../services/links.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-links',
  templateUrl: './list-links.component.html',
  styleUrls: ['./list-links.component.less']
})
export class ListLinksComponent extends TableHelper implements OnInit {

  id;

  constructor(
    private linksService: LinksService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.persistentFilters = {
      withRelated: 'local_route1,local_route2,route1,route2',
      route1_id: this.id
    };
  }

  ngOnInit() {
    this.initTableData(this.linksService);
  }


  deleteLink(link_id: number) {
    if (!this.authService.hasPermission('bus:links:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta ligação?', 'Eliminar').subscribe(result => {
      if (result) {
        this.linksService.delete(link_id).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Ligação eliminada com sucesso');
          this.initTableData(this.linksService);
        });
      }
    });
  }


  returnButton() {
    this.router.navigateByUrl('bus/routes/list');
  }

  form() {
    this.router.navigateByUrl('bus/routes/link/create');
  }
}
