import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPricesTablesComponent } from './list-prices-tables.component';

describe('ListPricesTablesComponent', () => {
  let component: ListPricesTablesComponent;
  let fixture: ComponentFixture<ListPricesTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPricesTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPricesTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
