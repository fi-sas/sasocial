import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { PricesService } from '../../../../services/prices.service';
import { PriceModel } from '../../../../models/price.model';
import { RoutesService } from '../../services/routes.service';
import { PricesTablesService } from '../../services/prices-tables.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-prices-tables',
  templateUrl: './list-prices-tables.component.html',
  styleUrls: ['./list-prices-tables.component.less']
})
export class ListPricesTablesComponent extends TableHelper implements OnInit {


  editPrice = 0;
  editPriceValue = null;
  ticket_types = {
    TICKET: "Bilhete",
    PASS: "Passe",
  }
  id;

  constructor(activatedRoute: ActivatedRoute,
    uiService: UiService,
    private priceService: PricesTablesService,
    private authService: AuthService,
    router: Router,
    private pricesService: PricesService) {
    super(uiService, router, activatedRoute);
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.persistentFilters = {
      withRelated: 'prices,profile,tax',
      route_id: this.id
    };
  }

  ngOnInit() {
    this.initTableData(this.priceService);
  }

  deletePriceTable(id: number) {
    if (!this.authService.hasPermission('bus:price_tables:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta tabela?', 'Eliminar').subscribe(result => {
      if (result) {
        this.priceService.delete(id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Tabela eliminada com sucesso');
          this.initTableData(this.priceService);
        });
      }
    });
  }


  changeHour(price_id, price) {
    if (!this.authService.hasPermission('bus:price_tables:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (price_id !== this.editPrice) {
      this.editPrice = price_id;
      this.editPriceValue = price;
    } else {
      this.editPrice = 0;
      this.editPriceValue = price;
    }
  }

  savePrice(price: PriceModel) {
    if (!this.authService.hasPermission('bus:price_tables:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }
    const price_obj = new PriceModel();
    price_obj.id = price.id;
    price_obj.value = this.editPriceValue;

    this.pricesService.patch(price_obj.id, price_obj).subscribe(response => {
      this.uiService.showMessage(
        MessageType.success,
        'Preço atualizado com sucesso'
      );
      this.editPrice = 0;
      price.value = price_obj.value;
    })

  }

  returnButton() {
    this.router.navigateByUrl('bus/routes/list');
  }

  form() {
    this.router.navigateByUrl('bus/routes/price-table/form/' + this.id);
  }

}
