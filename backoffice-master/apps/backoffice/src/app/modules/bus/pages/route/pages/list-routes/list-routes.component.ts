import { RouteModel } from '../../../../models/routes.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { RoutesService } from '../../services/routes.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-routes',
  templateUrl: './list-routes.component.html',
  styleUrls: ['./list-routes.component.less']
})
export class ListRoutesComponent extends TableHelper implements OnInit {

  editRouteIdName = 0;
  editRouteIdLocal: string;

  editRouteID = 0;
  isVisible = false;

  inputDistance;
  inputInstruction;

  formatterDistance = value => `${value} KM`;
  parserDistance = value => value.replace('KM ', '');


  constructor(private routesService: RoutesService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'locals,contact',
    };
  }

  ngOnInit(): void {
    this.initTableData(this.routesService);
  }
  

  showModal(contact_id: number): void {
    /* this.contact = contact.data[0]; */
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  deleteRoute(id: number) {
    if (!this.authService.hasPermission('bus:routes:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta rota?', 'Eliminar').subscribe(result => {
      if (result) {
        this.routesService.delete(id).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Rota eliminada com sucesso');
          this.initTableData(this.routesService);
        });
      }
    });
  }

  changeRouteName(route_id) {
    if (!this.authService.hasPermission('bus:routes:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (route_id !== this.editRouteIdName) {
      this.editRouteIdName = route_id;
    } else {
      this.editRouteIdName = 0;
    }
  }

  changeRouteLocal(route_id, local_index, instruction, distance) {
    this.inputInstruction = instruction;
    this.inputDistance = distance;

    let id_compound;

    id_compound = route_id + '.' + local_index;

    if (id_compound !== this.editRouteIdLocal) {

      this.editRouteID = route_id;
      this.editRouteIdLocal = id_compound;

    } else {
      this.editRouteIdLocal = "";
      this.editRouteID = 0;
    }
  }


  saveRouteName(route_name: string, route_id: number) {
    if (!this.authService.hasPermission('bus:routes:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    let routeObj = new RouteModel()

    routeObj.name = route_name

    this.routesService.patch(route_id, routeObj).pipe(first()).subscribe(() => {
      this.initTableData(this.routesService);
      this.editRouteIdName = 0;
    });
  }

  saveRouteLocal(local_id: number, route_id: number) {

    this.routesService.updateRouteLocal(route_id, local_id, this.inputInstruction, this.inputDistance).pipe(first()).subscribe(() => {

      this.editRouteIdLocal = "";
      this.initTableData(this.routesService);

    });

  }


}
