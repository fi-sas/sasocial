import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTicketsBoughtComponent } from './list-tickets-bought.component';

describe('ListTicketsBoughtComponent', () => {
  let component: ListTicketsBoughtComponent;
  let fixture: ComponentFixture<ListTicketsBoughtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTicketsBoughtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTicketsBoughtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
