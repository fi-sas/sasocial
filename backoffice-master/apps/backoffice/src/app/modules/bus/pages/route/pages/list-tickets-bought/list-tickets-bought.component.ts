import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { TicketsBoughtsService } from '../../services/tickets-boughts.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-tickets-bought',
  templateUrl: './list-tickets-bought.component.html',
  styleUrls: ['./list-tickets-bought.component.less']
})
export class ListTicketsBoughtComponent extends TableHelper implements OnInit {

  id;

  constructor(
    private ticketsBoughtService: TicketsBoughtsService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
      super(uiService, router, activatedRoute);
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.persistentFilters = {
        withRelated: 'price,departure_local,arrival_local,user',
        route_id: this.id
      };
     }

  ngOnInit() {
    this.initTableData(this.ticketsBoughtService);
  }


  confirmTicket(id: number, value: boolean) {
    if (!this.authService.hasPermission('bus:tickets_bought') || !value) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Confirmar', 'Pretende confirmar este bilhete?', 'Confirmar').subscribe(result => {
      if (result) {
        this.ticketsBoughtService.confirm(id).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Bilhete confirmado com sucesso');
          this.initTableData(this.ticketsBoughtService);
        });
      }
    });
  }

  returnButton() {
    this.router.navigateByUrl('bus/routes/list');
  }
}
