import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { HourUpdate } from "@fi-sas/backoffice/modules/bus/models/timetable.model";
import { formatDate } from "@angular/common";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { TimetableService } from '../../services/timetable.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-timetable',
  templateUrl: './list-timetables.component.html',
  styleUrls: ['./list-timetables.component.less']
})
export class ListTimetablesComponent extends TableHelper implements OnInit {

  editHour = 0;
  hour_obj: HourUpdate;
  date = null;
  id;


  constructor(activatedRoute: ActivatedRoute,
    private timetableService: TimetableService,
    uiService: UiService,
    router:Router,
    private authService: AuthService) { 
      super(uiService, router, activatedRoute);
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.persistentFilters = {
        withRelated: 'type_day,hours',
        route_id: this.id
      };
    }

    ngOnInit() {
      this.initTableData(this.timetableService);
    }


  deleteTimetable(id: number) {
    if (!this.authService.hasPermission('bus:timetables:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta tabela?', 'Eliminar').subscribe(result => {
      if (result) {
        this.timetableService.delete(id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Tabela eliminada com sucesso');
          this.initTableData(this.timetableService);
        });
      }
    });
  }

  deleteLine(timetable_id: number, line_number: number) {
    if (!this.authService.hasPermission('bus:timetables:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta linha?', 'Eliminar').subscribe(result => {
      if (result) {
        this.timetableService.deleteLine(timetable_id, line_number).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Linha eliminada com sucesso');
          this.initTableData(this.timetableService);
        });
      }
    });
  }

  changeHour(hour_id, hour) {
    if (!this.authService.hasPermission('bus:timetables:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (hour_id !== this.editHour) {
      this.editHour = hour_id;
      this.date = new Date("02-13-2019 " + hour);
    } else {
      this.editHour = 0;
      this.date = null;
    }
  }

  saveHour(hour_id: number) {
    if (!this.authService.hasPermission('bus:timetables:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.hour_obj = new HourUpdate();
    this.hour_obj.hour = formatDate(this.date, 'HH:mm:ss', 'pt');

    this.timetableService.updateHour(hour_id, this.hour_obj.hour.toString()).pipe(first()).subscribe(() => {
      this.editHour = 0;
      this.hour_obj = new HourUpdate();
      this.date = null;
      this.uiService.showMessage(
        MessageType.success,
        'Hora atualizada com sucesso'
      );
      this.initTableData(this.timetableService);
    });
  }

  returnButton() {
    this.router.navigateByUrl('bus/routes/list');
  }
  
  form() {
    this.router.navigateByUrl('bus/routes/timetables/form/'+this.id);
  }
}
