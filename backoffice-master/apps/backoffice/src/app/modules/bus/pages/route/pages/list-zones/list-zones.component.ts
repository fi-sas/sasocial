import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';

import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { RoutesService } from '../../services/routes.service';
import { ZonesService } from '../../services/zones.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-zones',
  templateUrl: './list-zones.component.html',
  styleUrls: ['./list-zones.component.less']
})
export class ListZonesComponent extends TableHelper implements OnInit {

  id;

  constructor(private zonesServices: ZonesService,
     activatedRoute: ActivatedRoute,
    uiService: UiService,
    private authService: AuthService,
    router: Router) {
    super(uiService, router, activatedRoute);
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.persistentFilters = {
      route_id: this.id
    };
  }

  ngOnInit() {
    this.initTableData(this.zonesServices);
  }


  deleteZone(id: number) {
    if (!this.authService.hasPermission('bus:zones:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta zona?', 'Eliminar').subscribe(result => {
      if (result) {
        this.zonesServices.delete(id).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Zona eliminada com sucesso');
          this.initTableData(this.zonesServices);
        });
      }
    });
  }



  returnButton() {
    this.router.navigateByUrl('bus/routes/list');
  }

  form() {
    this.router.navigateByUrl('bus/routes/zones/form/' + this.id);
  }

}
