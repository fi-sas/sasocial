import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormLinkComponent } from './pages/form-link/form-link.component';
import { FormPriceTableComponent } from './pages/form-price-table/form-price-table.component';
import { FormRouteComponent } from './pages/form-route/form-route.component';
import { FormTicketConfigComponent } from './pages/form-ticket-config/form-ticket-config.component';
import { FromTimetableComponent } from './pages/form-timetable/from-timetable.component';
import { FormZoneComponent } from './pages/form-zone/form-zone.component';
import { ListLinksComponent } from './pages/list-links/list-links.component';
import { ListPricesTablesComponent } from './pages/list-prices-tables/list-prices-tables.component';
import { ListRoutesComponent } from './pages/list-routes/list-routes.component';
import { ListTicketsBoughtComponent } from './pages/list-tickets-bought/list-tickets-bought.component';
import { ListTimetablesComponent } from './pages/list-timetables/list-timetables.component';
import { ListZonesComponent } from './pages/list-zones/list-zones.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListRoutesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:routes:read'},
  },
  {
    path: 'create',
    component: FormRouteComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'bus:routes:create'},
  },
  {
    path: 'link/create',
    component: FormLinkComponent,
    data: { breadcrumb: 'Criar ligação', title: 'Criar ligação', scope: 'bus:links:create'},
  },
  {
    path: 'link/list/:id',
    component: ListLinksComponent,
    data: { breadcrumb: 'Listar ligações', title: 'Listar ligações', scope: 'bus:links:read'},
  },
  {
    path: 'timetables/list/:id',
    component: ListTimetablesComponent,
    data: { breadcrumb: 'Listar horários', title: 'Listar horários', scope: 'bus:timetables:read'},
  },
  {
    path: 'timetables/form/:id',
    component: FromTimetableComponent,
    data: { breadcrumb: 'Listar horários', title: 'Listar horários', scope: 'bus:timetables:update'},
  },
  {
    path: 'price-table/list/:id',
    component: ListPricesTablesComponent,
    data: { breadcrumb: 'Listar tabela de preços', title: 'Listar tabela de preços', scope: 'bus:price_tables:read'},
  },
  {
    path: 'price-table/form/:id',
    component: FormPriceTableComponent,
    data: { breadcrumb: 'Criar tabela de preços', title: 'Criar tabela de preços', scope: 'bus:price_tables:create'},
  },
  {
    path: 'zones/list/:id',
    component: ListZonesComponent,
    data: { breadcrumb: 'Listar zonas', title: 'Listar zonas', scope: 'bus:zones:read'},
  },
  {
    path: 'zones/form/:id',
    component: FormZoneComponent,
    data: { breadcrumb: 'Criar zona', title: 'Criar zona', scope: 'bus:zones:create'},
  },
  {
    path: 'tickets-bought/list/:id',
    component: ListTicketsBoughtComponent,
    data: { breadcrumb: 'Listar bilhetes', title: 'Listar bilhetes', scope: 'bus:tickets_bought:read'},
  },
  {
    path: 'ticket-config/form/:id',
    component: FormTicketConfigComponent,
    data: { breadcrumb: 'Configuração do bilhete', title: 'Configuração do bilhete', scope: 'bus:ticket_config:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutesBusRoutingModule { }
