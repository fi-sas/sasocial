import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { RoutesBusRoutingModule } from './route-routing.module';
import { FormLinkComponent } from './pages/form-link/form-link.component';
import { FormRouteComponent } from './pages/form-route/form-route.component';
import { ListRoutesComponent } from './pages/list-routes/list-routes.component';
import { ListTimetablesComponent } from './pages/list-timetables/list-timetables.component';
import { FromTimetableComponent } from './pages/form-timetable/from-timetable.component';
import { FormPriceTableComponent } from './pages/form-price-table/form-price-table.component';
import { ListPricesTablesComponent } from './pages/list-prices-tables/list-prices-tables.component';
import { FormZoneComponent } from './pages/form-zone/form-zone.component';
import { ListZonesComponent } from './pages/list-zones/list-zones.component';
import { ListLinksComponent } from './pages/list-links/list-links.component';
import { ListTicketsBoughtComponent } from './pages/list-tickets-bought/list-tickets-bought.component';
import { FormTicketConfigComponent } from './pages/form-ticket-config/form-ticket-config.component';

@NgModule({
  declarations: [
    FormLinkComponent,
    ListLinksComponent,
    FormRouteComponent,
    ListRoutesComponent,
    ListTimetablesComponent,
    FromTimetableComponent,
    FormPriceTableComponent,
    ListPricesTablesComponent,
    FormZoneComponent,
    ListZonesComponent,
    ListTicketsBoughtComponent,
    FormTicketConfigComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RoutesBusRoutingModule
  ]

})
export class RouteBusModule { }
