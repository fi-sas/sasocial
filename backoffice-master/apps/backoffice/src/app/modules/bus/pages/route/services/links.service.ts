

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { LinkModel } from '../../../models/link.model';


@Injectable({
  providedIn: 'root'
})
export class LinksService extends Repository<LinkModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.LINKS_ID';
    this.entities_url = 'BUS.LINKS';
  }
  
}


