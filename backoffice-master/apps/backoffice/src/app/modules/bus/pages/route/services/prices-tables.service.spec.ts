import { TestBed, inject } from '@angular/core/testing';
import { PricesTablesService } from './prices-tables.service';


describe('PricesTablesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PricesTablesService]
    });
  });

  it('should be created', inject([PricesTablesService], (service: PricesTablesService) => {
    expect(service).toBeTruthy();
  }));
});
