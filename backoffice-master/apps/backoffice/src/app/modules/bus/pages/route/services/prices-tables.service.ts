

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { PricesTablesModel } from '../../../models/prices-tables.model';


@Injectable({
  providedIn: 'root'
})
export class PricesTablesService extends Repository<PricesTablesModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.PRICE_TABLES_ID';
    this.entities_url = 'BUS.PRICE_TABLES';
  }
  
}

