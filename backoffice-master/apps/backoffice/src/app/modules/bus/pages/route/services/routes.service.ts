

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { RouteModel } from '../../../models/routes.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RoutesService extends Repository<RouteModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.ROUTES_ID';
    this.entities_url = 'BUS.ROUTES';
  }
  updateRouteName(id: number, name: string): Observable<Resource<RouteModel>> {

    const route_name = {
      route_name: name
    };

    return this.resourceService.update<RouteModel>(this.urlService.get('ROUTES_NAME', { id }), route_name);
  }

  updateRouteLocal(id: number, local_id: number, instruction: string, distance: number): Observable<Resource<RouteModel>> {

    const local = {
      id: local_id,
      instruction: instruction,
      distance: distance
    };

    return this.resourceService.update<RouteModel>(this.urlService.get('ROUTES_LOCAL', { id }), local);
  }

}

