import { TestBed, inject } from '@angular/core/testing';
import { TicketsBoughtsService } from './tickets-boughts.service';


describe('TicketsBoughtsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketsBoughtsService]
    });
  });

  it('should be created', inject([TicketsBoughtsService], (service: TicketsBoughtsService) => {
    expect(service).toBeTruthy();
  }));
});
