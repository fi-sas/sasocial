

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { RouteModel } from '../../../models/routes.model';
import { Observable } from 'rxjs';
import { TicketsBoughtModel } from '../../../models/tickets_bought.model';


@Injectable({
  providedIn: 'root'
})
export class TicketsBoughtsService extends Repository<TicketsBoughtModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.TICKETS_BOUGHT';
    this.entities_url = 'BUS.TICKETS_BOUGHT';
  }

  confirm(id: number): Observable<Resource<TicketsBoughtModel>> {
    return this.resourceService.update<TicketsBoughtModel>(this.urlService.get('BUS.TICKETS_BOUGHT_ID_CONFIRM', {id}),{})
  }
  
}

