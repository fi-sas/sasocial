import { TestBed, inject } from '@angular/core/testing';
import { TicketsConfigsService } from './tickets-configs.service';

describe('TicketsConfigsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketsConfigsService]
    });
  });

  it('should be created', inject([TicketsConfigsService], (service: TicketsConfigsService) => {
    expect(service).toBeTruthy();
  }));
});
