

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { TicketsConfigsModel } from '../../../models/tickets_configs.model';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TicketsConfigsService extends Repository<TicketsConfigsModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.TICKET_CONFIG_ID';
    this.entities_url = 'BUS.TICKET_CONFIG';
  }

  readByRoute(route_id?: number) : Observable<Resource<TicketsConfigsModel>> {
    let params = new HttpParams();
    if (route_id) {
      params = params.set('query[route_id]', route_id.toString());
    }
    return this.resourceService.read<TicketsConfigsModel>(this.urlService.get('BUS.TICKET_CONFIG',{}),{ params })
  }

}

