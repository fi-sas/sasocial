

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { HourUpdate, TimetableModel} from '@fi-sas/backoffice/modules/bus/models/timetable.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TimetableService extends Repository<TimetableModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.TIMETABLES_ID';
    this.entities_url = 'BUS.TIMETABLES';
  }
  deleteLine(timetable_id : number, line_number: number): any {
    return this.resourceService.delete(this.urlService.get('BUS.TIMETABLE_DELETE_LINE', {timetable_id, line_number} ), {});
  }

  updateHour(id: number, hour: string): Observable<Resource<TimetableModel>> {
    const hourUpdate = new HourUpdate();
    hourUpdate.id = id;
    hourUpdate.hour = hour;
    return this.resourceService.update<TimetableModel>(this.urlService.get('BUS.TIMETABLE_UPDATE_HOUR', {}), hourUpdate)
  }

}

