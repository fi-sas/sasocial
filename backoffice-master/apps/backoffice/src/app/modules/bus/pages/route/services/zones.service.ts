

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ZoneModel } from '../../../models/zones.model';


@Injectable({
  providedIn: 'root'
})
export class ZonesService extends Repository<ZoneModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.ZONES_ID';
    this.entities_url = 'BUS.ZONES';
  }

}

