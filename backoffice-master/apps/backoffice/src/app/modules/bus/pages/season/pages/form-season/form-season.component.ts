import { Component, OnInit } from '@angular/core';
import { SeasonModel } from '@fi-sas/backoffice/modules/bus/models/seasons.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { SeasonsService } from '../../services/seasons.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-season',
  templateUrl: './form-season.component.html',
  styleUrls: ['./form-season.component.less']
})
export class FormSeasonComponent implements OnInit {

  seasons: SeasonModel[];
  seasonForm: FormGroup;
  isLoading = false;
  isUpdate = false;
  idUpdate;
  updateSeason: SeasonModel = null;
  isSubmited = false;

  dateFormat = 'yyyy/MM/dd';

  constructor(
    private seasonsService: SeasonsService,
    private route: ActivatedRoute,
    private router: Router,
    private uiService: UiService,
    private authService: AuthService
  ) {

    this.seasonForm = new FormGroup({
      name: new FormControl('', [Validators.required, trimValidation]),
      active: new FormControl(true, Validators.required),
      dates: new FormControl('', Validators.required)
    });
  }

  get f() { return this.seasonForm.controls; }

  ngOnInit() {
    this.idUpdate = this.route.snapshot.paramMap.get('id');
    if (this.idUpdate) {
     this.seasonById();
    }
  }

  seasonById() {
    this.seasonsService.read(this.idUpdate).pipe(first()).subscribe((data)=> {
      this.updateSeason = data.data[0];
      this.seasonForm.patchValue({
        name: this.updateSeason.name,
        active: this.updateSeason.active,
        dates: [this.updateSeason.date_begin, this.updateSeason.date_end],
      });
      this.isUpdate = true;
    })
  }

  submit(value: any, valid: boolean) {
    if (!this.isUpdate && !this.authService.hasPermission('bus:seasons:create')) {
      return;
    }
    if (this.isUpdate && !this.authService.hasPermission('bus:seasons:update')) {
      return;
    }

    this.isSubmited = true;

    const season_model = new SeasonModel();


    if (!valid) {
      this.seasonForm.updateValueAndValidity();
      return;
    }

    this.seasonForm.disable();
    this.isLoading = true;

    season_model.name = value.name;
    season_model.active = value.active;
    season_model.date_begin = value.dates[0];
    season_model.date_end = value.dates[1];
    if(this.isUpdate) {
      this.seasonsService.update(this.idUpdate,season_model)
      .pipe(
        first(),
        finalize(
          () => {
            this.isLoading = false;
          }
        )
      )
      .subscribe(season => {
        this.router.navigate(['bus', 'season','list']);
        this.seasons = season.data;
        this.uiService.showMessage(
          MessageType.success,
          'Época alterada com sucesso'
        );
      });
    }else {
      this.seasonsService.create(season_model)
      .pipe(
        first(),
        finalize(
          () => {
            this.isLoading = false;
          }
        )
      )
      .subscribe(season => {
        this.router.navigate(['bus', 'season','list']);
        this.seasons = season.data;
        this.uiService.showMessage(
          MessageType.success,
          'Época criada com sucesso'
        );
      });
    }
   
  }

  returnButton() {
    this.router.navigate(['bus', 'season','list']);
  }

 
}
