import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { SeasonsService } from '../../services/seasons.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-seasons',
  templateUrl: './list-seasons.component.html',
  styleUrls: ['./list-seasons.component.less']
})
export class ListSeasonsComponent extends TableHelper implements OnInit {



  constructor(private seasonsService: SeasonsService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.initTableData(this.seasonsService);
  }

  deleteSeason(id: number) {
    if (!this.authService.hasPermission('bus:seasons:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta época?', 'Eliminar').subscribe(result => {
      if (result) {
        this.seasonsService.delete(id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, "Época eliminada com sucesso");
          this.initTableData(this.seasonsService);
        });
      }
    });
  }



}
