import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSeasonComponent } from './pages/form-season/form-season.component';
import { ListSeasonsComponent } from './pages/list-seasons/list-seasons.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSeasonsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:seasons:read'},
  },
  {
    path: 'create',
    component: FormSeasonComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'bus:seasons:create'},
  },
  {
    path: 'update/:id',
    component: FormSeasonComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'bus:seasons:update'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeasonBusRoutingModule { }
