import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { SeasonBusRoutingModule } from './season-routing.module';
import { FormSeasonComponent } from './pages/form-season/form-season.component';
import { ListSeasonsComponent } from './pages/list-seasons/list-seasons.component';

@NgModule({
  declarations: [
    ListSeasonsComponent,
    FormSeasonComponent
  ],
  imports: [
    CommonModule,
    AngularOpenlayersModule,
    SharedModule,
    SeasonBusRoutingModule
  ]

})
export class SeasonBusModule { }
