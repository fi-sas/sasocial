

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SeasonModel } from '../../../models/seasons.model';


@Injectable({
    providedIn: 'root'
})
export class SeasonsService extends Repository<SeasonModel>{

    constructor(
        resourceService: FiResourceService,
        urlService: FiUrlService
    ) {
        super(resourceService, urlService);
        this.entity_url = 'BUS.SEASONS_ID';
        this.entities_url = 'BUS.SEASONS';
    }

}
