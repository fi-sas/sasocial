import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { RouteModel } from '@fi-sas/backoffice/modules/bus/models/routes.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import * as moment from 'moment';
import { TicketsBoughtsService } from '../../../route/services/tickets-boughts.service';

@Component({
  selector: 'fi-sas-list-tickets',
  templateUrl: './list-tickets.component.html',
  styleUrls: ['./list-tickets.component.less']
})
export class ListTicketsComponent extends TableHelper implements OnInit {
  id: number|string;
  filter_date: Date[];
  routes: RouteModel[] = [];

  constructor(
    private ticketsBoughtService: TicketsBoughtsService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
      super(uiService, router, activatedRoute);
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.persistentFilters = {
        withRelated: 'price,departure_local,arrival_local,user',
        route_id: this.id
      };
     }

  ngOnInit() {
    this.initTableData(this.ticketsBoughtService);
  }


  confirmTicket(id: number, value: boolean) {
    if (!this.authService.hasPermission('bus:tickets_bought') || !value) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Confirmar', 'Pretende confirmar este bilhete?', 'Confirmar').subscribe(result => {
      if (result) {
        this.ticketsBoughtService.confirm(id).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Bilhete confirmado com sucesso');
          this.initTableData(this.ticketsBoughtService);
        });
      }
    });
  }

  listComplete() {
    this.filters.user_id = null;
    this.filters.date = null;
    this.filter_date = null;
    this.searchData(true)
  }

  updateFilters() {
    if (this.filter_date) {
        if(this.filter_date.length>0) {
            const temp = this.filter_date;
            this.filters['created_at'] = {};
            this.filters['created_at']['gte'] = moment(temp[0]).format('YYYY-MM-DD');
            this.filters['created_at']['lte'] = moment(temp[1]).format('YYYY-MM-DD');
        }else{
            this.filters['created_at'] = {};
            this.filter_date = null;
        }

    }
    this.searchData(true);
  }

}
