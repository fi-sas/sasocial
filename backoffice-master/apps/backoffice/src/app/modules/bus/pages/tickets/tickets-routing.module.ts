import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListTicketsComponent } from './pages/list-tickets/list-tickets.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTicketsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:locals:read'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketsBusRoutingModule { }
