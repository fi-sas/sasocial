import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsBusRoutingModule } from './tickets-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListTicketsComponent } from './pages/list-tickets/list-tickets.component';

@NgModule({
  declarations: [ListTicketsComponent],
  imports: [
    CommonModule,
    TicketsBusRoutingModule,
    SharedModule,
  ]
})
export class TicketsBusModule { }
