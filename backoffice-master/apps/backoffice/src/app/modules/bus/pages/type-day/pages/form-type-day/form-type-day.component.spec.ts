import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTypeDayComponent } from './form-type-day.component';

describe('FormTypeDayComponent', () => {
  let component: FormTypeDayComponent;
  let fixture: ComponentFixture<FormTypeDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTypeDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTypeDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
