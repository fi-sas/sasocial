import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize, first } from 'rxjs/operators';
import { TypesDaysModel} from '@fi-sas/backoffice/modules/bus/models/types_days.model';
import { DaysWeeksModel} from '@fi-sas/backoffice/modules/bus/models/types_days.model';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router } from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { TypesDaysService } from '../../services/types-days.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-type-day',
  templateUrl: './form-type-day.component.html',
  styleUrls: ['./form-type-day.component.less']
})
export class FormTypeDayComponent implements OnInit {

  type_day : TypesDaysModel[];
  daysWeeks: DaysWeeksModel[];
  typeDayForm: FormGroup;
  isLoading = false;
  isSubmited = false;

  constructor(
    private typesDaysService : TypesDaysService,
    private uiService: UiService,
    private router: Router,
    private authService: AuthService
  ) {

    this.typeDayForm = new FormGroup({
      name: new FormControl('',[Validators.required,trimValidation]),
      days_week_ids: new FormControl(null, Validators.required),
      active: new FormControl(true, Validators.required),
    });
  }

  get f() { return this.typeDayForm.controls; }

  ngOnInit() {
    this.getDaysWeek();
  }
  
  getDaysWeek(): void {

    this.typesDaysService.getDaysWeeks().pipe(first()).subscribe(daysWeeks => {
      this.daysWeeks = daysWeeks.data;
    });
  }

  submit(value: any, valid: boolean) {
    if(!this.authService.hasPermission('bus:types_days:create')){
        return;
    }
    this.isSubmited = true;
    if (!valid) {
      this.typeDayForm.updateValueAndValidity();
      return;
    }
    this.typeDayForm.disable();
    this.isLoading = true;

    let body = {
      name: this.typeDayForm.get('name').value,
      active: this.typeDayForm.get('active').value
    }

    if(this.typeDayForm.get('days_week_ids').value) {
      body['days_week_ids'] = this.typeDayForm.get('days_week_ids').value;
    }


    this.typesDaysService.create(value)
    .pipe(
      first(),
      finalize(
        () => {
          this.isLoading = false;
        }
      )
    )
    .subscribe(type_day => {
      this.type_day = type_day.data;
      this.router.navigate(['bus','type-day','list']);
      this.uiService.showMessage(
        MessageType.success,
        'Tipo de dia criado com sucesso'
      );
    });

  }



}
