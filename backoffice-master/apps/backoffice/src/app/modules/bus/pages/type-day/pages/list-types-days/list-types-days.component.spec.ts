import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypesDaysComponent } from './list-types-days.component';

describe('ListTypesDaysComponent', () => {
  let component: ListTypesDaysComponent;
  let fixture: ComponentFixture<ListTypesDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTypesDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypesDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
