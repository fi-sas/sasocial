import { DaysWeeksModel, TypesDaysModel } from '@fi-sas/backoffice/modules/bus/models/types_days.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { TypesDaysService } from '../../services/types-days.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';

@Component({
  selector: 'fi-sas-list-types-days',
  templateUrl: './list-types-days.component.html',
  styleUrls: ['./list-types-days.component.less']
})
export class ListTypesDaysComponent extends TableHelper implements OnInit {

  editTypeDayIdName = 0;
  day;
  daysWeeks: DaysWeeksModel[] = [];

  constructor(private typesDaysService: TypesDaysService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.initTableData(this.typesDaysService);
    this.getDaysWeek();
  }


  getDaysWeek(): void {

    this.typesDaysService.getDaysWeeks().pipe(first()).subscribe(daysWeeks => {
      this.daysWeeks = daysWeeks.data;
    });
  }

  deleteTypeDay(id: number) {
    if (!this.authService.hasPermission('bus:types_days:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este tipo de dia?', 'Eliminar').subscribe(result => {
      if (result) {
        this.typesDaysService.delete(id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, "Tipo de dia eliminado com sucesso");
          this.initTableData(this.typesDaysService);
        });
      }
    })
  }

  deleteDay(id: number, day_id: number) {

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este dia?', 'Eliminar').subscribe(result => {
      if (result) {
        this.typesDaysService.deleteDay(day_id).pipe(first()).subscribe(() => {
          this.uiService.showMessage(MessageType.success, "Dia eliminado com sucesso");
          this.initTableData(this.typesDaysService);
        });
      }
    });
  }

  addDay(id: number) {
    if (!this.authService.hasPermission('bus:types_days:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (this.day !== null) {
      this.typesDaysService.addDay(id, parseInt(this.day, 10)).pipe(first()).subscribe(() => {
        this.day = null;
        this.initTableData(this.typesDaysService);
      });
    }
  }

  changeTypeDayName(type_day_id) {
    if (!this.authService.hasPermission('bus:types_days:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (type_day_id !== this.editTypeDayIdName) {
      this.editTypeDayIdName = type_day_id;
    } else {
      this.editTypeDayIdName = 0;
    }
  }

  saveTypeDayName(name, type_day_id) {
    if (!this.authService.hasPermission('bus:types_days:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    let typeDay = new TypesDaysModel();

    typeDay.name = name;

    this.typesDaysService.patch(type_day_id, typeDay).pipe(first()).subscribe(() => {
      this.initTableData(this.typesDaysService);
      this.editTypeDayIdName = 0;
    });
  }

}
