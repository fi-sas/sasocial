import { TestBed, inject } from '@angular/core/testing';

import { TypesDaysService } from './types-days.service';

describe('TypesDaysService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypesDaysService]
    });
  });

  it('should be created', inject([TypesDaysService], (service: TypesDaysService) => {
    expect(service).toBeTruthy();
  }));
});
