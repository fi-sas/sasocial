

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';

import { HttpParams } from '@angular/common/http';
import { DayGroup, DaysWeeksModel, TypesDaysModel } from '../../../models/types_days.model';


@Injectable({
    providedIn: 'root'
})
export class TypesDaysService extends Repository<TypesDaysModel>{

    constructor(
        resourceService: FiResourceService,
        urlService: FiUrlService
    ) {
        super(resourceService, urlService);
        this.entity_url = 'BUS.TYPES_DAYS_ID';
        this.entities_url = 'BUS.TYPES_DAYS';
    }
    getDaysWeeks(): Observable<Resource<DaysWeeksModel>> {
        let params = new HttpParams();
        params = params.set('offset', '0');
        params = params.set('limit', '-1');
        params = params.set('sort', 'id');
        return this.resourceService.list<DaysWeeksModel>(this.urlService.get('BUS.DAYS_WEEKS', {}), { params })
    }


    deleteDay(day_group_id: number): any {
        return this.resourceService.delete(this.urlService.get('BUS.TYPES_DAYS_REMOVE_DAY', { day_group_id }), {})
    }

    addDay(type_day_id: number, day_id: number): Observable<Resource<DayGroup>> {
        const addDay = {
            type_day_id: type_day_id,
            day_id: day_id
        }
        return this.resourceService.create<DayGroup>(this.urlService.get('BUS.TYPES_DAYS_ADD_DAY', {}), addDay)
    }

}

