import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTypeDayComponent } from './pages/form-type-day/form-type-day.component';
import { ListTypesDaysComponent } from './pages/list-types-days/list-types-days.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTypesDaysComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'bus:types_days:read'},
  },
  {
    path: 'create',
    component: FormTypeDayComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'bus:types_days:create'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeDayBusRoutingModule { }
