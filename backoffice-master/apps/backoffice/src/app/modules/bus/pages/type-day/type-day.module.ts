import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { TypeDayBusRoutingModule } from './type-day-routing.module';
import { FormTypeDayComponent } from './pages/form-type-day/form-type-day.component';
import { ListTypesDaysComponent } from './pages/list-types-days/list-types-days.component';

@NgModule({
  declarations: [
    ListTypesDaysComponent,
    FormTypeDayComponent
  ],
  imports: [
    CommonModule,
    AngularOpenlayersModule,
    SharedModule,
    TypeDayBusRoutingModule
  ]

})
export class TypeDayBusModule { }
