import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ConfigurationsModel } from '../models/configurations.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationsService extends Repository<ConfigurationsModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.CONFIGURATIONS_ID';
    this.entities_url = 'BUS.CONFIGURATIONS';
  }

  update_createkey(key: string, valeu_key: string):  Observable<Resource<ConfigurationsModel>>{
    return this.resourceService.update<ConfigurationsModel>(this.urlService.get('BUS.CONFIGURATION_KEY', { key }), { value : valeu_key});
  }
}
