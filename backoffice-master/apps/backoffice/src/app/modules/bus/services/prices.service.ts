import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { PriceModel } from '../models/price.model';

@Injectable({
  providedIn: 'root'
})
export class PricesService extends Repository<PriceModel> {

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'BUS.PRICES_ID';
    this.entities_url = 'BUS.PRICES';
  }

}
