import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { CalendarComponent } from './calendar.components';

const routes: Routes = [
    {
        path: '',
        component: CalendarComponent,
        children: [
            {
                path: 'initial-page',
                component: InitialPageComponent,

            },
            { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
            {
                path: 'categories',
                loadChildren: './pages/categories/categories.module#CategoriesCalendarModule',
                data: { scope: 'calendar:categorys' }
            },
            {
                path: 'events',
                loadChildren: './pages/events/events.module#EventsCalendarModule',
                data: { scope: 'calendar:events' }
            },
           
            {
                path: '**',
                component: PageNotFoundComponent,
                data: {
                    breadcrumb: 'Página não encontrada',
                    title: 'Página não encontrada'
                }
            }
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CalendarRoutingModule { }
