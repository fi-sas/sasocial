import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
    selector: 'fi-sas-s',
    template: '<router-outlet></router-outlet>'
})
export class CalendarComponent implements OnInit {
    dataConfiguration: any;
    constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
        this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();

    }

    ngOnInit() {
        this.uiService.setSiderTitle(this.validTitleTraductions(18), 'calendar');

        this.uiService.setContentWrapperActive(true);
        this.uiService.removeSiderItems();

        const events = new SiderItem('Eventos', '', '', 'calendar:events');
        events.addChild(new SiderItem('Criar', '', '/calendar/events/create', 'calendar:events:create'));
        events.addChild(new SiderItem('Listar', '', '/calendar/events/list', 'calendar:events:read'));

        this.uiService.addSiderItem(events);


        const configuraction = new SiderItem('Configurações', '', '', 'calendar:categorys');
        const services = configuraction.addChild(new SiderItem('Categorias'));
        services.addChild(new SiderItem('Criar', '', '/calendar/categories/create', 'calendar:categorys:create'));
        services.addChild(new SiderItem('Listar', '', '/calendar/categories/list', 'calendar:categorys:read'));

        this.uiService.addSiderItem(configuraction);


        this.uiService.setSiderActive(true);
    }

    validTitleTraductions(id: number) {
        return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
    }
}
