import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "@fi-sas/backoffice/shared/shared.module";
import { CalendarRoutingModule } from "./calendar-routing.module";
import { CalendarComponent } from "./calendar.components";


@NgModule({
  declarations: [
    CalendarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CalendarRoutingModule
  ],
  providers:[

  ]
})
export class CalendarModule {}
