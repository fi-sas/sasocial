import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListCalendarEventsComponent } from './list-calendar-events.component';

describe('ListCalendarEventsComponent', () => {
  let component: ListCalendarEventsComponent;
  let fixture: ComponentFixture<ListCalendarEventsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListCalendarEventsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCalendarEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
