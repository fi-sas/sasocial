import { Component, Input, OnInit } from "@angular/core";
import { FullCalendarOptions, EventObject } from 'ngx-fullcalendar';
import { EventsCalendarModel } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";
import * as moment from 'moment';

@Component({
  selector: 'fi-list-calendar-events',
  templateUrl: './list-calendar-events.component.html',
  styleUrls: ['./list-calendar-events.component.less']
})

export class ListCalendarEventsComponent implements OnInit {
  @Input() filtersData: {
    category_id: number,
    city: string,
    start_published_date: Date,
    end_published_date: Date,
    start_date: Date,
    end_date: Date,
    search: string
  } = null;
  options: FullCalendarOptions;
  eventsInCalendar: EventObject[] = [];;
  events: EventsCalendarModel[];
  selectType = 'month';
  modalView = false;
  eventSelected: EventsCalendarModel = new EventsCalendarModel();
  constructor(public eventsService: EventsCalendarService) {

  }

  ngOnInit() {
    this.getEvents();
    this.options = {
      defaultDate: new Date(),
      header: {
        left: '',
        center: '',
        right: '',
      },
      editable: false,
      locale: 'pt',
      droppable: false,
      allDayText: 'Todo o dia',
      eventResizableFromStart: true,
      eventStartEditable: false,
      eventDurationEditable: false
    };
  }

  getEvents() {
    this.eventsService.list(1, -1, null, null,
      {
        withRelated: "images,category,translations,file,number_subscriptions,subscriptions",
        category_id: this.filtersData.category_id ? this.filtersData.category_id : null,
        city: this.filtersData.city ? this.filtersData.city : null,
        start_published_date: this.filtersData.start_published_date ? moment(this.filtersData.start_published_date).format('YYYY-MM-DD') : null,
        end_published_date: this.filtersData.end_published_date ? moment(this.filtersData.end_published_date).format('YYYY-MM-DD') : null,
        start_date: this.filtersData.start_date ? moment(this.filtersData.start_date).format('YYYY-MM-DD') : null,
        end_date: this.filtersData.end_date ? moment(this.filtersData.end_date).format('YYYY-MM-DD') : null,
        searchFields: this.filtersData.search ? 'title' : null,
        search: this.filtersData.search ? this.filtersData.search : null,
      }).subscribe(events => {
        this.events = events.data;
        this.eventsInCalendar = [];
        events.data.forEach(event => this.eventsInCalendar.push({
          id: event.id,
          title: event.translations.find(lang => lang.language_id === 3).title,
          start: event.start_date,
          end: event.end_date,
          backgroundColor: `${event.category.color}`,
          borderColor: `${event.category.color}`,
          startEditable: false,
          allDay: event.all_day
        })
        );
      });
  }

  listComplete() {
    this.filtersData.search = null;
    this.filtersData.end_date = null;
    this.filtersData.start_date = null;
    this.filtersData.start_published_date = null;
    this.filtersData.end_published_date = null;
    this.filtersData.category_id = null;
    this.filtersData.city = null;
    this.getEvents();
  }

  getEventModal(event: any) {
    this.modalView = true;
    this.eventSelected = this.events.find( e => e.id === Number(event.id));
  }

}