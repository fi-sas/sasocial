import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListNormalEventsComponent } from './list-normal-events.component';

describe('ListNormalEventsComponent', () => {
  let component: ListNormalEventsComponent;
  let fixture: ComponentFixture<ListNormalEventsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListNormalEventsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNormalEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
