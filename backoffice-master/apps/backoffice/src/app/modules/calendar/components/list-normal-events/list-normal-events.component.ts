import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { StatusEventos } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";

@Component({
    selector: 'fi-list-normal-events',
    templateUrl: './list-normal-events.component.html',
    styleUrls: ['./list-normal-events.component.less']
})

export class ListNormalEventsComponent extends TableHelper implements OnInit {
    @Input() filtersData = {};
    StatusEventos = StatusEventos;
    constructor(uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private modalService: NzModalService,
        public eventsService: EventsCalendarService) {
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            searchFields: 'title',
            withRelated: 'images,category,translations,file,number_subscriptions,subscriptions'
        };
    }

    ngOnInit() {
        this.filters = this.filtersData;
        this.initTableData(this.eventsService);
    }

    validTranslateTitle(id_trans: number, translation: any[]) {
        return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).title : '';
    }

    validTranslateName(id_trans: number, translation: any[]) {
        return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
    }

    listComplete() {
        this.filters.search = null;
        this.filters.end_date = null;
        this.filters.start_date = null;
        this.filters.start_published_date = null;
        this.filters.end_published_date = null;
        this.filters.category_id = null;
        this.filters.city = null;
        this.searchData(true);
    }

    filter() {
        this.filters = this.filtersData;
        this.searchData(true);
    }

    edit(id: number, status: string) {
        if (!this.authService.hasPermission('calendar:events:update') || status == 'CANCELLED') {
            return;
        }
        this.router.navigateByUrl('/calendar/events/edit/' + id);
    }

    aproveEvent(event) {
        if (!this.authService.hasPermission('calendar:events:status') || event.status == 'CANCELLED' || event.status == 'REJECTED' || event.status == 'APPROVED') {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai aprovar o evento. Tem a certeza que dejesa continuar?',
            nzOnOk: () => {
                const status = 'APPROVE';
                this.eventsService.changeStatus(event.id, status).subscribe((data) => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Evento aprovado com sucesso'
                    );
                    this.initTableData(this.eventsService);
                })
            },
        });
    }

    rejectEvent(event) {
        if (!this.authService.hasPermission('calendar:events:status') || event.status == 'CANCELLED' || event.status == 'REJECTED' || event.status == 'APPROVED') {
            return;
        }

        this.modalService.confirm({
            nzTitle: 'Vai rejeitar o evento. Tem a certeza que dejesa continuar?',
            nzOnOk: () => {
                const status = 'REJECT';
                this.eventsService.changeStatus(event.id, status).subscribe((data) => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Evento rejeitado com sucesso'
                    );
                    this.initTableData(this.eventsService);
                })
            },
        });


    }

    cancelEvent(event) {
        if (!this.authService.hasPermission('calendar:events:status') || event.status == 'CANCELLED' || event.status == 'REJECTED') {
            return;
        }

        this.modalService.confirm({
            nzTitle: 'Vai cancelar o evento. Tem a certeza que dejesa continuar?',
            nzOnOk: () => {
                const status = 'CANCEL';
                this.eventsService.changeStatus(event.id, status).subscribe((data) => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Evento cancelado com sucesso'
                    );
                    this.initTableData(this.eventsService);
                })
            },
        });


    }

    generateReport(event){
        if (!this.authService.hasPermission('calendar:events:report')) {
            return;
        }
        this.eventsService.report(event.id).subscribe((data) => {
            this.uiService.showMessage(
                MessageType.success,
                "Relatório gerado com sucesso"
              );
        })
    }
}