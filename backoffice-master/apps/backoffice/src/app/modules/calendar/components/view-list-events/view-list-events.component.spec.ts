import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewListEventsComponent } from './view-list-events.component';

describe('ViewListEventsComponent', () => {
  let component: ViewListEventsComponent;
  let fixture: ComponentFixture<ViewListEventsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ViewListEventsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewListEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
