import { Component, Input, OnInit } from "@angular/core";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { finalize, first } from "rxjs/operators";
import { EventsCalendarModel } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";

@Component({
    selector: 'fi-view-list-events',
    templateUrl: './view-list-events.component.html',
    styleUrls: ['./view-list-events.component.less']
})

export class ViewListEventsComponent implements OnInit {
    @Input() data: EventsCalendarModel = null;
    loadingReport = false;

    constructor(private eventsCalendarService:EventsCalendarService,
        private uiService: UiService){}

    ngOnInit(){}

    getReport() {
        this.loadingReport = true;
        this.eventsCalendarService.report(this.data.id).pipe(first(),finalize(()=>this.loadingReport = false)).subscribe((data)=> {
            this.uiService.showMessage(
                MessageType.success,
                'Relatório gerado com sucesso'
            );
        })
    }

}