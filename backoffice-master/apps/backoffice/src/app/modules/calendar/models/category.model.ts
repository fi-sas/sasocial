import { TranslationModel } from "../../configurations/models/translations.model";

export class CategoriesCalendarModel {
    color: string;
    active: boolean;
    translations: TranslationModel[];
    id: number;
}