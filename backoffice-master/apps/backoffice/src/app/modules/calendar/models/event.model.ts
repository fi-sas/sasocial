import { CategoriesCalendarModel } from "./category.model";

export class EventsCalendarModel {
    id: number;
    category_id: number;
    longitude: number;
    latitude: number;
    translations: TranslationModel[];
    start_date: Date;
    end_date: Date;
    start_published_date: Date;
    end_published_date: Date;
    responsable: string;
    phone: string;
    email: string;
    link: string;
    city: string;
    address: string;
    address_number: string;
    file_id: string;
    images: any[];
    max_number_subscriptions: number;
    postal_code: string;
    all_day: boolean;
    status: string;
    recurrence_type: string;
    category: CategoriesCalendarModel;
    type_subscription: string;
    file: any = null;
    subscriptions: SubscriptionModel[];
    number_subscriptions: number;
    frequency: string;
    repeat_frequency_number: number;
    repeat_day_monday: boolean;
    repeat_day_tuesday: boolean;
    repeat_day_wednesday: boolean;
    repeat_day_thursday: boolean;
    repeat_day_friday: boolean;
    repeat_day_saturday: boolean;
    repeat_day_sunday: boolean;
    never_ends: boolean;
    ends_in_date: Date;
    end_after_occurrences: number;
}

export class TranslationModel {
    language_id: number;
    title: string;
    description: string
}

export class SubscriptionModel {
    consent_terms: boolean;
    course_degree_id: number;
    course_id: number;
    created_at: Date;
    email: string;
    event_id: number;
    has_removed: boolean;
    id: number;
    name: string;
    phone: string;
    school_id: number;
    type: string;
}

export const StatusEventos = {
    SUBMITTED: { label: 'Submetido', color: 'green' },
    EDITED: { label: 'Editado', color: 'blue' },
    REJECTED: { label: 'Rejeitado', color: 'orange' },
    APPROVED: { label: 'Aprovado', color: 'green' },
    CANCELLED: { label: 'Cancelado', color: 'red' },
    PUBLISHED: { label: 'Publicado', color: 'blue' },
};
  
