import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CategoriesCalendarFormComponent } from './categories-form.component';

describe('CategoriesCalendarFormComponent', () => {
  let component: CategoriesCalendarFormComponent;
  let fixture: ComponentFixture<CategoriesCalendarFormComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CategoriesCalendarFormComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesCalendarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
