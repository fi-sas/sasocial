import { Component, OnInit } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { LanguageModel } from "@fi-sas/backoffice/modules/configurations/models/language.model";
import { LanguagesService } from "@fi-sas/backoffice/modules/configurations/services/languages.service";
import { finalize, first } from "rxjs/operators";
import { CategoriesCalendarModel } from "../../../models/category.model";
import { CategoriesCalendarService } from "../../../services/categories.service";
import * as _ from 'lodash';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";

@Component({
    selector: 'fi-categories-calendar-form',
    templateUrl: './categories-form.component.html',
    styleUrls: ['./categories-form.component.less']
})

export class CategoriesCalendarFormComponent implements OnInit {
    languagesLoading = false;
    languages: LanguageModel[] = null;
    loading = false;
    idEdit: number;
    category: CategoriesCalendarModel = new CategoriesCalendarModel();

    categoryForm = new FormGroup({
        color: new FormControl(null, [Validators.required]),
        translations: new FormArray([]),
        active: new FormControl(true, [Validators.required])
    });

    translations = this.categoryForm.get('translations') as FormArray;

    constructor(public categoryService: CategoriesCalendarService,
        private languagesService: LanguagesService,
        private uiService: UiService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.idEdit = params['id'];
        });

        if (this.idEdit != undefined) {
            this.getDataById(this.idEdit);
        } else {
            this.loadLanguages();
            this.categoryForm.patchValue({
                color: "#000000"
            })
        }
    }

    get f() { return this.categoryForm.controls; }

    getDataById(id: number) {
        this.categoryService.read(id).pipe(
            first()
        ).subscribe(results => {
            this.category = results.data[0];
            this.categoryForm.patchValue({
                ...this.category,
            });
            this.loadLanguages();
        });
    }

    backList() {
        this.router.navigateByUrl('/calendar/categories/list');
    }

    loadLanguages() {
        this.languagesLoading = true;

        this.languagesService
            .list(1, -1, 'order', 'ascend')
            .pipe(
                first(),
                finalize(() => (this.languagesLoading = false))
            )
            .subscribe((results) => {
                this.languages = [
                    results.data.find((l) => l.acronym.toLowerCase() === 'pt'),
                    ...results.data.filter((l) => l.acronym.toLowerCase() !== 'pt'),
                ];
                for (const iterator of this.languages) {
                    this.addTranslation(iterator.id);
                }
            });
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);

        return language ? language.name : 'Sem informação';
    }

    async addTranslation(language_id: number) {
        const translations = this.categoryForm.controls.translations as FormArray;
        if (this.idEdit) {
            const result = await this.category.translations.filter((x) => x.language_id === language_id);
            translations.push(
                new FormGroup({
                    language_id: new FormControl(language_id, Validators.required),
                    name: new FormControl(result[0].name, [Validators.required, trimValidation]),
                    description: new FormControl(result[0].description, [Validators.required, trimValidation]),
                })
            );
        } else {
            translations.push(
                new FormGroup({
                    language_id: new FormControl(language_id, Validators.required),
                    name: new FormControl('', [Validators.required, trimValidation]),
                    description: new FormControl('', [Validators.required, trimValidation]),
                })
            );
        }
    }

    submit(edit: boolean) {
        for (const i in this.categoryForm.controls) {
            if (i) {
                this.categoryForm.controls[i].markAsDirty();
                this.categoryForm.controls[i].updateValueAndValidity();
            }
        }
        for (const t in this.translations.controls) {
            if (t) {
                const tt = this.translations.get(t) as FormGroup;
                for (const i in tt.controls) {
                    if (i) {
                        tt.controls[i].markAsDirty();
                        tt.controls[i].updateValueAndValidity();
                    }
                }
            }
        }
        let sendValues: CategoriesCalendarModel = new CategoriesCalendarModel();
        if (this.categoryForm.valid) {
            this.loading = true;
            sendValues = this.categoryForm.value;
            if (edit) {
                sendValues.id = this.idEdit;
                this.categoryService.update(this.idEdit, sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.loading = false;
                        this.uiService.showMessage(
                            MessageType.success,
                            'Categoria alterada com sucesso'
                        );
                        this.backList();
                    });

            } else {
                this.categoryService.create(sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.loading = false;
                        this.uiService.showMessage(
                            MessageType.success,
                            'Categoria registada com sucesso'
                        );
                        this.backList();
                    });
            }
        }
    }

    copyField(fieldName: string, langId: number) {
        const fromFormControl = this.categoryForm.get(`translations.0.${fieldName}`);
        const toFormControl = this.categoryForm.get(`translations.${langId}.${fieldName}`);
        toFormControl.setValue(fromFormControl.value);
        toFormControl.markAsTouched();
        toFormControl.markAsDirty();
    }

}