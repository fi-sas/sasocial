import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CategoriesCalendarListComponent } from './categories-list.component';

describe('CategoriesCalendarListComponent', () => {
  let component: CategoriesCalendarListComponent;
  let fixture: ComponentFixture<CategoriesCalendarListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CategoriesCalendarListComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesCalendarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
