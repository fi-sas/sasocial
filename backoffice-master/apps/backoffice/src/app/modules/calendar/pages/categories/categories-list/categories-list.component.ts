import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { CategoriesCalendarModel } from "../../../models/category.model";
import { CategoriesCalendarService } from './../../../services/categories.service';

@Component({
    selector: 'fi-categories-calendar-list',
    templateUrl: './categories-list.component.html',
    styleUrls: ['./categories-list.component.less']
})

export class CategoriesCalendarListComponent extends TableHelper implements OnInit {
    status;
    constructor(uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        public authService: AuthService,
        private modalService: NzModalService,
        public categoryService: CategoriesCalendarService) {
        super(uiService, router, activatedRoute);
    }

    ngOnInit() {
        this.status = [
            {
                description: 'Ativo',
                value: true,
            },
            {
                description: 'Desativo',
                value: false,
            },
        ];

        this.persistentFilters = {
            searchFields: 'name,description',
        };
        this.initTableData(this.categoryService);
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true);
    }

    validTranslateName(id_trans: number, translation: any[]) {
        return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
    }

    edit(id: number) {
        if (!this.authService.hasPermission('calendar:categorys:update')) {
            return;
        }
        this.router.navigateByUrl('/calendar/categories/edit/' + id);
    }

    desactive(data: CategoriesCalendarModel) {
        if (!this.authService.hasPermission('calendar:categorys:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle:
                'Vai desativar esta Categoria. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(data),
        });
    }

    desactiveDataSubmit(data: CategoriesCalendarModel) {
        data.active = false;
        this.categoryService.desactive(data.id, data).pipe(first()).subscribe((result) => {
            this.uiService.showMessage(
                MessageType.success,
                'Categoria desativada com sucesso'
            );
            this.initTableData(this.categoryService);
        });
    }

    active(data: CategoriesCalendarModel) {
        if (!this.authService.hasPermission('calendar:categorys:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai ativar esta Categoria. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(data),
        });
    }

    activeDataSubmit(data: CategoriesCalendarModel) {
        data.active = true;
        this.categoryService.active(data.id, data).pipe(first()).subscribe((result) => {
            this.uiService.showMessage(
                MessageType.success,
                'Categoria ativada com sucesso'
            );
            this.initTableData(this.categoryService);
        });
    }
}