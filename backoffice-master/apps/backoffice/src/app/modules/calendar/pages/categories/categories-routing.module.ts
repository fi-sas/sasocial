import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesCalendarFormComponent } from './categories-form/categories-form.component';
import { CategoriesCalendarListComponent } from './categories-list/categories-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: CategoriesCalendarListComponent,
    data: { breadcrumb: 'Categorias', title: 'Listar', scope: 'calendar:categorys:read'}
  },
  {
    path: 'edit/:id',
    component: CategoriesCalendarFormComponent,
    data: { breadcrumb: 'Categoria', title: 'Editar', scope: 'calendar:categorys:update'}
  },
  {
    path: 'create',
    component: CategoriesCalendarFormComponent,
    data: { breadcrumb: 'Categoria', title: 'Criar', scope: 'calendar:categorys:create'}
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesCalendarRoutingModule { }
