import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CategoriesCalendarRoutingModule } from './categories-routing.module';
import { CategoriesCalendarListComponent } from './categories-list/categories-list.component';
import { CategoriesCalendarFormComponent } from './categories-form/categories-form.component';

@NgModule({
  declarations: [
    CategoriesCalendarListComponent,
    CategoriesCalendarFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CategoriesCalendarRoutingModule
  ],
})
export class CategoriesCalendarModule { }
