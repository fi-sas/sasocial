import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { LanguageModel } from "@fi-sas/backoffice/modules/configurations/models/language.model";
import { LanguagesService } from "@fi-sas/backoffice/modules/configurations/services/languages.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { EventsCalendarService } from "../../../services/events.service";
import * as _ from 'lodash';
import { EventsCalendarModel } from "../../../models/event.model";
import { proj } from 'openlayers';
import { cities } from "@fi-sas/backoffice/shared/common/cities";
import { CategoriesCalendarModel } from "../../../models/category.model";
import * as moment from 'moment';
import { CategoriesCalendarService } from "../../../services/categories.service";
import { FilesService } from "@fi-sas/backoffice/modules/medias/services/files.service";
import { Time } from "@angular/common";

@Component({
    selector: 'fi-events-events-form',
    templateUrl: './events-form.component.html',
    styleUrls: ['./events-form.component.less']
})

export class EventsFormComponent implements OnInit {
    languages: LanguageModel[] = [];
    step = 0;
    languagesLoading = false;
    loading = false;
    idEdit: number;
    submitted = false;
    fileListImg = [];
    file = null;
    categories: CategoriesCalendarModel[] = [];
    emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
        '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
        '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
        '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
        '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';
    defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();
    latitude: number = 39.3999;
    longitude: number = -8.2245;
    latitudeInit: number = 39.3999;
    longitudeInit: number = -8.2245;
    listFile = [];
    images = [];
    errorRepeat = false
    eventForm = new FormGroup({
        category_id: new FormControl(null, [Validators.required]),
        longitude: new FormControl(null, [Validators.required]),
        latitude: new FormControl(null, [Validators.required]),
        translations: new FormArray([]),
        start_date: new FormControl(null, [Validators.required]),
        start_hour: new FormControl(null, [Validators.required]),
        end_date: new FormControl(null, [Validators.required]),
        end_hour: new FormControl(null, [Validators.required]),
        start_published_date: new FormControl(null, [Validators.required]),
        end_published_date: new FormControl(null, [Validators.required]),
        responsable: new FormControl('', [Validators.required, trimValidation]),
        phone: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(18)]),
        email: new FormControl('', [Validators.pattern(this.emailRegex), Validators.required, trimValidation]),
        link: new FormControl('', [trimValidation]),
        city: new FormControl('', [trimValidation, Validators.required]),
        address: new FormControl('', [trimValidation, Validators.required]),
        address_number: new FormControl('', [Validators.required]),
        postal_code: new FormControl('', [Validators.required]),
        file_id: new FormControl(null),
        images: new FormControl([], [Validators.required]),
        max_number_subscriptions: new FormControl(''),
        type_subscription: new FormControl(null, [Validators.required]),
        recurrence_type: new FormControl(null, [Validators.required]),
        all_day: new FormControl(false, [Validators.required]),
        repeat_frequency_number: new FormControl(null, [Validators.min(1)]),
        frequency: new FormControl(null),
        repeat_day_monday: new FormControl(false),
        repeat_day_tuesday: new FormControl(false),
        repeat_day_wednesday: new FormControl(false),
        repeat_day_thursday: new FormControl(false),
        repeat_day_friday: new FormControl(false),
        repeat_day_saturday: new FormControl(false),
        repeat_day_sunday: new FormControl(false),
        never_ends: new FormControl(null),
        ends_in_date: new FormControl(null),
        end_after_occurrences: new FormControl(null, [Validators.min(1)]),
        closed_in: new FormControl(null),
    });
    translations = this.eventForm.get('translations') as FormArray;
    cities = cities;
    event: EventsCalendarModel = new EventsCalendarModel();

    readonly minuteStep: number = 1;
    private minuteRange = this.range(60, 0, this.minuteStep);
    protected readonly range24 = this.range(24);

    constructor(public eventService: EventsCalendarService,
        private categoriesService: CategoriesCalendarService,
        private languagesService: LanguagesService,
        private uiService: UiService,
        private route: ActivatedRoute,
        private fileService: FilesService,
        private router: Router) {
    }

    ngOnInit() {
        this.getCategories();
        this.route.params.subscribe(params => {
            this.idEdit = params['id'];
        });

        if (this.idEdit != undefined) {
            this.getDataById(this.idEdit);
        } else {
            this.loadLanguages();
        }

        this.cities = this.cities.sort((a, b) => {
            if (a.city > b.city) {
                return 1;
            }
            if (a.city < b.city) {
                return -1;
            }
            return 0;

        });
    }

    get f() { return this.eventForm.controls; }

    getDataById(id: number) {
        this.eventService.read(id, {
            withRelated: 'images,category,translations,file,number_subscriptions'
        }).pipe(
            first()
        ).subscribe(results => {
            this.event = results.data[0];
            this.eventForm.patchValue({
                ...this.event,
            });
            if (this.eventForm.get('latitude').value != null) {
                this.latitudeInit = this.eventForm.get('latitude').value;
                this.latitude = this.eventForm.get('latitude').value;
            } else {
                this.latitude = 39.3999;
                this.latitudeInit = 39.3999;
            }

            if (this.eventForm.get('longitude').value != null) {
                this.longitudeInit = this.eventForm.get('longitude').value;
                this.longitude = this.eventForm.get('longitude').value;
            } else {
                this.longitudeInit = -8.2245;
                this.longitude = -8.2245;
            }

            if (!this.event.all_day) {
                this.eventForm.get('start_hour').setValue(moment(this.event.start_date).toDate());
                this.eventForm.get('end_hour').setValue(moment(this.event.end_date).toDate());
            }

            if (this.event.never_ends) {
                this.eventForm.get('closed_in').setValue('never');
            } else if(this.event.ends_in_date) {
                this.eventForm.get('closed_in').setValue('date');
            } else if(this.event.end_after_occurrences) {
                this.eventForm.get('closed_in').setValue('after');
            }

            if (this.event.images && this.event.images.length > 0) {
                this.event.images.forEach(file => this.fileListImg.push(file.file_id));
            }

            if (this.event.file_id) {
                this.listFile.push(this.event.file_id);
            }
            this.loadLanguages();
        });
    }

    getValueFormat(value) {
        return Number(value).toFixed(6);
    }

    getCategories() {
        this.categoriesService.list(0, -1).pipe(first()).subscribe((data) => {
            this.categories = data.data;
        })
    }

    findCategory(id: number) {
        return this.categories.find((category) => category.id == id);
    }

    changeCity(event) {
        const findCity = this.cities.find((city) => city.city == event);
        if (findCity) {
            this.longitude = Number(findCity.lng);
            this.latitude = Number(findCity.lat);
            this.latitudeInit = Number(findCity.lat);
            this.longitudeInit = Number(findCity.lng);
            this.eventForm.get('latitude').setValue(this.latitude);
            this.eventForm.get('longitude').setValue(this.longitude);
        }
    }

    backList() {
        this.router.navigateByUrl('/calendar/events/list');
    }

    getLatLong(event) {
        let lonlat = proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
        this.longitude = lonlat[0];
        this.latitude = lonlat[1];
        this.eventForm.get('latitude').setValue(this.latitude);
        this.eventForm.get('longitude').setValue(this.longitude);
    }

    loadLanguages() {
        this.languagesLoading = true;

        this.languagesService
            .list(1, -1, 'order', 'ascend')
            .pipe(
                first(),
                finalize(() => (this.languagesLoading = false))
            )
            .subscribe((results) => {
                this.languages = [
                    results.data.find((l) => l.acronym.toLowerCase() === 'pt'),
                    ...results.data.filter((l) => l.acronym.toLowerCase() !== 'pt'),
                ];
                for (const iterator of this.languages) {
                    this.addTranslation(iterator.id);
                }
            });
    }

    async addTranslation(language_id: number) {
        const translations = this.eventForm.controls.translations as FormArray;
        if (this.idEdit) {
            const result = await this.event.translations.filter((x) => x.language_id === language_id);
            translations.push(
                new FormGroup({
                    language_id: new FormControl(language_id, Validators.required),
                    title: new FormControl(result[0].title, [Validators.required, trimValidation]),
                    description: new FormControl(result[0].description, [Validators.required, trimValidation]),
                })
            );
        } else {
            translations.push(
                new FormGroup({
                    language_id: new FormControl(language_id, Validators.required),
                    title: new FormControl('', [Validators.required, trimValidation]),
                    description: new FormControl('', [Validators.required, trimValidation]),
                })
            );
        }
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }

    onFileAdded(fileId: number) {
        this.fileListImg.push(fileId);
    }

    changeRecurrence(event) {
        if (event == 'RECURRENT') {
            this.eventForm.controls.closed_in.setValidators([Validators.required]);
            this.eventForm.controls.repeat_frequency_number.setValidators([Validators.required, Validators.min(1)]);
            this.eventForm.controls.frequency.setValidators([Validators.required]);
        } else {
            this.eventForm.controls.closed_in.clearValidators();
            this.eventForm.controls.frequency.clearValidators();
            this.eventForm.controls.repeat_frequency_number.clearValidators();
            this.eventForm.controls.repeat_frequency_number.setValidators([Validators.min(1)]);
            this.f.repeat_frequency_number.setValue(null);
            this.f.frequency.setValue(null);
            this.f.repeat_day_monday.setValue(false);
            this.f.repeat_day_tuesday.setValue(false);
            this.f.repeat_day_wednesday.setValue(false);
            this.f.repeat_day_thursday.setValue(false);
            this.f.repeat_day_friday.setValue(false);
            this.f.repeat_day_saturday.setValue(false);
            this.f.repeat_day_sunday.setValue(false);
            this.f.never_ends.setValue(false);
            this.f.ends_in_date.setValue(null);
            this.f.end_after_occurrences.setValue(null);
            this.f.closed_in.setValue(null);
        }
        this.eventForm.controls.closed_in.updateValueAndValidity();
        this.eventForm.controls.frequency.updateValueAndValidity();
        this.eventForm.controls.repeat_frequency_number.updateValueAndValidity();
    }

    changeCloseIn(event) {
        if (event == 'never') {
            this.eventForm.get('never_ends').setValue(true);
            this.f.ends_in_date.setValue(null);
            this.f.end_after_occurrences.setValue(null);
            this.eventForm.controls.ends_in_date.clearValidators();
            this.eventForm.controls.end_after_occurrences.setValidators([Validators.min(1)]);
        } else if (event == 'date') {
            this.eventForm.get('never_ends').setValue(false);
            this.f.end_after_occurrences.setValue(null);
            this.eventForm.controls.ends_in_date.setValidators([Validators.required]);
            this.eventForm.controls.end_after_occurrences.setValidators([Validators.min(1)]);
        } else if (event == 'after') {
            this.eventForm.get('never_ends').setValue(false);
            this.f.ends_in_date.setValue(null);
            this.eventForm.controls.ends_in_date.clearValidators();
            this.eventForm.controls.end_after_occurrences.setValidators([Validators.required, Validators.min(1)]);
        }
        this.eventForm.controls.ends_in_date.updateValueAndValidity();
        this.eventForm.controls.end_after_occurrences.updateValueAndValidity();
    }

    onFileDeletedImg(fileId: number) {
        this.fileListImg = this.fileListImg.filter((f) => f !== fileId);
    }

    onFileAddedOther(fileId) {
        this.listFile.push(fileId);
        this.eventForm.get('file_id').setValue(this.listFile[0]);
    }

    onFileDeleted(fileId: number) {
        this.listFile = this.listFile.filter((f) => f !== fileId);
        this.eventForm.get('file_id').setValue(null);
    }

    disableEndDate = (endDate: Date) => {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 1);
        return moment(endDate).isBefore(currentDate) || moment(this.eventForm.get('start_date').value).isSameOrAfter(endDate);
    };

    disableStartDate = (startDate: Date) => {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 1);
        return moment(startDate).isBefore(currentDate) || moment(this.eventForm.get('end_date').value).isSameOrBefore(startDate);
    };

    disableEndDatePublished = (endDate: Date) => {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 1);
        return moment(endDate).isBefore(currentDate) || moment(this.eventForm.get('start_published_date').value).isSameOrAfter(endDate);
    };

    disableStartDatePublished = (startDate: Date) => {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 1);
        return moment(startDate).isBefore(currentDate) || moment(this.eventForm.get('end_published_date').value).isSameOrBefore(startDate);
    };

    /*disabledStartHours = (hour: any) => {
      const hours = [];
      let endHour = new Date(this.eventForm.get("end_hour").value).getHours();
      if(this.eventForm.get("end_hour").value){
        while(endHour <= 23){
          hours.push(endHour);
          endHour++
        }
      }
      return hours;
    }

    disabledStartHourMinutes = (hour: any) => {
      return [];
    }*/
    disabledStartHours = (hour: number) => {
      if(!this.eventForm.get('end_hour').value){
        return [];
      }
      const initialTime = moment(this.eventForm.get('end_hour').value);
      const beginDate = moment();
      beginDate.set({ hour: initialTime.get('hour'), minute: 0, second: 0, millisecond: 0 });

      const pastHours = this.pastHours();
      return this.range24.filter(
        (hour: number) =>
        pastHours.includes(hour) ||
          this.hourHasAllMinutesDisabled(hour) ||
          moment()
            .set({ hour, minute: 0, second: 0, millisecond: 0 })
            .isAfter(beginDate, 'hour')
      );
    };

    disabledStartMinutes = (hour?: number): number[] => {
      if(!this.eventForm.get('end_hour').value){
        return [];
      }
      const invalidMinutes = this.minuteRange;
      if (isNaN(hour)) {
        return invalidMinutes;
      }

      const disabledPastMinutes = this.disabledFutureMinutes(hour);

      const initialTime = moment(this.eventForm.get('end_hour').value);
      if (hour < initialTime.get('hour')) {
        return disabledPastMinutes;
      }
      const beginDate = moment();
      beginDate.set({ hour: initialTime.get('hour'), minute: initialTime.get('minute'), second: 0, millisecond: 0 });

      const y = invalidMinutes.filter(
        (minute: number) =>
        disabledPastMinutes.includes(minute));

      return invalidMinutes.filter(
        (minute: number) =>
        disabledPastMinutes.includes(minute) ||
          moment()
            .set({ hour: initialTime.get('hour'), minute, second: 0, millisecond: 0 })
            .isAfter(beginDate, 'minute')
      );
    }

    disabledHours = (): number[] => {
      const initialTime = moment(this.eventForm.get('start_hour').value);
      const beginDate = moment();
      beginDate.set({ hour: initialTime.get('hour'), minute: 0, second: 0, millisecond: 0 });

      const disabledFutureHours = this.disabledFutureHours();
      return this.range24.filter(
        (hour: number) =>
          disabledFutureHours.includes(hour) ||
          this.hourHasAllMinutesDisabled(hour) ||
          moment()
            .set({ hour, minute: 0, second: 0, millisecond: 0 })
            .isBefore(beginDate, 'hour')
      );
    };
    disabledFutureHours = (): number[] => {
        const currentHour = moment().get('hour');
        return this.range24.filter((hour: number) => {
          if (currentHour === hour) {
            return this.hourHasAllMinutesDisabled(currentHour);
          }
          return hour > moment().get('hour');
        });
    };

    pastHours = (): number[] => {
      const currentHour = moment().get('hour');
      return this.range24.filter((hour: number) => {
        if (currentHour === hour) {
          return this.hourHasAllMinutesDisabled(currentHour);
        }
        return hour > moment().get('hour');
      });
  };

    private hourHasAllMinutesDisabled(hour: number) {
      return this.disabledMinutes(hour).length === this.minuteRange.length;
    }

    disabledMinutes = (hour?: number): number[] => {
      const invalidMinutes = this.minuteRange;
      if (isNaN(hour)) {
        return invalidMinutes;
      }

      const disabledFutureMinutes = this.disabledFutureMinutes(hour);
      const initialTime = moment(this.eventForm.get('start_hour').value);
      if (hour > initialTime.get('hour')) {
        return disabledFutureMinutes;
      }
      const beginDate = moment();
      beginDate.set({ hour: initialTime.get('hour'), minute: initialTime.get('minute'), second: 0, millisecond: 0 });

      return invalidMinutes.filter(
        (minute: number) =>
          disabledFutureMinutes.includes(minute) ||
          moment()
            .set({ hour: initialTime.get('hour'), minute, second: 0, millisecond: 0 })
            .isSameOrBefore(beginDate, 'minute')
      );
    };


  disabledFutureMinutes = (hour?: number): number[] => {
    const invalidMinutes = this.minuteRange;
    if (isNaN(hour)) {
      return invalidMinutes;
    }
    return invalidMinutes.filter((minute: number) =>
        moment().set({ hour, minute, second: 0, millisecond: 0 }).isSameOrAfter(moment(), 'minute')
      );
  };

    validTimeMin() {
        if (this.f.start_date.value && this.f.end_date.value) {
            const dateStart = moment(this.f.start_date.value).format('DD-MM-YYYY');
            const dateEnd = moment(this.f.end_date.value).format('DD-MM-YYYY');
            if (this.f.start_hour.value && this.f.end_hour.value && dateStart == dateEnd) {
                return moment(this.f.start_hour.value).isAfter(this.f.end_hour.value);
            }
        }

        return false;
    }

    validTimeMax() {
        if (this.f.start_date.value && this.f.end_date.value) {
            const dateStart = moment(this.f.start_date.value).format('DD-MM-YYYY');
            const dateEnd = moment(this.f.end_date.value).format('DD-MM-YYYY');
            if (this.f.start_hour.value && this.f.end_hour.value && dateStart == dateEnd) {
                return moment(this.f.end_hour.value).isBefore(this.f.start_hour.value);
            }
        }

        return false;
    }

    changeTypeSubscription(event) {
        if (event == 'SUB') {
            this.eventForm.controls.max_number_subscriptions.setValidators([Validators.required]);
        } else {
            this.eventForm.controls.max_number_subscriptions.setValue(null);
            this.eventForm.controls.max_number_subscriptions.clearValidators();
        }
        this.eventForm.controls.max_number_subscriptions.updateValueAndValidity();
    }

    copyField(fieldName: string, langId: number) {
        const fromFormControl = this.eventForm.get(`translations.0.${fieldName}`);
        const toFormControl = this.eventForm.get(`translations.${langId}.${fieldName}`);
        toFormControl.setValue(fromFormControl.value);
        toFormControl.markAsTouched();
        toFormControl.markAsDirty();
    }

    changeHour(event) {
        if (event) {
            this.eventForm.controls.start_hour.clearValidators();
            this.eventForm.get('start_hour').setValue(null);
            this.eventForm.controls.end_hour.clearValidators();
            this.eventForm.get('end_hour').setValue(null);
        } else {
            this.eventForm.controls.start_hour.setValidators([Validators.required]);
            this.eventForm.controls.end_hour.setValidators([Validators.required]);
        }
        this.eventForm.controls.end_hour.updateValueAndValidity();
        this.eventForm.controls.start_hour.updateValueAndValidity();
    }

    backStep() {
        this.step -= 1;
    }

    checkValidityOfControls(controls: AbstractControl[]): boolean {
        for (const i in controls) {
            if (i) {
                controls[i].markAsDirty();
                controls[i].updateValueAndValidity();
            }
        }
        let invalid = 0;
        for (const t in this.translations.controls) {
            if (t) {
                const tt = this.translations.get(t) as FormGroup;
                for (const i in tt.controls) {
                    if (i) {
                        tt.controls[i].markAsDirty();
                        tt.controls[i].updateValueAndValidity();
                        if (tt.controls[i].invalid) {
                            invalid++;
                        }
                    }

                }
            }
        }

        this.errorRepeat = false;
        if (this.eventForm.get('recurrence_type').value == 'RECURRENT' && this.eventForm.get('frequency').value == 'WEEKLY') {
            if(!this.f.repeat_day_monday.value && !this.f.repeat_day_tuesday.value && !this.f.repeat_day_wednesday.value && !this.f.repeat_day_thursday.value
                && !this.f.repeat_day_friday.value && !this.f.repeat_day_saturday.value && !this.f.repeat_day_saturday.value) {
                this.errorRepeat = true;
            }
        }

        return !controls.find(c => c.status !== 'VALID') && invalid == 0 && !this.errorRepeat;
    }

    changeRepeat(){
        this.f.repeat_day_monday.setValue(false);
        this.f.repeat_day_tuesday.setValue(false);
        this.f.repeat_day_wednesday.setValue(false);
        this.f.repeat_day_thursday.setValue(false);
        this.f.repeat_day_friday.setValue(false);
        this.f.repeat_day_saturday.setValue(false);
        this.f.repeat_day_saturday.setValue(false);
    }

    next() {
        let valid = false;
        switch (this.step) {
            case 0: {
                this.submitted = true;
                valid = this.checkValidityOfControls([
                    this.eventForm.get('recurrence_type'),
                    this.eventForm.get('closed_in'),
                    this.eventForm.get('ends_in_date'),
                    this.eventForm.get('end_after_occurrences'),
                    this.eventForm.get('repeat_frequency_number'),
                    this.eventForm.get('frequency'),
                    this.eventForm.get('category_id'),
                    this.eventForm.get('start_date'),
                    this.eventForm.get('start_hour'),
                    this.eventForm.get('end_date'),
                    this.eventForm.get('end_hour'),
                    this.eventForm.get('start_published_date'),
                    this.eventForm.get('end_published_date'),
                    this.eventForm.get('responsable'),
                    this.eventForm.get('email'),
                    this.eventForm.get('phone'),
                    this.eventForm.get('type_subscription'),
                    this.eventForm.get('max_number_subscriptions'),
                ]) && !this.validTimeMin() && !this.validTimeMax();
                break;
            }
            case 1: {
                valid = this.checkValidityOfControls([
                    this.eventForm.get('address'),
                    this.eventForm.get('address_number'),
                    this.eventForm.get('postal_code'),
                    this.eventForm.get('city')
                ]);
                break;
            }
            case 2: {
                this.submitted = true;
                valid = this.fileListImg.length != 0;
                if (this.eventForm.get('file_id').value) {
                    this.loadFileId();
                }
                this.loadMedias();
                break;
            }
        }

        if (valid) {
            this.step += 1;
            this.submitted = false;
        }
    }

    loadMedias() {
        this.fileService
            .listFilesByIDS(this.convertGalleryToFilesIDS())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.images = files.data;
                }
            );
    }

    selectDay(day: string) {
        this.errorRepeat = false;
        switch (day) {
            case 'segunda':
                this.f.repeat_day_monday.value ? this.eventForm.get('repeat_day_monday').setValue(false) : this.eventForm.get('repeat_day_monday').setValue(true);;
                break;
            case 'terça':
                this.f.repeat_day_tuesday.value ? this.eventForm.get('repeat_day_tuesday').setValue(false) : this.eventForm.get('repeat_day_tuesday').setValue(true);;
                break;
            case 'quarta':
                this.f.repeat_day_wednesday.value ? this.eventForm.get('repeat_day_wednesday').setValue(false) : this.eventForm.get('repeat_day_wednesday').setValue(true);;
                break;
            case 'quinta':
                this.f.repeat_day_thursday.value ? this.eventForm.get('repeat_day_thursday').setValue(false) : this.eventForm.get('repeat_day_thursday').setValue(true);;
                break;
            case 'sexta':
                this.f.repeat_day_friday.value ? this.eventForm.get('repeat_day_friday').setValue(false) : this.eventForm.get('repeat_day_friday').setValue(true);;
                break;
            case 'sabado':
                this.f.repeat_day_saturday.value ? this.eventForm.get('repeat_day_saturday').setValue(false) : this.eventForm.get('repeat_day_saturday').setValue(true);;
                break;
            case 'domingo':
                this.f.repeat_day_sunday.value ? this.eventForm.get('repeat_day_sunday').setValue(false) : this.eventForm.get('repeat_day_sunday').setValue(true);;
                break;
        }
    }

    loadFileId() {
        this.fileService
            .get(this.eventForm.get('file_id').value)
            .pipe(first())
            .subscribe(
                (files) => {
                    this.file = files.data[0];
                }
            );
    }

    convertGalleryToFilesIDS() {
        let filesIDS = [];
        this.fileListImg.forEach((fileID) => {
            filesIDS.push(fileID);
        });

        return filesIDS;
    }

    submit(edit: boolean) {
        let auxImg = [];
        this.fileListImg.forEach((file) => {
            auxImg.push(file);
        });
        this.eventForm.get('images').setValue(auxImg);
        if (this.eventForm.valid) {
            this.loading = true;
            this.submitted = false;
            let date_start;
            let date_end;
            let sendValues: EventsCalendarModel = new EventsCalendarModel();
            sendValues = this.eventForm.value;
            if (!this.eventForm.get('all_day').value) {
                const hourStart = moment(this.eventForm.get('start_hour').value).format("HH:mm:ss");
                date_start = moment(this.eventForm.get('start_date').value).format("YYYY-MM-DD") + ' ' + hourStart;
                const hour2End = moment(this.eventForm.get('end_hour').value).format("HH:mm:ss");
                date_end = moment(this.eventForm.get('end_date').value).format("YYYY-MM-DD") + ' ' + hour2End;
            } else {
                date_start = moment(this.eventForm.get('start_date').value).format("YYYY-MM-DD") + ' ' + '00:00:00';
                date_end = moment(this.eventForm.get('end_date').value).format("YYYY-MM-DD") + ' ' + '23:59:59';
            }
            sendValues.start_date = date_start;
            sendValues.end_date = date_end;
            if (edit) {
                sendValues.id = this.idEdit;
                this.eventService.update(this.idEdit, sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.loading = false;
                        this.uiService.showMessage(
                            MessageType.success,
                            'Evento alterado com sucesso'
                        );
                        this.backList();
                    });

            } else {
                this.eventService.create(sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.loading = false;
                        this.uiService.showMessage(
                            MessageType.success,
                            'Evento registado com sucesso'
                        );
                        this.backList();
                    });
            }
        }
    }

    protected range(to: number, from: number = 0, step: number = 1): number[] {
      const r: number[] = [];
      for (let i = from; i < to; i += step) {
        r.push(i);
      }
      return r;
    }

}
