import { Component, OnInit, ViewChild } from "@angular/core";
import { cities } from "@fi-sas/backoffice/shared/common/cities";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { ListCalendarEventsComponent } from "../../../components/list-calendar-events/list-calendar-events.component";
import { ListNormalEventsComponent } from "../../../components/list-normal-events/list-normal-events.component";
import { CategoriesCalendarModel } from "../../../models/category.model";
import { CategoriesCalendarService } from "../../../services/categories.service";

@Component({
    selector: 'fi-events-events-form',
    templateUrl: './events-list.component.html',
    styleUrls: ['./events-list.component.less']
})

export class EventsListComponent implements OnInit {
    @ViewChild("dataListNormalEvent", { static: false }) dataListNormalEvents: ListNormalEventsComponent = null;
    @ViewChild("dataListCalendarEvent", { static: false }) dataListCalendarEvent: ListCalendarEventsComponent = null;
    categories: CategoriesCalendarModel[] = [];
    cities = cities;
    viewSelected: string = 'list';
    filters: any = {};
    constructor(
        private categoriesService: CategoriesCalendarService, private authService: AuthService) {
    }

    ngOnInit() {
        this.getCategories();
        this.cities = this.cities.sort((a, b) => {
            if (a.city > b.city) {
                return 1;
            }
            if (a.city < b.city) {
                return -1;
            }
            return 0;

        });

    }

    getCategories() {
        this.categoriesService.list(0, -1).pipe(first()).subscribe((data) => {
            this.categories = data.data;
        })
    }

    listComplete() {
        this.filters.search = null;
        this.filters.end_date = null;
        this.filters.start_date = null;
        this.filters.start_published_date = null;
        this.filters.end_published_date = null;
        this.filters.category_id = null;
        this.filters.city = null;
        if (this.viewSelected == 'list') {
            this.dataListNormalEvents.listComplete();
        } else {
            this.dataListCalendarEvent.listComplete();
        }
    }

    filter() {
        if (this.viewSelected == 'list') {
            this.dataListNormalEvents.filter();
        } else {
            this.dataListCalendarEvent.getEvents();
        }
    }

}