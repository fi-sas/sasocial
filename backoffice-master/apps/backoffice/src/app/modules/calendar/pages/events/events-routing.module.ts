import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsFormComponent } from './events-form/events-form.component';
import { EventsListComponent } from './events-list/events-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'edit/:id',
    component: EventsFormComponent,
    data: { breadcrumb: 'Eventos', title: 'Editar', scope: 'calendar:events:update'}
  },
  {
    path: 'create',
    component: EventsFormComponent,
    data: { breadcrumb: 'Eventos', title: 'Criar', scope: 'calendar:events:create'}
  },
  {
    path: 'list',
    component: EventsListComponent,
    data: { breadcrumb: 'Eventos', title: 'Listar', scope: 'calendar:events:read'}
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsCalendarRouting { }
