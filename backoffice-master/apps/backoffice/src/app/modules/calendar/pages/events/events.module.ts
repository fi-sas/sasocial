import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { EventsCalendarRouting } from './events-routing.module';
import { EventsFormComponent } from './events-form/events-form.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { ListCalendarEventsComponent } from '../../components/list-calendar-events/list-calendar-events.component';
import { ListNormalEventsComponent } from '../../components/list-normal-events/list-normal-events.component';
import { ViewListEventsComponent } from '../../components/view-list-events/view-list-events.component';
import { NgxFullCalendarModule } from "ngx-fullcalendar";
import { EventsListComponent } from './events-list/events-list.component';

@NgModule({
  declarations: [
    EventsFormComponent,
    EventsListComponent,
    ListCalendarEventsComponent,
    ListNormalEventsComponent,
    ViewListEventsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EventsCalendarRouting,
    AngularOpenlayersModule,
    NgxFullCalendarModule
  ],
})
export class EventsCalendarModule { }
