import { TestBed, inject } from '@angular/core/testing';
import { CategoriesCalendarService } from './categories.service';


describe('CategoriesCalendarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoriesCalendarService]
    });
  });

  it('should be created', inject([CategoriesCalendarService], (service: CategoriesCalendarService) => {
    expect(service).toBeTruthy();
  }));
});
