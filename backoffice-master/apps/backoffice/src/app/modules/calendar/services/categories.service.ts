import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CategoriesCalendarModel } from '../models/category.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CategoriesCalendarService extends Repository<CategoriesCalendarModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'EVENTS.CATEGORIES';
    this.entity_url = 'EVENTS.CATEGORIES_ID';
  }

  desactive(id: number, data: CategoriesCalendarModel): Observable<Resource<CategoriesCalendarModel>> {
    return this.resourceService.patch<CategoriesCalendarModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: CategoriesCalendarModel): Observable<Resource<CategoriesCalendarModel>> {
    return this.resourceService.patch<CategoriesCalendarModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
