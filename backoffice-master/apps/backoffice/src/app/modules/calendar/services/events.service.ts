import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { EventsCalendarModel } from '../models/event.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EventsCalendarService extends Repository<EventsCalendarModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'EVENTS.EVENTS';
    this.entity_url = 'EVENTS.EVENTS_ID';
  }

  changeStatus(id,event): Observable<Resource<EventsCalendarModel>> {
    return this.resourceService.create<EventsCalendarModel>(this.urlService.get('EVENTS.CHANGE_STATUS', { id }), {
      event
    });
  }

  report(id): Observable<Resource<EventsCalendarModel>> {
    return this.resourceService.read<EventsCalendarModel>(this.urlService.get('EVENTS.REPORT', { id }));
  }

}
