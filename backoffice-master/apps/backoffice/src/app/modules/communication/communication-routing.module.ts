import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { CommunicationComponent } from '@fi-sas/backoffice/modules/communication/communication.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';


const routes: Routes = [
  {
    path: '',
    component: CommunicationComponent,
    children: [
      {
        path: 'category',
        loadChildren: './pages/category/category.module#CategoryCommunicationModule',
        data: { breadcrumb: 'Categorias', title: 'Categorias' },
        resolve: {}
      },
      {
        path: 'feed',
        loadChildren: './pages/feed/feed.module#FeedCommunicationModule',
        data: { breadcrumb: 'Feeds', title: 'Feeds' },
        resolve: {}
      },
      {
        path: 'post',
        loadChildren: './pages/post/post.module#PostCommunicationModule',
        data: { breadcrumb: 'Posts', title: 'Posts' },
        resolve: {}
      },
      {
        path: 'target-posts',
        loadChildren: './pages/target-posts/target-posts.module#TargetPostsCommunicationModule',
        data: { breadcrumb: 'Publicações Direcionadas', title: 'Publicações Direcionadas' },
        resolve: {}
      },
      {
        path: 'recipient-groups',
        loadChildren: './pages/recipient-groups/recipient-groups.module#RecipientGroupsCommunicationModule',
        data: { breadcrumb: 'Grupos Destinatários', title: 'Grupos Destinatários' },
        resolve: {}
      },
      {
        path: 'settings',
        loadChildren: './pages/geral-settings/geral-settings.module#GeralSettingsCommunicationModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações' },
        resolve: {}
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },


      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationRoutingModule {}
