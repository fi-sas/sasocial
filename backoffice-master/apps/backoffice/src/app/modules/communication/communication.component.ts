import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-communication',
  template: '<router-outlet></router-outlet>'
})
export class CommunicationComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService,private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(19), 'mail');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const categories = new SiderItem('Categorias');
    categories.addChild(
      new SiderItem('Criar', '', '/communication/category/create', 'communication:categories:create')
    );
    categories.addChild(
      new SiderItem('Listar', '', '/communication/category/list', 'communication:categories:read')
    );
    this.uiService.addSiderItem(categories);

    const feeds = new SiderItem('Feeds', '', '', 'communication:feeds');
    feeds.addChild(new SiderItem('Criar', '', '/communication/feed/create', 'communication:feeds:create'));
    feeds.addChild(new SiderItem('Listar', '', '/communication/feed/list', 'communication:feeds:read'));
    this.uiService.addSiderItem(feeds);

    const posts = new SiderItem('Publicações', '', '', 'communication:posts');
    posts.addChild(new SiderItem('Criar', '', '/communication/post/create', 'communication:posts:create'));
    posts.addChild(new SiderItem('Listar', '', '/communication/post/list', 'communication:posts:read'));
    this.uiService.addSiderItem(posts);

    const publications = new SiderItem('Publicações Direcionadas', '', '', 'communication:target_posts');
    publications.addChild(new SiderItem('Criar', '', '/communication/target-posts/create','communication:target_posts:create'));
    publications.addChild(new SiderItem('Listar', '', '/communication/target-posts/list','communication:target_posts:read'));
    this.uiService.addSiderItem(publications);

    const recipient = new SiderItem('Grupos Destinatários', '', '', 'authorization:targets');
    recipient.addChild(new SiderItem('Criar', '', '/communication/recipient-groups/create','authorization:targets:create'));
    recipient.addChild(new SiderItem('Listar', '', '/communication/recipient-groups/list','authorization:targets:read'));
    this.uiService.addSiderItem(recipient);

    const geralSetting = new SiderItem('Configurações Gerais', '', '/communication/settings/geral', 'communication:configurations:create');
    this.uiService.addSiderItem(geralSetting);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
