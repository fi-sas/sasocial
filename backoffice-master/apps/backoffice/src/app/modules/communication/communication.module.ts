import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { CommunicationRoutingModule } from '@fi-sas/backoffice/modules/communication/communication-routing.module';
import { CommunicationComponent } from './communication.component';
import { FormPostResolver } from '@fi-sas/backoffice/modules/communication/pages/post/form-post/form-post.resolver';
import { ListChannelsResolver } from '@fi-sas/backoffice/modules/communication/resolvers/list-channels.resolver';
import { ListAllCategoriesResolver } from '@fi-sas/backoffice/modules/communication/resolvers/list-all-categories.resolver';
import { PostsService } from '@fi-sas/backoffice/modules/communication/services/posts.service';

@NgModule({
  imports: [CommonModule, SharedModule, CommunicationRoutingModule],
  declarations: [
    CommunicationComponent,
  ],
  providers: [
    PostsService,
    ListAllCategoriesResolver,
    FormPostResolver,
    ListChannelsResolver
  ]
})
export class CommunicationModule {}
