export class CategoryTranslation {
  language_id: number;
  name: string;
}

export class CategoryModel {
  id: number;
  active: boolean;
  translations: CategoryTranslation[];
}
