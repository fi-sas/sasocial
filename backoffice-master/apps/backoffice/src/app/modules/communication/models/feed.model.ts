export class FeedModel {
  id: number;
  language_id: number;
  name: string;
  url: string;
  active: boolean;
}
