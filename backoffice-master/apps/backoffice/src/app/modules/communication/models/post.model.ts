import { CategoryModel } from '@fi-sas/backoffice/modules/communication/models/category.model';
import { ChannelModel } from '@fi-sas/backoffice/modules/communication/models/channel.model';
import { GroupModel } from '@fi-sas/backoffice/modules/configurations/models/group.model';

export enum PostStatus {
  WaitingApproval = 'WAITING_APPROVAL',
  Approved = 'APPROVED',
  Rejected = 'REJECTED'
}

export class PostTranslation {
  language_id: number;
  title: string;
  summary: string;
  body: string;
  location: string;
  file_id_9_16: number;
  file_id_16_9: number;
  file_id_1_1: number;
}

export class PostModel {
  id: number;
  status: PostStatus;
  tags: string;
  weight: string;
  date: Date;
  date_begin: Date;
  date_end: Date;
  translations: PostTranslation[];
  channels: ChannelModel[];
  groups: GroupModel[];
  category_id: number;
  category: CategoryModel;
  history: any[];
}
