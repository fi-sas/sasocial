export class RecipientGroupsModel {
    name: string;
    id: number;
    active: boolean;
    conditions: ConditionsModel[] = [];
}

export class ConditionsModel {
    field_name: string;
    field_values: any[] = [];
    field_value: string;
}
