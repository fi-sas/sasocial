export class TargetPostsModel {
    target_id: number;
    updated_at: Date;
    created_at: Date;
    created_by_id: number;
    schedule_date: Date;
    status: string;
    simplified_message: string;
    message: string;
    subject: string;
    category_id: string;
    id: number;
    target_post_notification_methods: NotificationMethodsModel[] = [];
}

export class NotificationMethodsModel {
    notification_method_id: number;
}