import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguagesResolver } from '@fi-sas/backoffice/shared/resolvers/languages.resolver';
import { FormCategoryComponent } from './form-category/form-category.component';
import { ListCategoriesComponent } from './list-categories/list-categories.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListCategoriesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'communication:categories:read'},
    resolve: { }
  },
  {
    path: 'create',
    component: FormCategoryComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'communication:categories:create'},
    resolve: { languages: LanguagesResolver }
  },
  {
    path: 'update/:id',
    component: FormCategoryComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'communication:categories:update'},
    resolve: { languages: LanguagesResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
