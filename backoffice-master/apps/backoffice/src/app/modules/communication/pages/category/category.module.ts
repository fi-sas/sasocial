import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CategoryRoutingModule } from './category-routing.module';
import { FormCategoryComponent } from './form-category/form-category.component';
import { ListCategoriesComponent } from './list-categories/list-categories.component';

@NgModule({
  declarations: [
    FormCategoryComponent,
    ListCategoriesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CategoryRoutingModule
  ],

})
export class CategoryCommunicationModule { }
