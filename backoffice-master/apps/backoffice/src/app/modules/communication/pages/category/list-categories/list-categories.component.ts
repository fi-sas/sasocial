import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { CategoriesService } from '@fi-sas/backoffice/modules/communication/services/categories.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.less']
})
export class ListCategoriesComponent extends TableHelper implements OnInit {
  loading = true;


  constructor(
    private categoriesService: CategoriesService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }


  ngOnInit() {
    this.initTableData(this.categoriesService);
  }

  deleteCategory(id: number) {
    if(!this.authService.hasPermission('communication:categories:delete')){
      return;
    }
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar esta categoria?', 'Eliminar')
      .subscribe(result => {
        if (result) {
          this.categoriesService.delete(id).pipe(first()).subscribe(r => {
            this.uiService.showMessage(
              MessageType.success,
              'Categoria eliminada com sucesso'
            );
            this.initTableData(this.categoriesService);
          });
        }
      });
  }

  edit(id) {
    if(!this.authService.hasPermission('communication:categories:update')){
      return;
    }
    this.router.navigateByUrl('communication/category/update/' + id);
  }
}
