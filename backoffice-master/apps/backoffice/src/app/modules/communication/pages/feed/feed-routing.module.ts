import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguagesResolver } from '@fi-sas/backoffice/shared/resolvers/languages.resolver';
import { ListAllCategoriesResolver } from '../../resolvers/list-all-categories.resolver';
import { FormFeedComponent } from './form-feed/form-feed.component';
import { ListFeedsComponent } from './list-feeds/list-feeds.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFeedsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'communication:feeds:read'},
    resolve: { languages: LanguagesResolver }
  },
  {
    path: 'create',
    component: FormFeedComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'communication:feeds:create'},
    resolve: { languages: LanguagesResolver, categories: ListAllCategoriesResolver }
  },
  {
    path: 'update/:id',
    component: FormFeedComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'communication:feeds:update'},
    resolve: { languages: LanguagesResolver, categories: ListAllCategoriesResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedRoutingModule { }
