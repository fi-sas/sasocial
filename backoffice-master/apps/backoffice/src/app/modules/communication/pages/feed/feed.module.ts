import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FeedRoutingModule } from './feed-routing.module';
import { FormFeedComponent } from './form-feed/form-feed.component';
import { ListFeedsComponent } from './list-feeds/list-feeds.component';

@NgModule({
  declarations: [
    FormFeedComponent,
    ListFeedsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FeedRoutingModule
  ],

})
export class FeedCommunicationModule { }
