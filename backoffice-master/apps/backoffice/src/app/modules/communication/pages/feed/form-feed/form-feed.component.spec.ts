import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFeedComponent } from './form-feed.component';

describe('FormFeedComponent', () => {
  let component: FormFeedComponent;
  let fixture: ComponentFixture<FormFeedComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormFeedComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
