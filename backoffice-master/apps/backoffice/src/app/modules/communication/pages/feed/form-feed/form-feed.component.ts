import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedModel } from '@fi-sas/backoffice/modules/communication/models/feed.model';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { FeedsService } from '@fi-sas/backoffice/modules/communication/services/feeds.service';
import { CategoryModel } from '@fi-sas/backoffice/modules/communication/models/category.model';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { CategoriesService } from '../../../services/categories.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-form-feed',
  templateUrl: './form-feed.component.html',
  styleUrls: ['./form-feed.component.less']
})
export class FormFeedComponent implements OnInit {
  formFeed = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    category_id: new FormControl('', [Validators.required]),
    url: new FormControl('', [Validators.required, trimValidation]),
    language_id: new FormControl('', [Validators.required]),
    active: new FormControl(true, [Validators.required])
  });
  submitted = false;
  get f() { return this.formFeed.controls; }

  isLoading = false;
  isUpdate = false;
  languages: LanguageModel[] = [];
  categories: CategoryModel[] = [];
  updateFeed: FeedModel = null;

  constructor(
    private feedsService: FeedsService,
    private categoriesService: CategoriesService,
    private router: Router,
    private uiService: UiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getCategories();
    this.languages = this.route.snapshot.data.languages.data;

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.getFeedId(id);
      this.isUpdate = true;
    }
  }

  getFeedId(id) {
    this.feedsService
      .read(id)
      .pipe(
        first()
      )
      .subscribe((results) => {
        this.updateFeed = results.data[0];
        this.formFeed.patchValue({
          ...this.updateFeed
        });
      });
  }

  getCategories() {
    this.categoriesService.list(1,-1,null, null, {
      active: true
    }).pipe(first()).subscribe((data)=> {
      this.categories = data.data;
    })
  }

  submit() {
    this.submitted = true;
    if (this.formFeed.valid) {
      this.isLoading = true;

    if (this.isUpdate) {
      this.feedsService.update(this.updateFeed.id, this.formFeed.value).pipe(first()).subscribe(
        result => {
          this.updateFeed = result.data[0];
          this.isLoading = false;
          this.router.navigateByUrl('communication/feed/list');
          this.uiService.showMessage(
            MessageType.success,
            'Feed alterado com sucesso'
          );
        },
        err => {
          this.isLoading = false;
          this.formFeed.enable();
        }
      );
    } else {
      this.feedsService.create(this.formFeed.value).pipe(first()).subscribe(
        result => {
          this.isUpdate = true;
          this.updateFeed = result.data[0];
          this.isLoading = false;
          this.router.navigateByUrl('communication/feed/list');
          this.uiService.showMessage(
            MessageType.success,
            'Feed criado com sucesso'
          );
        },
        err => {
          this.isLoading = false;
          this.formFeed.enable();
        }
      );
    }
    }
    
  }

  returnButton() {
    this.router.navigateByUrl('communication/feed/list');
  }
}
