import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { FeedsService } from '@fi-sas/backoffice/modules/communication/services/feeds.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-feeds',
  templateUrl: './list-feeds.component.html',
  styleUrls: ['./list-feeds.component.less']
})
export class ListFeedsComponent extends TableHelper implements OnInit {
  languages: LanguageModel[] = [];
  loading = true;


  constructor(
    private feedsService: FeedsService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  

  getLanguageName(language_id: number) {
    const Tlanguage = this.languages.find(
      language => language.id === language_id
    );
    return Tlanguage ? Tlanguage.name : 'Língua não encontrada';
  }

  ngOnInit() {
    this.languages = this.activatedRoute.snapshot.data.languages.data;
    this.initTableData(this.feedsService);
  }

  deleteFeed(id: number) {
    if(!this.authService.hasPermission('communication:feeds:delete')){
      return;
    }
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este feed?', 'Eliminar')
      .subscribe(result => {
        if (result) {
          this.feedsService.delete(id).pipe(first()).subscribe(r => {
            this.uiService.showMessage(
              MessageType.success,
              'Feed eliminado com sucesso'
            );
            this.initTableData(this.feedsService);
          });
        }
      });
  }

  edit(id) {
    if(!this.authService.hasPermission('communication:feeds:update')){
      return;
    }
    this.router.navigateByUrl('communication/feed/update/' +  id);
  }
}
