import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeralSettingsCommunicationComponent } from './geral-settings.component';



const routes: Routes = [
  { path: '', redirectTo: 'geral', pathMatch: 'full' },
  {
    path: 'geral',
    component: GeralSettingsCommunicationComponent,
    data: { breadcrumb: 'Gerais', title: 'Configurações Gerais', scope: 'communication:configurations:create' },
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeralSettingsRoutingModule { }
