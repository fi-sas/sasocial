import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GeralSettingsCommunicationComponent } from './geral-settings.component';


describe('GeralSettingsCommunicationComponent', () => {
  let component: GeralSettingsCommunicationComponent;
  let fixture: ComponentFixture<GeralSettingsCommunicationComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [GeralSettingsCommunicationComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(GeralSettingsCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
