import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { GeralSettingsCommunicationModel } from "../../models/geral-settings.model";
import { GeralSettingsCommunicationService } from "../../services/geral-settings.service";

@Component({
    selector: 'fi-sas-geral-settings-communication',
    templateUrl: './geral-settings.component.html',
    styleUrls: ['./geral-settings.component.less']
})
export class GeralSettingsCommunicationComponent implements OnInit {
    formData: FormGroup;
    loadingConfiguration = true;
    loading = false;
    submit = false;

    constructor(private fb: FormBuilder, private settings: GeralSettingsCommunicationService, private uiService: UiService) { }

    ngOnInit() {
        this.formData = this.fb.group({
            facebook_id: new FormControl('', [trimValidation]),
            facebook_access: new FormControl('', [trimValidation]),

        })
        this.getConfigurations();
    }

    get f() { return this.formData.controls; }

    getConfigurations() {
        this.settings.list(1, -1).pipe(first(), finalize(() => this.loadingConfiguration = false)).subscribe((data) => {
            this.formData.get('facebook_id').setValue(typeof data.data[0].FACEBOOK_PAGE_ID != 'object' ? data.data[0].FACEBOOK_PAGE_ID : '');
            this.formData.get('facebook_access').setValue(typeof data.data[0].FACEBOOK_ACCESS_TOKEN != 'object' ? data.data[0].FACEBOOK_ACCESS_TOKEN : '');
        })
    }

    submitForm() {
        this.loading = true;
        this.submit = true;
        let sendValue: GeralSettingsCommunicationModel = new GeralSettingsCommunicationModel();
        if (this.formData.valid) {
            this.submit = false;
            sendValue.FACEBOOK_PAGE_ID = this.formData.get('facebook_id').value;
            sendValue.FACEBOOK_ACCESS_TOKEN = this.formData.get('facebook_access').value;
            this.settings.create(sendValue).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
                this.uiService.showMessage(MessageType.success, 'Configurações alteradas com sucesso');
            })
        } else {
            this.loading = false;
        }

    }

}