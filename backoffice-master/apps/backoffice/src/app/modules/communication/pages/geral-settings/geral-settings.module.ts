import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { GeralSettingsRoutingModule } from './geral-settings-routing.module';
import { GeralSettingsCommunicationComponent } from './geral-settings.component';

@NgModule({
  declarations: [
    GeralSettingsCommunicationComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GeralSettingsRoutingModule
  ],

})
export class GeralSettingsCommunicationModule { }
