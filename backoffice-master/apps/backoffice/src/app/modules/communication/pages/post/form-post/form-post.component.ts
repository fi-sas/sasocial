import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import * as _ from 'lodash';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryModel } from '@fi-sas/backoffice/modules/communication/models/category.model';
import { ChannelModel } from '@fi-sas/backoffice/modules/communication/models/channel.model';
import { PostModel } from '@fi-sas/backoffice/modules/communication/models/post.model';
import { GroupModel } from '@fi-sas/backoffice/modules/configurations/models/group.model';
import * as dayjs from 'dayjs';
import * as moment from 'moment';
import { PostsService } from '@fi-sas/backoffice/modules/communication/services/posts.service';
import { FileType } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { finalize, first } from 'rxjs/operators';
import { CategoriesService } from '../../../services/categories.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { hasOwnProperty } from 'tslint/lib/utils';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'fi-sas-form-post',
  templateUrl: './form-post.component.html',
  styleUrls: ['./form-post.component.less']
})
export class FormPostComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  errorTrans = false;
  isStatusPage = false;
  submitted = false;
  channelsSelec = [];
  formPost = new FormGroup({
    tags: new FormControl([]),
    weight: new FormControl(1, [
      Validators.required,
      Validators.min(1),
      Validators.max(10)
    ]),
    date: new FormControl(new Date()),
    dateRange: new FormControl(null, [Validators.required]),
    category_id: new FormControl(null),
    channels: new FormControl([], [Validators.required]),
    groups: new FormControl([]),
    translations: new FormArray([])
  });
  get f() { return this.formPost.controls; }
  isLoading = false;
  isUpdate = false;
  categories: CategoryModel[] = [];
  channels: ChannelModel[] = [];
  groups: GroupModel[] = [];
  updatePost: PostModel = null;

  FileType = FileType;
  fileType: FileType = FileType.IMAGE;

  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  translations = this.formPost.get('translations') as FormArray;

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['insertImage','insertVideo']
    ]
};

  constructor(
    private postsService: PostsService,
    private categoriesService: CategoriesService,
    private languagesService: LanguagesService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.url.subscribe(param => {
      this.isStatusPage = param[0].path === 'status-post';
    });
    this.getCategories();
    this.channels = this.route.snapshot.data.channels.data;
    this.groups = this.route.snapshot.data.groups.data;

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.getPostId(id);
      this.isUpdate = true;
    } else {
      this.loadLanguages('');
    }

    this.formPost.controls.channels.valueChanges.subscribe(selectedChannels => {
      this.channelsSelec = selectedChannels;
      this.formControlChannelsChanged(selectedChannels);
    });
  }

  getPostId(id) {
    this.languages_loading = false;
    this.postsService
      .read(id)
      .pipe(
        first()
      )
      .subscribe((results) => {
        this.updatePost = results.data[0];
        this.existVideo(this.updatePost);
        this.formPost.patchValue({
          category_id: this.updatePost.category.id,
          tags: this.updatePost.tags ? this.updatePost.tags.split(',') : null,
          weight: this.updatePost.weight,
          date: this.updatePost.date,
          dateRange: [this.updatePost.date_begin, this.updatePost.date_end],
          channels: this.updatePost.channels.map(channel => channel.id),
          groups: this.updatePost.groups.map(group => group.id)
        });
        this.listOfSelectedLanguages = [];
        this.loadLanguages(this.updatePost);
      });
  }

  getCategories() {
    this.categoriesService.list(1, -1, null, null, {
      active: true
    }).pipe(first()).subscribe((data) => {
      this.categories = data.data;
    })
  }


  formControlChannelsChanged(selectedChannels: number[]) {
    const langs = this.translations;
    this.formPost.get('category_id').clearValidators();
    this.formPost.get('date').clearValidators();
    langs.controls.forEach(control => {
      control.get('summary').clearValidators();
      control.get('body').clearValidators();
      control.get('location').clearValidators();
      control.get('url').clearValidators();
      control.get('file_id_16_9').clearValidators();
      control.get('file_id_9_16').clearValidators();
      control.get('file_id_1_1').clearValidators();
    });
    if (selectedChannels.find(channel => channel === 1)) {
      // MEDIAKiosk
      langs.controls.forEach(control => {
        control.get('file_id_9_16').setValidators([Validators.required]);
        control.get('summary').setValidators([trimValidation]);
        control.get('location').setValidators([trimValidation]);
      });

    }
    if (selectedChannels.find(channel => channel === 2)) {
      // TICKER
      this.formPost.get('category_id').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('summary').setValidators([trimValidation]);
        control.get('location').setValidators([trimValidation]);
      });

    }

    if (selectedChannels.find(channel => channel === 3)) {
      // Event
      this.formPost.get('category_id').setValidators([Validators.required]);
      this.formPost.get('date').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('summary').setValidators([Validators.required, trimValidation]);
        control.get('body').setValidators([Validators.required, trimValidation]);
        control.get('location').setValidators([Validators.required, trimValidation]);
        control.get('file_id_1_1').setValidators([Validators.required]);

      });
    }
    if (selectedChannels.find(channel => channel === 4)) {
      // MOBILE
      this.formPost.get('category_id').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('summary').setValidators([Validators.required, trimValidation]);
        control.get('location').setValidators([trimValidation]);
        control.get('file_id_16_9').setValidators([Validators.required]);
        control.get('body').setValidators([Validators.required, trimValidation]);
      });

    }

    if (selectedChannels.find(channel => channel === 5)) {
      // MediaTV
      langs.controls.forEach(control => {
        control.get('summary').setValidators([trimValidation]);
        control.get('location').setValidators([trimValidation]);
        control.get('file_id_16_9').setValidators([Validators.required]);
      });
    }
    if (selectedChannels.find(channel => channel === 6)) {
      // WebPage
      this.formPost.get('category_id').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('location').setValidators([trimValidation]);
        control.get('summary').setValidators([Validators.required, trimValidation]);
        control.get('file_id_16_9').setValidators([Validators.required]);
        control.get('body').setValidators([Validators.required, trimValidation]);
      });
    }
    if (selectedChannels.find(channel => channel === 7)) {
      // Facebook
      this.formPost.get('category_id').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('location').setValidators([trimValidation]);
        control.get('summary').setValidators([Validators.required, trimValidation]);
        control.get('file_id_1_1').setValidators([Validators.required]);
        control.get('body').setValidators([Validators.required, trimValidation]);
      });
    }
    if (selectedChannels.find(channel => channel === 8)) {
      // UrlMediaTV
      this.formPost.get('category_id').setValidators([Validators.required]);
      langs.controls.forEach(control => {
        control.get('title').setValidators([Validators.required, trimValidation]);
        control.get('url').setValidators([Validators.required]);
      });
    }

    langs.controls.forEach(control => {
      control.get('title').updateValueAndValidity();
      control.get('summary').updateValueAndValidity();
      control.get('body').updateValueAndValidity();
      control.get('location').updateValueAndValidity();
      control.get('url').updateValueAndValidity();
      control.get('file_id_9_16').updateValueAndValidity();
      control.get('file_id_16_9').updateValueAndValidity();
      control.get('file_id_1_1').updateValueAndValidity();
    });
    this.formPost.get('category_id').updateValueAndValidity();
    this.formPost.get('date').updateValueAndValidity();
    this.formPost.updateValueAndValidity();
    this.formPost.markAsDirty();
  }

  changeType() {
    const langs = this.translations;
    langs.controls.forEach(control => {
      control.get('file_id_9_16').setValue(null);
      control.get('file_id_16_9').setValue(null);
      control.get('file_id_1_1').setValue(null);
    });
  }

  valid(value) {
    return this.channelsSelec.find(channel => channel === value) ? true : false;
  }

  getFileID_16_9_InputError(i: number) {
    if (this.translations.controls[i].get('file_id_16_9').errors) {
      return this.translations.controls[i].get('file_id_16_9').errors
        .required
        ? 'Campo obrigatório'
        : '';
    } else {
      return '';
    }
  }

  getFileID_9_16_InputError(i: number) {
    if (this.translations.controls[i].get('file_id_9_16').errors) {
      return this.translations.controls[i].get('file_id_9_16').errors
        .required
        ? 'Campo obrigatório'
        : '';
    } else {
      return '';
    }
  }

  getFileID_1_1_InputError(i: number) {
    if (this.translations.controls[i].get('file_id_1_1')) {
      return this.translations.controls[i].get('file_id_1_1').errors
        .required
        ? 'Campo obrigatório'
        : '';
    } else {
      return '';
    }
  }

  existFile() {
    const langs = this.translations;
    let aux = false;
    langs.controls.map(control => {
      if(control.value.file_id_1_1 || control.value.file_id_9_16 || control.value.file_id_16_9) {
        aux = true;
      }
    });
    if(aux) {
      return true;
    }
    return false;

  }

  existVideo(data) {
    let aux = false;
    data.translations.map(control => {
     if((control.file_id_1_1 && control.file_id_1_1.type == 'VIDEO') || (control.file_9_16 && control.file_9_16.type == 'VIDEO') ||
     (control.file_16_9 && control.file_16_9.type == 'VIDEO')) {
        aux = true;
      }
    });
    if(aux) {
      this.fileType = FileType.VIDEO;
    }

  }


  submit(value: any, valid: boolean) {
    this.submitted = true;
    if (this.formPost.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    if((this.valid(4) || this.valid(6)) && this.fileType === FileType.VIDEO) {
      return;
    }
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    if (this.formPost.valid && !this.errorTrans) {
      if (value.tags) {
        value.tags = value.tags.join();
      }

      value.date_begin = null;
      value.date_end = null;
      if (value.dateRange) {
        value.date_begin = moment(value.dateRange[0]);
        value.date_end = moment(value.dateRange[1]);
      }
      delete value.dateRange;
      this.isLoading = true;

      if (this.isUpdate) {
        this.postsService.update(this.updatePost.id, value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
          result => {
            this.updatePost = result.data[0];
            this.formPost.enable();
            this.uiService.showMessage(
              MessageType.success,
              'Publicação alterada com sucesso'
            );
            this.router.navigateByUrl('communication/post/list');
          },
          err => {
            this.formPost.enable();
          }
        );
      } else {
        this.postsService.create(value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
          result => {
            this.isUpdate = true;
            this.updatePost = result.data[0];
            this.formPost.enable();
            this.uiService.showMessage(
              MessageType.success,
              'Publicação criada com sucesso'
            );
            this.router.navigateByUrl('communication/post/list');
          },
          err => {
            this.formPost.enable();
          }
        );
      }
    }

  }

  loadLanguages(data: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        if (data) {
          this.startTranslation(data);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.formPost.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string, summary?: string, body?: string, location?: string, url?: string, file_id_9_16?: number, file_id_16_9?: number, file_id_1_1?: number) {
    const translations = this.formPost.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        title: new FormControl(name, [Validators.required, trimValidation]),
        summary: new FormControl(summary, [trimValidation]),
        body: new FormControl(body),
        location: new FormControl(location, [trimValidation]),
        url: new FormControl(url),
        file_id_9_16: new FormControl(file_id_9_16),
        file_id_16_9: new FormControl(file_id_16_9),
        file_id_1_1: new FormControl(file_id_1_1)
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.title,
          translation.summary,
          translation.body,
          translation.location,
          translation.url,
          translation.file_id_9_16,
          translation.file_id_16_9,
          translation.file_id_1_1
        );
        this.listOfSelectedLanguages.push(translation.language_id);

      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  returnButton() {
    this.router.navigateByUrl('communication/post/list');
  }
}
