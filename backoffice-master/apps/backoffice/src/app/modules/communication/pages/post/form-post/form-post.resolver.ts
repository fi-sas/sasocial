import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { PostModel } from '@fi-sas/backoffice/modules/communication/models/post.model';
import { PostsService } from '@fi-sas/backoffice/modules/communication/services/posts.service';

@Injectable()
export class FormPostResolver
  implements Resolve<Observable<Resource<PostModel>>> {
  constructor(private postsService: PostsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const id: number = parseInt(route.paramMap.get('id'), 10);
    return this.postsService
      .get(id)
      .pipe(first())
      .catch(() => {
        return EMPTY;
      });
  }
}
