import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';

import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { PostStatus } from '@fi-sas/backoffice/modules/communication/models/post.model';
import { PostsService } from '@fi-sas/backoffice/modules/communication/services/posts.service';
import { FeedsService } from '../../../services/feeds.service';
import { CategoriesService } from '@fi-sas/backoffice/modules/communication/services/categories.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.less']
})
export class ListPostsComponent extends TableHelper implements OnInit {

  public PostStatus = PostStatus;
  loading = true;
  isNotesVisible = false;
  isHistoryVisible = false;
  tempHistory = [];
  statusList = [];
  typologies_loading = true;
  categories;
  notes = '';
  selectPost = null;
  constructor(
    private postsService: PostsService,
    private feedsService: FeedsService,
    private categoriesService: CategoriesService,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    router: Router,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'title,summary,body',
      withRelated: 'channels,category,translations,groups,created_by,history'
    }
   }

   ngOnInit() {
    this.initTableData(this.postsService);
    this.getCategories();
  }

  fetchRss() {
    this.loading = true;
    this.feedsService.fetchRss().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        'Tickers atualizados'
      );
    });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.status = null;
    this.filters.category_id = null;
    this.filters.created_by_id = null;
    this.filters.date_begin = null;
    this.filters.date_end = null;
    this.searchData(true)
  }

  getCategories() {
    this.categoriesService.list(1,-1).pipe(first()).subscribe(
      categories => {
        this.categories = categories.data;
        this.typologies_loading = false;
      }
    )
  }


  deletePost(id: number) {
    if(!this.authService.hasPermission('communication:posts:delete')){
      return;
    }
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar esta publicação?', 'Eliminar')
      .subscribe(result => {
        if (result) {
          this.postsService.delete(id).pipe(first()).subscribe(r => {
            this.uiService.showMessage(
              MessageType.success,
              'Publicação eliminada com sucesso'
            );
            this.searchData();
          });
        }
      });
  }

  showHistory(data: any) {
    this.isHistoryVisible = true;
    this.tempHistory = data;
  }

  handleHistoryCancel() {
    this.isHistoryVisible = false;
  }

  handleHistoryOk() {
    this.isHistoryVisible = false;
  }


  edit(id) {
    if(!this.authService.hasPermission('communication:posts:update')){
      return;
    }
    this.router.navigateByUrl('communication/post/update/' +  id);
  }

  editStatus(data) {
    if(!this.authService.hasPermission('communication:posts:approve') && !this.authService.hasPermission('communication:posts:reject')){
      return;
    }
    this.selectPost = data;
    this.isNotesVisible = true;
  }

  approve() {

      this.postsService
        .approvePost(this.selectPost.id, this.notes, this.selectPost)
        .pipe(first())
        .subscribe(
          result => {
            this.isNotesVisible = false;
            this.uiService.showMessage(MessageType.success, "Estado alterado com sucesso, para aprovado.");
            this.searchData();
          }
        );
    }

    reject() {
        this.postsService
          .rejectPost(this.selectPost.id, this.notes, this.selectPost)
          .pipe(first())
          .subscribe(
            result => {
              this.isNotesVisible = false;
              this.uiService.showMessage(MessageType.success, "Estado alterado com sucesso, para rejeitado.");
              this.searchData();
            }
          );
  
    }

}
