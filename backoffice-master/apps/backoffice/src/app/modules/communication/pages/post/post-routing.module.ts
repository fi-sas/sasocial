import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguagesResolver } from '@fi-sas/backoffice/shared/resolvers/languages.resolver';
import { ListAllGroupsResolver } from '@fi-sas/backoffice/shared/resolvers/list-all-groups.resolver';
import { ListAllCategoriesResolver } from '../../resolvers/list-all-categories.resolver';
import { ListChannelsResolver } from '../../resolvers/list-channels.resolver';
import { FormPostComponent } from './form-post/form-post.component';
import { FormPostResolver } from './form-post/form-post.resolver';
import { ListPostsComponent } from './list-posts/list-posts.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListPostsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'communication:posts:read'},
    resolve: { }
  },
  {
    path: 'create',
    component: FormPostComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'communication:posts:create'},
    resolve: {
        languages: LanguagesResolver,
        channels: ListChannelsResolver,
        categories: ListAllCategoriesResolver,
        groups: ListAllGroupsResolver
      }
  },
  {
    path: 'update/:id',
    component: FormPostComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'communication:posts:update'},
    resolve: {
        languages: LanguagesResolver,
        post: FormPostResolver,
        channels: ListChannelsResolver,
        categories: ListAllCategoriesResolver,
        groups: ListAllGroupsResolver
      }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
