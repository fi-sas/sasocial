import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { PostRoutingModule } from './post-routing.module';
import { FormPostComponent } from './form-post/form-post.component';
import { ListPostsComponent } from './list-posts/list-posts.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [
    FormPostComponent,
    ListPostsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PostRoutingModule,
    AngularEditorModule
  ],

})
export class PostCommunicationModule { }
