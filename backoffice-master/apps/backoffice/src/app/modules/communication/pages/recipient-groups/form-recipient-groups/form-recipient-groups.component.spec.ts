import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormRecipientGroupsComponent } from './form-recipient-groups.component';



describe('FormRecipientGroupsComponent', () => {
  let component: FormRecipientGroupsComponent;
  let fixture: ComponentFixture<FormRecipientGroupsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormRecipientGroupsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRecipientGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
