import { Component, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ResidenceModel } from '@fi-sas/backoffice/modules/accommodation/modules/residences/models/residence.model';
import { ResidencesService } from '@fi-sas/backoffice/modules/accommodation/modules/residences/services/residences.service';
import { CourseDegreeModel } from '@fi-sas/backoffice/modules/configurations/models/course-degree.model';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { CoursesDegreesServices } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/courses-degrees.service';
import { DepartmentModel } from '@fi-sas/backoffice/modules/users/modules/departments/models/department.model';
import { DepartmentService } from '@fi-sas/backoffice/modules/users/modules/departments/services/departments.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { SectionsModel } from '@fi-sas/backoffice/modules/users/modules/sections/models/sections.model';
import { SectionsService } from '@fi-sas/backoffice/modules/users/modules/sections/services/sections.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, finalize, first, switchMap, map } from 'rxjs/operators';
import {
  ConditionsModel,
  RecipientGroupsModel,
} from '../../../models/recipient-groups.model';
import { RecipientGroupCommunicationService } from '../../../services/recipient-groups.service';

@Component({
  selector: 'fi-sas-form-recipient-groups',
  templateUrl: './form-recipient-groups.component.html',
  styleUrls: ['./form-recipient-groups.component.less'],
})
export class FormRecipientGroupsComponent implements OnInit, OnDestroy {
  @Input() post: boolean = false;
  private dataValue;

  get data(): any {
    return this.dataValue;
  }

  @Input()
  set data(val: any) {
    this.editLoading = true;
    this.formCreate = this.formNew();
    this.dataValue = val;
    this.valueCount = -1;
    if (this.dataValue) {
      this.getRecipentByData(this.dataValue);
      this.id = this.dataValue.id;
      this.isEdit = true;
      this.firstEditUnit = true;
      this.firstEditDegree = true;
      this.firstEditDepart = true;
    } else {
      this.editLoading = false;
    }
  }
  editLoading;
  formCreate = this.formNew();
  firstEditUnit: boolean = false;
  firstEditDegree: boolean = false;
  firstEditDepart: boolean = false;
  submitted = false;
  get f() {
    return this.formCreate.controls;
  }
  id;
  isEdit = false;
  isLoading = false;

  listUsers: UserModel[] = [];
  isLoadingUsers = false;
  usersSearchChange$ = new BehaviorSubject('');

  listProfiles: ProfileModel[] = [];
  courseDegreesList: CourseDegreeModel[] = [];
  coursesList: CourseModel[] = [];
  organicsList: OrganicUnitsModel[] = [];
  departmentsList: DepartmentModel[] = [];
  sectionsList: SectionsModel[] = [];
  isLoadingCounter = false;
  valueCount = -1;
  residencesList: ResidenceModel[] = [];

  constructor(
    private usersService: UsersService,
    private profilesService: ProfilesService,
    private organicUnitsService: OrganicUnitsService,
    private departmentsService: DepartmentService,
    private sectionsService: SectionsService,
    private router: Router,
    private uiService: UiService,
    private route: ActivatedRoute,
    private residencesService: ResidencesService,
    private recipientGroupService: RecipientGroupCommunicationService,
    private coursesDegreesServices: CoursesDegreesServices
  ) {}

  ngOnInit() {
    this.getResidence();
    this.getProfiles();
    this.getOrganicUnits();
    this.getDegrees();
    const id = this.route.snapshot.paramMap.get('id');
    if (id && !this.post) {
      this.editLoading = true;
      this.getRecipientGroupId(id);
      this.id = id;
      this.isEdit = true;
      this.firstEditUnit = true;
      this.firstEditDegree = true;
      this.firstEditDepart = true;
    }
    this.formCreate.valueChanges.subscribe((change) => {
      this.valueCount = -1;
    });

    this.initLoadUsers();
  }

  ngOnDestroy(): void {}

  initLoadUsers() {
    const loadUsers = (search: string): Observable<UserModel[]> =>
      this.usersService
        .list(1, 20, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,student_number,email,identification',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.listUsers = users;
      this.isLoadingUsers = false;
    });
  }

  formNew() {
    return new FormGroup({
      name: new FormControl('', [Validators.required, trimValidation]),
      active: new FormControl(true, [Validators.required]),
      field_values_user: new FormControl([]),
      field_values_profile: new FormControl([]),
      field_values_status: new FormControl([]),
      field_values_unit_organic: new FormControl([]),
      field_values_degree: new FormControl([]),
      field_values_course: new FormControl([]),
      field_values_department: new FormControl([]),
      field_values_section: new FormControl([]),
      field_values_accommodation_contracted: new FormControl(false, [
        Validators.required,
      ]),
      field_values_bike_contracted: new FormControl(false, [
        Validators.required,
      ]),
      field_values_scholarship: new FormControl(false, [Validators.required]),
      field_values_volunteering: new FormControl(false, [Validators.required]),
      field_values_food_ticket: new FormControl(false, [Validators.required]),
      field_values_residence_id: new FormControl([]),
    });
  }

  getResidence() {
    this.residencesService
      .list(1, -1, null, null, {
        active: true,
        available_for_assign: true,
        withRelated: false,
      })
      .pipe(first())
      .subscribe((result) => {
        this.residencesList = result.data;
      });
  }

  changeAccommodatiobContracted(event) {
    if (!event) {
      this.formCreate.get('field_values_residence_id').setValue([]);
    }
  }

  getRecipentByData(group: RecipientGroupsModel) {
    this.formCreate.patchValue({
      ...group,
    });
    if (group.conditions.length > 0) {
      group.conditions.forEach((cond) => {
        cond.field_values = cond.field_value
          .split(',')
          .map((value) => Number(value));
        if (cond.field_name == 'active') {
          this.formCreate
            .get('field_values_status')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'user_id') {
          this.getLoadUpdateUsers(cond.field_values);
          this.formCreate.get('field_values_user').setValue(cond.field_values);
        }
        if (cond.field_name == 'profile_id') {
          this.formCreate
            .get('field_values_profile')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'organic_unit_id') {
          this.formCreate
            .get('field_values_unit_organic')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'course_degree_id') {
          this.formCreate
            .get('field_values_degree')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'course_id') {
          this.formCreate
            .get('field_values_course')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'department_id') {
          this.formCreate
            .get('field_values_department')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'section_id') {
          this.formCreate
            .get('field_values_section')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'accommodation_contracted') {
          cond.field_value == '1'
            ? this.formCreate
                .get('field_values_accommodation_contracted')
                .setValue(true)
            : this.formCreate
                .get('field_values_accommodation_contracted')
                .setValue(false);
        }
        if (cond.field_name == 'residence_id') {
          this.formCreate
            .get('field_values_residence_id')
            .setValue(cond.field_values);
        }
        if (cond.field_name == 'ubike_bike_assigned') {
          cond.field_value == '1'
            ? this.formCreate.get('field_values_bike_contracted').setValue(true)
            : this.formCreate
                .get('field_values_bike_contracted')
                .setValue(false);
        }
        if (cond.field_name == 'scholarship_application_approved') {
          cond.field_value == '1'
            ? this.formCreate.get('field_values_scholarship').setValue(true)
            : this.formCreate.get('field_values_scholarship').setValue(false);
        }
        if (cond.field_name == 'volunteering_application_approved') {
          cond.field_value == '1'
            ? this.formCreate.get('field_values_volunteering').setValue(true)
            : this.formCreate.get('field_values_volunteering').setValue(false);
        }
        if (cond.field_name == 'food_tickets_purchased') {
          cond.field_value == '1'
            ? this.formCreate.get('field_values_food_ticket').setValue(true)
            : this.formCreate.get('field_values_food_ticket').setValue(false);
        }
      });
      this.getCourses();
      this.getDepartments();
      this.getSections();
    }
    this.editLoading = false;
  }

  getRecipientGroupId(id) {
    let group: RecipientGroupsModel = new RecipientGroupsModel();
    this.recipientGroupService
      .read(id)
      .pipe(first())
      .subscribe((results) => {
        group = results.data[0];
        this.formCreate.patchValue({
          ...group,
        });
        if (group.conditions.length > 0) {
          group.conditions.forEach((cond) => {
            cond.field_values = cond.field_value
              .split(',')
              .map((value) => Number(value));
            if (cond.field_name == 'active') {
              this.formCreate
                .get('field_values_status')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'user_id') {
              this.isLoadingUsers = true;
              this.usersService
                .list(0, 100, null, null, {
                  id: cond.field_values,
                })
                .pipe(
                  first(),
                  finalize(() => (this.isLoadingUsers = false))
                )
                .subscribe((res) => (this.listUsers = res.data));
              this.formCreate
                .get('field_values_user')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'user_id') {
              this.getLoadUpdateUsers(cond.field_values);
              this.formCreate
                .get('field_values_user')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'profile_id') {
              this.formCreate
                .get('field_values_profile')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'organic_unit_id') {
              this.formCreate
                .get('field_values_unit_organic')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'course_degree_id') {
              this.formCreate
                .get('field_values_degree')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'course_id') {
              this.formCreate
                .get('field_values_course')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'department_id') {
              this.formCreate
                .get('field_values_department')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'section_id') {
              this.formCreate
                .get('field_values_section')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'accommodation_contracted') {
              cond.field_value == '1'
                ? this.formCreate
                    .get('field_values_accommodation_contracted')
                    .setValue(true)
                : this.formCreate
                    .get('field_values_accommodation_contracted')
                    .setValue(false);
            }
            if (cond.field_name == 'residence_id') {
              this.formCreate
                .get('field_values_residence_id')
                .setValue(cond.field_values);
            }
            if (cond.field_name == 'ubike_bike_assigned') {
              cond.field_value == '1'
                ? this.formCreate
                    .get('field_values_bike_contracted')
                    .setValue(true)
                : this.formCreate
                    .get('field_values_bike_contracted')
                    .setValue(false);
            }
            if (cond.field_name == 'scholarship_application_approved') {
              cond.field_value == '1'
                ? this.formCreate.get('field_values_scholarship').setValue(true)
                : this.formCreate
                    .get('field_values_scholarship')
                    .setValue(false);
            }
            if (cond.field_name == 'volunteering_application_approved') {
              cond.field_value == '1'
                ? this.formCreate
                    .get('field_values_volunteering')
                    .setValue(true)
                : this.formCreate
                    .get('field_values_volunteering')
                    .setValue(false);
            }
            if (cond.field_name == 'food_tickets_purchased') {
              cond.field_value == '1'
                ? this.formCreate.get('field_values_food_ticket').setValue(true)
                : this.formCreate
                    .get('field_values_food_ticket')
                    .setValue(false);
            }
          });
          this.getCourses();
          this.getDepartments();
          this.getSections();
        }
        this.editLoading = false;
      });
  }

  getLoadUpdateUsers(users_ids: number[]) {
    this.isLoadingUsers = true;
    this.usersService
      .list(0, 100, null, null, {
        id: users_ids,
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.isLoadingUsers = false))
      )
      .subscribe((res) => {
        this.listUsers.push(...res.data);
      });
  }

  getProfiles() {
    this.profilesService
      .list(1, -1, null, null, {
        active: true,
        withRelated: false,
      })
      .pipe(first())
      .subscribe((data) => {
        this.listProfiles = data.data;
      });
  }

  getDegrees() {
    this.coursesDegreesServices
      .courseDegrees('true')
      .pipe(first())
      .subscribe((arg) => {
        this.courseDegreesList = arg.data;
      });
  }

  getOrganicUnits() {
    this.organicUnitsService
      .list(1, -1, null, null, { active: true })
      .pipe(first())
      .subscribe((results) => {
        this.organicsList = results.data;
      });
  }

  getDepartments() {
    if (
      this.formCreate.get('field_values_unit_organic').value &&
      this.formCreate.get('field_values_unit_organic').value.length > 0
    ) {
      this.departmentsService
        .departmentsArray(
          this.formCreate.get('field_values_unit_organic').value
        )
        .pipe(first())
        .subscribe((data) => {
          this.departmentsList = data.data;
        });
    }
  }

  getSections() {
    if (
      this.formCreate.get('field_values_department').value &&
      this.formCreate.get('field_values_department').value.length > 0
    ) {
      this.sectionsService
        .sectionsArray(this.formCreate.get('field_values_department').value)
        .pipe(first())
        .subscribe((data) => {
          this.sectionsList = data.data;
        });
    }
  }

  getCourses() {
    if (
      this.formCreate.get('field_values_unit_organic').value &&
      this.formCreate.get('field_values_unit_organic').value.length > 0 &&
      this.formCreate.get('field_values_degree').value &&
      this.formCreate.get('field_values_degree').value.length > 0
    ) {
      this.coursesDegreesServices
        .cursesArray(
          this.formCreate.get('field_values_degree').value,
          this.formCreate.get('field_values_unit_organic').value
        )
        .pipe(first())
        .subscribe((arg) => {
          this.coursesList = arg.data;
        });
    }
  }

  changeValue() {
    if (this.firstEditDegree) {
      this.firstEditDegree = false;
      return;
    }
    this.f.field_values_course.setValue([]);
    this.getCourses();
  }

  changeValueUnitOrganic() {
    if (this.firstEditUnit) {
      this.firstEditUnit = false;
      return;
    }
    this.f.field_values_department.setValue([]);
    this.f.field_values_course.setValue([]);
    this.getCourses();
    this.getDepartments();
  }

  changeValueDepartement() {
    if (this.firstEditDepart) {
      this.firstEditDepart = false;
      return;
    }
    this.f.field_values_section.setValue([]);
    this.getSections();
  }

  returnButton() {
    this.router.navigateByUrl('communication/recipient-groups/list');
  }

  conditionValid() {
    if (
      this.f.field_values_user.value.length > 0 ||
      this.f.field_values_profile.value.length > 0 ||
      this.f.field_values_status.value.length > 0 ||
      this.f.field_values_unit_organic.value.length > 0 ||
      this.f.field_values_degree.value.length > 0 ||
      this.f.field_values_course.value.length > 0 ||
      this.f.field_values_department.value.length > 0 ||
      this.f.field_values_section.value.length > 0 ||
      this.f.field_values_residence_id.value.length > 0 ||
      this.f.field_values_accommodation_contracted.value ||
      this.f.field_values_bike_contracted.value ||
      this.f.field_values_scholarship.value ||
      this.f.field_values_volunteering.value ||
      this.f.field_values_food_ticket.value
    ) {
      return true;
    }
    return false;
  }

  generateBody(): RecipientGroupsModel {
    let body: RecipientGroupsModel = new RecipientGroupsModel();

    if (this.f.field_values_accommodation_contracted.value) {
      let accommodation_contracted: ConditionsModel = new ConditionsModel();
      this.f.field_values_accommodation_contracted.value == true
        ? (accommodation_contracted.field_values = [1])
        : (accommodation_contracted.field_values = [0]);
      accommodation_contracted.field_name = 'accommodation_contracted';
      body.conditions.push(accommodation_contracted);
    }

    if (this.f.field_values_bike_contracted.value) {
      let bike_contracted: ConditionsModel = new ConditionsModel();
      this.f.field_values_bike_contracted.value == true
        ? (bike_contracted.field_values = [1])
        : (bike_contracted.field_values = [0]);
      bike_contracted.field_name = 'ubike_bike_assigned';
      body.conditions.push(bike_contracted);
    }

    if (this.f.field_values_scholarship.value) {
      let scholarship_contracted: ConditionsModel = new ConditionsModel();
      this.f.field_values_scholarship.value == true
        ? (scholarship_contracted.field_values = [1])
        : (scholarship_contracted.field_values = [0]);
      scholarship_contracted.field_name = 'scholarship_application_approved';
      body.conditions.push(scholarship_contracted);
    }

    if (this.f.field_values_volunteering.value) {
      let volunteering_contracted: ConditionsModel = new ConditionsModel();
      this.f.field_values_volunteering.value == true
        ? (volunteering_contracted.field_values = [1])
        : (volunteering_contracted.field_values = [0]);
      volunteering_contracted.field_name = 'volunteering_application_approved';
      body.conditions.push(volunteering_contracted);
    }
    if (this.f.field_values_food_ticket.value) {
      let ticket_contracted: ConditionsModel = new ConditionsModel();
      this.f.field_values_food_ticket.value == true
        ? (ticket_contracted.field_values = [1])
        : (ticket_contracted.field_values = [0]);
      ticket_contracted.field_name = 'food_tickets_purchased';
      body.conditions.push(ticket_contracted);
    }

    if (this.f.field_values_user.value.length > 0) {
      let user: ConditionsModel = new ConditionsModel();
      user.field_name = 'user_id';
      user.field_values = this.f.field_values_user.value;
      body.conditions.push(user);
    }
    if (this.f.field_values_profile.value.length > 0) {
      let profile: ConditionsModel = new ConditionsModel();
      profile.field_name = 'profile_id';
      profile.field_values = this.f.field_values_profile.value;
      body.conditions.push(profile);
    }
    if (this.f.field_values_status.value.length > 0) {
      let status: ConditionsModel = new ConditionsModel();
      status.field_name = 'active';
      status.field_values = this.f.field_values_status.value;
      body.conditions.push(status);
    }
    if (this.f.field_values_degree.value.length > 0) {
      let course_degree: ConditionsModel = new ConditionsModel();
      course_degree.field_name = 'course_degree_id';
      course_degree.field_values = this.f.field_values_degree.value;
      body.conditions.push(course_degree);
    }
    if (this.f.field_values_course.value.length > 0) {
      let course: ConditionsModel = new ConditionsModel();
      course.field_name = 'course_id';
      course.field_values = this.f.field_values_course.value;
      body.conditions.push(course);
    }
    if (this.f.field_values_unit_organic.value.length > 0) {
      let organic: ConditionsModel = new ConditionsModel();
      organic.field_name = 'organic_unit_id';
      organic.field_values = this.f.field_values_unit_organic.value;
      body.conditions.push(organic);
    }
    if (this.f.field_values_department.value.length > 0) {
      let depart: ConditionsModel = new ConditionsModel();
      depart.field_name = 'department_id';
      depart.field_values = this.f.field_values_department.value;
      body.conditions.push(depart);
    }
    if (this.f.field_values_section.value.length > 0) {
      let section: ConditionsModel = new ConditionsModel();
      section.field_name = 'section_id';
      section.field_values = this.f.field_values_section.value;
      body.conditions.push(section);
    }

    if (this.f.field_values_residence_id.value.length > 0) {
      let residence_id: ConditionsModel = new ConditionsModel();
      residence_id.field_name = 'residence_id';
      residence_id.field_values = this.f.field_values_residence_id.value;
      body.conditions.push(residence_id);
    }
    body.name = this.f.name.value;
    return body;
  }

  submit() {
    return new Observable((obs) => {
      this.submitted = true;
      if (this.formCreate.valid && this.conditionValid()) {
        this.isLoading = true;
        if (this.isEdit) {
          this.recipientGroupService
            .update(this.id, this.generateBody())
            .pipe(
              first(),
              finalize(() => (this.isLoading = false))
            )
            .subscribe((result) => {
              if (!this.post) {
                this.uiService.showMessage(
                  MessageType.success,
                  'Grupo alterado com sucesso'
                );

                this.returnButton();
              }
              obs.next(result);
              obs.complete();
            });
        } else {
          this.recipientGroupService
            .create(this.generateBody())
            .pipe(
              first(),
              finalize(() => (this.isLoading = false))
            )
            .subscribe((result) => {
              if (!this.post) {
                this.uiService.showMessage(
                  MessageType.success,
                  'Grupo criado com sucesso'
                );

                this.returnButton();
              }
              obs.next(result);
              obs.complete();
            });
        }
      } else {
        obs.next(false);
        obs.complete();
      }
    });
  }

  submitButton() {
    this.submit().pipe(first()).subscribe();
  }

  counter() {
    this.isLoadingCounter = true;
    this.recipientGroupService
      .count(this.generateBody())
      .pipe(
        first(),
        finalize(() => (this.isLoadingCounter = false))
      )
      .subscribe((data) => {
        this.valueCount = data.data[0];
      });
  }

  onSearchUsers(event) {
    this.isLoadingUsers = true;
    this.usersSearchChange$.next(event);
  }
}
