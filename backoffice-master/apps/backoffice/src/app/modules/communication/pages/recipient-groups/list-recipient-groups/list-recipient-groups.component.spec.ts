import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListRecipientGroupsComponent } from './list-recipient-groups.component';


describe('ListRecipientGroupsComponent', () => {
  let component: ListRecipientGroupsComponent;
  let fixture: ComponentFixture<ListRecipientGroupsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListRecipientGroupsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRecipientGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
