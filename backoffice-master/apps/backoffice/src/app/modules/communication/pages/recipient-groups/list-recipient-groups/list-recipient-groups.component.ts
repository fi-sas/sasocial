import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { RecipientGroupsModel } from "../../../models/recipient-groups.model";
import { RecipientGroupCommunicationService } from "../../../services/recipient-groups.service";

@Component({
    selector: 'fi-sas-list-recipient-groups',
    templateUrl: './list-recipient-groups.component.html',
    styleUrls: ['./list-recipient-groups.component.less']
})

export class ListRecipientGroupsComponent extends TableHelper implements OnInit {
    status = [];
    constructor(
        private recipientGroupCommunicationService: RecipientGroupCommunicationService,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        private modalService: NzModalService,
        router: Router,
        private authService: AuthService
    ) {
        super(uiService, router, activatedRoute);
        this.status = [
            {
              description: "Ativo",
              value: true
            },
            {
              description: "Desativo",
              value: false
            }
          ];
          this.persistentFilters = {
            searchFields: 'name',
          };
    }

    ngOnInit() {
        this.initTableData(this.recipientGroupCommunicationService);
    }

    edit(id) {
        if (!this.authService.hasPermission('authorization:targets:update')) {
            return;
        }

        this.router.navigateByUrl('communication/recipient-groups/update/' + id);
    }

    listComplete() {
        this.filters.active = null;
        this.filters.search = null;
        this.searchData(true)
      }

    desactive(data: RecipientGroupsModel) {
        if (!this.authService.hasPermission('authorization:targets:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai desativar este Grupo. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(data)
        });

    }

    desactiveDataSubmit(data: RecipientGroupsModel) {
        data.active = false;
        this.recipientGroupCommunicationService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Grupo desativado com sucesso'
                );
                this.initTableData(this.recipientGroupCommunicationService);
            });
    }

    active(data: RecipientGroupsModel) {
        if (!this.authService.hasPermission('authorization:targets:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai ativar este Grupo. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(data)
        });
    }

    activeDataSubmit(data: RecipientGroupsModel) {
        data.active = true;
        this.recipientGroupCommunicationService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Grupo ativado com sucesso'
                );
                this.initTableData(this.recipientGroupCommunicationService);
            });
    }

}