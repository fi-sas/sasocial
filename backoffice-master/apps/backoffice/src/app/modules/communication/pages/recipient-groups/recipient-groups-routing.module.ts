import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormRecipientGroupsComponent } from './form-recipient-groups/form-recipient-groups.component';
import { ListRecipientGroupsComponent } from './list-recipient-groups/list-recipient-groups.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListRecipientGroupsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:targets:read'},
    resolve: { }
  },
  {
    path: 'create',
    component: FormRecipientGroupsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:targets:create'},
    resolve: { }
  },
  {
    path: 'update/:id',
    component: FormRecipientGroupsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:targets:update'},
    resolve: { }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipientGroupsRoutingModule { }
