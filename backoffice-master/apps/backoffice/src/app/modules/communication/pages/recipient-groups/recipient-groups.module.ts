import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListRecipientGroupsComponent } from './list-recipient-groups/list-recipient-groups.component';
import { RecipientGroupsRoutingModule } from './recipient-groups-routing.module';
import { TargetPostsCommunicationModule } from '../target-posts/target-posts.module';

@NgModule({
  declarations: [
    ListRecipientGroupsComponent,
  ],
  imports: [ 
    CommonModule,
    SharedModule,
    RecipientGroupsRoutingModule,
    TargetPostsCommunicationModule
  ],

})
export class RecipientGroupsCommunicationModule { }
