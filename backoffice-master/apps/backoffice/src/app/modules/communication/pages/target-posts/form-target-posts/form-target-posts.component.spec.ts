import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormTargetPostsComponent } from './form-target-posts.component';


describe('FormTargetPostsComponent', () => {
  let component: FormTargetPostsComponent;
  let fixture: ComponentFixture<FormTargetPostsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormTargetPostsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTargetPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
