import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize, first } from "rxjs/operators";
import { CategoryModel } from "../../../models/category.model";
import { RecipientGroupsModel } from "../../../models/recipient-groups.model";
import { CategoriesService } from "../../../services/categories.service";
import { RecipientGroupCommunicationService } from "../../../services/recipient-groups.service";
import { FormRecipientGroupsComponent } from "../../recipient-groups/form-recipient-groups/form-recipient-groups.component";
import * as moment from 'moment';
import { NotificationMethodModel } from "@fi-sas/backoffice/modules/notifications/modules/notification-methods/models/notification-method.model";
import { NotificationsMethodsService } from "@fi-sas/backoffice/modules/notifications/modules/notification-methods/services/notifications-methods.service";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { TargetPostCommunicationService } from "../../../services/target-posts.service";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { NotificationMethodsModel, TargetPostsModel } from "../../../models/target-posts.model";
import { DomSanitizer } from "@angular/platform-browser";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";

@Component({
    selector: 'fi-sas-form-target-posts',
    templateUrl: './form-target-posts.component.html',
    styleUrls: ['./form-target-posts.component.less']
})
export class FormTargetPostsComponent implements OnInit {
    @ViewChild("dataRecipient", { static: false }) dataRecipients: FormRecipientGroupsComponent = null;
    formTargetPost = new FormGroup({
        target_id: new FormControl(null),
        category_id: new FormControl(null, [Validators.required]),
        schedule_date: new FormControl(null, [Validators.required]),
        notification_method_id: new FormControl([], [Validators.required]),
        active: new FormControl(true, [Validators.required]),
        subject: new FormControl('', [Validators.required,trimValidation]),
        simplified_message: new FormControl('', [Validators.required,trimValidation]),
        message: new FormControl('', [Validators.required]),
    });
    viewPreview = false;
    isLoadingPreview = false;
    listTarget: RecipientGroupsModel[] = [];
    listCategories: CategoryModel[] = [];
    listNotification: NotificationMethodModel[] = [];
    targetOpen = false;
    targetSelected: RecipientGroupsModel = new RecipientGroupsModel();
    id;
    loadingEdit = false;
    isEdit = false;
    errorGroup = false;
    isLoading = false;
    submitted = false;
    idTarget: number;
    get f() { return this.formTargetPost.controls; }
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
            { class: 'arial', name: 'Arial' },
            { class: 'times-new-roman', name: 'Times New Roman' },
            { class: 'calibri', name: 'Calibri' },
            { class: 'comic-sans-ms', name: 'Comic Sans MS' }
        ],
        customClasses: [
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ],
        sanitize: true,
        toolbarPosition: 'top',
        toolbarHiddenButtons: [
            ['insertImage','insertVideo']
          ]
    };
    disabledDate = (current: Date): boolean => {
        return moment(current).isBefore(moment(), 'day');
    };

    constructor(private recipientGroupService: RecipientGroupCommunicationService,
        private notificationsMethodsService: NotificationsMethodsService,
        private targetPostService: TargetPostCommunicationService,
        private uiService: UiService,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private router: Router, private categoriesService: CategoriesService,) {
        this.getTargets();
        this.getCategories();
        this.getNotifications();
    }

    ngOnInit() {
    }

   

    getTargets() {
        this.recipientGroupService.list(1, -1, null, null, {
            sort: 'name',
            active: 'true'
        }).pipe(first()).subscribe((data) => {
            this.listTarget = data.data;
            const id = this.route.snapshot.paramMap.get('id');
            if (id) {
                this.loadingEdit = true;
                    this.getTargetPostId(id);
                this.id = id;
                this.isEdit = true;
            }
        })
    }

    getTargetPostId(id) {
        let targetPost: TargetPostsModel = new TargetPostsModel();
        this.targetPostService
            .read(id)
            .pipe(
                first(), finalize(() => this.loadingEdit = false)
            )
            .subscribe((results) => {
                targetPost = results.data[0];
                this.formTargetPost.patchValue({
                    ...targetPost
                });
                this.formTargetPost.get('notification_method_id').setValue(targetPost.target_post_notification_methods.map(met => met.notification_method_id));
            })
    }

    getCategories() {
        this.categoriesService.list(1, -1).pipe(first()).subscribe((data) => {
            this.listCategories = data.data;
        })
    }

    getNotifications() {
        this.notificationsMethodsService.list(1, -1, null, null, {
            sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.listNotification = data.data;
        })
    }

    changeValue(event) {
        this.targetOpen = false;
        this.errorGroup = false;
        if (event) {
            this.targetSelected = this.listTarget.find((targ) => targ.id == event);
        }

    }


    submit() {
        this.submitted = true;
        this.errorGroup = false;
        if (!this.targetOpen && !this.formTargetPost.get('target_id').value) {
            this.errorGroup = true;
        }
        if (this.formTargetPost.valid && !this.errorGroup) {
            this.isLoading = true;
            let sendValue: TargetPostsModel = new TargetPostsModel();
            sendValue = this.formTargetPost.value;
            sendValue.target_post_notification_methods = [];
            this.formTargetPost.get('notification_method_id').value.forEach(element => {
                let method: NotificationMethodsModel = new NotificationMethodsModel();
                method.notification_method_id = element;
                sendValue.target_post_notification_methods.push(method);
            });

            this.dataRecipients.submit().pipe(first()).subscribe((targ) => {
console.log("targ");
console.log(targ);
                if(targ === false) {
                    this.isLoading = false;
                } else {
                    let aux: any = targ;
                    this.idTarget = aux.data[0].id;
                    if (this.isEdit) {
    
                        this.targetPostService.update(this.id, sendValue).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                            result => {
                                this.uiService.showMessage(
                                    MessageType.success,
                                    'Publicação alterada com sucesso'
                                );
                                this.returnButton();
                            },
                        )
                    } else {
                        sendValue.target_id = this.idTarget;
                        this.targetPostService.create(sendValue).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                            result => {
    
                                this.uiService.showMessage(
                                    MessageType.success,
                                    'Publicação criada com sucesso'
                                );
    
                                this.returnButton();
    
                            },
                        )
                    }
                }

            });

        }


    }

    returnButton() {
        this.router.navigateByUrl('communication/target-posts/list');
    }

    preview() {
        this.viewPreview = true;
    }

    getHTML(value) {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    }

    findNotification(id): string {
        if (this.viewPreview) {
            return this.listNotification.find((not) => not.id == id) ? this.listNotification.find((not) => not.id == id).name : '';
        }
    }


    findConditions(field_name) {
        if (this.viewPreview) {
            for (let i = 0; i < this.dataRecipients.generateBody().conditions.length; i++) {
                const cond = this.dataRecipients.generateBody().conditions[i];
                if (cond.field_name == 'active' && cond.field_name == field_name) {
                    return cond.field_values.map((value) => value == 0 ? 'Inativo' : 'Ativo').join(', ');
                }
                if (cond.field_name == 'course_degree_id' && cond.field_name == field_name) {
                    return this.dataRecipients.courseDegreesList.filter((degree) => {
                        return cond.field_values.includes(degree.id)
                    }).map((degree) => degree.name).join(', ');
                }
                if (cond.field_name == 'profile_id' && cond.field_name == field_name) {
                    return this.dataRecipients.listProfiles.filter((profile) => {
                        return cond.field_values.includes(profile.id)
                    }).map((profile) => profile.name).join(', ');
                }
                if (cond.field_name == 'course_id' && cond.field_name == field_name) {
                    return this.dataRecipients.coursesList.filter((couse) => {
                        return cond.field_values.includes(couse.id)
                    }).map((couse) => couse.name).join(', ');
                }
                if (cond.field_name == 'organic_unit_id' && cond.field_name == field_name) {
                    return this.dataRecipients.organicsList.filter((org) => {
                        return cond.field_values.includes(org.id)
                    }).map((org) => org.name).join(', ');
                }
                if (cond.field_name == 'department_id' && cond.field_name == field_name) {
                    return this.dataRecipients.departmentsList.filter((dep) => {
                        return cond.field_values.includes(dep.id)
                    }).map((dep) => dep.name).join(', ');
                }
                if (cond.field_name == 'section_id' && cond.field_name == field_name) {
                    return this.dataRecipients.sectionsList.filter((sec) => {
                        return cond.field_values.includes(sec.id)
                    }).map((sec) => sec.name).join(', ');
                }
            }
        }

    }

    findConditionsExist() {
        if (this.viewPreview && this.dataRecipients && this.dataRecipients.generateBody().conditions.length > 0) {
            return true;
        }

        return false;
    }

}