import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTargetPostsComponent } from './list-target-posts.component';

describe('ListTargetPostsComponent', () => {
  let component: ListTargetPostsComponent;
  let fixture: ComponentFixture<ListTargetPostsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListTargetPostsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTargetPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
