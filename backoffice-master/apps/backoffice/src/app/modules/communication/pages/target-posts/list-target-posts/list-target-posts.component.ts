import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { TargetPostCommunicationService } from "../../../services/target-posts.service";

@Component({
    selector: 'fi-sas-list-target-posts',
    templateUrl: './list-target-posts.component.html',
    styleUrls: ['./list-target-posts.component.less']
})
export class ListTargetPostsComponent extends TableHelper implements OnInit {

    constructor(
        private targetPostServices: TargetPostCommunicationService,
        private modalService: NzModalService,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        router: Router,
        private authService: AuthService
      ) {
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
          withRelated: "category,created_by,target_post_notification_methods,target"
        }
      }

    ngOnInit() {
      this.initTableData(this.targetPostServices);
    }

    validTranslateName(id_trans: number, translation: any[]) {
      return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
    }

    status(id, status, data) {
      if(!this.authService.hasPermission('communication:target_posts:status') || data.status!='WAITING_APPROVAL') {
        return;
      }
      this.modalService.confirm({
        nzTitle: 'Vai alterar o estado desta publicação. Tem a certeza que deseja continuar?',
        nzOnOk: () => {
          this.targetPostServices.changeStatus(id,status).pipe(first()).subscribe((data)=> {
            this.uiService.showMessage(
              MessageType.success,
              'Estado alterado com sucesso'
            );
            this.initTableData(this.targetPostServices);
          })
         
        }
      });

     
    }

    edit(id) {
      if(!this.authService.hasPermission('communication:target_posts:update')){
        return;
      }
      this.router.navigateByUrl('communication/target-posts/update/' + id);
    }
}