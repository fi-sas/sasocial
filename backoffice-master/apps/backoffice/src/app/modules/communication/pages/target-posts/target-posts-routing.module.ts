import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTargetPostsComponent } from './form-target-posts/form-target-posts.component';
import { ListTargetPostsComponent } from './list-target-posts/list-target-posts.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTargetPostsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'communication:target_posts:read'},
    resolve: { }
  },
  {
    path: 'create',
    component: FormTargetPostsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'communication:target_posts:create'},
    resolve: { }
  },
  {
    path: 'update/:id',
    component: FormTargetPostsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'communication:target_posts:update'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetPostsRoutingModule { }
