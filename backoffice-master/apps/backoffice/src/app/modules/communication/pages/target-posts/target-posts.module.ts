import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ListTargetPostsComponent } from './list-target-posts/list-target-posts.component';
import { TargetPostsRoutingModule } from './target-posts-routing.module';
import { FormTargetPostsComponent } from './form-target-posts/form-target-posts.component';
import { FormRecipientGroupsComponent } from '../recipient-groups/form-recipient-groups/form-recipient-groups.component';

@NgModule({
  declarations: [
    ListTargetPostsComponent,
    FormTargetPostsComponent,
    FormRecipientGroupsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TargetPostsRoutingModule,
    AngularEditorModule
  ],
  exports: [
    FormRecipientGroupsComponent
  ]
})
export class TargetPostsCommunicationModule { }
