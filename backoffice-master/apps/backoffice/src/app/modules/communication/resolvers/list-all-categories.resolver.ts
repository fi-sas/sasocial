import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { CategoryModel } from '@fi-sas/backoffice/modules/communication/models/category.model';

@Injectable()
export class ListAllCategoriesResolver
  implements Resolve<Observable<Resource<CategoryModel>>> {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  resolve() {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    return this.resourceService
      .list<CategoryModel>(this.urlService.get('COMMUNI_CATEGORIES'), {
        params
      })
      .pipe(first())
      .catch(() => {
        return EMPTY;
      });
  }
}
