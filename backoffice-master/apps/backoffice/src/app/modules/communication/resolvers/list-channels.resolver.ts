import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { ChannelModel } from '@fi-sas/backoffice/modules/communication/models/channel.model';

@Injectable()
export class ListChannelsResolver
  implements Resolve<Observable<Resource<ChannelModel>>> {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  resolve() {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    return this.resourceService
      .list<ChannelModel>(this.urlService.get('POSTS_CHANNELS'), {
        params
      })
      .pipe(first())
      .catch(() => {
        return EMPTY;
      });
  }
}
