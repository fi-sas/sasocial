

import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CategoryModel } from '../models/category.model';
import { first } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends Repository<CategoryModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'COMMUNI_CATEGORIES_ID';
    this.entities_url = 'COMMUNI_CATEGORIES';
  }
}