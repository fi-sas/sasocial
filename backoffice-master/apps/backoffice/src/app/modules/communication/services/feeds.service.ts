import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { FeedModel } from '@fi-sas/backoffice/modules/communication/models/feed.model';
import { first } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class FeedsService extends Repository<FeedModel>{
  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'FEEDS_ID';
    this.entities_url = 'FEEDS';
  }

  fetchRss(): Observable<Resource<{}>> {
    return this.resourceService
      .read<{}>(this.urlService.get('FEEDS_FETCH_RSS'), {})
      .pipe(first());

  }
}
