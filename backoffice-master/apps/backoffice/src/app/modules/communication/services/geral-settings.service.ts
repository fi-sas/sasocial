import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { GeralSettingsCommunicationModel } from '../models/geral-settings.model';

@Injectable({
  providedIn: 'root'
})
export class GeralSettingsCommunicationService extends Repository<GeralSettingsCommunicationModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'COMMUNI_CONFIG';
  }
}
