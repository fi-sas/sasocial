import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { PostModel } from '@fi-sas/backoffice/modules/communication/models/post.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class PostsService extends Repository<PostModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'POSTS_ID';
    this.entities_url = 'POSTS';
  }

  /*list(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    query?: object,
    search?: string
  ): Observable<Resource<PostModel>> {
    let params = new HttpParams();
    if (pageIndex) {
      params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
    }
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    } else {
      params = params.set('sort', '-id');
    }

    params = params.set('withRelated', 'translations,category,history,gallery,created_by');


    if(search) {
      params = params.append("search", search).append("searchFields", "title,summary,body");
    }

    if (query) {
      params = params.set('query', JSON.stringify(query));
    }
    return this.resourceService.list<PostModel>(this.urlService.get('POSTS'), {
      params
    });
  }*/

  get(id: number): Observable<Resource<PostModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'channels,category,translations,groups');
    return this.resourceService.read<PostModel>(
      this.urlService.get('POSTS_ID', { id }),
      { params }
    );
  }

  create(value: any): Observable<Resource<PostModel>> {
    return this.resourceService.create<PostModel>(
      this.urlService.get('POSTS'),
      value
    );
  }

  update(id: number, value: any): Observable<Resource<PostModel>> {
    return this.resourceService.update<PostModel>(
      this.urlService.get('POSTS_ID', { id }),
      value
    );
  }

  approvePost(
    id: number,
    notes: string,
    post: any
  ): Observable<Resource<PostModel>> {
    return this.resourceService.create<PostModel>(
      this.urlService.get('POSTS_APPROVE', { id }),
      { status: 'APPROVED', notes, post }
    );
  }

  rejectPost(
    id: number,
    notes: string,
    post: any
  ): Observable<Resource<PostModel>> {
    return this.resourceService.create<PostModel>(
      this.urlService.get('POSTS_REJECT', { id }),
      { status: 'REJECTED', notes, post }
    );
  }

  delete(id: number): Observable<any> {
    return this.resourceService.delete(this.urlService.get('POSTS_ID', { id }));
  }
}
