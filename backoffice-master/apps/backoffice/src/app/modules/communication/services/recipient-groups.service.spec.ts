import { TestBed, inject } from '@angular/core/testing';
import { RecipientGroupCommunicationService } from './recipient-groups.service';


describe('RecipientGroupCommunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipientGroupCommunicationService]
    });
  });

  it(
    'should be created',
    inject([RecipientGroupCommunicationService], (service: RecipientGroupCommunicationService) => {
      expect(service).toBeTruthy();
    })
  );
});
