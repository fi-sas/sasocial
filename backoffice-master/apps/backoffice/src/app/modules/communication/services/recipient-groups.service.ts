import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { RecipientGroupsModel } from '../models/recipient-groups.model';

@Injectable({
  providedIn: 'root'
})
export class RecipientGroupCommunicationService extends Repository<RecipientGroupsModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'USERS.TARGETS';
    this.entity_url = 'USERS.TARGETS_ID';
  }

  count(data: RecipientGroupsModel) {
    return this.resourceService.create<number>(
      this.urlService.get('USERS.TARGETS_COUNT'),
      data
    );

  }
}
