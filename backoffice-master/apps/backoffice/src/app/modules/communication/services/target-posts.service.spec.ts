import { TestBed, inject } from '@angular/core/testing';

import { TargetPostCommunicationService } from './target-posts.service';

describe('TargetPostCommunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TargetPostCommunicationService]
    });
  });

  it(
    'should be created',
    inject([TargetPostCommunicationService], (service: TargetPostCommunicationService) => {
      expect(service).toBeTruthy();
    })
  );
});
