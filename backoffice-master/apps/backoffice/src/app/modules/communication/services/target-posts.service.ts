import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { TargetPostsModel } from '../models/target-posts.model';

@Injectable({
  providedIn: 'root'
})
export class TargetPostCommunicationService extends Repository<TargetPostsModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'COMMUNI_TARGET_POSTS';
    this.entity_url = 'COMMUNI_TARGET_POSTS_ID';
  }


  changeStatus(id, status) {
    let url = this.urlService.get('COMMUNI_TARGET_POSTS_STATUS', {id}) + '?action=' + status;
    return this.resourceService.patch<TargetPostsModel>(url,{});
  }
}
