import { ComponentsService } from './../../services/components.service';
import {
  UiService,
  MessageType
} from './../../../../core/services/ui-service.service';
import { FormControl, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ComponentModel } from '@fi-sas/backoffice/modules/configurations/models/component.model';
import { Component, OnInit, Input } from '@angular/core';
import { DevicesService } from '../../services/devices.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-components-modal',
  templateUrl: './components-modal.component.html',
  styleUrls: ['./components-modal.component.less']
})
export class ComponentsModalComponent implements OnInit {
  @Input() device_id: number;
  components: ComponentModel[] = [];

  loading = false;

  formGroupComponent = new FormGroup({
    name: new FormControl('', Validators.required),
    active: new FormControl(true, Validators.required)
  });
  constructor(
    private modalRef: NzModalRef,
    private devicesService: DevicesService,
    private componentsService: ComponentsService,
    private uiService: UiService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.loadComponents();
  }

  desactivateComponent(id: number, value: boolean) {
    if (!this.authService.hasPermission('configuration:components:update')) {
      return;
    }

    this.componentsService.patch(id, { active: value }).subscribe(() => {
      this.loadComponents();
    });
  }

  removeComponentFromDevice(id: number) {
    if (!this.authService.hasPermission('configuration:components:delete')) {
      return;
    }

    this.uiService
      .showConfirm('Componente', 'Pretende eliminar este componente?')
      .subscribe(value => {
        if (value) {
          this.devicesService.removeComponentFromDevice(id).subscribe(
            result => {
              this.loadComponents();
              this.uiService.showMessage(
                MessageType.success,
                'Componente removido com sucesso'
              );
            },
            err => {}
          );
        }
      });
  }

  addComponentToDevice(value: any, valid: boolean) {
    if (!this.authService.hasPermission('configuration:components:create')) {
      return;
    }

    if (!valid) {
      this.formGroupComponent.updateValueAndValidity();
      return;
    }

    const comp = <ComponentModel>value;
    comp.device_id = this.device_id;
    this.devicesService.addComponentToDevice(comp).subscribe(
      result => {
        this.uiService.showMessage(
          MessageType.success,
          'Componente inserido com sucesso'
        );
        this.formGroupComponent.patchValue({
          name: '',
          active: true
        });
        this.loadComponents();
      },
      err => {}
    );
  }

  loadComponents() {
    this.loading = true;
    this.devicesService
      .getComponentFromDevice(this.device_id)
      .pipe(
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(components => {
        this.components = components.data;
      });
  }

  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }
}
