import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationsComponent } from '@fi-sas/backoffice/modules/configurations/configurations.component';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { TextApresentationComponent } from './pages/text-apresentation/text-apresentation.component';
import { FormInformationTextComponent } from './pages/form-information-texts/form-information-texts.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { ListRegulationComponent } from './pages/regulations/regulations.component';


const routes: Routes = [
  {
    path: '',
    component: ConfigurationsComponent,
    children: [
      {
        path: 'regulations',
        component: ListRegulationComponent,
        data: { breadcrumb: 'Regulamentos', title: 'Regulamentos' , scope:'configuration:regulations'},
      },
      {
        path: 'text-apresentation',
        component: TextApresentationComponent,
        data: { breadcrumb: 'Textos de Apresentação', title: 'Textos de Apresentação', scope:'configuration:apresentation_texts' },
      },
      {
        path: 'form-information-texts',
        component: FormInformationTextComponent,
        data: { breadcrumb: 'Textos de Informação de Formulários', title: 'Textos de Informação de Formulários', scope:'configuration:informations' },
      },
      /*{
        path: 'footer',
        loadChildren: './pages/footer/footer.module#FooterConfModule',
        data: { breadcrumb: 'Rodapé', title: 'Rodapé' },
      },*/
      {
        path: 'device',
        loadChildren: './pages/device/device.module#DeviceConfModule',
        data: { breadcrumb: 'Dispositivos', title: 'Dispositivos' },
      },
      {
        path: 'section',
        loadChildren: './pages/sections/sections.module#SectionsConfModule',
        data: { breadcrumb: 'Secções', title: 'Secções' },
      },
      {
        path: 'group',
        loadChildren: './pages/groups/groups.module#GroupsConfModule',
        data: { breadcrumb: 'Grupos', title: 'Grupos' },
      },
      {
        path: 'language',
        loadChildren: './pages/languages/language.module#LanguageConfModule',
        data: { breadcrumb: 'Linguagens', title: 'Linguagens' },
      },
      {
        path: 'tax',
        loadChildren: './pages/tax/tax.module#TaxConfModule',
        data: { breadcrumb: 'Taxas', title: 'Taxas' },
      },
      {
        path: 'service',
        loadChildren: './pages/services/services.module#ServiceConfModule',
        data: { breadcrumb: 'Serviços Internos', title: 'Serviços Internos' },
      },
      {
        path: 'external-service',
        loadChildren: './pages/external-services/external-services.module#ExternalServiceConfModule',
        data: { breadcrumb: 'Serviços Externos', title: 'Serviços Externos' },
      },
      {
        path: 'contact',
        loadChildren: './pages/contacts/contacts.module#ContactsConfModule',
        data: { breadcrumb: 'Contactos', title: 'Contactos' },
      },
      {
        path: 'course-degree',
        loadChildren: './pages/course-degree/course-degree.module#CourseDegreeConfModule',
        data: { breadcrumb: 'Graus Cursos', title: 'Graus Cursos' },
      },
      {
        path: 'course',
        loadChildren: './pages/course/course.module#CourseConfModule',
        data: { breadcrumb: 'Cursos', title: 'Cursos' },
      },
      {
        path: 'school-year',
        loadChildren: './pages/school-year/school-year.module#SchoolYearConfModule',
        data: { breadcrumb: 'Ano letivo', title: 'Ano letivo' },
      },
      {
        path: 'holiday',
        loadChildren: './pages/holiday/holiday.module#HolidayConfModule',
        data: { breadcrumb: 'Feriados', title: 'Feriados' },
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
