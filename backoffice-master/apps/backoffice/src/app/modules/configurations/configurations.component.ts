import { Component, OnInit } from '@angular/core';

import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-configurations',
  template: '<router-outlet></router-outlet>',
})
export class ConfigurationsComponent implements OnInit {
  dataConfiguration: any;
  constructor(
    private authService: AuthService,
    private configurationsService: ConfigurationGeralService,
    private uiService: UiService,
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(6), 'setting');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    /*const footers = new SiderItem('Rodapés (Kiosk)', '', '', 'configuration:footers');
    footers.addChild(new SiderItem('Criar', '', '/configurations/footer/create', 'configuration:footers:create'));
    footers.addChild(new SiderItem('Listar', '', '/configurations/footer/list', 'configuration:footers:read'));
    this.uiService.addSiderItem(footers);*/

    const bo = new SiderItem('Backoffice');
    const sections = bo.addChild(new SiderItem('Secções', '', '', 'configuration:sections_bo'));
    sections.addChild(new SiderItem('Criar', '', '/configurations/section/create', 'configuration:sections_bo:create'));
    sections.addChild(new SiderItem('Listar', '', '/configurations/section/list', 'configuration:sections_bo:read'));
    this.uiService.addSiderItem(bo);

    if (this.authService.hasPermission('configuration:regulations') || this.authService.hasPermission('configuration:apresentation_texts') || this.authService.hasPermission('configuration:informations')) {
      const wp = new SiderItem('Webpage');
      wp.addChild(new SiderItem('Regulamentos', '', '/configurations/regulations', 'configuration:regulations'));
      wp.addChild(new SiderItem('Textos de Apresentação', '', '/configurations/text-apresentation', 'configuration:apresentation_texts'));
      wp.addChild(new SiderItem('Textos de Informação Form.', '', '/configurations/form-information-texts', 'configuration:informations'));
      this.uiService.addSiderItem(wp);
    }

    const geral = new SiderItem('Geral');
    const devices = geral.addChild(new SiderItem('Dispositivos', '', '', 'configuration:devices'));
    devices.addChild(new SiderItem('Criar', '', '/configurations/device/create', 'configuration:devices:create'));
    devices.addChild(new SiderItem('Listar', '', '/configurations/device/list', 'configuration:devices:read'));

    const groups = geral.addChild(new SiderItem('Grupos', '', '', 'configuration:groups'));
    groups.addChild(new SiderItem('Criar', '', '/configurations/group/create', 'configuration:groups:create'));
    groups.addChild(new SiderItem('Listar', '', '/configurations/group/list', 'configuration:groups:read'));

    const languages = geral.addChild(new SiderItem('Línguas', '', '', 'configuration:languages'));
    languages.addChild(new SiderItem('Listar', '', '/configurations/language/list', 'configuration:languages:read'));
    languages.addChild(new SiderItem('Ordenar', '', '/configurations/language/order', 'configuration:languages:update'));

    const taxes = geral.addChild(new SiderItem('Taxas', '', '', 'configuration:taxes'));
    taxes.addChild(new SiderItem('Criar', '', '/configurations/tax/create', 'configuration:taxes:create'));
    taxes.addChild(new SiderItem('Listar', '', '/configurations/tax/list', 'configuration:taxes:read'));

    const services = geral.addChild(new SiderItem('Serviços Internos', '', '', 'configuration:services'));
    services.addChild(new SiderItem('Listar', '', '/configurations/service/list', 'configuration:services:read'));

    const externalServices = geral.addChild(new SiderItem('Serviços Externos', '', '', 'configuration:services'));
    externalServices.addChild(new SiderItem('Criar', '', '/configurations/external-service/create', 'configuration:services:create'));
    externalServices.addChild(new SiderItem('Listar', '', '/configurations/external-service/list', 'configuration:services:read'));


    const contacts = geral.addChild(new SiderItem('Contactos', '', '', 'configuration:contacts'));
    contacts.addChild(new SiderItem('Criar', '', '/configurations/contact/create', 'configuration:contacts:create'));
    contacts.addChild(new SiderItem('Listar', '', '/configurations/contact/list', 'configuration:contacts:read'));

    const courseDegrees = geral.addChild(new SiderItem('Graus Cursos', '', '', 'configuration:course-degrees'));
    courseDegrees.addChild(new SiderItem('Criar', '', '/configurations/course-degree/create', 'configuration:course-degrees:create'));
    courseDegrees.addChild(new SiderItem('Listar', '', '/configurations/course-degree/list', 'configuration:course-degrees:read'));

    const courses = geral.addChild(new SiderItem('Cursos', '', '', 'configuration:courses'));
    courses.addChild(new SiderItem('Criar', '', '/configurations/course/create', 'configuration:courses:create'));
    courses.addChild(new SiderItem('Listar', '', '/configurations/course/list', 'configuration:courses:read'));

    const schoolYear = geral.addChild(new SiderItem('Ano Letivo', '', '', 'configuration:academic_years'));
    schoolYear.addChild(new SiderItem('Criar', '', '/configurations/school-year/create', 'configuration:academic_years:create'));
    schoolYear.addChild(new SiderItem('Listar', '', '/configurations/school-year/list', 'configuration:academic_years:read'));
    this.uiService.addSiderItem(geral);

    const holidays = new SiderItem('Feriados', '', '', 'configuration:holidays');
    holidays.addChild(new SiderItem('Criar', '', '/configurations/holiday/create', 'configuration:holidays:create'));
    holidays.addChild(new SiderItem('Listar', '', '/configurations/holiday/list', 'configuration:holidays:read'));
    this.uiService.addSiderItem(holidays);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
