import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationsComponent } from './configurations.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ConfigurationsRoutingModule } from '@fi-sas/backoffice/modules/configurations/configurations-routing.module';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { DevicesService } from './services/devices.service';
import { FootersService } from './services/footers.service';
import { GroupsService } from './services/groups.service';
import { LanguagesService } from './services/languages.service';
import { ServicesService } from '../infrastructure/services/services.service';
import { ComponentsModalComponent } from './components/components-modal/components-modal.component';
import { InformationService } from './services/information.service';
import { PolicyService } from './services/policy.service';
import { TermsService } from './services/terms.service';
import { TextApresentationComponent } from './pages/text-apresentation/text-apresentation.component';
import { AcademicYearsService } from './services/academic-years.service';
import { FormInformationTextComponent } from './pages/form-information-texts/form-information-texts.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ListRegulationComponent } from './pages/regulations/regulations.component';
import { HolidaysService } from './services/holidays.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationsRoutingModule,
    AngularEditorModule
  ],
  declarations: [
    ConfigurationsComponent,
    ComponentsModalComponent,
    ListRegulationComponent,
    TextApresentationComponent,
    FormInformationTextComponent
  ],
  providers: [
    TermsService,
    PolicyService,
    InformationService,
    DevicesService,
    FootersService,
    GroupsService,
    LanguagesService,
    TaxesService,
    ServicesService,
    AcademicYearsService,
    HolidaysService
  ],
  entryComponents: [
    ComponentsModalComponent
  ]
})
export class ConfigurationModule { }
