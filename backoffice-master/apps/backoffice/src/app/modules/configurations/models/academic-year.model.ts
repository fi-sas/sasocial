export class AcademicYearModel {
  id: number;
  academic_year: string;
  start_date: Date;
  end_date: Date;
  current: boolean;
}
