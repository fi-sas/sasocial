export class ComponentModel {
  id: number;
  name: string;
  active: boolean;
  device_id: number;
}
