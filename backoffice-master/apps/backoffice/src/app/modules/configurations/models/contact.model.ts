export class ContactTranslationModel {
  language_id: 1;
  name: string;
  description: string;
}

export class ContactModel {
    email: string;
    url: string;
    translations: ContactTranslationModel[]
}
