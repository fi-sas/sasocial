import { CourseModel } from "./course.model";

export class CourseDegreeModel {
  id: number;
  name: string;
  courses: CourseModel[];
  created_at: Date;
  updated_at: Date;
}
