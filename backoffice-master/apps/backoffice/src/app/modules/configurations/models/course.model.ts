import { CourseDegreeModel } from "./course-degree.model";
import { SchoolModel } from "./school.model";

export class CourseModel {
  id: number;
  name: string;
  course_degree_id: number;
  courseDegree: CourseDegreeModel;
  organic_unit_id: number;
  school: SchoolModel;
  acronym: string;
  created_at: Date;
  updated_at: Date;
}
