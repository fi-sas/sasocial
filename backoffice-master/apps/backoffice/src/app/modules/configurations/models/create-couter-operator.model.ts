import { TranslationModel } from "@fi-sas/backoffice/modules/configurations/models/translations.model";

export class CreateCouterOperatorModel {
  id?: number;
  user_id: number;
  show_photo: boolean;
  active: boolean;
  name: string;
  photo: any;
  schedule: ScheduleModel[];
}

export class ScheduleModel {
  description: string;
  week_day: number;
  schedules: SchedulesModel[];
}

export class SchedulesModel {
  opening_hour: string;
  closing_hour: number;
}
