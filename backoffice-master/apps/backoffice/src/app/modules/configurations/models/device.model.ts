import { GroupModel } from './group.model';
import { ComponentModel } from './component.model';

export enum DeviceType {
  KIOSK = 'KIOSK',
  TV = 'TV',
  MOBILE = 'MOBILE',
  BO = 'BO',
  POS = 'POS',
  GENERIC = 'GENERIC'
}

export enum DeviceOrientation {
  LANDSCAPE = 'LANDSCAPE',
  PORTRAIT = 'PORTRAIT'
}

export class DeviceModel {
  id: number;
  uuid: string;
  name: string;
  type: DeviceType;
  orientation: DeviceOrientation;
  active: boolean;
  secondsToInactivity: number;
  groups: GroupModel[];
  CPUID: string;
  components: ComponentModel[];
}
