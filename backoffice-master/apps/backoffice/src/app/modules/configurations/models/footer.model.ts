import { GroupModel } from '@fi-sas/backoffice/modules/configurations/models/group.model';

export class FooterTranslation {
  file_id: number;
  language_id: number;
}

export class FooterModel {
  id: number;
  label: string;
  date_begin: string;
  date_end: string;
  active: boolean;
  translations: FooterTranslation[];
  groups: GroupModel[];
}
