
export class FormInformationTextsModel {
    id: number;
    name: string;
    key: string;
    translations: TranslationsModelFormInformation[]=[];
    visible: boolean;
}

export class TranslationsModelFormInformation {
    language_id: number;
    info: string;
    form_information_id: number;
}