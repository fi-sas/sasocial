import { DeviceModel } from './device.model';

export class GroupModel {
  id: number;
  name: string;
  active: boolean;
  devices: DeviceModel[];
}
