import { OrganicUnitsModel } from "../../infrastructure/models/organic-units.model";

export class HolidayModel {
    id: number;
    date: Date;
    translations: HolidayTranslationsModel[];
    organic_unit_ids?: [];
    organic_units: OrganicUnitsModel[];
    updated_at: Date;
    created_at: Date;
  }

  export class HolidayTranslationsModel {
    appointment_type_id: number;
    language_id: number;
    name: string;
  }