export class LanguageModel {
  id: number;
  name: string;
  order: number;
  acronym: string;
  active: boolean;
}
