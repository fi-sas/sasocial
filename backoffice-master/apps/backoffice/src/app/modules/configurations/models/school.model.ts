import { CourseModel } from "./course.model";

export class SchoolModel {
  id: number;
  name: string;
  acronym: string;
  courses: CourseModel[];
  created_at: Date;
  updated_at: Date;
}
