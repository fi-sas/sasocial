export class SectionModel {
    name: string;
    id: number;
    priority: number;
}