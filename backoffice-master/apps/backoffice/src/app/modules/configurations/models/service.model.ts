import { TranslationModel } from "@fi-sas/backoffice/modules/configurations/models/translations.model";

export class ServiceModel {
  id?: number;
  active: boolean;
  updated_at?: Date;
  created_at?: Date;
  target_id: number;
  translations?: TranslationModel[];
  profiles: any[];
  url: string;
  webpage_availability: boolean;
  kiosk_availability: boolean;
  app_availability: boolean;
  priority: number;
  section_bo_id: number;
}
