export class TaxModel {
  id: number;
  updated_at: Date;
  created_at: Date;
  name: string;
  description: string;
  tax_value: number;
  active: boolean;
}
