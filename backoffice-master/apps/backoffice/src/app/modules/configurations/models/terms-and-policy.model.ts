import { FileModel } from "../../medias/models/file.model";

export class TermsAndPolicyModel {
  id: number;
  language_id: number;
  service_id: number;
  text: string;
  file_ids: number[];
  files: TermsAndPolicyFilesModel[];
  created_at: Date;
  updated_at: Date;
}
export class TermsAndPolicyFilesModel {
  id: number;
  file: FileModel;
  file_id: number;
}
