import { FileModel } from "../../medias/models/file.model";

export class TextApresentationsModel {
  language_id: number;
  service_id: number;
  text: string
  id: number;
  file_ids: number[];
  files: TextApresentationsFilesModel[];
}

export class TextApresentationsFilesModel {
  id: number;
  apresentation_text_id: number;
  file: FileModel;
  file_id: number;
}
