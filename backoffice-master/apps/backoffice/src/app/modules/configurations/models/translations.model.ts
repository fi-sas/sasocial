export class TranslationModel {
  language_id: number;
  name: string;
  description: string
}
