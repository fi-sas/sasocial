import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { ContactsService } from "../../../services/contacts.service";

@Component({
  selector: 'fi-sas-list-contacts',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.less']
})
export class ListContactsComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public contactsService: ContactsService) {
    super(uiService, router, activatedRoute);

  }

  ngOnInit() {
    this.initTableData(this.contactsService);
    this.persistentFilters = {
      searchFields: "name,description"
    }
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }


  edit(id: number) {
    if(!this.authService.hasPermission('configuration:contacts:update')){
      return;
    }
    this.router.navigateByUrl('/configurations/contact/update/' + id);
  }

  delete(id: number) {
    if(!this.authService.hasPermission('configuration:contacts:delete')){
      return;
    }
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este contacto?', 'Eliminar')
      .subscribe(result => {
        if (result) {
          this.contactsService.delete(id).pipe(first()).subscribe(r => {
            this.uiService.showMessage(
              MessageType.success,
              'Contacto eliminado com sucesso'
            );
            this.initTableData(this.contactsService);
          });
        }
      });
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }
}