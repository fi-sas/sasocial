import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormContactsComponent } from './contacts-form/contacts-form.component';
import { ListContactsComponent } from './contacts-list/contacts-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListContactsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:contacts:read'},

  },
  {
    path: 'create',
    component: FormContactsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:contacts:create'},
  },
  {
    path: 'update/:id',
    component: FormContactsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:contacts:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule { }
