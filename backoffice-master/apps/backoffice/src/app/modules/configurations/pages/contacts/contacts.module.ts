import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ContactsRoutingModule } from './contacts-routing.module';
import { FormContactsComponent } from './contacts-form/contacts-form.component';
import { ListContactsComponent } from './contacts-list/contacts-list.component';

@NgModule({
  declarations: [
    ListContactsComponent,
    FormContactsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ContactsRoutingModule
  ],

})
export class ContactsConfModule { }
