import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormCourseDegreeComponent } from './course-degree-form.component';


describe('FormCourseDegreeComponent', () => {
  let component: FormCourseDegreeComponent;
  let fixture: ComponentFixture<FormCourseDegreeComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormCourseDegreeComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCourseDegreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
