import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { CourseDegreeModel } from "../../../models/course-degree.model";
import { CourseDegreesService } from "../../../services/course-degrees.service";

@Component({
    selector: 'fi-sas-form-course-degree',
    templateUrl: './course-degree-form.component.html',
    styleUrls: ['./course-degree-form.component.less']
})
export class FormCourseDegreeComponent implements OnInit {
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute, private courseDegreeService: CourseDegreesService, 
        private uiService: UiService, private router: Router) {
        
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getCourseDegreeId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getCourseDegreeId(id) {
        let courseDegree: CourseDegreeModel = new CourseDegreeModel();
        this.courseDegreeService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            courseDegree = results.data[0];
            this.formCreate.patchValue({
              ...courseDegree
            });
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
          this.isLoading = true;
            if (this.isEdit) {
             
                this.courseDegreeService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Grau de curso alterado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }else {
                this.courseDegreeService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Grau de curso criado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/course-degree/list');
    }
}