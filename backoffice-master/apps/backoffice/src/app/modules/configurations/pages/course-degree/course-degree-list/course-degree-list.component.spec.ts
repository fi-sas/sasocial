import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListCourseDegreeComponent } from './course-degree-list.component';


describe('ListCourseDegreeComponent', () => {
  let component: ListCourseDegreeComponent;
  let fixture: ComponentFixture<ListCourseDegreeComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListCourseDegreeComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCourseDegreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
