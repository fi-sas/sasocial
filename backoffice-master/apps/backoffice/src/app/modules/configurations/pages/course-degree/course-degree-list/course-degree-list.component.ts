import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { CourseDegreesService } from "../../../services/course-degrees.service";

@Component({
    selector: 'fi-sas-list-course-degree',
    templateUrl: './course-degree-list.component.html',
    styleUrls: ['./course-degree-list.component.less']
})
export class ListCourseDegreeComponent extends TableHelper implements OnInit {

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public courseDegreeService: CourseDegreesService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.courseDegreeService);
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:course-degrees:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/course-degree/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:course-degrees:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este grau de curso?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.courseDegreeService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Grau de curso eliminado com sucesso'
                );
                this.initTableData(this.courseDegreeService);
              });
            }
          });
      }
}