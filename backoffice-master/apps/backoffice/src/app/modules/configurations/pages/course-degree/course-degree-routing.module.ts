import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormCourseDegreeComponent } from './course-degree-form/course-degree-form.component';
import { ListCourseDegreeComponent } from './course-degree-list/course-degree-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListCourseDegreeComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:course-degrees:read'},

  },
  {
    path: 'create',
    component: FormCourseDegreeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:course-degrees:create'},
  },
  {
    path: 'update/:id',
    component: FormCourseDegreeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:course-degrees:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseDegreeRoutingModule { }
