import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CourseDegreeRoutingModule } from './course-degree-routing.module';
import { FormCourseDegreeComponent } from './course-degree-form/course-degree-form.component';
import { ListCourseDegreeComponent } from './course-degree-list/course-degree-list.component';

@NgModule({
  declarations: [
    FormCourseDegreeComponent,
    ListCourseDegreeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CourseDegreeRoutingModule
  ],

})
export class CourseDegreeConfModule { }
