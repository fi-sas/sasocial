import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { CoursesDegreesServices } from "@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/courses-degrees.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { CourseDegreeModel } from "../../../models/course-degree.model";
import { CourseModel } from "../../../models/course.model";
import { CoursesService } from "../../../services/courses.service";

@Component({
    selector: 'fi-sas-form-course',
    templateUrl: './course-form.component.html',
    styleUrls: ['./course-form.component.less']
})
export class FormCourseComponent implements OnInit {
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        external_ref: new FormControl('', [trimValidation]),
        acronym: new FormControl('', [Validators.required, trimValidation]),
        course_degree_id: new FormControl(null, [Validators.required]),
        organic_unit_id: new FormControl(null, [Validators.required]),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;
    unitOrganic: OrganicUnitsModel[] = [];
    lisCourseDegree:CourseDegreeModel[]=[];

    constructor(private route: ActivatedRoute, private courseService: CoursesService,
        private uiService: UiService, private router: Router, private unitOrganicService: OrganicUnitsService,
        private courseDegreeService: CoursesDegreesServices) {

    }

    ngOnInit() {
        this.getCoursesDregrees();
        this.getUnitOrganics();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getCourseId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getUnitOrganics() {
        this.unitOrganicService.list(1, -1, null, null, {
          sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.unitOrganic = data.data;
        })
    }

    getCoursesDregrees() {
        this.courseDegreeService.courseDegrees('true').pipe(first()).subscribe((data) => {
            this.lisCourseDegree = data.data;
        })
    }

    getCourseId(id) {
        let course: CourseModel = new CourseModel();
        this.courseService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            course = results.data[0];
            this.formCreate.patchValue({
              ...course
            });
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
          this.isLoading = true;
            if (this.isEdit) {
             
                this.courseService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Curso alterado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }else {
                this.courseService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Curso criado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/course/list');
    }
}