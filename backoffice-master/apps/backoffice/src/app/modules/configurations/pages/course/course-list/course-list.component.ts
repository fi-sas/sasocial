import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { first } from "rxjs/operators";
import { CoursesService } from "../../../services/courses.service";
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";


@Component({
    selector: 'fi-sas-list-course',
    templateUrl: './course-list.component.html',
    styleUrls: ['./course-list.component.less']
})
export class ListCourseComponent extends TableHelper implements OnInit {
    unitOrganic: OrganicUnitsModel[] = [];
    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        public courseService: CoursesService,
        private authService: AuthService,
        public unitOrganicService: OrganicUnitsService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.courseService);
        this.getUnitOrganic();
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:courses:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/course/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:courses:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este curso?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.courseService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Curso eliminado com sucesso'
                );
                this.initTableData(this.courseService);
              });
            }
          });
      }

      getUnitOrganic() {
        this.unitOrganicService.list(1,-1).pipe(first()).subscribe((data)=> {
          this.unitOrganic = data.data;
        })
      }

      findUnitOrganic(id) {
        return this.unitOrganic.find((unit)=> unit.id == id) ? this.unitOrganic.find((unit)=> unit.id == id).name : ''
      }
}