import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormCourseComponent } from './course-form/course-form.component';
import { ListCourseComponent } from './course-list/course-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListCourseComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:courses:read'},

  },
  {
    path: 'create',
    component: FormCourseComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:courses:create'},
  },
  {
    path: 'update/:id',
    component: FormCourseComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:courses:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }
