import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListCourseComponent } from './course-list/course-list.component';
import { FormCourseComponent } from './course-form/course-form.component';
import { CourseRoutingModule } from './course-routing.module';

@NgModule({
  declarations: [
    ListCourseComponent,
    FormCourseComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CourseRoutingModule
  ],

})
export class CourseConfModule { }
