import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { DevicesService } from "../../../services/devices.service";

@Component({
    selector: 'fi-sas-form-device',
    templateUrl: './device-form.component.html',
    styleUrls: ['./device-form.component.less']
})
export class FormDeviceComponent implements OnInit {
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        type: new FormControl(null, [Validators.required]),
        orientation: new FormControl(null, [Validators.required]),
        secondsToInactivity: new FormControl(null),
        CPUID: new FormControl(null),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute, private deviceService: DevicesService, 
        private uiService: UiService, private router: Router) {
        
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getDeviceId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getDeviceId(id) {
        let device: DeviceModel = new DeviceModel();
        this.deviceService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            device = results.data[0];
            this.formCreate.patchValue({
              ...device
            });
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
          this.isLoading = true;
            if (this.isEdit) {
             
                this.deviceService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Dispositivo alterado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }else {
                this.deviceService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Dispositivo criado com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/device/list');
    }
}