import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { DevicesService } from "../../../services/devices.service";

@Component({
    selector: 'fi-sas-list-device',
    templateUrl: './device-list.component.html',
    styleUrls: ['./device-list.component.less']
})
export class ListDeviceComponent extends TableHelper implements OnInit {

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public deviceService: DevicesService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.deviceService);
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:devices:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/device/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:devices:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este dispositivo?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.deviceService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Dispositivo eliminado com sucesso'
                );
                this.initTableData(this.deviceService);
              });
            }
          });
      }
}