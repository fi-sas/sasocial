import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDeviceComponent } from './device-form/device-form.component';
import { ListDeviceComponent } from './device-list/device-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListDeviceComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:devices:read'},

  },
  {
    path: 'create',
    component: FormDeviceComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:devices:create'},
  },
  {
    path: 'update/:id',
    component: FormDeviceComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:devices:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule { }
