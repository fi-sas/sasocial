import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { DeviceRoutingModule } from './device-routing.module';
import { ListDeviceComponent } from './device-list/device-list.component';
import { FormDeviceComponent } from './device-form/device-form.component';

@NgModule({
  declarations: [
    FormDeviceComponent,
    ListDeviceComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DeviceRoutingModule
  ],

})
export class DeviceConfModule { }
