import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormExternalServicesComponent } from './form-external-services/form-external-services.component';
import { ListExternalServicesComponent } from './list-external-services/list-external-services.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListExternalServicesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:services:read'},

  },
  {
    path: 'update/:id',
    component: FormExternalServicesComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:services:update'},
  },
  {
    path: 'create',
    component: FormExternalServicesComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:services:create'},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalServiceRoutingModule { }
