import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ExternalServiceRoutingModule } from './external-services-routing.module';
import { ListExternalServicesComponent } from './list-external-services/list-external-services.component';
import { FormExternalServicesComponent } from './form-external-services/form-external-services.component';

@NgModule({
  declarations: [
    ListExternalServicesComponent,
    FormExternalServicesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ExternalServiceRoutingModule
  ],

})
export class ExternalServiceConfModule { }
