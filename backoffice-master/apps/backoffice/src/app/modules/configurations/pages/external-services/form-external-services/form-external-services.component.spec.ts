import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormExternalServicesComponent } from './form-external-services.component';


describe('FormExternalServicesComponent', () => {
  let component: FormExternalServicesComponent;
  let fixture: ComponentFixture<FormExternalServicesComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormExternalServicesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExternalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
