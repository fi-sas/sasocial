import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { NzTabSetComponent } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { hasOwnProperty } from "tslint/lib/utils";
import { LanguageModel } from "../../../models/language.model";
import { ServiceModel } from "../../../models/service.model";
import { LanguagesService } from "../../../services/languages.service";
import { ServicesService } from "../../../services/services.service";
import * as _ from 'lodash';
import { ProfilesService } from "@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service";
import { ProfileModel } from "@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model";

@Component({
    selector: 'fi-sas-form-services-external',
    templateUrl: './form-external-services.component.html',
    styleUrls: ['./form-external-services.component.less']
})
export class FormExternalServicesComponent implements OnInit {
    @ViewChild('translationTabs', { static: true })
    translationTabs: NzTabSetComponent;
    errorTrans = false;
    id;
    formCreate = new FormGroup({
        translations: new FormArray([]),
        external_link: new FormControl('', [Validators.required, trimValidation]),
        is_external_service: new FormControl(true, [Validators.required]),
        icon: new FormControl(''),
        profiles: new FormControl([], [Validators.required]),
        active: new FormControl(false, [Validators.required]),
        kiosk_availability: new FormControl(false, [Validators.required]),
        webpage_availability: new FormControl(false, [Validators.required]),
        app_availability: new FormControl(false, [Validators.required]),
    });

    translations = this.formCreate.get('translations') as FormArray;
    submitted = false;
    get f() { return this.formCreate.controls; }
    languages: LanguageModel[] = [];
    languages_loading = false;
    listOfSelectedLanguages = [];
    isUpdate = false;
    isLoading = false;
    profiles: ProfileModel[] = []; 
    constructor(
        private route: ActivatedRoute, private languagesService: LanguagesService,
        private router: Router, private serviceServices: ServicesService, private uiService: UiService,
        private profilesService: ProfilesService) {
        this.getProfiles()
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getServiceId(id);
            this.id = id;
            this.isUpdate = true;
        } else {
            this.loadLanguages('');
        }
    }

    getProfiles(){
        this.profilesService.list(1,-1).pipe(first()).subscribe((data)=>{
            this.profiles = data.data;
        })
    }

    getServiceId(id) {
        this.languages_loading = false;
        let service: ServiceModel = new ServiceModel();
        this.serviceServices
            .read(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                service = results.data[0];
                this.formCreate.patchValue({
                    ...service
                });
                if(service.profiles) {
                    this.formCreate.get('profiles').setValue(service.profiles.map(profile => profile.profile_id));
                }
                this.listOfSelectedLanguages = [];
                this.loadLanguages(results.data[0]);
            });
    }

    loadLanguages(data: any) {
        this.languages_loading = true;
        this.languagesService
            .list(1, -1)
            .pipe(
                first(),
                finalize(() => (this.languages_loading = false))
            )
            .subscribe((results) => {
                this.languages = results.data;
                if (data) {
                    this.startTranslation(data);
                }
                else {
                    this.startTranslation('');
                }
            });
    }

    convertTranslationsToLanguageIDS(translations: any) {
        let languagesIDS = [];
        translations.value.forEach((languageID: any) => {
            languagesIDS.push(languageID.language_id);
        });
        return languagesIDS;
    }

    changeLanguage() {
        this.errorTrans = false;
        const languagesIDS = this.convertTranslationsToLanguageIDS(this.translations);
        if (this.listOfSelectedLanguages.length > languagesIDS.length) {
            this.addTranslation(
                _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
                null
            );
        } else {
            this.translations.removeAt(
                this.translations.value.findIndex(
                    (trans: any) =>
                        trans.language_id ===
                        _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
                )
            );
        }
    }

    addTranslation(language_id: number, name?: string, description?: string) {
        this.translations.push(
            new FormGroup({
                language_id: new FormControl(language_id, Validators.required),
                name: new FormControl(name, [Validators.required, trimValidation]),
                description: new FormControl(description, [Validators.required, trimValidation])
            })
        );
    }

    startTranslation(value) {
        if (value !== '') {
            value.translations.map((translation) => {
                this.addTranslation(
                    translation.language_id,
                    translation.name,
                    translation.description
                );
                this.listOfSelectedLanguages.push(translation.language_id);
            });
        } else {
            if (hasOwnProperty(this.languages[0], 'id')) {
                this.addTranslation(this.languages[0].id, null);
                this.listOfSelectedLanguages.push(this.languages[0].id);
            }
        }
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }

    returnButton() {
        this.router.navigateByUrl('/configurations/external-service/list');
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.get('translations').value.length == 0) {
            this.errorTrans = true;
        }
        let tabIndex = 0;
        for (const t in this.translations.controls) {
            if (t) {
                const tt = this.translations.get(t) as FormGroup;
                if (!tt.valid) {
                    this.translationTabs.nzSelectedIndex = tabIndex;
                    break;
                }
            }
            tabIndex++;
        }


        if (this.formCreate.valid && !this.errorTrans) {
            let profiles = [];
            this.formCreate.get('profiles').value.forEach(prof => {
                profiles.push({ profile_id: prof});
            });
            const sendValues = this.formCreate.value;
            sendValues.profiles = profiles;
            this.isLoading = true;

            if (this.isUpdate) {
                this.serviceServices.update(this.id, sendValues).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Serviço externo alterado com sucesso'
                        );
                        this.isLoading = false;
                        this.returnButton();
                    },
                )
            } else {
                this.serviceServices.create(sendValues).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Serviço externo criado com sucesso'
                        );
                        this.isLoading = false;
                        this.returnButton();
                    },
                )
            }

        }
    }
}