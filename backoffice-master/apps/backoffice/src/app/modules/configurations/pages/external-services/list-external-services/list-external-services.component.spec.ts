import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListExternalServicesComponent } from './list-external-services.component';


describe('ListExternalServicesComponent', () => {
  let component: ListExternalServicesComponent;
  let fixture: ComponentFixture<ListExternalServicesComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListExternalServicesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExternalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
