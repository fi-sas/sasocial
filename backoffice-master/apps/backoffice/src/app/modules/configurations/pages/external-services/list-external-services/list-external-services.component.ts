import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { ServicesService } from "../../../services/services.service";

@Component({
  selector: 'fi-sas-form-external-services',
  templateUrl: './list-external-services.component.html',
  styleUrls: ['./list-external-services.component.less']
})
export class ListExternalServicesComponent extends TableHelper implements OnInit {
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public serviceService: ServicesService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name,description",
      is_external_service: 'true'
    }
  }


  ngOnInit() {
    this.initTableData(this.serviceService);
  
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }



  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }

  edit(id) {
    if(!this.authService.hasPermission('configuration:services:update')){
      return;
    }
    this.router.navigateByUrl('/configurations/external-service/update/'+id);
  }

  delete(id) {
    if(!this.authService.hasPermission('configuration:services:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este serviço externo?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.serviceService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Serviço externo eliminado com sucesso'
              );
              this.initTableData(this.serviceService);
            });
          }
        });
  }

}