import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NzTabSetComponent } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { GroupModel } from "../../../models/group.model";
import { LanguageModel } from "../../../models/language.model";
import { GroupsService } from "../../../services/groups.service";
import { LanguagesService } from "../../../services/languages.service";
import * as _ from 'lodash';
import { hasOwnProperty } from "tslint/lib/utils";
import { FootersService } from "../../../services/footers.service";
import { FooterModel } from "../../../models/footer.model";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";

@Component({
    selector: 'fi-sas-form-footer',
    templateUrl: './footer-form.component.html',
    styleUrls: ['./footer-form.component.less']
})
export class FormFooterComponent implements OnInit {
    @ViewChild('translationTabs', { static: true })
    translationTabs: NzTabSetComponent;
    errorTrans = false;
    id;
    formCreate = new FormGroup({
        translations: new FormArray([]),
        label: new FormControl('', [Validators.required, trimValidation]),
        dateRange: new FormControl(null, [Validators.required]),
        group_ids: new FormControl(null, [Validators.required])
    });

    translations = this.formCreate.get('translations') as FormArray;
    submitted = false;
    get f() { return this.formCreate.controls; }
    languages: LanguageModel[] = [];
    languages_loading = false;
    listOfSelectedLanguages = [];
    lisGroups: GroupModel[] = [];
    isUpdate = false;
    isLoading = false;

    constructor(private groupsService: GroupsService,
        private route: ActivatedRoute, private languagesService: LanguagesService,
        private router: Router, private footerService: FootersService, private uiService: UiService) { }

    ngOnInit() {
        this.getGroups();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getFooterId(id);
            this.id = id;
            this.isUpdate = true;

        } else {
            this.loadLanguages('');
        }
    }

    getFooterId(id) {
        this.languages_loading = false;
        let footer: FooterModel = new FooterModel();
        this.footerService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            footer = results.data[0];
            this.formCreate.patchValue({
              ...footer
            });
           if (footer.groups.length>0) {
                let grouIdArray = [];
                for (let aux of footer.groups) {
                  grouIdArray.push(aux.id);
                }
                this.formCreate.controls.group_ids.patchValue(grouIdArray);
              }
            this.formCreate.get('dateRange').setValue(([results.data[0].date_begin, results.data[0].date_end]));
            this.listOfSelectedLanguages = [];
            this.loadLanguages(results.data[0]);
          });
    }

    getGroups() {
        this.groupsService.list(1, -1, null, null, {
            sort: 'name'
        })
            .pipe(
                first(),
            )
            .subscribe(
                (result) => {
                    this.lisGroups = result.data
                },
                (error) => { }
            )
    }

    loadLanguages(data: any) {
        this.languages_loading = true;
        this.languagesService
            .list(1, -1)
            .pipe(
                first(),
                finalize(() => (this.languages_loading = false))
            )
            .subscribe((results) => {
                this.languages = results.data;
                if (data) {
                    this.startTranslation(data);
                }
                else {
                    this.startTranslation('');
                }
            });
    }

    convertTranslationsToLanguageIDS(translations: any) {
        let languagesIDS = [];
        translations.value.forEach((languageID: any) => {
            languagesIDS.push(languageID.language_id);
        });
        return languagesIDS;
    }

    changeLanguage() {
        this.errorTrans = false;
        const languagesIDS = this.convertTranslationsToLanguageIDS(this.translations);
        if (this.listOfSelectedLanguages.length > languagesIDS.length) {
            this.addTranslation(
                _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
                null
            );
        } else {
            this.translations.removeAt(
                this.translations.value.findIndex(
                    (trans: any) =>
                        trans.language_id ===
                        _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
                )
            );
        }
    }

    addTranslation(language_id: number, file_id?: number) {
        this.translations.push(
            new FormGroup({
                language_id: new FormControl(language_id, Validators.required),
                file_id: new FormControl(file_id, [Validators.required])
            })
        );
    }

    startTranslation(value) {
        if (value !== '') {
            value.translations.map((translation) => {
                this.addTranslation(
                    translation.language_id,
                    translation.file_id
                );
                this.listOfSelectedLanguages.push(translation.language_id);
            });
        } else {
            if (hasOwnProperty(this.languages[0], 'id')) {
                this.addTranslation(this.languages[0].id, null);
                this.listOfSelectedLanguages.push(this.languages[0].id);
            }
        }
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }

    returnButton() {
        this.router.navigateByUrl('/configurations/footer/list');
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.get('translations').value.length == 0) {
            this.errorTrans = true;
        }
        let tabIndex = 0;
        for (const t in this.translations.controls) {
            if (t) {
                const tt = this.translations.get(t) as FormGroup;
                if (!tt.valid) {
                    this.translationTabs.nzSelectedIndex = tabIndex;
                    break;
                }
            }
            tabIndex++;
        }


        if (this.formCreate.valid && !this.errorTrans) {
            this.isLoading = true;
            const body: any = {
                translations: this.formCreate.get('translations').value,
                group_ids: this.formCreate.get('group_ids').value,
                label: this.formCreate.get('label').value,
                date_begin: this.formCreate.get('dateRange').value[0],
                date_end: this.formCreate.get('dateRange').value[1]
            }
            if (this.isUpdate) {
                this.footerService.update(this.id, body).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                  result => {
                    this.uiService.showMessage(
                      MessageType.success,
                      'Rodapé alterado com sucesso'
                    );
                    this.isLoading = false;
                    this.router.navigateByUrl('configurations/footer/list');
                  },
                )
            }else {
                this.footerService.create(body).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Rodapé criado com sucesso'
                      );
                      this.isLoading = false;
                      this.router.navigateByUrl('configurations/footer/list');
                    },
                  )
            }
        }
    }
}