import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { FootersService } from "../../../services/footers.service";

@Component({
    selector: 'fi-sas-list-footer',
    templateUrl: './footer-list.component.html',
    styleUrls: ['./footer-list.component.less']
})
export class ListFooterComponent extends TableHelper implements OnInit {

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public footersService: FootersService) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['withRelated'] = 'groups,translations';
        
    }

    ngOnInit(){
        this.initTableData(this.footersService);
        this.persistentFilters = {
            searchFields: "label"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.searchData(true)
    }

    validTranslate(id_trans: number, translation: any[]) {
        return translation.find((tran)=> tran.language_id == id_trans) ? true : false;
    }

    edit(id: number) {
      if(!this.authService.hasPermission('configuration:footers:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/footer/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:footers:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este rodapé?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.footersService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Rodapé eliminado com sucesso'
                );
                this.initTableData(this.footersService);
              });
            }
          });
      }
}