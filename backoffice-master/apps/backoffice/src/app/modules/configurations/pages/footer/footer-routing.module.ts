import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFooterComponent } from './footer-form/footer-form.component';
import { ListFooterComponent } from './footer-list/footer-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListFooterComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:footers:read'},

  },
  {
    path: 'create',
    component: FormFooterComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:footers:create'},
  },
  {
    path: 'update/:id',
    component: FormFooterComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:footers:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FooterRoutingModule { }
