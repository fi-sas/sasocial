import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FooterRoutingModule } from './footer-routing.module';
import { FormFooterComponent } from './footer-form/footer-form.component';
import { ListFooterComponent } from './footer-list/footer-list.component';

@NgModule({
  declarations: [
    FormFooterComponent,
    ListFooterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FooterRoutingModule
  ],

})
export class FooterConfModule { }
