import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormInformationTextComponent } from './form-information-texts.component';

describe('FormInformationTextComponent', () => {
  let component: FormInformationTextComponent;
  let fixture: ComponentFixture<FormInformationTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInformationTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInformationTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
