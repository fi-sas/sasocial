import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { NzTabSetComponent } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { FormInformationTextsModel, TranslationsModelFormInformation } from "../../models/form-information-texts.model";
import { LanguageModel } from "../../models/language.model";
import { FormInformationTextsService } from "../../services/form-information-text.service";
import { LanguagesService } from "../../services/languages.service";


@Component({
    selector: 'fi-sas-form-information-texts',
    templateUrl: './form-information-texts.component.html',
    styleUrls: ['./form-information-texts.component.less']
})
export class FormInformationTextComponent implements OnInit {
    @ViewChild('translationTabs', { static: true })
    translationTabs: NzTabSetComponent;
    loading = false;
    firstEdit: boolean = false;
    languages: LanguageModel[] = [];
    submitted: boolean;
    dataText: FormInformationTextsModel[] = [];
    idEdit: number;
    formInformationtextGroup = new FormGroup({
        formName: new FormControl(''),
        name: new FormControl(''),
        visible: new FormControl(false),
        translations: new FormArray([]),

    });
    translations = this.formInformationtextGroup.get('translations') as FormArray;

    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
            { class: 'arial', name: 'Arial' },
            { class: 'times-new-roman', name: 'Times New Roman' },
            { class: 'calibri', name: 'Calibri' },
            { class: 'comic-sans-ms', name: 'Comic Sans MS' }
        ],
        customClasses: [
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ],
        sanitize: true,
        toolbarPosition: 'top',
        toolbarHiddenButtons: [
            ['insertImage','insertVideo']
          ]
    };

    constructor(private formInformationTextsService: FormInformationTextsService,
        private languagesService: LanguagesService, private uiService: UiService,) {
    }

    ngOnInit() {
        this.getInfo();
        this.getLanguages();
    }

    addTranslation(language_id: number, info?: string) {
        this.translations.push(
            new FormGroup({
                language_id: new FormControl(language_id, Validators.required),
                info: new FormControl(info, Validators.required),
            })
        );
    }

    getLanguageName(language_id: number) { 
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }

    getInfo() {
        this.formInformationTextsService.list(1, -1, null, null, {
            sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.dataText = data.data;
        })
    }

    loadInfo(event) {
        this.loading = true;
        if (this.firstEdit) {
            this.firstEdit = false;
            this.loading = false;
            return;
        }
        this.formInformationTextsService.getInfoByKey(event).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
            this.idEdit = data.data[0].id;
            this.formInformationtextGroup.get('name').setValue(data.data[0].name);
            this.formInformationtextGroup.get('visible').setValue(data.data[0].visible);
            this.translations.clear();
             if (data.data[0].translations && data.data[0].translations.length>0) {
                data.data[0].translations.map((trans)=> {
                    this.addTranslation(trans.language_id, trans.info);
                })
            }else {
                this.languages.map((lang) => {
                    this.addTranslation(lang.id, '');
                })
            }
            this.changeValue(data.data[0].visible);
        })

    }

    getLanguages() {
        this.languagesService
            .list(1, -1)
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.languages = results.data;
            });
    }


    edit() {
        this.submitted = true;

        if (this.formInformationtextGroup.valid) {
             this.submitted = false;
             let sendValue: FormInformationTextsModel = new FormInformationTextsModel();
             sendValue = this.formInformationtextGroup.value;
             sendValue.id = this.idEdit;
             sendValue.key = this.formInformationtextGroup.get('formName').value;
             this.formInformationTextsService.update(this.idEdit, sendValue).pipe(first()).subscribe(() => {
                 this.uiService.showMessage(
                     MessageType.success,
                     'Textos de Informação alterados com sucesso!'
                 );
                 this.submitted = false;
                 this.firstEdit = true;
                 this.formInformationtextGroup.get('formName').setValue(null);
                 this.formInformationtextGroup.get('name').setValue(null);
                 this.translations.clear();
             })
         }
    }

    changeValue(event) {
        let add = this.formInformationtextGroup.get('translations') as FormArray;

        if (event == true) {
            add.controls.map(control => {
                control.get('info').setValidators([Validators.required]);
            });
        }else {
            add.controls.map(control => {
                control.get('info').clearValidators();
            });
        }
        add.controls.map(control => {
            control.get('info').updateValueAndValidity();
        });
    }

}
