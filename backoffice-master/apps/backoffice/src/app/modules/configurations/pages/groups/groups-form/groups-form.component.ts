import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { DeviceModel } from "../../../models/device.model";
import { GroupModel } from "../../../models/group.model";
import { DevicesService } from "../../../services/devices.service";
import { GroupsService } from "../../../services/groups.service";

@Component({
    selector: 'fi-sas-form-groups',
    templateUrl: './groups-form.component.html',
    styleUrls: ['./groups-form.component.less']
})
export class FormGroupsComponent implements OnInit {

    devices: DeviceModel[] = [];
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        devices: new FormControl(null, [Validators.required]),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private deviceService: DevicesService, private route: ActivatedRoute,
        private uiService: UiService, private router: Router, private groupsService: GroupsService) { }

    ngOnInit() {
        this.getDevices();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getGroupsId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getDevices() {
        this.deviceService.list(1, -1, null, null, {
          sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.devices = data.data;
        })
    }

    getGroupsId(id) {
        let group: GroupModel = new GroupModel();
        this.groupsService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            group = results.data[0];
            this.formCreate.patchValue({
              ...group
            });
            if (group.devices.length>0) {
                let deviceArray = [];
                for (let aux of group.devices) {
                    deviceArray.push(aux.id);
                }
                this.formCreate.controls.devices.patchValue(deviceArray);
              }
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
          this.isLoading = true;
            if (this.isEdit) {
                this.groupsService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Grupo alterado com sucesso'
                      );
                      this.isLoading = false;
                      this.returnButton();
                    },
                  )
            }else {
                this.groupsService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Grupo criado com sucesso'
                      );
                      this.isLoading = false;
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/group/list');
    }
}