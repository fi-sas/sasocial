import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { GroupsService } from "../../../services/groups.service";

@Component({
    selector: 'fi-sas-list-groups',
    templateUrl: './groups-list.component.html',
    styleUrls: ['./groups-list.component.less']
})
export class ListGroupsComponent extends TableHelper implements OnInit {

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public groupsService: GroupsService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.groupsService);
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:groups:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/group/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:groups:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este grupo?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.groupsService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Gropo eliminado com sucesso'
                );
                this.initTableData(this.groupsService);
              });
            }
          });
      }
}