import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormGroupsComponent } from './groups-form/groups-form.component';
import { ListGroupsComponent } from './groups-list/groups-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListGroupsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:groups:read'},

  },
  {
    path: 'create',
    component: FormGroupsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:groups:create'},
  },
  {
    path: 'update/:id',
    component: FormGroupsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:groups:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
