import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { GroupRoutingModule } from './groups-routing.module';
import { ListGroupsComponent } from './groups-list/groups-list.component';
import { FormGroupsComponent } from './groups-form/groups-form.component';

@NgModule({
  declarations: [
    ListGroupsComponent,
    FormGroupsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GroupRoutingModule
  ],

})
export class GroupsConfModule { }
