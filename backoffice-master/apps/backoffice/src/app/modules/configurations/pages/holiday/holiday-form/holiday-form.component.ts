import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { HolidayModel } from '../../../models/holiday.model';
import { LanguageModel } from '../../../models/language.model';
import { HolidaysService } from '../../../services/holidays.service';
import { LanguagesService } from '../../../services/languages.service';

@Component({
  selector: 'fi-sas-holiday-form',
  templateUrl: './holiday-form.component.html',
  styleUrls: ['./holiday-form.component.less']
})
export class HolidayFormComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  loadingLanguages = false;
  languages: LanguageModel[] = [];
  
  formCreate = new FormGroup({
    date: new FormControl('', [Validators.required]),
    translations: new FormArray([]),
    organic_unit_ids: new FormControl(null, []),
  });

  submitted = false;
  get f() { return this.formCreate.controls; }
  idToUpdate = null;;
  isEdit = false;
  isLoading = false;
  organicUnits: OrganicUnitsModel[] = [];
  organicUnitsLoading = false;

  translations = this.formCreate.get('translations') as FormArray;

  constructor(
    private route: ActivatedRoute, 
    private holidayService: HolidaysService, 
    private languagesService: LanguagesService, 
    private uiService: UiService, 
    private router: Router, 
    private organicUnitsService: OrganicUnitsService) {
    
}

  ngOnInit() {
    this.getOrganicUnits();
    this.loadLanguages();
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loadingLanguages = false))
      )
      .subscribe((results) => {
        this.languages = results.data;

        this.getHoliday();
      });
  }

  getHoliday(){
    this.route.paramMap.subscribe((data) => {
      if (data.get('id')) {
        let holiday: HolidayModel = new HolidayModel();
        this.isLoading = true;
        this.idToUpdate = data.get('id');
        this.holidayService
          .read(parseInt(data.get('id'), 10), {
            withRelated: 'translations, organic_units',
          })
          .pipe(
            first(),
            finalize(()=> this.isLoading = false)
          )
          .subscribe((result) => {
            holiday = result.data[0];
            if (result.data[0].translations) {
              result.data[0].translations.map((t) => {
                this.addTranslation(t.language_id, t.name);
              });
            }
            this.formCreate.patchValue({
              date: holiday.date,
              organic_unit_ids: (holiday.organic_units ||[]).map(res => res.id)
            })
            this.languages.map((l) => {
              const foundLnaguage = this.translations.value.find(
                (t) => t.language_id === l.id
              );
              if (!foundLnaguage) {
                this.addTranslation(l.id, '');
              }
            });
          });
      } else {
        this.languages.map((l) => {
          this.addTranslation(l.id, '');
        });
      }
    });
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.formCreate.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }
  
  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  getOrganicUnits(){
    this.organicUnitsLoading = true;
    this.organicUnitsService.list(1, -1).pipe(
      first(),
      finalize(() => this.organicUnitsLoading = false)).subscribe(results => {
        this.organicUnits = results.data;
      });
  }
  submit(value: any) {
    this.submitted = true;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.formCreate.valid) {
      this.isLoading = true;
      this.submitted = false;
      if (this.idToUpdate) {
        this.holidayService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Feriado alterado com sucesso'
            );
          });
      } else {
        this.holidayService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Feriado criado com sucesso'
            );
          });
      }
    }
  }

  backList() {
      this.router.navigateByUrl('configurations/holiday/list');
  }

}
