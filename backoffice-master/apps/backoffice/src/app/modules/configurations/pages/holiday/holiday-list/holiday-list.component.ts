import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { HolidaysService } from '../../../services/holidays.service';
import { first } from "rxjs/operators";

@Component({
  selector: 'fi-sas-holiday-list',
  templateUrl: './holiday-list.component.html',
  styleUrls: ['./holiday-list.component.less']
})
export class HolidayListComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public holidayService: HolidaysService) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.initTableData(this.holidayService);
    this.persistentFilters = {
        searchFields: "description"
    }
  }
  
  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  
  edit(id: number) {
    if(!this.authService.hasPermission('configuration:holidays:update')){
      return;
    }
      this.router.navigateByUrl('/configurations/holiday/update/' + id);
  }

  delete(id: number) {
    if(!this.authService.hasPermission('configuration:holidays:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este feriado?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.holidayService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Feriado eliminado com sucesso'
              );
              this.initTableData(this.holidayService);
            });
          }
        });
    }
    
    validTranslateName(id_trans: number, translation: any[]) {
      return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
    }
}
