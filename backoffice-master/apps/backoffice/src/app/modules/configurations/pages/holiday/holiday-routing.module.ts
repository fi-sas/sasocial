import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolidayFormComponent } from './holiday-form/holiday-form.component';
import { HolidayListComponent } from './holiday-list/holiday-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: HolidayListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:holidays:read'},

  },
 {
    path: 'create',
    component: HolidayFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:holidays:create'},
  },
  {
    path: 'update/:id',
    component: HolidayFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:holidays:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HolidayRoutingModule { }
