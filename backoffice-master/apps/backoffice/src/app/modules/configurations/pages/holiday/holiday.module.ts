import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { HolidayRoutingModule } from './holiday-routing.module';
import { HolidayListComponent } from './holiday-list/holiday-list.component';
import { HolidayFormComponent } from './holiday-form/holiday-form.component';


@NgModule({
  declarations: [
  HolidayListComponent,
  HolidayFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HolidayRoutingModule
  ],

})
export class HolidayConfModule { }
