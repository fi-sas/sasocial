import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormLanguageComponent } from './language-form.component';


describe('FormLanguageComponent', () => {
  let component: FormLanguageComponent;
  let fixture: ComponentFixture<FormLanguageComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormLanguageComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
