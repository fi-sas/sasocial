import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { LanguageModel } from "../../../models/language.model";
import { LanguagesService } from "../../../services/languages.service";

@Component({
    selector: 'fi-sas-form-language',
    templateUrl: './language-form.component.html',
    styleUrls: ['./language-form.component.less']
})
export class FormLanguageComponent implements OnInit {

    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        acronym: new FormControl(null, [Validators.required, trimValidation]),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute, private languageService: LanguagesService, 
        private uiService: UiService, private router: Router) {
        
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getLanguageId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getLanguageId(id) {
        let language: LanguageModel = new LanguageModel();
        this.languageService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            language = results.data[0];
            this.formCreate.patchValue({
              ...language
            });
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
            this.isLoading = true;
            if (this.isEdit) {
                this.languageService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Língua alterada com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }else {
                this.languageService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Língua criada com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/language/list');
    }

}