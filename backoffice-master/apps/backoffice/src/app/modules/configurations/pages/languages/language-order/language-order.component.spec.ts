import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderLanguageComponent } from './language-order.component';


describe('OrderLanguageComponent', () => {
  let component: OrderLanguageComponent;
  let fixture: ComponentFixture<OrderLanguageComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [OrderLanguageComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
