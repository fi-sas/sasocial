import { Component, OnInit } from "@angular/core";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { finalize, first } from "rxjs/operators";
import { LanguageModel } from "../../../models/language.model";
import { LanguagesService } from "../../../services/languages.service";

@Component({
    selector: 'fi-sas-order-language',
    templateUrl: './language-order.component.html',
    styleUrls: ['./language-order.component.less']
})
export class OrderLanguageComponent implements OnInit {
    languages: LanguageModel[] = [];
    isLoading = false;
    constructor(private languagesService: LanguagesService, private uiService: UiService) { }

    ngOnInit() {
        this.getLanguages();
    }

    getLanguages() {
        this.languagesService
            .list(1, -1, "order", "ascend")
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.languages = results.data;
            });
    }

    goUp(i: number) {
        if (i != 0) {
            const aux = this.languages[i];
            this.languages[i] = this.languages[i - 1];
            this.languages[i - 1] = aux;
        }
    }
    goDown(i: number) {
        const aux = this.languages[i];
        this.languages[i] = this.languages[i + 1];
        this.languages[i + 1] = aux;
    }

    submit() {
        this.isLoading = true;
        for (let index = 0; index < this.languages.length; index++) {
          const lang = this.languages[index];
          lang.order = index + 1;
        }
    
        this.languagesService.reorder(this.languages).pipe(first(),finalize(()=>this.isLoading = false)).subscribe(
          data => {
            this.uiService.showMessage(
              MessageType.success,
              'Ordem alterada com sucesso'
            );
          }
        );
    
      }
}