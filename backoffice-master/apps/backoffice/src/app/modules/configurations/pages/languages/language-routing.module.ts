import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormLanguageComponent } from './language-form/language-form.component';
import { OrderLanguageComponent } from './language-order/language-order.component';
import { ListLanguageComponent } from './languages-list/languages-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListLanguageComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:languages:read'},

  },
  /*{
    path: 'create',
    component: FormLanguageComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:languages:create'},
  },
  {
    path: 'update/:id',
    component: FormLanguageComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:languages:update'},
  },*/
  {
    path: 'order',
    component: OrderLanguageComponent,
    data: { breadcrumb: 'Ordenar', title: 'Ordenar', scope: 'configuration:languages:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguageRoutingModule { }
