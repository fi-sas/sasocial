import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { LanguageRoutingModule } from './language-routing.module';
import { OrderLanguageComponent } from './language-order/language-order.component';
import { FormLanguageComponent } from './language-form/language-form.component';
import { ListLanguageComponent } from './languages-list/languages-list.component';

@NgModule({
  declarations: [
    OrderLanguageComponent,
    FormLanguageComponent,
    ListLanguageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LanguageRoutingModule
  ],

})
export class LanguageConfModule { }
