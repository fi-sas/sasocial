import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { LanguagesService } from "../../../services/languages.service";

@Component({
    selector: 'fi-sas-list-language',
    templateUrl: './languages-list.component.html',
    styleUrls: ['./languages-list.component.less']
})
export class ListLanguageComponent extends TableHelper implements OnInit {
    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public languageService: LanguagesService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.languageService);
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
        if(!this.authService.hasPermission('configuration:languages:update')){
            return;
          }
        this.router.navigateByUrl('/configurations/language/update/' + id);
    }
}