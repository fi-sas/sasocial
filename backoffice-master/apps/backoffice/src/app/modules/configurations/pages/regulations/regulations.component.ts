import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NzModalService, UploadXHRArgs } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ServiceModel } from '../../models/service.model';
import { TermsAndPolicyModel } from '../../models/terms-and-policy.model';
import { ServicesService } from '../../services/services.service';
import { TermsService } from '../../services/terms.service';
import { PolicyService } from '../../services/policy.service';
import { InformationService } from '../../services/information.service';

@Component({
  selector: 'fi-sas-conf-regulations',
  templateUrl: './regulations.component.html',
  styleUrls: ['./regulations.component.less']
})
export class ListRegulationComponent implements OnInit {
  isVisibleEdit = false;
  isVisibleCreate = false;
  policiesFlg: boolean;
  infoFlg: boolean;
  termsFlg: boolean;
  isConfirmLoading = false;
  modalType: string;
  selectedService: ServiceModel;
  confForm = this.newForm();
  listOfData = [{}, {}, {}, {}];
  servicesList: ServiceModel[];
  eng = true;

  terms: TermsAndPolicyModel;
  policy: TermsAndPolicyModel;
  information: TermsAndPolicyModel;
  terms_eng: TermsAndPolicyModel;
  policy_eng: TermsAndPolicyModel;
  information_eng: TermsAndPolicyModel;
  files = [];
  modalText: string;
  id: number;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['insertImage','insertVideo']
    ]
  };
  get f() { return this.confForm.controls; }
  constructor(
    private servicesService: ServicesService,
    private fb: FormBuilder,
    private termsService: TermsService,
    private policyService: PolicyService,
    private informationService: InformationService,
    private modal: NzModalService,
    private authService: AuthService,
    private uiService: UiService,
    private filesService: FilesService
  ) {
    this.getServices();
  }

  ngOnInit() {
  }

  newForm() {
    return this.fb.group({
      // 1 step
      text: new FormControl('', [Validators.required])
    });
  }

  submitForm() { }


  showModal(language: string, whichModal: string, type: string): void {
    this.eng = false;
    if (language === 'eng') {
      this.eng = true;
    }
    if (whichModal === "edit") {
      this.isVisibleEdit = true;
    } else {
      this.isVisibleCreate = true;
    }
    this.modalType = type
    this.files = [];
    switch (type) {
      case 'Termos':
        if (!this.eng) {

          this.id = this.terms.id ? this.terms.id : null;
          this.modalText = this.terms.text ? this.terms.text : '';
          this.files = this.getFiles(this.terms.files);
        } else if (this.eng) {
          this.id = this.terms_eng.id ? this.terms_eng.id : null;
          this.modalText = this.terms_eng.text ? this.terms_eng.text : '';
          this.files = this.getFiles(this.terms_eng.files);
        }
        break;
      case 'Politicas de Privacidade':
        if (!this.eng) {
          this.id = this.policy.id ? this.policy.id : null;
          this.modalText = this.policy.text ? this.policy.text : '';
          this.files = this.getFiles(this.policy.files);
        } else if (this.eng) {
          this.id = this.policy_eng.id ? this.policy_eng.id : null;
          this.modalText = this.policy_eng.text ? this.policy_eng.text : '';
          this.files = this.getFiles(this.policy_eng.files);
        }
        break;
      case 'Informações':
        if (!this.eng) {
          this.id = this.information.id ? this.information.id : null;
          this.modalText = this.information.text ? this.information.text : '';
          this.files = this.getFiles(this.information.files);
        } else if (this.eng) {
          this.id = this.information_eng.id ? this.information_eng.id : null;
          this.modalText = this.information_eng.text ? this.information_eng.text : '';
          this.files = this.getFiles(this.information_eng.files);
        }
        break;

      default:
        break;
    }
  }

  submit() {
    switch (this.modalType) {
      case 'Termos':
        if (this.isVisibleEdit && this.authService.hasPermission('configuration:terms:update')) {
          this.updateTerms();
        } else if(this.authService.hasPermission('configuration:terms:create')){
          this.createTerms();
        }
        break;
      case 'Politicas de Privacidade':
        if (this.isVisibleEdit && this.authService.hasPermission('configuration:privacy_policies:update')) {
          this.updatePolicy();
        } else if(this.authService.hasPermission('configuration:privacy_policies:create')){
          this.createPolicy();
        }
        break;
      case 'Informações':
        if (this.isVisibleEdit && this.authService.hasPermission('configuration:regulations:update')) {
          this.updateInfo();
        } else if(this.authService.hasPermission('configuration:regulations:create')){
          this.createInfo();
        }
        break;

      default:
        break;
    }


    this.isConfirmLoading = true;
    this.isVisibleEdit = false;
    this.isVisibleCreate = false;
    this.isConfirmLoading = false;
  }

  handleCancel(): void {
    this.eng = false;
    this.isVisibleEdit = false;
    this.isVisibleCreate = false;
  }

  showDeleteConfirm(language: string, type): void {
    if (!this.authService.hasPermission('configuration:regulations:delete')) {
      return;
    }

    if (type === 'Termos' && !this.authService.hasPermission('configuration:terms:delete')) {
      return;
    }

    if (type === 'Politicas de Privacidade' && !this.authService.hasPermission('configuration:privacy_policies:delete')) {
      return;
    }

    if (type === 'Informações' && !this.authService.hasPermission('configuration:informations:delete')) {
      return;
    }

    this.eng = language === 'eng';

    this.modal.confirm({
      nzTitle: 'Eliminar regulamento?',
      nzContent: '<b style="color: red;">Tem a certeza que pretende eliminar definitivamente este regulamento?</b>',
      nzOkText: 'Sim',
      nzOkType: 'primary',
      nzOnOk: () => {
        switch (type) {
          case 'Termos':
            this.deleteTerms();
            break;
          case 'Politicas de Privacidade':
            this.deletePolicy();
            break;
          case 'Informações':
            this.deleteInfo();
            break;

          default:
            break;
        }
      },
      nzCancelText: 'Não',
      nzOnCancel: () => { }
    });
  }

  getServices() {
    this.servicesService.list(1, -1).pipe(first()).subscribe(
      result => {
        this.servicesList = result.data;
        this.selectedService = result.data[0];
        this.selectServices()
      }
    )
  }

  updateServices() {
    this.servicesService.list(1, -1).pipe(first()).subscribe(
      result => {
        this.servicesList = result.data;
      }
    )
  }

  selectServices() {
    this.getTerms();
    this.getInfo();
    this.getPolicy();
  }
  getFiles(files) {
    const arrayToReturn = [];
    if (files) {
      for (const file of files) {
        arrayToReturn.push(file.file);
      }
    }
    return arrayToReturn;

  }
  getFileIds() {
    const file_ids = [];
    for (const file of this.files) {
      file_ids.push(file.id);
    }
    return file_ids;
  }
  //TERMS
  getTerms() {
    this.termsService.getTerms(this.selectedService.id).subscribe(
      result => {
        if (result.data.length === 2) {
          if (result.data[0].language_id === 3) {
            this.terms = result.data[0];
            this.terms_eng = result.data[1];
          } else {
            this.terms = result.data[1];
            this.terms_eng = result.data[0];
          }
        } else if (result.data.length === 1) {
          if (result.data[0].language_id === 3) {
            this.terms = result.data[0];
            this.terms_eng = new TermsAndPolicyModel;
            this.terms_eng.language_id = 4;
          } else {
            this.terms = new TermsAndPolicyModel;
            this.terms.language_id = 3;
            this.terms_eng = result.data[0];
          }
        } else {
          this.terms = new TermsAndPolicyModel;
          this.terms_eng = new TermsAndPolicyModel;
          this.terms.language_id = 3;
          this.terms_eng.language_id = 4;
        }
      }
    );
  }

  createTerms() {
    let aux: TermsAndPolicyModel;

    if (this.eng) {
      this.terms_eng.text = this.modalText;
      this.terms_eng.service_id = this.selectedService.id;
      this.terms_eng.file_ids = this.getFileIds();
      aux = this.terms_eng;
    } else {
      this.terms.service_id = this.selectedService.id;
      this.terms.text = this.modalText;
      this.terms.file_ids = this.getFileIds();
      aux = this.terms;
    }
    this.termsService.create(aux).subscribe(
      result => {
        this.terms = result.data[0];
        this.files = [];
        this.updateServices();
        this.selectServices();
      }
    );
    this.eng = false;
  }


  updateTerms() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      this.terms_eng.text = this.modalText;
      aux = this.terms_eng;
    } else {
      this.terms.text = this.modalText;
      aux = this.terms;
    }
    aux.file_ids = this.getFileIds();
    aux.files = [];
    aux.created_at = undefined;
    this.termsService.update(this.id, aux).subscribe(
      result => {
        this.terms = result.data[0];

        this.updateServices();
        this.selectServices();
      }
    );
    this.files = [];
    this.eng = false;
  }

  deleteTerms() {
    let aux: TermsAndPolicyModel;

    if (this.eng) {
      aux = this.terms_eng;
    } else {
      aux = this.terms;
    }

    this.termsService.delete(aux.id).subscribe(
      result => {
        this.terms = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );

    this.eng = false;
  }

  //POLICY
  getPolicy() {
    this.policyService.getPolicy(this.selectedService.id).subscribe(
      result => {
        if (result.data.length === 2) {
          if (result.data[0].language_id === 3) {
            this.policy = result.data[0];
            this.policy_eng = result.data[1];
          } else {
            this.policy = result.data[1];
            this.policy_eng = result.data[0];
          }
        } else if (result.data.length === 1) {
          if (result.data[0].language_id === 3) {
            this.policy = result.data[0];
            this.policy_eng = new TermsAndPolicyModel;
            this.policy_eng.language_id = 4;
          } else {
            this.policy = new TermsAndPolicyModel;
            this.policy.language_id = 3;
            this.policy_eng = result.data[0];
          }
        } else {
          this.policy = new TermsAndPolicyModel;
          this.policy_eng = new TermsAndPolicyModel;
          this.policy.language_id = 3;
          this.policy_eng.language_id = 4;
        }

      }
    );
    this.eng = false;
  }

  createPolicy() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      this.policy_eng.text = this.modalText;
      this.policy_eng.service_id = this.selectedService.id;
      this.policy_eng.file_ids = this.getFileIds();
      aux = this.policy_eng;
    } else {
      this.policy.service_id = this.selectedService.id;
      this.policy.text = this.modalText;
      this.policy.file_ids = this.getFileIds();
      aux = this.policy;
    }
    this.policyService.create(aux).subscribe(
      result => {
        this.policy = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );
    this.eng = false;
  }

  updatePolicy() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      this.policy_eng.text = this.modalText;
      aux = this.policy_eng;
    } else {
      this.policy.text = this.modalText;
      aux = this.policy;
    }
    aux.file_ids = this.getFileIds();
    aux.files = [];
    aux.created_at = undefined;
    this.policyService.update(this.id, aux).subscribe(
      result => {
        this.policy = result.data[0];
        this.updateServices();
        this.selectServices();

      }
    );
    this.eng = false;
  }

  deletePolicy() {
    let aux: TermsAndPolicyModel;

    if (this.eng) {
      aux = this.policy_eng;
    } else {
      aux = this.policy;
    }

    this.policyService.delete(aux.id).subscribe(
      result => {
        this.policy = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );

    this.eng = false;
  }

  //INFO
  getInfo() {
    this.informationService.getImformation(this.selectedService.id).subscribe(
      result => {
        if (result.data.length === 2) {
          if (result.data[0].language_id === 3) {
            this.information = result.data[0];
            this.information_eng = result.data[1];
          } else {
            this.information = result.data[1];
            this.information_eng = result.data[0];
          }
        } else if (result.data.length === 1) {
          if (result.data[0].language_id === 3) {
            this.information = result.data[0];
            this.information_eng = new TermsAndPolicyModel;
            this.information_eng.language_id = 4;
          } else {
            this.information = new TermsAndPolicyModel;
            this.information.language_id = 3;
            this.information_eng = result.data[0];
          }
        } else {
          this.information = new TermsAndPolicyModel;
          this.information_eng = new TermsAndPolicyModel;
          this.information.language_id = 3;
          this.information_eng.language_id = 4;
        }
      }
    );
  }

  createInfo() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      this.information_eng.text = this.modalText;
      this.information_eng.service_id = this.selectedService.id;
      aux = this.information_eng;
    } else {
      this.information.service_id = this.selectedService.id;
      this.information.text = this.modalText;
      aux = this.information;
    }
    aux.file_ids = this.getFileIds();

    this.informationService.create(aux).subscribe(
      result => {
        this.information = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );
    this.eng = false;
  }

  updateInfo() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      this.information_eng.text = this.modalText;
      aux = this.information_eng;
    } else {
      this.information.text = this.modalText;
      aux = this.information;
    }
    aux.file_ids = this.getFileIds();
    aux.files = [];
    aux.created_at = undefined;
    this.informationService.update(this.id, aux).subscribe(
      result => {
        this.information = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );
    this.eng = false;
  }

  deleteInfo() {
    let aux: TermsAndPolicyModel;
    if (this.eng) {
      aux = this.information_eng;
    } else {
      aux = this.information;
    }
    this.informationService.delete(aux.id).subscribe(
      result => {
        this.information = result.data[0];
        this.updateServices();
        this.selectServices();
      }
    );
    this.eng = false;
  }



  uploadFile = (item: UploadXHRArgs) => {
    const formData = new FormData();
    /*   formData.append('name', item.file.name);
      formData.append('weight', this.weight.toString());
      formData.append('public', this.isPublic);
      formData.append('file_category_id', this.categoryId.toString());
      formData.append('language_id', this.languageId.toString());
      formData.append('file', item.file as any, item.file.name); */

    formData.append('file', item.file as any, item.file.name);
    formData.append('name', item.file.name);
    formData.append('weight', '1');
    formData.append('public', 'true');
    formData.append('file_category_id', '1');
    formData.append('language_id', '3');

    const req = this.filesService.createFile2(formData);

    return req.subscribe(
      (file) => {
        this.files.push(file.data[0]);
        this.uiService.showMessage(
          MessageType.success,
          "Ficheiro adicionado com sucesso"
        );
      },
      (err) => {
        item.onError!(err, item.file!);
        this.uiService.showMessage(
          MessageType.error,
          "Erro a adicionar ficheiro"
        );
      }
    );
  };
  deleteFile(id, file) {
   this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este ficheiro ?', 'Eliminar')
      .pipe(first())
      .subscribe(result => {
        if (result) {
          this.filesService.delete(id).subscribe(
            (file) => {
              this.files.splice(this.files.indexOf(x => x.id === id), 1);
              this.uiService.showMessage(
                MessageType.success,
                "Ficheiro removido com sucesso"
              );
            },
            (err) => {
              this.uiService.showMessage(
                MessageType.error,
                "Erro a remover ficheiro"
              );
            }
          )

        }
      });
  }

}

