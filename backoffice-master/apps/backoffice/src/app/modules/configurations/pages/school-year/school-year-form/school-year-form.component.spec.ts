import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormSchoolYearComponent } from './school-year-form.component';


describe('FormSchoolYearComponent', () => {
  let component: FormSchoolYearComponent;
  let fixture: ComponentFixture<FormSchoolYearComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormSchoolYearComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSchoolYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
