import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { AcademicYearModel } from "../../../models/academic-year.model";
import { AcademicYearsService } from "../../../services/academic-years.service";

@Component({
    selector: 'fi-sas-form-school-year',
    templateUrl: './school-year-form.component.html',
    styleUrls: ['./school-year-form.component.less']
})
export class FormSchoolYearComponent implements OnInit {
    formCreate = new FormGroup({
        academic_year: new FormControl('', [Validators.required, trimValidation]),
        dateRange: new FormControl(null, [Validators.required]),
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute, private academicYearsService: AcademicYearsService,
        private uiService: UiService, private router: Router) {

    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getSchoolYearId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getSchoolYearId(id) {
        let year: AcademicYearModel = new AcademicYearModel();
        this.academicYearsService
            .read(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                year = results.data[0];
                this.formCreate.patchValue({
                    ...year
                });
                this.formCreate.get('dateRange').setValue(([results.data[0].start_date, results.data[0].end_date]));
            });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
            this.isLoading = true;
            const body: any = {
                academic_year: this.formCreate.get('academic_year').value,
                start_date: this.formCreate.get('dateRange').value[0],
                end_date: this.formCreate.get('dateRange').value[1]
            }
            if (this.isEdit) {

                this.academicYearsService.update(this.id, body).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Ano letivo alterado com sucesso'
                        );
                        this.returnButton();
                    },
                )
            } else {
                this.academicYearsService.create(body).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Ano letivo criado com sucesso'
                        );
                        this.returnButton();
                    },
                )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/school-year/list');
    }
}