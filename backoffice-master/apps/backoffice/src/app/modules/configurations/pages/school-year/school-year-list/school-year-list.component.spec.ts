import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListSchoolYearComponent } from './school-year-list.component';


describe('ListSchoolYearComponent', () => {
  let component: ListSchoolYearComponent;
  let fixture: ComponentFixture<ListSchoolYearComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListSchoolYearComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSchoolYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
