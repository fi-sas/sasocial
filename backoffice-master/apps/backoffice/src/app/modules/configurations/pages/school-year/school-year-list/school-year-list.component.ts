import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { AcademicYearsService } from "../../../services/academic-years.service";

@Component({
    selector: 'fi-sas-list-school-year',
    templateUrl: './school-year-list.component.html',
    styleUrls: ['./school-year-list.component.less']
})
export class ListSchoolYearComponent extends TableHelper implements OnInit {

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public academicYearsService: AcademicYearsService) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit(){
        this.initTableData(this.academicYearsService);
        this.persistentFilters = {
            searchFields: "academic_year"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:academic_years:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/school-year/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:academic_years:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar este ano letivo?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.academicYearsService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Ano letivo eliminado com sucesso'
                );
                this.initTableData(this.academicYearsService);
              });
            }
          });
      }
}