import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSchoolYearComponent } from './school-year-form/school-year-form.component';
import { ListSchoolYearComponent } from './school-year-list/school-year-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSchoolYearComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:academic_years:read'},

  },
  {
    path: 'create',
    component: FormSchoolYearComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:academic_years:create'},
  },
  {
    path: 'update/:id',
    component: FormSchoolYearComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:academic_years:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolYearRoutingModule { }
