import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SchoolYearRoutingModule } from './school-year-routing.module';
import { ListSchoolYearComponent } from './school-year-list/school-year-list.component';
import { FormSchoolYearComponent } from './school-year-form/school-year-form.component';

@NgModule({
  declarations: [
    FormSchoolYearComponent,
    ListSchoolYearComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SchoolYearRoutingModule
  ],

})
export class SchoolYearConfModule { }
