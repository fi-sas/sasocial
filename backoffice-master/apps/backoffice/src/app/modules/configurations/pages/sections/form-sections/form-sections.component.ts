import { Component, OnInit } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";

import * as _ from 'lodash';
import { finalize, first } from "rxjs/operators";
import { SectionModel } from "../../../models/section.model";
import { SectionService } from "../../../services/section.service";

@Component({
    selector: 'fi-sas-form-sections',
    templateUrl: './form-sections.component.html',
    styleUrls: ['./form-sections.component.less']
})

export class FormSectionComponent implements OnInit {
   
    id;
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required,trimValidation]),
        priority: new FormControl(null, [Validators.required])
    });

    submitted = false;
    get f() { return this.formCreate.controls; }
    isUpdate = false;
    isLoading = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router, private sectionServices: SectionService, private uiService: UiService) { }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getSectionId(id);
            this.id = id;
            this.isUpdate = true;

        }
    }

    getSectionId(id) {
        let section: SectionModel = new SectionModel();
        this.sectionServices
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            section = results.data[0];
            this.formCreate.patchValue({
              ...section
            });
          });
    }


    returnButton() {
        this.router.navigateByUrl('/configurations/section/list');
    }

    submit() {
        this.submitted = true;

        if (this.formCreate.valid) {
            this.isLoading = true;
            let sendValues = this.formCreate.value;
            if (this.isUpdate) {
                this.sectionServices.update(this.id, sendValues).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                  result => {
                    this.uiService.showMessage(
                      MessageType.success,
                      'Secção alterada com sucesso'
                    );
                    this.isLoading = false;
                    this.returnButton();
                  },
                )
            }else {
                this.sectionServices.create(sendValues).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Secção criada com sucesso'
                      );
                      this.isLoading = false;
                      this.returnButton();
                    },
                  )
            }
               
        }
    }
}