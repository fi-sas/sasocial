import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListSectionComponent } from './list-sections.component';


describe('ListSectionComponent', () => {
  let component: ListSectionComponent;
  let fixture: ComponentFixture<ListSectionComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListSectionComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
