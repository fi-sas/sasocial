import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { SectionService } from "../../../services/section.service";

@Component({
  selector: 'fi-sas-form-sections',
  templateUrl: './list-sections.component.html',
  styleUrls: ['./list-sections.component.less']
})
export class ListSectionComponent extends TableHelper implements OnInit {
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public sectionService: SectionService) {
    super(uiService, router, activatedRoute);
  }


  ngOnInit() {
    this.initTableData(this.sectionService);
  }


  edit(id) {
    if(!this.authService.hasPermission('configuration:sections_bo:update')){
      return;
    }
    this.router.navigateByUrl('/configurations/section/update/'+id);
  }

  deleteSection(id) {
    if(!this.authService.hasPermission('configuration:sections_bo:delete')){
      return;
    }
    this.sectionService.delete(id).pipe(first()).subscribe((data)=>{
      this.uiService.showMessage(
        MessageType.success,
        'Secção eliminada com sucesso'
      );
      this.initTableData(this.sectionService);
    })
  }

}