import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSectionComponent } from './form-sections/form-sections.component';
import { ListSectionComponent } from './list-sections/list-sections.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSectionComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:sections_bo:read'},

  },
  {
    path: 'update/:id',
    component: FormSectionComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:sections_bo:update'},

  },
  {
    path: 'create',
    component: FormSectionComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:sections_bo:update'},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SectionRoutingModule { }
