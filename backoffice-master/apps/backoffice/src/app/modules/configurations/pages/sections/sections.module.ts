import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SectionRoutingModule } from './sections-routing.module';
import { FormSectionComponent } from './form-sections/form-sections.component';
import { ListSectionComponent } from './list-sections/list-sections.component';

@NgModule({
  declarations: [
    ListSectionComponent,
    FormSectionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SectionRoutingModule
  ],

})
export class SectionsConfModule { }
