import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListServicesComponent } from './services-list.component';


describe('ListServicesComponent', () => {
  let component: ListServicesComponent;
  let fixture: ComponentFixture<ListServicesComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListServicesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
