import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { ServicesService } from "../../../services/services.service";

@Component({
  selector: 'fi-sas-form-services',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.less']
})
export class ListServicesComponent extends TableHelper implements OnInit {
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public serviceService: ServicesService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name,description",
      is_external_service: 'false'
    }
  }


  ngOnInit() {
    this.initTableData(this.serviceService);
   
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  validTranslateDescription(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
  }

  edit(id) {
    if(!this.authService.hasPermission('configuration:services:update')){
      return;
    }
    this.router.navigateByUrl('/configurations/service/update/'+id);
  }

}