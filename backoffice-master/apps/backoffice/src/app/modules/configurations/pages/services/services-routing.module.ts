import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormServicesComponent } from './form-services/form-services.component';
import { ListServicesComponent } from './list-services/services-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListServicesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:services:read'},

  },
  {
    path: 'update/:id',
    component: FormServicesComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:services:update'},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
