import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ServiceRoutingModule } from './services-routing.module';
import { ListServicesComponent } from './list-services/services-list.component';
import { FormServicesComponent } from './form-services/form-services.component';

@NgModule({
  declarations: [
    ListServicesComponent,
    FormServicesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ServiceRoutingModule
  ],

})
export class ServiceConfModule { }
