import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormTaxComponent } from './tax-form.component';


describe('FormTaxComponent', () => {
  let component: FormTaxComponent;
  let fixture: ComponentFixture<FormTaxComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FormTaxComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
