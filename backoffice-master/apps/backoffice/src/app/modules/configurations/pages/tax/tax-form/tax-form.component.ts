import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { TaxModel } from "../../../models/tax.model";
import { TaxesService } from "../../../services/taxes.service";

@Component({
    selector: 'fi-sas-form-tax',
    templateUrl: './tax-form.component.html',
    styleUrls: ['./tax-form.component.less']
})
export class FormTaxComponent implements OnInit {
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        description: new FormControl('', [Validators.required, trimValidation]),
        tax_value: new FormControl(null, [Validators.required, Validators.min(0), Validators.max(1)]),
        active: new FormControl(false)
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;


    constructor(private route: ActivatedRoute, private taxService: TaxesService, 
        private uiService: UiService, private router: Router) {
        
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getTaxId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getTaxId(id) {
        let tax: TaxModel = new TaxModel();
        this.taxService
          .read(id)
          .pipe(
            first()
          )
          .subscribe((results) => {
            tax = results.data[0];
            this.formCreate.patchValue({
              ...tax
            });
          });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
          this.isLoading = true;
            if (this.isEdit) {
             
                this.taxService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Taxa alterada com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }else {
                this.taxService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                    result => {
                      this.uiService.showMessage(
                        MessageType.success,
                        'Taxa criada com sucesso'
                      );
                      this.returnButton();
                    },
                  )
            }


        }
    }

    returnButton() {
        this.router.navigateByUrl('configurations/tax/list');
    }
}