import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { first } from "rxjs/operators";
import { TaxesService } from "../../../services/taxes.service";

@Component({
    selector: 'fi-sas-list-tax',
    templateUrl: './tax-list.component.html',
    styleUrls: ['./tax-list.component.less']
})
export class ListTaxComponent extends TableHelper implements OnInit {
    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private authService: AuthService,
        public taxService: TaxesService) {
        super(uiService, router, activatedRoute);
        
    }


    ngOnInit() {
        this.initTableData(this.taxService);
        this.persistentFilters = {
            searchFields: "name"
        }
    }

    listComplete() {
        this.filters.search = null;
        this.filters.active = null;
        this.searchData(true)
    }


    edit(id: number) {
      if(!this.authService.hasPermission('configuration:taxes:update')){
        return;
      }
        this.router.navigateByUrl('/configurations/tax/update/' + id);
    }

    delete(id: number) {
      if(!this.authService.hasPermission('configuration:taxes:delete')){
        return;
      }
        this.uiService
          .showConfirm('Eliminar', 'Pretende eliminar esta taxa?', 'Eliminar')
          .subscribe(result => {
            if (result) {
              this.taxService.delete(id).pipe(first()).subscribe(r => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Taxa eliminada com sucesso'
                );
                this.initTableData(this.taxService);
              });
            }
          });
      }
}