import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTaxComponent } from './tax-form/tax-form.component';
import { ListTaxComponent } from './tax-list/tax-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTaxComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'configuration:taxes:read'},

  },
  {
    path: 'create',
    component: FormTaxComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'configuration:taxes:create'},
  },
  {
    path: 'update/:id',
    component: FormTaxComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'configuration:taxes:update'},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxRoutingModule { }
