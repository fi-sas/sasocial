import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormTaxComponent } from './tax-form/tax-form.component';
import { ListTaxComponent } from './tax-list/tax-list.component';
import { TaxRoutingModule } from './tax-routing.module';


@NgModule({
  declarations: [
    FormTaxComponent,
    ListTaxComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TaxRoutingModule
  ],

})
export class TaxConfModule { }
