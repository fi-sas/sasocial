import { Component, OnInit } from '@angular/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { UploadXHRArgs } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { LanguageModel } from '../../models/language.model';
import { ServiceModel } from '../../models/service.model';
import { TextApresentationsModel } from '../../models/text-apresentation.model';
import { LanguagesService } from '../../services/languages.service';
import { ServicesService } from '../../services/services.service';
import { TextApresentationService } from '../../services/text-apresentation.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'fi-sas-text-apresententation',
  templateUrl: './text-apresentation.component.html',
  styleUrls: ['./text-apresentation.component.less'],
})
export class TextApresentationComponent implements OnInit {
  servicesList: ServiceModel[];
  selectedService: ServiceModel;
  textDataTotal: TextApresentationsModel[] = [];
  textDataId: number;
  selectedLanguage: number;
  modalText: string;
  textInitWithText: boolean = false;
  languages_loading = false;
  languages: LanguageModel[] = [];
  submitted: boolean;
  file = [];
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['insertImage', 'insertVideo']],
  };

  constructor(
    private servicesService: ServicesService,
    private authService: AuthService,
    private textApresentationService: TextApresentationService,
    private languagesService: LanguagesService,
    private uiService: UiService,
    private filesService: FilesService
  ) {}

  ngOnInit() {
    this.getServices();
    this.getLanguages();
  }

  getLanguages() {
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.languages = this.languages.sort((a, b) => {
          if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return -1;
          }
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return 1;
          }
          return 0;
        });
      });
  }

  getServices() {
    this.servicesService
      .list(1, -1)
      .pipe(first())
      .subscribe((result) => {
        this.servicesList = result.data;
      });
  }

  selectServices() {
    this.modalText = '';
    this.textInitWithText = false;
    this.selectedLanguage = null;
    this.textDataTotal = [];
    this.textApresentationService.getTextByService(this.selectedService.id).subscribe((data) => {
      if (data.data.length != 0) {
        this.textDataTotal = data.data;
      }
    });
  }

  selectLanguages() {
    let aux: TextApresentationsModel[] = [];
    this.textDataId = null;
    this.modalText = '';
    this.textInitWithText = false;
    this.files = [];

    if (this.textDataTotal.length != 0) {
      aux = this.textDataTotal.filter((fil) => {
        return fil.language_id == this.selectedLanguage;
      });
      if (aux.length != 0) {
        this.modalText = aux[0].text;
        this.textDataId = aux[0].id;
        this.textInitWithText = true;
        this.files = this.getFiles(aux[0].files);
      }
    }
  }

  getFiles(files): FileModel[] {
    const arrayToReturn = [];
    for (const file of files) {
      arrayToReturn.push(file.file);
    }
    return arrayToReturn;
  }

  files = [];
  uploadFile = (item: UploadXHRArgs) => {
    const formData = new FormData();
    /*   formData.append('name', item.file.name);
      formData.append('weight', this.weight.toString());
      formData.append('public', this.isPublic);
      formData.append('file_category_id', this.categoryId.toString());
      formData.append('language_id', this.languageId.toString());
      formData.append('file', item.file as any, item.file.name); */

    formData.append('file', item.file as any, item.file.name);
    formData.append('name', item.file.name);
    formData.append('weight', '1');
    formData.append('public', 'true');
    formData.append('file_category_id', '1');
    formData.append('language_id', '3');

    const req = this.filesService.createFile2(formData);

    return req.subscribe(
      (file) => {
        this.files.push(file.data[0]);
        this.uiService.showMessage(MessageType.success, 'Ficheiro adicionado com sucesso');
      },
      (err) => {
        item.onError!(err, item.file!);
        this.uiService.showMessage(MessageType.error, 'Erro a adicionar ficheiro');
      }
    );
  };

  deleteFile(id) {
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este ficheiro ?', 'Eliminar')
      .pipe(first())
      .subscribe((result) => {
        if (result) {
          this.filesService.delete(id).subscribe(
            (file) => {
              this.files.splice(
                this.files.indexOf((x) => x.id === id),
                1
              );
              this.uiService.showMessage(MessageType.success, 'Ficheiro removido com sucesso');
            },
            (err) => {
              this.uiService.showMessage(MessageType.error, 'Erro a remover ficheiro');
            }
          );
        }
      });
  }

  save() {
    if (this.authService.hasPermission('configuration:apresentation_text_files:create')) {
      this.submitted = true;
      if (this.selectedLanguage != undefined && this.selectedService != undefined) {
        let sendValue: TextApresentationsModel = new TextApresentationsModel();
        sendValue.language_id = this.selectedLanguage;
        sendValue.service_id = this.selectedService.id;
        sendValue.text = this.modalText;
        const file_ids = [];
        for (const file of this.files) {
          file_ids.push(file.id);
        }
        sendValue.file_ids = file_ids;
        this.textApresentationService.createText(sendValue).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Texto de apresentação gravado com sucesso!');
          this.modalText = '';
          this.selectedLanguage = null;
          this.selectedService = null;
          this.textDataId = null;
          this.textDataTotal = [];
          this.files = [];
          this.submitted = false;
          this.textInitWithText = false;
        });
      }
    }
  }

  edit() {
    if (this.authService.hasPermission('configuration:apresentation_text_files:update')) {
      this.submitted = true;
      if (this.selectedLanguage !== undefined && this.selectedService !== undefined) {
        let sendValue: TextApresentationsModel = new TextApresentationsModel();
        sendValue.language_id = this.selectedLanguage;
        sendValue.service_id = this.selectedService.id;
        sendValue.text = this.modalText;
        sendValue.id = this.textDataId;
        const file_ids = [];
        for (const file of this.files) {
          file_ids.push(file.id);
        }
        sendValue.file_ids = file_ids;
        this.textApresentationService.updateText(this.textDataId, sendValue).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Texto de apresentação alterado com sucesso!');
          this.modalText = '';
          this.selectedLanguage = null;
          this.selectedService = null;
          this.textDataTotal = [];
          this.files = [];
          this.textDataId = null;
          this.submitted = false;
          this.textInitWithText = false;
        });
      }
    }
  }
}
