import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AcademicYearsService extends Repository<AcademicYearModel>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.ACADEMIC_YEARS';
    this.entity_url = 'CONFIGURATIONS.ACADEMIC_YEARS_ID';
  }

  getCurrentAcademicYear(): Observable<Resource<AcademicYearModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '1');
    return this.resourceService.list<AcademicYearModel>(this.urlService.get('CONFIGURATIONS.CURRENT_ACADEMIC_YEAR'), { params })
  }

}
