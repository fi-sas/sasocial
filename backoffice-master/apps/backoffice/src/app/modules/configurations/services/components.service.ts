import { Repository } from './../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { ComponentModel } from '../models/component.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService extends Repository<ComponentModel> {

  constructor(resourcerService: FiResourceService, urlService: FiUrlService) {
    super(resourcerService, urlService);
    this.entities_url = 'CONFIGURATIONS.COMPONENTS';
    this.entity_url = 'CONFIGURATIONS.COMPONENTS_ID';
   }
}
