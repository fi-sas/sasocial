
import { FiResourceService } from './../../../../../../../libs/core/src/lib/resource.service';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiUrlService } from '@fi-sas/core';
import { ContactModel } from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactsService extends Repository<ContactModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);

    this.entities_url = 'CONFIGURATIONS.CONTACTS';
    this.entity_url = 'CONFIGURATIONS.CONTACTS_ID';
  }
}
