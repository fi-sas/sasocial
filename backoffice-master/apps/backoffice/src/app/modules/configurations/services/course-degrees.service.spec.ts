import { TestBed } from '@angular/core/testing';

import { CourseDegreesService } from './course-degrees.service';

describe('CourseDegreesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseDegreesService = TestBed.get(CourseDegreesService);
    expect(service).toBeTruthy();
  });
});
