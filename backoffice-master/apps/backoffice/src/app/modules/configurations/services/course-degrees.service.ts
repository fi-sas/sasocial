import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CourseDegreeModel } from '../models/course-degree.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class CourseDegreesService extends Repository<CourseDegreeModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.COURSE_DEGREES';
    this.entity_url = 'CONFIGURATIONS.COURSE_DEGREES_ID';
  }
}
