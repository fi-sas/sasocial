import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CourseModel } from '../models/course.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class CoursesService  extends Repository<CourseModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.COURSES';
    this.entity_url = 'CONFIGURATIONS.COURSES_ID';
  }
}

