import { Repository } from './../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { first } from 'rxjs/operators';
import { ComponentModel } from '@fi-sas/backoffice/modules/configurations/models/component.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DevicesService extends Repository<DeviceModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.DEVICES';
    this.entity_url = 'CONFIGURATIONS.DEVICES_ID';
  }

  getTypes(): Observable<Resource<any>> {
    return this.resourceService
      .list(this.urlService.get('CONFIGURATIONS.DEVICES_TYPES'))
      .pipe(first());
  }

  getOrientations(): Observable<Resource<any>> {
    return this.resourceService
      .list(this.urlService.get('CONFIGURATIONS.DEVICES_ORIENTATIONS'))
      .pipe(first());
  }

  addComponentToDevice(
    comp: ComponentModel
  ): Observable<Resource<ComponentModel>> {
    return this.resourceService
      .create<ComponentModel>(this.urlService.get('CONFIGURATIONS.COMPONENTS'), comp)
      .pipe(first());
  }

  removeComponentFromDevice(id: number): Observable<any> {
    return this.resourceService
      .delete(this.urlService.get('CONFIGURATIONS.COMPONENTS_ID', { id }))
      .pipe(first());
  }

  getComponentFromDevice(device_id: number): Observable<any> {
    return this.resourceService
      .list(this.urlService.get('CONFIGURATIONS.DEVICES_COMPONENTS', { device_id }))
      .pipe(first());
  }
}
