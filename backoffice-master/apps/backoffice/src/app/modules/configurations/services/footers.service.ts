import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { FooterModel } from '@fi-sas/backoffice/modules/configurations/models/footer.model';
import { first } from 'rxjs/operators';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FootersService extends Repository<FooterModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.FOOTERS';
    this.entity_url = 'CONFIGURATIONS.FOOTERS_ID';
  }

  addGroupToFooter(
    footer_id: number,
    group_id: number
  ): Observable<Resource<any>> {
    return this.resourceService
      .create(this.urlService.get('CONFIGURATIONS.GROUPS_FOOTERS'), {
        footer_id,
        group_id
      })
      .pipe(first());
  }

  removeGroupFromFooter(footer_id: number, group_id: number): Observable<any> {
    return this.resourceService
      .delete(
        this.urlService.get('CONFIGURATIONS.GROUPS_FOOTERS_DELETE', {
          group_id,
          footer_id
        })
      )
      .pipe(first());
  }
}
