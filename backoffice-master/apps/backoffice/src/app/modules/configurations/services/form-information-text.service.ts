import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { FormInformationTextsModel } from '../models/form-information-texts.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormInformationTextsService extends Repository<FormInformationTextsModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.FORM_INFORMATION';
    this.entity_url = 'CONFIGURATIONS.FORM_INFORMATION_ID';
  }

  getInfoByKey(key: string) {
    let params = new HttpParams();
    params = params.append('query[key]', key);
    const url = this.urlService.get('CONFIGURATIONS.FORM_INFORMATION');
    return this.resourceService.read<any>(url, {
      params
    } );

  }

  

}
