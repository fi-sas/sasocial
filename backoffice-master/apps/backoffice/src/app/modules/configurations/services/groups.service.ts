import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { GroupModel } from '@fi-sas/backoffice/modules/configurations/models/group.model';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupsService extends Repository<GroupModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.GROUPS';
    this.entity_url = 'CONFIGURATIONS.GROUPS_ID';
  }

  addDeviceToGroup(
    group_id: number,
    device_id: number
  ): Observable<Resource<any>> {
    return this.resourceService
      .create(this.urlService.get('GROUPS_DEVICES'), {
        group_id,
        device_id
      })
      .pipe(first());
  }

  removeDeviceFromGroup(group_id: number, device_id: number): Observable<any> {
    return this.resourceService
      .delete(
        this.urlService.get('GROUPS_DEVICES_DELETE', {
          group_id,
          device_id
        })
      )
      .pipe(first());
  }
}
