import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { HolidayModel } from '../models/holiday.model';

@Injectable({
  providedIn: 'root'
})
export class HolidaysService extends Repository<HolidayModel>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.HOLIDAYS';
    this.entity_url = 'CONFIGURATIONS.HOLIDAYS_ID';
  }
}
