import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ServiceModel } from "@fi-sas/backoffice/modules/configurations/models/service.model";
import { Observable } from 'rxjs';
import { TermsAndPolicyModel } from '../models/terms-and-policy.model';

@Injectable({
  providedIn: 'root'
})
export class InformationService extends Repository<TermsAndPolicyModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.INFORMATIONS';
    this.entity_url = 'CONFIGURATIONS.INFORMATIONS_ID';
  }


  getImformation(id): Observable<Resource<TermsAndPolicyModel>> {
    const aux = this.urlService.get('CONFIGURATIONS.INFORMATIONS') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }
}
