import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { first } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class LanguagesService extends Repository<LanguageModel> {

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.LANGUAGES';
    this.entity_url = 'CONFIGURATIONS.LANGUAGES_ID';

  }

  reorder(data: LanguageModel[]): Observable<Resource<LanguageModel>> {
    const url = this.urlService.get('CONFIGURATIONS.REORDER_LANGUAGES');
    return this.resourceService.patch(url, { languages: data });
  }

}
