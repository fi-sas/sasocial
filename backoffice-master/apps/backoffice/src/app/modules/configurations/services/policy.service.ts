import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ServiceModel } from "@fi-sas/backoffice/modules/configurations/models/service.model";
import { Observable } from 'rxjs';
import { TermsAndPolicyModel } from '../models/terms-and-policy.model';

@Injectable({
  providedIn: 'root'
})
export class PolicyService extends Repository<TermsAndPolicyModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.PRIVACY_POLICIES';
    this.entity_url = 'CONFIGURATIONS.PRIVACY_POLICIES_ID';
  }


  getPolicy(id): Observable<Resource<TermsAndPolicyModel>> {
    const aux = this.urlService.get('CONFIGURATIONS.PRIVACY_POLICIES') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }
}
