import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SectionModel } from "@fi-sas/backoffice/modules/configurations/models/section.model";

@Injectable({
  providedIn: 'root'
})
export class SectionService extends Repository<SectionModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.SECTIONS';
    this.entity_url = 'CONFIGURATIONS.SECTIONS_ID';
  }
}
