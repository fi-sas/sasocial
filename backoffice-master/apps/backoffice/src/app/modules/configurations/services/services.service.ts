import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ServiceModel } from "@fi-sas/backoffice/modules/configurations/models/service.model";

@Injectable({
  providedIn: 'root'
})
export class ServicesService extends Repository<ServiceModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.SERVICES';
    this.entity_url = 'CONFIGURATIONS.SERVICES_ID';
  }
}
