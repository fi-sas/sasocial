import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';

@Injectable({
  providedIn: 'root'
})
export class TaxesService extends Repository<TaxModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.TAXES';
    this.entity_url = 'CONFIGURATIONS.TAXES_ID';
  }
}
