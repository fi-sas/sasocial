import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { TextApresentationsModel } from '../models/text-apresentation.model';

@Injectable({
  providedIn: 'root'
})
export class TextApresentationService extends Repository<TextApresentationsModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.TEXTAPRESENTATION';
    this.entity_url = 'CONFIGURATIONS.TEXTAPRESENTATION_ID';
  }



  getTextByService(id): Observable<Resource<TextApresentationsModel>> {
    const aux = this.urlService.get('CONFIGURATIONS.TEXTAPRESENTATION') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }

  createText(data: TextApresentationsModel): Observable<Resource<TextApresentationsModel>> {
    const url = this.urlService.get(this.entities_url);
    return this.resourceService.create(url, data);
  }

  updateText(id: number, data: TextApresentationsModel): Observable<Resource<TextApresentationsModel>> {
    const url = this.urlService.get(this.entity_url, { id });
    return this.resourceService.update(url, data);
  }
}
