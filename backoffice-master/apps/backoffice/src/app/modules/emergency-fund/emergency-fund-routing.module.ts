import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { EmergencyFundComponent } from './emergency-fund.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: EmergencyFundComponent,
    children: [
      {
        path: 'applications',
        loadChildren: '../emergency-fund/modules/applications/applications.module#ApplicationsModule',
        data: { breadcrumb: 'Candidaturas', title: 'Candidaturas' },
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmergencyFundRoutingModule { }
