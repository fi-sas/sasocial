import { SiderItemBadge } from '../../core/models/sider-item-badge';

import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { BehaviorSubject } from 'rxjs';
import { EmergencyFundService } from './services/emergency-fund.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-emergency-fund',
  template: '<router-outlet></router-outlet>',
})
export class EmergencyFundComponent implements OnInit {
  dataConfiguration: any;

  constructor(
    private uiService: UiService,
    private emergencyfundService: EmergencyFundService,
    private authservice: AuthService,
    private configurationsService: ConfigurationGeralService
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }
  
  ngOnInit() {
  
    this.uiService.setSiderTitle(this.validTitleTraductions(1), 'home');
  
  
    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();
  
    //const dashboard = new SiderItem('Painel', '', '/accommodation/dashboard', 'accommodation:applications:read');
    //this.uiService.addSiderItem(dashboard);
  
    const applications = new SiderItem('Candidaturas');
    applications.addChild(new SiderItem('Nova', '', '/emergency-fund/applications/form-application', 'emergency_fund:applications:create'));
    this.uiService.addSiderItem(applications);
  

    this.uiService.setSiderActive(true);
  }
    
  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
