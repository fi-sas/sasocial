import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmergencyFundRoutingModule } from './emergency-fund-routing.module';
import { EmergencyFundComponent } from './emergency-fund.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { EmergencyFundService } from './services/emergency-fund.service';
import { UsersService } from '../users/modules/users_users/services/users.service';

@NgModule({
  declarations: [
    EmergencyFundComponent,
    
  ],
  imports: [
    CommonModule,
    EmergencyFundRoutingModule,
    SharedModule,
  ],
  entryComponents: [
  ],
  providers: [
    EmergencyFundService,
    UsersService
  ],
})
export class EmergencyFundModule { }
