import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { FormApplicationComponent } from "./pages/form-application/form-application.component";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";


const routes: Routes = [
  { path: '', redirectTo: 'all-applications', pathMatch: 'full' }, 
  {
    path: 'form-application',
    component: FormApplicationComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'emergency_fund:applications:create' }
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    data: {
      breadcrumb: 'Página não encontrada',
      title: 'Página não encontrada'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
