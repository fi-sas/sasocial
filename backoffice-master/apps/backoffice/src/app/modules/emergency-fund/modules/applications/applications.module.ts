import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormApplicationComponent } from "./pages/form-application/form-application.component";
import { ApplicationsRoutingModule } from './application-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    FormApplicationComponent,
  ],
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    SharedModule
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class ApplicationsModule { }
