export class ApplicationActiveModel {
    id: number;
    active: boolean;
}