
export interface ApplicationStatusBulkModel {
    event: string;
    notes: string;
    ids: number[];
    extra_data: {};
}
