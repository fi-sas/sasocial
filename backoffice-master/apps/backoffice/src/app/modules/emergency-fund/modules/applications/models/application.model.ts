import { RoomModel } from '@fi-sas/backoffice/modules/accommodation/modules/rooms/models/room.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { PaymentMethodModel } from '@fi-sas/backoffice/modules/financial/models/payment-method.model';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';

export enum ApplicationStatus {
  DRAFTED = 'drafted',
  SUBMITTED = 'submitted',
  ANALYSED = 'analysed',
  CANCELLED = 'cancelled',
  PENDING = 'pending',
  UNASSIGNED = 'unassigned',
  QUEUED = 'queued',
  ASSIGNED = 'assigned',
  CONFIRMED = 'confirmed',
  REJECTED = 'rejected',
  OPPOSITION = 'opposition',
  CONTRACTED = 'contracted',
  CLOSED = 'closed',
}

export enum BillingStatus {
  WAITING = 'waiting',
  PROCCESSING = 'proccessing',
  ERROR_IN_PROCCESSIING = 'error_in_proccessing',
  PROCCESSED = 'proccessed',
  VERIFIEING = 'verifieing',
  ERROR_IN_VERIFIEING = 'error_in_verifieing',
  VERIFIED = 'verified',
}

export const BillingStatusResult = {
  waiting: { label: 'Espera de dados', color: 'green' },
  proccessing: { label: 'Em processamento', color: 'blue' },
  error_in_proccessing: { label: 'Error em processamento', color: 'red' },
  proccessed: { label: 'Processado', color: 'orange' },
  verifieing: { label: 'Em verificação', color: 'blue' },
  error_in_verifieing: { label: 'Erro em verificação', color: 'red' },
  verified: { label: 'Vereficado', color: 'green' },
};

export const ApplicationStatusTranslations = {
  drafted: { label: 'Rascunho', color: 'gray' },
  submitted: { label: 'Submetida', color: 'green' },
  analysed: { label: 'Em analise', color: 'green' },
  cancelled: { label: 'Cancelada', color: 'red' },
  pending: { label: 'Despacho', color: 'blue' },
  notAccepted: { label: 'Não Aceite', color: 'red' },
  unassigned: { label: 'Não Colocado', color: 'red' },
  queued: { label: 'Em Lista de Espera', color: 'blue' },
  assigned: { label: 'Colocado', color: 'green' },
  confirmed: { label: 'Confirmado', color: 'green' },
  rejected: { label: 'Rejeitado', color: 'orange' },
  contracted: { label: 'Contratado', color: 'green' },
  opposition: { label: 'Em oposição', color: 'red' },
  closed: { label: 'Fechada', color: 'blue' },
  withdrawal: { label: 'Desistência', color: 'red' },
};

export const ApplicationStatusMachine = {
  drafted: [
    {
      event: 'SUBMIT',
      type: 'success',
      text: 'Submeter',
      icon: 'check-circle',
      permission: null,
    },
  ],
  submitted: [
    {
      event: 'CANCEL',
      type: 'fail',
      text: 'Cancelar',
      icon: 'close-circle',
      permission: 'accommodation:applications:status',
    },
    {
      event: 'ANALYSE',
      type: 'success',
      text: 'Analisar',
      icon: 'audit',
      permission: 'accommodation:applications:status',
    },
  ],
  analysed: [
    {
      event: 'CANCEL',
      type: 'fail',
      text: 'Cancelar',
      icon: 'close-circle',
      permission: 'accommodation:applications:status',
    },
    {
      event: 'PENDING',
      type: 'success',
      text: 'Despacho',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  pending: [
    {
      event: 'DISPACH_APPPROVE',
      type: 'success',
      text: 'Aceitar',
      icon: 'check-circle',
      permission: 'accommodation:applications:dispatch',
    },
    {
      event: 'DISPACH_REJECT',
      type: 'fail',
      text: 'Rejeitar',
      icon: 'issues-close',
      permission: 'accommodation:applications:dispatch',
    },
  ],
  queued: [
    {
      event: 'PENDING',
      type: 'success',
      text: 'Despacho',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  assigned: [
    {
      event: 'CONFIRM',
      type: 'success',
      text: 'Confirmar',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
    {
      event: 'REJECT',
      type: 'fail',
      text: 'Rejeitar',
      icon: 'close-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  confirmed: [
    {
      event: 'CONTRACT',
      type: 'success',
      text: 'Contratar',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  rejected: [
    {
      event: 'ANALYSE',
      type: 'success',
      text: 'Analisar',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  opposition: [
    {
      event: 'PENDING',
      type: 'success',
      text: 'Despacho',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  contracted: [
    {
      event: 'CLOSE',
      type: 'warning',
      text: 'Fechar',
      icon: 'close-circle',
      permission: 'accommodation:applications:status',
    },
  ],
  closed: [
    {
      event: 'CONTRACT',
      type: 'success',
      text: 'Voltar a contrato',
      icon: 'check-circle',
      permission: 'accommodation:applications:status',
    },
  ],
};

export class ApplicationHistory {
  id: number;
  application_id: number;
  status: ApplicationStatus;
  user_id: number;
  user?: UserModel;
  notes: string;
  updated_at: string;
  created_at: string;
  decision: ApplicationDecision;
  room: RoomModel;
}

export enum ApplicationDecision {
  assign = 'ASSIGN',
  unassign = 'UNASSIGN',
  enqueue = 'ENQUEUE',
}
export const ApplicationDecisionList = [
  { label: 'Colocado', value: ApplicationDecision.assign },
  { label: 'Não Colocado', value: ApplicationDecision.unassign },
  { label: 'Lista de Espera', value: ApplicationDecision.enqueue },
];

export const ApplicationDecisionTranslations = {
  ASSIGN: { label: 'Colocado', color: 'green' },
  UNASSIGN: { label: 'Não Colocado', color: 'red' },
  ENQUEUE: { label: 'Lista de espera', color: 'blue' },
};

export class ApplicationModel {
  id: number;
  course_id: string;
  gender: string;
  mother_name: string;
  father_name: string;
  incapacity: boolean;
  incapacity_type: string;
  incapacity_degree: string;
  nationality: string;
  organic_unit_id: number;
  updated_at: Date;
  created_at: Date;
  status: ApplicationStatus;
  history: ApplicationHistory[];
  income_statement_files: any[];
  file_ids: any[];
  files: any[];
  full_name: string;
  applicant_birth_date: string;
  identification: number;
  document_type_id: number;
  document_type: DocumentTypeModel;
  per_capita_income: number;
  tin: number;
  student_number: string;
  course: CourseModel;
  course_degree: string;
  course_year: number;
  registry_validation: string;
  admission_date: string;
  email: string;
  secundary_email: string;
  phone_1: string;
  phone_2: string;
  address: string;
  city: string;
  postal_code: string;
  country: string;
  parent_profession_father: string;
  parent_profession_mother: string;
  household_elements_number: number;
  household_total_income: number;
  household_distance: number;
  international_student: boolean;
  patrimony_category_id: number;
  patrimony_category: string;
  house_hould_elements: HouseHoldElementsModel[];
  househould_elements_in_university: number;
  income_statement_files_ids: [];
  income_statement_file?: FileModel;
  income_statement_delivered: boolean;
  assigned_residence_id: number;
  residence_id: number;
  second_option_residence_id: number;
  regime_id: number;
  current_regime_id: number;
  extra_ids?: number[];
  has_used_residences_before: boolean;
  which_residence_id: string;
  which_room_id: string;
  whichRoom?: RoomModel;
  applied_scholarship: boolean;
  scholarship_value: number;
  has_scholarship: boolean;
  iban: string;
  swift: string;
  applicant_consent: boolean;
  consent_date: string;
  start_date: Date;
  end_date: Date;
  updated_end_date: Date;
  income_source: string[];
  other_income_source: string;
  room_id: number;
  room?: RoomModel;
  user_id: number;
  user?: UserModel;
  signed_contract_file_id: number;
  signed_contract_file?: FileModel;
  liability_term_file_id: number;
  liability_term_file?: FileModel;
  decision: ApplicationDecision;
  submission_date: string;
  submitted_out_of_date: boolean;
  observations: string;
  opposition_request: string;
  opposition_answer: string;
  has_opposed: boolean;
  billing_status: BillingStatus;
  total_unbilled: number;
  total_unpaid: number;
  preferred_typology_id: number;
  tariff_id?: number;
  organic_unit: OrganicUnitsModel;
  user_previous_applications: any;
  academic_year: string;
  application_phase_id: number;
  allow_direct_debit: boolean;
  occurrences: any;
}

export class HouseHoldElementsModel {
  kinship: string;
  profession: string;
}
