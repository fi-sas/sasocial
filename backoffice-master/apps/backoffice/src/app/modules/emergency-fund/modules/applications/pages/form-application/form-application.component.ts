import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from '@fi-sas/backoffice/modules/emergency-fund/modules/applications/services/applications.service';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { first, finalize } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { CoursesDegreesServices } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/courses-degrees.service';
import { EmergencyFundService } from '@fi-sas/backoffice/modules/emergency-fund/services/emergency-fund.service';
import { NzModalService } from 'ng-zorro-antd';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { compareTwoFieldsValidation, trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { PatrimonyCategoriesModel } from '../../models/patrimony-categories.model';
import { nationalities } from '@fi-sas/backoffice/shared/common/nationalities';
import { ApplicationActiveModel } from '../../models/application-active.model';
import { DocumentTypeService } from '@fi-sas/backoffice/modules/users/modules/document-type/services/document-type.service';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';


@Component({
  selector: 'fi-sas-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less']
})
export class FormApplicationComponent implements OnInit {
  fixed: boolean = false;
  index = 'identification';
  appActive: ApplicationActiveModel;
  other_income: boolean = false;
  studentSelectedFlg = false;
  studentSelected: UserModel;
  year: number = 0;
  isSubmited = false;
  disabledIdentification;
  fileListIncomeStatement = [];
  fileListOthers = [];
  residenceId: number = 0;
  current = 0;
  first = true;
  get f() { return this.aplicationForm.controls; }

  residenceApplicationModel = new ApplicationModel();
  nameResidenceSelected: string;
  nameUsedResidenceSelected: string;
  nameRoom: string;
  nameOrganic: string;
  nameCourseDegree: string;
  nameCourse: string;
  descPatrimony: string;
  residenceName: string;
  documentType: DocumentTypeModel;

  loading = false;

  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  aplicationForm: FormGroup;

  organics: OrganicUnitsModel[] = [];
  allow_optional_residence = false;
  show_iban_on_application_form = false;
  loadingButton = false;
  loadingRoom = false;
  user: UserModel;
  listPatrimony: PatrimonyCategoriesModel[] = [];
  courseDegreesList: any;
  coursesList: any;
  documentTypes: DocumentTypeModel[] = [];
  nationalities = nationalities;
  courseLoading = false;
  previousNumberElements = 1;
  academic_year: string = '';
  startDate: Date;
  endDate: Date;

  constructor(private fb: FormBuilder,
    private router: Router,
    private applicationService: ApplicationsService,
    private coursesDegreesServices: CoursesDegreesServices,
    private modal: NzModalService,
    private uiService: UiService,
    private organicUnitsService: OrganicUnitsService,
    private documentTypeService: DocumentTypeService,
    private academicYearsService: AcademicYearsService) {
  }

  newForm() {
    return this.fb.group({
      // 1 step
      full_name: new FormControl('', [Validators.required, trimValidation]),
      applicant_birth_date: new FormControl(null, [Validators.required]),
      gender: [null, [Validators.required]],

    });
  }

  ngOnInit() {
    //const data = this.accommodationService.getAccommodationData();


  }

  goToForm() {
    
    this.applicationService.verifyActive(this.studentSelected.id).pipe(first()).subscribe((data) => {
      this.appActive = data.data[0];
      if (!this.appActive.active) {
        this.aplicationForm = this.newForm();
        this.studentSelectedFlg = true;
        //this.fileListIncomeStatement = [];
        //this.fileListOthers = [];
        //this.first = true;
        this.newApplicaionStartingValues();
        //this.getDocumentTypes();
        //this.addNewGroup();
        //this.getPhases();
        //this.getConfigurations();
      } else {
        this.uiService.showMessage(MessageType.error, "Este estudante já tem uma candidatura");
      }
    });

    /*this.applicationService.canAply(this.studentSelected.id).pipe(first()).subscribe((data) => {
      this.canAply = data.data[0];
      if (this.canAply.can) {
        this.aplicationForm = this.newForm();
        this.studentSelectedFlg = true;
        this.fileListIncomeStatement = [];
        this.fileListOthers = [];
        this.first = true;
        this.newApplicaionStartingValues();
        this.getDocumentTypes();
        this.addNewGroup();
        this.getPhases();
        this.getConfigurations();
      } else {
        this.uiService.showMessage(MessageType.error, "Este estudante já tem uma candidatura");
      }
    })*/
  }

  cancelApplicationForm() {
    this.studentSelectedFlg = false;
    //this.addNewGroup()
    this.current = 0;
    this.studentSelected = null;
    this.aplicationForm.reset();
  }

  previous(): void {
    this.current -= 1;
    this.changeContent();
  }

  newApplicaionStartingValues() {
    if (this.studentSelected) {
      this.aplicationForm.get('full_name').patchValue(this.studentSelected.name);
      this.aplicationForm.get('full_name').updateValueAndValidity();
    }
    if (this.studentSelected.birth_date) {
      this.aplicationForm.get('applicant_birth_date').patchValue(this.studentSelected.birth_date);
      this.aplicationForm.get('applicant_birth_date').updateValueAndValidity();
    }
    if (this.studentSelected.gender) {
      this.aplicationForm.get('gender').patchValue(this.studentSelected.gender);
      this.aplicationForm.get('gender').updateValueAndValidity();
    }
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'identification';
        break;
      }
      case 1: {
        this.index = 'household';
        break;
      }
      case 2: {
        this.index = 'real_estate';
        break;
      }
      case 3: {
        this.index = 'household_expenses';
        break;
      }
      case 4: {
        this.index = 'expenses';
        break;
      }
      case 5: {
        this.index = 'documents';
        break;
      }
      case 6: {
        this.index = 'supports';
        break;
      }
      case 7: {
        this.index = 'explanation';
        break;
      }
      case 8: {
        this.index = 'revision';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }

  next(): void {
    this.isSubmited = true;
    let valid = false;
    switch (this.current) {
      case 0: {
        /*
        this.aplicationForm.get("student_address"),
        this.aplicationForm.get("student_zip_code"),
        this.aplicationForm.get("student_location"),
        this.aplicationForm.get("student_nationality"),
        this.aplicationForm.get("student_type_identif_doc"),
        this.aplicationForm.get("student_identif_number"),
        this.aplicationForm.get("student_nif"),
        this.aplicationForm.get("student_niss"),
        this.aplicationForm.get("student_iban"),
        this.aplicationForm.get("student_number"),
        this.aplicationForm.get("school_id"),
        this.aplicationForm.get("course_degree_id"),
        this.aplicationForm.get("course_id"),
        this.aplicationForm.get("academic_year"),
        this.aplicationForm.get("course_year"),
        this.aplicationForm.get("student_email"),
        this.aplicationForm.get("student_mobile_phone"),
        this.aplicationForm.get("student_address_classes"),
        this.aplicationForm.get("student_add_class_zip_code"),
        this.aplicationForm.get("student_add_class_location"),
        this.aplicationForm.get("student_schedule"),
        this.aplicationForm.get("student_regime"),
        this.aplicationForm.get("student_other_regime"),
*/
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('full_name'),
          this.aplicationForm.get('applicant_birth_date'),
          this.aplicationForm.get('gender')
        ]);
        valid = true;
        break;
      }
      case 1: {
        valid = true;
/*
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('email'),
          this.aplicationForm.get('secundary_email'),
          this.aplicationForm.get('phone_1'),
          this.aplicationForm.get('phone_2')
        ]);*/
        break;
      }
      case 2: {
        valid = true;
/*
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('residence_id'),
          this.aplicationForm.get('second_option_residence_id'),
          this.aplicationForm.get('dateRange'),
          this.aplicationForm.get('regime_id'),
          this.aplicationForm.get('extra_ids'),
          this.aplicationForm.get('has_used_residences_before'),
          this.aplicationForm.get('which_residence'),
          this.aplicationForm.get('which_room'),
        ]);*/
        break;
      }
      case 3: {
        valid = true;
/*
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('international_student'),
          this.aplicationForm.get('organic_unit_id'),
          this.aplicationForm.get('course_degree_id'),
          this.aplicationForm.get('course_id'),
          this.aplicationForm.get('course_year'),
          this.aplicationForm.get('student_number'),
          this.aplicationForm.get('admission_date')
        ]);*/

        break;
      }
      case 4: {
        valid = true;
/*
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('house_hould_elements'),
          this.aplicationForm.get('household_elements_number'),
          this.aplicationForm.get('household_total_income'),
          this.aplicationForm.get("income_source"),
          this.aplicationForm.get("other_income_source"),
          this.aplicationForm.get('patrimony_category_id'),
          this.aplicationForm.get('income_statement_delivered'),
          this.aplicationForm.get('househould_elements_in_university')
        ]) && this.validIncomeStatementDelivered();*/
        break;
      }
      case 5: {
        valid = true;
/*
        valid = this.checkValidityOfControls([
          this.aplicationForm.get('applied_scholarship'),
          this.aplicationForm.get('scholarship_value')
        ]);*/
        break;
      }
      case 6:
        valid = true;
        break;
    }

    if (valid) {
      this.isSubmited = false;
      this.current += 1;
      this.changeContent();
    }
  }

  valuesForm() {
    this.isSubmited = true;

    //let valid = true;
    this.isSubmited = false;
    this.current += 1;
    this.changeContent();
    let valid = this.checkValidityOfControls([
      this.aplicationForm.get("applicant_consent"),
    ]);
    if (valid) {
      this.residenceApplicationModel = new ApplicationModel();
      /*this.extrasSend = [];
      this.getNameStep1();
      this.getNamesStep2();
      this.getNamesStep3();
      this.getNamesStep4();*/
      this.isSubmited = false;
      this.current += 1;
      this.changeContent();
      // 0 step
      this.residenceApplicationModel.full_name = this.aplicationForm.controls.full_name.value;
      this.residenceApplicationModel.applicant_birth_date = this.aplicationForm.controls.applicant_birth_date.value;
      this.residenceApplicationModel.gender = this.aplicationForm.controls.gender.value;
      
    }
  }

  submitForm() {

    /*if (this.aplicationForm.valid) {
      this.loadingButton = true;
      this.applicationService.create(this.residenceApplicationModel).pipe(
        first(),
        finalize(() => this.loadingButton = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, "Candidatura submetida com sucesso");
        this.router.navigateByUrl('emergency-fund/dashboard');
      });
    }*/
    this.uiService.showMessage(MessageType.success, "Candidatura submetida com sucesso");
    this.router.navigateByUrl('emergency-fund/dashboard');

  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find(c => c.status !== 'VALID');
  }

}
