import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  generateURLSepaFile(billing_ids: number[], year: string, month: number) {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.GENERATE_SEPA_FILE');
    return this.resourceService.http.post(url, {
      billing_ids,
      year,
      month
    }, {
      headers: new HttpHeaders({
        'Accept': 'text/html, application/xhtml+xml, */*',
        //'Content-Type': 'application/x-www-form-urlencoded'
      }),
      observe: 'response',
      responseType: 'text'
    }).pipe(first(), map((data => {
      this.downLoadFile(data.body, data.headers.has('content-disposition') ? data.headers.get('content-disposition').split("filename=")[1].replace(/"/g, ''): "sepaFile.xml", 'application/xml')
    })));
  }


    /**
     * Method is use to download file.
     * @param data - Array Buffer data
     * @param type - type of the document.
     */
     downLoadFile(data: any, filename: string, type: string) {
      let blob = new Blob([data], { type: type});
      let url = window.URL.createObjectURL(blob);

      const downloadLink = document.createElement("a");
      downloadLink.href = url;
      downloadLink.download = filename;
      console.log(downloadLink)
      downloadLink.click();
  }

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  getApplicationReport(application_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.APPLICATION_REPORT');
    return this.resourceService.create<any>(url, {
      id: application_id,
    }).pipe(first());
  }

  getApplicationsResultsReport(residence_id: number, application_phase_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.APPLICATIONS_RESULTS');
    return this.resourceService.create<any>(url, {
      residence_id,
      application_phase_id
    }).pipe(first());
  }

  getApplicationsRawReport(academic_year: string, residence_id: number, application_phase_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.APPLICATIONS_RAW');
    return this.resourceService.create<any>(url, {
      academic_year,
      residence_id,
      application_phase_id
    }).pipe(first());
  }

  getClothingMap(residence_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.CLOTHING_MAP');
    return this.resourceService.create<any>(url, {
      residence_id,
    }).pipe(first());
  }

  getScholarshipMap(residence_id: number, application_phase_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.SCHOLARSHIP_MAP');
    return this.resourceService.create<any>(url, {
      residence_id,
      application_phase_id
    }).pipe(first());
  }

  getRegimeMap(residence_id: number, application_phase_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.REGIME_MAP');
    return this.resourceService.create<any>(url, {
      residence_id,
      application_phase_id
    }).pipe(first());
  }

  getExtrasMap(residence_id: number, application_phase_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.EXTRAS_MAP');
    return this.resourceService.create<any>(url, {
      residence_id,
      application_phase_id
    }).pipe(first());
  }

  getProcess(residence_id: number, academic_year: string, year: string, month: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.PROCESS');
    return this.resourceService.create<any>(url, {
      residence_id,
      academic_year,
      year,
      month
    }).pipe(first());
  }

  getReportSepa(residence_id: number, year: string, month: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.SEPA');
    return this.resourceService.create<any>(url, {
      residence_id,
      year,
      month
    }).pipe(first());
  }

  getTariffsMap(residence_id: number, academic_year: string, tariff_id: number, year: string, month: number): Observable<Resource<any>> {
    const url = this.urlService.get('ACCOMMODATION.REPORTS.TARIFFS_MAP');
    return this.resourceService.create<any>(url, {
      residence_id,
      academic_year,
      tariff_id,
      year,
      month
    }).pipe(first());
  }
}
