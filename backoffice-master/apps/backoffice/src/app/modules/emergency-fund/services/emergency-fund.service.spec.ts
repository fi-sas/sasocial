import { TestBed } from '@angular/core/testing';

import { EmergencyFundService } from './emergency-fund.service';

describe('EmergencyFundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmergencyFundService = TestBed.get(EmergencyFundService);
    expect(service).toBeTruthy();
  });
});
