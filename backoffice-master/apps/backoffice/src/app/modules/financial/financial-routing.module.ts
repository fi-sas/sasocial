import { FinancialComponent } from './financial.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { CashAccountSelectionComponent } from './pages/cash-account-selection/cash-account-selection.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';




const routes: Routes = [
  {
    path: '',
    component: FinancialComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'cash-account-selection',
        component: CashAccountSelectionComponent,
        data: { breadcrumb: 'Selecionar Caixa', title: 'Selecionar Caixa' , scope:'current_account:cash_account:read'}
      },
      {
        path: 'dashboard',
        loadChildren: '../financial/modules/dashboard/dashboard.module#DashboardModule',
        data: { scope: 'current_account' },
      },
      {
        path: 'reports',
        loadChildren: '../financial/modules/cc-reports/cc-reports.module#CcReportsModule',
      },
      {
        path: 'cash-list-history',
        loadChildren: './modules/cash-list-history/cash-list-history.module#CashListHistoryModule',
        data: { breadcrumb: 'Fecho de Caixa', title: 'Fecho de caixa' },
      },
      {
        path: 'current-accounts',
        loadChildren: '../financial/modules/current-account/current-account.module#CurrentAccountModule',
        data: { breadcrumb: 'Contas Correntes', title: 'Contas Correntes', scope: 'current_account:accounts' },
      },
      {
        path: 'configurations',
        loadChildren: '../financial/modules/configurations/configurations.module#FinancialConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações' },
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialRoutingModule { }
