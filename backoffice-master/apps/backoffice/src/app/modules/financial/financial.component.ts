import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CashAccountsService } from './services/cash-accounts.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-financial',
  template: '<router-outlet></router-outlet>'
})
export class FinancialComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private cashAccountsService: CashAccountsService, private authService: AuthService,
    private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(11), 'wallet');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const cashAccountChoose = new SiderItem('Escolher Caixa', '', '/financial/cash-account-selection','current_account:cash_account:read');
    this.uiService.addSiderItem(cashAccountChoose);


    this.cashAccountsService.selectedCashAccountsDataObservable().subscribe(data => {
         cashAccountChoose.name = '[' + data.code + '] ';
     });


    if(this.authService.hasPermission('current_account:cash_account:read')) {
      this.cashAccountsService.getDefaultSelectedCashAccount();
    }

    /*const dashboard = new SiderItem('Painel', '', '/financial/dashboard');
    this.uiService.addSiderItem(dashboard);*/

    const current_accounts = new SiderItem('Contas Corrente', '', '/financial/current-accounts','current_account:movements:read');
    this.uiService.addSiderItem(current_accounts);
    const box_list = new SiderItem('Fecho de Caixa', '', '/financial/cash-list-history','current_account:cash_account:read');
    this.uiService.addSiderItem(box_list);
    /*
        const movements = new SiderItem('Movimentos', '', '/financial/list-movements');
        this.uiService.addSiderItem(movements);

        const pendingMoviments = new SiderItem('Movimentos Pendentes', '', '/financial/list-pending-movements');
        this.uiService.addSiderItem(pendingMoviments);


        const orders = new SiderItem('Pedidos', '', '/financial/list-orders');
        this.uiService.addSiderItem(orders);

        const owner_applications = new SiderItem('Carregamentos');
        owner_applications.addChild(new SiderItem('Carregar', '', '/financial/list-owner-applications'));
        owner_applications.addChild(new SiderItem('Listar', '', '/financial/list-owner-applications'));
        this.uiService.addSiderItem(owner_applications);

        const payments = new SiderItem('Pagamentos', '', '/financial/list-payments');
        this.uiService.addSiderItem(payments);
    */
    const reports = new SiderItem('Relatórios', '', '/financial/reports');
    this.uiService.addSiderItem(reports);
    const configs = new SiderItem('Configurações');

    const config_accounts = configs.addChild(new SiderItem('Contas Corrente', '', '', 'current_account:accounts'));
    config_accounts.addChild(
      new SiderItem('Criar', '', '/financial/configurations/accounts/create','current_account:accounts:create')
    );
    config_accounts.addChild(
      new SiderItem('Listar', '', '/financial/configurations/accounts/list','current_account:accounts:read')
    );

    let config_document_types = configs.addChild(new SiderItem('Tipos de Movimentos', '', '', 'current_account:document_types'));
    config_document_types.addChild(
      new SiderItem(
        'Listar',
        '',
        '/financial/configurations/documents/list',
        'current_account:document_types:read'
      )
    );

    let config_cash_accounts = configs.addChild(new SiderItem('Caixas', '', '', 'current_account:cash_account'));
    config_cash_accounts.addChild(
      new SiderItem(
        'Criar',
        '',
        '/financial/configurations/cash-accounts/create',
        'current_account:cash_account:create'
      )
    );
    config_cash_accounts.addChild(
      new SiderItem(
        'Listar',
        '',
        '/financial/configurations/cash-accounts/list',
        'current_account:cash_account:read')
    );

    //configs.addChild(new SiderItem('Ficheiros', '', '/financial/configurations/documents', 'current_account:document_types:read'));

    this.uiService.addSiderItem(configs);

    this.uiService.setSiderActive(true);
  }
  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
