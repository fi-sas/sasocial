import { FinancialRoutingModule } from './financial-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FinancialComponent } from './financial.component';
import { CashAccountSelectionComponent } from './pages/cash-account-selection/cash-account-selection.component';
@NgModule({
  declarations: [
    FinancialComponent,
    CashAccountSelectionComponent
  ],
  imports: [
    CommonModule,
    FinancialRoutingModule,
    SharedModule,
    NgxChartsModule,
  ],
  providers: [
    DatePipe
  ]
})
export class FinancialModule { }
