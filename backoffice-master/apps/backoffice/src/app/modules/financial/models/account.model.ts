import { TagResult } from "@fi-sas/backoffice/shared/components/tag/tag.component";

export enum PlafondType {
  PLAFOND = 'PLAFOND',
  ADVANCE = 'ADVANCE'
}

export const PlafondTypeResults: { [key: string]: TagResult } = {
  PLAFOND: { label: 'Plafond', color: 'gray' },
  ADVANCE: { label: 'Adiantamento', color: 'gray' },
}

export enum DocumentType {
  ADIANTAMENTO = 'Adiantamento',
  ANULACAO = 'Anulação',
  CARREGAMENTO = 'Carregamento',
  FATURARECIBO = 'Fatura/Recibo',
  RECIBO = 'Recibo',
  FATURA = 'Fatura'
}

export class AccountModel {
  id: number;
  name: string;
  institute: string;
  tin: string;
  iban: string;
  allow_partial_payments: boolean;
  plafond_type: string;
  plafond_value: number;
  public_document_types: any;
  middleware_erp_exemption_reason: string;
  helpdesk_user_id: any;
  middleware_erp_path: string;
  created_at: string;
  updated_at: string;
  middleware_erp_document_types: any;
}
