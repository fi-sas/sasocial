import { PaymentMethodModel } from "./payment-method.model";

export class BalanceModel {
  selected = false;
  account_name: string;
  account_id: number;
  current_balance: number;
  in_debt: number;
  available_methods: PaymentMethodModel[];
}
