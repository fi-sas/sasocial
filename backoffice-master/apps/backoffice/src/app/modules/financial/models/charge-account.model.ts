export class ChargeAccountModel {
  user_id: number;
  charge_value: number;
  account_id: number;
  payment_method_id: number;
  payment_method_data: {};
  description: string;
}
