export class ChargeServiceModel {
  account_id: number;
  user_id: number;
  transaction_value: number;
  payment_method_id: string;
  description: string;
  cash_account_id: number;
}

class ItemsModel {
  service_id: number;
  product_code: string;
  name: string;
  description: string;
  quantity: number;
  vat_id: number;
  unit_value: number;
  discount_value: number;
  location: string;
  article_type: string;
  service_confirm_path: string;
  service_cancel_path: string;
}
