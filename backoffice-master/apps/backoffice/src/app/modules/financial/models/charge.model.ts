export enum ChargeStatus {
  CONFIRMED = 'Confirmed',
  PENDING = 'Pending',
  CANCELLED = 'Cancelled'
}

export class ChargeModel {
      id: number;
      user_id: number;
      device_id: number;
      charge_value: number;
      account_id: number;
      payment_method_id: number;
      payment_method_data: {};
      description: string;
      total_value: number;
      cost_value: number;
      fee: number;
      fee_value: number;
      user_supports_cost: boolean;
      status: ChargeStatus;
      updated_at: string;
      created_at: string;
}
