import { MovementOperation } from "./movement.model";

export class DefaultItemConfigurationModel {
  id: number;
  parameter_account_id: number;
  parameter_operation: MovementOperation;
  parameter_service_id: number;
  product_code: string;
  name: string;
  description: string;
  vat_id: number;
  erp_budget: string;
  erp_sncap: string;
  erp_income_cost_center: string;
  erp_funding_source: string;
  erp_program: string;
  erp_measure: string;
  erp_project: string;
  erp_activity: string;
  erp_action: string;
  erp_functional_classifier: string;
  erp_organic: string;
  created_at: Date;
  updated_at: Date;
}
