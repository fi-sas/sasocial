export class DocumentTypeModel {
  id: number;
  name: string;
  description: string;
  doc_type_acronym: string;
  operation: string;
  active: boolean;
  updated_at: string;
  created_at: string;
}
