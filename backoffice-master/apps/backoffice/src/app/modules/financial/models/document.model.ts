import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';

export class DocumentModel {
  id: number;
  type: string;
  series: number;
  number: number;
  date: string;
  expiration_date: string;
  value: number;
  file_id: string;
  updated_at: string;
  created_at: string;
  file: FileModel
}
