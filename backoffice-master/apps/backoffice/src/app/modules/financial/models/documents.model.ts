export class DocumentsModule {

  active: boolean;
  created_at: string;
  description: string;
  doc_type_acronym: string;
  id: number;
  name: string;
  operation: string;
  updated_at: Date;
  type: string;
  series: number;
  number: number;
  expiration_date: string;
  date: string;
  value: number;
  file_id: number;
  file: any;
  url: string;
}
