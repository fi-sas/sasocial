export class ItemsModel {
  id: number;
  movement_id: number;
  product_code: string;
  name: string;
  description: string;
  extra_info: ExtraInfo;
  quantity: number;
  unit_value: number;
  liquid_value: number;
  vat_value: number;
  total_value: number;
  vat_id: number;
  vat: number;
  location: string;
  article_type: string;
  erp_vat: string;
  erp_budget: any;
  erp_sncap: any;
  erp_income_cost_center: any;
  erp_funding_source: any;
  erp_program: any;
  erp_measure: any;
  erp_project: any;
  erp_activity: any;
  erp_action: any;
  erp_functional_classifier: any;
  erp_organic: any;
  updated_at: string;
  created_at: string;
  service_id: number;
  original_movement_id: any;
  paid_value: number;
  discount_value: number;
  service: {
      id: number;
      active: true;
      updated_at: string;
      created_at: string;
      target_id: any;
      translations: [
          {
              language_id: number;
              name: string;
              description: string;
          }
      ];
  };
}

class ExtraInfo {
  refectory_id: number;
  refectory_consume_at: string;
  refectory_meal_category: string;
  refectory_meal_type: string;
}
