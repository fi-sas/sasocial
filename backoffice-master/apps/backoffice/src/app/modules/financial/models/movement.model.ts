
import { ItemModel } from './item.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { DocumentModel } from './document.model';
import { AccountModel } from './account.model';



export enum MovementOperation {
  "CHARGE", 			// charge operation (+)
  "CANCEL", 			// undo charge operation (+)
  "RECEIPT", 			// add credit operation (+)
  "INVOICE", 			// add debit operation (-)
  "REFUND", 			// undo lend/charge operation (-) Reembolso/devolução
  "INVOICE_RECEIPT"
}

export enum TransactionType {
  IMMEDIATE = 'Immediate',
  CREADIT = 'Credit'
}

export enum MovementStatus {
  CONFIRMED = 'Confirmed',
  PENDING = 'Pending',
  PAID = 'Paid',
  CANCELLED = 'Cancelled',
  CANCELLEDLACKOFPAYMENT = 'Cancelled due to Lack of Payment'
}

export class MovementModel {
  id: number;
  operation: MovementOperation;
  expiration_date: string;
  account_id: number;
  user_id: number;
  user: UserModel;
  entity: string;
  tin: string;
  email: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  transaction_id: number;
  description: string;
  transaction_type: TransactionType;
  status: MovementStatus;
  payment_method_id: number;
  payment_method_name: string;
  affects_current_balance: true;
  transaction_value: number;
  erp_payment_method: string;
  erp_payment_account_code: string;
  current_balance: number;
  owing_value: number;
  advanced_value: number;
  paid_value: number;
  value_to_pay: number;
  updated_at: string;
  created_at: string;
  items: ItemModel[];
  account: AccountModel;
  documents: DocumentModel[];
  paid_by: MovementModel[];
  paid_by_pending: MovementModel[];
}
export class ArrayData {

  status = [
    {key: '', label: 'Estado'},
    {key: 'Confirmed', label: 'Confirmado'},
    {key: 'Pending', label: 'Pendente'},
    {key: 'Paid', label: 'Pago'},
    {key: 'Cancelled', label: 'Cancelado'},
    {key: 'Cancelled due to Lack of Payment', label: 'Cancelado devido a falta de pagamento'}
  ];

  operations = [
    {key: '', label: 'Operação'},
    {key: 'Adiantamento', label: 'Adiantamento'},
    {key: 'Anulação de Adiantamento', label: 'Anulação de Adiantamento'},
    {key: 'Carregamento', label: 'Carregamento'},
    {key: 'Fatura/Recibo', label: 'Fatura/Recibo'},
    {key: 'Recibo', label: 'Recibo'},
    {key: 'Fatura', label: 'Fatura'},
    {key: 'Devolução', label: 'Devolução'}
  ];

}
