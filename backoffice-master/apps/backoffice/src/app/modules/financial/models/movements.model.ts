
import { AccountModel } from './account.model';
import { CashAccountModel } from './cash-account.model';
import { ItemsModel } from './items.model';
import { PaymentMethodModel } from './payment-method.model';
import { DocumentModel } from './document.model';
export class MovementsModel {
  device_id: number;
  opened = false;
  selected = false;
  id: number;
  document_id: number;
  operation: string;
  expiration_date: string;
  account_id: number;
  user_id: number;
  entity: string;
  tin: string;
  email: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  transaction_id: number;
  transaction_value: number;
  owing_value: number;
  description: string;
  transaction_type: string;
  total_value: number;
  paid_value: number;
  in_debt_value: number;
  advanced_value: number;
  status: string;
  payment_method: PaymentMethodModel;
  payment_method_id: number;
  payment_method_name: string;
  erp_payment_method: string;
  erp_payment_account_code: string;
  affects_current_balance: boolean;
  current_balance: number;
  updated_at: string;
  created_at: string;
  value_to_pay: number;
  account: AccountModel;
  documents: DocumentModel[];
  items: ItemsModel[];
  cash_account: CashAccountModel;
}

export class ArrayData {

  status = [
    { key: '', label: 'Estado' },
    { key: 'CONFIRMED', label: 'Confirmado' },
    { key: 'PENDING', label: 'Pendente' },
    { key: 'CANCELLED', label: 'Anulado' },
    { key: 'CANCELLED_LACK_PAYMENT', label: 'Anulado/Rejeitado' },
    { key: 'PAID', label: 'Pago' }
  ];

  operations = [
    { key: '', label: 'Operação' },
    { key: 'CHARGE', label: 'Carregamento' },
    { key: 'CHARGE_NOT_IMMEDIATE', label: 'Carregamento (em espera)' },
    { key: 'CREDIT_NOTE', label: 'Nota de crédito' },
    { key: 'LEND', label: 'Empréstimo' },
    { key: 'REFUND', label: 'Restituição' },
    { key: 'CANCEL', label: 'Anulado' },
    { key: 'RECEIPT', label: 'Recibo' },
    { key: 'INVOICE', label: 'Fatura' },
    { key: 'INVOICE_RECEIPT', label: 'Fatura/Recibo' },
  ];
}
