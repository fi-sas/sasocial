import { ItemOrderModel } from "./item-order.model";

export enum OrderStatus {
  PENDING_PAYMENT = 'Pending Payment',
  CANCELLED = 'Cancelled',
  PAID = 'Paid',
  CONFIRMED = 'Confirmed'
}

export const OrderStatusTagResult = {
  'Pending Payment': { label: 'Pagamento pendente', color: 'blue'},
  'Cancelled': { label: 'Cancelada', color: 'red'},
  'Paid': { label: 'Paga', color: 'green'},
  'Confirmed': { label: 'Confirmada', color: 'green'}
}

export class OrderModel {
    id: number;
    account_id: number;
    user_id: number;
    payment_id: number;
    cancel_payment_id: number;
    value: number;
    status: OrderStatus;
    items: ItemOrderModel[];
}
