export class PayMovementsItem {
  id: number;
  value: number;
}

export class PayMovementsMovement {
  id: number;
  items: PayMovementsItem[];
}

export class PayMovementsModel {
  payment_method_id: number;
  payment_method_data: any;
  movements: PayMovementsMovement[];
}
