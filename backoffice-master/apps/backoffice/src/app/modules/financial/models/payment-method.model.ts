export class PaymentMethodModel {
  active: boolean;
  charge: boolean;
  created_at: Date;
  description: string;
  gateway_data: string;
  id: string;
  is_immediate: boolean;
  name: string;
  path: string;
  tag: string;
  updated_at: Date;
}
