import { UserModel } from '../../users/modules/users_users/models/user.model';
import { AccountModel } from './account.model';
import { PaymentMethodModel } from './payment-method.model';
export enum PaymentStatus {
  CONFIRMED = 'Confirmed',
  PEDING = 'Pending',
  CANCELLE = 'Cancelled'
}

export const PaymentStatusTagResult = {
  'Confirmed': { label: 'Confirmado', color: 'green'},
  'Pending': { label: 'Em espera', color: 'blue'},
  'Cancelled': { label: 'Cancelado', color: 'red'},
}

export class PaymentModel {
    id: number;
    account?: AccountModel;
    account_id: number;
    user?: UserModel;
    user_id: number;
    payment_method?: PaymentMethodModel;
    payment_method_id: number;
    payment_method_data: any;
    total_value: number;
    movement_ids: number[];
    status: PaymentStatus;
    expiration: string;
    created_at: string;
    updated_at: string;
}
