export class PaymentMethodSettingsModel {
    id: number;
    account_id: number;
    payment_method_id: string;
    min_charge_amount: number;
    max_charge_amount: number;
}