export class UnchargeAccountModel {
  user_id: number;
  uncharge_value: number;
  account_id: number;
  payment_method_id: number;
  payment_method_data: {};
  description: string;
}