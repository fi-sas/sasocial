export enum UnchargeStatus {
  CONFIRMED = 'Confirmed',
  PENDING = 'Pending',
  CANCELLED = 'Cancelled'
}

export class UnchargeModel {
      id: number;
      user_id: number;
      device_id: number;
      uncharge_value: number;
      account_id: number;
      payment_method_id: number;
      payment_method_data: {};
      description: string;
      total_value: number;
      cost_value: number;
      fee: number;
      fee_value: number;
      user_supports_cost: boolean;
      status: UnchargeStatus;
      updated_at: string;
      created_at: string;
}