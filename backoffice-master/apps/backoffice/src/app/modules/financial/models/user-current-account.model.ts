import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";

export class UserCurrentAccountModel {
  selected: boolean;
  user: UserModel;

  constructor(user: UserModel){
    this.selected = false;
    this.user = user;
  }
}
