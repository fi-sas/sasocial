import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashListHistoryComponent } from './cash-list-history.component';

const routes: Routes = [
  {
    path: '',
    component: CashListHistoryComponent,
    data: { breadcrumb: null, title: 'Fecho de Caixa' },
  },
  { path: '', redirectTo: '', pathMatch: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashListHistoryRoutingModule { 
}
