import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CashListHistoryComponent } from './cash-list-history.component';


describe('CashListHistoryComponent', () => {
  let component: CashListHistoryComponent;
  let fixture: ComponentFixture<CashListHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashListHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashListHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
