import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import * as moment from 'moment';
import { finalize, first } from "rxjs/operators";
import { NzMessageService } from "ng-zorro-antd";
import { CCReportsService } from "../../services/cc-reports.service";
import { CashAccountsService } from "../../services/cash-accounts.service";
import { CashAccountModel } from "../../models/cash-account.model";

@Component({
    selector: 'fi-sas-cash-list-history',
    templateUrl: './cash-list-history.component.html',
    styleUrls: ['./cash-list-history.component.less']
})

export class CashListHistoryComponent extends TableHelper implements OnInit {
    selectedCashAccount = null;
    selectOptions = [
        {
            value: moment().toISOString(),
            label: 'Hoje'
        },
        {
            value: moment().subtract(1, 'day').toISOString(),
            label: 'Ontem'
        },
        {
            value: 'CUSTOM',
            label: 'Personalizado'
        }
    ];
    loadingReport = false;
    selectedValue;
    dateFormat = 'yyyy/MM/dd';
    startDate = null;
    endDate = null;
    cashAccounts: CashAccountModel[] = [];
    boxInit;
    disableStartDate = (current: Date): boolean => {
        if (this.endDate !== null) {
            return !moment(this.endDate).isBetween(moment(current), moment());
        }
        return current > new Date();
    };

    disableEndDate = (current: Date): boolean => {
        if (this.startDate !== null) {
            return !moment(current).isBetween(moment(this.startDate), moment());
        }
        return current > new Date();

    };

    constructor(uiService: UiService,
        router: Router, activatedRoute: ActivatedRoute,
         private cashAccountsService: CashAccountsService,
        private ccReportsService: CCReportsService,
         private message: NzMessageService) {
        super(uiService, router, activatedRoute);

    }

    ngOnInit() {
        this.cashAccountsService.getUserAvailableCashAccounts().subscribe(response => {
            this.cashAccounts = response.data;
        });
        this.selectedCashAccount = this.cashAccountsService.selectedCashAccount();
        if (this.selectedCashAccount) {
            this.boxInit = this.selectedCashAccount.id;
        }
        this.selectedValue = this.selectOptions[0].value;
        this.persistentFilters = {
            withRelated: 'payment_method,cash_account,created_by',
            cash_account_id: this.boxInit,
            created_at: moment(this.selectedValue).format('YYYY-MM-DD')
        }
        this.initTableData(this.cashAccountsService);

    }

    searchData(reset = false) {
        this.loading = true;
        if (reset) {
          this.pageIndex = 1;
        }
    
        this.updateUrl();
    
        this.cashAccountsService.cashAccountClose(this.pageIndex, this.pageSize, this.sortKey, this.sortValue,
           this.filterObject())
        .pipe(
          first(),
          finalize(() => this.loading = false)
          )
        .subscribe(res => {
          this.totalData = res.link.total;
          this.data = res.data;
        })
      }

    onChangeSelectInput() {
        if (this.selectedValue !== 'CUSTOM') {
            this.startDate = null;
            this.endDate = null;
            this.getMovements(null, null, this.selectedValue);

        }
    }

    onChangeStartDate(startDate) {
        this.startDate = startDate ? startDate.toISOString() : null;
        if (this.startDate !== null && this.endDate !== null) {
            this.getMovements(this.startDate, this.endDate, null);
        }
    }

    onChangeEndDate(endDate) {
        this.endDate = endDate ? endDate.toISOString() : null;
        if (this.startDate !== null && this.endDate !== null) {
            this.getMovements(this.startDate, this.endDate, null);
        }
    }


    getMovements(startDate: string, endDate: string, date: string) {
        if (this.boxInit) {
            this.persistentFilters['cash_account_id'] = this.boxInit;
            if (startDate && endDate) {
                this.filters['created_at'] = {};
                this.filters['created_at']['lte'] = moment(endDate).format('YYYY-MM-DD');
                this.filters['created_at']['gte'] = moment(startDate).format('YYYY-MM-DD');
            } else if (date != 'CUSTOM') {
                this.filters.created_at = null;
                this.filters['created_at'] = moment(date).format('YYYY-MM-DD');
            }

            this.searchData(true);
        }

    }

    generateReport() {
        this.loadingReport = true;
        this.ccReportsService.reportCashAccountClose(
            this.boxInit,
            this.startDate,
            this.endDate,
            this.selectedValue
        )
            .pipe(
                first(),
                finalize(
                    () => {
                        this.loadingReport = false
                    }
                )
            ).subscribe(
                (report) => {
                    this.message.create('success', 'Relatório criado com sucesso');
                },
                (error) => {
                }
            );
    }
}