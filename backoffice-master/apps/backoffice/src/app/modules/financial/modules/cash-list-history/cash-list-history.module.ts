import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CashListHistoryRoutingModule } from './cash-list-history-routing.module';
import { CashListHistoryComponent } from './cash-list-history.component';

@NgModule({
  declarations: [
    CashListHistoryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CashListHistoryRoutingModule
  ]
})
export class CashListHistoryModule { }
