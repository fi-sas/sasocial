import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { CcReportsComponent } from "./cc-reports.component";



const routes: Routes = [
      {
        path: '',
        component: CcReportsComponent,
        data: { breadcrumb: 'Relatórios', title: 'Relatórios de Conta-corrente' }
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      },
      { path: '', redirectTo: '', pathMatch: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CcReportsRoutingModule { }

