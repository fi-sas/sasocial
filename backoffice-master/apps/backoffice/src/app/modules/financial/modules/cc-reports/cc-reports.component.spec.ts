import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcReportsComponent } from './cc-reports.component';

describe('CcReportsComponent', () => {
  let component: CcReportsComponent;
  let fixture: ComponentFixture<CcReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
