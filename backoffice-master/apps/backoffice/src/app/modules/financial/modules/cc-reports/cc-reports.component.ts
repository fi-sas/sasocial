import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { CCReportsService } from '../../services/cc-reports.service';
import { MovimentsService } from '../../services/moviments.service';
import { NzMessageService, NzTreeHigherOrderServiceToken } from 'ng-zorro-antd';
import { DeviceModel } from '@fi-sas/backoffice/modules/configurations/models/device.model';
import { FamiliesService } from '@fi-sas/backoffice/modules/alimentation/services/families.service';
import { DevicesService } from '@fi-sas/backoffice/modules/configurations/services/devices.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/configurations/models/service.model';
import { ServicesService } from '@fi-sas/backoffice/modules/configurations/services/services.service';
import { CurrentAccountsService } from '../../services/current-accounts.service';
import { AccountModel } from '../../models/account.model';
import { PaymentMethodModel } from '../../models/payment-method.model';
import { PaymentMethodsService } from '../../services/payment-methods.service';
import { ReportsService } from '@fi-sas/backoffice/modules/reports/services/reports.service';

function betweenDatesValidator(form: FormGroup): { [key: string]: boolean } {
  if (
    form.controls['out_of_date_from'].value !== null &&
    moment(form.controls['out_of_date_from'].value).isValid
  ) {
    if (form.controls['dateRange'].value) {
      if (
        !moment(form.controls['out_of_date_from'].value).isBetween(
          form.controls['dateRange'].value[0],
          form.controls['dateRange'].value[1]
        )
      ) {
        return { notBetween: true };
      }
    }
  }

  return null;
}

@Component({
  selector: 'fi-sas-cc-reports',
  templateUrl: './cc-reports.component.html',
  styleUrls: ['./cc-reports.component.less'],
})
export class CcReportsComponent implements OnInit {
  accountsLoading = false;
  accounts: AccountModel[] = [];
  filter_account_id = null;

  paymentMethodLoading= false;
  paymentMethods: PaymentMethodModel[] = [];

  deviceLoading = false;
  devices: DeviceModel[] = [];

  serviceLoading = false;
  services: ServiceModel[] = [];

  submit = false;
  filter_charges_rangeDate = null;

  filter_charges_report_rangeDate = null;


  chargesLoadingReport = false;
  movementsLoadingReport = false;

  formGroup = new FormGroup(
    {
      dateRange: new FormControl([], Validators.required),
      out_of_date_from: new FormControl(null, [Validators.required]),
      academic_year: new FormControl(null, Validators.required),
      application_phase: new FormControl(1, Validators.required),
      out_of_date_validation: new FormControl(false, Validators.required),
      out_of_date_validation_days: new FormControl(null),
      fixed_accommodation_period: new FormControl(false, Validators.required),

      filter_charges_report_rangeDate: new FormControl(
        false,
        Validators.required
      ),
      filter_movements_report_rangeDate: new FormControl(
        false,
        Validators.required
      ),
    },
    betweenDatesValidator
  );

  constructor(
    private reportsService: ReportsService,
    private ccReportsService: CCReportsService,
    private accountsService: CurrentAccountsService,
    private devicesService: DevicesService,
    private sercicesService: ServicesService,
    private paymentMethodsService: PaymentMethodsService,
    private message: NzMessageService
  ) {}

  ngOnInit() {
    this.loadAccounts();
    this.loadDevices();
    this.loadServices();
    this.loadPaymentMethods();
  }

  get f() {
    return this.formGroup.controls;
  }

  loadPaymentMethods() {
    this.paymentMethodLoading = true;
    this.accountsService
      .list(0, -1, null, null, {
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => this.paymentMethodLoading = false)
      )
      .subscribe((res) => {
        this.accounts = res.data;
      });
  }

  loadAccounts() {
    this.accountsLoading = true;
    this.paymentMethodsService
      .list(0, -1, null, null, {
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => this.accountsLoading = false)
      )
      .subscribe((res) => {
        this.paymentMethods = res.data;
      });
  }

  loadServices() {
    this.serviceLoading = true;
    this.sercicesService
      .list(0, -1, null, null, {
        withRelated: 'translations',
      })
      .pipe(
        first(),
        finalize(() => (this.serviceLoading = false))
      )
      .subscribe((res) => {
        this.services = res.data;
      });
  }

  loadDevices() {
    this.deviceLoading = true;
    this.devicesService
      .list(0, -1, null, null, {
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.deviceLoading = false))
      )
      .subscribe((res) => {
        this.devices = res.data;
      });
  }

  generateChargesReport() {

    if(this.checkCorrentAccount()) {
      return;
    }

    if (!this.filter_charges_report_rangeDate) {
      this.message.create(
        'warning',
        'Tem que indicar um período de datas válido!'
      );
      return;
    }

    this.chargesLoadingReport = true;
    this.ccReportsService
      .reportChargesPeriod(
        this.filter_charges_report_rangeDate
          ? moment(this.filter_charges_report_rangeDate[0]).format('YYYY-MM-DD')
          : '',
        this.filter_charges_report_rangeDate
          ? moment(this.filter_charges_report_rangeDate[1]).format('YYYY-MM-DD')
          : '',
        this.filter_account_id
      )
      .pipe(
        first(),
        finalize(() => (this.chargesLoadingReport = false))
      )
      .subscribe(
        (report) => {
          this.message.create('success', 'Relatório criado com sucesso');
        },
        (error) => {
          this.message.create('error', 'Ocorreu um erro ao gerar o relatório');
          console.error(error);
        }
      );
  }


  filter_charges_detailed_report_rangeDate = [];
  filter_charges_detailed_report_payment_method_id = null;
  filter_charges_detailed_report_device_ids = null;
  filter_charges_detailed_report_user_id = null;
  chargesDetailedLoadingReport = false;

  generateChargesDetailedReport() {

    if(this.checkCorrentAccount()) {
      return;
    }

    if (!this.filter_charges_detailed_report_rangeDate) {
      this.message.create(
        'warning',
        'Tem que indicar um período de datas válido!'
      );
      return;
    }

    this.chargesDetailedLoadingReport = true;
    this.ccReportsService
      .reportChargesDetailedPeriod({
        startDate: this.filter_charges_detailed_report_rangeDate[0],
        endDate: this.filter_charges_detailed_report_rangeDate[1],
        payment_method_id: this.filter_charges_detailed_report_payment_method_id,
        device_ids: this.filter_charges_detailed_report_device_ids,
        user_id: this.filter_charges_detailed_report_user_id,
        account_id: this.filter_account_id
      }
      )
      .pipe(
        first(),
        finalize(() => (this.chargesDetailedLoadingReport = false))
      )
      .subscribe(
        (report) => {
          this.message.create('success', 'Relatório criado com sucesso');
        },
        (error) => {
          this.message.create('error', 'Ocorreu um erro ao gerar o relatório');
          console.error(error);
        }
      );
  }

  filter_movements_report_rangeDate = null;
  filter_movements_report_operations = [];
  filter_movements_report_user_id = null;

  generateMovementsReport() {

    if(this.checkCorrentAccount()) {
      return;
    }

    if (!this.filter_movements_report_rangeDate) {
      this.message.create(
        'warning',
        'Tem que indicar um período de datas válido!'
      );
      return;
    }

    this.movementsLoadingReport = true;
    this.ccReportsService
      .reportMovementsPeriod(
        this.filter_movements_report_rangeDate
          ? moment(this.filter_movements_report_rangeDate[0]).format(
              'YYYY-MM-DD'
            )
          : '',
        this.filter_movements_report_rangeDate
          ? moment(this.filter_movements_report_rangeDate[1]).format(
              'YYYY-MM-DD'
            )
          : '',
        this.filter_movements_report_operations,
        this.filter_movements_report_user_id,
        this.filter_account_id
      )
      .pipe(
        first(),
        finalize(() => (this.movementsLoadingReport = false))
      )
      .subscribe(
        (report) => {
          this.message.create('success', 'Relatório criado com sucesso');
        },
        (error) => {
          this.message.create('error', 'Ocorreu um erro ao gerar o relatório');
          console.error(error);
        }
      );
  }

  filter_sales_resume_report_rangeDate = null;
  salesResumeLoadingReport = false;
  generateSalesResumeReport() {
    if(this.checkCorrentAccount()) {
      return;
    }

    if (!this.filter_sales_resume_report_rangeDate) {
      this.message.create(
        'warning',
        'Tem que indicar um período de datas válido!'
      );
      return;
    }
    this.salesResumeLoadingReport = true;
    this.ccReportsService
      .reportSalesResumePeriod(
        this.filter_sales_resume_report_rangeDate
          ? moment(this.filter_sales_resume_report_rangeDate[0]).format(
              'YYYY-MM-DD'
            )
          : '',
        this.filter_sales_resume_report_rangeDate
          ? moment(this.filter_sales_resume_report_rangeDate[1]).format(
              'YYYY-MM-DD'
            )
          : '',
        this.filter_account_id
      )
      .pipe(
        first(),
        finalize(() => (this.salesResumeLoadingReport = false))
      )
      .subscribe(
        (report) => {
          this.message.create('success', 'Relatório criado com sucesso');
        },
        (error) => {
          this.message.create('error', 'Ocorreu um erro ao gerar o relatório');
          console.error(error);
        }
      );
  }

  filter_sales_detailed_report_operations = [];
  filter_sales_detailed_report_rangeDate = null;
  filter_sales_detailed_report_device_ids = [];
  filter_sales_detailed_report_service_id = null;
  filter_sales_detailed_report_user_id = null;

  salesDetailedLoadingReport = false;
  generateSalesDetailedReport() {

    if(this.checkCorrentAccount()) {
      return;
    }

    if (!this.filter_sales_detailed_report_rangeDate) {
      this.message.create(
        'warning',
        'Tem que indicar um período de datas válido!'
      );
      return;
    }
    this.salesDetailedLoadingReport = true;
    this.ccReportsService
      .reportSalesDetailedPeriod({
        account_id: this.filter_account_id,
        startDate: this.filter_sales_detailed_report_rangeDate
          ? moment(this.filter_sales_detailed_report_rangeDate[0]).format(
              'YYYY-MM-DD'
            )
          : '',
        endDate: this.filter_sales_detailed_report_rangeDate
          ? moment(this.filter_sales_detailed_report_rangeDate[1]).format(
              'YYYY-MM-DD'
            )
          : '',
        device_ids: this.filter_sales_detailed_report_device_ids,
        operations: this.filter_sales_detailed_report_operations,
        service_id: this.filter_sales_detailed_report_service_id,
        user_id: this.filter_sales_detailed_report_user_id,
      })
      .pipe(
        first(),
        finalize(() => (this.salesDetailedLoadingReport = false))
      )
      .subscribe(
        (report) => {
          this.message.create('success', 'Relatório criado com sucesso');
        },
        (error) => {
          this.message.create('error', 'Ocorreu um erro ao gerar o relatório');
          console.error(error);
        }
      );
  }

  checkCorrentAccount() {
    if(!this.filter_account_id) {
      this.message.create(
        'warning',
        'Tem que indicar uma conta corrente primeiro!'
      );

      return true;
    }
    return false;
  }
}
