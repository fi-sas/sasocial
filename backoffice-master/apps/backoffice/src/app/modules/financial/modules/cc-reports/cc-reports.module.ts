import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CcReportsComponent } from './cc-reports.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CcReportsRoutingModule } from './cc-reports-routing.module';

@NgModule({
  declarations: [
    CcReportsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CcReportsRoutingModule
  ]
})
export class CcReportsModule { }
