import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashAccountsListComponent } from './pages/cash-accounts-list/cash-accounts-list.component';
import { ConfPaymentMethodsComponent } from './pages/configurations-payment-methods/configurations-payment-methods.component';
import { DocumentsEditComponent } from './pages/documents-edit/documents-edit.component';
import { DocumentsListComponent } from './pages/documents-list/documents-list.component';
import { FormAccountComponent } from './pages/form-account/form-account.component';
import { FormCashAccountsComponent } from './pages/form-cash-accounts/form-cash-accounts.component';
import { FormPaymentMethodsComponent } from './pages/form-payment-methods/form-payment-methods.component';
import { ListAccountsComponent } from './pages/list-accounts/list-accounts.component';

const routes: Routes = [
  { path: '', redirectTo: 'accounts', pathMatch: 'full' },
  {
    path: 'accounts',
    data: { breadcrumb: 'Contas Correntes', title: 'Contas Correntes' , scope: 'current_account:movements:read'},
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: ListAccountsComponent,
        data: { breadcrumb: 'Listar', title: 'Listar' , scope:'current_account:accounts:read'}
      },
      {
        path: 'create',
        component: FormAccountComponent,
        data: { breadcrumb: 'Criar', title: 'Criar', scope:'current_account:accounts:create' }
      },
      {
        path: 'edit/:id',
        component: FormAccountComponent,
        data: { breadcrumb: 'Editar', title: 'Editar', scope:'current_account:accounts:update' }
      },
     /* {
        path: 'defaults/:account_id',
        component: ListDefaultConfigsComponent,
        data: { breadcrumb: 'Valores por defeito', title: 'Valores por defeito' }
      },*/
      {
        path: 'payment-methods/:account_id',
        component: FormPaymentMethodsComponent,
        data: { breadcrumb: 'Métodos de pagamento disponíveis', title: 'Métodos de pagamento disponíveis' , scope:'payments:payment-methods:read'}
      },
      {
        path: 'payment-methods/configurations/:account_id',
        component: ConfPaymentMethodsComponent,
        data: { breadcrumb: 'Configurações métodos de pagamento', title: 'Configurações métodos de pagamento', scope:'payments:payment-methods:read' }
      }
    ]
  },
  {
    path: 'documents',
    data: { breadcrumb: 'Tipos de Movimentos', title: 'Tipos de Movimentos' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: DocumentsListComponent,
        data: { breadcrumb: 'Listar', title: 'Listar' , scope:'current_account:document_types:read'}
      },
      {
        path: 'edit/:id',
        component: DocumentsEditComponent,
        data: { breadcrumb: 'Editar', title: 'Editar', scope:'current_account:document_types:update' }
      }
    ]
  },
  {
    path: 'cash-accounts',
    data: { breadcrumb: 'Caixas', title: 'Caixas' },
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: CashAccountsListComponent,
        data: { breadcrumb: 'Listar', title: 'Listar', scope:'current_account:cash_account:read'}
      },
      {
        path: 'edit/:id',
        component: FormCashAccountsComponent,
        data: { breadcrumb: 'Editar', title: 'Editar', scope:'current_account:cash_account:update'}
      },
      {
        path: 'create',
        component: FormCashAccountsComponent,
        data: { breadcrumb: 'Criar', title: 'Criar', scope: 'current_account:cash_account:create'}
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
