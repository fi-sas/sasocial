import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { FormAccountComponent } from './pages/form-account/form-account.component';
import { ListAccountsComponent } from './pages/list-accounts/list-accounts.component';
import { ViewAccountComponent } from './pages/view-account/view-account.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListDefaultConfigsComponent } from './pages/list-default-configs/list-default-configs.component';
import { FormPaymentMethodsComponent } from './pages/form-payment-methods/form-payment-methods.component';
import { DocumentsEditComponent } from './pages/documents-edit/documents-edit.component';
import { DocumentsListComponent } from './pages/documents-list/documents-list.component';
import { CashAccountsListComponent } from './pages/cash-accounts-list/cash-accounts-list.component';
import { ViewCashAccountComponent } from './pages/view-cash-account/view-cash-account.component';
import { FormCashAccountsComponent } from './pages/form-cash-accounts/form-cash-accounts.component';
import { ConfPaymentMethodsComponent } from './pages/configurations-payment-methods/configurations-payment-methods.component';
@NgModule({
  declarations: [
    ListAccountsComponent,
    FormAccountComponent,
    ViewAccountComponent,
    ListDefaultConfigsComponent,
    FormPaymentMethodsComponent,
    ConfPaymentMethodsComponent,
    DocumentsEditComponent,
    DocumentsListComponent,
    CashAccountsListComponent,
    ViewCashAccountComponent,
    FormCashAccountsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationsRoutingModule
  ]
})
export class FinancialConfigurationsModule { }
