import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashAccountsListComponent } from './cash-accounts-list.component';

describe('CashAccountsListComponent', () => {
  let component: CashAccountsListComponent;
  let fixture: ComponentFixture<CashAccountsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashAccountsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashAccountsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
