import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CashAccountsService } from '@fi-sas/backoffice/modules/financial/services/cash-accounts.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-cash-accounts-list',
  templateUrl: './cash-accounts-list.component.html',
  styleUrls: ['./cash-accounts-list.component.less']
})
export class CashAccountsListComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private cashAccountsService: CashAccountsService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'users';
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'code',
        label: 'Código',
        sortable: true
      },
      {
        key: 'description',
        label: 'Descrição',
        sortable: true
      }
    )
    this.initTableData(this.cashAccountsService);
  }

  deleteCashAccount(id: number) {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este extra?', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.cashAccountsService.delete(id).pipe(first()).subscribe(result => {
          this.initTableData(this.cashAccountsService);
        });
      }
    });
  }

}
