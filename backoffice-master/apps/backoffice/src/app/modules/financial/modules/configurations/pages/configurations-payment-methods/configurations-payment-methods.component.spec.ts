import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfPaymentMethodsComponent } from './configurations-payment-methods.component';



describe('ConfPaymentMethodsComponent', () => {
  let component: ConfPaymentMethodsComponent;
  let fixture: ComponentFixture<ConfPaymentMethodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfPaymentMethodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfPaymentMethodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
