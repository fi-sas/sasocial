import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { PaymentMethodModel } from "@fi-sas/backoffice/modules/financial/models/payment-method.model";
import { PaymentMethodSettingsModel } from "@fi-sas/backoffice/modules/financial/models/payment_method_settings.model";
import { AccountsPaymentMethodsService } from "@fi-sas/backoffice/modules/financial/services/accounts_payments_methods.service";
import { PaymentMethodsService } from "@fi-sas/backoffice/modules/financial/services/payment-methods.service";
import { finalize, first } from "rxjs/operators";

@Component({
    selector: 'fi-sas-configurations-payment-methods',
    templateUrl: './configurations-payment-methods.component.html',
    styleUrls: ['./configurations-payment-methods.component.less']
})
export class ConfPaymentMethodsComponent implements OnInit {
    loadingConfiguration = false;
    loading = false;
    paymentsMethods: PaymentMethodModel[] = [];
    dataSettings: PaymentMethodSettingsModel[] = [];
    dataSettingsSelected: PaymentMethodSettingsModel = new PaymentMethodSettingsModel();
    isEdit = false;
    id;
    submitted = false;

    formData = new FormGroup({
        account_id: new FormControl(null, [Validators.required]),
        payment_method_id: new FormControl(null, [Validators.required]),
        min_charge_amount: new FormControl(null, [Validators.required,Validators.min(0)]),
        max_charge_amount: new FormControl(null, [Validators.required,Validators.min(0)]),
    });
    get f() { return this.formData.controls; }
    constructor(private accountsPaymentMethodsService: AccountsPaymentMethodsService,
        private uiService:UiService,
        private router: Router,
        private route: ActivatedRoute, private paymentMethodsService: PaymentMethodsService) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('account_id');
        this.listByAccountId(this.id);
        this.listMethods();
    }

    listByAccountId(id) {
        this.accountsPaymentMethodsService.listByAccountId(id).pipe(first(), finalize(() => this.loadingConfiguration = false)).subscribe((data) => {
            this.dataSettings = data.data;
        })
    }

    listMethods() {
        this.paymentMethodsService.list(1, -1, null, null, {
            active: true,
            sort: 'name'
        }).pipe(first()).subscribe((data) => {
            this.paymentsMethods = data.data;
        })
    }

    findValue(event) {
        this.isEdit = false;
        this.dataSettingsSelected = this.dataSettings.find((data) => data.payment_method_id == event);
        if (this.dataSettingsSelected) {
            this.isEdit = true;
            this.formData.get('max_charge_amount').setValue(this.dataSettingsSelected.max_charge_amount);
            this.formData.get('min_charge_amount').setValue(this.dataSettingsSelected.min_charge_amount);
        }else {
            this.formData.get('max_charge_amount').setValue(null);
            this.formData.get('min_charge_amount').setValue(null);
        }
    }

    submitForm(){
        
        let auxError = false;
        this.submitted = true;

        if(this.formData.get('min_charge_amount').value>this.formData.get('max_charge_amount').value){
            auxError = true;
        }
        this.formData.get('account_id').setValue(Number(this.id));
       
        if(this.formData.valid && !auxError) {
            this.loading = true;
            if (this.isEdit) {
                this.accountsPaymentMethodsService.patch(this.dataSettingsSelected.id, {
                    min_charge_amount: this.formData.get('min_charge_amount').value,
                    max_charge_amount: this.formData.get('max_charge_amount').value,
                
                }).pipe(first(),
                finalize(()=>this.loading = false)).subscribe(
                  result => {
                    this.formData.reset();
                    this.submitted = false;
                    this.isEdit = false;
                    this.uiService.showMessage(
                      MessageType.success,
                      'Configuração alterada com sucesso'
                    );
                    this.listByAccountId(this.id);
                  }
                );
              } else {
                this.accountsPaymentMethodsService.create(this.formData.value).pipe(first(),
                finalize(()=>this.loading = false)).subscribe(
                  result => {
                    this.formData.reset();
                    this.submitted = false;
                    this.uiService.showMessage(
                      MessageType.success,
                      'Configuração criada com sucesso'
                    );
                    this.listByAccountId(this.id);
                  })
              }
        }
        
    }

    backList() {
        this.router.navigate(['financial', 'configurations', 'accounts', 'list']);
    }
}