import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DocumentsModule } from '@fi-sas/backoffice/modules/financial/models/documents.model';
import { finalize, first } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentsServiceQueue } from '../../../../services/document.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-documents-edit',
  templateUrl: './documents-edit.component.html',
  styleUrls: ['./documents-edit.component.less']
})
export class DocumentsEditComponent implements OnInit {

  documentForm = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    description: new FormControl('', [Validators.required,trimValidation]),
    doc_type_acronym: new FormControl('', [Validators.required, Validators.maxLength(3), trimValidation]),
    active: new FormControl(false, [Validators.required]),
    operation: new FormControl(''),
  });
  submit = false;
  loading = false;
  idToUpdate = null;
  documentToUpdate: DocumentsModule;
  get f() { return this.documentForm.controls; }

  constructor(
    private activatedRoute: ActivatedRoute,
    private uiService: UiService,
    private router: Router,
    private documentsServiceQueue: DocumentsServiceQueue
  ) { }

  ngOnInit() {
    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id') ? this.activatedRoute.snapshot.params.id : null;
    if(this.idToUpdate) {
      this.documentsServiceQueue.read(this.idToUpdate)
      .pipe(
        first(),
        finalize( () => {} )
      ).subscribe(
        (result) => {
          this.documentToUpdate = result.data[0];
          if (document) {
            this.documentForm.patchValue({
              active: this.documentToUpdate.active,
              description: this.documentToUpdate.description,
              doc_type_acronym: this.documentToUpdate.doc_type_acronym,
              name: this.documentToUpdate.name,
              operation: this.documentToUpdate.operation
            });
          }
        },
        (error) => {}
      );
    }
  }

  submitForm() {

    this.loading = true;
    this.submit = true;
    if (this.documentForm.valid) {
      this.submit = false;
      this.documentToUpdate.name = this.documentForm.controls.name.value;
      this.documentToUpdate.description = this.documentForm.controls.description.value;
      this.documentToUpdate.doc_type_acronym = this.documentForm.controls.doc_type_acronym.value;
      this.documentToUpdate.active = this.documentForm.controls.active.value;

      this.documentsServiceQueue.update(this.idToUpdate, this.documentToUpdate)
      .pipe(
        first(),
        finalize(() => this.loading = false)
      )
      .subscribe(
        (result) => {
          this.uiService.showMessage(MessageType.success, 'Documento atualizado com sucesso');
          this.router.navigate(['financial', 'configurations', 'documents', 'list']);
        }
      );

    }
  }

  backList() {
    this.router.navigate(['financial', 'configurations', 'documents', 'list']);
  }
}
