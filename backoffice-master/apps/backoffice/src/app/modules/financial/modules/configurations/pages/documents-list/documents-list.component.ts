import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { DocumentsServiceQueue } from '../../../../services/document.service';

@Component({
  selector: 'fi-sas-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.less']
})
export class DocumentsListComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private documentsServiceQueue: DocumentsServiceQueue
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'description',
        label: 'Descrição',
        sortable: true
      },
      {
        key: 'doc_type_acronym',
        label: 'Tipo de documento',
        sortable: true
      },
      {
        key: 'active',
        label: 'Estado',
        sortable: true
      },
      {
        key: 'operation',
        label: 'Operação',
        sortable: true
      }
    )
    this.initTableData(this.documentsServiceQueue);
  }

}
