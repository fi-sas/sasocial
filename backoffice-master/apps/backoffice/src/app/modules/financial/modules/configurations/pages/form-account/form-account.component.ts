import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { isString } from 'lodash';
import {
  ibanValidator,
  trimValidation,
  validNIF,
} from '@fi-sas/backoffice/shared/validators/validators.validator';

import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { DocumentTypesService } from '@fi-sas/backoffice/modules/financial/services/document-types.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/financial/models/document-type.model';

@Component({
  selector: 'fi-sas-form-account',
  templateUrl: './form-account.component.html',
  styleUrls: ['./form-account.component.less'],
})
export class FormAccountComponent implements OnInit {
  accountForm = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    tin: new FormControl('', [Validators.required, validNIF]),
    iban: new FormControl('', [
      Validators.required,
      ibanValidator,
      trimValidation,
    ]),
    allow_partial_payments: new FormControl(false, [Validators.required]),
    plafond_type: new FormControl('ADVANCE', [Validators.required]),
    plafond_value: new FormControl(0, [Validators.required]),
    public_document_types: new FormControl([], [Validators.required]),
    middleware_erp_path: new FormControl('', [trimValidation]),
    middleware_erp_exemption_reason: new FormControl('', [trimValidation]),
    helpdesk_user_id: new FormControl([]),
    middleware_erp_document_types: new FormControl([]),
  });
  loading = false;
  get f() {
    return this.accountForm.controls;
  }
  submit = false;
  idToUpdate = null;

  usersSearchChange$ = new BehaviorSubject('');
  users: UserModel[] = [];
  selectedUser: UserModel;
  isLoadingUsers = false;

  erpDocsSearchChange$ = new BehaviorSubject('');
  erpDocs: DocumentTypeModel[] = [];
  selectedErpDoc: DocumentTypeModel;
  isLoadingErpDocs = false;

  constructor(
    public accountsService: CurrentAccountsService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private usersService: UsersService,
    private documentTypesService: DocumentTypesService
  ) {}

  ngOnInit() {
    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id')
      ? this.activatedRoute.snapshot.params.id
      : null;
    if (this.idToUpdate) {
      this.accountsService
        .read(this.idToUpdate, { withRelated: 'middleware_erp_document_types' })
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((result) => {
          this.accountForm.patchValue({
            name: result.data[0].name,
            tin: result.data[0].tin,
            iban: result.data[0].iban,
            allow_partial_payments: result.data[0].allow_partial_payments,
            plafond_type: result.data[0].plafond_type,
            plafond_value: result.data[0].plafond_value,
            public_document_types: result.data[0].public_document_types,
            middleware_erp_exemption_reason:
              result.data[0].middleware_erp_exemption_reason,
            helpdesk_user_id: result.data[0].helpdesk_user_id,
            middleware_erp_path: result.data[0].middleware_erp_path,
            middleware_erp_document_types: (
              result.data[0].middleware_erp_document_types || []
            ).map((medt) => medt.id),
          });

          let userID = result.data[0].helpdesk_user_id
            ? [{ id: result.data[0].helpdesk_user_id }]
            : [];

          this.usersService
            .filterByUserIdAndCanAccessBO(userID.map((u) => u.id))
            .subscribe((users) => {
              this.users = users.data;
            });

          this.documentTypesService
            .filterByDocumentTypeId(
              (result.data[0].middleware_erp_document_types || []).map(
                (d) => d.id
              )
            )
            .subscribe((erpDocs) => {
              this.erpDocs = erpDocs.data;
            });
        });
    }

    const loadUsers = (search: string) =>
      this.usersService
        .list(1, 20, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,email',
          can_access_BO: true, //JUST WHO CAN ACCESS BO
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const loadDocumentTypes = (search: string) =>
      this.documentTypesService
        .list(1, 20, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,description,doc_type_acronym',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.users = users;
      this.isLoadingUsers = false;
    });

    const optionErpDocs$: Observable<
      DocumentTypeModel[]
    > = this.erpDocsSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadDocumentTypes));
    optionErpDocs$.subscribe((erpDocs) => {
      this.erpDocs = erpDocs;
      this.isLoadingErpDocs = false;
    });
  }

  submitForm(value: any) {
    value.public_document_types = value.public_document_types.filter((dt) =>
      isString(dt)
    );
    const helpdeskUserId = this.accountForm.controls.helpdesk_user_id.value;
    value.helpdesk_user_id = Array.isArray(helpdeskUserId)
      ? helpdeskUserId[0] || null
      : helpdeskUserId || null;

    const selectedDocTypesIds = this.accountForm.controls
      .middleware_erp_document_types.value;
    value.middleware_erp_document_types = this.erpDocs.filter((v) =>
      selectedDocTypesIds.includes(v.id)
    );

    this.submit = true;
    if (this.accountForm.valid) {
      this.loading = true;
      this.submit = false;
      if (this.idToUpdate) {
        this.accountsService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Conta corrente atualizada com sucesso.'
            );
            this.router.navigate([
              'financial',
              'configurations',
              'accounts',
              'list',
            ]);
          });
      } else {
        this.accountsService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Conta corrente criada com sucesso.'
            );
            this.router.navigate([
              'financial',
              'configurations',
              'accounts',
              'list',
            ]);
          });
      }
    }
  }

  onSearchUsers(event) {
    this.isLoadingUsers = true;
    this.usersSearchChange$.next(event);
  }
  onSearchErpDocs(event) {
    this.isLoadingErpDocs = true;
    this.erpDocsSearchChange$.next(event);
  }

  backList() {
    this.router.navigate(['financial', 'configurations', 'accounts', 'list']);
  }
}
