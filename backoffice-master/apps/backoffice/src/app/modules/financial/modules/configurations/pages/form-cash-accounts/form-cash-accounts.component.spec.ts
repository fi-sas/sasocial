import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCashAccountsComponent } from './form-cash-accounts.component';

describe('FormCashAccountsComponent', () => {
  let component: FormCashAccountsComponent;
  let fixture: ComponentFixture<FormCashAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCashAccountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCashAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
