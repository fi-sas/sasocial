import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { CashAccountsService } from '@fi-sas/backoffice/modules/financial/services/cash-accounts.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import {
  ibanValidator,
  trimValidation,
} from '@fi-sas/backoffice/shared/validators/validators.validator';
import { relativeTimeRounding } from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-form-cash-accounts',
  templateUrl: './form-cash-accounts.component.html',
  styleUrls: ['./form-cash-accounts.component.less'],
})
export class FormCashAccountsComponent implements OnInit {
  cashAccountForm = new FormGroup({
    code: new FormControl('', [Validators.required, trimValidation]),
    description: new FormControl('', [Validators.required, trimValidation]),
    iban: new FormControl('', [ibanValidator]),
    users: new FormControl([]),
    created_at: new FormControl(null),
  });
  get f() {
    return this.cashAccountForm.controls;
  }
  loading = false;
  submit = false;
  idToUpdate = null;
  usersSearchChange$ = new BehaviorSubject('');
  users: UserModel[] = [];
  selectedUser: UserModel;
  isLoadingUsers = false;

  constructor(
    public cashAccountsService: CashAccountsService,
    private uiService: UiService,
    private router: Router,
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id')
      ? this.activatedRoute.snapshot.params.id
      : null;
    if (this.idToUpdate) {
      this.cashAccountsService
        .read(this.idToUpdate, { withRelated: 'users' })
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((result) => {
          this.cashAccountForm.patchValue({
            code: result.data[0].code,
            description: result.data[0].description,
            users: result.data[0].users.map((u) => u.id),
            created_at: result.data[0].created_at,
          });
          this.usersService
            .filterByUserId(result.data[0].users.map((u) => u.id))
            .subscribe((users) => {
              this.users = users.data;
            });
        });
    }
    const loadUsers = (search: string) =>
      this.usersService
        .list(1, 20, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,student_number,email,identification',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.users = users;
      this.isLoadingUsers = false;
    });
  }
  submitForm(value: any) {
    this.submit = true;

    if (this.cashAccountForm.valid) {
      this.submit = false;
      if (this.idToUpdate) {
        this.submit = false;
        this.cashAccountsService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            this.router.navigate([
              'financial',
              'configurations',
              'cash-accounts',
              'list',
            ]);
            this.uiService.showMessage(
              MessageType.success,
              'Caixa alterada com sucesso'
            );
          });
      } else {
        this.submit = false;
        this.cashAccountsService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Caixa criada com sucesso'
            );
            this.router.navigate([
              'financial',
              'configurations',
              'cash-accounts',
              'list',
            ]);
          });
      }
    }
  }
  onSearchUsers(event) {
    this.isLoadingUsers = true;
    this.usersSearchChange$.next(event);
  }

  backList() {
    this.router.navigate([
      'financial',
      'configurations',
      'cash-accounts',
      'list',
    ]);
  }
}
