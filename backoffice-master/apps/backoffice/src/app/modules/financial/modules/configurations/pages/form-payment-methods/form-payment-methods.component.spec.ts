import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPaymentMethodsComponent } from './form-payment-methods.component';

describe('FormPaymentMethodsComponent', () => {
  let component: FormPaymentMethodsComponent;
  let fixture: ComponentFixture<FormPaymentMethodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPaymentMethodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPaymentMethodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
