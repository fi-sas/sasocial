import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-form-payment-methods',
  templateUrl: './form-payment-methods.component.html',
  styleUrls: ['./form-payment-methods.component.less']
})
export class FormPaymentMethodsComponent implements OnInit {

  data: any[] = [];
  accountId: number = null;

  constructor(
    private accountsService: CurrentAccountsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.accountId = this.activatedRoute.snapshot.params.hasOwnProperty('account_id') ? this.activatedRoute.snapshot.params.account_id : null;
    this.accountsService.treePaymentMethods(this.accountId).subscribe(devices => {
      this.data = devices.data;
    });
  }

  changeValue(device_id: number, payment_method_id: string, active: boolean) {
    this.accountsService.updatePaymentMethod(
      this.accountId,
      device_id,
      payment_method_id,
      active
    ).subscribe(devices => {
      this.data = devices.data;
    });
  }
  backList() {
    this.router.navigate(['financial', 'configurations', 'accounts', 'list']);
}
}
