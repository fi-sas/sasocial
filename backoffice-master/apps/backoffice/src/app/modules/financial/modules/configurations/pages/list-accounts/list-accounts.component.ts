import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AccountModel, PlafondTypeResults } from '../../../../models/account.model';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-list-accounts',
  templateUrl: './list-accounts.component.html',
  styleUrls: ['./list-accounts.component.less']
})
export class ListAccountsComponent extends TableHelper implements OnInit {
  sortKeys = {
    name: 'Nome',
  };
  searchKeys = {
    name: 'Nome',
  };
  fields = {
    name: 'Nome',
  };

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private accountsService: CurrentAccountsService) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'plafond_type',
        label: 'Tipo de plafond',
        tag: PlafondTypeResults,
        sortable: true
      }
    )

    this.initTableData(this.accountsService);
  }

  deleteTemplate(id: number) {
    this.uiService.showConfirm('Eliminar modelo', 'Pretende eliminar este modelo', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.accountsService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Modelo eliminado com sucesso');
        });
      }
    });
  }

}
