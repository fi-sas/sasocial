import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDefaultConfigsComponent } from './list-default-configs.component';

describe('ListDefaultConfigsComponent', () => {
  let component: ListDefaultConfigsComponent;
  let fixture: ComponentFixture<ListDefaultConfigsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDefaultConfigsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDefaultConfigsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
