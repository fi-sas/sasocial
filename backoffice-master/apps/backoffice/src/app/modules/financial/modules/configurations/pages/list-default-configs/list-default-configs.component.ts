import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { DefaultItemConfigurationModel } from '@fi-sas/backoffice/modules/financial/models/default-item-configuration.model';
import { DefaultItemConfigurationService } from '@fi-sas/backoffice/modules/financial/services/default-item-configuration.service';
import  { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-default-configs',
  templateUrl: './list-default-configs.component.html',
  styleUrls: ['./list-default-configs.component.less']
})
export class ListDefaultConfigsComponent extends TableHelper implements OnInit {

  data: DefaultItemConfigurationModel[] = [];
  accountId: number = null;

  constructor(
    private defaultItemConfigsService: DefaultItemConfigurationService,
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {


    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    },
    {
      key: 'description',
      label: 'Descrição',
      sortable: true
    })
    this.accountId = this.activatedRoute.snapshot.params.hasOwnProperty('account_id') ? this.activatedRoute.snapshot.params.account_id : null;
    this.persistentFilters = {
      parameter_account_id: this.accountId
    };

    this.initTableData(this.defaultItemConfigsService);
  }

}
