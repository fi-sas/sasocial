import { Component, Input, OnInit } from '@angular/core';
import { AccountModel } from '../../../../models/account.model';

@Component({
  selector: 'fi-sas-view-account',
  templateUrl: './view-account.component.html',
  styleUrls: ['./view-account.component.less']
})
export class ViewAccountComponent implements OnInit {

  @Input() data: AccountModel = null;

  constructor() { }

  ngOnInit() {
  }

}
