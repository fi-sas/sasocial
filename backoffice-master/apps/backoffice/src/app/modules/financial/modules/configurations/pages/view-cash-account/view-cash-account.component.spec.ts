import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCashAccountComponent } from './view-cash-account.component';

describe('ViewCashAccountComponent', () => {
  let component: ViewCashAccountComponent;
  let fixture: ComponentFixture<ViewCashAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCashAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCashAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
