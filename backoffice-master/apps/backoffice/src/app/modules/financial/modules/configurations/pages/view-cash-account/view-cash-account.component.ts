import { Component, Input, OnInit } from '@angular/core';
import { CashAccountModel } from '@fi-sas/backoffice/modules/financial/models/cash-account.model';

@Component({
  selector: 'fi-sas-view-cash-account',
  templateUrl: './view-cash-account.component.html',
  styleUrls: ['./view-cash-account.component.less']
})
export class ViewCashAccountComponent implements OnInit {


  @Input() data: CashAccountModel = null;

  constructor() { }

  ngOnInit() {
  }

}
