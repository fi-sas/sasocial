import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { listUsersCurrentAccountComponent } from './list-user.component';


describe('listUsersCurrentAccountComponent', () => {
  let component: listUsersCurrentAccountComponent;
  let fixture: ComponentFixture<listUsersCurrentAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listUsersCurrentAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(listUsersCurrentAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
