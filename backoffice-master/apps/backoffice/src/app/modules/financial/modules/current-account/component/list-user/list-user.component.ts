import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { UsersService } from "@fi-sas/backoffice/modules/users/modules/users_users/services/users.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { Repository } from "@fi-sas/backoffice/shared/repository/repository.class";
import { NzDrawerRef } from "ng-zorro-antd";

@Component({
    selector: 'fi-sas-list-user-current-account',
    templateUrl: './list-user.component.html',
    styleUrls: ['./list-user.component.less']
})

export class listUsersCurrentAccountComponent extends TableHelper implements OnInit {

    constructor(public usersService: UsersService, uiService: UiService, private drawerRef: NzDrawerRef,
        router: Router,
        activatedRoute: ActivatedRoute){
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            searchFields: 'name,email,user_name'
        };
        this.columns.push(
            {
                key: 'name',
                label: 'Nome',
                sortable: true,
            },
            {
                key: 'email',
                label: 'Email',
                sortable: true,
            },
            {
                key: 'tin',
                label: 'Nif',
                sortable: true,
            },
            {
                key: 'city',
                label: 'Localidade',
                sortable: true,
            }
        );

       
    }

    ngOnInit() {
        this.initTableData(this.usersService);
    }

    initTableData(repository: Repository<any>) {
        this.repository = repository;
        if (!this.repository) {
          throw new Error('No service provided!');
        }
        this.searchData(true);
    }

    listComplete() {
        this.filters.student_number = null;
        this.filters.phone = null;
        this.filters.tin = null;
        this.filters.search = null;
        this.searchData(true);
    }

    selectUser(user){
        this.drawerRef.close(user);
    }
}