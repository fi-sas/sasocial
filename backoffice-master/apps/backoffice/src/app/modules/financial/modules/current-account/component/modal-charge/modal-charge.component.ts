import { Component, OnInit } from "@angular/core";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BalanceModel } from "@fi-sas/backoffice/modules/financial/models/balance.model";
import { ChargeServiceModel } from "@fi-sas/backoffice/modules/financial/models/charge-service.model";
import { CashAccountsService } from "@fi-sas/backoffice/modules/financial/services/cash-accounts.service";
import { ChargeAccountsService } from "@fi-sas/backoffice/modules/financial/services/charge-account.service";
import { NzMessageService, NzModalRef } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";

@Component({
    selector: 'fi-sas-modal-charge',
    templateUrl: './modal-charge.component.html',
    styleUrls: ['./modal-charge.component.less']
})

export class ModalChargeComponent implements OnInit {
    stepModal = 0;
    selectedAccount: BalanceModel;
    userId: number;
    formModal: any = {
        method: null,
        value: 0,
        description: '',
        errors: false
    };
    isConfirmLoading = false;

    constructor(private modal: NzModalRef, private message: NzMessageService, private cashAccountsService:CashAccountsService,
        private uiService:UiService, private chargeAccountsService:ChargeAccountsService) {

    }

    ngOnInit() {

    }

    getMoneyString(value: number, neutral: boolean) {
        let aux = Number(value.toString()).toFixed(2) + '€';
        aux = aux.split(".").join(",");
        if (value > 0 && !neutral) {
            aux = '+' + aux;
        }
        return aux;
    }

    verifyMoney(value: number, neutral: boolean) {
        if (!neutral) {
            if (value > 0) {
                return 'positive';
            } else if (value < 0) {
                return 'negative';
            }
        }
        return 'neutral';
    }

    handleCancel() {
        this.modal.close();
    }

    handleBack() {
        this.stepModal -= 1;
    }

    handleNext() {
        const valid = this.checkModalFormValid();

        if (valid) {
            this.stepModal += 1;
        }
    }

    checkModalFormValid() {
        this.formModal.errors = false;
        if (this.formModal.value === 0 || !this.formModal.value) {
            this.formModal.errors = true;
        }
        if (this.formModal.method === null || !this.formModal.method) {
            this.formModal.errors = true;
        }
        if (this.formModal.errors) {
            return false;
        }
        return true;
    }

    chargeAccounts() {
        const selectedCashAccount = this.cashAccountsService.selectedCashAccount();
        if (!selectedCashAccount) {
            this.uiService.showMessage(MessageType.error, "Obrigatório selecionar a caixa utilizada na transação");
            return;
        }
        this.isConfirmLoading = true;

        let charge: ChargeServiceModel = new ChargeServiceModel;
        charge.payment_method_id = this.formModal.method.id;
        charge.account_id = this.selectedAccount.account_id;
        charge.user_id = this.userId;
        charge.transaction_value = this.formModal.value;
        charge.cash_account_id = selectedCashAccount.id;

        if (this.formModal.description && this.formModal.description !== '') {
            charge.description = this.formModal.description;
        }

        this.chargeAccountsService.create(charge)
            .pipe(
                first(),
                finalize(() => {
                    this.isConfirmLoading = false;
                }))
            .subscribe(result => {
                this.stepModal = 2
            });
    }

    confirm(){
        this.modal.close(true);
    }
}