import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalUnChargeComponent } from './modal-uncharge.component';


describe('ModalUnChargeComponent', () => {
  let component: ModalUnChargeComponent;
  let fixture: ComponentFixture<ModalUnChargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalUnChargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUnChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
