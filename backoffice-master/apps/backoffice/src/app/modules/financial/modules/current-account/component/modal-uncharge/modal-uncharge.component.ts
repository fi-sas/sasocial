import { Component, OnInit } from "@angular/core";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BalanceModel } from "@fi-sas/backoffice/modules/financial/models/balance.model";
import { ChargeServiceModel } from "@fi-sas/backoffice/modules/financial/models/charge-service.model";
import { CashAccountsService } from "@fi-sas/backoffice/modules/financial/services/cash-accounts.service";
import { ChargeAccountsService } from "@fi-sas/backoffice/modules/financial/services/charge-account.service";
import { UnchargeAccountsService } from "@fi-sas/backoffice/modules/financial/services/uncharge-account.service";
import { NzMessageService, NzModalRef } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";

@Component({
    selector: 'fi-sas-modal-uncharge',
    templateUrl: './modal-uncharge.component.html',
    styleUrls: ['./modal-uncharge.component.less']
})

export class ModalUnChargeComponent implements OnInit {
    stepModal = 0;
    selectedAccount: BalanceModel;
    userId: number;
    formModal: any = {
        method: null,
        value: 0,
        description: '',
        errors: false
    };
    isConfirmLoading = false;

    constructor(private modal: NzModalRef, private message: NzMessageService, private cashAccountsService:CashAccountsService,
        private uiService:UiService, private unchargeAccountsService: UnchargeAccountsService) {

    }

    ngOnInit() {

    }

    getMoneyString(value: number, neutral: boolean) {
        let aux = Number(value.toString()).toFixed(2) + '€';
        aux = aux.split(".").join(",");
        if (value > 0 && !neutral) {
            aux = '+' + aux;
        }
        return aux;
    }

    verifyMoney(value: number, neutral: boolean) {
        if (!neutral) {
            if (value > 0) {
                return 'positive';
            } else if (value < 0) {
                return 'negative';
            }
        }
        return 'neutral';
    }

    handleCancel() {
        this.modal.close();
    }

    handleBack() {
        this.stepModal -= 1;
    }

    handleNext() {
        const valid = this.checkModalFormValid();

        if (valid) {
            this.stepModal += 1;
        }
    }

    checkModalFormValid() {
        this.formModal.errors = false;
        if (this.formModal.value === 0 || !this.formModal.value) {
            this.formModal.errors = true;
        } else if (this.formModal.value > this.selectedAccount.current_balance
          ) {
            this.formModal.errors = true;
            this.message.create('error', 'Montante indisponível para devolução');
          }
        if (this.formModal.method === null || !this.formModal.method) {
            this.formModal.errors = true;
        }
        if (this.formModal.errors) {
            return false;
        }
        return true;
    }

    unchargeAccounts() {
        const selectedCashAccount = this.cashAccountsService.selectedCashAccount();
        if (!selectedCashAccount) {
          this.uiService.showMessage(MessageType.error, "Obrigatório selecionar a caixa utilizada na transação");
          return;
        }
        this.isConfirmLoading = true;
        let uncharge: ChargeServiceModel = new ChargeServiceModel;
        uncharge.payment_method_id = this.formModal.method.id;
        uncharge.account_id = this.selectedAccount.account_id;
        uncharge.user_id = this.userId;
        uncharge.transaction_value = this.formModal.value;
        uncharge.cash_account_id = selectedCashAccount.id;
        if (this.formModal.description && this.formModal.description !== '') {
          uncharge.description = this.formModal.description;
        }
        this.unchargeAccountsService.create(uncharge)
          .pipe(
            first(),
            finalize(() => {
              this.isConfirmLoading = false;
            }))
          .subscribe(result => {
            this.stepModal += 1;
          });

      }

    confirm(){
        this.modal.close(true);
    }
}