import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { CashAccountsService } from "../../services/cash-accounts.service";
import { HistoryService } from "../../services/history.service";
import { MovimentsService } from "../../services/moviments.service";
import { NzDrawerService, NzMessageService, NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { listUsersCurrentAccountComponent } from "./component/list-user/list-user.component";
import { CurrentAccountsService } from "../../services/current-accounts.service";
import { ModalChargeComponent } from "./component/modal-charge/modal-charge.component";
import { ModalUnChargeComponent } from "./component/modal-uncharge/modal-uncharge.component";
import { BalanceModel } from "../../models/balance.model";
import { ArrayData } from "../../models/movements.model";
import { CashAccountModel } from "../../models/cash-account.model";
import { DatePipe } from "@angular/common";

@Component({
    selector: 'fi-sas-current-account',
    templateUrl: './current-account.component.html',
    styleUrls: ['./current-account.component.less']
})

export class CurrentAccountComponent extends TableHelper implements OnInit {
    loadingReport = false;
    accountsUserList: BalanceModel[] = [];
    selectedUser: UserModel = new UserModel();
    selectedAccount: BalanceModel;
    filter_createDate = null;
    filterList: ArrayData = new ArrayData();
    showHistory = false;
    selectedCashAccount = null;
    cashAccounts: CashAccountModel[] = [];
    loadingInfo = false;
    tabIndex = 0;
    constructor(uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute, private cashAccountsService: CashAccountsService, private historyService: HistoryService,
        private currentAccountsService: CurrentAccountsService, private message: NzMessageService, private modalService: NzModalService,
        private movimentsService: MovimentsService, private drawerService: NzDrawerService,public datepipe: DatePipe) {
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            withRelated: 'payment_method,document,items,device,payment',
            sort: '-created_at'
        };
    }

    ngOnInit() {
        this.selectedCashAccount = this.cashAccountsService.selectedCashAccount();
        if (this.selectedCashAccount) {
            this.cashAccountsService.getUserAvailableCashAccounts(
            ).subscribe(response => {
                this.cashAccounts = response.data;
            });
        }
    }

    showHistoryTable() {
        this.filters.user_id = this.selectedUser.id
        if (!this.showHistory) {
            this.persistentFilters['account_id'] = this.selectedAccount.account_id;
            this.initTableData(this.movimentsService);
        } else {
            this.persistentFilters['account_id'] = this.selectedAccount.account_id;
            this.initTableData(this.historyService);
        }
    }

    modalSearch() {
        this.loadingInfo = true;
        const modalDrawer = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
            nzContent: listUsersCurrentAccountComponent,
            nzWidth: (window.innerWidth / 24) * 12
         
        });
        modalDrawer.afterClose.pipe(first(),finalize(()=>this.loadingInfo =false)).subscribe((result) => {
            if (result) {
                this.selectedUser = result;
                this.accountsUserList = [];
                this.getAccounts(this.selectedUser.id);
            }
        });
    }

    updateFilters() {
        if (this.filter_createDate) {
            if(this.filter_createDate.length>0) {
                const temp = this.filter_createDate;
                this.filters['created_at'] = {};
                this.filters['created_at']['gte'] = this.datepipe.transform(temp[0], 'yyyy-MM-dd');
                this.filters['created_at']['lte'] = this.datepipe.transform(temp[1], 'yyyy-MM-dd');
            }else{
                this.filters['created_at'] = {};
                this.filter_createDate = null;
            }
            
        }
        this.searchData(true);
    }

    listComplete() {
        this.filters.status = null;
        this.filters.created_at = null;
        this.filters.operation = null;
        this.filter_createDate = null;
        this.searchData(true)
    }

    verifyMoney(value: number, neutral: boolean) {
        if (!neutral) {
            if (value > 0) {
                return 'positive';
            } else if (value < 0) {
                return 'negative';
            }
        }
        return 'neutral';
    }

    getMoneyString(value: number, neutral: boolean) {
        let aux = Number(value.toString()).toFixed(2) + '€';
        aux = aux.split(".").join(",");
        if (value > 0 && !neutral) {
            aux = '+' + aux;
        }
        return aux;
    }

    getPositiveNumber(value: number) {
        if (value < 0) {
            let aux = value.toString();
            let auxNumberPositive = aux.split('-')[1];
            return Number(auxNumberPositive);
        } else {
            return value;
        }
    }

    getErpErrors(errors) {
        return JSON.stringify(errors);
    }

    getAccounts(id: number) {
        this.currentAccountsService.getAccounts(id)
            .pipe(first())
            .subscribe(result => {
                this.accountsUserList = result.data;
                this.tabIndex = 0;
                if (this.accountsUserList.length > 0) {
                    this.selectedAccount = this.accountsUserList[0];
                    if (this.selectedAccount.account_id) {
                        this.showHistoryTable();
                    }
                }
            });
    }

    selectAccount(account, idx: number) {
        this.tabIndex = idx;
        this.selectedAccount = account;
        this.showHistoryTable();
    }

    showChargeModal() {
        const modalCharge = this.modalService.create({
            nzTitle: null,
            nzContent: ModalChargeComponent,
            nzFooter: null,
            nzWidth: 600,
            nzWrapClassName: 'vertical-center-modal',
            nzClosable: false,
            nzComponentParams: {
                selectedAccount: this.selectedAccount,
                userId: this.selectedUser.id
            },
        });
        modalCharge.afterClose.pipe(first()).subscribe((result) => {
            if(result){
                this.uiService.showMessage(MessageType.success, "Carregamento efetuado com sucesso");
                this.getAccounts(this.selectedUser.id);
            }
        });
    }

    showDevolutionModal() {
        const modalUnCharge = this.modalService.create({
            nzTitle: null,
            nzContent: ModalUnChargeComponent,
            nzFooter: null,
            nzWidth: 600,
            nzClosable: false,
            nzWrapClassName: 'vertical-center-modal',
            nzComponentParams: {
                selectedAccount: this.selectedAccount,
                userId: this.selectedUser.id
            },
        });
        modalUnCharge.afterClose.pipe(first()).subscribe((result) => {
            if(result){
                this.uiService.showMessage(MessageType.success, "Devolução efetuada com sucesso");
                this.getAccounts(this.selectedUser.id);
            }
        });
    }

    generateReport() {
        this.loadingReport = true;
        this.currentAccountsService.reportMovements(
            this.selectedAccount.account_id,
            this.selectedUser.id,
            this.filters.status,
            this.filters.operation,
            this.filter_createDate ? this.datepipe.transform(this.filter_createDate[0], 'yyyy-MM-dd') : '',
            this.filter_createDate ? this.datepipe.transform(this.filter_createDate[1], 'yyyy-MM-dd') : '',
            this.showHistory
        )
            .pipe(
                first(),
                finalize(() => this.loadingReport = false)
            ).subscribe(
                (report) => {
                    this.message.create('success', 'Relatório criado com sucesso');
                },
                (error) => {
                }
            );

    }

}