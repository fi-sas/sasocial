import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CurrentAccountRoutingModule } from './current-account-routing.module';
import { CurrentAccountComponent } from './current-account.component';
import { listUsersCurrentAccountComponent } from './component/list-user/list-user.component';
import { ModalChargeComponent } from './component/modal-charge/modal-charge.component';
import { ModalUnChargeComponent } from './component/modal-uncharge/modal-uncharge.component';

@NgModule({
  declarations: [
    CurrentAccountComponent,
    listUsersCurrentAccountComponent,
    ModalChargeComponent,
    ModalUnChargeComponent
  ],
  imports: [
    CommonModule,
    CurrentAccountRoutingModule,
    SharedModule
  ],
  entryComponents: [
    listUsersCurrentAccountComponent,
    ModalChargeComponent,
    ModalUnChargeComponent
  ]
})
export class CurrentAccountModule { }
