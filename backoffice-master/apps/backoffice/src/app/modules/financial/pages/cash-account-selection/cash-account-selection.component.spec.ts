import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashAccountSelectionComponent } from './cash-account-selection.component';

describe('CashAccountSelectionComponent', () => {
  let component: CashAccountSelectionComponent;
  let fixture: ComponentFixture<CashAccountSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashAccountSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashAccountSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
