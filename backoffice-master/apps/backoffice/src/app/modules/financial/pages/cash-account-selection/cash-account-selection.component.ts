import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { CashAccountModel } from '../../models/cash-account.model';
import { CashAccountsService } from '../../services/cash-accounts.service';

@Component({
  selector: 'fi-sas-cash-account-selection',
  templateUrl: './cash-account-selection.component.html',
  styleUrls: ['./cash-account-selection.component.less']
})
export class CashAccountSelectionComponent implements OnInit {


  loading = true;
  cashAccounts: CashAccountModel[] = [];
  selectedCashAccountId: number = null;

  constructor(
    private cashAccountsService: CashAccountsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cashAccountsService.getUserAvailableCashAccounts().pipe(
      finalize(() => this.loading = false)
    ).subscribe(response => {
      this.cashAccounts = response.data;
      if (this.cashAccountsService.selectedCashAccount()) {
        this.selectedCashAccountId = this.cashAccountsService.selectedCashAccount().id;
      }
    });
  }

  continueButton() {
    const cashAccount = this.cashAccounts.find(x => x.id == this.selectedCashAccountId);
    if (cashAccount) {
      this.cashAccountsService.selectCashAccount(cashAccount);
      this.router.navigate(['/financial', 'dashboard'])
    }
  }
}
