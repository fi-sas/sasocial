import { TestBed } from '@angular/core/testing';
import { AccountsPaymentMethodsService } from './accounts_payments_methods.service';


describe('AccountsPaymentMethodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountsPaymentMethodsService = TestBed.get(AccountsPaymentMethodsService);
    expect(service).toBeTruthy();
  });
});
