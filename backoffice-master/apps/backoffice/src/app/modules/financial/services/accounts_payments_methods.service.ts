import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { PaymentMethodSettingsModel } from '../models/payment_method_settings.model';

@Injectable({
  providedIn: 'root'
})
export class AccountsPaymentMethodsService extends Repository<PaymentMethodSettingsModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.ACCOUNTS_PAYMENTS_METHODS';
    this.entity_url = 'CURRENT_ACCOUNTS.ACCOUNTS_PAYMENTS_METHODS_ID';
  }

  listByAccountId(id_account: number) {
    let params = new HttpParams();
    params = params.set('query[account_id]', id_account.toString());
    return this.resourceService.list<PaymentMethodSettingsModel>(this.urlService.get('CURRENT_ACCOUNTS.ACCOUNTS_PAYMENTS_METHODS', {}), { params });
  }

}
