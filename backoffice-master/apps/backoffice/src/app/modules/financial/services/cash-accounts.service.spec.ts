import { TestBed } from '@angular/core/testing';

import { CashAccountsService } from './cash-accounts.service';

describe('CashAccountsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashAccountsService = TestBed.get(CashAccountsService);
    expect(service).toBeTruthy();
  });
});
