import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable, Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { CashAccountModel } from '../models/cash-account.model';
import { MovementModel } from '../models/movement.model';

@Injectable({
  providedIn: 'root'
})
export class CashAccountsService extends Repository<CashAccountModel>  {

  entity_url = 'CURRENT_ACCOUNTS.CASH_ACCOUNTS_ID';
  entities_url = 'CURRENT_ACCOUNTS.CASH_ACCOUNTS';
  private _cashAccountsData: Subject<CashAccountModel> = new Subject<CashAccountModel>();


  constructor(resourceService: FiResourceService, urlService: FiUrlService, private authService: AuthService) {
    super(resourceService, urlService);
  }


  cashAccountClose(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    params?: {}
  ): Observable<Resource<MovementModel>> {
    return this.resourceService
      .list<MovementModel>(
        this.urlService.get('CURRENT_ACCOUNTS.CASH_ACCOUNT_CLOSE', { ...this.persistentUrlParams }),
        {
          params: this.getQuery(
            pageIndex,
            pageSize,
            sortKey,
            sortValue,
            params
          ),
          headers: this.getHeaders(),
        }
      )
      .pipe(first());
  }

  selectCashAccount(cashAccount: CashAccountModel) {
    localStorage.setItem('financial_cash_Account' + '_' + this.authService.getUser().id.toString(), JSON.stringify(cashAccount));
    this.emitCashAccountsData();
  }

  selectedCashAccount() {
    return JSON.parse(localStorage.getItem('financial_cash_Account' + '_' + this.authService.getUser().id.toString())) as CashAccountModel;
  }

  getUserAvailableCashAccounts(): Observable<Resource<CashAccountModel>> {
    const url = this.urlService.get('CURRENT_ACCOUNTS.CASH_ACCOUNTS_AVAILABLE_ME');
    return this.resourceService.list<CashAccountModel>(url);
  }

  emitCashAccountsData() {
    this._cashAccountsData.next(this.selectedCashAccount());
  }

  selectedCashAccountsDataObservable() {
    return this._cashAccountsData.asObservable();
  }

  teste() {
    return this.getUserAvailableCashAccounts
  }

  getDefaultSelectedCashAccount() {

    let cash;
    const selected = this.selectedCashAccount();
    this.getUserAvailableCashAccounts().pipe(first()).subscribe(data => {
      cash = data.data;
      if(cash.length>0) {
        if (selected) {
          this.selectCashAccount(selected);
        } else {
          this.getUserAvailableCashAccounts().pipe(first()).subscribe(response => {
            if (response.data.length == 1) {
              this.selectCashAccount(response.data[0]);
            }
          });
        }
      }else {
        localStorage.removeItem('financial_cash_Account' + '_' + this.authService.getUser().id.toString());
      }

    })

  }

}
