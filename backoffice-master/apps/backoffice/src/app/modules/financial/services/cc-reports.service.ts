import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { MovementsModel } from '../models/movements.model';
@Injectable({
  providedIn: 'root',
})
export class CCReportsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  reportCashAccountClose(
    cash_account_id,
    startDate: string,
    endDate: string,
    date: string
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();
    params = params.set('query[cash_account_id]', cash_account_id);
    params = params.set('sort', '-created_at');
    if (startDate && endDate) {
      params = params.set(
        'query[created_at][gte]',
        moment(startDate).format('YYYY-MM-DD')
      );
      params = params.set(
        'query[created_at][lte]',
        moment(endDate).format('YYYY-MM-DD')
      );
    } else {
      params = params.set(
        'query[created_at]',
        moment(date).format('YYYY-MM-DD')
      );
    }
    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.CASH_ACCOUNT_CLOSE_REPORT'),
      {
        params,
      }
    );
  }

  reportChargesPeriod(
    startDate: string,
    endDate: string,
    account_id: number
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();
    params = params.set('sort', '-created_at');

    params = params.append('account_id', account_id.toString());

    params = params.set('start_date', moment(startDate).format('YYYY-MM-DD'));
    params = params.set('end_date', moment(endDate).format('YYYY-MM-DD'));

    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.CHARGES_PERIOD_REPORT'),
      {
        params,
      }
    );
  }

  reportChargesDetailedPeriod(query: {
    startDate: Date;
    endDate: Date;
    payment_method_id: number;
    device_ids: number[];
    user_id: number;
    account_id: number;
  }): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();

    params = params.append('account_id', query.account_id.toString());

    params = params.set(
      'start_date',
      moment(query.startDate).format('YYYY-MM-DD')
    );
    params = params.set('end_date', moment(query.endDate).format('YYYY-MM-DD'));

    if (query.payment_method_id) {
      params = params.append(
        'payment_method_id',
        query.payment_method_id.toString()
      );
    }

    if (query.device_ids) {
      query.device_ids.forEach((device_id) => {
        params = params.append('device_ids[]', device_id.toString());
      });
    }

    if (query.user_id) {
      params = params.append('user_id', query.user_id.toString());
    }

    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.CHARGES_DETAILED_PERIOD_REPORT'),
      {
        params,
      }
    );
  }

  reportMovementsPeriod(
    startDate: string,
    endDate: string,
    operations: string[],
    user_id: number,
    account_id: number
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();

    params = params.append('account_id', account_id.toString());

    if (operations) {
      operations.forEach((operation) => {
        params = params.append('operations[]', operation);
      });
    }

    if (user_id) {
      params = params.append('user_id', user_id.toString());
    }

    params = params.set('start_date', moment(startDate).format('YYYY-MM-DD'));
    params = params.set('end_date', moment(endDate).format('YYYY-MM-DD'));

    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_PERIOD_REPORT'),
      {
        params,
      }
    );
  }

  reportSalesResumePeriod(
    startDate: string,
    endDate: string,
    account_id: number
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();

    params = params.set('sort', '-created_at');

    params = params.append('account_id', account_id.toString());

    params = params.set('start_date', moment(startDate).format('YYYY-MM-DD'));
    params = params.set('end_date', moment(endDate).format('YYYY-MM-DD'));

    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.SALES_PERIOD_RESUME_REPORT'),
      {
        params,
      }
    );
  }

  reportSalesDetailedPeriod(query: {
    account_id: number;
    startDate: string;
    endDate: string;
    operations: string[];
    device_ids: number[];
    service_id: number;
    user_id: number;
  }): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();

    params = params.set('sort', '-created_at');

    params = params.append('account_id', query.account_id.toString());

    params = params.set(
      'start_date',
      moment(query.startDate).format('YYYY-MM-DD')
    );
    params = params.set('end_date', moment(query.endDate).format('YYYY-MM-DD'));

    if (query.operations) {
      query.operations.forEach((operation) => {
        params = params.append('operations[]', operation);
      });
    }

    if (query.device_ids) {
      query.device_ids.forEach((device_id) => {
        params = params.append('device_ids[]', device_id.toString());
      });
    }

    if (query.service_id) {
      params = params.append('service_id', query.service_id.toString());
    }

    if (query.user_id) {
      params = params.append('user_id', query.user_id.toString());
    }

    return this.resourceService.list<MovementsModel>(
      this.urlService.get('CURRENT_ACCOUNTS.SALES_PERIOD_DETAILED_REPORT'),
      {
        params,
      }
    );
  }
}
