import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ChargeServiceModel } from '../models/charge-service.model';

@Injectable({
  providedIn: 'root'
})
export class ChargeAccountsService extends Repository<ChargeServiceModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.CHARGE_ACCOUNT';
  }
}
