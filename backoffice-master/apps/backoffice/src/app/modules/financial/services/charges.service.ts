import { Observable } from 'rxjs';

import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { ChargeModel } from '../models/charge.model';
import { ChargeAccountModel } from '../models/charge-account.model';

@Injectable({
  providedIn: 'root'
})
export class ChargesService extends Repository<ChargeModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PAYMENTS.CHARGES';
    this.entity_url = 'PAYMENTS.CHARGES_ID';
  }

  chargeAccount(chargeAccount: ChargeAccountModel): Observable<Resource<ChargeModel>> {
    const url = this.urlService.get('PAYMENTS.CHARGES_CHARGE');
    return this.resourceService.create(url, chargeAccount);
  }
}
