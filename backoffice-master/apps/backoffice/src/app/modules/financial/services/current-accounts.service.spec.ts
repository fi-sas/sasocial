import { TestBed } from '@angular/core/testing';

import { CurrentAccountsService } from './current-accounts.service';

describe('CurrentAccountsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CurrentAccountsService = TestBed.get(CurrentAccountsService);
    expect(service).toBeTruthy();
  });
});
