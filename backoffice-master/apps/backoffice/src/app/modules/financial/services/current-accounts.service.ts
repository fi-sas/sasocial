import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AccountModel } from '../models/account.model';
import { BalanceModel } from '../models/balance.model';
import { MovementsModel } from '../models/movements.model';

@Injectable({
  providedIn: 'root',
})
export class CurrentAccountsService extends Repository<AccountModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'CURRENT_ACCOUNTS.ACCOUNTS_ID';
    this.entities_url = 'CURRENT_ACCOUNTS.ACCOUNTS';
  }

  treePaymentMethods(account_id: number): Observable<Resource<any>> {
    return this.resourceService
      .list<any>(
        this.urlService.get("CURRENT_ACCOUNTS.ACCOUNTS_ID_PAYMENT_METHODS", { account_id }),
        {}
      )
      .pipe(first());
  }

  updatePaymentMethod(account_id: number,device_id: number,payment_method_id: string,active: boolean): Observable<Resource<any>> {
    return this.resourceService
      .update<any>(
        this.urlService.get("CURRENT_ACCOUNTS.ACCOUNTS_ID_PAYMENT_METHODS", { account_id }),
        {
          account_id,
          device_id,
          payment_method_id,
          active
        }
      )
      .pipe(first());
  }

  getAccounts(
    user_id: number): Observable<Resource<BalanceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[user_id]', user_id.toString());
    params = params.set('withRelated', "available_methods");

    return this.resourceService.list<BalanceModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_BALANCES'), {
      params
    });
  }

  listMovements(
    pageIndex: number,
    pageSize: number,
    accountId: number,
    userId: number,
    statusFilter?: string,
    operationFilter?: string,
    minDate?: string,
    maxDate?: string,
    history?: boolean
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();
    params = params.set('offset', pageIndex.toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('query[user_id]', userId.toString());
    params = params.set('query[account_id]', accountId.toString());
    params = params.set('sort', '-created_at');
    if (statusFilter && statusFilter !== ''){
      params = params.set('query[status]', statusFilter);
    }
    if (operationFilter && operationFilter !== ''){
      params = params.set('query[operation]', operationFilter);
    }
    if (maxDate && maxDate !== ''){
      params = params.set('query[created_at][lte]', maxDate);
    }
    if (minDate && minDate !== ''){
      params = params.set('query[created_at][gte]', minDate);
    }

    if(history){
      return this.resourceService.list<MovementsModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_HISTORY'), {
        params
      });
    } else {
      return this.resourceService.list<MovementsModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS'), {
        params
      });
    }
  }


  reportMovements(
    accountId: number,
    userId: number,
    statusFilter?: string,
    operationFilter?: string,
    minDate?: string,
    maxDate?: string,
    history?: boolean
  ): Observable<Resource<MovementsModel>> {
    let params = new HttpParams();
    params = params.set('query[user_id]', userId.toString());
    params = params.set('query[account_id]', accountId.toString());
    params = params.set('sort', '-created_at');
    if (statusFilter && statusFilter !== ''){
      params = params.set('query[status]', statusFilter);
    }
    if (operationFilter && operationFilter !== ''){
      params = params.set('query[operation]', operationFilter);
    }
    if (maxDate && maxDate !== ''){
      params = params.set('query[created_at][lte]', maxDate);
    }
    if (minDate && minDate !== ''){
      params = params.set('query[created_at][gte]', minDate);
    }

    if(history){
      return this.resourceService.list<MovementsModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_HISTORY_REPORT'), {
        params
      });
    } else {
      return this.resourceService.list<MovementsModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_USER_REPORT'), {
        params
      });
    }
  }
}
