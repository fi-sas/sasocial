import { TestBed } from '@angular/core/testing';

import { DefaultItemConfigurationService } from './default-item-configuration.service';

describe('DefaultItemConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DefaultItemConfigurationService = TestBed.get(DefaultItemConfigurationService);
    expect(service).toBeTruthy();
  });
});
