import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { DefaultItemConfigurationModel } from '../models/default-item-configuration.model';

@Injectable({
  providedIn: 'root'
})
export class DefaultItemConfigurationService extends Repository<DefaultItemConfigurationModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.DEFAULT_ITEM_CONFIGURATION';
    this.entity_url = 'CURRENT_ACCOUNTS.DEFAULT_ITEM_CONFIGURATION_ID';
  }
}
