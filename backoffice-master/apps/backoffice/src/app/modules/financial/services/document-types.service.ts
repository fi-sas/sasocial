import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { DocumentTypeModel } from '../models/document-type.model';


@Injectable({
  providedIn: 'root'
})
export class DocumentTypesService extends Repository<DocumentTypeModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.DOCUMENTS';
    this.entity_url = 'CURRENT_ACCOUNTS.DOCUMENTS_ID';
  }

  filterByDocumentTypeId(ids: number[]): Observable<any> {
    let params = new HttpParams();
    for (const id of ids) {
      params = params.append('query[id]', id + '');
    }
    return this.resourceService
      .list<DocumentTypeModel[]>(this.urlService.get('CURRENT_ACCOUNTS.DOCUMENTS'), { params });
  }
}