import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { DocumentsModule } from '../models/documents.model';


@Injectable({
  providedIn: 'root'
})
export class DocumentsServiceQueue extends Repository<DocumentsModule>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.DOCUMENTS';
    this.entity_url = 'CURRENT_ACCOUNTS.DOCUMENTS_ID';
  }
}
