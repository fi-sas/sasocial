import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { UsersService } from '../../users/modules/users_users/services/users.service';
import { MovementModel } from '../models/movement.model';

@Injectable({
  providedIn: 'root'
})
export class MovementsService extends Repository<MovementModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService, private userService: UsersService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.MOVEMENTS';
    this.entity_url = 'CURRENT_ACCOUNTS.MOVEMENTS_ID';
  }

  listWithUsers(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string, params?: {}): Observable<Resource<MovementModel>> {
    return this.list(pageIndex, pageSize, sortKey, sortValue, params).pipe(first(), tap(result => {
      const user_ids = result.data.map(m => m.user_id);
      this.userService.list(1, -1, null ,null, { id: user_ids })
        .pipe(first()).subscribe(users => {
        result.data.map(m => {
          m.user = users.data.find(u => u.id === m.user_id);
        });
      });
    }));
  }

  getUserBalances(user_id: number): Observable<Resource<any>> {
    const url = this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS_BALANCES');
    const params = this.getQuery(null,null,null,null,{
      user_id
    });
    return this.resourceService.list<any>(url, { params }).pipe(first());
  }

  listMovements(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    statusFilter?: string,
    minDate?: string,
    maxDate?: string,
    operation?: string,
    status?: string,
    serviceId?: number,
    accountId?: number,
    userId?: number,
    tin?: number
  ):Observable<Resource<MovementModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('status', statusFilter.toString());

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    } else {
      params = params.set('sort', '-id');
    }

    params = params.set('withRelated', 'account,documents,items,paid_by,paid_by_pending,user');

    if (minDate !== null) {
      params = params.set('min_date', minDate);
    }

    if (maxDate !== null) {
      params = params.set('max_date', maxDate);
    }

    if (operation !== null) {
      params = params.set('operation', operation);
    }

    if (status !== null) {
      params = params.set('status', status);
    }

    if (serviceId !== null) {
      params = params.set('service_id', serviceId.toString());
    }

    if (accountId !== null) {
      params = params.set('account_id', accountId.toString());
    }

    if (userId !== null) {
      params = params.set('user_id', userId.toString());
    }

    if (tin !== null) {
      params = params.set('tin', tin.toString());
    }

    return this.resourceService.list<MovementModel>(this.urlService.get('CURRENT_ACCOUNTS.MOVEMENTS'), {
      params
    });
  }

}
