
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovimentsService extends Repository<any> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.MOVEMENTS';
  }
}
