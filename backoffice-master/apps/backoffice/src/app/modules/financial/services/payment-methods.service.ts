import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { PaymentMethodModel } from '../models/payment-method.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodsService extends Repository<PaymentMethodModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PAYMENTS.PAYMENTS_METHODS';
    this.entity_url = 'PAYMENTS.PAYMENTS_METHODS_ID';
  }
}
