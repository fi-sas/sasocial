import { HttpParams } from '@angular/common/http';
import { PayMovementsModel } from './../models/pay-movements.model';
import { Repository } from '../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { PaymentModel } from '../models/payment.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService extends Repository<PaymentModel> {
  constructor(
    resourceService: FiResourceService,
     urlService: FiUrlService
     ) {
    super(resourceService, urlService);
    this.entities_url = 'PAYMENTS.PAYMENTS';
    this.entity_url = 'PAYMENTS.PAYMENTS_ID';
  }

  payMovements(
    user_id: number,
    payMovements: PayMovementsModel
  ): Observable<Resource<PaymentModel[]>> {
    const url = this.urlService.get('PAYMENTS.PAYMENTS_PAY_MOVEMENTS');
    const params = new HttpParams().append('user_id', user_id.toLocaleString());
    return this.resourceService.create(url, payMovements, { params });
  }

  confirm(id: number): Observable<Resource<PaymentModel>> {
    const url = this.urlService.get('PAYMENTS.PAYMENTS_CONFIRM', { id });
    return this.resourceService.create(url, {});
  }

  cancel(id: number): Observable<Resource<PaymentModel>> {
    const url = this.urlService.get('PAYMENTS.PAYMENTS_CANCEL', { id });
    return this.resourceService.create(url, {});
  }
}
