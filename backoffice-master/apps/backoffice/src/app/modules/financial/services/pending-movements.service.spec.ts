import { TestBed } from '@angular/core/testing';

import { PendingMovementsService } from './pending-movements.service';

describe('PendingMovementsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingMovementsService = TestBed.get(PendingMovementsService);
    expect(service).toBeTruthy();
  });
});
