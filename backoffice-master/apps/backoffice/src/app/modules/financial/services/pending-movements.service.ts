import { first, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Repository } from './../../../shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { UsersService } from '../../users/modules/users_users/services/users.service';
import { MovementModel } from '../models/movement.model';

@Injectable({
  providedIn: 'root'
})
export class PendingMovementsService extends Repository<MovementModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService,
    private userService: UsersService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.PENDING_MOVEMENTS';
    this.entity_url = 'CURRENT_ACCOUNTS.CURRENT_PENDING_MOVEMENTS_ID';
  }

  list(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string, params?: {}): Observable<Resource<MovementModel>> {
    return this.resourceService.list<MovementModel>(this.urlService.get(this.entities_url), { params: this.getQuery(pageIndex,pageSize,sortKey,sortValue,params) })
    .pipe(first(), tap(result => {
      const user_ids = result.data.map(m => m.user_id);
      this.userService.list(1, -1, null ,null, { id: user_ids })
        .pipe(first()).subscribe(users => {
        result.data.map(m => {
          m.user = users.data.find(u => u.id === m.user_id);
        });
      });
    }));
  }

  confirm(id: number): Observable<Resource<MovementModel>> {
    const url = this.urlService.get('CURRENT_ACCOUNTS.PENDING_MOVEMENTS_ID_CONFIRM', { id });
    return this.resourceService.create(url, {});
  }

  cancel(id: number): Observable<Resource<MovementModel>> {
    const url = this.urlService.get('CURRENT_ACCOUNTS.PENDING_MOVEMENTS_ID_CANCEL', { id });
    return this.resourceService.create(url, {});
  }
}
