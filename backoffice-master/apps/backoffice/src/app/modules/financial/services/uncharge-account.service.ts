import { Observable } from 'rxjs';
import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { ChargeServiceModel } from '../models/charge-service.model';

@Injectable({
  providedIn: 'root'
})
export class UnchargeAccountsService extends Repository<ChargeServiceModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CURRENT_ACCOUNTS.UNCHARGE_ACCOUNT';
  }
}
