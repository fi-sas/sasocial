import { Observable } from 'rxjs';
import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { Injectable } from '@angular/core';
import { UnchargeAccountModel } from '../models/uncharge-account.model';
import { UnchargeModel } from '../models/uncharge.model';

@Injectable({
  providedIn: 'root'
})
export class UnchargesService extends Repository<UnchargeModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PAYMENTS.UNCHARGES';
    this.entity_url = 'PAYMENTS.UNCHARGES_ID';
  }

  unchargeAccount(unchargeAccount: UnchargeAccountModel): Observable<Resource<UnchargeModel>> {
    const url = this.urlService.get('PAYMENTS.UNCHARGES_UNCHARGE');
    return this.resourceService.create(url, unchargeAccount);
  }
}
