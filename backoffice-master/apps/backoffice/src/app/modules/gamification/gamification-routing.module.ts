import { RouterModule, Routes } from "@angular/router";
import { GamificationComponent } from "@fi-sas/backoffice/modules/gamification/gamification.component";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { NgModule } from "@angular/core";
import { FormChallengeTypeComponent } from "@fi-sas/backoffice/modules/gamification/pages/form-challenge-type/form-challenge-type.component";
import { ListChallengesTypesComponent } from "@fi-sas/backoffice/modules/gamification/pages/list-challenges-types/list-challenges-types.component";
import { ListTeamsComponent } from "@fi-sas/backoffice/modules/gamification/pages/list-teams/list-teams.component";
import { ListPlayersComponent } from "@fi-sas/backoffice/modules/gamification/pages/list-players/list-players.component";
import { FormChallengeComponent } from "@fi-sas/backoffice/modules/gamification/pages/form-challenge/form-challenge.component";
import { ListChallengesComponent } from "@fi-sas/backoffice/modules/gamification/pages/list-challenges/list-challenges.component";
import { FormCompetitionComponent } from "@fi-sas/backoffice/modules/gamification/pages/form-competition/form-competition.component";
import { ListCompetitionsComponent } from "@fi-sas/backoffice/modules/gamification/pages/list-competitions/list-competitions.component";
import { InitialPageComponent } from "@fi-sas/backoffice/shared/components/initial-page/initial-page.component";

const routes: Routes = [
  {
    path: '',
    component: GamificationComponent,
    children: [

      {
        path: 'form-challenge-type',
        component: FormChallengeTypeComponent,
        data: { breadcrumb: 'Criar Tipo de Desafio', title: 'Criar tipo de Desafio', scope: 'gamification:challenges-types:create' },
        resolve: { }
      },

      {
        path: 'list-challenges-types',
        component: ListChallengesTypesComponent,
        data: { breadcrumb: 'Listar Tipos de Desafios', title: 'Listar Tipos de Desafios', scope: 'gamification:challenges-types:read' },
        resolve: { }
      },

      {
        path: 'form-challenge',
        component: FormChallengeComponent,
        data: { breadcrumb: 'Criar Desafio', title: 'Criar Desafio', scope: 'gamification:challenges:create' },
        resolve: { }
      },

      {
        path: 'list-challenges',
        component: ListChallengesComponent,
        data: { breadcrumb: 'Listar Desafios', title: 'Listar Desafios', scope: 'gamification:challenges:read' },
        resolve: { }
      },

      {
        path: 'form-competition',
        component: FormCompetitionComponent,
        data: { breadcrumb: 'Criar Competição', title: 'Criar Competição', scope: 'gamification:competitions:create' },
        resolve: { }
      },

      {
        path: 'list-competitions',
        component: ListCompetitionsComponent,
        data: { breadcrumb: 'Listar Competições', title: 'Listar Competições', scope: 'gamification:competitions:read' },
        resolve: { }
      },

      {
        path: 'list-players',
        component: ListPlayersComponent,
        data: { breadcrumb: 'Listar Jogadores', title: 'Listar Jogadores', scope: 'gamification:players:read' },
        resolve: { }
      },

      {
        path: 'list-teams',
        component: ListTeamsComponent,
        data: { breadcrumb: 'Listar Equipas', title: 'Listar Equipas', scope: 'gamification:teams:read' },
        resolve: { }
      },

      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamificationRoutingModule {}
