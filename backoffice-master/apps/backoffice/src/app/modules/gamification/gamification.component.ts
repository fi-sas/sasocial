import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-gamification',
  template: '<router-outlet></router-outlet>'
})
export class GamificationComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(25), 'trophy');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const challenges = new SiderItem('Desafios', '', '', 'gamification:challenges');
    challenges.addChild(new SiderItem('Criar', '', '/gamification/form-challenge', 'gamification:challenges:create'));
    challenges.addChild(new SiderItem('Listar', '', '/gamification/list-challenges', 'gamification:challenges:read'));
    challenges.addChild(new SiderItem('Criar um tipo', '', '/gamification/form-challenge-type', 'gamification:challenges-types:create'));
    challenges.addChild(new SiderItem('Listar os tipos', '', '/gamification/list-challenges-types', 'gamification:challenges-types:read'));
    this.uiService.addSiderItem(challenges);

    const competitions = new SiderItem('Competições', '', '', 'gamification:competitions');
    competitions.addChild(new SiderItem('Criar', '', '/gamification/form-competition', 'gamification:competitions:create'));
    competitions.addChild(new SiderItem('Listar', '', '/gamification/list-competitions', 'gamification:competitions:read'));
    this.uiService.addSiderItem(competitions);


    const players = new SiderItem('Jogadores', '', '/gamification/list-players', 'gamification:players:read');
    this.uiService.addSiderItem(players);

    const teams = new SiderItem('Equipas', '', '/gamification/list-teams', 'gamification:teams:read');
    this.uiService.addSiderItem(teams);

    this.uiService.setSiderActive(true);
  }


  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
