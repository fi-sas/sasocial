import { GamificationModule } from "@fi-sas/backoffice/modules/gamification/gamification.module";

describe('GamificationModule', () => {
  let gamificationModule: GamificationModule;

  beforeEach(() => {
    gamificationModule = new GamificationModule();
  });

  it('should create an instance', () => {
    expect(gamificationModule).toBeTruthy();
  });
});
