import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamificationComponent } from './gamification.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { GamificationRoutingModule } from "@fi-sas/backoffice/modules/gamification/gamification-routing.module";
import { FormChallengeTypeComponent } from './pages/form-challenge-type/form-challenge-type.component';
import { ListChallengesTypesComponent } from './pages/list-challenges-types/list-challenges-types.component';
import { ListTeamsComponent } from './pages/list-teams/list-teams.component';
import { ViewTeamComponent } from './pages/view-team/view-team.component';
import { ListPlayersComponent } from './pages/list-players/list-players.component';
import { ViewPlayerComponent } from './pages/view-player/view-player.component';
import { FormChallengeComponent } from './pages/form-challenge/form-challenge.component';
import { ListChallengesComponent } from './pages/list-challenges/list-challenges.component';
import { FormCompetitionComponent } from './pages/form-competition/form-competition.component';
import { ListCompetitionsComponent } from './pages/list-competitions/list-competitions.component';

@NgModule({
  declarations: [
    GamificationComponent,
    FormChallengeTypeComponent,
    ListChallengesTypesComponent,
    ListTeamsComponent,
    ViewTeamComponent,
    ListPlayersComponent,
    ViewPlayerComponent,
    FormChallengeComponent,
    ListChallengesComponent,
    FormCompetitionComponent,
    ListCompetitionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GamificationRoutingModule
  ]
})
export class GamificationModule { }
