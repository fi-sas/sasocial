import { TranslationModel } from "@fi-sas/backoffice/modules/gamification/models/translation.model";
import { PlayerModel } from "@fi-sas/backoffice/modules/gamification/models/player.model";

export class ChallengeModel {
  id?: number;
  challenge_type_id: number;
  points: number;
  automated: boolean;
  tag: string;
  service_id: number;
  start_date: string;
  end_date: string;
  winners: number;
  updated_at?: string;
  created_at?: string;
  translations: TranslationModel[];
  players: PlayerModel[];
}

