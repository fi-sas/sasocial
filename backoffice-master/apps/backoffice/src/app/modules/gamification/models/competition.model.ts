import { TranslationModel } from "@fi-sas/backoffice/modules/gamification/models/translation.model";
import { ChallengeModel } from "@fi-sas/backoffice/modules/gamification/models/challenge.model";

export class CompetitionModel {
  id?: number;
  updated_at?: string;
  created_at?: string;
  translations?: TranslationModel[];
  challenges?: ChallengeModel[];
}

