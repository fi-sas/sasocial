import { TeamModel } from "@fi-sas/backoffice/modules/gamification/models/team.model";

export class PlayerModel {
  id?: number;
  nickname: string;
  user_id: number;
  points?: number;
  updated_at?: string;
  created_at?: string;
  team?: TeamModel[];
}

