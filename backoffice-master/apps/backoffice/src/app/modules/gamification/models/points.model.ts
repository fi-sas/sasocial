
export class PointsModel {
  challenge_id?: number;
  player_id: number;
  points?: number;
  updated_at?: string;
  created_at?: string;
}

