
export class RankingPlayers {
  player_id: number;
  score: number;
  position: number;
  nickname: string;
}

