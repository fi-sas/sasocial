
export class RankingTeams {
  team_id: number;
  score: number;
  nickname: string;
  position: number;
}

