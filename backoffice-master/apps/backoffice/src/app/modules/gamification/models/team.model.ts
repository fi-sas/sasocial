import { PlayerModel } from "@fi-sas/backoffice/modules/gamification/models/player.model";

export class TeamModel {
  id?: number;
  name: string;
  updated_at?: string;
  created_at?: string;
  players?: PlayerModel[]
}

