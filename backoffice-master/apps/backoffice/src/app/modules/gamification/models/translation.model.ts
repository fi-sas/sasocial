
export class TranslationModel {
  language_id: number;
  name: string;
  description?: string;
  rules?: string;
  team_prize?: string;
  player_prize?: string;
}

