import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormChallengeTypeComponent } from './form-challenge-type.component';

describe('FormChallengeTypeComponent', () => {
  let component: FormChallengeTypeComponent;
  let fixture: ComponentFixture<FormChallengeTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormChallengeTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormChallengeTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
