import { Component, Input, OnInit } from '@angular/core';
import { ChallengeTypeService } from "@fi-sas/backoffice/modules/gamification/services/challenge-type.service";
import { InputType } from "@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface";
import { Validators } from "@angular/forms";

@Component({
  selector: 'fi-sas-form-challenge-type',
  templateUrl: './form-challenge-type.component.html',
  styleUrls: ['./form-challenge-type.component.less']
})
export class FormChallengeTypeComponent implements OnInit {

  fields: any[] = [];
  @Input()
  isUpdate = false;

  constructor(public challengeTypeService :ChallengeTypeService) { }

  ngOnInit() {
    this.fields.push(
      {
        key: 'tag',
        type: InputType.input,
        templateOptions: {
          name: 'tag',
          label: 'Tag',
          validators: [Validators.required]
        },
        row: 1,
        span: 24
      },
      {
        key: 'description',
        type: InputType.input,
        templateOptions: {
          name: 'descrição',
          label: 'Descrição',
          validators: [Validators.required]
        },
        row: 2,
        span: 24
      }
    );
  }

}
