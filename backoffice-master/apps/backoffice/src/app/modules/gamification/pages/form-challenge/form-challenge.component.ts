import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { ChallengesService } from "@fi-sas/backoffice/modules/gamification/services/challenges.service";
import { InputType } from "@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface";
import { Validators } from "@angular/forms";
import { ServicesService } from "@fi-sas/backoffice/modules/configurations/services/services.service";
import { ChallengeTypeService } from "@fi-sas/backoffice/modules/gamification/services/challenge-type.service";
import { NzModalRef, NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { ChallengeModel } from "@fi-sas/backoffice/modules/gamification/models/challenge.model";
import { PointsModel } from "@fi-sas/backoffice/modules/gamification/models/points.model";
import { RankingTeams } from "@fi-sas/backoffice/modules/gamification/models/ranking-teams";
import { RankingPlayers } from "@fi-sas/backoffice/modules/gamification/models/ranking-players";

@Component({
  selector: 'fi-sas-form-challenge',
  templateUrl: './form-challenge.component.html',
  styleUrls: ['./form-challenge.component.less']
})
export class FormChallengeComponent implements OnInit {

  playersModal: NzModalRef;
  rankingModal: NzModalRef;
  challenge: ChallengeModel = null;
  fields: any[] = [];
  @Input()
  isUpdate = false;

  rankingTeams: RankingTeams[] = [];
  rankingPlayers: RankingPlayers[] = [];

  loading: Boolean = false;

  pageIndex = 1;
  pageSize = 10;
  total = 1;

  constructor(public challengeService: ChallengesService,
              private serviceService: ServicesService,
              private challengeTypeService: ChallengeTypeService,
              private modalService: NzModalService,
              private uiService: UiService,
              private listService: ListService) {
    this.listService.selectedItemObservable().subscribe(challenge => this.challenge = challenge);

  }

  createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>, size: number = 900): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size
    });
  }

  playersInChallenge(title: TemplateRef<{}>, content: TemplateRef<{}>, footer: TemplateRef<{}>, size: number) {
    this.playersModal = this.createTplModal(title, content, footer, size);
  }

  closePlayerModal() {
    this.playersModal.close();
  }

  removePlayer(challenge_id: number, player_id: number) {
    this.challengeService.removePlayer(challenge_id, player_id).pipe(first()).subscribe(() => {
      this.challenge.players.splice(
        this.challenge.players.indexOf(this.challenge.players.find(p => p.id === player_id)),
        1);
      this.listService.itemUpdated(this.challenge);
      this.uiService.showMessage(
        MessageType.success,
        'Removido com sucesso'
      );
      this.closePlayerModal();
    }, () => {
      this.uiService.showMessage(
        MessageType.success,
        'Erro ao Remover'
      )
    });
  }

  getTeamsRankings(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;
    this.challengeService.rankingsTeams(this.pageIndex,this.pageSize, this.challenge.id).pipe(first()).subscribe(teamRanking => {
         this.total = teamRanking.link.total;
         this.rankingTeams = teamRanking.data;
         this.rankingPlayers = [];
         this.loading = false;
    });
  }

  getPlayersRankings(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;
    this.challengeService.rankingsPlayers(this.pageIndex,this.pageSize, this.challenge.id).pipe(first()).subscribe(playerRanking => {
      this.total = playerRanking.link.total;
      this.rankingPlayers = playerRanking.data;
      this.rankingTeams = [];
      this.loading = false;
    });
  }

  rankings(title: TemplateRef<{}>, content: TemplateRef<{}>, footer: TemplateRef<{}>, size: number) {
    if (this.challenge !== null) {
      this.getTeamsRankings();
    }
    this.rankingModal = this.createTplModal(title, content, footer, size);
  }

  closeRankingModal() {
    this.rankingModal.close();
  }

  addPlayerPoints(challenge_id, player_id) {

    const playerPoints: PointsModel = {player_id: player_id};
    this.challengeService.addPlayerScore(playerPoints, challenge_id).pipe(first()).subscribe(points => {

      this.challenge.players[
        this.challenge.players.indexOf(
          this.challenge.players.find(p => p.id === player_id)
        )].points += points.data[0].points;

      this.uiService.showMessage(
        MessageType.success,
        'Pontos inseridos com sucesso'
      );
    }, () =>{
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao inserir pontos'
      );
    });
  }

  selectRankingTeam() {
    if (this.challenge !== null) {
      this.getTeamsRankings(true);
    }
  }

  selectRankingPlayer() {
    if (this.challenge !== null) {
      this.getPlayersRankings(true);
    }
  }

  ngOnInit() {

    this.fields.push(
      {
        key: 'translations',
        row: 1,
        type: InputType.multilanguage,
        fields: [{
          key: 'name',
          type: InputType.input,
          templateOptions: {
            name: 'nome',
            label: 'Name',
            validators: [Validators.required]
          },
          row: 1,
          span: 24
        },
        {
          key: 'description',
          type: InputType.textarea,
          templateOptions: {
            name: 'descrição',
            label: 'Descrição',
            validators: [Validators.required]
          },
          row: 1,
          span: 24
        },
        {
          key: 'rules',
          type: InputType.textarea,
          templateOptions: {
            name: 'Regras',
            label: 'Regras',
            validators: [Validators.required]
          },
          row: 1,
          span: 24
        },
        {
          key: 'team_prize',
          type: InputType.textarea,
          templateOptions: {
            name: 'prêmio de equipe',
            label: 'Prêmio de equipe',
            validators: [Validators.required]
          },
          row: 1,
          span: 24
        },
          {
            key: 'player_prize',
            type: InputType.textarea,
            templateOptions: {
              name: 'prêmio de jogador',
              label: 'Prêmio de jogador',
              validators: [Validators.required]
            },
            row: 1,
            span: 24
          }]
      },
      {
        key: 'service_id',
        model_name: 'service',
        type: InputType.select,
        templateOptions: {
          name: 'serviço',
          label: 'Serviço',
          repository: this.serviceService,
          selectLabelKey: 'translations',
          validators: [Validators.required]
        },
        row: 2,
        span: 8
      },
      {
        key: 'challenge_type_id',
        model_name: 'challenge_type',
        type: InputType.select,
        templateOptions: {
          name: 'tipo de desafio',
          label: 'Tipo de desafio',
          repository: this.challengeTypeService,
          selectLabelKey: 'tag',
          validators: [Validators.required]
        },
        row: 2,
        span: 8
      },
      {
        key: 'tag',
        type: InputType.input,
        templateOptions: {
          name: 'tag',
          label: 'Tag',
          validators: [Validators.required, Validators.pattern('([A-Z_0-9]*)\\b')]
        },
        row: 2,
        span: 8
      },
      {
        key: 'start_date',
        key2: 'end_date',
        type: InputType.daterange,
        templateOptions: {
          name: 'início e fim do desafio',
          label: 'Início e fim do desafio',
          validators: [Validators.required]
        },
        row: 3,
        span: 8
      },
      {
        key: 'points',
        type: InputType.number,
        defaultValue: 1,
        templateOptions: {
          name: 'pontos',
          label: 'Pontos',
          min: 1,
          step: 1,
          validators: [Validators.required]
        },
        row: 4,
        span: 3
      },
      {
        key: 'winners',
        type: InputType.number,
        defaultValue: 1,
        templateOptions: {
          name: 'nª de vencedores',
          label: 'Nª de vencedores',
          min: 1,
          step: 1,
          validators: [Validators.required]
        },
        row: 4,
        span: 3
      },
      {
        key: 'automated',
        type: InputType.switch,
        defaultValue: false,
        templateOptions: {
          name: 'criação automática',
          label: 'Criação automática',
          validators: [Validators.required]
        },
        row: 5,
        span: 3
      }
    );
  }

}
