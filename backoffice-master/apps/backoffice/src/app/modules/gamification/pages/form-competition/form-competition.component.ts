import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { NzModalRef, NzModalService } from "ng-zorro-antd";
import { CompetitionModel } from "@fi-sas/backoffice/modules/gamification/models/competition.model";
import { RankingTeams } from "@fi-sas/backoffice/modules/gamification/models/ranking-teams";
import { RankingPlayers } from "@fi-sas/backoffice/modules/gamification/models/ranking-players";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { CompetitionsService } from "@fi-sas/backoffice/modules/gamification/services/competitions.service";
import { ChallengesService } from "@fi-sas/backoffice/modules/gamification/services/challenges.service";
import { first } from "rxjs/operators";
import { InputType } from "@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface";
import { Validators } from "@angular/forms";


@Component({
  selector: 'fi-sas-form-competition',
  templateUrl: './form-competition.component.html',
  styleUrls: ['./form-competition.component.less']
})
export class FormCompetitionComponent implements OnInit {

  rankingModal: NzModalRef;
  competition: CompetitionModel = null;
  fields: any[] = [];
  @Input()
  isUpdate = false;

  rankingTeams: RankingTeams[] = [];
  rankingPlayers: RankingPlayers[] = [];

  loading: Boolean = false;

  pageIndex = 1;
  pageSize = 10;
  total = 1;


  constructor(public competitionService: CompetitionsService,
              private challengeService: ChallengesService,
              private modalService: NzModalService,
              private uiService: UiService,
              private listService: ListService) {
    this.listService.selectedItemObservable().subscribe(competition => this.competition = competition)
  }

  createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>, size: number = 900): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size
    });
  }

  getTeamsRankings(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;
    this.competitionService.rankingsTeams(this.pageIndex,this.pageSize, this.competition.id).pipe(first()).subscribe(teamRanking => {
      this.total = teamRanking.link.total;
      this.rankingTeams = teamRanking.data;
      this.rankingPlayers = [];
      this.loading = false;
    });
  }

  getPlayersRankings(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;
    this.competitionService.rankingsPlayers(this.pageIndex,this.pageSize, this.competition.id).pipe(first()).subscribe(playerRanking => {
      this.total = playerRanking.link.total;
      this.rankingPlayers = playerRanking.data;
      this.rankingTeams = [];
      this.loading = false;
    });
  }

  rankings(title: TemplateRef<{}>, content: TemplateRef<{}>, footer: TemplateRef<{}>, size: number) {
    if (this.competition !== null) {
      this.getTeamsRankings();
    }
    this.rankingModal = this.createTplModal(title, content, footer, size);
  }

  closeRankingModal() {
    this.rankingModal.close();
  }

  selectRankingTeam() {
    if (this.competition !== null) {
      this.getTeamsRankings(true);
    }
  }

  selectRankingPlayer() {
    if (this.competition !== null) {
      this.getPlayersRankings(true);
    }
  }

  ngOnInit() {

    this.fields.push(
      {
        key: 'translations',
        row: 1,
        type: InputType.multilanguage,
        fields: [{
          key: 'name',
          type: InputType.input,
          templateOptions: {
            name: 'nome',
            label: 'Name',
            validators: [Validators.required]
          },
          row: 1,
          span: 24
        }]
      },
      {
        key: 'challenge_id',
        model_name: 'challenge',
        type: InputType.select,
        templateOptions: {
          name: 'desafios',
          label: 'Desafios',
          repository: this.challengeService,
          selectLabelKey: 'translations',
          validators: [Validators.required],
          multiSelect: true
        },
        row: 2,
        span: 24
      },


      )
  }

}
