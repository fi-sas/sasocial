import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChallengesTypesComponent } from './list-challenges-types.component';

describe('ListChallengesTypesComponent', () => {
  let component: ListChallengesTypesComponent;
  let fixture: ComponentFixture<ListChallengesTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListChallengesTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListChallengesTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
