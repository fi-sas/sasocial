import { Component, OnInit } from '@angular/core';
import { ChallengeTypeService } from "@fi-sas/backoffice/modules/gamification/services/challenge-type.service";

@Component({
  selector: 'fi-sas-list-challenges-types',
  templateUrl: './list-challenges-types.component.html',
  styleUrls: ['./list-challenges-types.component.less']
})
export class ListChallengesTypesComponent implements OnInit {

  fields = {};
  sortKeys: any = {
    tag: 'Tag',
    description: 'Descrição'
  };
  searchKeys = {tag: 'Tag'};

  constructor(public challengeTypeService: ChallengeTypeService) { }

  ngOnInit() {
    this.fields = {
      tag: 'Tag'
    }
  }

}
