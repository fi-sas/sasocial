import { Component, OnInit } from '@angular/core';
import { ChallengesService } from "@fi-sas/backoffice/modules/gamification/services/challenges.service";
import { CustomControl, CustomControlOption } from "@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface";
import { ServicesService } from "@fi-sas/backoffice/modules/configurations/services/services.service";
import { FiConfigurator } from "@fi-sas/configurator";
import { TagComponent } from "@fi-sas/backoffice/shared/components/tag/tag.component";

@Component({
  selector: 'fi-sas-list-challenges',
  templateUrl: './list-challenges.component.html',
  styleUrls: ['./list-challenges.component.less']
})
export class ListChallengesComponent implements OnInit {

  withRelated = ['challengeType'];
  fields = {};
  sortKeys: any = {
    points: 'Pontos',
    winners: 'Vencedores',
    start_date: 'Data de Início',
    end_date: 'Data de fim'
  };

  searchKeys = {name: 'Nome', tag: 'Tag'};

  customSearchs: CustomControl[] = [
    {
      name: 'start_date',
      label: 'Data de início',
      type: CustomControlOption.DATE,
    },
    {
      name: 'end_date',
      label: 'Data de fim',
      type: CustomControlOption.DATE
    },
    {name: 'automated',label: 'Automático',
      type: CustomControlOption.SELECT,
      values: {
        true: 'Sim',
        false: 'Não'
      }
    }
  ];

  serviceSelect = {};
  default_language: number;

  constructor(public challengeService: ChallengesService,
              private serviceService: ServicesService,
              private configurator: FiConfigurator) {
    this.default_language = this.configurator.getOption('DEFAULT_LANG_ID');
  }

  ngOnInit() {

    this.serviceService.list(-1, -1).subscribe(services => {
      services.data.map(service => {
        this.serviceSelect[service.id] = service.translations.find(lang => lang.language_id === this.default_language).name;
      });
      this.customSearchs.push({
        name: 'service_id',
        label: 'Serviço',
        type: CustomControlOption.SELECT,
        values: this.serviceSelect
      });
    });

    this.fields = {
      // 'translations.name': { translation: 'Nome' },
      start_date: 'Data de início',
      automated: {
        label: 'Automático',
        results: TagComponent.YesNoTag
      }
    }
  }

}
