import { Component, OnInit } from '@angular/core';
import { CompetitionsService } from "@fi-sas/backoffice/modules/gamification/services/competitions.service";

@Component({
  selector: 'fi-sas-list-competitions',
  templateUrl: './list-competitions.component.html',
  styleUrls: ['./list-competitions.component.less']
})
export class ListCompetitionsComponent implements OnInit {

  withRelated = ['translations', 'challenges', 'challenges.translations'];
  fields = {};
  sortKeys: any = {};

  searchKeys = {name: 'Nome'};

  constructor(public competitionService: CompetitionsService) { }

  ngOnInit() {
    this.fields = {
      'translations.name': { translation: 'Nome' }
    }
  }

}
