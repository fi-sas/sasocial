import { Component, OnInit } from '@angular/core';
import { PlayersService } from "@fi-sas/backoffice/modules/gamification/services/players.service";

@Component({
  selector: 'fi-sas-list-players',
  templateUrl: './list-players.component.html',
  styleUrls: ['./list-players.component.less']
})
export class ListPlayersComponent implements OnInit {

  withRelated = ['team'];
  fields = {};
  sortKeys: any = {
    nickname: 'Nickname'
  };
  searchKeys = {nickname: 'Nickname'};

  constructor(public playerService: PlayersService) { }

  ngOnInit() {
    this.fields = {
      nickname: 'Nickname'
    }
  }

}
