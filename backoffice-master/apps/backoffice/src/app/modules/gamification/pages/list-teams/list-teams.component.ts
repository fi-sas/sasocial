import { Component, OnInit } from '@angular/core';
import { TeamsService } from "@fi-sas/backoffice/modules/gamification/services/teams.service";

@Component({
  selector: 'fi-sas-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.less']
})
export class ListTeamsComponent implements OnInit {

  withRelated = ['players'];
  fields = {};
  sortKeys: any = {
    name: 'Nome'
  };
  searchKeys = {name: 'Nome'};

  constructor(public teamService: TeamsService) { }

  ngOnInit() {
    this.fields = {
      name: 'Nome'
    }
  }

}
