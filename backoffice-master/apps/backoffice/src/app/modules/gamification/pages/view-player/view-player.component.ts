import { Component, OnInit } from '@angular/core';
import { PlayerModel } from "@fi-sas/backoffice/modules/gamification/models/player.model";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { PlayersService } from "@fi-sas/backoffice/modules/gamification/services/players.service";
import { first } from "rxjs/operators";

@Component({
  selector: 'fi-sas-view-player',
  templateUrl: './view-player.component.html',
  styleUrls: ['./view-player.component.less']
})
export class ViewPlayerComponent implements OnInit {

  pagination: boolean = false;
  player: PlayerModel = null;

  constructor(private listService: ListService,
              private uiService: UiService,
              private playerService: PlayersService) { }

  deletePlayer(player_id : number) {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esse jogador?', 'Eliminar').subscribe(result => {
      if (result) {
        this.playerService.delete(player_id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.player);
        });
      }
    });
  }

  ngOnInit() {
    this.listService.selectedItemObservable().subscribe(item => {
      this.player = item;
    })
  }

}
