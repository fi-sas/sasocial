import { Component, OnInit } from '@angular/core';
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { TeamModel } from "@fi-sas/backoffice/modules/gamification/models/team.model";
import { first } from "rxjs/operators";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TeamsService } from "@fi-sas/backoffice/modules/gamification/services/teams.service";

@Component({
  selector: 'fi-sas-view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.less']
})
export class ViewTeamComponent implements OnInit {

  pagination: boolean = false;
  team: TeamModel = null;

  constructor(private listService: ListService,
              private uiService: UiService,
              private teamService: TeamsService) { }

  deleteTeam(team_id : number) {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar essa equipa?', 'Eliminar').subscribe(result => {
      if (result) {
        this.teamService.delete(team_id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.team);
        });
      }
    });
  }

  ngOnInit() {
    this.listService.selectedItemObservable().subscribe(item => {
      this.team = item;
    })
  }

}
