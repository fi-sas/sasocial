import { TestBed } from '@angular/core/testing';

import { ChallengeTypeService } from './challenge-type.service';

describe('ChallengeTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChallengeTypeService = TestBed.get(ChallengeTypeService);
    expect(service).toBeTruthy();
  });
});
