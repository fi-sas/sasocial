import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ChallengeTypeModel } from "@fi-sas/backoffice/modules/gamification/models/challenge-type.model";

@Injectable({
  providedIn: 'root'
})
export class ChallengeTypeService extends Repository<ChallengeTypeModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CHALLENGESTYPES';
    this.entity_url = 'CHALLENGESTYPES_ID';
  }
}
