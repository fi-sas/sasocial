import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ChallengeModel } from "@fi-sas/backoffice/modules/gamification/models/challenge.model";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { RankingTeams } from "@fi-sas/backoffice/modules/gamification/models/ranking-teams";
import { RankingPlayers } from "@fi-sas/backoffice/modules/gamification/models/ranking-players";
import { PointsModel } from "@fi-sas/backoffice/modules/gamification/models/points.model";

@Injectable({
  providedIn: 'root'
})
export class ChallengesService extends Repository<ChallengeModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CHALLENGES_GAMI';
    this.entity_url = 'CHALLENGES_GAMI_ID';
  }

  rankingsTeams(pageIndex: number, pageSize: number, id: number, team_id?: number): Observable<Resource<RankingTeams>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (team_id) {
        params = params.set('team_id', team_id.toString());
    }
    return this.resourceService.list<RankingTeams>(this.urlService.get('CHALLENGES_RANKING_TEAMS',{id}), { params });
  }

  rankingsPlayers(pageIndex: number, pageSize: number, id: number, player_id?: number): Observable<Resource<RankingPlayers>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (player_id) {
      params = params.set('player_id', player_id.toString());
    }
    return this.resourceService.list<RankingPlayers>(this.urlService.get('CHALLENGES_RANKING_PLAYERS',{id}), { params });
  }

  removePlayer(id: number, player_id: number): any {
    return this.resourceService.delete(this.urlService.get('CHALLENGES_PLAYER', { id, player_id } ), {});
  }

  addPlayerScore(playerScore: PointsModel, id: number): Observable<Resource<PointsModel>> {

    return this.resourceService.create<PointsModel>(this.urlService.get('CHALLENGES_SCORE', {id}), playerScore);
  }

}
