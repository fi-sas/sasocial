import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CompetitionModel } from "@fi-sas/backoffice/modules/gamification/models/competition.model";
import { Observable } from "rxjs";
import { RankingTeams } from "@fi-sas/backoffice/modules/gamification/models/ranking-teams";
import { HttpParams } from "@angular/common/http";
import { RankingPlayers } from "@fi-sas/backoffice/modules/gamification/models/ranking-players";

@Injectable({
  providedIn: 'root'
})
export class CompetitionsService extends Repository<CompetitionModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'COMPETITIONS';
    this.entity_url = 'COMPETITIONS_ID';
  }

  rankingsTeams(pageIndex: number, pageSize: number, challenge_id: number, team_id?: number): Observable<Resource<RankingTeams>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (team_id) {
      params = params.set('team_id', team_id.toString());
    }
    return this.resourceService.list<RankingTeams>(this.urlService.get('COMPETITIONS_RANKING_TEAMS',{challenge_id}), { params });
  }

  rankingsPlayers(pageIndex: number, pageSize: number, challenge_id: number, player_id?: number): Observable<Resource<RankingPlayers>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (player_id) {
      params = params.set('player_id', player_id.toString());
    }
    return this.resourceService.list<RankingPlayers>(this.urlService.get('COMPETITIONS_RANKING_PLAYERS',{challenge_id}), { params });
  }
}
