import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { PlayerModel } from "@fi-sas/backoffice/modules/gamification/models/player.model";

@Injectable({
  providedIn: 'root'
})
export class PlayersService extends Repository<PlayerModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PLAYERS';
    this.entity_url = 'PLAYERS_ID';
  }
}
