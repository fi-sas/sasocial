import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { TeamModel } from "@fi-sas/backoffice/modules/gamification/models/team.model";

@Injectable({
  providedIn: 'root'
})
export class TeamsService extends Repository<TeamModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'TEAMS';
    this.entity_url = 'TEAMS_ID';
  }
}
