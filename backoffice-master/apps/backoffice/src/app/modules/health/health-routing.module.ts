import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { HealthComponent } from './health.component';

const routes: Routes = [
  {
    path: '',
    component: HealthComponent,
    children: [
      {
        path: 'specialty',
        loadChildren: './modules/specialty/specialty.module#SpecialtyModule',
        data: { breadcrumb: 'Especialidades', title: 'Especialidades' },
      },
      {
        path: 'place',
        loadChildren: './modules/place/place.module#PlaceModule',
        data: { breadcrumb: 'Local', title: 'Local' },
      },
      {
        path: 'tariff',
        loadChildren: './modules/tariff/tariff.module#TariffModule',
        data: { breadcrumb: 'Tarifários', title: 'Tarifários' },
      },
      {
        path: 'appointment-type',
        loadChildren: './modules/appointment-type/appointment-type.module#AppointmentTypeModule',
        data: { breadcrumb: 'Tipos Consulta', title: 'Tipos Consulta' },
      },
      {
        path: 'professionals',
        loadChildren: './modules/doctors/doctors.module#DoctorsModule',
        data: { breadcrumb: 'Profissionais', title: 'Profissionais' },
      },
      {
        path: 'schedules',
        loadChildren: './modules/schedules/schedule.module#ScheduleModule',
        data: { breadcrumb: 'Horários', title: 'Horários' },
      },
      {
        path: 'appointments',
        loadChildren: './modules/appointments/appointments.module#AppointmentsModule',
        data: { breadcrumb: 'Marcações', title: 'Marcações' },
      },
      {
        path: 'complaints',
        loadChildren: './modules/complaints/complaints.module#ComplaintModule',
        data: { breadcrumb: 'Reclamações', title: 'Reclamações' },
      },
      {
        path: 'configurations',
        loadChildren: './modules/configuration/configuration.module#HealthConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações' },
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRoutingModule { }
