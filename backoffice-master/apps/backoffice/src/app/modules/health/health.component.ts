import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-health',
  template: '<router-outlet></router-outlet>',
})
export class HealthComponent implements OnInit {
  dataConfiguration: any;

  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService, private authservice: AuthService,) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    this.uiService.setSiderTitle(this.validTitleTraductions(7), 'heart');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const specialty = new SiderItem('Especialidades');
    specialty.addChild(new SiderItem('Criar', '', '/health/specialty/create', 'health:specialty:create'));
    specialty.addChild(new SiderItem('Listar', '', '/health/specialty/list', 'health:specialty:read'));
    this.uiService.addSiderItem(specialty);

    const appointments = new SiderItem('Marcações');
    appointments.addChild(new SiderItem('Criar', '', '/health/appointments/create', 'health:appointment:create'));
    appointments.addChild(new SiderItem('Listar', '', '/health/appointments/list', 'health:appointment:read'));
    this.uiService.addSiderItem(appointments);

    const complaints = new SiderItem('Reclamações');
    complaints.addChild(new SiderItem('Listar', '', '/health/complaints/list', 'health:appointment:complaint'));
    this.uiService.addSiderItem(complaints);

    const doctors = new SiderItem('Profissionais');
    const createDoctor = doctors.addChild(new SiderItem('Externo'));
    createDoctor.addChild(new SiderItem('Criar', '', '/health/professionals/create-external', 'health:doctor-external:create'));
    createDoctor.addChild(new SiderItem('Listar', '', '/health/professionals/list-external', 'health:doctor-external:read'));
    doctors.addChild(new SiderItem('Associar Profissional', '', '/health/professionals/create-internal', 'health:doctor:create-internal'));
    doctors.addChild(new SiderItem('Listar', '', '/health/professionals/list', 'health:doctor:read'));

    this.uiService.addSiderItem(doctors);

    const schedules = new SiderItem('Horários');
    schedules.addChild(new SiderItem('Criar', '', '/health/schedules/create', 'health:period:create'));
    schedules.addChild(new SiderItem('Listar', '', '/health/schedules/list', 'health:period:periods'));
    this.uiService.addSiderItem(schedules);

    const configurations = new SiderItem('Configurações');
    const place = configurations.addChild(new SiderItem('Local'));
    place.addChild(new SiderItem('Criar', '', '/health/place/create', 'health:place:create'));
    place.addChild(new SiderItem('Listar', '', '/health/place/list', 'health:place:read'));
    const tariffs = configurations.addChild(new SiderItem('Tarifários'));
    tariffs.addChild(new SiderItem('Criar', '', '/health/tariff/create', 'health:tariff:create'));
    tariffs.addChild(new SiderItem('Listar', '', '/health/tariff/list', 'health:tariff:read'));
    const appointmentTypes = configurations.addChild(new SiderItem('Tipos Consulta'));
    appointmentTypes.addChild(new SiderItem('Criar', '', '/health/appointment-type/create', 'health:appointment-type:create'));
    appointmentTypes.addChild(new SiderItem('Listar', '', '/health/appointment-type/list', 'health:appointment-type:read'));
    configurations.addChild(new SiderItem('Configurações Gerais', '', '/health/configurations/form', 'health:configurations:create'));
    this.uiService.addSiderItem(configurations);
    
    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
