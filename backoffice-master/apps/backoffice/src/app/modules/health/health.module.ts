import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UsersService } from '../users/modules/users_users/services/users.service';
import { HealthRoutingModule } from './health-routing.module';
import { HealthComponent } from './health.component';

@NgModule({
  declarations: [
    HealthComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HealthRoutingModule,
  ],
  providers: [
    UsersService
  ],
})
export class HealthModule { }
