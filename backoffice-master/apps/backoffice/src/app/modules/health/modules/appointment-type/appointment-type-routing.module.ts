import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentTypeFormComponent } from './pages/appointment-type-form/appointment-type-form.component';
import { AppointmentTypeListComponent } from './pages/appointment-type-list/appointment-type-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: AppointmentTypeFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:appointment-type:create' }
  },
  {
    path: 'edit/:id',
    component: AppointmentTypeFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:appointment-type:create' }
  },
  {
    path: 'list',
    component: AppointmentTypeListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:appointment-type:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentTypeRoutingModule { }
