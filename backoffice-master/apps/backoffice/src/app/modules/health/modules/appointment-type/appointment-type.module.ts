import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentTypeFormComponent } from './pages/appointment-type-form/appointment-type-form.component';
import { AppointmentTypeListComponent } from './pages/appointment-type-list/appointment-type-list.component';
import { AppointmentTypeRoutingModule } from './appointment-type-routing.module';

@NgModule({
  declarations: [
    AppointmentTypeFormComponent,
    AppointmentTypeListComponent
  ],
  imports: [
    CommonModule,
    AppointmentTypeRoutingModule,
    SharedModule
  ]
})
export class AppointmentTypeModule { }
