export class AppointmentTypeModel {
    id: number;
    translations: AppointmentTypeModelTranslationModel[];
    created_at: Date;
    update_at: Date;
}

export class AppointmentTypeModelTranslationModel{
    appointment_type_id: number;
    language_id: number;
    name: string;
}

