import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentTypeFormComponent } from './appointment-type-form.component';

describe('AppointmentTypeFormComponent', () => {
  let component: AppointmentTypeFormComponent;
  let fixture: ComponentFixture<AppointmentTypeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentTypeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
