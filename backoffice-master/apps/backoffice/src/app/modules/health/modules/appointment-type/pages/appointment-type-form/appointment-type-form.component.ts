import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeService } from '../../services/appointment-type.service';

@Component({
  selector: 'fi-sas-appointment-type-form',
  templateUrl: './appointment-type-form.component.html',
  styleUrls: ['./appointment-type-form.component.less']
})
export class AppointmentTypeFormComponent implements OnInit {

  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  
  loadingLanguages = false;
  languages: LanguageModel[] = [];
  
  appointmentTypeGroup = new FormGroup({
    translations: new FormArray([]),
  });
  translations = this.appointmentTypeGroup.get('translations') as FormArray;
  
  get f() { return this.appointmentTypeGroup.controls; }
  idToUpdate = null;
  submit = false;
  isLoading = false;

  constructor(
    private languagesService: LanguagesService,
    public appointmentTypeService: AppointmentTypeService,
    private uiService: UiService,
    public activateRoute: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.loadLanguages();
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loadingLanguages = false))
      )
      .subscribe((results) => {
        this.languages = results.data;

        this.appointmentType();
      });
  }

  appointmentType() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.isLoading = true;
        this.idToUpdate = data.get('id');
        this.appointmentTypeService
          .read(parseInt(data.get('id'), 10), {
            withRelated: 'translations',
          })
          .pipe(
            first(),
            finalize(()=> this.isLoading = false)
          )
          .subscribe((result) => {
            if (result.data[0].translations) {
              result.data[0].translations.map((t) => {
                this.addTranslation(t.language_id, t.name);
              });
            }

            this.languages.map((l) => {
              const foundLnaguage = this.translations.value.find(
                (t) => t.language_id === l.id
              );
              if (!foundLnaguage) {
                this.addTranslation(l.id, '');
              }
            });
          });
      } else {
        this.languages.map((l) => {
          this.addTranslation(l.id, '');
        });
      }
    });
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.appointmentTypeGroup.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }
  
  submitForm(value: any) {
    this.submit = true;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.appointmentTypeGroup.valid) {
      this.isLoading = true;
      this.submit = false;
      if (this.idToUpdate) {
        this.appointmentTypeService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Tipo de consulta alterado com sucesso'
            );
          });
      } else {
        this.appointmentTypeService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Tipo de consulta criado com sucesso'
            );
          });
      }
    }
  }

  backList() {
    this.router.navigate(['health', 'appointment-type', 'list']);
  }

}
