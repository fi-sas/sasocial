import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { AppointmentTypeService } from '../../services/appointment-type.service';

@Component({
  selector: 'fi-sas-appointment-type-list',
  templateUrl: './appointment-type-list.component.html',
  styleUrls: ['./appointment-type-list.component.less']
})
export class AppointmentTypeListComponent extends TableHelper implements OnInit {

  languages: LanguageModel[] = [];
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    public appointmentTypeService: AppointmentTypeService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.loadLanguages();
    this.persistentFilters = {
      searchFields: 'name',
    };
  }

  ngOnInit() {
    this.initTableData(this.appointmentTypeService);
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  loadLanguages() {
    this.languageService.list(1, -1).pipe(
      first()
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  edit(id: number) {
    if(!this.authService.hasPermission('health:appointment-type:update')){
      return;
    }
      this.router.navigateByUrl('/health/appointment-type/edit/' + id);
  }
  
  delete(id: number) {
    if(!this.authService.hasPermission('health:appointment-type:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este tipo de consulta?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.appointmentTypeService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Tipo de consulta eliminado com sucesso'
              );
              this.initTableData(this.appointmentTypeService);
            });
          }
        });
    }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

}
