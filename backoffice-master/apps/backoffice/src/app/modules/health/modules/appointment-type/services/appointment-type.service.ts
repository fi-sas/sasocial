import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { AppointmentTypeModel } from '../models/appointment-type.model';

@Injectable({
  providedIn: 'root'
})
export class AppointmentTypeService extends Repository<AppointmentTypeModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'HEALTH.APPOINTMENT_TYPE';
    this.entity_url = 'HEALTH.APPOINTMENT_TYPE_ID';
  }
}
