import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentsFormComponent } from './pages/appointments-form/appointments-form.component';

import { AppointmentsListComponent } from './pages/appointments-list/appointments-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: AppointmentsFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:appointment:create' }
  },
  {
    path: 'edit/:id',
    component: AppointmentsFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:appointment:create' }
  },
  {
    path: 'list',
    component: AppointmentsListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:appointment:read' }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentsRoutingModule { }
