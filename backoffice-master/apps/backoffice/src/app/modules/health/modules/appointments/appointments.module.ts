import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsRoutingModule } from './appointments-routing.module';
import { AppointmentsFormComponent } from './pages/appointments-form/appointments-form.component';
import { AppointmentsListComponent } from './pages/appointments-list/appointments-list.component';
import { AppointmentStepOneComponent } from './components/appointment-step-one/appointment-step-one.component';
import { AppointmentStepTwoComponent } from './components/appointment-step-two/appointment-step-two.component';
import { AppointmentStepThreeComponent } from './components/appointment-step-three/appointment-step-three.component';
import { AppointmentStepFourComponent } from './components/appointment-step-four/appointment-step-four.component';
import { AppointmentStepFiveComponent } from './components/appointment-step-five/appointment-step-five.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { NgxFullCalendarModule } from "ngx-fullcalendar";
import { ListUsersComponent } from './components/list-users/list-users.component';
import { ViewAppointmentComponent } from './components/view-appointment/view-appointment.component';
import { BuyModalBoxComponent } from './components/buy-modal-box/buy-modal-box.component';

@NgModule({
  declarations: [
  AppointmentsFormComponent,
  AppointmentsListComponent,
  AppointmentStepOneComponent,
  AppointmentStepTwoComponent,
  AppointmentStepThreeComponent,
  AppointmentStepFourComponent,
  AppointmentStepFiveComponent,
  ListUsersComponent,
  ViewAppointmentComponent,
  BuyModalBoxComponent,
],
  imports: [
    CommonModule,
    AppointmentsRoutingModule,
    SharedModule,
    AngularOpenlayersModule,
    NgxFullCalendarModule 
  ],
  entryComponents: [
    ListUsersComponent
  ]
})

export class AppointmentsModule { }
