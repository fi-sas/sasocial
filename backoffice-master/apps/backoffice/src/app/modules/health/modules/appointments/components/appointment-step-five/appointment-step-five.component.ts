import { Component, Input, OnInit } from '@angular/core';
import { hasOwnProperty } from 'tslint/lib/utils';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { PriceVariationsModel } from '../../../specialty/models/price-variations.model';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';

@Component({
  selector: 'fi-sas-appointment-step-five',
  templateUrl: './appointment-step-five.component.html',
  styleUrls: ['./appointment-step-five.component.less']
})
export class AppointmentStepFiveComponent implements OnInit {
  
  usersLoading = false

  @Input() steps = [];
  @Input() arrayValues: {
    specialties: SpecialtyModel[];
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: PriceVariationsModel[];
    } = null;

  specialtyFound: SpecialtyModel;

  constructor() { }

  ngOnInit() {
  }

  findSpecialty(id: number) {
    if (this.arrayValues !== null) {
      this.specialtyFound = this.arrayValues.specialties.find(
        (value) => value.id === id
      );
      if (
        this.specialtyFound !== undefined &&
        hasOwnProperty(this.specialtyFound, 'translations')
      ) {
        return this.specialtyFound.translations.find((tran) => tran.language_id == 3).name;
      }
    }
    return id;
  }

  findDoctor(id: number){
    let doctorFound = null;
    if (this.arrayValues !== null) {
      doctorFound = this.arrayValues.doctors.find(
        (value) => value.id === id
      );
      if (
        doctorFound !== undefined &&
        hasOwnProperty(doctorFound, 'user')
      ) {
        return doctorFound.user.name;
      }
    }
    return id;
  }

  findAppointmentType(id: number){
    let appointmentTypeFound = null;
    if (this.arrayValues !== null) {
      appointmentTypeFound = this.arrayValues.appointmentTypes.find(
        (value) => value.id === id
      );
      if (
        appointmentTypeFound !== undefined &&
        hasOwnProperty(appointmentTypeFound, 'translations')
      ) {
        return appointmentTypeFound.translations.find((tran) => tran.language_id == 3).name;
      }
    }
    return id;
  }

  findTariff(id: number){
    let tariffFound = null;
    if (this.arrayValues !== null) {
      tariffFound = this.arrayValues.tariffs.find(
        (value) => value.id === id
      );
      if (tariffFound !== undefined) {
        return tariffFound.description;
      }
    }
    return id;
  }
}
