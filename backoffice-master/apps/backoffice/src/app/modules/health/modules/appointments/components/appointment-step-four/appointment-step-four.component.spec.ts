import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentStepFourComponent } from './appointment-step-four.component';

describe('AppointmentStepFourComponent', () => {
  let component: AppointmentStepFourComponent;
  let fixture: ComponentFixture<AppointmentStepFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentStepFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentStepFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
