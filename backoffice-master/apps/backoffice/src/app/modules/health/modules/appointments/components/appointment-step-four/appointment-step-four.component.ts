import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { TariffModel } from '../../../tariff/models/tariff.model';
import { SpecialtiesFormModel } from '../../models/appointment-form.model';

@Component({
  selector: 'fi-sas-appointment-step-four',
  templateUrl: './appointment-step-four.component.html',
  styleUrls: ['./appointment-step-four.component.less']
})
export class AppointmentStepFourComponent implements OnInit {
  
  @Input() specialty: SpecialtiesFormModel = null;
  @Input() payment: SpecialtiesFormModel = null;
  @Input() arrayValues: {
    specialties: SpecialtyModel[];
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: TariffModel[];
  } = null;

  tariffsLoading = false;
  tariffs: TariffModel[] = [];

  selectedSpecialty: SpecialtyModel = null;

  paymentForm = new FormGroup({
    payment_type:new FormControl(null, [Validators.required]),
    tariff_id: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required]),
  })

  get f() { return this.paymentForm.controls; }
  submitted = false;

  constructor(
  ) { }

  ngOnInit() {
    this.getTariffs();

    this.selectedSpecialty = this.arrayValues.specialties.find( s => s.id === this.specialty.specialty_id);
      this.paymentForm.patchValue({
        payment_type: this.selectedSpecialty.payment_type
    })

    if (this.payment !== null) {
      this.paymentForm.patchValue({
        ...this.payment,
      });
    }  
  }

  getTariffs(){
    if(this.arrayValues){
      this.tariffs = this.arrayValues.tariffs;
    }
  }

  isDisabled(id){
    if(this.selectedSpecialty){
      return this.selectedSpecialty.price_variations.find(price => price.tariff_id === id) ? false : true
    }
  }

  getPriceInformation(){
    const tariffSelected = this.paymentForm.get('tariff_id').value;
    if(tariffSelected){
      this.paymentForm.patchValue({
        price: this.selectedSpecialty.price_variations.find(price => price.tariff_id === tariffSelected).price
      })
    }
  }

  submitForm(): any {
    this.submitted = true;
    if (this.paymentForm.valid) {
      this.submitted = false;
      this.paymentForm.updateValueAndValidity();
      return this.paymentForm.value;
    }
    for (const i in this.paymentForm.controls) {
      if (this.paymentForm.controls[i]) {
        this.paymentForm.controls[i].markAsDirty();
        this.paymentForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }
}
