import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentStepOneComponent } from './appointment-step-one.component';

describe('AppointmentStepOneComponent', () => {
  let component: AppointmentStepOneComponent;
  let fixture: ComponentFixture<AppointmentStepOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentStepOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
