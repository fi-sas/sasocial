import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { SpecialtyService } from '../../../specialty/services/specialty.service';
import { DoctorScheduleFormModel, SpecialtiesFormModel } from '../../models/appointment-form.model';
import * as moment from 'moment';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import { FullCalendarOptions, EventObject } from 'ngx-fullcalendar';
import { EventModel } from '../../../schedules/models/schedule.model';
import { AppointmentTypeService } from '../../../appointment-type/services/appointment-type.service';
import { TariffModel } from '../../../tariff/models/tariff.model';
import { TariffService } from '../../../tariff/services/tariff.service';
import { NzModalRef, NzModalService } from "ng-zorro-antd";

@Component({
  selector: 'fi-sas-appointment-step-one',
  templateUrl: './appointment-step-one.component.html',
  styleUrls: ['./appointment-step-one.component.less']
})
export class AppointmentStepOneComponent implements OnInit {

  @Input() specialtyInfo: SpecialtiesFormModel = null;
  @ViewChild ('calendar', { static: false }) calendarComponent;
  
  options: FullCalendarOptions;
  eventsInCalendar: EventObject[] = [];
  events: EventModel[];
  event: EventModel = null;
  infoModal: NzModalRef;

  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();

  specialtiesLoading = false;
  specialtiesList: SpecialtyModel[] = [];
  specialtySelected: SpecialtyModel;

  doctorsLoading = false;
  doctors: DoctorModel[] = [];
  doctorSelected: DoctorModel;

  appointmentTypesLoading = false;
  appointmentTypesList: AppointmentTypeModel[] = [];

  schedulesLoading = false;
  schedulesList: DoctorScheduleFormModel;

  tariffsLoading = false;
  tariffs: TariffModel[] = [];

  specialtyForm = new FormGroup({
    doctor_id: new FormControl(null, Validators.required),
    specialty_id: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
    appointment_type_id: new FormControl(null, Validators.required),
    overlapAppointment: new FormControl(false)
  });

  overlapAppointmentForm =  new FormGroup({
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
  })

  get f() { return this.specialtyForm.controls; }
  get z() { return this.overlapAppointmentForm.controls; }
  submitted = false;
  isEdit = false;
  
  constructor(
    public activateRoute: ActivatedRoute,
    public router: Router,
    private doctorsService: DoctorsService,
    private specialtyService: SpecialtyService,
    private appointmentTypeService: AppointmentTypeService,
    private tariffsService: TariffService,
    private modalService: NzModalService
  ) { }

  ngOnInit() {

    this.getSpecialties();
    this.getAppointmentTypes();
    this.getAllDoctors();
    this.getTariffs();

    if (this.specialtyInfo && this.specialtyInfo.specialty_id) {
      this.specialtyService.read(this.specialtyInfo.specialty_id).pipe(
        first(),
        finalize(() => this.specialtiesLoading = false)).subscribe(results => {
          this.specialtySelected = results.data[0];
          
          this.specialtyForm.patchValue({
            ...this.specialtyInfo,
            start_hour: moment(this.specialtyInfo.start_hour, "HH:mm:ss").toDate(),
          });
          this.options = {
            defaultDate: new Date(this.f.date.value),
            header: {
              left: '',
              center: '',
              right: '',
            },
            editable: false,
            locale: 'pt',
            droppable: false,
            eventResizableFromStart: true,
          };
        }); 
    }else{
      this.options = {
        defaultDate: new Date(),
        header: {
          left: '',
          center: '',
          right: '',
        },
        editable: false,
        locale: 'pt',
        droppable: false,
        eventResizableFromStart: true,
      };
    }
  }


  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialtiesList = results.data
      });
  }

  getTariffs(){
    this.tariffsLoading = true;
    this.tariffsService.list(1, -1).pipe(
      first(),
      finalize(() => this.tariffsLoading = false)).subscribe(results => {
        this.tariffs = results.data;
      });
  }

  getAllDoctors(){
    this.doctorsService.list(1, -1).pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctors = results.data
      });
  }

  isDoctorDisabled(id){
    if( this.specialtySelected){
      return this.specialtySelected.doctors.find( d => d.id === id) ? false : true
    }
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list(1, -1).pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypesList = results.data;
      });
  }

  getSpecialtyInfo (){
    this.specialtySelected = this.specialtiesList.find(s => s.id === this.specialtyForm.get('specialty_id').value);
    this.clearFormValues();
  }

  getDoctorInfo(){
    this.doctorSelected = this.doctors.find(d => d.id ===  this.specialtyForm.get('doctor_id').value);
    this.clearFormValues();
    this.getSchedule();
  }

  clearFormValues(){
    this.specialtyForm.patchValue({
      date: null,
      start_hour: null,
      end_hour: null,
      overlapAppointment: false
    })
  }

  getSchedule(){
    const doctor_id = this.specialtyForm.get('doctor_id').value;
    if(doctor_id && this.specialtySelected){
      this.eventsInCalendar = [];
      this.events = [];
      if(!this.specialtySelected.is_external){
        this.schedulesLoading = true;
        this.specialtyService.getScheduleDoctor(this.specialtySelected.id, doctor_id).pipe(
          first(),
          finalize(() => this.schedulesLoading = false)).subscribe(results => {
            this.events = results.data;
            this.events.forEach(event => {
              if(event.is_holiday){
                this.eventsInCalendar.push({
                  id: event.id,
                  title: event.title,
                  start: event.start_date,
                  allDay: true,
                });
              }else{
                this.eventsInCalendar.push({
                  id: event.id,
                  title: event.title,
                  start: event.start_date,
                  end: event.end_date,
                });  
              }
              
            });
          });
      }
    }
  }

  disableDate = (endDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(endDate).isSameOrBefore(currentDate);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  submitForm(): any {
    this.submitted = true;
    if (this.specialtyForm.valid) {
      this.specialtyForm.patchValue({
        date: moment(this.specialtyForm.controls.date.value).format("YYYY-MM-DD"),
        start_hour: moment(this.specialtyForm.controls.start_hour.value).format("HH:mm:ss"),
        end_hour: this.specialtyForm.controls.end_hour.value ?this.specialtyForm.controls.end_hour.value : moment(this.specialtyForm.controls.start_hour.value).add(this.specialtySelected.avarage_time, "minutes").format("HH:mm:ss")
      });
      this.submitted = false;
      this.specialtyForm.updateValueAndValidity();
      return this.specialtyForm.value;
    }
    for (const i in this.specialtyForm.controls) {
      if (this.specialtyForm.controls[i]) {
        this.specialtyForm.controls[i].markAsDirty();
        this.specialtyForm.controls[i].updateValueAndValidity();
      }
    }

    return null;
  }

  getArrayValues(): {
    specialties: SpecialtyModel[];
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: TariffModel[];
  } {
    return {
      specialties: this.specialtiesList,
      doctors: this.doctors,
      appointmentTypes: this.appointmentTypesList,
      tariffs: this.tariffs
    };
  }

  isAppointmentTypeDisabled(value:any){
    if(this.specialtySelected){
      return this.specialtySelected.appointment_types.find(d => d.id === value) ? false : true
    }
  }

  selectAppointmentSchedule(eventSelect: any){
    this.event = this.events.find( e => e.id=== eventSelect.id);
    if(this.event && this.event.is_available){
      const date = moment(eventSelect.start).format("YYYY-MM-DD");
      const start_hour =eventSelect.start;
      const end_hour = moment(eventSelect.end).format("HH:mm:ss");
      this.specialtyForm.patchValue({
        date: date,
        start_hour: start_hour,
        end_hour: end_hour,
        overlapAppointment: false
      })
    }
    /*else if(this.event && !this.event.is_available){
      this.event = this.events.find(e => e.id === eventSelect.id);
      this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
    }*/
  }

  appointmentOverlap(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>){
    this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
  }

  createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: 900
    });
  }

  overlapAppointment(){
      this.specialtyForm.patchValue({
        date: moment(this.overlapAppointmentForm.get("date").value).format("YYYY-MM-DD"),
        start_hour: moment(this.overlapAppointmentForm.get("start_hour").value),
        end_hour: moment(this.overlapAppointmentForm.get("end_hour").value).format("HH:mm:ss"),
        overlapAppointment: true
      })
    this.closeEventModal();
  }

  closeEventModal() {
    this.infoModal.close();
  }

}
