import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentStepThreeComponent } from './appointment-step-three.component';

describe('AppointmentStepThreeComponent', () => {
  let component: AppointmentStepThreeComponent;
  let fixture: ComponentFixture<AppointmentStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentStepThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
