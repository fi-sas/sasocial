import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { SpecialtiesFormModel } from '../../models/appointment-form.model';

@Component({
  selector: 'fi-sas-appointment-step-three',
  templateUrl: './appointment-step-three.component.html',
  styleUrls: ['./appointment-step-three.component.less']
})
export class AppointmentStepThreeComponent implements OnInit {

  @Input() attachments: SpecialtiesFormModel = null;
  
  attachmentsForm = new FormGroup({
    attachments_ids: new FormControl(null, []),
    notes: new FormControl(null, [trimValidation]),
    allow_historic: new FormControl(true),
  })

  get f() { return this.attachmentsForm.controls; }
  submitted = false;
  file_ids = [];

  constructor() { }

  ngOnInit() {
    if (this.attachments !== null) {
      this.attachmentsForm.patchValue({
        ...this.attachments,
      });
    }
  }

  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }

  submitForm(): any {
    this.submitted = true;
    if (this.attachmentsForm.valid) {
      this.submitted = false;
      this.attachmentsForm.updateValueAndValidity();
      return this.attachmentsForm.value;
    }
    for (const i in this.attachmentsForm.controls) {
      if (this.attachmentsForm.controls[i]) {
        this.attachmentsForm.controls[i].markAsDirty();
        this.attachmentsForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

}
