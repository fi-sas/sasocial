import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentStepTwoComponent } from './appointment-step-two.component';

describe('AppointmentStepTwoComponent', () => {
  let component: AppointmentStepTwoComponent;
  let fixture: ComponentFixture<AppointmentStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentStepTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
