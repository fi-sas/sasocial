import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';
import { DocumentTypeService } from '@fi-sas/backoffice/modules/users/modules/document-type/services/document-type.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { SpecialtiesFormModel } from '../../models/appointment-form.model';
import { nationalities } from '@fi-sas/backoffice/shared/common/nationalities';
import { ListUsersComponent } from '../list-users/list-users.component';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-appointment-step-two',
  templateUrl: './appointment-step-two.component.html',
  styleUrls: ['./appointment-step-two.component.less']
})
export class AppointmentStepTwoComponent implements OnInit {
  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
  '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
  '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
  '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
  '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  @Input() userInformation: SpecialtiesFormModel = null;
  
//  usersList: UserModel[] = [];
  usersLoading = false;
  selectedUser: UserModel;

  //loadingInfo = false;

  documentTypesLoading = false;
  documentTypes: DocumentTypeModel[] = [];

  organicUnits: OrganicUnitsModel[] = [];
  organicUnitsLoading = false;
  
  nationalities = nationalities;
  //usersSearchChange$ = new BehaviorSubject('');
  
  userForm = new FormGroup({
    user_id: new FormControl(null, [Validators.required]),
    name: new FormControl(null, [trimValidation]),
    student_number: new FormControl(null),
    niss: new FormControl(null),
    document_type_id: new FormControl(null),
    identification_number: new FormControl(null),
    nif: new FormControl(null),
    address: new FormControl(null, [trimValidation]),
    postal_code: new FormControl('', [Validators.pattern('\\d{4}-\\d{3}$')]),
    city: new FormControl(null, [trimValidation]),
    nationality: new FormControl(null),
    email: new FormControl('', [trimValidation, Validators.pattern(this.emailRegex)]),
    mobile_phone_number: new FormControl(null),
    telephone_number: new FormControl(null),
    course_id: new FormControl(null),
    organic_unit_id: new FormControl(null),
    course_year: new FormControl(null),
    birth_date: new FormControl(null),
    has_scholarship: new FormControl(null),
  })

  get f() { return this.userForm.controls; }
  submitted = false;
  
  constructor(
    public activateRoute: ActivatedRoute,
    public router: Router,
    private userService: UsersService,
    private documentTypeService: DocumentTypeService,
    private organicUnitsService: OrganicUnitsService,
    private drawerService: NzDrawerService,
  ) { }

  ngOnInit() {
    //this.getUsers();
    this.getDocumentTypes();
    this.getOrganicUnits();
    
    if (this.userInformation !== null) {
      this.userForm.patchValue({
        ...this.userInformation,
      });
    }
  }

  /*getUsers() {
    const loadUsers = (search: string): Observable<UserModel[]> =>
      this.userService
        .list(0, 100, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,student_number,email,identification',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.usersList = users;
      this.usersLoading = false;
    });
  }*/

  /*onSearchUsers(event) {
    this.usersLoading = true;
    this.usersSearchChange$.next(event);
  }*/

  getDocumentTypes(){
    let idCC = false;
    this.documentTypesLoading = true
    this.documentTypeService.list(1, -1, null, null, { active: true }).pipe(first(), finalize(() => this.documentTypesLoading = false)).subscribe((data) => {
      this.documentTypes = data.data;
      this.documentTypes.forEach((data) => {
        if (data.id == 2) {
          idCC = true;
        }
      })
      if (idCC && !this.userForm.get('document_type_id').value) {
        this.userForm.get('document_type_id').setValue(2);
      }

    })
  }

  getOrganicUnits(){
    this.organicUnitsLoading = true;
    this.organicUnitsService.list(1, -1).pipe(
      first(),
      finalize(() => this.organicUnitsLoading = false)).subscribe(results => {
        this.organicUnits = results.data;
      });
  }

  modalSearch() {
    this.usersLoading = true;
    const modalDrawer = this.drawerService.create({
        nzMask: true,
        nzMaskStyle: {
            'opacity': '0',
            '-webkit-animation': 'none',
            'animation': 'none',
        },
        nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
        nzContent: ListUsersComponent,
        nzWidth: (window.innerWidth / 24) * 12
     
    });
    modalDrawer.afterClose.pipe(first(),finalize(()=> this.usersLoading =false)).subscribe((result) => {
        if (result) {
          this.selectedUser = result;
          this.userForm.patchValue({
            user_id: this.selectedUser.id,
            name: this.selectedUser.name,
            student_number: this.selectedUser.student_number,
            document_type_id: this.selectedUser.document_type_id,
            identification_number: this.selectedUser.identification,
            nif: this.selectedUser.tin,
            address: this.selectedUser.address,
            postal_code: this.selectedUser.postal_code,
            city: this.selectedUser.city,
            nationality: this.selectedUser.nationality,
            email: this.selectedUser.email,
            mobile_phone_number: this.selectedUser.phone,
            course_department_id: this.selectedUser.department ? this.selectedUser.department.id : this.selectedUser.course_id,
            organic_unit_id: this.selectedUser.organic_unit_id,
            course_year: this.selectedUser.course_year,
            birth_date: this.selectedUser.birth_date ? moment(this.selectedUser.birth_date).format("YYYY-MM-DD") : null
          })
        }
    });
  }

  submitForm(): any {
    this.submitted = true;
    if (this.userForm.valid) {
      this.submitted = false;
      this.userForm.updateValueAndValidity();
      return this.userForm.value;
    }
    for (const i in this.userForm.controls) {
      if (this.userForm.controls[i]) {
        this.userForm.controls[i].markAsDirty();
        this.userForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

}
