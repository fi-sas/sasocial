import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyModalBoxComponent } from './buy-modal-box.component';

describe('BuyModalBoxComponent', () => {
  let component: BuyModalBoxComponent;
  let fixture: ComponentFixture<BuyModalBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyModalBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyModalBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
