import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { AppointmentModel } from '../../models/appointment.model';

@Component({
  selector: 'fi-sas-buy-modal-box',
  templateUrl: './buy-modal-box.component.html',
  styleUrls: ['./buy-modal-box.component.less']
})
export class BuyModalBoxComponent implements OnInit {

  @Input() appointment: AppointmentModel;
  @Input() specialty: SpecialtyModel;
  @Output() successBuy = new EventEmitter();

  isVisible = false;

  constructor(

  ) { }

  ngOnInit() {
  }

  getAppointmentType(appointment_type_id){
    if (this.specialty !== null) {
      return this.specialty.appointment_types.find(
        (value) => value.id === appointment_type_id
      ).translations;
    }
    return appointment_type_id;
  }
  
  showBuyModal(): void {
    this.isVisible = true;
  }

  hideBuyModal(): void {
    this.isVisible = false;
  }

  confirmAppointment(){
    this.hideBuyModal();
    this.successBuy.emit(true);
  }

  handleCancel(){
    this.hideBuyModal();
    this.successBuy.emit(false);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
