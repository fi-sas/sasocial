import { Component, Input, OnInit } from '@angular/core';
import { AppointmentModel } from '../../models/appointment.model';

@Component({
  selector: 'fi-sas-view-appointment',
  templateUrl: './view-appointment.component.html',
  styleUrls: ['./view-appointment.component.less']
})
export class ViewAppointmentComponent implements OnInit {

  @Input() appointment: AppointmentModel = new AppointmentModel();


  constructor() { }

  ngOnInit() {}

  
  validTranslateName(id_trans: number, translation: any[]) {
    return translation && translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans) : '---';
  }

}
