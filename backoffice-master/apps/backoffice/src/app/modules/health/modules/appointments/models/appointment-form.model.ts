export class SpecialtiesFormModel{
    id: number;
    specialty_id: number;
    doctor_id: number;
    start_hour: string;
    date: string;
    appointment_type_id: number;
}

export class DoctorScheduleFormModel{
  message?: string;
  availableHours: HourDoctorScheduleModel[];
}

export class HourDoctorScheduleModel{
  hour: string;
  is_available: boolean;
}

