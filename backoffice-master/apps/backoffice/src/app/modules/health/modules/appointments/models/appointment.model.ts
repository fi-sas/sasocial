import { CourseModel } from "@fi-sas/backoffice/modules/configurations/models/course.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { DocumentTypeModel } from "@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model";
import { AppointmentTypeModel } from "../../appointment-type/models/appointment-type.model";
import { DoctorModel } from "../../doctors/models/doctor.model";
import { SpecialtyModel } from "../../specialty/models/specialty.model";

export class AppointmentModel{
  id?: number;
  user_id: number;
  corporate_id: number;
  student_number: string;
  name: string;
  gender: string
  email: string;
  mobile_phone_number: string;
  telephone_number: string;
  document_type_id: number;
  identification_number: number;
  nif: string;
  niss: string;
  address: string;
  postal_code: string;
  city: string;
  nationality: string;
  course_id: number;
  course_year: number;
  department_id: number;
  organic_unit_id: number;
  specialty_id: number;
  date: Date
  start_hour: string;
  end_hour: string;
  has_scholarship: boolean;
  notes: string;
  allow_historic: boolean;
  appointment_type_id: number;
  tariff_id: number;
  doctor_id: number;
  attachments_ids:[];
  course?: CourseModel;
  organicUnit?: OrganicUnitsModel;
  documentType?: DocumentTypeModel;
  specialty?: SpecialtyModel;
  status: string;
  price: number;
  payment_type?: string;
  doctor?: DoctorModel;
  appointment_type?: AppointmentTypeModel;
  birth_date?: Date;
}

export const AppointmentStatusTag = {
    'CREATED': { label: 'Criação', color: 'blue' },
    'APPROVED': { label: 'Marcada', color: 'green' },
    'PENDING': { label: 'Por Confirmar', color: 'yellow' },
    'MISSED': { label: 'Faltou', color: 'red' },
    'CLOSED': { label: 'Consulta Realizada', color: 'red' },
    'CANCELED': { label: 'Cancelada', color: 'orange' },
    'CHANGED': { label: 'Alterada', color: 'purple' },
  };

  export class AppointmentChangeModel {
    date: Date;
    start_hour: string;
    end_hour: string;
    observations: string;
  };

  export class AppointmentHistoricResponseModel {
    appointment: AppointmentModel;
    historic: AppointmentHistoricModel[];
  }

  export class AppointmentHistoricModel {
    appointment_id: number;
    created_at: Date
    date: Date
    doctor_id: number
    end_hour: string
    id: number
    observations: string
    specialty_id: number
    start_hour: string
    status: string
    updated_at: Date
  }