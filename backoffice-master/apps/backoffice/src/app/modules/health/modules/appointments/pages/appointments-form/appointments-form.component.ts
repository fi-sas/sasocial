import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { SpecialtiesFormModel } from '../../models/appointment-form.model';
import { Location } from '@angular/common';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import * as moment from 'moment';
import { PriceVariationsModel } from '../../../specialty/models/price-variations.model';
import { AppointmentModel } from '../../models/appointment.model';
import { AppointmentsService } from '../../services/appointments.service';
import { finalize, first } from 'rxjs/operators';



@Component({
  selector: 'fi-sas-appointments-form',
  templateUrl: './appointments-form.component.html',
  styleUrls: ['./appointments-form.component.less']
})
export class AppointmentsFormComponent implements OnInit {

  step = 0;
  loading = false;
  buttonActionName = 'Seguinte';

  idToUpdate = null;

  loadingAppointment = false;
  appointment: AppointmentModel;
  specialty: SpecialtyModel;

  arrayValues: {
    specialties: SpecialtyModel[];
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: PriceVariationsModel[];
  } = null;

  @ViewChild('specialtyForm', null) specialtyForm: SpecialtiesFormModel;
  @ViewChild('userForm', null) userForm: SpecialtiesFormModel;
  @ViewChild('attachmentsForm', null) attachmentsForm: SpecialtiesFormModel;
  @ViewChild('paymentForm', null) paymentForm: SpecialtiesFormModel;
  @ViewChild('modalBox', null) buy;

  steps = [
    {
      name: 'specialtyForm',
      formValues: {},
    },
    {
      name: 'userForm',
      formValues: {},
    },
    {
      name: 'attachmentsForm',
      formValues: {},
    },
    {
      name: 'paymentForm',
      formValues: {},
    },
  ];

  constructor(
    private uiService: UiService,
    private router: Router,
    private appointmentService: AppointmentsService,
  ) { }

  ngOnInit() {}

  getData(raw: object, fields: string[]) {
    const data = {};
    fields.forEach((key) => (data[key] = raw[key]));
    return data;
  }

  nextStep(): void {
    if (this.step < this.steps.length) {
      const form = this[this.steps[this.step].name].submitForm();
      if (form !== null) {
        if (this.step === 0) {
          this.arrayValues = this[this.steps[this.step].name].getArrayValues();
        }
        this.steps[this.step].formValues = form;
        this.step++;
        this.buttonActionName = this.step === 4 ? 'Submeter' : 'Seguinte';
      }
    } else {
      this.submitAppointment();
    }
  }

  previousStep(): void {
    if (this.step > 0) {
      this.step--;
      this.buttonActionName = this.step === 4 ? 'Submeter' : 'Seguinte';
    }
  }

  stepPosition(position: number) {
    this.step = position;
  }

  submitAppointment() {
    this.loading = true;
    this.appointment = {} as AppointmentModel;
    this.steps.forEach((step) => {
      this.appointment = Object.assign(this.appointment, step.formValues);
    })
    this.specialty = this.arrayValues.specialties.find(s => s.id === this.appointment.specialty_id);
    if(this.appointment && this.appointment.price > 0 && this.appointment.payment_type === "Pré Pagamento"){
      this.buy.showBuyModal();
    }else if(this.appointment && (this.appointment.price === 0 ||this.appointment.payment_type === "Ato Consulta")){
      this.confirmAppointment(true);
    }


  }

  returnButton() {
    this.router.navigateByUrl('health/appointments/list');
}


  changeStep(event) {
    if(event>=0) {
      this.step = event;
      this.buttonActionName = 'Seguinte';
    }
  }

  confirmAppointment(event: any){
    if(event){
      this.appointmentService.create(this.appointment).pipe(first(),finalize(() =>this.loading = false)).subscribe(
        result => {
          this.uiService.showMessage(
            MessageType.success,
            'Marcação criada com sucesso'
          );
          this.returnButton();
        },
      )
    }else{
      this.loading = false;
    }
  }
}