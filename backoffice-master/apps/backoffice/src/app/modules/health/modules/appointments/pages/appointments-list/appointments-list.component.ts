import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { AppointmentsService } from '../../services/appointments.service';
import * as moment from 'moment';
import { AppointmentChangeModel, AppointmentHistoricResponseModel, AppointmentModel, AppointmentStatusTag } from '../../models/appointment.model';
import { EventObject, FullCalendarOptions } from 'ngx-fullcalendar';
import { EventModel } from '../../../schedules/models/schedule.model';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { SpecialtyService } from '../../../specialty/services/specialty.service';
import { finalize, first } from 'rxjs/operators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppointmentTypeService } from '../../../appointment-type/services/appointment-type.service';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { status_appointment } from '@fi-sas/backoffice/modules/health/utils/health.util';


@Component({
  selector: 'fi-sas-appointments-list',
  templateUrl: './appointments-list.component.html',
  styleUrls: ['./appointments-list.component.less']
})
export class AppointmentsListComponent  extends TableHelper implements OnInit  {

  options: FullCalendarOptions;
  eventsInCalendar: EventObject[] = [];
  events: EventModel[];
  event: EventModel = null;
  infoModal: NzModalRef;
  selectedAppointment: AppointmentModel;
  schedulesLoading = false;

  appointmentHistoric: AppointmentHistoricResponseModel;

  appointmentTypes: AppointmentTypeModel[] = [];
  appointmentTypesLoading = false;

  specialties: SpecialtyModel[] = [];
  specialtiesLoading = false;

  doctors: DoctorModel[] = [];
  doctorsLoading = false;
  
  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();

  changeAppointmentForm = new FormGroup({
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
    observations: new FormControl(null, Validators.required),
  })

  get f() { return this.changeAppointmentForm.controls; }
  submitted = false;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    private appointmentService: AppointmentsService,
    private specialtyService: SpecialtyService,
    private modalService: NzModalService,
    private appointmentTypeService: AppointmentTypeService,
    private doctorService: DoctorsService,
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'status',
        label: 'Estado',
        sortable: false,
        tag: AppointmentStatusTag,
      },
      {
        key: '',
        label: 'Nº Aluno/Corporate',
        sortable: false,
        template: (data) => {
          return data.student_number? data.student_number : data.corporate_id;
        },
      },
      {
        key: 'name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: 'niss',
        label: 'NISS',
        sortable: false,
      },
      {
        key: '',
        label: 'Data da Consulta',
        sortable: false,
        template: (data) => {
          return moment(data.date).format('YYYY-MM-DD');
        },
      },
      {
        key: 'start_hour',
        label: 'Hora Inicio',
        sortable: false,
      },
      {
        key: 'end_hour',
        label: 'Hora Fim',
        sortable: false,
      },
      {
        key: '',
        label: 'Duração da Consulta',
        sortable: false,
        template: (data) => {
          const avarageTime = moment(data.end_hour, "HH:mm:ss").diff( moment(data.start_hour, "HH:mm:ss"), "minutes")
          return avarageTime.toString();
        },
      },
      {
        key: 'specialty.translations.name',
        label: 'Especialidades',
        sortable: false,
        // @ts-ignore
        template: (data) => {
          return data.specialty.translations.find((t) => t.language_id === 3).name;
        },
      },
    );
  }


  ngOnInit() {

    this.options = {
      defaultDate: new Date(),
   
      editable: false,
      locale: 'pt',
      droppable: false,
      eventResizableFromStart: true,
    };
    this.getAppointmentTypes();
    this.getSpecialties();
    this.getDoctors();
    this.initTableData(this.appointmentService);
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list(1, -1).pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypes = results.data
      });
  }

  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialties = results.data;
      });
  }

  getDoctors(){
    this.doctorsLoading = true;
    this.doctorService.list(1, -1).pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctors = results.data
      });
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

    absenceAppointment(id: number){
      if(!this.authService.hasPermission('health:appointment:absence')){
        return;
      }
        this.uiService
        .showConfirm('Sim', 'Pretende marcar falta?', 'Sim')
        .subscribe(result => {
          if (result) {
            this.appointmentService.absence(id).subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Falta marcada'
              );
              this.initTableData(this.appointmentService);
            });
          }
        });
    }

    approveRequest(id: number){
      if(!this.authService.hasPermission('health:appointment:approve-request')){
        return;
      }
        this.uiService
        .showConfirm('Aprovar', 'Pretende aprovar o pedido de marcação?', 'Aprovar')
        .subscribe(result => {
          if (result) {
            this.appointmentService.approveRequest(id).subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Marcação Aprovada'
              );
              this.initTableData(this.appointmentService);
            });
          }
        });
    }

    changeAppointment(id: number, tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>){
        this.appointmentService.read(id).subscribe((result) => {
          this.selectedAppointment = result.data[0];
          this.schedulesLoading = true;
          if(!this.selectedAppointment.specialty.is_external){
            this.specialtyService.getScheduleDoctor(this.selectedAppointment.specialty_id, this.selectedAppointment.doctor_id).pipe(
              first(),
              finalize(() => this.schedulesLoading = false)).subscribe(results => {
                this.events = results.data;
                this.events.forEach(event => {
                  if(event.is_holiday){
                    this.eventsInCalendar.push({
                      id: event.id,
                      title: event.title,
                      start: event.start_date,
                      allDay: true,
                    });
                  }else{
                    this.eventsInCalendar.push({
                      id: event.id,
                      title: event.title,
                      start: event.start_date,
                      end: event.end_date,
                    });  
                  }
                  
                });
              });
          }
          this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
        });
    }

    disableDate = (endDate: Date) => {
      const currentDate = new Date();
      currentDate.setDate(currentDate.getDate());
      return moment(endDate).isSameOrBefore(currentDate);
    }

    cancelAppointment(id: number){
      if(!this.authService.hasPermission('health:appointment:cancel')){
        return;
      }
        this.uiService
        .showConfirm('Cancelar', 'Pretende cancelar a marcação?', 'Cancelar')
        .subscribe(result => {
          if (result) {
            this.appointmentService.cancel(id).subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Marcação Cancelada'
              );
              this.initTableData(this.appointmentService);
            });
          }
        });
    }

    generateCertificate(id: number){
      if(!this.authService.hasPermission('health:appointment:certificate')){
        return;
      }
        this.uiService
        .showConfirm('Emitir', 'Pretende emitir o certificado de presença?', 'Emitir')
        .subscribe(result => {
          if (result) {
            this.appointmentService.cancel(id).subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Certificado de presença emitido com sucesso'
              );
              this.initTableData(this.appointmentService);
            });
          }
        });
    }

    openHistoric(id: number, tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>){
      this.appointmentHistoric = null;
      this.appointmentService.getHistoric(id).subscribe((result) => {
        this.appointmentHistoric = result.data[0];
        this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
      });
    }

    selectAppointmentSchedule(eventSelect: any){
      this.event = this.events.find( e => e.id=== eventSelect.id);
      if(this.event && this.event.is_available){
        const date = moment(eventSelect.start).format("YYYY-MM-DD");
        const start_hour = eventSelect.start;
        const end_hour = moment(eventSelect.end).format("HH:mm:ss");
        this.changeAppointmentForm.patchValue({
          date: date,
          start_hour: start_hour,
          end_hour: end_hour,
          //overlapAppointment: false
        })
      }
      /*else if(this.event && !this.event.is_available){
        this.event = this.events.find(e => e.id === eventSelect.id);
        this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
      }*/
    }

    saveAppointmentChanged(){
      if(!this.authService.hasPermission('health:appointment:change')){
        return;
      }
      if (this.changeAppointmentForm.valid) {
        let obj = new AppointmentChangeModel;        
          obj.date = this.changeAppointmentForm.controls.date.value;
          obj.start_hour = moment(this.changeAppointmentForm.controls.start_hour.value).format("HH:mm:ss");
          obj.end_hour = this.changeAppointmentForm.controls.end_hour.value ? this.changeAppointmentForm.controls.end_hour.value :moment(this.changeAppointmentForm.controls.start_hour.value).add(this.selectedAppointment.specialty.avarage_time, "minutes").format("HH:mm:ss");
          obj.observations = this.changeAppointmentForm.controls.observations.value; 
        this.uiService
        .showConfirm('Alterar', 'Pretende alterar a marcação?', 'Alterar')
        .subscribe(result => {
          if (result) {
            this.appointmentService.changeAppointment(this.selectedAppointment.id, obj).subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Marcação Alterada com sucesso'
              );
              this.closeEventModal();
              this.initTableData(this.appointmentService);
            });
          }
        });
      }
    }

    closeEventModal() {
      this.eventsInCalendar = [];
      this.changeAppointmentForm.reset();
      this.infoModal.close();
    }

    createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>): NzModalRef {
      return this.modalService.create({
        nzTitle: tplTitle,
        nzContent: tplContent,
        nzFooter: tplFooter,
        nzMaskClosable: false,
        nzClosable: true,
        nzWidth: 900
      });
    }

    getAppointmentStatus(status){
      return status_appointment.find(s => s.status === status).badge.label
    }

    getAppointmentBadgeColor(status){
      return status_appointment.find(s => s.status === status).badge.color
    }

    generateCalendarExport(id: number){
      this.appointmentService.read(id).pipe().subscribe(response => {
        const appointment = response.data[0];
        if(appointment){
          let filename = appointment.specialty.translations[0].name + "_" + appointment.name + "_" + moment(appointment.date).format("YYYY-MM-DD") + "_"+ moment(appointment.start_hour, "HH:mm:ss") +".ics"
          this.appointmentService.generateCalendarExport(id, filename).pipe().subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Calendário criado com sucesso'
            );
          });
        }
      });
    }

}
