import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { ComplaintModel } from '../../complaints/models/complaint.model';
import { AppointmentChangeModel, AppointmentHistoricResponseModel, AppointmentModel } from '../models/appointment.model';

@Injectable({
  providedIn: 'root'
})
export class AppointmentsService extends Repository<AppointmentModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'HEALTH.APPOINTMENT';
    this.entity_url = 'HEALTH.APPOINTMENT_ID';
  }

  cancel(id: number): Observable<Resource<AppointmentModel>> {
    return this.resourceService.create<AppointmentModel>(
      this.urlService.get('HEALTH.CANCEL_APPOINTMENT', {
        id,
      }),{});
  }

  absence(id: number): Observable<Resource<AppointmentModel>> {
    return this.resourceService.create<AppointmentModel>(
      this.urlService.get('HEALTH.ABSENCE_APPOINTMENT', {
        id,
      }),{});
  }

  approveRequest(id: number): Observable<Resource<AppointmentModel>> {
    return this.resourceService.create<AppointmentModel>(
      this.urlService.get('HEALTH.APPROVE_REQUEST_APPOINTMENT', {
        id,
      }),{});
  }

  generateAttendanceCertificate(id: number): Observable<Resource<AppointmentModel>> {
    return this.resourceService.read<AppointmentModel>(
      this.urlService.get('HEALTH.APPOINTMENT_ATTENDANCE_CERTIFICATE', {
        id,
      }),{});
  }

  changeAppointment(id: number, newAppointment: AppointmentChangeModel): Observable<Resource<AppointmentChangeModel>> {
    return this.resourceService.create<AppointmentChangeModel>(
      this.urlService.get('HEALTH.CHANGE_APPOINTMENT', {
        id,
      }),{...newAppointment});
  }

  getHistoric(id: number): Observable<Resource<AppointmentHistoricResponseModel>> {
    return this.resourceService.list<AppointmentHistoricResponseModel>(
      this.urlService.get('HEALTH.HISTORIC_APPOINTMENT', {
        id,
      }),{});
  }

  answerComplaint(id: number, form: any): Observable<Resource<ComplaintModel>> {
    return this.resourceService.create<ComplaintModel>(
      this.urlService.get('HEALTH.ANSWER_COMPLAINT', {
        id,
      }),{ ...form });
  }

  generateCalendarExport(id: number, filename: string){
    const url = this.urlService.get('HEALTH.CALENDAR_EXPORT', {id})
    return this.resourceService.http.get(url, {}).pipe(first(), map((data => {
      this.downLoadFile(data, filename);
    })));
  }

  downLoadFile(file, filenameEvent) {
    let filename = filenameEvent ? filenameEvent : "event.ics";
    const downloadLink = document.createElement("a");
    downloadLink.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(file);
    downloadLink.download = filename;
    console.log(downloadLink)
    downloadLink.click();
  }

}
