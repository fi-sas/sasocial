import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplaintRoutingModule } from './complaint-routing.module';
import { ComplaintListComponent } from './pages/complaint-list/complaint-list.component';
import { ViewComplaintComponent } from './components/view-complaint/view-complaint.component';


@NgModule({
  declarations: [
  ComplaintListComponent,
  ViewComplaintComponent
],
  imports: [
    CommonModule,
    ComplaintRoutingModule,
    SharedModule
  ]
})
export class ComplaintModule { }
