import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { AppointmentsService } from '../../../appointments/services/appointments.service';
import { ComplaintModel } from '../../models/complaint.model';

@Component({
  selector: 'fi-sas-view-complaint',
  templateUrl: './view-complaint.component.html',
  styleUrls: ['./view-complaint.component.less']
})
export class ViewComplaintComponent implements OnInit {
  
  @Input() complaint: ComplaintModel = null;
  @Output() refresh = new EventEmitter<boolean>();
  
  submitted = false;
  isLoading = false;
  complaintResponseForm = new FormGroup({
    complaint_answer: new FormControl(null),
  });

  constructor(
    private appointmentService: AppointmentsService,
    private uiService: UiService, 
    private ref: ChangeDetectorRef,
  ) { }

  ngOnInit() {
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  submit() {
    this.submitted = true;
    if (this.complaintResponseForm.valid) {
      this.isLoading = true;
      this.appointmentService.answerComplaint(this.complaint.appointment.id, this.complaintResponseForm.value)
        .pipe(
          first(),
          finalize(() =>this.isLoading = false))
        .subscribe(result => {
          this.uiService.showMessage(
            MessageType.success,
              'Reclamação Respondida com sucesso'
            );
            this.refresh.emit(true);
            this.ref.detectChanges();
        })
    }
  }

}
