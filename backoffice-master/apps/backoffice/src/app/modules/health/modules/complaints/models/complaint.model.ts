import { AppointmentModel } from "../../appointments/models/appointment.model";

export class ComplaintModel{
    id?: number;
    observation?: string;
    files_ids?: [];
    complaint_answer?: string;
    appointment?: AppointmentModel;
    status?: string;
    created_at?: Date;
}

export const ComplaintStatusTag = {
    'CREATED': { label: 'Não Respondido', color: 'red' },
    'SOLVED': { label: 'Respondido', color: 'green' },
  };
