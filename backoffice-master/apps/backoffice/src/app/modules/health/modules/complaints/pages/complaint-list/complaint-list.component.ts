import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeModel } from '../../../appointment-type/models/appointment-type.model';
import { AppointmentTypeService } from '../../../appointment-type/services/appointment-type.service';
import { AppointmentStatusTag } from '../../../appointments/models/appointment.model';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { SpecialtyService } from '../../../specialty/services/specialty.service';
import { ComplaintStatusTag } from '../../models/complaint.model';
import { ComplaintsService } from '../../services/complaints.service';

@Component({
  selector: 'fi-sas-complaint-list',
  templateUrl: './complaint-list.component.html',
  styleUrls: ['./complaint-list.component.less']
})
export class ComplaintListComponent extends TableHelper implements OnInit {

  appointmentTypes: AppointmentTypeModel[] = [];
  appointmentTypesLoading = false;

  specialties: SpecialtyModel[] = [];
  specialtiesLoading = false;

  doctors: DoctorModel[] = [];
  doctorsLoading = false;
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private appointmentTypeService: AppointmentTypeService,
    private doctorService: DoctorsService,
    private specialtyService: SpecialtyService,
    public complaintService: ComplaintsService
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'appointment.status',
        label: 'Estado Consulta',
        sortable: false,
        tag: AppointmentStatusTag,
      },
      {
        key: '',
        label: 'Nº Aluno/Corporate',
        sortable: false,
        template: (data) => {
          return data.appointment.student_number? data.appointment.student_number : data.appointment.corporate_id;
        },
      },
      {
        key: 'appointment.name',
        label: 'Nome',
        sortable: false
      },
      {
        key: 'appointment.niss',
        label: 'NISS',
        sortable: false
      },
      {
        key: 'appointment.specialty.translations.name',
        label: 'Especialidade',
        sortable: false,
        // @ts-ignore
        template: (data) => {
          return data.appointment.specialty.translations.find((t) => t.language_id === 3).name;
        },
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ComplaintStatusTag,
      },
    );
  }

  ngOnInit() {
    this.getAppointmentTypes();
    this.getSpecialties();
    this.getDoctors();
    this.persistentFilters['searchFields'] = 'description';
    this.initTableData(this.complaintService);
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list(1, -1).pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypes = results.data
      });
  }

  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialties = results.data;
      });
  }

  getDoctors(){
    this.doctorsLoading = true;
    this.doctorService.list(1, -1).pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctors = results.data
      });
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  refresh(event) {
    if(event){
      this.initTableData(this.complaintService);
    }
  }

  answer(id: number){

  }
  
}
