import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { ComplaintModel } from '../models/complaint.model';

@Injectable({
  providedIn: 'root'
})
export class ComplaintsService extends Repository<ComplaintModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'HEALTH.COMPLAINT';
  }
}
