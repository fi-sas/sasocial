import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationsFormComponent } from './pages/configurations-form/configurations-form.component';


const routes: Routes = [
  { path: '', redirectTo: 'form', pathMatch: 'full' },
  {
    path: 'form',
    component: ConfigurationsFormComponent,
    data: { breadcrumb: 'Configurações', title: 'Configurações', scope: 'health:configurations:create' },  
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }