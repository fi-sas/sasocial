import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationsRoutingModule } from './configuration-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ConfigurationService } from './services/configuration.service';
import { ConfigurationsFormComponent } from './pages/configurations-form/configurations-form.component';

@NgModule({
  declarations: [
    ConfigurationsFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationsRoutingModule
  ],
  providers: [
    ConfigurationService
  ]
})
export class HealthConfigurationsModule { }
