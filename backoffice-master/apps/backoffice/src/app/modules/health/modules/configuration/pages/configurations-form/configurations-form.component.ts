import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';
import { finalize, first } from 'rxjs/operators';
import { ConfigurationModel } from '../../models/configurations.model';
import { ConfigurationService } from '../../services/configuration.service';

@Component({
  selector: 'fi-sas-configurations-form',
  templateUrl: './configurations-form.component.html',
  styleUrls: ['./configurations-form.component.less']
})
export class ConfigurationsFormComponent implements OnInit {
  loading = false;
  loadingCurrentAccounts = false;
  submit = false;

  configurationForm = new FormGroup({
    current_account_id: new FormControl(null),
  });

  currentAccounts: AccountModel[] = [];
  
  constructor(
    private currentAccountService: CurrentAccountsService,
    private configurationsService: ConfigurationService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.getCurrentAccounts();
    this.getConfigurations()
  }

  getCurrentAccounts(){
    this.loadingCurrentAccounts = true;
    this.currentAccountService
      .list(1, -1, 'name', 'ascend')
      .pipe(first(), finalize(() => this.loadingCurrentAccounts = false))
      .subscribe((results) => this.currentAccounts = results.data);
  }

  getConfigurations(){
    this.loading = true;
    this.configurationsService.list(1, -1). pipe(first(), finalize(() => this.loading = false)).subscribe(response => {
      this.configurationForm.get('current_account_id').setValue(response.data[0].CURRENT_ACCOUNT_ID ? Number(response.data[0].CURRENT_ACCOUNT_ID): null );
    })
  }

  submitForm(){
    this.loading = true;
    this.submit = true;
    let sendValue: ConfigurationModel = new ConfigurationModel();
    if (this.configurationForm.valid) {
      this.submit = false;
      sendValue.CURRENT_ACCOUNT_ID = this.configurationForm.get('current_account_id').value;
      this.configurationsService.create(sendValue).pipe(first(), finalize(() => this.loading = false)).subscribe(()=> {
        this.uiService.showMessage(MessageType.success, 'Configurações alteradas com sucesso');
      })
    } else {
      this.loading = false;
    }
  }

}
