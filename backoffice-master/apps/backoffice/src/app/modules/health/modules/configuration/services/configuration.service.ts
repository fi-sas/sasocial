import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { ConfigurationModel } from '../models/configurations.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService extends Repository<ConfigurationModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService );
    this.entities_url = 'HEALTH.CONFIGURATIONS';
  }
}
