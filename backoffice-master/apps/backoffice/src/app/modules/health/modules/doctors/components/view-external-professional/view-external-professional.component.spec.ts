import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExternalProfessionalComponent } from './view-external-professional.component';

describe('ViewExternalProfessionalComponent', () => {
  let component: ViewExternalProfessionalComponent;
  let fixture: ComponentFixture<ViewExternalProfessionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewExternalProfessionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExternalProfessionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
