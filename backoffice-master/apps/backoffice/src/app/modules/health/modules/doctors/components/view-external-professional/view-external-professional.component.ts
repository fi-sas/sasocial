import { Component, Input, OnInit } from '@angular/core';
import { DoctorExternalModel } from '../../models/doctor.model';

@Component({
  selector: 'fi-sas-view-external-professional',
  templateUrl: './view-external-professional.component.html',
  styleUrls: ['./view-external-professional.component.less']
})
export class ViewExternalProfessionalComponent implements OnInit {

  @Input() externalDoctor: DoctorExternalModel = null;

  constructor() { }

  ngOnInit() {
  }

}
