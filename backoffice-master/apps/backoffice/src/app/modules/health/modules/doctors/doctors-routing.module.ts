import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorsExternalFormComponent } from './pages/doctors-external-form/doctors-external-form.component';
import { DoctorsExternalListComponent } from './pages/doctors-external-list/doctors-external-list.component';
import { DoctorsInternalFormComponent } from './pages/doctors-internal-form/doctors-internal-form.component';
import { DoctorsListComponent } from './pages/doctors-list/doctors-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create-internal',
    component: DoctorsInternalFormComponent,
    data: { breadcrumb: 'Criar Interno', title: 'Criar', scope: 'health:doctor:create-internal' },

  },
  {
    path: 'create-external',
    component: DoctorsExternalFormComponent,
    data: { breadcrumb: 'Criar Externo', title: 'Criar', scope: 'health:doctor-external:create' },
  },
  {
    path: 'list-external',
    component: DoctorsExternalListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:doctor-external:read' }
  },
  {
    path: 'external/edit/:id',
    component: DoctorsExternalFormComponent,
    data: { breadcrumb: 'Editar Externo', title: 'Editar Externo', scope: 'health:doctor-external:create' }
  },
  {
    path: 'edit/:id',
    component: DoctorsInternalFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:doctor:create' }
  },
  {
    path: 'list',
    component: DoctorsListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:doctor:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule { }
