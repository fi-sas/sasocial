import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsListComponent } from './pages/doctors-list/doctors-list.component';
import { DoctorsRoutingModule } from './doctors-routing.module';
import { DoctorsInternalFormComponent } from './pages/doctors-internal-form/doctors-internal-form.component';
import { DoctorsExternalFormComponent } from './pages/doctors-external-form/doctors-external-form.component';
import { DoctorsExternalListComponent } from './pages/doctors-external-list/doctors-external-list.component';
import { ViewExternalProfessionalComponent } from './components/view-external-professional/view-external-professional.component';

@NgModule({
  declarations: [
    DoctorsListComponent,
    DoctorsInternalFormComponent,
    DoctorsExternalFormComponent,
    DoctorsExternalListComponent,
    ViewExternalProfessionalComponent
  ],
  imports: [
    CommonModule,
    DoctorsRoutingModule,
    SharedModule
  ]
})
export class DoctorsModule { }
