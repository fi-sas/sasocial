import { FileModel } from "@fi-sas/backoffice/modules/medias/models/file.model";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { SpecialtyModelTranslationModel } from "../../specialty/models/specialty.model";

export class DoctorModel{
    id: number;
    user?: UserModel;
    user_id: number;
    allow_historic: boolean;
    is_external: boolean;
    attachments_ids: []
    created_at: Date;
    update_at: Date;
    specialties_ids?: [];
}

export class DoctorExternalModel {
    id?: number;
    user_id?: number;
    user?: UserModel;
    file_id: number;
    file?: FileModel;
    name: string;
    email: string;
    gender: string;
    address: string;
    city: string;
    country: string;
    phone: string;
    tin: string;
    postal_code: string;
    attachments_ids?: number[];
  }

export class DoctorAttachementModel{
    id: number;
    doctor_id: number;
    file_id: number;
    created_at: Date;
    update_at: Date;
}

export class DoctorSpecialtyModel{
  id: number;
  avarage_time: number;
  translations: SpecialtyModelTranslationModel[];
  doctor_specialty_id: number;
}

export const Gender = {
    M: { value: 'M', label: 'Masculino' },
    F: { value: 'F', label: 'Feminino' },
    I: { value: 'U', label: 'Outro' },
  }