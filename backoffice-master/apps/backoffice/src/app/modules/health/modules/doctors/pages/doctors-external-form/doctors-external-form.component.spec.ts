import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsExternalFormComponent } from './doctors-external-form.component';

describe('DoctorsExternalFormComponent', () => {
  let component: DoctorsExternalFormComponent;
  let fixture: ComponentFixture<DoctorsExternalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsExternalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsExternalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
