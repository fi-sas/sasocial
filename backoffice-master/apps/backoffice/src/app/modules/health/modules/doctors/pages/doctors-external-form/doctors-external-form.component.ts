import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { countries } from '@fi-sas/backoffice/shared/common/countries';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { DoctorExternalModel, DoctorModel, Gender } from '../../models/doctor.model';
import { DoctorsExternalService } from '../../services/doctors-external.service';
import { cities } from '@fi-sas/backoffice/shared/common/cities';

@Component({
  selector: 'fi-sas-doctors-external-form',
  templateUrl: './doctors-external-form.component.html',
  styleUrls: ['./doctors-external-form.component.less']
})
export class DoctorsExternalFormComponent implements OnInit {
  doctorExt: DoctorExternalModel = null;
  countries = countries;
  genders = Object.values(Gender);
  file_ids = [];

  formCreate = new FormGroup({
    name: new FormControl(null, [Validators.required, trimValidation]),
    nif: new FormControl(null, [Validators.required]),
    gender: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.email, Validators.required]),
    phone: new FormControl(null, [Validators.required,trimValidation]),
    address: new FormControl(null, [Validators.required,trimValidation]),
    postal_code: new FormControl(null, [Validators.required]),
    city: new FormControl(null, [Validators.required,trimValidation]),
    country: new FormControl(null, [Validators.required]),
    allow_historic: new FormControl(true),
    attachments_ids: new FormControl(null, []),
  });

  submitted = false;
  get f() { return this.formCreate.controls; }
  id;
  isEdit = false;
  isLoading = false;
  cities = cities;
  
  constructor(private route: ActivatedRoute, private doctorExternalService: DoctorsExternalService, 
    private uiService: UiService, private router: Router) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
        this.isLoading = true;
        this.getProfessional(id);
        this.id = id;
        this.isEdit = true;
    }
  }

  getProfessional(id){
    let doctorExternal: DoctorExternalModel = new DoctorExternalModel();
    this.doctorExternalService
      .read(id)
      .pipe(
        first(),
        finalize(() => this.isLoading = false)
      )
      .subscribe((results) => {
        doctorExternal = results.data[0];
        this.formCreate.patchValue({
          ...doctorExternal
        });
        if(doctorExternal.attachments_ids){
          this.file_ids.push(...doctorExternal.attachments_ids);
        }
      }); 
  }

  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }

  submit() {
    this.submitted = true;

    if (this.formCreate.valid) {
      this.isLoading = true;
      this.formCreate.get("attachments_ids").setValue(this.file_ids);
        if (this.isEdit) {
            this.doctorExternalService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Profissional Externo alterado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }else {
            this.doctorExternalService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Profissional Externo criado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }
    }
  }

  returnButton() {
      this.router.navigateByUrl('health/professionals/list-external');
  }

}
