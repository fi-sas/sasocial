import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsExternalListComponent } from './doctors-external-list.component';

describe('DoctorsExternalListComponent', () => {
  let component: DoctorsExternalListComponent;
  let fixture: ComponentFixture<DoctorsExternalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsExternalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsExternalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
