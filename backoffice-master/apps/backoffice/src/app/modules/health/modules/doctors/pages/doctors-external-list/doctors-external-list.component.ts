import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { DoctorsExternalService } from '../../services/doctors-external.service';

@Component({
  selector: 'fi-sas-doctors-external-list',
  templateUrl: './doctors-external-list.component.html',
  styleUrls: ['./doctors-external-list.component.less']
})
export class DoctorsExternalListComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    public doctorExternalService: DoctorsExternalService
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'email',
        label: 'Email',
        sortable: true,
      },
      {
        key: 'is_active',
        label: 'Ativo',
        tag: TagComponent.YesNoTag
      },
      {
        key: 'is_validated',
        label: 'Validado',
        tag: TagComponent.YesNoTag
      },
    );
  }

  ngOnInit() {
    this.persistentFilters['searchFields'] = 'name';
    this.initTableData(this.doctorExternalService);
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  edit(id: number) {
    if(!this.authService.hasPermission('health:doctor-external:update')){
      return;
    }
    this.router.navigateByUrl('/health/professionals/external/edit/' + id);
  }

  delete(id: number) {
    if(!this.authService.hasPermission('health:doctor-external:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este profissional?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.doctorExternalService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Profissional eliminado com sucesso'
              );
              this.initTableData(this.doctorExternalService);
            });
          }
        });
    }

  validate(id: number, status: boolean) {
    if(!this.authService.hasPermission('health:doctor-external:validate')){
      return;
    }
      this.uiService
      .showConfirm('Validar', 'Pretende validar este profissional?', 'Validar')
      .subscribe(result => {
        if (result) {
          this.doctorExternalService.validate(id).subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Profissional Externo Validado com sucesso'
            );
            this.initTableData(this.doctorExternalService);
          });
        }
      });
    }
}
