import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsInternalFormComponent } from './doctors-internal-form.component';

describe('DoctorsInternalFormComponent', () => {
  let component: DoctorsInternalFormComponent;
  let fixture: ComponentFixture<DoctorsInternalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsInternalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsInternalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
