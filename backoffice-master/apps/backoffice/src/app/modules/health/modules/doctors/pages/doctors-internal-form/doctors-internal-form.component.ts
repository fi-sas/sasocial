import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, finalize, first, map, switchMap } from 'rxjs/operators';
import { DoctorModel } from '../../models/doctor.model';
import { DoctorsService } from '../../services/doctors.service';

@Component({
  selector: 'fi-sas-doctors-internal-form',
  templateUrl: './doctors-internal-form.component.html',
  styleUrls: ['./doctors-internal-form.component.less']
})
export class DoctorsInternalFormComponent implements OnInit {

  usersLoading = false;
  users: UserModel[] = [];
  isLoading= false;
  idToUpdate = null;
  submitted = false;
  isEdit= false;
  file_ids = [];
  doctor: DoctorModel = new DoctorModel();
  usersSearchChange$ = new BehaviorSubject('');
  
  formCreate = new FormGroup({
    user_id: new FormControl(null, [Validators.required]),
    allow_historic: new FormControl(true),
    attachments_ids: new FormControl(null, []),
  })

  get f() { return this.formCreate.controls; }

  constructor(
    private uiService: UiService,
    public route: ActivatedRoute,
    public router: Router,
    public doctorsService: DoctorsService,
    public userService: UsersService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
        this.isLoading = true;
        this.getDoctor(id);
        this.idToUpdate = id;
        this.isEdit = true;
    }
    this.getUsers();
  }
  
  getDoctor(id){
    this.doctorsService
      .read(id)
      .pipe(
        first(),
        finalize(() => this.isLoading = false)
      )
      .subscribe((results) => {
        this.doctor = results.data[0];
        this.users.push(this.doctor.user);
        this.doctor.attachments_ids.map(at => {
          this.onFileAdded(at);
        })
        this.formCreate.patchValue({
          ...this.doctor,
        });
        if(this.doctor.attachments_ids){
          this.file_ids.push(...this.doctor.attachments_ids);
        }
      });  
  }


  getUsers() {
    const loadUsers = (search: string): Observable<UserModel[]> =>
      this.userService
        .list(0, 100, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,student_number,email,identification',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.users = users;
      this.usersLoading = false;
    });
  }

  onSearchUsers(event) {
    this.usersLoading = true;
    this.usersSearchChange$.next(event);
  }

  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }
  
  submit() {
    this.submitted = true;
    if (this.formCreate.valid) {
      this.isLoading = true;
      this.formCreate.get("attachments_ids").setValue(this.file_ids);
      this.formCreate.addControl('is_external', new FormControl( this.doctor.is_external ?  this.doctor.is_external : false ));
        if (this.isEdit) {
            this.doctorsService.update(this.idToUpdate, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Profissional alterado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }else {
            this.doctorsService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Profissional criado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }


    }
  }

  returnButton() {
      this.router.navigateByUrl('health/professionals/list');
  }

}
