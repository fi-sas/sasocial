import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { DoctorsService } from '../../services/doctors.service';

@Component({
  selector: 'fi-sas-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.less']
})
export class DoctorsListComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    public doctorService: DoctorsService
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'user.name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'allow_historic',
        label: 'Historico',
        tag: TagComponent.YesNoTag
      },
      {
        key: 'is_external',
        label: 'Externo',
        tag: TagComponent.YesNoTag
      },
    );
    this.persistentFilters = {
      searchFields: 'name',
    };
  }

  ngOnInit() {
    this.initTableData(this.doctorService);
  }
  
  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  edit(id: number) {
    if(!this.authService.hasPermission('health:doctor:update')){
      return;
    }
    this.router.navigateByUrl('/health/professionals/edit/' + id);
  }

  delete(id: number) {
    if(!this.authService.hasPermission('health:doctor:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este profissional?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.doctorService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Profissional eliminado com sucesso'
              );
              this.initTableData(this.doctorService);
            });
          }
        });
    }

}
