import { TestBed } from '@angular/core/testing';

import { DoctorsExternalService } from './doctors-external.service';

describe('DoctorsExternalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DoctorsExternalService = TestBed.get(DoctorsExternalService);
    expect(service).toBeTruthy();
  });
});
