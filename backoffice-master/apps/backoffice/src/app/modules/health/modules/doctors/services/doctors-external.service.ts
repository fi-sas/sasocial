import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs/Observable';
import { DoctorExternalModel } from '../models/doctor.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorsExternalService extends Repository<DoctorExternalModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService,urlService);
    this.entities_url = 'HEALTH.DOCTOR_EXTERNAL';
    this.entity_url = 'HEALTH.DOCTOR_EXTERNAL_ID';
   }

   
  validate(id: number): Observable<Resource<DoctorExternalModel>> {
    return this.resourceService.create<DoctorExternalModel>(
      this.urlService.get('HEALTH.VALIDATE_DOCTOR_EXTERNAL', {
        id,
      }),{});
  }

}
