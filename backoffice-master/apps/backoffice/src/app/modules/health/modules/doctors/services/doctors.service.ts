import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs-compat/Observable';
import { PeriodModel } from '../../schedules/models/period.model';
import { EventModel } from '../../schedules/models/schedule.model';
import { DoctorExternalModel, DoctorModel, DoctorSpecialtyModel } from '../models/doctor.model';


@Injectable({
  providedIn: 'root'
})
export class DoctorsService extends Repository<DoctorModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService,urlService);
    this.entities_url = 'HEALTH.DOCTOR';
    this.entity_url = 'HEALTH.DOCTOR_ID';
  }

  createExternal(doctor: any): Observable<Resource<DoctorExternalModel>> {
    return this.resourceService.create<DoctorExternalModel>(
      this.urlService.get('HEALTH.CREATE_DOCTOR_EXTERNAL', {}), { ...doctor });
  }

  getSpecialties(id: any): Observable<Resource<DoctorSpecialtyModel>> {
    return this.resourceService.list<DoctorSpecialtyModel>(this.urlService.get('HEALTH.DOCTOR_GET_SPECIALTIES', {id}));
  }

  updateSchedule(id: any, periodSubmit: PeriodModel): Observable<Resource<PeriodModel>> {
    return this.resourceService.create<PeriodModel>(
      this.urlService.get('HEALTH.UPDATE_DOCTOR_SCHEDULE', {id}), { ...periodSubmit });
  }

  getPeriod(id: any): Observable<Resource<PeriodModel>> {
    return this.resourceService.read<PeriodModel>(
      this.urlService.get('HEALTH.GET_DOCTOR_PERIOD', {id}), {});
  }

  createSchedule(periodSubmit: PeriodModel): Observable<Resource<PeriodModel>> {
    return this.resourceService.create<PeriodModel>(
      this.urlService.get('HEALTH.CREATE_DOCTOR_SCHEDULE', {}), { ...periodSubmit });
  }

  deleteSchedule(id: number) {
    return this.resourceService.delete(this.urlService.get('HEALTH.DELETE_DOCTOR_SCHEDULE', {id}));
  }

  getCalendarBySpecialty(doctor_specialty_id:number): Observable<Resource<EventModel>> {
    let params = new HttpParams();
    params = params.set('doctor_specialty_id', doctor_specialty_id.toString());
    return this.resourceService.list<EventModel>(this.urlService.get('HEALTH.DOCTOR_GET_CALENDAR_BY_SPECIALTY', {}), { params });
  }

}
