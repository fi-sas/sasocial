export class PlaceModel {
    id?: number;
    name: string;
    email: string;
    address: string;
    postal_code: string;
    city: string;
    phone: number;
    fax: number;
    is_active: boolean;
    updated_at: Date;
    created_at: Date;
}

export const PlaceActiveInactive = {
    true: { label: 'Ativo', color: 'green' },
    false: { label: 'Inativo', color: 'red' },
  };