import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { PlaceModel } from '../../models/place.model';
import { PlaceService } from '../../services/place.service';
import { cities } from '@fi-sas/backoffice/shared/common/cities';

@Component({
  selector: 'fi-sas-places-form',
  templateUrl: './places-form.component.html',
  styleUrls: ['./places-form.component.less']
})
export class PlacesFormComponent implements OnInit {
  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
  '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
  '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
  '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
  '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  formCreate = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    address: new FormControl('', [Validators.required, trimValidation]),
    city: new FormControl('', [Validators.required, trimValidation]),
    postal_code: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, trimValidation, Validators.pattern(this.emailRegex)]),
    phone: new FormControl('',[
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(8),
    ]),
    fax: new FormControl('',[
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(8),
    ]),
    is_active: new FormControl(true)
  });
  
  submitted = false;
  get f() { return this.formCreate.controls; }
  id;
  isEdit = false;
  isLoading = false;
  cities = cities;
  
  constructor(private route: ActivatedRoute, private placeService: PlaceService, 
    private uiService: UiService, private router: Router) {
    
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
        this.isLoading = true;
        this.getPlace(id);
        this.id = id;
        this.isEdit = true;
    }
  }

  getPlace(id){
    let place: PlaceModel = new PlaceModel();
    this.placeService
      .read(id)
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((results) => {
        place = results.data[0];
        this.formCreate.patchValue({
          ...place
        });
      });  
  }

  submit() {
    this.submitted = true;
    if (this.formCreate.valid) {
      this.isLoading = true;
        if (this.isEdit) {
            this.placeService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Local alterado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }else {
            this.placeService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Local criado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }
    }
  }

  returnButton() {
      this.router.navigateByUrl('health/place/list');
  }

}
