import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { PlaceActiveInactive } from '../../models/place.model';
import { PlaceService } from '../../services/place.service';
import { cities } from '@fi-sas/backoffice/shared/common/cities';

@Component({
  selector: 'fi-sas-places-list',
  templateUrl: './places-list.component.html',
  styleUrls: ['./places-list.component.less']
})
export class PlacesListComponent extends TableHelper implements OnInit {

  cities = cities;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    public placeService: PlaceService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'name,address,city,is_active'
    };
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true
      },
      {
        key: 'address',
        label: 'Morada',
        sortable: true
      },
      {
        key: 'city',
        label: 'Cidade',
        sortable: true
      },
      {
        key: 'postal_code',
        label: 'Código Postal',
        sortable: false
      },
      {
        key: 'is_active',
        label: 'Estado',
        tag: PlaceActiveInactive,
        sortable: false,
      }
    );
  }

  ngOnInit() {
    this.initTableData(this.placeService);
  }

  listComplete() {
    this.filters.active = null;
    this.filters.city = null;
    this.filters.address = null;
    this.filters.search = null;
    this.searchData(true)
  }

  edit(id: number) {
    if(!this.authService.hasPermission('health:place:update')){
      return;
    }
      this.router.navigateByUrl('/health/place/edit/' + id);
  }

  status(id: number, status: boolean) {
    if(!this.authService.hasPermission('health:place:status')){
      return;
    }
    if(status){
      this.uiService
      .showConfirm('Ativar', 'Pretende ativar este local?', 'Ativar')
      .subscribe(result => {
        if (result) {
          this.placeService.status(id, status).subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Local ativado com sucesso'
            );
            this.initTableData(this.placeService);
          });
        }
      });
    }else{
      this.uiService
      .showConfirm('Desativar', 'Pretende desativar este local?', 'Desativar')
      .subscribe(result => {
        if (result) {
          this.placeService.status(id, status).subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Local desativado com sucesso'
            );
            this.initTableData(this.placeService);
          });
        }
      });
    }
  }

  delete(id: number) {
    if(!this.authService.hasPermission('health:place:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este local?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.placeService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Local eliminado com sucesso'
              );
              this.initTableData(this.placeService);
            });
          }
        });
    }


}
