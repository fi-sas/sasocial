import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacesFormComponent } from './pages/places-form/places-form.component';
import { PlacesListComponent } from './pages/places-list/places-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: PlacesFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:place:create' }
  },
  {
    path: 'edit/:id',
    component: PlacesFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:place:create' }
  },
  {
    path: 'list',
    component: PlacesListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:place:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaceRoutingModule { }
