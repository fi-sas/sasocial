import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlacesFormComponent } from './pages/places-form/places-form.component';
import { PlacesListComponent } from './pages/places-list/places-list.component';
import { PlaceRoutingModule } from './place-routing.module';


@NgModule({
  declarations: [
    PlacesFormComponent,
    PlacesListComponent
  ],
  imports: [
    CommonModule,
    PlaceRoutingModule,
    SharedModule
  ]
})
export class PlaceModule { }
