import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { PlaceModel } from '../models/place.model';

@Injectable({
  providedIn: 'root'
})
export class PlaceService extends Repository<PlaceModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService,urlService);
    this.entities_url = 'HEALTH.PLACE';
    this.entity_url = 'HEALTH.PLACE_ID';
  }

  status(id: number, is_active: boolean): Observable<Resource<PlaceModel>> {
    return this.resourceService.create<PlaceModel>(
      this.urlService.get('HEALTH.PLACE_STATUS', {
        id,
      }),
      {is_active}
    );
  }

}
