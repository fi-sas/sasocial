import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { NzDrawerRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';
import { SpecialtyService } from '../../../specialty/services/specialty.service';

@Component({
  selector: 'fi-sas-list-doctors',
  templateUrl: './list-doctors.component.html',
  styleUrls: ['./list-doctors.component.less']
})
export class ListDoctorsComponent extends TableHelper implements OnInit {

  specialtiesList: SpecialtyModel[] = [];
  specialtiesLoading = false;

  constructor(
    public doctorService: DoctorsService, 
    private specialtyService: SpecialtyService,
    uiService: UiService, 
    private drawerRef: NzDrawerRef,
    router: Router,
    activatedRoute: ActivatedRoute,
    ){
    super(uiService, router, activatedRoute); 
    this.persistentFilters = {
      searchFields: 'name,specialty_id'
    };
    this.columns.push(
      {
        key: 'user.name',
        label: 'Nome',
        sortable: true,
      },
  );
  }

  ngOnInit() {
    this.getSpecialties();
    this.initTableData(this.doctorService);
  }

  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialtiesList = results.data
      });
  }

  initTableData(repository: Repository<any>) {
    this.repository = repository;
    if (!this.repository) {
      throw new Error('No service provided!');
    }
    this.searchData(true);
}

  listComplete() {
    this.filters.specialty_id = null;
    this.filters.search = null;
    this.searchData(true);
  }

  selectUser(user){
    this.drawerRef.close(user);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

}
