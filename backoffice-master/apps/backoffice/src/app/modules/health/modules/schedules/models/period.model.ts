import { ScheduleModel } from "./schedule.model";

export class PeriodModel {
    id?: number;
    doctor_specialty_id?: number;
    doctor_id?: number;
    specialty_id?: number;
    schedules?: PeriodScheduleModel[];
    start_date: Date;
    end_date: Date
}

export class PeriodScheduleModel{
    start_date: Date;
    end_date: Date;
    day?: string
    days?: ScheduleModel[];
}