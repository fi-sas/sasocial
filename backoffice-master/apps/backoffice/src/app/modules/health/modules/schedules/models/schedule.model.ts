export class ScheduleModel{
    days: DaysModel[];
    hours?: HoursModel[];
    day: string
    end_hour?: string
    start_hour?: string
}

export class DaysModel{
    id: number;
    period_id: number;
    day: string;
    start_hour: string;
    end_hour: string;
}

export class HoursModel{
    start_hour: string;
    end_hour: string;  
}

export class EventModel{
    id: string;
    title: string;
    start_date: string;
    end_date: string;
    is_available: boolean;
    is_holiday: boolean;
}