import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { DoctorModel, DoctorSpecialtyModel } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { DayModel } from '../../models/day.model';
import { PeriodModel, PeriodScheduleModel } from '../../models/period.model';
import { HoursModel, ScheduleModel } from '../../models/schedule.model';
import { week_days } from '@fi-sas/backoffice/modules/health/utils/health.util';
import { SpecialtyService } from '../../../specialty/services/specialty.service';
import { SpecialtyModel } from '../../../specialty/models/specialty.model';

@Component({
  selector: 'fi-sas-schedule-form',
  templateUrl: './schedule-form.component.html',
  styleUrls: ['./schedule-form.component.less']
})
export class ScheduleFormComponent implements OnInit {

  specialtiesLoading = false;
  specialtiesList: SpecialtyModel[] = [];

  doctorsLoading = false;
  doctors: DoctorModel[] = [];
  doctorSelected: DoctorModel;

  
  specialtiesDoctorList: DoctorSpecialtyModel[]= [];

  submitModel: PeriodModel[] = [];

  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();


  formCreate = new FormGroup({
    doctor_id: new FormControl('', Validators.required),
    specialty_id: new FormControl('', Validators.required),
    schedules: new FormArray([])
  });

  get f() { return this.formCreate.controls; }
  schedules = this.formCreate.get('schedules') as FormArray;

  idToUpdate = null;
  submitted = false;
  isLoading = false;
  isEdit = false;
  startDate: Date;
  endDate: Date;
  availableWeekDays: DayModel[] = [];

  constructor(
    private uiService: UiService,
    public activateRoute: ActivatedRoute,
    public router: Router,
    private languagesService: LanguagesService,
    private doctorsService: DoctorsService,
    private specialtyService: SpecialtyService,
  ) { 
  }

  ngOnInit() {
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id) {
        this.isLoading = true;
        this.getPeriod(id);
        this.idToUpdate = id;
        this.isEdit = true;
    }else{
      this.addScheduleSection();
    }
    this.getDoctors();
    this.getSpecialties();
  }

  getPeriod(id){
    let period = new PeriodModel();
    this.doctorsService
    .getPeriod(id)
    .pipe(
      first(),
      finalize(() => this.isLoading = false)
    )
    .subscribe((result) => {
      period = result.data[0];
      let index = 0;
      let indexWeek = 0;
      
      this.formCreate.patchValue({
        doctor_id: period.doctor_id,
        specialty_id: period.specialty_id,
      })
      const schedulesForm = this.formCreate.controls.schedules as FormArray;
      for(const sch of period.schedules){
        schedulesForm.push(
          new FormGroup({
            dateRange: new FormControl([sch.start_date, sch.end_date] , [Validators.required]),
            days:new FormArray([])
          })
        );
        const weekDayControl = schedulesForm["controls"][index].get("days") as FormArray;
        for(const day of sch.days){
          weekDayControl.push(new FormGroup({
            day: new FormControl(day.day , [Validators.required]),
            hours: new FormArray([])
          }))
          const hourControl = weekDayControl['controls'][indexWeek].get('hours') as FormArray;
          for(const hour of day.days){
            hourControl.push(new FormGroup({
              start_hour: new FormControl(moment(hour.start_hour, "HH:mm:ss").toDate()),
              end_hour: new FormControl(moment(hour.end_hour, "HH:mm:ss").toDate()),
            }))
          }
          indexWeek++;
        }
        this.filterDaySelection(index);
        index++;
      }

    })
  }

  scheduleSection(){
    return new FormGroup({
      dateRange: new FormControl(null , [Validators.required]),
      days: new FormArray([
        this.daySection(),
      ])
    })
  }

  daySection(){
    return new FormGroup({
      day: new FormControl(null, Validators.required),
      hours: new FormArray([
        this.hourSection(),
      ])
    })
  }

  hourSection(){
    return new FormGroup({
      start_hour: new FormControl(null),
      end_hour: new FormControl(null),
    })
  }

  getDoctors(){
    this.doctorsLoading = true;
    this.doctorsService.list(1, -1).pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctors = results.data;
      });
  }

  addScheduleSection() {
    const schedules = this.formCreate.controls.schedules as FormArray;
    schedules.push(this.scheduleSection());
  }

  addDaySection(i) {
    const control = this.formCreate.get('schedules')['controls'][i].get('days') as FormArray;
    control.push(this.daySection());
  }

  addHourSection(i,j) {
    const control = this.formCreate.get('schedules')['controls'][i].get('days')['controls'][j].get('hours') as FormArray;
    control.push(this.hourSection());
  }

  deleteHourSection(i,j,k){
   const control = this.formCreate.get(['schedules',i,'days',j,'hours']) as FormArray;
   control.removeAt(k);
  }

  deleteDaySection(i, j){
    const control = this.formCreate.get('schedules')['controls'][i].get('days') as FormArray;
    control.removeAt(j);
  }

  deletePeriodSection(i){
    const control = this.formCreate.get('schedules') as FormArray;
    control.removeAt(i);
  }

  getSchedules(form) {
    return form.controls.schedules.controls;
  }

  getDays(form) {
    return form.controls.days.controls;
  }

  getHours(form) {
    return form.controls.hours.controls as FormGroup  
  }

  /*-----------------------------------------------------*/

  isDisabledDay(value:any , i){
    const days = this.formCreate.get('schedules')['controls'][i].get('days').value;
    return !days.find( d => d.day === value) && this.availableWeekDays.find(d => d.value === value) ? false : true
  }

  disabledDatePeriod = (current: Date) => {
    const end = new Date(this.endDate);
    end.setDate(end.getDate());
    return moment(current, "YYYY-MM-DD").isAfter(moment(end).format("YYYY-MM-DD")) || moment(current, "YYYY-MM-DD").isBefore(moment(this.startDate).format("YYYY-MM-DD"));
  }

  filterDaySelection(i){
    this.availableWeekDays=[];
    const dateRange = this.formCreate.get('schedules')['controls'][i].get('dateRange').value;
    const startDate = moment(dateRange[0]);
    const endDate =  moment(dateRange[1]);
    while(startDate.isSameOrBefore(endDate)){
      if( !this.availableWeekDays.find(a => a.day_number === startDate.day())){
        const dayModel = new DayModel();
        dayModel.value = week_days.find(wd => wd.day_number === startDate.day()).day
        dayModel.day = week_days.find(wd => wd.day_number === startDate.day()).translations.find( t => t.language_id === 3).day
        dayModel.day_number = startDate.day()
        this.availableWeekDays.push(dayModel)
    
      }
      startDate.add(1, 'd');
    }

    const daysPicked =  this.formCreate.get('schedules')['controls'][i].get('days') as FormArray;
    if(daysPicked.length > 0){
      for(const dayPicked of daysPicked.controls){
        if(dayPicked.value.day){
          const checkIfDayIsPicked = !this.availableWeekDays.find(d => d.value === dayPicked.value.day)
          if(checkIfDayIsPicked){
            dayPicked['controls'].day.setValue(null)
          }
        }
      }
    }
  }

  isSpecialtyDisabled(specialty_id){
    return this.specialtiesDoctorList.find( s => s.id === specialty_id) 
    && this.specialtiesList.find( s => s.id === specialty_id).is_active 
    && !this.specialtiesList.find( s => s.id === specialty_id).is_external ? false : true
  }

  checkHoursPeriod(i,j,k){

    const hoursPicked =  this.formCreate.get('schedules')['controls'][i].get('days')['controls'][j].get('hours') as FormArray;
      for(const hourPicked of hoursPicked.controls){
        console.log(hourPicked);
       /* if(dayPicked.value.day){
          const checkIfDayIsPicked = !this.availableWeekDays.find(d => d.value === dayPicked.value.day)
          if(checkIfDayIsPicked){
            dayPicked['controls'].day.setValue(null)
          }
        }
      }*/
    }

   /* const controlValue = 
    if()
    const control = this.formCreate.get('schedules')['controls'][i].get('days')['controls'][j].get('hours') as FormArray;
    const y = control.controls.map(c =>{
      console.log(c)
    })
    //console.log(control);
    //console.log(control.(k));*/
  }

  /*---------------------------------------*/
  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialtiesList = results.data
      });
  }

  getSpecialtiesByDoctor(){
    if(!this.isEdit){
      this.formCreate.get('specialty_id').setValue(null);
      this.doctorSelected = this.doctors.find((p) => p.id === this.formCreate.get('doctor_id').value);
      this.doctorsService.getSpecialties(this.doctorSelected.id).pipe(
        first(),
        finalize(() => this.specialtiesLoading = false))
      .subscribe(result => {
        this.specialtiesDoctorList = result.data;
      })
    }
  }
 
  clearSchedule(){
    const control = this.formCreate.get('schedules') as FormArray;
    control.clear()
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  submit() {
    this.submitted = true;
    if (this.formCreate.valid) {
      this.isLoading = true;

      if (this.isEdit) {
        this.doctorsService.updateSchedule(this.idToUpdate, this.fillSubmitObject()).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
          result => {
            this.uiService.showMessage(
              MessageType.success,
              'Horário alterado com sucesso'
            );
            this.returnButton();
          },
        );
      }else{
        this.doctorsService.createSchedule(this.fillSubmitObject()).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
          result => {
            this.uiService.showMessage(
              MessageType.success,
              'Horário criado com sucesso'
            );
            this.returnButton();
          },
        );
      }
    }
  }

  fillSubmitObject(){
    const periodsSchedule : PeriodScheduleModel[] = [];

    for(const period of this.formCreate.controls.schedules.value){
      const periodSchedule = new PeriodScheduleModel();
      periodSchedule.start_date = period.dateRange[0];
      periodSchedule.end_date = period.dateRange[1];

      const scheduleModelArray : ScheduleModel[] = [];
      for(const day of period.days){
        const hourModelArray : HoursModel[] = [];
        for(const hour of day.hours){
          const hourModel = new HoursModel();
          hourModel.start_hour = moment(hour.start_hour).format("HH:mm:ss");
          hourModel.end_hour = moment(hour.end_hour).format("HH:mm:ss");
          hourModelArray.push(hourModel);
        }
        const scheduleModel = new ScheduleModel();
        scheduleModel.day = day.day;
        scheduleModel.hours = hourModelArray;
        scheduleModelArray.push(scheduleModel)
      }
      periodSchedule.days = scheduleModelArray;
      periodsSchedule.push(periodSchedule);
    }
    const periodSubmit = new PeriodModel();
    periodSubmit.doctor_id = this.formCreate.controls.doctor_id.value;
    periodSubmit.specialty_id = this.formCreate.controls.specialty_id.value;
    periodSubmit.schedules = periodsSchedule;
    
    return periodSubmit;
  }

  returnButton() {
      this.router.navigateByUrl('health/schedules/list');
  }

}
