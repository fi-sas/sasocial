import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import * as moment from 'moment';
import { NzDrawerService, NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { DoctorModel, DoctorSpecialtyModel,  } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { ListDoctorsComponent } from '../../components/list-doctors/list-doctors.component';
import { PeriodModel } from '../../models/period.model';
import { SheduleService } from '../../services/shedule.service';
import { week_days } from '../../../../utils/health.util';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { EventObject, FullCalendarOptions } from 'ngx-fullcalendar';
import { EventModel } from '../../models/schedule.model';

@Component({
  selector: 'fi-sas-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.less']
})
export class ScheduleListComponent extends TableHelper implements OnInit {

  loadingInfo = false;
  isLoading = false;
  selectedUser: DoctorModel = new DoctorModel();

  selectedSpecialty: DoctorSpecialtyModel;
  specialtiesDoctorList: DoctorSpecialtyModel[]= [];

  periodsUserList :PeriodModel[]= [];
  tabIndex = 0;
  filter_createDate = null;
  week_days = week_days;
  
  options: FullCalendarOptions;
  eventsInCalendar: EventObject[] = [];
  events: EventModel[];
  event: EventModel = null;
  infoModal: NzModalRef;
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute, 
    private message: NzMessageService, 
    private modalService: NzModalService,
    private drawerService: NzDrawerService,
    private doctorsService: DoctorsService,
    private scheduleService: SheduleService,
    private authService: AuthService,
    ) {
      super(uiService, router, activatedRoute);
    }


  ngOnInit() {
    
    this.options = {
      defaultDate: new Date(),
      contentHeight: 300,
      editable: false,
      locale: 'pt',
      droppable: false,
      eventResizableFromStart: true,
    };
  }

  listComplete() {
    this.filters.day = null;
    this.filter_createDate = null;
    this.searchData(true)
  }

  updateFilters() {
    if (this.filter_createDate) {
        const temp = this.filter_createDate;
        this.filters['created_at'] = {};
        this.filters['created_at']['lte'] = moment(temp[0]).format('YYYY-MM-DD');
        this.filters['created_at']['gte'] = moment(temp[1]).format('YYYY-MM-DD');
    }
    this.searchData(true);
}

  modalSearch() {
    this.loadingInfo = true;
    const modalDrawer = this.drawerService.create({
        nzMask: true,
        nzMaskStyle: {
            'opacity': '0',
            '-webkit-animation': 'none',
            'animation': 'none',
        },
        nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
        nzContent: ListDoctorsComponent,
        nzWidth: (window.innerWidth / 24) * 12
     
    });
    modalDrawer.afterClose.pipe(first(),finalize(()=> this.loadingInfo =false)).subscribe((result) => {
        if (result) {
          this.selectedUser = result;
          this.specialtiesDoctorList = [];
          this.getPeriods(this.selectedUser.id);
        }
    });
  }

  getPeriods(id: number) {
    this.doctorsService.getSpecialties(id)
    .pipe(first())
    .subscribe(result => {
      this.specialtiesDoctorList = result.data;
      this.tabIndex = 0;
      if (this.specialtiesDoctorList.length > 0) {
        this.selectedSpecialty = this.specialtiesDoctorList[0];
        if (this.selectedSpecialty.id) {
          this.loadTable();
        }
      }
    });
  }

  selectSchedule(specialty, i: number) {
    this.tabIndex = i;
    this.selectedSpecialty = specialty;
    this.loadTable();
}

  loadTable(){
      this.persistentFilters['doctor_specialty_id'] = this.selectedSpecialty.doctor_specialty_id;
      this.initTableData(this.scheduleService);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  getDays(days: any[]){
    return days.map((d) => this.getDay(d.day));
  }

  getDay(day: string){
    return week_days.find(wd => wd.day === day).translations.find(t => t.language_id === 3).day
  }

  openCalendar(specialty: DoctorSpecialtyModel, tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>){
  this.eventsInCalendar = [];
   this.doctorsService.getCalendarBySpecialty(specialty.doctor_specialty_id).pipe(first())
    .subscribe(results => {
      this.events = results.data;
      this.events.forEach(event => {
        if(event.is_holiday){
          this.eventsInCalendar.push({
            id: event.id,
            title: event.title,
            start: event.start_date,
            allDay: true,
          });
        }else{
          this.eventsInCalendar.push({
            id: event.id,
            title: event.title,
            start: event.start_date,
            end: event.end_date,
          });  
        }
      });
      this.infoModal = this.createTplModal(tplTitle, tplContent, tplFooter);
    });
  }

  
  edit(id: number) {
    if(!this.authService.hasPermission('health:doctor:schedule:update')){
      return;
    }
      this.router.navigateByUrl('/health/schedules/edit/' + id);
  }

  delete(id: any){
    if(!this.authService.hasPermission('health:doctor:schedule:remove')){
      return;
    }
    this.uiService
    .showConfirm('Eliminar', 'Pretende eliminar este horário?', 'Eliminar')
    .subscribe(result => {
      if (result) {
        this.doctorsService.deleteSchedule(id).pipe(first()).subscribe(r => {
          this.uiService.showMessage(
            MessageType.success,
            'Horário eliminado com sucesso'
          );
          this.loadTable();
        });
      }
    });
  }

  closeEventModal() {
    this.infoModal.close();
  }

  createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: 900
    });
  }
}
