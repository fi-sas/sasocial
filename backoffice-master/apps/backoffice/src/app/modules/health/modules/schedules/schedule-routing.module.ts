import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleFormComponent } from './pages/schedule-form/schedule-form.component';
import { ScheduleListComponent } from './pages/schedule-list/schedule-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: ScheduleFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:period:create' }
  },
  {
    path: 'edit/:id',
    component: ScheduleFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:period:create' }
  },
  {
    path: 'list',
    component: ScheduleListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:period:periods' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleRoutingModule { }
