import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { ScheduleListComponent } from './pages/schedule-list/schedule-list.component';
import { ScheduleFormComponent } from './pages/schedule-form/schedule-form.component';
import { ListDoctorsComponent } from './components/list-doctors/list-doctors.component';
import { NgxFullCalendarModule } from 'ngx-fullcalendar';

@NgModule({
  declarations: [
   ScheduleListComponent,
   ScheduleFormComponent,
   ListDoctorsComponent,
  ],
  imports: [
    CommonModule,
    ScheduleRoutingModule,
    SharedModule,
    NgxFullCalendarModule
  ],
  entryComponents: [
    ListDoctorsComponent
  ]
})
export class ScheduleModule { }
