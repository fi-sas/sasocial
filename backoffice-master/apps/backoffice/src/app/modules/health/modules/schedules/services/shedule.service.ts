import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { PeriodModel } from '../models/period.model';

@Injectable({
  providedIn: 'root'
})
export class SheduleService extends Repository<PeriodModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService,urlService);
    this.entities_url = 'HEALTH.PERIOD';
  }
}
