import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSpecialtyComponent } from './view-specialty.component';

describe('ViewSpecialtyComponent', () => {
  let component: ViewSpecialtyComponent;
  let fixture: ComponentFixture<ViewSpecialtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSpecialtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
