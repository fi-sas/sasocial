import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Column } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { SpecialtyCheckListTypes, SpecialtyModel, SpecialtyPaymentTypeTags } from '../../models/specialty.model';

@Component({
  selector: 'fi-sas-view-specialty',
  templateUrl: './view-specialty.component.html',
  styleUrls: ['./view-specialty.component.less']
})
export class ViewSpecialtyComponent implements OnInit {
  
  @Input() specialty: SpecialtyModel = null;
  @Output() newItemEvent = new EventEmitter<string>();
  SpecialtyCheckListTypes = SpecialtyCheckListTypes;
  SpecialtyPaymentTypeTags = SpecialtyPaymentTypeTags;

  columns: Column[] = [];
  
  constructor() { }

  ngOnInit() {
  }

  addNewItem(value: any) {
    this.newItemEvent.emit(value);
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }
}
