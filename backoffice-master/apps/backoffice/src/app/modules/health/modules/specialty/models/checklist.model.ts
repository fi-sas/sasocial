export class ChecklistModel{
    id: number;
    specialty_id: number;
    question: string;
    answer_type: string;
    created_at: Date;
    update_at: Date;
}