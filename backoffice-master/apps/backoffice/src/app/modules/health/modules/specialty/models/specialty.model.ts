import { AppointmentTypeModel } from "../../appointment-type/models/appointment-type.model";
import { DoctorModel } from "../../doctors/models/doctor.model";
import { PlaceModel } from "../../place/models/place.model";
import { ChecklistModel } from "./checklist.model";
import { PriceVariationsModel } from "./price-variations.model";

export class SpecialtyModel {
    id: number;
    translations: SpecialtyModelTranslationModel[];
    appointment_types: AppointmentTypeModel[];
    price_variations: PriceVariationsModel[];
    doctors_ids: number[];
    doctors: DoctorModel[];
    appointment_types_ids: number[];
    place_id: number;
    place: PlaceModel;
    checklist: ChecklistModel[];
    is_external: boolean;
    description: string;
    regulation: string;
    avarage_time: number;
    product_code: string;
    payment_type: string;
    payment_information: string;
    is_active: boolean;
    organic_unit_id: number;
    created_at: Date;
    update_at: Date;
}

export class SpecialtyModelTranslationModel{
    specialty_id: number;
    language_id: number;
    name: string;
}

export const SpecialtyInternalExternal = {
    true: { label: 'Instituição  Externa', color: 'blue' },
    false: { label: 'Serviço Interno', color: 'grey' },
  };

  export const TariffScholarShip = {
    true: { label: 'Bolseiro', color: 'blue' },
    false: { label: 'Não Bolseiro', color: 'grey' },
  };

  export const SpecialtyActiveInactive = {
    true: { label: 'Ativo', color: 'green' },
    false: { label: 'Inativo', color: 'red' },
  };

  export const SpecialtyPaymentTypeTags = {
    'Pré Pagamento': { label: 'Pré Pagamento'},
    'Ato Consulta': { label: 'Ato da Consulta'},
  };

  export const SpecialtyCheckListTypes = {
    text: { label: 'Texto'},
    boolean: { label: 'Sim/Não'},
    number: { label: 'Número'},
    date: { label: 'Data'},
  };
