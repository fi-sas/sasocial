import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { TaxModel } from '@fi-sas/backoffice/modules/configurations/models/tax.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TaxesService } from '@fi-sas/backoffice/modules/configurations/services/taxes.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeModelTranslationModel } from '../../../appointment-type/models/appointment-type.model';
import { AppointmentTypeService } from '../../../appointment-type/services/appointment-type.service';
import { DoctorModel } from '../../../doctors/models/doctor.model';
import { DoctorsService } from '../../../doctors/services/doctors.service';
import { PlaceModel } from '../../../place/models/place.model';
import { PlaceService } from '../../../place/services/place.service';
import { TariffModel } from '../../../tariff/models/tariff.model';
import { TariffService } from '../../../tariff/services/tariff.service';
import { SpecialtyService } from '../../services/specialty.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'fi-sas-specialty-form',
  templateUrl: './specialty-form.component.html',
  styleUrls: ['./specialty-form.component.less']
})
export class SpecialtyFormComponent implements OnInit {

  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  
  languagesLoading = false;
  languages: LanguageModel[] = [];

  placesLoading = false;
  places: PlaceModel[] = [];
  placeSelected: PlaceModel;

  appointmentTypesLoading = false;
  appointmentTypes: AppointmentTypeModelTranslationModel[] = [];
  
  doctorsLoading = false;
  doctors: DoctorModel[] = [];

  taxsLoading = false;
  taxs: TaxModel[] = [];

  tariffsLoading = false;
  tariffs: TariffModel[] = [];

  organicUnitsLoading = false;
  organicUnits: OrganicUnitsModel[] = [];

  modalText: string;

  specialtyGroup = new FormGroup({
    translations: new FormArray([]),
    appointment_types_ids: new FormControl(null, []),
    product_code: new FormControl('', [Validators.required,trimValidation]),
    is_active: new FormControl(true),
    is_external: new FormControl(false),
    organic_unit_id: new FormControl(null),
    place_id: new FormControl(null, []),
    address: new FormControl(null),
    postal_code: new FormControl(null),
    city: new FormControl(null),
    email: new FormControl(null),
    phone: new FormControl(null),
    fax: new FormControl(null),
    description: new FormControl('', Validators.required),
    regulation: new FormControl('', Validators.required),
    doctors_ids: new FormControl(null, []),
    avarage_time: new FormControl('', [Validators.required]),
    price_variations: new FormArray([]),
    payment_information: new FormControl(null), 
    payment_type: new FormControl(null), 
    checklist: new FormArray([]),
  });

  translations = this.specialtyGroup.get('translations') as FormArray;
  price_variations = this.specialtyGroup.get('price_variations') as FormArray;
  checklist = this.specialtyGroup.get('checklist') as FormArray;

  get f() { return this.specialtyGroup.controls; }
  idToUpdate = null;
  submit = false;
  isLoading = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '100',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['insertImage', 'insertVideo']],
  };

  constructor(
    private uiService: UiService,
    public activateRoute: ActivatedRoute,
    public router: Router,
    private languagesService: LanguagesService,
    public specialtyService: SpecialtyService,
    private appointmentTypeService: AppointmentTypeService,
    private placeService: PlaceService,
    private doctorsService: DoctorsService,
    private tariffsService: TariffService,
    private taxService: TaxesService,
    private organicUnitService: OrganicUnitsService
  ) { }

  ngOnInit() {
    this.getAppointmentTypes();
    this.getPlaces();
    this.getDoctors();
    this.getTariffs();
    this.getVat();
    this.getOrganicUnits();
    this.loadLanguages();
  }

  loadLanguages() {
    this.languagesLoading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languagesLoading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;

        this.getSpecialty();
      });
  }

  getSpecialty() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.isLoading = true;
        this.idToUpdate = data.get('id');
        this.specialtyService
          .read(parseInt(data.get('id'), 10), {
            withRelated: 'translations, appointment_types, doctors, place, price_variations, checklist',
          })
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((result) => {
            if (result.data[0].translations) {
              result.data[0].translations.map((t) => {
                this.addTranslation(t.language_id, t.name);
              });
            }
            if (result.data[0].price_variations) {
              result.data[0].price_variations.map((price) => {
                this.addPriceLine(price.tariff_id, price.vat_id, price.price);
              });
            }
            if (result.data[0].checklist) {
              result.data[0].checklist.map((check) => {
                this.addChecklistLine(check.question, check.answer_type);
              });
            }
            this.modalText = result.data[0].payment_information,
            this.specialtyGroup.patchValue({
              appointment_types_ids: (result.data[0].appointment_types||[]).map(res => res.id),
              is_active: result.data[0].is_active,
              is_external: result.data[0].is_external,
              product_code: result.data[0].product_code,
              organic_unit_id: result.data[0].organic_unit_id,
              place_id: result.data[0].place_id,
              address:  result.data[0].place.address,
              description: result.data[0].description,
              regulation: result.data[0].regulation,
              doctors_ids: (result.data[0].doctors||[]).map(res => res.id),
              avarage_time: result.data[0].avarage_time,
              payment_type: result.data[0].payment_type,
            });
            this.languages.map((l) => {
              const foundLnaguage = this.translations.value.find(
                (t) => t.language_id === l.id
              );
              if (!foundLnaguage) {
                this.addTranslation(l.id, '');
              }
            });
          });
      } else {
        this.languages.map((l) => {
          this.addTranslation(l.id, '');
        });
        this.addPriceLine(null,null,null);
        this.addChecklistLine('','')
      }
    });
  }

  getOrganicUnits(){
    this.organicUnitsLoading = true;
    this.organicUnitService.list(1, -1).pipe(
      first(),
      finalize(() => this.organicUnitsLoading = false)).subscribe(results => {
        this.organicUnits = results.data;
      });
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list(1, -1).pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypes = results.data.map(res => res.translations.find((t) => t.language_id === 3));
      });
  }

  getPlaces(){
    this.placesLoading = true;
    this.placeService.list(1, -1).pipe(
      first(),
      finalize(() => this.placesLoading = false)).subscribe(results => {
        this.places = results.data;
      });
  }

  getDoctors(){
    this.doctorsLoading = true;
    this.doctorsService.list(1, -1).pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctors = results.data;
      });
  }

  getTariffs(){
    this.tariffsLoading = true;
    this.tariffsService.list(1, -1).pipe(
      first(),
      finalize(() => this.tariffsLoading = false)).subscribe(results => {
        this.tariffs = results.data;
      });
  }

  getVat(){
    this.taxsLoading = true;
    this.taxService.list(1, -1).pipe(
      first(),
      finalize(() => this.taxsLoading = false)).subscribe(results => {
        this.taxs = results.data;
      });
  }

  setAddressInfo(){
    this.placeSelected = this.places.find((p) => p.id === this.specialtyGroup.get('place_id').value);
    if(this.placeSelected){
      this.specialtyGroup.patchValue({
        address: this.placeSelected.address,
        postal_code: this.placeSelected.postal_code,
        city: this.placeSelected.city,
        email: this.placeSelected.email,
        phone: this.placeSelected.phone,
        fax: this.placeSelected.fax,
      })
    }else{
      this.specialtyGroup.patchValue({
        place_id: null,
        address: null,
        postal_code: null,
        city: null,
        email: null,
        phone: null,
        fax: null,
      })
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.specialtyGroup.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }

  addPriceLine(tariff_id1: number, vat_id1: number, price1:number) {
    const price_variations = this.specialtyGroup.controls.price_variations as FormArray;
    price_variations.push(
      new FormGroup({
        tariff_id: new FormControl(tariff_id1, [Validators.required]),
        vat_id: new FormControl(vat_id1, [Validators.required]),
        price: new FormControl(price1, [Validators.required]),
      })
    );
  }

  deletePriceLine(priceIndex: any) {
    if (this.price_variations.at(priceIndex)) {
      this.price_variations.removeAt(priceIndex);
    }
  }

  addChecklistLine(question: string, answer_type:string) {
    const checklist = this.specialtyGroup.controls.checklist as FormArray;
    checklist.push(
      new FormGroup({
        question: new FormControl(question, [Validators.required,trimValidation]),
        answer_type: new FormControl(answer_type, [Validators.required]),
      })
    );
  }

  deleteChecklistLine(checklistIndex: any) {
    if (this.checklist.at(checklistIndex)) {
      this.checklist.removeAt(checklistIndex);
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
  }

  isTariffDisabled(tariff_id){
    const tariffs = this.specialtyGroup.get('price_variations').value;
    return tariffs.find( tariff => tariff.tariff_id === tariff_id) ? true : false
  }
    
  submitForm() {
    this.submit = true;
    this.specialtyGroup.patchValue({
      payment_information: this.modalText
    })
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.specialtyGroup.valid) {
      this.isLoading = true;
      this.submit = false;
      if (this.idToUpdate) {
        this.specialtyService
          .update(this.idToUpdate, this.specialtyGroup.value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Especialidade alterada com sucesso'
            );
          });
      } else {
        this.specialtyService
          .create(this.specialtyGroup.value)
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe((results) => {
            this.backList();
            this.uiService.showMessage(
              MessageType.success,
              'Especialidade criada com sucesso'
            );
          });
      }
    }
  }

  backList() {
    this.router.navigate(['health', 'specialty', 'list']);
  }

}
