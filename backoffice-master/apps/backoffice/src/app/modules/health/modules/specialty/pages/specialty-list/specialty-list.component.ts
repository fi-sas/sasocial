import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeModel, AppointmentTypeModelTranslationModel } from '../../../appointment-type/models/appointment-type.model';
import { AppointmentTypeService } from '../../../appointment-type/services/appointment-type.service';
import { SpecialtyActiveInactive, SpecialtyInternalExternal, SpecialtyModel } from '../../models/specialty.model';
import { SpecialtyService } from '../../services/specialty.service';

@Component({
  selector: 'fi-sas-specialty-list',
  templateUrl: './specialty-list.component.html',
  styleUrls: ['./specialty-list.component.less']
})
export class SpecialtyListComponent extends TableHelper implements OnInit {

  appointmentTypes: AppointmentTypeModelTranslationModel[] = [];
  appointmentTypesLoading = false;

  specialties: SpecialtyModel[] = [];
  specialtiesLoading = false;
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    private specialtyService: SpecialtyService,
    private appointmentTypeService: AppointmentTypeService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: 'name,email,user_name'
    };

    this.columns.push(
      {
        key: 'translations.name',
        label: 'Serviços / Especialidades',
        sortable: false,
        // @ts-ignore
        template: (data) => {
          const service = data.translations.find((t) => t.language_id === 3)
            .name;
          return service;
        },
      },
      {
        key: 'place.name',
        label: 'Instituição / Clínica',
        sortable: false,
      },
      {
        key: 'product_code',
        label: 'Código',
        sortable: false,
  
      },
      {
        key: 'is_external',
        label: 'Tipo',
        tag: SpecialtyInternalExternal,
        sortable: false,
      },
      {
        key: 'is_active',
        label: 'Estado',
        tag: SpecialtyActiveInactive,
        sortable: false,
      }
    );
  }

  ngOnInit() {
  
    this.initTableData(this.specialtyService);
    this.getSpecialties();
    this.getAppointmentTypes();
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }
  
  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list(1, -1).pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypes = results.data.map(res => res.translations.find((t) => t.language_id === 3));
      });
  }

  getSpecialties(){
    this.specialtiesLoading = true;
    this.specialtyService.list(1, -1).pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)).subscribe(results => {
        this.specialties = results.data;
      });
  }
  
  edit(id: number) {
    if(!this.authService.hasPermission('health:specialty:update')){
      return;
    }
      this.router.navigateByUrl('/health/specialty/edit/' + id);
  }

  status(id: number, status: boolean) {
    if(!this.authService.hasPermission('health:specialty:status')){
      return;
    }
    if(status){
      this.uiService
      .showConfirm('Ativar', 'Pretende ativar esta especialidade?', 'Ativar')
      .subscribe(result => {
        if (result) {
          this.specialtyService.status(id, status).subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Serviço / Especialidade ativada com sucesso'
            );
            this.initTableData(this.specialtyService);
          });
        }
      });
    }else{
      this.uiService
      .showConfirm('Desativar', 'Pretende desativar esta especialidade?', 'Desativar')
      .subscribe(result => {
        if (result) {
          this.specialtyService.status(id, status).subscribe((result) => {
            this.uiService.showMessage(
              MessageType.success,
              'Serviço / Especialidade desativado com sucesso'
            );
            this.initTableData(this.specialtyService);
          });
        }
      });
    }
  }

  delete(id: number) {
    if(!this.authService.hasPermission('health:specialty:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar esta especialidade?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.specialtyService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Especialidade eliminado com sucesso'
              );
              this.initTableData(this.specialtyService);
            });
          }
        });
    }

  validTranslateName(id_trans: number, translation: any[]) {
    return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).name : '';
    }
}
