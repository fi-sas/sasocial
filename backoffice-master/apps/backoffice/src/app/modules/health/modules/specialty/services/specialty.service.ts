import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { DoctorModel } from '../../doctors/models/doctor.model';
import { EventModel } from '../../schedules/models/schedule.model';
import { SpecialtyModel } from '../models/specialty.model';

@Injectable({
  providedIn: 'root'
})
export class SpecialtyService extends Repository<SpecialtyModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'HEALTH.SPECIALTY';
    this.entity_url = 'HEALTH.SPECIALTY_ID';
  }

  status(id: number, is_active: boolean): Observable<Resource<SpecialtyModel>> {
    return this.resourceService.create<SpecialtyModel>(
      this.urlService.get('HEALTH.SPECIALTY_STATUS', {
        id,
      }),
      {is_active}
    );
  }

  getDoctors(id: any): Observable<Resource<DoctorModel>> {
    return this.resourceService.list<DoctorModel>(
      this.urlService.get('HEALTH.SPECIALTY_DOCTORS', {id}), {});
  }

  getScheduleDoctor(id: any, doctor_id: number): Observable<Resource<EventModel>> {
    let params = new HttpParams();
    params = params.set('doctor_id', doctor_id.toString());
    return this.resourceService.list<EventModel>(this.urlService.get('HEALTH.SPECIALTY_SCHEDULE_DOCTOR', {id}), {params});
  }

}
