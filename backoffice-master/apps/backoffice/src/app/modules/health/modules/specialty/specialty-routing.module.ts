import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialtyFormComponent } from './pages/specialty-form/specialty-form.component';
import { SpecialtyListComponent } from './pages/specialty-list/specialty-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: SpecialtyFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:specialty:create' }
  },
  {
    path: 'edit/:id',
    component: SpecialtyFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:specialty:create' }
  },
  {
    path: 'list',
    component: SpecialtyListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:specialty:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialtyRoutingModule { }
