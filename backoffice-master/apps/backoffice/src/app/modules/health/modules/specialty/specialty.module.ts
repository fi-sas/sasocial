import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialtyRoutingModule } from './specialty-routing.module';
import { SpecialtyListComponent } from './pages/specialty-list/specialty-list.component';
import { SpecialtyFormComponent } from './pages/specialty-form/specialty-form.component';
import { ViewSpecialtyComponent } from './components/view-specialty/view-specialty.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [SpecialtyListComponent, SpecialtyFormComponent, ViewSpecialtyComponent],
  imports: [
    CommonModule,
    SpecialtyRoutingModule,
    SharedModule,
    AngularEditorModule
  ]
})
export class SpecialtyModule { }
