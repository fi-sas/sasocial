export class TariffModel {
    id: number;
    description: string;
    is_scolarship: boolean;
    profiles: ProfileUserModel[];
}

export class ProfileUserModel{
    id: number;
    tariff_id: number;
    profile_id: number;
}