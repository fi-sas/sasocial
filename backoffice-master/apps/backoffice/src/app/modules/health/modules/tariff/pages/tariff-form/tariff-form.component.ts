import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { TariffModel } from '../../models/tariff.model';
import { TariffService } from '../../services/tariff.service';

@Component({
  selector: 'fi-sas-tariff-form',
  templateUrl: './tariff-form.component.html',
  styleUrls: ['./tariff-form.component.less']
})
export class TariffFormComponent implements OnInit {

  profiles: ProfileModel[] = [];
  profilesLoading = false;

  formCreate = new FormGroup({
    description: new FormControl('', [Validators.required, trimValidation]),
    is_scolarship: new FormControl(false),
    profile_ids: new FormControl(null, []),
  });

  submitted = false;
  get f() { return this.formCreate.controls; }
  id;
  isEdit = false;
  isLoading = false;

  constructor(private route: ActivatedRoute, private tariffService: TariffService, 
    private uiService: UiService, private router: Router, private profilesService: ProfilesService) {
    
}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
        this.isLoading = true;
        this.getTariff(id);
        this.id = id;
        this.isEdit = true;
    }
    this.getProfiles();
  }

  
  getProfiles(){
    this.profilesLoading = true;
    this.profilesService.list(1, -1).pipe(
      first(),
      finalize(() => this.profilesLoading = false)).subscribe(results => {
        this.profiles = results.data;
      });
  }

  getTariff(id){
    let tariff: TariffModel = new TariffModel();
    this.tariffService
      .read(id)
      .pipe(
        first(),
        finalize(() => this.isLoading = false)
      )
      .subscribe((results) => {
        tariff = results.data[0];
        this.formCreate.patchValue({
          ...tariff
        });
      });  
  }

  submit() {
    this.submitted = true;
    if (this.formCreate.valid) {
      this.isLoading = true;
        if (this.isEdit) {
            this.tariffService.update(this.id, this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Tarifário alterado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }else {
            this.tariffService.create(this.formCreate.value).pipe(first(),finalize(() =>this.isLoading = false)).subscribe(
                result => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Tarifário criado com sucesso'
                  );
                  this.returnButton();
                },
              )
        }
    }
  }

  returnButton() {
      this.router.navigateByUrl('health/tariff/list');
  }

}
