import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { first } from 'rxjs/operators';
import { TariffService } from '../../services/tariff.service';

@Component({
  selector: 'fi-sas-tariff-list',
  templateUrl: './tariff-list.component.html',
  styleUrls: ['./tariff-list.component.less']
})
export class TariffListComponent extends TableHelper implements OnInit {
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private languageService: LanguagesService,
    private authService: AuthService,
    public tariffService: TariffService
  ) {
    super(uiService, router, activatedRoute);
    this.columns.push(
      {
        key: 'description',
        label: 'Descrição',
        sortable: true
      },
      {
        key: 'is_scolarship',
        label: 'Bolsa',
        tag: TagComponent.YesNoTag
      },
    );
  }

  ngOnInit() {
    this.persistentFilters['searchFields'] = 'description';
    this.initTableData(this.tariffService);
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  edit(id: number) {
    if(!this.authService.hasPermission('health:tariff:update')){
      return;
    }
      this.router.navigateByUrl('/health/tariff/edit/' + id);
  }
  
  delete(id: number) {
    if(!this.authService.hasPermission('health:tariff:delete')){
      return;
    }
      this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar este tarifário?', 'Eliminar')
        .subscribe(result => {
          if (result) {
            this.tariffService.delete(id).pipe(first()).subscribe(r => {
              this.uiService.showMessage(
                MessageType.success,
                'Tarifário eliminado com sucesso'
              );
              this.initTableData(this.tariffService);
            });
          }
        });
    }

}
