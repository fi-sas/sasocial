import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TariffFormComponent } from './pages/tariff-form/tariff-form.component';
import { TariffListComponent } from './pages/tariff-list/tariff-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: TariffFormComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'health:tariff:create' }
  },
  {
    path: 'edit/:id',
    component: TariffFormComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'health:tariff:create' }
  },
  {
    path: 'list',
    component: TariffListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'health:tariff:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TariffRoutingModule { }
