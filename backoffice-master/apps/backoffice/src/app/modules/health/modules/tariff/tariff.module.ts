import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TariffFormComponent } from './pages/tariff-form/tariff-form.component';
import { TariffListComponent } from './pages/tariff-list/tariff-list.component';
import { TariffRoutingModule } from './tariff-routing.module';

@NgModule({
  declarations: [
    TariffFormComponent,
    TariffListComponent
  ],
  imports: [
    CommonModule,
    TariffRoutingModule,
    SharedModule
  ]
})
export class TariffModule { }
