export const week_days = [
    {
        day: "Sunday",
        day_number: 0,
        translations:[
            {
                language_id: 3,
                day: "Domingo"
            },
            {
                language_id: 4,
                day: "Sunday"
            },
        ]
    },
    {
        day: "Monday",
        day_number: 1,
        translations:[
            {
                language_id: 3,
                day: "Segunda-feira"
            },
            {
                language_id: 4,
                day: "Monday"
            },
        ]
    },
    {
        day: "Tuesday",
        day_number: 2,
        translations:[
            {
                language_id: 3,
                day: "Terça-feira"
            },
            {
                language_id: 4,
                day: "Tuesday"
            },
        ]
    },
    {
        day: "Wednesday",
        day_number: 3,
        translations:[
            {
                language_id: 3,
                day: "Quarta-feira"
            },
            {
                language_id: 4,
                day: "Wednesday"
            },
        ]
    },
    {
        day: "Thursday",
        day_number: 4,
        translations:[
            {
                language_id: 3,
                day: "Quinta-feira"
            },
            {
                language_id: 4,
                day: "Thursday"
            },
        ]
    },
    {
        day: "Friday",
        day_number: 5,
        translations:[
        {
            language_id: 3,
            day: "Sexta-feira"
        },
        {
            language_id: 4,
            day: "Friday"
        },
        ]
    },
    {
        day: "Saturday",
        day_number: 6,
        translations:[
            {
                language_id: 3,
                day: "Sábado"
            },
            {
                language_id: 4,
                day: "Saturday"
            },
        ]
    }
]

export const status_appointment = [
    {
        status: 'CREATED',
        badge: {
            label: 'Criação', 
            color: 'blue'
        }
    },
    {
        status: 'APPROVED',
        badge: {
            label: 'Marcada', 
            color: 'green'
        }
    },
    {
        status: 'PENDING',
        badge: {
            label: 'Por Confirmar', 
            color: 'yellow'
        }
    },
    {
        status: 'MISSED',
        badge: {
            label: 'Falta Marcada', 
            color: 'red'
        }
    },
    {
        status: 'CLOSED',
        badge: {
            label: 'Consulta Realizada', 
            color: 'red'
        }
    },
    {
        status: 'CANCELED',
        badge: {
            label: 'Cancelada', 
            color: 'orange'
        }
    },
    {
        status: 'CHANGED',
        badge: {
            label: 'Alterada', 
            color: 'purple'
        }
    },
]

