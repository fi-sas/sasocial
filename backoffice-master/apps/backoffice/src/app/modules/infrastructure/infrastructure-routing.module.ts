import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { InfrastructureComponent } from "@fi-sas/backoffice/modules/infrastructure/infrastructure.component";
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';



const routes: Routes = [{
  path: '',
  component: InfrastructureComponent,
  children: [
    {
      path: 'initial-page',
      component: InitialPageComponent,
      
    },
    { path: '', redirectTo: 'initial-page', pathMatch: 'full' },

    {
      path: 'assets',
      loadChildren: './pages/assets/assets.module#AssetsInfrastructureModule',
      data: { breadcrumb: 'Ativos', title: 'Ativos', scope: 'infrastructure:assets' },
      resolve: {}
    },
    {
      path: 'locations',
      loadChildren: './pages/locations/location.module#InfrastructureConfigurationModule',
      data: { breadcrumb: 'Localizações', title: 'Localizações', scope: 'infrastructure:configuration' },
      resolve: {}
    },
    {
      path: 'reports',
      loadChildren: './pages/reports/reports.module#ReportsInfrastructureModule',
      data: { breadcrumb: 'Relatórios', title: 'Relatórios'},
      resolve: {}
    },
    {
      path: '**',
      component: PageNotFoundComponent,
      data: {
        breadcrumb: 'Página não encontrada',
        title: 'Página não encontrada'
      }
    }
  ],
  data: { breadcrumb: null, title: null }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class InfrastructureRoutingModule { }
