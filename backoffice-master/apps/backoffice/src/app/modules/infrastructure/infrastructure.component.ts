import { Component, OnInit } from '@angular/core';
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { SiderItem } from "@fi-sas/backoffice/core/models/sider-item";
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';


@Component({
  selector: 'fi-sas-infrastructure',
  template: '<router-outlet></router-outlet>'
})
export class InfrastructureComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(9), 'bank');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const assets = new SiderItem('Ativos', '', '', 'infrastructure:assets');
    assets.addChild(new SiderItem('Criar', '', '/infrastructure/assets/create-asset', 'infrastructure:assets:create'));
    assets.addChild(new SiderItem('Listar', '', '/infrastructure/assets/list-assets', 'infrastructure:assets:read'));
    this.uiService.addSiderItem(assets);

    const locations = new SiderItem('Localizações', '', '', 'infrastructure:configuration');
    locations.addChild(new SiderItem('Configuração de infraestruturas', '', '/infrastructure/locations/configuration'));

    const organic_unities = locations.addChild(new SiderItem('Unidades Orgânicas', '', '', 'infrastructure:organic-units'));
    organic_unities.addChild(new SiderItem('Criar', '', '/infrastructure/locations/organic-units/create', 'infrastructure:organic-units:create'));
    organic_unities.addChild(new SiderItem('Listar', '', '/infrastructure/locations/organic-units/list', 'infrastructure:organic-units:read'));

    const buildings = locations.addChild(new SiderItem('Edifícios', '', '', 'infrastructure:buildings'));
    buildings.addChild(new SiderItem('Criar', '', '/infrastructure/locations/buildings/create', 'infrastructure:buildings:create'));
    buildings.addChild(new SiderItem('Listar', '', '/infrastructure/locations/buildings/list', 'infrastructure:buildings:read'));

    const wings = locations.addChild(new SiderItem('Alas', '', '', 'infrastructure:wings'));
    wings.addChild(new SiderItem('Criar', '', '/infrastructure/locations/wings/create', 'infrastructure:wings:create'));
    wings.addChild(new SiderItem('Listar', '', '/infrastructure/locations/wings/list', 'infrastructure:wings:read'));

    const floors = locations.addChild(new SiderItem('Pisos', '', '', 'infrastructure:floors'));
    floors.addChild(new SiderItem('Criar', '', '/infrastructure/locations/floors/create', 'infrastructure:floors:create'));
    floors.addChild(new SiderItem('Listar', '', '/infrastructure/locations/floors/list', 'infrastructure:floors:read'));

    const rooms = locations.addChild(new SiderItem('Divisões', '', '', 'infrastructure:rooms'));
    rooms.addChild(new SiderItem('Criar', '', '/infrastructure/locations/divisions/create', 'infrastructure:rooms:create'));
    rooms.addChild(new SiderItem('Listar', '', '/infrastructure/locations/divisions/list', 'infrastructure:rooms:read'));

    const services = locations.addChild(new SiderItem('Serviços', '', '', 'infrastructure:services'));
    services.addChild(new SiderItem('Criar', '', '/infrastructure/locations/services/create', 'infrastructure:services:create'));
    services.addChild(new SiderItem('Listar', '', '/infrastructure/locations/services/list', 'infrastructure:services:read'));

    const areas = locations.addChild(new SiderItem('Áreas', '', '', 'infrastructure:areas'));
    areas.addChild(new SiderItem('Criar', '', '/infrastructure/locations/areas/create', 'infrastructure:areas:create'));
    areas.addChild(new SiderItem('Listar', '', '/infrastructure/locations/areas/list', 'infrastructure:areas:read'));

    const categories = locations.addChild(new SiderItem('Categorias', '', '', 'infrastructure:categories'));
    categories.addChild(new SiderItem('Criar', '', '/infrastructure/locations/categories/create', 'infrastructure:categories:create'));
    categories.addChild(new SiderItem('Listar', '', '/infrastructure/locations/categories/list', 'infrastructure:categories:read'));

    this.uiService.addSiderItem(locations);

    /* const reports = new SiderItem('Relatórios');
     reports.addChild(new SiderItem('Listagem', '', '/infrastructure/reports/listing'));
     this.uiService.addSiderItem(reports);*/

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
