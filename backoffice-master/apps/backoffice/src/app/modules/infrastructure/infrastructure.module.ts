import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { InfrastructureRoutingModule } from './infrastructure-routing.module';
import { InfrastructureComponent} from "@fi-sas/backoffice/modules/infrastructure/infrastructure.component";

import { AssetsService } from "@fi-sas/backoffice/modules/infrastructure/services/assets.service";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { FloorsService } from "@fi-sas/backoffice/modules/infrastructure/services/floors.service";
import { LocationsService } from "@fi-sas/backoffice/modules/infrastructure/services/locations.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { RoomsService } from "@fi-sas/backoffice/modules/infrastructure/services/rooms.service";
import { ServicesService } from "@fi-sas/backoffice/modules/infrastructure/services/services.service";
import { WingsService } from "@fi-sas/backoffice/modules/infrastructure/services/wings.service";
import { AreaService } from './services/area.service';
import { CategoryService } from './services/category.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureRoutingModule
  ],
  declarations: [
    InfrastructureComponent,

  ],
  providers: [
    AssetsService,
    BuildingService,
    FloorsService,
    LocationsService,
    OrganicUnitsService,
    RoomsService,
    ServicesService,
    WingsService,
    FloorsService,
    AreaService,
    CategoryService
  ]
})
export class InfrastructureModule { }
