import { LocationModel } from "@fi-sas/backoffice/modules/infrastructure/models/location.model";

export class AssetModel {

  id?: number;
  code: string;
  designation: string;
  area_id: number;
  category_id: number;
  maintenance: boolean;
  maintenance_type: string; //INTERNAL or EXTERNAL
  observations: string;
  acquisition_date: Date;
  acquisition_value: number;
  electric_power: number;
  classifier: string;
  manufacturer: string; //marca
  model: string;
  color: string;
  serial_number: string;
  length: number;
  width: number;
  height: number;
  rfid: string;
  barcode: string;
  erp: string;
  n_sequencial: number;
  n_good: number; //BEM
  initials_n_good: string;// Sigla N_Bem
  active: boolean;
  location_id: number;
  updated_at?: string;
  created_at?: string;
  location?: LocationModel;
  documents: DocumentModel[];
}


export class DocumentModel{
  file_id: number;
  document_type_id: number;

}