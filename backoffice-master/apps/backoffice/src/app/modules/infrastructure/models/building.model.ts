import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { OrganicUnitsModel } from "./organic-units.model";

export class BuildingModel {

  id?: number;
  name: string;
  code: string;
  address: string;
  district: string;
  city: string;
  locality: string;
  postal_code: string;
  active: boolean;
  area: number;
  organic_unit_id: number;
  organicUnit: OrganicUnitsModel[];
  updated_at?: string;
  created_at?: string;
  wings: WingModel[];
}
