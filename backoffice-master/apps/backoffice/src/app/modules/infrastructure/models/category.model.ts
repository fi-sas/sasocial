export class CategoryModel {

    id?: number;
    name: string;
    description: string;
    active: boolean;
    updated_at?: string;
    created_at?: string;
  }
  