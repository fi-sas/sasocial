import { RoomModel } from "@fi-sas/backoffice/modules/infrastructure/models/room.model";
import { WingModel } from "./wing.model";

export class FloorModel {

  id?: number;
  name: string;
  code: string;
  active: boolean;
  wing_id: number;
  wing: WingModel[];
  updated_at?: string;
  created_at?: string;
  rooms?: RoomModel[];
}
