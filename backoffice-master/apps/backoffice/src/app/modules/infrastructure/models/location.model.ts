
export class LocationModel {

  id?: number;
  description: string;
  code: string;
  organic_unit_id: number;
  building_id: number;
  wing_id: number;
  floor_id: number;
  service_id: number;
  room_id: number;
  active: boolean;
  created_at?: string;
  updated_at?: string;

}


