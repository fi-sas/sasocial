import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";

export class OrganicUnitsModel {

  id?: number;
  name: string;
  code: string;
  address: string;
  locality: string;
  city: string;
  district: string;
  postal_code: string;
  phone: string;
  email: string;
  active: boolean;
  updated_at?: string;
  created_at?: string;
  buildings?: BuildingModel[];
  is_school: boolean;
  external_ref: string;
  dgs_code?: string;
}
