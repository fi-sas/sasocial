import { ServiceModel } from "@fi-sas/backoffice/modules/alimentation/models/service.model";

export class RoomModel {

  id?: number;
  name: string;
  code: string;
  active: boolean;
  floor_id: number;
  updated_at?: string;
  created_at?: string;
  services?: ServiceModel[];
}
