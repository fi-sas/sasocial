
export class ServiceModel {

  id?: number;
  name: string;
  code: string;
  active: boolean;
  room_id: number;
  updated_at?: string;
  created_at?: string;
}
