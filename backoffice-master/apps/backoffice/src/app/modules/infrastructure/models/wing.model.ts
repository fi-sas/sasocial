import { FloorModel} from "@fi-sas/backoffice/modules/infrastructure/models/floor.model";
import { BuildingModel } from "./building.model";

export class WingModel {

  id?: number;
  name: string;
  code: string;
  active: boolean;
  building_id: number;
  updated_at?: string;
  created_at?: string;
  building: BuildingModel[];
  floor?: FloorModel[];
}
