import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateAssetsInfrastructureComponent } from './create-asset/create-asset.component';
import { ListAssetsInfrastructureComponent } from './list-assets/list-assets.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-assets', pathMatch: 'full' },
  {
    path: 'create-asset',
    component: CreateAssetsInfrastructureComponent,
    data: { breadcrumb: 'Criar', title: 'Criar Ativo'},
    resolve: { }
  },
  {
    path: 'create-asset/:id',
    component: CreateAssetsInfrastructureComponent,
    data: { breadcrumb: 'Criar', title: 'Criar Ativo'},
    resolve: { }
  },
  {
    path: 'list-assets',
    component: ListAssetsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar Ativos'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsInfrastructureRoutingModule { }
