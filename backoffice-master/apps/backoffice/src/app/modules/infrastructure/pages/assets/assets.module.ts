import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { AssetsInfrastructureRoutingModule } from './assets-routing.module';
import { ListAssetsInfrastructureComponent } from './list-assets/list-assets.component';
import { CreateAssetsInfrastructureComponent } from './create-asset/create-asset.component';


@NgModule({
  declarations: [
    ListAssetsInfrastructureComponent,
    CreateAssetsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AssetsInfrastructureRoutingModule,
    
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class AssetsInfrastructureModule { }
