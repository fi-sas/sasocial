import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { AreaModel } from "../../../models/area.model";
import { AssetModel } from "../../../models/asset.model";
import { BuildingModel } from "../../../models/building.model";
import { CategoryModel } from "../../../models/category.model";
import { DocumentTypeModel } from "../../../models/documents.model";
import { FloorModel } from "../../../models/floor.model";
import { LocationModel } from "../../../models/location.model";
import { OrganicUnitsModel } from "../../../models/organic-units.model";
import { RoomModel } from "../../../models/room.model";
import { ServiceModel } from "../../../models/service.model";
import { WingModel } from "../../../models/wing.model";
import { AreaService } from "../../../services/area.service";
import { AssetsService } from "../../../services/assets.service";
import { BuildingService } from "../../../services/building.service";
import { CategoryService } from "../../../services/category.service";
import { DocumentsTypesService } from "../../../services/documents-types.service";
import { FloorsService } from "../../../services/floors.service";
import { LocationsService } from "../../../services/locations.service";
import { OrganicUnitsService } from "../../../services/organic-units.service";
import { RoomsService } from "../../../services/rooms.service";
import { ServicesService } from "../../../services/services.service";
import { WingsService } from "../../../services/wings.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-infrastructure-create-assets',
    templateUrl: './create-asset.component.html',
    styleUrls: ['./create-asset.component.less']
})


export class CreateAssetsInfrastructureComponent implements OnInit {
    loadingEdit = false;
    formCreate: FormGroup;
    formLocation: FormGroup;
    submitted: boolean = false;
    submittedLocation: boolean = false;
    locations: LocationModel[];
    modalVisible: boolean = false;
    organics: OrganicUnitsModel[];
    buildings: BuildingModel[];
    wings: WingModel[];
    floors: FloorModel[];
    divisions: RoomModel[];
    services: ServiceModel[];
    nameUA: string = '';
    nameBuildings: string = '';
    nameWings: string = '';
    nameFloors: string = '';
    nameDivisions: string = '';
    nameServices: string = '';
    areas: AreaModel[];
    categories: CategoryModel[];
    id: number;
    documentsType: DocumentTypeModel[];
    loading: boolean;
    edit = false;

    constructor(
        private uiService: UiService,
        private organicUnitsService: OrganicUnitsService,
        private buildingService: BuildingService,
        private formBuilder: FormBuilder,
        private wingsService: WingsService,
        private roomsService: RoomsService,
        private floorsService: FloorsService,
        private servicesService: ServicesService,
        private categoryService: CategoryService,
        private areaService: AreaService,
        private assetsService: AssetsService,
        private documentsTypesService: DocumentsTypesService,
        private route: ActivatedRoute,
        private router: Router,
        private locationsService: LocationsService,
        private authService: AuthService) { }

    ngOnInit() {
        this.formCreate = this.newFormCreate();
        this.formLocation = this.newFormLocation();
        this.getLocations();
        this.getOrganicUnits();
        this.getAreas();
        this.getCategories();
        this.getDocumentsType();
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });

        if (this.id != undefined) {//edit
            this.edit = true;
            this.getDataAssetById(this.id);
        }

    }

    get f() { return this.formCreate.controls; }
    get ff() { return this.formLocation.controls; }

    newFormCreate() {
        return this.formBuilder.group({
            code: ['', [Validators.required, trimValidation]],
            designation: ['', [Validators.required, trimValidation]],
            location_id: ['', [Validators.required]],
            classifier: ['', [Validators.required, trimValidation]],
            manufacturer: ['', [Validators.required, trimValidation]],
            model: ['', [Validators.required,trimValidation]],
            color: ['', [Validators.required, trimValidation]],
            serial_number: ['', [Validators.required,trimValidation]],
            length: ['', [Validators.required]],
            width: ['', [Validators.required]],
            height: ['', [Validators.required]],
            electric_power: [''],
            acquisition_value: ['', [Validators.required]],
            acquisition_date: ['', [Validators.required]],
            observations: ['', trimValidation],
            rfid: ['', [Validators.required,trimValidation]],
            barcode: ['', [Validators.required,trimValidation]],
            erp: ['', [Validators.required,trimValidation]],
            n_sequencial: ['', [Validators.required]],
            n_good: ['', [Validators.required]],
            initials_n_good: ['', [Validators.required, trimValidation]],
            active: [false, [Validators.required]],
            maintenance: [false, [Validators.required]],
            maintenance_type: [null],
            category_id: ['', [Validators.required]],
            area_id: ['', [Validators.required]],
            documents: this.formBuilder.array([])
        });
    }

    getDataAssetById(id: number) {
        this.loadingEdit = true;
        let asset: AssetModel = new AssetModel();
        this.assetsService.getAssetById(id).pipe(
            first(), finalize(() => this.loadingEdit = false)
        ).subscribe(results => {
            asset = results.data[0];
            this.formCreate.patchValue({
                code: asset.code ? asset.code : null,
                designation: asset.designation ? asset.designation : null,
                location_id: asset.location_id ? asset.location_id : null,
                classifier: asset.classifier ? asset.classifier : null,
                manufacturer: asset.manufacturer ? asset.manufacturer : null,
                model: asset.model ? asset.model : null,
                color: asset.color ? asset.color : null,
                serial_number: asset.serial_number ? asset.serial_number : null,
                length: asset.length ? asset.length : null,
                width: asset.width ? asset.width : null,
                height: asset.height ? asset.height : null,
                electric_power: asset.electric_power ? asset.electric_power : null,
                acquisition_value: asset.acquisition_value ? asset.acquisition_value : null,
                acquisition_date: asset.acquisition_date ? asset.acquisition_date : null,
                observations: asset.observations ? asset.observations : null,
                rfid: asset.rfid ? asset.rfid : null,
                barcode: asset.barcode ? asset.barcode : null,
                erp: asset.erp ? asset.erp : null,
                n_sequencial: asset.n_sequencial ? asset.n_sequencial : null,
                n_good: asset.n_good ? asset.n_good : null,
                initials_n_good: asset.initials_n_good ? asset.initials_n_good : null,
                maintenance: asset.maintenance ? asset.maintenance : false,
                maintenance_type: asset.maintenance_type ? asset.maintenance_type : null,
                category_id: asset.category_id ? asset.category_id : null,
                area_id: asset.area_id ? asset.area_id : null,
                active: asset.active ? asset.active : false,
            });
            asset.documents.map(doc => {
                this.addNewDocuments(doc);
            });
        });

    }

    changeMaintenance(event) {
        this.formCreate.get('maintenance_type').setValue(null);
        if (event == true) {
            this.formCreate.get('maintenance_type').setValidators(null);
            this.formCreate.get('maintenance_type').setValidators(Validators.required);
            this.formCreate.get('maintenance_type').updateValueAndValidity();
        } else {
            this.formCreate.get('maintenance_type').setValidators(null);
            this.formCreate.get('maintenance_type').updateValueAndValidity();
        }
    }

    getDocuments(): FormArray {
        return this.formCreate.get("documents") as FormArray;
    }

    addNewDocuments(doc?: any) {
        let documents: FormArray;
        documents = this.formCreate.get('documents') as FormArray;
        documents.push(this.initDoc(doc));
    }

    initDoc(doc?: any) {
        return this.formBuilder.group({
            document_type_id: [doc ? doc.document_type_id : '', [Validators.required]],
            file_id: [doc ? doc.file_id : null, [Validators.required]]
        });
    }

    removeDocument(i: number) {
        this.getDocuments().removeAt(i);
    }

    getDocumentsType() {
        this.documentsTypesService.list(1, -1, null, null, {
            sort:'name'
        }).pipe(
            first()
        ).subscribe(results => {
            this.documentsType = results.data;
        });
    }

    getCategories() {
        this.categoryService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.categories = results.data;
        });
    }

    getAreas() {
        this.areaService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.areas = results.data;
        });
    }

    getBuildings(id) {
        this.buildingService.list(1, -1, null, null, { active: true, organic_unit_id: id, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.buildings = results.data;
        });
    }

    getOrganicUnits() {
        this.organicUnitsService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.organics = results.data;
        });
    }

    getWings(id) {
        this.wingsService.list(1, -1, null, null, { active: true, building_id: id, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.wings = results.data;
        });
    }

    getFloors(id) {
        this.floorsService.list(1, -1, null, null, { active: true, wing_id: id, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.floors = results.data;
        });
    }

    getDivisions(id) {
        this.roomsService.list(1, -1, null, null, { active: true, floor_id: id, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.divisions = results.data;
        });
    }

    getServices(id) {
        this.servicesService.list(1, -1, null, null, { active: true, room_id: id, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.services = results.data;
        });
    }

    submitAsset(edit: boolean) {
        this.submitted = true;
        this.loading = true;
        let sendValues: AssetModel = new AssetModel();
        if (this.formCreate.valid) {
            sendValues = this.formCreate.value;
            sendValues.acquisition_value = Number(sendValues.acquisition_value);
            sendValues.electric_power = Number(sendValues.electric_power);
            sendValues.length = Number(sendValues.length);
            sendValues.width = Number(sendValues.width);
            sendValues.height = Number(sendValues.height);
            sendValues.n_sequencial = Number(sendValues.n_sequencial);
            sendValues.n_good = Number(sendValues.n_good);
            if (edit && this.authService.hasPermission('infrastructure:assets:update')) { 
                sendValues.id = this.id;
                this.assetsService.update(sendValues.id, sendValues).pipe(
                    first(), finalize(() => (this.loading = false))).subscribe(() => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Ativo alterado com sucesso'
                        );
                        this.backList();
                    });
            } else if(!edit && this.authService.hasPermission('infrastructure:assets:create')) {
                this.assetsService.create(sendValues).pipe(
                    first(), finalize(() => (this.loading = false))).subscribe(() => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Ativo registado com sucesso'
                        );
                        this.backList();
                    });
            } else {
                this.uiService.showMessage(
                  MessageType.warning,
                  'O utilizador não têm acesso ao serviço solicitado'
                );
                this.loading = false;
            }

        } else {
            this.loading = false;
        }
    }

    backList() {
        this.router.navigateByUrl('/infrastructure/assets/list-assets');
    }

    resetFormCreate() {
        this.submitted = false;
        this.formCreate.reset();
        this.formCreate = this.newFormCreate();
    }

    /*********************************************LOCATIONS******************************* */

    newFormLocation() {
        return this.formBuilder.group({
            organic: ['', [Validators.required]],
            organic_code: [''],
            building: [''],
            building_code: [''],
            wing: [''],
            wing_code: [''],
            floor: [''],
            floor_code: [''],
            division: [''],
            division_code: [''],
            service: [''],
            service_code: [''],
            code: [''],
            description: ['', [Validators.required]]
        });
    }

    getLocations() {
        this.locationsService.list(1, -1).pipe(
            first()
        ).subscribe(results => {
            this.locations = results.data;
            if (this.locations) {
                if (this.locations.length >= 1) {
                    this.locations = this.locations.filter(
                        (thing, i, arr) => arr.findIndex(t => t.description === thing.description) === i
                    );
                }
            }


        });
    }

    dataOrganicFilter(event) {
        if (event) {
            let organicFilter = this.organics.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('organic_code').setValue(organicFilter[0].code);
            this.nameUA = organicFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.getBuildings(event);
            this.formLocation.get('building').setValue(null);
            this.formLocation.controls['building'].enable();
        }

    }

    dataBuildingFilter(event) {
        if (event) {
            let buildingFilter = this.buildings.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('building_code').setValue(buildingFilter[0].code);
            this.nameBuildings = buildingFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.getWings(event);
            this.formLocation.get('wing').setValue(null);
            this.formLocation.controls['wing'].enable();

        }
    }

    dataWingFilter(event) {
        if (event) {
            let wingFilter = this.wings.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('wing_code').setValue(wingFilter[0].code);
            this.nameWings = wingFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.formLocation.get('building').setValidators(Validators.required);
            this.formLocation.get('building').updateValueAndValidity();
            this.getFloors(event);
            this.formLocation.get('floor').setValue(null);
            this.formLocation.controls['floor'].enable();
        }
    }

    dataFloorFilter(event) {
        if (event) {
            let floorFilter = this.floors.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('floor_code').setValue(floorFilter[0].code);
            this.nameFloors = floorFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.formLocation.get('building').setValidators(Validators.required);
            this.formLocation.get('building').updateValueAndValidity();
            this.formLocation.get('wing').setValidators(Validators.required);
            this.formLocation.get('wing').updateValueAndValidity();
            this.getDivisions(event);
            this.formLocation.get('division').setValue(null);
            this.formLocation.controls['division'].enable();
        }
    }

    dataDivisionFilter(event) {
        if (event) {
            let divisionFilter = this.divisions.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('division_code').setValue(divisionFilter[0].code);
            this.nameDivisions = divisionFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.formLocation.get('building').setValidators(Validators.required);
            this.formLocation.get('building').updateValueAndValidity();
            this.formLocation.get('wing').setValidators(Validators.required);
            this.formLocation.get('wing').updateValueAndValidity();
            this.formLocation.get('floor').setValidators(Validators.required);
            this.formLocation.get('floor').updateValueAndValidity();
            this.getServices(event);
            this.formLocation.get('service').setValue(null);
            this.formLocation.controls['service'].enable();
        }
    }

    dataServiceFilter(event) {
        if (event) {
            let serviceFilter = this.services.filter((fil) => {
                return fil.id == event;
            })
            this.formLocation.get('service_code').setValue(serviceFilter[0].code);
            this.nameServices = serviceFilter[0].name;
            this.formLocation.get('description').setValue('');
            this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
                (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
                (this.nameWings ? ('|' + this.nameWings) : '') +
                (this.nameFloors ? ('|' + this.nameFloors) : '') +
                (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
                (this.nameServices ? ('|' + this.nameServices) : ''));
            this.formLocation.get('building').setValidators(Validators.required);
            this.formLocation.get('building').updateValueAndValidity();
            this.formLocation.get('wing').setValidators(Validators.required);
            this.formLocation.get('wing').updateValueAndValidity();
            this.formLocation.get('floor').setValidators(Validators.required);
            this.formLocation.get('floor').updateValueAndValidity();
            this.formLocation.get('division').setValidators(Validators.required);
            this.formLocation.get('division').updateValueAndValidity();

        }
    }

    openModal() {
        this.modalVisible = true;
        this.formLocation.controls['organic_code'].disable();
        this.formLocation.controls['building'].disable();
        this.formLocation.controls['building_code'].disable();
        this.formLocation.controls['wing'].disable();
        this.formLocation.controls['wing_code'].disable();
        this.formLocation.controls['floor_code'].disable();
        this.formLocation.controls['floor'].disable();
        this.formLocation.controls['division_code'].disable();
        this.formLocation.controls['division'].disable();
        this.formLocation.controls['service_code'].disable();
        this.formLocation.controls['service'].disable();
        this.formLocation.controls['description'].disable();
    }

    cancelModal() {
        this.modalVisible = false;
        this.resetFormLocation();
    }

    resetFormLocation() {
        this.submittedLocation = false;
        this.formLocation.reset();
        this.formLocation = this.newFormLocation();
        this.nameUA = '';
        this.nameBuildings = '';
        this.nameWings = '';
        this.nameFloors = '';
        this.nameDivisions = '';
        this.nameServices = '';
    }


    saveLocation() {
        this.submittedLocation = true;
        let sendValues: LocationModel = new LocationModel();
        if (this.formLocation.valid) {
            sendValues.organic_unit_id = this.ff.organic.value;
            if (this.ff.building.value) {
                sendValues.building_id = this.ff.building.value;
            }
            if (this.ff.wing.value) {
                sendValues.wing_id = this.ff.wing.value;
            }
            if (this.ff.floor.value) {
                sendValues.floor_id = this.ff.floor.value;
            }
            if (this.ff.service.value) {
                sendValues.service_id = this.ff.service.value;
            }
            if (this.ff.division.value) {
                sendValues.room_id = this.ff.division.value;
            }
            if (this.ff.code.value) {
                sendValues.code = this.ff.code.value;
            }
            sendValues.description = this.ff.description.value;
            sendValues.active = true;
            this.locationsService.create(sendValues).pipe(
                first()).subscribe(() => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Localização registada com sucesso'
                    );
                    this.getLocations();
                    this.modalVisible = false;
                    this.resetFormLocation();
                });
        }
    }

}
