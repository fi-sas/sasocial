import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { first } from "rxjs/operators";
import { NzModalService } from "ng-zorro-antd";
import { AssetModel } from "../../../models/asset.model";
import { LocationModel } from "../../../models/location.model";
import { AssetsService } from "../../../services/assets.service";
import { LocationsService } from "../../../services/locations.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-infrastructure-list-assets',
    templateUrl: './list-assets.component.html',
    styleUrls: ['./list-assets.component.less']
})

export class ListAssetsInfrastructureComponent extends TableHelper implements OnInit {
    locations: LocationModel[];
    loading: boolean = false;

    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private assetsService: AssetsService,
        private modalService: NzModalService,
        private locationsService: LocationsService,
        private authService: AuthService) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['withRelated'] = 'location';
    }

    ngOnInit() {
        this.getLocations();
        this.initTableData(this.assetsService);
        this.persistentFilters = {
            searchFields: "code,designation"
        }
    }

    getLocations() {
        this.locationsService.list(1, -1).pipe(
            first()
        ).subscribe(results => {
            this.locations = results.data;
                if (this.locations.length >= 1) {
                    this.locations = this.locations.filter(
                        (thing, i, arr) => arr.findIndex(t => t.description === thing.description) === i
                    );
                }
        });
    }

    listComplete() {
        this.filters.search = null;
        this.filters.location_id = null;
        this.filters.created_at = null;
        this.searchData(true)
    }

    desactive(data: AssetModel) {
        if(!this.authService.hasPermission('infrastructure:assets:update')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não têm acesso ao serviço solicitado'
            );
        
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai desativar este ativo. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(data)
        });
    }

    desactiveDataSubmit(data: AssetModel) {
        data.active = false;
        this.assetsService
            .desactive(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Ativo desativado com sucesso'
                );
                this.initTableData(this.assetsService);
            });
    }

    active(data: AssetModel) {
        if(!this.authService.hasPermission('infrastructure:assets:update')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não têm acesso ao serviço solicitado'
            );
        
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai ativar este ativo. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(data)
        });
    }

    activeDataSubmit(data: AssetModel) {
        data.active = true;
        this.assetsService
            .active(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Ativo ativado com sucesso'
                );
                this.initTableData(this.assetsService);
            });
    }

    edit(id: number) {
        if(!this.authService.hasPermission('infrastructure:assets:update')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não têm acesso ao serviço solicitado'
            );
        
            return;
        }
        this.router.navigateByUrl('/infrastructure/assets/create-asset/' + id);
    }

}