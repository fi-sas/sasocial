import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreasCreateComponent } from './areas-create/areas-create.component';
import { AreasInfrastructureComponent } from './areas/areas.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: AreasCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:areas:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: AreasCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:areas:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: AreasInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:areas:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureAreaRoutingModule { }
