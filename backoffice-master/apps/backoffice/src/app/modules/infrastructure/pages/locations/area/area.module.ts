import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { AreasCreateComponent } from './areas-create/areas-create.component';
import { AreasInfrastructureComponent } from './areas/areas.component';
import { InfrastructureAreaRoutingModule } from './area-routing.module';


@NgModule({
  declarations: [
    AreasCreateComponent,
    AreasInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureAreaRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class AreaConfigurationModule { }
