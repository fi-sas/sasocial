import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AreaModel } from '@fi-sas/backoffice/modules/infrastructure/models/area.model';
import { AreaService } from '@fi-sas/backoffice/modules/infrastructure/services/area.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
@Component({
  selector: 'fi-sas-areas-create',
  templateUrl: './areas-create.component.html',
  styleUrls: ['./areas-create.component.less']
})
export class AreasCreateComponent implements OnInit {
  loadingEdit = false;
  formCreate: FormGroup;
  submitted: boolean = false;
  idEdit;
  isEdit: boolean = false;
  loading = false;

  constructor(
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private areasService: AreaService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.formCreate = this.newFormCreate();

    const aux = this.activateRoute.snapshot.paramMap.get('id');
    if (aux) {
      this.isEdit = true;
      this.idEdit = +aux;
      this.getEdit();
    }
  }

  getEdit() {
    this.loadingEdit = true;
    this.areasService.read(this.idEdit).pipe(first(), finalize(() => this.loadingEdit = false))
      .subscribe(
        (service) => {
          this.formCreate.get('name').patchValue(service.data[0].name);
          this.formCreate.get('description').patchValue(service.data[0].description);
          this.formCreate.get('active').patchValue(service.data[0].active);
        }
      );
  }


  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required,trimValidation]],
      description: ['', [Validators.required,trimValidation]],
      active: [false, [Validators.required]]
    });
  }

  submitAreas(edit: boolean) {
    this.loading = true;
    this.submitted = true;
    let sendValues: AreaModel = new AreaModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:areas:create')) {
        this.areasService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Área registada com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/areas/list');
          });

      } else if(edit && this.authService.hasPermission('infrastructure:areas:update')){
        sendValues.id = this.idEdit;
        this.areasService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Área alterada com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/areas/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }

  }

  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }

  get f() { return this.formCreate.controls; }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/areas/list');
  }
}
