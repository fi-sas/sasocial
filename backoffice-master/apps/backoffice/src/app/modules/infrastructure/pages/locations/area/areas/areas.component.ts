import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { AreaModel } from "@fi-sas/backoffice/modules/infrastructure/models/area.model";
import { AreaService } from "@fi-sas/backoffice/modules/infrastructure/services/area.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-areas-infrastructure',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.less']
})


export class AreasInfrastructureComponent extends TableHelper implements OnInit {
  status = [];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private areasService: AreaService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'room';
  }

  ngOnInit() {
    this.initTableData(this.areasService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: AreaModel) {
    if(!this.authService.hasPermission('infrastructure:areas:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Área. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: AreaModel) {
    data.active = false;
    this.areasService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Área desativada com sucesso'
        );
        this.initTableData(this.areasService);
      });
  }

  active(data: AreaModel) {
    if(!this.authService.hasPermission('infrastructure:areas:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Área. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: AreaModel) {
    data.active = true;
    this.areasService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Área ativada com sucesso'
        );
        this.initTableData(this.areasService);
      });
  }

  edit(data) {
    if(!this.authService.hasPermission('infrastructure:areas:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/areas/edit/' + data.id);
  }

  listComplete() {
    this.filters.search = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }
}
