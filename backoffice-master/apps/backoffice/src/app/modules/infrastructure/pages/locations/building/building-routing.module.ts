import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuildingsCreateComponent } from './buildings-create/buildings-create.component';
import { BuildingsInfrastructureComponent } from './buildings/buildings.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: BuildingsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:buildings:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: BuildingsCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:buildings:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: BuildingsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:buildings:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureBuildingRoutingModule { }
