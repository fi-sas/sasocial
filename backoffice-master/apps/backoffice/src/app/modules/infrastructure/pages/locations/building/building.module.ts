import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureBuildingRoutingModule } from './building-routing.module';
import { BuildingsCreateComponent } from './buildings-create/buildings-create.component';
import { BuildingsInfrastructureComponent } from './buildings/buildings.component';


@NgModule({
  declarations: [
    BuildingsCreateComponent,
    BuildingsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureBuildingRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class BuildingConfigurationModule { }
