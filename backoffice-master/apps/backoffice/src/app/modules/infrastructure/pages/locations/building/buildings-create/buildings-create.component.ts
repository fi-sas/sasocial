import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-buildings-create',
  templateUrl: './buildings-create.component.html',
  styleUrls: ['./buildings-create.component.less']
})
export class BuildingsCreateComponent implements OnInit {

  //for the filtration

  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  loading = false;
  idEdit;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;
  idConfigurationUO;
  idOrganUnit: number = -1;
  loadingEdit = false;

  constructor(
    private buildingService: BuildingService,
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private organicService: OrganicUnitsService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getOrganicUnits();
    this.formCreate = this.newFormCreate();
    this.idConfigurationUO = localStorage.getItem('configuration-buildings/create');
    localStorage.removeItem('configuration-buildings/create');
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id) {
      this.isEdit = true;
      this.idEdit = id;
      this.getEdit();
    }
  }

  get f() { return this.formCreate.controls; }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required,trimValidation]],
      code: ['', [Validators.required,trimValidation]],
      address: ['', [Validators.required,trimValidation]],
      district: ['', [Validators.required,trimValidation]],
      city: ['', [Validators.required,trimValidation]],
      locality: ['', [Validators.required,trimValidation]],
      postal_code: ['', [Validators.required]],
      organic_unit_id: ['', [Validators.required]],
      active: [false, [Validators.required]],
      area: ['', [Validators.required]],
    });
  }

  back() {
    this.router.navigateByUrl('/infrastructure/locations/configuration');
    localStorage.setItem("backConfigurationInfra", 'true');
  }

  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }

  submitBuilding(edit: boolean) {
    this.submitted = true;
    this.loading = true;
    let sendValues: BuildingModel = new BuildingModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      sendValues.area = Number(sendValues.area);
      if (!edit && this.authService.hasPermission('infrastructure:buildings:create')) {
        this.buildingService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Edifício registado com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/buildings/list');
          });
      } else if(edit && this.authService.hasPermission('infrastructure:buildings:update')) {
        sendValues.id = this.idEdit;
        this.buildingService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Edifício alterado com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/buildings/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }

  }

  getEdit() {
    this.loadingEdit = true;
    this.buildingService.read(this.idEdit).pipe(first(), finalize(() => this.loadingEdit = false))
      .subscribe(
        (building) => {
          this.formCreate.get('name').patchValue(building.data[0].name);
          this.formCreate.get('code').patchValue(building.data[0].code);
          this.formCreate.get('address').patchValue(building.data[0].address);
          this.formCreate.get('district').patchValue(building.data[0].district);
          this.formCreate.get('locality').patchValue(building.data[0].locality);
          this.formCreate.get('postal_code').patchValue(building.data[0].postal_code);
          this.formCreate.get('active').patchValue(building.data[0].active);
          this.formCreate.get('area').patchValue(building.data[0].area);
          this.formCreate.get('city').patchValue(building.data[0].city);
          this.formCreate.get('organic_unit_id').patchValue(building.data[0].organic_unit_id);
        }
      )
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: "name"
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/buildings/list');
  }
}
