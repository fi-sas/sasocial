import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-buindings-infrastructure',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.less']
})


export class BuildingsInfrastructureComponent extends TableHelper implements OnInit {

  loading: boolean = false;
  loadingOrganicUnit = true;
  organicUnitSelected: number;
  organitUnits: OrganicUnitsModel[] = [];
  status = [];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private organicService: OrganicUnitsService,
    private buildingService: BuildingService,
    private modalService: NzModalService,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'organicUnit';
  }

  ngOnInit() {
    this.getOrganicUnits();
    this.initTableData(this.buildingService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: BuildingModel) {
    if (!this.authService.hasPermission('infrastructure:buildings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Edifício. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: BuildingModel) {
    data.active = false;
    this.buildingService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Edifício desativado com sucesso'
        );
        this.initTableData(this.buildingService);
      });
  }

  active(data: BuildingModel) {
    if (!this.authService.hasPermission('infrastructure:buildings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Edifício. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: BuildingModel) {
    data.active = true;
    this.buildingService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Edifício ativado com sucesso'
        );
        this.initTableData(this.buildingService);
      });
  }
  delete(data: BuildingModel) {
    if (!this.authService.hasPermission('infrastructure:buildings:delete')) {
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Tem a certeza que dejesa eliminar este edificio?',
      nzOnOk: () => this.deleteDataSubmit(data)
    });
  }
  deleteDataSubmit(data: BuildingModel) {
    data.active = true;
    this.buildingService
      .delete(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Edifício eliminado com sucesso'
        );
        this.initTableData(this.buildingService);
      });
  }

  edit(data) {
    if (!this.authService.hasPermission('infrastructure:buildings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/buildings/edit/' + data.id);
  }
  listComplete() {
    this.organicUnitSelected = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }


  search() {
    this.filters.organic_unit_id = this.organicUnitSelected;
    this.searchData(true);
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }
}

