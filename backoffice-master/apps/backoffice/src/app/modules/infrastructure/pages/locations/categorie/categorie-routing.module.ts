import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesCreateComponent } from './categories-create/categories-create.component';
import { CategoriesInfrastructureComponent } from './categories/categories.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: CategoriesCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:categories:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: CategoriesCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:categories:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: CategoriesInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:categories:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureCategorieRoutingModule { }
