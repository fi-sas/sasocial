import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureCategorieRoutingModule } from './categorie-routing.module';
import { CategoriesCreateComponent } from './categories-create/categories-create.component';
import { CategoriesInfrastructureComponent } from './categories/categories.component';


@NgModule({
  declarations: [
    CategoriesCreateComponent,
    CategoriesInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureCategorieRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class CategorieConfigurationModule { }
