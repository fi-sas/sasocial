import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CategoryModel } from '@fi-sas/backoffice/modules/infrastructure/models/category.model';
import { CategoryService } from '@fi-sas/backoffice/modules/infrastructure/services/category.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.less']
})
export class CategoriesCreateComponent implements OnInit {
  loadingEdit = false;
  loading = false;
  idEdit;
  id: string;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;

  constructor(
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private categoriesService: CategoryService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.formCreate = this.newFormCreate();

    const aux = this.activateRoute.snapshot.paramMap.get('id');
    if (aux) {
      this.isEdit = true;
      this.idEdit = aux;
      this.getEdit();
    }
  }

  get f() { return this.formCreate.controls; }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required,trimValidation]],
      description: ['', [Validators.required,trimValidation]],
      active: [false, [Validators.required]]
    });
  }

  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }


  submitCategories(edit: boolean) {
    this.loading = true;
    this.submitted = true;
    let sendValues: CategoryModel = new CategoryModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:categories:create')) {
        this.categoriesService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Categoria registada com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/categories/list');
          });
      } else if (edit && this.authService.hasPermission('infrastructure:categories:update')){
        sendValues.id = this.idEdit;
        this.categoriesService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Categoria alterada com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/categories/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }

  }

  getEdit() {
    this.loadingEdit = true;
    this.categoriesService.read(this.idEdit).pipe(first(), finalize(() => this.loadingEdit = false))
      .subscribe(
        (service) => {
          this.formCreate.get('name').patchValue(service.data[0].name);
          this.formCreate.get('description').patchValue(service.data[0].description);
          this.formCreate.get('active').patchValue(service.data[0].active);
        }
      );
  }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/categories/list');
  }
}
