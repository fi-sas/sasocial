import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { CategoryModel } from "@fi-sas/backoffice/modules/infrastructure/models/category.model";
import { CategoryService } from "@fi-sas/backoffice/modules/infrastructure/services/category.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-categories-infrastructure',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.less']
})


export class CategoriesInfrastructureComponent extends TableHelper implements OnInit {

  formCreate: FormGroup;
  submitted: boolean = false;
  idEdit: number;
  isEdit: boolean = false;
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private categoriesService: CategoryService,
    private formBuilder: FormBuilder,
    private authService: AuthService) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'room';
  }

  ngOnInit() {
    this.initTableData(this.categoriesService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  listComplete() {
    this.filters.search = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }

  desactive(data: CategoryModel) {
    if(!this.authService.hasPermission('infrastructure:categories:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Categoria. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: CategoryModel) {
    data.active = false;
    this.categoriesService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Categoria desativada com sucesso'
        );
        this.initTableData(this.categoriesService);
      });
  }

  active(data: CategoryModel) {
    if(!this.authService.hasPermission('infrastructure:categories:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Categoria. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: CategoryModel) {
    data.active = true;
    this.categoriesService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Categoria ativada com sucesso'
        );
        this.initTableData(this.categoriesService);
      });
  }

  edit(data) {
    if(!this.authService.hasPermission('infrastructure:categories:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/categories/edit/' + data.id);
  }

}
