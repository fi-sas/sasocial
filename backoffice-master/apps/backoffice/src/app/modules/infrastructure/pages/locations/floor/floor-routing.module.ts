import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FloorsCreateComponent } from './floors-create/floors-create.component';
import { FloorsInfrastructureComponent } from './floors/floors.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: FloorsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:floors:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: FloorsCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:floors:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: FloorsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:floors:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureFloorRoutingModule { }
