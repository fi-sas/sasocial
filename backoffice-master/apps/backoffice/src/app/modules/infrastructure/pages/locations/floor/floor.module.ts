import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { FloorsCreateComponent } from './floors-create/floors-create.component';
import { FloorsInfrastructureComponent } from './floors/floors.component';
import { InfrastructureFloorRoutingModule } from './floor-routing.module';

@NgModule({
  declarations: [
    FloorsCreateComponent,
    FloorsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureFloorRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class FloorConfigurationModule { }
