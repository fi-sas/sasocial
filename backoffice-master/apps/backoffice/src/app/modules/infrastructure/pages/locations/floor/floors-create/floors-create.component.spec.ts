import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorsCreateComponent } from './floors-create.component';

describe('FloorsCreateComponent', () => {
  let component: FloorsCreateComponent;
  let fixture: ComponentFixture<FloorsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloorsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
