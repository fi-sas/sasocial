import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { FloorModel } from '@fi-sas/backoffice/modules/infrastructure/models/floor.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { WingModel } from '@fi-sas/backoffice/modules/infrastructure/models/wing.model';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { FloorsService } from '@fi-sas/backoffice/modules/infrastructure/services/floors.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { WingsService } from '@fi-sas/backoffice/modules/infrastructure/services/wings.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-floors-create',
  templateUrl: './floors-create.component.html',
  styleUrls: ['./floors-create.component.less']
})
export class FloorsCreateComponent implements OnInit {
  loadingEdit = false;
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  loadingWing = false;
  wings: WingModel[] = [];

  loading = false;
  idEdit;
  id: string;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;
  idConfigurationWing;

  constructor(
    private buildingService: BuildingService,
    private wingService: WingsService,
    private floorsService: FloorsService,
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private organicService: OrganicUnitsService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getOrganicUnits();
    this.formCreate = this.newFormCreate();
    this.idConfigurationWing = localStorage.getItem('configuration-floors/create');
    localStorage.removeItem('configuration-floors/create');
    const aux = this.activateRoute.snapshot.paramMap.get('id');
    if (aux) {
      this.isEdit = true;
      this.idEdit = aux;
      this.getEdit();
    }
  }

  get f() { return this.formCreate.controls; }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required, trimValidation]],
      code: ['', [Validators.required, trimValidation]],
      wing_id: ['', [Validators.required]],
      building_id: ['', [Validators.required]],
      organic_unit_id: ['', [Validators.required]],
      active: [false, [Validators.required]]
    });
  }

  back() {
    this.router.navigateByUrl('/infrastructure/locations/configuration');
    localStorage.setItem("backConfigurationInfra", 'true');
  }

  getEdit() {
    this.loadingEdit = true;
    this.floorsService.read(this.idEdit)
      .subscribe(
        (floor) => {
          this.formCreate.get('name').patchValue(floor.data[0].name);
          this.formCreate.get('code').patchValue(floor.data[0].code);
          this.formCreate.get('active').patchValue(floor.data[0].active);
          if (floor.data[0].wing_id) {
            this.wingService.read(floor.data[0].wing_id)
              .subscribe(
                (wing) => {
                  if (wing.data[0].building_id) {
                    this.buildingService.read(wing.data[0].building_id).pipe(first(), finalize(() => this.loadingEdit = false))
                      .subscribe(
                        (building) => {
                          if (building.data[0].organic_unit_id) {
                            this.formCreate.get('organic_unit_id').setValue(building.data[0].organic_unit_id);
                            this.formCreate.get('building_id').setValue(wing.data[0].building_id);
                            this.formCreate.get('wing_id').setValue(floor.data[0].wing_id);
                          }
                        }
                      );

                  } else {
                    this.loadingEdit = false;
                  }
                }
              );
          } else {
            this.loadingEdit = false;
          }
        }
      );
  }

  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }

  submitFloor(edit: boolean) {
    this.loading = true;
    this.submitted = true;
    let sendValues: FloorModel = new FloorModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:floors:create')) {
        this.floorsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Piso registado com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/floors/list');
          });
      } else if (edit && this.authService.hasPermission('infrastructure:floors:update')) {
        sendValues.id = this.idEdit;
        this.floorsService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Piso alterado com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/floors/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }

  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    if (id) {
      this.formCreate.get('building_id').setValue(null);
      this.formCreate.get('wing_id').setValue(null);
      this.loadingBuilding = true;
      this.buildingService.list(1, -1, null, null, {
        active: true,
        organic_unit_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
        this.buindings = data.data;
      })
    }
  }

  getWingsByBuilding(id: number) {
    if (id) {
      this.formCreate.get('wing_id').setValue(null);
      this.loadingWing = true;
      this.wingService.list(1, -1, null, null, {
        active: true,
        building_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingWing = false)).subscribe((data) => {
        this.wings = data.data;
      })
    }
  }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/floors/list');
  }

}
