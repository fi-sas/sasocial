import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { WingsService } from "@fi-sas/backoffice/modules/infrastructure/services/wings.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { FloorModel } from "../../../../models/floor.model";
import { FloorsService } from "../../../../services/floors.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-floors-infrastructure',
  templateUrl: './floors.component.html',
  styleUrls: ['./floors.component.less']
})


export class FloorsInfrastructureComponent extends TableHelper implements OnInit {
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  organicUnitSelected: number;
  buildingSelected: number;
  wingsSelected: number;
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  loadingWing = false;
  wings: WingModel[] = [];
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private floorsService: FloorsService,
    private organicService: OrganicUnitsService,
    private buildingService: BuildingService,
    private wingService: WingsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'wing';
  }

  ngOnInit() {
    this.getOrganicUnits();
    this.initTableData(this.floorsService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: FloorModel) {
    if (!this.authService.hasPermission('infrastructure:floors:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Piso. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: FloorModel) {
    data.active = false;
    this.floorsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Piso desativado com sucesso'
        );
        this.initTableData(this.floorsService);
      });
  }

  active(data: FloorModel) {
    if (!this.authService.hasPermission('infrastructure:floors:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Piso. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: FloorModel) {
    data.active = true;
    this.floorsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Piso ativado com sucesso'
        );
        this.initTableData(this.floorsService);
      });
  }
  delete(data: FloorModel) {
    if (!this.authService.hasPermission('infrastructure:floors:delete')) {
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Tem a certeza que dejesa eliminar este piso?',
      nzOnOk: () => this.deleteDataSubmit(data)
    });
  }
  deleteDataSubmit(data: FloorModel) {
    this.floorsService
      .delete(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Piso eliminado com sucesso'
        );
        this.initTableData(this.floorsService);
      });
  }

  edit(data) {
    if (!this.authService.hasPermission('infrastructure:floors:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/floors/edit/' + data.id);
  }

  listComplete() {
    this.organicUnitSelected = null;
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.building_id = null;
    this.filters.wing_id = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }

  search() {
    this.filters.organic_unit_id = this.organicUnitSelected;
    this.filters.building_id = this.buildingSelected;
    this.filters.wing_id = this.wingsSelected;
    this.searchData(true);
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.loadingBuilding = true;
    this.buildingService.list(1, -1, null, null, {
      active: true,
      organic_unit_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
      this.buindings = data.data;
    })
  }

  getWingsByBuilding(id: number) {
    this.wingsSelected = null;
    this.loadingWing = true;
    this.wingService.list(1, -1, null, null, {
      active: true,
      building_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingWing = false)).subscribe((data) => {
      this.wings = data.data;
    })
  }
}
