import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { BuildingModel } from "../../../models/building.model";
import { FloorModel } from "../../../models/floor.model";
import { OrganicUnitsModel } from "../../../models/organic-units.model";
import { RoomModel } from "../../../models/room.model";
import { ServiceModel } from "../../../models/service.model";
import { WingModel } from "../../../models/wing.model";
import { BuildingService } from "../../../services/building.service";
import { FloorsService } from "../../../services/floors.service";
import { OrganicUnitsService } from "../../../services/organic-units.service";
import { RoomsService } from "../../../services/rooms.service";
import { ServicesService } from "../../../services/services.service";
import { WingsService } from "../../../services/wings.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-infrastructure-configuration',
    templateUrl: './infrastructure_configuration.component.html',
    styleUrls: ['./infrastructure_configuration.component.less']
})


export class InfrastructureConfigurationComponent implements OnInit {
    organics: OrganicUnitsModel[];
    buildings: BuildingModel[];
    wings: WingModel[];
    floors: FloorModel[];
    divisions: RoomModel[];
    services: ServiceModel[];
    idOrganic: number;
    idBuilding: number;
    idWing: number;
    idFloor: number;
    idDivision: number;
    buildingsView: boolean = false;
    floorsView: boolean = false;
    wingsView: boolean = false;
    divisionView: boolean = false;
    servicesView: boolean = false;


    constructor(private organicUnitsService: OrganicUnitsService,
        private floorsService: FloorsService,
        private router: Router,
        private roomsService: RoomsService,
        private servicesService: ServicesService,
        private buildingService: BuildingService,
        private wingsService: WingsService,
        private authService: AuthService) {

    }

    ngOnInit() {
        this.getOrganicUnits();
        let auxUA = localStorage.getItem("UAInfra");
        let auxBuild = localStorage.getItem("BuildInfra");
        let auxWingsInfra = localStorage.getItem("WingsInfra");
        let auxFloorsInfra = localStorage.getItem("FloorsInfra");
        let auxDivisionsInfra = localStorage.getItem("DivisionsInfra");
        let back = localStorage.getItem("backConfigurationInfra");
        if (auxUA !== null && auxUA !== "null" && auxUA !== undefined && back) {
            this.idOrganic = Number(auxUA);
            this.getBuildings(this.idOrganic);
        }
        if (auxBuild !== null && auxBuild !== undefined && back) {
            this.idBuilding = Number(auxBuild);
            this.getWings(this.idBuilding);
        }
        if (auxWingsInfra !== null && auxWingsInfra !== undefined && back) {
            this.idWing = Number(auxWingsInfra);
            this.getFloors(this.idWing);
        }
        if (auxFloorsInfra !== null && auxFloorsInfra !== undefined && back) {
            this.idFloor = Number(auxFloorsInfra);
            this.getDivisions(this.idFloor);

        }
        if (auxDivisionsInfra !== null && auxDivisionsInfra !== undefined && back) {
            this.idDivision = Number(auxDivisionsInfra);
            this.getServices(this.idDivision);

        }
        this.deleteStorage();
             
    }

    deleteStorage(){
        localStorage.removeItem("DivisionsInfra");
        localStorage.removeItem("FloorsInfra");
        localStorage.removeItem("WingsInfra");
        localStorage.removeItem("BuildInfra");
        localStorage.removeItem("UAInfra");
        localStorage.removeItem("backConfigurationInfra");
    }

    getOrganicUnits() {
        if(this.authService.hasPermission('infrastructure:organic-units:read')){
            this.organicUnitsService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
                first()
            ).subscribe(results => {
                this.organics = results.data;
            });
        }
    }

    getBuildings(id) {
        if(this.authService.hasPermission('infrastructure:buildings:read')){
            this.idOrganic = id;
            this.floorsView = false;
            this.wingsView = false;
            this.divisionView = false;
            this.servicesView = false;
            this.idBuilding = -1;
            this.idWing = -1;
            this.idFloor = -1;
            this.idDivision = -1;
            this.buildingService.list(1, -1, null, null, {
                organic_unit_id: id,
                active: true,
                sort:'name'
            }).pipe(
                first()
            ).subscribe(results => {
                this.buildings = results.data;
                this.buildingsView = true;
            });
        } 
    }

    getWings(id) {
        if(this.authService.hasPermission('infrastructure:wings:read')){
            this.idBuilding = id;
            this.floorsView = false;
            this.divisionView = false;
            this.servicesView = false;
            this.idWing = -1;
            this.idFloor = -1;
            this.idDivision = -1;
            this.wingsService.list(1, -1, null, null, {
                building_id: id,
                active: true,
                sort:'name'
            }).pipe(
                first()
            ).subscribe(results => {
                this.wings = results.data;
                this.wingsView = true;
            });
        }
    }

    getFloors(id) {
        if(this.authService.hasPermission('infrastructure:floors:read')){
            this.idWing = id;
            this.idDivision = -1;
            this.divisionView = false;
            this.servicesView = false;
            this.idFloor = -1;
            this.idDivision = -1;
            this.floorsService.list(1, -1, null, null, {
                wing_id: id,
                active: true,
                sort:'name'
            }).pipe(
                first()
            ).subscribe(results => {
                this.floors = results.data;
                this.floorsView = true;
            });
        }
    }

    getDivisions(id) {
        if(this.authService.hasPermission('infrastructure:rooms:read')){
            this.idFloor = id;
            this.servicesView = false;
            this.idDivision = -1;
            this.roomsService.list(1, -1, null, null, {
                floor_id: id,
                active: true,
                sort:'name'
            }).pipe(
                first()
            ).subscribe(results => {
                this.divisions = results.data;
                this.divisionView = true;
            });
        }
    }

    getServices(id) {
        if(this.authService.hasPermission('infrastructure:services:read')){
            this.idDivision = id;
            this.servicesService.list(1, -1, null, null, {
                room_id: id,
                active: true,
                sort:'name'
            }).pipe(
                first()
            ).subscribe(results => {
                this.services = results.data;
                this.servicesView = true;
            });
        }
    }

    newData(router: string, id: number) {
        if(this.idOrganic != null){
            localStorage.setItem('UAInfra', "" + this.idOrganic);
        }else{

            localStorage.setItem('UAInfra', null);
        }
        if(this.idBuilding != -1 && this.idBuilding != null){
            localStorage.setItem('BuildInfra', "" + this.idBuilding);
        }
        if(this.idWing != -1 && this.idWing != null){
            localStorage.setItem('WingsInfra', "" + this.idWing);
        }
        if(this.idFloor != -1 && this.idFloor != null){
            localStorage.setItem('FloorsInfra', "" + this.idFloor);
        }
        if(this.idDivision != -1 && this.idDivision != null){
            localStorage.setItem('DivisionsInfra', "" + this.idDivision);
        }
        if(router != 'organic-units/create'){
            localStorage.setItem('configuration-' + router, "" + id);
        }else{
            localStorage.setItem('configuration-' + router, 'true');
        }
        this.router.navigateByUrl('/infrastructure/locations/' + router);
    }
} 