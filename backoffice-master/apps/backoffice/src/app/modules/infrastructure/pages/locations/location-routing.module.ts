import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfrastructureConfigurationComponent } from './infrastructure_configuration/infrastructure_configuration.component';



const routes: Routes = [
  { path: '', redirectTo: 'configuration', pathMatch: 'full' },
  {
    path: 'configuration',
    component: InfrastructureConfigurationComponent,
    data: { breadcrumb: 'Listar Infraestruturas', title: 'Listar Infraestruturas'},
    resolve: { }
  },
  //wings
  {
    path: 'wings',
    loadChildren: './wing/wing.module#WingConfigurationModule',
    data: { breadcrumb: 'Alas', title: 'Alas', scope:'infrastructure:wings'},
    resolve: { }
  },
  //services
  {
    path: 'services',
    loadChildren: './service/service.module#ServiceConfigurationModule',
    data: { breadcrumb: 'Serviços', title: 'Serviços', scope:'infrastructure:services'},
    resolve: { }
  },
  //rooms aka divisions
  {
    path: 'divisions',
    loadChildren: './room/room.module#RoomConfigurationModule',
    data: { breadcrumb: 'Divisões', title: 'Divisões', scope:'infrastructure:rooms'},
    resolve: { }
  },
  // organic units
  {
    path: 'organic-units',
    loadChildren: './organic-unit/organic-unit.module#OrganicUnitConfigurationModule',
    data: { breadcrumb: 'Unidades Orgânicas', title: 'Unidades Orgânicas', scope:'infrastructure:organic-units'},
    resolve: { }
  },
  //floors
  {
    path: 'floors',
    loadChildren: './floor/floor.module#FloorConfigurationModule',
    data: { breadcrumb: 'Pisos', title: 'Pisos', scope:'infrastructure:floors'},
    resolve: { }
  },
  //buildings
  {
    path: 'buildings',
    loadChildren: './building/building.module#BuildingConfigurationModule',
    data: { breadcrumb: 'Edifícios', title: 'Edifícios', scope:'infrastructure:buildings'},
    resolve: { }
  },
  //areas
  {
    path: 'areas',
    loadChildren: './area/area.module#AreaConfigurationModule',
    data: { breadcrumb: 'Áreas', title: 'Áreas', scope:'infrastructure:areas'},
    resolve: { }
  },
  //categories
  {
    path: 'categories',
    loadChildren: './categorie/categorie.module#CategorieConfigurationModule',
    data: { breadcrumb: 'Categorias', title: 'Categorias', scope:'infrastructure:categories'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureConfigurationRoutingModule { }
