import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureConfigurationRoutingModule } from './location-routing.module';
import { InfrastructureConfigurationComponent } from './infrastructure_configuration/infrastructure_configuration.component';


@NgModule({
  declarations: [
    InfrastructureConfigurationComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureConfigurationRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class InfrastructureConfigurationModule { }
