import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganicUnitsCreateComponent } from './organic-units-create/organic-units-create.component';
import { OrganicUnitsInfrastructureComponent } from './organic-units/organic-units.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: OrganicUnitsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:organic-units:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: OrganicUnitsCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:organic-units:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: OrganicUnitsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:organic-units:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureOrganicUnitRoutingModule { }
