import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureOrganicUnitRoutingModule } from './organic-unit-routing.module';
import { OrganicUnitsCreateComponent } from './organic-units-create/organic-units-create.component';
import { OrganicUnitsInfrastructureComponent } from './organic-units/organic-units.component';


@NgModule({
  declarations: [
    OrganicUnitsCreateComponent,
    OrganicUnitsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureOrganicUnitRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class OrganicUnitConfigurationModule { }
