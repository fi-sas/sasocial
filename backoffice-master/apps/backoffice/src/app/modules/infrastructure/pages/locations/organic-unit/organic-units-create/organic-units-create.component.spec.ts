import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganicUnitsCreateComponent } from './organic-units-create.component';

describe('OrganicUnitsCreateComponent', () => {
  let component: OrganicUnitsCreateComponent;
  let fixture: ComponentFixture<OrganicUnitsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganicUnitsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganicUnitsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
