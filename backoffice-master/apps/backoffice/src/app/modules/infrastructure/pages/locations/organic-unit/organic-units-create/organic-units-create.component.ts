import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
@Component({
  selector: 'fi-sas-organic-units-create',
  templateUrl: './organic-units-create.component.html',
  styleUrls: ['./organic-units-create.component.less']
})
export class OrganicUnitsCreateComponent implements OnInit {

  loading = false;
  loadingEdit = false;
  idEdit;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;
  isBack;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private organicUnitsService: OrganicUnitsService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.formCreate = this.newFormCreate();
    this.isBack = localStorage.getItem('configuration-organic-units/create');
    localStorage.removeItem('configuration-organic-units/create');
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id) {
      this.isEdit = true;
      this.idEdit = id;
      this.getEdit();
    }
  }

  get f() { return this.formCreate.controls; }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required, trimValidation]],
      code: ['', [Validators.required, trimValidation]],

      address: ['', [Validators.required, trimValidation]],
      locality: ['', [Validators.required, trimValidation]],
      city: ['', [Validators.required, trimValidation]],
      district: ['', [Validators.required, trimValidation]],
      postal_code: ['', [Validators.required, Validators.pattern(/^\d{4}-\d{3}$/),]],
      phone: ['', [Validators.required, trimValidation]],
      email: ['', [Validators.email]],
      dgs_code: ['', [trimValidation]],
      active: [false, [Validators.required]],
      external_ref: ['', trimValidation],
      is_school: [false, [Validators.required]]
    });
  }

  back() {
    this.router.navigateByUrl('/infrastructure/locations/configuration');
    localStorage.setItem("backConfigurationInfra", 'true');
  }

  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }


  submitOrganicUnits(edit: boolean) {
    this.submitted = true;
    if (this.formCreate.valid) { 
      this.loading = true;
      let sendValues: OrganicUnitsModel = new OrganicUnitsModel();
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:organic-units:create')) {
        this.organicUnitsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Unidade Orgânica registada com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/organic-units/list');
          });
      } else if (edit && this.authService.hasPermission('infrastructure:organic-units:update')) {
        sendValues.id = this.idEdit;
        this.organicUnitsService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Unidade Orgânica alterada com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/organic-units/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }
  }

  getEdit() {
    this.loadingEdit = true;
    this.organicUnitsService.read(this.idEdit).pipe(first(), finalize(() => this.loadingEdit = false))
      .subscribe(
        (organic) => {
          this.formCreate.get('name').patchValue(organic.data[0].name);
          this.formCreate.get('code').patchValue(organic.data[0].code);
          this.formCreate.get('address').patchValue(organic.data[0].address);
          this.formCreate.get('locality').patchValue(organic.data[0].locality);
          this.formCreate.get('city').patchValue(organic.data[0].city);
          this.formCreate.get('district').patchValue(organic.data[0].district);
          this.formCreate.get('postal_code').patchValue(organic.data[0].postal_code);
          this.formCreate.get('phone').patchValue(organic.data[0].phone);
          this.formCreate.get('email').patchValue(organic.data[0].email);
          this.formCreate.get('active').patchValue(organic.data[0].active);
          this.formCreate.get('is_school').patchValue(organic.data[0].is_school);
          this.formCreate.get('external_ref').patchValue(organic.data[0].external_ref);
          this.formCreate.get('dgs_code').patchValue(organic.data[0].dgs_code);
        }
      );
  }


  backList() {
    this.router.navigateByUrl('/infrastructure/locations/organic-units/list');
  }

}
