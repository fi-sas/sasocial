import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { OrganicUnitsModel } from "../../../../models/organic-units.model";
import { OrganicUnitsService } from "../../../../services/organic-units.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-organic-units-infrastructure',
  templateUrl: './organic-units.component.html',
  styleUrls: ['./organic-units.component.less']
})


export class OrganicUnitsInfrastructureComponent extends TableHelper implements OnInit {

  filterState: boolean = null;
  filterDate: Date = null;
  filterCode = '';
  filterName = ''
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private organicUnitsService: OrganicUnitsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.initTableData(this.organicUnitsService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }


  listComplete() {
    this.filters.name = null;
    this.filters.active = null;
    this.filters.code = null;
    this.filters.created_at = null;
    this.searchData(true)
  }


  desactive(data: OrganicUnitsModel) {
    if(!this.authService.hasPermission('infrastructure:organic-units:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Unidade Orgânica. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: OrganicUnitsModel) {
    data.active = false;
    this.organicUnitsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Unidade Orgânica desativada com sucesso'
        );
        this.initTableData(this.organicUnitsService);
      });
  }

  active(data: OrganicUnitsModel) {
    if(!this.authService.hasPermission('infrastructure:organic-units:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Unidade Orgânica. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: OrganicUnitsModel) {
    data.active = true;
    this.organicUnitsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Unidade Orgânica ativada com sucesso'
        );
        this.initTableData(this.organicUnitsService);
      });
  }

  edit(data) {
    if(!this.authService.hasPermission('infrastructure:organic-units:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      
      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/organic-units/edit/' + data.id);
  }


  search() {
    this.searchData(true);
  }
}
