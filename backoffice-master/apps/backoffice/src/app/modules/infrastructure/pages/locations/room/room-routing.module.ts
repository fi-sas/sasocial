import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoomsCreateComponent } from './rooms-create/rooms-create.component';
import { RoomsInfrastructureComponent } from './rooms/rooms.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: RoomsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:rooms:create'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: RoomsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:rooms:update'},
    resolve: { }
  },
  {
    path: 'list',
    component: RoomsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:rooms:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureRoomRoutingModule { }
