import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { RoomsCreateComponent } from './rooms-create/rooms-create.component';
import { RoomsInfrastructureComponent } from './rooms/rooms.component';
import { InfrastructureRoomRoutingModule } from './room-routing.module';


@NgModule({
  declarations: [
    RoomsCreateComponent,
    RoomsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureRoomRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class RoomConfigurationModule { }
