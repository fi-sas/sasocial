import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { FloorModel } from '@fi-sas/backoffice/modules/infrastructure/models/floor.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { RoomModel } from '@fi-sas/backoffice/modules/infrastructure/models/room.model';
import { WingModel } from '@fi-sas/backoffice/modules/infrastructure/models/wing.model';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { FloorsService } from '@fi-sas/backoffice/modules/infrastructure/services/floors.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { RoomsService } from '@fi-sas/backoffice/modules/infrastructure/services/rooms.service';
import { WingsService } from '@fi-sas/backoffice/modules/infrastructure/services/wings.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
@Component({
  selector: 'fi-sas-rooms-create',
  templateUrl: './rooms-create.component.html',
  styleUrls: ['./rooms-create.component.less']
})
export class RoomsCreateComponent implements OnInit {
  loadingEdit = false;
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  loadingWing = false;
  wings: WingModel[] = [];
  floors: FloorModel[] = [];
  loadingFloors = false;

  loading = false;
  idEdit;
  id: string;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;
  idConfigurationFloor;

  constructor(
    private buildingService: BuildingService,
    private wingService: WingsService,
    private floorsService: FloorsService,
    private roomsService: RoomsService,
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private organicService: OrganicUnitsService,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.formCreate = this.newFormCreate();
    this.idConfigurationFloor = localStorage.getItem('configuration-divisions/create');
    localStorage.removeItem('configuration-divisions/create');
    this.getOrganicUnits()

    const aux = this.activateRoute.snapshot.paramMap.get('id');
    if (aux) {
      this.isEdit = true;
      this.idEdit = aux;
      this.getEdit();
    }
  }

  getEdit() {
    this.loadingEdit = true;
    this.roomsService.read(this.idEdit)
      .subscribe(
        (room) => {
          this.formCreate.get('name').patchValue(room.data[0].name);
          this.formCreate.get('code').patchValue(room.data[0].code);
          this.formCreate.get('active').patchValue(room.data[0].active);
          if (room.data[0].floor_id) {
            this.floorsService.read(room.data[0].floor_id)
              .subscribe(
                (floor) => {
                  if (floor.data[0].wing_id) {
                    this.wingService.read(floor.data[0].wing_id)
                      .subscribe(
                        (wing) => {
                          if (wing.data[0].building_id) {
                            this.buildingService.read(wing.data[0].building_id).pipe(first(), finalize(() => this.loadingEdit = false))
                              .subscribe(
                                (building) => {
                                  if (building.data[0].organic_unit_id) {
                                    this.formCreate.get('organic_unit_id').setValue(building.data[0].organic_unit_id);
                                    this.formCreate.get('building_id').setValue(wing.data[0].building_id);
                                    this.formCreate.get('wing_id').setValue(floor.data[0].wing_id);
                                    this.formCreate.get('floor_id').setValue(room.data[0].floor_id);
                                  }
                                }
                              );

                          } else {
                            this.loadingEdit = false;
                          }
                        }
                      );
                  } else {
                    this.loadingEdit = false;
                  }
                }
              );
          } else {
            this.loadingEdit = false;
          }
        }
      )
  }

  get f() { return this.formCreate.controls; }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required, trimValidation]],
      code: ['', [Validators.required, trimValidation]],
      organic_unit_id: ['', [Validators.required]],
      building_id: ['', [Validators.required]],
      wing_id: ['', [Validators.required]],
      floor_id: ['', [Validators.required]],
      active: [false, [Validators.required]]
    });
  }


  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }


  submitRooms(edit: boolean) {
    this.loading = true;
    this.submitted = true;
    let sendValues: RoomModel = new RoomModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:rooms:create')) {
        this.roomsService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Divisão registada com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/divisions/list');
          });
      } else if (edit && this.authService.hasPermission('infrastructure:rooms:update')) {
        sendValues.id = this.idEdit;
        this.roomsService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Divisão alterada com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/divisions/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }

  }

  back() {
    this.router.navigateByUrl('/infrastructure/locations/configuration');
    localStorage.setItem("backConfigurationInfra", 'true');
  }


  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    if (id) {
      this.formCreate.get('building_id').setValue(null);
      this.formCreate.get('wing_id').setValue(null);
      this.formCreate.get('floor_id').setValue(null);
      this.loadingBuilding = true;
      this.buildingService.list(1, -1, null, null, {
        active: true,
        organic_unit_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
        this.buindings = data.data;
      })
    }
  }

  getWingsByBuilding(id: number) {
    if (id) {
      this.formCreate.get('wing_id').setValue(null);
      this.formCreate.get('floor_id').setValue(null);
      this.loadingWing = true;
      this.wingService.list(1, -1, null, null, {
        active: true,
        building_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingWing = false)).subscribe((data) => {
        this.wings = data.data;
      })
    }
  }

  getFloorsByWing(id: number) {
    if (id) {
      this.formCreate.get('floor_id').setValue(null);
      this.loadingFloors = true;
      this.floorsService.list(1, -1, null, null, {
        active: true,
        wing_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingFloors = false)).subscribe((data) => {
        this.floors = data.data;
      })
    }
  }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/divisions/list');
  }

}
