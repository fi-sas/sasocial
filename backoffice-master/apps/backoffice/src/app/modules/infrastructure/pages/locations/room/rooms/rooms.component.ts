import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { FloorModel } from "@fi-sas/backoffice/modules/infrastructure/models/floor.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { RoomModel } from "@fi-sas/backoffice/modules/infrastructure/models/room.model";
import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { FloorsService } from "@fi-sas/backoffice/modules/infrastructure/services/floors.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { RoomsService } from "@fi-sas/backoffice/modules/infrastructure/services/rooms.service";
import { WingsService } from "@fi-sas/backoffice/modules/infrastructure/services/wings.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-rooms-infrastructure',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.less']
})


export class RoomsInfrastructureComponent extends TableHelper implements OnInit {
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  organicUnitSelected: number;
  buildingSelected: number;
  wingsSelected: number;
  floorsSelected: number;
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  loadingWing = false;
  wings: WingModel[] = [];
  loadingFloor = false;
  floors: FloorModel[] = [];
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private roomsService: RoomsService,
    private floorsService: FloorsService,
    private organicService: OrganicUnitsService,
    private buildingService: BuildingService,
    private wingService: WingsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'floor';
  }

  ngOnInit() {
    this.getOrganicUnits();
    this.initTableData(this.roomsService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: RoomModel) {
    if (!this.authService.hasPermission('infrastructure:rooms:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Divisão. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: RoomModel) {
    data.active = false;
    this.roomsService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Divisão desativada com sucesso'
        );
        this.initTableData(this.roomsService);
      });
  }

  active(data: RoomModel) {
    if (!this.authService.hasPermission('infrastructure:rooms:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Divisão. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }
  delete(data: RoomModel) {
    if (!this.authService.hasPermission('infrastructure:rooms:delete')) {
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Tem a certeza que dejesa eliminar esta divisão?',
      nzOnOk: () => this.deleteSubmit(data)
    });
  }
  deleteSubmit(data: RoomModel) {
    this.roomsService.delete(data.id).subscribe(result => {
      this.uiService.showMessage(
        MessageType.success,
        'Divisão eliminada com sucesso'
      );
      this.initTableData(this.roomsService);
    });
  }

  activeDataSubmit(data: RoomModel) {
    data.active = true;
    this.roomsService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Divisão ativada com sucesso'
        );
        this.initTableData(this.roomsService);
      });
  }

  edit(data) {
    if (!this.authService.hasPermission('infrastructure:rooms:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/divisions/edit/' + data.id);
  }


  listComplete() {
    this.organicUnitSelected = null;
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.building_id = null;
    this.filters.wing_id = null;
    this.filters.floor_id = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }


  search() {
    this.filters.organic_unit_id = this.organicUnitSelected;
    this.filters.building_id = this.buildingSelected;
    this.filters.wing_id = this.wingsSelected;
    this.filters.floor_id = this.floorsSelected;
    this.searchData(true);
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.loadingBuilding = true;
    this.buildingService.list(1, -1, null, null, {
      active: true,
      organic_unit_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
      this.buindings = data.data;
    })
  }

  getWingsByBuilding(id: number) {
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.loadingWing = true;
    this.wingService.list(1, -1, null, null, {
      active: true,
      building_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingWing = false)).subscribe((data) => {
      this.wings = data.data;
    })
  }

  getFloorsByWing(id: number) {
    this.floorsSelected = null;
    this.loadingFloor = true;
    this.floorsService.list(1, -1, null, null, {
      active: true,
      wing_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingFloor = false)).subscribe((data) => {
      this.floors = data.data;
    })
  }
}
