import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesCreateComponent } from './services-create/services-create.component';
import { ServicesInfrastructureComponent } from './services/services.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: ServicesCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:services:create'},
    resolve: { }
  },
  {
    path: 'list',
    component: ServicesInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:services:update'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: ServicesCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:services:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureServiceRoutingModule { }
