import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureServiceRoutingModule } from './service-routing.module';
import { ServicesInfrastructureComponent } from './services/services.component';
import { ServicesCreateComponent } from './services-create/services-create.component';


@NgModule({
  declarations: [
    ServicesCreateComponent,
    ServicesInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureServiceRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class ServiceConfigurationModule { }
