import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { RoomsModule } from "@fi-sas/backoffice/modules/accommodation/modules/rooms/rooms.module";
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { FloorModel } from "@fi-sas/backoffice/modules/infrastructure/models/floor.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { RoomModel } from "@fi-sas/backoffice/modules/infrastructure/models/room.model";
import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { FloorsService } from "@fi-sas/backoffice/modules/infrastructure/services/floors.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { RoomsService } from "@fi-sas/backoffice/modules/infrastructure/services/rooms.service";
import { WingsService } from "@fi-sas/backoffice/modules/infrastructure/services/wings.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { ServiceModel } from "../../../../models/service.model";
import { ServicesService } from "../../../../services/services.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-services-infrastructure',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.less']
})


export class ServicesInfrastructureComponent extends TableHelper implements OnInit {

  organicUnitSelected: number;
  buildingSelected: number;
  wingsSelected: number;
  floorsSelected: number;
  roomsSelected: number;

  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  loadingWing = false;
  wings: WingModel[] = [];
  loadingFloor = false;
  floors: FloorModel[] = [];
  loadingRoom = false;
  rooms: RoomModel[] = [];
  status = [];


  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private serviceService: ServicesService,
    private formBuilder: FormBuilder,
    private floorsService: FloorsService,
    private organicService: OrganicUnitsService,
    private buildingService: BuildingService,
    private wingService: WingsService,
    private roomsService: RoomsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'room';
  }

  ngOnInit() {
    this.getOrganicUnits();
    this.initTableData(this.serviceService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: ServiceModel) {
    if (!this.authService.hasPermission('infrastructure:services:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar este Serviço. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: ServiceModel) {
    data.active = false;
    this.serviceService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Serviço desativado com sucesso'
        );
        this.initTableData(this.serviceService);
      });
  }

  active(data: ServiceModel) {
    if (!this.authService.hasPermission('infrastructure:services:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar este Serviço. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: ServiceModel) {
    data.active = true;
    this.serviceService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Serviço ativado com sucesso'
        );
        this.initTableData(this.serviceService);
      });
  }

  delete(data: ServiceModel) {
    if (!this.authService.hasPermission('infrastructure:services:delete')) {
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Tem a certeza que dejesa eliminar este serviço?',
      nzOnOk: () => this.deleteDataSubmit(data)
    });
  }

  deleteDataSubmit(data: ServiceModel) {
    this.serviceService
      .delete(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Serviço eliminado com sucesso'
        );
        this.initTableData(this.serviceService);
      });
  }

  edit(data) {
    if (!this.authService.hasPermission('infrastructure:services:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/services/edit/' + data.id);
  }

  listComplete() {
    this.organicUnitSelected = null;
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.roomsSelected = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.building_id = null;
    this.filters.wing_id = null;
    this.filters.floor_id = null;
    this.filters.room_id = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.active = null;
    this.searchData(true)
  }

  search() {
    this.filters.organic_unit_id = this.organicUnitSelected;
    this.filters.building_id = this.buildingSelected;
    this.filters.wing_id = this.wingsSelected;
    this.filters.floor_id = this.floorsSelected;
    this.filters.room_id = this.roomsSelected;
    this.searchData(true);
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    this.buildingSelected = null;
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.roomsSelected = null;
    this.loadingBuilding = true;
    this.buildingService.list(1, -1, null, null, {
      active: true,
      organic_unit_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
      this.buindings = data.data;
    })
  }

  getWingsByBuilding(id: number) {
    this.wingsSelected = null;
    this.floorsSelected = null;
    this.roomsSelected = null;
    this.loadingWing = true;
    this.wingService.list(1, -1, null, null, {
      active: true,
      building_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingWing = false)).subscribe((data) => {
      this.wings = data.data;
    })
  }

  getFloorsByWing(id: number) {
    this.floorsSelected = null;
    this.roomsSelected = null;
    this.loadingFloor = true;
    this.floorsService.list(1, -1, null, null, {
      active: true,
      wing_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingFloor = false)).subscribe((data) => {
      this.floors = data.data;
    })
  }

  getRoomsByFloors(id: number) {
    this.roomsSelected = null;
    this.loadingRoom = true;
    this.roomsService.list(1, -1, null, null, {
      active: true,
      floor_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingRoom = false)).subscribe((data) => {
      this.rooms = data.data;
    })
  }
}
