import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WingsCreateComponent } from './wings-create/wings-create.component';
import { WingsInfrastructureComponent } from './wings/wings.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  //areas
  {
    path: 'create',
    component: WingsCreateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'infrastructure:wings:create'},
    resolve: { }
  },
  {
    path: 'list',
    component: WingsInfrastructureComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'infrastructure:wings:update'},
    resolve: { }
  },
  {
    path: 'edit/:id',
    component: WingsCreateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'infrastructure:wings:read'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureWingRoutingModule { }
