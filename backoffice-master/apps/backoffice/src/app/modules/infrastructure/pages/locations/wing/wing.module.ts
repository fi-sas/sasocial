import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { InfrastructureWingRoutingModule } from './wing-routing.module';
import { WingsCreateComponent } from './wings-create/wings-create.component';
import { WingsInfrastructureComponent } from './wings/wings.component';


@NgModule({
  declarations: [
    WingsCreateComponent,
    WingsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InfrastructureWingRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class WingConfigurationModule { }
