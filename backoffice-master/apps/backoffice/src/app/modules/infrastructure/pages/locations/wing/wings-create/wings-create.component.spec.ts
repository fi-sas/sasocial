import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WingsCreateComponent } from './wings-create.component';

describe('WingsCreateComponent', () => {
  let component: WingsCreateComponent;
  let fixture: ComponentFixture<WingsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WingsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WingsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
