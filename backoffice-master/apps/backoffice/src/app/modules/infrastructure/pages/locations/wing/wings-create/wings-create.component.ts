import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { WingModel } from '@fi-sas/backoffice/modules/infrastructure/models/wing.model';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { WingsService } from '@fi-sas/backoffice/modules/infrastructure/services/wings.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-wings-create',
  templateUrl: './wings-create.component.html',
  styleUrls: ['./wings-create.component.less']
})
export class WingsCreateComponent implements OnInit {
  loadingEdit = false;
  idConfigurationBuild;
  loading = false;
  idEdit;
  id: string;
  isEdit: boolean = false;
  submitted: boolean = false;
  formCreate: FormGroup;
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  loadingBuilding = false;
  buindings: BuildingModel[] = [];

  constructor(
    private buildingService: BuildingService,
    private wingService: WingsService,
    private uiService: UiService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private organicService: OrganicUnitsService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  get f() { return this.formCreate.controls; }

  ngOnInit() {
    this.getOrganicUnits();
    this.formCreate = this.newFormCreate();

    this.idConfigurationBuild = localStorage.getItem('configuration-wings/create');
    localStorage.removeItem('configuration-wings/create');
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id) {
      this.isEdit = true;
      this.idEdit = id;
      this.getEdit();
    }
  }

  newFormCreate() {
    return this.formBuilder.group({
      name: ['', [Validators.required,trimValidation]],
      code: ['', [Validators.required,trimValidation]],
      organic_unit_id: ['', [Validators.required]],
      building_id: ['', [Validators.required]],
      active: [false, [Validators.required]]
    });
  }

  back() {
    this.router.navigateByUrl('/infrastructure/locations/configuration');
    localStorage.setItem("backConfigurationInfra", 'true');
  }

  getEdit() {
    this.loadingEdit = true;
    this.wingService.read(this.idEdit)
      .subscribe(
        (wing) => {
          this.formCreate.get('name').patchValue(wing.data[0].name);
          this.formCreate.get('code').patchValue(wing.data[0].code);
          this.formCreate.get('active').patchValue(wing.data[0].active);
          if (wing.data[0].building_id) {
            this.buildingService.read(wing.data[0].building_id).pipe(first(), finalize(() => this.loadingEdit = false))
              .subscribe(
                (building) => {
                  if (building.data[0].organic_unit_id) {
                    this.formCreate.get('organic_unit_id').patchValue(building.data[0].organic_unit_id);
                    this.formCreate.get('building_id').patchValue(wing.data[0].building_id);
                  }
                }
              );

          }else {
            this.loadingEdit  = false;
          }
        }
      );
  }


  submitWing(edit: boolean) {
    this.loading = true;
    this.submitted = true;
    let sendValues: WingModel = new WingModel();
    if (this.formCreate.valid) {
      sendValues = this.formCreate.value;
      if (!edit && this.authService.hasPermission('infrastructure:wings:create')) {
        this.wingService.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Ala registada com sucesso'
            );
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/wings/list');
          });


        } else if (edit && this.authService.hasPermission('infrastructure:wings:update')){
        sendValues.id = this.idEdit;
        this.wingService.update(this.idEdit, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Ala alterada com sucesso'
            );
            this.isEdit = false;
            this.formCreate.reset();
            this.submitted = false;
            this.router.navigateByUrl('/infrastructure/locations/wings/list');
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    }
  }


  resetForm() {
    this.submitted = false;
    this.formCreate.reset();
    this.formCreate = this.newFormCreate();
    this.isEdit = false;
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    if(id) {
      this.formCreate.get('building_id').setValue(null);
      this.loadingBuilding = true;
      this.buildingService.list(1, -1, null, null, {
        active: true,
        organic_unit_id: id,
        sort: 'name'
      }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
        this.buindings = data.data;
      })
    }
    
  }

  backList() {
    this.router.navigateByUrl('/infrastructure/locations/wings/list');
  }
}
