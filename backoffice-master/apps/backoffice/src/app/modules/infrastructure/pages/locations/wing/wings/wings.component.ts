import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { BuildingService } from "@fi-sas/backoffice/modules/infrastructure/services/building.service";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { WingsService } from "@fi-sas/backoffice/modules/infrastructure/services/wings.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-wings-infrastructure',
  templateUrl: './wings.component.html',
  styleUrls: ['./wings.component.less']
})


export class WingsInfrastructureComponent extends TableHelper implements OnInit {
  loadingOrganicUnit = true;
  organitUnits: OrganicUnitsModel[] = [];
  organicUnitSelected: number;
  buildingSelected: number;
  buindings: BuildingModel[] = [];
  loadingBuilding = false;
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private modalService: NzModalService,
    private wingService: WingsService,
    private organicService: OrganicUnitsService,
    private buildingService: BuildingService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters['withRelated'] = 'building';
  }

  ngOnInit() {
    this.getOrganicUnits();
    this.initTableData(this.wingService);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  desactive(data: WingModel) {
    if (!this.authService.hasPermission('infrastructure:wings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai desativar esta Ala. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data)
    });
  }

  desactiveDataSubmit(data: WingModel) {
    data.active = false;
    this.wingService
      .desactive(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Ala desativada com sucesso'
        );
        this.initTableData(this.wingService);
      });
  }

  active(data: WingModel) {
    if (!this.authService.hasPermission('infrastructure:wings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta Ala. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data)
    });
  }

  activeDataSubmit(data: WingModel) {
    data.active = true;
    this.wingService
      .active(data.id, data)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Ala ativada com sucesso'
        );
        this.initTableData(this.wingService);
      });
  }
  delete(data: WingModel) {
    if (!this.authService.hasPermission('infrastructure:wings:update')) {
      return;
    }
    this.modalService.confirm({
      nzTitle: 'Tem a certeza que dejesa eliminar esta ala?',
      nzOnOk: () => this.deleteDataSubmit(data)
    });
  }
  deleteDataSubmit(data: WingModel) {
    this.wingService
      .delete(data.id)
      .subscribe(result => {
        this.uiService.showMessage(
          MessageType.success,
          'Ala eliminada com sucesso'
        );
        this.initTableData(this.wingService);
      });
  }

  edit(data) {
    if (!this.authService.hasPermission('infrastructure:wings:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );

      return;
    }
    this.router.navigateByUrl('/infrastructure/locations/wings/edit/' + data.id);
  }
  listComplete() {
    this.organicUnitSelected = null;
    this.buildingSelected = null;
    this.filters.search = null;
    this.filters.organic_unit_id = null;
    this.filters.name = null;
    this.filters.code = null;
    this.filters.building_id = null;
    this.filters.active = null;
    this.searchData(true)
  }

  search() {
    this.filters.organic_unit_id = this.organicUnitSelected;
    this.filters.building_id = this.buildingSelected;
    this.searchData(true);
  }

  getOrganicUnits() {
    this.organicService.list(1, -1, null, null, {
      active: true,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingOrganicUnit = false)).subscribe((data) => {
      this.organitUnits = data.data;
    })
  }

  getBuildingByUO(id: number) {
    this.buildingSelected = null;
    this.loadingBuilding = true;
    this.buildingService.list(1, -1, null, null, {
      active: true,
      organic_unit_id: id,
      sort: 'name'
    }).pipe(first(), finalize(() => this.loadingBuilding = false)).subscribe((data) => {
      this.buindings = data.data;
    })
  }
}
