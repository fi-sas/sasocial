import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsInfrastructureComponent } from './reports.component';

const routes: Routes = [
  { path: '', redirectTo: 'listing', pathMatch: 'full' },
  {
    path: 'listing',
    component: ReportsInfrastructureComponent,
    data: { breadcrumb: 'Relatórios', title: 'Listagem de Relatórios'},
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsInfrastructureRoutingModule { }
