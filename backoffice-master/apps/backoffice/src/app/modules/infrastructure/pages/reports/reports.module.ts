import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgApexchartsModule } from "ng-apexcharts";
import { ReportsInfrastructureComponent } from './reports.component';
import { ReportsInfrastructureRoutingModule } from './reports-routing.module';

@NgModule({
  declarations: [
    ReportsInfrastructureComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxChartsModule,
    NgApexchartsModule,
    ReportsInfrastructureRoutingModule
  ],
})
export class ReportsInfrastructureModule { }
