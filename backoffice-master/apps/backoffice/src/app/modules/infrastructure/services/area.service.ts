
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AreaModel } from '../models/area.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AreaService extends Repository<AreaModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.AREAS';
    this.entity_url = 'INFRASTRUCTURE.AREAS_ID';
  }
  

  desactive(id: number, data: AreaModel): Observable<Resource<AreaModel>> {
    return this.resourceService.patch<AreaModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: AreaModel): Observable<Resource<AreaModel>> {
    return this.resourceService.patch<AreaModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}

