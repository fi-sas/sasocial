import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AssetModel } from "@fi-sas/backoffice/modules/infrastructure/models/asset.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AssetsService extends Repository<AssetModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.ASSETS';
    this.entity_url = 'INFRASTRUCTURE.ASSETS_ID';
  }

  desactive(id: number, data: AssetModel): Observable<Resource<AssetModel>> {
    return this.resourceService.patch<AssetModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: AssetModel): Observable<Resource<AssetModel>> {
    return this.resourceService.patch<AssetModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  getAssetById(id: number): Observable<Resource<AssetModel>> {
    return this.resourceService.list(this.urlService.get(this.entity_url, { id }));
  }
}
