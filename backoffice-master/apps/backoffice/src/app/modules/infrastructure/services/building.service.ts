import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { BuildingModel } from "@fi-sas/backoffice/modules/infrastructure/models/building.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuildingService extends Repository<BuildingModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.BUILDINGS';
    this.entity_url = 'INFRASTRUCTURE.BUILDINGS_ID';
  }

  desactive(id: number, data: BuildingModel): Observable<Resource<BuildingModel>> {
    return this.resourceService.patch<BuildingModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: BuildingModel): Observable<Resource<BuildingModel>> {
    return this.resourceService.patch<BuildingModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
