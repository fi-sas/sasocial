import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { CategoryModel } from '../models/category.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CategoryService extends Repository<CategoryModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.CATEGORIES';
    this.entity_url = 'INFRASTRUCTURE.CATEGORY_ID';
  }


  desactive(id: number, data: CategoryModel): Observable<Resource<CategoryModel>> {
    return this.resourceService.patch<CategoryModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: CategoryModel): Observable<Resource<CategoryModel>> {
    return this.resourceService.patch<CategoryModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
