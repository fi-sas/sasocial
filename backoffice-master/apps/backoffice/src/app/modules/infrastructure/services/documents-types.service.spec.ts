import { TestBed } from '@angular/core/testing';

import { DocumentsTypesService } from './documents-types.service';

describe('DocumentsTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentsTypesService = TestBed.get(DocumentsTypesService);
    expect(service).toBeTruthy();
  });
});
