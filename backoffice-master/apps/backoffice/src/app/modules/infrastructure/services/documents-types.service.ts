import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { DocumentTypeModel } from '../models/documents.model';

@Injectable({
  providedIn: 'root',
})
export class DocumentsTypesService extends Repository<DocumentTypeModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.DOCUMENT_TYPES';
    this.entity_url = 'INFRASTRUCTURE.DOCUMENT_TYPES_ID';
  }
}
