import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FloorModel } from "@fi-sas/backoffice/modules/infrastructure/models/floor.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FloorsService extends Repository<FloorModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.FLOORS';
    this.entity_url = 'INFRASTRUCTURE.FLOORS_ID';
  }

  desactive(id: number, data: FloorModel): Observable<Resource<FloorModel>> {
    return this.resourceService.patch<FloorModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: FloorModel): Observable<Resource<FloorModel>> {
    return this.resourceService.patch<FloorModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
