import { TestBed } from '@angular/core/testing';

import { LevelListInfraService } from './level-list-infra.service';

describe('LevelListInfraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LevelListInfraService = TestBed.get(LevelListInfraService);
    expect(service).toBeTruthy();
  });
});
