import { finalize } from 'rxjs/operators';
import { RoomModel } from '@fi-sas/backoffice/modules/infrastructure/models/room.model';
import { BehaviorSubject } from 'rxjs';
import { RoomsService } from '@fi-sas/backoffice/modules/infrastructure/services/rooms.service';
import { FloorsService } from '@fi-sas/backoffice/modules/infrastructure/services/floors.service';
import { WingsService } from '@fi-sas/backoffice/modules/infrastructure/services/wings.service';
import { FloorModel } from '@fi-sas/backoffice/modules/infrastructure/models/floor.model';
import { WingModel } from '@fi-sas/backoffice/modules/infrastructure/models/wing.model';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { OrganicUnitsService } from './organic-units.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LevelListInfraService {
  listOrganicUnitsLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  listBuildingsLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  listWingsLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  listFloorsLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  listRoomsLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  listOrganicUnits: BehaviorSubject<OrganicUnitsModel[]> = new BehaviorSubject<
    OrganicUnitsModel[]
  >([]);
  listBuildings: BehaviorSubject<BuildingModel[]> = new BehaviorSubject<
    BuildingModel[]
  >([]);
  listWings: BehaviorSubject<WingModel[]> = new BehaviorSubject<WingModel[]>(
    []
  );
  listFloors: BehaviorSubject<FloorModel[]> = new BehaviorSubject<FloorModel[]>(
    []
  );
  listRooms: BehaviorSubject<RoomModel[]> = new BehaviorSubject<RoomModel[]>(
    []
  );

  selectedOrganicUnitSubject: BehaviorSubject<
    OrganicUnitsModel
  > = new BehaviorSubject<OrganicUnitsModel>(null);
  selectedBuildingSubject: BehaviorSubject<BuildingModel> = new BehaviorSubject<
    BuildingModel
  >(null);
  selectedWingSubject: BehaviorSubject<WingModel> = new BehaviorSubject<
    WingModel
  >(null);
  selectedFloorSubject: BehaviorSubject<FloorModel> = new BehaviorSubject<
    FloorModel
  >(null);
  selectedRoomSubject: BehaviorSubject<RoomModel> = new BehaviorSubject<
    RoomModel
  >(null);

  constructor(
    private organicUnitsService: OrganicUnitsService,
    private buildingsService: BuildingService,
    private wingsService: WingsService,
    private floorsService: FloorsService,
    private roomService: RoomsService
  ) {
    this.loadOrganicUnits();
  }

  loadOrganicUnits() {
    this.listOrganicUnitsLoading.next(true);
    this.organicUnitsService
      .list(1, -1, null, null, { withRelated: false })
      .pipe(finalize(() => this.listOrganicUnitsLoading.next(false)))
      .subscribe(units => this.listOrganicUnits.next(units.data));
  }

  selectOrganicUnit(ou: OrganicUnitsModel) {
    this.selectedOrganicUnitSubject.next(ou);
    this.loadBuildings(ou ? ou.id : null);

    this.selectBuilding(null);
  }

  loadBuildings(organicUnitId: number) {
    if (organicUnitId) {
      this.listBuildingsLoading.next(true);
      this.buildingsService
        .list(1, -1, null, null, { organic_unit_id: organicUnitId, withRelated: false })
        .pipe(finalize(() => this.listBuildingsLoading.next(false)))
        .subscribe(buildings => this.listBuildings.next(buildings.data));
    } else {
      this.listBuildings.next([]);
    }
  }

  selectBuilding(b: BuildingModel) {
    this.selectedBuildingSubject.next(b);
    this.loadWings(b ? b.id: null);

    this.selectWing(null);
  }

  loadWings(buildingId: number) {
    if (buildingId) {
      this.listWingsLoading.next(true);
      this.wingsService
        .list(1, -1, null, null, { building_id: buildingId, withRelated: false })
        .pipe(finalize(() => this.listWingsLoading.next(false)))
        .subscribe(wings => this.listWings.next(wings.data));
    } else {
      this.listWings.next([]);
    }
  }

  selectWing(wing: WingModel) {
    this.selectedWingSubject.next(wing);
    this.loadFloors(wing ? wing.id : null);

    this.selectFloor(null);
  }

  loadFloors(wingId: number) {
    if (wingId) {
      this.listFloorsLoading.next(true);
      this.floorsService
        .list(1, -1, null, null, { wing_id: wingId, withRelated: false })
        .pipe(finalize(() => this.listFloorsLoading.next(false)))
        .subscribe(floors => this.listFloors.next(floors.data));
    } else {
      this.listFloors.next([]);
    }
  }

  selectFloor(floor: FloorModel) {
    this.selectedFloorSubject.next(floor);
    this.loadFloors(floor ? floor.id : null);

    this.selectRoom(null);
  }

  loadRooms(floorId: number) {
    if (floorId) {
      this.listRoomsLoading.next(true);
      this.roomService
        .list(1, -1, null, null, { floor_id: floorId, withRelated: false })
        .pipe(finalize(() => this.listRoomsLoading.next(false)))
        .subscribe(rooms => this.listRooms.next(rooms.data));
    } else {
      this.listRooms.next([]);
    }
  }

  selectRoom(room: RoomModel) {
    this.selectedRoomSubject.next(room);
  }
}
