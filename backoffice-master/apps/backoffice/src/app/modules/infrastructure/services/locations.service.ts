import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { LocationModel } from "@fi-sas/backoffice/modules/infrastructure/models/location.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationsService extends Repository<LocationModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.LOCATIONS';
    this.entity_url = 'INFRASTRUCTURE.LOCATIONS_ID';
  }

}
