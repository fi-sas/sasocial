import { TestBed, inject } from '@angular/core/testing';

import { OrganicUnitsService } from './organic-units.service';

describe('OrganicUnitsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganicUnitsService]
    });
  });

  it('should be created', inject([OrganicUnitsService], (service: OrganicUnitsService) => {
    expect(service).toBeTruthy();
  }));
});
