import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrganicUnitsService extends Repository<OrganicUnitsModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.ORGANIC_UNITS';
    this.entity_url = 'INFRASTRUCTURE.ORGANIC_UNITS_ID';
  }

  desactive(id: number, data: OrganicUnitsModel): Observable<Resource<OrganicUnitsModel>> {
    return this.resourceService.patch<OrganicUnitsModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: OrganicUnitsModel): Observable<Resource<OrganicUnitsModel>> {
    return this.resourceService.patch<OrganicUnitsModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
