import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { RoomModel } from "@fi-sas/backoffice/modules/infrastructure/models/room.model";
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomsService extends Repository<RoomModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.ROOMS';
    this.entity_url = 'INFRASTRUCTURE.ROOMS_ID';
  }
  desactive(id: number, data: RoomModel): Observable<Resource<RoomModel>> {
    return this.resourceService.patch<RoomModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: RoomModel): Observable<Resource<RoomModel>> {
    return this.resourceService.patch<RoomModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  listRoomsBuild(building_id) {
    let params = new HttpParams();
    params = params.set('query[building_id]', building_id + '');
    params = params.set('query[active]', 'true');
    params = params.set('sort', 'name');
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    return this.resourceService.list<RoomModel>(this.urlService.get(this.entities_url),
    {
      params
    });
  }

}
