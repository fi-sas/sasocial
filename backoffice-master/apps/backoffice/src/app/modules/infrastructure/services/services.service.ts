import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ServiceModel } from "@fi-sas/backoffice/modules/infrastructure/models/service.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService extends Repository<ServiceModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.SERVICES_INFRA';
    this.entity_url = 'INFRASTRUCTURE.SERVICES_INFRA_ID';
  }

  desactive(id: number, data: ServiceModel): Observable<Resource<ServiceModel>> {
    return this.resourceService.patch<ServiceModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: ServiceModel): Observable<Resource<ServiceModel>> {
    return this.resourceService.patch<ServiceModel>(this.urlService.get(this.entity_url, { id }), data);
  }

}
