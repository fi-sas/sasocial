import { TestBed, inject } from '@angular/core/testing';

import { WingsService } from './wings.service';

describe('WingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WingsService]
    });
  });

  it('should be created', inject([WingsService], (service: WingsService) => {
    expect(service).toBeTruthy();
  }));
});
