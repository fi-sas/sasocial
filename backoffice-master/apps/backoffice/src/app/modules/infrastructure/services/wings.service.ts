import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { WingModel } from "@fi-sas/backoffice/modules/infrastructure/models/wing.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WingsService extends Repository<WingModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'INFRASTRUCTURE.WINGS';
    this.entity_url = 'INFRASTRUCTURE.WINGS_ID';
  }

  desactive(id: number, data: WingModel): Observable<Resource<WingModel>> {
    return this.resourceService.patch<WingModel>(this.urlService.get(this.entity_url, { id }), data);
  }

  active(id: number, data: WingModel): Observable<Resource<WingModel>> {
    return this.resourceService.patch<WingModel>(this.urlService.get(this.entity_url, { id }), data);
  }
}
