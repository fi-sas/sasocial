
export enum FileType {
  IMAGE = 'IMAGE',
  VIDEO = 'VIDEO',
  DOCUMENT = 'DOCUMENT'
}

export class FileModel {
  id: number;
  type: FileType;
  weight: number;
  mime_type: string;
  path: string;
  filename: string;
  url: string;
  width: number;
  height: number;
}
