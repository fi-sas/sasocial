import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { first } from 'rxjs/operators';
import { HttpParams, HttpRequest, HttpEvent } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FilesService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  list(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    isPublic?: number
  ): Observable<Resource<FileModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('withRelated', 'category,translations');

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }

    if (isPublic) {
      params = params.set('public', isPublic.toString());
    }

    params = params.set('withRelated', 'category,translations');

    return this.resourceService
      .list<FileModel>(this.urlService.get('FILES'), { params })
      .pipe(first());
  }

  listFilesByIDS(ids: number[]): Observable<Resource<FileModel>> {
    let httpParams = new HttpParams();
    ids.forEach((id) => {
      httpParams = httpParams.append('query[id]', id.toString());
    });
    return this.resourceService.list<FileModel>(
      this.urlService.get('FILES', {}),
      { params: httpParams }
    );
  }

   createFile(file: FormData): Observable<HttpEvent<FileModel>> {
    return this.resourceService.http.request(new HttpRequest<FormData>('POST', this.urlService.get('FILES', {}), file, {
      reportProgress: true
    }));
  }

  createFile2(file: FormData): Observable<Resource<FileModel>> {
    return this.resourceService
      .create<FileModel>(this.urlService.get('FILES'), file)
      .pipe(first());
  }

  get(id: number): Observable<Resource<FileModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'category,translations');
    return this.resourceService
      .read<FileModel>(this.urlService.get('FILES_ID', { id }), { params })
      .pipe(first());
  }

  delete(id: number): Observable<any> {
    return this.resourceService
      .delete(this.urlService.get('FILES_ID', { id }))
      .pipe(first());
  }
}
