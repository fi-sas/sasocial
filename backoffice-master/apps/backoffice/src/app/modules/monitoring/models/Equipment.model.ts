export class EquipmentModel {
    id: number;
    measurement_source_id: number;
    name: string;
    mark: string;
    model: string;
    serial_number: string;
    key: string;
    location_id: number;
    type: string;
    min_value: number;
    max_value: number;
    unit: string;
    relevance: string;
    modbus_data : modBus;
    users: User_Responsable[];
    minutes_notifications: number;
    measurement_group_id: number;
    scouting_certificate_file_id: number;
    validity: Date;
    active: boolean;
  }
  class User_Responsable {
    user_id: number;
    measurement_configuration_id: number;
  }

class modBus {
    registerNumber: number;
    format: string;
}