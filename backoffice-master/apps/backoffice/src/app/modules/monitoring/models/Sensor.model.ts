export class SensorModel {
    id: number;
    measurement_source_id: number;
    name: string;
    location_id: number;
    type: string;
    min_value: number;
    max_value: number;
    unit_id: number;
    relevance: string;
    modbus_data : modBus;
    minutes_notifications: number;
    measurement_equipment_id: number;
    active: boolean;
  }

class modBus {
    registerNumber: number;
    format: string;
}