export class SourceConnectionModel {
    id: number;
    name: string;
    type: string;
    connection_data: Connection_Data;
  }


class Connection_Data{
    host: string;
    port: number;
    default_timeout: number;
}