export class UnitsModel {
    id: number;
    name: string;
    acronym: string;
    active: boolean;
  }