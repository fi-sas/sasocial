import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { MonitoringComponent } from './monitoring.component';
import { MonitoringDashboardComponent } from './pages/monitoring-dashboard/monitoring-dashboard.component';







const routes: Routes = [
  {
    path: '',
    component: MonitoringComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'equipments',
        loadChildren: './pages/equipments/equipments.module#EquipmentsModule',
        data: { breadcrumb: 'Equipamentos', title: 'Equipamentos' },
      }, 
      
      {
        path: 'group-equipments',
        loadChildren: './pages/group-equipments/group-equipments.module#GroupEquipmentsModule',
        data: { breadcrumb: 'Grupo Equipamentos', title: 'Grupo Equipamentos' },
      },
      {
        path: 'general-config',
        loadChildren: './pages/general-config-modbus/general-config.module#GeneralConfigModule',
        data: { breadcrumb: 'Configuração geral ModBus', title: 'Configuração geral ModBus' },
      },
      {
        path: 'units',
        loadChildren: './pages/measurement-units/units.module#UnitsModule',
        data: { breadcrumb: 'Unidades de Medição', title: 'Unidades de Medição' },
      },
      {
        path: 'sensor',
        loadChildren: './pages/sensors/sensors.module#SensorModule',
        data: { breadcrumb: 'Sensor', title: 'Sensor' },
      },
      {
        path: 'dashboard',
        component: MonitoringDashboardComponent,
        data: { breadcrumb: 'Dashboard', title: 'DashBoard' },
      },

      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringRoutingModule {
}
