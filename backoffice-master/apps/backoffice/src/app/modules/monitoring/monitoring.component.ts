import { Component, OnInit } from '@angular/core';
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { SiderItem } from "@fi-sas/backoffice/core/models/sider-item";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-private-accommodation',
  template: '<router-outlet></router-outlet>'
})
export class MonitoringComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private authService: AuthService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(26), 'dashboard');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();
    const dashboard = new SiderItem('Dashboard');
    const dash = dashboard.addChild(new SiderItem('Dashboard', '', '/consumption/dashboard'));
    this.uiService.addSiderItem(dash);
    const configurations_modubus = new SiderItem('Configurações');
    const equipments = configurations_modubus.addChild(new SiderItem('Equipamentos', '', '/consumption/configurations'));
    equipments.addChild(new SiderItem('Criar', '', '/consumption/equipments/create' ));
    equipments.addChild(new SiderItem('Listar', '', '/consumption/equipments/list'));
    const group_equipments =  configurations_modubus.addChild(new SiderItem('Grupo de Equipamentos', '', ''));
    group_equipments.addChild(new SiderItem('Criar', '', '/consumption/group-equipments/create' ));
    group_equipments.addChild(new SiderItem('Listar', '', '/consumption/group-equipments/list'));
    const sensor =  configurations_modubus.addChild(new SiderItem('Sensor de Medição', '', ''));
    sensor.addChild(new SiderItem('Criar', '', '/consumption/sensor/create' ));
    sensor.addChild(new SiderItem('Listar', '', '/consumption/sensor/list'));
    const units =  configurations_modubus.addChild(new SiderItem('Unidades de Medição', '', ''));
    units.addChild(new SiderItem('Criar', '', '/consumption/units/create' ));
    units.addChild(new SiderItem('Listar', '', '/consumption/units/list'));
    this.uiService.addSiderItem(configurations_modubus);
    const configurations = new SiderItem('Configuração geral modbus');
    const modbus = configurations.addChild(new SiderItem('Configuração geral ModBus', '', '/consumption/general-config/modbus'))
    this.uiService.addSiderItem(modbus);
    this.uiService.setSiderActive(true);
  }
  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}

