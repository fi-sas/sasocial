import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonitoringComponent } from './monitoring.component';
import { MonitoringRoutingModule } from './monitoring-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { MonitoringDashboardComponent } from './pages/monitoring-dashboard/monitoring-dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgApexchartsModule } from 'ng-apexcharts';


@NgModule({
  declarations: [MonitoringComponent, MonitoringDashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    MonitoringRoutingModule,
    NgxChartsModule,
    NgApexchartsModule,
  ]
})
export class MonitoringModule { }
