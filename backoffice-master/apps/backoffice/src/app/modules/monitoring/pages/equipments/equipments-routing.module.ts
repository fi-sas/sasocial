import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEquipmentsComponent } from './form-equipments/form-equipments.component';
import { EquipementsListComponent } from './list-equipaments/list-equipments.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: EquipementsListComponent,
    data: { breadcrumb: 'Listar', title: 'Listar'},

  },
  {
    path: 'create',
    component: FormEquipmentsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar'},
  },
  {
    path: 'edit/:id',
    component: FormEquipmentsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar',},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipmentsRoutingModule { }
