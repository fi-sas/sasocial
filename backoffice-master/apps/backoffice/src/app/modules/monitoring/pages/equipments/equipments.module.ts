import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { EquipmentsRoutingModule } from './equipments-routing.module';
import { FormEquipmentsComponent } from './form-equipments/form-equipments.component';
import { EquipementsListComponent } from './list-equipaments/list-equipments.component';
import { ViewEquipmentsComponent } from './list-equipaments/view-equipments/view-equipments.component';

@NgModule({
  declarations: [
    FormEquipmentsComponent,
    EquipementsListComponent,
    ViewEquipmentsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EquipmentsRoutingModule
  ],

})
export class EquipmentsModule { }
