import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEquipmentsComponent } from './form-equipments.component';

describe('FormEquipmentsComponent', () => {
  let component: FormEquipmentsComponent;
  let fixture: ComponentFixture<FormEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
