import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { EquipmentModel } from '../../../models/Equipment.model';
import { EquipementsService } from '../../../services/equipements.service';
import { first, finalize, debounceTime, switchMap, map } from 'rxjs/operators';
import { EquipmentGroup } from '../../../models/Equipment_Group.model';
import { EquipmentGroupService } from '../../../services/equipment-group.service';
import { SourceConnectionModel } from '../../../models/Source_Connection.model';
import { SourceConnectionService } from '../../../services/source-connection.service';
import { LocationModel } from '@fi-sas/backoffice/modules/infrastructure/models/location.model';
import { LocationsService } from '@fi-sas/backoffice/modules/infrastructure/services/locations.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { BuildingModel } from '@fi-sas/backoffice/modules/infrastructure/models/building.model';
import { WingModel } from '@fi-sas/backoffice/modules/infrastructure/models/wing.model';
import { FloorModel } from '@fi-sas/backoffice/modules/infrastructure/models/floor.model';
import { RoomModel } from '@fi-sas/backoffice/modules/infrastructure/models/room.model';
import { ServiceModel } from '@fi-sas/backoffice/modules/infrastructure/models/service.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { BuildingService } from '@fi-sas/backoffice/modules/infrastructure/services/building.service';
import { WingsService } from '@fi-sas/backoffice/modules/infrastructure/services/wings.service';
import { RoomsService } from '@fi-sas/backoffice/modules/infrastructure/services/rooms.service';
import { FloorsService } from '@fi-sas/backoffice/modules/infrastructure/services/floors.service';
import { ServicesService } from '@fi-sas/backoffice/modules/infrastructure/services/services.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-equipments',
  templateUrl: './form-equipments.component.html',
  styleUrls: ['./form-equipments.component.less']
})
export class FormEquipmentsComponent implements OnInit {


  loadingEdit = false;
  submitted = false;
  id;

  equipments_group: EquipmentGroup[];
  connections_source: SourceConnectionModel[];
  locations: LocationModel[];
  modalVisible: boolean = false;

  users: UserModel[] = [];
  isLoadingUsers = true;
  usersSearchChange$ = new BehaviorSubject('');


  //LOCATION
  formLocation: FormGroup;
  organics: OrganicUnitsModel[];
  buildings: BuildingModel[];
  wings: WingModel[];
  floors: FloorModel[];
  divisions: RoomModel[];
  services: ServiceModel[];
  nameUA: string = '';
  nameBuildings: string = '';
  nameWings: string = '';
  nameFloors: string = '';
  nameDivisions: string = '';
  nameServices: string = '';
  submittedLocation: boolean = false;


  formEquipment = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.maxLength(255), trimValidation]),
    mark: new FormControl(null, [Validators.maxLength(255), Validators.required, trimValidation]),
    model: new FormControl(null, [Validators.maxLength(255), Validators.required, trimValidation]),
    serial_number: new FormControl(null, [Validators.maxLength(255), Validators.required, trimValidation]),
    users: new FormControl(null),
    measurement_group_id: new FormControl(null, [Validators.required]),
    measurement_source_id: new FormControl(null, [Validators.required]),
    location_id: new FormControl(null, [Validators.required]),
    scouting_certificate_file_id: new FormControl(null),
    validity: new FormControl(null),
    active: new FormControl(true, [Validators.required])
  });

  constructor(private uiService: UiService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private equipmentsService: EquipementsService,
    public equipmentGroupService: EquipmentGroupService,
    private locationsService: LocationsService,
    private organicUnitsService: OrganicUnitsService,
    private buildingService: BuildingService,
    private wingsService: WingsService,
    private roomsService: RoomsService,
    private floorsService: FloorsService,
    private servicesService: ServicesService,
    private usersService: UsersService,
    public sourceConnection: SourceConnectionService) { }

  ngOnInit() {
    this.formLocation = this.newFormLocation();
    this.loadGroups();
    this.loadSourceConnection();
    this.loadLocations();
    this.getOrganicUnits();
    const loadUsers = (search: string) =>
      this.usersService.list(1, 20, null, null, {
        withRelated: false,
        search,
        searchFields: 'name,user_name,student_number,email,identification'
      }).pipe(first(), map((results) => results.data));

    const optionUsers$: Observable<UserModel[]> = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe(users => {
      this.users = users;
      this.isLoadingUsers = false;
    });

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.loadingEdit = true;
      this.getDataEquipmentById(this.id);
    }

  }

  onSearchUsers(event) {
    this.isLoadingUsers = true;
    this.usersSearchChange$.next(event);
  }

  backList() {
    this.router.navigateByUrl('/consumption/equipments/list');
  }


  loadGroups() {
    this.equipmentGroupService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.equipments_group = results.data;
    });
  }

  loadSourceConnection() {
    this.sourceConnection.list(1, -1, null, null).pipe(
      first()
    ).subscribe(results => {
      this.connections_source = results.data;
    });
  }

  loadLocations() {
    this.locationsService.list(1, -1).pipe(
      first()
    ).subscribe(results => {
      this.locations = results.data;
      if (this.locations) {
        if (this.locations.length >= 1) {
          this.locations = this.locations.filter(
            (thing, i, arr) => arr.findIndex(t => t.description === thing.description) === i
          );
        }
      }
    });
  }

  getDataEquipmentById(id: number) {
    let equipment: EquipmentModel = new EquipmentModel();
    this.equipmentsService.read(this.id)
      .pipe(
        first(),
        finalize(() => this.loadingEdit = false)
      )
      .subscribe((results) => {
        equipment = results.data[0];
        this.formEquipment.patchValue({
          name: equipment.name ? equipment.name : null,
          mark: equipment.mark ? equipment.mark : null,
          model: equipment.name ? equipment.name : null,
          serial_number: equipment.name ? equipment.name : null,
          users: equipment.users ? equipment.users.map((user) => user.user_id) : null,
          measurement_group_id: equipment.measurement_group_id ? equipment.measurement_group_id : null,
          measurement_source_id: equipment.measurement_source_id ? equipment.measurement_source_id : null,
          scouting_certificate_file_id: equipment.scouting_certificate_file_id ? equipment.scouting_certificate_file_id : null,
          validity: equipment.validity ? equipment.validity : null,
          location_id: equipment.location_id ? equipment.location_id : null,
          active: equipment.active ? equipment.active : null,
        });
        this.loadingEdit = false;
      });
  }

  get f() { return this.formEquipment.controls; }

  

  submitEquipment(edit) {
    this.submitted = true;
    let sendValues: EquipmentModel = new EquipmentModel();
    sendValues = this.formEquipment.value;
    if (this.formEquipment.valid) {
      if (edit) {
        sendValues.id = this.id;
        this.equipmentsService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Equipamento alterado com sucesso'
            );
            this.backList();
          });
      } else {
        this.equipmentsService.create(sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Equipamento registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  openModal() {
    this.modalVisible = true;
    this.formLocation.controls['organic_code'].disable();
    this.formLocation.controls['building'].disable();
    this.formLocation.controls['building_code'].disable();
    this.formLocation.controls['wing'].disable();
    this.formLocation.controls['wing_code'].disable();
    this.formLocation.controls['floor_code'].disable();
    this.formLocation.controls['floor'].disable();
    this.formLocation.controls['division_code'].disable();
    this.formLocation.controls['division'].disable();
    this.formLocation.controls['service_code'].disable();
    this.formLocation.controls['service'].disable();
    this.formLocation.controls['description'].disable();
  }

  newFormLocation() {
    return this.formBuilder.group({
      organic: ['', [Validators.required]],
      organic_code: [''],
      building: [''],
      building_code: [''],
      wing: [''],
      wing_code: [''],
      floor: [''],
      floor_code: [''],
      division: [''],
      division_code: [''],
      service: [''],
      service_code: [''],
      code: [''],
      description: ['', [Validators.required]]
    });
  }


  cancelModal() {
    this.modalVisible = false;
    this.resetFormLocation();
  }

  resetFormLocation() {
    this.submittedLocation = false;
    this.formLocation.reset();
    this.formLocation = this.newFormLocation();
    this.nameUA = '';
    this.nameBuildings = '';
    this.nameWings = '';
    this.nameFloors = '';
    this.nameDivisions = '';
    this.nameServices = '';
  }

  getBuildings(id) {
    this.buildingService.list(1, -1, null, null, { active: true, organic_unit_id: id }).pipe(
      first()
    ).subscribe(results => {
      this.buildings = results.data;
    });
  }

  getOrganicUnits() {
    this.organicUnitsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.organics = results.data;
    });
  }

  getWings(id) {
    this.wingsService.list(1, -1, null, null, { active: true, building_id: id }).pipe(
      first()
    ).subscribe(results => {
      this.wings = results.data;
    });
  }

  getFloors(id) {
    this.floorsService.list(1, -1, null, null, { active: true, wing_id: id }).pipe(
      first()
    ).subscribe(results => {
      this.floors = results.data;
    });
  }

  getDivisions(id) {
    this.roomsService.list(1, -1, null, null, { active: true, floor_id: id }).pipe(
      first()
    ).subscribe(results => {
      this.divisions = results.data;
    });
  }

  getServices(id) {
    this.servicesService.list(1, -1, null, null, { active: true, room_id: id }).pipe(
      first()
    ).subscribe(results => {
      this.services = results.data;
    });
  }

  dataOrganicFilter(event) {
    if (event) {
      let organicFilter = this.organics.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('organic_code').setValue(organicFilter[0].code);
      this.nameUA = organicFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.getBuildings(event);
      this.formLocation.get('building').setValue(null);
      this.formLocation.controls['building'].enable();
    }

  }

  dataBuildingFilter(event) {
    if (event) {
      let buildingFilter = this.buildings.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('building_code').setValue(buildingFilter[0].code);
      this.nameBuildings = buildingFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.getWings(event);
      this.formLocation.get('wing').setValue(null);
      this.formLocation.controls['wing'].enable();

    }
  }

  dataWingFilter(event) {
    if (event) {
      let wingFilter = this.wings.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('wing_code').setValue(wingFilter[0].code);
      this.nameWings = wingFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.formLocation.get('building').setValidators(Validators.required);
      this.formLocation.get('building').updateValueAndValidity();
      this.getFloors(event);
      this.formLocation.get('floor').setValue(null);
      this.formLocation.controls['floor'].enable();
    }
  }

  dataFloorFilter(event) {
    if (event) {
      let floorFilter = this.floors.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('floor_code').setValue(floorFilter[0].code);
      this.nameFloors = floorFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.formLocation.get('building').setValidators(Validators.required);
      this.formLocation.get('building').updateValueAndValidity();
      this.formLocation.get('wing').setValidators(Validators.required);
      this.formLocation.get('wing').updateValueAndValidity();
      this.getDivisions(event);
      this.formLocation.get('division').setValue(null);
      this.formLocation.controls['division'].enable();
    }
  }

  dataDivisionFilter(event) {
    if (event) {
      let divisionFilter = this.divisions.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('division_code').setValue(divisionFilter[0].code);
      this.nameDivisions = divisionFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.formLocation.get('building').setValidators(Validators.required);
      this.formLocation.get('building').updateValueAndValidity();
      this.formLocation.get('wing').setValidators(Validators.required);
      this.formLocation.get('wing').updateValueAndValidity();
      this.formLocation.get('floor').setValidators(Validators.required);
      this.formLocation.get('floor').updateValueAndValidity();
      this.getServices(event);
      this.formLocation.get('service').setValue(null);
      this.formLocation.controls['service'].enable();
    }
  }

  dataServiceFilter(event) {
    if (event) {
      let serviceFilter = this.services.filter((fil) => {
        return fil.id == event;
      })
      this.formLocation.get('service_code').setValue(serviceFilter[0].code);
      this.nameServices = serviceFilter[0].name;
      this.formLocation.get('description').setValue('');
      this.formLocation.get('description').setValue((this.nameUA ? this.nameUA : '') +
        (this.nameBuildings ? ('|' + this.nameBuildings) : '') +
        (this.nameWings ? ('|' + this.nameWings) : '') +
        (this.nameFloors ? ('|' + this.nameFloors) : '') +
        (this.nameDivisions ? ('|' + this.nameDivisions) : '') +
        (this.nameServices ? ('|' + this.nameServices) : ''));
      this.formLocation.get('building').setValidators(Validators.required);
      this.formLocation.get('building').updateValueAndValidity();
      this.formLocation.get('wing').setValidators(Validators.required);
      this.formLocation.get('wing').updateValueAndValidity();
      this.formLocation.get('floor').setValidators(Validators.required);
      this.formLocation.get('floor').updateValueAndValidity();
      this.formLocation.get('division').setValidators(Validators.required);
      this.formLocation.get('division').updateValueAndValidity();

    }
  }

  get ff() { return this.formLocation.controls; }

  saveLocation() {
    this.submittedLocation = true;
    let sendValues: LocationModel = new LocationModel();
    if (this.formLocation.valid) {
      sendValues.organic_unit_id = this.ff.organic.value;
      if (this.ff.building.value) {
        sendValues.building_id = this.ff.building.value;
      }
      if (this.ff.wing.value) {
        sendValues.wing_id = this.ff.wing.value;
      }
      if (this.ff.floor.value) {
        sendValues.floor_id = this.ff.floor.value;
      }
      if (this.ff.service.value) {
        sendValues.service_id = this.ff.service.value;
      }
      if (this.ff.division.value) {
        sendValues.room_id = this.ff.division.value;
      }
      if (this.ff.code.value) {
        sendValues.code = this.ff.code.value;
      }
      sendValues.description = this.ff.description.value;
      sendValues.active = true;
      this.locationsService.create(sendValues).pipe(
        first()).subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Localização registada com sucesso'
          );
          this.loadLocations();
          this.modalVisible = false;
          this.resetFormLocation();
        });
    }
  }
}

function asObservable() {
  throw new Error('Function not implemented.');
}

