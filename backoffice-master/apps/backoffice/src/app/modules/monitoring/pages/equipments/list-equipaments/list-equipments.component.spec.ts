import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipementsListComponent } from './list-equipments.component';

describe('EquipementsListComponent', () => {
  let component: EquipementsListComponent;
  let fixture: ComponentFixture<EquipementsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipementsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipementsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
