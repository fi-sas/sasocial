import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from 'ng-zorro-antd';
import { EquipmentModel } from '../../../models/Equipment.model';
import { EquipementsService } from '../../../services/equipements.service';

@Component({
  selector: 'fi-sas-equipments-list',
  templateUrl: './list-equipments.component.html',
  styleUrls: ['./list-equipments.component.less']
})

export class EquipementsListComponent extends TableHelper implements OnInit  {
  
  public YesNoTag = TagComponent.YesNoTag;

  status = [];
  constructor(uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public equipmentService: EquipementsService,
    private modalService: NzModalService,) { 
      super(uiService, router, activatedRoute);
      this.persistentFilters = {
        withRelated: 'users,location,measurement_group,measurement_config'
      }
    }

  ngOnInit() {
    this.status = [
      {
        description: 'Ativo',
        value: true,
      },
      {
        description: 'Desativo',
        value: false,
      },
    ];

    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'mark',
        label: 'Marca',
        sortable: true,
      },
      {
        key: 'serial_number',
        label: 'Número de serie',
        sortable: true,
      },
      {
        key: 'group',
        label: 'Grupo de Equipamentos',
        sortable: false,
      },
      {
        key: 'location',
        label: 'Localização',
        sortable: false,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      }
    );



    this.initTableData(this.equipmentService);
    this.persistentFilters = {
      searchFields: 'name,mark,model,serial_number',
    };
  }


  EditEquipment(id: number) {
    this.router.navigateByUrl('/consumption/equipments/edit/' + id);
  }


  desactive(data: EquipmentModel) {
    this.modalService.confirm({
      nzTitle:
        'Vai desativar este equipamentos. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data),
    });
  }

  desactiveDataSubmit(data: EquipmentModel) {
    data.active = false;
    this.equipmentService.desactive(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Equipamento desativado com sucesso'
      );
      this.initTableData(this.equipmentService);
    });
  }


  active(data: EquipmentModel) {
    this.modalService.confirm({
      nzTitle: 'Vai ativar este equipamento. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data),
    });
  }

  activeDataSubmit(data: EquipmentModel) {
    data.active = true;
    this.equipmentService.active(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Equipamento ativado com sucesso'
      );
      this.initTableData(this.equipmentService);
    });
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }




}
