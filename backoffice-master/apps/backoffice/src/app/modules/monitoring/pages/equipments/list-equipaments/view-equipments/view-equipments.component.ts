import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fi-sas-view-equipments',
  templateUrl: './view-equipments.component.html',
  styleUrls: ['./view-equipments.component.less']
})
export class ViewEquipmentsComponent implements OnInit {

  @Input() data: any = null;

  constructor() { }

  ngOnInit() {
  }

}
