import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralConfigModbusComponent } from './general-config-modbus.component';

describe('GeneralConfigModbusComponent', () => {
  let component: GeneralConfigModbusComponent;
  let fixture: ComponentFixture<GeneralConfigModbusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralConfigModbusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralConfigModbusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
