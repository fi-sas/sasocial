import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SourceConnectionService } from '../../services/source-connection.service';
import { first, finalize } from 'rxjs/operators';
import { SourceConnectionModel } from '../../models/Source_Connection.model';
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-general-config-modbus',
  templateUrl: './general-config-modbus.component.html',
  styleUrls: ['./general-config-modbus.component.less']
})
export class GeneralConfigModbusComponent implements OnInit {

  
  sourceConnectionForm = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    port: new FormControl('', [Validators.required]),
    host: new FormControl('', [Validators.required, trimValidation]),
    default_timeout: new FormControl(''),
  });
  submitted = false;

  sourceConnectionModel: SourceConnectionModel = new SourceConnectionModel();

  loading = false;
  id;
  
  constructor( private uiService: UiService,
    public sourceConnection: SourceConnectionService ) { }
    

  ngOnInit() {  
    this.get_source_connection();
  }

  get f() { return this.sourceConnectionForm.controls; }


  get_source_connection() {
    this.sourceConnection.list(1, -1, null, null, { withRelated: false })
    .pipe(first(),
      finalize(() => this.loading = false))
    .subscribe(results => {
      if(results.data.length > 0 ){
        this.sourceConnectionModel = results.data[0];
        this.id = this.sourceConnectionModel.id;
        this.sourceConnectionForm.patchValue({
          name: this.sourceConnectionModel.name ? this.sourceConnectionModel.name : null,
          port: this.sourceConnectionModel.connection_data.port ? this.sourceConnectionModel.connection_data.port  : null,
          host: this.sourceConnectionModel.connection_data.host ? this.sourceConnectionModel.connection_data.host  : null,
          default_timeout: this.sourceConnectionModel.connection_data.default_timeout ? this.sourceConnectionModel.connection_data.default_timeout  : null,
        });
      }
    });
  }

  submitConfig(edit: boolean){
    this.submitted = true;
    let sendValues: SourceConnectionModel = new SourceConnectionModel();
    if (this.sourceConnectionForm.valid) {
      const connection_data = {
        host: this.sourceConnectionForm.get('host').value,
        port: this.sourceConnectionForm.get('port').value,
        default_timeout: this.sourceConnectionForm.get('default_timeout').value,
      }
      sendValues = this.sourceConnectionForm.value;
      sendValues.connection_data = connection_data;
      sendValues.type = "modbus/tcp";
      if (edit) {
        sendValues.id = this.sourceConnectionModel.id;
        this.sourceConnection.update(sendValues.id, sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Configuração geral alterada com sucesso'
            );
          });
      }
      else {
        this.sourceConnection.create(sendValues).pipe(
          first(), finalize(() => this.loading = false)).subscribe(() => {
            this.loading = false;
            this.uiService.showMessage(
              MessageType.success,
              'Configuração geral criada com sucesso'
            );
          });
      }
    }
  }
}
