import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralConfigModbusComponent } from './general-config-modbus.component';

const routes: Routes = [
  { path: '', redirectTo: 'modbus', pathMatch: 'full' },
  {
    path: 'modbus',
    component: GeneralConfigModbusComponent,
    data: { breadcrumb: 'ModBus', title: 'ModBus'},
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneralConfigRoutingModule { }