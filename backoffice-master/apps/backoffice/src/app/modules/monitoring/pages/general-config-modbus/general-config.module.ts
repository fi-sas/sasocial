import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { GeneralConfigModbusComponent } from './general-config-modbus.component';
import { GeneralConfigRoutingModule } from './general-config-routing.module';

@NgModule({
  declarations: [
    GeneralConfigModbusComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GeneralConfigRoutingModule
  ],

})
export class GeneralConfigModule { }
