import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGroupEquipmentsComponent } from './form-group-equipments.component';

describe('FormGroupEquipmentsComponent', () => {
  let component: FormGroupEquipmentsComponent;
  let fixture: ComponentFixture<FormGroupEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormGroupEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGroupEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
