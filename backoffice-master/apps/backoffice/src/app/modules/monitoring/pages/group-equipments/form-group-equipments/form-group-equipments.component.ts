import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { EquipmentGroup } from '../../../models/Equipment_Group.model';
import { EquipmentGroupService } from '../../../services/equipment-group.service';
import { finalize, first } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-group-equipments',
  templateUrl: './form-group-equipments.component.html',
  styleUrls: ['./form-group-equipments.component.less']
})
export class FormGroupEquipmentsComponent implements OnInit {

  loadingEdit = false;
  submitted = false;
  id;

  formGroupEquipments = new FormGroup({
    description: new FormControl(null, [Validators.required, Validators.maxLength(255), trimValidation]),
    active: new FormControl(true, [Validators.required])
  });

  constructor(private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    public equipmentGroupService: EquipmentGroupService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.loadingEdit = true;
      this.getDataGroupById(this.id);
    }
  }

  backList() {
    this.router.navigateByUrl('/consumption/group-equipments/list');
  }

  submitGroup(edit: boolean) {
    this.submitted = true;
    let sendValues: EquipmentGroup = new EquipmentGroup();
    if (this.formGroupEquipments.valid ) {
      sendValues = this.formGroupEquipments.value;
      if (edit) {
        sendValues.id = this.id;
        this.equipmentGroupService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Grupo de equipamentos alterado com sucesso'
            );
            this.backList();
          });
      }
      else {
        this.equipmentGroupService.create(sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Grupo de equipamentos registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  getDataGroupById(id: number) {
    let equipment_group: EquipmentGroup = new EquipmentGroup();
    this.equipmentGroupService.read(this.id)
    .pipe(
      first(),
      finalize(() => this.loadingEdit = false)
    )
    .subscribe((results) => {
      equipment_group = results.data[0];
      this.formGroupEquipments.patchValue({
        description: equipment_group.description ? equipment_group.description : null,
        active: equipment_group.active ? equipment_group.active : null, 
      });
      this.loadingEdit = false;
    });
  }

  get f() { return this.formGroupEquipments.controls; }

  
  getDescription() {
    return this.f.description.errors.required
      ? 'Campo Obrigatório'
      : this.f.description.errors.maxlength
        ? 'Tamanho máximo de 255'
        : '';
  }

}
