import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormGroupEquipmentsComponent } from './form-group-equipments/form-group-equipments.component';
import { ListGroupEquipmentsComponent } from './list-group-equipments/list-group-equipments.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListGroupEquipmentsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar'},

  },
  {
    path: 'create',
    component: FormGroupEquipmentsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar'},
  },
  {
    path: 'edit/:id',
    component: FormGroupEquipmentsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar',},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupEquipmentsRoutingModule { }
