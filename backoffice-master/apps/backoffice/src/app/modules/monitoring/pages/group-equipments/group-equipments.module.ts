import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormGroupEquipmentsComponent } from './form-group-equipments/form-group-equipments.component';
import { GroupEquipmentsRoutingModule } from './group-equipments-routing.module';
import { ListGroupEquipmentsComponent } from './list-group-equipments/list-group-equipments.component';

@NgModule({
  declarations: [
    ListGroupEquipmentsComponent,
    FormGroupEquipmentsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    GroupEquipmentsRoutingModule
  ],

})
export class GroupEquipmentsModule { }
