import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupEquipmentsComponent } from './list-group-equipments.component';

describe('ListGroupEquipmentsComponent', () => {
  let component: ListGroupEquipmentsComponent;
  let fixture: ComponentFixture<ListGroupEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGroupEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
