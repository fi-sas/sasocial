import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipmentGroupService } from '../../../services/equipment-group.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { EquipmentGroup } from '../../../models/Equipment_Group.model';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'fi-sas-list-group-equipments',
  templateUrl: './list-group-equipments.component.html',
  styleUrls: ['./list-group-equipments.component.less']
})
export class ListGroupEquipmentsComponent extends TableHelper implements OnInit {
  
  public YesNoTag = TagComponent.YesNoTag;

  status = [];
  constructor(uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public equipmentGroupService: EquipmentGroupService,
    private modalService: NzModalService,
    ) {
    super(uiService, router, activatedRoute)
  }

  ngOnInit() {
    this.status = [
      {
        description: 'Ativo',
        value: true,
      },
      {
        description: 'Desativo',
        value: false,
      },
    ];
    this.columns.push(
      {
        key: 'description',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      }
    );
    this.initTableData(this.equipmentGroupService);
    this.persistentFilters = {
      searchFields: 'description',
    };
    
  }


  EditEquipmentGroup(id: number) {
    this.router.navigateByUrl('/consumption/group-equipments/edit/' + id);
  }


  desactive(data: EquipmentGroup) {
    this.modalService.confirm({
      nzTitle:
        'Vai desativar este grupo de equipamentos. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data),
    });
  }

  desactiveDataSubmit(data: EquipmentGroup) {
    data.active = false;
    this.equipmentGroupService.desactive(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Grupo de equipamentos desativado com sucesso'
      );
      this.initTableData(this.equipmentGroupService);
    });
  }


  active(data: EquipmentGroup) {
    this.modalService.confirm({
      nzTitle: 'Vai ativar este grupo de equipamentos. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data),
    });
  }

  activeDataSubmit(data: EquipmentGroup) {
    data.active = true;
    this.equipmentGroupService.active(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Grupo de equipamentos ativado com sucesso'
      );
      this.initTableData(this.equipmentGroupService);
    });
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

}
