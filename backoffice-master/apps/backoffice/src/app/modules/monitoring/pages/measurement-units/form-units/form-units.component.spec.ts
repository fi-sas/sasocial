import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormUnitsComponent } from './form-units.component';

describe('FormUnitsComponent', () => {
  let component: FormUnitsComponent;
  let fixture: ComponentFixture<FormUnitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormUnitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUnitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
