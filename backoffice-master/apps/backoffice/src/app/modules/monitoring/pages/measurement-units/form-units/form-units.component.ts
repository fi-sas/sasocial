import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators'
import { UnitsModel } from '../../../models/Units.model';
import { UnitsService } from '../../../services/units.service';


@Component({
  selector: 'fi-sas-form-units',
  templateUrl: './form-units.component.html',
  styleUrls: ['./form-units.component.less']
})
export class FormUnitsComponent implements OnInit {

  loadingEdit = false;
  submitted = false;
  id;

  formUnit = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.maxLength(255), trimValidation]),
    acronym: new FormControl(null, [Validators.required, Validators.maxLength(6), trimValidation]),
    active: new FormControl(true, [Validators.required])
  });


  constructor(private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    public unitsService: UnitsService) { }

  ngOnInit() {
    
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.loadingEdit = true;
      this.getDataUnitById();
    }

  }



  getDataUnitById(){
    let unit: UnitsModel = new UnitsModel();
    this.unitsService.read(this.id)
    .pipe(
      first(),
      finalize(() => this.loadingEdit = false)
    )
    .subscribe((results) => {
      unit = results.data[0];
      this.formUnit.patchValue({
        name: unit.name ? unit.name : null,
        acronym: unit.acronym ? unit.acronym : null,
        active: unit.active ? unit.active : null, 
      });
      this.loadingEdit = false;
    });
  }

  get f() { return this.formUnit.controls; }


  submitUnit(edit) {
    this.submitted = true;
    let sendValues: UnitsModel = new UnitsModel();
    if (this.formUnit.valid ) {
      sendValues = this.formUnit.value;
      if(edit){
        sendValues.id = this.id;
        this.unitsService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Unidade de medição alterado com sucesso'
            );
            this.backList();
          });

      }
      else {
        this.unitsService.create(sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Unidade de medição registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }

  backList(){
    this.router.navigateByUrl('/consumption/units/list');
  }

}
