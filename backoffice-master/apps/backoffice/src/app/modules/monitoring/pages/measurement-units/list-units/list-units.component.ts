import { Component, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd';
import { UnitsService } from '../../../services/units.service';
import { UnitsModel } from '../../../models/Units.model';


@Component({
  selector: 'fi-sas-list-units',
  templateUrl: './list-units.component.html',
  styleUrls: ['./list-units.component.less']
})
export class ListUnitsComponent extends TableHelper implements OnInit {


  public YesNoTag = TagComponent.YesNoTag;

  status = [];

  constructor(uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public unitService: UnitsService,
    private modalService: NzModalService,
  ) {
    super(uiService, router, activatedRoute)
  }


  ngOnInit() {
    this.status = [
      {
        description: 'Ativo',
        value: true,
      },
      {
        description: 'Desativo',
        value: false,
      },
    ];
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'acronym',
        label: 'Acrónimo',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      }
    );
    this.initTableData(this.unitService);
    this.persistentFilters = {
      searchFields: 'name,acronym',
    };
  }


  EditUnit(id: number) {
    this.router.navigateByUrl('/consumption/units/edit/' + id);
  }

  desactive(data: UnitsModel) {
    this.modalService.confirm({
      nzTitle:
        'Vai desativar esta unidade de medição. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data),
    });
  }

  desactiveDataSubmit(data: UnitsModel) {
    data.active = false;
    this.unitService.desactive(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Unidade de medição desativado com sucesso'
      );
      this.initTableData(this.unitService);
    });
  }

  active(data: UnitsModel) {
    this.modalService.confirm({
      nzTitle: 'Vai ativar esta unidade de medição. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data),
    });
  }

  activeDataSubmit(data: UnitsModel) {
    data.active = true;
    this.unitService.active(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Unidade de medição ativado com sucesso'
      );
      this.initTableData(this.unitService);
    });
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }


}
