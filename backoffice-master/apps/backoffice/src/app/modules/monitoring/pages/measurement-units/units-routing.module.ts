import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormUnitsComponent } from './form-units/form-units.component';
import { ListUnitsComponent } from './list-units/list-units.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListUnitsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar'},

  },
  {
    path: 'create',
    component: FormUnitsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar'},
  },
  {
    path: 'edit/:id',
    component: FormUnitsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar',},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitsRoutingModule { }
