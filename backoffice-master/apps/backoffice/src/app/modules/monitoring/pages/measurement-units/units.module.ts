import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormUnitsComponent } from './form-units/form-units.component';
import { ListUnitsComponent } from './list-units/list-units.component';
import { UnitsRoutingModule } from './units-routing.module';


@NgModule({
  declarations: [
    FormUnitsComponent,
    ListUnitsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UnitsRoutingModule
  ],

})
export class UnitsModule { }
