import { Component, OnInit } from '@angular/core';
import { LocationModel } from '@fi-sas/backoffice/modules/infrastructure/models/location.model';
import { LocationsService } from '@fi-sas/backoffice/modules/infrastructure/services/locations.service';
import { first, finalize, debounceTime, switchMap, map } from 'rxjs/operators';
import * as moment from 'moment';
import { EquipmentGroup } from '../../models/Equipment_Group.model';
import { EquipmentModel } from '../../models/Equipment.model';
import { EquipmentGroupService } from '../../services/equipment-group.service';
import { EquipementsService } from '../../services/equipements.service';
import { DashboardService } from '../../services/dashboard.service';
import { ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexFill, ApexLegend, ApexNonAxisChartSeries, ApexPlotOptions, ApexResponsive, ApexStroke, ApexTitleSubtitle, ApexTooltip, ApexXAxis, ApexYAxis } from 'ng-apexcharts';


export type ChartOptions_Max_Min = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
};

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  labels: number[];
  title: ApexTitleSubtitle;
};


@Component({
  selector: 'fi-sas-monitoring-dashboard',
  templateUrl: './monitoring-dashboard.component.html',
  styleUrls: ['./monitoring-dashboard.component.less']
})
export class MonitoringDashboardComponent implements OnInit {


  locations: LocationModel[];
  equipments_group: EquipmentGroup[];
  equipments: EquipmentModel[];
  groupSelected: number;
  locationSelected: number;
  equipmentSelected: number = null;
  search = null;
  sensorSelected: any = null;
  sensors = [];
  listAlerts = [];

  //barCharts
  radioValue = "ever";
  date: Date;
  categories_bar_chart = [];
  series_bar_chart = [];
  public ChartOptions_Max_Min: Partial<ChartOptions_Max_Min>;
  dataRange = [
    moment().toISOString(),
    moment().toISOString(),
  ];


  //lineCharts
  date_line: Date
  start_date: Date;
  end_date: Date;
  loading_chart_line = false;
  category_line_chart;
  dataRangeLine = [
    moment().toISOString(),
    moment().toISOString(),
  ];
  radioValueLine = "ever";
  public ChartOptions: Partial<ChartOptions>;

  constructor(private locationsService: LocationsService,
    public equipmentGroupService: EquipmentGroupService,
    private equipmentsService: EquipementsService,
    private dashboardService: DashboardService) { }

  ngOnInit() {
    this.loadLocations();
    this.loadGroups();
    this.ChartOptions_Max_Min = {
      series: [
      ],
      chart: {
        type: "bar",
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"]
      },
      yaxis: {
        title: {
          text: "Medições maximas"
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
      }
    };
    this.ChartOptions = {
      series: [],
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: false
        },
        animations: {
          enabled: true
        }
      },
      stroke: {
        curve: "straight"
      },
      xaxis: {
        categories: []
      },
    };
  }

  loadLocations() {
    this.locationsService.list(1, -1).pipe(
      first()
    ).subscribe(results => {
      this.locations = results.data;
      if (this.locations) {
        if (this.locations.length >= 1) {
          this.locations = this.locations.filter(
            (thing, i, arr) => arr.findIndex(t => t.description === thing.description) === i
          );
        }
      }
    });
  }

  loadGroups() {
    this.equipmentGroupService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.equipments_group = results.data;
    });
  }

  setLocationGroup(event) {
    this.loadEquipments(this.locationSelected, this.groupSelected);
  }

  loadEquipments(location_id, group_id) {
    this.equipmentsService.list(1, -1, null, null, { active: true, location_id: location_id, measurement_group_id: group_id }).pipe(
      first()
    ).subscribe(results => {
      this.equipments = results.data;
    });
  }

  searchData() {
    if (this.equipmentSelected !== null) {
      this.search = true;
      this.dashboardService.getLastMeasurement(this.equipmentSelected).pipe(
        first()
      ).subscribe(results => {
        let sensor = results.data.map((element) => {
          let container = {
            name: element.name,
            unit_name: element.unit,
            unit_acronym: element.unit_acronym,
            max: element.max,
            min: element.min,
            id: element.id,
            value: element.measurement.length > 0 ? element.measurement[0].last : 0
          };
          return container;

        });
        this.sensors = sensor;
      });
    }

  }

  selectSensor(sensor) {
    this.sensorSelected = sensor;
    this.getLastAlerts();
    this.resetCharts();
  }

  getLastAlerts() {
    this.dashboardService.getAlertsBySensor(this.sensorSelected.id).pipe(
      first()
    ).subscribe(results => {
      this.listAlerts = results.data;
    });
  }

  loadSensors() {
    this.dashboardService.getSensor().pipe(
      first()
    ).subscribe(results => {
      this.equipmentSelected = results.data[0].id;
      this.searchData();
    });
  }

  getMaxMin(filter: string, start_date, end_date) {
    this.dashboardService.getMaxMin(this.sensorSelected.id, start_date, end_date, filter).pipe(
      first()
    ).subscribe(results => {
      if (filter === "BY_MONTH") {
        this.categories_bar_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD"));
      }
      if (filter === "BY_DAY") {
        this.categories_bar_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD"));
      }
      if (filter === "BY_HOURS") {
        this.categories_bar_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD hh:mm:ss"));
      }
      if (filter === "NO_FILTER") {
        this.categories_bar_chart = [moment().format("YYYY-MM-DD")];
      }
      const min = {
        name: "Minimo",
        data: results.data.map((element) => element.min),
      }
      const max = {
        name: "Maximo",
        data: results.data.map((element) => element.max),
      }
      this.series_bar_chart = [max, min];
    });
  }

  changeRadio() {
    if (this.radioValue === "NO_FILTER") {
      this.changeDate();
    }
  }

  changeDate() {
    if (this.radioValue === "BY_MONTH") {
      var start_date = new Date(this.dataRange[0]).toISOString();
      var end_date = new Date(this.dataRange[1]).toISOString();
      this.getMaxMin(this.radioValue, start_date, end_date);
    } else {
      var start_date = moment(this.date).startOf('day').toISOString();
      var end_date = moment(this.date).endOf('day').toISOString();
      this.getMaxMin(this.radioValue, start_date, end_date);
    }
  }

  changeRadioLine() {
    if (this.radioValueLine === "NO_FILTER") {
      this.changeDateLine();
    }
  }


  changeDateLine() {
    if (this.radioValueLine === "AVG_BY_MONTH") {
      var start_date = new Date(this.dataRangeLine[0]).toISOString();
      var end_date = new Date(this.dataRangeLine[1]).toISOString();
      this.getValuesMeasurements(this.radioValueLine, start_date, end_date)
    }
    if (this.radioValueLine === "BY_DAY") {
      if ((this.date_line !== undefined && this.date_line !== null) && (this.end_date !== undefined && this.end_date !== null) && (this.start_date !== undefined && this.start_date !== null)) {
        const date = moment(this.date_line).format("YYYY-MM-DD");
        const hora_inicio = moment(this.start_date).format("HH:mm:ss");
        const hora_fim = moment(this.end_date).format("HH:mm:ss");
        var start_date = new Date(date.toString() + ' ' + hora_inicio.toString()).toISOString();
        var end_date = new Date(date.toString() + ' ' + hora_fim.toString()).toISOString();
        this.getValuesMeasurements(this.radioValueLine, start_date, end_date)
      }
    }
    if (this.radioValueLine === "AVG_BY_HOURS" || this.radioValueLine === "NO_FILTER") {
      var start_date = moment(this.date).startOf('day').toISOString();
      var end_date = moment(this.date).endOf('day').toISOString();
      this.getValuesMeasurements(this.radioValueLine, start_date, end_date)
    }

  }

  getValuesMeasurements(filter: string, start_date, end_date) {
    this.loading_chart_line = true;
    this.dashboardService.getMeasurements(this.sensorSelected.id, start_date, end_date, filter).pipe(
      first()
    ).subscribe(results => {
      if (filter === "NO_FILTER") {
        const values = {
          name: "Medições",
          data: results.data.map((element) => element.measure_value)
        }
        this.ChartOptions.series = [values];
        this.category_line_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD HH:mm:ss"))
      }
      if (filter === "BY_DAY") {
        const values = {
          name: "Medições",
          data: results.data.map((element) => element.measure_value)
        }
        this.ChartOptions.series = [values];
        this.category_line_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD HH:mm:ss"))
      }
      if (filter === "AVG_BY_MONTH") {
        const values = {
          name: "Medições",
          data: results.data.map((element) => element.mean)
        }
        this.ChartOptions.series = [values];
        this.category_line_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD"))
      }
      if (filter === "AVG_BY_HOURS") {
        const values = {
          name: "Medições",
          data: results.data.map((element) => element.mean)
        }
        this.ChartOptions.series = [values];
        this.category_line_chart = results.data.map((element) => moment(element.time).format("YYYY-MM-DD HH:mm:ss"))
      }
      this.loading_chart_line = false;
    });
  }

  resetCharts() {
    this.series_bar_chart = [];
    this.categories_bar_chart = [];
    this.ChartOptions.series = [];
    this.category_line_chart = [];
    this.radioValue = "ever";
    this.radioValueLine = "ever"
  }

  clearFilters() {
    this.sensorSelected = null;
    this.equipmentSelected = null;
    this.locationSelected = null;
    this.groupSelected = null;
    this.sensors = [];
    this.search = null;
    this.resetCharts();
  }

}
