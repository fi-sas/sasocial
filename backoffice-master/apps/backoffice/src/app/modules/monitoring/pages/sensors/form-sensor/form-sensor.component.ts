import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EquipmentModel } from '../../../models/Equipment.model';
import { EquipementsService } from '../../../services/equipements.service';
import { first, finalize, debounceTime, switchMap, map } from 'rxjs/operators';
import { UnitsModel } from '../../../models/Units.model';
import { UnitsService } from '../../../services/units.service';
import { SensorModel } from '../../../models/Sensor.model';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SensorsService } from '../../../services/sensors.service';
import { ActivatedRoute, Router } from '@angular/router';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-sensor',
  templateUrl: './form-sensor.component.html',
  styleUrls: ['./form-sensor.component.less']
})
export class FormSensorComponent implements OnInit {

  
  loadingEdit = false;
  submitted = false;
  id;
  equipments: EquipmentModel[];
  list_units: UnitsModel[];
  list_formats = ["Integer16", "UInteger16", "Integer32", "UInteger32", "Float", "Double"];
  relevances = [
    { description: "Alta", value: "high" }, { description: "Media", value: "medium" }, { description: "Baixa", value: "low" }
  ]

  formSensor = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.maxLength(255), trimValidation]),
    min_value: new FormControl(null, Validators.required),
    max_value : new FormControl(null, Validators.required),
    unit_id: new FormControl(null, [Validators.required]),
    relevance: new FormControl(null),
    registerNumber: new FormControl(null, [Validators.required]),
    format: new FormControl(null, [Validators.required]),
    measurement_equipment_id: new FormControl(null, [Validators.required]),
    minutes_notifications: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required])
  });

  constructor(private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    private equipmentsService: EquipementsService,
    private unitsService: UnitsService,
    private sensorService: SensorsService
  ) { }

  ngOnInit() {
    this.loadEquipments();
    this.loadUnits();

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.loadingEdit = true;
      this.getDataSensorById();
    }

  }

  loadEquipments() {
    this.equipmentsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.equipments = results.data;
    });
  }

  loadUnits(){
    this.unitsService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.list_units = results.data;
    });
  }

  sensor; 
  getDataSensorById() {
    let sensor: SensorModel = new SensorModel();
    this.sensorService.read(this.id)
      .pipe(
        first(),
        finalize(() => this.loadingEdit = false)
      )
      .subscribe((results) => {
        sensor = results.data[0];
        this.sensor = results.data[0];
        this.formSensor.patchValue({
          name: sensor.name ? sensor.name : null,
          min_value: sensor.min_value ? sensor.min_value : null,
          max_value: sensor.max_value ? sensor.max_value : null,
          unit_id: sensor.unit_id ? sensor.unit_id : null,
          measurement_equipment_id : sensor.measurement_equipment_id ?  sensor.measurement_equipment_id : null,
          relevance: sensor.relevance ? sensor.relevance : null,
          registerNumber: sensor.modbus_data ? sensor.modbus_data.registerNumber : null,
          format: sensor.modbus_data ? sensor.modbus_data.format : null,
          minutes_notifications: sensor.minutes_notifications ? sensor.minutes_notifications : null,
          active: sensor.active ? sensor.active : false,
        });
        this.loadingEdit = false;
      });

  }

  submitEquipment(edit) {
    this.submitted = true;
    let sendValues: SensorModel = new SensorModel();
    const modbus = {
      registerNumber: this.formSensor.get('registerNumber').value,
      format: this.formSensor.get('format').value,
    }
    sendValues = this.formSensor.value;
    sendValues.type = "measure";
    sendValues.modbus_data = modbus;
    if (this.formSensor.valid && this.formSensor.get('max_value').value > this.formSensor.get('min_value').value) {
      if (edit) {
        sendValues.id = this.id;
        this.sensorService.update(this.id, sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Sensor alterado com sucesso'
            );
            this.backList();
          });
      } else {
        this.sensorService.create(sendValues).pipe(
          first(), finalize(() => this.loadingEdit = false)).subscribe(() => {
            this.loadingEdit = false;
            this.uiService.showMessage(
              MessageType.success,
              'Sensor registada com sucesso'
            );
            this.backList();
          });
      }
    }
  }


  backList(){
    this.router.navigateByUrl('/consumption/sensor/list');
  }

  get f() { return this.formSensor.controls; }

}
