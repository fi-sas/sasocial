import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { NzModalService } from 'ng-zorro-antd';
import { SensorModel } from '../../../models/Sensor.model';
import { SensorsService } from '../../../services/sensors.service';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { EquipementsService } from '../../../services/equipements.service';
import { UnitsService } from '../../../services/units.service';
import { EquipmentModel } from '../../../models/Equipment.model';
import { first, finalize, debounceTime, switchMap, map } from 'rxjs/operators';
import { UnitsModel } from '../../../models/Units.model';

@Component({
  selector: 'fi-sas-list-sensors',
  templateUrl: './list-sensors.component.html',
  styleUrls: ['./list-sensors.component.less']
})
export class ListSensorsComponent extends TableHelper implements OnInit  {


  public YesNoTag = TagComponent.YesNoTag;

  status = [];
  equipments : EquipmentModel[];
  list_units: UnitsModel[];

  constructor(uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public sensorService: SensorsService,
    public unitService: UnitsService,
    public equipmentService: EquipementsService,
    private modalService: NzModalService,
    ) {
    super(uiService, router, activatedRoute)
    this.persistentFilters = {
      searchFields: 'name',
      withRelated: 'measurement_equipment,unit'
    };
  }

  ngOnInit() {
    this.status = [
      {
        description: 'Ativo',
        value: true,
      },
      {
        description: 'Desativo',
        value: false,
      },
    ];

    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'equipment',
        label: 'Equipamento',
        sortable: false,
      },
      {
        key: 'max_value',
        label: 'Valor máximo',
        sortable: true,
      },
      {
        key: 'min_value',
        label: 'Valor mínimo',
        sortable: true,
      },
      {
        key: 'unit',
        label: 'Unidade de medição',
        sortable: false,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      }
    );
    this.initTableData(this.sensorService);
    this.loadEquipments()
    this.loadUnits();
  }

  loadEquipments(){
    this.equipmentService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.equipments = results.data;
    })
  }

  loadUnits(){
    this.unitService.list(1, -1, null, null, { active: true }).pipe(
      first()
    ).subscribe(results => {
      this.list_units = results.data;
    });
  }

  EditSensor(id: number) {
    this.router.navigateByUrl('/consumption/sensor/edit/' + id);
  }

  desactive(data: SensorModel) {
    this.modalService.confirm({
      nzTitle:
        'Vai desativar este sensor. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.desactiveDataSubmit(data),
    });
  }

  desactiveDataSubmit(data: SensorModel) {
    data.active = false;
    this.sensorService.desactive(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Sensor desativado com sucesso'
      );
      this.initTableData(this.sensorService);
    });
  }

  active(data: SensorModel) {
    this.modalService.confirm({
      nzTitle: 'Vai ativar este sensor. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.activeDataSubmit(data),
    });
  }

  activeDataSubmit(data: SensorModel) {
    data.active = true;
    this.sensorService.active(data.id, data).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Sensor ativado com sucesso'
      );
      this.initTableData(this.sensorService);
    });
  }

  listComplete() {
    this.filters.active = null;
    this.filters.search = null;
    this.searchData(true)
  }

}
