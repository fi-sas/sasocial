import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSensorComponent } from './form-sensor/form-sensor.component';
import { ListSensorsComponent } from './list-sensors/list-sensors.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSensorsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar'},

  },
  {
    path: 'create',
    component: FormSensorComponent,
    data: { breadcrumb: 'Criar', title: 'Criar'},
  },
  {
    path: 'edit/:id',
    component: FormSensorComponent,
    data: { breadcrumb: 'Editar', title: 'Editar',},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SensorsRoutingModule { }
