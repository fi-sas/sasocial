import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FormSensorComponent } from './form-sensor/form-sensor.component';
import { ListSensorsComponent } from './list-sensors/list-sensors.component';
import { SensorsRoutingModule } from './sensors-routing.module';

@NgModule({
  declarations: [
    FormSensorComponent,
    ListSensorsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SensorsRoutingModule
  ],

})
export class SensorModule { }
