import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService  extends Repository<any> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) { 
      super(resourceService, urlService);
    }

  getLastMeasurement(equipment_id: number): Observable<Resource<any>> {
    return this.resourceService.list<any>(this.urlService.get('MONITORING.DASHBOARD_LAST_MEASUREMENT', { id: equipment_id }));
  }

  getMaxMin(sensor_id: number, start_date: string, end_date: string, filter: string): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.set('start_date', start_date);
    params = params.set('end_date', end_date);
    params = params.set('filter', filter);
    return this.resourceService.list<any>(this.urlService.get('MONITORING.MAX_MIN_BY_SENSOR', { id: sensor_id }),{ params })
  }

  getAlertsBySensor(sensor_id: number ) : Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.set('limit', "5");
    params = params.set('query[measurement_configuration_id]', sensor_id.toString());
    params = params.set('withRelated', 'measurement_configuration');
    return this.resourceService.list<any>(this.urlService.get('MONITORING.LIST_ALERTS_SENSOR', { id: sensor_id }),{ params })
  }


  getMeasurements(sensor_id: number, start_date: string, end_date: string, filter: string) {
    let params = new HttpParams();
    params = params.set('start_date', start_date);
    params = params.set('end_date', end_date);
    params = params.set('filter', filter);
    return this.resourceService.list<any>(this.urlService.get('MONITORING.LIST_MEASUREMENTS_BY_SENSOR', { id: sensor_id }),{ params })
  }

  getSensor() {
    return this.resourceService.list<any>(this.urlService.get('MONITORING.LIST_SENSORS'));
  }


}
