import { TestBed } from '@angular/core/testing';

import { EquipementsService } from './equipements.service';

describe('EquipementsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EquipementsService = TestBed.get(EquipementsService);
    expect(service).toBeTruthy();
  });
});
