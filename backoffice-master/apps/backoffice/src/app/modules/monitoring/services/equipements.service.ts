import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { EquipmentModel } from '../models/Equipment.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipementsService  extends Repository<EquipmentModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) { 
    super(resourceService, urlService);
      this.entities_url = 'MONITORING.EQUIPMENTS';
      this.entity_url = 'MONITORING.EQUIPMENTS_ID';
  }

  
  desactive(id: number, data: any): Observable<Resource<EquipmentModel>> {
    return this.resourceService.patch<EquipmentModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: any): Observable<Resource<EquipmentModel>> {
    return this.resourceService.patch<EquipmentModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }
}
