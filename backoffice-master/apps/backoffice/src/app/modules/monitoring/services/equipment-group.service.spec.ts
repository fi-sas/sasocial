import { TestBed } from '@angular/core/testing';

import { EquipmentGroupService } from './equipment-group.service';

describe('EquipmentGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EquipmentGroupService = TestBed.get(EquipmentGroupService);
    expect(service).toBeTruthy();
  });
});
