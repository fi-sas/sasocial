import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { EquipmentGroup } from '../models/Equipment_Group.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipmentGroupService  extends Repository<EquipmentGroup> {

  constructor( resourceService: FiResourceService,
    urlService: FiUrlService) { 
      super(resourceService, urlService);
      this.entities_url = 'MONITORING.GROUP_EQUIPMENTS';
      this.entity_url = 'MONITORING.GROUP_EQUIPMENTS_ID';
  }


  desactive(id: number, data: any): Observable<Resource<EquipmentGroup>> {
    return this.resourceService.patch<EquipmentGroup>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }

  active(id: number, data: any): Observable<Resource<EquipmentGroup>> {
    return this.resourceService.patch<EquipmentGroup>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders()});
  }
}
