import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { SensorModel } from '../models/Sensor.model';


@Injectable({
  providedIn: 'root'
})
export class SensorsService extends Repository<SensorModel> {

  
  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) { 
    super(resourceService, urlService);
      this.entities_url = 'MONITORING.SENSOR';
      this.entity_url = 'MONITORING.SENSOR_ID';
  }


   desactive(id: number, data: any): Observable<Resource<SensorModel>> {
    return this.resourceService.patch<SensorModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders() });
  }

  active(id: number, data: any): Observable<Resource<SensorModel>> {
    return this.resourceService.patch<SensorModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders() });
  }
}
