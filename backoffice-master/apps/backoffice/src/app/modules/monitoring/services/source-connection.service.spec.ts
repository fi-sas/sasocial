import { TestBed } from '@angular/core/testing';

import { SourceConnectionService } from './source-connection.service';

describe('SourceConnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SourceConnectionService = TestBed.get(SourceConnectionService);
    expect(service).toBeTruthy();
  });
});
