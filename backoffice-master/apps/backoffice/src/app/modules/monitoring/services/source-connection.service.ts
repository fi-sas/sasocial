import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { SourceConnectionModel } from '../models/Source_Connection.model';

@Injectable({
  providedIn: 'root'
})
export class SourceConnectionService  extends Repository<SourceConnectionModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) { 
      super(resourceService, urlService);
      this.entities_url = 'MONITORING.SOURCE_CONNECTION';
      this.entity_url = 'MONITORING.SOURCE_CONNECTION_ID';
  }
}
