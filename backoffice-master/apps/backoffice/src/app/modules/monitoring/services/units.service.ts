import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { UnitsModel } from '../models/Units.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UnitsService extends Repository<UnitsModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'MONITORING.UNITS';
    this.entity_url = 'MONITORING.UNITS_ID';
  }


  desactive(id: number, data: any): Observable<Resource<UnitsModel>> {
    return this.resourceService.patch<UnitsModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders() });
  }

  active(id: number, data: any): Observable<Resource<UnitsModel>> {
    return this.resourceService.patch<UnitsModel>(this.urlService.get(this.entity_url, { id }), data, { headers: this.getHeaders() });
  }
}
