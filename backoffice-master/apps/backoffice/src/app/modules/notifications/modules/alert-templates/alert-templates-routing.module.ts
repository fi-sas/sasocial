import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAlertTemplateComponent } from './pages/form-alert-template/form-alert-template.component';
import { ListAlertTemplatesComponent } from './pages/list-alert-templates/list-alert-templates.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAlertTemplatesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope:'notifications:alert-templates:read' }
  },
  {
    path: 'create',
    component: FormAlertTemplateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope:'notifications:alert-templates:create' }
  },
  {
    path: 'edit/:id',
    component: FormAlertTemplateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'notifications:alert-templates:update' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlertTemplatesRoutingModule { }
