import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormAlertTemplateComponent } from './pages/form-alert-template/form-alert-template.component';
import { ViewAlertTemplateComponent } from './pages/view-alert-template/view-alert-template.component';
import { ListAlertTemplatesComponent } from './pages/list-alert-templates/list-alert-templates.component';
import { AlertTemplatesRoutingModule } from './alert-templates-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { MonacoEditorModule } from 'ngx-monaco-editor';

@NgModule({
  declarations: [FormAlertTemplateComponent, ViewAlertTemplateComponent, ListAlertTemplatesComponent],
  imports: [
    CommonModule,
    AlertTemplatesRoutingModule,
    SharedModule,
    MonacoEditorModule
  ],
  providers: []
})
export class AlertTemplatesModule { }
