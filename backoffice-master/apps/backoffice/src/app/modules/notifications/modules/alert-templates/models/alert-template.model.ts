
enum AlertTemplateMediasType {
  IMAGE = 'Image',
  ATTACHMENT = 'Attachment'
}

export class AlertTemplateMediaModel {
  type: AlertTemplateMediasType;
  key: string;
  file_id: number;
}

export class AlertTemplateModel  {
  id: number;
  updated_at: string;
  created_at: string;
  name: string;
  template: string;
  data: any;
  medias: AlertTemplateMediaModel[];
}
