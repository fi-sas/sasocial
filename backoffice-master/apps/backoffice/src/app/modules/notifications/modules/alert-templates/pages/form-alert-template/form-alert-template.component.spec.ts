import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAlertTemplateComponent } from './form-alert-template.component';

describe('FormAlertTemplateComponent', () => {
  let component: FormAlertTemplateComponent;
  let fixture: ComponentFixture<FormAlertTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAlertTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAlertTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
