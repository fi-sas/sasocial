
import {
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { AlertTemplateMediaModel } from '../../models/alert-template.model';
import { AlertTemplatesService } from '../../services/alert-templates.service';

@Component({
  selector: 'fi-sas-form-alert-template',
  templateUrl: './form-alert-template.component.html',
  styleUrls: ['./form-alert-template.component.less'],
})
export class FormAlertTemplateComponent implements OnInit {
  loading = false;
  idtoUpdate = null;
  submit = false;
  editorOptions = { theme: 'vs', language: 'html' };

  alertTemplateGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required,trimValidation]),
    template: new FormControl('', [Validators.required]),
    data: new FormControl({}, [Validators.required]),
    medias: new FormArray([]),
  });
  get f() { return this.alertTemplateGroup.controls; }
  // PREVIEW
  //@ViewChildren(UploadFileComponent) uploadsFile: QueryList<UploadFileComponent>;
  @ViewChild('preview', { static: true }) preview: HTMLDivElement;
  src = this.sanitizer.bypassSecurityTrustHtml(
    '<iframe width="100%" height="500px" frameBorder="0"></iframe>'
  );
  medias_preview = {};

  constructor(
    private uiService: UiService,
    public router: Router,
    private sanitizer: DomSanitizer,
    public activateRoute: ActivatedRoute,
    public alertTemplatesService: AlertTemplatesService
  ) {}

  utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  ngOnInit() {
    this.load();

    this.alertTemplateGroup.get('template').valueChanges.subscribe((html) => {
      /*
      for (let media of this.getMedias().controls) {
        const mediafg = media as FormGroup;
        if(!this.medias_preview[mediafg.get('key').value]) {
          this.medias_preview[mediafg.get('key').value] = this.uploadsFile.find(uc => uc.value === mediafg.get('file_id').value).mediaObj.url;

        }
      }

      Object.keys(this.medias_preview).map(key => {
        html = html.replace(`{{medias.image.${key}.url}}`, this.medias_preview[key])
      }); */

      html = 'data:text/html;base64,' + this.utf8_to_b64(html);
      this.src = this.sanitizer.bypassSecurityTrustHtml(
        '<iframe width="100%" height="500px" frameBorder="0"  src="' +
          html +
          '"></iframe>'
      );
    });
  }

  load() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idtoUpdate = data.get('id');
        this.alertTemplatesService.read(this.idtoUpdate).subscribe((result) => {
          this.alertTemplateGroup.patchValue({
            name: result.data[0].name,
            template: result.data[0].template,
            data: result.data[0].data,
          });

          result.data[0].medias.map((media) => {
            this.addNewMedia(media);
          });
        });
      }
    });
  }

  getMedias(): FormArray {
    return this.alertTemplateGroup.get('medias') as FormArray;
  }

  addNewMedia(media?: AlertTemplateMediaModel) {
    const medias = <FormArray>this.alertTemplateGroup.get('medias');
    medias.push(
      new FormGroup({
        type: new FormControl(media ? media.type : 'IMAGE', [
          Validators.required,
        ]),
        key: new FormControl(media ? media.key : '', [Validators.required,trimValidation]),
        file_id: new FormControl(media ? media.file_id : null, [
          Validators.required,
        ]),
      })
    );
  }

  removeMedia(i: number) {
    this.getMedias().removeAt(i);
  }

  submitForm(event: any, value: any) {
    this.submit = true;
    // FORM
    for (const i in this.alertTemplateGroup.controls) {
      if (i) {
        this.alertTemplateGroup.controls[i].markAsDirty();
        this.alertTemplateGroup.controls[i].updateValueAndValidity();
      }
    }

    for (let media of this.getMedias().controls) {
      const mediafg = media as FormGroup;
      for (const i in mediafg.controls) {
        if (i) {
          mediafg.controls[i].markAsDirty();
          mediafg.controls[i].updateValueAndValidity();
        }
      }
    }

    if (this.alertTemplateGroup.valid) {
      this.loading = true;
      this.submit = false;
      if (this.idtoUpdate) {
        this.alertTemplatesService
          .update(this.idtoUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            this.router.navigate(['notifications', 'alert-templates', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Modelo de alerta alterado com sucesso'
            );
          });
      } else {
        this.alertTemplatesService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            this.router.navigate(['notifications', 'alert-templates', 'list']);
            this.uiService.showMessage(
              MessageType.success,
              'Modelo de alerta criado com sucesso'
            );
          });
      }
    }
  }

  returnButton() {
      this.router.navigate(['notifications', 'alert-templates', 'list']);
  }
}
