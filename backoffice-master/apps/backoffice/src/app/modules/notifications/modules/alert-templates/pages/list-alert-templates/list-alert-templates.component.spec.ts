import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAlertTemplatesComponent } from './list-alert-templates.component';

describe('ListAlertTemplatesComponent', () => {
  let component: ListAlertTemplatesComponent;
  let fixture: ComponentFixture<ListAlertTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAlertTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAlertTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
