import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';
import { AlertTemplatesService } from '../../services/alert-templates.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-list-alert-templates',
  templateUrl: './list-alert-templates.component.html',
  styleUrls: ['./list-alert-templates.component.less']
})
export class ListAlertTemplatesComponent  extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private alertTemplatesService: AlertTemplatesService
  ) { 
    super(uiService, router, activatedRoute);

    this.persistentFilters = {
      withRelated: 'false'
    }
  }

  ngOnInit() {
    this.columns.push(
      {
      key: 'name',
      label: 'Nome',
      sortable: true
    },
    {
      key: '',
      label: 'Criado em',
      template: (data) => {
        return moment(new Date(data.created_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    },
    {
      key: '',
      label: 'Alterado em',
      template: (data) => {
        return moment(new Date(data.updated_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    },
    )

    this.initTableData(this.alertTemplatesService);
  }

  deleteAlertType(id: number) {
    this.uiService.showConfirm('Eliminar modelo de alerta', 'Pretende eliminar este modelo de alerta', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.alertTemplatesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Modelo de alerta eliminado com sucesso');
        });
      }
    });
  }
}

