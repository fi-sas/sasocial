import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAlertTemplateComponent } from './view-alert-template.component';

describe('ViewAlertTemplateComponent', () => {
  let component: ViewAlertTemplateComponent;
  let fixture: ComponentFixture<ViewAlertTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAlertTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAlertTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
