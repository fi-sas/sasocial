import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertTemplateModel } from '../../models/alert-template.model';

@Component({
  selector: 'fi-sas-view-alert-template',
  templateUrl: './view-alert-template.component.html',
  styleUrls: ['./view-alert-template.component.less']
})
export class ViewAlertTemplateComponent implements OnInit {

  @Input() data: AlertTemplateModel = null;

  constructor(public domSanitizer:DomSanitizer) {}

  ngOnInit() {
  }

  encodeURIComponent(html: string): string {
    return encodeURIComponent(html);
  }
}
