import { TestBed } from '@angular/core/testing';

import { AlertTemplatesService } from './alert-templates.service';

describe('AlertTemplatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlertTemplatesService = TestBed.get(AlertTemplatesService);
    expect(service).toBeTruthy();
  });
});
