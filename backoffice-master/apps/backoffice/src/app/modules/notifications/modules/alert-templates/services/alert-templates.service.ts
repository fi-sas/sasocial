import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { AlertTemplateModel } from '../models/alert-template.model';

@Injectable({
  providedIn: 'root'
})
export class AlertTemplatesService  extends Repository<AlertTemplateModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'NOTIFICATIONS.ALERT_TEMPLATES';
    this.entity_url = 'NOTIFICATIONS.ALERT_TEMPLATES_ID';
  }
}
