import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAlertTypeComponent } from './pages/form-alert-type/form-alert-type.component';
import { ListAlertTypesComponent } from './pages/list-alert-types/list-alert-types.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAlertTypesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope:'notifications:alert-types:read' }
  },
  {
    path: 'create',
    component: FormAlertTypeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope:'notifications:alert-types:create' }
  },
  {
    path: 'edit/:id',
    component: FormAlertTypeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope:'notifications:alert-types:update'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlertTypesRoutingModule { }
