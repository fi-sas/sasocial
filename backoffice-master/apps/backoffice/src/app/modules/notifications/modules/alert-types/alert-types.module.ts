import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertTypesRoutingModule } from './alert-types-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListAlertTypesComponent } from './pages/list-alert-types/list-alert-types.component';
import { FormAlertTypeComponent } from './pages/form-alert-type/form-alert-type.component';
import { AlertTypesService } from './services/alert-types.service';
import { ViewAlertTypeComponent } from './pages/view-alert-type/view-alert-type.component';
import { MonacoEditorModule } from 'ngx-monaco-editor';

@NgModule({
  declarations: [
    FormAlertTypeComponent,
    ListAlertTypesComponent,
    ViewAlertTypeComponent
  ],
  imports: [
    CommonModule,
    AlertTypesRoutingModule,
    MonacoEditorModule,
    SharedModule
  ],
  providers: [
    AlertTypesService
  ]
})
export class AlertTypesModule { }
