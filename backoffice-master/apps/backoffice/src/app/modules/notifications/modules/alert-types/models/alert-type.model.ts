import { AlertTemplateModel } from "../../alert-templates/models/alert-template.model";
import { NotificationMethodModel } from "../../notification-methods/models/notification-method.model";


enum AlertTypeMediasType {
  IMAGE = 'Image',
  ATTACHMENT = 'Attachment'
}

export class AlertTypeMediaModel {
  type: AlertTypeMediasType;
  key: string;
  file_id: number;
}

export class AlertTypeNotificationsMethodsModel {
  notification_method_id: number;
  priority: number;
  updated_at?: string;
  created_at?: string;
  notification_method?: NotificationMethodModel;
}

export class AlertTypeModel {
  id: number;
  alert_template_id?: number;
  alert_template?: AlertTemplateModel;
  updated_at: string;
  created_at: string;
  service_id: number;
  name: string;
  description: string;
  subject: string;
  message: string;
  simplified_message: string;
  data: {};
  medias: AlertTypeMediaModel[];
  key: string;
  endpoint_url: string;
  notification_methods: AlertTypeNotificationsMethodsModel[];
}
