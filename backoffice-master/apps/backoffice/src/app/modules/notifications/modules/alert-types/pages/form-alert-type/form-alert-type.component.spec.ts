import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAlertTypeComponent } from './form-alert-type.component';

describe('FormAlertTypeComponent', () => {
  let component: FormAlertTypeComponent;
  let fixture: ComponentFixture<FormAlertTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAlertTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAlertTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
