import { Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { AlertTypesService } from '../../services/alert-types.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from '@fi-sas/backoffice/modules/configurations/services/services.service';
import { NotificationsMethodsService } from '../../../notification-methods/services/notifications-methods.service';
import { AlertsService } from '../../../alerts/services/alerts.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ServiceModel } from '@fi-sas/backoffice/modules/configurations/models/service.model';
import { first, finalize } from 'rxjs/operators';
import { NotificationMethodModel } from '@fi-sas/backoffice/modules/notifications/modules/notification-methods/models/notification-method.model';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AlertTypeNotificationsMethodsModel } from '../../models/alert-type.model';
import { AlertTypeMediaModel } from '../../models/alert-type.model';
import { AlertTemplateModel } from '../../../alert-templates/models/alert-template.model';
import { AlertTemplatesService } from '../../../alert-templates/services/alert-templates.service';
import { DomSanitizer } from '@angular/platform-browser';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-alert-type',
  templateUrl: './form-alert-type.component.html',
  styleUrls: ['./form-alert-type.component.less'],
})
export class FormAlertTypeComponent implements OnInit {
  loading = false;
  idtoUpdate = null;

  simulateModalVisible = false;
  simulateData= {};

  services: ServiceModel[] = [];
  load_services = false;
  submit = false;
  notificationMethods: NotificationMethodModel[] = [];
  load_notificationsMethods = false;

  alert_templates: AlertTemplateModel[] = [];

  load_alert_templates = false;

  editorOptions = { theme: 'vs', language: 'html' };

  alertTypeGroup: FormGroup = new FormGroup({
    service_id: new FormControl(null, [Validators.required]),
    alert_template_id: new FormControl(null, [Validators.required]),
    key: new FormControl('', [Validators.required,trimValidation]),
    name: new FormControl('', [Validators.required,trimValidation]),
    description: new FormControl('', [Validators.required,trimValidation]),
    // TEMPLATE
    subject: new FormControl('', [Validators.required,trimValidation]),
    simplified_message: new FormControl('', [Validators.required,trimValidation]),
    message: new FormControl('', [Validators.required]),
    data: new FormControl({}, [Validators.required]),
    medias: new FormArray([]),
    notification_methods: new FormArray([]),
  });
  get f() { return this.alertTypeGroup.controls; }
  // PREVIEW
  //@ViewChildren(UploadFileComponent) uploadsFile: QueryList<UploadFileComponent>;
  @ViewChild('preview', { static: true }) preview: HTMLDivElement;
  src = this.sanitizer.bypassSecurityTrustHtml(
    '<iframe width="100%" height="500px" frameBorder="0"></iframe>'
  );
  medias_preview = {};

  constructor(
    private uiService: UiService,
    public router: Router,
    private sanitizer: DomSanitizer,
    public activateRoute: ActivatedRoute,
    public alertTypesService: AlertTypesService,
    private alertTemplatesService: AlertTemplatesService,
    public servicesService: ServicesService,
    private alertsService: AlertsService,
    public notificationsMethodsService: NotificationsMethodsService,
    private authService: AuthService
  ) {}

  utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  ngOnInit() {
    this.load();
    this.loadServices();
    this.loadTemplates();
    this.loadNotificationMethods();

    this.alertTypeGroup.get('message').valueChanges.subscribe((html) => {
      /*
    for (let media of this.getMedias().controls) {
      const mediafg = media as FormGroup;
      if(!this.medias_preview[mediafg.get('key').value]) {
        this.medias_preview[mediafg.get('key').value] = this.uploadsFile.find(uc => uc.value === mediafg.get('file_id').value).mediaObj.url;

      }
    }

    Object.keys(this.medias_preview).map(key => {
      html = html.replace(`{{medias.image.${key}.url}}`, this.medias_preview[key])
    }); */

      const alertTemplate = this.alert_templates.find(
        (t) => t.id === this.alertTypeGroup.get('alert_template_id').value
      );

      let template = '';
      if (alertTemplate) {
        template = alertTemplate.template.replace('{{{message}}}', html);
      }

      html = 'data:text/html;base64,' + this.utf8_to_b64(template);
      this.src = this.sanitizer.bypassSecurityTrustHtml(
        '<iframe width="100%" height="500px" frameBorder="0"  src="' +
          html +
          '"></iframe>'
      );
    });
  }

  load() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idtoUpdate = data.get('id');
        this.alertTypesService.read(this.idtoUpdate).subscribe((result) => {
          this.alertTypeGroup.patchValue({
            service_id: result.data[0].service_id,
            key: result.data[0].key,
            alert_template_id: result.data[0].alert_template_id,
            name: result.data[0].name,
            description: result.data[0].description,
            subject: result.data[0].subject,
            simplified_message: result.data[0].simplified_message,
            message: result.data[0].message,
            data: result.data[0].data,
          });

          this.simulateData = result.data[0].data;

          result.data[0].medias.map((media) => {
            this.addNewMedia(media);
          });

          result.data[0].notification_methods.map((nm) => {
            this.addNewNotificationMethod(nm);
          });

        });
      }
    });
  }

  loadTemplates() {
    this.load_alert_templates = true;
    this.alertTemplatesService
      .list(1, -1, null, null)
      .pipe(
        first(),
        finalize(() => (this.load_alert_templates = false))
      )
      .subscribe((results) => {
        this.alert_templates = results.data;
      });
  }

  loadServices() {
    this.load_services = true;
    this.servicesService
      .list(1, -1, null, null,{
        active: true
      })
      .pipe(
        first(),
        finalize(() => (this.load_services = false))
      )
      .subscribe((results) => {
        this.services = results.data;
      });
  }

  loadNotificationMethods() {
    this.load_notificationsMethods = true;
    this.notificationsMethodsService
      .list(1, -1, null, null, {
        sort: 'name'
      })
      .pipe(
        first(),
        finalize(() => (this.load_notificationsMethods = false))
      )
      .subscribe((results) => {
        this.notificationMethods = results.data;
      });
  }

  getMedias(): FormArray {
    return this.alertTypeGroup.get('medias') as FormArray;
  }

  addNewMedia(media?: AlertTypeMediaModel) {
    const medias = <FormArray>this.alertTypeGroup.get('medias');
    medias.push(
      new FormGroup({
        type: new FormControl(media ? media.type : 'IMAGE', [
          Validators.required,
        ]),
        key: new FormControl(media ? media.key : '', [Validators.required,trimValidation]),
        file_id: new FormControl(media ? media.file_id : null, [
          Validators.required,
        ]),
      })
    );
  }

  removeMedia(i: number) {
    this.getMedias().removeAt(i);
  }

  getNotificationMethods(): FormArray {
    return this.alertTypeGroup.get('notification_methods') as FormArray;
  }

  addNewNotificationMethod(nm?: AlertTypeNotificationsMethodsModel) {
    const notification_methods = <FormArray>(
      this.alertTypeGroup.get('notification_methods')
    );
    notification_methods.push(
      new FormGroup({
        notification_method_id: new FormControl(
          nm ? nm.notification_method_id : null,
          [Validators.required]
        ),
        priority: new FormControl(nm ? nm.priority : 1, [Validators.required]),
      })
    );
  }

  removeNotificationMethod(i: number) {
    this.getNotificationMethods().removeAt(i);
  }

  sendTestNotification(value: any) {
    this.alertsService
      .createAlert(value.key, {
        user_id: this.authService.getUser().id,
        user_data: {},
        data: value.data,
        medias: [],
      })
      .subscribe(
        (results) => {},
        (err) => {}
      );
  }

  submitForm(event: any, value: any, simulate: boolean = false) {
    // FORM
    this.submit = true;
    for (const i in this.alertTypeGroup.controls) {
      if (i) {
        this.alertTypeGroup.controls[i].markAsDirty();
        this.alertTypeGroup.controls[i].updateValueAndValidity();
      }
    }

    for (let media of this.getMedias().controls) {
      const mediafg = media as FormGroup;
      for (const i in mediafg.controls) {
        if (i) {
          mediafg.controls[i].markAsDirty();
          mediafg.controls[i].updateValueAndValidity();
        }
      }
    }

    for (let nm of this.getNotificationMethods().controls) {
      const nmfg = nm as FormGroup;
      for (const i in nmfg.controls) {
        if (i) {
          nmfg.controls[i].markAsDirty();
          nmfg.controls[i].updateValueAndValidity();
        }
      }
    }

    if (this.alertTypeGroup.valid) {
      this.loading = true;
      this.submit = false;
      if (this.idtoUpdate) {
        this.alertTypesService
          .update(this.idtoUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            if(!simulate) {
              this.router.navigate(['notifications', 'alert-types', 'list']);
            } else {
              this.openSimulateModal();
            }


            this.uiService.showMessage(
              MessageType.success,
              'Tipo de alerta alterado com sucesso'
            );
          });
      } else {
        this.alertTypesService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((results) => {
            if(!simulate) {
              this.router.navigate(['notifications', 'alert-types', 'list']);
            } else {
              this.idtoUpdate = results.data[0].id;
              this.simulateData = value.data;
              this.openSimulateModal();
            }

            this.uiService.showMessage(
              MessageType.success,
              'Tipo de alerta criado com sucesso'
            );
          });
      }
    }
  }

  openSimulateModal() {
    this.simulateModalVisible = true;
  }

  closeSimulateModal() {
    this.simulateModalVisible = false;
  }

  simulate() {
    this.loading = true;
    this.alertTypesService.simulate(this.idtoUpdate, this.simulateData).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
      this.simulateModalVisible = false;
      this.uiService.showMessage(
        MessageType.success,
        'Simulação enviada com sucesso'
      );
    });
  }

  returnButton() {
    this.router.navigate(['notifications', 'alert-types', 'list']);
  }
}
