import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAlertTypesComponent } from './list-alert-types.component';

describe('ListAlertTypesComponent', () => {
  let component: ListAlertTypesComponent;
  let fixture: ComponentFixture<ListAlertTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAlertTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAlertTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
