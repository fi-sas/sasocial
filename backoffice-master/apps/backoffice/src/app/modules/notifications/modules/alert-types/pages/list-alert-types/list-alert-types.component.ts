import { Keys } from '../../../../../sport/models/configuration.model';
import { AlertTypesService } from '../../services/alert-types.service';
import { Component, OnInit } from '@angular/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-alert-types',
  templateUrl: './list-alert-types.component.html',
  styleUrls: ['./list-alert-types.component.less']
})
export class ListAlertTypesComponent  extends TableHelper  implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private alertTypesService: AlertTypesService
  ) { 
    super(uiService, router, activatedRoute);

    this.persistentFilters = {
      withRelated: 'template,template.medias,notification_methods',
      searchFields: 'name,key',
    }
  }

  ngOnInit() {
    this.columns.push(
      {
      key: 'name',
      label: 'Nome',
      sortable: true
    },
      {
      key: 'key',
      label: 'Chave',
      sortable: true
    }
    )

    this.initTableData(this.alertTypesService);
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true)
  }

  deleteAlertType(id: number) {
    this.uiService.showConfirm('Eliminar tipo de alerta', 'Pretende eliminar este tipo de alerta', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.alertTypesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Tipo de alerta eliminado com sucesso');
        });
      }
    });
  }
}
