import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAlertTypeComponent } from './view-alert-type.component';

describe('ViewAlertTypeComponent', () => {
  let component: ViewAlertTypeComponent;
  let fixture: ComponentFixture<ViewAlertTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAlertTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAlertTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
