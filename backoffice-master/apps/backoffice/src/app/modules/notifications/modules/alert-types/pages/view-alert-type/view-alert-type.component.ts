import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertTypeModel } from '../../models/alert-type.model';

@Component({
  selector: 'fi-sas-view-alert-type',
  templateUrl: './view-alert-type.component.html',
  styleUrls: ['./view-alert-type.component.less']
})
export class ViewAlertTypeComponent implements OnInit {

  @Input() data: AlertTypeModel = null;

  constructor(public domSanitizer:DomSanitizer) {}

  ngOnInit() {
  }

  encodeURIComponent(html: string): string {
    return encodeURIComponent(html);
  }

}
