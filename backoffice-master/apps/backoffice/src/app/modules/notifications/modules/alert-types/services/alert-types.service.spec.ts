import { TestBed } from '@angular/core/testing';

import { AlertTypesService } from './alert-types.service';

describe('AlertTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlertTypesService = TestBed.get(AlertTypesService);
    expect(service).toBeTruthy();
  });
});
