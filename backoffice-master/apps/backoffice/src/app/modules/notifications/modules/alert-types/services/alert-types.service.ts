import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AlertTypeModel } from '../models/alert-type.model';
import { AlertModel } from '../../alerts/models/alert.model';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlertTypesService extends Repository<AlertTypeModel> {

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'NOTIFICATIONS.ALERT_TYPES';
    this.entity_url = 'NOTIFICATIONS.ALERT_TYPES_ID';
  }

  simulate(
    id: number, data: any
  ): Observable<Resource<AlertModel>> {
    return this.resourceService
      .create<AlertModel>(
        this.urlService.get('NOTIFICATIONS.ALERT_TYPE_SIMULATE_ID', { id }),
        {
          data
        }
      )
      .pipe(first());
  }
}
