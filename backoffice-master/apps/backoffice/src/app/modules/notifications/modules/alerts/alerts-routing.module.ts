import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAlertsComponent } from './pages/list-alerts/list-alerts.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListAlertsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope:'notifications:alerts:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlertsRoutingModule { }
