import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertsRoutingModule } from './alerts-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListAlertsComponent } from './pages/list-alerts/list-alerts.component';
import { ViewAlertComponent } from './pages/view-alert/view-alert.component';
import { AlertsService } from './services/alerts.service';

@NgModule({
  declarations: [
    ListAlertsComponent,
    ViewAlertComponent
  ],
  imports: [
    CommonModule,
    AlertsRoutingModule,
    SharedModule
  ],
  providers: [
    AlertsService
  ]  
})
export class AlertsModule { }
