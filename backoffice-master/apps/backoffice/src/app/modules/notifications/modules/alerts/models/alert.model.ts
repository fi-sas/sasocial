
import { AlertTypeMediaModel, AlertTypeModel } from "../../alert-types/models/alert-type.model";
import { NotificationModel } from "../../notifications/models/notification.model";


export enum AlertStatus {
  NEW = 'NEW',
  PENDING= 'PENDING',
  NOTIFIED= 'NOTIFIED',
  READ= 'READ',
  ERROR= 'ERROR'
}

export class AlertHistoryModel {
    id: number;
    status: AlertStatus;
    user_id: number;
    notes: string;
    updated_at: string;
    created_at: string

}

export class AlertModel {
      id?: number;
      type?: AlertTypeModel;
      alert_type_id?: number;
      user_id: number;
      user_data: {
        name?: string;
        email?: string;
        phone?: string;
        fb_messenger_id?: string;
      };
      data: {};
      medias?: AlertTypeMediaModel[];
      updated_at?: string;
      created_at?: string;
      status?: AlertStatus;
      history?: AlertHistoryModel[];
      notifications?: NotificationModel[]
}
