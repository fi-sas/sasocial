import { AlertsService } from '../../services/alerts.service';
import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-alerts',
  templateUrl: './list-alerts.component.html',
  styleUrls: ['./list-alerts.component.less']
})
export class ListAlertsComponent  extends TableHelper  implements OnInit {

  pageSize = 10;

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public alertsService: AlertsService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'type,notifications,history'
    };
    this.columns.push({
      key: 'type.name',
      label: 'Tipo',
      sortable: false
    },
    {
      key: 'status',
      label: 'Estado',
      sortable: true,
      tag: {
        NEW: { label: 'Novo', color: 'blue'},
        PENDING: { label: 'Em espera', color: 'yellow'},
        NOTIFIED: { label: 'Notificado', color: 'green'},
        READ: { label: 'Lido', color: 'green'},
        ERROR: { label: 'Erro', color: 'red'}
      }
    })

    this.initTableData(this.alertsService);
  }

  retry(id: number) {
    this.uiService.showConfirm("Reenviar", "Pretende reenviar esta alerta").pipe(
      first()
    ).subscribe(confirm => {
      if (confirm) {
        this.alertsService.retryAlert(id).subscribe();
        this.uiService.showMessage(MessageType.info, "Alerta a ser reenviado");
      }
    });
  }
}
