import { AlertModel } from '../../models/alert.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-alert',
  templateUrl: './view-alert.component.html',
  styleUrls: ['./view-alert.component.less']
})
export class ViewAlertComponent implements OnInit {

  resultStatus = {
    NEW: { label: 'Novo', color: 'blue'},
    PENDING: { label: 'Em espera', color: 'yellow'},
    NOTIFIED: { label: 'Notificado', color: 'green'},
    READ: { label: 'Lido', color: 'green'},
    ERROR: { label: 'Erro', color: 'red'}
  };

  resultNotificationStatus = {
    SUCCESS: { label: 'Sucesso', color: 'blue'},
    PENDING: { label: 'Em espera', color: 'yellow'},
    ERROR: { label: 'Erro', color: 'red'}
  };

  @Input() data: AlertModel = null;

  constructor(
  ) { }

  ngOnInit() {
  }

}
