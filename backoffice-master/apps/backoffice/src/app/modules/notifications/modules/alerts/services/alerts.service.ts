import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AlertModel } from '../models/alert.model';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlertsService extends Repository<AlertModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'NOTIFICATIONS.ALERTS';
    this.entity_url = 'NOTIFICATIONS.ALERTS_ID';
  }

  createAlert(alert_key: string, entity: AlertModel) {
    return this.resourceService.create<AlertModel>(
      this.urlService.get('NOTIFICATIONS.ALERTS_CREATE', { alert_key }),
      entity)
      .pipe(
        first()
      );
  }

  retryAlert(id: number) {
    return this.resourceService.create<AlertModel>(
      this.urlService.get('NOTIFICATIONS.ALERTS_ID_RETRY', { id }),
      {})
      .pipe(
        first()
      );
  }
}
