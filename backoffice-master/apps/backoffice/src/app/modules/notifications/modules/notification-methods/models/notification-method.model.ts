export enum NotificationMethodService {
  INTERNAL = 'INTERNAL',
  EMAIL = 'EMAIL',
  SMS = 'SMS',
  PUSH_NOTIFICAITONS = 'PUSH_NOTIFICATIONS',
  FACEBOOK_MESSENGER = 'FACEBOOK_MESSENGER',
  WHATSAPP = 'WHATSAPP'
}

export class NotificationMethodModel {
  id: number;
  name: string;
  description: string;
  service: NotificationMethodService;
  configs: {};
  updated_at: string;
  created_at: string;
}
