import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormNotificationsMethodsComponent } from './pages/form-notifications-methods/form-notifications-methods.component';
import { ListNotificationsMethodsComponent } from './pages/list-notifications-methods/list-notifications-methods.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListNotificationsMethodsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'notifications:notification-methods:read'}
  },
  {
    path: 'create',
    component: FormNotificationsMethodsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'notifications:notification-methods:create' }
  },
  {
    path: 'edit/:id',
    component: FormNotificationsMethodsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'notifications:notification-methods:update' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationMethodsRoutingModule { }
