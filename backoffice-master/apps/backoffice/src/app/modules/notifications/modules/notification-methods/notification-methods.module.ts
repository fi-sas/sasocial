import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationMethodsRoutingModule } from './notification-methods-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListNotificationsMethodsComponent } from './pages/list-notifications-methods/list-notifications-methods.component';
import { FormNotificationsMethodsComponent } from './pages/form-notifications-methods/form-notifications-methods.component';
import { NotificationsMethodsService } from './services/notifications-methods.service';
import { ViewNotificationMethodComponent } from './pages/view-notification-method/view-notification-method.component';

@NgModule({
  declarations: [
    ListNotificationsMethodsComponent,
    FormNotificationsMethodsComponent,
    ViewNotificationMethodComponent
  ],
  imports: [
    CommonModule,
    NotificationMethodsRoutingModule,
    SharedModule
  ],
  providers: [
    NotificationsMethodsService
  ]
})
export class NotificationMethodsModule { }
