import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNotificationsMethodsComponent } from './form-notifications-methods.component';

describe('FormNotificationsMethodsComponent', () => {
  let component: FormNotificationsMethodsComponent;
  let fixture: ComponentFixture<FormNotificationsMethodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNotificationsMethodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNotificationsMethodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
