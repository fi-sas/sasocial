
import { Component, OnInit, Input } from '@angular/core';
import { NotificationsMethodsService } from '../../services/notifications-methods.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-notifications-methods',
  templateUrl: './form-notifications-methods.component.html',
  styleUrls: ['./form-notifications-methods.component.less']
})
export class FormNotificationsMethodsComponent implements OnInit {
  idtoUpdate = null;
  loading = false;

  services = [
    { value: 'EMAIL', label: 'Email', active: true },
    { value: 'FACEBOOK_MESSENGER', label: 'Facebook Messenger', active: false },
    { value: 'INTERNAL', label: 'Interno', active: false },
    { value: 'PUSH_NOTIFICATIONS', label: 'Notificações Push (Firebase)', active: true },
    { value: 'SMS', label: 'SMS', active: true },
    { value: 'WHATSAPP', label: 'WhatsApp', active: false }
  ];

  methodGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    description: new FormControl('', [Validators.required,trimValidation]),
    service: new FormControl(null, [Validators.required]),
    configs: new FormControl({}, [Validators.required]),
  });

  constructor(
    private uiService: UiService,
    public router: Router,
    public activateRoute: ActivatedRoute,
    public notificationsMethodsService: NotificationsMethodsService
  ) { }

  ngOnInit() {
    this.load();
  }

  load() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idtoUpdate = data.get('id');
        this.notificationsMethodsService.read(this.idtoUpdate).subscribe(result => {
          this.methodGroup.patchValue({
            name: result.data[0].name,
            description: result.data[0].description,
            service: result.data[0].service,
            configs: result.data[0].configs
          });
        });
      }
    });
  }

  submitForm(event: any, value: any) {

    for (const i in this.methodGroup.controls) {
      if (i) {
        this.methodGroup.controls[i].markAsDirty();
        this.methodGroup.controls[i].updateValueAndValidity();
      }
    }

    if (this.methodGroup.valid) {
      this.loading = true;

      if (this.idtoUpdate) {
        this.notificationsMethodsService.update(this.idtoUpdate, value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['notifications', 'notifications-methods', 'list']);
            this.uiService.showMessage(MessageType.success, 'Método de notificação alterado com sucesso');
          });
      } else {
        this.notificationsMethodsService.create(value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['notifications', 'notifications-methods', 'list']);
            this.uiService.showMessage(MessageType.success, 'Método de notificação criado com sucesso');
          });
      }
    }

  }

  returnButton() {
 this.router.navigate(['notifications', 'notifications-methods', 'list']);
  }
}
