import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNotificationsMethodsComponent } from './list-notifications-methods.component';

describe('ListNotificationsMethodsComponent', () => {
  let component: ListNotificationsMethodsComponent;
  let fixture: ComponentFixture<ListNotificationsMethodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListNotificationsMethodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNotificationsMethodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
