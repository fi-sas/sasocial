import { Component, OnInit } from '@angular/core';
import { NotificationsMethodsService } from '../../services/notifications-methods.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-list-notifications-methods',
  templateUrl: './list-notifications-methods.component.html',
  styleUrls: ['./list-notifications-methods.component.less']
})
export class ListNotificationsMethodsComponent  extends TableHelper  implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private notificationsMethodsService: NotificationsMethodsService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
      key: 'name',
      label: 'Nome',
      sortable: true
    },
      {
      key: 'description',
      label: 'Descrição',
      sortable: true
    },
    {
      key: '',
      label: 'Criado em',
      template: (data) => {
        return moment(new Date(data.created_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    },
    {
      key: '',
      label: 'Alterado em',
      template: (data) => {
        return moment(new Date(data.updated_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    },
      {
      key: 'service',
      label: 'Serviço',
      sortable: true,
      tag: {
        INTERNAL: { label: 'Interno', color: 'gray'},
        EMAIL: { label: 'Email', color: 'blue'},
        SMS: { label: 'SMS', color: 'blue'},
        PUSH_NOTIFICATIONS: { label: 'Push Notifications', color: 'blue'},
        FACEBOOK_MESSENGER: { label: 'Facebook Messenger', color: 'blue'},
        WHATSAPP: { label: 'WhatsApp', color: 'blue'}
      }
    },
    )

    this.initTableData(this.notificationsMethodsService);
  }

  deleteNotificationMethod(id: number) {
    this.uiService.showConfirm('Eliminar método de notificação', 'Pretende eliminar este método de notificação', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.notificationsMethodsService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Método de notificação eliminado com sucesso');
        });
      }
    });
  }
}
