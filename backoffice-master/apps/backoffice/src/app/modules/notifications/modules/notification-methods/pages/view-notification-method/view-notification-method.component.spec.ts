import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewNotificationMethodComponent } from './view-notification-method.component';

describe('ViewNotificationMethodComponent', () => {
  let component: ViewNotificationMethodComponent;
  let fixture: ComponentFixture<ViewNotificationMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewNotificationMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewNotificationMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
