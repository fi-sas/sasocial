import { Component, OnInit, Input } from '@angular/core';
import { NotificationMethodModel } from '@fi-sas/backoffice/modules/notifications/modules/notification-methods/models/notification-method.model';

@Component({
  selector: 'fi-sas-view-notification-method',
  templateUrl: './view-notification-method.component.html',
  styleUrls: ['./view-notification-method.component.less']
})
export class ViewNotificationMethodComponent implements OnInit {

  @Input() data: NotificationMethodModel = null;

  constructor() { }

  ngOnInit() {
  }

}
