import { TestBed } from '@angular/core/testing';

import { NotificationsMethodsService } from './notifications-methods.service';

describe('NotificationsMethodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotificationsMethodsService = TestBed.get(NotificationsMethodsService);
    expect(service).toBeTruthy();
  });
});
