import { NotificationMethodModel } from '../models/notification-method.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationsMethodsService extends Repository<NotificationMethodModel>{

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'NOTIFICATIONS.NOTIFICATIONS_METHODS';
    this.entity_url = 'NOTIFICATIONS.NOTIFICATIONS_METHODS_ID';
  }
}
