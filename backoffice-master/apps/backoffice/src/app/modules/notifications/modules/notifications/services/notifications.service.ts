import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { NotificationModel } from '../models/notification.model';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService extends Repository<NotificationModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'NOTIFICATIONS.NOTIFICATIONS';
    this.entity_url = 'NOTIFICATIONS.NOTIFICATIONS_ID';
  }

  getUserNotifications(
    pageIndex: number,
    pageSize: number
  ): Observable<Resource<NotificationModel>> {
    return this.resourceService
      .list<NotificationModel>(
        this.urlService.get('NOTIFICATIONS.NOTIFICATIONS_INTERNAL', { }),
        {
          params: this.getQuery(
            pageIndex,
            pageSize
          ),
        }
      )
      .pipe(first());
  }
}
