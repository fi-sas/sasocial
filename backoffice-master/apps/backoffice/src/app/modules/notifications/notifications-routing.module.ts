import { PageNotFoundComponent } from './../../components/page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsComponent } from './notifications.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationsComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'alerts',
        loadChildren: './modules/alerts/alerts.module#AlertsModule',
        data: { breadcrumb: 'Alertas', title: 'Alertas', scope: 'notifications:alerts' },
      },
      {
        path: 'alert-types',
        loadChildren: './modules/alert-types/alert-types.module#AlertTypesModule',
        data: { breadcrumb: 'Tipos de Alertas', title: 'Tipos de Alertas', scope: 'notifications:alert-types' },
      },
      {
        path: 'alert-templates',
        loadChildren: './modules/alert-templates/alert-templates.module#AlertTemplatesModule',
        data: { breadcrumb: 'Modelos de Alertas', title: 'Modelos de Alertas', scope: 'notifications:alert-templates' },
      },
      {
        path: 'notifications-methods',
        loadChildren: './modules/notification-methods/notification-methods.module#NotificationMethodsModule',
        data: { breadcrumb: 'Métodos de Notificação', title: 'Métodos de Notificação', scope: 'notifications:notification-methods' },
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
