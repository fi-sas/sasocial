import { SiderItem } from './../../core/models/sider-item';
import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-notifications',
  template: '<router-outlet></router-outlet>'
})
export class NotificationsComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService,
    private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(12), 'message');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const alertTypes = new SiderItem('Tipos de Alertas', '', '', 'notifications:alert-types');
    alertTypes.addChild(new SiderItem('Criar', '', '/notifications/alert-types/create','notifications:alert-types:create'));
    alertTypes.addChild(new SiderItem('Listar', '', '/notifications/alert-types/list', 'notifications:alert-types:read'));
    this.uiService.addSiderItem(alertTypes);

    const alertTemplate = new SiderItem('Modelos de Alertas', '', '', 'notifications:alert-templates');
    alertTemplate.addChild(new SiderItem('Criar', '', '/notifications/alert-templates/create', 'notifications:alert-templates:create'));
    alertTemplate.addChild(new SiderItem('Listar', '', '/notifications/alert-templates/list','notifications:alert-templates:read'));
    this.uiService.addSiderItem(alertTemplate);


    const notificationMethod = new SiderItem('Métodos de Notificações', '', '', 'notifications:notification-methods');
    notificationMethod.addChild(new SiderItem('Criar', '', '/notifications/notifications-methods/create', 'notifications:notification-methods:create'));
    notificationMethod.addChild(new SiderItem('Listar', '', '/notifications/notifications-methods/list','notifications:notification-methods:read'));
    this.uiService.addSiderItem(notificationMethod);


    const alerts = new SiderItem('Alertas', '', '', 'notifications:alerts');
    alerts.addChild(new SiderItem('Listar', '', '/notifications/alerts/list','notifications:alerts:read'));
    this.uiService.addSiderItem(alerts);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
