export enum WantedAdsStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected'
}

export enum FurnishedStatus {
  Yes = 'Y',
  No = 'N',
  Indeferent = 'I'
}

export class WantedAdsModel {
  id?: number;
  author_name: string;
  author_nationality: string;
  author_phone: string;
  author_email: string;
  typology_id: number;
  max_price: number;
  furnished: string;
  zone: string;
  description: string;
  author_consent: true;
  start_date: string;
  end_date: string;
  status: string;
  updated_at?: string;
  created_at?: string;
  user_id?: number;
  history?: HistoryModel;
  typology?: TypologyModel;
}

export class TypologyModel {
  id?: number;
  name: string;
  updated_at: string;
  created_at: string;
}

export class HistoryModel {
  status: string;
  user_id: number;
  notes: string;
  updated_at: string;
  created_at: string;
}

export class WantedAdsRejectAndAprove {
  notes: string;
}



