import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComplaintsComponent } from './pages/list-complaints/list-complaints.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListComplaintsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'private_accommodation:complaints:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplaintsRoutingModule { }
