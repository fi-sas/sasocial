import { SharedModule } from '../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplaintsRoutingModule } from './complaints-routing.module';
import { ListComplaintsComponent } from './pages/list-complaints/list-complaints.component';
import { ViewComplaintComponent } from './pages/view-complaint/view-complaint.component';
import { PaSharedModule } from '../pa-shared/pa-shared.module';

@NgModule({
  declarations: [
    ListComplaintsComponent,
    ViewComplaintComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PaSharedModule,
    ComplaintsRoutingModule
  ]
})
export class ComplaintsModule { }
