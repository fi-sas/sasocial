import { ListingModel } from '../../listings/models/listing.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export enum ComplainStatus {
  New = 'New',
  Closed = 'Closed',
}

export class ComplaintModel {
  id?: number;
  description: string;
  listing_id: number;
  user_id: number;
  status: string;
  updated_at?: string;
  created_at?: string;
  listing: ListingModel;
  response: ResponseModel;
  user?: UserModel;
}

export class ResponseModel {
  id?: number;
  complaint_id?: number;
  description: string;
  user_id?: number;
  user?: UserModel;
  updated_at?: string;
  created_at?: string;
}

export const ComplaintTagStatus = {
  New: { color: 'blue', label: 'Nova' },
  Closed: { color: 'black', label: 'Fechada' },
};
