import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplaintsService } from '../../services/complaints.service';
import { first, finalize } from 'rxjs/operators';
import {
  ComplaintTagStatus,
  ComplaintModel,
} from '../../models/complaint.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-complaints',
  templateUrl: './list-complaints.component.html',
  styleUrls: ['./list-complaints.component.less'],
})
export class ListComplaintsComponent extends TableHelper implements OnInit {
  users: UserModel[] = [];
  users_loading = false;

  // MODAL RESPONSE
  isResponseModalVisible = false;
  responseModalComplaint: ComplaintModel = null;
  responseModalDescription = '';
  responseModalLoading = false;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public complaintsService: ComplaintsService,
    private usersService: UsersService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'user.name',
        label: 'Utilizador',
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: ComplaintTagStatus,
        sortable: true,
      }
    );

    this.persistentFilters['withRelated'] = 'response,user,listing';

    this.initTableData(this.complaintsService);
  }

  deleteComplaint(id: number) {
    if (!this.authService.hasPermission('private_accommodation:complaints:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar reclamação',
        'Pretende eliminar esta reclamação ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.complaintsService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Reclamação eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  responseModal(disabled: boolean, complaint: ComplaintModel) {
    if (!this.authService.hasPermission('private_accommodation:complaints:send-response')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    if (!disabled) {
      this.isResponseModalVisible = true;
      this.responseModalComplaint = complaint;
    }
  }

  handleResponseCancel() {
    this.isResponseModalVisible = false;
    this.responseModalComplaint = null;
    this.responseModalDescription = '';
  }

  handleResponseOk() {
    let response: any = {
      description: this.responseModalDescription,
    };

    this.responseModalLoading = true;

    this.complaintsService
      .response(this.responseModalComplaint.id, response)
      .pipe(
        first(),
        finalize(() => (this.responseModalLoading = false))
      )
      .subscribe(() => {
        this.searchData();
        this.handleResponseCancel();
      });
  }

  responseEmpty(response: object) {
    return Object.keys(response).length === 0;
  }

  listComplete() {
    this.filters.user_id = '';
    this.searchData(true);
  }
}
