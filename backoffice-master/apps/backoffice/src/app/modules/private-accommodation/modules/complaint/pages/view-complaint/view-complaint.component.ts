import { ListingsService } from '../../../listings/services/listings.service';
import { ComplaintModel } from '../../models/complaint.model';
import { Component, OnInit, Input } from '@angular/core';
import { ListingModel } from '../../../listings/models/listing.model';
import { first, finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-view-complaint',
  templateUrl: './view-complaint.component.html',
  styleUrls: ['./view-complaint.component.less'],
})
export class ViewComplaintComponent implements OnInit {
  @Input() complaint: ComplaintModel = null;

  listing: ListingModel = null;
  listing_loading = false;

  constructor(private listingsService: ListingsService) {}

  ngOnInit() {}

  loadListingByID() {
    if (this.listing === null) {
      this.listing_loading = true;
      this.listingsService
        .read(this.complaint.listing_id, {
          withRelated:
            'amenities,expenses,typology,history,translations,medias,owner',
        })
        .pipe(
          first(),
          finalize(() => (this.listing_loading = false))
        )
        .subscribe((results) => {
          this.listing = results.data[0];
        });
    }
  }

  responseEmpty(response: object) {
    return Object.keys(response).length === 0;
  }
}
