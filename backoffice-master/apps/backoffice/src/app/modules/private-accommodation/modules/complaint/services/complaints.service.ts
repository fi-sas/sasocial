import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ComplaintModel, ResponseModel } from '../models/complaint.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ComplaintsService extends Repository<ComplaintModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PRIVATE_ACCOMMODATION.COMPLAINTS';
    this.entity_url = 'PRIVATE_ACCOMMODATION.COMPLAINTS_ID';
  }

  response(
    complaint_id: number,
    response: ResponseModel
  ): Observable<Resource<ComplaintModel>> {
    const id = complaint_id;
    return this.resourceService.create<ComplaintModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.COMPLAINTS_RESPONSE', { id }),
      response
    );
  }
}
