import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepFiveComponent } from './listing-step-five.component';

describe('ListingStepFiveComponent', () => {
  let component: ListingStepFiveComponent;
  let fixture: ComponentFixture<ListingStepFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
