import { ConfigurationsService } from '@fi-sas/backoffice/modules/private-accommodation/modules/pa-configurations/services/configurations.service';
import { Gender, Occupation } from './../../models/listing.model';
import { RequirementFormModel } from './../../models/listing-form.model';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { first, finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-listing-step-five',
  templateUrl: './listing-step-five.component.html',
  styleUrls: ['./listing-step-five.component.less'],
})
export class ListingStepFiveComponent implements OnInit {
  @Input() requirements: RequirementFormModel = null;

  genders = Object.values(Gender);
  occupations = Object.values(Occupation);
  submitted: boolean = false;

  maxPublishDays = 1;
  maxPublishDay_loading = false;

  requirementForm = new FormGroup({
    allows_smokers: new FormControl(null, Validators.required),
    allows_pets: new FormControl(null, Validators.required),
    gender: new FormControl(null, Validators.required),
    occupation: new FormControl(null, Validators.required),
    start_date: new FormControl(null, Validators.required),
    end_date: new FormControl(null, Validators.required),
  });

  constructor(private configurationsService: ConfigurationsService) {}

  ngOnInit() {
    this.getMaxPublishDays();
    if (this.requirements !== null) {
      this.requirementForm.patchValue({
        ...this.requirements,
      });
    }
  }

  get f() { return this.requirementForm.controls; }

  submitForm(): any {
    this.submitted = true;
    if (this.requirementForm.valid) {
      this.submitted = false;
      this.requirementForm.updateValueAndValidity();
      this.requirementForm.value.start_date = moment(this.requirementForm.value.start_date).toISOString();
      this.requirementForm.value.end_date = moment(this.requirementForm.value.end_date).toISOString();
      return this.requirementForm.value;
    }
    for (const i in this.requirementForm.controls) {
      if (this.requirementForm.controls[i]) {
        this.requirementForm.controls[i].markAsDirty();
        this.requirementForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

  clearEndDate(){
    this.requirementForm.get("end_date").setValue(null);
}

  getMaxPublishDays() {
    this.maxPublishDay_loading = true;
    this.configurationsService
      .maxPublishDay()
      .pipe(
        first(),
        finalize(() => (this.maxPublishDay_loading = false))
      )
      .subscribe((results) => {
        this.maxPublishDays = results.data[0];
      });
  }

  disableStartDate = (current: Date) => {
    return moment(current).subtract(-1, 'days').isBefore(new Date());
  };

  disableEndDate = (current: Date) => {
    if (this.requirementForm.value.start_date !== null) {
      return !moment(current).isBetween(
        moment(this.requirementForm.value.start_date),
        moment(this.requirementForm.value.start_date).add(
          this.maxPublishDays,
          'days'
        )
      );
    } else {
      return moment(current).isAfter(
        moment(new Date()).add(this.maxPublishDays, 'days')
      );
    }
  };
}
