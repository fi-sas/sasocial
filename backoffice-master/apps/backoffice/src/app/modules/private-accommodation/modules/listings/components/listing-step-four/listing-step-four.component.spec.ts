import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepFourComponent } from './listing-step-four.component';

describe('ListingStepFourComponent', () => {
  let component: ListingStepFourComponent;
  let fixture: ComponentFixture<ListingStepFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
