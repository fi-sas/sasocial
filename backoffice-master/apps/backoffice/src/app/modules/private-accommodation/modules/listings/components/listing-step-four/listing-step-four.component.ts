import { ExpensesFormModel } from './../../models/listing-form.model';
import { Component, OnInit, Input } from '@angular/core';
import { Expenses } from '../../models/listing.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'fi-sas-listing-step-four',
  templateUrl: './listing-step-four.component.html',
  styleUrls: ['./listing-step-four.component.less'],
})
export class ListingStepFourComponent implements OnInit {
  @Input() expenses: ExpensesFormModel = null;

  expensesList = Object.values(Expenses);

  expensesForm = new FormGroup({
    water: new FormControl(false),
    gas: new FormControl(false),
    condo: new FormControl(false),
    electricity: new FormControl(false),
    cable_tv: new FormControl(false),
    internet: new FormControl(false),
    furniture: new FormControl(false),
    cleaning: new FormControl(false),
    bedding: new FormControl(false),
  });

  constructor() {}

  ngOnInit() {
    if (this.expenses !== null) {
      this.expensesForm.patchValue({
        ...this.expenses,
      });
    }
  }

  submitForm(): any {
    if (this.expensesForm.valid) {
      this.expensesForm.updateValueAndValidity();
      return this.expensesForm.value;
    }
    for (const i in this.expensesForm.controls) {
      if (this.expensesForm.controls[i]) {
        this.expensesForm.controls[i].markAsDirty();
        this.expensesForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }
}
