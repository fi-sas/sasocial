import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepOneComponent } from './listing-step-one.component';

describe('ListingStepOneComponent', () => {
  let component: ListingStepOneComponent;
  let fixture: ComponentFixture<ListingStepOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
