import { hasOwnProperty } from 'tslint/lib/utils';
import { OwnerService } from './../../../owners/services/owner.service';
import { PropertiesFormModel } from './../../models/listing-form.model';
import { HouseTypology, UsableSpace } from './../../models/listing.model';
import { TypologyModel } from './../../../pa-typologies/models/typology.model';
import { Component, OnInit, Input, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import { TypologiesService } from '../../../pa-typologies/services/typologies.service';
import { OwnerModel } from '../../../owners/models/owner.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import * as _ from 'lodash';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';

@Component({
  selector: 'fi-sas-listing-step-one',
  templateUrl: './listing-step-one.component.html',
  styleUrls: ['./listing-step-one.component.less'],
})
export class ListingStepOneComponent implements OnInit {
  @ViewChild('translationTabsTitle',null)
  translationTabsTitle: NzTabSetComponent;

  @ViewChild('translationTabsWhole',null)
  translationTabsWhole: NzTabSetComponent;

  @ViewChildren('tabRoomTrans') tabRoomTrans:QueryList<NzTabSetComponent>;

  houseTypologies = Object.values(HouseTypology);
  usableSpaces = Object.values(UsableSpace);
  errorRoom: boolean = false;
  @Input() properties: PropertiesFormModel = null;

  typologies: TypologyModel[] = [];
  typologies_loading = false;
  submitted: boolean = false;

  owners: OwnerModel[] = [];
  owners_loading = false;

  languages: LanguageModel[] = [];
  languages_loading = false;

  propertyForm = new FormGroup({
    owner_id: new FormControl(null, [Validators.required]),
    usable_space: new FormControl(null, [Validators.required]),
    typology_id: new FormControl(null, [Validators.required]),
    house_typology: new FormControl(null, [Validators.required]),
    price: new FormControl(null),
    area: new FormControl(null),
    floor: new FormControl(null, [Validators.required]),
    translations: new FormArray([]),
    rooms: new FormArray([]),
  });

  translations = this.propertyForm.get('translations') as FormArray;
  rooms = this.propertyForm.get('rooms') as FormArray;

  listOfSelectedLanguages = [];
  listOfSelectedLanguagesRooms = [];

  constructor(
    private typologiesService: TypologiesService,
    private ownerService: OwnerService,
    private languagesService: LanguagesService
  ) {
    this.loadTypologies();
    this.loadOwners();
    this.loadLanguages();
  }

  ngOnInit() {
    if (this.properties !== null) {
      this.propertyForm.patchValue({
        ...this.properties,
      });
      if (this.properties.usable_space == "SHARED_ROOM") {
        this.listOfSelectedLanguagesRooms = [];
        this.properties.rooms.map(room => {
          this.addNewRoom(room)
        })
      }
    }
  }

  get f() { return this.propertyForm.controls; }

  changeUsableSpace(event) {
    let add: FormArray;
    add = this.propertyForm.get('translations') as FormArray;
    if (event == 'WHOLE_HOUSE') {
      this.propertyForm.get('area').setValidators(Validators.required);
      this.propertyForm.get('price').setValidators(Validators.required);
      add.controls.forEach(control => {
        control.get('description').setValidators([Validators.required,trimValidation]);
      });
      this.rooms.controls = [];
      this.errorRoom = false;
    } else {
      this.listOfSelectedLanguagesRooms = [];
      add.controls.forEach(control => {
        control.get('description').clearValidators();
        control.get('description').setValue('');
      });
      this.propertyForm.get('area').setValue(null);
      this.propertyForm.get('price').setValue(null);
      this.propertyForm.get('area').setValidators(null);
      this.propertyForm.get('price').setValidators(null);
    }
    add.controls.forEach(control => {
      control.get('description').updateValueAndValidity();
    });
    this.propertyForm.get('area').updateValueAndValidity();
    this.propertyForm.get('price').updateValueAndValidity();

  }

  addNewRoom(room?: any) {
    this.errorRoom = false;
    const rooms = this.propertyForm.controls.rooms as FormArray;
    rooms.push(
      new FormGroup({
        price: new FormControl(room ? room.price : '', Validators.required),
        room_type: new FormControl(room ? room.room_type : '', Validators.required),
        shared_wc: new FormControl(room ? room.shared_wc : false, Validators.required),
        translations: new FormArray([], Validators.required),
        area: new FormControl(room ? room.area : '', Validators.required),
      })
    );
    if (room == undefined) {
      this.startTranslationRooms(rooms.length - 1, true);
    } else {
      this.startTranslationRooms(rooms.length - 1, false);
    }

  }

  removeRoom(i: number) {
    this.rooms.removeAt(i);
  }

  submitForm(): any {
    this.submitted = true;
    this.errorRoom = false;
    this.translationValid();
    if (this.propertyForm.get('usable_space').value == 'SHARED_ROOM') {
      if (this.propertyForm.get('rooms').value.length == 0) {
        this.errorRoom = true;
      } else {
        this.errorRoom = false;
      }
    } 

    if (this.propertyForm.valid && !this.errorRoom) {
      this.submitted = false;
      this.propertyForm.updateValueAndValidity();
      if (this.propertyForm.get('usable_space').value == 'WHOLE_HOUSE' && this.listOfSelectedLanguages.length !== 0) {
        return this.propertyForm.value;
      }
      if (this.propertyForm.get('usable_space').value == 'SHARED_ROOM' && this.listOfSelectedLanguagesRooms.length !== 0) {
        return this.propertyForm.value;
      }
    }
    for (const i in this.propertyForm.controls) {
      if (this.propertyForm.controls[i]) {
        this.propertyForm.controls[i].markAsDirty();
        this.propertyForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

  translationValid() {

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabsTitle.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    let tabIndexTranWhole = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          if(this.translationTabsWhole) {
            this.translationTabsWhole.nzSelectedIndex = tabIndexTranWhole;
            break;
          }
          
        }
      }
      tabIndexTranWhole++;
    }

   let tabIndexTranRoom = 0;
    for (const t of this.rooms.controls) {
      const trans = t.get('translations');
      if (t) {
        const tt = trans as FormArray;
        if (!tt.valid) {
          for(let translation of tt.controls) {
            if(!translation.valid) {
              this.tabRoomTrans.toArray()[this.rooms.controls.indexOf(t)].nzSelectedIndex = tabIndexTranRoom;
            }
            tabIndexTranRoom++;
          }
        }
      }
     
    }
  }

  getArrayValues(): {
    typologies: TypologyModel[];
    owners: OwnerModel[];
    languages: LanguageModel[];
  } {
    return {
      typologies: this.typologies,
      owners: this.owners,
      languages: this.languages,
    };
  }

  loadTypologies() {
    this.typologies_loading = true;
    this.typologiesService
      .list(1, -1, null, null, {
        sort: 'name'
      })
      .pipe(
        first(),
        finalize(() => (this.typologies_loading = false))
      )
      .subscribe((results) => {
        this.typologies = results.data;
      });
  }

  loadOwners() {
    this.owners_loading = true;
    this.ownerService
      .list(1, -1, null, null, { withRelated: 'user' })
      .pipe(
        first(),
        finalize(() => (this.owners_loading = false))
      )
      .subscribe((results) => {
        this.owners = results.data;
      });
  }

  loadLanguages() {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.listOfSelectedLanguages = [];
        this.startTranslation(this.properties.translations ? this.properties : '');
      });

  }

  startTranslation(value) {
    if (value !== '') {
        value.translations.map((translation) => {
          this.addTranslation(
            translation.language_id,
            translation.description,
            translation.name,
          );
          this.listOfSelectedLanguages.push(translation.language_id);
        });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '','');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  startTranslationRooms(index: number, newRoom: boolean) {
    this.listOfSelectedLanguagesRooms[index] = [];
    if (this.properties !== null && !_.isEmpty(this.properties) && newRoom == false) {

      if (this.properties.usable_space == "SHARED_ROOM") {
        this.properties.rooms[index].translations.map((translation) => {
          this.addTranslationRooms(
            index,
            translation.language_id,
            translation.description
          );
          this.listOfSelectedLanguagesRooms[index].push(translation.language_id);
        });
      } else {
        if (hasOwnProperty(this.languages[0], 'id')) {
          this.addTranslationRooms(index, this.languages[0].id, '');
          this.listOfSelectedLanguagesRooms[index].push(this.languages[0].id);
        }
      }

    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslationRooms(index, this.languages[0].id, '');
        this.listOfSelectedLanguagesRooms[index].push(this.languages[0].id);
      }
    }
  }


  getDescriptionTranslation(index: number): FormArray {
    const rooms = this.propertyForm.controls.rooms as FormArray;
    let aux = rooms.at(index) as FormGroup;
    return aux.controls.translations as FormArray;
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  addTranslation(language_id: number, description?: string, name?:string) {
    const translations = this.propertyForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        description: new FormControl(description),
        name: new FormControl(name, [Validators.required, trimValidation]),
      })
    );
  }

  addTranslationRooms(index: number, language_id: number, description?: string) {
    const translations = this.getDescriptionTranslation(index);
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        description: new FormControl(description, [Validators.required, trimValidation]),
      })
    );
  }


  changeLanguage() {
    const translations = this.propertyForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);

    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        '',''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }



  changeLanguageRooms(index: number) {
    const translations = this.getDescriptionTranslation(index);
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);

    if (this.listOfSelectedLanguagesRooms[index].length > languagesIDS.length) {
      this.addTranslationRooms(index,
        _.difference(this.listOfSelectedLanguagesRooms[index], languagesIDS)[0],
        ''
      );
    } else {
      this.getDescriptionTranslation(index).removeAt(
        this.getDescriptionTranslation(index).value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguagesRooms[index])[0]
        )
      );
    }
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

}
