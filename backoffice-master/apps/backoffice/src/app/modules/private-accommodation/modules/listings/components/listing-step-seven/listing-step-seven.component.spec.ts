import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepSevenComponent } from './listing-step-seven.component';

describe('ListingStepSevenComponent', () => {
  let component: ListingStepSevenComponent;
  let fixture: ComponentFixture<ListingStepSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
