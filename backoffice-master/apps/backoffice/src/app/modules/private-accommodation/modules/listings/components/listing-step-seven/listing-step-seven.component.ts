import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { hasOwnProperty } from 'tslint/lib/utils';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {
  Expenses,
  Amenities,
  HouseTypology,
  UsableSpace,
  Gender,
  Occupation,
} from '../../models/listing.model';
import { TypologyModel } from '../../../pa-typologies/models/typology.model';
import { OwnerModel } from '../../../owners/models/owner.model';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-listing-step-seven',
  templateUrl: './listing-step-seven.component.html',
  styleUrls: ['./listing-step-seven.component.less'],
})
export class ListingStepSevenComponent implements OnInit {
  expensesList = Object.values(Expenses);
  amenitiesList = Object.values(Amenities);
  houseTypologies = HouseTypology;
  usableSpaces = UsableSpace;
  genders = Gender;
  occupations = Occupation;

  medias = [];

  @Input() steps = [];
  @Input() arrayValues: {
    typologies: TypologyModel[];
    owners: OwnerModel[];
    languages: LanguageModel[];
  } = null;

  @Output() changeStep: EventEmitter<number> = new EventEmitter<number>();

  loadingFiles = false;

  constructor(private filesService: FilesService) {}

  ngOnInit() {
    this.loadMedias();
  }

  findOwners(id: number) {
    if (this.arrayValues !== null) {
      let ownerFound = null;
      ownerFound = this.arrayValues.owners.find((value) => value.id === id);

      if (ownerFound !== undefined && hasOwnProperty(ownerFound, 'user')) {
        return ownerFound.user.name;
      }
    }
    return id;
  }

  findTypology(id: number) {
    let typologyFound = null;
    if (this.arrayValues !== null) {
      typologyFound = this.arrayValues.typologies.find(
        (value) => value.id === id
      );
      if (
        typologyFound !== undefined &&
        hasOwnProperty(typologyFound, 'name')
      ) {
        return typologyFound.name;
      }
    }
    return id;
  }

  findLanguages(id: number) {
    let languageFound = null;
    if (this.arrayValues !== null) {
      languageFound = this.arrayValues.languages.find(
        (value) => value.id === id
      );
      if (
        languageFound !== undefined &&
        hasOwnProperty(languageFound, 'acronym')
      ) {
        return languageFound.acronym;
      }
    }
    return id;
  }

  loadMedias() {
    this.filesService
      .listFilesByIDS(this.convertGalleryToFilesIDS())
      .pipe(first())
      .subscribe(
        (files) => {
          this.medias = files.data;
          this.loadingFiles = false;
        },
        () => {
          this.loadingFiles = false;
        }
      );
  }

  convertGalleryToFilesIDS() {
    let filesIDS = [];
    this.steps[5].formValues.medias.forEach((fileID) => {
      filesIDS.push(fileID.file_id);
    });
    return filesIDS;
  }

  changeValue(step) {
    this.changeStep.emit(step);
  }
}
