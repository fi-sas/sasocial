import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepSixComponent } from './listing-step-six.component';

describe('ListingStepSixComponent', () => {
  let component: ListingStepSixComponent;
  let fixture: ComponentFixture<ListingStepSixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepSixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
