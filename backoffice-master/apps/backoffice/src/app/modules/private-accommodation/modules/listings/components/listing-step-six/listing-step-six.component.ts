import { Gallery } from './../../models/listing-form.model';
import { Component, OnInit, Input } from '@angular/core';
import { ConfigurationsService } from '../../../pa-configurations/services/configurations.service';
import { first, finalize } from 'rxjs/operators';
import * as _ from "lodash";

@Component({
  selector: 'fi-sas-listing-step-six',
  templateUrl: './listing-step-six.component.html',
  styleUrls: ['./listing-step-six.component.less'],
})
export class ListingStepSixComponent implements OnInit {
  @Input() gallery: Gallery = null;

  fileList = [];
  maxMedias_loading = false;
  submitted: boolean = false;

  // Gallery Options
  listType = 'picture-card';
  multiUploads = true;
  maxMedias = 0;
  buttonName = 'Carregar Imagem'

  constructor(private configurationsService: ConfigurationsService) {}

  ngOnInit() {
    this.getMaxMedias();
    if (this.gallery !== null && !_.isEmpty(this.gallery)) {
      this.fileList = this.convertGalleryToFilesIDS();
    }
  }

  onFileAdded(fileId: number) {
    this.submitted = false;
    this.fileList.push(fileId);
  }

  onFileDeleted(fileId: number) {
    this.fileList = this.fileList.filter((f) => f !== fileId);
  }

  getMaxMedias() {
    this.maxMedias_loading = true;
    this.configurationsService.maxMedias().pipe(
      first(),
      finalize(() => this.maxMedias_loading = false)
    ).subscribe(results => {
      this.maxMedias = results.data[0];
    });
  }

  submitForm(): any {
    this.submitted = true;
    if (this.fileList.length !== 0) {
      this.submitted = false;
      this.gallery.medias = [];
      this.fileList.forEach((file) => {
        this.gallery.medias.push({ file_id: file });
      });

      return this.gallery;
    }
    return null;
  }

  convertGalleryToFilesIDS() {
    let filesIDS = [];
    this.gallery.medias.forEach((fileID) => {
      filesIDS.push(fileID.file_id)
    });
    return filesIDS;
  };
}
