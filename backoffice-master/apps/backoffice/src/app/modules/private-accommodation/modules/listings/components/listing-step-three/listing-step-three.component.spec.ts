import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepThreeComponent } from './listing-step-three.component';

describe('ListingStepThreeComponent', () => {
  let component: ListingStepThreeComponent;
  let fixture: ComponentFixture<ListingStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
