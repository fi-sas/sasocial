import { Component, OnInit, Input } from '@angular/core';
import { AmenitiesFormModel } from '../../models/listing-form.model';
import { FormGroup, FormControl } from '@angular/forms';
import { Amenities } from '../../models/listing.model';

@Component({
  selector: 'fi-sas-listing-step-three',
  templateUrl: './listing-step-three.component.html',
  styleUrls: ['./listing-step-three.component.less'],
})
export class ListingStepThreeComponent implements OnInit {
  @Input() amenities: AmenitiesFormModel = null;

  amenitiesList = Object.values(Amenities);

  amenitiesForm = new FormGroup({
    kitchen_access: new FormControl(false),
    microwave: new FormControl(false),
    stove: new FormControl(false),
    fridge: new FormControl(false),
    dishwasher: new FormControl(false),
    washing_machine: new FormControl(false),
    elevator: new FormControl(false),
    heating_system: new FormControl(false),
    cooling_system: new FormControl(false),
    tv: new FormControl(false),
    cable_tv: new FormControl(false),
    internet: new FormControl(false),
    parking_garage: new FormControl(false),
    private_wc: new FormControl(false),
  });

  constructor() {}

  ngOnInit() {
    if (this.amenities !== null) {
      this.amenitiesForm.patchValue({
        ...this.amenities,
      });
    }
  }

  submitForm(): any {
    if (this.amenitiesForm.valid) {
      this.amenitiesForm.updateValueAndValidity();
      return this.amenitiesForm.value;
    }
    for (const i in this.amenitiesForm.controls) {
      if (this.amenitiesForm.controls[i]) {
        this.amenitiesForm.controls[i].markAsDirty();
        this.amenitiesForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }
}
