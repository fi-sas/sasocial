import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingStepTwoComponent } from './listing-step-two.component';

describe('ListingStepTwoComponent', () => {
  let component: ListingStepTwoComponent;
  let fixture: ComponentFixture<ListingStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingStepTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
