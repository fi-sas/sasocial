import { LocationFormModel } from './../../models/listing-form.model';
import { Component, OnInit, Input } from '@angular/core';
import { cities } from '../../../../../../shared/common/cities';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { proj } from 'openlayers';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-listing-step-two',
  templateUrl: './listing-step-two.component.html',
  styleUrls: ['./listing-step-two.component.less'],
})
export class ListingStepTwoComponent implements OnInit {
  @Input() location: LocationFormModel = null;

  cities = cities;
  submitted: boolean = false;
  latitude: number = 39.3999;
  longitude: number = -8.2245;

  latitudeInit: number = 39.3999;
  longitudeInit: number = -8.2245;

  locationForm = new FormGroup({
    address: new FormControl(null, [Validators.required, trimValidation]),
    address_no: new FormControl(null, [Validators.required, trimValidation]),
    postal_code: new FormControl(null, [Validators.required]),
    city: new FormControl(null, [Validators.required]),
    longitude: new FormControl(null),
    latitude: new FormControl(null),
  });

  constructor() {

  }

  ngOnInit() {
    if (this.location !== null) {
      this.locationForm.patchValue({
        ...this.location,
      });
      if (this.locationForm.get('latitude').value != null) {
        this.latitudeInit = this.locationForm.get('latitude').value;
        this.latitude = this.locationForm.get('latitude').value;
      } else {
        this.latitude = 39.3999;
        this.latitudeInit = 39.3999;
      }

      if (this.locationForm.get('longitude').value != null) {
        this.longitudeInit = this.locationForm.get('longitude').value;
        this.longitude = this.locationForm.get('longitude').value;
      } else {
        this.longitudeInit = -8.2245;
        this.longitude = -8.2245;
      }
    }

    this.cities = this.cities.sort((a, b) => {
      if (a.city > b.city) {
        return 1;
      }
      if (a.city < b.city) {
        return -1;
      }
      return 0;

    });
  }

  get f() { return this.locationForm.controls; }


  changeCity(event) {
    const findCity = this.cities.find((city) => city.city == event);
    if (findCity) {
      this.longitude = Number(findCity.lng);
      this.latitude = Number(findCity.lat);
      this.latitudeInit = Number(findCity.lat);
      this.longitudeInit = Number(findCity.lng);
      this.locationForm.get('latitude').setValue(this.latitude);
      this.locationForm.get('longitude').setValue(this.longitude);
    }
  }

  getValueFormat(value) {
    return Number(value).toFixed(6);
  }

  getLatLong(event) {
    var lonlat = proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
    this.longitude = lonlat[0];
    this.latitude = lonlat[1];
    this.locationForm.get('latitude').setValue(this.latitude);
    this.locationForm.get('longitude').setValue(this.longitude);
  }

  submitForm(): any {
    this.submitted = true;
    if (this.locationForm.valid) {
      this.submitted = false;
      this.locationForm.updateValueAndValidity();
      return this.locationForm.value;
    }
    for (const i in this.locationForm.controls) {
      if (this.locationForm.controls[i]) {
        this.locationForm.controls[i].markAsDirty();
        this.locationForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }
}
