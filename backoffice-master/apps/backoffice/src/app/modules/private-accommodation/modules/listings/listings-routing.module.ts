import { FormListingComponent } from './pages/form-listing/form-listing.component';
import { ListListingComponent } from './pages/list-listing/list-listing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
    {
    path: 'create',
    component: FormListingComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'private_accommodation:listings:create' },
  },
  {
    path: 'edit/:id',
    component:FormListingComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'private_accommodation:listings:update' },
  },
  {
    path: 'list',
    component: ListListingComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'private_accommodation:listings:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingsRoutingModule { }
