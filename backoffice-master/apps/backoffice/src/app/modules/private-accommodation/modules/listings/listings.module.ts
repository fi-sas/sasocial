import { PaSharedModule } from './../pa-shared/pa-shared.module';
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListingsRoutingModule } from './listings-routing.module';
import { ListListingComponent } from './pages/list-listing/list-listing.component';
import { FormListingComponent } from './pages/form-listing/form-listing.component';
import { ListingStepOneComponent } from './components/listing-step-one/listing-step-one.component';
import { ListingStepTwoComponent } from './components/listing-step-two/listing-step-two.component';
import { ListingStepThreeComponent } from './components/listing-step-three/listing-step-three.component';
import { ListingStepFourComponent } from './components/listing-step-four/listing-step-four.component';
import { ListingStepFiveComponent } from './components/listing-step-five/listing-step-five.component';
import { ListingStepSixComponent } from './components/listing-step-six/listing-step-six.component';
import { ListingStepSevenComponent } from './components/listing-step-seven/listing-step-seven.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';

@NgModule({
  declarations: [
    ListListingComponent,
    FormListingComponent,
    ListingStepOneComponent,
    ListingStepTwoComponent,
    ListingStepThreeComponent,
    ListingStepFourComponent,
    ListingStepFiveComponent,
    ListingStepSixComponent,
    ListingStepSevenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PaSharedModule,
    ListingsRoutingModule,
    AngularOpenlayersModule
  ]
})
export class ListingsModule { }
