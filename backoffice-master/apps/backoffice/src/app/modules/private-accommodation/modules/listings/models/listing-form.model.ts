import { TranslationModel } from "./listing.model";

export class PropertiesFormModel {
  price: number;
  house_typology: string;
  usable_space: string;
  area: string;
  floor: string;
  typology_id: number;
  owner_id: number;
  translations: TranslationModel[];
  rooms: RoomModel[];
  rooms_number: number;
}

export class LocationFormModel {
  address: string;
  address_no: string;
  postal_code: string;
  city: string;
  longitude: string;
  latitude: string;
}

export class AmenitiesFormModel {
  kitchen_access: boolean;
  microwave: boolean;
  stove: boolean;
  fridge: boolean;
  dishwasher: boolean;
  washing_machine: boolean;
  elevator: boolean;
  heating_system: boolean;
  cooling_system: boolean;
  tv: boolean;
  cable_tv: boolean;
  internet: boolean;
  parking_garage: boolean;
  private_wc: boolean;
}

export class ExpensesFormModel {
  water: boolean;
  gas: boolean;
  condo: boolean;
  electricity: boolean;
  cable_tv: boolean;
  internet: boolean;
  cleaning: boolean;
  furniture: boolean;
  bedding: boolean;
}

export class RequirementFormModel {
  allows_smokers: boolean;
  allows_pets: boolean;
  gender: string;
  occupation: string;
  start_date: string;
  end_date: string;
}

export class Gallery {
  medias: FileID[];
}

class FileID {
  file_id: number;
}

export class RoomModel {
  price: string;
  room_type: string;
  shared_wc: boolean;
  translations: TranslationModel[];
  area: string;
  active: boolean;
}