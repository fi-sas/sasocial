import { FileModel } from './../../../../medias/models/file.model';
import { OwnerModel } from "../../owners/models/owner.model";
import { TypologyModel } from "../../pa-typologies/models/typology.model";

export class ListingModel {
  id?: number;
  typology_id: number;
  area: number;
  floor: number;
  price: number;
  longitude: number;
  latitude: number;
  address: string;
  address_no: string;
  postal_code: string;
  city: string;
  country: string;
  allows_smokers: boolean;
  allows_pets: boolean;
  gender: string;
  occupation: string;
  house_typology: string;
  usable_space: string;
  start_date: string;
  end_date: string;
  visualizations?: string;
  owner_id: number;
  status: string;
  user_id?: number;
  updated_at?: string;
  created_at?: string;
  amenities? : AmenitiesModel;
  expenses? : ExpensesModel;
  translations?: TranslationModel[];
  medias?: MediaModel[];
  typology : TypologyModel;
  history?: HistoryModel[];
  owner?: OwnerModel;
  rooms: RoomModel[];
  rooms_number: number;
}

export class AmenitiesModel {
  kitchen_access: boolean;
  microwave: boolean;
  stove: boolean;
  fridge: boolean;
  dishwasher: boolean;
  washing_machine: boolean;
  elevator: boolean;
  heating_system: boolean;
  cooling_system: boolean;
  tv: boolean;
  cable_tv: boolean;
  internet: boolean;
  parking_garage: boolean;
  private_wc: boolean;
}

export class ExpensesModel {
  water: boolean;
  gas: boolean;
  condo: boolean;
  electricity: boolean;
  cable_tv: boolean;
  internet: boolean;
  cleaning: boolean;
  furniture: boolean;
  bedding: boolean;
}

export class TranslationModel {
  language_id: number;
  description: string;
}

class MediaModel {
  file_id: number;
  file?: FileModel;
  updated_at?: string;
  created_at?: string;
}

class HistoryModel {
  start_date: string;
  end_date: string;
  status: string;
  user_id: number;
  notes: string;
  updated_at: string;
  created_at: string;
}

export class ListingRejectAndApprove {
  notes: string;
}


export class RoomModel {
  price: string;
  room_type: string;
  shared_wc: boolean;
  translations: TranslationModel[];
  area: string;
}

export const StatusListing = [
  { value: 'Pending', label: 'Em Espera' },
  { value: 'Approved', label: 'Aprovado' },
  { value: 'Rejected', label: 'Rejeitado' },
];

export const statusMachines = {
  Pending: {
    Approve: { label: 'Aprovar'},
    Reject: { label: 'Rejeitar'},
  },
  Approved: {},
  Rejected: {},
}

export const ListingTagStatus = {
  Pending: { color: 'blue', label: 'Em Espera' },
  Approved: { color: 'green', label: 'Aprovado' },
  Rejected: { color: 'red', label: 'Rejeitado' }
};

export const HouseTypology = {
  HOUSE: { value: 'HOUSE', label: 'Casa' },
  APARTMENT: { value: 'APARTMENT', label: 'Apartamento' },
}

export const UsableSpace = {
  WHOLE_HOUSE: { value: 'WHOLE_HOUSE', label: ' Habitação inteira' },
  SHARED_ROOM: { value: 'SHARED_ROOM', label: 'Quarto' },
}

export const Gender = {
  M: { value: 'M', label: 'Homens' },
  F: { value: 'F', label: 'Mulheres' },
  I: { value: 'I', label: 'Misto' },
}

export const Occupation = {
  N: { value: 'N', label: 'Estudantes Nacionais' },
  F: { value: 'F', label: 'Erasmus' },
  I: { value: 'I', label: 'Indeferenciado' }
}

export const Amenities = {
  kitchen_access: { value: 'kitchen_access', label: 'Acesso à cozinha' },
  microwave: { value: 'microwave', label: 'Micro-ondas' },
  stove: { value: 'stove', label: 'Fogão' },
  fridge: { value: 'fridge', label: 'Frigorífico' },
  dishwasher: { value: 'dishwasher', label: 'Máquina lavar loiça' },
  washing_machine: { value: 'washing_machine', label: 'Máquina lavar roupa' },
  elevator: { value: 'elevator', label: 'Elevador' },
  heating_system: {  value: 'heating_system', label: 'Sistema de aquecimento'},
  cooling_system: { value: 'cooling_system', label: 'Sistema de refrigeração'},
  cable_tv: { value: 'cable_tv', label: 'Televisão por cabo'},
  internet: { value: 'internet', label: 'Internet'},
  parking_garage: { value: 'parking_garage', label: 'Garagem'},
  private_wc: { value: 'private_wc', label: 'Quarto de banho privado'},
  tv: { value: 'tv', label: 'Televisão'}
}

export const Expenses = {
  water: { value: 'water', label: 'Água' },
  gas: { value: 'gas', label: 'Gás' },
  condo: { value: 'condo', label: 'Condomínio' },
  electricity: { value: 'electricity', label: 'Electricidade' },
  cable_tv: { value: 'cable_tv', label: 'Televisão por cabo' },
  internet: { value: 'internet', label: 'Internet' },
  cleaning: { value: 'cleaning', label: 'Roupa da cama' },
  furniture: { value: 'furniture', label: 'Mobiliário' },
  bedding: { value: 'bedding', label: 'Limpeza' },}


