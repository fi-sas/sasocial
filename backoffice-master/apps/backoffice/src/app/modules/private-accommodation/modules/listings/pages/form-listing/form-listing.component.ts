import {
  PropertiesFormModel,
  LocationFormModel,
  AmenitiesFormModel,
  ExpensesFormModel,
  RequirementFormModel,
  Gallery,
} from './../../models/listing-form.model';
import { ListingModel } from './../../models/listing.model';
import { ListingsService } from './../../services/listings.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TypologyModel } from '../../../pa-typologies/models/typology.model';
import { OwnerModel } from '../../../owners/models/owner.model';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { Location } from '@angular/common';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
@Component({
  selector: 'fi-sas-form-listing',
  templateUrl: './form-listing.component.html',
  styleUrls: ['./form-listing.component.less'],
})
export class FormListingComponent implements OnInit {
  step = 0;
  loading = false;
  buttonActionName = 'Seguinte';

  idToUpdate = null;

  loadingListing = false;

  arrayValues: {
    typologies: TypologyModel[];
    owners: OwnerModel[];
    languages: LanguageModel[];
  } = null;

  @ViewChild('propertyForm', null) propertyForm: PropertiesFormModel;
  @ViewChild('locationForm', null) locationForm: LocationFormModel;
  @ViewChild('amenitiesForm', null) amenitiesForm: AmenitiesFormModel;
  @ViewChild('expensesForm', null) expensesForm: ExpensesFormModel;
  @ViewChild('requirementsForm', null) requirementsForm: RequirementFormModel;
  @ViewChild('galleryForm', null) galleryForm: Gallery;

  steps = [
    {
      name: 'propertyForm',
      formValues: {},
    },
    {
      name: 'locationForm',
      formValues: {},
    },
    {
      name: 'amenitiesForm',
      formValues: {},
    },
    {
      name: 'expensesForm',
      formValues: {},
    },
    {
      name: 'requirementsForm',
      formValues: {},
    },
    {
      name: 'galleryForm',
      formValues: {},
    },
  ];

  constructor(
    private listingsService: ListingsService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activateRoute: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.loadingListing = true;

        this.idToUpdate = data.get('id');
        this.listingsService
          .read(parseInt(data.get('id'), 10), {
            withRelated: 'medias,translations,amenities,expenses,rooms',
          })
          .subscribe(
            (result) => {
              this.steps[0].formValues = this.getData(result.data[0], [
                'price',
                'house_typology',
                'usable_space',
                'area',
                'floor',
                'typology_id',
                'translations',
                'owner_id',
                'rooms'
              ]);
              this.steps[1].formValues = this.getData(result.data[0], [
                'address',
                'address_no',
                'postal_code',
                'city',
                'longitude',
                'latitude',
              ]);
              this.steps[2].formValues = result.data[0].amenities;
              this.steps[3].formValues = result.data[0].expenses;
              this.steps[4].formValues = this.getData(result.data[0], [
                'allows_smokers',
                'allows_pets',
                'gender',
                'occupation',
                'start_date',
                'end_date',
              ]);
              this.steps[5].formValues['medias'] = result.data[0].medias;
              this.loadingListing = false;
            },
            () => {
              this.loadingListing = false;
              this.location.back();
            }
          );
      }
    });
  }

  getData(raw: object, fields: string[]) {
    const data = {};
    fields.forEach((key) => (data[key] = raw[key]));
    return data;
  }

  nextStep(): void {
    if (this.step < this.steps.length) {
      if (this[this.steps[this.step].name].submitForm() !== null) {
        if (this.step === 0) {
          this.arrayValues = this[this.steps[this.step].name].getArrayValues();
        }
        this.steps[this.step].formValues = this[
          this.steps[this.step].name
        ].submitForm();
        this.step++;
        this.buttonActionName = this.step === 6 ? 'Submeter' : 'Seguinte';
      }
    } else {
      this.submitListing();
    }
  }

  previousStep(): void {
    if (this.step > 0) {
      this.step--;
      this.buttonActionName = this.step === 6 ? 'Submeter' : 'Seguinte';
    }
  }

  stepPosition(position: number) {
    this.step = position;
  }

  submitListing() {
    let listing = {} as ListingModel;

    this.loading = true;

    this.steps.forEach((step) => {
      switch (step.name) {
        case 'amenitiesForm':
          listing = Object.assign(listing, { amenities: step.formValues });
          break;
        case 'expensesForm':
          listing = Object.assign(listing, { expenses: step.formValues });
          break;
        default:
          listing = Object.assign(listing, step.formValues);
      }
    });
    if (listing.rooms.length > 0) {
      listing.rooms_number = listing.rooms.length;
    } else {
      listing.rooms_number = 0;
    }

    if (this.idToUpdate && this.authService.hasPermission('private_accommodation:listings:update')) {
      listing.status = 'Pending';
      this.listingsService
        .patch(this.idToUpdate, listing)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Habitação alterada com sucesso.'
          );
          this.location.back();
        });
    } else if (!this.idToUpdate && this.authService.hasPermission('private_accommodation:listings:create')) {
      this.listingsService
        .create(listing)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Habitação inserida com sucesso.'
          );
          this.router.navigate(['private-accommodation', 'listings', 'list']);
        });
    } else {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      this.loading = false;
    }
  }

  changeStep(event) {
    if(event>=0) {
      this.step = event;
      this.buttonActionName = 'Seguinte';
    }
  }
}
