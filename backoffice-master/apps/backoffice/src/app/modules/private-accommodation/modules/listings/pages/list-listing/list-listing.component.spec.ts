import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListListingComponent } from './list-listing.component';

describe('ListListingComponent', () => {
  let component: ListListingComponent;
  let fixture: ComponentFixture<ListListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
