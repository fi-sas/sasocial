import { OwnerService } from './../../../owners/services/owner.service';
import { OwnerModel } from './../../../owners/models/owner.model';
import { StatusListing, ListingModel, ListingTagStatus, statusMachines } from './../../models/listing.model';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ListingsService } from '../../services/listings.service';
import { first, finalize } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-listing',
  templateUrl: './list-listing.component.html',
  styleUrls: ['./list-listing.component.less']
})
export class ListListingComponent extends TableHelper implements OnInit {

  statusListing = StatusListing;

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalListing: ListingModel = null;
  changeStatusModalActions = {};
  changeStatusModalListingAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;

  owners: OwnerModel[] = []
  owners_loading = false;

  pageSize = 10;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public listingsService: ListingsService,
    private ownerService: OwnerService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'owner.user.name',
        label: 'Proprietário',
        sortable: false,
      },
      {
        key: 'typology.name',
        label: 'Tipologia',
        sortable: false,
      },
      {
        key: 'address',
        label: 'Morada',
        sortable: true,
      },
      {
        key: 'city',
        label: 'Cidade',
        sortable: true,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: ListingTagStatus,
        sortable: true,
      }
    );

    this.persistentFilters['withRelated'] = 'amenities,expenses,typology,history,translations,medias,owner,rooms';

    this.initTableData(this.listingsService);

    let statusPanel: string;
    statusPanel = localStorage.getItem('statusPropertyFilter');
    if(statusPanel != null){
      this.filters.status = statusPanel;
      this.searchData();
    }else{
      this.filters.status = null;
      this.filters.owner_id = null;
    }
    localStorage.removeItem('statusPropertyFilter');

    this.loadOwners();
  }

  loadOwners() {
    this.owners_loading = true;
    this.ownerService.list(1, -1, null, null, {withRelated: 'user'}).pipe(
      first(),
      finalize(() => this.owners_loading = false)
    ).subscribe(results => {
      this.owners = results.data;
    });
  }

  changeStateModal(listing: ListingModel, disabled: boolean) {
    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalListing = listing;
      this.changeStatusModalActions = statusMachines[listing.status];
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalListing = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalListingAction = null;
    this.changeStatusModalNotes = '';
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalListingAction = action;
  }

  handleChangeStatusOk() {
      let statusObj: any = {
        notes: this.changeStatusModalNotes,
      };

      this.changeStatusModalLoading = true;

      if (this.changeStatusModalListingAction === 'Approve') {
        if(!this.authService.hasPermission('private_accommodation:listings:status')) {
          this.uiService.showMessage(
            MessageType.warning,
            'O utilizador não têm acesso ao serviço solicitado'
          );
          this.changeStatusModalLoading = false;
          return;
        }

        this.listingsService
          .approve(this.changeStatusModalListing.id, statusObj)
          .pipe(
            first(),
            finalize(() => (this.changeStatusModalLoading = false))
          )
          .subscribe(() => {
            this.searchData();
            this.handleChangeStatusCancel();
          });
      } else if (this.changeStatusModalListingAction === 'Reject') {
        if(!this.authService.hasPermission('private_accommodation:listings:status')) {
          this.uiService.showMessage(
            MessageType.warning,
            'O utilizador não têm acesso ao serviço solicitado'
          );
          this.changeStatusModalLoading = false;
          return;
        }

        this.listingsService
          .reject(this.changeStatusModalListing.id, statusObj)
          .pipe(
            first(),
            finalize(() => (this.changeStatusModalLoading = false))
          )
          .subscribe(() => {
            this.searchData();
            this.handleChangeStatusCancel();
          });
      }
  }

  deleteListing(id: number) {
    if(!this.authService.hasPermission('private_accommodation:listings:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar habitação',
        'Pretende eliminar esta habitação ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.listingsService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Habitação eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.status = null;
    this.filters.owner_id = null;
    this.searchData(true);
  }
}
