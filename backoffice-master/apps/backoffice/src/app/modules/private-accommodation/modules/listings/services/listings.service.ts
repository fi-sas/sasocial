import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ListingModel, ListingRejectAndApprove } from '../models/listing.model';

@Injectable({
  providedIn: 'root',
})
export class ListingsService extends Repository<ListingModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PRIVATE_ACCOMMODATION.LISTINGS';
    this.entity_url = 'PRIVATE_ACCOMMODATION.LISTINGS_ID';
  }

  approve(
    id: number,
    listingNotes: ListingRejectAndApprove
  ): Observable<Resource<ListingModel>> {
    return this.resourceService.create<ListingModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_APPROVE', { id }),
      listingNotes
    );
  }

  reject(
    id: number,
    listingNotes: ListingRejectAndApprove
  ): Observable<Resource<ListingModel>> {
    return this.resourceService.create<ListingModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_REJECT', { id }),
      listingNotes
    );
  }
}
