export enum OwnerStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected',
}

export class OwnerApplicationsModel {
  id?: number;
  full_name: string;
  identification: string;
  email: string;
  password?: string;
  pin?: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  issues_receipt: boolean;
  description: string;
  applicant_consent: boolean;
  phone: string;
  status?: string;
  owner_id?: number;
  updated_at?: string;
  created_at?: string;
  history?: HistoryModel[] = [];
  owner: OwnerModel;
}

class HistoryModel {
  id: number;
  owner_application_id: number;
  status: string;
  user_id: number;
  notes: string;
  updated_at: string;
  created_at: string;
}

class OwnerModel {
  id: number;
  user_id: number;
  application?: OwnerApplicationsModel;
  updated_at: string;
  created_at: string;
}

export class OwnerRejectAndApprove {
  notes: string;
}
