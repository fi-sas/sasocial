import { ListingModel } from '../../listings/models/listing.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export enum OwnerStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected'
}

export class OwnerModel {
  id?: number;
  user_id: number;
  status: string;
  description: string;
  applicant_consent: boolean;
  active: boolean;
  issues_receipt: boolean;
  updated_at?: string;
  created_at?: string;
  user?: UserModel;
  listings?: ListingModel[];
}

export const StatusOwner = [
  { value: 'Pending', label: 'Em Espera' },
  { value: 'Approved', label: 'Aprovado' },
  { value: 'Rejected', label: 'Rejeitado' },
];

export const statusMachines = {
  Pending: {
    Approve: { label: 'Aprovar'},
    Reject: { label: 'Rejeitar'},
  },
  Approved: {},
  Rejected: {},
}

export const OwnerTagStatus = {
  Pending: { color: 'blue', label: 'Em Espera' },
  Approved: { color: 'green', label: 'Aprovado' },
  Rejected: { color: 'red', label: 'Rejeitado' }
};

export const Gender = {
  M: { value: 'M', label: 'Masculino' },
  F: { value: 'F', label: 'Feminina' },
  I: { value: 'U', label: 'Outro' },
}

