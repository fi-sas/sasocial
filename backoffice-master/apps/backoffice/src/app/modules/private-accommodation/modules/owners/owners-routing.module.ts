import { FormOwnerComponent } from './pages/form-owner/form-owner.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListOwnersComponent } from './pages/list-owners/list-owners.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormOwnerComponent ,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'private_accommodation:owners:create' },
  },
  {
    path: 'edit/:id',
    component: FormOwnerComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'private_accommodation:owners:update' },
  },
  {
    path: 'list',
    component: ListOwnersComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'private_accommodation:owners:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnersRoutingModule { }
