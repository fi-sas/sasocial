import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnersRoutingModule } from './owners-routing.module';
import { FormOwnerComponent } from './pages/form-owner/form-owner.component';
import { ListOwnersComponent } from './pages/list-owners/list-owners.component';
import { ViewOwnerComponent } from './pages/view-owner/view-owner.component';

@NgModule({
  declarations: [
    ListOwnersComponent,
    ViewOwnerComponent,
    FormOwnerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    OwnersRoutingModule
  ]
})
export class OwnersModule { }
