import { OwnerApplicationsService } from './../../services/owner-applications.service';
import { Component, OnInit } from '@angular/core';
import { OwnerApplicationsModel } from '../../models/owner-applications.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { countries } from '@fi-sas/backoffice/shared/common/countries';
import * as moment from 'moment';
import { Gender } from '../../models/owner.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';
import { DocumentTypeService } from '@fi-sas/backoffice/modules/users/modules/document-type/services/document-type.service';
import { nationalities } from '@fi-sas/backoffice/shared/common/nationalities';

@Component({
  selector: 'fi-sas-form-owner',
  templateUrl: './form-owner.component.html',
  styleUrls: ['./form-owner.component.less'],
})
export class FormOwnerComponent implements OnInit {
  owner: OwnerApplicationsModel = null;

  idToUpdate = null;
  documentTypes: DocumentTypeModel[] = [];
  ownerLoading = false;

  countries = countries;
  genders = Object.values(Gender);
  nationalities = nationalities;
  ownerApplicationForm = new FormGroup({
    name: new FormControl(null, [Validators.required,trimValidation]),
    tin: new FormControl(null, [Validators.required]),
    identification: new FormControl(null),
    document_type_id: new FormControl(null),
    gender: new FormControl(null),
    nationality: new FormControl(null),
    birth_date: new FormControl(null),
    email: new FormControl(null, [Validators.email, Validators.required,trimValidation]),
    phone: new FormControl(null, [Validators.required,trimValidation]),
    address: new FormControl(null, [Validators.required,trimValidation]),
    postal_code: new FormControl(null, [Validators.required]),
    city: new FormControl(null, [Validators.required,trimValidation]),
    country: new FormControl(null, [Validators.required]),
    issues_receipt: new FormControl(false, [Validators.required]),
    description: new FormControl(null, [Validators.required,trimValidation]),
    applicant_consent: new FormControl(true),
  });

  constructor(
    private ownerApplicationsService: OwnerApplicationsService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private documentTypeService: DocumentTypeService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.getDocumentTypes();
  }

  disableFutureDates = (current: Date) => {
    return moment(current).isAfter(new Date());
  };

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.ownerLoading = true;

      if (this.owner && this.authService.hasPermission('private_accommodation:owners:update')) {
        this.ownerApplicationsService
          .patch(this.idToUpdate, formValue)
          .pipe(
            first(),
            finalize(() => (this.ownerLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Proprietário alterado com sucesso.'
            );
            this.location.back();
          });
      } else if (!this.owner && this.authService.hasPermission('private_accommodation:owners:create')) {

        if (formValue.identification === '' || formValue.identification === null) {
          delete formValue.identification
        }

        if (formValue.tin === '' || formValue.tin === null) {
          delete formValue.tin
        }

        this.ownerApplicationsService
          .create(formValue)
          .pipe(
            first(),
            finalize(() => (this.ownerLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Proprietário inserido com sucesso.'
            );
            this.router.navigate([
              'private-accommodation',
              'owners',
              'list',
            ]);
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.ownerLoading = false;
      }
    } else {
      for (const i in this.ownerApplicationForm.controls) {
        if (this.ownerApplicationForm.controls[i]) {
          this.ownerApplicationForm.controls[i].markAsDirty();
          this.ownerApplicationForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  getDocumentTypes() {
    this.documentTypeService.list(1, -1, null, null, { active: true }).pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

  returnButton() {
    this.router.navigate([
      'private-accommodation',
      'owners',
      'list',
    ]);
  }
}
