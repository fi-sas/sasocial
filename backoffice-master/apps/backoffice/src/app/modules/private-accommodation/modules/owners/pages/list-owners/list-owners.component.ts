import { OwnerApplicationsService } from './../../services/owner-applications.service';
import {
  StatusOwner,
  OwnerModel,
  statusMachines,
  OwnerTagStatus,
} from './../../models/owner.model';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OwnerService } from '../../services/owner.service';
import { first, finalize } from 'rxjs/operators';
import { ChangeActiveModel } from '../../models/change-active.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { DocumentTypeService } from '@fi-sas/backoffice/modules/users/modules/document-type/services/document-type.service';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';

@Component({
  selector: 'fi-sas-list-owners',
  templateUrl: './list-owners.component.html',
  styleUrls: ['./list-owners.component.less'],
})
export class ListOwnersComponent extends TableHelper implements OnInit {

  statusOwner = StatusOwner;
  documentTypes: DocumentTypeModel[] = [];
  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalOwner: OwnerModel = null;
  changeStatusModalActions = {};
  changeStatusModalApplicationAction = null;
  changeStatusModalNotes = null;
  changeStatusModalLoading = false;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public ownerService: OwnerService,
    private documentTypeService: DocumentTypeService,
    private ownerApplicationsService: OwnerApplicationsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
    this.getDocumentsTypes();
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'user.name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: 'user.identification',
        label: 'Identificação',
        sortable: false,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: false,
      },
      {
        key: 'applicant_consent',
        label: 'Consentimento',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: OwnerTagStatus,
        sortable: true,
      }
    );

    this.persistentFilters['withRelated'] = 'user';

    this.initTableData(this.ownerService);

    let statusPanel: string;
    statusPanel = localStorage.getItem('statusOwnerFilter');
    if (statusPanel != null) {
      this.filters.status = statusPanel;
      this.searchData();
    } else {
      this.filters.status = null;
    }
    localStorage.removeItem('statusOwnerFilter');
  }

  changeActive(ownerID: number, active: boolean) {
    this.uiService
      .showConfirm(
        active ? 'Ativar' : 'Desativar',
        active
          ? 'Pretende ativar este proprietário?'
          : 'Pretende desativar este proprietário?',
        active ? 'Ativar' : 'Desativar'
      )
      .subscribe((result) => {
        if (result) {
          const notes = active ? 'Ativado' : 'Desativado';
          const changeActive: ChangeActiveModel = {
            active: active,
            notes: notes,
          };
          this.ownerService
            .changeActive(ownerID, changeActive)
            .pipe(first())
            .subscribe(() => {
              this.searchData();
            });
        }
      });
  }

  changeStateModal(owner: OwnerModel, disabled: boolean) {
    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalOwner = owner;
      this.changeStatusModalActions = statusMachines[owner.status];
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalOwner = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalApplicationAction = null;
    this.changeStatusModalNotes = '';
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalApplicationAction = action;
  }

  handleChangeStatusOk() {
    let statusObj: any = {
      notes: this.changeStatusModalNotes,
    };

    this.changeStatusModalLoading = true;

    if (this.changeStatusModalApplicationAction === 'Approve') {
      if (!this.authService.hasPermission('private_accommodation:owners:approve')) {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.changeStatusModalLoading = false;
        return;
      }

      this.ownerApplicationsService
        .approve(this.changeStatusModalOwner.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe(() => {
          this.searchData();
          this.handleChangeStatusCancel();
        });
    } else if (this.changeStatusModalApplicationAction === 'Reject') {
      if (!this.authService.hasPermission('private_accommodation:owners:reject')) {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.changeStatusModalLoading = false;
        return;
      }

      this.ownerApplicationsService
        .reject(this.changeStatusModalOwner.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe(() => {
          this.searchData();
          this.handleChangeStatusCancel();
        });
    }
  }

  listComplete() {
    this.filters.status = null;
    this.searchData(true);
  }

  deleteOwner(id: number) {
    if (!this.authService.hasPermission('private_accommodation:owners:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar proprietário',
        'Pretende eliminar este proprietário ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.ownerService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Proprietário eliminado com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  getDocumentsTypes() {
    this.documentTypeService.list(1, -1, null, null, { active: true }).pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

}
