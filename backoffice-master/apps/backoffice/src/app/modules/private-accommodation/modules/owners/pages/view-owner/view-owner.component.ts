import { OwnerModel } from './../../models/owner.model';
import { Component, OnInit, Input } from '@angular/core';
import { DocumentTypeModel } from '@fi-sas/backoffice/modules/users/modules/document-type/models/document-type.model';

@Component({
  selector: 'fi-sas-view-owner',
  templateUrl: './view-owner.component.html',
  styleUrls: ['./view-owner.component.less']
})
export class ViewOwnerComponent implements OnInit {

  @Input() owner: OwnerModel = null;
  @Input() documentType: DocumentTypeModel[] = [];

  constructor() { }

  ngOnInit() {}

  findDocument(id) {
    if(id) {
      const aux = this.documentType.find(data => data.id == id).translations;
      return aux ? (aux.find(tran => tran.language_id == 3).description) : '-';
    }
    return '-';
  }


  
}
