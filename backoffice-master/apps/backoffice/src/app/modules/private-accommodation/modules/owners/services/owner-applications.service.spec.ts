import { TestBed, inject } from '@angular/core/testing';

import { OwnerApplicationsService } from './owner-applications.service';

describe('OwnerApplicationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OwnerApplicationsService]
    });
  });

  it('should be created', inject([OwnerApplicationsService], (service: OwnerApplicationsService) => {
    expect(service).toBeTruthy();
  }));
});
