import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import {
  OwnerRejectAndApprove,
  OwnerApplicationsModel,
} from '../models/owner-applications.model';

@Injectable({
  providedIn: 'root',
})
export class OwnerApplicationsService extends Repository<
  OwnerApplicationsModel
> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PRIVATE_ACCOMMODATION.OWNERS_APPLICATIONS';
    this.entity_url = 'PRIVATE_ACCOMMODATION.OWNERS_APPLICATIONS_ID';
  }
  //WEMAKE: Deve existir scope??
  approve(
    id: number,
    ownerNotes: OwnerRejectAndApprove
  ): Observable<Resource<OwnerApplicationsModel>> {
    return this.resourceService.create<OwnerApplicationsModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS_APPLICATIONS_APPROVE', { id }),
      ownerNotes
    );
  }

  reject(
    id: number,
    ownerNotes: OwnerRejectAndApprove
  ): Observable<Resource<OwnerApplicationsModel>> {
    return this.resourceService.create<OwnerApplicationsModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS_APPLICATIONS_REJECT', { id }),
      ownerNotes
    );
  }
}
