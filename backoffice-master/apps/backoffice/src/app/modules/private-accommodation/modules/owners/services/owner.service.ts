import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { OwnerModel } from '../models/owner.model';
import { ChangeActiveModel } from '../models/change-active.model';

@Injectable({
  providedIn: 'root'
})
export class OwnerService extends Repository<OwnerModel> {

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'PRIVATE_ACCOMMODATION.OWNERS';
    this.entity_url = 'PRIVATE_ACCOMMODATION.OWNERS_ID';
  }

  changeActive(id: number, changeActive: ChangeActiveModel): Observable<Resource<any>> {
    return this.resourceService.create<any>(this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS_CHANGE_ACTIVE', {id}), changeActive);
  }
}
