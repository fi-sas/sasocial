export class ConfigurationModel {
  admin_notification_email: string;
  max_listing_publish_days: number;
  max_medias_per_listing: number;
  max_wantedads_publish_days: number;
  owner_profile_id: number;
  owner_regulation_file_id: number;
  owner_user_group_ids: number[];
}
