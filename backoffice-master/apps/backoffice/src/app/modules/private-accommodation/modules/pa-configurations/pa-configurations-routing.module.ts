import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'form', pathMatch: 'full' },
  {
    path: 'form',
    component: FormConfigurationsComponent,
    data: {
      breadcrumb: 'Configurações',
      title: 'Configurações',
      scope: 'private_accommodation:configurations:configure',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaConfigurationsRoutingModule { }
