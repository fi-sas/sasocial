import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaConfigurationsRoutingModule } from './pa-configurations-routing.module';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';

@NgModule({
  declarations: [
    FormConfigurationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PaConfigurationsRoutingModule
  ]
})
export class PaConfigurationsModule { }
