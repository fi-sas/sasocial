import { Component, OnInit } from '@angular/core';
import { ConfigurationModel } from '../../models/configurations.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ConfigurationsService } from '../../services/configurations.service';
import { finalize, first } from 'rxjs/operators';
import { UserGroupsService } from '@fi-sas/backoffice/modules/users/modules/users-groups/services/user-groups.service';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-form-configurations',
  templateUrl: './form-configurations.component.html',
  styleUrls: ['./form-configurations.component.less'],
})
export class FormConfigurationsComponent implements OnInit {
  profiles = [];
  user_groups = [];

  configuration: ConfigurationModel;
  configurationForm = new FormGroup({
    max_medias_per_listing: new FormControl(0, [Validators.required, Validators.min(1)]),
    max_listing_publish_days: new FormControl(0, [Validators.required, Validators.min(1)]),
    max_wantedads_publish_days: new FormControl(0, [Validators.required, Validators.min(1)]),
    admin_notification_email: new FormControl(null, [
      Validators.email,
      Validators.required
    ]),
    owner_profile_id: new FormControl(null, [Validators.required]),
    owner_user_group_ids: new FormControl(null, [Validators.required]),
    owner_regulation_file_id: new FormControl(null, [Validators.required]),
  });

  loading = false;

  constructor(
    private route: ActivatedRoute,
    private uiService: UiService,
    private configurationsService: ConfigurationsService,
    private userGroupsService: UserGroupsService,
    private profilesService: ProfilesService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.configurationsService.list(0, 1, null, null).pipe(first(), finalize(() => this.loading = false)).subscribe(configs => {
      if(configs.data.length){
        this.configurationForm.controls.max_medias_per_listing.patchValue(configs.data[0].max_medias_per_listing);
        this.configurationForm.controls.max_listing_publish_days.patchValue(configs.data[0].max_listing_publish_days);
        this.configurationForm.controls.max_wantedads_publish_days.patchValue(configs.data[0].max_wantedads_publish_days);
        this.configurationForm.controls.admin_notification_email.patchValue(configs.data[0].admin_notification_email ? JSON.parse(configs.data[0].admin_notification_email): null);
        this.configurationForm.controls.owner_profile_id.patchValue(configs.data[0].owner_profile_id);

        if(configs.data[0].owner_user_group_ids && JSON.parse(configs.data[0].owner_user_group_ids).length !== 0 ) {
          this.configurationForm.controls.owner_user_group_ids.patchValue(JSON.parse(configs.data[0].owner_user_group_ids));
        }
        this.configurationForm.controls.owner_regulation_file_id.patchValue(configs.data[0].owner_regulation_file_id);
      }
    });

    this.userGroupsService.list(1, -1, null, null, {
      sort: 'name', withRelated: false
    }).pipe(first()).subscribe(results => {
      this.user_groups = results.data;
    });
    this.profilesService.list(1, -1, null, null, {
      sort: 'name', withRelated: false
    }).pipe(first()).subscribe(results => {
      this.profiles = results.data;
    });
  }

  submitForm(value: any) {
    if (!this.authService.hasPermission('private_accommodation:configurations:configure')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }


    if (!this.configurationForm.valid) {
      this.updateValidity();
      return;
    }


    this.configurationForm.disable();
    this.loading = true;

    this.configurationsService.create(value).pipe(first(), finalize(() => {
      this.loading = false;
      this.configurationForm.enable();
    })).subscribe(result => {

      this.uiService.showMessage(
        MessageType.success,
        'Configuração guardada com sucesso'
      );
    });
  }

  updateValidity() {
    for (const key in this.configurationForm.controls) {
      this.configurationForm.controls[key].updateValueAndValidity()
    }
  }
}
