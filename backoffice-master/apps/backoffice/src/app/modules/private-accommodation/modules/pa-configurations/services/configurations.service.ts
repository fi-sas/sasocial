import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ConfigurationModel } from '../models/configurations.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { url } from 'inspector';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationsService extends Repository<any> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);

    this.entities_url = 'PRIVATE_ACCOMMODATION.CONFIGURATIONS';
    this.entity_url = 'PRIVATE_ACCOMMODATION.CONFIGURATIONS';
  }

  maxMedias(): Observable<Resource<number>> {
    return this.resourceService.read<number>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIGURATIONS_MAX_MEDIAS_PER_LISTING', {}), {});
  }

  maxPublishDay(): Observable<Resource<number>> {
    return this.resourceService.read<number>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIGURATIONS_MAX_LISTING_PUBLISH_DAYS', {}), {});
  }
}
