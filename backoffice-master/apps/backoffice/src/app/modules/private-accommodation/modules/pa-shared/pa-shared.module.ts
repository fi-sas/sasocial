import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewListingComponent } from './pages/view-listing/view-listing.component';

@NgModule({
  declarations: [
    ViewListingComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    ViewListingComponent,
  ]
})
export class PaSharedModule { }
