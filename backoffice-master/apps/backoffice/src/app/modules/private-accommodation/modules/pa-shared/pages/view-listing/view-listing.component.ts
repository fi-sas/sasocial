import { hasOwnProperty } from 'tslint/lib/utils';
import { Component, OnInit, Input } from '@angular/core';
import { ListingModel, UsableSpace, HouseTypology, Gender, Occupation } from '../../../listings/models/listing.model';

@Component({
  selector: 'fi-sas-view-listing',
  templateUrl: './view-listing.component.html',
  styleUrls: ['./view-listing.component.less']
})
export class ViewListingComponent implements OnInit {

  @Input() listing: ListingModel = null;

  enums = {
    USABLE_SPACE: UsableSpace,
    HOUSE_TYPOLOGY: HouseTypology,
    GENDER: Gender,
    OCCUPATION: Occupation
  };

  effect = 'scrollx';

  constructor() { }

  ngOnInit() {
  }

  filterEnum(setEnum: string, value: string): string {
    let enumSelected = this.enums[setEnum];
    if(enumSelected[value] != null){
      return ( hasOwnProperty(enumSelected[value], 'label') ?  enumSelected[value].label : '' );
    }else{
      return '';
    }
  }

}
