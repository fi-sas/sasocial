import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';
import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Criar Tipologia', title: 'Criar', scope: 'private_accommodation:typologies:create' },
  },
  {
    path: 'edit/:id',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Editar Tipologia', title: 'Editar', scope: 'private_accommodation:typologies:update' },
  },
  {
    path: 'list',
    component: ListTypologiesComponent,
    data: { breadcrumb: 'Listar Tipologias', title: 'Listar', scope: 'private_accommodation:typologies:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaTypologiesRoutingModule { }
