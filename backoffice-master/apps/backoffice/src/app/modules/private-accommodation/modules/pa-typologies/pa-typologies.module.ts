import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';
import { SharedModule } from '../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaTypologiesRoutingModule } from './pa-typologies-routing.module';

@NgModule({
  declarations: [
    FormTypologyComponent,
    ListTypologiesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PaTypologiesRoutingModule
  ]
})
export class PaTypologiesModule { }
