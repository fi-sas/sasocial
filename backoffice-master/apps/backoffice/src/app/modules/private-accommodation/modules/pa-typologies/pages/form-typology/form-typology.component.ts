import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TypologiesService } from '../../services/typologies.service';
import { first, finalize } from 'rxjs/operators';
import { Location } from '@angular/common';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-typology',
  templateUrl: './form-typology.component.html',
  styleUrls: ['./form-typology.component.less'],
})
export class FormTypologyComponent implements OnInit {
  typologyForm = new FormGroup({
    name: new FormControl('', [Validators.required,trimValidation]),
  });
  loading = false;
  idToUpdate = null;

  constructor(
    public typologiesService: TypologiesService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id')
      ? this.activatedRoute.snapshot.params.id
      : null;
    if (this.idToUpdate) {
      this.typologiesService
        .read(this.idToUpdate)
        .pipe(first())
        .subscribe((result) => {
          this.typologyForm.patchValue({
            name: result.data[0].name,
          });
        });
    }
  }

  submitForm(value: any) {
    if (this.typologyForm.valid) {
      this.loading = true;
      if (this.idToUpdate && this.authService.hasPermission('private_accommodation:typologies:update')) {
        this.typologiesService
          .update(this.idToUpdate, value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Tipologia atualizada com sucesso'
            );
            this.location.back();
          });
      } else if(!this.idToUpdate && this.authService.hasPermission('private_accommodation:typologies:create')) {
        this.typologiesService
          .create(value)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Tipologia inserida com sucesso'
            );
            this.router.navigate([
              'private-accommodation',
              'typologies',
              'list',
            ]);
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não têm acesso ao serviço solicitado'
        );
        this.loading = false;
      }
    } else {
      for (const i in this.typologyForm.controls) {
        if (this.typologyForm.controls[i]) {
          this.typologyForm.controls[i].markAsDirty();
          this.typologyForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton() {
    this.router.navigate([
      'private-accommodation',
      'typologies',
      'list',
    ]);
  }
}
