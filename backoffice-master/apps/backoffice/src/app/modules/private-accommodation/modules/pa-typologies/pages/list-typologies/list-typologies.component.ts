import { Component, OnInit } from '@angular/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TypologiesService } from '../../services/typologies.service';
import { first } from 'rxjs/operators';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-typologies',
  templateUrl: './list-typologies.component.html',
  styleUrls: ['./list-typologies.component.less']
})
export class ListTypologiesComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public typologiesService: TypologiesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
    );

    this.persistentFilters = {
      searchFields: 'name',
      sort:'name'
    };

    this.initTableData(this.typologiesService);
  }

  deleteTypology(id: number) {
    if(!this.authService.hasPermission('private_accommodation:typologies:delete')){
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar tipologia',
        'Pretende eliminar esta tipologia ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.typologiesService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Tipologia eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
    }

    listComplete() {
      this.filters.search = null;
      this.searchData(true);
    }
}
