import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { finalize, first } from "rxjs/operators";
import { ListingsService } from "../../modules/listings/services/listings.service";
import { OwnerService } from "../../modules/owners/services/owner.service";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-sas-dashboard-private-accommodation',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less']
})

export class DashboardPrivateAccommodationComponent implements OnInit {
    loadingStatsProperty = false;
    loadingStatsOwner = false;
    ownerPending: number;
    ownerRejected: number;
    ownerApproved: number;
    propertyPending: number;
    propertyRejected: number;
    propertyApproved: number;

    constructor(private ownerService: OwnerService, private listingsService: ListingsService, private router: Router, private authService: AuthService){

    }

    ngOnInit() {
        if(this.authService.hasPermission('private_accommodation:listings:read')){
            this.getProperties();
        }

        if(this.authService.hasPermission('private_accommodation:owners:read')){
            this.getOwners();
        }
    }

    getProperties() {
        this.loadingStatsProperty = true;
        this.listingsService.list(1, -1).pipe(
            first(), finalize(() => this.loadingStatsProperty = false)
        ).subscribe(results => {
            this.propertyPending = (results.data.filter((fil) => { return fil.status == 'Pending' })).length;
            this.propertyApproved = (results.data.filter((fil) => { return fil.status == 'Approved' })).length;
            this.propertyRejected = (results.data.filter((fil) => { return fil.status == 'Rejected' })).length;
        });
    }

    getOwners() {
        this.loadingStatsOwner = true;
        this.ownerService.list(1, -1).pipe(
            first(), finalize(() => this.loadingStatsOwner = false)
        ).subscribe(results => {
            this.ownerPending = (results.data.filter((fil) => { return fil.status == 'Pending' })).length;
            this.ownerApproved = (results.data.filter((fil) => { return fil.status == 'Approved' })).length;
            this.ownerRejected = (results.data.filter((fil) => { return fil.status == 'Rejected' })).length;
        });
    }

    detailProperty(status: string){
        if(!this.authService.hasPermission('private_accommodation:listings:read')) {
            return false;
        }

        localStorage.setItem('statusPropertyFilter', status);
        this.router.navigate(['/private-accommodation/listings/list']);
    }

    detailOwner(status: string){
        if(!this.authService.hasPermission('private_accommodation:owners:read')) {
            return false;
        }

        localStorage.setItem('statusOwnerFilter', status);
        this.router.navigate(['/private-accommodation/owners/list']);
    }
}