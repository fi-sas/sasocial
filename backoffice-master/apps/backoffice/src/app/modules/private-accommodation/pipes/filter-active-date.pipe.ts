import { Pipe, PipeTransform } from '@angular/core';
import * as dayjs from 'dayjs';

@Pipe({
  name: 'filterActiveDate'
})
export class FilterActiveDatePipe implements PipeTransform {

  transform(dateBegin: string, dateEnd: string): boolean {

      return dayjs(dateEnd).isBefore(dayjs(dateBegin));
  }

}
