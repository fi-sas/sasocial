import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterLang'
})
export class FilterLangPipe implements PipeTransform {

  transform(value: any, id : number): any {

    return value.find(lang => lang.id === id).acronym;
  }

}
