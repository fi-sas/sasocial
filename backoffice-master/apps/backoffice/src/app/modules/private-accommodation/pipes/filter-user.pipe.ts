import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterUser'
})
export class FilterUserPipe implements PipeTransform {

  transform(value: any, user_id: number): any {

    return value.find(user => user.id === user_id);
  }

}
