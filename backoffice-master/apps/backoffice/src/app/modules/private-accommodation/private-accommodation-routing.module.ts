import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { PrivateAccommodationComponent } from '@fi-sas/backoffice/modules/private-accommodation/private-accommodation.component';
import { DashboardPrivateAccommodationComponent } from './pages/dashboard/dashboard.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateAccommodationComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'typologies',
        loadChildren: './modules/pa-typologies/pa-typologies.module#PaTypologiesModule',
        data: { breadcrumb: 'Tipologias', title: 'Tipologias', scope: 'private_accommodation:typologies' },
      },
      {
        path: 'owners',
        loadChildren: './modules/owners/owners.module#OwnersModule',
        data: { breadcrumb: 'Proprietários', title: 'Proprietários', scope: 'private_accommodation:owners' },
      },
      {
        path: 'listings',
        loadChildren: './modules/listings/listings.module#ListingsModule',
        data: { breadcrumb: ' Habitações', title: 'Habitações', scope: 'private_accommodation:listings' }
      },
      {
        path: 'complaints',
        loadChildren: './modules/complaint/complaints.module#ComplaintsModule',
        data: { breadcrumb: ' Reclamações', title: 'Reclamações', scope: 'private_accommodation:complaints' }
      },
      {
        path: 'dashboard',
        component: DashboardPrivateAccommodationComponent,
        data: { breadcrumb: ' Painel', title: 'Painel' }
      },
      {
        path: 'configurations',
        loadChildren: './modules/pa-configurations/pa-configurations.module#PaConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações', scope: 'private_accommodation:configurations:configure' }
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateAccommodationsRoutingModule {
}
