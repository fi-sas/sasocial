import { Component, OnInit } from '@angular/core';
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { SiderItem } from "@fi-sas/backoffice/core/models/sider-item";
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-private-accommodation',
  template: '<router-outlet></router-outlet>'
})
export class PrivateAccommodationComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private authService: AuthService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(15), 'environment');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const dashboard = new SiderItem('Painel', '', '/private-accommodation/dashboard');
    this.uiService.addSiderItem(dashboard);

    const typologies = new SiderItem('Tipologias','','','private_accommodation:typologies');
    typologies.addChild(new SiderItem('Criar', '', '/private-accommodation/typologies/create', 'private_accommodation:typologies:create'));
    typologies.addChild(new SiderItem('Listar', '', '/private-accommodation/typologies/list', 'private_accommodation:typologies:read'));
    this.uiService.addSiderItem(typologies);

    const owners = new SiderItem('Proprietários','','','private_accommodation:owners');
    owners.addChild(new SiderItem('Criar', '', '/private-accommodation/owners/create', 'private_accommodation:owners:create'));
    owners.addChild(new SiderItem('Listar', '', '/private-accommodation/owners/list', 'private_accommodation:owners:read'));
    this.uiService.addSiderItem(owners);

    const listings = new SiderItem('Habitações','','','private_accommodation:listings');
    listings.addChild(new SiderItem('Criar', '', '/private-accommodation/listings/create', 'private_accommodation:listings:create'));
    listings.addChild(new SiderItem('Listar', '', '/private-accommodation/listings/list', 'private_accommodation:listings:read'));
    this.uiService.addSiderItem(listings);

    const complaints = new SiderItem('Reclamações','','','private_accommodation:complaints');
    complaints.addChild(new SiderItem('Listar', '', '/private-accommodation/complaints/list', 'private_accommodation:complaints:read'));
    this.uiService.addSiderItem(complaints);

    const configurations = new SiderItem('Configurações', '', '/private-accommodation/configurations/form','private_accommodation:configurations:configure');
    this.uiService.addSiderItem(configurations);

    this.uiService.setSiderActive(true);
  }
  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
