import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { PrivateAccommodationsRoutingModule } from '@fi-sas/backoffice/modules/private-accommodation/private-accommodation-routing.module';
import { PrivateAccommodationComponent } from './private-accommodation.component';
import { FilterLangPipe } from './pipes/filter-lang.pipe';
import { FilterUserPipe } from './pipes/filter-user.pipe';
import { WantedAdsService } from '@fi-sas/backoffice/modules/private-accommodation/services/wanted-ads.service';
import { FilterActiveDatePipe } from './pipes/filter-active-date.pipe';
import { ConfigurationsService } from '@fi-sas/backoffice/modules/private-accommodation/modules/pa-configurations/services/configurations.service';
import { DashboardPrivateAccommodationComponent } from './pages/dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PrivateAccommodationsRoutingModule
  ],
  declarations: [
    PrivateAccommodationComponent,
    FilterLangPipe,
    FilterUserPipe,
    FilterActiveDatePipe,
    DashboardPrivateAccommodationComponent
  ],
  providers: [
    WantedAdsService,
    ConfigurationsService,
  ],
})
export class PrivateAccommodationModule {}
