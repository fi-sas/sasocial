import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { WantedAdsModel, WantedAdsRejectAndAprove} from "@fi-sas/backoffice/modules/private-accommodation/models/wanted-ads.model";
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class WantedAdsService extends Repository<WantedAdsModel> {

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'PRIVATE_ACCOMMODATION.WANTED_ADS_ID';
    this.entities_url = 'PRIVATE_ACCOMMODATION.WANTED_ADS';
  }

  aprove(id: number, wantedAdsNotes: WantedAdsRejectAndAprove): Observable<Resource<WantedAdsModel>> {
    return this.resourceService.create<WantedAdsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.WANTED_ADS_APPROVE', {id}), wantedAdsNotes);
  }

  reject(id: number, wantedAdsNotes: WantedAdsRejectAndAprove): Observable<Resource<WantedAdsModel>> {
    return this.resourceService.create<WantedAdsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.WANTED_ADS_REJECT', {id}), wantedAdsNotes);
  }


}



