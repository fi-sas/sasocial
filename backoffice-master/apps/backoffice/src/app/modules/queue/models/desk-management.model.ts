import { deskModelQueue } from "./desk.model";
import { OperatorModelQueue } from "./operator.model";
import { ServiceModelQueue } from "./service.model";

export class DeskManagementResponseModelQueue{
    operator_id: number;
    operating: boolean;
    desk_id: number;
    desk: deskModelQueue[] = [];
    operator: OperatorModelQueue[] = [];
    service: ServiceModelQueue[] = [];
    tickets_waiting: number;
    media_time: number;
    photo: string;
  }
  
  