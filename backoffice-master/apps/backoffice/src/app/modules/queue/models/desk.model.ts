import { TranslationModel } from "../../configurations/models/translations.model";

export class deskModelQueue {
    id: number;
    name: string;
    actual_state: boolean;
    active: boolean;
    updated_at?: Date;
    created_at?: Date;
}

export class ticketsModelQueue {
    total_tickets: number;
    tickets_waiting: number;
    tickets_priority: number;
    tickets_appointment: number;
    subject: any;
    desks: string[];
    date: Date = new Date();
}