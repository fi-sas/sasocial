import { deskModelQueue } from "./desk.model";
import { OperatorModelQueue } from "./operator.model";
import { SubjectModelQueue } from "./subject.model";

export class subjectId {
    subject_id: number;
  }
  
export class GlobalConfigurationModelQueue {
    id:number;
    operator_id: number;
    desk_id: number;
    subjects: subjectId[] = [];
    inicial_date: Date;
    operating: boolean;
    subject_id: number;
    subject: SubjectModelQueue[];
}

export class GlobalConfigurationResponseModelQueue{
  subject_id: number;
  operator_id: number;
  operating: boolean;
  inicial_date: Date;
  id: number;
  final_date: Date;
  desk_id: number;
  desk: deskModelQueue[] = [];
  operator: OperatorModelQueue[] = [];
  subject: SubjectModelQueue[] = [];
}

