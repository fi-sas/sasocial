import { UserModel } from "../../users/modules/users_users/models/user.model";

export class OperatorModelQueue {
    user_id: number;
    show_photo: boolean;
    active: boolean;
    name: string;
    id: number;
    user: UserModel;
}