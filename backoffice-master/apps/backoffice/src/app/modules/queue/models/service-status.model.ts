import { ServiceModelQueue } from "./service.model";

export class ServiceStatusQueue {
    services_open: DetailServiceStatus;
    services_close: DetailServiceStatus;
}

export class DetailServiceStatus {
    total_number: number;
    services: ServiceModelQueue[] = [];
}