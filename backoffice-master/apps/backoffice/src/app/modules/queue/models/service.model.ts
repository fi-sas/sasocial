import { SubjectModelQueue } from "./subject.model";

export class ServiceModelQueue {
  id?: number;
  active: boolean;
  updated_at?: Date;
  created_at?: Date;
  name: string;
  subjects: SubjectModelQueue[] = [];
  translations: TranslationModel[];
  groups: any[];
}

export class TranslationModel {
    id: number;
    name: string;
    service_id: number;
    language_id: number;
    updated_at?: Date;
    created_at?: Date;
}
