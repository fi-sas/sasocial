import { ServiceModelQueue } from "./service.model";

export class SubjectModelQueue {
    id?: number;
    active: boolean;
    updated_at?: Date;
    created_at?: Date;
    name: string;
    code: string;
    schedule_description: string;
    avg_time: number;
    max_tolerance: number;
    appointments: boolean;
    appointments_interval: number;
    estimated_time: number;
    service_id?: number;
    service: ServiceModelQueue[] = [];
    schedule: ScheduleModelQueue[]=[];
    n_tickets: number;
    percentage: string;
    total: number;
    out_of_goal: number;
    translations: TranslationsModel[] = [];
  }

export class ScheduleModelQueue {
  description: string;
  week_day: number
  schedules: SchedulesModelQueue[] = []
}
  
export class SchedulesModelQueue {
  opening_hour: string;
  closing_hour: string;
}

export class TranslationsModel {
  id: number;
  name: string;
  subject_id: number;
  language_id: number;
  updated_at?: Date;
  created_at?: Date;
  schedule_description: string;
}
