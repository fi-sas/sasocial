export class TicketsReportModelQueue {
    total_ticket_digital: number;
    total_ticket_paper: number;
    total_number: number;
    average_recovered: string;
    total_number_open: number;
    total_number_close: number;
}