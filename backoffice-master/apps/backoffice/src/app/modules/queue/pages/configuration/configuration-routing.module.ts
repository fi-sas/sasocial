import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesListComponent } from './services/services-list/service-list.component';
import { ServicesFormComponent } from './services/service-form/service-form.component';
import { SubjectsListComponent } from './subjects/subjects-list/subjects-list.component';
import { SubjectFormComponent } from './subjects/subjects-form/subjects-form.component';

const routes: Routes = [
  { path: '', redirectTo: 'services', pathMatch: 'full' },
  {
    path: 'subjects',
    loadChildren: './subjects/subjects.module#SubjectsModule',
  },
  {
    path: 'services',
    loadChildren: './services/services.module#ServicesModule',
  },
  {
    path: 'desks',
    loadChildren: './desks/desks.module#DesksModule',
  },
  {
    path: 'global-configurations',
    loadChildren: './global-configuration/global-configuration.module#GlobalConfigurationModule',
  },
  {
    path: 'operators',
    loadChildren: './operator/operator.module#OperatorModule',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationQueueRoutingModule { }
