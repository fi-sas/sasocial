import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { ConfigurationQueueRoutingModule } from './configuration-routing.module';

import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';


@NgModule({
  declarations: [
   
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationQueueRoutingModule
  ],
  providers: [
    FilesService,
    ApplicationsService
  ],
})
export class ConfigurationQueueModule { }
