import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DesksFormComponent } from './desks-form.component';

describe('DesksFormComponent', () => {
  let component: DesksFormComponent;
  let fixture: ComponentFixture<DesksFormComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [DesksFormComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DesksFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
