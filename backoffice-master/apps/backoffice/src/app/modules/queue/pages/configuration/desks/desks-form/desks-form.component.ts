import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { DeskServiceQueue } from '@fi-sas/backoffice/modules/queue/services/desk.service';
import { ActivatedRoute, Router } from '@angular/router';
import { deskModelQueue } from '@fi-sas/backoffice/modules/queue/models/desk.model';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-desks-form-queue',
  templateUrl: './desks-form.component.html',
  styleUrls: ['./desks-form.component.less']
})
export class DesksFormComponent implements OnInit {

  validateLoading = false;
  eraseLoading = false;
  deleteLoading = false;
  createDeskForm: FormGroup;
  selectedEdit: any;
  edit = false;
  submitted = false;
  id: number;

  get f() { return this.createDeskForm.controls; }

  constructor(private fb: FormBuilder,
    private deskServiceQueue: DeskServiceQueue,
    private router: Router,
    private route: ActivatedRoute,
    private message: NzMessageService) {
    this.createDeskForm = fb.group({
      name: [null, [Validators.required, trimValidation]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.getDeskId(this.id);
    }
  }

  getDeskId(id) {
    this.edit = true;
    let desk: deskModelQueue = new deskModelQueue();
    this.deskServiceQueue
      .read(id)
      .pipe(
        first()
      )
      .subscribe((results) => {
        desk = results.data[0];
        this.selectedEdit = desk;
        this.createDeskForm.patchValue({
          ...desk
        });
        
      });
  }

  submitForm() {
    let data;
    this.submitted = true;

    if (this.createDeskForm.valid) {
      if (!this.edit) {
        this.validateLoading = true;
        data = {
          name: this.createDeskForm.controls.name.value,
          active: true,
          actual_state: false
        }
        this.deskServiceQueue.create(data)
        .pipe(first())
        .subscribe(results => {
          this.message.create('success','Balcão criado com sucesso');
          this.router.navigateByUrl('/queue/configuration/desks/list');

        });
      } else {
        this.validateLoading = true;
        data = {
          name: this.createDeskForm.controls.name.value,
          active: true,
          actual_state: this.selectedEdit.actual_state
        }
        this.selectedEdit.name = this.createDeskForm.controls.name.value;
        this.deskServiceQueue.update(this.selectedEdit.id, this.selectedEdit)
        .pipe(first())
        .subscribe(results => {
          this.message.create('success','Balcão editado com sucesso');
          this.router.navigateByUrl('/queue/configuration/desks/list');

        });
      }
    }
  }

  backList() {
    this.router.navigateByUrl('/queue/configuration/desks/list');
  }

}
