import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DesksListComponent } from './desks-list.component';

describe('DesksListComponent', () => {
  let component: DesksListComponent;
  let fixture: ComponentFixture<DesksListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [DesksListComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DesksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
