import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { NzMessageService } from 'ng-zorro-antd/message';
import { DeskServiceQueue } from "@fi-sas/backoffice/modules/queue/services/desk.service";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";

@Component({
    selector: 'fi-sas-desks-list-queue',
    templateUrl: './desks-list.component.html',
    styleUrls: ['./desks-list.component.less']
})
export class DesksListComponent extends TableHelper implements OnInit {

    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        private modal: NzModalService,
        uiService: UiService,
        private message: NzMessageService,
        private deskServiceQueue: DeskServiceQueue,
        private authService: AuthService
    ) {
        super(uiService, router, activatedRoute);
    }

    ngOnInit() {
       
        this.initTableData(this.deskServiceQueue);
    }


    edit(id: number) {
        if(!this.authService.hasPermission('queue:desks:update')){
            return;
          }
        this.router.navigateByUrl('/queue/configuration/desks/update/' + id);
    }

    activeConfirm(id): void {
        if(!this.authService.hasPermission('queue:desks:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer ativar este balcão?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.change(id,true); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    desactiveConfirm(id): void {
        if(!this.authService.hasPermission('queue:desks:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer desativar este balcão?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.change(id,false); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }


    change(id: any, ative) {
        let body: any = {
            active: ative
        };
        this.loading = true;
        this.deskServiceQueue.patch(id, body)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                if(ative) {
                    this.message.create('success', 'Balcão ativo com sucesso');
                }else {
                    this.message.create('success', 'Balcão desativo com sucesso');
                }
               
                this.initTableData(this.deskServiceQueue);
            });
    }
}