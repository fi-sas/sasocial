import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesksFormComponent } from './desks-form/desks-form.component';
import { DesksListComponent } from './desks-list/desks-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: DesksListComponent,
    data: { breadcrumb: 'Balcões', title: 'Listar', scope: 'queue:desks:read' },
  },
  {
    path: 'update/:id',
    component: DesksFormComponent,
    data: { breadcrumb: 'Balcão', title: 'Editar', scope: 'queue:desks:create'},
  },
  {
    path: 'create',
    component: DesksFormComponent,
    data: { breadcrumb: 'Balcão', title: 'Criar', scope: 'queue:desks:create' },
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesksRoutingModule { }
