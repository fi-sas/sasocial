import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { DesksRoutingModule } from './desks-routing.module';
import { DesksFormComponent } from './desks-form/desks-form.component';
import { DesksListComponent } from './desks-list/desks-list.component';

@NgModule({
  declarations: [
    DesksFormComponent,
    DesksListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DesksRoutingModule
  ],
})
export class DesksModule { }
