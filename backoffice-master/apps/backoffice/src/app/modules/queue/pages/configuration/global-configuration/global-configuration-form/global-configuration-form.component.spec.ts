import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlobalConfigurationFormQueueComponent } from './global-configuration-form.component';

describe('GlobalConfigurationFormQueueComponent', () => {
  let component: GlobalConfigurationFormQueueComponent;
  let fixture: ComponentFixture<GlobalConfigurationFormQueueComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [GlobalConfigurationFormQueueComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalConfigurationFormQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
