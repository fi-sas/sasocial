import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { finalize, first } from "rxjs/operators";


import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NzModalService } from "ng-zorro-antd";
import { ServiceModelQueue } from "@fi-sas/backoffice/modules/queue/models/service.model";
import { SubjectModelQueue } from "@fi-sas/backoffice/modules/queue/models/subject.model";
import { OperatorModelQueue } from "@fi-sas/backoffice/modules/queue/models/operator.model";
import { deskModelQueue } from "@fi-sas/backoffice/modules/queue/models/desk.model";
import { OperatorServiceQueue } from "@fi-sas/backoffice/modules/queue/services/operators.service";
import { ServicesServiceQueue } from "@fi-sas/backoffice/modules/queue/services/services.service";
import { DeskServiceQueue } from "@fi-sas/backoffice/modules/queue/services/desk.service";
import { GlobalConfigurationService } from "@fi-sas/backoffice/modules/queue/services/global-configuration.service";
import { GlobalConfigurationListService } from "@fi-sas/backoffice/modules/queue/services/global-configuration-list.service";
import { GlobalConfigurationModelQueue, subjectId } from "@fi-sas/backoffice/modules/queue/models/global-configuration.model";



@Component({
    selector: 'fi-queue-global-configuration-form',
    templateUrl: './global-configuration-form.component.html',
    styleUrls: ['./global-configuration-form.component.less']
})

export class GlobalConfigurationFormQueueComponent implements OnInit {
    formCreate: FormGroup;
    id: number;
    errorForm: string = '';
    noContinue: boolean = false;
    servicesLoading: boolean = false;
    operatorLoading: boolean = false;
    deskLoading: boolean = false;
    loading: boolean = false;
    services: ServiceModelQueue[] = [];
    subjects: SubjectModelQueue[] = [];
    operators: OperatorModelQueue[] = [];
    desks: deskModelQueue[] = [];
    isEdit = false;

    constructor(private formBuilder: FormBuilder,
        private operatorServiceQueue: OperatorServiceQueue,
        private servicesServiceQueue: ServicesServiceQueue,
        private deskServiceQueue: DeskServiceQueue,
        private router: Router,
        private route: ActivatedRoute,
        private uiService: UiService,
        private globalConfigurationService: GlobalConfigurationService,
        private globalConfigurationListService: GlobalConfigurationListService,
        private modalService: NzModalService) {
        this.getServices();
        this.getOperators();
        this.getDesks();
    }

    ngOnInit() {
        this.formCreate = this.formBuilder.group({
            service: ['', [Validators.required]],
            subject: ['', [Validators.required]],
            operator: ['', [Validators.required]],
            desk: ['', [Validators.required]],
        });


    }

    get f() { return this.formCreate.controls; }

    getServices() {
        this.servicesLoading = true;
        this.servicesServiceQueue.list(1, -1, null, null, {
            withRelated: 'subjects,translations',
            active: true
        }).pipe(
            first(), finalize(() => this.servicesLoading = false)
        ).subscribe(results => {
            this.services = results.data;
            this.route.params.subscribe(params => {
                this.id = params['id'];
            });

            if (this.id != undefined) {
                this.edit(this.id);
            }
        });
    }

    getSubjects(event) {
        this.formCreate.get('subject').setValue(null);
        this.services.forEach((fil) => {
            if (fil.id == event) {
                this.subjects = fil.subjects;
            }
        })
        this.errorForm = '';
        if (this.subjects.length == 0) {
            this.noContinue = true;
        } else {
            this.noContinue = false;
        }
    }

    getOperators() {
        this.operatorLoading = true;
        this.operatorServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(), finalize(() => this.operatorLoading = false)).subscribe(results => {
                this.operators = results.data;
            });
    }

    getDesks() {
        this.deskLoading = true;
        this.deskServiceQueue.list(1, -1, null, null, {
            active: true,
            sort: 'name'
        }).pipe(
            first(),
            finalize(() => this.deskLoading = false)).subscribe(results => {
                this.desks = results.data;
            });
    }

    save() {
        this.loading = true;
        if (this.formCreate.valid) {
            let sendValue = new GlobalConfigurationModelQueue();
            let idSubject = new subjectId();
            idSubject.subject_id = this.formCreate.get('subject').value;
            sendValue.subjects.push(idSubject);
            sendValue.operator_id = this.formCreate.get('operator').value;
            sendValue.desk_id = this.formCreate.get('desk').value;
            this.errorForm = ''; 
            this.globalConfigurationService.create(sendValue).pipe(
                first(), finalize(() => this.loading = false)).subscribe(() => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Configuração criada com sucesso'
                    );
                    this.router.navigateByUrl('/queue/configuration/global-configurations/list');
                });

        } else {
            this.loading = false;
            this.errorForm = 'Preencha os campos obrigatórios';
        }

    }


    edit(id: number) {
        this.isEdit = true;
        this.globalConfigurationService
            .read(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.formCreate.get('service').setValue(results.data[0].subject[0].service[0].id);
                this.getSubjects(results.data[0].subject[0].service[0].id);
                this.formCreate.get('subject').setValue(results.data[0].subject_id);
                this.formCreate.get('operator').setValue(results.data[0].operator_id);
                this.formCreate.get('desk').setValue(results.data[0].desk_id);
                this.id = results.data[0].id;

            });
    }

    editDataSave() {
        if (this.formCreate.valid) {
            let sendValue = new GlobalConfigurationModelQueue();
            sendValue.subject_id = this.formCreate.get('subject').value;
            sendValue.operator_id = this.formCreate.get('operator').value;
            sendValue.desk_id = this.formCreate.get('desk').value;
            this.errorForm = '';
            this.globalConfigurationService.update(this.id, sendValue).pipe(
                first()).subscribe(() => {
                    this.uiService.showMessage(
                        MessageType.success,
                        'Configuração alterada com sucesso'
                    );
                    this.router.navigateByUrl('/queue/configuration/global-configurations/list');
                });

        } else {
            this.errorForm = 'Preencha os campos obrigatórios';
        }
    }

    backList() {
        this.router.navigateByUrl('/queue/configuration/global-configurations/list');
    }
}