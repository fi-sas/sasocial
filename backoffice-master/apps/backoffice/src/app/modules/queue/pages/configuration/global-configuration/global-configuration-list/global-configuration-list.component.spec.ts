import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { globalConfigurationListComponent } from './global-configuration-list.component';

describe('globalConfigurationListComponent', () => {
  let component: globalConfigurationListComponent;
  let fixture: ComponentFixture<globalConfigurationListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [globalConfigurationListComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(globalConfigurationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
