import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { first } from "rxjs/operators";
import { GlobalConfigurationListService } from "@fi-sas/backoffice/modules/queue/services/global-configuration-list.service";
import { GlobalConfigurationService } from "@fi-sas/backoffice/modules/queue/services/global-configuration.service";
@Component({
    selector: 'fi-sas-global-configuration-list-queue',
    templateUrl: './global-configuration-list.component.html',
    styleUrls: ['./global-configuration-list.component.less']
})
export class globalConfigurationListComponent extends TableHelper implements OnInit {

    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        private modalService: NzModalService,
        private globalConfigurationService: GlobalConfigurationService,
        private globalConfigurationListService: GlobalConfigurationListService
    ) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['withRelated'] = 'desk,operator,subject';
    }

    ngOnInit() {
       
        this.initTableData(this.globalConfigurationListService);
    }


    edit(id: number) {
        this.router.navigateByUrl('/queue/configuration/global-configurations/update/' + id);
    }

    desactiveData(id: number) {
        this.modalService.confirm({
            nzTitle: 'Vai desativar esta configuração. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(id)
        });
    }

    desactiveDataSubmit(id: number) {
        this.globalConfigurationService.desactive(id).pipe(
            first()).subscribe(() => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Configuração desativada com sucesso'
                );
                this.initTableData(this.globalConfigurationListService);
            });
    }

    activeData(id: number) {
        this.modalService.confirm({
            nzTitle: 'Vai ativar esta configuração. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(id)
        });
    }

    activeDataSubmit(id: number) {
        this.globalConfigurationService.active(id).pipe(
            first()).subscribe(() => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Configuração ativada com sucesso'
                );
                this.initTableData(this.globalConfigurationListService);
            });
    }

}