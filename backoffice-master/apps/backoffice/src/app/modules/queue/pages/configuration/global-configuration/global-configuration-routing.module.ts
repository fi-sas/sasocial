import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GlobalConfigurationFormQueueComponent } from './global-configuration-form/global-configuration-form.component';
import { globalConfigurationListComponent } from './global-configuration-list/global-configuration-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: globalConfigurationListComponent,
    data: { breadcrumb: 'Associação', title: 'Listar', scope: 'queue:desk_operators:read' },
  },
  {
    path: 'update/:id',
    component: GlobalConfigurationFormQueueComponent,
    data: { breadcrumb: 'Associação', title: 'Editar', scope: 'queue:desk_operators:create' },
  },
  {
    path: 'create',
    component: GlobalConfigurationFormQueueComponent,
    data: { breadcrumb: 'Associação', title: 'Criar', scope: 'queue:desk_operators:create' },
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlobalOperationRoutingModule { }
