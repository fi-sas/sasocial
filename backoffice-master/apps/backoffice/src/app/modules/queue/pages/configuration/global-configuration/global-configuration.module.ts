import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { GlobalOperationRoutingModule } from './global-configuration-routing.module';
import { globalConfigurationListComponent } from './global-configuration-list/global-configuration-list.component';
import { GlobalConfigurationFormQueueComponent } from './global-configuration-form/global-configuration-form.component';

@NgModule({
  declarations: [
    globalConfigurationListComponent,
    GlobalConfigurationFormQueueComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GlobalOperationRoutingModule
  ],
})
export class GlobalConfigurationModule { }
