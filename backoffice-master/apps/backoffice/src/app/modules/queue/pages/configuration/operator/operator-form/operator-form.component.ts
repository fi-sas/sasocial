import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzMessageService, NzModalService, UploadFile } from 'ng-zorro-antd';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OperatorServiceQueue } from '@fi-sas/backoffice/modules/queue/services/operators.service';
import { OperatorModelQueue } from '@fi-sas/backoffice/modules/queue/models/operator.model';

@Component({
  selector: 'fi-sas-operator-form-queue',
  templateUrl: './operator-form.component.html',
  styleUrls: ['./operator-form.component.less']
})
export class OperatorFormComponent implements OnInit {

  previewVisible = false;
  index = 0;
  previewImage: string | undefined = '';
  file = [];
  avatarUrlPhoto: any;
  edit = false;
  show_photo = false;
  mediaObj: FileModel = null;
  mediaUrl: string = null;
  isLoading = false;
  previousCouter = -1;
  submitLoading = false;
  handleError: any;
  couterList: any;
  couter: any;
  errorScheduls = false;
  submitted = false;
  couterForm: FormGroup;
  id: number;

  constructor(
    private modal: NzModalService,
    private resourceService: FiResourceService,
    private usersService: UsersService,
    private operatorService: OperatorServiceQueue,
    private urlService: FiUrlService,
    private router: Router,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private uiService: UiService,
    private fb: FormBuilder) {
    this.couterForm = this.getNewForm();

    this.couterForm.controls.user_id.valueChanges.subscribe(data => {
      if (data && data !== this.previousCouter) {
        this.previousCouter = data
        this.couterForm.controls.user_id.patchValue(data);
        this.couter = {
          show_photo: false,
          active: true,
          user_id: data,
          user: {}
        }
        this.couter.user.id = data
        this.loadUser(data);
      }
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.onClickEdit(this.id);
    }

    this.getCouterList();
    this.validName();
  }

  get f() { return this.couterForm.controls; }

  getNewForm() {
    return this.fb.group({
      id: [null],
      image_file: [null],
      user_id: [null],
      show_photo: [false],
      edit_selected_person: [''],
      monday: this.fb.array([]),
      tuesday: this.fb.array([]),
      wednesday: this.fb.array([]),
      thursday: this.fb.array([]),
      friday: this.fb.array([])
    });

  }

  validName() {
    if (!this.edit) {
      this.f.user_id.setValidators([Validators.required]);
    }else {
      this.f.user_id.setValidators(null);
    }
    this.f.user_id.updateValueAndValidity();
  }

  loadImage(file_id) {
    this.resourceService
      .read<FileModel>(this.urlService.get('FILES_ID', { id: file_id }))
      .pipe(first())
      .subscribe(
        result => {
          this.mediaObj = result.data[0];

          if (this.mediaObj) {
            return result.data[0].url;
          }
          this.isLoading = false;
        },
        err => {
          this.uiService.showMessage(
            MessageType.error,
            'Não foi possivel carregar a imagem'
          );
          this.isLoading = false;
        }
      );
  }

  validScheduls() {
    let monday = this.f.monday.value;
    let tuesday = this.f.tuesday.value;
    let wednesday = this.f.wednesday.value;
    let thursday = this.f.thursday.value;
    let friday = this.f.friday.value;
    this.errorScheduls = false;
    if (monday.length == 0 && tuesday.length == 0 && wednesday.length == 0 && thursday.length == 0
      && friday.length == 0) {
      this.errorScheduls = true;
    }

    if (monday.length > 0) {
      monday.forEach(monday => {
        if (monday.closing_hour == null || monday.opening_hour == null) {
          this.errorScheduls = true;
        }
      });
    }
    if (tuesday.length > 0) {
      tuesday.forEach(tuesday => {
        if (tuesday.closing_hour == null || tuesday.opening_hour == null) {
          this.errorScheduls = true;
        }
      });
    }
    if (wednesday.length > 0) {
      wednesday.forEach(wednesday => {
        if (wednesday.closing_hour == null || wednesday.opening_hour == null) {
          this.errorScheduls = true;
        }
      });
    }
    if (thursday.length > 0) {
      thursday.forEach(thursday => {
        if (thursday.closing_hour == null || thursday.opening_hour == null) {
          this.errorScheduls = true;
        }
      });
    }
    if (friday.length > 0) {
      friday.forEach(friday => {
        if (friday.closing_hour == null || friday.opening_hour == null) {
          this.errorScheduls = true;
        }
      });
    }

  }

  getCouterList() {
    this.couterForm.reset();
    this.couterForm = this.getNewForm();
    this.couter = null;
    this.couterList = [];
    this.operatorService.list(1, -1, null, null, { withRelated: "schedule_operator,user", active: true }).pipe(first()).subscribe(results => {
      this.couterList = results.data;
    });
  }

  loadUser(user_id: number, array_id: number = null) {
    this.usersService.read(user_id, {
      withRelated: false
    }).pipe(
      first()
    ).subscribe(results => {
      this.couter.user = results.data[0]
      this.couter.user.picture = results.data[0].picture ? this.loadImage(results.data[0].picture) : null;
    });
  }

  postCouterList() {
    if (this.couterForm.valid) {
      this.errorScheduls = false;
      this.couter = {};
      this.couter.operator = {
        show_photo: this.couterForm.controls.show_photo.value,
        user_id: this.couterForm.controls.user_id.value,
        active: true
      }

      this.couter.schedule = [
        {
          "description": "Segunda-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.monday.value
        },
        {
          "description": "Terça-feira",
          "week_day": 2,
          "schedules": this.couterForm.controls.tuesday.value
        },
        {
          "description": "Quarta-feira",
          "week_day": 3,
          "schedules": this.couterForm.controls.wednesday.value
        },
        {
          "description": "Quinta-feira",
          "week_day": 4,
          "schedules": this.couterForm.controls.thursday.value
        },
        {
          "description": "Sexta-feira",
          "week_day": 5,
          "schedules": this.couterForm.controls.friday.value
        }
      ];
      this.operatorService.create(this.couter).pipe(first()).subscribe(results => {
        this.uiService.showMessage(
          MessageType.success,
          'Operador criado com sucesso'
      );
      this.router.navigateByUrl('/queue/configuration/operators/list');
      });
    }
  }

  updateCouterList() {
    if (this.couterForm.valid) {
      this.errorScheduls = false;
      this.couter.operator = {
        id: this.couter.id,
        user_id: this.couter.user_id,
        show_photo: this.couterForm.controls.show_photo.value,
        active: true
      }
      this.couter.schedule = [
        {
          "operator_id": this.couter.user_id,
          "description": "Segunda-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.monday.value
        },
        {
          "operator_id": this.couter.user_id,
          "description": "Terça-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.tuesday.value
        },
        {
          "operator_id": this.couter.user_id,
          "description": "Quarta-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.wednesday.value
        },
        {
          "operator_id": this.couter.user_id,
          "description": "Quinta-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.thursday.value
        },
        {
          "operator_id": this.couter.user_id,
          "description": "Sexta-feira",
          "week_day": 1,
          "schedules": this.couterForm.controls.friday.value
        }
      ];
      this.operatorService.update(this.couter.id, this.couter).pipe(first()).subscribe(results => {
        this.uiService.showMessage(
          MessageType.success,
          'Operador alterado com sucesso'
      );
      this.router.navigateByUrl('/queue/configuration/operators/list');
      });
    }
  }

  getLenghtArray(day: any) {
    const add = this.couterForm.get(day) as FormArray;
    return add.length;
  }

  createItem() {
    return this.fb.group({
      opening_hour: [],
      closing_hour: []
    });
  }

  addNewGroup(day: string) {
    let add: FormArray;
    add = this.couterForm.get(day) as FormArray;
    add.push(this.createItem());
  }

  deleteGroup(index: number, day: string) {
    const add = this.couterForm.get(day) as FormArray;
    add.removeAt(index);
  }

  onChangeValueImage() {
    this.show_photo = !this.show_photo;
  }

  submitForm() {
    this.submitted = true;
    const photo = this.couterForm.get('show_photo').value
    photo == null ? this.couterForm.get('show_photo').setValue(false) : this.couterForm.get('show_photo').setValue(photo);
    if(this.edit) {
      this.couterForm.get('user_id').setValue(this.couter.user_id);
    }
    this.validScheduls();
    if (this.couterForm.valid && !this.errorScheduls) {
      this.submitted = false;
      this.submitLoading = true;
      if (!this.edit) {
        this.postCouterList();
      } else {
        this.updateCouterList();
      }
    }
    this.submitLoading = false;
  }

  personSelected() {
    this.couterForm.controls.full_name.patchValue(this.couterForm.controls.edit_selected_person.value);
  }


  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };

  onClickEdit(id: number) {
    let operator: OperatorModelQueue = new OperatorModelQueue();
    this.operatorService
      .read(id, { withRelated: "schedule_operator,user"})
      .pipe(
        first()
      )
      .subscribe((results) => {
        operator = results.data[0];
        this.edit = true;
        this.couter = operator;
        this.couterForm.patchValue({
          show_photo: this.couter.show_photo
        })
        for (let i = 0; i < this.couter.schedule_operator.length; i++) {
          this.couter.schedule_operator[i].hours.forEach((time) => {
            switch (this.couter.schedule_operator[i].week_day) {
              case 1:
                this.addEditTimes('monday', time.opening_hour, time.closing_hour);
                break;
              case 2:
                this.addEditTimes('tuesday', time.opening_hour, time.closing_hour);
                break;
              case 3:
                this.addEditTimes('wednesday', time.opening_hour, time.closing_hour);
                break;
              case 4:
                this.addEditTimes('thursday', time.opening_hour, time.closing_hour);
                break;
              case 5:
                this.addEditTimes('friday', time.opening_hour, time.closing_hour);
                break;
              default:
                break;
            }
          });
        }
      });
   
  }

  addEditTimes(day: any, open: any, close: any) {

    if (open.split(':')[0].length === 1) {
      open = '0' + open
    }

    if (close.split(':')[0].length === 1) {
      close = '0' + close
    }

    if (open.split(':')[1].length === 1) {
      if (Number(open.split(':')[1]) < 5) {
        open = open + '0'
      }
      else {
        open.split(':')[1] = '0' + open.split(':')[1];
      }
    }

    if (close.split(':')[1].length === 1) {
      if (Number(close.split(':')[1]) < 5) {
        close = close + '0'
      }
      else {
        close.split(':')[1] = '0' + close.split(':')[1];
      }
    }

    let add: FormArray;
    add = this.couterForm.get(day) as FormArray;
    add.push(this.fb.group({
      opening_hour: [open],
      closing_hour: [close]
    }));
  }

  
  backList() {
    this.router.navigateByUrl('/queue/configuration/operators/list');
  }

}
