import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { NzMessageService } from 'ng-zorro-antd/message';
import { OperatorServiceQueue } from "@fi-sas/backoffice/modules/queue/services/operators.service";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";

@Component({
    selector: 'fi-sas-operator-list-queue',
    templateUrl: './operator-list.component.html',
    styleUrls: ['./operator-list.component.less']
})
export class OperatorListComponent extends TableHelper implements OnInit {

    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        private modal: NzModalService,
        uiService: UiService,
        private message: NzMessageService,
        private authService: AuthService,
        private operatorService: OperatorServiceQueue
    ) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['active'] = 'true';
    }

    ngOnInit() {
       
        this.initTableData(this.operatorService);
    }


    edit(id: number) {
        if(!this.authService.hasPermission('queue:desk_operators:update')){
            return;
          }
        this.router.navigateByUrl('/queue/configuration/operators/update/' + id);
    }

    deleteConfirm(id): void {
        if(!this.authService.hasPermission('queue:desk_operators:delete')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer eliminar este operador?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.delete(id); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    delete(id: any) {
        let body: any = {
            active: false
        };
        this.loading = true;
        this.operatorService.patch(id, body)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                this.message.create('success', 'Operador eliminado com sucesso');
                this.initTableData(this.operatorService);
            });
    }
}