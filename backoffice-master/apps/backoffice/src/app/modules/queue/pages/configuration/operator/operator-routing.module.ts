import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatorFormComponent } from './operator-form/operator-form.component';
import { OperatorListComponent } from './operator-list/operator-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: OperatorListComponent,
    data: { breadcrumb: 'Operador', title: 'Listar', scope: 'queue:desk_operators:read' },
  },
  {
    path: 'update/:id',
    component: OperatorFormComponent,
    data: { breadcrumb: 'Operador', title: 'Editar', scope: 'queue:desk_operators:create' },
  },
  {
    path: 'create',
    component: OperatorFormComponent,
    data: { breadcrumb: 'Operador', title: 'Criar', scope: 'queue:desk_operators:create' },
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperatorRoutingModule { }
