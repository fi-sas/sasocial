import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { OperatorFormComponent } from './operator-form/operator-form.component';
import { OperatorListComponent } from './operator-list/operator-list.component';
import { OperatorRoutingModule } from './operator-routing.module';

@NgModule({
  declarations: [
    OperatorFormComponent,
    OperatorListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    OperatorRoutingModule
  ],
})
export class OperatorModule { }
