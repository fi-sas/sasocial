import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GroupsService } from '@fi-sas/backoffice/modules/configurations/services/groups.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { hasOwnProperty } from 'tslint/lib/utils';
import * as _ from 'lodash';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { ServicesServiceQueue } from '@fi-sas/backoffice/modules/queue/services/services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceModelQueue } from '@fi-sas/backoffice/modules/queue/models/service.model';

@Component({
  selector: 'fi-sas-services-form-queue',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.less']
})
export class ServicesFormComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  errorTrans = false;
  languages: LanguageModel[] = [];
  languages_loading = false;
  listOfSelectedLanguages = [];
  validateLoading = false;
  servicesList: any[] = [];
  edit = false;
  id: number;
  submitted = false;
  listOfOption = [];
  createServiceForm = new FormGroup({
    translations: new FormArray([]),
    groups: new FormControl(null, [Validators.required])
  });

  translations = this.createServiceForm.get('translations') as FormArray;

  get f() { return this.createServiceForm.controls; }

  constructor(
    private serviceQueue: ServicesServiceQueue,
    private message: NzMessageService,
    private authService: AuthService,
    private groupsService: GroupsService,
    private router: Router,
    private route: ActivatedRoute,
    private languagesService: LanguagesService
  ) {
  }

  ngOnInit() {
    this.getServices();
    this.getGroups();
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != undefined) {
      this.languages_loading = true;
      this.getServiceId(this.id);

    } else {
      this.loadLanguages('');
    }
  }

  getGroups() {
    this.groupsService.list(1, -1, null, null, {
      sort: 'name'
    })
      .pipe(
        first(),
        finalize(() => { })
      )
      .subscribe(
        (result) => {
          this.listOfOption = result.data
        },
        (error) => { }
      )
  }

  getServiceId(id: number) {
    this.validateLoading = true;
    this.edit = true;
    let service: ServiceModelQueue = new ServiceModelQueue();
    this.languages_loading = false;
    this.serviceQueue
      .read(id)
      .pipe(
        first(), finalize(() => this.validateLoading = false)
      )
      .subscribe((results) => {
        service = results.data[0];
        this.createServiceForm.patchValue({
          ...service
        });
        if (service.groups) {
          let grouIdArray = [];
          for (let aux of service.groups) {
            grouIdArray.push(aux.group_id);
          }
          this.createServiceForm.controls.groups.patchValue(grouIdArray);
        }
        this.listOfSelectedLanguages = [];
        this.loadLanguages(service);
      });
  }

  backList() {
    this.router.navigateByUrl('/queue/configuration/services/list');
  }

  submitForm() {
    this.submitted = true;
    if (this.createServiceForm.get('translations').value.length == 0) {
      this.errorTrans = true;
    }
    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }
    if (this.createServiceForm.valid && !this.errorTrans) {
      const body: any = {
        translations:
          this.createServiceForm.get('translations').value
        ,
        groups: []
      }

      for (const group of this.createServiceForm.controls.groups.value) {
        body.groups.push({ group_id: group });
      }
      this.validateLoading = true;

      if (!this.edit && this.authService.hasPermission('queue:services:create')) {
        this.serviceQueue.create(body)
          .pipe(
            first(),
            finalize(
              () => {
                this.validateLoading = false;
              }
            )
          )
          .subscribe(results => {
            this.message.create('success', 'Serviço criado com sucesso');
            this.router.navigateByUrl('/queue/configuration/services/list');
          });
      } else if (this.authService.hasPermission('queue:services:update')) {
        this.serviceQueue.update(this.id, body)
          .pipe(
            first(),
            finalize(
              () => {
                this.validateLoading = false;
              }
            )
          )
          .subscribe(results => {
            this.message.create('success', 'Serviço editado com sucesso');
            this.router.navigateByUrl('/queue/configuration/services/list');
          });
      }
    }

  }

  getServices() {
    this.serviceQueue.list(1, -1, null, null, { active: true })
      .pipe(first())
      .subscribe(results => {
        this.servicesList = results.data;
      });

  }


  loadLanguages(data: any) {
    this.languages_loading = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languages_loading = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        if (data) {
          this.startTranslation(data);
        }
        else {
          this.startTranslation('');
        }
      });
  }

  convertTranslationsToLanguageIDS(translations: any) {
    let languagesIDS = [];
    translations.value.forEach((languageID: any) => {
      languagesIDS.push(languageID.language_id);
    });
    return languagesIDS;
  }

  changeLanguage() {
    this.errorTrans = false;
    const translations = this.createServiceForm.controls.translations as FormArray;
    const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
    if (this.listOfSelectedLanguages.length > languagesIDS.length) {
      this.addTranslation(
        _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
        ''
      );
    } else {
      this.translations.removeAt(
        this.translations.value.findIndex(
          (trans: any) =>
            trans.language_id ===
            _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
        )
      );
    }
  }

  addTranslation(language_id: number, name?: string) {
    const translations = this.createServiceForm.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        name: new FormControl(name, [Validators.required, trimValidation])
      })
    );
  }

  startTranslation(value) {
    if (value !== '') {
      value.translations.map((translation) => {
        this.addTranslation(
          translation.language_id,
          translation.name
        );
        this.listOfSelectedLanguages.push(translation.language_id);
      });
    } else {
      if (hasOwnProperty(this.languages[0], 'id')) {
        this.addTranslation(this.languages[0].id, '');
        this.listOfSelectedLanguages.push(this.languages[0].id);
      }
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }


}
