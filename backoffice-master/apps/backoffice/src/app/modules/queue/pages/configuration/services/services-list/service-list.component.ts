import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ServicesServiceQueue } from "@fi-sas/backoffice/modules/queue/services/services.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";

@Component({
    selector: 'fi-sas-services-list-queue',
    templateUrl: './service-list.component.html',
    styleUrls: ['./service-list.component.less']
})
export class ServicesListComponent extends TableHelper implements OnInit {

    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        private modal: NzModalService,
        uiService: UiService,
        private message: NzMessageService,
        private authService: AuthService,
        private servicesServiceQueue: ServicesServiceQueue
    ) {
        super(uiService, router, activatedRoute);
    }

    ngOnInit() {
       
        this.initTableData(this.servicesServiceQueue);
    }


    editService(id: number) {
        if(!this.authService.hasPermission('queue:services:update')){
            return;
          }
        this.router.navigateByUrl('/queue/configuration/services/update/' + id);
    }

    inativeServiceConfirm(id): void {
        if(!this.authService.hasPermission('queue:services:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer inativar este serviço?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.inativeService(id); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    ativeServiceConfirm(id): void {
        if(!this.authService.hasPermission('queue:services:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer ativar este serviço?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.activeService(id); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    inativeService(id: any) {
        let body: any = {
            active: true
        };
        this.loading = true;
        this.servicesServiceQueue.desativeService(id)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                this.message.create('success', 'Serviço desativo com sucesso');
                this.initTableData(this.servicesServiceQueue);
            });
    }

    activeService(id: any) {
        let body: any = {
            active: true
        };
        this.loading = true;
        this.servicesServiceQueue.patch(id, body)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                this.message.create('success', 'Serviço ativo com sucesso');
                this.initTableData(this.servicesServiceQueue);
            });
    }
}