import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesFormComponent } from './service-form/service-form.component';
import { ServicesListComponent } from './services-list/service-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ServicesListComponent,
    data: { breadcrumb: 'Serviços', title: 'Listar', scope: 'queue:services:read' },
  },
  {
    path: 'update/:id',
    component: ServicesFormComponent,
    data: { breadcrumb: 'Serviço', title: 'Editar', scope: 'queue:services:create' },
  },
  {
    path: 'create',
    component: ServicesFormComponent,
    data: { breadcrumb: 'Serviço', title: 'Criar', scope: 'queue:services:create' },
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
