import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

import { ServicesListComponent } from './services-list/service-list.component';
import { ServicesFormComponent } from './service-form/service-form.component';
import { ServicesRoutingModule } from './services-routing.module';

@NgModule({
  declarations: [
    ServicesFormComponent,
    ServicesListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ServicesRoutingModule
  ],
})
export class ServicesModule { }
