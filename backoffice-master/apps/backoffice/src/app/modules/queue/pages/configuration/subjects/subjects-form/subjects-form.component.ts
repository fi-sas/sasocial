import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NzMessageService, NzTabSetComponent } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import * as _ from 'lodash';
import { ServiceModelQueue } from "@fi-sas/backoffice/modules/queue/models/service.model";
import { ServicesServiceQueue } from "@fi-sas/backoffice/modules/queue/services/services.service";
import { SubjectServiceQueue } from "@fi-sas/backoffice/modules/queue/services/subjects.service";
import { SubjectModelQueue } from "@fi-sas/backoffice/modules/queue/models/subject.model";
import { LanguageModel } from "@fi-sas/backoffice/modules/configurations/models/language.model";
import { LanguagesService } from "@fi-sas/backoffice/modules/configurations/services/languages.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { hasOwnProperty } from "tslint/lib/utils";

@Component({
    selector: 'fi-queue-subjects-form',
    templateUrl: './subjects-form.component.html',
    styleUrls: ['./subjects-form.component.less']
})
export class SubjectFormComponent implements OnInit {
    @ViewChild('translationTabs', { static: true })
    translationTabs: NzTabSetComponent;
    errorTrans = false;
    servicesLoading = false;
    services: ServiceModelQueue[] = [];
    languages: LanguageModel[] = [];
    languages_loading = false;
    listOfSelectedLanguages = [];
    submitted = false;
    errorScheduls = false;
    isEdit = false;
    id: number;
    loading = false;
    formCreate = new FormGroup({
        translations: new FormArray([]),
        code: new FormControl('', [Validators.required, trimValidation]),
        estimated_time: new FormControl(null, [Validators.required]),
        max_tolerance: new FormControl('', [Validators.required]),
        service_id: new FormControl('', [Validators.required]),
        appointments: new FormControl(''),
        duration: new FormControl(null, [Validators.required]),
        monday: new FormArray([]),
        tuesday: new FormArray([]),
        wednesday: new FormArray([]),
        thursday: new FormArray([]),
        friday: new FormArray([]),
    });

    translations = this.formCreate.get('translations') as FormArray;

    constructor(
        private formBuilder: FormBuilder,
        private languagesService: LanguagesService,
        private servicesServiceQueue: ServicesServiceQueue,
        private router: Router,
        private route: ActivatedRoute,
        private message: NzMessageService,
        private subjectServiceQueue: SubjectServiceQueue) {

    }

    ngOnInit() {
        this.getServices();
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });

        if (this.id != undefined) {
            this.languages_loading = true;
            this.getSubjectId(this.id);

        } else {
            this.loadLanguages('');
        }
    }

    get f() { return this.formCreate.controls; }

    getServices() {
        this.servicesLoading = true;
        this.servicesServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(), finalize(() => this.servicesLoading = false)
        ).subscribe(results => {
            this.services = results.data;
        });
    }


    loadLanguages(data: any) {
        this.languages_loading = true;
        this.languagesService
            .list(1, -1)
            .pipe(
                first(),
                finalize(() => (this.languages_loading = false))
            )
            .subscribe((results) => {
                this.languages = results.data;
                if (data) {
                    this.startTranslation(data);
                }
                else {
                    this.startTranslation('');
                }
            });
    }

    convertTranslationsToLanguageIDS(translations: any) {
        let languagesIDS = [];
        translations.value.forEach((languageID: any) => {
            languagesIDS.push(languageID.language_id);
        });
        return languagesIDS;
    }

    changeLanguage() {
        this.errorTrans = false;
        const translations = this.formCreate.controls.translations as FormArray;
        const languagesIDS = this.convertTranslationsToLanguageIDS(translations);
        if (this.listOfSelectedLanguages.length > languagesIDS.length) {
            this.addTranslation(
                _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
                ''
            );
        } else {
            this.translations.removeAt(
                this.translations.value.findIndex(
                    (trans: any) =>
                        trans.language_id ===
                        _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
                )
            );
        }
    }

    addTranslation(language_id: number, name?: string, schedule_description?: string) {
        const translations = this.formCreate.controls.translations as FormArray;
        translations.push(
            new FormGroup({
                language_id: new FormControl(language_id, Validators.required),
                name: new FormControl(name, [Validators.required, trimValidation]),
                schedule_description: new FormControl(schedule_description, [Validators.required, trimValidation])
            })
        );
    }

    startTranslation(value) {
        if (value !== '') {
            value.translations.map((translation) => {
                this.addTranslation(
                    translation.language_id,
                    translation.name,
                    translation.schedule_description
                );
                this.listOfSelectedLanguages.push(translation.language_id);
            });
        } else {
            if (hasOwnProperty(this.languages[0], 'id')) {
                this.addTranslation(this.languages[0].id, '');
                this.listOfSelectedLanguages.push(this.languages[0].id);
            }
        }
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }


    addNewDay(day: string) {
        let add: FormArray;
        add = this.formCreate.get(day) as FormArray;
        add.push(this.initDay());
    }

    initDay() {
        return this.formBuilder.group({
            opening_hour: [],
            closing_hour: []
        });
    }

    deleteDay(index: number, day: string) {
        const add = this.formCreate.get(day) as FormArray;
        add.removeAt(index);
    }

    validScheduls() {
        let monday = this.f.monday.value;
        let tuesday = this.f.tuesday.value;
        let wednesday = this.f.wednesday.value;
        let thursday = this.f.thursday.value;
        let friday = this.f.friday.value;
        this.errorScheduls = false;
        if (monday.length == 0 && tuesday.length == 0 && wednesday.length == 0 && thursday.length == 0
            && friday.length == 0) {
            this.errorScheduls = true;
        }

        if (monday.length > 0) {
            monday.forEach(monday => {
                if (monday.closing_hour == null || monday.opening_hour == null) {
                    this.errorScheduls = true;
                }
            });
        }
        if (tuesday.length > 0) {
            tuesday.forEach(tuesday => {
                if (tuesday.closing_hour == null || tuesday.opening_hour == null) {
                    this.errorScheduls = true;
                }
            });
        }
        if (wednesday.length > 0) {
            wednesday.forEach(wednesday => {
                if (wednesday.closing_hour == null || wednesday.opening_hour == null) {
                    this.errorScheduls = true;
                }
            });
        }
        if (thursday.length > 0) {
            thursday.forEach(thursday => {
                if (thursday.closing_hour == null || thursday.opening_hour == null) {
                    this.errorScheduls = true;
                }
            });
        }
        if (friday.length > 0) {
            friday.forEach(friday => {
                if (friday.closing_hour == null || friday.opening_hour == null) {
                    this.errorScheduls = true;
                }
            });
        }

    }

    transformHourToMin(hour: string) {
        let minutes: number;
        let hourSplitter = (hour).split(":");
        if (hourSplitter[0] == '00') {
            minutes = Number(hourSplitter[1]);
        } else {
            minutes = Number(hourSplitter[0]) * 60 + Number(hourSplitter[1]);
        }
        return minutes;
    }

    transformMinToHour(minuts: number): string {
        let h = Math.floor(minuts / 60);
        let m = Math.floor(minuts % 60);
        let hDisplay;
        let mDisplay;
        if (h == 0) {
            hDisplay = '00';
        } else if (h < 10) {
            hDisplay = '0' + h;
        } else {
            hDisplay = h;
        }
        if (m == 0) {
            mDisplay = '00';
        } else if (m < 10) {
            mDisplay = m + '0';
        } else {
            mDisplay = m;
        }
        return hDisplay + ':' + mDisplay;
    }

    transformHourToSecond(hour: string): number {
        let seconds: number;
        let hourSplitter = (hour).split(":");
        if (hourSplitter[0] == '00') {
            seconds = Number(hourSplitter[1]) * 60;
        } else {
            seconds = (Number(hourSplitter[0]) * 3600) + (Number(hourSplitter[1]) * 60);
        }
        return seconds;
    }

    transformSecondsToHours(seconds: number): string {
        let h = Math.floor(seconds / 3600);
        let m = Math.floor(seconds % 3600 / 60);
        let hDisplay;
        let mDisplay;
        if (h == 0) {
            hDisplay = '00';
        } else if (h < 10) {
            hDisplay = '0' + h;
        } else {
            hDisplay = h;
        }
        if (m == 0) {
            mDisplay = '00';
        } else if (m < 10) {
            mDisplay = m + '0';
        } else {
            mDisplay = m;
        }
        return hDisplay + ':' + mDisplay;
    }

    save(isEdit: boolean) {
        let sendValues: SubjectModelQueue = new SubjectModelQueue();
        this.submitted = true;
        this.loading = true;
        this.validScheduls();
        if (this.formCreate.get('translations').value.length == 0) {
            this.errorTrans = true;
          }
          let tabIndex = 0;
          for (const t in this.translations.controls) {
            if (t) {
              const tt = this.translations.get(t) as FormGroup;
              if (!tt.valid) {
                this.translationTabs.nzSelectedIndex = tabIndex;
                break;
              }
            }
            tabIndex++;
          }
        if (this.formCreate.valid && !this.errorScheduls && !this.errorTrans) {

            sendValues.translations =  this.formCreate.get('translations').value;
            sendValues.code = this.f.code.value;
            sendValues.estimated_time = this.f.estimated_time.value*60;
            sendValues.avg_time = sendValues.estimated_time;
            sendValues.max_tolerance = Number(this.f.max_tolerance.value);
            sendValues.active = true;
            sendValues.appointments = this.f.appointments.value == '' || this.f.appointments.value == false ? false : true;;
            sendValues.appointments_interval = this.f.duration.value;
            sendValues.service_id = this.f.service_id.value;
            sendValues.schedule = [{
                "description": 'Segunda-feira',
                "week_day": 1,
                "schedules": this.f.monday.value
            },
            {
                "description": 'Terça-feira',
                "week_day": 2,
                "schedules": this.f.tuesday.value
            },
            {
                "description": 'Quarta-feira',
                "week_day": 3,
                "schedules": this.f.wednesday.value
            },
            {
                "description": 'Quinta-feira',
                "week_day": 4,
                "schedules": this.f.thursday.value
            },
            {
                "description": 'Sexta-feira',
                "week_day": 5,
                "schedules": this.f.friday.value
            },

            ]
            if (!isEdit) {
                this.subjectServiceQueue.create(sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.message.create('success', 'Fila criada com sucesso');
                        this.router.navigateByUrl('/queue/configuration/subjects/list');
                    });
            } else {
                sendValues.id = this.id;
                this.subjectServiceQueue.update(this.id, sendValues).pipe(
                    first(), finalize(() => this.loading = false)).subscribe(() => {
                        this.message.create('success', 'Fila alterada com sucesso');
                        this.router.navigateByUrl('/queue/configuration/subjects/list');
                    });
            }

        }else {
           this.loading = false;
        }
    }

    getSubjectId(id) {
        this.isEdit = true;
        let subject: SubjectModelQueue = new SubjectModelQueue();
        this.languages_loading = false;
        this.subjectServiceQueue
          .read(id)
          .pipe(
            first(), finalize(() => this.loading = false)
          )
          .subscribe((results) => {
            subject = results.data[0];
            this.formCreate.patchValue({
              ...subject
            });
            this.formCreate.get('estimated_time').setValue(subject.estimated_time/60);
            this.formCreate.get('duration').setValue(subject.appointments_interval);
            this.listOfSelectedLanguages = [];
            this.loadLanguages(subject);
            if (subject.schedule) {
                for (let i = 0; i < subject.schedule.length; i++) {
                    subject.schedule[i].schedules.forEach((time) => {
                        switch (subject.schedule[i].week_day) {
                            case 1:
                                this.addEditTimes('monday', time.opening_hour, time.closing_hour);
                                break;
                            case 2:
                                this.addEditTimes('tuesday', time.opening_hour, time.closing_hour);
                                break;
                            case 3:
                                this.addEditTimes('wednesday', time.opening_hour, time.closing_hour);
                                break;
                            case 4:
                                this.addEditTimes('thursday', time.opening_hour, time.closing_hour);
                                break;
                            case 5:
                                this.addEditTimes('friday', time.opening_hour, time.closing_hour);
                                break;
                            default:
                                break;
                        }
                    });
                }
            }
          });
       
    }

    addEditTimes(day: any, open: any, close: any) {
        if (open.split(':')[0].length === 1) {
            open = '0' + open
        }
        if (close.split(':')[0].length === 1) {
            close = '0' + close
        }
        if (open.split(':')[1].length === 1) {
            if (Number(open.split(':')[1]) < 5) {
                open = open + '0'
            }
            else {
                open.split(':')[1] = '0' + open.split(':')[1];
            }
        }
        if (close.split(':')[1].length === 1) {
            if (Number(close.split(':')[1]) < 5) {
                close = close + '0'
            }
            else {
                close.split(':')[1] = '0' + close.split(':')[1];
            }
        }
        let add: FormArray;
        add = this.formCreate.get(day) as FormArray;
        add.push(this.formBuilder.group({
            opening_hour: [open],
            closing_hour: [close]
        }));
    }

    
  backList() {
    this.router.navigateByUrl('/queue/configuration/subjects/list');
  }

}

