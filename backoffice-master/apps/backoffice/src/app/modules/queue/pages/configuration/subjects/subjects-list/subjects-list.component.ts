import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { NzMessageService } from 'ng-zorro-antd/message';
import { SubjectServiceQueue } from "@fi-sas/backoffice/modules/queue/services/subjects.service";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";

@Component({
    selector: 'fi-sas-subjects-list-queue',
    templateUrl: './subjects-list.component.html',
    styleUrls: ['./subjects-list.component.less']
})
export class SubjectsListComponent extends TableHelper implements OnInit {

    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        private modal: NzModalService,
        uiService: UiService,
        private message: NzMessageService,
        private subjectServiceQueue: SubjectServiceQueue,
        private authService: AuthService
    ) {
        super(uiService, router, activatedRoute);
    }

    ngOnInit() {
        this.initTableData(this.subjectServiceQueue);
    }


    edit(id: number) {
        if(!this.authService.hasPermission('queue:subjects:update')){
            return;
          }
        this.router.navigateByUrl('/queue/configuration/subjects/update/' + id);
    }
    
    inativeConfirm(id): void {
        if(!this.authService.hasPermission('queue:subjects:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer inativar esta fila?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.inativeService(id); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    ativeConfirm(id): void {
        if(!this.authService.hasPermission('queue:subjects:update')){
            return;
          }
        this.modal.confirm({
            nzTitle: 'Tem a certeza que quer ativar esta fila?',
            nzOkText: 'Sim',
            nzOkType: 'danger',
            nzOnOk: () => { this.activeService(id); },
            nzCancelText: 'Não',
            nzOnCancel: () => { }
        });
    }

    inativeService(id: any) {
        let body: any = {
            active: true
        };
        this.loading = true;
        this.subjectServiceQueue.desativeSubject(id)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                this.message.create('success', 'Fila desativa com sucesso');
                this.initTableData(this.subjectServiceQueue);
            });
    }

    activeService(id: any) {
        let body: any = {
            active: true
        };
        this.loading = true;
        this.subjectServiceQueue.patch(id, body)
            .pipe(first(), finalize(()=>this.loading = false))
            .subscribe(results => {
                this.message.create('success', 'Fila ativa com sucesso');
                this.initTableData(this.subjectServiceQueue);
            });
    }


}