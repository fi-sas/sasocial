import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectFormComponent } from './subjects-form/subjects-form.component';
import { SubjectsListComponent } from './subjects-list/subjects-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: SubjectsListComponent,
    data: { breadcrumb: 'Filas', title: 'Listar', scope: 'queue:subjects:read' },
  },
  {
    path: 'update/:id',
    component: SubjectFormComponent,
    data: { breadcrumb: 'Fila', title: 'Editar', scope: 'queue:subjects:create' },
  },
  {
    path: 'create',
    component: SubjectFormComponent,
    data: { breadcrumb: 'Fila', title: 'Criar', scope: 'queue:subjects:create' },
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubjectsRoutingModule { }
