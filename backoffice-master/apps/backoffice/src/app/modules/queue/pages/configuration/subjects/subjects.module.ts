import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

import { SubjectFormComponent } from './subjects-form/subjects-form.component';
import { SubjectsListComponent } from './subjects-list/subjects-list.component';
import { SubjectsRoutingModule } from './subjects-routing.module';

@NgModule({
  declarations: [
    SubjectFormComponent,
    SubjectsListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SubjectsRoutingModule
  ],
})
export class SubjectsModule { }
