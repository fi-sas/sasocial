import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: 'fi-queue-view-history',
    templateUrl: './view-history.component.html',
    styleUrls: ['./view-history.component.less']
})
export class ViewHistoryComponent implements OnInit {
    @Input() data;
    
    ngOnInit() {
        
    }
}