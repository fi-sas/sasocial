import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { TicketDeskServiceQueue } from "../../../services/ticket-desk.service";
import { finalize, first } from "rxjs/operators";
import { TicketSubjectServiceQueue } from "../../../services/subject-tickets.service";
import { DeskManagementServiceQueue } from "../../../services/desk-management.service";
import { NzMessageService } from "ng-zorro-antd/message";
import { TransferTicketServiceQueue } from "../../../services/transfer-ticket.service";
import { BehaviorSubject, Subject, timer } from 'rxjs';
import { UsersService } from "@fi-sas/backoffice/modules/users/modules/users_users/services/users.service";
import { OperatorServiceQueue } from "../../../services/operators.service";
import { SubjectsOpenQueue } from "../../../services/subjects-open.service";
import { SubjectServiceQueue } from "../../../services/subjects.service";

@Component({
  selector: 'fi-queue-desk-management',
  templateUrl: './desk-management.component.html',
  styleUrls: ['./desk-management.component.less']
})
export class DeskManagementQueueComponent extends TableHelper implements OnInit, OnDestroy {

  deskSelectedAssociation;
  pagination: any;
  deskPaused = false;
  ticketList: any[] = [];
  subjectList: any[] = [];
  historyList: any[] = [];
  actionDesk = '';
  studentList: any;
  noOperator: boolean;
  searchChange$ = new BehaviorSubject('');
  optionList: string[] = [];
  selectedUser?: string;
  tranferOptionsList: any[] = [];
  loadingTicketsTransfer = false;
  selectedTicket: any;
  deskStartForm = new FormGroup({
    description: new FormControl(null)
  });
  ticketsLoading = false;
  selectedNote: any;
  loadingTicket = false;
  timeOccurred = 0;
  timeOccurredInterval = null;
  array_id_subjects = [];
  userTransferForm = new FormGroup({
    id: new FormControl(null, Validators.required)
  });
  userModel: UserModel;
  proccessLoading = false;
  refresh;
  isVisible = false;
  isVisible_name = '';
  actualXsPage = 0;
  isConfirmLoading = false;
  userSelect: any = {};
  modalGeston = false;
  idService: number;
  gestonLoading = false;
  listSubjectsGeston = [];
  dataSendGeston = [];
  modalRetreat = false;
  loadingRetreat = false;
  ticketsRetreat = [];
  readonly MAX_TEXTAREA_CHARS = 5000;
  modalNotify = false;
  notifyType: string = null; 
  isNotifing= false;

  constructor(
    private transferTicketServiceQueue: TransferTicketServiceQueue,
    private message: NzMessageService,
    private usersService: UsersService,
    private ticketDeskServiceQueue: TicketDeskServiceQueue,
    uiService: UiService,
    private deskManagementServiceQueue: DeskManagementServiceQueue,
    private ticketSubjectServiceQueue: TicketSubjectServiceQueue,
    private subjectServiceQueue:SubjectServiceQueue,
    private subjectsOpenQueue: SubjectsOpenQueue,
    router: Router,
    private authService: AuthService,
    activatedRoute: ActivatedRoute,
    private operatorService: OperatorServiceQueue,
    private userService: UsersService
  ) {
    super(uiService, router, activatedRoute);

  }

  ngOnDestroy(): void {
    if (this.timeOccurredInterval) {
      clearInterval(this.timeOccurredInterval);
    }
    if (this.refresh) {
      clearInterval(this.refresh);
    }
  }

  ngOnInit() {
    this.getUserLogged();
    this.refresh = setInterval(() => {
      this.getTickets(1);
    }, 10000);
  }

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }
  getTicketNumber(id) {
    let aux;
    if (id < 10) {
      aux = '00' + id
    } else if (id < 100) {
      aux = '0' + id
    }
    return aux
  }

  getTicketStudent() {
    this.usersService.list(1, -1, null, null, { profile_id: 1 })
      .subscribe(arg => {
        this.studentList = arg.data;
      });
  }


  selectTicket(ticket) {
    this.selectedTicket = ticket;
    this.historyList = [];
    if (this.selectedTicket && this.selectedTicket.initial_time != null) {
      this.timeOccurred = Date.now() - new Date(this.selectedTicket.initial_time).getTime();
      if (this.timeOccurred < 0) {
        this.selectedTicket.initial_time = new Date();
        this.timeOccurred = 0;
      }
      clearInterval(this.timeOccurredInterval);
      this.timeOccurredInterval = setInterval(() => {
        this.timeOccurred = Date.now() - new Date(this.selectedTicket.initial_time).getTime();
        if (this.timeOccurred < 0) {
          this.timeOccurred = 0;
        }
      }, 1000);

    }

    this.deskStartForm.controls.description.setValue(ticket.notes ? ticket.notes : '');
    if (this.selectedTicket.student_number) {
      this.getHistoryStudent(this.selectedTicket.student_number, 'student_number');
    };
  }

  getTime(time) {
    let aux = time.split('T')[1];
    aux = aux.split(':')[0] + ':' + aux.split(':')[1];
    return aux;
  }

  getHistoryStudent(id, type) {
    this.ticketDeskServiceQueue.getHistoryStudent(id, type, -1, 1)
      .subscribe(arg => {
        this.historyList = arg.data;
      });
  }

  pauseDesk() {
    if (this.deskPaused) {
      this.deskManagementServiceQueue.endPauseDesk(this.deskSelectedAssociation.desk_id).subscribe(data => {
        if (data.data[0].desk[0].actual_state == false) {
          this.actionDesk = 'Pause';
        } else if (data.data[0].desk[0].actual_state == true) {
          this.actionDesk = 'Operating';
        }
        this.deskPaused = !this.deskPaused;
      });
    } else {
      this.deskManagementServiceQueue.startPauseDesk(this.deskSelectedAssociation.desk_id).subscribe(data => {
        if (data.data[0].desk[0].actual_state == false) {
          this.actionDesk = 'Pause';
        } else if (data.data[0].desk[0].actual_state == true) {
          this.actionDesk = 'Operating';
        }
        this.deskPaused = !this.deskPaused;
      });
    }
  }



  getUserLogged() {
    this.userModel = this.authService.getUser();
    setTimeout(() => {
      this.getSubjects();
    }, 1000);

  }

  getTickets(pageNumber: number) {
    this.ticketsLoading = true;
    this.ticketDeskServiceQueue.getTicket(this.array_id_subjects, pageNumber, 5, this.deskSelectedAssociation.desk_id).pipe(
      first(), finalize(() => this.ticketsLoading = false)).subscribe(results => {
        this.ticketList = results.data;
        this.pagination = results.link;
      });
  }

  getSubjects() {
    this.loadingTicket = true;
    this.operatorService.list(0, 1, null, null, { user_id: this.userModel.id, active: true }).pipe(first()).subscribe(desk_operator => {
      if (desk_operator.data.length == 0) {
        this.noOperator = true;
      } else {
        this.noOperator = false;
        this.ticketSubjectServiceQueue.getSubjects(desk_operator.data[0].id).pipe(first(), finalize(() => this.loadingTicket = false)).subscribe(results => {
          this.subjectList = results.data;
          this.deskSelectedAssociation = this.subjectList[0];
          if (this.subjectList.length > 0) {
            if (this.subjectList[0].desk[0].actual_state == false && this.subjectList[0].operating == true) {
              this.actionDesk = 'Pause';
              this.deskPaused = true;
            } else if (this.subjectList[0].desk[0].actual_state == true && this.subjectList[0].operating == true) {
              this.actionDesk = 'Operating';
              this.deskPaused = false;
            } else if (this.subjectList[0].desk[0].actual_state == false && this.subjectList[0].operating == false) {
              this.actionDesk = 'Closed';
            }
            this.array_id_subjects = [];
            this.subjectList.forEach(element => {
              this.array_id_subjects.push(element.subject_id);
            });
            this.getTickets(1);
          } else {
            this.message.create('error', `Inicie um Balcão`);
            this.router.navigateByUrl('/queue/desk/desk-start');
          }


        });
      }
    });

  }

  endDesk() {
    this.deskManagementServiceQueue.closeDesk(this.deskSelectedAssociation.desk_id, this.deskSelectedAssociation.operator_id).subscribe(data => {
      this.actionDesk = 'Closed';
    });
  }

  startDesk() {
    this.deskManagementServiceQueue.startDesk(this.deskSelectedAssociation.desk_id, this.deskSelectedAssociation.operator_id).subscribe(data => {
      this.actionDesk = 'Operating';
      this.deskPaused = false;
    });
  }

  submit() {
    if (!this.selectedTicket) {
      this.message.create('error', `Por favor selecione um Ticket`);
    } else {
      if (this.deskStartForm.controls.description.value) {
        this.proccessLoading = true;
        this.ticketDeskServiceQueue.saveTicketNote(this.selectedTicket.id, this.deskStartForm.controls.description.value).pipe(first(),
          finalize(() => this.proccessLoading = false))
          .subscribe(result => {
            this.selectedTicket.notes = this.deskStartForm.controls.description.value;
            this.message.create('success', `Ticket gravado com sucesso`);
          });

      } else {
        this.message.create('error', `Digite uma nota`);
      }
    }

  }

  showModal(modal: string, item?: any): void {

    if (this.selectedTicket) {
      this.isVisible_name = modal;
      this.isVisible = true;
      if (modal === 'transfer') {
        this.loadingTicketsTransfer = true;
        this.userTransferForm = new FormGroup({
          id: new FormControl(null, Validators.required)
        });
        this.transferTicketServiceQueue.list(1, -1, null, null, { active: true, withRelated: 'service, translations' }).pipe(first(), finalize(() => this.loadingTicketsTransfer = false)).subscribe(results => {
          this.tranferOptionsList = results.data.filter(subject => subject.id != this.selectedTicket.subject_id);
        });
      } else if (modal === 'user') {
        this.userSelect = null
        this.getTicketStudent();
      } else if (modal === 'note') {
        this.selectedNote = item;
      }
    } else {
      this.message.create('error', 'Nenhum ticket selecionado');
    }
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    this.isVisible_name = '';
    this.isVisible = false;
    this.isConfirmLoading = false;
  }

  handleCancel(): void {
    this.isVisible = false;
    this.isVisible_name = '';
  }

  callTicket() {
    this.ticketDeskServiceQueue.callTicket(this.selectedTicket.id, this.subjectList[0].desk_id).subscribe(result => {
      this.selectedTicket.call_times = result.data[0].call_times;
      this.selectedTicket.status = result.data[0].status;
    });
  }

  endTicket() {
    this.ticketDeskServiceQueue.endTicket(this.selectedTicket.id).subscribe(result => {
      this.isVisible = false;
      this.isVisible_name = '';
      this.selectedTicket = null;
      this.getUserLogged();
      this.historyList = [];
      clearInterval(this.timeOccurredInterval);
    });
  }

  didntComeTicket() {
    this.ticketDeskServiceQueue.didntComeTicket(this.selectedTicket.id).subscribe(result => {
      this.selectedTicket = null;
      this.isVisible = false;
      this.isVisible_name = '';
      this.historyList = [];
      this.getUserLogged();
    });
  }

  beginTicket() {
    this.ticketDeskServiceQueue.beginTicket(this.selectedTicket.id).subscribe(result => {
      this.isVisible = false;
      this.isVisible_name = '';
      this.selectTicket(result.data[0]);
      this.getTickets(1);
    });
  }


  checkDisable() {
    if (this.selectedTicket && this.actionDesk == 'Operating') {
      return false;
    }
    return true;
  }

  checkBtnsControl(btn: string) {
    if (this.selectedTicket) {
      if (btn === 'identify') {
        if (!this.selectedTicket.student_number) {
          return false;
        }
      } else if (btn === 'call') {
        if (this.selectedTicket.status === 'EM_ATENDIMENTO') {
          return false;
        }
      }
    }
    return true;
  }

  checkMenuTicket() {
    if (this.selectedTicket) {
      if (this.selectedTicket.status === 'EM_ATENDIMENTO' && this.selectedTicket.attended_by_id == this.userModel.id) {
        return true;
      }
    }
    return false;
  }

  saveTicketUser() {
    if (this.userSelect) {
      this.userService.read(this.userSelect).pipe(first()).subscribe((data) => {
        this.ticketDeskServiceQueue.saveUser(this.selectedTicket.id, data.data[0].student_number, data.data[0].email).subscribe(result => {
          this.selectTicket(result.data[0]);
          this.getTickets(1);
        });
        this.handleCancel();
      })
    } else {
      this.message.create('error', 'Preencha o campo obrigatório');
    }


  }


  tranferTicketWhere() {
    if (this.tranferOptionsList.length > 0) {
      if (this.userTransferForm.valid) {
        this.ticketDeskServiceQueue.tranferTicketWhere(this.selectedTicket.id, this.userTransferForm.controls.id.value).subscribe(result => {
          this.getUserLogged();
          clearInterval(this.timeOccurredInterval);
          this.selectedTicket = null;
          this.historyList = [];
          this.message.create('success', 'Ticket transferido com sucesso');
        });
        this.handleCancel();
      } else {
        this.message.create('error', 'Selecione uma fila');
      }
    } else {
      this.message.create('error', 'Não existem filas disponíveis para transferir');
      this.handleCancel();
    }

  }

  getTicketNotResponded() {
    if (this.selectedTicket.call_times) {
      const auxLength = this.selectedTicket.call_times.length;
      let aux;
      if (auxLength > 0) {
        aux = 'Não respondeu às ';
      }
      let dateToString: Date;
      for (let i = auxLength - 1; i >= 0; i--) {
        aux += ' -';
        dateToString = new Date(this.selectedTicket.call_times[i])
        aux += ' ' + dateToString.getHours() + ':' + dateToString.getMinutes();
      }
      return aux;
    }
    return '';
  }


  paginationController(direction: number) {
    if (this.pagination) {
      if (this.pagination.totalPages >= this.pagination.page + direction &&
        1 <= this.pagination.page + direction) {
        this.pagination.page = this.pagination.page + direction;
        this.getTickets(this.pagination.page)
      }
    }
  }

  getNotes(note): string {
    return note.length > 30 ? (note.substring(0, 50) + '...') : note;
  }

  goViewModalGeston() {
    this.modalGeston = true;
    this.gestonLoading = true;
    this.subjectServiceQueue.list(1,-1,null,null, {
      id:this.subjectList[0].subject_id
    }).pipe(first()).subscribe((data)=> {
      if(data.data.length>0) {
        this.idService = data.data[0].service_id;
        this.subjectsOpenQueue.getListSubjectsOpen(this.idService).pipe(first(),finalize(()=>this.gestonLoading = false)).subscribe((open)=> {
          this.listSubjectsGeston = open.data;
        })
      }
    })
  }

  onChangeSub(event) {
    if (event.length > 0) {
      this.dataSendGeston = event;
    } else {
      this.dataSendGeston = [];
    }
  }

  saveGestonSubject(operating: boolean) {
    this.isConfirmLoading = true;
    let aux = [];
    this.dataSendGeston.forEach((value)=> {
      let auxSub = {
        subject_id: value
      }
      aux.push(auxSub);
    })

    let sendValues = {
      operating: operating,
      desk_id: this.deskSelectedAssociation.desk_id,
      operator_id: this.deskSelectedAssociation.operator_id,
      subjects: aux

    }
    this.subjectsOpenQueue.saveGestonSubject(sendValues).pipe(first(),finalize(()=> this.isConfirmLoading = false)).subscribe((data)=> {
      if(operating) {
        this.message.create('success', 'Fila adicionada com sucesso');
      }else {
        this.message.create('success', 'Fila removida com sucesso');
      }
      this.getUserLogged();
      this.modalGeston = false;
    })
  }

  openModalRetreatTicket(){
    this.modalRetreat = true;
    this.loadingRetreat = true;
    this.ticketDeskServiceQueue.get5RecallTicket(this.array_id_subjects, this.deskSelectedAssociation.desk_id).pipe(
      first(), finalize(() => this.loadingRetreat = false)).subscribe(results => {
        this.ticketsRetreat = results.data;
      });
   
  }

  recall(ticket) {
    this.ticketDeskServiceQueue.saveRecallTicket(ticket.id).pipe(first()).subscribe(()=> {
      this.modalRetreat = false;
      this.getTickets(1);
    })
  }

  openModalNotifyUsers(){
    this.modalNotify = true;
  }

  notifyUsers(){
    this.isNotifing= true;
    if(this.deskSelectedAssociation.desk_id){
      this.deskManagementServiceQueue.broadcastNotification(this.deskSelectedAssociation.desk_id, this.notifyType).pipe(first(), finalize(() => this.isNotifing = false)).subscribe( response => {
        this.modalNotify = false;
        this.notifyType = null;
        if(response.data){
          this.message.create('success', `Notificação enviada com sucesso`);
        }else{
          this.message.create('error', 'Erro no envio da notificação');
        }
      }); 
    }
  }

}
