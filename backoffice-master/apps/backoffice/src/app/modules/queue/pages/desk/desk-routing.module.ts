import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeskManagementQueueComponent } from './desk-management/desk-management.component';
import { DeskStartQueueComponent } from './desk-start/desk-start.component';
import { HistoryQueueComponent } from './history/history.component';
import { TicketsAppointmentsComponent } from './tickets-appointments/tickets-appointments.component';

const routes: Routes = [
  { path: '', redirectTo: 'desk-management', pathMatch: 'full' },
  {
    path: 'desk-management',
    component: DeskManagementQueueComponent,
    data: { breadcrumb: 'Operar Balcão', title: 'Operar Balcão'},
    resolve: { }
  },
  {
    path: 'desk-start',
    component: DeskStartQueueComponent,
    data: { breadcrumb: 'Iniciar Balcão', title: 'Iniciar Balcão'},
    resolve: { }
  },
  {
    path: 'history',
    component: HistoryQueueComponent,
    data: { breadcrumb: 'Histórico', title: 'Histórico'},
    resolve: { }
  },
  {
    path: 'tickets-appointments',
    component: TicketsAppointmentsComponent,
    data: { breadcrumb: 'Senhas / Marcações', title: 'Senhas / Marcações'},
    resolve: { }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeskQueueRoutingModule { }
