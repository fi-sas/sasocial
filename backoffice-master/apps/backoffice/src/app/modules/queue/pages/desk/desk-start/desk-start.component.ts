import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/backoffice/core/services/ui-service.service";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { finalize, first } from "rxjs/operators";
import { DeskManagementResponseModelQueue } from "../../../models/desk-management.model";
import { deskModelQueue, ticketsModelQueue } from "../../../models/desk.model";
import { GlobalConfigurationModelQueue, subjectId } from "../../../models/global-configuration.model";
import { ServiceModelQueue } from "../../../models/service.model";
import { SubjectModelQueue } from "../../../models/subject.model";
import { DeskManagementServiceQueue } from "../../../services/desk-management.service";
import { DeskServiceQueue } from "../../../services/desk.service";
import { GlobalConfigurationService } from "../../../services/global-configuration.service";
import { OperatorServiceQueue } from "../../../services/operators.service";
import { ServicesServiceQueue } from "../../../services/services.service";
import { TicketsServiceQueue } from "../../../services/tickets.service";

@Component({
    selector: 'fi-queue-desk-start',
    templateUrl: './desk-start.component.html',
    styleUrls: ['./desk-start.component.less']
})
export class DeskStartQueueComponent extends TableHelper implements OnInit {
    servicesLoading: boolean = false;
    subjectsLoading: boolean = false;
    deskLoading: boolean = false;
    loading: boolean = false;
    loadingStartDesk: boolean = false;
    errorForm: string = '';
    userModel: UserModel;
    operatorId: number;
    formUpdate: FormGroup;
    services: ServiceModelQueue[] = [];
    subjects: SubjectModelQueue[] = [];
    desks: deskModelQueue[] = [];
    noContinue: boolean = false;
    tickets: ticketsModelQueue[] = [];

    constructor(private formBuilder: FormBuilder,
        private globalConfigurationService: GlobalConfigurationService,
        private deskManagementServiceQueue: DeskManagementServiceQueue,
        private servicesServiceQueue: ServicesServiceQueue, 
        private ticketsServiceQueue: TicketsServiceQueue,
        private authService: AuthService,
        private operatorServiceQueue: OperatorServiceQueue,
        private deskServiceQueue: DeskServiceQueue, uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute) {
        super(uiService, router, activatedRoute);
        
    }

    ngOnInit() {
        this.formUpdate = this.formBuilder.group({
            service: ['', [Validators.required]],
            subject: ['', [Validators.required]],
            desk: ['', [Validators.required]],
        });

        this.getUserLogged();
        this.getServices();
        this.getDesks();
        this.getTickets();
        this.initTableData(this.deskManagementServiceQueue);
    }

    getUserLogged() {
        this.authService.getUserObservable().subscribe(user => {
            this.userModel = user;
        });
        this.userModel = this.authService.getUser();
        this.getOperator();
    }

    validUserId(idOperator:number){
        if(this.userModel.id == idOperator){
            return true;
        }else{
            return false;
        }
    }

    getServices() {
        this.servicesLoading = true;
        this.servicesServiceQueue.list(1, -1, null, null, {
            withRelated: 'subjects,translations',
            active: true
        }).pipe(
            first(), finalize(() => this.servicesLoading = false)
        ).subscribe(results => {
            this.services = results.data;
        });
    }

    getSubjects(event) {
        this.formUpdate.get('subject').setValue(null);
        this.services.filter((fil) => {
            if (fil.id == event) {
                this.subjects = fil.subjects;
            }
        })
        this.errorForm = '';
        if (this.subjects.length == 0) {
            this.noContinue = true;
        } else {
            this.noContinue = false;
        }
    }

    getDesks() {
        this.deskLoading = true;
        this.deskServiceQueue.list(1, -1, null, null, {
            active: true,
            sort: 'name'
        }).pipe(
            first(),
            finalize(() => this.deskLoading = false)).subscribe(results => {
                this.desks = results.data;
            });
    }

    getOperator() {
        this.operatorServiceQueue.list(1, -1, null, null, {
            user_id: this.userModel.id
        })
            .pipe(first())
            .subscribe(resis => {
                if(resis.data.length>0) {
                    this.operatorId = resis.data[0].id;
                }
         
            });
    }

    getTickets(){
        this.ticketsServiceQueue.list(1, -1)
            .pipe(first())
            .subscribe(resis => {
                this.tickets = resis.data;
            });
    
    }

    update() {
        this.loadingStartDesk = true;
        if (this.formUpdate.valid) {
            let sendValue = new GlobalConfigurationModelQueue();
            let idSubject = new subjectId();
            idSubject.subject_id = this.formUpdate.get('subject').value;
            sendValue.subjects.push(idSubject);
            sendValue.desk_id = this.formUpdate.get('desk').value;
            this.errorForm = '';
            sendValue.operator_id  = this.operatorId;
            this.globalConfigurationService.startDeskUpdate(sendValue).pipe(
                first(), finalize(() => this.router.navigateByUrl('queue/desk/desk-management'))).subscribe(() => {
                    this.loadingStartDesk = false
                        this.uiService.showMessage(
                            MessageType.success,
                            'Balcão Iniciado'
                        );     
                    
                });
        } else {
            this.loadingStartDesk = false;
            this.errorForm = 'Preencha os campos obrigatórios';
        }
    }

    startDesk(data: DeskManagementResponseModelQueue){
        this.deskManagementServiceQueue.startDesk(data.desk_id, data.operator_id).pipe(
            first()).subscribe(() => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Balcão Iniciado'
                );
            });
            this.router.navigateByUrl('queue/desk/desk-management');
    }

    formatSecondstoHours(value: number){
        let hours = Math.floor(value / 3600); 
        let minutes = value % 60;

        return (hours == 0 ? '00' : hours) + 'h' + ((('' + minutes).split('.')[0]) ? (('' + minutes).split('.')[0]) : minutes) + 'm';
    }

}  