import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ApplicationsService } from '@fi-sas/backoffice/modules/accommodation/modules/aco_applications/services/applications.service';
import { DeskQueueRoutingModule } from './desk-routing.module';
import { NzAutocompleteModule } from 'ng-zorro-antd';
import { DeskManagementQueueComponent } from './desk-management/desk-management.component';
import { DeskStartQueueComponent } from './desk-start/desk-start.component';
import { HistoryQueueComponent } from './history/history.component';
import { ViewHistoryComponent } from './components/view-history/view-history.component';
import { TicketsAppointmentsComponent } from './tickets-appointments/tickets-appointments.component';


@NgModule({
  declarations: [
    DeskStartQueueComponent,
    DeskManagementQueueComponent,
    HistoryQueueComponent,
    TicketsAppointmentsComponent,
    ViewHistoryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DeskQueueRoutingModule,
    NzAutocompleteModule
  ],
  providers: [
    ApplicationsService
  ],
})
export class DeskQueueModule { }
