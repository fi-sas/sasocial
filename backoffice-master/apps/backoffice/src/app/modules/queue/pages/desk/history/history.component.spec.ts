import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HistoryQueueComponent } from './history.component';

describe('HistoryQueueComponent', () => {
  let component: HistoryQueueComponent;
  let fixture: ComponentFixture<HistoryQueueComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [HistoryQueueComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
