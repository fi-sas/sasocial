import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { first } from "rxjs/operators";
import { OperatorModelQueue } from "../../../models/operator.model";
import { ServiceModelQueue } from "../../../models/service.model";
import { SubjectModelQueue } from "../../../models/subject.model";
import { OperatorServiceQueue } from "../../../services/operators.service";
import { ServicesServiceQueue } from "../../../services/services.service";
import { SubjectServiceQueue } from "../../../services/subjects.service";
import * as moment from 'moment';
import { HistoryServiceQueue } from "../../../services/history.service";
import { deskModelQueue } from "../../../models/desk.model";
import { DeskServiceQueue } from "../../../services/desk.service";

@Component({
    selector: 'fi-queue-desk-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.less']
})
export class HistoryQueueComponent extends TableHelper implements OnInit {
    services: ServiceModelQueue[] = [];
    subjects: SubjectModelQueue[] = [];
    operators: OperatorModelQueue[] = [];
    desks: deskModelQueue[] = [];
    filter_date = null;
    currentDate = moment().startOf('day').toDate();

    constructor(private servicesServiceQueue: ServicesServiceQueue,
        private subjectService: SubjectServiceQueue,
        private operatorServiceQueue: OperatorServiceQueue,
        private historyService: HistoryServiceQueue,
        private deskServiceQueue: DeskServiceQueue,
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['withRelated']="attended_by,user";
    }

    ngOnInit() {
        this.getServices();
        this.getSubjects();
        this.getOperator();
        this.getDesks();
        const dayCurrent = moment(this.currentDate).format('YYYY-MM-DD');
        const day2 = moment(this.currentDate.setDate(this.currentDate.getDate() - 5)).format('YYYY-MM-DD');
        this.filter_date = [day2, dayCurrent];
        this.extraFilters['start_date'] = moment(this.filter_date[0]).format('YYYY-MM-DD');
        this.extraFilters['end_date'] = moment(this.filter_date[1]).format('YYYY-MM-DD');
        this.initTableData(this.historyService);
    }

    getServices() {
        this.servicesServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(),
        ).subscribe(results => {
            this.services = results.data;
        });
    }

    getSubjects() {
        this.subjectService.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(),
        ).subscribe(results => {
            this.subjects = results.data;
        });
    }

    getDesks() {
        this.deskServiceQueue.list(1, -1, null, null, {
            active: true,
            sort: 'name'
        }).pipe(
            first()).subscribe(results => {
                this.desks = results.data;
            });
    }

    getOperator() {
        this.operatorServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first()).subscribe(results => {
                this.operators = results.data;
            });
    }

    listComplete() {
        this.extraFilters.service_id = null;
        this.extraFilters.subject_id = null;
        this.extraFilters.operator_id = null;
        this.extraFilters.desk_id = null;
        this.filter_date = null;
        this.extraFilters.start_date = null;
        this.extraFilters.end_date = null;
        this.searchData(true);
    }

    updateFilters() {
        if (this.filter_date) {
            if (this.filter_date.length > 0) {
                const temp = this.filter_date;
                this.extraFilters['start_date'] = moment(temp[0]).format('YYYY-MM-DD');
                this.extraFilters['end_date'] = moment(temp[1]).format('YYYY-MM-DD');
            } else {
                this.filter_date = null;
                this.extraFilters.start_date = null;
                this.extraFilters.end_date = null;
            }

        }
        this.searchData(true);
    }
}