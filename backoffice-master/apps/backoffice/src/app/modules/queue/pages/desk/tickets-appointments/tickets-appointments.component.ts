import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { first } from "rxjs/operators";
import { deskModelQueue } from "../../../models/desk.model";
import { OperatorModelQueue } from "../../../models/operator.model";
import { ServiceModelQueue } from "../../../models/service.model";
import { SubjectModelQueue } from "../../../models/subject.model";
import { DeskServiceQueue } from "../../../services/desk.service";
import { OperatorServiceQueue } from "../../../services/operators.service";
import { ServicesServiceQueue } from "../../../services/services.service";
import { SubjectServiceQueue } from "../../../services/subjects.service";
import { TicketAppointmentServiceQueue } from "../../../services/tickets-subjects.service";
import * as moment from 'moment';

@Component({
    selector: 'fi-queue-tickets-appointments',
    templateUrl: './tickets-appointments.component.html',
    styleUrls: ['./tickets-appointments.component.less']
})
export class TicketsAppointmentsComponent extends TableHelper implements OnInit {
    services: ServiceModelQueue[] = [];
    subjects: SubjectModelQueue[] = [];
    operators: OperatorModelQueue[] = [];
    desks: deskModelQueue[] = [];
    filter_date = null;
    
    constructor(private servicesServiceQueue: ServicesServiceQueue,
        private subjectService: SubjectServiceQueue,
        private operatorServiceQueue: OperatorServiceQueue,
        private ticketAppointment: TicketAppointmentServiceQueue,
        private deskServiceQueue: DeskServiceQueue, uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute) {
        super(uiService, router, activatedRoute);
        this.persistentFilters['withRelated']="attended_by,user";
    }

    ngOnInit() {
        this.getServices();
        this.getSubjects();
        this.getOperator();
        this.getDesks();
        this.initTableData(this.ticketAppointment);
    }

    getServices() {
        this.servicesServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(),
        ).subscribe(results => {
            this.services = results.data;
        });
    }

    getSubjects() {
        this.subjectService.list(1, -1, null, null, {
            active: true
        }).pipe(
            first(),
        ).subscribe(results => {
            this.subjects = results.data;
        });
    }

    getDesks() {
        this.deskServiceQueue.list(1, -1, null, null, {
            active: true,
            sort: 'name'
        }).pipe(
            first()).subscribe(results => {
                this.desks = results.data;
            });
    }

    getOperator() {
        this.operatorServiceQueue.list(1, -1, null, null, {
            active: true
        }).pipe(
            first()).subscribe(results => {
                this.operators = results.data;
            });
    }

    listComplete() {
        this.extraFilters.service_id = null;
        this.extraFilters.subject_id = null;
        this.extraFilters.operator_id = null;
        this.extraFilters.desk_id = null;
        this.filter_date = null;
        this.extraFilters.start_date = null;
        this.extraFilters.end_date = null;
        this.searchData(true);
    }

    updateFilters() {
        if (this.filter_date) {
            if (this.filter_date.length > 0) {
                const temp = this.filter_date;
                this.extraFilters['start_date'] = moment(temp[0]).format('YYYY-MM-DD');
                this.extraFilters['end_date'] = moment(temp[1]).format('YYYY-MM-DD');
            } else {
                this.filter_date = null;
                this.extraFilters.start_date = null;
                this.extraFilters.end_date = null;
            }

        }
        this.searchData(true);
    }

}