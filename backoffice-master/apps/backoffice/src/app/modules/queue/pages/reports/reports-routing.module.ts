import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsQueueComponent } from './reports.component';

const routes: Routes = [
  { path: '', redirectTo: 'listing', pathMatch: 'full' },
  {
    path: 'listing',
    component: ReportsQueueComponent,
    data: { breadcrumb: 'Relatórios', title: 'Relatórios'},
    resolve: { }
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsQueueRoutingModule { }
