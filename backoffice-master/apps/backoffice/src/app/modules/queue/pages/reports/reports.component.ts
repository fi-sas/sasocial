import { Component, OnInit } from "@angular/core";
import { ApexDataLabels, ApexFill, ApexPlotOptions } from "ng-apexcharts";
import {
    ApexNonAxisChartSeries,
    ApexResponsive,
    ApexChart
} from "ng-apexcharts";
import { first } from "rxjs/operators";
import { AvgHourReportsModelQueue } from "../../models/avg-hour-reports.model";
import { SubjectModelQueue } from "../../models/subject.model";
import { ReportsServiceQueue } from "../../services/reports.service";
import { TicketsReportModelQueue } from "../../models/tickets-reports.model";
import { DetailServiceStatus } from "../../models/service-status.model";

export type ChartOptions = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    plotOptions: ApexPlotOptions;
    responsive: ApexResponsive[];
    labels: any;
    dataLabels: ApexDataLabels;
    fill: ApexFill;
};

export type ChartOptionsTickets = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    plotOptions: ApexPlotOptions;
    responsive: ApexResponsive[];
    labels: any;
    dataLabels: ApexDataLabels;
    fill: ApexFill;
};

@Component({
    selector: 'fi-queue-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.less']
})

export class ReportsQueueComponent implements OnInit {
    gradient = false;
    chartOptions: Partial<ChartOptions>;
    chartOptionsTickets: Partial<ChartOptions>;
    dataAllTickets: SubjectModelQueue[];
    averageRunningTime: AvgHourReportsModelQueue;
    avgPause: AvgHourReportsModelQueue;
    averageWaitingTime: AvgHourReportsModelQueue;
    dataTicketsDigitalPaper: TicketsReportModelQueue;
    dataTicketsAttendance: TicketsReportModelQueue;
    dataTicketsCancelednotappear: TicketsReportModelQueue;
    avgAttendanceTime: AvgHourReportsModelQueue;
    avgBetweenCallAttendence: AvgHourReportsModelQueue;
    averageRecoveredTickets: TicketsReportModelQueue;
    totalTicketsTransfered: TicketsReportModelQueue;
    totalTicketsWaiting: TicketsReportModelQueue;
    dataTicketsSoutOfGoal: SubjectModelQueue[];
    totalTicketsAttendance: TicketsReportModelQueue;
    attendanceAboveAvg: TicketsReportModelQueue;
    totalDesksStatus: TicketsReportModelQueue;
    serviceStatusOpen: DetailServiceStatus;
    serviceStatusClose: DetailServiceStatus;

    constructor(private reportsAllTicketsServiceQueue: ReportsServiceQueue) {
    }

    ngOnInit() {
        this.chartOptions = {
            series: [],
            chart: {
                type: "donut"
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: "gradient"
            },
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: true,
                            total: {
                                showAlways: true,
                                show: true
                            }
                        }
                    }
                }
            },
            labels: [],
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 280
                        },
                        legend: {
                            position: "bottom"
                        }
                    }
                }
            ]
        };

        this.chartOptionsTickets = {
            series: [],
            chart: {
                type: "donut"
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: "gradient"
            },
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: true,
                            total: {
                                showAlways: true,
                                show: true
                            }
                        }
                    }
                }
            },
            labels: [],
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 280
                        },
                        legend: {
                            position: "bottom"
                        }
                    }
                }
            ]
        };

        this.getAllTickets();
        this.getAverageRunningTime();
        this.getAvgPause();
        this.getAverageWaitingTime();
        this.getTicketsDigitalPaper();
        this.getTicketsAttendance();
        this.getTicketsCancelednotappear();
        this.getAvgAttendanceTime();
        this.getAvgBetweenCallAttendence();
        this.getAverageRecoveredTickets();
        this.getTotalTicketsTransfered();
        this.getTotalTicketsWaiting();
        this.getTicketsSoutOfGoal();
        this.getTotalTicketsAttendance();
        this.getAttendanceAboveAvg();
        this.getTotalDesksStatus();
        this.getServiceStatus();
    }

    getAllTickets() {
        this.reportsAllTicketsServiceQueue.list(1, -1).pipe(
            first()).subscribe(results => {
                this.dataAllTickets = results.data;
                this.chartOptions.series = this.dataAllTickets.map(t => t.total);
                this.chartOptions.labels = this.dataAllTickets.map(t => {
                    return (t.translations.find(tra => tra.language_id == 3).name).concat(' | ', t.percentage, '%');
                });
            });
    }

    getAverageRunningTime() {
        this.reportsAllTicketsServiceQueue.getAverageRunningTime().pipe(
            first()).subscribe(results => {
                this.averageRunningTime = results.data[0];
            });
    }

    getAvgPause() {
        this.reportsAllTicketsServiceQueue.getAvgPauses().pipe(
            first()).subscribe(results => {
                this.avgPause = results.data[0];
            });
    }

    getAverageWaitingTime() {
            this.reportsAllTicketsServiceQueue.getWaitingTime().pipe(
                first()).subscribe(results => {
                    this.averageWaitingTime = results.data[0];
                });
    }

    getTicketsDigitalPaper() {
        this.reportsAllTicketsServiceQueue.getTicketsDigitalPaper().pipe(
            first()).subscribe(results => {
                this.dataTicketsDigitalPaper = results.data[0];
            });
    }

    getTicketsAttendance() {

        this.reportsAllTicketsServiceQueue.getTicketsAttendance().pipe(
            first()).subscribe(results => {
                this.dataTicketsAttendance = results.data[0];
            });
    }

    getTicketsCancelednotappear() {

        this.reportsAllTicketsServiceQueue.getTicketsCancelednotappear().pipe(
            first()).subscribe(results => {
                this.dataTicketsCancelednotappear = results.data[0];
            });
    }

    getAvgAttendanceTime() {
        this.reportsAllTicketsServiceQueue.getAvgAttendanceTime().pipe(
            first()).subscribe(results => {
                this.avgAttendanceTime = results.data[0];
            });
    }

    getAvgBetweenCallAttendence() {
        this.reportsAllTicketsServiceQueue.getAvgCallAttendence().pipe(
            first()).subscribe(results => {
                this.avgBetweenCallAttendence = results.data[0];
            });
    }

    getAverageRecoveredTickets() {
        this.reportsAllTicketsServiceQueue.getAverageRecoveredTickets().pipe(
            first()).subscribe(results => {
                this.averageRecoveredTickets = results.data[0];
            });
    }

    getTotalTicketsTransfered() {
            this.reportsAllTicketsServiceQueue.getTotalTicketsTransfered().pipe(
                first()).subscribe(results => {
                    this.totalTicketsTransfered = results.data[0];
                });
    }

    getTotalTicketsWaiting() {
            this.reportsAllTicketsServiceQueue.getTotalTicketsWaiting().pipe(
                first()).subscribe(results => {
                    this.totalTicketsWaiting = results.data[0];
                });
    }

    getTicketsSoutOfGoal() {

        this.reportsAllTicketsServiceQueue.getTicketsSoutOfGoal().pipe(
            first()).subscribe(results => {
                this.dataTicketsSoutOfGoal = results.data;
                this.chartOptionsTickets.series = this.dataTicketsSoutOfGoal.map(t => t.out_of_goal);
                this.chartOptionsTickets.labels = this.dataTicketsSoutOfGoal.map(t => t.translations.find(tra => tra.language_id == 3).name);
            });
    }

    getTotalTicketsAttendance() {

            this.reportsAllTicketsServiceQueue.getTotalTicketsAttendance().pipe(
                first()).subscribe(results => {
                    this.totalTicketsAttendance = results.data[0];
                });
    }

    getAttendanceAboveAvg() {
        this.reportsAllTicketsServiceQueue.getAttendanceAboveAvg().pipe(
            first()).subscribe(results => {
                this.attendanceAboveAvg = results.data[0];
            });
    }

    getTotalDesksStatus() {
        this.reportsAllTicketsServiceQueue.getTotalDesksStatus().pipe(
            first()).subscribe(results => {
                this.totalDesksStatus = results.data[0];
            });
    }

    getServiceStatus() {

        this.reportsAllTicketsServiceQueue.getTotalServicesStatus().pipe(
            first()).subscribe(results => {
                this.serviceStatusOpen = results.data[0].services_open;
                this.serviceStatusClose = results.data[0].services_close;
            });
    }

    changeHour(value): string{
        if(value) {
            let aux = ('' + value).split(":");
            return aux[0]+'h'+aux[1]+'m';
        }
        return '';
        
    }
}
