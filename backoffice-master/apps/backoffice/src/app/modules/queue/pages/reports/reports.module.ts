import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ReportsQueueRoutingModule } from './reports-routing.module';
import { ReportsQueueComponent } from './reports.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [
    ReportsQueueComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxChartsModule,
    NgApexchartsModule,
    ReportsQueueRoutingModule
  ],
})
export class ReportsQueueModule { }
