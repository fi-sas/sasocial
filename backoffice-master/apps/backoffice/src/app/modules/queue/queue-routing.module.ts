import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';
import { QueueComponent } from './queue.components';

const routes: Routes = [
    {
        path: '',
        component: QueueComponent,
        children: [
            {
                path: 'initial-page',
                component: InitialPageComponent,

            },
            { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
            {
                path: 'configuration',
                loadChildren: './pages/configuration/configuration.module#ConfigurationQueueModule',
                data: { scope: 'queue:global_configurations' }
            },
            {
                path: 'desk',
                loadChildren: './pages/desk/desk.module#DeskQueueModule',
                data: { scope: 'queue:desks' }
            },
            {
                path: 'reports',
                loadChildren: '../queue/pages/reports/reports.module#ReportsQueueModule',
                data: { scope: 'queue:reports' }
            },
            {
                path: '**',
                component: PageNotFoundComponent,
                data: {
                    breadcrumb: 'Página não encontrada',
                    title: 'Página não encontrada'
                }
            }
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class QueueRoutingModule { }
