import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
    selector: 'fi-sas-queue',
    template: '<router-outlet></router-outlet>'
})
export class QueueComponent implements OnInit {
    dataConfiguration: any;
    constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
        this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();

    }

    ngOnInit() {
        //Acrescentar permissões!!
        this.uiService.setSiderTitle(this.validTitleTraductions(21), 'ordered-list');

        this.uiService.setContentWrapperActive(true);
        this.uiService.removeSiderItems();

        const configuraction = new SiderItem('Configurações');

        const services = configuraction.addChild(new SiderItem('Serviços', '', '', 'queue:services'));
        services.addChild(new SiderItem('Criar', '', '/queue/configuration/services/create', 'queue:services:create'));
        services.addChild(new SiderItem('Listar', '', '/queue/configuration/services/list', 'queue:services:read'));

        const subjects = configuraction.addChild(new SiderItem('Filas', '', '', 'queue:subjects'));
        subjects.addChild(new SiderItem('Criar', '', '/queue/configuration/subjects/create', 'queue:subjects:create'));
        subjects.addChild(new SiderItem('Listar', '', '/queue/configuration/subjects/list', 'queue:subjects:read'));

        const desks = configuraction.addChild(new SiderItem('Balcões', '', '', 'queue:desks'));
        desks.addChild(new SiderItem('Criar', '', '/queue/configuration/desks/create', 'queue:desks:create'));
        desks.addChild(new SiderItem('Listar', '', '/queue/configuration/desks/list', 'queue:desks:read'));

        const desk_operators = configuraction.addChild(new SiderItem('Operador Balcão', '', '', 'queue:desk_operators'));
        desk_operators.addChild(new SiderItem('Criar', '', '/queue/configuration/operators/create', 'queue:desk_operators:create'));
        desk_operators.addChild(new SiderItem('Listar', '', '/queue/configuration/operators/list', 'queue:desk_operators:read'));


        const conf_global = configuraction.addChild(new SiderItem('Assoc. Operador/Balcão', '', '', 'queue:global_configurations'));
        conf_global.addChild(new SiderItem('Criar', '', '/queue/configuration/global-configurations/create', 'queue:global_configurations'));
        conf_global.addChild(new SiderItem('Listar', '', '/queue/configuration/global-configurations/list', 'queue:global_configurations'));

        this.uiService.addSiderItem(configuraction);

        const operator = new SiderItem('Operar Balcão');
        operator.addChild(new SiderItem('Operar', '', '/queue/desk/desk-management'));
        operator.addChild(new SiderItem('Iniciar', '', '/queue/desk/desk-start'));
        operator.addChild(new SiderItem('Senhas / Marcações','', '/queue/desk/tickets-appointments'));
        operator.addChild(new SiderItem('Histórico', '', '/queue/desk/history','queue:ticket_history:read'));
        this.uiService.addSiderItem(operator);

        const reports = new SiderItem('Relatórios');
        reports.addChild(new SiderItem('Listagem', '', '/queue/reports/listing',));
        this.uiService.addSiderItem(reports);

        this.uiService.setSiderActive(true);
    }

    validTitleTraductions(id: number) {
        return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
    }
}
