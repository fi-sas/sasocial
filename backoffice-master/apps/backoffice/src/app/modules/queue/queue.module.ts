import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "@fi-sas/backoffice/shared/shared.module";
import { QueueRoutingModule } from "./queue-routing.module";
import { QueueComponent } from "./queue.components";
import { DeskServiceQueue } from "./services/desk.service";
import { GlobalConfigurationService } from "./services/global-configuration.service";
import { OperatorServiceQueue } from "./services/operators.service";
import { ServicesServiceQueue } from "./services/services.service";


@NgModule({
  declarations: [
    QueueComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    QueueRoutingModule
  ],
  providers:[
    ServicesServiceQueue,
    DeskServiceQueue,
    OperatorServiceQueue,
    GlobalConfigurationService

  ]
})
export class QueueModule {}
