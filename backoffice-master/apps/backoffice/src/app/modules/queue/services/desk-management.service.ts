import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { DeskManagementResponseModelQueue } from '../models/desk-management.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DeskManagementServiceQueue extends Repository<DeskManagementResponseModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.GET_MANAGEMENT_SUBJECT_DESK_OPERATORS';
  }

  startDesk(id_desk: number, id_operator: number): Observable<Resource<DeskManagementResponseModelQueue>> {
    const url = this.urlService.get('QUEUE.START_DESK', { desk_id: id_desk, operator_id: id_operator });
    return this.resourceService.create(url, {});
  }

  startPauseDesk(id_desk: number): Observable<Resource<DeskManagementResponseModelQueue>> {
    const url = this.urlService.get('QUEUE.START_PAUSE_DESK') + id_desk;
    return this.resourceService.create(url, {});
  }

  endPauseDesk(id_desk: number): Observable<Resource<DeskManagementResponseModelQueue>> {
    const url = this.urlService.get('QUEUE.END_PAUSE_DESK') + id_desk;
    return this.resourceService.create(url, {});
  }

  closeDesk(id_desk: number, id_operator: number): Observable<Resource<DeskManagementResponseModelQueue>> {
    return this.resourceService.create<DeskManagementResponseModelQueue>(this.urlService.get('QUEUE.END_DESK', { desk_id: id_desk, operator_id: id_operator }), { });
  }

  broadcastNotification(desk_id: number, notification_action: string):Observable<Resource<boolean>> {
    return this.resourceService.create<boolean>(this.urlService.get('QUEUE.BROADCAST_NOTIFICATION', {}), { desk_id, notification_action });
  }
  
}
