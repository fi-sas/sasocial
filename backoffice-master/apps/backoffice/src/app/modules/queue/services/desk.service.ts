import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { deskModelQueue } from '../models/desk.model';


@Injectable({
  providedIn: 'root'
})
export class DeskServiceQueue extends Repository<deskModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'QUEUE.DESKS_ID';
    this.entities_url = 'QUEUE.DESKS';
  }
}
