
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { GlobalConfigurationResponseModelQueue } from '../models/global-configuration.model';

@Injectable({
    providedIn: 'root'
})
export class GlobalConfigurationListService extends Repository<GlobalConfigurationResponseModelQueue>{

    constructor(resourceService: FiResourceService,
        urlService: FiUrlService) {
        super(resourceService, urlService);
        this.entities_url = 'QUEUE.GET_SUBJECT_DESK_OPERATORS';
    }
}
