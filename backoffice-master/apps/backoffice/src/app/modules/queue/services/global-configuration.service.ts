
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { GlobalConfigurationModelQueue } from '../models/global-configuration.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GlobalConfigurationService {

    constructor(private resourceService: FiResourceService,
        private urlService: FiUrlService) {
    }

    desactive(id: number): Observable<Resource<GlobalConfigurationModelQueue>> {
        const url = this.urlService.get('QUEUE.DESACTIVE_SUBJECT_DESK_OPERATORS', { id });
        return this.resourceService.create(url, {});
    }

    active(id: number): Observable<Resource<GlobalConfigurationModelQueue>> {
        const url = this.urlService.get('QUEUE.ACTIVE_SUBJECT_DESK_OPERATORS', { id });
        return this.resourceService.create(url, {});
    }

    update(id: number, data: GlobalConfigurationModelQueue): Observable<Resource<GlobalConfigurationModelQueue>> {
        const url = this.urlService.get('QUEUE.UPDATE_SUBJECT_DESK_OPERATORS', { id });
        return this.resourceService.update(url, data);
    }

    create(data: GlobalConfigurationModelQueue): Observable<Resource<GlobalConfigurationModelQueue>> {
        const url = this.urlService.get('QUEUE.GET_SUBJECT_DESK_OPERATORS');
        return this.resourceService.create(url, data);
    }

    startDeskUpdate(data: GlobalConfigurationModelQueue): Observable<Resource<GlobalConfigurationModelQueue>> {
        const url = this.urlService.get('QUEUE.UPDATE_START_SUBJECT_DESK_OPERATORS');
        return this.resourceService.create(url, data);
    }

    read(id: number): Observable<Resource<GlobalConfigurationModelQueue>> {
        let params = new HttpParams();
        params = params.set('withRelated', 'subject');
        const url = this.urlService.get('QUEUE.UPDATE_SUBJECT_DESK_OPERATORS', { id });
        return this.resourceService.read(url, {
            params
        });
    }
}
