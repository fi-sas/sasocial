import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { OperatorModelQueue } from '../models/operator.model';

@Injectable({
  providedIn: 'root'
})
export class OperatorServiceQueue extends Repository<OperatorModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'QUEUE.OPERATORS_ID';
    this.entities_url = 'QUEUE.OPERATORS';
  }
}
