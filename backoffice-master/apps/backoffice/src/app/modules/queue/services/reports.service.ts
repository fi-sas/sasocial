import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SubjectModelQueue } from '../models/subject.model';
import { Observable } from 'rxjs';
import { TicketsReportModelQueue } from '../models/tickets-reports.model';
import { AvgHourReportsModelQueue } from '../models/avg-hour-reports.model';
import { ServiceStatusQueue } from '../models/service-status.model';


@Injectable({
  providedIn: 'root'
})

export class ReportsServiceQueue extends Repository<SubjectModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.TOTAL_TICKETS';
  }
  
  getAttendanceAboveAvg(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.ATTENDANCEABOVEAVG'));
  }

  getAverageRecoveredTickets(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVERAGERECOVEREDTICKETS'));
  }

  getAverageRunningTime(): Observable<Resource<AvgHourReportsModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVERAGE_RUNNING_TIME'));
  }

  getAvgAttendanceTime(): Observable<Resource<AvgHourReportsModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVGATTENDANCETIME'));
  }

  getAvgCallAttendence(): Observable<Resource<AvgHourReportsModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVGBETWEENCALLATTEDENCE'));
  }

  getAvgPauses(): Observable<Resource<AvgHourReportsModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVG_PAUSES'));
  }

  getTicketsAttendance(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TICKET_ATTENDANCE'));
  }

  getTicketsCancelednotappear(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TICKET_CANCELEDNOTAPPEAR'));
  }

  getTicketsDigitalPaper(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TICKET_DIGITAL_PAPER'));
  }

  getTicketsSoutOfGoal(): Observable<Resource<SubjectModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TICKETSOUTOFGOAL'));
  }

  getTotalDesksStatus(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TOTALDESKSSTATUS'));
  }

  getTotalServicesStatus(): Observable<Resource<ServiceStatusQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TOTALSERVICESSTATUS'));
  }

  getTotalTicketsAttendance(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TOTALTICKETSATTENDANCE'));
  }

  getTotalTicketsTransfered(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TOTALTICKETSTRANSFERED'));
  }

  getTotalTicketsWaiting(): Observable<Resource<TicketsReportModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.TOTALTICKETSACTUALWAITING'));
  }

  getWaitingTime(): Observable<Resource<AvgHourReportsModelQueue>> {
    return this.resourceService.list(this.urlService.get('QUEUE.AVERAGE_WAITING_TIME'));
  }
}