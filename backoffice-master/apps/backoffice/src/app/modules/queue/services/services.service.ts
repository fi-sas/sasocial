import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ServiceModelQueue } from '../models/service.model';


@Injectable({
  providedIn: 'root'
})
export class ServicesServiceQueue extends Repository<ServiceModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'QUEUE.SERVICES_ID';
    this.entities_url = 'QUEUE.SERVICES';
  }

  desativeService(id) {
    const url = this.urlService.get('QUEUE.SERVICES_DESACTIVE', { id });
    return this.resourceService.create(url, {});
  }

}
