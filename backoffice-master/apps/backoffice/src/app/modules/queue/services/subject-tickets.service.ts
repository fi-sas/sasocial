import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ticketsModelQueue } from '../models/desk.model';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TicketSubjectServiceQueue extends Repository<any>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.GET_USER_LOGGED_SUBJECTS';
  }

  getSubjects(id: number): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.GET_USER_LOGGED_SUBJECTS', { id });
    return this.resourceService.list(url, {});
  }

}
