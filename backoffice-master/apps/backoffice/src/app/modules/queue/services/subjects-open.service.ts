import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ticketsModelQueue } from '../models/desk.model';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SubjectsOpenQueue extends Repository<any>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.SUBJECTS_OPEN';
  }

  getListSubjectsOpen(service_id): Observable<Resource<any>> {
    return this.resourceService.list(this.urlService.get('QUEUE.SUBJECTS_OPEN') + '?service_id='+service_id);
  }

  saveGestonSubject(sendValues) {
    return this.resourceService.create<any>(this.urlService.get('QUEUE.SUBJECT_DESK_OPERATORS'), sendValues );
  }

}
