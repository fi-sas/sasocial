import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SubjectModelQueue } from '../models/subject.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectServiceQueue extends Repository<SubjectModelQueue>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entity_url = 'QUEUE.UPDATE_SUBJECT';
    this.entities_url = 'QUEUE.SUBJECTS';
  }

  desativeSubject(id) {
    const url = this.urlService.get('QUEUE.SUBJECT_DESACTIVE', { id });
    return this.resourceService.create(url, {});
  }

}
