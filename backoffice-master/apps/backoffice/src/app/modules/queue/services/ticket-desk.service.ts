import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ticketsModelQueue } from '../models/desk.model';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class TicketDeskServiceQueue extends Repository<ticketsModelQueue>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.TICKET_DESK_GET';
  }

  getTicket(id_array, pageNumber: number, pageSize: number,id_desk): Observable<Resource<any>> {

    let url = this.urlService.get('QUEUE.TICKET_DESK_GET') + '?pageSize=' + pageSize + '&page=' +
    pageNumber + '&desk_id='+ id_desk + '&withRelated=subject';
    if (id_array.length > 0) {
      for (const id of id_array) {
        url = url + '&subjects=' + id;
      }
    }

    return this.resourceService.list(url, {});
  }

  get5RecallTicket(id_array, id_desk): Observable<Resource<any>> {

    let url = this.urlService.get('QUEUE.RECALL_TICKETS') + '?pageSize=' + 5 + '&page=' +
    1 + '&desk_id='+ id_desk + '&withRelated=subject';
    if (id_array.length > 0) {
      for (const id of id_array) {
        url = url + '&subjects=' + id;
      }
    }

    return this.resourceService.list(url, {});
  }

  saveRecallTicket(id): Observable<Resource<any>> {
    let url = this.urlService.get('QUEUE.RECALL');

    return this.resourceService.create(url, {id});
  }

  endTicket(id): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_DESK') +
      id + '/end';
    return this.resourceService.patch(url, {}, {});
  }

  didntComeTicket(id): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_DESK') + id + '/didntcome';
    return this.resourceService.patch(url, {}, {});
  }

  callTicket(id_ticket, id_desk): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_DESK') + id_ticket +
      '/call?desk=' + id_desk;
    return this.resourceService.patch(url, {}, {});
  }

  beginTicket(id): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_DESK') +
      id + '/begin';
    return this.resourceService.patch(url, {}, {});
  }

  /*
  transferTicket(ticket_id, subject_id, offset: number, limit: number): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_TRANSFER_1') +
    '/' + ticket_id +
    this.urlService.get('QUEUE.TICKET_TRANSFER_2') + subject_id +
    '?limit=' + limit + '&offset=' + offset;
    return this.resourceService.patch(url, {}, {}); ??????????????????
  }
*/
  transferTicketOptions(): Observable<Resource<any>> {

    return this.resourceService.list(this.urlService.get('QUEUE.SUBJECTS'));

  }
  /*(): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.SUBJECTS');
    return this.resourceService.list(url+'?query={​​​"active":true}',{}
      );
  }*/

  saveTicketNote(id, newNote): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.SAVE_TICKET_NOTE') + id;
    return this.resourceService.patch(url, { notes: newNote }, {});
  }

  getHistoryStudent(id_search: any, type_id: string, offset: number, limit: number): Observable<Resource<any>> {
    let url = this.urlService.get('QUEUE.GET_STUDENT_TICKET_HISTORY') +
      '?limit=' + limit + '&offset=' + offset;

    if (type_id === 'email') {
      url += '&email=' + id_search;
    } else {
      url += '&student_number=' + id_search;
    }
    url += '&withRelated=subject';

    return this.resourceService.list(url, {});
  }

  getTicketStudent(): Observable<Resource<any>> {
    const url = this.urlService.get('USERS.USERS') + '?query={​​​​​​"profile_id":1}';

    return this.resourceService.list(url, {});
  }

  tranferTicketWhere(ticket_id: any, subject_id: any): Observable<Resource<any>> {
    const url = this.urlService.get('QUEUE.TICKET_TRANSFER') + ticket_id +
      '/transfer?subject_id=' + subject_id;
    return this.resourceService.update(url, {});
  }

  saveUser(id: number, number: string, email: string): Observable<Resource<ticketsModelQueue>> {
    return this.resourceService.patch<ticketsModelQueue>(this.urlService.get('QUEUE.TICKET_DESK') + id, { 'student_number': number , 'email': email });
  }

}
