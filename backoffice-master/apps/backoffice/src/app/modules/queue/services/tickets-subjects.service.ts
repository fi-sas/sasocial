import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { OperatorModelQueue } from '../models/operator.model';

@Injectable({
  providedIn: 'root'
})
export class TicketAppointmentServiceQueue extends Repository<any>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.TICKETS_SUBJECTS';
  }
}
