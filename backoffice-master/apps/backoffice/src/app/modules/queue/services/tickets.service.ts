import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ticketsModelQueue } from '../models/desk.model';


@Injectable({
  providedIn: 'root'
})
export class TicketsServiceQueue extends Repository<ticketsModelQueue>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'QUEUE.GET_TICKETS';
  }

}
