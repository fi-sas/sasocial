import { FileModel } from "../../medias/models/file.model"
import { UserModel } from "../../users/modules/users_users/models/user.model"
import { TemplateModel } from "../modules/templates/models/template.model"

export enum ReportStatus {
  "WAITING", "PROCCESSING", "READY", "FAILED"
}

export class ReportModel {
  id: number;
  name: string;
  status: ReportStatus;
  template_id: number;
  template?: TemplateModel;
  user_id: number;
  user?: UserModel;
  file_id: number;
  file?: FileModel;
  created_at: Date;
  unread: boolean;
  total_unread: number;
}
