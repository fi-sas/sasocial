import { TemplateModel } from "../modules/templates/models/template.model";

export class TemplatePrintModel extends TemplateModel {
    filename: string;
}