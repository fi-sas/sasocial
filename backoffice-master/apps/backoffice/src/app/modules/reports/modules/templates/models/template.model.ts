import { FileModel } from "@fi-sas/backoffice/modules/medias/models/file.model";

export class TemplateModel {
    id: number;
    key: string;
    name: string;
    description: string;
    data: { key: string; value: any };
    options: { key: string; value: any };
    file?: FileModel;
    file_id: number;
}
