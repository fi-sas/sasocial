import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TemplatesService } from '../../services/templates.service';
import { first, finalize } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.less']
})
export class FormTemplateComponent implements OnInit {

  idToUpdate = null;
  loading = false;

  typologyGroup = new FormGroup({
    key: new FormControl('', [Validators.required,trimValidation]),
    name: new FormControl('', [Validators.required,trimValidation]),
    description: new FormControl('', trimValidation),
    data: new FormControl({}, Validators.required),
    options: new FormControl({}, Validators.required),
    file_id: new FormControl(null, Validators.required),
  });

  constructor(
    private uiService: UiService,
    public router: Router,
    private templateService: TemplatesService,
    public activateRoute: ActivatedRoute,

  ) {
  }

  ngOnInit() {
    this.load();
  }

  load() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.templateService.read(this.idToUpdate).subscribe(result => {
          this.typologyGroup.patchValue({
            key: result.data[0].key,
            name: result.data[0].name,
            description: result.data[0].description,
            data: result.data[0].data,
            options: result.data[0].options,
            file_id: result.data[0].file_id,
          });
        });
      }
    });
  }

  submitForm(value: any) {

    for (const i in this.typologyGroup.controls) {
      if (i) {
        this.typologyGroup.controls[i].markAsDirty();
        this.typologyGroup.controls[i].updateValueAndValidity();
      }
    }

    if (this.typologyGroup.valid) {
      this.loading = true;

      if (this.idToUpdate) {
        this.templateService.update(this.idToUpdate, value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['reports', 'templates', 'list']);
            this.uiService.showMessage(MessageType.success, 'Modelo alterado com sucesso');
          });
      } else {
        this.templateService.create(value).pipe(
          first(),
          finalize(() => this.loading = false)).subscribe(results => {
            this.router.navigate(['reports', 'templates', 'list']);
            this.uiService.showMessage(MessageType.success, 'Modelo criado com sucesso');
          });
      }
    }

  }

  returnButton() {
    this.router.navigate(['reports', 'templates', 'list']);
  }

}
