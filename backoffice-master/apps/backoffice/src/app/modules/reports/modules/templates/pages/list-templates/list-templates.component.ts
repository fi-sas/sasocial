import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TemplatesService } from '../../services/templates.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-templates',
  templateUrl: './list-templates.component.html',
  styleUrls: ['./list-templates.component.less']
})
export class ListTemplatesComponent extends TableHelper  implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private templateService: TemplatesService
  ) {
    super(uiService, router, activatedRoute);
    this.sortKey = 'key';
    this.idKey = 'key';
  }

  ngOnInit() {
    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    })

    this.initTableData(this.templateService);
  }

  deleteTemplate(id: number) {
    this.uiService.showConfirm('Eliminar modelo', 'Pretende eliminar este modelo', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.templateService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Modelo eliminado com sucesso');
        });
      }
    });
  }
}
