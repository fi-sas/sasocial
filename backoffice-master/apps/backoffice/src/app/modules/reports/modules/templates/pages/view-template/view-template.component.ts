import { Component, OnInit, Input } from '@angular/core';
import { TemplateModel } from '../../models/template.model';

@Component({
  selector: 'fi-sas-view-template',
  templateUrl: './view-template.component.html',
  styleUrls: ['./view-template.component.less']
})
export class ViewTemplateComponent implements OnInit {

  @Input() data: TemplateModel = null;

  constructor() { }

  ngOnInit() {
  }

}
