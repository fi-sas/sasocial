import { Injectable } from '@angular/core';
import { TemplateModel } from '../models/template.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TemplatesService extends Repository<TemplateModel> {

  entities_url = 'REPORTS.TEMPLATES';
  entity_url = 'REPORTS.TEMPLATES_ID';

constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService)
}


}

