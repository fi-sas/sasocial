import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormTemplateComponent } from './pages/form-template/form-template.component';
import { ListTemplatesComponent } from './pages/list-templates/list-templates.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormTemplateComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'reports:templates:create' }
  },
  {
    path: 'edit/:id',
    component: FormTemplateComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'reports:templates:update' }
  },
  {
    path: 'list',
    component: ListTemplatesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'reports:templates:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplatesRoutingModule { }
