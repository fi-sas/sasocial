import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplatesRoutingModule } from './templates-routing.module';
import { FormTemplateComponent } from './pages/form-template/form-template.component';
import { ListTemplatesComponent } from './pages/list-templates/list-templates.component';
import { ViewTemplateComponent } from './pages/view-template/view-template.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { TemplatesService } from './services/templates.service';

@NgModule({
  declarations: [FormTemplateComponent, ListTemplatesComponent, ViewTemplateComponent],
  imports: [
    CommonModule,
    TemplatesRoutingModule,
    SharedModule
  ],
  providers: [
    TemplatesService
  ]
})
export class TemplatesModule { }
