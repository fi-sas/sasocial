import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-reports',
  template: '<router-outlet></router-outlet>',
})
export class ReportsComponent implements OnInit {
  dataConfiguration: any;
  constructor(
    private uiService: UiService, private configurationsService: ConfigurationGeralService) {
      this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
    }

  ngOnInit() {
    this.uiService.setSiderTitle(this.validTitleTraductions(23), 'file-text');
    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const templates = new SiderItem('Modelos', '', '', 'reports:templates');
    templates.addChild(new SiderItem('Criar', '', '/reports/templates/create', 'reports:templates:create'));
    templates.addChild(new SiderItem('Listar', '', '/reports/templates/list', 'reports:templates:read'));
    this.uiService.addSiderItem(templates);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
