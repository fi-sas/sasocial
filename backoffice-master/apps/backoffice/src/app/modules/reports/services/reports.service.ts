import { Injectable } from '@angular/core';
import { FiUrlService, FiResourceService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ReportModel } from '../models/report.model';
import { interval, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ReportsService extends Repository<ReportModel> {

  entities_url = 'REPORTS.REPORTS';
  entity_url = 'REPORTS.REPORTS_ID';

  constructor(urlService: FiUrlService, resourceService: FiResourceService) {
    super(resourceService, urlService);
  }
}
