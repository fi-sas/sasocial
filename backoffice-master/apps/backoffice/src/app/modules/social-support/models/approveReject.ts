import { ApplicationModel } from "@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model";
import { ExperienceModel } from "@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model";

export class ApproveRejectModel {
  notes: string;
  event: string;
  applications?: ApplicationModel;
  experience?: ExperienceModel;
}

