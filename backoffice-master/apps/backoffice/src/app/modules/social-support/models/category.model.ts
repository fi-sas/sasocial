
export class CategoryModel {
  id: number;
  file_category_id: number;
  name: string;
  description: string;
  created_at?: string;
  updated_at?: string;
}

