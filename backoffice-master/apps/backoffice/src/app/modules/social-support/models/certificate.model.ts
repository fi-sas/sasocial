import { TranslationModel } from "@fi-sas/backoffice/modules/social-support/models/translation.model";
import { CategoryModel } from "@fi-sas/backoffice/modules/social-support/models/category.model";

export class CertificateModel {
  id: number;
  file_category_id: number;
  public: boolean;
  type: string;
  mime_type: string;
  path: string;
  weight: number;
  width: number;
  height: number;
  url: string;
  created_at: string;
  updated_at: string;
  trasnlations: TranslationModel[];
  category: CategoryModel;
}

