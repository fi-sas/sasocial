export type DISPATCH_DECISION = 'ACCEPT' | 'REJECT';

interface ChangeToDispatch<T> {
  event: 'DISPATCH';
  decision: T;
  notes?: string;
}

interface ValidateDispatchDecision {
  decision_dispatch: DISPATCH_DECISION
}

export type SendDispatchData = ChangeToDispatch<string> | ValidateDispatchDecision;
