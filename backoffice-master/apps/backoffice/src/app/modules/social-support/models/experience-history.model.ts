import { UserModel } from "../../users/modules/users_users/models/user.model";

export class ExperienceHistoryModel {
  id?: number;
  experience_id: number;
  status: string;
  user_id: string;
  user: UserModel;
  notes: string;
  created_at: string;
  updated_at: string;
}
