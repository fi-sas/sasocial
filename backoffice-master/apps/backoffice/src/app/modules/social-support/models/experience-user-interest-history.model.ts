import { UserModel } from "../../users/modules/users_users/models/user.model";

export class ExperienceUserInterestHistoryModel {
  id?: number;
  user_interest_id: number;
  status: string;
  user_id: string;
  notes: string;
  created_at: string;
  updated_at: string;
  user: UserModel;
}
