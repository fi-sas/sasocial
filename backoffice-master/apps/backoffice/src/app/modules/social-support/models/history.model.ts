import { UserModel } from "../../users/modules/users_users/models/user.model";

// TODO: this is used by volunteering module. Delete this file when the dependency is removed
export class HistoryModel {
  id?: number;
  experience_id: number;
  status: string;
  user_id: string;
  user: UserModel;
  notes: string;
  created_at: string;
  updated_at: string;
}
