import { ApplicationModel } from "@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model";
import { ExperienceModel } from "@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model";

export enum PaymentGridStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected'
}

export class PaymentGridModel {
  id?: number;
  application_id: number;
  experience_id: number;
  payment_month: number;
  payment_year: number;
  payment_model: string;
  payment_value: number;
  number_hours: number;
  payment_value_iban: number;
  payment_value_ca: number;
  status?: string;
  notes: string;
  created_at?: string;
  updated_at?: string;
  application?: ApplicationModel;
  experience?: ExperienceModel;
}

export class PaymentGridData {
  static payment_model = [
    {value: 'BA', label:'Bolsa Anual', active: true},
    {value: 'VH', label: 'Valor Hora', active: true}
  ];

  static status = [
    {value: 'Pending', label:'Pendente', active: true, color: '#D4B483'},
    {value: 'Approved', label: 'Aprovado', active: true, color: '#0d69dd'},
    {value: 'Rejected', label: 'Rejeitado', active: true, color: '#D0021B'}
  ];
}

