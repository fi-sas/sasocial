
export class TranslationModel {
  id: number;
  language_id: number;
  file_id: number;
  name: string;
  created_at?: string;
  updated_at?: string;
}

