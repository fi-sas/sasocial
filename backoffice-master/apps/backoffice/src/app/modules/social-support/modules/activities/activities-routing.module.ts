import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormActivityComponent } from './pages/form-activity/form-activity.component';
import { ListActivitiesComponent } from './pages/list-activities/list-activities.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormActivityComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'social_scholarship:activities:create' },
  },
  {
    path: 'edit/:id',
    component: FormActivityComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'social_scholarship:activities:update' },
  },
  {
    path: 'list',
    component: ListActivitiesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:activities:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivitiesRoutingModule { }
