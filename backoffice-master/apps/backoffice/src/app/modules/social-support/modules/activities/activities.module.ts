import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesRoutingModule } from './activities-routing.module';
import { FormActivityComponent } from './pages/form-activity/form-activity.component';
import { ListActivitiesComponent } from './pages/list-activities/list-activities.component';
import { ActivitiesService } from './services/activities.service';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    FormActivityComponent,
    ListActivitiesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ActivitiesRoutingModule
  ],
  providers: [
    ActivitiesService
  ]
})
export class ActivitiesModule { }
