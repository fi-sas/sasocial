import { ApplicationModel } from "@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model";

export class ActivityModel {
  id?: number;
  name: string;
  active: boolean;
  created_at?: string;
  updated_at?: string;
  applications?: ApplicationModel[];
  activity?: modelActiv;
}

export class modelActiv {
  name: string;
}

