import { Component, OnInit } from '@angular/core';
import { ActivitiesService } from "@fi-sas/backoffice/modules/social-support/modules/activities/services/activities.service";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { Location } from '@angular/common';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-activity',
  templateUrl: './form-activity.component.html',
  styleUrls: ['./form-activity.component.less']
})
export class FormActivityComponent implements OnInit {

  activityForm = new FormGroup({
    name: new FormControl('', [Validators.required,trimValidation]),
    active: new FormControl(true, [Validators.required]),
  });
  loading = false;
  idToUpdate = null;

  constructor(
    public activitiesService: ActivitiesService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {

    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id') ? this.activatedRoute.snapshot.params.id : null;
    if (this.idToUpdate) {
      this.activitiesService.read(this.idToUpdate).pipe(first()).subscribe(result => {
        this.activityForm.patchValue({
          name: result.data[0].name,
          active: result.data[0].active,
        });
      });
    }

  }


  submitForm(event: any, value: any) {

    if (this.activityForm.valid) {
      this.loading = true;
      if (this.idToUpdate && this.authService.hasPermission('social_scholarship:activities:update')) {
        this.activitiesService.update(this.idToUpdate, value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Atividade atualizada com sucesso');
          this.location.back();
        });
      } else if (this.authService.hasPermission('social_scholarship:activities:create')) {
        this.activitiesService.create(value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Atividade inserida com sucesso');
          this.router.navigate(['social-support', 'activities', 'list']);
        });
      }
    } else {

      for (const i in this.activityForm.controls) {
        if (this.activityForm.controls[i]) {
          this.activityForm.controls[i].markAsDirty();
          this.activityForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton() {
    this.router.navigate(['social-support', 'activities', 'list']);
  }
}
