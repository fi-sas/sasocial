import { Component, OnInit } from '@angular/core';
import { ActivitiesService } from "@fi-sas/backoffice/modules/social-support/modules/activities/services/activities.service";
import { TagComponent } from "@fi-sas/backoffice/shared/components/tag/tag.component";
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-activities',
  templateUrl: './list-activities.component.html',
  styleUrls: ['./list-activities.component.less']
})
export class ListActivitiesComponent extends TableHelper implements OnInit {

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public activityService: ActivitiesService) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {

    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    }, {
      key: 'active',
      label: 'Ativo',
      sortable: true,
      tag: TagComponent.YesNoTag
    })

    this.persistentFilters['withRelated'] = false;
    this.persistentFilters['searchFields'] = 'name';

    this.initTableData(this.activityService);
  }

  deleteActivity(id: number) {

    if (!this.authService.hasPermission('social_scholarship:activities:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar atividade', 'Pretende eliminar esta atividade ?', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.activityService.delete(id).pipe(first()).subscribe(res => {
          this.uiService.showMessage(MessageType.success, "Atividade eliminada com sucesso");
          this.searchData();
        });
      }
    });
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true);
  }
}
