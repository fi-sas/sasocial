import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ActivityModel } from "@fi-sas/backoffice/modules/social-support/modules/activities/models/activity.model";

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService extends Repository<ActivityModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.ACTIVITIES';
    this.entity_url = 'SOCIAL_SUPPORT.ACTIVITIES_ID';
  }
}
