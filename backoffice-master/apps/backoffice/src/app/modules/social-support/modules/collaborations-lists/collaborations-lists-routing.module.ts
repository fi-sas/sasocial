
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaborationsListsComponent } from './pages/collaborations-lists.component';

const routes: Routes = [
  { path: '', redirectTo: 'student', pathMatch: 'full' },
  {
    path: 'student',
    component: CollaborationsListsComponent,
    data: { breadcrumb: 'Lista por Estudante', title: 'Lista por Estudante', scope: 'social_scholarship:experiences:read' },
  },
  {
    path: 'offer',
    component: CollaborationsListsComponent,
    data: { breadcrumb: 'Lista por Oferta', title: 'Lista por Oferta', scope: 'social_scholarship:experiences:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborationsListsRoutingModule { }
