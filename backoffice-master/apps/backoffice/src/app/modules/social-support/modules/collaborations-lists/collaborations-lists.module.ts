import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CollaborationsListsRoutingModule } from './collaborations-lists-routing.module';
import { CollaborationsListsComponent } from './pages/collaborations-lists.component';
import { listOfferSelectedComponent } from './components/list-offer-selected/list-offer-selected.component';
import { listStudentSelectedComponent } from './components/list-student-selected/list-student-selected.component';
import { FormCertificateComponent } from './components/form-certificate/form-certificate.component';
import { FormMonthlyReportComponent } from './components/form-monthly-report/form-monthly-report.component';
import { FormFinalReportComponent } from './components/form-final-report/form-final-report.component';
import { AttendanceListComponent } from './components/attendance-list/attendance-list.component';
import { FormAttendanceComponent } from './components/form-attendance/form-attendance.component';
import { FormAbsenceComponent } from './components/form-absence/form-absence.component';

@NgModule({
  declarations: [
    CollaborationsListsComponent,
    listOfferSelectedComponent,
    listStudentSelectedComponent,
    FormCertificateComponent,
    FormMonthlyReportComponent,
    FormFinalReportComponent,
    AttendanceListComponent,
    FormAttendanceComponent,
    FormAbsenceComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CollaborationsListsRoutingModule
  ],
  entryComponents: [
    listOfferSelectedComponent,
    listStudentSelectedComponent,
    FormCertificateComponent,
    FormMonthlyReportComponent,
    FormFinalReportComponent,
    FormAttendanceComponent,
    FormAbsenceComponent,
  ]

})
export class CollaborationsListsModule { }
