import { Component, Input, OnChanges, OnInit, SimpleChanges  } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AttendancesModel } from '@fi-sas/backoffice/modules/social-support/models/attendances.model';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { AttendancesService } from '../../services/attendances.service';
import { FormAttendanceComponent } from '../form-attendance/form-attendance.component';
import { FormAbsenceComponent } from '../form-absence/form-absence.component';

@Component({
  selector: 'fi-sas-attendance-list',
  templateUrl: './attendance-list.component.html',
  styleUrls: ['./attendance-list.component.less']
})
export class AttendanceListComponent  extends TableHelper implements OnInit, OnChanges {
  @Input() userInterestId: number;

  attendanceFormModal: NzModalRef = null;

  constructor(
    activatedRoute: ActivatedRoute,
    router: Router,
    uiService: UiService,
    private modalService: NzModalService,
    private attendancesService: AttendancesService,
    private authService: AuthService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.userInterestId.previousValue && changes.userInterestId.currentValue && changes.userInterestId.previousValue !== changes.userInterestId.currentValue){
      this.searchData();
    }
  }

  searchData(){
    this.persistentFilters['user_interest_id'] = this.userInterestId;
    super.searchData();
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'n_hours',
        label: 'Horas',
        sortable: true,
      },
      {
        key: 'date',
        label: 'Data de submissão',
        sortable: true,
      },
      {
        key: 'justification_period',
        label: 'Periodo a Justificar',
        sortable: false,
      },
      {
        key: 'justification',
        label: 'Justificação',
        sortable: false,
      },
      {
        key: 'executed_service',
        label: 'Serviço executado',
        sortable: false,
      },
      {
        key: 'file',
        label: 'Anexos',
        sortable: false,
      },
    );

    this.initTableData(this.attendancesService);
  }

  validateAttendanceModal(attendance: AttendancesModel): void {
    if(!this.authService.hasPermission('social_scholarship:attendances:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }
    if(attendance.status != 'PENDING') {
      return;
    }

    this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormAttendanceComponent,
      nzFooter: null,
      nzComponentParams: { attendance },
    })
    .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.searchData();
        }
      });
  }

  rejectAttendanceModal(attendance: AttendancesModel): void {
    if(!this.authService.hasPermission('social_scholarship:attendances:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(attendance.status != 'PENDING') {
      return;
    }

    this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormAbsenceComponent,
      nzFooter: null,
      nzComponentParams: { attendance },
    })
    .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.searchData();
        }
      });
  }
}