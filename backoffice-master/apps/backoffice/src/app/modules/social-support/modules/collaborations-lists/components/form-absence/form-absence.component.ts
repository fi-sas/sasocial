import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AttendancesModel } from '@fi-sas/backoffice/modules/social-support/models/attendances.model';
import { NzModalRef } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { AttendancesService } from '../../../collaborations-lists/services/attendances.service';

@Component({
  selector: 'fi-sas-form-absence',
  templateUrl: './form-absence.component.html',
  styleUrls: ['./form-absence.component.less'],
})
export class FormAbsenceComponent implements OnInit {
  readonly minuteStep: number = 1;

  @Input() attendance = null;

  attendanceLoading = false;
  attendanceForm: FormGroup;

  private stringToDate(dateStr: string): Date | null {
    return dateStr ? new Date(dateStr) : null;
  }

  formatterHours = (value: number) => `${value} h`;

  constructor(
    public attendanceService: AttendancesService,
    private uiService: UiService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {

    this.attendanceForm = new FormGroup({
      initial_time: new FormControl(this.stringToDate(this.attendance.initial_time), Validators.required),
      final_time: new FormControl(this.stringToDate(this.attendance.final_time), Validators.required),
      reject_reason: new FormControl('', [trimValidation]),
    });

  }

  public clearForm() {
    this.attendanceForm.reset({
      n_hours_reject: '',
      reject_reason: ''
    });
  }

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.attendanceLoading = true;
      const data = {
        initial_time : new Date(this.attendanceForm.value.initial_time).toISOString(),
        final_time : new Date(this.attendanceForm.value.final_time).toISOString(),
        reject_reason: this.attendanceForm.value.reject_reason,
      };

      this.attendanceService.reject(this.attendance.id, data)
        .pipe(
          first(),
          finalize(() => (this.attendanceLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Presença alterada com sucesso'
          );
          this.attendanceForm.reset();
          this.close(true);
        });
      } else {
        for (const i in this.attendanceForm.controls) {
          if (this.attendanceForm.controls[i]) {
            this.attendanceForm.controls[i].markAsDirty();
            this.attendanceForm.controls[i].updateValueAndValidity();
          }
        }
      }
  }


  close(state: boolean): void {
    this.modalRef.close(state);
  }
}
