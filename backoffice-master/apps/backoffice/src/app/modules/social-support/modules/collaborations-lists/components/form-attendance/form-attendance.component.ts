import { Component, Input, OnInit } from '@angular/core';
//import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AttendancesModel } from '@fi-sas/backoffice/modules/social-support/models/attendances.model';
import { NzModalRef } from 'ng-zorro-antd';
import * as moment from 'moment';
import { AttendancesService } from '../../../collaborations-lists/services/attendances.service';

@Component({
  selector: 'fi-sas-form-attendance',
  templateUrl: './form-attendance.component.html',
  styleUrls: ['./form-attendance.component.less'],
})
export class FormAttendanceComponent implements OnInit {
  @Input() attendance: AttendancesModel = null;

  attendanceLoading = false;

  constructor(
    public attendanceService: AttendancesService,
    private uiService: UiService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {

  }

  submit() {
    this.attendanceLoading = true;
    this.attendanceService.validate(this.attendance.id)
      .pipe(
        first(),
        finalize(() => (this.attendanceLoading = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          'Presença validada com sucesso'
        );
        this.close(true);
      });
  }


  close(state: boolean): void {
    this.modalRef.close(state);
  }
}
