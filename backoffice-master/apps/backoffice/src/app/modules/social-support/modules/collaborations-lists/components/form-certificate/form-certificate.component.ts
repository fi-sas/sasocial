import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ExperienceModel } from '../../../experiences/models/experience.model';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { UserInterestModel } from '../../../collaborations-lists/models/user-interest.model';
import { UserInterestsService } from '../../../collaborations-lists/services/user-interests.service';


@Component({
  selector: 'fi-sas-form-certificate',
  templateUrl: './form-certificate.component.html',
  styleUrls: ['./form-certificate.component.less'],
})
export class FormCertificateComponent implements OnInit {
  @Input() userInterest: UserInterestModel = null;
  @Input() experience: ExperienceModel = null;

  certificateForm_loading = false;
  certificateForm = new FormGroup({
    certificate_file_id: new FormControl(null, Validators.required),
  });

  loading = false;

  constructor(
    public userInterestService: UserInterestsService,
    private uiService: UiService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {    
    if (this.userInterest) {
      this.setcertificate(this.userInterest);
    }
  }

  setcertificate(userInterest: UserInterestModel) {
    this.certificateForm.patchValue({
      certificate_file_id: this.userInterest.certificate_file_id ? this.userInterest.certificate_file_id : null,
    });
  }

  close(state: boolean): void {
    this.modalRef.close(state);
  }

  generateCertificate() {
    this.loading = true
    this.userInterestService
      .generateCertificate(this.userInterest.id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((results) => {
        this.uiService.showMessage(
          MessageType.success,
          'Certificado gerado com sucesso.'
        );
        this.close(true)
      });
  }

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.certificateForm_loading = true;
      if (this.userInterest) {
        this.userInterestService
          .addCertificate(this.userInterest.id, this.certificateForm.value.certificate_file_id)
          .pipe(
            first(),
            finalize(() => (this.certificateForm_loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'certificado inserido com sucesso.'
            );
            this.certificateForm.reset();
            this.close(true)
          });
      }
    } else {
      for (const i in this.certificateForm.controls) {
        if (this.certificateForm.controls[i]) {
          this.certificateForm.controls[i].markAsDirty();
          this.certificateForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }
}
