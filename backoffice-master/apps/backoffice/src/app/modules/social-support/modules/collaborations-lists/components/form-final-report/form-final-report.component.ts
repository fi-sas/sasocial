import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ExperienceModel } from '../../../experiences/models/experience.model';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { UserInterestModel } from '../../../collaborations-lists/models/user-interest.model';
import { UserInterestsService } from '../../../collaborations-lists/services/user-interests.service';

@Component({
  selector: 'fi-sas-form-final-report',
  templateUrl: './form-final-report.component.html',
  styleUrls: ['./form-final-report.component.less'],
})
export class FormFinalReportComponent implements OnInit {
  @Input() finalReport: UserInterestModel = null;
  @Input() experience: ExperienceModel = null;

  reportForm_loading = false;
  reportForm = new FormGroup({
    report_avaliation: new FormControl(null, [Validators.required]),
  });

  constructor(
    public userInterestService: UserInterestsService,
    private uiService: UiService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {
    if (this.finalReport) {
      this.setReport(this.finalReport);
    }
  }

  setReport(report: UserInterestModel) {
    this.reportForm.patchValue({
      report_avaliation: this.finalReport.report_avaliation ? this.finalReport.report_avaliation : null,
    });
  }

  public clearForm() {
    this.reportForm.reset({
      monthYear: new Date(),
      report: '',
    });
  }

  close(state: boolean): void {
    this.modalRef.close(state);
  }

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.reportForm_loading = true;
      if (this.finalReport) {
        this.userInterestService
          .addFinalReport(this.finalReport.id, this.reportForm.value.report_avaliation)
          .pipe(
            first(),
            finalize(() => (this.reportForm_loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Relatório final enviado com sucesso.'
            );
            this.reportForm.reset();
            this.close(true);
          });
      }
    } else {
      for (const i in this.reportForm.controls) {
        if (this.reportForm.controls[i]) {
          this.reportForm.controls[i].markAsDirty();
          this.reportForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }
}
