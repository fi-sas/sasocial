import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMonthlyReportComponent } from './form-monthly-report.component';

describe('FormMonthlyReportComponent', () => {
  let component: FormMonthlyReportComponent;
  let fixture: ComponentFixture<FormMonthlyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMonthlyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMonthlyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
