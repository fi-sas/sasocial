import { Component, Input, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MonthlyReportsModel } from '@fi-sas/backoffice/modules/social-support/models/monthly-reports.model';
import { ExperienceModel } from '../../../experiences/models/experience.model';
import * as moment from 'moment';
import { first, finalize } from 'rxjs/operators';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalRef } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { MonthlyReportsService } from '../../../collaborations-lists/services/monthly-reports.service';

@Component({
  selector: 'fi-sas-form-monthly-report',
  templateUrl: './form-monthly-report.component.html',
  styleUrls: ['./form-monthly-report.component.less'],
})
export class FormMonthlyReportComponent implements OnInit {
  @Input() user_interest_id: number;
  @Input() monthlyReport: MonthlyReportsModel = null;
  @Input() experience: ExperienceModel = null;

  reportForm_loading = false;
  reportForm = new FormGroup({
    monthYear: new FormControl(new Date(), Validators.required),
    report: new FormControl('', [Validators.required,trimValidation]),
    file_id: new FormControl(null, [Validators.required]),
  });



  dateValidator = (current: Date) => {
    if (this.experience) {
      return !moment(current).isBetween(
        this.experience.start_date,
        this.experience.end_date,
        'months',
        '[]'
      );
    } else {
      return false;
    }
  };

  constructor(
    public monthlyReportService: MonthlyReportsService,
    private uiService: UiService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {
    if (this.monthlyReport) {
      this.setReport(this.monthlyReport);
    }
  }

  close(state: boolean): void {
    this.modalRef.close(state);
  }

  setReport(report: MonthlyReportsModel) {
    this.monthlyReport = report;

    this.reportForm.patchValue({
      monthYear: moment()
        .set('month', report.month - 1)
        .set('year', report.year)
        .toDate(),
      report: report.report,
      file_id: report.file_id ? report.file_id : null,
    });
  }

  public clearForm() {
    this.reportForm.reset({
      monthYear: new Date(),
      report: '',
    });
  }

  onChangeRequired(){
    let isEmpty: boolean = false;    
    if(!this.reportForm.value.file_id && !this.reportForm.value.report){
      isEmpty = true;
    }

    if(!isEmpty){
      this.reportForm.get("report").setValidators(null);
      this.reportForm.get("file_id").setValidators(null);
    }else{
      this.reportForm.get("report").setValidators([Validators.required,trimValidation]);
      this.reportForm.get("file_id").setValidators([Validators.required]);
    }
  }

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.reportForm_loading = true;
      if (this.monthlyReport) {
        this.monthlyReportService
          .patch(this.monthlyReport.id, {
            user_interest_id: this.monthlyReport.user_interest_id,
            month: moment(this.reportForm.value.monthYear).month() + 1,
            year: moment(this.reportForm.value.monthYear).year(),
            report: this.reportForm.value.report,
            file_id: this.reportForm.value.file_id,
          })
          .pipe(
            first(),
            finalize(() => (this.reportForm_loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Relatório mensal alterado com sucesso'
            );
            this.reportForm.reset();
            this.monthlyReportService.submittedMonthlyReport();
            this.close(true);
          });
      } else {
        this.monthlyReportService
          .create({
            user_interest_id: this.user_interest_id,
            month: moment(this.reportForm.value.monthYear).month() + 1,
            year: moment(this.reportForm.value.monthYear).year(),
            report: this.reportForm.value.report,
            file_id: this.reportForm.value.file_id,
          })
          .pipe(
            first(),
            finalize(() => (this.reportForm_loading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Relatório inserido com sucesso'
            );
            this.reportForm.reset();
            this.close(true);
            this.monthlyReportService.submittedMonthlyReport();
          });
      }
    } else {
      for (const i in this.reportForm.controls) {
        if (this.reportForm.controls[i]) {
          this.reportForm.controls[i].markAsDirty();
          this.reportForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }
}
