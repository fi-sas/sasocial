import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { listOfferSelectedComponent } from './list-offer-selected.component';


describe('listOfferSelectedComponent', () => {
  let component: listOfferSelectedComponent;
  let fixture: ComponentFixture<listOfferSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listOfferSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(listOfferSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
