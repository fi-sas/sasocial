import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { listStudentSelectedComponent } from './list-student-selected.component';


describe('listStudentSelectedComponent', () => {
  let component: listStudentSelectedComponent;
  let fixture: ComponentFixture<listStudentSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listStudentSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(listStudentSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
