import { ApplicationModel } from "@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { ExperienceModel } from "../../experiences/models/experience.model";

export class UserInterestModel {
  experience_id: number;
  id: number;
  last_status: string;
  status: string;
  certificate_file: string;
  certificate_file_id: number;
  contract_file: string;
  contract_file_id: number;
  student_avaliation: string;
  student_file_avaliation: number;
  report_avaliation: number;
  user_id: number;
  user?: UserModel;
  application?: ApplicationModel;
  experience?: ExperienceModel;
}
