import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CollaborationsListsComponent } from './collaborations-lists.component';


describe('CollaborationsListsComponent', () => {
  let component: CollaborationsListsComponent;
  let fixture: ComponentFixture<CollaborationsListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationsListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationsListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
