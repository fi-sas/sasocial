import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { FilesService } from "@fi-sas/backoffice/modules/medias/services/files.service";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { NzDrawerService, NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { MonthlyReportsModel } from "../../../models/monthly-reports.model";
import { ExperienceModel } from "../../experiences/models/experience.model";
import { listOfferSelectedComponent } from "../components/list-offer-selected/list-offer-selected.component";
import { UserInterestModel } from "../models/user-interest.model";
import { MonthlyReportsService } from "../services/monthly-reports.service";
import * as moment from 'moment';
import { UserInterestsService } from "../services/user-interests.service";
import { ConfigurationsService } from "../../ss_configurations/services/configurations.service";
import { listStudentSelectedComponent } from "../components/list-student-selected/list-student-selected.component";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { FormCertificateComponent } from "../components/form-certificate/form-certificate.component";
import { FormMonthlyReportComponent } from "../components/form-monthly-report/form-monthly-report.component";
import { FormFinalReportComponent } from "../components/form-final-report/form-final-report.component";

@Component({
    selector: 'fi-sas-collaborations-lists',
    templateUrl: './collaborations-lists.component.html',
    styleUrls: ['./collaborations-lists.component.less']
})

export class CollaborationsListsComponent extends TableHelper implements OnInit {

    offer = false;
    currentMonthHours = 0;
    previousMonthHours = 0;
    reachedMaxApplicationPreviousMonthlyHours = false;
    reachedMaxApplicationCurrentMonthlyHours = false;
    tabIndex = 0;
    selectTabIndex = 0;
    maxApplicationMonthlyHours = null;
    loadingInfo = false;
    selectedMonthlyReport: MonthlyReportsModel[] = [];
    selectedExperience: ExperienceModel = new ExperienceModel;
    selectedStudent: UserModel = new UserModel;
    userInterests: UserInterestModel[] = [];
    selectedUserInterest: UserInterestModel;
    selectedCollaborationTotals = null;
    studentEvaluationFileUrl: string = '';
    isStudentEvaluationReportVisible = false;

    constructor(uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute, private drawerService: NzDrawerService, private userInterestsService: UserInterestsService,
        private fileService: FilesService, private monthlyReportsService: MonthlyReportsService, private authService: AuthService,
        private modalService: NzModalService, private configurationsService: ConfigurationsService) {
        super(uiService, router, activatedRoute);
        if (window.location.href.split("/")[window.location.href.split("/").length - 1].split('?')[0] == 'student') {
            this.offer = false;
        } else {
            this.offer = true;
        }
    }

    ngOnInit() {
        this.columns.push(
            {
                key: 'report.created_at',
                label: 'Relatórios',
                sortable: false,
            },
            {
                key: 'report.date',
                label: 'Data',
                sortable: false,
            },
        );
        this.getMaxApplicationMonthlyHours();
        this.setSelectTab();
        this.initTableData(this.monthlyReportsService);
    }

    setSelectTab() {
        if (this.authService.hasPermission('social_scholarship:attendances:read!')) {
            this.selectTabIndex = 0;
        } else if (this.authService.hasPermission('social_scholarship:monthly-reports:read!')) {
            this.selectTabIndex = 1;
        }
    }

    getMaxApplicationMonthlyHours() {
        /*this.configurationsService.readMaxApplicationMonthlyHours().pipe(first()).subscribe(result => {
            this.maxApplicationMonthlyHours = result.data[0].value;
        });*/

        this.configurationsService.list(0,0).pipe().subscribe(response => {
            if(response.data[0].MAX_HOURS_WORK){
                this.maxApplicationMonthlyHours = JSON.parse(response.data[0].MAX_HOURS_WORK)
            }
        })
    }

    modalSearch() {
        this.loadingInfo = true;
        if (this.offer) {
            const modalDrawer = this.drawerService.create({
                nzMask: true,
                nzMaskStyle: {
                    'opacity': '0',
                    '-webkit-animation': 'none',
                    'animation': 'none',
                },
                nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
                nzContent: listOfferSelectedComponent,
                nzWidth: (window.innerWidth / 24) * 12

            });
            modalDrawer.afterClose.pipe(first(), finalize(() => this.loadingInfo = false)).subscribe((result) => {
                if (result) {
                    this.selectedExperience = result;
                    this.userInterests = [];
                    this.selectedUserInterest = null;
                    this.loadExperienceUsers();

                }
            });
        } else {
            const modalDrawer = this.drawerService.create({
                nzMask: true,
                nzMaskStyle: {
                    'opacity': '0',
                    '-webkit-animation': 'none',
                    'animation': 'none',
                },
                nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
                nzContent: listStudentSelectedComponent,
                nzWidth: (window.innerWidth / 24) * 12

            });
            modalDrawer.afterClose.pipe(first(), finalize(() => this.loadingInfo = false)).subscribe((result) => {
                if (result) {
                    this.selectedStudent = result;
                    this.userInterests = [];
                    this.selectedUserInterest = null;
                    this.loadUserExperiences();
                }
            });
        }
    }

    loadExperienceUsers() {
        this.userInterestsService
            .list(1, -1, null, null, {
                status: 'COLABORATION',
                experience_id: this.selectedExperience.id,
                withRelated: "user,application,experience"
            })
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.userInterests = results.data;
                if (this.userInterests.length > 0) {
                    this.changeUserInterest(this.userInterests[0], 0);
                } else {
                    this.uiService.showMessage(
                        MessageType.warning,
                        'Não tem colaboradores nesta oferta'
                    );
                }

            });
    }

    loadUserExperiences() {
        this.userInterestsService
            .list(1, -1, null, null, {
                status: 'COLABORATION',
                user_id: this.selectedStudent.user_id,
                withRelated: "user,application,experience"
            })
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.userInterests = results.data;
                if (this.userInterests.length > 0) {
                    this.changeUserInterest(this.userInterests[0], 0);
                } else {
                    this.uiService.showMessage(
                        MessageType.warning,
                        'Sem informação'
                    );
                }
            });
    }

    changeUserInterest(selectedUserInterest: UserInterestModel, idx) {
        this.loadingInfo = true;
        this.tabIndex = idx;
        this.selectedUserInterest = selectedUserInterest;
        this.studentEvaluationFileUrl = '';

        this.loadStudentEvaluationFile();
        this.loadMonthlyReports(selectedUserInterest.id);
        this.loadColaborationTotals(selectedUserInterest.id);
        this.loadMonthlyHours(selectedUserInterest.id);
    }

    loadStudentEvaluationFile() {
        if (this.selectedUserInterest.student_file_avaliation) {
            this.fileService
                .get(this.selectedUserInterest.student_file_avaliation)
                .pipe(first())
                .subscribe(
                    (file) => {
                        this.studentEvaluationFileUrl = file.data[0].url;
                    },
                    () => { }
                );
        }
    }

    loadMonthlyReports(id: number) {
        this.monthlyReportsService
            .list(0, -1, null, null, {
                user_interest_id: id,
            })
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.selectedMonthlyReport = results.data.map(monthlyReport => {
                    return {
                        ...monthlyReport, date: moment({
                            year: monthlyReport.year,
                            month: monthlyReport.month - 1,
                            day: 1
                        })
                    }
                });
            });
    }


    loadColaborationTotals(id: number) {
        this.selectedCollaborationTotals = null;
        this.userInterestsService
            .generalReports(id)
            .pipe(
                first(), finalize(() => this.loadingInfo = false)
            )
            .subscribe((results) => {
                this.selectedCollaborationTotals = results.data[0];
            });
    }


    loadMonthlyHours(id: number) {
        this.userInterestsService
            .monthlyHours(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.currentMonthHours = results.data[0].current_month.total_hours;
                this.previousMonthHours = results.data[0].last_month.total_hours;
                this.checkMaxApplicationMonthlyHours();
            });
    }

    checkMaxApplicationMonthlyHours() {
        if (this.previousMonthHours > this.maxApplicationMonthlyHours) {
            this.reachedMaxApplicationPreviousMonthlyHours = true;
        } else {
            this.reachedMaxApplicationPreviousMonthlyHours = false;
        }

        if (this.currentMonthHours > this.maxApplicationMonthlyHours) {
            this.reachedMaxApplicationCurrentMonthlyHours = true;
        } else {
            this.reachedMaxApplicationCurrentMonthlyHours = false;
        }
    }

    showCertificateModal(userInterest: UserInterestModel) {
        if (!this.authService.hasPermission('social_scholarship:experience-user-interest:certificate')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.modalService.create({
            nzWidth: 800,
            nzMaskClosable: false,
            nzClosable: true,
            nzContent: FormCertificateComponent,
            nzFooter: null,
            nzComponentParams: {
                userInterest: userInterest,
                experience: this.selectedExperience,
            },
        });
    }


    showMonthlyReportModal(user_interest_id: number, monthlyReport: MonthlyReportsModel) {
        if (!this.authService.hasPermission('social_scholarship:monthly-reports:update')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.modalService.create({
            nzWidth: 800,
            nzMaskClosable: false,
            nzClosable: true,
            nzContent: FormMonthlyReportComponent,
            nzFooter: null,
            nzComponentParams: {
                user_interest_id: user_interest_id,
                experience: this.selectedExperience,
                monthlyReport: monthlyReport
            },
        });
    }

    showStudentEvaluationReport() {
        if (!this.authService.hasPermission('social_scholarship:experience-user-interest:read')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.isStudentEvaluationReportVisible = true;
    }

    closeStudentEvaluationReport() {
        this.isStudentEvaluationReportVisible = false;
    }

    showFinalReportModal(finalReport: UserInterestModel) {
        if (!this.authService.hasPermission('social_scholarship:experience-user-interest:final_report')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }
        this.modalService.create({
            nzWidth: 800,
            nzMaskClosable: false,
            nzClosable: true,
            nzContent: FormFinalReportComponent,
            nzFooter: null,
            nzComponentParams: {
                experience: this.selectedExperience,
                finalReport: finalReport
            },
        });
    }


    generateMonthlyReport(id: number) {
        if (!this.authService.hasPermission('social_scholarship:monthly-reports:create')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.loading = true;
        this.monthlyReportsService.generateMonthlyReport(id).pipe(
            first(),
            finalize(() => this.loading = false)
        ).subscribe((result) => {
            this.uiService.showMessage(
                MessageType.success,
                'Pedido de relatório enviado'
            );
        });
    }
}