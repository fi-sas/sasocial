import { AbsenceReasonModel } from '../../../models/absence_reason.model';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { AttendancesModel } from '@fi-sas/backoffice/modules/social-support/models/attendances.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AttendancesService extends Repository<AttendancesModel> {
  attendanceSubmitted = new BehaviorSubject(false);
  absenceSubmitted = new BehaviorSubject(false);

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.ATTENDANCES';
    this.entity_url = 'SOCIAL_SUPPORT.ATTENDANCES_ID';
  }

  attendanceObservable(): Observable<boolean> {
    return this.attendanceSubmitted.asObservable();
  }

  submittedAttendance() {
    this.attendanceSubmitted.next(true);
  }

  absenceObservable(): Observable<boolean> {
    return this.absenceSubmitted.asObservable();
  }

  submittedAbsence() {
    this.absenceSubmitted.next(true);
  }

  getAbsenceReason(
    id: number,
    withRelated?: string
  ): Observable<Resource<AbsenceReasonModel>> {
    return this.resourceService.read<AbsenceReasonModel>(
      this.urlService.get('SOCIAL_SUPPORT.ABSENCE_REASON_ACCEPT', {
        id,
        withRelated,
      }),
      {}
    );
  }

  absenceReasonChangeStatus(
    id: number,
    status: object,
    withRelated?: string
  ): Observable<Resource<AbsenceReasonModel>> {
    return this.resourceService.create<AbsenceReasonModel>(
      this.urlService.get('SOCIAL_SUPPORT.ABSENCE_REASON_ACCEPT', {
        id,
        withRelated,
      }),
      status
    );
  }

  changeStatus(id: number, data) {
    return this.resourceService.create<AttendancesModel>(
      this.urlService.get('SOCIAL_SUPPORT.ATTENDANCES_ID_CHAGE_STATUS', { id }),
      data
    );
  }

  validate(id: number) {
    return this.changeStatus(id, { status: 'ACCEPTED' });
  }

  reject(id: number, data) {
    return this.changeStatus(id, Object.assign({ status: 'REJECTED' }, data));
  }
}
