import { TestBed } from '@angular/core/testing';

import { MonthlyReportsService } from './monthly-reports.service';

describe('MonthlyReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonthlyReportsService = TestBed.get(MonthlyReportsService);
    expect(service).toBeTruthy();
  });
});
