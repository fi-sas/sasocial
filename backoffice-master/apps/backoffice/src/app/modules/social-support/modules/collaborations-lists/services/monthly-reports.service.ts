import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { MonthlyReportsModel } from "@fi-sas/backoffice/modules/social-support/models/monthly-reports.model";
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonthlyReportsService extends Repository<MonthlyReportsModel>{

  monthlyReportSubmitted = new BehaviorSubject(false);
  monthlyReportGenerated = new BehaviorSubject(false);

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.MONTHLY_REPORTS';
    this.entity_url = 'SOCIAL_SUPPORT.MONTHLY_REPORTS_ID';
  }

  /*generateMonthlyReport(
    application_id: number,
    start_date: string,
    end_date: string,
  ): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.MONTHLY_REPORTS_GENERATE', {
        application_id,
        start_date,
        end_date,
      }),
      {}
    );
  }*/

  generateMonthlyReport(id: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('SOCIAL_SUPPORT.MONTHLY_REPORTS_GENERATE', { id }),
      {}
    );
  }

  monthlyReportObservable(): Observable<boolean> {
    return this.monthlyReportSubmitted.asObservable();
  }

  submittedMonthlyReport() {
    this.monthlyReportSubmitted.next(true);
  }

  monthlyReportGeneratedObservable(): Observable<boolean> {
    return this.monthlyReportGenerated.asObservable();
  }

  generatedMonthlyReport() {
    this.monthlyReportGenerated.next(true);
  }

}
