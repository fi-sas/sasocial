import { TestBed } from '@angular/core/testing';

import { PaymentGridService } from './payment-grid.service';

describe('PaymentGridService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentGridService = TestBed.get(PaymentGridService);
    expect(service).toBeTruthy();
  });
});
