import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { PaymentGridModel } from "@fi-sas/backoffice/modules/social-support/models/payment-grid.model";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PaymentGridService extends Repository<PaymentGridModel>{

  paymentGridSubmitted = new BehaviorSubject(false);

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.PAYMENT_GRID';
    this.entity_url = 'SOCIAL_SUPPORT.PAYMENT_GRID_ID';
  }

  paymentGridObservable(): Observable<boolean> {
    return this.paymentGridSubmitted.asObservable();
  }

  submittedPaymentGrid() {
    this.paymentGridSubmitted.next(true);
  }

  approve(id: number, withRelated?: string): Observable<Resource<PaymentGridModel>> {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_APPROVE', {id, withRelated}), {});
  }

  reject(id: number, withRelated?: string): Observable<Resource<PaymentGridModel>> {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_REJECT', {id, withRelated}), {});
  }
}
