import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

@Injectable({
  providedIn: 'root',
})

export class UserColaborationService extends Repository<UserModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.EXPERIENCES_USER_COLLABORATION';
  }
}