import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

import { BehaviorSubject, Observable } from "rxjs";
import { UserInterestModel } from '../models/user-interest.model';

@Injectable({
  providedIn: 'root',
})
export class UserInterestsService extends Repository<UserInterestModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.USER_INTEREST';
    this.entity_url = 'SOCIAL_SUPPORT.USER_INTEREST_ID';
  }

  generalReports(id: number): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.GENERAL_REPORTS_USER_INTEREST', { id }),
      {}
    );
  }

  addCertificate(id: number, certificate_file_id: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('SOCIAL_SUPPORT.USER_INTEREST_ADD_CERTIFICATE', { id }),
      {
        certificate_file_id: certificate_file_id
      }
    );
  }

  generateCertificate(id: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('SOCIAL_SUPPORT.USER_INTEREST_GENERATE_CERTIFICATE', { id }),
      {}
    );
  }

  addFinalReport(id: number, report_avaliation: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('SOCIAL_SUPPORT.USER_INTEREST_ADD_FINAL_REPORT', { id }),
      {
        avaliation_file: report_avaliation
      }
    );
  }

  monthlyHours(id: number): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.MONTHLY_HOURS', { id }),
      {}
    );
  }
}
