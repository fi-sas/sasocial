import { ListComplainsComponent } from './pages/list-complains/list-complains.component';
import { ListApplicationsComplainsComponent } from './pages/list-complains-applications/list-complains-applications.component';
import { ListExperiencesComplainsComponent } from './pages/list-complains-experiences/list-complains-experiences.component';
import { ListUserInterestsComplainsComponent } from './pages/list-complains-user-interests/list-complains-user-interests.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListComplainsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:general-complains:read' },
  },
  {
    path: 'list-applications',
    component: ListApplicationsComplainsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:complain-applications:read' },
  },
  {
    path: 'list-experiences',
    component: ListExperiencesComplainsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:complain-applications:read' },
  },
  {
    path: 'list-user-interests',
    component: ListUserInterestsComplainsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:complain_user_interests:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplainsRoutingModule { }
