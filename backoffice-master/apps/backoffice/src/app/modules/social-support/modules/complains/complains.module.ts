import { NgModule } from '@angular/core';
import { ComplainsRoutingModule } from './complains-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CommonModule } from '@angular/common';
import { ListComplainsComponent } from './pages/list-complains/list-complains.component';
import { ListApplicationsComplainsComponent } from './pages/list-complains-applications/list-complains-applications.component';
import { ListExperiencesComplainsComponent } from './pages/list-complains-experiences/list-complains-experiences.component';
import { ListUserInterestsComplainsComponent } from './pages/list-complains-user-interests/list-complains-user-interests.component';
import { ViewComplainsComponent } from './pages/view-complains/view-complains.component';

@NgModule({
  declarations: [
    ListComplainsComponent,
    ViewComplainsComponent,
    ListApplicationsComplainsComponent,
    ListExperiencesComplainsComponent,
    ListUserInterestsComplainsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComplainsRoutingModule
  ],
  entryComponents: [
  ],

})

export class ComplainsModule { }
