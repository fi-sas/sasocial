export class GeneralComplainModel {
    id?: number;
    complain?: string;
    response?: string;
    status?: string;
    file?: string;
    file_id?: string;
    created_at?: string;
    updated_at?: string;
    user_id?: number;
}

export class ComplainsData {

    static status = {
      SUBMITTED: { label: 'Submetida', active: true, color: '#4d9de0' },
      ANALYSED: { label: 'Analisada', active: true, color: '#7768AE' },
      REPLYED: { label: 'Respondida', active: true, color: '#9DBBAE' },
    };

}