import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApplicationsComplainsComponent } from './list-complains-applications.component';

describe('ListComplainsComponent', () => {
  let component: ListApplicationsComplainsComponent;
  let fixture: ComponentFixture<ListApplicationsComplainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApplicationsComplainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationsComplainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
