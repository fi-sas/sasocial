import { Component, OnDestroy, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ApplicationsComplainsService } from '@fi-sas/backoffice/modules/social-support/modules/complains/services/applications-complains.service';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { ComplainsData } from '@fi-sas/backoffice/modules/social-support/modules/complains/models/general-complains.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-sas-list-complains-applications',
    templateUrl: './list-complains-applications.component.html',
    styleUrls: ['./list-complains-applications.component.less'],
})

export class ListApplicationsComplainsComponent
  extends TableHelper
  implements OnInit, OnDestroy {

  status = ComplainsData.status;
  complainStatuses = [];

  users_loading = false;
  users: UserModel[] = [];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private applicationsComplainsService: ApplicationsComplainsService,
    private authService: AuthService,
    private usersService: UsersService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadStatuses();

    this.persistentFilters['withRelated'] = [].join(',');

    this.columns.push(
      {
        key: 'created_at',
        label: 'Data',
        template: (data) => moment(data.created_at).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: null,
        label: 'Nome',
        template: (data) => data.user ? data.user.name : null,
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: this.status,
        filters: this.complainStatuses.map(status => {
          return {
            text: status.label,
            value: status.key,
          }
        }),
        showFilter: true,
        filterMultiple: false,
      },
      {
        key: null,
        label: 'Anexos',
        template: (data) => data.file_id != null ? 'Sim' : 'Não',
        sortable: false,
      },
    );

    this.initTableData(this.applicationsComplainsService);

  }

  ngOnDestroy() { }

  loadStatuses() {
    this.complainStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }


  changeStatus(data: any, disabled: boolean) {

    if(!this.authService.hasPermission('social_scholarship:complain-applications:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(!disabled) {
      this.applicationsComplainsService
      .saveStatus(data.id, { status: 'ANALYSED'})
      .subscribe((result) => {
        data.status = result.data[0].status;


        this.uiService.showMessage(
          MessageType.success,
          'Estado alterado com sucesso'
        );

      });
    }
  }

  listComplete() {
    this.filters.created_at = null;
    this.searchData(true);
  }

}
