import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExperiencesComplainsComponent } from './list-complains-experiences.component';

describe('ListComplainsComponent', () => {
  let component: ListExperiencesComplainsComponent;
  let fixture: ComponentFixture<ListExperiencesComplainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExperiencesComplainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExperiencesComplainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
