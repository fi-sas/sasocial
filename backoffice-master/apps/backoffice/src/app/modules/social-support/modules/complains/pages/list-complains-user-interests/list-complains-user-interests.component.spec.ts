import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUserInterestsComplainsComponent } from './list-complains-user-interests.component';

describe('ListUserInterestsComplainsComponent', () => {
  let component: ListUserInterestsComplainsComponent;
  let fixture: ComponentFixture<ListUserInterestsComplainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUserInterestsComplainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUserInterestsComplainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
