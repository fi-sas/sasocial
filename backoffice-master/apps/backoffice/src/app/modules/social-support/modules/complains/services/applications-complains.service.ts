import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from "rxjs";
import { GeneralComplainModel } from '@fi-sas/backoffice/modules/social-support/modules/complains/models/general-complains.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsComplainsService extends Repository<any>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.COMPLAINS_APPLICATIONS';
    this.entity_url = 'SOCIAL_SUPPORT.COMPLAINS_APPLICATIONS';
  }

  saveResponse(id: number, response: GeneralComplainModel): Observable<Resource<GeneralComplainModel>> {
    return this.resourceService.create<GeneralComplainModel>(this.urlService.get('SOCIAL_SUPPORT.COMPLAINS_APPLICATIONS_RESPONSE', { id }), response);
  }

  saveStatus(id: number, status: GeneralComplainModel): Observable<Resource<GeneralComplainModel>> {
    return this.resourceService.create<GeneralComplainModel>(this.urlService.get('SOCIAL_SUPPORT.COMPLAINS_APPLICATIONS_STATUS', { id }), status);
  }
}
