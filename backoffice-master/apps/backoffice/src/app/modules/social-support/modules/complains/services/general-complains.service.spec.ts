import { TestBed } from '@angular/core/testing';

import { GeneralComplainsService } from './general-complains.service';

describe('GeneralComplainsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralComplainsService = TestBed.get(GeneralComplainsService);
    expect(service).toBeTruthy();
  });
});
