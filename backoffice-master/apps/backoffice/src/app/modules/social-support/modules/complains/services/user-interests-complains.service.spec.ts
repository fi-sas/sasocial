import { TestBed } from '@angular/core/testing';

import { UserInterestsComplainsService } from './user-interests-complains.service';

describe('UserInterestsComplainsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserInterestsComplainsService = TestBed.get(UserInterestsComplainsService);
    expect(service).toBeTruthy();
  });
});
