import { Component, Input } from '@angular/core';

import { ApplicationModel } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';

@Component({
  selector: 'fi-sas-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.less'],
})
export class ApplicationDetailComponent {
  @Input() application: ApplicationModel;

  constructor() {}

  filterSchedule(scheduleID: number) {
    const found = (this.application.preferred_schedule || []).find((schedule) => schedule.schedule_id === scheduleID);

    return found !== undefined;
  }
}
