import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

import { finalize, first } from 'rxjs/operators';

import { ExperienceUserInterestInterviewModel } from '../../models/experience-user-interest-interview.model';
import { ExperienceUserInterestsService } from '../../services/experience-user-interests.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.less'],
})
export class InterviewsListComponent {
  private _interviews: ExperienceUserInterestInterviewModel[] = [];
  @Input() set interviews(interviews: ExperienceUserInterestInterviewModel[]) {
    this._interviews = interviews;
  }
  get interviews() {
    return this._interviews;
  }

  form: FormGroup = new FormGroup({
    file_id: new FormControl(null),
    notes: new FormControl('', [Validators.required, trimValidation]),
  });
  isLoading: {[key: number]: boolean} = {};

  constructor(private experienceUserInterestService: ExperienceUserInterestsService, private uiService: UiService) {}

  submit(valid: boolean, value: any, id: any, index: number) {
    if (valid) {
      this.isLoading[index] = true;
      this.experienceUserInterestService
        .saveInterview(id, value)
        .pipe(
          first(),
          finalize(() => (this.isLoading[index] = false))
        )
        .subscribe((result) => {
          result.data.forEach((interview) => {
            const replaceInterviewIndex = this.interviews.findIndex((i) => i.id === interview.id);
            if (replaceInterviewIndex >= 0) {
              this.interviews[replaceInterviewIndex] = interview;
            }
          });
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso.');
        });
    }  else {
      for (const i in this.form.controls) {
        if (this.form.controls[i]) {
          this.form.controls[i].markAsDirty();
          this.form.controls[i].updateValueAndValidity();
        }
      }
    }
  }
}
