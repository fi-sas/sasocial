import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExperienceUserInterestsComponent } from './pages/list-experience-user-interests/list-experience-user-interests.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListExperienceUserInterestsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:experience-user-interest:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExperienceUserInterestsRoutingModule { }
