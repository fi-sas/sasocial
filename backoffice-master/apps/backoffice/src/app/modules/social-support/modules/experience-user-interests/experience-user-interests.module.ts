import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ApplicationDetailComponent, InterviewsListComponent } from './components';
import { ApplicationsModule } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/applications.module';
import { ExperienceUserInterestsRoutingModule } from './experience-user-interests-routing.module';
import { ExperienceUserInterestsService } from './services/experience-user-interests.service';
import { ListExperienceUserInterestsComponent } from './pages/list-experience-user-interests/list-experience-user-interests.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SsSharedModule } from '../ss-shared/ss-shared.module';
import { ViewExperienceUserInterestComponent } from './pages/view-experience-user-interests/view-experience-user-interests.component';

@NgModule({
  declarations: [
    ApplicationDetailComponent,
    InterviewsListComponent,
    ListExperienceUserInterestsComponent,
    ViewExperienceUserInterestComponent,
  ],
  imports: [ApplicationsModule, CommonModule, ExperienceUserInterestsRoutingModule, SharedModule, SsSharedModule],
  providers: [ExperienceUserInterestsService],
  entryComponents: [ApplicationDetailComponent, InterviewsListComponent],
})
export class ExperienceUserInterestsModule {}
