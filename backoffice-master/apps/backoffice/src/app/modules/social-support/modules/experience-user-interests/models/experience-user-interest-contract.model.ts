export class ExperienceUserInterestContractModel {
  id?: number;
  contract_file_id: number;
  created_at?: string;
  updated_at?: string;
}
