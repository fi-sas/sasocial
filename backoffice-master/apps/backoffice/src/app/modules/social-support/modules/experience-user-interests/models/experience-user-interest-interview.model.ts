import { ExperienceUserInterestModel } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';

export class ExperienceUserInterestInterviewModel {
  id?: number;
  user_interest_id: number;
  userInterest?: ExperienceUserInterestModel;
  local: string;
  date: string;
  responsable_id: number;
  responsable?: UserModel;
  notes: string;
  observations: string;
  scope: string;
  file_id: number;
  file?: FileModel;
  created_at?: string;
  updated_at?: string;
}
