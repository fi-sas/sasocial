import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExperienceUserInterestsComponent } from './list-experience-user-interests.component';

describe('ListExperiencesComponent', () => {
  let component: ListExperienceUserInterestsComponent;
  let fixture: ComponentFixture<ListExperienceUserInterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListExperienceUserInterestsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExperienceUserInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
