import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import * as moment from 'moment';

import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ExperiencesService } from '@fi-sas/backoffice/modules/social-support/modules/experiences/services/experiences.service';
import {
  ExperienceUserInterestData,
  ExperienceUserInterestModel,
  ExperienceUserInterestStatus,
  ExperienceUserInterestStatusEvent,
  ExperienceUserInterestTagResult,
} from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest.model';
import { ExperienceUserInterestsService } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/services/experience-user-interests.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { InterviewsListComponent } from '../../components';

@Component({
  selector: 'fi-sas-list-experience-user-interest',
  templateUrl: './list-experience-user-interests.component.html',
  styleUrls: ['./list-experience-user-interests.component.less'],
})
export class ListExperienceUserInterestsComponent extends TableHelper implements OnInit {
  status = ExperienceUserInterestData.status;
  ExperienceUserInterestTagResult = ExperienceUserInterestTagResult;
  statusMachine = ExperienceUserInterestData.statusMachine;
  ExperienceUserInterestStatus = ExperienceUserInterestStatus;
  experienceStatuses = [];
  languages = [];

  // FILTERS
  students_loading = false;
  students: UserModel[] = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExperience: ExperienceUserInterestModel = null;
  changeStatusModalActions: Partial<Record<ExperienceUserInterestStatusEvent, { label: string; color: string }>> = {};
  changeStatusModalExperienceAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  changeStatusRequirementNumber = null;

  //MODAL INTERVIEW
  isInterviewModalVisible = false;
  interviewModalUserInterest: ExperienceUserInterestModel = new ExperienceUserInterestModel();
  interviewModalLoading = false;

  interviewForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    local: new FormControl('', [Validators.required, trimValidation]),
    scope: new FormControl('', [Validators.required, trimValidation]),
    observations: new FormControl(null, [trimValidation]),
    responsable_id: new FormControl(null, [Validators.required]),
  });

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });

  constructor(
    private authService: AuthService,
    private languageService: LanguagesService,
    private modalService: NzModalService,
    public activatedRoute: ActivatedRoute,
    public experienceService: ExperiencesService,
    public experienceUserInterestsService: ExperienceUserInterestsService,
    public router: Router,
    public uiService: UiService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadStatuses();
    this.loadLanguages();
    this.persistentFilters['withRelated'] = [
      'experience',
      'user',
      'history',
      'application',
      'interview',
      'historic_applications',
      'historic_colaborations',
      'contract_file',
    ].join(',');

    const status: string = this.activatedRoute.snapshot.queryParamMap.get('status');

    if (status) {
      this.persistentFilters['status'] = status;
    }

    this.columns.push(
      {
        key: 'created_at',
        label: 'Data de submissão',
        template: (data) => moment(data.created_at).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: null,
        label: 'Número',
        template: (data) => (data.user && data.user.student_number ? data.user.student_number : data.application? data.application.student_number :null),
        sortable: false,
      },
      {
        key: null,
        label: 'Candidato',
        template: (data) => (data.user ? data.user.name : null),
        sortable: false,
      },
      {
        key: null,
        label: 'Titulo da colaboração',
        template: (data) => (data.experience.translations[0] ? data.experience.translations[0].title : null),
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: this.status,
        filters: this.experienceStatuses.map((experienceStatus) => {
          return {
            text: experienceStatus.label,
            value: experienceStatus.key,
          };
        }),
        showFilter: true,
        filterMultiple: false,
      }
    );

    this.initTableData(this.experienceUserInterestsService);
  }

  loadLanguages() {
    this.loading = true;

    this.languageService
      .list(1, -1, 'id', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => (this.languages = result.data));
  }

  loadStatuses() {
    this.experienceStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  deleteExperience(id: number) {
    if (!this.authService.hasPermission('social_scholarship:experience-user-interest:delete')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar esta manifestação de interesse?', 'Eliminar')
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.experienceUserInterestsService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.uiService.showMessage(MessageType.success, 'Manifestação de interesse eliminada com sucesso');
              this.searchData();
            });
        }
      });
  }

  changeStateModal(experience: ExperienceUserInterestModel, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:experience-user-interest:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    if(!Object.keys(ExperienceUserInterestData.statusMachine[experience.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalExperience = experience;
      this.changeStatusModalActions = ExperienceUserInterestData.statusMachine[experience.status];
    }
  }

  reopenModal(id: number, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:experience-user-interest:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    if (!disabled) {
      this.uiService
        .showConfirm('Reabrir manifestação de interesse', 'Pretende reabrir esta manifestação de interesse?', 'Reabrir')
        .pipe(first())
        .subscribe((confirm) => {
          if (confirm) {
            this.experienceUserInterestsService
              .revertStatus(id)
              .pipe(first())
              .subscribe((result) => {
                this.uiService.showMessage(MessageType.success, 'Manifestação de interesse reaberta com sucesso');
                this.searchData();
              });
          }
        });
    }
  }

  handleChangeStatusOk() {    
    this.changeStatusModalLoading = true;

      const obj: any = {
        event: this.changeStatusModalExperienceAction.toUpperCase(),
        notes: this.changeStatusModalNotes,
      };
    
      this.experienceService
        .read(this.changeStatusModalExperience.experience_id, { withRelated: ['users_colaboration'] })
        .subscribe((result) => {
          if (
            obj.event === 'APPROVE' &&
            this.changeStatusModalExperience.experience.number_simultaneous_candidates <=
              result.data[0].users_colaboration.length
          ) {
            this.uiService
              .showConfirm(
                'Máximo de colaborações atingido',
                'Foi atingido o máximo de colaborações definidas. Tem a certeza que pretende aprovar a colaboração?',
                'Aprovar colaboração'
              )
              .pipe(
                first(),
                finalize(() => this.changeStatusModalLoading = false)
              )
              .subscribe((confirm) => {
                if (confirm) {
                  this.changeCandidateStatus(obj);
                }
              });
          } else {
            this.changeCandidateStatus(obj);
          }
        });
  }

  changeCandidateStatus(obj: any) {

    if (this.changeStatusRequirementNumber != null) {
      obj.experience = { requirement_number: this.changeStatusRequirementNumber };
    }

    if(this.changeStatusModalExperience.status !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'INTERVIEW'){
      this.experienceUserInterestsService
      .status(this.changeStatusModalExperience.id, obj)
      .pipe(
        first(),
        finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe((result) => {
        this.changeStatusModalExperience.status = result.data[0].status;
        this.uiService.showMessage(MessageType.success, 'Alteração de estado efetuada com sucesso.');
        this.handleChangeStatusCancel();
      });
    }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'INTERVIEW' ){
      if (this.interviewForm.valid) {
        this.interviewForm.get('observations').setValue(this.changeStatusModalNotes);
        this.experienceUserInterestsService
          .scheduleInterview(this.changeStatusModalExperience.id, this.interviewForm.value)
          .pipe(
            first(),
            finalize(() => {
              this.interviewModalLoading = false;
              this.changeStatusModalLoading = false;
            })
          )
          .subscribe(() => {
            this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso');
            this.handleChangeStatusCancel();
          });
      }else{
        this.updateAndValidityForm(this.interviewForm);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'DISPATCH'){
      if(this.form.valid){
        this.experienceUserInterestsService.sendDispatch(this.changeStatusModalExperience.id, {
          event: this.changeStatusModalExperienceAction.toUpperCase(),
          decision: this.form.get('decision').value,
          notes: this.changeStatusModalNotes,
        })
          .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
          .subscribe(() => {
            this.handleChangeStatusCancel();
          });
      }else{
        this.updateAndValidityForm(this.form);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalExperience.status === 'DISPATCH'){
      this.experienceUserInterestsService
      .sendDispatch(this.changeStatusModalExperience.id, { decision_dispatch: this.changeStatusModalExperienceAction.toUpperCase() })
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe(() => this.handleChangeStatusCancel());
    }else{
      this.changeStatusModalLoading = false;
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalExperience = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalExperienceAction = null;
    this.changeStatusModalNotes = '';
    this.searchData();
    this.form.reset();
    this.interviewForm.reset();
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalExperienceAction = action;
    this.form.reset();
    this.interviewForm.reset();
  }

  interviewModal(userInterest: ExperienceUserInterestModel, enabled: boolean) {
    if (enabled) {
      this.isInterviewModalVisible = true;
      this.interviewModalUserInterest = userInterest;
    }
  }

  handleInterviewCancel() {
    this.isInterviewModalVisible = false;
    this.interviewModalUserInterest = null;
    this.interviewForm.reset();
  }

  handleInterviewConfirm(event: any, value: any) {
    this.interviewModalLoading = true;
    if (this.interviewForm.valid) {
      this.experienceUserInterestsService
        .scheduleInterview(this.interviewModalUserInterest.id, value)
        .pipe(
          first(),
          finalize(() => (this.interviewModalLoading = false))
        )
        .subscribe((result) => {
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso');

          this.searchData();
          this.handleInterviewCancel();
        });
    } else {
      this.updateAndValidityForm(this.interviewForm);
    }
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  listComplete() {
    this.filters.created_at = null;
    this.filters.user_id = null;
    this.searchData(true);
  }

  registerInterview(data: ExperienceUserInterestModel) {
    this.modalService.create({
      nzTitle: 'Registar entrevistas',
      nzContent: InterviewsListComponent,
      nzComponentParams: { interviews: data.interview },
      nzFooter: null,
    });
  }

 /* dispatchModal(userInterest: ExperienceUserInterestModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.experienceUserInterestsService.sendDispatch(userInterest.id, data),
        actions: Object.keys(ExperienceUserInterestData.statusMachine.DISPATCH).map<{
          key: ExperienceUserInterestStatus;
          label: string;
        }>((key) => ({
          key: ExperienceUserInterestData.eventToDispatchDecisionMapper[key],
          label: ExperienceUserInterestData.statusMachine.DISPATCH[key].label,
        })),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance
              .onSubmit()
              .then(() => {
                this.searchData();
                modalRef.close();
              })
              .catch(() => null);
          },
        },
      ],
    });
  }*/

  getDispatchActions = () => {
    return Object.keys(ExperienceUserInterestData.statusMachine.DISPATCH).map<{
      key: ExperienceUserInterestData;
      label: string;
    }>((key) => ({
      key: ExperienceUserInterestData.eventToDispatchDecisionMapper[key],
      label: ExperienceUserInterestData.statusMachine.DISPATCH[key].label,
    }));
  }

  dispatchChangeStatues(userInterest: ExperienceUserInterestModel, acceptDecision: boolean) {
    this.experienceUserInterestsService
      .sendDispatch(userInterest.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }
}
