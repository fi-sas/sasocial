import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExperienceUserInterestComponent } from './view-experience-user-interests.component';

describe('ViewExperienceUserInterestComponent', () => {
  let component: ViewExperienceUserInterestComponent;
  let fixture: ComponentFixture<ViewExperienceUserInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewExperienceUserInterestComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExperienceUserInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
