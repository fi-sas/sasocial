import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import {
  ExperienceUserInterestData,
  ExperienceUserInterestModel,
  ExperienceUserInterestTagResult,
} from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest.model';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { StatusInfoModel } from '@fi-sas/backoffice/modules/social-support/models/status-info.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ExperienceUserInterestsService } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/services/experience-user-interests.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import { ApplicationsService } from '../../../ss_applications/services/applications.service';
import { ApplicationModel } from '../../../ss_applications/models/application.model';
import { ViewApplicationModalComponent } from '../../../ss_applications/pages/view-application-modal/view-application-modal.component';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { ApplicationDetailComponent } from '../../components';

@Component({
  selector: 'fi-sas-view-experience-user-interests',
  templateUrl: './view-experience-user-interests.component.html',
  styleUrls: ['./view-experience-user-interests.component.less'],
})
export class ViewExperienceUserInterestComponent implements OnInit {
  @Input() data: ExperienceUserInterestModel = null;

  @Output() itemChange = new EventEmitter<boolean>();

  UsersInterestTagResult = ExperienceUserInterestTagResult;
  loading = false;
  application: ApplicationModel = null;
  experienceUserInterest: ExperienceUserInterestModel = null;
  interviewLoading = false;
  interviewFormLoading = false;
  contractLoading = false;
  contractFormLoading = false;
  status = ExperienceUserInterestData.status;
  statusMachine = ExperienceUserInterestData.statusMachine;
  decisionToStateMapper = ExperienceUserInterestData.decisionToStateMapper;
  modalsRefs: { name: string; modal: NzModalRef }[] = [];

  statusInfo: StatusInfoModel;

  interviewForm = new FormGroup({
    file_id: new FormControl(null, []),
    notes: new FormControl('', [Validators.required, trimValidation]),
  });

  contractForm = new FormGroup({
    contract_file_id: new FormControl(null, []),
  });

  constructor(
    private uiService: UiService,
    private applicationService: ApplicationsService,
    private modalService: NzModalService,
    public experienceUserInterestService: ExperienceUserInterestsService
  ) { }

  createTplModal(
    tplTitle: TemplateRef<{}>,
    tplContent: TemplateRef<{}>,
    tplFooter: TemplateRef<{}>,
    size: number = 900,
    padding: string = '25px'
  ): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size,
      nzBodyStyle: { padding: padding },
    });
  }

  ngOnInit() {
    this.setcontract();
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 500);
  }

  generateModal(
    name: string,
    modalTitle: TemplateRef<{}>,
    modalContent: TemplateRef<{}>,
    modalFooter: TemplateRef<{}>,
    modalSize: number,
    padding?: string
  ) {
    this.modalsRefs.push({
      name: name,
      modal: this.createTplModal(modalTitle, modalContent, modalFooter, modalSize, padding),
    });
  }

  filterSchedule(scheduleID: number) {
    const found = this.data.application.preferred_schedule.find((schedule) => schedule.schedule_id === scheduleID);

    return found !== undefined;
  }

  getExperienceCertificate(id) {
    this.applicationService
      .generateCertificate(id)
      .pipe(first())
      .subscribe((data) => {
        this.uiService.showMessage(MessageType.success, 'Certificado gerado com sucesso');
      });
  }

  submit(valid: boolean, value: any, id: any) {
    this.interviewFormLoading = true;
    this.experienceUserInterestService
      .saveInterview(id, value)
      .pipe(
        first(),
        finalize(() => (this.interviewFormLoading = false))
      )
      .subscribe((result) => {
        this.data.interview = this.data.interview.map((interview) => {
          if (interview.id === result.data[0].id) {
            return result.data[0];
          }

          return interview;
        });

        this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso.');
      });
  }

  submitContract(valid: boolean, value: any, id: any) {
    this.contractFormLoading = true;
    this.experienceUserInterestService
      .saveContract(id, value)
      .pipe(
        first(),
        finalize(() => (this.contractFormLoading = false))
      )
      .subscribe((result) => { });

    this.uiService.showMessage(MessageType.success, 'Contracto inserido com sucesso.');
  }

  setcontract() {
    this.contractForm.patchValue({
      contract_file_id: this.data.contract_file_id ? this.data.contract_file_id : null,
    });
  }

  viewApplicationModal(id: number) {
    this.applicationService
      .read(id, {
        id: id,
        withRelated:
          'history,experience,preferred_activities,attendance,monthly_reports,payment_grids,user,course,course_degree,school,cv_file,organic_units,preferred_schedule,historic_colaborations,interviews,historic_applications',
      })
      .pipe(
        first(),
        finalize(() => true)
      )
      .subscribe((result) => {
        this.application = result.data[0];
        this.modalService.create({
          nzWidth: 1024,
          nzMaskClosable: false,
          nzClosable: true,
          nzTitle: 'Candidatura',
          nzContent: ApplicationDetailComponent,
          nzFooter: null,
          nzComponentParams: {
            application: this.application,
          },
        });
      });
  }

  dispatchChangeStatues(application: ExperienceUserInterestModel, acceptDecision: boolean) {
    this.experienceUserInterestService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.itemChange.emit(true);
      });
  }
}
