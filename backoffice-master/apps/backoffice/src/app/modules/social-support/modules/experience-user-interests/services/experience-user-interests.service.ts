import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApproveRejectModel } from '@fi-sas/backoffice/modules/social-support/models/approveReject';
import { ExperienceModel } from '@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model';
import { ExperienceUserInterestContractModel } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest-contract.model';
import { ExperienceUserInterestInterviewModel } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest-interview.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SendDispatchData } from '../../../models';

@Injectable({
  providedIn: 'root',
})
export class ExperienceUserInterestsService extends Repository<ExperienceModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS';
    this.entity_url = 'SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_ID';
  }

  status(id: number, experience_status: ApproveRejectModel): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_STATUS', { id }),
      experience_status
    );
  }

  revertStatus(id: Number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTEREST_LAST_STATUS', { id }),
      {}
    );
  }

  scheduleInterview(
    id: number,
    interview: ExperienceUserInterestInterviewModel
  ): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_SCHEDULE_INTERVIEW', { id }),
      interview
    );
  }

  saveInterview(
    id: number,
    interview: ExperienceUserInterestInterviewModel
  ): Observable<Resource<ExperienceUserInterestInterviewModel>> {
    return this.resourceService.create<ExperienceUserInterestInterviewModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTEREST_SAVE_INTERVIEW', { id }),
      interview
    );
  }

  saveContract(
    id: number,
    contract: ExperienceUserInterestContractModel
  ): Observable<Resource<ExperienceUserInterestContractModel>> {
    return this.resourceService.patch<ExperienceUserInterestContractModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_ID', { id }),
      contract
    );
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_STATUS', { id }),
      data
    );
  }
}
