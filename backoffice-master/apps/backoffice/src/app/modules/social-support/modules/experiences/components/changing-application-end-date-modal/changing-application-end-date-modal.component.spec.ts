import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangingApplicationEndDateModalComponent } from './changing-application-end-date-modal.component';

describe('ChangingApplicationEndDateModalComponent', () => {
  let component: ChangingApplicationEndDateModalComponent;
  let fixture: ComponentFixture<ChangingApplicationEndDateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangingApplicationEndDateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangingApplicationEndDateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
