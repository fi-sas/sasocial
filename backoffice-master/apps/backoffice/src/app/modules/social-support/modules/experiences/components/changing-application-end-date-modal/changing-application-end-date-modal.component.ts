import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

import { ExperienceModel } from '../../models/experience.model';
import { ExperiencesService } from '../../services/experiences.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-changing-application-end-date-modal',
  templateUrl: './changing-application-end-date-modal.component.html',
  styleUrls: ['./changing-application-end-date-modal.component.less']
})
export class ChangingApplicationEndDateModalComponent implements OnInit {

  @Input() experience: ExperienceModel = null
  @Input() serviceFn: (data: any) => Observable<Resource<unknown>>;

  form = new FormGroup({
    new_end_date: new FormControl(null, Validators.required),
  });
  loading = false;

  publishDate = (date) => moment(date).isBefore(new Date(), 'd');

  constructor(
    private experienceService: ExperiencesService
  ) { }

  ngOnInit() {
  }

  onSubmit(): Promise<any>{
    if (!this.isFormValid()) {
      return Promise.reject();  
    }else{
      this.loading = true;
      return Promise.resolve(true);
    }
  }

  private isFormValid() {
    for (const controlName in this.form.controls) {
      if (this.form.controls[controlName]) {
        this.form.controls[controlName].markAsDirty();
        this.form.controls[controlName].updateValueAndValidity();
      }
    }

    return this.form.valid;
  }

 

}
