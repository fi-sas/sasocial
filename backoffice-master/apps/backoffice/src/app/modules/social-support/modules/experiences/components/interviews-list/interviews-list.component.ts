import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { ExperienceUserInterestInterviewModel } from '../../../experience-user-interests/models/experience-user-interest-interview.model';
import { ExperienceUserInterestsService } from '../../../experience-user-interests/services/experience-user-interests.service';

@Component({
  selector: 'fi-sas-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.less']
})
export class InterviewsListComponent {

  @Input() interviews: ExperienceUserInterestInterviewModel[]= [];

  form: FormGroup = new FormGroup({
    file_id: new FormControl(null),
    notes: new FormControl('', [Validators.required, trimValidation]),
  });
  isLoading: {[key: number]: boolean} = {};
  
  constructor(private experienceUserInterestService: ExperienceUserInterestsService, private uiService: UiService) {}
 
  submit(valid: boolean, value: any, id: any, index: number) {
    if (valid) {
      this.isLoading[index] = true;
      this.experienceUserInterestService
        .saveInterview(id, value)
        .pipe(
          first(),
          finalize(() => (this.isLoading[index] = false))
        )
        .subscribe((result) => {
          result.data.forEach((interview) => {
            const replaceInterviewIndex = this.interviews.findIndex((i) => i.id === interview.id);
            if (replaceInterviewIndex >= 0) {
              this.interviews[replaceInterviewIndex] = interview;
            }
          });
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso.');
        });
    }  else {
      for (const i in this.form.controls) {
        if (this.form.controls[i]) {
          this.form.controls[i].markAsDirty();
          this.form.controls[i].updateValueAndValidity();
        }
      }
    }
  }

}
