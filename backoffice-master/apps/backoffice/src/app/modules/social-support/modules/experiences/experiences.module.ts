import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExperiencesRoutingModule } from './experiences-routing.module';
import { ExperiencesService } from './services/experiences.service';
import { FormExperienceComponent } from './pages/form-experience/form-experience.component';
import { ListExperiencesComponent } from './pages/list-experiences/list-experiences.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SsSharedModule } from '../ss-shared/ss-shared.module';
import { ViewExperienceComponent } from './pages/view-experience/view-experience.component';
import { ChangingApplicationEndDateModalComponent } from './components/changing-application-end-date-modal/changing-application-end-date-modal.component';
import { InterviewsListComponent } from './components/interviews-list/interviews-list.component';

@NgModule({
  declarations: [FormExperienceComponent, ListExperiencesComponent, ViewExperienceComponent, ChangingApplicationEndDateModalComponent, InterviewsListComponent],
  imports: [CommonModule, SharedModule, ExperiencesRoutingModule, SsSharedModule],
  providers: [ExperiencesService],
  entryComponents: [ChangingApplicationEndDateModalComponent, InterviewsListComponent]
})
export class ExperiencesModule {}
