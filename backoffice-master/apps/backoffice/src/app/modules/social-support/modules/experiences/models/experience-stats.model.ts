export class ExperienceStatsModel {
    SUBMITTED: number;
    RETURNED: number;
    ANALYSED: number;
    APPROVED: number;
    DISPATCH: number;
    PUBLISHED: number;
    REJECTED: number;
    CANCELLED: number;
    SEND_SEEM: number;
    EXTERNAL_SYSTEM: number;
    SELECTION: number;
    IN_COLABORATION: number;
    CLOSED: number;
    CONFIRMED: number;
  }
