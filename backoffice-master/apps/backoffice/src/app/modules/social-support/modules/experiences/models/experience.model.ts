import { UsersInterestModel } from './users-interest-model';
import { ExperienceUserInterestModel } from '@fi-sas/backoffice/modules/social-support/modules/experience-user-interests/models/experience-user-interest.model';
import { ExperienceHistoryModel } from '@fi-sas/backoffice/modules/social-support/models/experience-history.model';
import { ApplicationModel } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';

export enum ExperienceStatus {
  ANALYSED = 'ANALYSED',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  CLOSED = 'CLOSED',
  DISPATCH = 'DISPATCH',
  EXTERNAL_SYSTEM = 'EXTERNAL_SYSTEM',
  IN_COLABORATION = 'IN_COLABORATION',
  REJECTED = 'REJECTED',
  RETURNED = 'RETURNED',
  SELECTION = 'SELECTION',
  SEND_SEEM = 'SEND_SEEM',
  SUBMITTED = 'SUBMITTED',
}

export enum ExperienceStatusEvent {
  ANALYSE = 'ANALYSE',
  APPROVE = 'APPROVE',
  CANCEL = 'CANCEL',
  CLOSE = 'CLOSE',
  COLABORATION = 'COLABORATION',
  DISPATCH = 'DISPATCH',
  REJECT = 'REJECT',
  RETURN = 'RETURN',
  SELECTION = 'SELECTION',
  SENDEXTERNAL = 'SENDEXTERNAL',
  SENDSEEM = 'SENDSEEM',
  SUBMIT = 'SUBMIT',
}
export enum ExperienceDispatchDecision {
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
  RETURNED = 'RETURNED',
}

export const ExperienceTagResult: Record<ExperienceStatus, { color: string; label: string; }> = {
  ANALYSED: { color: '#7768AE', label: 'Em análise' },
  APPROVED: { color: '#0d69dd', label: 'Aprovada' },
  CANCELLED: { color: '#A4A4A4', label: 'Cancelada' },
  CLOSED: { color: '#000000', label: 'Fechada' },
  DISPATCH: { color: 'orange', label: 'Despacho' },
  EXTERNAL_SYSTEM: { color: '#076743', label: 'Sistema externo' },
  IN_COLABORATION: { color: '#1ba974', label: 'Em colaboração' },
  REJECTED: { color: '#D0021B', label: 'Rejeitada' },
  RETURNED: { color: '#88ccf1', label: 'Devolvida' },
  SELECTION: { color: '#076743', label: 'Seleção' },
  SEND_SEEM: { color: '#D4B483', label: 'Emitir parecer' },
  SUBMITTED: { color: '#4D9DE0', label: 'Submetida' },
};

export class ScheduleSocialSupportModel {
  schedule_id: number;
  time_begin: string;
  time_end: string;
}

export class ExperienceModel {
  id?: number;
  academic_year: string;
  title: string;
  proponent_service: string;
  organic_unit_id: number;
  organic_unit?: OrganicUnitsModel;
  address: string;
  applicant_profile: string;
  job: string;
  number_monthly_hours: number;
  total_hours_estimation: number;
  schedule: ScheduleSocialSupportModel[];
  payment_model: string;
  payment_value: number;
  perc_student_iban: number;
  perc_student_ca: number;
  current_account?: AccountModel;
  current_account_id: number;
  experience_responsible?: UserModel;
  experience_responsible_id: number;
  experience_advisor?: UserModel;
  experience_advisor_id: number;
  email_search_advisor: string;
  description: string;
  start_date: string;
  end_date: string;
  data_responsible_id: number;
  data_advisor_id: number;
  number_candidates: number;
  number_simultaneous_candidates: number;
  number_weekly_hours: number;
  publish_date: string;
  application_deadline_date: string;
  selection_criteria: string;
  attachment_file?: FileModel;
  attachment_file_id: number;
  contract_file?: FileModel;
  contract_file_id: number;
  certificate_file?: FileModel;
  certificate_file_id: number;
  status?: string;
  translations?: TranslationModel[];
  created_at?: string;
  updated_at?: string;
  history?: ExperienceHistoryModel[];
  users_interest?: UsersInterestModel[];
  users_colaboration?: ExperienceUserInterestModel[];
  applications?: ApplicationModel[];
  holydays_availability: boolean;
  decision?: string;
}

class Translations {
  title: string;
  proponent_service: string;
  selection_criteria: string;
  applicant_profile: string;
  job: string;
  description: string;
  language_id: number;
}

export class TranslationModel {
  id: number;
  title: string;
  proponent_service: string;
  applicant_profile: string;
  job: string;
  description: string;
  selection_criteria: string;
  language_id: number;
  language: LanguageModel;
  experience_id: number;
  updated_at?: string;
  created_at?: string;
}

export class ExperienceData {
  static payment_model = [
    { value: 'BA', label: 'Bolsa Anual', active: true },
    { value: 'VH', label: 'Valor Hora', active: true },
  ];

  static status: Record<ExperienceStatus, { label: string; active: boolean; color: string; }> = {
    ANALYSED: { label: 'Em análise', active: true, color: ExperienceTagResult.ANALYSED.color },
    APPROVED: { label: 'Aprovada', active: true, color: ExperienceTagResult.APPROVED.color },
    CANCELLED: { label: 'Cancelada', active: true, color: ExperienceTagResult.CANCELLED.color },
    CLOSED: { label: 'Fechada', active: true, color: ExperienceTagResult.CLOSED.color },
    DISPATCH: { label: 'Despacho', active: true, color: ExperienceTagResult.DISPATCH.color },
    EXTERNAL_SYSTEM: { label: 'Sistema externo', active: true, color: ExperienceTagResult.EXTERNAL_SYSTEM.color },
    IN_COLABORATION: { label: 'Em colaboração', active: true, color: ExperienceTagResult.IN_COLABORATION.color },
    REJECTED: { label: 'Rejeitada', active: true, color: ExperienceTagResult.REJECTED.color },
    RETURNED: { label: 'Devolvida', active: true, color: ExperienceTagResult.RETURNED.color },
    SELECTION: { label: 'Seleção', active: true, color: ExperienceTagResult.SELECTION.color },
    SEND_SEEM: { label: 'Emitir parecer', active: true, color: ExperienceTagResult.SEND_SEEM.color },
    SUBMITTED: { label: 'Submetida', active: true, color: ExperienceTagResult.SUBMITTED.color },
  };

  static statusMachine: Record<ExperienceStatus, Partial<Record<ExperienceStatusEvent, { label: string; color: string; }>>> = {
    SUBMITTED: {
      ANALYSE: { label: 'Analisar', color: ExperienceTagResult.ANALYSED.color },
      CANCEL: { label: 'Cancelar', color: ExperienceTagResult.CANCELLED.color },
    },
    ANALYSED: {
      DISPATCH: { label: 'Despacho', color: ExperienceTagResult.DISPATCH.color },
      SENDSEEM: { label: 'Emitir parecer', color: ExperienceTagResult.SEND_SEEM.color },
      CANCEL: { label: 'Cancelar', color: ExperienceTagResult.CANCELLED.color },
      RETURN: { label: 'Devolver', color: ExperienceTagResult.RETURNED.color },
    },
    DISPATCH: {
      APPROVE: { label: 'Aprovar', color: ExperienceTagResult.APPROVED.color },
      REJECT: { label: 'Rejeitar', color: ExperienceTagResult.REJECTED.color },
    },
    APPROVED: {
      CANCEL: { label: 'Cancelar', color: ExperienceTagResult.CANCELLED.color },
      SELECTION: { label: 'Seleção', color: ExperienceTagResult.SELECTION.color },
    },
    SELECTION: {
      COLABORATION: { label: 'Iniciar colaboração', color: ExperienceTagResult.IN_COLABORATION.color },
      CANCEL: { label: 'Cancelar', color: ExperienceTagResult.CANCELLED.color },
    },
    IN_COLABORATION: {
      CLOSE: { label: 'Fechar', color: ExperienceTagResult.CLOSED.color },
    },
    RETURNED: {
      SUBMIT: { label: 'Sumbeter', color: ExperienceTagResult.SUBMITTED.color },
    },
    SEND_SEEM: {
      SENDEXTERNAL: { label: 'Sistema externo', color: ExperienceTagResult.EXTERNAL_SYSTEM.color },
      DISPATCH: { label: 'Despacho', color: ExperienceTagResult.DISPATCH.color },
      RETURN: { label: 'Devolver', color: ExperienceTagResult.RETURNED.color },
    },
    EXTERNAL_SYSTEM: {
      SENDSEEM: { label: 'Emitir parecer', color: ExperienceTagResult.SEND_SEEM.color },
    },
    REJECTED: {},
    CANCELLED: {},
    CLOSED: {},
  };

  static eventToDispatchDecisionMapper: Partial<Record<ExperienceStatusEvent, ExperienceDispatchDecision>> = {
    APPROVE: ExperienceDispatchDecision.ACCEPTED,
    REJECT: ExperienceDispatchDecision.REJECTED,
    RETURN: ExperienceDispatchDecision.RETURNED,
  }

  static decisionToStateMapper: Record<ExperienceDispatchDecision, ExperienceStatus> = {
    ACCEPTED: ExperienceStatus.APPROVED,
    REJECTED: ExperienceStatus.REJECTED,
    RETURNED: ExperienceStatus.RETURNED,
  }
}
