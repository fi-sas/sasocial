export class UserInterestStatsModel {
    SUBMITTED: number;
    WAITING: number;
    APPROVED: number;
    INTERVIEWED: number;
    ACCEPTED: number;
    DISPATCH: number;
    CANCELLED: number;
    CLOSED: number;
    ANALYSED: number;
    DECLINED: number;
    WITHDRAWAL: number;
    COLABORATION: number;
    NOT_SELECTED: number;
  }