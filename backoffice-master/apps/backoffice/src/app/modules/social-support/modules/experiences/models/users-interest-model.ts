import { UsersModule } from '@fi-sas/backoffice/modules/users/users.module';

export class UsersInterestModel {
  id: number;
  experience_id: number;
  user_id: number;
  is_interest: boolean;
  user?: UsersModule;
  updated_at: string;
  created_at: string;
}

export const UsersInterestTagResult = {
  SUBMITTED: { color: '#4d9de0', label: 'Submetida' },
  ANALYSED: { color: '#7768AE', label: 'Analisada' },
  INTERVIEWED: { color: '#88ccf1', label: 'Entrevista' },
  APPROVED: { color: '#0d69dd', label: 'Aprovada' },
  WAITING: { color: '#f8ca00', label: 'Em lista de espera' },
  NOT_SELECTED: { color: '#C1666B', label: 'Não selecionado' },
  ACCEPTED: { color: '#9DBBAE', label: 'Aceite' },
  COLABORATION: { color: '#1ba974', label: 'Em colaboração' },
  WITHDRAWAL: { color: '#246A73', label: 'Em desistência' },
  DECLINED: { color: '#D0021B', label: 'Rejeitada' },
  CANCELLED: { color: '#A4A4A4', label: 'Cancelada' },
  CLOSED: { color: '#000000', label: 'Fechada' },
  DISPATCH: { color: 'orange', label: 'Despacho' },
  WITHDRAWAL_ACCEPTED: { color: '#246A73', label: 'Desistência aceite' },
}
