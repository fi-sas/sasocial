import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

import { first, finalize, map } from 'rxjs/operators';
import * as moment from 'moment';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationsService } from '../../../ss_configurations/services/configurations.service';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';
import { ExperienceModel } from '@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model';
import { ExperiencesService } from '@fi-sas/backoffice/modules/social-support/modules/experiences/services/experiences.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { Observable } from 'rxjs';

@Component({
  selector: 'fi-sas-form-experience',
  templateUrl: './form-experience.component.html',
  styleUrls: ['./form-experience.component.less'],
})
export class FormExperienceComponent implements OnInit {
  idToUpdate = null;
  submitted = false;
  experience: ExperienceModel = null;
  experienceLoading = false;

  currentAcademicYear: string = null;

  academicYears: AcademicYearModel[] = [];
  academicYearsLoading = false;
  organicUnits: OrganicUnitsModel[] = [];
  organicUnitsLoading = false;
  currentAccounts: AccountModel[] = [];
  currentAccountsLoading = false;

  isScheduleModalVisible = false;
  scheduleModalBegin = null;
  scheduleModalEnd = null;
  scheduleActiveId = null;
  schedules = {
    1: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    2: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    3: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    4: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    5: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    6: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    7: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    8: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    9: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    10: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    11: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    12: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    13: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    14: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    15: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    16: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    17: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    18: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
    19: {
      active: false,
      begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate(),
    },
    20: {
      active: false,
      begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
    },
    21: {
      active: false,
      begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(),
      end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate(),
    },
  };
  experienceForm = new FormGroup({
    academic_year: new FormControl(null, [Validators.required]),
    publish_date: new FormControl(new Date(), [Validators.required]),
    application_deadline_date: new FormControl(moment().add(1, 'd').toDate(), [Validators.required]),
    organic_unit_id: new FormControl(null, [Validators.required]),
    address: new FormControl('', [Validators.required, trimValidation]),
    experience_responsible_id: new FormControl(null, [Validators.required]),
    experience_advisor_id: new FormControl(null, [Validators.required]),
    number_candidates: new FormControl(1, [Validators.min(1)]),
    number_simultaneous_candidates: new FormControl(1, [Validators.min(1)]),
    interval_job: new FormControl(
      [moment().add(1, 'd').toDate(), moment().add(3, 'd').toDate()],
      [Validators.required]
    ),
    number_weekly_hours: new FormControl(0, [Validators.required, Validators.min(0)]),
    total_hours_estimation: new FormControl(0, [Validators.required, Validators.min(0)]),
    holydays_availability: new FormControl(false, [Validators.required]),
    attachment_file_id: new FormControl(null, []),
    payment_model: new FormControl('VH', [Validators.required]),
    payment_value: new FormControl(1, [Validators.required, Validators.min(0.01)]),
    perc_student_iban: new FormControl({ value: 100, disabled: true }, [Validators.required, Validators.min(0), Validators.max(100)]),
    perc_student_ca: new FormControl({ value: null, disabled: true }),
    translations: new FormArray([]),
  });
  translations = this.experienceForm.get('translations') as FormArray;
  languagesLoading = false;
  languages: LanguageModel[] = null;
  ca_required = false;

  responsiblesProfilesIds: number[] = [];

  publishDate = (date) => moment(date).isBefore(new Date(), 'd');
  disabledIntervalJob = (date) => moment(date).isSameOrBefore();

  constructor(
    public experienceService: ExperiencesService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activateRoute: ActivatedRoute,
    private organicUnitService: OrganicUnitsService,
    private currentAccountService: CurrentAccountsService,
    private languagesService: LanguagesService,
    private academicYearsService: AcademicYearsService,
    private authService: AuthService,
    private configurationsService: ConfigurationsService
  ) {
    this.configurationsService
      .getResponsibleProfileIds()
      .pipe(first())
      .subscribe((ids) => (this.responsiblesProfilesIds = ids));

    this.canUseCurrentAccountConfig().subscribe((canUse) => {
      if (!canUse) {
        this.experienceForm.get('perc_student_iban').setValue(100);
        this.experienceForm.get('perc_student_iban').disable();
        return;
      }

      this.experienceForm.get('perc_student_ca').setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
      this.experienceForm.get('perc_student_ca').enable();
      this.experienceForm.get('perc_student_iban').enable();
      this.experienceForm.get('perc_student_iban').markAsTouched();
    });
  }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');

        this.experienceService.read(parseInt(data.get('id'), 10), {}).subscribe(
          (result) => {
            this.experience = result.data[0];
            this.experienceForm.patchValue({
              ...result.data[0],
              interval_job: [result.data[0].start_date, result.data[0].end_date],
              perc_student_iban: result.data[0].perc_student_iban * 100,
              perc_student_ca: result.data[0].perc_student_ca * 100,
            });

            if (result.data[0].schedule) {
              result.data[0].schedule.map((s) => {
                this.schedules[s.schedule_id].active = true;
                this.schedules[s.schedule_id].begin = s.time_begin;
                this.schedules[s.schedule_id].end = s.time_end;
              });
            }
            this.loadLanguages();
          },
          () => {
            this.location.back();
          }
        );
      } else {
        this.loadLanguages();
      }
    });

    this.loadAcademicYears();
    this.loadOrganicUnits();
    this.loadCurrentAccounts();
  }

  loadLanguages() {
    this.languagesLoading = true;

    this.languagesService
      .list(1, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.languagesLoading = false))
      )
      .subscribe((results) => {
        // Force PT to always be the first language
        this.languages = [
          results.data.find((l) => l.acronym.toLowerCase() === 'pt'),
          ...results.data.filter((l) => l.acronym.toLowerCase() !== 'pt'),
        ];
        for (const iterator of this.languages) {
          this.addTranslation(iterator.id);
        }
      });
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);

    return language ? language.name : 'Sem informação';
  }

  async addTranslation(language_id: number) {
    const translations = this.experienceForm.controls.translations as FormArray;

    if (this.experience && this.experience.translations.length > 0) {
      const result = await this.experience.translations.filter((x) => x.language_id === language_id);
      translations.push(
        new FormGroup({
          language_id: new FormControl(language_id, Validators.required),
          title: new FormControl(result[0].title, [Validators.required, trimValidation]),
          description: new FormControl(result[0].description, [Validators.required, trimValidation]),
          job: new FormControl(result[0].job, [Validators.required, trimValidation]),
          proponent_service: new FormControl(result[0].proponent_service, [Validators.required, trimValidation]),
          applicant_profile: new FormControl(result[0].applicant_profile, [Validators.required, trimValidation]),
          selection_criteria: new FormControl(result[0].selection_criteria, [Validators.required, trimValidation]),
        })
      );
    } else {
      translations.push(
        new FormGroup({
          language_id: new FormControl(language_id, Validators.required),
          title: new FormControl('', [Validators.required, trimValidation]),
          description: new FormControl('', [Validators.required, trimValidation]),
          job: new FormControl('', [Validators.required, trimValidation]),
          proponent_service: new FormControl('', [Validators.required, trimValidation]),
          applicant_profile: new FormControl('', [Validators.required, trimValidation]),
          selection_criteria: new FormControl('', [Validators.required, trimValidation]),
        })
      );
    }
  }

  loadAcademicYears() {
    this.academicYearsLoading = true;
    this.academicYearsService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.academicYearsLoading = false))
      )
      .subscribe((results) => {
        this.academicYears = results.data;
        const currentYear = results.data.filter((year) => year.current === true);

        if (currentYear.length > 0) {
          this.currentAcademicYear = currentYear[0].academic_year;
          this.experienceForm.get('academic_year').setValue(currentYear[0].academic_year);
        }
      });
  }

  loadOrganicUnits() {
    this.organicUnitsLoading = true;

    this.organicUnitService
      .list(1, -1, null, null, { withRelated: false, active: true })
      .pipe(
        first(),
        finalize(() => (this.organicUnitsLoading = false))
      )
      .subscribe((results) => (this.organicUnits = results.data));
  }

  loadCurrentAccounts() {
    this.currentAccountsLoading = true;

    this.currentAccountService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.currentAccountsLoading = false))
      )
      .subscribe((results) => (this.currentAccounts = results.data));
  }

  public perc_student_changed(perc_student_iban, perc_student_ca) {
    if (perc_student_iban != null) {
      this.experienceForm.get('perc_student_ca').setValue(100 - perc_student_iban);
    }

    if (perc_student_ca != null) {
      this.experienceForm.get('perc_student_iban').setValue(100 - perc_student_ca);
    }
  }

  public scheduleChanged(id: number, value: boolean) {
    this.scheduleActiveId = id;
    this.scheduleModalBegin = this.schedules[this.scheduleActiveId].begin;
    this.scheduleModalEnd = this.schedules[this.scheduleActiveId].end;
    this.isScheduleModalVisible = true;
  }

  handleScheduleModalCancel() {
    this.isScheduleModalVisible = false;
  }

  handleScheduleModalOk() {
    this.isScheduleModalVisible = false;

    this.schedules[this.scheduleActiveId].begin = this.scheduleModalBegin;
    this.schedules[this.scheduleActiveId].end = this.scheduleModalEnd;
  }

  submit(valid: boolean, value: any) {
    const formValue = { ...value };
    this.submitted = true;
    let error = false;
    if (
      this.experienceForm.get('total_hours_estimation').value < this.experienceForm.get('number_weekly_hours').value
    ) {
      error = true;
    }
    if (valid && !error) {
      this.submitted = false;
      this.experienceLoading = true;

      formValue.schedule = Object.keys(this.schedules)
        .filter((s) => this.schedules[s].active)
        .map((s) => {
          return {
            schedule_id: s,
            time_begin: this.schedules[s].begin,
            time_end: this.schedules[s].end,
          };
        });
      formValue.start_date = formValue.interval_job[0];
      formValue.end_date = formValue.interval_job[1];
      delete formValue.interval_job;

      if (this.experienceForm.get('perc_student_ca').enabled) {
        formValue.perc_student_ca = formValue.perc_student_ca / 100;
      }
      formValue.perc_student_iban = formValue.perc_student_iban ? formValue.perc_student_iban / 100 : 0;

      // TODO: Remove this
      formValue.status = 'SUBMITTED';
      if (this.experience && this.authService.hasPermission('social_scholarship:experiences:update')) {
        this.experienceService
          .update(this.idToUpdate, formValue)
          .pipe(
            first(),
            finalize(() => (this.experienceLoading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(MessageType.success, 'Oferta alterada com sucesso');
            this.location.back();
          });
      } else if (this.authService.hasPermission('social_scholarship:experiences:create')) {
        this.experienceService
          .create(formValue)
          .pipe(
            first(),
            finalize(() => (this.experienceLoading = false))
          )
          .subscribe((result) => {
            this.uiService.showMessage(MessageType.success, 'Oferta inserida com sucesso');
            this.router.navigate(['social-support', 'experiences', 'list']);
          });
      } else {
        this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
        return;
      }
    }
  }

  returnButton() {
    this.router.navigate(['social-support', 'experiences', 'list']);
  }

  copyField(fieldName: string, langId: number) {
    const fromFormControl = this.experienceForm.get(`translations.0.${fieldName}`);
    const toFormControl = this.experienceForm.get(`translations.${langId}.${fieldName}`);
    toFormControl.setValue(fromFormControl.value);
    toFormControl.markAsTouched();
    toFormControl.markAsDirty();
  }

  private canUseCurrentAccountConfig(): Observable<boolean> {
    return this.configurationsService.list(0,0).pipe( first(),
      map((result) => {
        return result.data.length && result.data[0].HAS_CURRENT_ACCOUNT ? JSON.parse( result.data[0].HAS_CURRENT_ACCOUNT) : false
      }));
  }
}
