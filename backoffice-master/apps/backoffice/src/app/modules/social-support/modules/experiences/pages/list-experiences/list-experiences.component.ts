import { Component, OnInit } from '@angular/core';
import { ExperiencesService } from '@fi-sas/backoffice/modules/social-support/modules/experiences/services/experiences.service';
import {
  ExperienceData,
  ExperienceModel,
  ExperienceStatus,
} from '@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { CoursesService } from '@fi-sas/backoffice/modules/configurations/services/courses.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { SendDispatchData } from '@fi-sas/backoffice/modules/social-support/models';
import { DispatchModalComponent } from '../../../ss-shared/components';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { ChangingApplicationEndDateModalComponent } from '../../components/changing-application-end-date-modal/changing-application-end-date-modal.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'fi-sas-list-experience',
  templateUrl: './list-experiences.component.html',
  styleUrls: ['./list-experiences.component.less'],
})
export class ListExperiencesComponent extends TableHelper implements OnInit {
  status = ExperienceData.status;
  statusMachine = ExperienceData.statusMachine;
  experienceStatuses = [];
  languages = [];

  // FILTERS
  academic_years_loading = false;
  courses_loading = false;
  experience_advisors_loading = false;
  organic_units_loading = false;
  academic_years: AcademicYearModel[] = [];
  courses: CourseModel[] = [];
  experience_advisors: UserModel;
  organic_units: OrganicUnitsModel[] = [];
  schools: OrganicUnitsModel[] = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExperience: ExperienceModel = null;
  changeStatusModalActions = {};
  changeStatusModalExperienceAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  changeStatusRequirementNumber = null;
  advisor_id = null;

  // NOTIFICATION MODAL STATUS
  isNotificationModalVisible = false;
  notificationModalExperience: ExperienceModel = null;
  notificationModalCourseYear = null;
  notificationModalCourseId = null;
  notificationModalSchoolId = null;
  notificationModalMessage = null;
  notificationModalTargetUsers = [];
  notificationModalSearchTargetsLoading = false;
  notificationModalNotifyLoading = false;
  notificationModalStudentWithoutActiveCol = null;

  currentAcademicYear: AcademicYearModel[];

  ExperienceStatus = ExperienceStatus;
  
  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private organicUnitsService: OrganicUnitsService,
    public experienceService: ExperiencesService,
    private languageService: LanguagesService,
    private academicYearsService: AcademicYearsService,
    private coursesService: CoursesService,
    private authService: AuthService,
    private modalService: NzModalService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadAcademicYears();
    this.loadCourses();
    this.loadOrganicUnits();
    this.loadLanguages();
    this.loadStatuses();
    this.persistentFilters['searchFields'] = 'title,description,job';
    this.persistentFilters['withRelated'] = [
      'experience_responsible',
      'experience_advisor',
      'organic_unit',
      'translations',
      'users_colaboration'
    ].join(',');

    let status: string = this.activatedRoute.snapshot.queryParamMap.get('status');
    if (status) {
      this.persistentFilters['status'] = status;
    }
    let academicYear: string = this.activatedRoute.snapshot.queryParamMap.get('academicYear');
    if (academicYear) {
      this.filters.academic_year = academicYear;
    } else {
      this.loadCurrentAcademicYear();
    }

    this.columns.push(
      {
        key: 'created_at',
        label: 'Data de submissão',
        template: (data) => moment(data.created_at).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: 'start_date',
        label: 'Início da Colaboração',
        template: (data) => moment(data.start_date).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: 'end_date',
        label: 'Fim da Colaboração',
        template: (data) => moment(data.end_date).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: 'publish_date',
        label: 'Data de publicação',
        template: (data) => {
          return data.publish_date != null
            ? moment(data.publish_date).format('DD/MM/YYYY') +
                (moment(new Date()).format('DD/MM/YYYY') >= moment(data.publish_date).format('DD/MM/YYYY') &&
                (data.status === 'SUBMITTED' || data.status === 'SELECTION')
                  ? String.fromCodePoint(0x2757)
                  : '')
            : '';
        },
        sortable: true,
      },
      {
        key: null,
        label: 'Titulo da colaboração',
        template: (data) => (data.translations[0] ? data.translations[0].title : null),
        sortable: false,
      },
      {
        key: null,
        label: 'Requerente',
        template: (data) => (data.experience_responsible ? data.experience_responsible.name : null),
        sortable: false,
      },
      {
        key: null,
        label: 'UA',
        template: (data) => (data.organic_unit ? data.organic_unit.name : null),
        sortable: false,
      },
      {
        key: null,
        label: 'Colab. ativos',
        template: (data) => {
          return data.users_colaboration ? data.users_colaboration.length : ' - ';
        },
        sortable: false,
      },
      {
        key: null,
        label: 'Colab. necessários',
        template: (data) => {
          return data.number_simultaneous_candidates ? data.number_simultaneous_candidates : ' - ';
        },
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: this.status,
        filters: this.experienceStatuses.map((status) => {
          return {
            text: status.label,
            value: status.key,
          };
        }),
        showFilter: true,
        filterMultiple: false,
      }
    );

    this.initTableData(this.experienceService);
  }

  loadLanguages() {
    this.loading = true;

    this.languageService
      .list(1, -1, 'id', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => (this.languages = result.data));
  }

  getCandidateReport(id: number){
    this.loading = true;
    this.experienceService.getCandidateReport(id).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
        'Relatório gerado com sucesso'
      );
    });
  }


  loadStatuses() {
    this.experienceStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  loadAcademicYears() {
    this.academic_years_loading = true;
    this.academicYearsService
      .list(1, -1, 'start_date', 'descend')
      .pipe(
        first(),
        finalize(() => (this.academic_years_loading = false))
      )
      .subscribe((results) => (this.academic_years = results.data));
  }

  loadCurrentAcademicYear() {
    this.academicYearsService
      .getCurrentAcademicYear()
      .pipe(first())
      .subscribe((result) => {
        this.currentAcademicYear = result.data;
        if (this.currentAcademicYear.length !== 0) {
          this.filters.academic_year = this.currentAcademicYear[0].academic_year;
          this.searchData(true);
        }
      });
  }

  loadCourses() {
    this.courses_loading = true;

    this.coursesService
      .list(1, -1, 'name', 'ascend', {
        active: true,
        organic_unit_id: this.notificationModalSchoolId,
      })
      .pipe(
        first(),
        finalize(() => (this.courses_loading = false))
      )
      .subscribe((results) => (this.courses = results.data));
  }

  loadOrganicUnits() {
    this.organic_units_loading = true;

    this.organicUnitsService
      .list(1, -1, 'name', 'ascend', {
        active: true,
      })
      .pipe(
        first(),
        finalize(() => (this.organic_units_loading = false))
      )
      .subscribe((results) => {
        this.organic_units = results.data;
        this.schools = results.data.filter((data) => data.is_school == true);
      });
  }

  changePublish(id: number, published: boolean, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:experiences:update')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if (!disabled) {
      this.uiService
        .showConfirm('Remover publicação', 'Pretende remover a publicação desta oferta?', 'Remover publicação')
        .pipe(first())
        .subscribe((confirm) => {
          if (confirm) {
            this.experienceService
              .changePublish(id, published)
              .pipe(first())
              .subscribe((result) => {
                this.uiService.showMessage(MessageType.success, 'Publicação de oferta removida com sucesso');
                this.searchData();
              });
          }
        });
    }
  }

  getUserInterestsReport(id: number, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:general-reports:report_by_user_interest')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if (!disabled) {
      this.uiService
        .showConfirm(
          'Manifestações de interesse',
          'Pretende gerar relatório de manifestações de interesse?',
          'Gerar relatório'
        )
        .pipe(first())
        .subscribe((confirm) => {
          if (confirm) {
            this.experienceService
              .userInterestsReport(id)
              .pipe(first())
              .subscribe((result) => {
                this.uiService.showMessage(MessageType.success, 'Relatório gerado com sucesso');
                this.searchData();
              });
          }
        });
    }
  }

  changeStateModal(experience: ExperienceModel, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:experiences:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if(!Object.keys(ExperienceData.statusMachine[experience.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalExperience = experience;
      this.changeStatusModalActions = ExperienceData.statusMachine[experience.status];
    }
  }

  edit(experience: ExperienceModel, disabled: boolean) {
    if (!disabled) {
      this.router.navigateByUrl('/social-support/experiences/edit/' + experience.id);
    }
   }

  handleChangeStatusOk() {
    if (!this.authService.hasPermission('social_scholarship:experiences:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.changeStatusModalLoading = true;

    const obj: any = {
      event: this.changeStatusModalExperienceAction.toUpperCase(),
      notes: this.changeStatusModalNotes,
    };
    if (this.changeStatusModalExperienceAction === 'approve') {
      if (this.advisor_id === null && this.changeStatusModalExperience.experience_advisor_id === null) {
        this.uiService.showMessage(MessageType.warning, 'O campo orientador é de preenchimento obrigatório');
      } else {
        if (this.advisor_id !== null) {
          obj.experience = { experience_advisor_id: this.advisor_id };
        }
      }
    }

    if (this.changeStatusRequirementNumber != null) {
      obj.experience = { requirement_number: this.changeStatusRequirementNumber };
    }

    if (this.changeStatusModalExperienceAction === 'return') {
      if (this.changeStatusModalNotes.length === 0) {
        this.uiService.showMessage(MessageType.warning, 'O campo observações é de preenchimento obrigatório');

        this.changeStatusModalLoading = false;
        return;
      }

      obj.experience = { return_reason: this.changeStatusModalNotes };
    }

    if(this.changeStatusModalExperience.status !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'DISPATCH'){
      this.experienceService
      .status(this.changeStatusModalExperience.id, obj)
      .pipe(
        first(),
        finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe((result) => {
        this.changeStatusModalExperience.status = result.data[0].status;
        this.handleChangeStatusCancel();
        this.uiService.showMessage(MessageType.success, 'Estado alterado com sucesso');
        this.searchData();
      });
    }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'DISPATCH'){
      if(this.form.valid){
        this.experienceService.sendDispatch(this.changeStatusModalExperience.id, {
          event: this.changeStatusModalExperienceAction.toUpperCase(),
          decision: this.form.get('decision').value,
          notes: this.changeStatusModalNotes,
        })
        .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
        .subscribe(() => {
          this.handleChangeStatusCancel();
          this.searchData();
        });
      }else{
        this.updateAndValidityForm(this.form);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalExperience.status === 'DISPATCH'){
      this.experienceService
      .sendDispatch(this.changeStatusModalExperience.id, { decision_dispatch: this.changeStatusModalExperienceAction.toUpperCase() })
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe(() => {
        this.handleChangeStatusCancel();
        this.searchData();
      });
    }else{
      this.changeStatusModalLoading = false;
    }
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }


  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalExperience = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalExperienceAction = null;
    this.changeStatusModalNotes = '';
    this.searchData();
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalExperienceAction = action;
  }

  notificationModal(experience: ExperienceModel, disabled: boolean) {
    if (!disabled) {
      this.isNotificationModalVisible = true;
      this.notificationModalExperience = experience;
    }
  }

  closingDateChangeModal(experience: ExperienceModel, disabled: boolean){
    if (!disabled) {
      const modalRef: NzModalRef<ChangingApplicationEndDateModalComponent> = this.modalService.create({
        nzTitle: 'Alterar Data Fecho de Candidaturas',
        nzContent: ChangingApplicationEndDateModalComponent,
        nzComponentParams: { experience},
        nzFooter: [
          { label: 'Sair', onClick: (_) => modalRef.close() },
          {
            label: 'Alterar',
            type: 'primary',
            onClick: (componentInstance) => {
              componentInstance.onSubmit().then(() => {
                componentInstance.onSubmit().then(() => {
                  this.experienceService.changeClosingDate(experience.id, componentInstance.form.value.new_end_date).pipe(first())
                  .subscribe(() => {
                    this.searchData();
                    modalRef.close();
                  });
                });
              }).catch(e => {
                console.log(e)
              });
            },
          },
        ],
      });
    }
  }

  handleNotificationSearchTargets() {
    this.notificationModalSearchTargetsLoading = true;

    this.experienceService
      .targetUsers(
        this.notificationModalExperience.id,
        this.notificationModalCourseYear,
        this.notificationModalCourseId,
        this.notificationModalSchoolId,
        this.notificationModalStudentWithoutActiveCol
      )
      .pipe(
        first(),
        finalize(() => (this.notificationModalSearchTargetsLoading = false))
      )
      .subscribe((result) => (this.notificationModalTargetUsers = result.data));
  }

  handleNotificationCancel() {
    this.isNotificationModalVisible = false;
    this.notificationModalExperience = null;
    this.notificationModalTargetUsers = [];
    this.notificationModalCourseYear = null;
    this.notificationModalCourseId = null;
    this.notificationModalSchoolId = null;
    this.notificationModalMessage = null;
    this.notificationModalStudentWithoutActiveCol = false;
  }

  handleNotificationOk() {
    this.notificationModalNotifyLoading = true;

    this.experienceService
      .sendNotifications(
        this.notificationModalExperience.id,
        this.notificationModalTargetUsers.map((user) => user.id)
      )
      .pipe(
        first(),
        finalize(() => (this.notificationModalNotifyLoading = false))
      )
      .subscribe(() => {
        this.handleNotificationCancel();
        this.uiService.showMessage(MessageType.success, 'Notificação(ões) enviada(s) sucesso');
      });
  }

  listComplete() {
    this.filters.created_at = null;
    this.filters.academic_year = null;
    this.filters.organic_unit_id = null;
    this.filters.experience_advisor_id = null;
    this.searchData(true);
  }

  /*dispatchModal(experience: ExperienceModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.experienceService.sendDispatch(experience.id, data),
        actions: Object.keys(ExperienceData.statusMachine.DISPATCH).map<{ key: ExperienceStatus; label: string }>(
          (key) => ({
            key: ExperienceData.eventToDispatchDecisionMapper[key],
            label: ExperienceData.statusMachine.DISPATCH[key].label,
          })
        ),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.onSubmit().then(() => {
              this.searchData();
              modalRef.close();
            });
          },
        },
      ],
    });
  }*/

  dispatchChangeStatues(experience: ExperienceModel, acceptDecision: boolean) {
    this.experienceService
      .sendDispatch(experience.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }

  getDispatchActions = () => {
    return Object.keys(ExperienceData.statusMachine.DISPATCH).map<{ key: ExperienceStatus; label: string }>(
      (key) => ({
        key: ExperienceData.eventToDispatchDecisionMapper[key],
        label: ExperienceData.statusMachine.DISPATCH[key].label,
      }));
  }


  generateReport(id: number) {
    this.experienceService
      .generateReport(id)
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Relatório gerado com sucesso.');
      });
  }
}
