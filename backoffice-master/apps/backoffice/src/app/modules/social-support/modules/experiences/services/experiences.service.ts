import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ApproveRejectModel } from '@fi-sas/backoffice/modules/social-support/models/approveReject';
import { ExperienceModel } from '@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SendDispatchData } from '../../../models';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class ExperiencesService extends Repository<ExperienceModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.EXPERIENCES';
    this.entity_url = 'SOCIAL_SUPPORT.EXPERIENCES_ID';
  }

  status(id: number, experience_status: ApproveRejectModel): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_STATUS', { id }),
      experience_status
    );
  }

  changePublish(id: number, published: boolean): Observable<Resource<ExperienceModel>> {
    return this.resourceService.update<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_CHANGE_PUBLISH', { id }),
      { published }
    );
  }

  changeClosingDate(id: number, new_end_date: any): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_CHANGE_CLOSING_DATE', { id }),
      { new_end_date }
    );
  }

  targetUsers(
    id: number,
    course_year: number,
    course_id: number,
    school_id: number,
    without_active_collaborations: string
  ): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();

    if (course_year) {
      params = params.set('course_year', String(course_year));
    }

    if (course_id) {
      params = params.set('course_id', String(course_id));
    }

    if (school_id) {
      params = params.set('school_id', String(school_id));
    }
    if(without_active_collaborations){
      params = params.set('without_active_collaborations', without_active_collaborations);
    }

    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_TARGET_USERS', { id }),
      { params }
    );
  }

  getCandidateReport(id: number): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_CANDIDATE_REPORT', { id }),
      {}
    );
  }
  
  sendNotifications(id: number, user_ids: number[]): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_SEND_NOTIFICATIONS', { id }),
      { user_ids }
    );
  }

  students(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    params?: {}
  ): Observable<Resource<UserModel>> {
    return this.resourceService
      .list<UserModel>(
        this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_USER_COLLABORATION', { ...this.persistentUrlParams }),
        { params: this.getQuery(pageIndex, pageSize, sortKey, sortValue, params) }
      )
      .pipe(first());
  }

  userInterestsReport(id: number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.read<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_USER_INTERESTS_REPORT', { id })
    );
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_STATUS', { id }),
      data
    );
  }

  generateReport(id: number) {
    return this.resourceService.read(this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_GENERATE_REPORT', { id }), {});
  }
}
