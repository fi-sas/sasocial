import { FormExternalUserComponent } from './pages/form-external-user/form-external-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExternalUsersComponent } from './pages/list-external-users/list-external-users.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormExternalUserComponent ,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'social_scholarship:external_entities:create' },
  },
  {
    path: 'edit/:id',
    component: FormExternalUserComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'social_scholarship:external_entities:update' },
  },
  {
    path: 'list',
    component: ListExternalUsersComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:external_entities:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalUsersRoutingModule { }
