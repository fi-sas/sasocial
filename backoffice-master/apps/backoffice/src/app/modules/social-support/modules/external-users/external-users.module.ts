import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExternalUsersRoutingModule } from './external-users-routing.module';
import { FormExternalUserComponent } from './pages/form-external-user/form-external-user.component';
import { ListExternalUsersComponent } from './pages/list-external-users/list-external-users.component';
import { ViewExternalUserComponent } from './pages/view-external-user/view-external-user.component';

@NgModule({
  declarations: [
    ListExternalUsersComponent,
    ViewExternalUserComponent,
    FormExternalUserComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ExternalUsersRoutingModule
  ]
})
export class ExternalUsersModule { }
