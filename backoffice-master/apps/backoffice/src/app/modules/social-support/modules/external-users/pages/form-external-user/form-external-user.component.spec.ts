import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormExternalUserComponent } from './form-external-users.component';

describe('FormExternalUserComponent', () => {
  let component: FormExternalUserComponent;
  let fixture: ComponentFixture<FormExternalUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormExternalUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExternalUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
