import { ExternalUserService } from './../../services/external-user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { countries } from '@fi-sas/backoffice/shared/common/countries';
import * as moment from 'moment';
import { ExternalUserModel, Gender } from '../../models/external-user.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-external-user',
  templateUrl: './form-external-user.component.html',
  styleUrls: ['./form-external-user.component.less'],
})
export class FormExternalUserComponent implements OnInit {
  externalUser: ExternalUserModel = null;

  idToUpdate = null;

  externalUserLoading = false;

  countries = countries;
  genders = Object.values(Gender);

  externalUserForm = new FormGroup({
    name: new FormControl(null, [Validators.required, trimValidation]),
    tin: new FormControl(null, [Validators.required]),
    gender: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.email, Validators.required]),
    phone: new FormControl(null, [Validators.required, trimValidation, Validators.min(0)]),
    address: new FormControl(null, [Validators.required, trimValidation]),
    postal_code: new FormControl(null, [Validators.required]),
    city: new FormControl(null, [Validators.required, trimValidation]),
    country: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required, trimValidation]),
    function_description: new FormControl(null, [Validators.required, trimValidation]),
    file_id: new FormControl(null)
  });

  loading = false;

  constructor(
    private externalUserService: ExternalUserService,
    private uiService: UiService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {
      this.loading = true;
      this.externalUserService
        .read(this.idToUpdate, {})
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((results) => {
          this.externalUserForm.patchValue({
            ...results.data[0],
            ...results.data[0].user,
          });
        });
    }
  }

  disableFutureDates = (current: Date) => {
    return moment(current).isAfter(new Date());
  };

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.externalUserLoading = true;

      if (this.idToUpdate && this.authService.hasPermission('social_scholarship:external_entities:update')) {
        this.externalUserService
          .patch(this.idToUpdate, formValue)
          .pipe(
            first(),
            finalize(() => (this.externalUserLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Utilizador externo alterado com sucesso'
            );
            this.location.back();
          });
      } else if (!this.idToUpdate && this.authService.hasPermission('social_scholarship:external_entities:create')) {
        this.externalUserService
          .create(formValue)
          .pipe(
            first(),
            finalize(() => (this.externalUserLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Utilizador externo inserido com sucesso'
            );
            this.router.navigate([
              'social-support',
              'external-users',
              'list',
            ]);
          });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não tem acesso ao serviço solicitado'
        );
        this.externalUserLoading = false
        return;
      }
    } else {
      for (const i in this.externalUserForm.controls) {
        if (this.externalUserForm.controls[i]) {
          this.externalUserForm.controls[i].markAsDirty();
          this.externalUserForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton() {
    this.router.navigate([
      'social-support',
      'external-users',
      'list',
    ]);
  }
}
