import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExternalUsersComponent } from './list-external-users.component';

describe('ListExternalUsersComponent', () => {
  let component: ListExternalUsersComponent;
  let fixture: ComponentFixture<ListExternalUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExternalUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExternalUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
