import { ExternalUserModel } from '../../models/external-user.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-external-user',
  templateUrl: './view-external-user.component.html',
  styleUrls: ['./view-external-user.component.less']
})
export class ViewExternalUserComponent implements OnInit {

  @Input() externalUser: ExternalUserModel = null;

  constructor() { }

  ngOnInit() {}

}
