import { ExperienceUserInterestModel } from "../../experience-user-interests/models/experience-user-interest.model";

export class PaymentGridModel {
    id: number;
    user_interest_id: number;
    payment_month: number;
    payment_year: number;
    payment_model: string;
    payment_value: number;
    number_hours: number;
    payment_value_iban: number;
    payment_value_ca: number;
    user_account_id: number;
    status: string;
    notes: string;
    hours_pay: number;
    hours_transit: number;
    is_interest: boolean;
    user_interest? : ExperienceUserInterestModel;
    iban: string;
    updated_at: string;
    created_at: string;
  }