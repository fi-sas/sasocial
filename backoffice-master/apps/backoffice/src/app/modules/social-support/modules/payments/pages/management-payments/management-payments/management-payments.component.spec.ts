import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementPaymentsComponent } from './management-payments.component';

describe('ManagementPaymentsComponent', () => {
  let component: ManagementPaymentsComponent;
  let fixture: ComponentFixture<ManagementPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagementPaymentsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
