import { Component, OnInit } from '@angular/core';

import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ExperiencesService } from '../../../services/experiences.service';
import { finalize, first } from 'rxjs/operators';
import { ExperienceModel } from '../../../../experiences/models/experience.model';
import { ExperienceUserInterestModel } from '../../../../experience-user-interests/models/experience-user-interest.model';
import { UserInterestsService } from '../../../services/user-interests.service';
import { PaymentsGridService } from '../../../services/payments-grid.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { PaymentGridModel } from '../../../models/payment-grid.model';
import { NzModalService } from 'ng-zorro-antd';
import { id } from '@swimlane/ngx-charts/release/utils';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-management-payments',
  templateUrl: './management-payments.component.html',
  styleUrls: ['./management-payments.component.less'],
})
export class ManagementPaymentsComponent extends TableHelper implements OnInit {
  list_months = [
    { month: 1, description: 'Janeiro' },
    { month: 2, description: 'Fevereiro' },
    { month: 3, description: 'Março' },
    { month: 4, description: 'Abril' },
    { month: 5, description: 'Maio' },
    { month: 6, description: 'Junho' },
    { month: 7, description: 'Julho' },
    { month: 8, description: 'Agosto' },
    { month: 9, description: 'Setembro' },
    { month: 10, description: 'Outubro' },
    { month: 11, description: 'Novembro' },
    { month: 12, description: 'Dezembro' },
  ];

  list_status = [
    { status: 'APPROVED', description: 'Aprovado' },
    { status: 'PAYED', description: 'Pago' },
    { status: 'PENDING', description: 'Pendente' },
    { status: 'REJECTED', description: 'Rejeitado' },
  ];

  current_date = new Date();
  current_month = this.current_date.getMonth() + 1;
  current_year = this.current_date.getFullYear();
  loading = false;
  list_experiences_advisor: ExperienceModel[] = new Array();

  list_user_interests: ExperienceUserInterestModel[] = new Array();
  list_years: Number[] = new Array();

  accessModalVisible = false;

  experienceForm = new FormGroup({
    month: new FormControl(this.current_month),
    year: new FormControl(this.current_year),
    experience: new FormControl(null),
    interest: new FormControl(null),
    status: new FormControl(null),
  });

  payment_grid_form = new FormGroup({
    iban: new FormControl(null, trimValidation),
    hours_pay: new FormControl(null, [Validators.min(0)]),
    hours_transit: new FormControl(null, [Validators.min(0)]),
    payment_value_iban: new FormControl(null, [Validators.min(0)]),
    payment_value_cc: new FormControl(null, [Validators.min(0)]),
    user_account_id: new FormControl(null, []),
  });

  currentAccounts: AccountModel[] = [];
  currentAccountsLoading = false;
  submit = false;
  constructor(
    public experienceService: ExperiencesService,
    public experienceUserInterestsService: UserInterestsService,
    public paymentGridService: PaymentsGridService,
    router: Router,
    private modalService: NzModalService,
    private currentAccountService: CurrentAccountsService,
    private authService: AuthService,
    activatedRoute: ActivatedRoute,
    uiService: UiService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadCurrentAccounts();

    for (var i = 0; i <= 3; i++) {
      let year = this.current_year - i;
      this.list_years.push(year);
    }
    //this.persistentFilters['isAdvisor'] = true;
    this.persistentFilters['withRelated'] = 'user_interest,user_account';
    this.initTableData(this.paymentGridService);
    this.experienceService
      .listPreviouslyActive()
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.loading = false;
        this.list_experiences_advisor = result.data;
      });
  }

  get f() {
    return this.payment_grid_form.controls;
  }

  selectUserInterest() {
    if (this.experienceForm.controls.experience.value !== null) {
      this.experienceForm.controls.interest.setValue(null);
      const experience_id = this.experienceForm.controls.experience.value;
      this.experienceUserInterestsService
        .listByStatusAndExperience(experience_id)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((result) => {
          this.loading = false;
          this.list_user_interests = result.data;
        });
    }
  }

  applyFilters() {
    this.persistentFilters = {};
    //this.persistentFilters['isAdvisor'] = true;
    this.persistentFilters['withRelated'] = 'user_interest,user_account';
    if (this.experienceForm.controls.status.value !== null) {
      this.persistentFilters['status'] = this.experienceForm.controls.status.value;
    }
    if (this.experienceForm.controls.month.value !== null) {
      this.persistentFilters['payment_month'] = this.experienceForm.controls.month.value;
    }
    if (this.experienceForm.controls.year.value !== null) {
      this.persistentFilters['payment_year'] = this.experienceForm.controls.year.value;
    }
    
    if (this.experienceForm.controls.interest.value !== null) {
      this.persistentFilters['user_interest_id'] = this.experienceForm.controls.interest.value;
    }
    this.initTableData(this.paymentGridService);
  }

  createPaymentGrid() {
    if (!this.authService.hasPermission('social_scholarship:payment-grid:create')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if (
      this.experienceForm.controls.month.value === null ||
      this.experienceForm.controls.year.value === null ||
      this.experienceForm.controls.experience.value === null ||
      this.experienceForm.controls.interest.value === null
    ) {
      this.uiService.showMessage(
        MessageType.error,
        "Deve indicar 'Mês', 'Ano', 'Oferta' e 'Utilizador em colaboração'"
      );
    }

    if (this.experienceForm.controls.interest.value !== null) {
      this.paymentGridService
        .createPaymentGrid(
          this.experienceForm.controls.month.value,
          this.experienceForm.controls.year.value,
          this.experienceForm.controls.interest.value
        )
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.loading = false;
          this.uiService.showMessage(MessageType.success, 'Grelha de criada com sucesso');
          this.applyFilters();
        });
    }
  }

  hours_transit = 0;
  toggleAccessModal(payment_grid: number, status: string) {
    if (
      (status === 'PENDING' && !this.authService.hasPermission('social_scholarship:payment-grid:update')) ||
      (status === 'REOPEN' &&
        (!this.authService.hasPermission('social_scholarship:payment-grid:reopen') ||
          !this.authService.hasPermission('social_scholarship:payment-grid:update')))
    ) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if (payment_grid !== null) {
      this.selected_payment_grid = payment_grid;
      this.payment_grid_form.controls.iban.setValue(this.selected_payment_grid.iban);
      this.payment_grid_form.controls.user_account_id.setValue(this.selected_payment_grid.user_account_id);
      this.payment_grid_form.controls.hours_pay.setValue(this.selected_payment_grid.hours_pay);
      this.payment_grid_form.controls.payment_value_iban.setValue(
        this.selected_payment_grid.payment_value_iban.toFixed(2)
      );
      this.payment_grid_form.controls.payment_value_cc.setValue(this.selected_payment_grid.payment_value_ca.toFixed(2));
      this.hours_transit = this.selected_payment_grid.hours_transit;
    }
    this.accessModalVisible = !this.accessModalVisible;
    if (this.accessModalVisible === false) {
      this.initTableData(this.paymentGridService);
    }
  }

  selected_payment_grid = null;

  changePaymentIban() {
    if (this.payment_grid_form.controls.hours_pay.value !== this.selected_payment_grid.number_hours) {
      if (this.selected_payment_grid.user_interest.experience.payment_value != null) {
        const valor_total =
          this.payment_grid_form.controls.hours_pay.value *
          this.selected_payment_grid.user_interest.experience.payment_value;
        if (
          this.payment_grid_form.controls.payment_value_iban.value <= valor_total &&
          this.payment_grid_form.controls.payment_value_cc.value !== 0
        ) {
          const value = valor_total - this.payment_grid_form.controls.payment_value_iban.value;
          this.payment_grid_form.controls.payment_value_cc.setValue(value.toFixed(2));
        }
      }
    } else {
      const totalvalue = this.selected_payment_grid.payment_value_iban + this.selected_payment_grid.payment_value_ca;
      if (this.payment_grid_form.controls.payment_value_iban.value <= totalvalue) {
        const value = totalvalue - this.payment_grid_form.controls.payment_value_iban.value;
        this.payment_grid_form.controls.payment_value_cc.setValue(value.toFixed(2));
      }
    }
  }

  changePaymentCC() {
    if (this.payment_grid_form.controls.hours_pay.value !== this.selected_payment_grid.number_hours) {
      if (this.selected_payment_grid.user_interest.experience.payment_value != null) {
        const valor_total =
          this.payment_grid_form.controls.hours_pay.value *
          this.selected_payment_grid.user_interest.experience.payment_value;
        if (
          this.payment_grid_form.controls.payment_value_cc.value <= valor_total &&
          this.payment_grid_form.controls.payment_value_iban.value !== 0
        ) {
          const value = valor_total - this.payment_grid_form.controls.payment_value_cc.value;
          this.payment_grid_form.controls.payment_value_iban.setValue(value.toFixed(2));
        }
      }
    } else {
      const totalvalue = this.selected_payment_grid.payment_value_iban + this.selected_payment_grid.payment_value_ca;
      if (this.payment_grid_form.controls.payment_value_cc.value <= totalvalue) {
        const value = totalvalue - this.payment_grid_form.controls.payment_value_cc.value;
        this.payment_grid_form.controls.payment_value_iban.setValue(value.toFixed(2));
      }
    }
  }

  changeHoursPay() {
    this.validateHours();
    const totalhours = this.selected_payment_grid.number_hours;
    if (this.payment_grid_form.controls.hours_pay.value <= totalhours) {
      let valor = this.convert_hours_minutes(this.payment_grid_form.controls.hours_pay.value);
      const hourstransit = this.convert_hours_minutes(totalhours) - valor;
      this.hours_transit = this.format_minutes_to_hours(hourstransit);
      if (this.selected_payment_grid.user_interest.experience.payment_value !== null) {
        const perct_iban = this.selected_payment_grid.user_interest.experience.perc_student_iban;
        const perct_cc = this.selected_payment_grid.user_interest.experience.perc_student_ca;
        const total_pay = (valor / 60) * this.selected_payment_grid.user_interest.experience.payment_value;
        const value_iba = (total_pay * perct_iban).toFixed(2);
        const value_cc = (total_pay * perct_cc).toFixed(2);
        this.payment_grid_form.controls.payment_value_iban.setValue(value_iba);
        this.payment_grid_form.controls.payment_value_cc.setValue(value_cc);
      }
    } else {
      if (this.selected_payment_grid.user_interest.experience.payment_value !== null) {
        const perct_iban = this.selected_payment_grid.user_interest.experience.perc_student_iban;
        const perct_cc = this.selected_payment_grid.user_interest.experience.perc_student_ca;
        const total_pay =
          this.payment_grid_form.controls.hours_pay.value *
          this.selected_payment_grid.user_interest.experience.payment_value;
        const value_iba = (total_pay * perct_iban).toFixed(2);
        const value_cc = (total_pay * perct_cc).toFixed(2);
        this.payment_grid_form.controls.payment_value_iban.setValue(value_iba);
        this.payment_grid_form.controls.payment_value_cc.setValue(value_cc);
      }
      this.hours_transit = 0;
    }
  }

  editPaymentGrid() {
    if (
      (this.selected_payment_grid.status === 'PENDING' &&
        !this.authService.hasPermission('social_scholarship:payment-grid:update')) ||
      (this.selected_payment_grid.status === 'REOPEN' &&
        (!this.authService.hasPermission('social_scholarship:payment-grid:reopen') ||
          !this.authService.hasPermission('social_scholarship:payment-grid:update')))
    ) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }
    this.submit = true;
    if (this.payment_grid_form.valid) {
      let sendValues = new PaymentGridModel();
      this.submit = false;
      //if permission social_scholarship:payment-grid:financial is not granted
      //forces value to stay the same
      sendValues.user_account_id = this.selected_payment_grid.user_account_id;
      if (this.authService.hasPermission('social_scholarship:payment-grid:financial')) {
        sendValues.user_account_id = this.payment_grid_form.controls.user_account_id.value;
      }

      sendValues.user_interest_id = this.selected_payment_grid.user_interest_id;
      sendValues.number_hours = this.selected_payment_grid.number_hours;
      sendValues.payment_model = this.selected_payment_grid.payment_model;
      sendValues.payment_year = this.selected_payment_grid.payment_year;
      sendValues.payment_month = this.selected_payment_grid.payment_month;
      sendValues.status = this.selected_payment_grid.status;
      sendValues.hours_pay = this.payment_grid_form.controls.hours_pay.value;
      sendValues.hours_transit = this.hours_transit;
      sendValues.hours_transit = this.hours_transit;
      sendValues.iban = this.payment_grid_form.controls.iban.value;
      sendValues.payment_value_iban = this.payment_grid_form.controls.payment_value_iban.value;
      sendValues.payment_value_ca = this.payment_grid_form.controls.payment_value_cc.value;
      sendValues.payment_value =
        parseFloat(this.payment_grid_form.controls.payment_value_iban.value) +
        parseFloat(this.payment_grid_form.controls.payment_value_cc.value);
      if (this.payment_grid_form.valid) {
        this.paymentGridService
          .update(this.selected_payment_grid.id, sendValues)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => {
            this.accessModalVisible = false;
            this.loading = false;
            this.uiService.showMessage(MessageType.success, 'Grelha de pagamento alterada com sucesso');
            this.initTableData(this.paymentGridService);
          });
      }
    }
  }

  validateHours() {
    const hours_pay = this.payment_grid_form.controls.hours_pay.value.toString();
    const hours_pay_split = hours_pay.split('.');
    if (hours_pay_split.length >= 2) {
      const x = hours_pay_split[1];
      if (x > 59) {
        this.payment_grid_form.controls['hours_pay'].setErrors({ maxLength: true });
      }
    }
  }

  confirmApprove(id: number) {
    if (!this.authService.hasPermission('social_scholarship:payment-grid:approve')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.modalService.confirm({
      nzTitle: 'Vai aprovar a grelha de pagamento. Tem a certeza que deseja continuar?',
      nzOnOk: () => this.sendToAproved(id),
    });
  }

  sendToAproved(id: number) {
    this.paymentGridService
      .sendToApproved(id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => {
        this.accessModalVisible = false;
        this.loading = false;
        this.uiService.showMessage(MessageType.success, 'Grelha de pagamento aprovada com sucesso');
        this.initTableData(this.paymentGridService);
      });
  }

  confirmPay(id: number) {
    if (!this.authService.hasPermission('social_scholarship:payment-grid:pay')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.modalService.confirm({
      nzTitle: 'Vai definir a grelha de pagamento como Pago. Tem a certeza que deseja continuar?',
      nzOnOk: () => this.sendToPay(id),
    });
  }

  sendToPay(id: number) {
    this.paymentGridService
      .sendToPay(id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => {
        this.accessModalVisible = false;
        this.loading = false;
        this.uiService.showMessage(MessageType.success, 'Grelha de pagamento definida como Pago com sucesso');
        this.initTableData(this.paymentGridService);
      });
  }

  confirmReopen(id: number) {
    if (!this.authService.hasPermission('social_scholarship:payment-grid:reopen')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.modalService.confirm({
      nzTitle: 'Vai reabir a grelha de pagamento. Tem a certeza que deseja continuar?',
      nzOnOk: () => this.sendToReopen(id),
    });
  }

  sendToReopen(id: number) {
    this.paymentGridService
      .sendToReopen(id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => {
        this.accessModalVisible = false;
        this.loading = false;
        this.uiService.showMessage(MessageType.success, 'Grelha de pagamento reaberta com sucesso');
        this.initTableData(this.paymentGridService);
      });
  }

  printGrid(id: number) {
    this.paymentGridService
      .printGrid(id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => {
        this.accessModalVisible = false;
        this.loading = false;
        this.uiService.showMessage(MessageType.success, 'Report gerado com sucesso');
      });
  }

  convert_hours_minutes(hours_minutes) {
    const valor_string = hours_minutes.toString();
    const valor_split = valor_string.split('.');
    let total_minutes = 0;
    if (valor_split.length >= 2) {
      const hours = parseFloat(valor_split[0]) * 60;
      let minutes = 0;
      if (valor_split[1].length >= 2) {
        minutes = parseFloat(valor_split[1]);
      } else {
        minutes = parseInt(valor_split[1] + '0');
      }
      total_minutes = hours + minutes;
    } else {
      total_minutes = parseFloat(valor_string) * 60;
    }
    return total_minutes;
  }

  format_minutes_to_hours(total_minutes) {
    const strings = total_minutes.toString();
    if (strings.length >= 2) {
      const a = Math.floor(total_minutes / 60) + '.' + (total_minutes % 60);
      return parseFloat(parseFloat(a).toFixed(2));
    } else {
      return parseFloat('0.0' + total_minutes);
    }
  }

  loadCurrentAccounts() {
    this.currentAccountsLoading = true;

    this.currentAccountService
      .list(0, -1)
      .pipe(
        first(),
        finalize(() => (this.currentAccountsLoading = false))
      )
      .subscribe((results) => (this.currentAccounts = results.data));
  }

  listComplete() {
    this.experienceForm.get('status').setValue(null);
    this.filters.status = null;
    this.experienceForm.get('month').setValue(null);
    this.filters.payment_month = null;
    this.experienceForm.get('year').setValue(null);
    this.filters.payment_year = null;
    this.experienceForm.get('experience').setValue(null);
    this.filters.experience_id = null;
    this.experienceForm.get('interest').setValue(null);
    this.filters.user_interest_id = null;

    this.searchData(true);
  }
}
