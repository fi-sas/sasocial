import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementPaymentsComponent } from './pages/management-payments/management-payments/management-payments.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ManagementPaymentsComponent,
    data: { breadcrumb: 'Pagamentos', title: 'Pagamentos', scope: 'social_scholarship:payment-grid:read'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
