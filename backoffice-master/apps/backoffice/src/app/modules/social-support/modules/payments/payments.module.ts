import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { PaymentsRoutingModule } from './payments-routing.module';
import { ManagementPaymentsComponent } from './pages/management-payments/management-payments/management-payments.component';

@NgModule({
  declarations: [ManagementPaymentsComponent],
  imports: [
    CommonModule, SharedModule, PaymentsRoutingModule
  ]
})
export class PaymentsModule { }
