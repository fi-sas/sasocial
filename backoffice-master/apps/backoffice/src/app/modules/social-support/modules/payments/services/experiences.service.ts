import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from "rxjs";

import { ExperienceModel } from '../../experiences/models/experience.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class ExperiencesService extends Repository<ExperienceModel> {

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.EXPERIENCES';
    this.entity_url = 'SOCIAL_SUPPORT.EXPERIENCES_ID';
  }

  listAdvisorExperiences(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.set('query[status]', 'IN_COLABORATION');
    return this.resourceService.list<ExperienceModel>(this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_ADVISOR'), { params });
  }

  listPreviouslyActive(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.append('query[status]', 'IN_COLABORATION');
    params = params.append('query[status]', 'CLOSED');
    return this.resourceService.list<ExperienceModel>(this.urlService.get(this.entities_url), { params });
  }
}
