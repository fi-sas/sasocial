import { TestBed } from '@angular/core/testing';

import { PaymentsGridService } from './payments-grid.service';

describe('PaymentsGridService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentsGridService = TestBed.get(PaymentsGridService);
    expect(service).toBeTruthy();
  });
});
