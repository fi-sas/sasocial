import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { PaymentGridModel } from '../models/payment-grid.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentsGridService extends Repository<PaymentGridModel> {

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.PAYMENT_GRID';
    this.entity_url = 'SOCIAL_SUPPORT.PAYMENT_GRID_ID';
  }

  createPaymentGrid(month, year, user_interest_id) {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_CREATE', {month, year, user_interest_id}), {});
  }

  sendToApproved(id) {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_APPROVED', {id}), {});
  }

  sendToPay(id) {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_PAY', {id}), {});
  }

  sendToReopen(id) {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_REOPEN', {id}), {});
  }

  printGrid(id) {
    return this.resourceService.create<PaymentGridModel>(this.urlService.get('SOCIAL_SUPPORT.PAYMENT_GRID_REPORT', {id}), {});
  }
}
