import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ExperienceUserInterestModel } from '../../experience-user-interests/models/experience-user-interest.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserInterestsService extends Repository<ExperienceUserInterestModel> {

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS';
    this.entity_url = 'SOCIAL_SUPPORT.EXPERIENCE_USER_INTERESTS_ID';
  }

  listByStatusAndExperience(experience_id): Observable<Resource<ExperienceUserInterestModel>> {
    let params = new HttpParams();
    params = params.append('query[status]', 'COLABORATION');
    params = params.append('query[status]', 'CLOSED');
    params = params.append('query[status]', 'WITHDRAWAL');
    params = params.append('query[status]', 'WITHDRAWAL_ACCEPTED');
    if (!isNaN(experience_id)) {
      params = params.set('query[experience_id]', experience_id);
    }
    return this.resourceService.list<ExperienceUserInterestModel>(this.urlService.get(this.entities_url), { params });
  }

}
