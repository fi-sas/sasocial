import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchDecisionCardComponent } from './dispatch-decision-card.component';

describe('DispatchDecisionCardComponent', () => {
  let component: DispatchDecisionCardComponent;
  let fixture: ComponentFixture<DispatchDecisionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchDecisionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchDecisionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
