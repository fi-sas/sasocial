import { PanelStatsComponent } from './pages/panel-stats/panel-stats.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListApplicationsComponent } from './pages/list-applications/list-applications.component';

const routes: Routes = [
  { path: '', redirectTo: 'stats', pathMatch: 'full' },
  {
    path: 'stats',
    component: PanelStatsComponent,
    data: { breadcrumb: 'Painel', title: 'Painel', scope: 'social_scholarship:applications:read' },
    resolve: {}
  },
  {
    path: 'app-all',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'social_scholarship:applications:read', applicationsStatus: '', allApplications: true },
    resolve: {}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
