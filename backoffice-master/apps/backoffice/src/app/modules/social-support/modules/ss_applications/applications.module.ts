import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationsService } from './services/applications.service';
import { InterviewsListComponent } from './components';
import { ListApplicationsComponent } from './pages/list-applications/list-applications.component';
import { PanelStatsComponent } from './pages/panel-stats/panel-stats.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SsSharedModule } from '../ss-shared/ss-shared.module';
import { ViewApplicationComponent } from './pages/view-application/view-application.component';
import { ViewApplicationModalComponent } from './pages/view-application-modal/view-application-modal.component';

@NgModule({
  declarations: [
    InterviewsListComponent,
    ListApplicationsComponent,
    PanelStatsComponent,
    ViewApplicationComponent,
    ViewApplicationModalComponent,
  ],
  imports: [CommonModule, SharedModule, ApplicationsRoutingModule, SsSharedModule],
  exports: [ViewApplicationComponent],
  entryComponents: [InterviewsListComponent, ViewApplicationModalComponent],
  providers: [ApplicationsService],
})
export class ApplicationsModule {}
