import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

import { finalize, first } from 'rxjs/operators';

import { ApplicationInterviewModel } from '../../models/application-interview.model';
import { ApplicationsService } from '../../services/applications.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.less'],
})
export class InterviewsListComponent {
  private _interviews: ApplicationInterviewModel[] = [];
  @Input() set interviews(interviews: ApplicationInterviewModel[]) {
    this._interviews = interviews;
  }
  get interviews() {
    return this._interviews;
  }

  form: FormGroup = new FormGroup({
    file_id: new FormControl(null),
    notes: new FormControl('', [Validators.required, trimValidation]),
  });
  isLoading: { [key: number]: boolean } = {};

  constructor(private applicationsService: ApplicationsService, private uiService: UiService) { }

  submit(valid: boolean, value: any, id: any, index: number) {
    if (valid) {
      this.isLoading[index] = true;
      this.applicationsService
        .saveInterview(id, value)
        .pipe(
          first(),
          finalize(() => (this.isLoading[index] = false))
        )
        .subscribe((result) => {
          result.data.forEach((interview) => {
            const replaceInterviewIndex = this.interviews.findIndex((i) => i.id === interview.id);
            if (replaceInterviewIndex >= 0) {
              this.interviews[replaceInterviewIndex] = interview;
            }
          });
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso.');
        });
    } else {
      for (const i in this.form.controls) {
        if (this.form.controls[i]) {
          this.form.controls[i].markAsDirty();
          this.form.controls[i].updateValueAndValidity();
        }
      }
    }
  }
}
