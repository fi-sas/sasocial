import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { ApplicationModel } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";

export class ApplicationInterviewModel {
    id?: number;
    application_id: number; 
    application?: ApplicationModel;
    local: string;
    date: string;
    responsable_id: number;
    responsable?: UserModel;
    notes: string;
    observations: string;
    scope: string;
    interview_id: number;
    file_id: number;
    file?: FileModel;
    created_at?: string;
    updated_at?: string;
  }
  