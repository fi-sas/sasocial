export class ApplicationStatsModel {
  SUBMITTED: number;
  WAITING: number;
  APPROVED: number;
  INTERVIEWED: number;
  PUBLISHED: number;
  CHANGED: number;
  ACCEPTED: number;
  DISPATCH: number;
  CANCELLED: number;
  CLOSED: number;
  ACKNOWLEDGED: number;
  ANALYSED: number;
  DECLINED: number;
  WITHDRAWAL: number;
  EXPIRED: number;
}
