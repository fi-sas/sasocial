import { ApplicationHistoryModel } from '@fi-sas/backoffice/modules/social-support/models/application-history.model';
import { PaymentGridModel } from '@fi-sas/backoffice/modules/social-support/models/payment-grid.model';
import { ExperienceModel } from '@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model';
import { ActivityModel } from '@fi-sas/backoffice/modules/social-support/modules/activities/models/activity.model';
import { MonthlyReportsModel } from '@fi-sas/backoffice/modules/social-support/models/monthly-reports.model';
import { AttendancesModel } from '@fi-sas/backoffice/modules/social-support/models/attendances.model';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { SchoolModel } from '@fi-sas/backoffice/modules/configurations/models/school.model';
import { ApplicationInterviewModel } from './application-interview.model';
import { ApplicationHistoricModel } from './application-historic.model';
import { CollaborationHistoricModel } from './collaboration-historic.model';
import { Gender } from '@fi-sas/backoffice/shared/models/gender';

export enum ApplicationStatus {
  ACCEPTED = 'ACCEPTED',
  ANALYSED = 'ANALYSED',
  CANCELLED = 'CANCELLED',
  DECLINED = 'DECLINED',
  DISPATCH = 'DISPATCH',
  EXPIRED = 'EXPIRED',
  INTERVIEWED = 'INTERVIEWED',
  SUBMITTED = 'SUBMITTED',
}

export enum ApplicationStatusEvent {
  ACCEPT = 'ACCEPT',
  ANALYSE = 'ANALYSE',
  CANCEL = 'CANCEL',
  DECLINE = 'DECLINE',
  DISPATCH = 'DISPATCH',
  EXPIRE = 'EXPIRE',
  INTERVIEW = 'INTERVIEW',
}

export enum ApplicationDispatchDecision {
  ACCEPTED = 'ACCEPTED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}

export const ApplicationTagResult = {
  ACCEPTED: { color: '#9DBBAE', label: 'Aprovado' },
  ANALYSED: { color: '#7768AE', label: 'Em Análise' },
  CANCELLED: { color: '#A4A4A4', label: 'Cancelado' },
  DECLINED: { color: '#D0021B', label: 'Rejeitado' },
  DISPATCH: { color: 'orange', label: 'Despacho' },
  EXPIRED: { color: '#ff8000', label: 'Expirado' },
  INTERVIEWED: { color: '#88ccf1', label: 'Entrevista' },
  SUBMITTED: { color: '#4D9DE0', label: 'Submetido' },
  WAITING: { color: '#f8ca00', label: 'Lista de espera' },
};

export class ApplicationModel {
  id?: number;
  academic_year: string;
  mentor_or_mentee: string;
  course_year: string;
  experience_id: number;
  name: string;
  student_number: string;
  course_id: number;
  course?: CourseModel;
  birth_date: string;
  nationality: string;
  tin: string;
  identification_number: string;
  iban: string;
  address: string;
  postal_code: string;
  city: string;
  email: string;
  phone_number: string;
  has_scholarship: boolean;
  has_applied_scholarship: boolean;
  scholarship_monthly_value: number;
  has_emergency_fund: boolean;
  has_profissional_experience: boolean;
  type_of_company: string;
  job_at_company: string;
  job_description: string;
  organic_unit_id: number;
  school?: SchoolModel;
  organic_units?: number[] | OrganicUnitsModel[];
  preferred_activity_id: number;
  has_collaborated_last_year: boolean;
  previous_activity_description: string;
  preferred_schedule: PreferredScheduleModel[];
  foreign_languages: boolean;
  which_languages: string;
  cv_file_id: number;
  cv_file?: FileModel;
  has_holydays_availability: boolean;
  observations: string;
  user_id?: number;
  user?: UserModel;
  country: string;
  language_skills: string[];
  status?: string;
  updated_at?: string;
  created_at?: string;
  payment_grids?: PaymentGridModel[];
  experience?: ExperienceModel;
  history?: ApplicationHistoryModel[];
  preferred_activity?: ActivityModel;
  preferred_activities?: ActivityModel[];
  monthly_reports?: MonthlyReportsModel[];
  attendance?: AttendancesModel[];
  interviews?: ApplicationInterviewModel[];
  student_name?: string;
  student_code_postal?: string;
  student_location?: string;
  student_birthdate?: string;
  student_nationality?: string;
  student_tin?: number;
  student_identification?: number;
  student_email?: string;
  student_mobile_phone?: number;
  student_phone?: number;
  student_address?: string;
  last_status?: string;
  is_waiting_scholarship_response: boolean;
  historic_applications?: ApplicationHistoricModel[];
  historic_colaborations?: CollaborationHistoricModel[];
  genre: Gender;
  housed_residence: boolean;
  other_scholarship: boolean;
  other_scholarship_values?: number;
  decision?: string;
  files?: { name: string; file: FileModel }[];
  files_ids?: number[];
}

export class PreferredScheduleModel {
  id: never;
  day_week: string;
  day_period: string;
  schedule_id: number;
}

export class ApplicationData {
  static mentor_or_mentee = [
    { value: 'MENTOR', label: 'Mentor', active: true },
    { value: 'MENTEE', label: 'Mentorado', active: true },
  ];

  static status = [
    {
      value: ApplicationStatus.ACCEPTED,
      label: 'Aprovado',
      active: true,
      color: ApplicationTagResult.ACCEPTED.color,
      order: 4,
    },
    {
      value: ApplicationStatus.ANALYSED,
      label: 'Em Análise',
      active: true,
      color: ApplicationTagResult.ANALYSED.color,
      order: 2,
    },
    {
      value: ApplicationStatus.CANCELLED,
      label: 'Cancelado',
      active: true,
      color: ApplicationTagResult.CANCELLED.color,
      order: -1,
    },
    {
      value: ApplicationStatus.DECLINED,
      label: 'Rejeitado',
      active: true,
      color: ApplicationTagResult.DECLINED.color,
      order: -1,
    },
    {
      value: ApplicationStatus.DISPATCH,
      label: 'Despacho',
      active: true,
      color: ApplicationTagResult.DISPATCH.color,
      order: -1,
    },
    {
      value: ApplicationStatus.EXPIRED,
      label: 'Expirado',
      active: true,
      color: ApplicationTagResult.EXPIRED.color,
      order: -1,
    },
    {
      value: ApplicationStatus.INTERVIEWED,
      label: 'Entrevista',
      active: true,
      color: ApplicationTagResult.INTERVIEWED.color,
      order: 3,
    },
    {
      value: ApplicationStatus.SUBMITTED,
      label: 'Submetido',
      active: true,
      color: ApplicationTagResult.SUBMITTED.color,
      order: 0,
    },
  ];

  static statusMachine: Record<ApplicationStatus, Partial<Record<ApplicationStatusEvent, { label: string; color: string; }>>> = {
    SUBMITTED: {
      ANALYSE: { label: 'Analisar', color: ApplicationTagResult.ANALYSED.color },
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
    },
    ANALYSED: {
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
      DISPATCH: { label: 'Despacho', color: ApplicationTagResult.DISPATCH.color },
      INTERVIEW: { label: 'Entrevista', color: ApplicationTagResult.INTERVIEWED.color },
    },
    INTERVIEWED: {
      DISPATCH: { label: 'Despacho', color: ApplicationTagResult.DISPATCH.color },
    },
    DISPATCH: {
      ACCEPT: { label: 'Aceitar', color: ApplicationTagResult.ACCEPTED.color },
      DECLINE: { label: 'Rejeitar', color: ApplicationTagResult.DECLINED.color },
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
    },
    DECLINED: {
      ANALYSE: { label: 'Analisar', color: ApplicationTagResult.ANALYSED.color },
    },
    ACCEPTED: {
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
      EXPIRE: { label: 'Expirar', color: ApplicationTagResult.EXPIRED.color },
    },
    EXPIRED: {},
    CANCELLED: {},
  };

  static eventToDispatchDecisionMapper: Partial<Record<ApplicationStatusEvent, ApplicationDispatchDecision>> = {
    ACCEPT: ApplicationDispatchDecision.ACCEPTED,
    CANCEL: ApplicationDispatchDecision.CANCELLED,
    DECLINE: ApplicationDispatchDecision.REJECTED,
  }

  static decisionToStateMapper: Record<ApplicationDispatchDecision, ApplicationStatus> = {
    ACCEPTED: ApplicationStatus.ACCEPTED,
    CANCELLED: ApplicationStatus.CANCELLED,
    REJECTED: ApplicationStatus.DECLINED,
  }
}
