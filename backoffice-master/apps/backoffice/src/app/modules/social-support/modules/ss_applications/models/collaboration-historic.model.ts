import { TranslationModel } from "@fi-sas/backoffice/modules/social-support/modules/experiences/models/experience.model";

export class CollaborationHistoricModel {
    academic_year: string;
    experience_translations: TranslationModel[];
    user_manifest_id: number;
}