import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { first, finalize } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { ApplicationsService } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/applications.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { CoursesService } from '@fi-sas/backoffice/modules/configurations/services/courses.service';
import { ExperiencesService } from '../../../experiences/services/experiences.service';
import { InterviewsListComponent } from '../../components';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import {
  ApplicationData,
  ApplicationModel,
  ApplicationStatus,
  ApplicationTagResult,
} from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { DispatchModalComponent } from '../../../ss-shared/components';
import { SendDispatchData } from '@fi-sas/backoffice/modules/social-support/models';
import { ExperienceUserInterestStatus } from '../../../experience-user-interests/models/experience-user-interest.model';

@Component({
  selector: 'fi-sas-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less'],
})
export class ListApplicationsComponent extends TableHelper implements OnInit {
  ApplicationTagResult = ApplicationTagResult;
  ApplicationStatus = ApplicationStatus;

  status = ApplicationData.status;
  statusMachine = ApplicationData.statusMachine;
  allApplications = false;
  currentAcademicYear: AcademicYearModel[];

  //FILTERS
  academic_years: AcademicYearModel[] = [];
  load_academic_years = false;
  courses: CourseModel[] = [];
  load_courses = false;
  schools: OrganicUnitsModel[] = [];
  load_schools = false;
  applicationStatuses = [];
  yes_no_options = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalApplication: ApplicationModel = null;
  changeStatusModalActions = {};
  changeStatusModalApplicationAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;

  //MODAL INTERVIEW
  isInterviewModalVisible = false;
  interviewModalApplication: ApplicationModel = new ApplicationModel();
  interviewModalLoading = false;

  interviewForm = new FormGroup({
    date: new FormControl(new Date(), [Validators.required]),
    local: new FormControl('', [Validators.required, trimValidation]),
    scope: new FormControl('', [Validators.required, trimValidation]),
    responsable_id: new FormControl(null, [Validators.required]),
  });


  ExperienceUserInterestStatus = ExperienceUserInterestStatus;
  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });

  constructor(
    activatedRoute: ActivatedRoute,
    router: Router,
    uiService: UiService,
    public applicationService: ApplicationsService,
    public experiencesService: ExperiencesService,
    private academicYearsService: AcademicYearsService,
    private authService: AuthService,
    private coursesService: CoursesService,
    private modalService: NzModalService,
    private organicUnitsService: OrganicUnitsService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadSchools();
    this.loadAcademicYears();
    this.loadStatuses();
    this.loadYesNoOptions();
    this.persistentFilters['withRelated'] = 'course,interviews';

    let status: string = this.activatedRoute.snapshot.queryParamMap.get('status');
    if (status) {
      this.persistentFilters['status'] = status;
    }
    let academicYear: string = this.activatedRoute.snapshot.queryParamMap.get('academicYear');
    if (academicYear) {
      this.filters.academic_year = academicYear;
    } else {
      this.loadCurrentAcademicYear();
    }

    this.columns.push(
      {
        key: 'student_number',
        label: 'Número',
        sortable: true,
      },
      {
        key: 'student_name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'student_email',
        label: 'Email',
        sortable: true,
      },
      {
        key: 'course.name',
        label: 'Curso',
        sortable: false,
      },
      {
        key: 'academic_year',
        label: 'Ano Académico',
        sortable: true,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ApplicationTagResult,
        filters: this.applicationStatuses.map((applicationStatus) => {
          return {
            text: applicationStatus.label,
            value: applicationStatus.value,
          };
        }),
        showFilter: true,
        filterMultiple: false,
      }
    );
    this.academicYearsService
      .getCurrentAcademicYear()
      .pipe(
        first(),
        finalize(() => (this.load_courses = false))
      )
      .subscribe((result) => {
        this.currentAcademicYear = result.data;
      });

    this.initTableData(this.applicationService);
  }

  loadYesNoOptions() {
    this.yes_no_options = [
      {
        label: 'Sim',
        value: true,
      },
      {
        label: 'Não',
        value: false,
      },
    ];
  }

  loadCurrentAcademicYear() {
    this.academicYearsService
      .getCurrentAcademicYear()
      .pipe(first())
      .subscribe((result) => {
        this.currentAcademicYear = result.data;
        if (this.currentAcademicYear.length !== 0) {
          this.filters.academic_year = this.currentAcademicYear[0].academic_year;
          this.searchData(true);
        }
      });
  }

  loadStatuses() {
    this.applicationStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  loadCourses(schoolId) {
    if (schoolId === null) {
      this.courses = null;

      return false;
    }

    this.load_courses = true;
    this.coursesService
      .list(1, -1, 'name', 'ascend', { organic_unit_id: schoolId })
      .pipe(
        first(),
        finalize(() => (this.load_courses = false))
      )
      .subscribe((result) => {
        this.courses = result.data;
      });
  }

  loadSchools() {
    this.load_schools = true;

    this.organicUnitsService
      .list(1, -1, 'name', 'ascend', { withRelated: false, is_school: true, active: true })
      .pipe(
        first(),
        finalize(() => (this.load_schools = false))
      )
      .subscribe((results) => {
        this.schools = results.data;
      });
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(1, -1, 'start_date', 'descend')
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  deleteApplication(id: number) {
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar esta candidatura?', 'Eliminar')
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.applicationService
            .delete(id)
            .pipe(first())
            .subscribe((result) => {
              this.uiService.showMessage(MessageType.success, 'Candidatura eliminada com sucesso');
              this.searchData();
            });
        }
      });
  }

  changeStateModal(application: ApplicationModel, disabled: boolean) {
    if (!this.authService.hasPermission('social_scholarship:applications:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    if(!Object.keys(ApplicationData.statusMachine[application.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalApplication = application;

      this.changeStatusModalActions = ApplicationData.statusMachine[application.status];
    }
  }

  handleChangeStatusOk() {
    this.changeStatusModalLoading = true;
    if(this.changeStatusModalApplication.status !== 'DISPATCH' && this.changeStatusModalApplicationAction.toUpperCase() !== 'DISPATCH' && this.changeStatusModalApplicationAction.toUpperCase() !== 'INTERVIEW'){
      let statusObj: any = {
        event: this.changeStatusModalApplicationAction.toUpperCase(),
        notes: this.changeStatusModalNotes,
      };
      this.applicationService
        .status(this.changeStatusModalApplication.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe((result) => {
          if (this.allApplications) {
            this.changeStatusModalApplication.status = result.data[0].status;
            const appIndex = this.data.findIndex((a) => a.id === this.changeStatusModalApplication.id);
            if (appIndex !== undefined) {
              this.data[appIndex].status = result.data[0].status;
              this.data = [...this.data];
            }
          } else {
            this.searchData();
          }
          this.handleChangeStatusCancel();
        });
    }else if(this.changeStatusModalApplicationAction.toUpperCase() === 'INTERVIEW' ){
      if (this.interviewForm.valid) {
        this.interviewForm.get('observations').setValue(this.changeStatusModalNotes);
        this.applicationService
        .scheduleInterview(this.changeStatusModalApplication.id, this.interviewForm.value)
        .pipe(
          first(),
          finalize(() => {
            this.interviewModalLoading = false;
            this.changeStatusModalLoading = false;
          })
        )
        .subscribe((result) => {
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso');
          this.handleChangeStatusCancel();
          this.searchData();
        });
      }else{
        this.updateAndValidityForm(this.interviewForm);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalApplicationAction.toUpperCase() === 'DISPATCH'){
      if(this.form.valid){
        this.applicationService.sendDispatch(this.changeStatusModalApplication.id, {
          event: this.changeStatusModalApplicationAction.toUpperCase(),
          decision: this.form.get('decision').value,
          notes: this.changeStatusModalNotes,
        })
          .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
          .subscribe(() => {
            this.handleChangeStatusCancel();
            this.searchData();
          });
      }else{
        this.updateAndValidityForm(this.form);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalApplication.status === 'DISPATCH'){
      this.applicationService
      .sendDispatch(this.changeStatusModalApplication.id, { decision_dispatch: this.changeStatusModalApplicationAction.toUpperCase() })
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe(() => {
        this.handleChangeStatusCancel();
        this.searchData();
      });
    }else{
      this.changeStatusModalLoading = false;
    }
  }
    /*if(this.changeStatusModalApplication.status !== 'DISPATCH'){
      let statusObj: any = {
        event: this.changeStatusModalApplicationAction.toUpperCase(),
        notes: this.changeStatusModalNotes,
      };
  
      this.changeStatusModalLoading = true;
      this.applicationService
        .status(this.changeStatusModalApplication.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe((result) => {
          if (this.allApplications) {
            this.changeStatusModalApplication.status = result.data[0].status;
            const appIndex = this.data.findIndex((a) => a.id === this.changeStatusModalApplication.id);
            if (appIndex !== undefined) {
              this.data[appIndex].status = result.data[0].status;
              this.data = [...this.data];
            }
          } else {
            this.searchData();
          }
  
          this.handleChangeStatusCancel();
        });
    }else{
      let sendData = {
        decision_dispatch: this.changeStatusModalApplicationAction.toUpperCase(),
        notes: this.changeStatusModalNotes
      }
      this.applicationService.sendDispatch(this.changeStatusModalApplication.id, sendData)
      .pipe(first(),
      finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${this.changeStatusModalApplicationAction.toUpperCase() === 'ACCEPT' ? 'approvada' : 'rejeitada'}`);
        this.isChangeStatusModalVisible = false;
        this.changeStatusModalNotes = '';
        this.searchData();
      });
    }*/
 

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalApplication = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalApplicationAction = null;
    this.changeStatusModalNotes = '';
    this.form.reset();
    this.interviewForm.reset();
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalApplicationAction = action;
    this.form.reset();
    this.interviewForm.reset();
  }

  interviewModal(application: ApplicationModel, enabled: boolean) {
    if (enabled) {
      this.isInterviewModalVisible = true;
      this.interviewModalApplication = application;
    }
  }

  handleInterviewConfirm(event: any, value: any) {
    this.interviewModalLoading = true;
    if (this.interviewForm.valid) {
      this.applicationService
        .scheduleInterview(this.interviewModalApplication.id, value)
        .pipe(
          first(),
          finalize(() => (this.interviewModalLoading = false))
        )
        .subscribe((result) => {
          this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso.');

          this.searchData();
          this.handleInterviewCancel();
        });
    } else {
      for (const i in this.interviewForm.controls) {
        if (this.interviewForm.controls[i]) {
          this.interviewForm.controls[i].markAsDirty();
          this.interviewForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  handleInterviewCancel() {
    this.isInterviewModalVisible = false;
    this.interviewModalApplication = null;
    this.interviewForm.reset();
  }

  restoreApplication(application: ApplicationModel) {
    if (!this.authService.hasPermission('social_scholarship:applications:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.uiService
      .showConfirm(
        'Recuperar candidatura',
        `Pretende recuperar a candidatura do estudante ${application.student_name}?`,
        'Recuperar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.applicationService
            .changeToLastStatus(application.id)
            .pipe(
              first(),
              finalize(() => (this.interviewModalLoading = false))
            )
            .subscribe((result) => {
              this.uiService.showMessage(MessageType.success, 'Candidatura recuperada.');

              this.searchData();
            });
        }
      });
  }

  getApplicationReport(id: number) {
    this.loading = true;
    this.applicationService
      .getApplicationReport(id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.uiService.showMessage(MessageType.success, 'Pedido de relatório enviado.');
      });
  }

  listComplete() {
    this.filters.created_at = null;
    this.filters.academic_year = null;
    this.filters.school_id = null;
    this.filters.course_id = null;
    this.filters.student_number = null;
    this.filters.user_id = null;
    this.filters.has_scholarship = null;
    this.filters.has_profissional_experience = null;
    this.filters.has_emergency_fund = null;
    this.searchData(true);
  }

  registerInterview(data: ApplicationModel) {
    this.modalService.create({
      nzTitle: 'Registar entrevistas',
      nzContent: InterviewsListComponent,
      nzComponentParams: { interviews: data.interviews },
      nzFooter: null,
    });
  }

  dispatchModal(application: ApplicationModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.applicationService.sendDispatch(application.id, data),
        actions: Object.keys(ApplicationData.statusMachine.DISPATCH).map<{ key: ApplicationStatus; label: string }>(
          (key) => ({
            key: ApplicationData.eventToDispatchDecisionMapper[key],
            label: ApplicationData.statusMachine.DISPATCH[key].label,
          })
        ),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.onSubmit().then(() => {
              this.searchData();
              modalRef.close();
            });
          },
        },
      ],
    });
  }

  dispatchChangeStatues(application: ApplicationModel, acceptDecision: boolean) {
    this.applicationService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }

  getDispatchActions = () => {
    return Object.keys(ApplicationData.statusMachine.DISPATCH).map<{ key: ApplicationStatus; label: string }>(
      (key) => ({
        key: ApplicationData.eventToDispatchDecisionMapper[key],
        label: ApplicationData.statusMachine.DISPATCH[key].label,
      })
    );
  }
}
