import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelStatsComponent } from './panel-stats.component';

describe('PanelStatsComponent', () => {
  let component: PanelStatsComponent;
  let fixture: ComponentFixture<PanelStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PanelStatsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
