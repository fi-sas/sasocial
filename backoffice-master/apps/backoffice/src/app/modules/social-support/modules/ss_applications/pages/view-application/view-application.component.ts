import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { first, finalize } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';

import {
  ApplicationData,
  ApplicationModel,
} from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { ApplicationsService } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/applications.service';
import { ApplicationTagResult } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { StatusInfoModel } from '@fi-sas/backoffice/modules/social-support/models/status-info.model';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ViewApplicationModalComponent } from './../view-application-modal/view-application-modal.component';

@Component({
  selector: 'fi-sas-view-application',
  templateUrl: './view-application.component.html',
  styleUrls: ['./view-application.component.less'],
})
export class ViewApplicationComponent implements OnInit {
  private _data: ApplicationModel = null;
  @Input() set data(data: ApplicationModel) {
    this._data = data;
    if (data && data.decision) {
      const dispatchState = this.decisionToStateMapper[data.decision];
      const state = this.status.find((s) => s.value === dispatchState);
      if (state) {
        this.dispatchDecision = state.label
      }
    }
  }
  get data() {
    return this._data;
  }

  @Output() itemChange = new EventEmitter<boolean>();

  ApplicationTagResult = ApplicationTagResult;
  loading = false;

  loadingCertificate = false;
  application: ApplicationModel = null;
  interviewLoading = false;
  interviewFormLoading = false;
  status = ApplicationData.status;
  statusMachine = ApplicationData.statusMachine;
  decisionToStateMapper = ApplicationData.decisionToStateMapper;
  modalsRefs: { name: string; modal: NzModalRef }[] = [];

  statusInfo: StatusInfoModel;

  interviewForm = new FormGroup({
    file_id: new FormControl(null, []),
    notes: new FormControl('', [Validators.required, trimValidation]),
  });

  dispatchDecision: string;

  constructor(
    private authService: AuthService,
    private modalService: NzModalService,
    private uiService: UiService,
    public applicationService: ApplicationsService
  ) {}

  createTplModal(
    tplTitle: TemplateRef<{}>,
    tplContent: TemplateRef<{}>,
    tplFooter: TemplateRef<{}>,
    size: number = 900,
    padding: string = '25px'
  ): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size,
      nzBodyStyle: { padding: padding },
    });
  }

  ngOnInit() {
    this.loading = true;
    this.applicationService
      .read(this.data.id, {
        id: this.data.id,
        withRelated:
          'history,preferred_activities,user,course,course_degree,school,cv_file,organic_units,preferred_schedule,historic_colaborations,interviews,historic_applications,files',
      })
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => (this.application = result.data[0]));
  }

  generateModal(
    name: string,
    modalTitle: TemplateRef<{}>,
    modalContent: TemplateRef<{}>,
    modalFooter: TemplateRef<{}>,
    modalSize: number,
    padding?: string
  ) {
    this.modalsRefs.push({
      name: name,
      modal: this.createTplModal(modalTitle, modalContent, modalFooter, modalSize, padding),
    });
  }

  filterStatus() {
    return this.status.filter((s) => s.order >= 0);
  }

  filterSchedule(scheduleID: number) {
    let found = this.application.preferred_schedule.find((schedule) => schedule.schedule_id === scheduleID);

    return found !== undefined;
  }
  submit(valid: boolean, value: any, id: any) {
    if (!this.authService.hasPermission('social_scholarship:applications:status')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }

    this.interviewFormLoading = true;
    this.applicationService
      .saveInterview(id, value)
      .pipe(
        first(),
        finalize(() => (this.interviewFormLoading = false))
      )
      .subscribe((result) => {
        let interviews = this.application.interviews.map((interview) => {
          if (interview.id === result.data[0].id) {
            return result.data[0];
          }

          return interview;
        });

        this.application.interviews = interviews;

        this.uiService.showMessage(MessageType.success, 'Entrevista inserida com sucesso');
      });
  }

  viewApplicationModal(id: number) {
    this.applicationService
      .read(id, {
        id: id,
        withRelated:
          'history,experience,preferred_activities,attendance,monthly_reports,payment_grids,user,course,course_degree,school,cv_file,organic_units,preferred_schedule,historic_colaborations,interviews,historic_applications',
      })
      .pipe(
        first(),
        finalize(() => true)
      )
      .subscribe((result) => {
        this.application = result.data[0];
        this.modalService.create({
          nzWidth: 1024,
          nzMaskClosable: false,
          nzClosable: true,
          nzContent: ViewApplicationModalComponent,
          nzFooter: null,
          nzComponentParams: {
            application: this.application,
          },
        });
      });
  }

  getExperienceCertificate(id) {
    this.loadingCertificate = true;
    this.applicationService
      .generateCertificate(id)
      .pipe(
        first(),
        finalize(() => (this.loadingCertificate = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Certificado gerado com sucesso');
      });
  }

  dispatchChangeStatues(application: ApplicationModel, acceptDecision: boolean) {
    this.applicationService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.itemChange.emit(true);
      });
  }
}
