import { ApplicationStatsModel } from './../models/application-stats';
import { ApplicationInterviewModel } from './../models/application-interview.model';
import { UserInterestStatsModel } from './../../experiences/models/user-interest-stats.model';
import { ExperienceStatsModel } from './../../experiences/models/experience-stats.model';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ApplicationModel } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/models/application.model';
import { ApproveRejectModel } from '@fi-sas/backoffice/modules/social-support/models/approveReject';
import { CertificateModel } from '@fi-sas/backoffice/modules/social-support/models/certificate.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { SendDispatchData } from '../../../models';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsService extends Repository<ApplicationModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.APPLICATIONS';
    this.entity_url = 'SOCIAL_SUPPORT.APPLICATIONS_ID';
  }

  status(
    id: number,
    application_status: ApproveRejectModel
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_STATUS', { id }),
      application_status
    );
  }

  certificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_CERTIFICATE', { id }),
      {}
    );
  }

  stats(academic_year: string): Observable<Resource<ApplicationStatsModel>> {
    let params = new HttpParams();
    params = params.append('academic_year', academic_year);
    return this.resourceService.read<ApplicationStatsModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_STATS', {}),
      {params }
    );
  }

  generateCertificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(
      this.urlService.get('SOCIAL_SUPPORT.GENERATE_COLLABORATION_CERTIFICATE', { id }),
      {}
    );
  }

  scheduleInterview(
    id: number,
    interview: ApplicationInterviewModel
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATION_SCHEDULE_INTERVIEW', { id }),
      interview
    );
  }

  saveInterview(
    id: number,
    interview: ApplicationInterviewModel
  ): Observable<Resource<ApplicationInterviewModel>> {
    return this.resourceService.create<ApplicationInterviewModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_SAVE_INTERVIEW', { id }),
      interview
    );
  }

  changeToLastStatus(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_LAST_STATUS', { id }),
      {}
    );
  }

  getApplicationReport(id: number): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_REPORT', { id }),
      {}
    );
  }

  experienceStats(academic_year): Observable<Resource<ExperienceStatsModel>> {
    let params = new HttpParams();

      params = params.append('academic_year', academic_year);

    return this.resourceService.read<ExperienceStatsModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_STATS', {  }),
      {params }
    );
  }

  experienceUserInterest(academic_year): Observable<Resource<UserInterestStatsModel>> {
    let params = new HttpParams();

    params = params.append('academic_year', academic_year);
    return this.resourceService.read<UserInterestStatsModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCE_USER_INTEREST_STATS', {}),
      { params}
    );
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_STATUS', { id }),
      data
    );
  }
}
