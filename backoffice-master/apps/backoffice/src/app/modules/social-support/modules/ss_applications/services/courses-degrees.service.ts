import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ContactModel } from '@fi-sas/backoffice/modules/configurations/models/contact.model';
import { CourseDegreeModel } from '@fi-sas/backoffice/modules/configurations/models/course-degree.model';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesDegreesServices {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) {}



  // COURSE_DEGREES: '@api_configuration:/course-degrees',
  // COURSES: '@api_configuration:/courses',
  // CONTACTS: '@api_configuration:/contacts'
  //              - courseDegree
  courses(course_degree_id?: number, organic_unit_id?: number, withRelated?: string): Observable<Resource<CourseModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    if (course_degree_id) {
      params = params.set('query[course_degree_id]', course_degree_id.toString());
    }
    params = organic_unit_id ? params.set('query[organic_unit_id]', organic_unit_id.toString()) : params;
    return this.resourceService.list<CourseModel>(this.urlService.get('CONFIGURATIONS.COURSES'), { params });
  }

  //  withRelated - courses
  courseDegrees(withRelated?: string, active?: string): Observable<Resource<CourseDegreeModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    if(active){
      params = params.set('active', active);
    }
    params = params.set('sort', 'name');
    

    return this.resourceService.list<CourseDegreeModel>(this.urlService.get('CONFIGURATIONS.COURSE_DEGREES'), { params });
  }

  contacts(): Observable<Resource<ContactModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');

    return this.resourceService.list<ContactModel>(this.urlService.get('CONFIGURATIONS.CONTACT'), { params });
  }

  cursesArray(course_degree_id?: number[], organic_unit_id?: number[], withRelated?: string): Observable<Resource<CourseModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    if (course_degree_id.length>0) {
      course_degree_id.forEach((degree)=> {
        params = params.append('query[course_degree_id]', degree.toString());
      })
     
    }
    if (organic_unit_id.length>0) {
      organic_unit_id.forEach((organic)=> {
        params = params.append('query[organic_unit_id]', organic.toString());
      })
     
    }
    return this.resourceService.list<CourseModel>(this.urlService.get('CONFIGURATIONS.COURSES'), { params });
  }
}
