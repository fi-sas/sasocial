import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';

const routes: Routes = [
  { path: '', redirectTo: 'form', pathMatch: 'full' },
  {
    path: 'form',
    component: FormConfigurationsComponent,
    data: { breadcrumb: 'Configurações', title: 'Configurações', scope: 'social_scholarship:configurations:create' },  
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
