export class ConfigurationModel {
  created_at?: string;
  id?: number;
  key: string;
  updated_at?: string;
  value?: string;
}

