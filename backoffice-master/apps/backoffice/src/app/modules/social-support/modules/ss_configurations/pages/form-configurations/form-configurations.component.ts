import { Component, OnDestroy, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { first, finalize, map, takeUntil, switchMap, debounceTime } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject} from 'rxjs';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AccountModel } from '@fi-sas/backoffice/modules/financial/models/account.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationsService } from '../../services/configurations.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { GeralSettingsModel } from '@fi-sas/backoffice/modules/social-support/models/geral-settings.model';

@Component({
  selector: 'fi-sas-form-configurations',
  templateUrl: './form-configurations.component.html',
  styleUrls: ['./form-configurations.component.less'],
})
export class FormConfigurationsComponent implements OnInit, OnDestroy {
  loading = false;
  configurationLoading: boolean;
  submit = false;
  //FILTERS
  academic_years: AcademicYearModel[] = [];
  load_academic_years = false;

  configurationForm = new FormGroup({
    financial_notification_email: new FormControl('', [Validators.required, Validators.email]),
    max_application_monthly_hours: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(309)]),
    has_current_account: new FormControl(false, [Validators.required]),
    current_account_id: new FormControl({ value: null, disabled: true }),
    profile_id: new FormControl(null, [Validators.required]),
    responsable_profile: new FormControl(null),
    assigned_status_auto_change: new FormControl(false, Validators.required),
    status_auto_change_days: new FormControl(null),
    event_for_auto_change: new FormControl({ value: null, disabled: true }),
    application_notifications: new FormControl([]),
    experience_notifications: new FormControl([]),
    interests_notifications: new FormControl([]),
    accepted_application_status_auto_change: new FormControl(false, Validators.required),
  });

  closeApplications = new FormGroup({
    close_academic_year: new FormControl(null, [Validators.required]),
  });



  //profiles$: Observable<ProfileModel[]> = this.getProfiles();
  //configsIds: { [key: string]: number } = {};
  usersLoading = false;
  users: UserModel[] = [];
  profiles: ProfileModel[] = [];
  currentAccounts: AccountModel[] = [];
  usersSearchChange$ = new BehaviorSubject('');

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private academicYearsService: AcademicYearsService,
    private authService: AuthService,
    private currentAccountService: CurrentAccountsService,
    private profilesService: ProfilesService,
    private uiService: UiService,
    public configurationsService: ConfigurationsService,
    private usersService: UsersService
  ) {
    this.configurationForm
    .get('assigned_status_auto_change')
    .valueChanges.pipe(takeUntil(this.destroy$))
    .subscribe((value: boolean) => {
      const formControl = this.configurationForm.get('status_auto_change_days');
      if (value === true) {
        formControl.setValidators(Validators.required);
        formControl.enable();
      } else {
        formControl.setValidators(null);
        formControl.disable();
      }
      formControl.updateValueAndValidity();
    });

    this.configurationForm.get('has_current_account').valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value: boolean) => {
      const formControl = this.configurationForm.get('current_account_id');
      if (value === true) {
        formControl.setValidators(Validators.required);
        formControl.enable();
      } else {
        formControl.setValidators(null);
        formControl.disable();
      }
      formControl.updateValueAndValidity();
    });
  }

  ngOnInit() {
    this.getProfiles();
    this.loadCurrentAccounts();
    this.loadAcademicYears();
    this.getConfigs();
    this.getUsers();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  getConfigs(){
    this.configurationsService.list(0, 0).pipe(first(), finalize(() => this.loading = false )).subscribe(response => {
      this.configurationForm.get('financial_notification_email').setValue(this.convertData(response.data[0].FINANCIAL_NOTIFICATION_EMAIL, null));
      this.configurationForm.get('max_application_monthly_hours').setValue(response.data[0].MAX_HOURS_WORK);
      this.configurationForm.get('responsable_profile').setValue(this.convertData(response.data[0].RESPONSABLE_PROFILE, []));
      this.configurationForm.get('profile_id').setValue(response.data[0].EXTERNAL_ENTITY_PROFILE_ID ? Number(response.data[0].EXTERNAL_ENTITY_PROFILE_ID): null);
      this.configurationForm.get('assigned_status_auto_change').setValue(this.convertData(response.data[0].ASSIGNED_STATUS_AUTO_CHANGE, null));
      this.configurationForm.get('status_auto_change_days').setValue(this.convertData(response.data[0].STATUS_AUTO_CHANGE_DAYS, null));
      this.configurationForm.get('accepted_application_status_auto_change').setValue(this.convertData(response.data[0].ACCEPTED_APPLICATION_STATUS_AUTO_CHANGE, null));

      if(this.convertData(response.data[0].HAS_CURRENT_ACCOUNT, false)){
        this.configurationForm.get('has_current_account').setValue(this.convertData(response.data[0].HAS_CURRENT_ACCOUNT, null));
        this.configurationForm.get('current_account_id').enable();

      }
      if(this.convertData(response.data[0].CURRENT_ACCOUNT, false)){
        this.configurationForm.get('current_account_id').setValue(response.data[0].CURRENT_ACCOUNT ? Number(response.data[0].CURRENT_ACCOUNT): null);
      }

      if(response.data[0].EVENT_FOR_AUTO_CHANGE){
        this.configurationForm.get('event_for_auto_change').setValue(response.data[0].EVENT_FOR_AUTO_CHANGE);
        this.configurationForm.get('event_for_auto_change').disable();
      }

      if(response.data[0].APPLICATION_EMAILS){
        let usersIds =  this.convertData(response.data[0].APPLICATION_EMAILS, []);
        usersIds = Array.isArray(usersIds) ? usersIds: [];
        this.getLoadUpdateUsers(usersIds);
        this.configurationForm.get('application_notifications').setValue(usersIds);
      }

      if(response.data[0].EXPERIENCE_EMAILS){
        let usersIds =  this.convertData(response.data[0].EXPERIENCE_EMAILS, []);
        usersIds = Array.isArray(usersIds) ? usersIds: [];
        this.getLoadUpdateUsers(usersIds);
        this.configurationForm.get('experience_notifications').setValue(usersIds);
      }

      if(response.data[0].USER_INTEREST_EMAILS){
        let usersIds =  this.convertData(response.data[0].USER_INTEREST_EMAILS, []);
        usersIds = Array.isArray(usersIds) ? usersIds: [];
        this.getLoadUpdateUsers(usersIds);
        this.configurationForm.get('interests_notifications').setValue(usersIds);
      }


    })
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(0, -1, 'start_date', 'descend')
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => (this.academic_years = result.data));
  }

  getLoadUpdateUsers(users_ids) {
    this.usersLoading = true;
    this.usersService
      .list(0, 100, null, null, {
        id: users_ids,
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.usersLoading = false))
      )
      .subscribe((res) => {
        this.users.push(...res.data);
      });
  }

  getUsers() {
    const loadUsers = (search: string): Observable<UserModel[]> =>
      this.usersService
        .list(0, 100, null, null, {
          withRelated: false,
          search,
          searchFields: 'name,user_name,student_number,email,identification',
        })
        .pipe(
          first(),
          map((results) => results.data)
        );

    const optionUsers$: Observable<
      UserModel[]
    > = this.usersSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadUsers));
    optionUsers$.subscribe((users) => {
      this.users = users;
      this.usersLoading = false;
    });
  }

  onSearchUsers(event) {
    this.usersLoading = true;
    this.usersSearchChange$.next(event);
  }

  submitForm(event){
    event.preventDefault();

    this.submit = true;
    this.loading = true;
    let sendValue: GeralSettingsModel = new GeralSettingsModel();

    if (!this.configurationForm.valid) {
      this.loading = false;
      Object.keys(this.configurationForm.controls).forEach(key => {
        this.configurationForm.controls[key].markAsDirty();
        this.configurationForm.controls[key].updateValueAndValidity();
      });
      this.configurationForm.markAsDirty();
      return;
    }

    if (this.configurationForm.valid) {
        this.submit = false;
        sendValue.FINANCIAL_NOTIFICATION_EMAIL = this.configurationForm.get('financial_notification_email').value;
        sendValue.MAX_HOURS_WORK = this.configurationForm.get('max_application_monthly_hours').value;
        sendValue.EXTERNAL_ENTITY_PROFILE_ID = this.configurationForm.get('profile_id').value;
        sendValue.ASSIGNED_STATUS_AUTO_CHANGE = this.configurationForm.get('assigned_status_auto_change').value;
        sendValue.STATUS_AUTO_CHANGE_DAYS = this.configurationForm.get('status_auto_change_days').value;
        sendValue.ACCEPTED_APPLICATION_STATUS_AUTO_CHANGE = this.configurationForm.get('accepted_application_status_auto_change').value;
        sendValue.RESPONSABLE_PROFILE = this.configurationForm.get('responsable_profile').value ? this.configurationForm.get('responsable_profile').value : null;
        sendValue.HAS_CURRENT_ACCOUNT = this.configurationForm.get('has_current_account').value;
        sendValue.CURRENT_ACCOUNT = this.configurationForm.get('current_account_id').value;
        sendValue.APPLICATION_EMAILS = this.configurationForm.get('application_notifications').value ? this.configurationForm.get('application_notifications').value  : null;
        sendValue.EXPERIENCE_EMAILS = this.configurationForm.get('experience_notifications').value ? this.configurationForm.get('experience_notifications').value : null;
        sendValue.USER_INTEREST_EMAILS = this.configurationForm.get('interests_notifications').value ? this.configurationForm.get('interests_notifications').value : null;
        this.configurationsService.create(sendValue).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
            this.uiService.showMessage(MessageType.success, 'Configurações alteradas com sucesso');
        })
    } else {
        this.loading = false;
    }
  }

  submitFormCloseApplications(event: any, value: any) {
    if (!this.authService.hasPermission('social_scholarship:configurations:update')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }
    this.submit = true;
    if (this.closeApplications.valid) {
      this.submit = false;
      this.loading = true;
      this.configurationsService
        .saveCloseApplication(value.close_academic_year)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((result) => {
          this.uiService.showMessage(MessageType.success, 'Configurações atualizadas com sucesso');
        });
    }
  }

  loadCurrentAccounts() {
    this.currentAccountService
      .list(1, -1, 'name', 'ascend')
      .pipe(first())
      .subscribe((results) => (this.currentAccounts = results.data));
  }

  getProfiles() {
    this.profilesService.list(1, -1, 'name', 'ascend', { withRelated: false }).pipe(first()).subscribe(response => {
      this.profiles = response.data;
    })
  }

  convertData(data, returnValue){
    try{
      return JSON.parse(data) ;
    }catch(e){
      return returnValue;
    }
  }
}
