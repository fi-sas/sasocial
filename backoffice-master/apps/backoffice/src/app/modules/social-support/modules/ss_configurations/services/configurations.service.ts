import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ConfigurationModel } from '@fi-sas/backoffice/modules/social-support/modules/ss_configurations/models/configuration.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { map } from 'rxjs/operators';
import { GeralSettingsModel } from '../../../models/geral-settings.model';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationsService extends Repository<GeralSettingsModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'SOCIAL_SUPPORT.CONFIGURATIONS';
  }

  saveCloseApplication(close_academic_year: string): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(
      this.urlService.get('SOCIAL_SUPPORT.CONFIGURATIONS_CLOSE_APPLICATIONS'),
      { academic_year: close_academic_year }
    );
  }

  getResponsibleProfileIds(): Observable<number[]> {
    return this.resourceService
      .list<ConfigurationModel>(this.urlService.get('SOCIAL_SUPPORT.CONFIGURATIONS_RESPONSIBLES_PROFILES_IDS'))
      .pipe(
        map((result) => {
          return this.getIdsByKey('responsable_profile', result);
        })
      );
  }

  private getIdsByKey(key: string, data) {
    const item = (data.data || []).find((config) => config.key === key);
    if (!item) {
      return [];
    }
    return (item.value || '').split(',').map((v) => parseInt(v));
  }

}
