import { TestBed } from '@angular/core/testing';

import { GeneralReportsService } from './general-reports.service';

describe('GeneralReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralReportsService = TestBed.get(GeneralReportsService);
    expect(service).toBeTruthy();
  });
});
