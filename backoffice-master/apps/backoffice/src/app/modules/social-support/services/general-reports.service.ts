import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { GeneralReportsApplicationModel } from '../models/general-reports.model';

@Injectable({
  providedIn: 'root',
})
export class GeneralReportsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  generalReportsApplications(
    id: number
  ): Observable<Resource<GeneralReportsApplicationModel>> {
    return this.resourceService.read<GeneralReportsApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.GENERAL_REPORTS_APPLICATIONS', {
        id
      })
    );
  }
}
