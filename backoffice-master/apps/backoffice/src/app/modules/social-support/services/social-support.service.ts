import { ApplicationsService } from '@fi-sas/backoffice/modules/social-support/modules/ss_applications/services/applications.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { first, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SocialSupportService {

  loading = false;

  private _applicationStats: Subject<any> = new Subject<any>();

  constructor(private applicationsService: ApplicationsService) { }

  applicationStatsObservable() {
    return this._applicationStats.asObservable();
  }

}
