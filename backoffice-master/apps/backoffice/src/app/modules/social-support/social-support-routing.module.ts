import { RouterModule, Routes } from "@angular/router";
import { SocialSupportComponent } from "@fi-sas/backoffice/modules/social-support/social-support.component";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { NgModule } from "@angular/core";
import { InitialPageComponent } from "@fi-sas/backoffice/shared/components/initial-page/initial-page.component";


const routes: Routes = [
  {
    path: '',
    component: SocialSupportComponent,
    children: [
      {
        path: 'applications',
        loadChildren: '../social-support/modules/ss_applications/applications.module#ApplicationsModule',
        data: { breadcrumb: 'Candidaturas', title: 'Candidaturas', scope: 'social_scholarship:applications'},
      },
      {
        path: 'experiences',
        loadChildren: '../social-support/modules/experiences/experiences.module#ExperiencesModule',
        data: { breadcrumb: 'Ofertas', title: 'Ofertas', scope: 'social_scholarship:experiences'},
      },
      {
        path: 'payments',
        loadChildren: '../social-support/modules/payments/payments.module#PaymentsModule',
        data: { breadcrumb: 'Pagamentos', title: 'Pagamentos', scope: 'social_scholarship:payment-grid'},
      },
      {
        path: 'experience-user-interests',
        loadChildren: '../social-support/modules/experience-user-interests/experience-user-interests.module#ExperienceUserInterestsModule',
        data: { breadcrumb: 'Manifestações de interesse', title: 'Manifestações de interesse', scope: 'social_scholarship:experience-user-interest'},
      },
      {
        path: 'collaborations-list',
        loadChildren: '../social-support/modules/collaborations-lists/collaborations-lists.module#CollaborationsListsModule',
        data: { breadcrumb: 'Colaborações', title: 'Colaborações', scope: 'social_scholarship:experiences:read'},
      },
      {
        path: 'complains',
        loadChildren: '../social-support/modules/complains/complains.module#ComplainsModule',
        data: { breadcrumb: 'Contestações / Reclamações', title: 'Contestações / Reclamações'},
      },
      {
        path: 'activities',
        loadChildren: '../social-support/modules/activities/activities.module#ActivitiesModule',
        data: { breadcrumb: 'Atividades', title: 'Atividades', scope: 'social_scholarship:activities'},
      },
      {
        path: 'configurations',
        loadChildren: '../social-support/modules/ss_configurations/configurations.module#SocialSupportConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações', scope: 'social_scholarship:configurations'},
      },
      {
        path: 'external-users',
        loadChildren: '../social-support/modules/external-users/external-users.module#ExternalUsersModule',
        data: { breadcrumb: 'Utilizadores Externos', title: 'Utilizadores Externos', scope: 'social_scholarship:external_entities'},
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'applications', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocialSupportRoutingModule {}
