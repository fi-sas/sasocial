import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialSupportComponent } from './social-support.component';

describe('SocialSupportComponent', () => {
  let component: SocialSupportComponent;
  let fixture: ComponentFixture<SocialSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
