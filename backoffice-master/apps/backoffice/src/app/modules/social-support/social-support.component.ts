import { SocialSupportService } from './services/social-support.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SiderItemBadge } from '@fi-sas/backoffice/core/models/sider-item-badge';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-social-support',
  template: '<router-outlet></router-outlet>',
})
export class SocialSupportComponent implements OnInit, OnDestroy {
  totalSubmitted = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#4d9de0',
  });

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }
  dataConfiguration: any;
  constructor(
    private uiService: UiService,
    private socialSupportService: SocialSupportService,
    private authService: AuthService,
    private configurationsService: ConfigurationGeralService
  ) {
    this.subscriptions = this.socialSupportService.applicationStatsObservable().subscribe(statsApplication => {
      this.updateBadges(statsApplication);
    });
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  updateBadges(stats: any) {
    this.totalSubmitted.next({
      value: stats.SUBMITTED ? stats.SUBMITTED : '-',
      color: '#4d9de0',
    });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(17), 'team');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const stats = new SiderItem(
      'Painel',
      '',
      '/social-support/applications/stats',
      'social_scholarship:applications:read'
    );
    this.uiService.addSiderItem(stats);

    const applications = new SiderItem('Candidaturas', '', '', 'social_scholarship:applications');
    applications.addChild(
      new SiderItem(
        'Listar',
        '',
        '/social-support/applications/app-all',
        'social_scholarship:applications:read',
        null,
        this.totalSubmitted
      )
    );
    this.uiService.addSiderItem(applications);

    const experiences = new SiderItem('Ofertas', '', '', 'social_scholarship:experiences');
    experiences.addChild(
      new SiderItem(
        'Criar',
        '',
        '/social-support/experiences/create',
        'social_scholarship:experiences:create'
      )
    );
    experiences.addChild(
      new SiderItem(
        'Listar',
        '',
        '/social-support/experiences/list',
        'social_scholarship:experiences:read'
      )
    );
    this.uiService.addSiderItem(experiences);

    const experienceUserInterests = new SiderItem('Manifestações de interesse', '', '', 'social_scholarship:experience-user-interest');
    experienceUserInterests.addChild(
      new SiderItem(
        'Listar',
        '',
        '/social-support/experience-user-interests/list',
        'social_scholarship:experience-user-interest:read'
      )
    );
    this.uiService.addSiderItem(experienceUserInterests);

    const collaborations = new SiderItem('Colaborações', '', '', 'social_scholarship:experiences');
    collaborations.addChild(
      new SiderItem(
        'Lista por Estudante',
        '',
        '/social-support/collaborations-list/student',
        'social_scholarship:experiences:list'
      ),

    );
    collaborations.addChild(
      new SiderItem(
        'Lista por Oferta',
        '',
        '/social-support/collaborations-list/offer',
        'social_scholarship:experiences:list'
      ),

    );
    this.uiService.addSiderItem(collaborations);

    const complains = new SiderItem('Contestações / Reclamações', '', '', 'social_scholarship:general-complains');
    complains.addChild(
      new SiderItem(
        'Geral',
        '',
        '/social-support/complains/list',
        'social_scholarship:general-complains:list'
      )
    );
    complains.addChild(
      new SiderItem(
        'Manifestações de Interesse',
        '',
        '/social-support/complains/list-user-interests',
        'social_scholarship:complain_user_interests:list'
      )
    );
    complains.addChild(
      new SiderItem(
        'Ofertas',
        '',
        '/social-support/complains/list-experiences',
        'social_scholarship:complain-applications:list'
      )
    );
    complains.addChild(
      new SiderItem(
        'Colaboradores',
        '',
        '/social-support/complains/list-applications',
        'social_scholarship:complain-applications:list'
      )
    );
    this.uiService.addSiderItem(complains);

    const payments = new SiderItem(
      'Pagamentos',
      '',
      '/social-support/payments/list',
      'social_scholarship:payment-grid'
    );
    this.uiService.addSiderItem(payments);

    const externalUsers = new SiderItem('Utilizadores Externos', '', '', 'social_scholarship:external_entities');
    externalUsers.addChild(
      new SiderItem(
        'Criar',
        '',
        '/social-support/external-users/create',
        'social_scholarship:external_entities:create'
      )
    );
    externalUsers.addChild(
      new SiderItem(
        'Listar',
        '',
        '/social-support/external-users/list',
        'social_scholarship:external_entities:read'
      )
    );
    this.uiService.addSiderItem(externalUsers);


    const configurations = new SiderItem('Configurações');
    const activities = configurations.addChild(new SiderItem('Atividades Preferenciais', '', '', 'social_scholarship:activities'));
    activities.addChild(new SiderItem(
      'Criar',
      '',
      '/social-support/activities/create',
      'social_scholarship:activities:create'
    )
    );
    activities.addChild(
      new SiderItem(
        'Listar',
        '',
        '/social-support/activities/list',
        'social_scholarship:activities:read'
      )
    );

    this.uiService.addSiderItem(configurations);

    const geralSetting = new SiderItem('Configurações Gerais', '', '/social-support/configurations/form',
      'social_scholarship:configurations:create');
    this.uiService.addSiderItem(geralSetting);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
