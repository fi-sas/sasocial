import { SocialSupportModule } from "@fi-sas/backoffice/modules/social-support/social-support.module";

describe('SocialSupportModule', () => {
  let socialSupport: SocialSupportModule;

  beforeEach(() => {
    socialSupport = new SocialSupportModule();
  });

  it('should create an instance', () => {
    expect(socialSupport).toBeTruthy();
  });
});
