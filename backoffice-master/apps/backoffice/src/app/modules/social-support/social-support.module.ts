import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialSupportComponent } from "@fi-sas/backoffice/modules/social-support/social-support.component";
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SocialSupportRoutingModule } from "@fi-sas/backoffice/modules/social-support/social-support-routing.module";

@NgModule({
  declarations: [
    SocialSupportComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SocialSupportRoutingModule
  ]
})
export class SocialSupportModule { }
