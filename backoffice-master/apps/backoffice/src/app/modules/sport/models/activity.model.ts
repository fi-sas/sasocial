import { ModalityModel } from "@fi-sas/backoffice/modules/sport/models/modality.model";
import { VenueModel } from "@fi-sas/backoffice/modules/sport/models/venue.model";

export class ActivityModel {
  id?: number;
  type: string;
  modality_id: number;
  venue_id: number;
  event_id?: number;
  name: string;
  description: string;
  start_date: string;
  end_date: string;
  training_day?: string;
  start_time: string;
  end_time?: string;
  active: boolean;
  created_at?: string;
  updated_at?: string;
  venue?: VenueModel;
  modality?: ModalityModel;
}

export class ArrayData {

  training_day = [
    {key: '', pt:'Todos'},
    {key: 'MONDAY', pt:'Segunda-feira'},
    {key: 'TUESDAY', pt: 'Terça-feira'},
    {key: 'WEDNESDAY', pt: 'Quarta-feira'},
    {key: 'THURSDAY', pt: 'Quinta-feira'},
    {key: 'FRIDAY', pt: 'Sexta-feira'},
    {key: 'SATURDAY', pt: 'Sábado'},
    {key: 'SUNDAY', pt: 'Domingo'}
  ];

  type = [
    {key: 'GENERAL ACTIVITY', pt:'Atividade geral'},
    {key: 'REGULAR TRAINING', pt: 'Treino regular'},
    {key: 'COMPETITION', pt: 'Competição'}
  ];

}
