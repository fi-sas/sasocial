
export enum Keys {
  Regular_training_event_category = 'regular_training_event_category_id',
  Competition_event_category = 'competition_event_category_id',
  General_activity_event_category = 'general_activity_event_category_id',
  Activity_registration_form = 'activity_registration_form_id'
}

export class ConfigurationModel {
  id?: number;
  key?: string;
  value: number;
  updated_at?: string;
  created_at?: string;
}


