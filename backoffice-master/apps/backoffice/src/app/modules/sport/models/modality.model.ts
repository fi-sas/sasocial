import { ActivityModel } from "@fi-sas/backoffice/modules/sport/models/activity.model";

export class ModalityModel {
  id?: number;
  name: string;
  active: boolean;
  file_id: number;
  created_at?: string;
  updated_at?: string;
  activities?: ActivityModel[];
  regular_trainings?: ActivityModel[];
  general_activities?: ActivityModel[];
  competitions?: ActivityModel[];
}

