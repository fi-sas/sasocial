import { HistoryModel } from "@fi-sas/backoffice/modules/sport/models/history.model";
import { VenueModel } from "@fi-sas/backoffice/modules/sport/models/venue.model";

export enum ReservationStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected',
  Cancelled = 'Cancelled'
}

export class ReservationModel {
  id?: number;
  venue_id: number;
  user_id: number;
  user_profile_id: number;
  user_profile_name: string;
  name: string;
  student_number: string;
  identification_number: string;
  tin: string;
  address: string;
  postal_code: string;
  city: string;
  email: string;
  phone_number: string;
  start_date: string;
  end_date: string;
  recurrence: string;
  usage_description: string;
  file_id?: number;
  status: string;
  created_at?: string;
  updated_at?: string;
  history?: HistoryModel[];
  venue?: VenueModel;
}

export class ArrayData {

  recurrence = [
    {key: 'None', pt:'Nenhuma'},
    {key: 'Daily', pt: 'Diariamente'},
    {key: 'Weekly', pt: 'Semanal'}
  ];

}

export class RejectModel {
  notes: string;
}

export class ApproveModel {
  notes: string;
}
