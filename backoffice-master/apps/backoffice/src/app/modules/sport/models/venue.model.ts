import { ActivityModel } from "@fi-sas/backoffice/modules/sport/models/activity.model";
import { ReservationModel } from "@fi-sas/backoffice/modules/sport/models/reservation.model";

export class VenueModel {
  id?: number;
  name: string;
  active: boolean;
  longitude: number;
  latitude: number;
  address: string;
  address_no: string;
  city: string;
  location_id?: number;
  created_at?: string;
  updated_at?: string;
  reservations?: ReservationModel[];
  activities?: ActivityModel[];
  regular_trainings?: ActivityModel[];
  general_activities?: ActivityModel[];
  competitions?: ActivityModel[];
}
