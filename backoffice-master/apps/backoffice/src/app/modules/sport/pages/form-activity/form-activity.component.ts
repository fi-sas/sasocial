import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivityModel, ArrayData } from "@fi-sas/backoffice/modules/sport/models/activity.model";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { ActivitiesService } from "@fi-sas/backoffice/modules/sport/services/activities.service";
import { ModalitiesService } from "@fi-sas/backoffice/modules/sport/services/modalities.service";
import { VenuesService } from "@fi-sas/backoffice/modules/sport/services/venues.service";
import { first } from "rxjs/operators";
import { ModalityModel } from "@fi-sas/backoffice/modules/sport/models/modality.model";
import { VenueModel } from "@fi-sas/backoffice/modules/sport/models/venue.model";

@Component({
  selector: 'fi-sas-form-activity',
  templateUrl: './form-activity.component.html',
  styleUrls: ['./form-activity.component.less']
})
export class FormActivityComponent implements OnInit {

  @Input() isUpdate = false;

  activityForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    modality_id: new FormControl('', Validators.required),
    venue_id: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    active: new FormControl(true)
  });

  isLoading = false;

  updateActivity: ActivityModel = null;

  modalities: ModalityModel[] = [];
  venues: VenueModel[] = [];

  public types = new ArrayData().type;

  constructor(private uiService: UiService,
              private listService: ListService,
              private activityService: ActivitiesService,
              private modalityService: ModalitiesService,
              private venueService: VenuesService) { }

  ngOnInit() {

    this.getModalities();
    this.getVenues();

    if (this.isUpdate) {
      this.listService.selectedItemObservable().subscribe(item => {

        if (item) {
          this.isLoading = true;
          this.updateActivity = item;

          this.activityForm.patchValue({
            name: this.updateActivity.name,
            type: this.updateActivity.type,
            description: this.updateActivity.description,
            modality_id: this.updateActivity.modality_id,
            venue_id: this.updateActivity.venue_id,
            date: [this.updateActivity.start_date, this.updateActivity.end_date],
            active: this.updateActivity.active
          });

          this.isUpdate = true;
          this.isLoading = false;
        }
      });
    }
  }

  getModalities() {
    this.modalityService.list(1,-1).subscribe(modality => {
      this.modalities = modality.data;
    },() => {
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao carregar as modalidades'
      )
    });
  }

  getVenues() {
    this.venueService.list(1,-1).subscribe(venues => {
      this.venues = venues.data;
    },() => {
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao carregar os espaços'
      )
    });
  }

  getNameInputError() {
    return this.activityForm.controls.name.errors.required
      ? 'Introduza um nome'
      : '';
  }

  getTypeInputError() {
    return this.activityForm.controls.type.errors.required
      ? 'Introduza um tipo de atividade'
      : '';
  }

  getDescriptionInputError() {
    return this.activityForm.controls.description.errors.required
      ? 'Introduza uma descrição'
      : '';
  }

  getModalityInputError() {
    return this.activityForm.controls.modality_id.errors.required
      ? 'Selecione uma modalidade'
      : '';
  }

  getVenueInputError() {
    return this.activityForm.controls.venue_id.errors.required
      ? 'Selecione um espaço'
      : '';
  }

  getDateInputError() {
    return this.activityForm.controls.date.errors.required
      ? 'Selecione uma data inicial e final'
      : '';
  }

  submit(value: any, valid: boolean) {

    if (!valid) {
      this.activityForm.updateValueAndValidity();
      return;
    }

    value.start_date = value.date[0];
    value.end_date = value.date[1];
    delete value.date;

    this.activityForm.disable();
    this.isLoading = true;

    if (this.isUpdate) {
      this.activityService.update(this.updateActivity.id, value).subscribe(activity => {
        this.updateActivity = activity.data[0];
        this.uiService.showMessage(MessageType.success, 'Atividade alterada com sucesso');
        this.listService.itemUpdated(this.updateActivity);
        this.isLoading = false;
        this.activityForm.enable();
      }, () => {
        this.isLoading = false;
        this.activityForm.enable();
      });
    } else {

      this.activityService.create(value).subscribe(modality => {
        this.updateActivity = modality.data[0];
        this.isLoading = false;
        this.uiService.showMessage(MessageType.success, 'Atividade inserida com sucesso');
        this.activityForm.enable();
      }, () => {
        this.isLoading = false;
        this.activityForm.enable();
      });
    }
  }

  deleteActivity() {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta atividade?', 'Eliminar').subscribe(result => {
      if (result) {
        this.activityService.delete(this.updateActivity.id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.updateActivity);
        });
      }
    });
  }

}
