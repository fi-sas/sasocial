import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ConfigurationsService } from "@fi-sas/backoffice/modules/sport/services/configurations.service";
import { forkJoin } from "rxjs";
import { FiConfigurator } from "@fi-sas/configurator";

@Component({
  selector: 'fi-sas-form-configurations',
  templateUrl: './form-configurations.component.html',
  styleUrls: ['./form-configurations.component.less']
})
export class FormConfigurationsComponent implements OnInit {

  configurations = null;
  configurationForm: FormGroup;
  isLoading = false;
  categories = null;
  registration_forms = null;
  language_id: number;

  constructor(private route: ActivatedRoute,
              private uiService: UiService,private configurator: FiConfigurator) {

    this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
    this.configurationForm = new FormGroup({
      regular_training_event_category_id: new FormControl('', Validators.required),
      competition_event_category_id: new FormControl('', Validators.required),
      general_activity_event_category_id: new FormControl('', Validators.required),
      activity_registration_form_id: new FormControl('', Validators.required)
    });
  }

  submit(value) {

   /* this.isLoading = true;
    this.configurationForm.disable();

    const observable = forkJoin(
      this.configurationService.setRegularTrainingEventCategory({value: value.regular_training_event_category_id}),
      this.configurationService.setGeneralActivityEventCategory({value: value.competition_event_category_id}),
      this.configurationService.setCompetitionEventCategory({value: value.general_activity_event_category_id}),
      this.configurationService.setActivityRegistrationForm({value: value.activity_registration_form_id}),
    );

    observable.subscribe(() => {

      this.isLoading = false;
      this.configurationForm.enable();
      this.uiService.showMessage(
        MessageType.success,
        'Configuração guardada com sucesso'
      );
    }, () => {
      this.isLoading = false;
      this.configurationForm.enable();
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao guardar a configuração'
      )
    });
*/
  }

  getEventCategories() {
    /*this.eventCategoryService.list(1,-1).subscribe(eventCategories => {
      this.categories = eventCategories.data;
    },() => {
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao carregar as categorias'
      )
    });*/
  }

  getRegistrationForms() {
   /* this.registrationFormService.list(1,-1).subscribe(registrationForms => {
      this.registration_forms = registrationForms.data;
    },() => {
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao carregar os formulário de registro'
      )
    });*/
  }

  getRegularTrainingEventCategoryError() {
    return this.configurationForm.controls.regular_training_event_category_id.errors.required
      ? 'Selecione a categoria'
      : this.configurationForm.controls.regular_training_event_category_id.errors.pattern ? 'Selecione a categoria': '';
  }

  getCompetitionEventCategoryError() {
    return this.configurationForm.controls.competition_event_category_id.errors.required
      ? 'Selecione a categoria'
      : ''
  }

  getGeneralActivityEventCategoryError() {
    return this.configurationForm.controls.general_activity_event_category_id.errors.required
      ? 'Selecione a categoria'
      : '';
  }

  getActivityRegistrationFormError() {
    return this.configurationForm.controls.activity_registration_form_id.errors.required
      ? 'Selecione um formulário'
      : '';
  }

  ngOnInit() {

    this.getEventCategories();
    this.getRegistrationForms();

    this.configurations = this.route.snapshot.data.configurations;

    if (this.configurations !== null) {

      this.configurationForm.patchValue({
        regular_training_event_category_id: this.configurations.regular_training_event_category[0].value ,
        competition_event_category_id: this.configurations.competition_event_category[0].value,
        general_activity_event_category_id: this.configurations.general_activity_event_category[0].value,
        activity_registration_form_id: this.configurations.activity_registration_form[0].value
      });

    }
  }

}
