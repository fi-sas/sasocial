import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

import { ConfigurationsService } from "@fi-sas/backoffice/modules/sport/services/configurations.service";

@Injectable()
export class FormConfigurationsResolver
  implements Resolve<Observable<Resource<any>>> {
  constructor(
    private configurationsService: ConfigurationsService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {

    return Observable.create(o => {

      class DataOutModel {
        regular_training_event_category: any;
        competition_event_category: any;
        general_activity_event_category: any;
        activity_registration_form: any;
      }

      const data_out = new DataOutModel();

      this.configurationsService.readRegularTrainingEventCategory().pipe(first()).subscribe(regular_training_event_category => {

        data_out.regular_training_event_category = regular_training_event_category.data;

        this.configurationsService.readCompetitionEventCategory().pipe(first()).subscribe(competition_event_category => {

          data_out.competition_event_category = competition_event_category.data;

          this.configurationsService.readGeneralActivityEventCategory().pipe(first()).subscribe(general_activity_event_category => {

            data_out.general_activity_event_category = general_activity_event_category.data;

            this.configurationsService.readActivityRegistrationForm().pipe(first()).subscribe(activity_registration_form => {

                data_out.activity_registration_form = activity_registration_form.data;

                o.next(data_out);
                o.complete();

            },error0 => {

              o.error(error0);

            });

          }, error1 => {

            o.error(error1);
          });

        }, error2 => {
          o.error(error2);
        });

      }, error3 => {
        o.error(error3);
      });
    }, () => {
      return EMPTY;
    });

  }
}
