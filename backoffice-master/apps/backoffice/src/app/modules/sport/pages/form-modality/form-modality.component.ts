import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { ModalitiesService } from "@fi-sas/backoffice/modules/sport/services/modalities.service";
import { ModalityModel } from "@fi-sas/backoffice/modules/sport/models/modality.model";
import { first } from "rxjs/operators";

@Component({
  selector: 'fi-sas-form-modality',
  templateUrl: './form-modality.component.html',
  styleUrls: ['./form-modality.component.less']
})
export class FormModalityComponent implements OnInit {

  @Input() isUpdate = false;

  modalityForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    file_id: new FormControl('', Validators.required),
    active: new FormControl(true)
  });

  isLoading = false;
  updateModality: ModalityModel = null;


  constructor(private uiService: UiService,
              private listService: ListService,
              private mobilityService: ModalitiesService) { }

  ngOnInit() {
    if (this.isUpdate) {
      this.listService.selectedItemObservable().subscribe(item => {

        if (item) {
          this.isLoading = true;
          this.updateModality = item;

          this.modalityForm.patchValue({
            name: this.updateModality.name,
            file_id: this.updateModality.file_id,
            active: this.updateModality.active
          });

          this.isUpdate = true;
          this.isLoading = false;
        }
      });
    }
  }

  getNameInputError() {
    return this.modalityForm.controls.name.errors.required
      ? 'Introduza um nome'
      : '';
  }

  getImageInputError() {
    return this.modalityForm.controls.file_id.errors.required
      ? 'Insere uma imagem'
      : '';
  }

  submit(value: any, valid: boolean) {

    if (!valid) {
      this.modalityForm.updateValueAndValidity();
      return;
    }

    this.modalityForm.disable();
    this.isLoading = true;

    if (this.isUpdate) {
      this.mobilityService.update(this.updateModality.id, value).subscribe(modality => {
        this.updateModality = modality.data[0];
        this.uiService.showMessage(MessageType.success, 'Modalidade alterada com sucesso');
        this.listService.itemUpdated(this.updateModality);
        this.isLoading = false;
        this.modalityForm.enable();
      });
    } else {

      this.mobilityService.create(value).subscribe(modality => {
        this.updateModality = modality.data[0];
        this.isLoading = false;
        this.uiService.showMessage(MessageType.success, 'Modalidade inserida com sucesso');
        this.modalityForm.enable();
      });
    }
  }

  deleteModality() {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta modalidade?', 'Eliminar').subscribe(result => {
      if (result) {
        this.mobilityService.delete(this.updateModality.id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.updateModality);
        });
      }
    });
  }

}
