import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { VenueModel } from "@fi-sas/backoffice/modules/sport/models/venue.model";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { VenuesService } from "@fi-sas/backoffice/modules/sport/services/venues.service";
import { first } from "rxjs/operators";
import { LocationsService } from "@fi-sas/backoffice/modules/infrastructure/services/locations.service";
import { LocationModel } from "@fi-sas/backoffice/modules/infrastructure/models/location.model";

@Component({
  selector: 'fi-sas-form-venue',
  templateUrl: './form-venue.component.html',
  styleUrls: ['./form-venue.component.less']
})
export class FormVenueComponent implements OnInit {

  @Input() isUpdate = false;

  venueForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    active: new FormControl(true),
    longitude: new FormControl('', [
      Validators.required,
      Validators.pattern('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}')
    ]),
    latitude: new FormControl('', [
      Validators.required,
      Validators.pattern('^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\\.{1}\\d{1,6}')
    ]),
    address: new FormControl('', Validators.required),
    address_no: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    location_id: new FormControl('')
  });

  isLoading = false;
  updateVenue: VenueModel = null;
  locations: LocationModel[] = [];

  constructor(private uiService: UiService,
              private listService: ListService,
              private venueService: VenuesService,
              private locationService: LocationsService) { }

  ngOnInit() {

    this.getLocations();

    if (this.isUpdate) {
      this.listService.selectedItemObservable().subscribe(item => {

        if (item) {
          this.isLoading = true;
          this.updateVenue = item;

          this.venueForm.patchValue({
            name: this.updateVenue.name,
            active: this.updateVenue.active,
            longitude: this.updateVenue.longitude,
            latitude: this.updateVenue.longitude,
            address: this.updateVenue.address,
            address_no: this.updateVenue.address_no,
            city: this.updateVenue.city,
            location_id: this.updateVenue.location_id
          });

          this.isUpdate = true;
          this.isLoading = false;
        }
      });
    }
  }

  getLocations() {

    this.locationService.list(1,-1).subscribe(locations => {
      this.locations = locations.data;
    },() => {
      this.uiService.showMessage(
        MessageType.error,
        'Erro ao carregar as localizações'
      )
    });

  }

  getNameInputError() {
    return this.venueForm.controls.name.errors.required
      ? 'Introduza um nome'
      : '';
  }

  getLatitudeInputError() {
    return this.venueForm.controls.latitude.errors.required
      ? 'Introduza a latitude'
      : this.venueForm.controls.latitude.errors.pattern
        ? 'Introduza uma latitude válida'
        : '';
  }

  getLongitudeInputError() {
    return this.venueForm.controls.longitude.errors.required
      ? 'Introduza a longitude'
      : this.venueForm.controls.longitude.errors.pattern
        ? 'Introduza uma longitude válida'
        : '';
  }

  getAddressInputError() {
    return this.venueForm.controls.address.errors.required
      ? 'Introduza um morada'
      : '';
  }

  getAddressNoInputError() {
    return this.venueForm.controls.address_no.errors.required
      ? 'Introduza o número da porta'
      : '';
  }

  getCityInputError() {
    return this.venueForm.controls.city.errors.required
      ? 'Introduza uma cidade'
      : '';
  }

  submit(value: any, valid: boolean) {

    if (!valid) {
      this.venueForm.updateValueAndValidity();
      return;
    }

    this.venueForm.disable();
    this.isLoading = true;

    if (this.isUpdate) {
      this.venueService.update(this.updateVenue.id, value).subscribe(venue => {
        this.updateVenue = venue.data[0];
        this.uiService.showMessage(MessageType.success, 'Espaço alterado com sucesso');
        this.listService.itemUpdated(this.updateVenue);
        this.isLoading = false;
        this.venueForm.enable();
      }, () => {
        this.isLoading = false;
        this.venueForm.enable();
      });
    } else {

      this.venueService.create(value).subscribe(modality => {
        this.updateVenue = modality.data[0];
        this.isLoading = false;
        this.uiService.showMessage(MessageType.success, 'Espaço inserido com sucesso');
        this.venueForm.enable();
      }, () => {
        this.isLoading = false;
        this.venueForm.enable();
      });
    }
  }

  deleteVenue() {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar este espaço?', 'Eliminar').subscribe(result => {
      if (result) {
        this.venueService.delete(this.updateVenue.id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.updateVenue);
        });
      }
    });
  }

}
