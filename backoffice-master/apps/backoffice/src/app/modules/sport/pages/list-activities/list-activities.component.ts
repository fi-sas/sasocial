import { Component, OnInit } from '@angular/core';
import { ActivitiesService } from "@fi-sas/backoffice/modules/sport/services/activities.service";
import { CustomControlOption, CustomControl } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { VenuesService } from "@fi-sas/backoffice/modules/sport/services/venues.service";
import { FiConfigurator } from "@fi-sas/configurator";

@Component({
  selector: 'fi-sas-list-activities',
  templateUrl: './list-activities.component.html',
  styleUrls: ['./list-activities.component.less']
})
export class ListActivitiesComponent implements OnInit {

  customSearchs: CustomControl[] =  [
    {name: 'type',label: 'Tipo',
      type: CustomControlOption.SELECT,
      values: {
        'GENERAL ACTIVITY': 'Atividade geral',
        'REGULAR TRAINING': 'Treinos regulares',
        COMPETITION: 'Competição'
      }
    }, {name: 'training_day',label: 'Dia da semana',
      type: CustomControlOption.SELECT,
      values: {
        MONDAY: 'Segunda-feira',
        TUESDAY: 'Terça-feira',
        WEDNESDAY: 'Quarta-feira',
        THURSDAY: 'Quinta-feira',
        FRIDAY: 'Sexta-feira',
        SATURDAY: 'Sábado',
        SUNDAY: 'Domingo'
      }
    }
  ];
  venueSelect = {};
  eventSelect = {};
  language_id: number;

  constructor(public activityService: ActivitiesService,
              private venueService: VenuesService,
              private configurator: FiConfigurator) {
    this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
  }

  ngOnInit() {
    this.venueService.list(-1, -1).subscribe(venues => {
      venues.data.map(venue => {
        this.venueSelect[venue.id] = venue.name;
      });
      this.customSearchs.push({
        name: 'venue_id',
        label: 'Espaços',
        type: CustomControlOption.SELECT,
        values: this.venueSelect
      });
    });


    /*this.eventService.list(-1,-1).subscribe( events => {
      events.data.map(event => {
        this.eventSelect[event.id] = event.translations.find(translation => translation.language_id === this.language_id).name;
      });
      this.customSearchs.push({
        name: 'event_id',
        label: 'Eventos',
        type: CustomControlOption.SELECT,
        values: this.eventSelect
      });
    });*/
  }

}
