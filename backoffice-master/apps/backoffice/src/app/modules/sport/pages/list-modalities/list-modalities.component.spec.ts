import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListModalitiesComponent } from './list-modalities.component';

describe('ListModalitiesComponent', () => {
  let component: ListModalitiesComponent;
  let fixture: ComponentFixture<ListModalitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListModalitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListModalitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
