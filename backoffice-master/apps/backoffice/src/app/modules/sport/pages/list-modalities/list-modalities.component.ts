import { Component, OnInit } from '@angular/core';
import { ModalitiesService } from "@fi-sas/backoffice/modules/sport/services/modalities.service";
import { CustomControlOption, CustomControl } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';

@Component({
  selector: 'fi-sas-list-modalities',
  templateUrl: './list-modalities.component.html',
  styleUrls: ['./list-modalities.component.less']
})
export class ListModalitiesComponent implements OnInit {

  CustomControlOption = CustomControlOption;

  constructor(public modalityService: ModalitiesService) { }

  ngOnInit() {
  }

}
