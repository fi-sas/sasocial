import { Component, OnInit } from '@angular/core';
import { ReservationsService } from "@fi-sas/backoffice/modules/sport/services/reservations.service";
import { CustomControlOption, CustomControl } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { VenuesService } from "@fi-sas/backoffice/modules/sport/services/venues.service";
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';

@Component({
  selector: 'fi-sas-list-reservations',
  templateUrl: './list-reservations.component.html',
  styleUrls: ['./list-reservations.component.less']
})
export class ListReservationsComponent implements OnInit {

  customSearchs: CustomControl[] =  [
    {name: 'status',label: 'Estado',
      type: CustomControlOption.SELECT,
      values: {
        Pending: 'Em espera',
        Approved: 'Aprovado',
        Rejected: 'Rejeitado',
        Cancelled: 'Cancelado'
      }
    }, {name: 'recurrence',label: 'Recorrência',
      type: CustomControlOption.SELECT,
      values: {
        None: 'Nenhuma',
        Daily: 'Diariamente',
        Weekly: 'Semanal',
      }
    }, {
      name: 'user_id',
      label: 'Utilizadores',
      type: CustomControlOption.SELECT,
      repository: this.userService,
      searchOnServer: true,
      searchKey: 'name'
    },
    {
      name: 'user_profile_id',
      label: 'Perfis',
      type: CustomControlOption.SELECT,
      repository: this.profileService,
      searchOnServer: true,
      searchKey: 'name'
    },{
      name: 'venue_id',
      label: 'Espaços',
      type: CustomControlOption.SELECT,
      repository: this.venueService,
      searchOnServer: true,
      searchKey: 'name'
    }
  ];

  constructor(public reservationService: ReservationsService,
              private venueService: VenuesService,
              private userService: UsersService,
              private profileService: ProfilesService) { }

  ngOnInit() {
  }

}
