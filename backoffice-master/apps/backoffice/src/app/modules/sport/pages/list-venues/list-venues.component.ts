import { Component, OnInit } from '@angular/core';
import { VenuesService } from "@fi-sas/backoffice/modules/sport/services/venues.service";
import { CustomControlOption } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';

@Component({
  selector: 'fi-sas-list-venues',
  templateUrl: './list-venues.component.html',
  styleUrls: ['./list-venues.component.less']
})
export class ListVenuesComponent implements OnInit {

  CustomControlOption = CustomControlOption;

  constructor(public venueService: VenuesService) { }

  ngOnInit() {
  }

}
