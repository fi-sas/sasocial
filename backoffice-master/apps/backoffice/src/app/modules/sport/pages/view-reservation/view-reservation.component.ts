import { Component, OnInit } from '@angular/core';
import {
  ArrayData,
  ReservationModel,
  ReservationStatus
} from "@fi-sas/backoffice/modules/sport/models/reservation.model";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { ListService } from "@fi-sas/backoffice/shared/services/list.service";
import { ReservationsService } from "@fi-sas/backoffice/modules/sport/services/reservations.service";
import { first } from "rxjs/operators";
import { ApproveModel, RejectModel } from "@fi-sas/backoffice/modules/sport/models/reservation.model";

@Component({
  selector: 'fi-sas-view-reservation',
  templateUrl: './view-reservation.component.html',
  styleUrls: ['./view-reservation.component.less']
})
export class ViewReservationComponent implements OnInit {


  isHistoryVisible = false;
  isNotesVisible = false;
  notes = '';
  reservation: ReservationModel = null;
  ReservationStatus = ReservationStatus;
  public recurrences = new ArrayData().recurrence;

  constructor(private uiService: UiService,
              private listService: ListService,
              private reservationService: ReservationsService) { }

  showHistory() {
    this.isHistoryVisible = true;
  }

  handleHistoryCancel() {
    this.isHistoryVisible = false;
  }

  handleHistoryOk() {
    this.isHistoryVisible = false;
  }

  showStatusModal() {
    this.isNotesVisible = true;
  }

  handleNotesCancel() {
    this.isNotesVisible = false;
  }

  approve() {
    this.uiService.showConfirm('Aprovar', 'Pretende aprovar esta reserva?', 'Aprovar').subscribe(result => {
      if (result) {
        const notes : ApproveModel = {notes: this.notes};
        this.reservationService.approve(this.reservation.id, notes).pipe(first()).subscribe(value => {
          this.reservation.status = value.data[0].status;
          this.listService.itemUpdated(this.reservation);
          this.handleNotesCancel();
        });
      }
    });
  }

  reject() {
    this.uiService.showConfirm('Rejeitar', 'Pretende rejeitar esta reserva?', 'Rejeitar').subscribe(result => {
      if (result) {
        const notes : RejectModel = {notes: this.notes};
        this.reservationService.reject(this.reservation.id, notes).pipe(first()).subscribe(value => {
          this.reservation.status = value.data[0].status;
          this.listService.itemUpdated(this.reservation);
          this.handleNotesCancel();
        });
      }
    });
  }

  deleteReservation(id : number) {

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta reserva?', 'Eliminar').subscribe(result => {
      if (result) {
        this.reservationService.delete(id).pipe(first()).subscribe(() => {
          this.listService.deleteItem(this.reservation);
        });
      }
    });
  }

  ngOnInit() {
    this.listService.selectedItemObservable().subscribe(item => {
      this.reservation = item;
    })
  }

}
