import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterRecurrence'
})
export class FilterRecurrencePipe implements PipeTransform {

  transform(reservationArray: any, reservation_name: string): any {
    return reservationArray.find(reservation => reservation.key === reservation_name);
  }

}
