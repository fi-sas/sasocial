import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ActivityModel } from "@fi-sas/backoffice/modules/sport/models/activity.model";

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService extends Repository<ActivityModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'ACTIVITIES';
    this.entity_url = 'ACTIVITIES_ID';
  }
}
