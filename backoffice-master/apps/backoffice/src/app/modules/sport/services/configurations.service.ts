import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ConfigurationModel } from "@fi-sas/backoffice/modules/sport/models/configuration.model";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  getConfigurations(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string, key?: string, created_at?: string, updated_at?: string): Observable<Resource<ConfigurationModel>> {

    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }

    if(key) {
      params = params.set('key', key);
    }

    if(created_at) {
      params = params.set('created_at', created_at);
    }

    if(updated_at) {
      params = params.set('updated_at', updated_at);
    }

    return this.resourceService.read<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_SPORT'), {params});
  }

  setRegularTrainingEventCategory(configuration: ConfigurationModel): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_REGULAR_TRAINING_EVENT_CATEGORY_ID'), configuration);
  }

  setCompetitionEventCategory(configuration: ConfigurationModel): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_COMPETITION_EVENT_CATEGORY_ID'), configuration);
  }

  setGeneralActivityEventCategory(configuration: ConfigurationModel): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_GENERAL_ACTIVITY_EVENT_CATEGORY_ID'), configuration);
  }

  setActivityRegistrationForm(configuration: ConfigurationModel): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_ACTIVITY_REGISTRATION_FORM_ID'), configuration);
  }


  readRegularTrainingEventCategory(): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.read<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_REGULAR_TRAINING_EVENT_CATEGORY_ID'));
  }

  readCompetitionEventCategory(): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.read<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_COMPETITION_EVENT_CATEGORY_ID'));
  }

  readGeneralActivityEventCategory(): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.read<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_GENERAL_ACTIVITY_EVENT_CATEGORY_ID'));
  }

  readActivityRegistrationForm(): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.read<ConfigurationModel>(this.urlService.get('CONFIGURATIONS_ACTIVITY_REGISTRATION_FORM_ID'));
  }
}
