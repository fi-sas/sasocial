import { TestBed } from '@angular/core/testing';

import { ModalitiesService } from './modalities.service';

describe('ModalitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalitiesService = TestBed.get(ModalitiesService);
    expect(service).toBeTruthy();
  });
});
