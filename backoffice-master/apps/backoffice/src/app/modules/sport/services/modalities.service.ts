import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ModalityModel } from "@fi-sas/backoffice/modules/sport/models/modality.model";

@Injectable({
  providedIn: 'root'
})
export class ModalitiesService extends Repository<ModalityModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'MODALITIES';
    this.entity_url = 'MODALITIES_ID';
  }
}
