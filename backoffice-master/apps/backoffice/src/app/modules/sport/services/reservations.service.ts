import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ReservationModel, ApproveModel, RejectModel } from "@fi-sas/backoffice/modules/sport/models/reservation.model";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ReservationsService extends Repository<ReservationModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'RESERVATIONS';
    this.entity_url = 'RESERVATIONS_ID';
  }

  approve(id: number, reservationApprove: ApproveModel): Observable<Resource<ReservationModel>> {
    return this.resourceService.create<ReservationModel>(this.urlService.get('RESERVATIONS_APPROVE', {id}), reservationApprove);
  }

  reject(id: number, reservationReject: RejectModel): Observable<Resource<ReservationModel>> {
    return this.resourceService.create<ReservationModel>(this.urlService.get('RESERVATIONS_REJECT', {id}), reservationReject);
  }
}
