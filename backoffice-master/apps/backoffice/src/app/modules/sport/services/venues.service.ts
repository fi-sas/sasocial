import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { VenueModel } from "@fi-sas/backoffice/modules/sport/models/venue.model";

@Injectable({
  providedIn: 'root'
})
export class VenuesService extends Repository<VenueModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VENUES';
    this.entity_url = 'VENUES_ID';
  }
}
