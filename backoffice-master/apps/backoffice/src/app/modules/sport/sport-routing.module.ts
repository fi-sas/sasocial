import { RouterModule, Routes } from "@angular/router";
import { SportComponent } from "@fi-sas/backoffice/modules/sport/sport.component";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { NgModule } from "@angular/core";
import { FormModalityComponent } from "@fi-sas/backoffice/modules/sport/pages/form-modality/form-modality.component";
import { FormVenueComponent } from "@fi-sas/backoffice/modules/sport/pages/form-venue/form-venue.component";
import { FormActivityComponent } from "@fi-sas/backoffice/modules/sport/pages/form-activity/form-activity.component";
import { FormConfigurationsComponent } from "@fi-sas/backoffice/modules/sport/pages/form-configurations/form-configurations.component";
import { FormConfigurationsResolver } from "@fi-sas/backoffice/modules/sport/pages/form-configurations/form-configurations.resolver";
import { ListActivitiesComponent } from "@fi-sas/backoffice/modules/sport/pages/list-activities/list-activities.component";
import { ListVenuesComponent } from "@fi-sas/backoffice/modules/sport/pages/list-venues/list-venues.component";
import { ListModalitiesComponent } from "@fi-sas/backoffice/modules/sport/pages/list-modalities/list-modalities.component";
import { ListReservationsComponent } from "@fi-sas/backoffice/modules/sport/pages/list-reservations/list-reservations.component";
import { InitialPageComponent } from "@fi-sas/backoffice/shared/components/initial-page/initial-page.component";

const routes: Routes = [
  {
    path: '',
    component: SportComponent,
    children: [
      {
        path: 'form-modality',
        component: FormModalityComponent ,
        data: { breadcrumb: 'Criar Modalidade', title: 'Criar Modalidade', scope: 'sport:modalities:create' },
        resolve: { }
      },

      {
        path: 'list-modalities',
        component: ListModalitiesComponent ,
        data: { breadcrumb: 'Listar Modalidades', title: 'Listar Modalidades', scope: 'sport:modalities:read' },
        resolve: { }
      },

      {
        path: 'form-venue',
        component: FormVenueComponent ,
        data: { breadcrumb: 'Criar Espaço', title: 'Criar Espaço', scope: 'sport:venues:create' },
        resolve: { }
      },

      {
        path: 'list-venues',
        component: ListVenuesComponent ,
        data: { breadcrumb: 'Listar Espaços', title: 'Listar Espaços', scope: 'sport:venues:read' },
        resolve: { }
      },

      {
        path: 'form-activity',
        component: FormActivityComponent,
        data: { breadcrumb: 'Criar Atividade', title: 'Criar Atividade', scope: 'sport:activities:create' },
        resolve: { }
      },

      {
        path: 'list-activities',
        component: ListActivitiesComponent,
        data: { breadcrumb: 'Listar Atividades', title: 'Listar Atividades', scope: 'sport:activities:read' },
        resolve: { }
      },

      {
        path: 'list-reservations',
        component: ListReservationsComponent,
        data: { breadcrumb: 'Listar Reservas', title: 'Listar Reservas', scope: 'sport:reservations:read' },
        resolve: { }
      },

      {
        path: 'form-configurations',
        component: FormConfigurationsComponent ,
        data: { breadcrumb: 'Configurações', title: 'Configurações', scope: 'sport:configurations:create' },
        resolve: { configurations: FormConfigurationsResolver }
      },

      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SportRoutingModule {}
