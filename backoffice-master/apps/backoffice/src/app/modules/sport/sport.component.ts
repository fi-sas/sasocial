import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-sport',
  template: '<router-outlet></router-outlet>'
})
export class SportComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(24), 'dribbble');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const modalities = new SiderItem('Modalidades', '', '', 'sport:modalities');
    modalities.addChild(new SiderItem('Criar', '', '/sport/form-modality', 'sport:modalities:create'));
    modalities.addChild(new SiderItem('Listar', '', '/sport/list-modalities', 'sport:modalities:read'));
    this.uiService.addSiderItem(modalities);

    const venues = new SiderItem('Espaços', '', '', 'sport:venues');
    venues.addChild(new SiderItem('Criar', '', '/sport/form-venue', 'sport:venues:create'));
    venues.addChild(new SiderItem('Listar', '', '/sport/list-venues', 'sport:venues:read'));
    this.uiService.addSiderItem(venues);

    const activities = new SiderItem('Atividades', '', '', 'sport:activities');
    activities.addChild(new SiderItem('Criar', '', '/sport/form-activity', 'sport:activities:create'));
    activities.addChild(new SiderItem('Listar', '', '/sport/list-activities', 'sport:activities:read'));
    this.uiService.addSiderItem(activities);

    const reservations = new SiderItem('Reservas', '', '', 'sport:reservations');
    reservations.addChild(new SiderItem('Listar', '', '/sport/list-reservations', 'sport:reservations:read'));
    this.uiService.addSiderItem(reservations);

    const configurations = new SiderItem('Configurações','','/sport/form-configurations' ,'sport:configurations:read');
    this.uiService.addSiderItem(configurations);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
