import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SportComponent } from './sport.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SportRoutingModule } from "@fi-sas/backoffice/modules/sport/sport-routing.module";
import { FormModalityComponent } from './pages/form-modality/form-modality.component';
import { FormVenueComponent } from './pages/form-venue/form-venue.component';
import { FormActivityComponent } from './pages/form-activity/form-activity.component';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';
import { FormConfigurationsResolver } from "@fi-sas/backoffice/modules/sport/pages/form-configurations/form-configurations.resolver";
import { ListActivitiesComponent } from './pages/list-activities/list-activities.component';
import { ListVenuesComponent } from './pages/list-venues/list-venues.component';
import { ListModalitiesComponent } from './pages/list-modalities/list-modalities.component';
import { ListReservationsComponent } from './pages/list-reservations/list-reservations.component';
import { ViewReservationComponent } from './pages/view-reservation/view-reservation.component';
import { FilterRecurrencePipe} from "./pipes/filter-recurrence.pipe";

@NgModule({
  declarations: [SportComponent,
                 FormModalityComponent,
                 FormVenueComponent,
                 FormActivityComponent,
                 FormConfigurationsComponent,
                 ListActivitiesComponent,
                 ListVenuesComponent,
                 ListModalitiesComponent,
                 ListReservationsComponent,
                 ViewReservationComponent,
                 FilterRecurrencePipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    SportRoutingModule
  ],
  providers: [
    FormConfigurationsResolver
  ]
})
export class SportModule { }
