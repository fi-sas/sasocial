import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';

import { BackupsService } from '../../services/backups.service';

@Component({
  selector: 'fi-sas-backups',
  templateUrl: './backups.component.html',
  styleUrls: ['./backups.component.less'],
})
export class BackupsComponent implements OnInit {
  loading = false;
  services = [];
  general = null;

  constructor(
    private backupsService: BackupsService,
    private modalService: NzModalService
  ) {}

  ngOnInit() {
    this.loadServices();
  }

  loadServices() {
    this.loading = true;
    this.backupsService
      .getServices()
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.services = result.data;

        this.general = this.services.find((s) => s.name === 'general');
        this.services = this.services.filter((s) => s.name !== 'general');
        this.services.forEach((s) => {
          s.backups_hourly = s.backups_hourly.reverse();
          s.backups_daily = s.backups_daily.reverse();
          s.backups_weekly = s.backups_weekly.reverse();
          s.backups_monthly = s.backups_monthly.reverse();
          s.backups_yearly = s.backups_yearly.reverse();
          s.backups_others = s.backups_others.reverse();
        });
      });
  }

  editing = {
    hourly: false,
    daily: false,
    weekly: false,
    monthly: false,
    yearly: false,
  };
  editable = false;
  activeEdit(service: any) {
    this.editing = {
      hourly: service.hourly,
      daily: service.daily,
      weekly: service.weekly,
      monthly: service.monthly,
      yearly: service.yearly,
    };
    this.editable = true;
  }

  saveEdit(service: any) {
    this.loading = true;
    this.backupsService
      .updateServices(service.name, this.editing)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => {
        service.hourly = this.editing.hourly;
        service.daily = this.editing.daily;
        service.weekly = this.editing.weekly;
        service.monthly = this.editing.monthly;
        service.yearly = this.editing.yearly;
        this.editable = false;
      });
  }

  cancelEdit() {
    this.editable = false;
  }

  backupAll() {
    this.modalService.confirm({
      nzTitle: 'Backup geral',
      nzContent:
        'Pretende fazer uma backup de todos os serviços disponiveis no momento?',
      nzOnOk: () => {
        this.backupsService
          .backupAll()
          .pipe(first())
          .subscribe(() => {
            this.loadServices();
          });
      },
    });
  }

  getStatusColor(status: string) {
    switch (status) {
      case 'IDLE':
        return 'gray';
      case 'WAITING':
        return 'yellow';
      case 'PROCESSING':
        return 'blue';
      case 'DONE':
        return 'green';
      case 'ERROR':
        return 'red';
      default:
        return 'gray';
    }
  }

  getStatusTranslation(status: string) {
    switch (status) {
      case 'IDLE':
        return 'Idle';
      case 'WAITING':
        return 'Em espera';
      case 'PROCESSING':
        return 'A processar';
      case 'DONE':
        return 'Terminado';
      case 'ERROR':
        return 'Erro';
      default:
        return 'Idle';
    }
  }

  getTranslationPeriod(period: string): string {
    switch (period) {
      case 'hourly':
        return 'Hora';
      case 'daily':
        return 'Dia';
      case 'weekly':
        return 'Semana';
      case 'monthly':
        return 'Mês';
      case 'yearly':
        return 'Ano';
      default:
        return 'Outro';
    }
  }

  backupListMode = 'daily';

  backupListByMode(service) {
    if (!service) {
      return [];
    }

    return service['backups_' + this.backupListMode];
  }

  restoreProcess(service: any, backup: any) {
    this.modalService.confirm({
      nzTitle: 'ATENÇÃO!',
      nzContent:
        'O processo que de restauro pode <b>interromper o serviço</b> e causar a <b>perda irreversivel de dados</b>. <br> Têm a certeza que pretende prosseguir ?',
      nzOnOk: () => {
        this.isVisibleRestoreModal = true;
        this.serviceToRestore = service;
        console.log(this.serviceToRestore);
        this.backupToRestore = backup;
      },
    });
  }

  serviceToRestore = null;
  backupToRestore = null;
  restoreModalPassword = null;
  isVisibleRestoreModal = false;
  loadingModal = false;
  handleCancelRestoreModal() {
    this.isVisibleRestoreModal = false;
    this.restoreModalPassword = false;
    this.serviceToRestore = null;
    this.backupToRestore = null;
  }
  handleOkRestoreModal() {
    this.isVisibleRestoreModal = false;

    this.loadingModal = true;
    this.backupsService
      .restoreBackup(
        this.restoreModalPassword,
        this.serviceToRestore.name,
        this.backupToRestore.period,
        this.backupToRestore.path
      )
      .pipe(
        first(),
        finalize(() => (this.loadingModal = false))
      )
      .subscribe((res) => {
        this.handleCancelRestoreModal();
      });
  }
}
