import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { CacheService } from '../../services/cache.service';

@Component({
  selector: 'fi-sas-cache',
  templateUrl: './cache.component.html',
  styleUrls: ['./cache.component.less'],
})
export class CacheComponent implements OnInit {
  loading = false;
  info = null;

  search_loading = false;
  search_term = '';
  keys = [];

  constructor(
    private cacheService: CacheService,
    private modalService: NzModalService
  ) {}

  ngOnInit() {
    this.loadInfo();
  }

  loadInfo() {
    this.loading = true;
    this.cacheService
      .getInfo()
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.info = result.data[0];
      });
  }

  getDBs() {
    if (this.info) {
      const dbKeys = Object.keys(this.info).filter((k) => k.startsWith('db'));

      return dbKeys.map((k) => {
        const rawData = this.info[k];
        let dbInfo = {};
        dbInfo['name'] = k;
        rawData.split(',').forEach((element) => {
          const ele = element.split('=');
          dbInfo[ele[0]] = ele[1];
        });
        return dbInfo;
      });
    }

    return [];
  }

  deleteKey(key: string) {
    this.search_loading = true;
    this.cacheService
      .deleteKeys([key])
      .pipe(
        first(),
        finalize(() => (this.search_loading = false))
      )
      .subscribe((result) => {
        var keyIndex = this.keys.indexOf(key);
        this.keys.splice(keyIndex, 1);
        this.keys = [...this.keys];
      });
  }

  search(search: string) {
    this.search_loading = true;
    this.cacheService
      .getKeys(search)
      .pipe(
        first(),
        finalize(() => (this.search_loading = false))
      )
      .subscribe((result) => {
        this.keys = result.data;
      });
  }

  deleteAll() {
    this.modalService.confirm({
      nzTitle: 'Eliminar tudo',
      nzContent: 'Têm a certeza que pretende eliminar toda a cache ?',
      nzOnOk: () => {
        return new Promise((resolve) => {
          this.cacheService
            .deleteALLKeys()
            .pipe(
              first(),
              finalize(() => resolve())
            )
            .subscribe(() => {
              this.search('');
            });
        });
      },
    });
  }
}
