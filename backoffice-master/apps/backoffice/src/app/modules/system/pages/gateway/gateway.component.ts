import { Component, OnInit } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { GatewayService } from '../../services/gateway.service';

@Component({
  selector: 'fi-sas-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.less']
})
export class GatewayComponent implements OnInit {

  loading = false;
  data = null;

  constructor(
    private gatewayService: GatewayService
  ) { }

  ngOnInit() {
    this.loadAlias();
  }

  loadAlias() {
    this.loading = true;
    this.gatewayService.getAlias().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.data = result.data;
    });
  }

  getColorMethod(method: string) {
    switch(method) {
      case 'GET':
        return 'blue';
      case 'POST':
        return 'green';
      case 'PUT':
        return 'orange';
      case '*':
          return 'purple';
      case 'DELETE':
        return 'red';
      default: 
        return 'gray'
    }
  }

  getColorVisibility(visibility: string) {
    switch(visibility) {
      case 'private':
        return 'orange';
      case 'public':
        return 'green';
      case 'published':
        return 'blue';
      default: 
        return 'gray'
    }
  }

  regenerate() {this.loading = true;
    this.gatewayService.gatewayRegenerate().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      this.loadAlias();
    });
  }

}
