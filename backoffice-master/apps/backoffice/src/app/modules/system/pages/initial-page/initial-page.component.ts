import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fi-sas-initial-page',
  templateUrl: './initial-page.component.html',
  styleUrls: ['./initial-page.component.less']
})
export class InitialPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
