import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class BackupsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  getServices() {
    return this.resourceService.list<any>(
      this.urlService.get('SASOCIAL.BACKUPS', {})
    );
  }

  updateServices(name: string, options: any) {
    return this.resourceService.update<any>(
      this.urlService.get('SASOCIAL.BACKUPS_ID', { id: name }),
      {
        hourly: options.hourly,
        daily: options.daily,
        weekly: options.weekly,
        monthly: options.monthly,
        yearly: options.yearly,
      }
    );
  }

  backupAll() {
    return this.resourceService.create<any>(
      this.urlService.get('SASOCIAL.BACKUPS_ALL', {}),
      {}
    );
  }

  restoreBackup(
    password: string,
    service_name: string,
    period: string,
    filename: string
  ) {
    return this.resourceService.create<any>(
      this.urlService.get('SASOCIAL.BACKUPS_RESTORE', { id: name }),
      {
        password,
        service_name,
        period,
        filename,
      }
    );
  }
}
