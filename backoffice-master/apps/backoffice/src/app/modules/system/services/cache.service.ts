import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class CacheService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  getInfo() {
    return this.resourceService.read<any>(
      this.urlService.get('SASOCIAL.CACHE_INFO', {})
    );
  }

  getKeys(search: string) {
    return this.resourceService.list<any>(
      this.urlService.get('SASOCIAL.CACHE', {}),
      {
        params: new HttpParams().append('search', search),
      }
    );
  }

  deleteKeys(keys: string[]) {
    let params = new HttpParams();

    keys.forEach((k) => {
      params = params.append('keys[]', k);
    });

    return this.resourceService.delete(
      this.urlService.get('SASOCIAL.CACHE_DELETE', {}),
      {
        params,
      }
    );
  }

  deleteALLKeys() {
    return this.resourceService.delete(
      this.urlService.get('SASOCIAL.CACHE_DELETE_ALL', {}),
      {}
    );
  }

  updateServices(name: string, options: any) {
    return this.resourceService.update<any>(
      this.urlService.get('SASOCIAL.BACKUPS_ID', { id: name }),
      {
        hourly: options.hourly,
        daily: options.daily,
        monthly: options.monthly,
        yearly: options.yearly,
      }
    );
  }

  updateService() {}
}
