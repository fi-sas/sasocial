import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class GatewayService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}


  getAlias() {
    return this.resourceService.list<any>(
      this.urlService.get('SASOCIAL.GATEWAY_LIST', {}), {
        params: new HttpParams().append('withActionSchema', 'true')
      }
    );
  }

  gatewayRegenerate() {
    return this.resourceService.create<any>(
      this.urlService.get('SASOCIAL.GATEWAY_REGENERATE', {}), {}
    );
  }
}
