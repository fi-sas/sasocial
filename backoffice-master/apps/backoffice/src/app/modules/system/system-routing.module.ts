import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { BackupsComponent } from './pages/backups/backups.component';
import { CacheComponent } from './pages/cache/cache.component';
import { GatewayComponent } from './pages/gateway/gateway.component';
import { InitialPageComponent } from './pages/initial-page/initial-page.component';
import { SystemComponent } from './system.component';

const routes: Routes = [
  {
    path: '',
    component: SystemComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },

      {
        path: 'backups',
        component: BackupsComponent,
        data: { breadcrumb: 'Backups', title: 'Backups' },
      },
      {
        path: 'cache',
        component: CacheComponent,
        data: { breadcrumb: 'Cache', title: 'Cache' },
      },
      {
        path: 'gateway',
        component: GatewayComponent,
        data: { breadcrumb: 'Gateway', title: 'Gateway' },
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada',
        },
      },
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRoutingModule {}
