import { Component, OnInit } from '@angular/core';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-system',
  template: '<router-outlet></router-outlet>',
})
export class SystemComponent implements OnInit {
  dataConfiguration: any;

  constructor(
    private uiService: UiService,
    private configurationsService: ConfigurationGeralService
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    this.uiService.setSiderTitle(this.validTitleTraductions(31), 'code');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const panel = new SiderItem(
      'Painel',
      '',
      '/system/initial-page',
      'sasocial:system'
    );
    this.uiService.addSiderItem(panel);

    const backups = new SiderItem(
      'Backups',
      '',
      '/system/backups',
      'sasocial:system:backups'
    );
    this.uiService.addSiderItem(backups);

    const cache = new SiderItem(
      'Cache',
      '',
      '/system/cache',
      'sasocial:system:cache'
    );
    this.uiService.addSiderItem(cache);

    const gateway = new SiderItem(
      'Gateway',
      '',
      '/system/gateway',
      'sasocial:system:gateway'
    );
    this.uiService.addSiderItem(gateway);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find((t) => t.id === id) != null &&
      this.dataConfiguration.find((t) => t.id === id) != undefined
      ? this.dataConfiguration.find((t) => t.id === id).translations
      : [];
  }
}
