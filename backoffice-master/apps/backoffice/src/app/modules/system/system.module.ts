import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemComponent } from './system.component';
import { InitialPageComponent } from './pages/initial-page/initial-page.component';
import { BackupsComponent } from './pages/backups/backups.component';
import { SystemRoutingModule } from './system-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CacheComponent } from './pages/cache/cache.component';
import { GatewayComponent } from './pages/gateway/gateway.component';

@NgModule({
  declarations: [SystemComponent, InitialPageComponent, BackupsComponent, CacheComponent, GatewayComponent],
  imports: [CommonModule, SystemRoutingModule, SharedModule],
})
export class SystemModule {}
