import { SubjectsApplicationForm } from './models/application-form.model';
import { ListApplicationFormsComponent } from './pages/list-application-forms/list-application-forms.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormApplicationComponent } from './pages/form-application/form-application.component';
import { FormConfigurationsResolver } from '../ub-configurations/pages/form-configurations/form-configurations.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'all',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Todos', title: 'Todos', scope: 'u_bike:application-forms:read', subject: null },
  },
  {
    path: 'complaints',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Reclamações', title: 'Reclamações', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.COMPLAINT  },
  },
  {
    path: 'absences',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Ausências', title: 'Ausências', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.ABSENCE  },
  },
  {
    path: 'renovations',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Renovações', title: 'Renovações', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.RENOVATION  },
  },
  {
    path: 'maintenance-request',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Pedido de Manutenção', title: 'Pedido de Manutenção', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.MAINTENANCE_REQUEST  },
  },
  {
    path: 'additional-maintenance_kit',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Kit de Manutenção Adicional', title: 'Kit de Manutenção Adicional', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.ADDITIONAL_MAINTENANCE_KIT  },
  },
  {
    path: 'thefts',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Roubos', title: 'Roubos', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.THEFT  },
  },
  {
    path: 'private-maintenance',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Manutenções Privadas', title: 'Manutenções Privadas', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.PRIVATE_MAINTENANCE  },
  },
  {
    path: 'breakdown-communication',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Comunicação de Avarias', title: 'Comunicação de Avarias', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.BREAKDOWN  },
  },
  {
    path: 'withdrawal',
    component: ListApplicationFormsComponent,
    data: { breadcrumb: 'Desistências', title: 'Desistências', scope: 'u_bike:application-forms:read', subject: SubjectsApplicationForm.WITHDRAWAL  },
  },
  {
    path: 'create',
    component: FormApplicationComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'u_bike:applications:create'  },
    resolve: { configurations: FormConfigurationsResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationFormsRoutingModule { }
