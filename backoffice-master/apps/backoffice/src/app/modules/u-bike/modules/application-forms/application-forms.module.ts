import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationFormsRoutingModule } from './application-forms-routing.module';
import { ListApplicationFormsComponent } from './pages/list-application-forms/list-application-forms.component';
import { ViewApplicationFormComponent } from './pages/view-application-form/view-application-form.component';
import { FormApplicationComponent } from './pages/form-application/form-application.component';
import { FormConfigurationsResolver } from '../ub-configurations/pages/form-configurations/form-configurations.resolver';

@NgModule({
  declarations: [
    ListApplicationFormsComponent,
    ViewApplicationFormComponent,
    FormApplicationComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationFormsRoutingModule
  ],
  providers:[
    FormConfigurationsResolver
  ]
})
export class ApplicationFormsModule { }
