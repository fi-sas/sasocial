export class ApplicationFormStatusModel {
  action: string;
  answer: string;
  decision?: string;
  notes: string;
}
