import { ValueModel } from './../../../../bus/models/prices-tables.model';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { HistoryModel } from './history.model';
import { ApplicationModel } from '../../ub-applications/models/application.model';

export class ApplicationFormModel {
  id: number;
  subject: string;
  application_id: number;
  request_description: string;
  file_id: number;
  answer: string;
  answered: boolean;
  read: boolean;
  decision: string;
  status: string;
  begin_date: string;
  end_date: string;
  created_at: string;
  updated_at: string;
  file?: FileModel;
  application?: ApplicationModel;
  history?: HistoryModel;
}

export enum SubjectsApplicationForm {
  THEFT = 'THEFT',
  WITHDRAWAL = 'WITHDRAWAL',
  ABSENCE = 'ABSENCE',
  COMPLAINT = 'COMPLAINT',
  RENOVATION = 'RENOVATION',
  MAINTENANCE_REQUEST = 'MAINTENANCE_REQUEST',
  PRIVATE_MAINTENANCE = 'PRIVATE_MAINTENANCE',
  ADDITIONAL_MAINTENANCE_KIT = 'ADDITIONAL_MAINTENANCE_KIT',
  BREAKDOWN = 'BREAKDOWN'
}

export enum StatusApplicationForm {
  PENDING = 'Pending',
  APPROVED = 'Approved',
  REJECTED = 'Rejected',
  WITHDRAW = 'Withdraw',
}

export const ApplicationFormTagStatus = {
  submitted: { color: 'blue', label: 'Submetido' },
  analysis: { color: 'blue', label: 'Em Análise' },
  approved: { color: 'green', label: 'Aprovado' },
  dispatch: { color: 'orange', label: 'Despacho' },
  rejected: { color: 'red', label: 'Rejeitado' },
  closed: { color: 'gray', label: 'Fechado' },
  withdrawal: { color: 'gray', label: 'Desistiu' },
};

export const ApplicationFormTagDecision = {
  approved: { color: 'green', label: 'Aprovado' },
  rejected: { color: 'red', label: 'Rejeitado' },
};

export const Decision = [
   { value: 'approved', label: 'Aprovado' },
   { value: 'rejected', label: 'Rejeitado' },
];

export const statusMachines = {
  THEFT: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      close: { label: 'Fechar', answer: true, decision: true },
    },
    closed: {},
  },

  WITHDRAWAL: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      dispatch: { label: 'Despacho', answer: true, decision: true },
    },
    dispatch: {
      approve: { label: 'Aprovar',  answer: false, decision: false },
      reject: { label: 'Rejeitar',  answer: false, decision: false},
    },
    approved: {},
  },

  ABSENCE: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      dispatch: { label: 'Despacho', answer: true, decision: true },
    },
    dispatch: {
      approve: { label: 'Aprovar', answer: false, decision: false },
      reject: { label: 'Rejeitar', answer: false, decision: false },
    },
    approved: {},
  },

  COMPLAINT: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      dispatch: { label: 'Despacho', answer: true, decision: true },
    },
    dispatch: {
      approve: { label: 'Aprovar', answer: false, decision: false },
      reject: { label: 'Rejeitar', answer: false, decision: false },
    },
    approved: {},
  },

  RENOVATION: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      dispatch: { label: 'Despacho', answer: true, decision: true },
    },
    dispatch: {
      approve: { label: 'Aprovar', answer: false, decision: false },
      reject: { label: 'Rejeitar', answer: false, decision: false },
    },
    approved: {},
    rejected: {},
  },

  MAINTENANCE_REQUEST: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      approve: { label: 'Aprovar', answer: true, decision: false },
      reject: { label: 'Rejeitar', answer: true, decision: false },
    },
    approved: {},
    rejected: {},
  },

  PRIVATE_MAINTENANCE: {

  },

  BREAKDOWN: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      approve: { label: 'Aprovar', answer: true, decision: false },
      reject: { label: 'Rejeitar', answer: true, decision: false },
    },
    approved: {},
    rejected: {},
  },

  ADDITIONAL_MAINTENANCE_KIT: {
    submitted: {
      analyse: { label: 'Analisar', answer: false, decision: false },
    },
    analysis: {
      approve: { label: 'Aprovar', answer: false, decision: false },
      reject: { label: 'Rejeitar', answer: false, decision: false },
    },
    approved: {},
    rejected: {},
  },
};
