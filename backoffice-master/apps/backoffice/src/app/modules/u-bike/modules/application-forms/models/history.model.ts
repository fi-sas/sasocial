export class HistoryModel {
  id: number;
  application_form_id: number;
  status: string;
  user_id: number;
  notes: string;
  updated_at: string;
  created_at: string;
}
