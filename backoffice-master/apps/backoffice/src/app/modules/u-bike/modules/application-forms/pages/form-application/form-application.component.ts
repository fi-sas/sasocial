import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import * as moment from 'moment';
import { TypologyModel } from '../../../bike-typology/models/typology.model';
import { TypologiesService } from '../../../bike-typology/services/typologies.service';
import { ApplicationModel } from '../../../ub-applications/models/application.model';
import { ApplicationsService } from '../../../ub-applications/services/applications.service';
import { NzModalService } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { finalize, first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';

@Component({
  selector: 'fi-sas-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less'],
})
export class FormApplicationComponent implements OnInit, OnDestroy {
  formApplicationUbike: FormGroup;
  submitted: boolean = false;
  loadingButton: boolean = false;
  step: number = 0;
  defaultTimeDurationOpenValue: Date;
  dataTypology: TypologyModel[] = [];
  sendValue = new ApplicationModel();
  startDate: Date;
  endDate: Date;
  configurations = null;
  organicUnits: OrganicUnitsModel[] = [];
  loadingOrganicUnits: boolean = false;

  buttonStyle = {
    color: 'black',
    height: '24px',
    'text-align': 'center',
  };

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private typologiesService: TypologiesService,
    private applicationsService: ApplicationsService,
    private uiService: UiService,
    private router: Router,
    private modalService: NzModalService,
    private route: ActivatedRoute,
    private organicUnitsService: OrganicUnitsService
  ) {
    this.defaultTimeDurationOpenValue = new Date();
    this.defaultTimeDurationOpenValue.setHours(0, 1);
  }

  disableYear = (current: Date) => {
    return moment(current).isAfter(new Date());
  };
  disableEndDate = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);

    return (
      moment(endDate).isBefore(currentDate) ||
      moment(this.formApplicationUbike.get('start_date').value).isAfter(endDate) ||
      moment(endDate).isAfter(this.endDate) ||
      moment(endDate).isBefore(this.startDate)
    );
  };

  disableStartDate = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);

    return (
      moment(startDate).isBefore(currentDate) ||
      //moment(this.formApplicationUbike.get("start_date").value).isBefore(startDate) ||
      moment(startDate).isAfter(this.endDate) ||
      moment(startDate).isBefore(this.startDate)
    );
  };

  disableEndDate2 = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return (
      moment(endDate).isBefore(currentDate) ||
      moment(this.formApplicationUbike.get('start_date').value).isAfter(endDate)
    );
  };

  disableStartDate2 = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return (
      moment(startDate).isBefore(currentDate) ||
      moment(this.formApplicationUbike.get('end_date').value).isBefore(startDate)
    );
  };

  getOrganitUnit(preferred_pickup_organict_unit_id){
    return this.organicUnits.length ? this.organicUnits.find(ou => ou.id === preferred_pickup_organict_unit_id).name : "";
  }

  ngOnInit() {
    //manufacturer, model, daily_consumption, year_manufactured -> They are mandatory in MS ERROOORRRRRRR!!!

    this.formApplicationUbike = this.formBuilder.group({
      user_id: [null, [Validators.required]],
      daily_commute: ['', [Validators.required]],
      manufacturer: [''],
      model: [''],
      daily_consumption: [''],
      year_manufactured: [''],
      engine_power: [''],
      avg_vehicle_consumption_100: [''],
      daily_kms: ['', [Validators.required, Validators.min(0.01), Validators.max(1000)]],
      number_of_times: ['', [Validators.required, Validators.min(0.01), Validators.max(50)]],
      avg_weekly_distance: ['', [Validators.required, Validators.min(0.01), Validators.max(100000)]],
      total_weekly_distance: ['', [Validators.required, Validators.min(0.01), Validators.max(1000000)]],
      travel_time_school: [null, [Validators.required]],
      travel_time_school_bike: [null, [Validators.required]],
      typology_first_preference_id: ['', [Validators.required]],
      other_typology_preference: [''],
      start_date: ['', [Validators.required]],
      end_date: ['', [Validators.required]],
      weight: ['', [Validators.required, Validators.min(1), Validators.max(400)]],
      height: ['', [Validators.required, Validators.min(1), Validators.max(4)]],
      applicant_consent: ['', [Validators.required]],
      preferred_pickup_organict_unit_id: [null, [Validators.required]],
    });
    this.getListOrganicUnits();

    this.getListTypologies();
    // this.getRegulaments();
    this.getStartEndDate();

    this.handleDependentFields();
    this.formApplicationUbike
      .get('start_date')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => this.controlOverlapDates());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  get f() {
    return this.formApplicationUbike.controls;
  }

  getListTypologies() {
    this.typologiesService
      .list(0, -1)
      .pipe()
      .subscribe((typology) => {
        this.dataTypology = typology.data.filter(t => t.active);
      });
  }

  getListOrganicUnits(){
    this.loadingOrganicUnits = true;
    this.organicUnitsService.list(1, -1).pipe(first(), finalize(() => this.loadingOrganicUnits = false)).subscribe(response => {
      this.organicUnits = response.data;
    })
  }

  // getRegulaments(): void {
  //   this.applicationHomepageService.read().pipe(first()).subscribe(homepage => {
  //     this.regulaments = homepage.data[0];
  //   });
  // }

  scrollTop() {
    window.scrollTo(0, 0);
  }

  controlOverlapDates() {
    const start: Date = new Date(this.formApplicationUbike.get('start_date').value);
    const end: Date = new Date(this.formApplicationUbike.get('end_date').value);
    if (end) {
      if (start > end) {
        this.formApplicationUbike.get('end_date').setValue(null);
      }
    }
  }

  filterTypology(value: number) {
    let name: string;
    this.dataTypology.filter((fil) => {
      if (fil.id == value) {
        name = fil.name;
      }
    });
    return name;
  }

  nextStep(stepNow: number) {
    this.submitted = true;
    switch (stepNow) {
      case 1: {
        if (
          this.fieldsValid([
            'avg_weekly_distance',
            'daily_commute',
            'daily_kms',
            'number_of_times',
            'total_weekly_distance',
            'travel_time_school_bike',
            'travel_time_school',
            'user_id',
          ])
        ) {
          if (
            ['Car', 'Motorcycle'].includes(this.formApplicationUbike.get('daily_commute').value) &&
            !this.fieldsValid([
              'manufacturer',
              'daily_consumption',
              'model',
              'year_manufactured',
              'engine_power',
              'avg_vehicle_consumption_100',
            ])
          ) {
            return;
          }
          this.step = 1;
          if (
            this.formApplicationUbike.get('year_manufactured').value != '' &&
            this.formApplicationUbike.get('year_manufactured').value != undefined &&
            this.formApplicationUbike.get('year_manufactured').value != null
          ) {
            let value = this.formApplicationUbike.get('year_manufactured').value;
            let year = moment(value).format('yyyy');
            this.formApplicationUbike.get('year_manufactured').setValue(year);
          }
          this.submitted = false;
          this.scrollTop();
        }
        break;
      }
      case 2: {
        if (this.fieldsValid(['typology_first_preference_id', 'preferred_pickup_organict_unit_id', 'start_date', 'end_date'])) {
          this.step = 2;
          this.submitted = false;
          if (this.formApplicationUbike.get('other_typology_preference').value == '') {
            this.formApplicationUbike.get('other_typology_preference').setValue(false);
          }

          this.scrollTop();
        }
        break;
      }
      case 3: {
        if (this.fieldsValid(['weight', 'height'])) {
          this.step = 3;
          this.submitted = false;
          this.scrollTop();
        }
        break;
      }
      case 4: {
        if (this.f.applicant_consent.valid && this.f.applicant_consent.value == true) {
          this.step = 4;
          this.submitted = false;
          this.scrollTop();
        }
        break;
      }
    }
  }

  submit() {
    this.formatValues();
    if (this.formApplicationUbike.valid) {
      this.modalService.confirm({
        nzTitle: 'Deseja submeter a candidatura?',
        nzOnOk: () => this.callSubmit(),
      });
    }
  }

  callSubmit() {
    this.loadingButton = true;
    this.applicationsService
      .create(this.sendValue)
      .pipe(
        first(),
        finalize(() => (this.loadingButton = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Candidatura submetida com sucesso');
        this.router.navigateByUrl('/u-bike/applications/list');
      });
  }

  transformHourToMin(hour: string): number {
    let minutes: number;
    let hourString = moment(hour).format('HH:mm').toString();
    let hourSplitter = hourString.split(':');
    if (hourSplitter[0] == '00') {
      minutes = Number(hourSplitter[1]);
    } else {
      minutes = Number(hourSplitter[0]) * 60 + Number(hourSplitter[1]);
    }
    return minutes;
  }

  formatMinutes(value: string): string {
    let hourString = moment(value).format('HH:mm').toString();
    let hourSplitter = hourString.split(':');
    return (value = `${hourSplitter[0]}h${hourSplitter[1]}m`);
  }

  formatValues() {
    this.sendValue.applicant_consent = this.f.applicant_consent.value;
    this.sendValue.daily_commute = this.f.daily_commute.value;
    if (this.sendValue.daily_commute === 'Car' || this.sendValue.daily_commute === 'Motorcycle') {
      this.sendValue.avg_vehicle_consumption_100 = Number(this.f.avg_vehicle_consumption_100.value.replace(',', '.'));
      this.sendValue.engine_power = Number(this.f.engine_power.value.replace(',', '.'));
      this.sendValue.year_manufactured = Number(this.f.year_manufactured.value);
      this.sendValue.daily_consumption = this.f.daily_consumption.value;
      this.sendValue.manufacturer = this.f.manufacturer.value;
      this.sendValue.model = this.f.model.value;
    }
    this.sendValue.avg_weekly_distance = Number(this.f.avg_weekly_distance.value.replace(',', '.'));
    this.sendValue.daily_kms = Number(this.f.daily_kms.value.replace(',', '.'));
    this.sendValue.end_date = this.f.end_date.value;
    this.sendValue.height = Number(this.f.height.value.replace(',', '.'));
    this.sendValue.number_of_times = Number(this.f.number_of_times.value);
    this.sendValue.other_typology_preference = this.f.other_typology_preference.value;
    this.sendValue.start_date = this.f.start_date.value;
    this.sendValue.total_weekly_distance = Number(this.f.total_weekly_distance.value.replace(',', '.'));
    this.sendValue.travel_time_school = this.transformHourToMin(this.f.travel_time_school_bike.value);
    this.sendValue.travel_time_school_bike = this.transformHourToMin(this.f.travel_time_school_bike.value);
    this.sendValue.typology_first_preference_id = this.f.typology_first_preference_id.value;
    this.sendValue.weight = Number(this.f.weight.value.replace(',', '.'));
    this.sendValue.user_id = this.f.user_id.value;
    this.sendValue.preferred_pickup_organict_unit_id = this.f.preferred_pickup_organict_unit_id.value;
  }

  getStartEndDate() {
    this.configurations = this.route.snapshot.data.configurations;

    if (this.configurations !== null) {
      this.startDate  =this.configurations.application_start_date[0] ? new Date(this.configurations.application_start_date[0]) : null;
        this.endDate = this.configurations.application_end_date[0] ?new Date( this.configurations.application_end_date[0]) : null;
    }
  }

  disabledDurationMinutes(hour: number): number[] {
    if (!hour) {
      return [0];
    }
    return [];
  }

  private handleDependentFields() {
    const carMotorcycleFields: { key: string; validators: ValidatorFn[] }[] = [
      { key: 'manufacturer', validators: [trimValidation, Validators.required] },
      { key: 'model', validators: [trimValidation, Validators.required] },
      { key: 'daily_consumption', validators: [Validators.required] },
      { key: 'year_manufactured', validators: [Validators.required] },
      { key: 'engine_power', validators: [Validators.min(1), Validators.max(1000)] },
      { key: 'avg_vehicle_consumption_100', validators: [Validators.min(1), Validators.max(1000)] },
    ];

    this.formApplicationUbike
      .get('daily_commute')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value: string) => {
        const isCarMotorcycle = ['Car', 'Motorcycle'].includes(value);
        carMotorcycleFields.forEach((field) => {
          const fc = this.formApplicationUbike.get(field.key);
          if (fc) {
            if (isCarMotorcycle) {
              fc.setValidators(field.validators);
            } else {
              fc.clearValidators();
            }
            fc.updateValueAndValidity();
          }
        });
      });
  }

  private fieldsValid(fields: string[]): boolean {
    let valid = true;
    fields.forEach((fieldName: string) => {
      const fc = this.formApplicationUbike.get(fieldName);
      valid = valid && fc.valid;
      fc.markAsDirty({ onlySelf: true });
      fc.updateValueAndValidity({ onlySelf: true });
    });
    return valid;
  }
}
