import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApplicationFormsComponent } from './list-application-forms.component';

describe('ListApplicationFormsComponent', () => {
  let component: ListApplicationFormsComponent;
  let fixture: ComponentFixture<ListApplicationFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApplicationFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
