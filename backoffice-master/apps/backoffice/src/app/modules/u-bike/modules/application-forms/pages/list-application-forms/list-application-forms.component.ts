import { hasOwnProperty } from 'tslint/lib/utils';
import {
  ApplicationFormTagStatus,
  ApplicationFormModel,
  statusMachines,
  ApplicationFormTagDecision,
  Decision,
} from './../../models/application-form.model';
import { SubjectsApplicationForm } from '../../models/application-form.model';
import { ApplicationFormsService } from './../../services/application-forms.service';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import * as _ from 'lodash';
@Component({
  selector: 'fi-sas-list-application-forms',
  templateUrl: './list-application-forms.component.html',
  styleUrls: ['./list-application-forms.component.less'],
})
export class ListApplicationFormsComponent extends TableHelper
  implements OnInit {
  subjects = SubjectsApplicationForm;

  subject: string = null;

  status = ApplicationFormTagStatus;

  decisions = Decision;

  statusToFilter = [];
  status_loading = false;

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalApplicationForm: ApplicationFormModel = null;
  changeStatusModalActions = {};
  changeStatusModalApplicationAction = null;
  changeStatusModalNotes = ' ';
  changeStatusModalAnswer = null;
  changeStatusModalDecision = null;
  changeStatusModalLoading = false;

  changeStatusModalNotesBool = false;
  changeStatusModalAnswerBool = false;
  changeStatusModalDecisionBool = false;

  answerEnable = false;
  decisionEnable = false;

  constructor(
    activatedRoute: ActivatedRoute,
    router: Router,
    uiService: UiService,
    public applicationFormsService: ApplicationFormsService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.status_loading = true;
    this.persistentFilters['withRelated'] = 'application,history,user';

    this.activatedRoute.data.subscribe((data) => {
      this.subject = data.subject ? data.subject : null;
      if (this.subject !== null) {
        this.persistentFilters['subject'] = this.subject;
        this.subjectStatus(this.subject);
      }
    });

    this.columns.push(
      {
        key: 'application.full_name',
        label: 'Utilizador',
        sortable: false,
      },
      {
        key: 'decision',
        label: 'Decisão',
        tag: ApplicationFormTagDecision,
        sortable: true,
      },
      {
        key: 'read',
        label: 'Lida',
        tag: {
          true: { label: 'Sim', color: 'green' },
          false: { label: 'Não', color: 'red' },
        },
        sortable: true,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: ApplicationFormTagStatus,
        sortable: true,
      }
    );

    this.initTableData(this.applicationFormsService);
  }

  subjectStatus(subject: string) {
    this.statusToFilter = [];

    const statusSubj = Object.keys(statusMachines[subject]);

    statusSubj.forEach((sts) => {
      if (this.status[sts] !== undefined) {
        this.statusToFilter.push({ label: this.status[sts].label, value: sts });
      }
    });

    this.status_loading = false;
  }

  changeStateModal(applicationForm: ApplicationFormModel, disabled: boolean) {
    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalApplicationForm = applicationForm;
      this.changeStatusModalActions =
        statusMachines[applicationForm.subject][applicationForm.status];
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalApplicationForm = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalApplicationAction = null;
    this.changeStatusModalNotes = '';
    this.answerEnable = false;
    this.decisionEnable = false;
  }

  changeStatusModalActiveAction(action: string) {
    if (!_.isEmpty(this.changeStatusModalActions)) {
      if (hasOwnProperty(this.changeStatusModalActions[action], 'answer')) {
        this.answerEnable = this.changeStatusModalActions[action].answer;
      }

      if (hasOwnProperty(this.changeStatusModalActions[action], 'decision')) {
        this.decisionEnable = this.changeStatusModalActions[action].decision;
      }
    }
    this.changeStatusModalApplicationAction = action;
  }

  handleChangeStatusOk() {
    let statusObj: any = {
      notes: this.changeStatusModalNotes,
    };

    if (this.changeStatusModalAnswer) {
      this.changeStatusModalAnswer = this.changeStatusModalAnswer.trim();
    }
    if (this.changeStatusModalNotes) {
      this.changeStatusModalNotes = this.changeStatusModalNotes.trim();
    }

    if (this.decisionEnable) {
      statusObj = Object.assign(
        { decision: this.changeStatusModalDecision },
        statusObj
      );
      if (!this.changeStatusModalDecision) {
        this.uiService.showMessage(
          MessageType.error,
          'Preencha a decisão'
        );
        return;
      }
    }
    if (this.answerEnable) {
      statusObj = Object.assign(
        { answer: this.changeStatusModalAnswer },
        statusObj
      );
      if (!this.changeStatusModalAnswer) {
        this.uiService.showMessage(
          MessageType.error,
          'Preencha a resposta'
        );
        return;
      }
    }

    this.changeStatusModalLoading = true;

    if (this.changeStatusModalApplicationAction === 'approve') {
      this.applicationFormsService
        .approve(this.changeStatusModalApplicationForm.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe(() => {
          this.searchData();

          this.handleChangeStatusCancel();
        });
    } else if (this.changeStatusModalApplicationAction === 'reject') {
      this.applicationFormsService
        .reject(this.changeStatusModalApplicationForm.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe(() => {
          this.searchData();

          this.handleChangeStatusCancel();
        });
    } else {
      statusObj = Object.assign(
        { action: this.changeStatusModalApplicationAction },
        statusObj
      );
      this.applicationFormsService
        .status(this.changeStatusModalApplicationForm.id, statusObj)
        .pipe(
          first(),
          finalize(() => (this.changeStatusModalLoading = false))
        )
        .subscribe(() => {
          this.searchData();

          this.handleChangeStatusCancel();
        });
    }
  }

  deleteApplicationForm(id: number) {
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar?', 'Eliminar')
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.applicationFormsService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Eliminado com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.status = null;
    this.filters.read = null;
    this.searchData(true)
  }
}
