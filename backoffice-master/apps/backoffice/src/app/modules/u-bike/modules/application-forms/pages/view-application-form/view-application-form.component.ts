import { Component, OnInit, Input } from '@angular/core';
import { ApplicationFormModel, ApplicationFormTagStatus } from '../../models/application-form.model';

@Component({
  selector: 'fi-sas-view-application-form',
  templateUrl: './view-application-form.component.html',
  styleUrls: ['./view-application-form.component.less']
})
export class ViewApplicationFormComponent implements OnInit {

  @Input() applicationForm: ApplicationFormModel = null;

  ApplicationFormTagStatus = ApplicationFormTagStatus;

  constructor() { }

  ngOnInit() {}

}
