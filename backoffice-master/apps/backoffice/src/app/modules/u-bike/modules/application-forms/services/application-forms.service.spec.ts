import { TestBed } from '@angular/core/testing';

import { ApplicationFormsService } from './application-forms.service';

describe('ApplicationFormsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationFormsService = TestBed.get(ApplicationFormsService);
    expect(service).toBeTruthy();
  });
});
