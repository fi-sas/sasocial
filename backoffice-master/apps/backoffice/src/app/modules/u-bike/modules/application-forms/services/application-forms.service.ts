import { ApplicationFormModel } from '../models/application-form.model';
import { Injectable } from '@angular/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ApplicationFormStatusModel } from '../models/application-form-status.model';

@Injectable({
  providedIn: 'root',
})
export class ApplicationFormsService extends Repository<ApplicationFormModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.APPLICATION_FORMS';
    this.entity_url = 'U_BIKE.APPLICATION_FORMS_ID';
  }

  status(
    id: number,
    applicationForm_status: ApplicationFormStatusModel
  ): Observable<Resource<ApplicationFormModel>> {
    return this.resourceService.create<ApplicationFormModel>(
      this.urlService.get('U_BIKE.APPLICATION_FORMS_ID_STATUS', { id }),
      applicationForm_status
    );
  }

  approve(
    id: number,
    applicationForm_status: ApplicationFormStatusModel
  ): Observable<Resource<ApplicationFormModel>> {
    return this.resourceService.create<ApplicationFormModel>(
      this.urlService.get('U_BIKE.APPLICATION_FORMS_APPROVE', { id }),
      applicationForm_status
    );
  }

  reject(
    id: number,
    applicationForm_status: ApplicationFormStatusModel
  ): Observable<Resource<ApplicationFormModel>> {
    return this.resourceService.create<ApplicationFormModel>(
      this.urlService.get('U_BIKE.APPLICATION_FORMS_REJECT', { id }),
      applicationForm_status
    );
  }

  delete(id: number): Observable<any> {
    return this.resourceService.delete(
      this.urlService.get('U_BIKE.APPLICATION_FORMS_ID', { id })
    );
  }
}
