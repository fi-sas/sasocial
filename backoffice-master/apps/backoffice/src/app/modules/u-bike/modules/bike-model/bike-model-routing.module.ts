import { FormBikeModelComponent } from './pages/form-bike-model/form-bike-model.component';
import { ListBikeModelComponent } from './pages/list-bike-model/list-bike-model.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormBikeModelComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar',
      scope: 'u_bike:bike_models:create',
    },
  },
  {
    path: 'edit/:id',
    component: FormBikeModelComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar',
      scope: 'u_bike:bike_models:update',
    },
  },
  {
    path: 'list',
    component: ListBikeModelComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar',
      scope: 'u_bike:bike_models:read',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BikeModelRoutingModule {}
