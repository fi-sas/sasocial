import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BikeModelRoutingModule } from './bike-model-routing.module';
import { ListBikeModelComponent } from './pages/list-bike-model/list-bike-model.component';
import { FormBikeModelComponent } from './pages/form-bike-model/form-bike-model.component';
import { ViewBikeModelComponent } from './pages/view-bike-model/view-bike-model.component';

@NgModule({
  declarations: [
    ListBikeModelComponent,
    FormBikeModelComponent,
    ViewBikeModelComponent,
  ],
  imports: [CommonModule, SharedModule, BikeModelRoutingModule],
})
export class BikeModelModule {}
