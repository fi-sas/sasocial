import { FileModel } from './../../../../medias/models/file.model';
import { TypologyModel } from "../../bike-typology/models/typology.model";
import { Bike } from '../../bike/models/bike.model';

export class BikeModel {
  id: number;
  model: string;
  model_conformity_file_id: number;
  brakes: string;
  brake_types: string;
  frame_type: string;
  front_suspension: string;
  tyres: string;
  seat_fixation_mechanism: string;
  front_wheel_fixation_mechanism: string;
  back_wheel_fixation_mechanism: string;
  handlebar_fixation_mechanism: string;
  gear_mechanism: string;
  chain_guard: string;
  front_illumination: string;
  back_illumination: string;
  side_reflections: string;
  pedal_reflections: string;
  front_reflections: string;
  back_reflections: string;
  sound_warning: string;
  instruction_manual: string;
  rest: string;
  mudguards: string;
  luggage_support: string;
  monitoring_system: string;
  coating_requirements: string;
  other_accessories: string;
  typology_id: number;
  created_at: string;
  updated_at: string;
  bikes?: Bike[];
  model_conformity_file?: FileModel;
  typology?: TypologyModel;
}

