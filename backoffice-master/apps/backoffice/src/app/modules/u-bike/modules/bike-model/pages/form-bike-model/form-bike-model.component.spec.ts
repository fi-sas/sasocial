import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBikeModelComponent } from './form-bike-model.component';

describe('FormBikeModelComponent', () => {
  let component: FormBikeModelComponent;
  let fixture: ComponentFixture<FormBikeModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBikeModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBikeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
