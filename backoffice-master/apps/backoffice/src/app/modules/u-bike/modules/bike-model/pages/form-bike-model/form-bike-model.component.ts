import { TypologyModel } from './../../../bike-typology/models/typology.model';
import { BikesModelService } from './../../services/bike-model.service';
import { TypologiesService } from './../../../bike-typology/services/typologies.service';
import { BikeModel } from './../../models/bike-model.model';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { first, finalize } from 'rxjs/operators';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";


@Component({
  selector: 'fi-sas-form-bike-model',
  templateUrl: './form-bike-model.component.html',
  styleUrls: ['./form-bike-model.component.less']
})
export class FormBikeModelComponent implements OnInit {

  bikeModel: BikeModel = null

  idToUpdate = null;

  bikeModelLoading = false;
  typologies_loading = false;

  typologies: TypologyModel[]= []

  bikeModelForm;

  constructor(
    private fb: FormBuilder,
    private typologiesService: TypologiesService,
    private bikesModelService: BikesModelService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activateRoute: ActivatedRoute
  ) {
    this.bikeModelForm = this.fb.group({
      model: new FormControl(null, [Validators.required, trimValidation]),
      brakes: new FormControl(null, [Validators.required, trimValidation]),
      brake_types: new FormControl(null, [Validators.required, trimValidation]),
      frame_type: new FormControl(null, [Validators.required, trimValidation]),
      front_suspension: new FormControl(null, [Validators.required, trimValidation]),
      tyres: new FormControl(null, [Validators.required, trimValidation]),
      seat_fixation_mechanism: new FormControl(null, [Validators.required, trimValidation]),
      front_wheel_fixation_mechanism: new FormControl(null, [Validators.required, trimValidation]),
      back_wheel_fixation_mechanism: new FormControl(null, [Validators.required, trimValidation]),
      handlebar_fixation_mechanism: new FormControl(null, [Validators.required, trimValidation]),
      gear_mechanism: new FormControl(null, [Validators.required, trimValidation]),
      chain_guard: new FormControl(null, [Validators.required, trimValidation]),
      front_illumination: new FormControl('', [Validators.required, trimValidation]),
      back_illumination: new FormControl(null, [Validators.required, trimValidation]),
      side_reflections: new FormControl(null, [Validators.required, trimValidation]),
      pedal_reflections: new FormControl(null, [Validators.required, trimValidation]),
      front_reflections: new FormControl(null, [Validators.required, trimValidation]),
      back_reflections: new FormControl(null, [Validators.required, trimValidation]),
      sound_warning: new FormControl(null, [Validators.required, trimValidation]),
      model_conformity_file_id: new FormControl(null, [Validators.required]),
      instruction_manual: new FormControl(null, [Validators.required, trimValidation]),
      rest: new FormControl(null, [Validators.required, trimValidation]),
      mudguards: new FormControl(null, [Validators.required, trimValidation]),
      luggage_support: new FormControl(null, [Validators.required, trimValidation]),
      monitoring_system: new FormControl(null, [Validators.required]),
      coating_requirements: new FormControl(null, [Validators.required, trimValidation]),
      other_accessories: new FormControl(' ', [trimValidation]),
      typology_id: new FormControl(null, [Validators.required]),
    }
    );
  }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.bikesModelService.read(parseInt(data.get('id'), 10), {
        }).subscribe(result => {
          this.bikeModel = result.data[0];
          this.bikeModelForm.patchValue({
            ...result.data[0],
          });
        }, () => {
          this.location.back();
        });
      }
    });
    this.loadTypologies();
  }

  loadTypologies() {
    this.typologies_loading = true;
    this.typologiesService.list(1, -1).pipe(
      first(),
      finalize(() => this.typologies_loading = false)
    ).subscribe(results => {
      this.typologies = results.data.filter(typology => typology.active);
    });
  }


  submit() {
    const formValue = { ...this.bikeModelForm.value };
    if (this.bikeModelForm.valid) {
      this.bikeModelLoading = true;

      if(this.bikeModel) {
        this.bikesModelService.patch(this.idToUpdate, formValue).pipe(
          first(),
          finalize(() => this.bikeModelLoading = false)
        ).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Modelo de bicicleta alterado com sucesso.');
          this.location.back();
        });
      } else  {
        this.bikesModelService.create(formValue).pipe(
          first(),
          finalize(() => this.bikeModelLoading = false)
        ).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Modelo de bicicleta inserido com sucesso.');
          this.router.navigate(['u-bike', 'bike-model', 'list']);
        });
      }
    } else {

      for (const i in this.bikeModelForm.controls) {
        if (this.bikeModelForm.controls[i]) {
          this.bikeModelForm.controls[i].markAsDirty();
          this.bikeModelForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton() {
    this.router.navigate(['u-bike', 'bike-model', 'list']);
  }

}
