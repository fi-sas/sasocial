import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBikeModelComponent } from './list-bike-model.component';

describe('ListBikeModelComponent', () => {
  let component: ListBikeModelComponent;
  let fixture: ComponentFixture<ListBikeModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBikeModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBikeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
