import { TypologyModel } from './../../../bike-typology/models/typology.model';
import { TypologiesService } from './../../../bike-typology/services/typologies.service';
import { BikesModelService } from './../../services/bike-model.service';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-bike-model',
  templateUrl: './list-bike-model.component.html',
  styleUrls: ['./list-bike-model.component.less'],
})
export class ListBikeModelComponent extends TableHelper implements OnInit {

  typologies_loading = false;
  typologies: TypologyModel[] = [];

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private typologiesService: TypologiesService ,
    public bikesModelService: BikesModelService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'model',
        label: 'Modelo',
        sortable: true,
      },
      {
        key: 'typology',
        label: 'Tipologia',
        sortable: true
      }
    );

    this.persistentFilters = {
      withRelated: 'typology,model_conformity_file',
      searchFields: 'model'
    }

    this.initTableData(this.bikesModelService);
    this.loadTypologies();
  }

  loadTypologies() {
    this.typologies_loading = true;
    this.typologiesService.list(1, -1).pipe(
      first(),
      finalize(() => this.typologies_loading = false)
    ).subscribe(results => {
      this.typologies = results.data;
    });
  }

  deleteBikeModel(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar modelo de bibicleta',
        'Pretende eliminar este modelo de bibicleta ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.bikesModelService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Modelo de bicicleta eliminado com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.typology_id = null;
    this.searchData(true)
  }
}
