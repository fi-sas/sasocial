import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBikeModelComponent } from './view-bike-model.component';

describe('ViewBikeModelComponent', () => {
  let component: ViewBikeModelComponent;
  let fixture: ComponentFixture<ViewBikeModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBikeModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBikeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
