import { BikeModel } from './../../models/bike-model.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-bike-model',
  templateUrl: './view-bike-model.component.html',
  styleUrls: ['./view-bike-model.component.less']
})
export class ViewBikeModelComponent implements OnInit {

  @Input() data: BikeModel = null;

  constructor() { }

  ngOnInit() {}

}
