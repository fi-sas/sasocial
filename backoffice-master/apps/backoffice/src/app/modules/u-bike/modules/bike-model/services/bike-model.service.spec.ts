import { TestBed } from '@angular/core/testing';
import { BikesModelService } from './bike-model.service';

describe('BikeModelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BikesModelService = TestBed.get(BikesModelService);
    expect(service).toBeTruthy();
  });
});
