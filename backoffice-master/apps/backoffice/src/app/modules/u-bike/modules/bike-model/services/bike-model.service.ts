import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { BikeModel } from '../models/bike-model.model';

@Injectable({
  providedIn: 'root',
})
export class BikesModelService extends Repository<BikeModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.BIKE_MODEL';
    this.entity_url = 'U_BIKE.BIKE_MODEL_ID';
  }
}
