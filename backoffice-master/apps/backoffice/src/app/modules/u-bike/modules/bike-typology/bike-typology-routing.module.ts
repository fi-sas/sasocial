import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'u_bike:typologies:create' },
  },
  {
    path: 'edit/:id',
    component: FormTypologyComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'u_bike:typologies:update' },
  },
  {
    path: 'list',
    component: ListTypologiesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'u_bike:typologies:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeTypologyRoutingModule { }
