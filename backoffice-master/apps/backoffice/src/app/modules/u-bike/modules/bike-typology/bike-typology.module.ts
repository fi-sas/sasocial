import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BikeTypologyRoutingModule } from './bike-typology-routing.module';
import { ListTypologiesComponent } from './pages/list-typologies/list-typologies.component';
import { FormTypologyComponent } from './pages/form-typology/form-typology.component';

@NgModule({
  declarations: [
    ListTypologiesComponent,
    FormTypologyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BikeTypologyRoutingModule
  ]
})
export class BikeTypologyModule { }
