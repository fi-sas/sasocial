export class TypologyModel {
  id?: number;
  name: string;
  active: boolean;
  updated_at?: string;
  created_at?: string;
}