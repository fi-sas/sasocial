import { TypologiesService } from './../../services/typologies.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { Location } from '@angular/common';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";

@Component({
  selector: 'fi-sas-form-typology',
  templateUrl: './form-typology.component.html',
  styleUrls: ['./form-typology.component.less']
})
export class FormTypologyComponent implements OnInit {

  typologyForm;
  loading = false;
  idToUpdate = null;

  constructor(
    private fb: FormBuilder,
    public typologiesService: TypologiesService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
  ) {
    this.typologyForm = this.fb.group({
      name: new FormControl('', [Validators.required, trimValidation]),
      active: new FormControl(true, [Validators.required]),
    });
  }

  ngOnInit() {
    this.idToUpdate = this.activatedRoute.snapshot.params.hasOwnProperty('id') ? this.activatedRoute.snapshot.params.id : null;
    if (this.idToUpdate) {
      this.typologiesService.read(this.idToUpdate).pipe(first()).subscribe(result => {
        this.typologyForm.patchValue({
          name: result.data[0].name,
          active: result.data[0].active,
        });
      });
    }
  }

  submitForm(event: any, value: any) {

    if (this.typologyForm.valid) {
      this.loading = true;
      if (this.idToUpdate) {
        this.typologiesService.update(this.idToUpdate, value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Tipologia atualizada com sucesso');
          this.location.back();
        });
      } else {
        this.typologiesService.create(value).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Tipologia criada com sucesso');
          this.router.navigate(['u-bike', 'typologies', 'list']);
        });
      }
    } else {

      for (const i in this.typologyForm.controls) {
        if (this.typologyForm.controls[i]) {
          this.typologyForm.controls[i].markAsDirty();
          this.typologyForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton() {
    this.router.navigate(['u-bike', 'typologies', 'list']);
  }

}
