import { TypologiesService } from './../../services/typologies.service';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-list-typologies',
  templateUrl: './list-typologies.component.html',
  styleUrls: ['./list-typologies.component.less'],
})
export class ListTypologiesComponent extends TableHelper implements OnInit {
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public typologiesService: TypologiesService
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      searchFields: "name"
  }
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        sortable: true,
        tag: TagComponent.YesNoTag,
      }
    );

    this.initTableData(this.typologiesService);
  }

  deleteTypology(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar tipologia',
        'Pretende eliminar esta tipologia ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.typologiesService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Tipologia eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.active = null;
    this.searchData(true)
  }
}
