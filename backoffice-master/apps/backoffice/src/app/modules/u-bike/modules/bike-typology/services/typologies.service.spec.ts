import { TestBed, inject } from '@angular/core/testing';

import { TypologiesService } from './typologies.service';

describe('TypologiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypologiesService]
    });
  });

  it('should be created', inject([TypologiesService], (service: TypologiesService) => {
    expect(service).toBeTruthy();
  }));
});
