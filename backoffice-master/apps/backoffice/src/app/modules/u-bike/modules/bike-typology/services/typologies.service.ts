import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { TypologyModel } from '../models/typology.model';

@Injectable({
  providedIn: 'root'
})
export class TypologiesService extends Repository<TypologyModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.BIKE_TYPOLOGIES';
    this.entity_url = 'U_BIKE.BIKE_TYPOLOGIES_ID';
  }
}
