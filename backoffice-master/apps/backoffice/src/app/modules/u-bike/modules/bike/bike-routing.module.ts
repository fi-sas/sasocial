import { ListBikesComponent } from './pages/list-bikes/list-bikes.component';
import { FormBikeComponent } from './pages/form-bike/form-bike.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'create',
    component: FormBikeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'u_bike:bikes:create' },
  },
  {
    path: 'edit/:id',
    component: FormBikeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'u_bike:bikes:update' },
  },
  {
    path: 'list',
    component: ListBikesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'u_bike:bikes:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeRoutingModule { }
