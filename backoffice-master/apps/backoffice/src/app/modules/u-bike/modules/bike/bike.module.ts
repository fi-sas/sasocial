import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BikeRoutingModule } from './bike-routing.module';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ListBikesComponent } from './pages/list-bikes/list-bikes.component';
import { FormBikeComponent } from './pages/form-bike/form-bike.component';
import { ViewBikeComponent } from './pages/view-bike/view-bike.component';

@NgModule({
  declarations: [ListBikesComponent, FormBikeComponent, ViewBikeComponent],
  imports: [
    CommonModule,
    SharedModule,
    BikeRoutingModule
  ]
})
export class BikeModule { }
