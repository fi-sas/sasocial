import { BikeModel } from "../../bike-model/models/bike-model.model";
import { AssetModel } from "@fi-sas/backoffice/modules/infrastructure/models/asset.model";

export class Bike {
  id: number;
  identification: string;
  size: string;
  bike_model_id: number;
  asset_id: number;
  created_at: string;
  updated_at: string;
  bike_model?: BikeModel;
  asset?: AssetModel;
}
