import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBikeComponent } from './form-bike.component';

describe('FormBikeComponent', () => {
  let component: FormBikeComponent;
  let fixture: ComponentFixture<FormBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
