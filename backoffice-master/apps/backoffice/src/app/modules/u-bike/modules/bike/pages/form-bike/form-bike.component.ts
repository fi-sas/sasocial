import { BikeModel } from './../../../bike-model/models/bike-model.model';
import { BikesService } from './../../services/bikes.service';
import { Bike } from './../../models/bike.model';
import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { BikesModelService } from '../../../bike-model/services/bike-model.service';
import { Location } from '@angular/common';
import { finalize, first, debounceTime, switchMap, map} from 'rxjs/operators';
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { AssetModel } from '@fi-sas/backoffice/modules/infrastructure/models/asset.model';
import { AssetsService } from '@fi-sas/backoffice/modules/infrastructure/services/assets.service';

@Component({
  selector: 'fi-sas-form-bike',
  templateUrl: './form-bike.component.html',
  styleUrls: ['./form-bike.component.less'],
})
export class FormBikeComponent implements OnInit {
  bike: Bike = null;

  idToUpdate = null;

  bikeLoading = false;

  sizes = null;

  bikes_models_loading = false;
  bikeModels: BikeModel[] = [];

  bikeForm;
  assetsSearchChange$ = new BehaviorSubject('');
  assets: AssetModel[] = [];
  isLoadingAssets = false;

  constructor(
    private fb: FormBuilder,
    private bikesService: BikesService,
    private bikesModelService: BikesModelService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activateRoute: ActivatedRoute,
    private assetsService: AssetsService,
  ) {
    this.bikeForm = this.fb.group({
      identification: new FormControl(null, [Validators.required, trimValidation]),
      size: new FormControl(null, [Validators.required,trimValidation]),
      bike_model_id: new FormControl(null, [Validators.required]),
      asset_id: new FormControl(null),
    });
  }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe((data) => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.bikesService.read(parseInt(data.get('id'), 10), {}).subscribe(
          (result) => {
            this.bike = result.data[0];
            this.bikeForm.patchValue({
              ...result.data[0],
            });
          },
          () => {
            this.location.back();
          }
        );
      }
    });
    this.loadBikeModels();
    const loadAssets = (search: string) =>
      this.assetsService.list(1, 20, null, null, {
        withRelated: false,
        active:true,
        search,
        searchFields: 'designation,code'
      }).pipe(first(), map((results) => results.data));


    const optionAssets$: Observable<AssetModel[]> = this.assetsSearchChange$
      .asObservable()
      .pipe(debounceTime(500))
      .pipe(switchMap(loadAssets));
      optionAssets$.subscribe(assets => {
      this.assets = assets;
      this.isLoadingAssets = false;
    });
  }

  onSearchAssets(event) {
    this.isLoadingAssets = true;
    this.assetsSearchChange$.next(event);
  }

  loadBikeModels() {
    this.bikes_models_loading = true;
    this.bikesModelService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.bikes_models_loading = false))
      )
      .subscribe((results) => {
        this.bikeModels = results.data;
      });
  }

  submit() {
    const formValue = { ...this.bikeForm.value };
    if (this.bikeForm.valid) {
      this.bikeLoading = true;
      formValue.size = formValue.size + '';
      if (this.bike) {
        this.bikesService
          .patch(this.idToUpdate, formValue)
          .pipe(
            first(),
            finalize(() => (this.bikeLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Bicicleta alterada com sucesso.'
            );
            this.location.back();
          });
      } else {
        this.bikesService
          .create(formValue)
          .pipe(
            first(),
            finalize(() => (this.bikeLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              'Bicicleta inserida com sucesso.'
            );
            this.router.navigate(['u-bike', 'bike', 'list']);
          });
      }
    } else {
      for (const i in this.bikeForm.controls) {
        if (this.bikeForm.controls[i]) {
          this.bikeForm.controls[i].markAsDirty();
          this.bikeForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  returnButton(){
    this.router.navigate(['u-bike', 'bike', 'list']);
  }

}
