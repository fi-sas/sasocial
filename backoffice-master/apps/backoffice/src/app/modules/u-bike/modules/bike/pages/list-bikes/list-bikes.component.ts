import { BikeModel } from './../../../bike-model/models/bike-model.model';
import { BikesService } from './../../services/bikes.service';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { BikesModelService } from '../../../bike-model/services/bike-model.service';

@Component({
  selector: 'fi-sas-list-bikes',
  templateUrl: './list-bikes.component.html',
  styleUrls: ['./list-bikes.component.less']
})
export class ListBikesComponent  extends TableHelper implements OnInit {

  bikes_models_loading = false;
  bikeModels: BikeModel[] = [];

  constructor(uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private bikesModelService: BikesModelService,
    public bikesService: BikesService ) {
      super(uiService, router, activatedRoute);
    }

  ngOnInit() {
    this.columns.push(
      {
        key: 'identification',
        label: 'Identificação',
        sortable: true,
      },
      {
        key: 'size',
        label: 'Tamanho',
        sortable: true,
      },
      {
        key: 'bike_model.model',
        label: 'Modelo',
        sortable: false,
      }
    );

    this.persistentFilters = {
      withRelated: 'bike_model,asset',
      searchFields: 'identification,size'
    }

    this.initTableData(this.bikesService);
    this.loadBikeModels();
  }

  loadBikeModels() {
    this.bikes_models_loading = true;
    this.bikesModelService
      .list(1, -1)
      .pipe(
        first(),
        finalize(() => (this.bikes_models_loading = false))
      )
      .subscribe((results) => {
        this.bikeModels = results.data;
      });
  }

  deleteBike(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar bicicleta',
        'Pretende eliminar esta bicicleta ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.bikesService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Bicicleta eliminada com sucesso'
              );
              this.searchData();
            });
        }
      });
  }

  listComplete() {
    this.filters.search = null;
    this.filters.identification = null;
    this.filters.size = null;
    this.filters.bike_model_id = null;
    this.searchData(true)
  }

}
