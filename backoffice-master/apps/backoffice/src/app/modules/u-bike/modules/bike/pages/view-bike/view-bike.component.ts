import { Bike } from './../../models/bike.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-view-bike',
  templateUrl: './view-bike.component.html',
  styleUrls: ['./view-bike.component.less'],
})
export class ViewBikeComponent implements OnInit {
  @Input() bike: Bike = null;

  constructor() {}

  ngOnInit() {}
}
