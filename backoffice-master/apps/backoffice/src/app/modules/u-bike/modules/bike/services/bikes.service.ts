import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Bike } from '../models/bike.model';

@Injectable({
  providedIn: 'root',
})
export class BikesService extends Repository<Bike> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.BIKE';
    this.entity_url = 'U_BIKE.BIKE_ID';
  }

  listBikes(
    offset?: number,
    limit?: number,
    free?: boolean,
    sort: string = 'id',
    withRelated: string = 'bike_model.typology'
  ): Observable<Resource<Bike>> {
    let params = new HttpParams();
    params = params.set('limit', limit.toString());
    params = params.set('sort', 'id');
    params = params.set('withRelated', withRelated.toString());

    return this.resourceService.list<Bike>(this.urlService.get('U_BIKE.BIKE'), {
      params,
    });
  }

  listFreeBikes(
    offset?: number,
    limit?: number,
    free?: boolean,
    sort: string = 'id',
    withRelated: string = 'bike_model.typology'
  ): Observable<Resource<Bike>> {
    let params = new HttpParams();

    params = params.set('offset', offset.toString());
    params = params.set('limit', limit.toString());
    if (free) {
      params = params.set('free', free.toString());
    }
    params = params.set('sort', 'id');
    params = params.set('withRelated', withRelated.toString());

    return this.resourceService.list<Bike>(this.urlService.get('U_BIKE.FREE_BIKES'), {
      params,
    });
  }

  generateBikesListReport(data): Observable<Resource<any>>{
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.LIST_BIKES_REPORT'), data );
  }
}
