
export class TrainingModel {
  id?: number;
  application_id: number;
  date: string;
  user_responsible_id?: number;
  venue: string;
  certificate_file_id: number;
  created_at?: string;
  updated_at?: string;
}

