import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { TrainingModel } from '../models/training.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService extends Repository<TrainingModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.TRAININGS';
    this.entity_url = 'U_BIKE.TRAININGS_ID';
  }
}
