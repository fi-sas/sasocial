import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { ApplicationModel } from '../../ub-applications/models/application.model';
import { Bike } from '../../bike/models/bike.model';

export class TripModel {
  id: number;
  bike_id: number;
  bike_typology_id: number;
  typology_name: string;
  manufacturer: string;
  model: string;
  fuel_type: string;
  year_manufactured: number;
  engine_power: number;
  application_id: number;
  user_id: number;
  user_profile_id: number;
  profile_name: string;
  user_age: number;
  distance: number;
  height_diff: number;
  energy: number;
  co2_emissions_saved: number;
  time: number;
  trip_start: string;
  trip_end: string;
  is_weekend: number;
  is_holiday: number;
  description: string;
  avg_speed: number;
  watts: number;
  created_at: string;
  updated_at: string;
  application?: ApplicationModel;
  user?: UserModel;
  bike?: Bike;
}

