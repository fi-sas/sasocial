import { BikesService } from './../../../bike/services/bikes.service';
import { Bike } from './../../../bike/models/bike.model';
import { TypologyModel } from './../../../bike-typology/models/typology.model';
import { TypologiesService } from './../../../bike-typology/services/typologies.service';
import { TripsService } from './../../services/trips.service';
import { Component, OnInit } from '@angular/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';
import { NzMessageService } from 'ng-zorro-antd';
import * as moment from 'moment';
@Component({
  selector: 'fi-sas-list-trips',
  templateUrl: './list-trips.component.html',
  styleUrls: ['./list-trips.component.less']
})
export class ListTripsComponent extends TableHelper  implements OnInit {

  profiles_loading = false;
  users_loading = false;
  typologies_loading = false;
  bikes_loading = false;
  isConfirmLoading = false;

  profiles: ProfileModel[] = [];
  typologies: TypologyModel[] = [];
  bikes: Bike[] = [];
  isVisible=false;
  edit_trip: any;
  modalErrors = false;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private profilesService: ProfilesService,
    private usersService: UsersService,
    private typologiesService: TypologiesService,
    private bikesService: BikesService,
    public tripsService: TripsService,
    private message: NzMessageService
  ) { super(uiService, router, activatedRoute);}

  ngOnInit() {
    this.columns.push(
      {
        key: 'bike.identification',
        label: 'Bicicleta',
        sortable: true,
        sortKey: 'bike.identification'
      },
      {
        key: 'typology_name',
        label: 'Tipologia',
        sortable: true
      },
      {
        key: 'user.name',
        label: 'Utilizador',
        sortable: false
      },
      {
        key: 'profile_name',
        label: 'Perfil',
        sortable: true
      },
      {
        key: 'distance',
        label: 'KMs Percorridos',
        sortable: true
      },
      {
        key: 'trip_start',
        label: 'Data da Viagem',
        sortable: true
      },
      {
        key: 'time',
        label: 'Tempo (min.)',
        sortable: true
      }
    );

    this.persistentFilters['withRelated'] = 'application,bike,user';

    this.sortKey = 'trip_start';
    this.sortValue = 'descend';

    this.initTableData(this.tripsService);

    this.loadTypologies();
    this.loadProfiles();
    this.loadBikes();
  }

  loadProfiles() {
    this.profiles_loading = true;
    this.profilesService.list(1, -1, null, null, {
      sort: 'name'
    }).pipe(
      first(),
      finalize(() => this.profiles_loading = false)
    ).subscribe(results => {
      this.profiles = results.data;
    });
  }


  loadTypologies() {
    this.typologies_loading = true;
    this.typologiesService.list(1, -1, null, null, {
      sort: 'name'
    }).pipe(
      first(),
      finalize(() => this.typologies_loading = false)
    ).subscribe(results => {
      this.typologies = results.data;
    });
  }

  loadBikes() {
    this.bikes_loading = true;
    this.bikesService.list(1, -1, null, null, {
      sort: 'identification'
    }).pipe(
      first(),
      finalize(() => this.bikes_loading = false)
    ).subscribe(results => {
      this.bikes = results.data;
    });
  }

  deleteTrip(id: number) {
    this.uiService
      .showConfirm(
        'Eliminar percurso',
        'Pretende eliminar este percurso ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.tripsService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Percurso eliminado com sucesso'
              );
              this.searchData();
            });
        }
      });
  }


  trimParameters() {
    this.edit_trip.description =this.edit_trip.description.trim();
    if(this.edit_trip.description === ''){
      this.edit_trip.description = null;
    }
  }

  editTrip() {
    this.trimParameters()
    if (
      !this.edit_trip.distance ||
      !this.edit_trip.time ||
      !this.edit_trip.trip_start ||
      !this.edit_trip.description
    ) {
      this.modalErrors = true;
      return;
    } else {
      this.modalErrors = false;
    }
    this.tripsService
      .patch(this.edit_trip.id,this.edit_trip)
      .pipe(
        first(),
        finalize(() => {
          this.isVisible = false;
        })
      )
      .subscribe(
        () => {
          this.message.success('Percurso guardado com sucesso');
          this.searchData();
        }
      );
  }

  disableDate = (date: Date)=> {
    var currentDate = new Date();
    return moment(date).isAfter(currentDate);
  }

  showModal(trip: any): void {
    this.edit_trip = trip;
    this.isVisible = true;
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
    }, 3000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  listComplete() {
    this.filters.search = null;
    this.filters.bike_id = null;
    this.filters.user_id = null;
    this.filters.profile_name = null;
    this.filters.distance = null;
    this.searchData(true)
  }



}
