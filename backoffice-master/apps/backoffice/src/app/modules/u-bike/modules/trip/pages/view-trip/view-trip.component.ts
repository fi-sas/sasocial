import { TripModel } from './../../models/trip.model';
import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-view-trip',
  templateUrl: './view-trip.component.html',
  styleUrls: ['./view-trip.component.less']
})
export class ViewTripComponent implements OnInit {

  @Input() data: TripModel = null;

  constructor() { }

  ngOnInit() {}

  formatTime(minutes: number): string {
    const time = moment.utc(moment.duration(minutes, "minutes").asMilliseconds()).format("HH:mm");
    const hourSplitter = time.split(':');
    return `${hourSplitter[0]}h${hourSplitter[1]}`;
  }

}
