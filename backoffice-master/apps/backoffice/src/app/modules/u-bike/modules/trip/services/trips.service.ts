import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { TripModel } from '../models/trip.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TripsService extends Repository<TripModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.TRIPS';
    this.entity_url = 'U_BIKE.TRIPS_ID';
  }
 
  generateKmsReport(data): Observable<Resource<any>>{
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.KMS_TRAVELED_REPORT'), data );
  }
}
