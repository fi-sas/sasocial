import { ListTripsComponent } from './pages/list-trips/list-trips.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListTripsComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar',
      scope: 'u_bike:trips:read',
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripRoutingModule { }
