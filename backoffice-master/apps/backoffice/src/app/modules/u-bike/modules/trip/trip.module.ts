import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TripRoutingModule } from './trip-routing.module';
import { ListTripsComponent } from './pages/list-trips/list-trips.component';
import { ViewTripComponent } from './pages/view-trip/view-trip.component';

@NgModule({
  declarations: [ListTripsComponent, ViewTripComponent],
  imports: [
    CommonModule,
    SharedModule,
    TripRoutingModule
  ]
})
export class TripModule { }
