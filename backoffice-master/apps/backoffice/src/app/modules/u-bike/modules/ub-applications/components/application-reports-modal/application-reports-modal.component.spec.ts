import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationReportsModalComponent } from './application-reports-modal.component';

describe('ApplicationReportsModalComponent', () => {
  let component: ApplicationReportsModalComponent;
  let fixture: ComponentFixture<ApplicationReportsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationReportsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationReportsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
