import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';

@Component({
  selector: 'fi-sas-application-reports-modal',
  templateUrl: './application-reports-modal.component.html',
  styleUrls: ['./application-reports-modal.component.less']
})
export class ApplicationReportsModalComponent implements OnInit {

  @Input() application_id: number = null;
  loading: boolean = true;
  application: ApplicationModel;
  submitted: boolean = false;
  consent_terms_file = null;
  delivery_reception_file = null;
  filterTypes = ['application/pdf'];
  loadingFileConsent: boolean = false;
  loadingFileDelivery: boolean = false;
  loadingSubmit: boolean = false;

  form = new FormGroup({
    consent_terms_file_id: new FormControl(null),
    delivery_reception_file_id: new FormControl(null)
  });

  get f() { return this.form.controls; }
  
  constructor(
    private modalRef: NzModalRef,
    private applicationService: ApplicationsService,
    private uiService: UiService,
    private fileService: FilesService,
  ) { }

  ngOnInit() {
    this.applicationService.read(this.application_id)
    .pipe(first())
    .subscribe(response => {
      this.application = response.data[0];
      this.form.get('consent_terms_file_id').setValue(this.application.consent_terms_file_id);
      this.form.get('delivery_reception_file_id').setValue(this.application.delivery_reception_file_id);
      this.loadConsentFile();
      this.loadDeliveryFile();
      this.loading = false;
    });
  }

  loadConsentFile(){
    if(this.application.consent_terms_file_id){
      this.loadingFileConsent = true;
      this.fileService.get(this.application.consent_terms_file_id).pipe(first(), finalize(() => this.loadingFileConsent = false)).subscribe((file)=>{
        this.consent_terms_file = file.data[0];
      });
    }
  }

  loadDeliveryFile(){
    if(this.application.delivery_reception_file_id){
      this.loadingFileDelivery = true;
      this.fileService.get(this.application.delivery_reception_file_id).pipe(first(), finalize(() => this.loadingFileDelivery = false)).subscribe((file)=>{
        this.delivery_reception_file = file.data[0];
      });
    }
  }


  close() {
    this.modalRef.close();
    this.modalRef.destroy();
  }


  submit() {
    if(this.f.consent_terms_file_id.value || this.f.delivery_reception_file_id.value){
      this.submitted = true;
      this.loadingSubmit = true;
      this.applicationService.submitConsentDeliveryFile(this.application.id, this.form.value).pipe(first(), finalize(() => this.loadingSubmit = false))
        .subscribe(() => {
          this.uiService.showMessage(MessageType.success, "Documentos inseridos com sucesso.");
         this.close();
        });
    }
  }

}
