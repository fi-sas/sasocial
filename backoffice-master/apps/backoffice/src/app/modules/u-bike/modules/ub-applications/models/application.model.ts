import { UserModel } from './../../../../users/modules/users_users/models/user.model';
import { TypologyModel } from "../../bike-typology/models/typology.model";
import { HistoryModel } from "./history.model";
import { Bike } from "../../bike/models/bike.model";
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';

export enum UBikeApplicationStatus {
  Waiting_Approval = 'Waiting Approval',
  Approved = 'Approved',
  Accepted = 'Accepted',
  Rejected = 'Rejected',
  Withdraw = 'Withdraw'
}

export enum DailyCommute {
  WALK = 'Walk',
  CAR = 'Car',
  MOTORCYCLE = 'Motorcycle',
  PUBLIC_TRANSPORT = 'Public Transport',
  BUS = 'Bus',
  TRAIN = 'Train',
  BIKE = 'Bike',
  OTHER = 'Other'
}

export class ApplicationModel {
  id?: number;
  full_name: string;
  birth_date: string;
  gender: string;
  weight: number;
  height: number;
  address: string;
  postal_code: string;
  email: string;
  phone_number: string;
  student_number: string;
  start_date: string;
  end_date?: string;
  typology_first_preference: TypologyModel;
  other_typology_preference?: boolean;
  daily_commute: string;
  daily_consumption: string;
  daily_kms: number;
  number_of_times: number;
  avg_weekly_distance: number;
  total_weekly_distance: number;
  travel_time_school: number;
  travel_time_school_bike: number;
  manufacturer?: string;
  model?: string;
  year_manufactured?: number;
  engine_power?: number;
  avg_vehicle_consumption_100: number;
  applicant_consent: boolean;
  consent_date?: string;
  bike_id?: number;
  user_id?: number;
  status?: string;
  pickup_location?: string;
  updated_at?: string;
  created_at?: string;
  decision?: string;
  daily_co2?: number;
  history?: HistoryModel[];
  bike?: Bike;
  user?: UserModel;
  documents?: any;
  typology_first_preference_id?: number;
  preferred_pickup_organict_unit_id: number;
  preferred_pickup_organict_unit?: OrganicUnitsModel;
  delivery_reception_file_id: number;
  consent_terms_file_id: number;
}


export class StatusApplicationModel {
  status: string;
  notes: string;
}

// status [ Waiting Approval, Approved, Accepted, Rejected, Withdraw ]
export class ApproveApplicationModel {
  status: string;
  notes: string;
  application?: ApplicationModel;

}

export class RejectApplicationModel {
  notes: string;
  application?: ApplicationModel;

}

export class ArrayData {

  consumption = [
    { key: 'Electric', pt: 'Elétrico' },
    { key: 'GPL', pt: 'GPL' },
    { key: 'Diesel', pt: 'Diesel' },
    { key: 'Gasoline', pt: 'Gasolina' },
    { key: null, pt: '' }
  ];

  commute = [
    { key: '', pt: 'Todos os Transportes' },
    { key: 'Walk', pt: 'A pé' },
    { key: 'Car', pt: 'Carro' },
    { key: 'Motorcycle', pt: 'Mota' },
    { key: 'Train', pt: 'Comboio' },
    { key: 'Bus', pt: 'Autocarro' },
    { key: 'Bike', pt: 'Bicicleta' },
    { key: 'Other', pt: 'Outro' },
    { key: null, pt: '' }
  ];

  status = [
    { key: '', pt: 'Todos os Estados', color: '' },
    { key: 'elaboration', pt: 'Elaboração', color: '#E1BC29' },
    { key: 'submitted', pt: 'Submetida', color: '#4D9DE0' },
    { key: 'analysis', pt: 'Em Análise', color: '#7768AE' },
    { key: 'cancelled', pt: 'Cancelada', color: '#D0021B' },
    { key: 'dispatch', pt: 'Em Despacho', color: '#88CCF1' },
    { key: 'assigned', pt: 'Atribuída', color: '#076743' },
    { key: 'enqueued', pt: 'Em Lista de Espera', color: '#D4B483' },
    { key: 'unassigned', pt: 'Não Atribuída', color: '#C1666B' },
    { key: 'suspended', pt: 'Suspensa', color: '#8EA4D2' },
    { key: 'complaint', pt: 'Queixa', color: '#F75C03' },
    { key: 'accepted', pt: 'Aceite pelo Utilizador', color: '#9DBBAE' },
    { key: 'contracted', pt: 'Contratada', color: '#1BA974' },
    { key: 'rejected', pt: 'Rejeitada pelo Utilizador', color: '#D0021B' },
    { key: 'quiting', pt: 'Em Desistência', color: '#EF476F' },
    { key: 'withdrawal', pt: 'Retirada', color: '#246A73' },
    { key: 'closed', pt: 'Fechada', color: '#000000' },
    { key: null, pt: '', color: '' }
  ];

  gender = [
    { key: 'M', pt: 'Masculino', color: 'blue' },
    { key: 'F', pt: 'Feminino', color: 'pink' },
    { key: 'U', pt: 'Outro', color: 'gray' },
    { key: null, pt: '-', color: 'gray' }
  ];

  condition = [
    { key: 'false', pt: 'Não' },
    { key: 'true', pt: 'Sim' },
    { key: null, pt: '' }
  ];

}
