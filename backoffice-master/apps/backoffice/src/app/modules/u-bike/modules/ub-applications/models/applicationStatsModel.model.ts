export class ApplicationStatsModel {
  elaboration: number;
  submitted: number;
  analysis: number;
  cancelled: number;
  dispatch: number;
  assigned: number;
  enqueued: number;
  unassigned: number;
  suspended: number;
  complaint: number;
  accepted: number;
  contracted: number;
  rejected: number;
  quiting: number;
  withdrawal: number;
  closed: number;
}
