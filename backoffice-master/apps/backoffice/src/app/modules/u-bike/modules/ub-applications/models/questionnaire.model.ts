export class QuestionnaireModel {
  institute: string;
  profile: string;
  course_year: number;
  gender: string;
  daily_kms: string;
  number_of_times: number;
  avg_weekly_distance: string;
  total_weekly_distance: string;
  daily_commute: string;
  travel_time_school: string;
  typology_first_preference: string;
  travel_time_school_bike: string;
  daily_consumption: string;
  avg_vehicle_consumption_100: string;
}
