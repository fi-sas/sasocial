import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApplicationModel, ArrayData } from '../../models/application.model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApplicationsService } from '../../services/applications.service';
import { BikesService } from '../../../bike/services/bikes.service';
import { Bike } from '../../../bike/models/bike.model';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';
import { TripModel } from '../../../trip/models/trip.model';
import { TripsService } from '../../../trip/services/trips.service';
import { ListPrint } from '../../models/list-print.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { ApplicationReportsModalComponent } from '../../components/application-reports-modal/application-reports-modal.component';
@Component({
  selector: 'fi-sas-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less'],
})
export class ListApplicationsComponent extends TableHelper implements OnInit, OnDestroy {
  loadingQuestionnaire = false;

  editButtonDisable = true;

  statusList: ArrayData = new ArrayData();

  mapOfExpandData: { [key: string]: boolean } = {};

  // Status Modal
  isModalVisible = false;
  isChangeLoading = false;
  notes = '';

  // Form
  statusForm: FormGroup;
  renderFormByStatus: string;
  decisionApplication: string;

  //Bike Modal
  isBikeModalVisible = false;
  bikeForm: FormGroup;
  changeBikeLoading = false;

  //Bike add km Modal
  isBikeKilometersVisible = false;

  //Trip Modal
  isTripModalVisible = false;
  tripLoading = false;
  tripFormModal: NzModalRef = null;

  //Print Modal
  isPrintModalVisible = false;
  printForm: FormGroup;
  printLoading = false;
  file_types = [
    { label: 'PDF', value: 'pdf' },
    { label: 'Excel', value: 'xlsx' },
  ];
  file_type = 'pdf';
  status = '';
  periodDate = [];
  orderFilter = 'created_at';
  order = 'ASC';
  orderFilters = [
    { name: 'Data de Submissão', key: 'created_at' },
    { name: 'Nome', key: 'full_name' },
    { name: 'Peso', key: 'weight' },
    { name: 'Altura', key: 'height' },
    { name: 'Número de Estudante', key: 'student_number' },
  ];
  ordering = [
    { name: 'Ascendente', key: 'ASC' },
    { name: 'Decrescente', key: 'DESC' },
  ];

  listFreeBikes: Bike[];

  dataInModal: ApplicationModel = null;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  trip: TripModel = null;

  idToUpdate = null;

  tripForm = new FormGroup({
    distance: new FormControl(null, [Validators.required]),
    height_diff: new FormControl(0),
    time: new FormControl(null, [Validators.required]),
    trip_start: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required, trimValidation]),
  });

  constructor(
    public applicationService: ApplicationsService,
    public bikesService: BikesService,
    uiService: UiService,
    private formBuilder: FormBuilder,
    private tripsService: TripsService,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private modalService: NzModalService,
    router: Router
  ) {
    super(uiService, router, activatedRoute);
    this.statusForm = this.formBuilder.group({
      notes: [''],
      decision: [''],
      bike: [''],
      pickupLocation: [''],
      finalStatus: [''],
      contract_file_id: [''],
    });
    this.bikeForm = this.formBuilder.group({
      bike_id: [''],
      pickup_location: [''],
    });
    this.subscriptions = this.applicationService.tripObservable().subscribe(() => {
      if (this.tripFormModal !== null) {
        this.tripFormModal.close();
      }
    });
    this.persistentFilters = {
      withRelated: 'bike,preferred_pickup_organict_unit',
      searchFields: 'full_name,identification,student_number',
    };
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    this.initTableData(this.applicationService);
    this.getBikes();
  }

  getStatus(status: string): any {
    return this.statusList.status.find((x) => x.key === status);
  }

  getCommute(commute: string): any {
    return this.statusList.commute.find((x) => x.key === commute);
  }

  getBikes(): void {
    this.bikesService
      .listFreeBikes(0, -1, true)
      .pipe(first())
      .subscribe(
        (res) => {
          this.listFreeBikes = res.data;
        },
        () => {}
      );
  }

  deleteApplication(id: number) {
    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta candidatura?', 'Eliminar').subscribe((result) => {
      if (result) {
        this.applicationService.delete(id).subscribe((r) => {
          this.uiService.showMessage(MessageType.success, 'Candidatura eliminada com sucesso');
          this.searchData();
        });
      }
    });
  }

  // Modal
  showModal(data: ApplicationModel): void {
    if (this.changeBikeButtonEnable(data.status)) {
      return;
    }
    this.cleanValidators();
    this.getBikes();
    this.isModalVisible = true;
    this.dataInModal = data;
    this.renderFormByStatus = data.status;

    if (this.renderFormByStatus === 'analysis' || this.renderFormByStatus === 'complaint') {
      this.statusForm.get('decision').setValidators([Validators.required]);
    } else if (this.renderFormByStatus === 'accepted') {
      this.statusForm.get('contract_file_id').setValidators([Validators.required]);
    }else if (this.renderFormByStatus === 'assigned') {
      this.statusForm.get('bike').setValidators([Validators.required]);
      this.statusForm.get('pickupLocation').setValidators([Validators.required]);
    } else {
      this.statusForm.get('decision').setValidators(null);
      this.statusForm.get('decision').reset();
    }

    this.statusForm.get('decision').updateValueAndValidity();
    this.statusForm.reset();
  }

  closeModal(): void {
    this.isModalVisible = false;
    this.cleanValidators();
  }

  cleanValidators(){
    this.statusForm.get('decision').setValidators(null);
    this.statusForm.get('decision').reset();
    this.statusForm.get('decision').updateValueAndValidity();
    this.statusForm.get('contract_file_id').setValidators(null);
    this.statusForm.get('contract_file_id').reset();
    this.statusForm.get('contract_file_id').updateValueAndValidity();
    this.statusForm.get('bike').setValidators(null);
    this.statusForm.get('bike').reset();
    this.statusForm.get('bike').updateValueAndValidity();
    this.statusForm.get('pickupLocation').setValidators(null);
    this.statusForm.get('pickupLocation').reset();
    this.statusForm.get('pickupLocation').updateValueAndValidity();
  }

  changeStatus(value: any, applicationData: ApplicationModel): void {
    this.getBikes();
    this.isChangeLoading = true;

    // tslint:disable-next-line:forin
    for (const key in this.statusForm.controls) {
      this.statusForm.controls[key].markAsDirty();
      this.statusForm.controls[key].updateValueAndValidity();
    }

    if (this.statusForm.get('decision').value && this.statusForm.get('decision').value !== '') {
      this.statusForm.get('decision').setValue(this.statusForm.get('decision').value.trim());
    }

    switch (applicationData.status) {
      case 'complaint':
        this.applicationService
          .changeStatusBackToDispatch(applicationData.id, value.notes, value.decision, value.pickupLocation, value.bike)
          .pipe(first())
          .subscribe(
            () => {
              this.isChangeLoading = false;
              this.searchData();
              this.closeModal();
            },
            () => {
              this.closeModal();
            }
          );
        break;

      case 'submitted':
        this.applicationService
          .changeStatusToAnalysis(applicationData.id, value.notes)
          .pipe(first())
          .subscribe(
            () => {
              this.isChangeLoading = false;
              this.searchData();
              this.closeModal();
            },
            () => {
              this.closeModal();
            }
          );
        break;

      case 'enqueued':
      case 'analysis':
        this.applicationService
          .changeStatusToDispatch(applicationData.id, value.notes, value.decision, value.pickupLocation, value.bike)
          .pipe(first())
          .subscribe(
            () => {
              this.isChangeLoading = false;
              this.searchData();
              this.closeModal();
            },
            () => {
              this.closeModal();
            }
          );
        break;
      case 'quiting':
        this.applicationService
          .changeStatusToWithdrawal(applicationData.id, value.notes)
          .pipe(first())
          .subscribe(
            () => {
              this.isChangeLoading = false;
              this.searchData();
              this.closeModal();
            },
            () => {
              this.closeModal();
            }
          );
        break;

      case 'dispatch':
        const decision = this.statusForm.get('decision').value;
        if (!decision) {
          this.isChangeLoading = false;
        }
        switch (decision) {
          case 'accept':
            this.applicationService
              .adminAccept(applicationData.id, value.notes)
              .pipe(
                first(),
                finalize(() => {
                  this.isChangeLoading = false;
                  this.closeModal();
                })
              )
              .subscribe((result) => {
                this.searchData();
              });
            break;

          case 'reject':
            this.applicationService
              .changeStatusToReanalyse(applicationData.id, value.notes)
              .pipe(
                first(),
                finalize(() => {
                  this.isChangeLoading = false;
                  this.closeModal();
                })
              )
              .subscribe((result) => {
                this.searchData();
              });

            break;

          default:
            break;
        }
        // abrir mensagem de erro
        break;

      case 'accepted':
        this.applicationService
          .changeStatusToContracted(applicationData.id, value.notes, this.statusForm.get('contract_file_id').value)
          .pipe(first())
          .subscribe(
            (result) => {
              this.isChangeLoading = false;
              this.searchData();
              this.closeModal();
            },
            (error) => {
              this.closeModal();
            }
          );
        break;

      case 'contracted':
        const decisions = this.statusForm.get('decision').value;
        if (decisions === 'close') {
          this.applicationService
            .changeStatusToClosed(applicationData.id, value.notes)
            .pipe(first())
            .subscribe(
              () => {
                this.isChangeLoading = false;
                this.searchData();
                this.closeModal();
              },
              () => {
                this.closeModal();
              }
            );
        } else if (decisions === 'suspend') {
          this.applicationService
            .changeStatusToSuspended(applicationData.id, value.notes)
            .pipe(first())
            .subscribe(
              () => {
                this.isChangeLoading = false;
                this.searchData();
                this.closeModal();
              },
              () => {
                this.closeModal();
              }
            );
        }
        break;

      case 'suspended':
        const decisionss = this.statusForm.get('decision').value;
        if (decisionss === 'close') {
          this.applicationService
            .changeStatusToClosed(applicationData.id, value.notes)
            .pipe(first())
            .subscribe(
              () => {
                this.isChangeLoading = false;
                this.searchData();
                this.closeModal();
              },
              () => {
                this.closeModal();
              }
            );
        } else if (decisionss === 'unsuspend') {
          this.applicationService
            .changeStatusToContractedFromSuspend(applicationData.id, value.notes)
            .pipe(first())
            .subscribe(
              () => {
                this.isChangeLoading = false;
                this.searchData();
                this.closeModal();
              },
              () => {
                this.closeModal();
              }
            );
        }
        break;
    }
  }

  showBikeKm(applicationData: ApplicationModel): void {
    this.dataInModal = applicationData;
    this.isBikeKilometersVisible = true;
  }

  closeBikeKm() {
    this.isBikeKilometersVisible = false;
  }

  showBikeModal(applicationData: ApplicationModel): void {
    if (this.editBikeButtonEnable(applicationData.status)) {
      return;
    }
    this.bikeForm.get('bike_id').setValidators([Validators.required]);
    this.bikeForm.get('pickup_location').setValidators([Validators.required]);
    this.dataInModal = applicationData;
    this.getBikes();
    this.isBikeModalVisible = true;
  }

  closeBikeModal(): void {
    this.isBikeModalVisible = false;
    this.bikeForm.get('bike_id').reset();
    this.bikeForm.get('bike_id').updateValueAndValidity();
    this.bikeForm.get('pickup_location').reset();
    this.bikeForm.get('pickup_location').updateValueAndValidity();
  }

  changeBike(value: any) {
    this.changeBikeLoading = true;
    for (const key in this.bikeForm.controls) {
      this.bikeForm.controls[key].markAsDirty();
      this.bikeForm.controls[key].updateValueAndValidity();
    }

    if (this.bikeForm.valid) {
      this.applicationService
        .patch(this.dataInModal.id, value)
        .pipe(
          first(),
          finalize(() => (this.changeBikeLoading = false))
        )
        .subscribe(() => {
          this.bikeForm.reset();
          this.closeBikeModal();
          this.getBikes();
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Bicicleta alterada com sucesso.');
        });
    } else {
      this.changeBikeLoading = false;
    }
  }

  editBikeButtonEnable(status: string) {
    const statusToEnable = ['dispatch', 'assigned', 'accepted', 'contracted'];
    if (status) {
      return !statusToEnable.includes(status);
    }
    return false;
  }

  changeBikeButtonEnable(status: string) {
    const statusToEnable = ['cancelled', 'closed', 'unassigned', 'rejected', "withdrawal"];
    if (status) {
      return statusToEnable.includes(status);
    }
    return false;
  }

  printFile(): void {
    if (this.periodDate.length > 0) {
      let data: ListPrint = new ListPrint();
      if (this.status) {
        data.status = this.status;
      }
      data.order_by = this.orderFilter;
      data.order = this.order;
      data.start_date = this.periodDate[0];
      data.end_date = this.periodDate[1];
      data.doc_type = this.file_type;

      this.printLoading = true;
      this.applicationService
        .print(data)
        .pipe(
          first(),
          finalize(() => (this.printLoading = false))
        )
        .subscribe(() => {
          this.isPrintModalVisible = false;
          this.uiService.showMessage(MessageType.success, 'Impressão de listagem realizada com sucesso');
        });
    } else {
      this.uiService.showMessage(MessageType.error, 'Preencha o período');
    }
  }

  showPrintModal(): void {
    this.isPrintModalVisible = true;
  }

  closePrintModal(): void {
    this.isPrintModalVisible = false;
  }

  applicationAdminAccept(data: ApplicationModel): void {
    if (!this.authService.hasPermission('u_bike:applications:dispatch')) {
      return;
    }
    this.applicationService
      .adminAccept(data.id)
      .pipe(first())
      .subscribe(
        () => {
          this.isChangeLoading = false;
          this.searchData();
          this.closeModal();
        },
        () => {
          this.closeModal();
        }
      );
  }

  disableEndDate = (current: Date) => {
    return moment().isBefore(current);
  };

  listComplete() {
    this.filters.status = null;
    this.filters.daily_commute = null;
    this.filters.student_number = null;
    this.filters.search = null;
    this.filters.identification = null;
    this.searchData(true);
  }

  applicationAdminReject(data: ApplicationModel): void {
    if (!this.authService.hasPermission('u_bike:applications:dispatch')) {
      return;
    }
    this.applicationService
      .adminReject(data.id)
      .pipe(first())
      .subscribe(
        () => {
          this.isChangeLoading = false;
          this.searchData();
          this.closeModal();
        },
        () => {
          this.closeModal();
        }
      );
  }

  //Form
  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.statusForm.reset();
    // tslint:disable-next-line:forin
    for (const key in this.statusForm.controls) {
      this.statusForm.controls[key].markAsPristine();
      this.statusForm.controls[key].updateValueAndValidity();
    }
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    }
    return {};
  };

  conditionalFormByStatus(evt) {
    this.decisionApplication = evt;

    if (this.decisionApplication === 'assigned') {
      this.statusForm.get('bike').setValidators([Validators.required]);
      this.statusForm.get('pickupLocation').setValidators([Validators.required]);
    } else {
      this.statusForm.get('bike').setValidators(null);
      this.statusForm.get('pickupLocation').setValidators(null);
      this.statusForm.get('bike').reset();
      this.statusForm.get('pickupLocation').reset();
    }
    this.statusForm.get('bike').updateValueAndValidity();
    this.statusForm.get('pickupLocation').updateValueAndValidity();
  }

  getButtonName(status: string): string {
    switch (status) {
      case 'submitted':
        return 'Análise';
      case 'unassigned':
        return 'Fechar';
      case 'accepted':
        return 'Contratar';
      case 'contracted':
        return 'Confirmar';
      default:
        return 'Alterar Estado';
    }
  }

  generateQuestionnaire(application: ApplicationModel) {
    this.loadingQuestionnaire = true;

    this.applicationService
      .getQuestionnairePdf(application.id)
      .pipe(
        first(),
        finalize(() => (this.loadingQuestionnaire = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Questionário gerado com sucesso');
      });
  }

  formatTime(minutes: number): string {
    const time = moment.utc(moment.duration(minutes, 'minutes').asMilliseconds()).format('HH:mm');
    const hourSplitter = time.split(':');
    return `${hourSplitter[0]}h${hourSplitter[1]}`;
  }

  closeTripModal(saved: boolean) {}

  submit() {
    const formValue: TripModel = this.tripForm.value;
    formValue.application_id = this.dataInModal.id;
    formValue.trip_end = null;
    formValue.distance = +formValue.distance;
    formValue.time = +formValue.time;

    if (this.tripForm.get('description').value.trim() === '') {
      this.tripForm.get('description').setErrors({ incorrect: true });
      this.tripForm.setErrors({ incorrect: true });
    }
    if (this.tripForm.valid) {
      this.tripsService
        .create(formValue)
        .pipe(
          first(),
          finalize(() => {
            this.tripLoading = false;
            this.reset();
          })
        )
        .subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Registro inserido com sucesso.');
        });
    }
  }

  generateConsentFile(id: number){
    this.applicationService.getConsentTermsFile(id).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,'O relatório está a ser gerado. Assim que for concluído ficará disponível na área de relatórios.');
    });
  }

  generateDeliveryFile(id: number){
    this.applicationService.getDeliveryReceptionFile(id).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,'O relatório está a ser gerado. Assim que for concluído ficará disponível na área de relatórios.');
    });
  }

  submitReportModal(id: number){
    const modal = this.modalService.create({
      nzTitle: 'Submeter documentos',
      nzContent: ApplicationReportsModalComponent,
      nzWidth: 650,
      nzComponentParams: {
        application_id: id
      },
      nzFooter: [
        {
          label: 'Sair',
          onClick: componentInstance => {
            componentInstance.close();
          }
        },
        {
          label: 'Submeter',
          type: 'success',
          onClick: componentInstance => {
            componentInstance.submit();
          }
        },
      ],
    });
  }

  reset() {
    this.tripForm.reset();
  }
}
