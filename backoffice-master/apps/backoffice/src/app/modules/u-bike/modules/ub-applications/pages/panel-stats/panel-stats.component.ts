import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApplicationStatsModel } from '../../models/applicationStatsModel.model';
import { ApplicationsService } from '../../services/applications.service';
import { ApplicationModel, ArrayData } from '../../models/application.model';

@Component({
  selector: 'fi-sas-panel-stats',
  templateUrl: './panel-stats.component.html',
  styleUrls: ['./panel-stats.component.less']
})
export class PanelStatsComponent implements OnInit {



  stats: ApplicationStatsModel = {
    elaboration: 0,
    submitted: 0,
    analysis: 0,
    cancelled: 0,
    dispatch: 0,
    assigned: 0,
    enqueued: 0,
    unassigned: 0,
    suspended: 0,
    complaint: 0,
    accepted: 0,
    contracted: 0,
    rejected: 0,
    quiting: 0,
    withdrawal: 0,
    closed: 0
  };

  grid = {
    nzXs: 24,
    nzSm: 24,
    nzMd: 12,
    nzLg: 8,
    nzXl: 6,
    nzXXl: 4,
  }

  loadingStats = false;
  statusList: ArrayData = new ArrayData();
  constructor(private applicationsService: ApplicationsService) { }

  ngOnInit() {
    this.getStats();
  }

  getStats() {
    this.loadingStats = true;
    this.applicationsService.stats().pipe(first()).subscribe(stats => {
      this.stats = stats.data[0];
      this.loadingStats = false;
    }, () => {
      this.loadingStats = false;
    });
  }


  getStatus(status: string): any {
    return this.statusList.status.find((x) => x.key === status);
  }

}
