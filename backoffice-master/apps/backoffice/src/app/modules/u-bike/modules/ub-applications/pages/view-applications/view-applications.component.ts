import { Component, OnInit, Input } from '@angular/core';
import { ApplicationModel, ArrayData } from '../../models/application.model';
import * as moment from 'moment';
import { ApplicationsService } from '../../services/applications.service';
import { first, finalize } from 'rxjs/operators';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';

@Component({
  selector: 'fi-sas-view-applications',
  templateUrl: './view-applications.component.html',
  styleUrls: ['./view-applications.component.less']
})
export class ViewApplicationsComponent implements OnInit {

  userDetailData: ApplicationModel = new ApplicationModel();
  @Input() id: number;

  consent_terms_file = null;
  delivery_reception_file = null;
  contractLoading: boolean = false;

  documentForm = new FormGroup({
   name: new FormControl("", [Validators.required]),
    file_id: new FormControl(null, [Validators.required]),
  })

  submitted = false;
  loading = false;
  gender: String;
  consuption: String;
  commute: String;

  constructor(public applicationService: ApplicationsService, private uiService: UiService, private fileService: FilesService,) {
  }


  get f() { return this.documentForm.controls; }

  statusList: ArrayData = new ArrayData();


  getStatusDetail(status: string): any {
    return this.statusList.status.find((x) => x.key === status);
  }

  formatTime(minutes: number): string {
    const time = moment.utc(moment.duration(minutes, "minutes").asMilliseconds()).format("HH:mm");
    const hourSplitter = time.split(':');
    return `${hourSplitter[0]}h${hourSplitter[1]}`;
  }

  getGender(gender: string): any {
    return this.statusList.gender.find((x) => x.key === gender);
  }

  getConsumption(consumption: string): any {
    return this.statusList.consumption.find((x) => x.key === consumption);
  }

  getCommute(commute: string): any {
    return this.statusList.commute.find((x) => x.key === commute);
  }

  removeDocument(id: number, index: number) {
    this.applicationService.removeDocument(id).subscribe((r) => {
      this.uiService.showMessage(
        MessageType.success,
        'Documento eliminada com sucesso'
      );
      if (index !== -1) {
        this.userDetailData.documents.splice(index, 1);
    }
    });
  }

  saveDocument() {
    this.submitted = true;
    if (this.documentForm.valid) {
      this.contractLoading = true;
      this.applicationService.saveDocument(this.userDetailData.id, this.documentForm.controls.file_id.value, this.documentForm.controls.name.value).pipe(first(), finalize(() => this.contractLoading = false)).subscribe((r) => {
        this.uiService.showMessage(
          MessageType.success,
          'Documento adicionado com sucesso'
        );
        this.applicationService.listDocumentsApplication(this.userDetailData.id).subscribe((r) => {
          this.userDetailData.documents = r.data;
        });
      });
    }
  }

  ngOnInit() {
    this.loading = true;
    this.applicationService
    .read(this.id, {
      id: this.id,
      withRelated: "history,typology_first_preference,bike,user,documents,preferred_pickup_organict_unit"
    })
    .pipe(
      first(),
      finalize(() => (this.loading = false))
    )
    .subscribe((result) => {
      this.loading = false;
      this.userDetailData = result.data[0];
      this.gender = this.getGender(result.data[0].gender).pt;
      this.consuption = this.getConsumption(result.data[0].daily_consumption).pt;
      this.commute = this.getCommute(result.data[0].daily_commute).pt;
      this.loadConsentFile();
      this.loadDeliveryFile();
    });
  }


  loadConsentFile(){
    if(this.userDetailData.consent_terms_file_id){
      this.fileService.get(this.userDetailData.consent_terms_file_id).pipe(first()).subscribe((file)=>{
        this.consent_terms_file = file.data[0];
      });
    }
  }

  loadDeliveryFile(){
    if(this.userDetailData.delivery_reception_file_id){
      this.fileService.get(this.userDetailData.delivery_reception_file_id).pipe(first()).subscribe((file)=>{
        this.delivery_reception_file = file.data[0];
      });
    }
  }

}
