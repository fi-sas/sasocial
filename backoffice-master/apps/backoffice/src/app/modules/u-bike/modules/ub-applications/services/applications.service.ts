import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { BehaviorSubject, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { QuestionnaireModel } from '../models/questionnaire.model';
import { TemplatePrintModel } from '@fi-sas/backoffice/modules/reports/models/template-print.model';
import { ApplicationModel } from '../models/application.model';
import { ApplicationStatsModel } from '../models/applicationStatsModel.model';
import { ListPrint } from '../models/list-print.model';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsService extends Repository<ApplicationModel> {
  registeredTrip = new BehaviorSubject(false);

  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.APPLICATIONS';
    this.entity_url = 'U_BIKE.APPLICATIONS_ID';
  }

  tripObservable(): Observable<boolean> {
    return this.registeredTrip.asObservable();
  }

  recordTrip() {
    this.registeredTrip.next(true);
  }

  /* list(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    statusFilter?: string,
    dailyCommuteFilter?: string,
    userFullNameFilter?: string,
    student_numberFilter?: string,
    withRelated?: string,
  ): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());

    if (statusFilter) {
      params = params.set('query[status]', statusFilter.toString());
    }

    if (dailyCommuteFilter) {
      params = params.set('query[daily_commute]', dailyCommuteFilter.toString());
    }

    if (userFullNameFilter) {
      params = params.set('searchFields', 'full_name');
      params = params.set('search', userFullNameFilter.toString());
    }

    if (student_numberFilter) {
      params = params.set('searchFields', 'student_number');
      params = params.set('search', student_numberFilter.toString());
    }

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    } else {
      params = params.set('sort', '-id');
    }

    let aux = 'history,typology_first_preference,bike,user'
    withRelated = withRelated?(aux + ',' + withRelated):aux;

    params = params.set('withRelated', withRelated);

    return this.resourceService.list<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS'), {
      params
    });
  }*/

  delete(id: number): Observable<any> {
    return this.resourceService.delete(this.urlService.get('U_BIKE.APPLICATIONS_ID', { id }));
  }

  changeStatusToAnalysis(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'analyse',
      notes,
    });
  }

  changeStatusToContractedFromSuspend(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'unsuspend',
      notes,
    });
  }

  changeStatusBackToDispatch(id: number, notes: string, decision: string,pickup_location?: string,
    bike_id?: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'dispatch',
      notes,
      application: {decision,pickup_location, bike_id},
    });
  }

  changeStatusToDispatch(
    id: number,
    notes: string,
    decision: string,
    pickup_location?: string,
    bike_id?: number
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'dispatch',
      notes,
      application: { decision, pickup_location, bike_id },
    });
  }

  changeStatusToContracted(id: number, notes: string, contract_file: any): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'hire',
      notes,
      application: {
        contract_file_id: contract_file,
      },
    });
  }

  changeStatusToSuspended(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'suspend',
      notes,
    });
  }

  changeStatusToClosed(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'close',
      notes,
    });
  }

  changeStatusToQuiting(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'quit',
      notes,
    });
  }

  changeStatusToWithdrawal(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'withdrawal',
      notes,
    });
  }

  changeStatusToReanalyse(id: number, notes: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action: 'reanalyse',
      notes,
      application: {
        bike_id: null,
        decision: null,
      },
    });
  }

  adminAccept(id: number, notes: string = 'Aprovado pelo administrador'): Observable<Resource<ApplicationModel>> {
    return this.adminAcceptRefuse(id, notes, 'accept');
  }

  adminReject(id: number, notes: string = 'Rejeitado pelo administrador'): Observable<Resource<ApplicationModel>> {
    return this.adminAcceptRefuse(id, notes, 'reject');
  }

  private adminAcceptRefuse(
    id: number,
    notes: string,
    action: 'accept' | 'reject',
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID_STATUS', { id }), {
      action,
      notes,
    });
  }

  getQuestionnairePdf(application_id: number): Observable<any> {
    return this.resourceService.create<TemplatePrintModel>(this.urlService.get('U_BIKE.PRINT_QUEST'), {
      application_id: application_id,
    });
  }

  print(data: ListPrint): Observable<Resource<ListPrint>> {
    return this.resourceService.create<ListPrint>(this.urlService.get('U_BIKE.PRINT'), data);
  }

  stats(): Observable<Resource<ApplicationStatsModel>> {
    return this.resourceService.read<ApplicationStatsModel>(this.urlService.get('U_BIKE.APPLICATIONS_STATS', {}), {});
  }

  removeDocument(id: number): Observable<Resource<any>> {
    return this.resourceService.delete(
      this.urlService.get('U_BIKE.APPLICATION_REMOVE_DOCUMENTS', {
        id: id,
      })
    );
  }

  saveDocument(application_id: number, file_id: number, name: string): Observable<Resource<any>> {
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.APPLICATION_SAVE_DOCUMENTS'), {
      application_id: application_id,
      file_id: file_id,
      name: name,
    });
  }

  listDocumentsApplication(application_id: number): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.set('query[application_id]', application_id.toString());
    return this.resourceService.list<any>(this.urlService.get('U_BIKE.APPLICATION_DOCUMENTS'), { params });
  }

  generateApplicationReport(data): Observable<Resource<any>>{
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.APPLICATIONS_REPORT'), data );
  }
  
  getConsentTermsFile(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.GET_CONSENT_TERMS_FILE',  { id }), {});
  }

  getDeliveryReceptionFile(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.GET_DELIVERY_RECEPTION_FILE',  { id }), {});
  }

  submitConsentDeliveryFile(id:number, data: any): Observable<Resource<any>> {
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.SUBMIT_CONSENT_DELIVERY_REPORTS', { id }), {...data});
  }
}
