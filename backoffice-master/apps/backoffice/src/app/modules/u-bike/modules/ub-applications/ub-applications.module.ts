
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UbApplicationsRoutingModule } from './ub-applications-routing.module';
import { ListApplicationsComponent } from './pages/list-applications/list-applications.component';
import { ViewApplicationsComponent } from './pages/view-applications/view-applications.component';
import { PanelStatsComponent } from './pages/panel-stats/panel-stats.component';
import { ApplicationReportsModalComponent } from './components/application-reports-modal/application-reports-modal.component';

@NgModule({
  declarations: [
    ListApplicationsComponent,
    ViewApplicationsComponent,
    PanelStatsComponent,
    ApplicationReportsModalComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UbApplicationsRoutingModule
  ],
  entryComponents: [
    ApplicationReportsModalComponent
  ]
})
export class UbApplicationsModule {}
