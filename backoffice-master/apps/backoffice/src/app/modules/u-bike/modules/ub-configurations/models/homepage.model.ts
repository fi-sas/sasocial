export class HomepageModel {
  key? : string;
  value: Value;
}

export class Value {
  cover: string;
  title: string;
  text: string[];
  conditions: string;
}
