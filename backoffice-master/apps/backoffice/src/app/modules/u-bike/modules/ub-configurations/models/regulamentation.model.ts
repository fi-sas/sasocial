export class RegulamentationModel {
  key? : string;
  value: Value;
}

export class Value {
  language_id: number;
  regulamentation: string[]
}
