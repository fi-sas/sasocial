import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { first, finalize } from 'rxjs/operators';

import * as moment from 'moment';

import { ConfigurationsService } from '../../services/configurations.service';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { GeralSettingsModel } from '../../models/geral-settings.model';

@Component({
  selector: 'fi-sas-form-configurations',
  templateUrl: './form-configurations.component.html',
  styleUrls: ['./form-configurations.component.less'],
})
export class FormConfigurationsComponent implements OnInit {
  configurations = null;
  form: FormGroup;
  isLoading = false;

  constructor(
    private configurationService: ConfigurationsService,
    private route: ActivatedRoute,
    private uiService: UiService
  ) {
    this.form = new FormGroup({
      application_date: new FormControl(null, Validators.required),
      //total_number_bikes: new FormControl(1),
      missing_trips: new FormControl(null, [Validators.min(0)]),
      notification_hour: new FormControl(null),
      notifications_antecedence_days: new FormControl(null),
      notifications_frequency: new FormControl(null),
    });
  }

  ngOnInit() {
    this.configurations = this.route.snapshot.data.configurations;

    if (this.configurations !== null) {
      let start_date =
        this.configurations.application_start_date !== undefined
          ? this.configurations.application_start_date
          : null;
      let end_date =
        this.configurations.application_end_date !== undefined
          ? this.configurations.application_end_date
          : null;

      this.form.patchValue({
        application_date: [
          moment(start_date).isValid()
            ? moment(start_date).toDate()
            : new Date(),
          moment(end_date).isValid() ? moment(end_date).toDate() : new Date(),
        ],
      });
    }

    this.getConfigs();
  }

  getConfigs() {
    this.isLoading = true;
    this.configurationService
      .list(0, 0)
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((configs) => {
        this.form
          .get('missing_trips')
          .setValue(configs.data[0].APPLICATION_MISSING_TRIPS);
        this.form
          .get('notification_hour')
          .setValue(
            configs.data[0].APPLICATION_NOTIFICATION_HOUR
              ? moment(configs.data[0].APPLICATION_NOTIFICATION_HOUR).isValid()
                ? moment(configs.data[0].APPLICATION_NOTIFICATION_HOUR).toDate()
                : null
              : null
          );
        this.form
          .get('notifications_antecedence_days')
          .setValue(
            configs.data[0].APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS
          );
        this.form
          .get('notifications_frequency')
          .setValue(
            configs.data[0].APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY
          );

        this.form.patchValue({
          application_date: [
            moment(configs.data[0].APPLICATION_START_DATE).isValid()
              ? moment(configs.data[0].APPLICATION_START_DATE).toDate()
              : new Date(),
            moment(configs.data[0].APPLICATION_END_DATE).isValid()
              ? moment(configs.data[0].APPLICATION_END_DATE).toDate()
              : new Date(),
          ],
        });
      });
  }

  submit() {
    this.isLoading = true;
    let sendValue: GeralSettingsModel = new GeralSettingsModel();
    if (!this.form.valid) {
      this.isLoading = false;
      Object.keys(this.form.controls).forEach((key) => {
        this.form.controls[key].markAsDirty();
        this.form.controls[key].updateValueAndValidity();
      });
      this.form.markAsDirty();
      return;
    }
    if (this.form.valid) {
      sendValue.APPLICATION_MISSING_TRIPS = this.form.get(
        'missing_trips'
      ).value;
      sendValue.APPLICATION_NOTIFICATION_HOUR = this.form.get(
        'notification_hour'
      ).value
        ? moment(this.form.get('notification_hour').value).toISOString()
        : moment(new Date()).toString();
      sendValue.APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS = this.form.get(
        'notifications_antecedence_days'
      ).value;
      sendValue.APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY = this.form.get(
        'notifications_frequency'
      ).value;
      sendValue.APPLICATION_START_DATE = this.form.get('application_date')
        .value[0]
        ? moment(this.form.get('application_date').value[0]).toISOString()
        : null;
      sendValue.APPLICATION_END_DATE = this.form.get('application_date')
        .value[1]
        ? moment(this.form.get('application_date').value[1]).toISOString()
        : null;
      this.configurationService
        .create(sendValue)
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Configurações alteradas com sucesso'
          );
        });
    } else {
      this.isLoading = false;
    }
  }
}
