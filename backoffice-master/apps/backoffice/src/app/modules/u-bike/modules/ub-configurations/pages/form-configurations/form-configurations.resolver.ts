import { Injectable } from '@angular/core';

import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { ConfigurationsService } from '../../services/configurations.service';
import { CONFIGURATIONS } from '../../utils/configurations.util';
@Injectable()
export class FormConfigurationsResolver implements Resolve<Observable<Resource<any>>> {
  constructor(private configurationsService: ConfigurationsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return Observable.create(
      (o: any) => {
        class DataOutModel {
          application_start_date: any;
          application_end_date: any;
          total_number_bikes: any;
        }

        const data_out = new DataOutModel();

        this.configurationsService
          .getConfigByKey(CONFIGURATIONS.APPLICATION_START_DATE)
          .pipe(first())
          .subscribe(
            (start_date) => {
              data_out.application_start_date = start_date.data;

              this.configurationsService
                .getConfigByKey(CONFIGURATIONS.APPLICATION_END_DATE)
                .pipe(first())
                .subscribe(
                  (end_date) => {
                    data_out.application_end_date = end_date.data;
                    o.next(data_out);
                    o.complete();
                  },
                  (error2) => {
                    o.error(error2);
                  }
                );
            },
            (error3) => {
              o.error(error3);
            }
          );
      },
      () => {
        return EMPTY;
      }
    );
  }
}
