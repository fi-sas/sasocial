import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ConfigurationModel } from '../models/configuration.model';
import { GeralSettingsModel } from '../models/geral-settings.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationsService extends Repository<GeralSettingsModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'U_BIKE.CONFIGURATIONS';
  }

  getConfigByKey(key: string ): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.read<ConfigurationModel>(this.urlService.get('U_BIKE.CONFIGURATIONS_BY_KEY', { key }));
  }

}
