import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';
import { FormConfigurationsResolver } from './pages/form-configurations/form-configurations.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'form-configuration', pathMatch: 'full' },
  {
    path: 'form-configuration',
    component: FormConfigurationsComponent,
    data: {
      breadcrumb: 'Configurações',
      title: 'Configurações',
      scope: 'u_bike:configs:read',
    },
    resolve: { configurations: FormConfigurationsResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbConfigurationsRoutingModule { }
