import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UbConfigurationsRoutingModule } from './ub-configurations-routing.module';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';
import { FormConfigurationsResolver } from './pages/form-configurations/form-configurations.resolver';

@NgModule({
  declarations: [
    FormConfigurationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UbConfigurationsRoutingModule
  ],
  providers: [
    FormConfigurationsResolver
  ]
})
export class UbConfigurationsModule { }
