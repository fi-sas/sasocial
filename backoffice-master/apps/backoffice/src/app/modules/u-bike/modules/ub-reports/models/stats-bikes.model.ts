
export class StatsBikesModel {
  total_bikes?: number;
  bikes_in_use?: number;
  bikes_usage_perc?: number;
  aggregate_data: AggregateData;
}


class AggregateData {
  profile_name?: string;
  typology_name?: string;
  is_holiday?: number;
  is_weekend?: number;
  count_trips: number;
  sum_distance: number;
  sum_time: number;
  average_time: number;
  sum_energy: number;
  sum_watts: number;
  sum_co2_emissions_saved: number;
  average_height_diff: number;
  max_height_diff: number;
  average_avg_speed: number;
  max_avg_speed: number;
  bus_saved_co2: number;
  train_saved_co2: number;
  gasoline_saved_co2: number;
  diesel_saved_co2: number;
  gpl_saved_co2: number;
  eletric_saved_co2: number;
}

export class ArrayBikeData {
  aggregate = [
    {key: 'profile_name', pt: 'Perfil'},
    {key: 'typology_name', pt: 'Tipologia'},
    {key: 'is_holiday', pt: 'Fim de Semana'},
    {key: 'is_weekend', pt: 'Feriado'}
  ];
}
