export class StatsProjectModel {
  total_users: number;
  users_enrolled: number;
  users_enrolled_perc: number;
  aggregate_data?: AggregateData;
  totalNumberUsers: number;
}

class AggregateData {
  profile_name: string;
  total_users_profile: number;
  profile_users_perc_of_total: number;
  profile_bike_users: number;
  profile_adherence_perc: number;
}
