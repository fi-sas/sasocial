import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { StatsProjectModel } from '../../models/stats-project.model';
import { ReportsService } from '../../services/reports.service';

@Component({
  selector: 'fi-sas-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.less'],
})
export class ViewProjectComponent implements OnInit {
  project: StatsProjectModel = null;

  constructor(
    private reports : ReportsService
  ) {}

  ngOnInit() {
    this.reports.statsProject().pipe(first()).subscribe(project => {
      this.project = project.data[0];
    });
  }
}
