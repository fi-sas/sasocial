import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as moment from 'moment';

import { finalize, first } from 'rxjs/operators';
import { StatsBikesModel, ArrayBikeData } from '../../models/stats-bikes.model';
import { Bike } from '../../../bike/models/bike.model';
import { ApplicationModel } from '../../../ub-applications/models/application.model';
import { TripModel } from '../../../trip/models/trip.model';
import { BikesService } from '../../../bike/services/bikes.service';
import { ApplicationsService } from '../../../ub-applications/services/applications.service';
import { ReportsService } from '../../services/reports.service';
import { TripsService } from '../../../trip/services/trips.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';

@Component({
  selector: 'fi-sas-view-stats',
  templateUrl: './view-stats.component.html',
  styleUrls: ['./view-stats.component.less'],
})
export class ViewStatsComponent implements OnInit {
  stats: StatsBikesModel = null;
  bikes: Bike[] = [];
  applications: ApplicationModel[] = [];
  aggregation = ArrayBikeData;
  organicUnits: OrganicUnitsModel [] = [];
  loadingOrganicUnits: boolean = false;

  loadingStats = false;
  loadingBikes = false;
  loadingApplications = false;
  loadingReportFile = false;

  application: ApplicationModel = null;
  startDate: string = null;
  endDate: string = null;
  aggregate: string = null;
  trip: TripModel = null;

  //APPLICATIONS REPORT
  userApplicationReport: ApplicationModel = null;
  bikeAppReport = null;
  dateRangeAppReport = null;
  generatingReportLoading = false;
  organicUnitApplicationReport: OrganicUnitsModel = null;

  //KMS REPORT
  dateRangeKmsReport = null;
  generatingKmsReportLoading = false;

  //LIST BIKES REPORT
  listBikesReportDataRange = null;
  listBikesReportLoading = false;

  statistics = {
    bikes_in_use: 0,
    total_bikes: 0,
    bikes_usage_perc: 0,
  };

  grid = {
    nzXs: 24,
    nzSm: 24,
    nzMd: 12,
    nzLg: 12,
    nzXl: 8,
    nzXXl: 6,
  }

  bikeID: number = null;

  constructor(
    private bikesServices: BikesService,
    private applicationsServices: ApplicationsService,
    private reportsServices: ReportsService,
    private tripsServices: TripsService,
    private uiService: UiService,
    private organicUnitsService: OrganicUnitsService,
  ) { }

  ngOnInit() {

    this.reportsServices.statsBike().pipe(first()).subscribe(stats => {
      this.stats = stats.data[0];
      this.statistics.bikes_in_use = this.stats.bikes_in_use;
      this.statistics.total_bikes = this.stats.total_bikes;
      this.statistics.bikes_usage_perc = this.stats.bikes_usage_perc;
    })

    this.getBikes();
    this.getApplications();
    this.getOrganicUnits();
  }


  getBikes() {
    this.loadingBikes = true;
    this.bikesServices
      .listBikes(1, -1)
      .pipe(first())
      .subscribe(
        (bikes) => {
          this.bikes = bikes.data;
          this.loadingBikes = false;
        },
        () => {
          this.loadingBikes = false;
        }
      );
  }

  getApplications() {
    this.loadingApplications = true;
    this.applicationsServices
      .list(1, -1, null, null, {
        status: 'contracted',
        withRelated: 'user'
      })
      .pipe(first())
      .subscribe(
        (applications) => {
          this.applications = applications.data;
          this.loadingApplications = false;
        },
        () => {
          this.loadingApplications = false;
        }
      );
  }

  getStats() {
    let user;
    if (this.application) {
      user = this.application.user_id;
    }
    this.loadingStats = true;
    this.reportsServices
      .statsBike(
        this.bikeID,
        user,
        this.startDate,
        this.endDate,
        this.aggregate
      )
      .pipe(first())
      .subscribe(
        (stats) => {
          this.stats = stats.data[0];
          this.loadingStats = false;
        },
        () => {
          this.loadingStats = false;
        }
      );
  }

  async getReport() {
    let user;
    if (this.application) {
      user = this.application.user_id;
    }
    let trip_params = {};
    trip_params = Object.assign(trip_params, {
      withRelated: 'application,bike',
    });
    if(this.bikeID) {
      trip_params = Object.assign(trip_params, {
        bike_id: this.bikeID,
      });
    }
    if(user) {
      trip_params = Object.assign(trip_params, {
        user_id: user,
      });
    }
    
    this.loadingReportFile = true;
    this.tripsServices
      .list(1, 1, 'trip_start', 'ascend', trip_params)
      .pipe(first())
      .subscribe((trip) => {
          this.report(trip,user)
      });
  }

  report(trip,user){
    let start_date, end_date;
    let tripData: TripModel = null;
    tripData = trip.data.length !== 0 ? trip.data[0] : null;
    if (this.startDate === null) {
      start_date =
        trip.data.length !== 0
          ? moment(trip.data[0].trip_start).hour(0).minute(0).toISOString()
          : moment(new Date()).hour(0).minute(0).toISOString();
    } else {
      start_date = moment(this.startDate).hour(23).minute(59).toISOString();
    }
    if (this.endDate === null) {
      end_date = moment(new Date()).hour(23).minute(59).toISOString();
    } else {
      end_date = moment(this.endDate).hour(23).minute(59).toISOString();;
    }
    this.reportsServices
      .getReportPdf(user, this.bikeID, start_date, end_date)
      .pipe(first(),finalize(()=>this.loadingReportFile = false))
      .subscribe(
        () => {
          this.uiService.showMessage(
            MessageType.success,
            'Documento gerado com sucesso'
          );
        },
      );
  }

  
  getOrganicUnits() {
    this.loadingOrganicUnits = true;
    this.organicUnitsService
      .list(1, -1, 'name', 'ascend', {
        active: true,
      })
      .pipe(
        first(),
        finalize(() => (this.loadingOrganicUnits = false))
      )
      .subscribe((results) => {
        this.organicUnits = results.data;
      });
  }

  generateApplicationsReport(){
    this.generatingReportLoading = true;
    this.applicationsServices.generateApplicationReport( this.fillApplicationReportObj()).pipe(first(), finalize(() => this.generatingReportLoading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        'Listagem de candidaturas gerado com sucesso'
      );
    })
  }

  generateKmsReport(){
    this.generatingKmsReportLoading = true;
    const data:any = {}
    if(this.dateRangeKmsReport) {
      data.start_date = this.dateRangeKmsReport[0];
      data.end_date = this.dateRangeKmsReport[1];
    }
    this.tripsServices.generateKmsReport(data).pipe(first(), finalize(() => this.generatingKmsReportLoading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        'Relatório Quilómetros percorridos gerado com sucesso'
      );
    })
  }

  generatelistBikesReport(){
    this.listBikesReportLoading = true;
    const data:any = {}
    if(this.listBikesReportDataRange) {
      data.start_date = this.listBikesReportDataRange[0];
      data.end_date = this.listBikesReportDataRange[1];
    }
    this.bikesServices.generateBikesListReport(data).pipe(first(), finalize(() => this.listBikesReportLoading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        'Listagem Bicicletas gerado com sucesso'
      );
    })
  }

  fillApplicationReportObj(){
    const obj: any = {}
    if(this.userApplicationReport) obj.user_id = this.userApplicationReport.user_id;
    if(this.bikeAppReport) obj.bike_id = this.bikeAppReport;
    if(this.dateRangeAppReport){
      obj.start_date = this.dateRangeAppReport[0];
      obj.end_date = this.dateRangeAppReport[1];
    }
    if(this.organicUnitApplicationReport) obj.organic_unit_id = this.organicUnitApplicationReport.id;
    return obj
  }

  listComplete() {
    this.startDate = null;
    this.endDate = null;
    this.application = null;
    this.bikeID = null;
    this.getStats();
  }
}
