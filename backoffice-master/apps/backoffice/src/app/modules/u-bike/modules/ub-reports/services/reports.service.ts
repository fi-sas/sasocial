import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { TemplatePrintModel } from '@fi-sas/backoffice/modules/reports/models/template-print.model';
import { first, map } from 'rxjs/operators';
import { StatsProjectModel } from '../models/stats-project.model';
import { StatsBikesModel } from '../models/stats-bikes.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  statsProject(): Observable<Resource<StatsProjectModel>> {
    return this.resourceService.read<StatsProjectModel>(
      this.urlService.get('U_BIKE.STATS_PROJECT')
    );
  }

  statsBike(
    bike_id?: number,
    user_id?: number,
    start_date?: string,
    end_date?: string,
    aggregation?: string
  ): Observable<Resource<StatsBikesModel>> {
    let params = new HttpParams();
    if (user_id) {
      params = params.set('user_id', user_id.toString());
    }

    if (start_date) {
      params = params.set('start_date', moment(start_date).hour(0).minute(0).toISOString());
    }

    if (bike_id) {
      params = params.set('bike_id', bike_id.toString());
    }

    if (end_date) {
      params = params.set('end_date', moment(end_date).hour(23).minute(59).toISOString());
    }

    if (aggregation) {
      params = params.set('aggregation', aggregation);
    }

    return this.resourceService.read<StatsBikesModel>(
      this.urlService.get('U_BIKE.STATS_BIKE', {}),
      { params }
    );
  }

  getReportPdf(
    user_id,
    bike_id,
    start_date: string,
    end_date: string,
  ): Observable<any> {
    return this.resourceService.create<any>( this.urlService.get("U_BIKE.APPLICATION_STATS_PRINT"), {
      user_id: user_id,
      bike_id: bike_id,
      start_date: start_date,
      end_date: end_date,
    });
  }
}
