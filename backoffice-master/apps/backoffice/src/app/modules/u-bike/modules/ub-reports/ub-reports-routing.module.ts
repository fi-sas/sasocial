import { ViewProjectComponent } from './pages/view-project/view-project.component';
import { ViewStatsComponent } from './pages/view-stats/view-stats.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'view-project', pathMatch: 'full' },
  {
    path: 'view-project',
    component: ViewProjectComponent,
    data: {
      breadcrumb: 'Dados Gerais',
      title: 'Dados Gerais',
      scope: 'u_bike:stats:project',
    },
  },
  {
    path: 'view-stats',
    component: ViewStatsComponent,
    data: {
      breadcrumb: 'Reportes',
      title: 'Reportes',
      scope: 'u_bike:stats:bikes',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UbReportsRoutingModule { }
