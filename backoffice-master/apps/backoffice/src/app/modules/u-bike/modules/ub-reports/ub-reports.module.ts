import { ViewStatsComponent } from './pages/view-stats/view-stats.component';
import { ViewProjectComponent } from './pages/view-project/view-project.component';
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UbReportsRoutingModule } from './ub-reports-routing.module';

@NgModule({
  declarations: [
    ViewProjectComponent,
    ViewStatsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UbReportsRoutingModule
  ],
  providers: [
  ]
})
export class UbReportsModule { }
