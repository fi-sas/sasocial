import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCommute'
})
export class FilterCommutePipe implements PipeTransform {

  transform(value: any, commute_name: string): any {
    return value.find(commute => commute.key === commute_name);
  }

}
