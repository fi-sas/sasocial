import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterConsumption'
})
export class FilterConsumptionPipe implements PipeTransform {

  transform(value: any, consumption_name: string): any {
    return value.find(consumption => consumption.key === consumption_name);
  }

}
