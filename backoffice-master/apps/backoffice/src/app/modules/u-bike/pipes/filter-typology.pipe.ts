import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTypology'
})
export class FilterTypologyPipe implements PipeTransform {

  transform(value: any, typology_id: number): any {

    return value.find(typology => typology.id === typology_id);
  }

}
