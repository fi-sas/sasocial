import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UBikeComponent } from "@fi-sas/backoffice/modules/u-bike/u-bike.component";
import { PageNotFoundComponent } from "@fi-sas/backoffice/components/page-not-found/page-not-found.component";
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [{

  path: '',
  component: UBikeComponent,
  children: [
    {
      path: 'initial-page',
      component: InitialPageComponent,
      
    },
    { path: '', redirectTo: 'initial-page', pathMatch: 'full' },

    {
      path: 'applications',
      loadChildren: './modules/ub-applications/ub-applications.module#UbApplicationsModule',
      data: { breadcrumb: 'Candidaturas', title: 'Candidaturas'},
    },

    {
      path: 'application-forms',
      loadChildren: './modules/application-forms/application-forms.module#ApplicationFormsModule',
      data: { breadcrumb: 'Formulários', title: 'Formulários'},
    },

    {
      path: 'typologies',
      loadChildren: './modules/bike-typology/bike-typology.module#BikeTypologyModule',
      data: { breadcrumb: 'Tipologias', title: 'Tipologias'},
    },

    {
      path: 'bike-model',
      loadChildren: './modules/bike-model/bike-model.module#BikeModelModule',
      data: { breadcrumb: 'Modelos de Bicicletas', title: 'Modelos de Bicicletas'},
    },

    {
      path: 'bike',
      loadChildren: './modules/bike/bike.module#BikeModule',
      data: { breadcrumb: 'Bicicletas', title: 'Bicicletas'},
    },

    {
      path: 'trip',
      loadChildren: './modules/trip/trip.module#TripModule',
      data: { breadcrumb: 'Percursos', title: 'Percursos'},
    },

    {
      path: 'reports',
      loadChildren: './modules/ub-reports/ub-reports.module#UbReportsModule',
      data: { breadcrumb: 'Reportes', title: 'Reportes'},
    },


    {
      path: 'configurations',
      loadChildren: './modules/ub-configurations/ub-configurations.module#UbConfigurationsModule',
      data: { breadcrumb: 'Configurações', title: 'Configurações' },
    },

    {
      path: '**',
      component: PageNotFoundComponent,
      data: {
        breadcrumb: 'Página não encontrada',
        title: 'Página não encontrada'
      }
    }
  ],
  data: { breadcrumb: null, title: null }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UBikeRoutingModule { }
