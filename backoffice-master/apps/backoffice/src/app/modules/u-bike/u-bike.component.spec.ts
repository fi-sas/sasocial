import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UBikeComponent } from './u-bike.component';

describe('UBikeComponent', () => {
  let component: UBikeComponent;
  let fixture: ComponentFixture<UBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
