import { Component, OnInit } from '@angular/core';
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { SiderItem } from "@fi-sas/backoffice/core/models/sider-item";
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-u-bike',
  template: '<router-outlet></router-outlet>'
})
export class UBikeComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {

    this.uiService.setSiderTitle(this.validTitleTraductions(13), 'bold');

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    const panel = new SiderItem(
      'Painel',
      '',
      '/u-bike/applications/stats',
      'u_bike:applications:read'
    );
    this.uiService.addSiderItem(panel);

    const applications = new SiderItem('Candidaturas', '', '', 'u_bike:applications');
    applications.addChild(new SiderItem('Listar', '', '/u-bike/applications/list', 'u_bike:applications:read'));
    applications.addChild(new SiderItem('Criar', '', '/u-bike/application-forms/create', 'u_bike:applications:create'));
    this.uiService.addSiderItem(applications);

    const applicationForms = new SiderItem('Formulários', '', '', 'u_bike:application-forms');
    applicationForms.addChild(new SiderItem('Reclamações', '', '/u-bike/application-forms/complaints', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Ausências', '', '/u-bike/application-forms/absences', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Renovações', '', '/u-bike/application-forms/renovations', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Pedidos de Manutenção', '', '/u-bike/application-forms/maintenance-request', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Kit de Manutenção Adicional', '', '/u-bike/application-forms/additional-maintenance_kit', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Roubos', '', '/u-bike/application-forms/thefts', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Manutenções Privadas', '', '/u-bike/application-forms/private-maintenance', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Comunicação de Avarias', '', '/u-bike/application-forms/breakdown-communication', 'u_bike:application-forms:read'));
    applicationForms.addChild(new SiderItem('Desistências', '', '/u-bike/application-forms/withdrawal', 'u_bike:application-forms:read'));

    this.uiService.addSiderItem(applicationForms);

    const typologies = new SiderItem('Tipologias', '', '', 'u_bike:typologies');
    typologies.addChild(new SiderItem('Criar', '', '/u-bike/typologies/create', 'u_bike:typologies:create'));
    typologies.addChild(new SiderItem('Listar', '', '/u-bike/typologies/list', 'u_bike:typologies:read'));
    this.uiService.addSiderItem(typologies);

    const bikesModel = new SiderItem('Modelo de Bicicletas', '', '', 'u_bike:bike_models');
    bikesModel.addChild(new SiderItem('Criar', '', '/u-bike/bike-model/create', 'u_bike:bike_models:create'));
    bikesModel.addChild(new SiderItem('Listar', '', '/u-bike/bike-model/list', 'u_bike:bike_models:read'));
    this.uiService.addSiderItem(bikesModel);

    const bikes = new SiderItem('Bicicletas', '', '', 'u_bike:bikes');
    bikes.addChild(new SiderItem('Criar', '', '/u-bike/bike/create', 'u_bike:bikes:create'));
    bikes.addChild(new SiderItem('Listar', '', '/u-bike/bike/list', 'u_bike:bikes:read'));
    this.uiService.addSiderItem(bikes);

    const trips = new SiderItem('Percursos', '', '', 'u_bike:bikes');
    trips.addChild(new SiderItem('Listar', '', '/u-bike/trip/list', 'u_bike:bikes:read'));
    this.uiService.addSiderItem(trips);

    const reports = new SiderItem('Monitorizações e Reportes', '', '', 'u_bike:stats');
    reports.addChild(new SiderItem('Dados Gerais', '', '/u-bike/reports/view-project', 'u_bike:stats:project'));
    reports.addChild(new SiderItem('Reportes', '', '/u-bike/reports/view-stats', 'u_bike:stats:bikes'));
    this.uiService.addSiderItem(reports);

    const configurations = new SiderItem('Configurações');
    configurations.addChild(new SiderItem('Gerais', '', '/u-bike/configurations/form-configuration'));
    this.uiService.addSiderItem(configurations);



    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }

}
