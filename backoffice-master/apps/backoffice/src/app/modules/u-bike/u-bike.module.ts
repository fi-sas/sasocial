import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UBikeRoutingModule } from './u-bike-routing.module';
import { NgxMaskModule } from "ngx-mask";
import { SharedModule } from "@fi-sas/backoffice/shared/shared.module";
import { UBikeComponent } from "@fi-sas/backoffice/modules/u-bike/u-bike.component";
import { FilterTypologyPipe } from './pipes/filter-typology.pipe';
import { FilterCommutePipe } from './pipes/filter-commute.pipe';
import { FilterConsumptionPipe } from './pipes/filter-consumption.pipe';
import { FilterStatusPipe } from './pipes/filter-status.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UBikeRoutingModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    UBikeComponent,
    FilterTypologyPipe,
    FilterCommutePipe,
    FilterConsumptionPipe,
    FilterStatusPipe,
  ]

})
export class UBikeModule { }
