import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { finalize, first } from 'rxjs/operators';
import { ScopeModel } from '../../modules/scopes/models/scope.model';
import { ScopesService } from '../../modules/scopes/services/scopes.service';

@Component({
  selector: 'fi-sas-scope-tree',
  templateUrl: './scope-tree.component.html',
  styleUrls: ['./scope-tree.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScopeTreeComponent),
      multi: true,
    },
  ],
})
export class ScopeTreeComponent implements OnInit, ControlValueAccessor {
  disabled = false;
  scopesLoading = false;
  tree: ScopeModel[] = [];
  expanded = {};
  selected = {};
  indeterminated = {};

  // Function to call when the value changes.
  onChange = (ids: number[]) => {};
  // Function to call when the input is touched (when a scope is clicked).
  onTouched = () => {};

  constructor(public scopesService: ScopesService) {}

  get value(): number[] {
    return Object.keys(this.selected)
      .map((id) => parseInt(id))
      .filter((id) => this.selected[id] === true);
  }

  writeValue(obj: any): void {
    this.selected = {};

    if (Array.isArray(obj)) {
      obj.map((id) => {
        this.selected[id] = true;
      });
      this.onChange(this.value);
    } else {
      throw new Error('Obj must be a array.');
    }
  }

  registerOnChange(fn: (ids: number[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  ngOnInit() {
    this.loadScopes();
  }

  updateValue(scope?: ScopeModel) {
    if (scope && this.selected[scope.id]) {
      this.selectChilds(scope);
    } else {
      this.deselectChilds(scope);
    }

    this.checkInderdeminated(this.tree);

    this.onChange(this.value);
  }

  selectChilds(scope: ScopeModel) {
    scope.children.forEach((child_Scope) => {
      this.selected[child_Scope.id] = true;
      this.selectChilds(child_Scope);
    });
  }

  deselectChilds(scope: ScopeModel) {
    scope.children.forEach((child_Scope) => {
      this.selected[child_Scope.id] = false;
      this.deselectChilds(child_Scope);
    });
  }

  checkInderdeminated(scopes: ScopeModel[]) {
    scopes.forEach((s) => {

      if (!this.selected[s.id]) {
        this.checkInderdeminated(s.children);

        const childsIndeterminated = s.children.filter((c) => {
          if (this.indeterminated[c.id]) {
            return true;
          }
          return false;
        });

        if (childsIndeterminated.length > 0) {
          this.indeterminated[s.id] = true;
        } else {
          const selected = s.children.filter((c) => {
            if (this.selected[c.id]) {
              return true;
            }
            return false;
          });

          if (selected.length !== 0 && selected.length < s.children.length) {
            this.indeterminated[s.id] = true;
          } else if (selected.length === 0 || selected.length === s.children.length) {
            if(selected.length === s.children.length && s.children.length > 0) {
              this.selected[s.id] = true;
            }
            this.indeterminated[s.id] = false;
          }
        }
      }
    });
  }

  loadScopes() {
    this.scopesLoading = true;
    this.scopesService
      .tree()
      .pipe(
        first(),
        finalize(() => (this.scopesLoading = false))
      )
      .subscribe((results) => {
        this.tree = results.data;
        this.checkInderdeminated(this.tree);
      });
  }
}
