import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDepartmentsComponent } from './pages/form-department/form-department.component';
import { ListDepartmentsComponent } from './pages/list-departments/list-departments.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListDepartmentsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:departments:read' }
  },
  {
    path: 'create',
    component: FormDepartmentsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:departments:create' }
  },
  {
    path: 'update/:id',
    component: FormDepartmentsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:departments:update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentsRoutingModule { }
