import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { DepartmentService } from './services/departments.service';
import { DepartmentsRoutingModule } from './departments-routing.module';
import { ListDepartmentsComponent } from './pages/list-departments/list-departments.component';
import { FormDepartmentsComponent } from './pages/form-department/form-department.component';

@NgModule({
  declarations: [
    FormDepartmentsComponent,
    ListDepartmentsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DepartmentsRoutingModule,
  ],
  providers: [
    DepartmentService
  ]
})
export class DepartmentModule { }
