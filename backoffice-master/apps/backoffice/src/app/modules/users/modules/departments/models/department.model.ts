export class DepartmentModel {
    id: number;
    name: string;
    organic_unit_id: number;
    active: boolean;
}