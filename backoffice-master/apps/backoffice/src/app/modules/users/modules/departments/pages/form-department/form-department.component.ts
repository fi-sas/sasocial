import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { DepartmentModel } from "../../models/department.model";
import { DepartmentService } from "../../services/departments.service";

@Component({
    selector: 'fi-sas-form-department',
    templateUrl: './form-department.component.html',
    styleUrls: ['./form-department.component.less']
})
export class FormDepartmentsComponent implements OnInit {

    organicsList: OrganicUnitsModel[] = [];
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        organic_unit_id: new FormControl(null, [Validators.required]),
        active: new FormControl(true, [Validators.required]),
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute,
        private organicUnitsService: OrganicUnitsService,
        private uiService: UiService, private router: Router,
        private departmentService: DepartmentService) {

    }

    ngOnInit() {
        this.getOrganicUnits();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getDepartmentId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getDepartmentId(id) {
        let department: DepartmentModel = new DepartmentModel();
        this.departmentService
            .read(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                department = results.data[0];
                this.formCreate.patchValue({
                    ...department
                });
            });
    }

    getOrganicUnits() {
        this.organicUnitsService.list(1, -1, null, null, { active: true , sort:'name'}).pipe(
            first()
        ).subscribe(results => {
            this.organicsList = results.data;
        });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
            this.isLoading = true;
            if (this.isEdit) {

                this.departmentService.update(this.id, this.formCreate.value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Departamento alterado com sucesso'
                        );
                        this.returnButton();
                    },
                )
            } else {
                this.departmentService.create(this.formCreate.value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Departamento criado com sucesso'
                        );
                        this.returnButton();
                    },
                )
            }
        }
    }



    returnButton() {
        this.router.navigateByUrl('users/departments/list');
    }
}