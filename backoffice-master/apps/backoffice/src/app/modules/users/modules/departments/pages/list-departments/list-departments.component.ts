import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { DepartmentModel } from "../../models/department.model";
import { DepartmentService } from "../../services/departments.service";

@Component({
    selector: 'fi-sas-list-departments',
    templateUrl: './list-departments.component.html',
    styleUrls: ['./list-departments.component.less']
})
export class ListDepartmentsComponent extends TableHelper implements OnInit {
    status = [];
    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private modalService: NzModalService,
        private authService: AuthService,
        public departmentsService: DepartmentService) {
        super(uiService, router, activatedRoute);
        this.status = [
            {
              description: "Ativo",
              value: true
            },
            {
              description: "Desativo",
              value: false
            }
          ];
          this.persistentFilters = {
            searchFields: 'name',
          };

    }

    ngOnInit() {
        this.initTableData(this.departmentsService);
    }

    edit(id: number) {
        if (!this.authService.hasPermission('authorization:departments:update')) {
            return;
        }
        this.router.navigateByUrl('/users/departments/update/' + id);
    }

    listComplete() {
        this.filters.active = null;
        this.filters.search = null;
        this.searchData(true);
      }

    desactive(data: DepartmentModel) {
        if (!this.authService.hasPermission('authorization:departments:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai desativar este Departamento. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(data)
        });

    }

    desactiveDataSubmit(data: DepartmentModel) {
        data.active = false;
        this.departmentsService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Departamento desativado com sucesso'
                );
                this.initTableData(this.departmentsService);
            });
    }

    active(data: DepartmentModel) {
        if (!this.authService.hasPermission('authorization:departments:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai ativar este Departamento. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(data)
        });
    }

    activeDataSubmit(data: DepartmentModel) {
        data.active = true;
        this.departmentsService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Departamento ativado com sucesso'
                );
                this.initTableData(this.departmentsService);
            });
    }

}
