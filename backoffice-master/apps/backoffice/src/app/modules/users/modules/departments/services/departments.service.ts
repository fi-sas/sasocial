import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { DepartmentModel } from '../models/department.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends Repository<DepartmentModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.DEPARTMENTS_ID';
    this.entities_url = 'USERS.DEPARTMENTS';
  }

  departmentsArray(organic_unit_id?: number[]): Observable<Resource<DepartmentModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('active', 'true');
    
    if (organic_unit_id.length>0) {
      organic_unit_id.forEach((organic)=> {
        params = params.append('query[organic_unit_id]', organic.toString());
      })
     
    }
    return this.resourceService.list<DepartmentModel>(this.urlService.get('USERS.DEPARTMENTS'), { params });
  }
}
