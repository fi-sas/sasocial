import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDocumentTypeComponent } from './pages/form-document-type/form-document-type.component';
import { ListDocumentTypeComponent } from './pages/list-document-type/list-document-type.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListDocumentTypeComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:document-types:read' }
  },
  {
    path: 'create',
    component: FormDocumentTypeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:document-types:create' }
  },
  {
    path: 'edit/:id',
    component: FormDocumentTypeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:document-types:update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentTypeRoutingModule { }
