import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { DocumentTypeService } from './services/document-type.service';
import { DocumentTypeRoutingModule } from './document-type-routing.module';
import { FormDocumentTypeComponent } from './pages/form-document-type/form-document-type.component';
import { ListDocumentTypeComponent } from './pages/list-document-type/list-document-type.component';

@NgModule({
  declarations: [
    FormDocumentTypeComponent,
    ListDocumentTypeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DocumentTypeRoutingModule,
  ],
  providers: [
    DocumentTypeService
  ]
})
export class DocumentTypeModule { }
