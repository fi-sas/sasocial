import { TranslationModel } from "@fi-sas/backoffice/modules/configurations/models/translations.model";

export class DocumentTypeModel {
    id: number;
    active: boolean;
    external_ref: string;
    translations: TranslationModel[];
}