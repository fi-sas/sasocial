import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first, finalize } from 'rxjs/operators';
import { DocumentTypeService } from '../../services/document-type.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-document-type',
  templateUrl: './form-document-type.component.html',
  styleUrls: ['./form-document-type.component.less']
})
export class FormDocumentTypeComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;

  loadingLanguages = false;
  languages: LanguageModel[] = [];
  documentTypeGroup = new FormGroup({
    active: new FormControl(false, [Validators.required]),
    external_ref: new FormControl('', trimValidation),
    translations: new FormArray([]),
  });
  translations = this.documentTypeGroup.get('translations') as FormArray;
  submitted = false;
  idToUpdate = null;
  loading = false;


  constructor(
    public documentTypeService: DocumentTypeService,
    public route: ActivatedRoute,
    public router: Router,
    public uiService: UiService,
    public languagesService: LanguagesService
  ) { }

  ngOnInit() {
    this.loadLanguages();
  }

  addTranslation(language_id: number, description?: string) {
    const translations = this.documentTypeGroup.controls.translations as FormArray;
    translations.push(
      new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        description: new FormControl(description, [Validators.required,trimValidation]),
      })
    );
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService
      .list(0, -1, 'order', 'ascend')
      .pipe(
        first(),
        finalize(() => (this.loadingLanguages = false))
      )
      .subscribe((results) => {
        this.languages = results.data;
        this.loadDocumentType();
      });
  }

  loadDocumentType() {
    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {
      this.loading = true;
      this.documentTypeService.read(this.idToUpdate).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(results => {
        this, this.documentTypeGroup.patchValue({
          external_ref: results.data[0].external_ref,
          active: results.data[0].active,
        })


        if (results.data[0].translations) {
          results.data[0].translations.map((t) => {
            this.addTranslation(t.language_id, t.description);
          });
        }

        this.languages.map((l) => {
          const foundLnaguage = this.translations.value.find(
            (t) => t.language_id === l.id
          );
          if (!foundLnaguage) {
            this.addTranslation(l.id, '');
          }
        });
      });
    } else {
      this.languages.map((l) => {
        this.addTranslation(l.id, '');
      });
    }
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find((l) => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  back() {
    this.router.navigateByUrl('users/document-type/list');
  }

  submitForm() {
    this.submitted = true;
    for (const i in this.documentTypeGroup.controls) {
      if (i) {
        this.documentTypeGroup.controls[i].markAsDirty();
        this.documentTypeGroup.controls[i].updateValueAndValidity();
      }
    }

    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        for (const i in tt.controls) {
          if (i) {
            tt.controls[i].markAsDirty();
            tt.controls[i].updateValueAndValidity();
          }
        }
      }
    }

    let tabIndex = 0;
    for (const t in this.translations.controls) {
      if (t) {
        const tt = this.translations.get(t) as FormGroup;
        if (!tt.valid) {
          this.translationTabs.nzSelectedIndex = tabIndex;
          break;
        }
      }
      tabIndex++;
    }

    if (this.documentTypeGroup.valid) {
      this.submitted = false;
      if (this.idToUpdate) {
        this.loading = true;
        this.documentTypeService.update(this.idToUpdate, this.documentTypeGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)
        ).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Tipo de documento atualizado com sucesso');
          this.router.navigate(['users', 'document-type', 'list']);
        });
      } else {
        this.loading = true;
        this.documentTypeService.create(this.documentTypeGroup.value).pipe(
          first(),
          finalize(() => this.loading = false)
        ).subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Tipo de documento criado com sucesso');
          this.router.navigate(['users', 'document-type', 'list']);
        });
      }
    }
  }
}
