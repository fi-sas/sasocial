import { Component, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { DocumentTypeService } from '../../services/document-type.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';

@Component({
    selector: 'fi-sas-list-document-type',
    templateUrl: './list-document-type.component.html',
    styleUrls: ['./list-document-type.component.less']
})
export class ListDocumentTypeComponent extends TableHelper implements OnInit {
    languages: LanguageModel[] = [];
    constructor(
        router: Router,
        activatedRoute: ActivatedRoute,
        uiService: UiService,
        public documentTypeService: DocumentTypeService,
        public languageService: LanguagesService
    ) {
        super(uiService, router, activatedRoute);
        this.loadLanguages();
    }

    ngOnInit() {

        this.initTableData(this.documentTypeService);
    }

    deleteDocumentType(id: number) {
        this.uiService.showConfirm('Eliminar tipo de documento', 'Pretende eliminar este tipo de documento', 'Eliminar').pipe(first()).subscribe(confirm => {
            if (confirm) {
                this.documentTypeService.delete(id).pipe(first()).subscribe(result => {
                    this.searchData();
                    this.uiService.showMessage(MessageType.success, 'Tipo de documento eliminado com sucesso');
                });
            }
        });
    }

    validTranslateDescription(id_trans: number, translation: any[]) {
        return translation.find((tran) => tran.language_id == id_trans) ? translation.find((tran) => tran.language_id == id_trans).description : '';
      }

    loadLanguages() {
        this.languageService.list(1, -1).pipe(
          first()
          ).subscribe(result => {
          this.languages = result.data;
        });
      }
    
    
      getLanguageTitle(language_id: number) {
        const language = this.languages.find(l => l.id === language_id);
        return language ? language.name : 'Sem informação';
      }

}
