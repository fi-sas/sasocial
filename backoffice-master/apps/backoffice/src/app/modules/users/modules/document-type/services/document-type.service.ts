import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { DocumentTypeModel } from '../models/document-type.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService extends Repository<DocumentTypeModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.DOCUMENT_TYPES_ID';
    this.entities_url = 'USERS.DOCUMENT_TYPES';
  }

}
