import { ScopeModel } from "../../scopes/models/scope.model";

export class ProfileModel {
  id: number;
  updated_at: Date
  created_at: Date;
  name: string;
  scopes?: ScopeModel[];
  active: boolean;
  external_ref: string;
}
