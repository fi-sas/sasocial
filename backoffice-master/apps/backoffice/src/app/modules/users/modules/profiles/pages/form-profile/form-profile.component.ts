import { Component, OnInit } from '@angular/core';
import { ProfilesService } from '../../services/profiles.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { first, finalize } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-profile',
  templateUrl: './form-profile.component.html',
  styleUrls: ['./form-profile.component.less']
})
export class FormProfileComponent implements OnInit {

  loading = false;
  idToUpdate = null;

  profileGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    external_ref: new FormControl('', trimValidation),
    active: new FormControl(true, [Validators.required]),
    scope_ids: new FormControl([])
  })

  constructor(
    public profileService: ProfilesService,
    public route: ActivatedRoute,
    public router: Router,
    public uiService: UiService,
    public location: Location,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {

      this.loading = true;
      this.profileService.read(this.idToUpdate, {
        withRelated: 'scopes',
      }).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(results => {
        this.profileGroup.patchValue({
          ...results.data[0],
          scope_ids: results.data[0].scopes.map(s => s.id)
        });
      });
    }
  }


  return() {
    this.router.navigate(['/', 'users', 'profiles', 'list']);
  }

  done() {

    for (const i in this.profileGroup.controls) {
      if (i) {
        this.profileGroup.controls[i].markAsDirty();
        this.profileGroup.controls[i].updateValueAndValidity();
      }
    }

    if (!this.profileGroup.valid) {
      return;
    }

    if (this.idToUpdate && this.authService.hasPermission('authorization:profiles:update')) {
      this.loading = true;
      this.profileService.update(this.idToUpdate, this.profileGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Perfil atualizado com sucesso');
        this.location.back();
      });
    } else if(!this.idToUpdate && this.authService.hasPermission('authorization:profiles:create')) {
      this.loading = true;
      this.profileService.create(this.profileGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Perfil criado com sucesso');
        this.router.navigate(['users', 'profiles', 'list']);
      });
    }
  }

}

