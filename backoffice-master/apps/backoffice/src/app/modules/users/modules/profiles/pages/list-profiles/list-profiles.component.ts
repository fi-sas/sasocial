import { Component, OnInit } from '@angular/core';
import { TagComponent } from "@fi-sas/backoffice/shared/components/tag/tag.component";
import { ProfilesService } from '../../services/profiles.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import * as moment from 'moment';

@Component({
  selector: 'fi-sas-list-profiles',
  templateUrl: './list-profiles.component.html',
  styleUrls: ['./list-profiles.component.less']
})
export class ListProfilesComponent extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public profilesService: ProfilesService,
    private authService: AuthService
    ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.persistentFilters = {
      withRelated: false
    };

    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    },{
      key: '',
      label: 'Criado em',
      template: (data) => {
        return moment(new Date(data.created_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    },
    {
      key: '',
      label: 'Alterado em',
      template: (data) => {
        return moment(new Date(data.updated_at)).format('DD/MM/YYYY');
      },
      sortable: true,
    }, {
      key: 'external_ref',
      label: 'Referência externa',
      sortable: true
    },{
      key: 'active',
      label: 'Ativo',
      tag: TagComponent.YesNoTag,
      sortable: true
    },
    );

    this.initTableData(this.profilesService);
  }

  deleteProfile(id: number) {
    if(!this.authService.hasPermission('authorization:profiles:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar perfil', 'Pretende eliminar este perfil', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.profilesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Perfil eliminado com sucesso');
        });
      }
    });
  }

  ngOnDestroy(): void { }

}
