import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProfilesComponent } from './pages/list-profiles/list-profiles.component';
import { FormProfileComponent } from './pages/form-profile/form-profile.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListProfilesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:profiles:read' },
    resolve:{}
  },
  {
    path: 'create',
    component: FormProfileComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:profiles:create' }
  },
  {
    path: 'update/:id',
    component: FormProfileComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:profiles:update' },
    resolve: { }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
