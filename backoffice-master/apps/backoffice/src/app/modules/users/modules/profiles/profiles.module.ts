import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilesRoutingModule } from './profiles-routing.module';
import { ProfilesService } from './services/profiles.service';
import { FormProfileComponent } from './pages/form-profile/form-profile.component';
import { ListProfilesComponent } from './pages/list-profiles/list-profiles.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UsersSharedModule } from '../users-shared/users-shared.module';

@NgModule({
  declarations: [
    FormProfileComponent,
    ListProfilesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersSharedModule,
    ProfilesRoutingModule,
  ],
  providers: [
    ProfilesService
  ]
})
export class ProfilesModule { }
