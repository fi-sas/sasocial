import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ProfileModel } from "@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model";

@Injectable({
  providedIn: 'root'
})
export class ProfilesService extends Repository<ProfileModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'USERS.PROFILES';
    this.entity_url = 'USERS.PROFILES_ID';
  }
}
