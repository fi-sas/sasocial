import { ProfileModel } from "../../profiles/models/profile.model";
import { UserGroupModel } from "../../users-groups/models/user-group.model";

export class ScopeModel {
  id: number;
  name: string;
  permission: string;
  parent_id?: number;
  parent?: ScopeModel;
  children: ScopeModel[];
  grants: string[];
  profile_ids: number[];
  user_groups_ids: number[];
  profiles?: ProfileModel[];
  user_groups?: UserGroupModel[];
}
