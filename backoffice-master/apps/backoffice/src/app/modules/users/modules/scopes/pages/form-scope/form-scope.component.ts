
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { UserGroupsService } from '../../../users-groups/services/user-groups.service';
import { ScopesService } from '../../services/scopes.service';
import { first, finalize } from 'rxjs/operators';
import { Location } from '@angular/common';
import { UserGroupModel } from '../../../users-groups/models/user-group.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-scope',
  templateUrl: './form-scope.component.html',
  styleUrls: ['./form-scope.component.less']
})
export class FormScopeComponent implements OnInit {

  idToUpdate = null;

  user_groups: UserGroupModel[] = [];
  user_group_loading = false;

  scopeGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    permission: new FormControl('', [Validators.required, Validators.pattern('([\\w-]*):([\\w-]*):?([\\w-]+)?\\b')]),
    grants: new FormControl([]),
  });

  loading = false;

  constructor(
    public scopesService: ScopesService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private uiService: UiService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {

      this.loading = true;
      this.scopesService.read(this.idToUpdate).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(results => {
        this.scopeGroup.patchValue({
          ...results.data[0],
        });
      });
    }

  }



  done() {

    for (const i in this.scopeGroup.controls) {
      if (i) {
        this.scopeGroup.controls[i].markAsDirty();
        this.scopeGroup.controls[i].updateValueAndValidity();
      }
    }

    if (!this.scopeGroup.valid) {
      return;
    }

    // SET REGEX TO NULL IN CASE OF EMPTY
    if(this.scopeGroup.value.regex === '') {
      this.scopeGroup.patchValue({
        regex: null
      });
    }

    if (this.idToUpdate && this.authService.hasPermission('authorization:scopes:update')) {
      this.loading = true;
      this.scopesService.update(this.idToUpdate, this.scopeGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Permissão atualizada com sucesso');
        this.location.back();
      });
    } else if(!this.idToUpdate && this.authService.hasPermission('authorization:scopes:create')) {
      this.loading = true;
      this.scopesService.create(this.scopeGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Permissão criada com sucesso');
        this.router.navigate(['users', 'scopes', 'list']);
      });
    }
  }

  returnButton() {
    this.router.navigate(['users', 'scopes', 'list']);
  }
}
