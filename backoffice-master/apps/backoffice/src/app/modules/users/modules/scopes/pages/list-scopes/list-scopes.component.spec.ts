import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListScopesComponent } from './list-scopes.component';
import { NgZorroAntdModule, NzTableComponent } from 'ng-zorro-antd';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ListScopesComponent', () => {
  let component: ListScopesComponent;
  let fixture: ComponentFixture<ListScopesComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          BrowserDynamicTestingModule,
          SharedModule,
          HttpClientTestingModule
        ],
        providers: [FiUrlService, FiResourceService],
        declarations: [ListScopesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListScopesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
