import { CustomControlOption } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { Component, OnInit } from '@angular/core';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { ScopesService } from '../../services/scopes.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-scopes',
  templateUrl: './list-scopes.component.html',
  styleUrls: ['./list-scopes.component.less']
})
export class ListScopesComponent extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public scopesService: ScopesService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.persistentFilters = {
      withRelated: 'user_groups',
      searchFields: 'name,permission'
    }

    this.initTableData(this.scopesService);
  }

  deleteScope(id: number) {
    if(!this.authService.hasPermission('authorization:scopes:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }
    this.uiService.showConfirm('Eliminar permissão', 'Pretende eliminar esta permissão', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.scopesService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Permissão eliminada com sucesso');
        });
      }
    });
  }

  listComplete() {
    this.filters.search = null;
    this.searchData(true);
  }
}
