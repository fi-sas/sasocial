import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListScopesComponent } from './pages/list-scopes/list-scopes.component';
import { FormScopeComponent } from './pages/form-scope/form-scope.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListScopesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:scopes:read' }
  },
  /*{
    path: 'create',
    component: FormScopeComponent,
    data: { breadcrumb: 'Criar', title: 'Criar' , scope: 'authorization:scopes:create' }
  },
  {
    path: 'update/:id',
    component: FormScopeComponent,
    data: { breadcrumb: 'Editar', title: 'Editar' , scope: 'authorization:scopes:update' }
  },*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScopesRoutingModule { }
