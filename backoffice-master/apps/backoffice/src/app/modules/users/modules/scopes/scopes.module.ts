import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScopesRoutingModule } from './scopes-routing.module';
import { ScopesService } from './services/scopes.service';
import { FormScopeComponent } from './pages/form-scope/form-scope.component';
import { ListScopesComponent } from './pages/list-scopes/list-scopes.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    FormScopeComponent,
    ListScopesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ScopesRoutingModule,
  ],
  providers: [
    ScopesService
  ]
})
export class ScopesModule { }
