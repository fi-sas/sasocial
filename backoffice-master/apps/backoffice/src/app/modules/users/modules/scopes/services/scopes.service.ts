import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ScopeModel } from '@fi-sas/backoffice/modules/users/modules/scopes/models/scope.model';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScopesService extends Repository<ScopeModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.SCOPES_ID';
      this.entities_url = 'USERS.SCOPES';
  }

  tree(): Observable<Resource<any>> {
    return this.resourceService.list<any>(this.urlService.get('USERS.SCOPES_TREE')).pipe(first());
  }
}
