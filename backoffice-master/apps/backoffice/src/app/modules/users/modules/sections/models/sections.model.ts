export class SectionsModel {
    id: number;
    name: string;
    department_id: number;
    active: boolean;
}