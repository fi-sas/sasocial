import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { trimValidation } from "@fi-sas/backoffice/shared/validators/validators.validator";
import { finalize, first } from "rxjs/operators";
import { DepartmentModel } from "../../../departments/models/department.model";
import { DepartmentService } from "../../../departments/services/departments.service";
import { SectionsModel } from "../../models/sections.model";
import { SectionsService } from "../../services/sections.service";

@Component({
    selector: 'fi-sas-form-sections',
    templateUrl: './form-sections.component.html',
    styleUrls: ['./form-sections.component.less']
})
export class FormSectionsComponent implements OnInit {

    departmentList: DepartmentModel[] = [];
    formCreate = new FormGroup({
        name: new FormControl('', [Validators.required, trimValidation]),
        department_id: new FormControl(null, [Validators.required]),
        active: new FormControl(true, [Validators.required]),
    });
    submitted = false;
    get f() { return this.formCreate.controls; }
    id;
    isEdit = false;
    isLoading = false;

    constructor(private route: ActivatedRoute,
        private departmentService: DepartmentService,
        private uiService: UiService, private router: Router,
        private sectionsService: SectionsService) {

    }

    ngOnInit() {
        this.getDepartments();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getSectionId(id);
            this.id = id;
            this.isEdit = true;
        }
    }

    getSectionId(id) {
        let section: SectionsModel = new SectionsModel();
        this.sectionsService
            .read(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                section = results.data[0];
                this.formCreate.patchValue({
                    ...section
                });
            });
    }

    getDepartments() {
        this.departmentService.list(1, -1, null, null, { active: true, sort:'name' }).pipe(
            first()
        ).subscribe(results => {
            this.departmentList = results.data;
        });
    }

    submit() {
        this.submitted = true;
        if (this.formCreate.valid) {
            this.isLoading = true;
            if (this.isEdit) {

                this.sectionsService.update(this.id, this.formCreate.value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Secção alterada com sucesso'
                        );
                        this.returnButton();
                    },
                )
            } else {
                this.sectionsService.create(this.formCreate.value).pipe(first(), finalize(() => this.isLoading = false)).subscribe(
                    result => {
                        this.uiService.showMessage(
                            MessageType.success,
                            'Secção criada com sucesso'
                        );
                        this.returnButton();
                    },
                )
            }
        }
    }



    returnButton() {
        this.router.navigateByUrl('users/sections/list');
    }
}