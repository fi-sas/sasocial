import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzModalService } from "ng-zorro-antd";
import { SectionsModel } from "../../models/sections.model";
import { SectionsService } from "../../services/sections.service";

@Component({
    selector: 'fi-sas-list-sections',
    templateUrl: './list-sections.component.html',
    styleUrls: ['./list-sections.component.less']
})
export class ListSectionsComponent extends TableHelper implements OnInit {
    status = [];
    constructor(
        uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private modalService: NzModalService,
        private authService: AuthService,
        public sectionsService: SectionsService) {
        super(uiService, router, activatedRoute);
        this.status = [
            {
              description: "Ativo",
              value: true
            },
            {
              description: "Desativo",
              value: false
            }
          ];
          this.persistentFilters = {
            searchFields: 'name',
          };
    }

    ngOnInit() {
        this.initTableData(this.sectionsService);
    }

    edit(id: number) {
        if (!this.authService.hasPermission('authorization:sections:update')) {
            return;
        }
        this.router.navigateByUrl('/users/sections/update/' + id);
    }

    listComplete() {
        this.filters.active = null;
        this.filters.search = null;
        this.searchData(true);
      }

    desactive(data: SectionsModel) {
        if (!this.authService.hasPermission('authorization:sections:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai desativar esta Secção. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.desactiveDataSubmit(data)
        });

    }

    desactiveDataSubmit(data: SectionsModel) {
        data.active = false;
        this.sectionsService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Secção desativada com sucesso'
                );
                this.initTableData(this.sectionsService);
            });
    }

    active(data: SectionsModel) {
        if (!this.authService.hasPermission('authorization:sections:update')) {
            return;
        }
        this.modalService.confirm({
            nzTitle: 'Vai ativar esta Secção. Tem a certeza que dejesa continuar?',
            nzOnOk: () => this.activeDataSubmit(data)
        });
    }

    activeDataSubmit(data: SectionsModel) {
        data.active = true;
        this.sectionsService
            .patch(data.id, data)
            .subscribe(result => {
                this.uiService.showMessage(
                    MessageType.success,
                    'Secção ativada com sucesso'
                );
                this.initTableData(this.sectionsService);
            });
    }

}
