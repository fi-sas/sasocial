import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSectionsComponent } from './pages/form-sections/form-sections.component';
import { ListSectionsComponent } from './pages/list-sections/list-sections.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListSectionsComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:sections:read' }
  },
  {
    path: 'create',
    component: FormSectionsComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:sections:create' }
  },
  {
    path: 'update/:id',
    component: FormSectionsComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:sections:update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SectionsRoutingModule { }
