import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { SectionsService } from './services/sections.service';
import { SectionsRoutingModule } from './sections-routing.module';
import { FormSectionsComponent } from './pages/form-sections/form-sections.component';
import { ListSectionsComponent } from './pages/list-sections/list-sections.component';

@NgModule({
  declarations: [
    FormSectionsComponent,
    ListSectionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SectionsRoutingModule,
  ],
  providers: [
    SectionsService
  ]
})
export class SectionsModule { }
