import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SectionsModel } from '../models/sections.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SectionsService extends Repository<SectionsModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.SECTIONS_ID';
    this.entities_url = 'USERS.SECTIONS';
  }

  sectionsArray(department_id?: number[]): Observable<Resource<SectionsModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('active', 'true');

    if (department_id.length>0) {
      department_id.forEach((organic)=> {
        params = params.append('query[department_id]', organic.toString());
      })
     
    }
    return this.resourceService.list<SectionsModel>(this.urlService.get('USERS.SECTIONS'), { params });
  }

}
