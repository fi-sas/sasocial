import { ScopeModel } from '@fi-sas/backoffice/modules/users/modules/scopes/models/scope.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export class UserGroupModel {
  id: number;
  name: string;
  scopes: ScopeModel[];
  users: UserModel[];
}
