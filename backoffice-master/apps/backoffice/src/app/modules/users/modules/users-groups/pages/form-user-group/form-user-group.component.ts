import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { UserGroupsService } from '../../services/user-groups.service';
import { ScopesService } from '../../../scopes/services/scopes.service';
import { ScopeModel } from '../../../scopes/models/scope.model';
import { first, finalize } from 'rxjs/operators';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-user-group',
  templateUrl: './form-user-group.component.html',
  styleUrls: ['./form-user-group.component.less']
})
export class FormUserGroupComponent implements OnInit {

  loading = false;
  idToUpdate = null;

  scopes: ScopeModel[] = [];
  scopesLoading = false;

  userGroupGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    scope_ids: new FormControl([])
  });

  constructor(
    public userGroupsService: UserGroupsService,
    public scopesService: ScopesService,
    public route: ActivatedRoute,
    public router: Router,
    public uiService: UiService,
    public location: Location,
    private authService: AuthService
  ) { }


  ngOnInit() {
    this.loadScopes();

    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {

      this.loading = true;
      this.userGroupsService.read(this.idToUpdate, { withRelated: "scopes" }).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(results => {
        this.userGroupGroup.patchValue({
          ...results.data[0],
          scope_ids: results.data[0].scopes.map(s => s.id)
        });
      });
    }
  }

  loadScopes() {
    this.scopesLoading = true;
    this.scopesService.list(1, -1, null, null, {
      withRelated: false
    }).pipe(
      first(),
      finalize(() => this.scopesLoading = false)
    ).subscribe(results => {
      this.scopes = results.data;
    });
  }


  return() {
    this.router.navigate(['/', 'users', 'users-groups', 'list']);
  }

  done() {

    for (const i in this.userGroupGroup.controls) {
      if (i) {
        this.userGroupGroup.controls[i].markAsDirty();
        this.userGroupGroup.controls[i].updateValueAndValidity();
      }
    }

    if (!this.userGroupGroup.valid) {
      return;
    }

    if (this.idToUpdate && this.authService.hasPermission('authorization:user-groups:update')) {
      this.loading = true;
      this.userGroupsService.update(this.idToUpdate, this.userGroupGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Grupo de utilizadores atualizado com sucesso!');
        this.location.back();
      });
    } else if(!this.idToUpdate && this.authService.hasPermission('authorization:user-groups:create')) {
      this.loading = true;
      this.userGroupsService.create(this.userGroupGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Grupo de utilizadores criado com sucesso!');
        this.router.navigate(['users', 'users-groups', 'list']);
      });
    }
  }
}
