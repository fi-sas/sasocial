import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUsersGroupsComponent } from './list-users-groups.component';
import { NgZorroAntdModule, NzTableComponent } from 'ng-zorro-antd';
import { ListScopesComponent } from '@fi-sas/backoffice/modules/users/modules/scopes/pages/list-scopes/list-scopes.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ListUsersGroupsComponent', () => {
  let component: ListUsersGroupsComponent;
  let fixture: ComponentFixture<ListUsersGroupsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          BrowserDynamicTestingModule,
          SharedModule,
          HttpClientTestingModule
        ],
        providers: [FiUrlService, FiResourceService],
        declarations: [ListUsersGroupsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsersGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
