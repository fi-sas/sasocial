import { Component, OnInit } from '@angular/core';
import { UserGroupsService } from '../../services/user-groups.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-users-groups',
  templateUrl: './list-users-groups.component.html',
  styleUrls: ['./list-users-groups.component.less']
})
export class ListUsersGroupsComponent extends TableHelper implements OnInit {

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    public userGroupsService: UserGroupsService,
    private authService: AuthService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.persistentFilters = {
      withRelated: false
    };

    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    })

    this.initTableData(this.userGroupsService);
  }

  deleteUserGroup(id: number) {
    if(!this.authService.hasPermission('authorization:user-groups:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não têm acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar grupo de utilizadores', 'Pretende eliminar este grupo de utilizadores', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.userGroupsService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Grupo eliminado com sucesso');
        });
      }
    });
  }
}
