import { TestBed, inject } from '@angular/core/testing';

import { UserGroupsService } from './user-groups.service';

describe('UserGroupsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserGroupsService]
    });
  });

  it(
    'should be created',
    inject([UserGroupsService], (service: UserGroupsService) => {
      expect(service).toBeTruthy();
    })
  );
});
