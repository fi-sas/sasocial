import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { UserGroupModel } from '@fi-sas/backoffice/modules/users/modules/users-groups/models/user-group.model';
import { first } from 'rxjs/operators';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root'
})
export class UserGroupsService extends Repository<UserGroupModel> {
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'USERS.USERS_GROUPS';
    this.entity_url = 'USERS.USERS_GROUPS_ID';
  }

  addScopeToUserGroup(
    user_group_id: number,
    scope_id: number
  ): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('USER_GROUPS_SCOPES'), {
        user_group_id,
        scope_id
      })
      .pipe(first());
  }

  removeScopeFromUserGroup(
    user_group_id: number,
    scope_id: number
  ): Observable<any> {
    return this.resourceService
      .delete(
        this.urlService.get('USER_GROUPS_SCOPES_DELETE', {
          user_group_id,
          scope_id
        })
      )
      .pipe(first());
  }
}
