import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsersGroupsComponent } from './pages/list-users-groups/list-users-groups.component';
import { FormUserGroupComponent } from './pages/form-user-group/form-user-group.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListUsersGroupsComponent,
    data: {
      breadcrumb: 'Listar',
      title: 'Listar', scope: 'authorization:user-groups:read'
    }
  },
  {
    path: 'create',
    component: FormUserGroupComponent,
    data: {
      breadcrumb: 'Criar',
      title: 'Criar', scope: 'authorization:user-groups:create'
    }
  },
  {
    path: 'update/:id',
    component: FormUserGroupComponent,
    data: {
      breadcrumb: 'Editar',
      title: 'Editar', scope: 'authorization:user-groups:update'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersGroupsRoutingModule { }
