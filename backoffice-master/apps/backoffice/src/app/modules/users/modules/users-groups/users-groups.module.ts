import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersGroupsRoutingModule } from './users-groups-routing.module';
import { UserGroupsService } from './services/user-groups.service';
import { FormUserGroupComponent } from './pages/form-user-group/form-user-group.component';
import { ListUsersGroupsComponent } from './pages/list-users-groups/list-users-groups.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UsersSharedModule } from '../users-shared/users-shared.module';

@NgModule({
  declarations: [
    FormUserGroupComponent,
    ListUsersGroupsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersSharedModule,
    UsersGroupsRoutingModule,
  ],
  providers: [
    UserGroupsService
  ]
})
export class UsersGroupsModule { }
