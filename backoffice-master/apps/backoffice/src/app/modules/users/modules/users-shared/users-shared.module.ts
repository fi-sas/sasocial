import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScopeTreeComponent } from '../../components/scope-tree/scope-tree.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    ScopeTreeComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ScopeTreeComponent
  ]
})
export class UsersSharedModule { }
