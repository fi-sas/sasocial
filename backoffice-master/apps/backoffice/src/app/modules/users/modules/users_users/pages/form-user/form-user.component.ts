import { Component, Input, OnInit } from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProfilesService } from '../../../profiles/services/profiles.service';
import { ScopesService } from '../../../scopes/services/scopes.service';
import { UsersService } from '../../services/users.service';
import { UserGroupsService } from '../../../users-groups/services/user-groups.service';
import { ProfileModel } from '../../../profiles/models/profile.model';
import { UserGroupModel } from '../../../users-groups/models/user-group.model';
import { ScopeModel } from '../../../scopes/models/scope.model';
import {
  debounceTime,
  finalize,
  first,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import * as moment from 'moment';
import { countries } from '@fi-sas/backoffice/shared/common/countries';
import { nationalities } from '@fi-sas/backoffice/shared/common/nationalities';
import { Location } from '@angular/common';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { Observable } from 'rxjs';
import { Resource } from '@fi-sas/core';
import { UserModel } from '../../models/user.model';
import { DocumentTypeModel } from '../../../document-type/models/document-type.model';
import { DocumentTypeService } from '../../../document-type/services/document-type.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';

@Component({
  selector: 'fi-sas-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.less'],
})
export class FormUserComponent implements OnInit {
  loading = false;

  current = 0;
  index = 'personalData';

  countries = countries;
  nationalities = nationalities;

  document_types: DocumentTypeModel[] = [];
  document_types_loading = false;

  profiles: ProfileModel[] = [];
  profiles_loading = false;

  user_groups: UserGroupModel[] = [];
  user_group_loading = false;

  scopes: ScopeModel[] = [];
  scopes_loading = false;

  userGroup = new FormGroup({
    student_number: new FormControl(null, [trimValidation]),
    name: new FormControl(null, [Validators.required, trimValidation]),
    birth_date: new FormControl(moment().subtract('year', 18).toDate(), [
      Validators.required,
    ]),
    gender: new FormControl('M', [Validators.required]),

    document_type_id: new FormControl(null),
    identification: new FormControl(null, [Validators.maxLength(15), trimValidation]),
    tin: new FormControl(null, [Validators.required, Validators.maxLength(11), trimValidation]),
    nationality: new FormControl(null, [Validators.required]),
    active: new FormControl(true, [Validators.required]),
    is_final_consumer: new FormControl(false, [Validators.required]),
    institute: new FormControl(null, [trimValidation]),
    external: new FormControl(false, []),
    phone: new FormControl(null, [trimValidation]),
    email: new FormControl(
      null,
      [Validators.required, Validators.email,trimValidation],
      this.validateEmailViaServer.bind(this)
    ),
    fb_messenger_id: new FormControl(null, [trimValidation]),

    address: new FormControl(null, [Validators.required,trimValidation]),
    city: new FormControl(null, [Validators.required, trimValidation]),
    country: new FormControl('Portugal', [Validators.required]),
    postal_code: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^\d{4}-\d{3}$/),
    ]),

    rfid: new FormControl(null, []),
    rfid_validity: new FormControl(null, []),
    user_name: new FormControl(
      null,
      [Validators.required, trimValidation],
      this.validateUsernameViaServer.bind(this)
    ),

    can_access_BO: new FormControl(false, [Validators.required]),
    profile_id: new FormControl(null, [Validators.required]),
    user_group_ids: new FormControl([], []),
    scope_ids: new FormControl([], []),
    blocked_fields:new FormControl([], []),
  });

  idToUpdate = null;

  constructor(
    public usersService: UsersService,
    private userGroupsService: UserGroupsService,
    private scopesService: ScopesService,
    private profilesService: ProfilesService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private uiService: UiService,
    private authService: AuthService,
    private documentTypesService: DocumentTypeService
  ) { }

  ngOnInit() {
    this.idToUpdate = this.route.snapshot.params.id;

    if (this.idToUpdate) {
      // REMOVE PASSWORD PIN USERNAME FROM FORM
      this.userGroup.removeControl('email');
      this.userGroup.removeControl('username');
      this.loading = true;
      this.usersService
        .read(this.idToUpdate, {
          withRelated: 'scopes,user_groups',
        })
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((results) => {
          this.userGroup.patchValue({
            ...results.data[0],
            user_group_ids: results.data[0].user_groups.map((ug) => ug.id),
            scope_ids: results.data[0].scopes.map((sp) => sp.id),
          });
        });
    }

    this.loadProfiles();
    this.loadUserGroups();
    this.loadScopes();
    this.loadDocumentTypes();
  }

  lastRequestedEmail = null;
  validateEmailViaServer(
    control: FormControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    if (!control.valueChanges || control.pristine) {
      return Observable.of(null);
    } else {
      if (control.value === this.lastRequestedEmail) {
        return Observable.of(null);
      }
      this.lastRequestedEmail = control.value;

      return control.valueChanges.pipe(
        debounceTime(1000),
        take(1),
        switchMap((value) =>
          this.usersService
            .list(0, 1, null, null, {
              withRelated: false,
              fields: 'email',
              email: value,
            })
            .pipe(
              first(),
              map((resource: Resource<UserModel>) => {
                return resource.data.length > 0
                  ? {
                    emailInUse: true,
                  }
                  : null;
              })
            )
        ),
        tap(() => {
          control.markAsTouched();
        })
      );
    }
  }

  lastRequestedUsername = null;
  validateUsernameViaServer(
    control: AbstractControl
  ): Observable<ValidationErrors | null> {
    if (!control.valueChanges || control.pristine) {
      return Observable.of(null);
    } else {
      if (control.value === this.lastRequestedUsername) {
        return Observable.of(null);
      }
      this.lastRequestedUsername = control.value;

      return control.valueChanges.pipe(
        debounceTime(1000),
        take(1),
        switchMap((value) =>
          this.usersService
            .list(0, 1, null, null, {
              withRelated: false,
              fields: 'user_name',
              user_name: value,
            })
            .pipe(
              first(),
              map((resource: Resource<UserModel>) => {
                return resource.data.length > 0
                  ? {
                    user_nameInUse: true,
                  }
                  : null;
              })
            )
        ),
        tap(() => {
          control.markAsTouched();
        })
      );
    }
  }

  loadProfiles() {
    this.profiles_loading = true;
    this.profilesService
      .list(1, -1, null, null, {
        withRelated: false,
        sort:'name'
      })
      .pipe(
        first(),
        finalize(() => (this.profiles_loading = false))
      )
      .subscribe((result) => {
        this.profiles = result.data;
      });
  }

  loadUserGroups() {
    this.user_group_loading = true;
    this.userGroupsService
      .list(1, -1, null, null, {
        withRelated: false,
        sort:'name'
      })
      .pipe(
        first(),
        finalize(() => (this.user_group_loading = false))
      )
      .subscribe((result) => {
        this.user_groups = result.data;
      });
  }

  loadScopes() {
    this.scopes_loading = true;
    this.scopesService
      .list(1, -1, null, null, {
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.scopes_loading = false))
      )
      .subscribe((result) => {
        this.scopes = result.data;
      });
  }

  loadDocumentTypes() {
    this.document_types_loading = true;
    this.documentTypesService
      .list(1, -1, null, null, {
      })
      .pipe(
        first(),
        finalize(() => (this.document_types_loading = false))
      )
      .subscribe((result) => {
        this.document_types = result.data;
      });
  }

  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    if (!this.validateStep()) {
      return;
    }

    this.current += 1;
    this.changeContent();
  }
  goToStep(step: number) {
    if (step > this.current) {
      for (let index = this.current; index < step; index++) {
        this.current = index;
        const valid = this.validateStep();
        if (!valid) {
          this.changeContent();
          return;
        }
      }
    }
    this.current = step;
    this.changeContent();
  }

  done(): void {
    this.checkValidityOfControls([
      this.userGroup.get('profile_id')
    ]);
    if (!this.userGroup.valid) {
      return;
    }

    if (this.idToUpdate && this.authService.hasPermission('authorization:users:update')) {
      this.loading = true;
      this.usersService.update(this.idToUpdate, this.userGroup.value).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(MessageType.success, 'Utilizador atualizado com sucesso!');
        this.location.back();
      });
    } else if (!this.idToUpdate && this.authService.hasPermission('authorization:users:create')) {
      this.loading = true;
      this.usersService
        .create(this.userGroup.value)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Utilizador criado com sucesso!'
          );
          this.router.navigate(['users', 'users', 'list']);
        });
    }
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== 'VALID');
  }

  validateStep(): boolean {
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = this.checkValidityOfControls([
          this.userGroup.get('student_number'),
          this.userGroup.get('name'),
          this.userGroup.get('birth_date'),
          this.userGroup.get('gender'),
          this.userGroup.get('document_type_id'),
          this.userGroup.get('identification'),
          this.userGroup.get('nationality'),
          this.userGroup.get('tin'),
          this.userGroup.get('active'),
          this.userGroup.get('is_final_consumer'),
          this.userGroup.get('institute'),
          this.userGroup.get('external'),
        ]);
        break;
      }
      case 1: {
        const controls = [
          this.userGroup.get('phone'),
          this.userGroup.get('name'),
          this.userGroup.get('fb_messenger_id'),
        ];
        if (!this.idToUpdate) {
          controls.push(this.userGroup.get('email'));
        }
        valid = this.checkValidityOfControls(controls);
        break;
      }
      case 2: {
        valid = this.checkValidityOfControls([
          this.userGroup.get('address'),
          this.userGroup.get('city'),
          this.userGroup.get('country'),
          this.userGroup.get('postal_code'),
        ]);
        break;
      }
      case 3: {
        const controls = [
          this.userGroup.get('rfid'),
          this.userGroup.get('rfid_validity'),
          //this.userGroup.get('can_change_password'),
        ];
        if (!this.idToUpdate) {
          controls.push(
            ...[
              this.userGroup.get('user_name'),
              //this.userGroup.get('pin'),
              //this.userGroup.get('password'),
            ]
          );
        }
        valid = this.checkValidityOfControls(controls);
        break;
      }
      case 4: {
        valid = this.checkValidityOfControls([
          this.userGroup.get('can_access_BO'),
          this.userGroup.get('profile_id'),
          this.userGroup.get('user_group_ids'),
          this.userGroup.get('scope_ids'),
          this.userGroup.get('blocked_fields'),
        ]);
        break;
      }
    }
    return valid;
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'personalData';
        break;
      }
      case 1: {
        this.index = 'contactsData';
        break;
      }
      case 2: {
        this.index = 'addressData';
        break;
      }
      case 3: {
        this.index = 'authenticationData';
        break;
      }
      case 4: {
        this.index = 'permissionData';
        break;
      }
      case 5: {
        this.index = 'permissionData';
        break;
      }
      case 5: {
        this.index = 'success';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
}
