import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUsersComponent } from './list-users.component';
import { NgZorroAntdModule, NzTableComponent } from 'ng-zorro-antd';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CommonModule } from '@angular/common';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ListUsersComponent', () => {
  let component: ListUsersComponent;
  let fixture: ComponentFixture<ListUsersComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          CommonModule,
          BrowserDynamicTestingModule,
          RouterTestingModule,
          SharedModule,
          HttpClientTestingModule
        ],
        providers: [FiUrlService, FiResourceService],
        declarations: [ListUsersComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
