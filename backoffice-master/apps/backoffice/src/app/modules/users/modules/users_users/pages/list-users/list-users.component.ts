import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { ProfileModel } from '../../../profiles/models/profile.model';
import { ProfilesService } from '../../../profiles/services/profiles.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'fi-sas-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.less'],
})
export class ListUsersComponent
  extends TableHelper
  implements OnInit, OnDestroy {
  profiles: ProfileModel[] = [];
  profile_loading = false;
  status = [];
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public usersService: UsersService,
    private profilesService: ProfilesService
  ) {
    super(uiService, router, activatedRoute);
    this.status = [
      {
        description: "Ativo",
        value: true
      },
      {
        description: "Desativo",
        value: false
      }
    ];
  }

  ngOnInit(): void {
    this.persistentFilters = {
      searchFields: 'name,student_number,email,user_name,identification,tin',
      withRelated: "profile,user_groups,scopes,devices,course,organicUnit,department,section"
    };

    this.columns.push(
      {
        key: 'student_number',
        label: 'Numero',
        sortable: true,
      },
      {
        key: 'email',
        label: 'Email',
        sortable: true,
      },
      {
        key: 'name',
        label: 'Nome',
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: TagComponent.YesNoTag,
        sortable: true,
      }
    );

    this.initTableData(this.usersService);
    this.loadProfiles();
  }

  ngOnDestroy(): void { }

  listComplete() {
    this.filters.active = null;
    this.filters.can_access_BO = null;
    this.filters.external = null;
    this.filters.search = null;
    this.filters.profile_id = null;
    this.filters.sourced_by = null;
    this.searchData(true);
  }

  loadProfiles() {
    this.profile_loading = true;
    this.profilesService
      .list(0, -1, null, null, {
        withRelated: false,
      })
      .pipe(
        first(),
        finalize(() => (this.profile_loading = false))
      )
      .subscribe((result) => {
        this.profiles = result.data;
      });
  }

  changeStatusAccount(user: UserModel) {
    this.uiService
      .showConfirm(
        user.active ? 'Desativar' : 'Ativar',
        `Pretender alterar o estado desta conta para ${user.active ? 'desativo' : 'ativo'
        }`
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.usersService
            .changeAccountStatus(user.id, !user.active)
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                `Estado da conta alterado para  ${user.active ? 'desativo' : 'ativo'
                }`
              );
              this.searchData();
            });
        }
      });
  }

  sendVerificationEmail(user: UserModel) {
    if (!user.account_verified) {
      this.usersService.createNewAccountVerify(user.email).subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          'Notificação de verificação de conta enviada'
        );
      });
    } else {
      this.uiService.showMessage(
        MessageType.error,
        'Esta conta já se encontra  válida'
      );
    }
  }
}
