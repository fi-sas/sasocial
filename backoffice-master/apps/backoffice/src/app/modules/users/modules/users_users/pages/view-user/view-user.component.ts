import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'fi-sas-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.less']
})
export class ViewUserComponent implements OnInit {

  gender_map = {
    M: "Masculino",
    F: "Feminino",
    U: "Outro"
  };

  @Input() data: UserModel = null;

  constructor() { }

  ngOnInit() {
  }

}
