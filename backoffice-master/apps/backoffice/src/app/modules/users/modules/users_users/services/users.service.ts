import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { first } from 'rxjs/operators';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends Repository<UserModel>{
  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entity_url = 'USERS.USERS_ID';
    this.entities_url = 'USERS.USERS';
  }

  addGroupToUser(user_id: number, user_group_id: number): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('USERS_USER_GROUP'), {
        user_id,
        user_group_id
      })
      .pipe(first());
  }

  removeGroupFromUser(user_id: number, user_group_id: number): Observable<any> {
    return this.resourceService
      .delete(
        this.urlService.get('USERS_USER_GROUP_DELETE', {
          user_id,
          user_group_id
        })
      )
      .pipe(first());
  }

  addScopeToUser(user_id: number, scope_id: number): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('USERS_SCOPES'), {
        user_id,
        scope_id
      })
      .pipe(first());
  }

  removeScopeToUser(user_id: number, scope_id: number): Observable<any> {
    return this.resourceService
      .delete(
        this.urlService.get('USERS_SCOPES_DELETE', {
          user_id,
          scope_id
        })
      )
      .pipe(first());
  }

  createNewAccountVerify(email: string): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('USERS.ACCOUNT_VERIFY'), {
        email
      })
      .pipe(first());
  }

  changeAccountStatus(user_id: number, active: boolean): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('USERS.CHANGE_STATUS', { user_id }), {
        active
      })
      .pipe(first());
  }

  filterByUserId(ids: number[]): Observable<any> {
    let params = new HttpParams();
    for (const id of ids) {
      params = params.append('query[id]', id + '');
    }
    return this.resourceService
      .list<UserModel[]>(this.urlService.get('USERS.USERS'), { params });
  }

  filterByUserIdAndCanAccessBO(ids: number[]): Observable<any> {
    let params = new HttpParams();
    params = params.append('query[can_access_BO]', 'true');
    for (const id of ids) {
      params = params.append('query[id]', id + '');
    }
    return this.resourceService
      .list<UserModel[]>(this.urlService.get('USERS.USERS'), { params });
  }

  filterByProfileId(ids: number[]): Observable<any> {
    let params = new HttpParams();
    for (const id of ids) {
      params = params.append('query[profile_id]', id + '');
    }
    return this.resourceService
      .list<UserModel[]>(this.urlService.get('USERS.USERS'), { params });
  }
}
