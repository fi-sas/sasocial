import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormUserComponent } from './pages/form-user/form-user.component';
import { ListUsersComponent } from './pages/list-users/list-users.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListUsersComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'authorization:users:read' },
  },
  {
    path: 'create',
    component: FormUserComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'authorization:users:create' },
  },
  {
    path: 'update/:id',
    component: FormUserComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'authorization:users:update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Users_UsersRoutingModule { }
