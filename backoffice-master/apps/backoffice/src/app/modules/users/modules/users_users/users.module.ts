import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Users_UsersRoutingModule } from './users-routing.module';
import { UsersService } from './services/users.service';
import { FormUserComponent } from './pages/form-user/form-user.component';
import { ListUsersComponent } from './pages/list-users/list-users.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewUserComponent } from './pages/view-user/view-user.component';

@NgModule({
  declarations: [
    FormUserComponent,
    ListUsersComponent,
    ViewUserComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    Users_UsersRoutingModule,
  ],
  providers: [
    UsersService
  ]
})
export class Users_UsersModule { }
