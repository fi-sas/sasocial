import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { UsersComponent } from '@fi-sas/backoffice/modules/users/users.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: 'initial-page',
        component: InitialPageComponent,
        
      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: 'users',
        loadChildren: '../users/modules/users_users/users.module#Users_UsersModule',
        data: { breadcrumb: 'Utilizadores', title: 'Utilizadores', scope: 'authorization:users' },
        canActivate: []
      },
      {
        path: 'scopes',
        loadChildren: '../users/modules/scopes/scopes.module#ScopesModule',
        data: { breadcrumb: 'Permissões', title: 'Permissões', scope: 'authorization:scopes' },
        canActivate: []
      },
      {
        path: 'users-groups',
        loadChildren: '../users/modules/users-groups/users-groups.module#UsersGroupsModule',
        data: { breadcrumb: 'Grupos de utilizadores', title: 'Grupos de utilizadores', scope: 'authorization:user-groups' },
        canActivate: []
      },
      {
        path: 'document-type',
        loadChildren: '../users/modules/document-type/document-type.module#DocumentTypeModule',
        data: { breadcrumb: 'Tipos de documento', title: 'Tipos de documento' },
        canActivate: []
      },
      {
        path: 'profiles',
        loadChildren: '../users/modules/profiles/profiles.module#ProfilesModule',
        data: { breadcrumb: 'Perfis', title: 'Perfis', scope: 'authorization:profiles' },
        canActivate: []
      },
      {
        path: 'departments',
        loadChildren: '../users/modules/departments/departments.module#DepartmentModule',
        data: { breadcrumb: 'Departamento', title: 'Departamento', scope: 'authorization:departments' },
        canActivate: []
      },

      {
        path: 'sections',
        loadChildren: '../users/modules/sections/sections.module#SectionsModule',
        data: { breadcrumb: 'Secções', title: 'Secções', scope: 'authorization:sections' },
        canActivate: []
      },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada'
        }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
