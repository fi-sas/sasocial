import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { Route } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [OverlayModule, SharedModule, RouterTestingModule],
        providers: [UiService],
        declarations: [UsersComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
