import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-users',
  template: '<router-outlet></router-outlet>'
})
export class UsersComponent implements OnInit {
  dataConfiguration: any;
  constructor(private uiService: UiService, private configurationsService: ConfigurationGeralService) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    this.uiService.setSiderTitle(this.validTitleTraductions(8), 'user');

    const users = new SiderItem('Utilizadores', '', '', 'authorization:users');
    users.addChild(new SiderItem('Criar', '', '/users/users/create', 'authorization:users:create'));
    users.addChild(new SiderItem('Listar', '', '/users/users/list', 'authorization:users:read'));
    this.uiService.addSiderItem(users);

    const profiles = new SiderItem('Perfis', '', '', 'authorization:profiles');
    profiles.addChild(new SiderItem('Criar', '', '/users/profiles/create', 'authorization:profiles:create'));
    profiles.addChild(new SiderItem('Listar', '', '/users/profiles/list', 'authorization:profiles:read'));
    this.uiService.addSiderItem(profiles);

    const users_groups = new SiderItem('Grupos Utilizadores', '', '', 'authorization:user-groups');
    users_groups.addChild(new SiderItem('Criar', '', '/users/users-groups/create', 'authorization:user-groups:create'));
    users_groups.addChild(new SiderItem('Listar', '', '/users/users-groups/list', 'authorization:user-groups:read'));
    this.uiService.addSiderItem(users_groups);

    const permissions = new SiderItem('Permissões', '', '', 'authorization:scopes');
    //permissions.addChild(new SiderItem('Criar', '', '/users/scopes/create', 'authorization:scopes:create'));
    permissions.addChild(new SiderItem('Listar', '', '/users/scopes/list', 'authorization:scopes:read'));
    this.uiService.addSiderItem(permissions);

    const document_type = new SiderItem('Tipos Docs. Identificação', '', '', 'authorization:document-types');
    document_type.addChild(new SiderItem('Criar', '', '/users/document-type/create', 'authorization:document-types:create'));
    document_type.addChild(new SiderItem('Listar', '', '/users/document-type/list', 'authorization:document-types:read'));
    this.uiService.addSiderItem(document_type);

    const departments = new SiderItem('Departamentos', '', '', 'authorization:departments');
    departments.addChild(new SiderItem('Criar', '', '/users/departments/create', 'authorization:departments:create'));
    departments.addChild(new SiderItem('Listar', '', '/users/departments/list', 'authorization:departments:read'));
    this.uiService.addSiderItem(departments);

    const sections = new SiderItem('Secções', '', '', 'authorization:sections');
    sections.addChild(new SiderItem('Criar', '', '/users/sections/create', 'authorization:sections:create'));
    sections.addChild(new SiderItem('Listar', '', '/users/sections/list', 'authorization:sections:read'));
    this.uiService.addSiderItem(sections);

    this.uiService.setSiderActive(true);
  }

  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }
}
