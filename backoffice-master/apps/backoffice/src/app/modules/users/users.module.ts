import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { UsersRoutingModule } from '@fi-sas/backoffice/modules/users/users-routing.module';
import { UsersComponent } from './users.component';

@NgModule({
  imports: [CommonModule,
    SharedModule,
    UsersRoutingModule,
  ],
  declarations: [
    UsersComponent,
  ],
  providers: [
  ]
})
export class UsersModule {}
