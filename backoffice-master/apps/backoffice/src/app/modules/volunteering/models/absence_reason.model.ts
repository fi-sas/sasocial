export class AbsenceReasonModel {
  id?: number;
  application_attendance_id: number;
  start_date: string;
  end_date: string;
  reason: string;
  attachment_file_id: number;
  accept: boolean;
  created_at: string;
  updated_at: string;
  reject_reason?: string;
}
