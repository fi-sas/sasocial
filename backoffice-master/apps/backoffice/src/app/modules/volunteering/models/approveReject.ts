import { ExperienceModel } from "../modules/experiences/models/experience.model";
import { ApplicationModel } from "../modules/v_applications/models/application.model";



export class ApproveRejectModel {
  notes: string;
  event: string;
  applications?: ApplicationModel;
  experience?: ExperienceModel;
}