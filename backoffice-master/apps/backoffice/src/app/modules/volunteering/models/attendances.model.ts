import { AbsenceReasonModel } from './absence_reason.model';
import { ApplicationModel } from "@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model";

export class AttendancesModel {
  id?: number;
  date: string;
  was_present: boolean;
  notes: string;
  n_hours: number;
  n_hours_reject: number;
  created_at?: string;
  updated_at?: string;
  applications?: ApplicationModel[];
  absence_reason?: AbsenceReasonModel[];
  executed_service: string;
  initial_time: string;
  final_time: string;
  status: string;
}
