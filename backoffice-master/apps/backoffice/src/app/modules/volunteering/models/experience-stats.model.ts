export class ExperienceStatsModel {
    SUBMITTED: number;
    RETURNED: number;
    ANALYSED: number;
    APPROVED: number;
    PUBLISHED: number;
    REJECTED: number;
    CANCELED: number;
    SEND_SEEM: number;
    EXTERNAL_SYSTEM: number;
    SELECTION: number;
    IN_COLABORATION: number;
    CLOSED: number;
    CONFIRMED: number;
    DISPATCH: number;
  }
