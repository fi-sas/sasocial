import { UserModel } from "../../users/modules/users_users/models/user.model";

export class HistoryModel {
  id?: number;
  experience_id: number;
  status: string;
  user_id: string;
  notes: string;
  created_at: string;
  user: UserModel;
  updated_at: string;
}