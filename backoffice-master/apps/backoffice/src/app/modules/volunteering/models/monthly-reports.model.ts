export class MonthlyReportsModel {
  id?: number;
  application_id?: number;
  month: number;
  year: number;
  report: string;
  user_interest_id: number;
  file_id: number;
  created_at?: string;
  updated_at?: string;
}

export class MonthlyReportsData {

  static months = [
    {value: 1, label: 'Janeiro', active: true},
    {value: 2, label: 'Fevereiro', active: true},
    {value: 3, label: 'Março', active: true},
    {value: 4, label: 'Abril', active: true},
    {value: 5, label: 'Maio', active: true},
    {value: 6, label: 'Junho', active: true},
    {value: 7, label: 'Julho', active: true},
    {value: 8, label: 'Agosto', active: true},
    {value: 9, label: 'Setembro', active: true},
    {value: 10, label: 'Outubro', active: true},
    {value: 11, label: 'Novembro', active: true},
    {value: 12, label: 'Dezembro', active: true}
  ];
}