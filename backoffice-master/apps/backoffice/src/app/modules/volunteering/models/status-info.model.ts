export class StatusInfoModel {
  presentStatus: string;
  presentStatusLabel: string;
  previousStatus: string;
  nextStatus: string;
  presentOrder: number;
  previousOrder: number;
}

