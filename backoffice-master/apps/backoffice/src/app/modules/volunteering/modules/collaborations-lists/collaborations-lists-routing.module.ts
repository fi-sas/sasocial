
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaborationsListsVolunteeringComponent } from './pages/collaborations-lists.component';

const routes: Routes = [
  { path: '', redirectTo: 'student', pathMatch: 'full' },
  {
    path: 'student',
    component: CollaborationsListsVolunteeringComponent,
    data: { breadcrumb: 'Lista por Estudante', title: 'Lista por Estudante', scope: 'volunteering:experiences:read' },
  },
  {
    path: 'action',
    component: CollaborationsListsVolunteeringComponent,
    data: { breadcrumb: 'Lista por Ação', title: 'Lista por Ação', scope: 'volunteering:experiences:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborationsListsVolunteeringRoutingModule { }
