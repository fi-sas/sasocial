import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { CollaborationsListsVolunteeringRoutingModule } from './collaborations-lists-routing.module';
import { RejectAbsenceJustificationComponent } from './components/reject-absence-justification/reject-absence-justification.component';
import { FormFinalReportComponent } from './components/form-final-report/form-final-report.component';
import { FormCertificateComponent } from './components/form-certificate/form-certificate.component';
import { FormAttendanceComponent } from './components/form-attendance/form-attendance.component';
import { FormAbsenceComponent } from './components/form-absence/form-absence.component';
import { CollaborationsListsVolunteeringComponent } from './pages/collaborations-lists.component';
import { listStudentSelectedComponent } from './components/list-student-selected/list-student-selected.component';
import { listActionSelectedComponent } from './components/list-action-selected/list-action-selected.component';
import { AttendanceListComponent } from './components/attendance-list/attendance-list.component';

@NgModule({
  declarations: [
    CollaborationsListsVolunteeringComponent,
    RejectAbsenceJustificationComponent,
    FormFinalReportComponent,
    FormCertificateComponent,
    FormAttendanceComponent,
    FormAbsenceComponent,
    listStudentSelectedComponent,
    listActionSelectedComponent,
    AttendanceListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CollaborationsListsVolunteeringRoutingModule
  ],
  entryComponents: [
    RejectAbsenceJustificationComponent,
    FormFinalReportComponent,
    FormCertificateComponent,
    FormAttendanceComponent,
    listStudentSelectedComponent,
    listActionSelectedComponent,
    FormAbsenceComponent
  ]

})
export class CollaborationsListsVolunteeringModule { }
