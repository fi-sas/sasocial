import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { AttendancesModel } from '@fi-sas/backoffice/modules/volunteering/models/attendances.model';
import { NzModalRef } from 'ng-zorro-antd';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { AttendancesService } from '../../services/attendances.service';

@Component({
  selector: 'fi-sas-form-absence',
  templateUrl: './form-absence.component.html',
  styleUrls: ['./form-absence.component.less'],
})
export class FormAbsenceComponent implements OnInit {
  //@Input() application_id: number;
  @Input() attendance = null;
  //@Input() experience: ExperienceModel = null;

  attendanceLoading = false;
  attendanceForm = new FormGroup({
    n_hours_reject: new FormControl(0, [Validators.required]),
    reject_reason: new FormControl('', trimValidation),
  });


  formatterHours = (value: number) => `${value} h`;

  constructor(
    public attendanceService: AttendancesService,
    private uiService: UiService,
    private authService: AuthService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {

    if (this.attendance) {
      this.setAttendance(this.attendance);
    }
  }

  public setAttendance(attendance: AttendancesModel) {
    this.attendance = attendance;
    attendance.n_hours_reject ? attendance.n_hours_reject : attendance.n_hours_reject = 0;
    this.attendanceForm.patchValue({
      ...attendance,
    });
  }

  public clearForm() {
    this.attendanceForm.reset({
      n_hours_reject: '',
      reject_reason: ''
    });
  }
  

  submit(valid: boolean, formValue: any) {
    if (valid) {
      this.attendanceLoading = true;
      const data = {
        n_hours: this.attendance.n_hours,
        n_hours_reject: this.attendanceForm.value.n_hours_reject,
        reject_reason: this.attendanceForm.value.reject_reason,
      };

      this.attendanceService.reject(this.attendance.id, data)
        .pipe(
          first(),
          finalize(() => (this.attendanceLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            'Presença alterada com sucesso.'
          );
          this.attendanceForm.reset();
          this.close(true);
        });
      } else {
        for (const i in this.attendanceForm.controls) {
          if (this.attendanceForm.controls[i]) {
            this.attendanceForm.controls[i].markAsDirty();
            this.attendanceForm.controls[i].updateValueAndValidity();
          }
        }
      }
  }

  close(state: boolean): void {
    this.modalRef.close(state);
  }
}
