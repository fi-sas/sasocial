import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFinalReportComponent } from './form-final-report.component';

describe('FormFinalReportComponent', () => {
  let component: FormFinalReportComponent;
  let fixture: ComponentFixture<FormFinalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFinalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFinalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
