import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { listActionSelectedComponent } from './list-action-selected.component';


describe('listActionSelectedComponent', () => {
  let component: listActionSelectedComponent;
  let fixture: ComponentFixture<listActionSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listActionSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(listActionSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
