import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { Repository } from "@fi-sas/backoffice/shared/repository/repository.class";
import { NzDrawerRef } from "ng-zorro-antd";
import { ExperiencesService } from "../../../experiences/services/experiences.service";

@Component({
    selector: 'fi-sas-list-action-selected',
    templateUrl: './list-action-selected.component.html',
    styleUrls: ['./list-action-selected.component.less']
})

export class listActionSelectedComponent extends TableHelper implements OnInit {

    constructor(uiService: UiService, private drawerRef: NzDrawerRef,
        router: Router,
        activatedRoute: ActivatedRoute,
        private experiencesService: ExperiencesService,){
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            status: 'IN_COLABORATION',
            searchFields: 'title'
        };
        this.columns.push(
            {
                key: '',
                label: 'Título',
                sortable: false,
                template: (offer) => {
                    return offer.translations.find(x => x.language_id == 3) ? offer.translations.find(x => x.language_id == 3).title : '';
                },
            },
            {
                key: '',
                label: 'Atividades a desenvolver',
                sortable: false,
                template: (offer) => {
                    return offer.translations.find(x => x.language_id == 3) ? offer.translations.find(x => x.language_id == 3).job : '';
                },
            },
        );
       
    }

    ngOnInit(){
        this.initTableData(this.experiencesService);
    }

    initTableData(repository: Repository<any>) {
        this.repository = repository;
        if (!this.repository) {
          throw new Error('No service provided!');
        }
        this.searchData(true);
    }

    listComplete() {
        this.filters.search = null;
        this.searchData(true);
    }

    selectOffer(offer){
        this.drawerRef.close(offer);
    }
}