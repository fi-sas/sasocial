import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { listStudentVolunteeringSelectedComponent } from './list-student-selected.component';


describe('listStudentVolunteeringSelectedComponent', () => {
  let component: listStudentVolunteeringSelectedComponent;
  let fixture: ComponentFixture<listStudentVolunteeringSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listStudentVolunteeringSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(listStudentVolunteeringSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
