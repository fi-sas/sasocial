import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { Repository } from "@fi-sas/backoffice/shared/repository/repository.class";
import { NzDrawerRef } from "ng-zorro-antd";
import { UserColaborationService } from "../../services/user-colaboration.service";

@Component({
    selector: 'fi-sas-list-student-selected',
    templateUrl: './list-student-selected.component.html',
    styleUrls: ['./list-student-selected.component.less']
})

export class listStudentSelectedComponent extends TableHelper implements OnInit {

    constructor(uiService: UiService, private drawerRef: NzDrawerRef,
        router: Router,
        activatedRoute: ActivatedRoute,
        private userColaborationService: UserColaborationService,){
        super(uiService, router, activatedRoute);
        this.persistentFilters = {
            searchFields: 'name,email',
            withRelated: 'user'
        };
        this.columns.push(
            {
                key: 'name',
                label: 'Nome',
                sortable: false,
            },
            {
                key: 'email',
                label: 'Email',
                sortable: false,
            },
            {
                key: 'tin',
                label: 'Nif',
                sortable: false,
            },
            {
                key: 'location',
                label: 'Localidade',
                sortable: false,
            },
        );
       
    }

    ngOnInit(){
        this.initTableData(this.userColaborationService);
    }

    initTableData(repository: Repository<any>) {
        this.repository = repository;
        if (!this.repository) {
          throw new Error('No service provided!');
        }
        this.searchData(true);
    }

    listComplete() {
        this.filters.search = null;
        this.filters.student_number = null;
        this.filters.tin = null;
        this.searchData(true);
    }

    selectStudent(user){
        this.drawerRef.close(user);
    }
}