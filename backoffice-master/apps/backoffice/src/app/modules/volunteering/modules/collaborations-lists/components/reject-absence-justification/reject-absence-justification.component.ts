import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';

import { AttendancesModel } from '@fi-sas/backoffice/modules/volunteering/models/attendances.model';
import { AttendancesService } from '../../services/attendances.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';


@Component({
  selector: 'app-reject-absence-justification',
  templateUrl: './reject-absence-justification.component.html',
  styleUrls: ['./reject-absence-justification.component.less'],
})
export class RejectAbsenceJustificationComponent {
  private _attendance: AttendancesModel;

  @Input() set attendance(attendance: AttendancesModel) {
    this._attendance = attendance;

    this.form = this.getForm();
  }

  get attendance() {
    return this._attendance;
  }

  form: FormGroup;

  isLoading: boolean;

  constructor(
    private attendancesService: AttendancesService,
    private uiService: UiService,
    private modalRef: NzModalRef,
  ) {}

  onSubmit() {
    if (this.form.valid) {
      this.isLoading = true;
      const data = {
        reject_reason: this.form.value.reject_reason,
      };
      this.attendancesService
        .rejectJustification(this.attendance.absence_reason[0].id, data)
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(MessageType.success, 'Justificação rejeitada com sucesso.');
          this.modalRef.close(true);
        });
    } else {
      for (const i in this.form.controls) {
        if (this.form.controls[i]) {
          this.form.controls[i].markAsDirty();
          this.form.controls[i].updateValueAndValidity();
        }
      }
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return 'Campo obrigatório';
    }
    return null;
  }

  close(): void {
    this.modalRef.close(false);
  }

  private getForm() {
    return new FormGroup({
      reject_reason: new FormControl('', [Validators.required, trimValidation]),
    });
  }
}
