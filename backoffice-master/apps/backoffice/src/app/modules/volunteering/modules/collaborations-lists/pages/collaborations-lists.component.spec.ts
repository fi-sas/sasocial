import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CollaborationsListsVolunteeringComponent } from './collaborations-lists.component';


describe('CollaborationsListsVolunteeringComponent', () => {
  let component: CollaborationsListsVolunteeringComponent;
  let fixture: ComponentFixture<CollaborationsListsVolunteeringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationsListsVolunteeringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationsListsVolunteeringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
