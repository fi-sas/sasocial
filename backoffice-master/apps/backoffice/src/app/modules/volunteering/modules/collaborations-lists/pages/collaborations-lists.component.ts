import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { FilesService } from "@fi-sas/backoffice/modules/medias/services/files.service";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { TableHelper } from "@fi-sas/backoffice/shared/helpers/table.helper";
import { AuthService } from "@fi-sas/backoffice/shared/services/auth.service";
import { NzDrawerService, NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { ExperienceModel } from "../../experiences/models/experience.model";
import { FormCertificateComponent, FormFinalReportComponent, listActionSelectedComponent } from "../components";
import { listStudentSelectedComponent } from "../components/list-student-selected/list-student-selected.component";
import { UserInterestModel } from "../models/user-interest.model";
import { UserInterestsService } from "../services/user-interests.service";

@Component({
    selector: 'fi-sas-collaborations-lists',
    templateUrl: './collaborations-lists.component.html',
    styleUrls: ['./collaborations-lists.component.less']
})

export class CollaborationsListsVolunteeringComponent extends TableHelper implements OnInit {
    selectedExperience: ExperienceModel = new ExperienceModel;
    loadingInfo = false;
    userInterests: UserInterestModel[] = [];
    action = false;
    selectedStudent: UserModel = new UserModel;
    selectedUserInterest: UserInterestModel;
    tabIndex = 0;
    studentEvaluationFileUrl: string = null;
    selectedCollaborationTotals = null;
    isStudentEvaluationReportVisible = false;

    constructor(uiService: UiService,
        router: Router,
        activatedRoute: ActivatedRoute, private drawerService: NzDrawerService, private fileService: FilesService,
        private modalService: NzModalService, private userInterestsService: UserInterestsService, private authService: AuthService) {
        super(uiService, router, activatedRoute);
        if (window.location.href.split("/")[window.location.href.split("/").length - 1].split('?')[0] == 'student') {
            this.action = false;
        } else {
            this.action = true;
        }
    }

    ngOnInit() { }


    modalSearch() {
        this.loadingInfo = true;
        if (this.action) {
            const modalDrawer = this.drawerService.create({
                nzMask: true,
                nzMaskStyle: {
                    'opacity': '0',
                    '-webkit-animation': 'none',
                    'animation': 'none',
                },
                nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
                nzContent: listActionSelectedComponent,
                nzWidth: (window.innerWidth / 24) * 12

            });
            modalDrawer.afterClose.pipe(first(), finalize(() => this.loadingInfo = false)).subscribe((result) => {
                if (result) {
                    this.selectedExperience = result;
                    this.userInterests = [];
                    this.selectedUserInterest = null;
                    this.loadExperienceUsers();
                }
            });
        } else {
            const modalDrawer = this.drawerService.create({
                nzMask: true,
                nzMaskStyle: {
                    'opacity': '0',
                    '-webkit-animation': 'none',
                    'animation': 'none',
                },
                nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
                nzContent: listStudentSelectedComponent,
                nzWidth: (window.innerWidth / 24) * 12

            });
            modalDrawer.afterClose.pipe(first(), finalize(() => this.loadingInfo = false)).subscribe((result) => {
                if (result) {
                    this.selectedStudent = result;
                    this.userInterests = [];
                    this.selectedUserInterest = null;
                    this.loadUserExperiences();
                }
            });
        }
    }

    loadExperienceUsers() {
        this.userInterestsService
            .listCollaborations(this.selectedExperience.id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.userInterests = results.data;
                if (this.userInterests.length > 0) {
                    this.changeUserInterest(this.userInterests[0], 0);
                } else {
                    this.uiService.showMessage(
                        MessageType.warning,
                        'Sem informação'
                    );
                }
            });
    }

    loadUserExperiences() {
        this.userInterestsService
            .list(1, -1, null, null, {
                //status: 'COLABORATION',
                user_id: this.selectedStudent.user_id,
                withRelated: "user,application,experience"
            })
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.userInterests = results.data;
                if (this.userInterests.length > 0) {
                    this.changeUserInterest(this.userInterests[0], 0);
                } else {
                    this.uiService.showMessage(
                        MessageType.warning,
                        'Sem informação'
                    );
                }
            });
    }

    changeUserInterest(selectedUserInterest: UserInterestModel, idx) {
        this.loadingInfo = true;
        this.tabIndex = idx;
        this.selectedUserInterest = selectedUserInterest;
        this.studentEvaluationFileUrl = null;
        this.loadStudentEvaluationFile();
        this.loadColaborationTotals(selectedUserInterest.id);
    }

    loadStudentEvaluationFile() {
        if (this.selectedUserInterest.student_file_avaliation) {
            this.fileService
                .get(this.selectedUserInterest.student_file_avaliation)
                .pipe(first())
                .subscribe(
                    (file) => {
                        this.studentEvaluationFileUrl = file.data[0].url;
                    },
                    () => { }
                );
        }
    }

    loadColaborationTotals(id: number) {
        this.selectedCollaborationTotals = null;
        this.userInterestsService
            .generalReports(id)
            .pipe(
                first(), finalize(() => this.loadingInfo = false)
            )
            .subscribe((results) => {
                this.selectedCollaborationTotals = results.data[0];
            });
    }


    showCertificateModal(userInterest: UserInterestModel) {
        if (!this.authService.hasPermission('volunteering:experience-user-interests:certificate')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.modalService.create({
            nzWidth: 800,
            nzMaskClosable: false,
            nzClosable: true,
            nzContent: FormCertificateComponent,
            nzFooter: null,
            nzComponentParams: {
                userInterest: userInterest,
                experience: this.selectedExperience,
            },
        }).afterClose.pipe(first())
            .subscribe((success: boolean) => {
                if (success) {
                    this.changeUserInterest(userInterest, 0);
                }
            });
    }

    showStudentEvaluationReport() {
        if (!this.authService.hasPermission('volunteering:experience-user-interests:read')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.isStudentEvaluationReportVisible = true;
    }

    closeStudentEvaluationReport() {
        this.isStudentEvaluationReportVisible = false;
    }


    showFinalReportModal(finalReport: UserInterestModel) {
        if (!this.authService.hasPermission('volunteering:experience-user-interests:final_report')) {
            this.uiService.showMessage(
                MessageType.warning,
                'O utilizador não tem acesso ao serviço solicitado'
            );
            return;
        }

        this.modalService.create({
            nzWidth: 800,
            nzMaskClosable: false,
            nzClosable: true,
            nzContent: FormFinalReportComponent,
            nzFooter: null,
            nzComponentParams: {
                experience: this.selectedExperience,
                finalReport: finalReport
            },
        });
    }

}