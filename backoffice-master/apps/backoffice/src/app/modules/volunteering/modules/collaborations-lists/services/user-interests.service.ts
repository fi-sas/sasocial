import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { UserInterestModel } from '../models/user-interest.model';
import { BehaviorSubject, Observable } from "rxjs";
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UserInterestsService extends Repository<UserInterestModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.USER_INTEREST';
    this.entity_url = 'VOLUNTEERING.USER_INTEREST_ID';
  }

  generalReports(id: number): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('VOLUNTEERING.GENERAL_REPORTS_USER_INTEREST', { id }),
      {}
    );
  }

  addCertificate(id: number, certificate_file_id: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('VOLUNTEERING.USER_INTEREST_ADD_CERTIFICATE', { id }),
      {
        certificate_file_id: certificate_file_id
      }
    );
  }

  generateCertificate(id: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('VOLUNTEERING.USER_INTEREST_GENERATE_CERTIFICATE', { id }),
      {}
    );
  }

  addFinalReport(id: number, report_avaliation: number): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get('VOLUNTEERING.USER_INTEREST_ADD_FINAL_REPORT', { id }),
      {
        avaliation_file: report_avaliation
      }
    );
  }

  listCollaborations(
    experienceId: number,
    status: string[] = ['COLABORATION', 'WITHDRAWAL', 'WITHDRAWAL_ACCEPTED', 'DECLINED'],
    withRelated: string[] = ['user', 'application', 'experience'],
  ): Observable<Resource<UserInterestModel>> {
    let params = new HttpParams();
    params = params.append('query[experience_id]', experienceId.toString());
    if (status.length) {
      status.forEach((s) => params = params.append('query[status]', s));
    }
    if (withRelated.length) {
      params = params.append('withRelated', withRelated.join(','));
    }
    return this.resourceService.list<UserInterestModel>(this.urlService.get(this.entities_url), { params })
  }
}
