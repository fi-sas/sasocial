import { Component, OnDestroy, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { GeneralComplainsService } from '@fi-sas/backoffice/modules/volunteering/modules/complains/services/general-complains.service';
import * as moment from 'moment';
import { ComplainsData } from '@fi-sas/backoffice/modules/volunteering/modules/complains/models/general-complains.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
    selector: 'fi-sas-list-complains',
    templateUrl: './list-complains.component.html',
    styleUrls: ['./list-complains.component.less'],
})

export class ListComplainsComponent
  extends TableHelper
  implements OnInit, OnDestroy {

  status = ComplainsData.status;
  complainStatuses = [];
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private authService: AuthService,
    public generalComplainsService: GeneralComplainsService
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnDestroy() {
  }

  ngOnInit() {
    this.loadStatuses();
    this.persistentFilters['withRelated'] = [

    ].join(',');

    this.columns.push(
      {
        key: 'created_at',
        label: 'Data',
        template: (data) => moment(data.created_at).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: null,
        label: 'Nome',
        template: (data) => data.user ? data.user.name : null,
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: this.status,
        filters: this.complainStatuses.map(status => {
          return {
            text: status.label,
            value: status.key,
          }
        }),
        showFilter: true,
        filterMultiple: false,
      },
      {
        key: null,
        label: 'Anexos',
        template: (data) => data.file_id != null ? 'Sim' : 'Não',
        sortable: false,
      },
    );

    this.initTableData(this.generalComplainsService);
  }

  loadStatuses() {
    this.complainStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  changeStatus(data: any, disabled: boolean) {
    if(!this.authService.hasPermission('volunteering:general-complains:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(!disabled) {
      this.generalComplainsService
      .saveStatus(data.id, { status: 'ANALYSED'})
      .subscribe((result) => {
        data.status = result.data[0].status;
        this.uiService.showMessage(
          MessageType.success,
          'Estado alterado com sucesso'
        );

      });
    }
  }

  listComplete() {
    this.filters.created_at = null;
    this.searchData(true);
  }

  updateList() {
    this.searchData();
  }
}