import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { GeneralComplainsService } from '@fi-sas/backoffice/modules/volunteering/modules/complains/services/general-complains.service';
import { ApplicationsComplainsService } from '@fi-sas/backoffice/modules/volunteering/modules/complains/services/applications-complains.service';
import { ExperiencesComplainsService } from '@fi-sas/backoffice/modules/volunteering/modules/complains/services/experiences-complains.service';
import { UserInterestsComplainsService } from '@fi-sas/backoffice/modules/volunteering/modules/complains/services/user-interests-complains.service';
import { finalize, first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { Output, EventEmitter } from '@angular/core';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';

@Component({
  selector: 'fi-sas-view-complains',
  templateUrl: './view-complains.component.html',
  styleUrls: ['./view-complains.component.less'],
})
export class ViewComplainsComponent implements OnInit {

  @Input() data: any = null;
  @Input() complain: string = null;
  @Output() updateEvent = new EventEmitter<boolean>();

  complainLoading = false;
  complainFormLoading = false;
  loadingFileConsent = false;
  file = null;

  complainForm = new FormGroup({
    response: new FormControl('', [Validators.required, trimValidation]),
  })

  constructor(
    private uiService: UiService,
    private modalService: NzModalService,
    public generalComplainsService: GeneralComplainsService,
    public applicationsComplainsService: ApplicationsComplainsService,
    public experiencesComplainsService: ExperiencesComplainsService,
    private authService: AuthService,
    public userInterestsComplainsService: UserInterestsComplainsService,
    private fileService: FilesService,
  ) {}

  complainServices = {
    'general': this.generalComplainsService,
    'applications': this.applicationsComplainsService,
    'experiences': this.experiencesComplainsService,
    'user-interest': this.userInterestsComplainsService
  }

  submit(valid: boolean, value: any, id: any) {
    if((this.complain === 'general' && !this.authService.hasPermission('volunteering:general-complains:update')) ||
       (this.complain === 'applications' && !this.authService.hasPermission('volunteering:complain-applications:update')) ||
       (this.complain === 'experiences' && !this.authService.hasPermission('volunteering:complain-experiences:update')) ||
       (this.complain === 'user-interest' && !this.authService.hasPermission('volunteering:complain-user-interests:update'))
      ) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    this.complainFormLoading = true;

    this.complainServices[this.complain]
    .saveResponse(id, value)
      .pipe(first(), finalize(() => (this.complainFormLoading = false)))
      .subscribe((result) => {
        this.data = result.data[0];

        this.uiService.showMessage(
          MessageType.success,
          'Resposta inserida com sucesso.'
        );

        this.updateComplainList();

      });
  }

  updateComplainList () {
    this.updateEvent.emit();
  }

  ngOnInit() {
    this.loadFile();
  }


  loadFile(){
    if(this.data.file_id){
      this.loadingFileConsent = true;
      this.fileService.get(this.data.file_id).pipe(first(), finalize(() => this.loadingFileConsent = false)).subscribe((file)=>{
        this.file = file.data[0];
      });
    }
  }
}
