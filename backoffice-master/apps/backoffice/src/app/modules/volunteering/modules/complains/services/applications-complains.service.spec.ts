import { TestBed } from '@angular/core/testing';

import { ApplicationsComplainsService } from './applications-complains.service';

describe('ApplicationsComplainsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationsComplainsService = TestBed.get(ApplicationsComplainsService);
    expect(service).toBeTruthy();
  });
});
