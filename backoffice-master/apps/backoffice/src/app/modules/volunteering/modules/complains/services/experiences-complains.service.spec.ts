import { TestBed } from '@angular/core/testing';

import { ExperiencesComplainsService } from './experiences-complains.service';

describe('ExperiencesComplainsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExperiencesComplainsService = TestBed.get(ExperiencesComplainsService);
    expect(service).toBeTruthy();
  });
});
