import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from "rxjs";
import { GeneralComplainModel } from '@fi-sas/backoffice/modules/volunteering/modules/complains/models/general-complains.model';

@Injectable({
  providedIn: 'root'
})
export class GeneralComplainsService extends Repository<any>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.COMPLAINS_GENERAL';
    this.entity_url = 'VOLUNTEERING.COMPLAINS_GENERAL';
  }

  saveResponse(id: number, response: GeneralComplainModel): Observable<Resource<GeneralComplainModel>> {
    return this.resourceService.create<GeneralComplainModel>(this.urlService.get('VOLUNTEERING.COMPLAINS_GENERAL_RESPONSE', { id }), response);
  }

  saveStatus(id: number, status: GeneralComplainModel): Observable<Resource<GeneralComplainModel>> {
    return this.resourceService.create<GeneralComplainModel>(this.urlService.get('VOLUNTEERING.COMPLAINS_GENERAL_STATUS', { id }), status);
  }
}
