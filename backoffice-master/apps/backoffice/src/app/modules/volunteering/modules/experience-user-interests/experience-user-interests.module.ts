import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ApplicationsModule } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/applications.module';
import { ExperienceUserInterestsRoutingModule } from './experience-user-interests-routing.module';
import { ExperienceUserInterestsService } from './services/experience-user-interests.service';
import { ListExperienceUserInterestsComponent } from './pages/list-experience-user-interests/list-experience-user-interests.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewExperienceUserInterestComponent } from './pages/view-experience-user-interests/view-experience-user-interests.component';
import { VSharedModule } from '../v-shared/v-shared.module';

@NgModule({
  declarations: [ListExperienceUserInterestsComponent, ViewExperienceUserInterestComponent],
  imports: [ApplicationsModule, CommonModule, ExperienceUserInterestsRoutingModule, SharedModule, VSharedModule],
  providers: [ExperienceUserInterestsService],
})
export class ExperienceUserInterestsModule { }
