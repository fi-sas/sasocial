import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { ExperienceModel } from '@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model';
import { ApplicationModel } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';
import { CollaborationHistoricModel } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/collaboration-historic.model';
import { ExperienceUserInterestHistoryModel } from '@fi-sas/backoffice/modules/volunteering/models/experience-user-interest-history.model';

export enum ExperienceUserInterestStatus {
  ACCEPTED = 'ACCEPTED',
  ANALYSED = 'ANALYSED',
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  CLOSED = 'CLOSED',
  COLABORATION = 'COLABORATION',
  DECLINED = 'DECLINED',
  DISPATCH = 'DISPATCH',
  NOT_SELECTED = 'NOT_SELECTED',
  SUBMITTED = 'SUBMITTED',
  WAITING = 'WAITING',
  WITHDRAWAL = 'WITHDRAWAL',
  WITHDRAWAL_ACCEPTED = 'WITHDRAWAL_ACCEPTED',
}

export enum ExperienceUserInterestStatusEvent {
  ACCEPT = 'ACCEPT',
  ANALYSE = 'ANALYSE',
  APPROVE = 'APPROVE',
  CANCEL = 'CANCEL',
  CLOSE = 'CLOSE',
  COLABORATION = 'COLABORATION',
  DECLINE = 'DECLINE',
  DISPATCH = 'DISPATCH',
  NOTSELECT = 'NOTSELECT',
  WAITING = 'WAITING',
  WITHDRAWAL = 'WITHDRAWAL',
  WITHDRAWALACCEPTED = 'WITHDRAWALACCEPTED',
}

export enum ExperienceUserInterestDispatchDecision {
  ACCEPTED = 'ACCEPTED',
  NOT_SELECTED = 'NOT_SELECTED',
  REJECTED = 'REJECTED',
  WAITING = 'WAITING',
}

export class ExperienceUserInterestModel {
  application: ApplicationModel;
  certificate_file_id: number;
  created_at: string;
  decision?: string;
  experience_id: number;
  experience: ExperienceModel;
  historic_applications: ApplicationModel[];
  historic_colaborations: CollaborationHistoricModel[];
  history: ExperienceUserInterestHistoryModel[];
  id: number;
  last_status: string;
  report_avaliation_file_id: number;
  report_avaliation: string;
  status: string;
  student_avaliation: string;
  student_file_avaliation: number;
  updated_at: string;
  user_id: number;
  user?: UserModel;
}

export const ExperienceUserInterestTagResult = {
  ACCEPTED: { color: '#9DBBAE', label: 'Aceite' },
  ANALYSED: { color: '#7768AE', label: 'Em análise' },
  APPROVED: { color: '#0d69dd', label: 'Aprovada' },
  CANCELLED: { color: '#A4A4A4', label: 'Cancelada' },
  CLOSED: { color: '#000000', label: 'Fechada' },
  COLABORATION: { color: '#1ba974', label: 'Em participação' },
  DECLINED: { color: '#D0021B', label: 'Rejeitada' },
  DISPATCH: { color: '#88CCF1', label: 'Despacho' },
  NOT_SELECTED: { color: '#C1666B', label: 'Não selecionado' },
  SUBMITTED: { color: '#4d9de0', label: 'Submetida' },
  WAITING: { color: '#f8ca00', label: 'Em lista de espera' },
  WITHDRAWAL_ACCEPTED: { color: '#1ba974', label: 'Desistência aceite' },
  WITHDRAWAL: { color: '#246A73', label: 'Em desistência' },
}

export class ExperienceUserInterestData {

  static status: Record<ExperienceUserInterestStatus, { label: string; active: boolean; color: string; }> = {
    ACCEPTED: { label: 'Aceite', active: true, color: ExperienceUserInterestTagResult.ACCEPTED.color },
    ANALYSED: { label: 'Em análise', active: true, color: ExperienceUserInterestTagResult.ANALYSED.color },
    APPROVED: { label: 'Aprovada', active: true, color: ExperienceUserInterestTagResult.APPROVED.color },
    CANCELLED: { label: 'Cancelada', active: true, color: ExperienceUserInterestTagResult.CANCELLED.color },
    CLOSED: { label: 'Fechada', active: true, color: ExperienceUserInterestTagResult.CLOSED.color },
    COLABORATION: { label: 'Em participação', active: true, color: ExperienceUserInterestTagResult.COLABORATION.color },
    DECLINED: { label: 'Rejeitada', active: true, color: ExperienceUserInterestTagResult.DECLINED.color },
    DISPATCH: { label: 'Despacho', active: true, color: ExperienceUserInterestTagResult.DISPATCH.color },
    NOT_SELECTED: { label: 'Não selecionado', active: true, color: ExperienceUserInterestTagResult.NOT_SELECTED.color },
    SUBMITTED: { label: 'Submetida', active: true, color: ExperienceUserInterestTagResult.SUBMITTED.color },
    WAITING: { label: 'Lista de espera', active: true, color: ExperienceUserInterestTagResult.WAITING.color },
    WITHDRAWAL_ACCEPTED: { label: 'Desistência aceite', active: true, color: ExperienceUserInterestTagResult.WITHDRAWAL_ACCEPTED.color },
    WITHDRAWAL: { label: 'Em desistência', active: true, color: ExperienceUserInterestTagResult.WITHDRAWAL.color },
  };

  static statusMachine: Record<ExperienceUserInterestStatus, Partial<Record<ExperienceUserInterestStatusEvent, { label: string; color: string; }>>> = {
    SUBMITTED: {
      ANALYSE: { label: 'Analisar', color: ExperienceUserInterestTagResult.ANALYSED.color },
      CANCEL: { label: 'Cancelar', color: ExperienceUserInterestTagResult.CANCELLED.color },
    },
    ANALYSED: {
      CANCEL: { label: 'Cancelar', color: ExperienceUserInterestTagResult.CANCELLED.color },
      DISPATCH: { label: 'Despacho', color: ExperienceUserInterestTagResult.DISPATCH.color },
    },
    DISPATCH: {
      APPROVE: { label: 'Aprovar', color: ExperienceUserInterestTagResult.APPROVED.color },
      WAITING: { label: 'Lista de espera', color: ExperienceUserInterestTagResult.WAITING.color },
      NOTSELECT: { label: 'Não selecionado', color: ExperienceUserInterestTagResult.NOT_SELECTED.color },
      DECLINE: { label: 'Rejeitar', color: ExperienceUserInterestTagResult.DECLINED.color },
    },
    APPROVED: {
      ACCEPT: { label: 'Aceitar', color: ExperienceUserInterestTagResult.ACCEPTED.color },
      CANCEL: { label: 'Cancelar', color: ExperienceUserInterestTagResult.CANCELLED.color },
    },
    ACCEPTED: {
      CANCEL: { label: 'Cancelar', color: ExperienceUserInterestTagResult.CANCELLED.color },
      COLABORATION: { label: 'Em colaboração', color: ExperienceUserInterestTagResult.COLABORATION.color },
    },
    COLABORATION: {
      WITHDRAWAL: { label: 'Desistir', color: ExperienceUserInterestTagResult.WITHDRAWAL.color },
      CLOSE: { label: 'Fechar', color: ExperienceUserInterestTagResult.CLOSED.color },
    },
    WAITING: {
      ANALYSE: { label: 'Analisar', color: ExperienceUserInterestTagResult.ANALYSED.color },
      CANCEL: { label: 'Cancelar', color: ExperienceUserInterestTagResult.CANCELLED.color },
      CLOSE: { label: 'Fechar', color: ExperienceUserInterestTagResult.CLOSED.color },
    },
    NOT_SELECTED: {
      ANALYSE: { label: 'Analisar', color: ExperienceUserInterestTagResult.ANALYSED.color },
      CLOSE: { label: 'Fechar', color: ExperienceUserInterestTagResult.CLOSED.color },
    },
    WITHDRAWAL: {
      WITHDRAWALACCEPTED: { label: 'Desistência aceite', color: ExperienceUserInterestTagResult.WITHDRAWAL.color },
      COLABORATION: { label: 'Em colaboração', color: ExperienceUserInterestTagResult.COLABORATION.color },
    },
    WITHDRAWAL_ACCEPTED: {
    },
    CANCELLED: {},
    DECLINED: {},
    CLOSED: {},
  };

  static eventToDispatchDecisionMapper: Partial<Record<ExperienceUserInterestStatusEvent, ExperienceUserInterestDispatchDecision>> = {
    APPROVE: ExperienceUserInterestDispatchDecision.ACCEPTED,
    DECLINE: ExperienceUserInterestDispatchDecision.REJECTED,
    NOTSELECT: ExperienceUserInterestDispatchDecision.NOT_SELECTED,
    WAITING: ExperienceUserInterestDispatchDecision.WAITING,
  }

  static decisionToStateMapper: Record<ExperienceUserInterestDispatchDecision, ExperienceUserInterestStatus> = {
    ACCEPTED: ExperienceUserInterestStatus.ACCEPTED,
    REJECTED: ExperienceUserInterestStatus.DECLINED,
    NOT_SELECTED: ExperienceUserInterestStatus.NOT_SELECTED,
    WAITING: ExperienceUserInterestStatus.WAITING,
  }
}
