<div class="container">
  <div nz-row>
    <div nz-col [nzXs]="24" [nzSm]="24" [nzMd]="4" [nzLg]="4" [nzXl]="3" [nzXXl]="3">
      <div nz-row>
        <nz-form-label>Data de submissão</nz-form-label>
        <nz-form-control>
          <nz-date-picker [(ngModel)]="filters.created_at"></nz-date-picker>
        </nz-form-control>
      </div>
    </div>
    <div nz-col [nzXs]="0" [nzSm]="0" [nzMd]="1" [nzLg]="1" [nzXl]="1" [nzXXl]="1"></div>
    <div nz-col [nzXs]="24" [nzSm]="24" [nzMd]="5" [nzLg]="5" [nzXl]="5" [nzXXl]="5">
      <div nz-row>
        <nz-form-label>Candidato</nz-form-label>
        <nz-form-control>
          <fi-sas-user-select [(ngModel)]="filters.user_id" [returnID]="true"></fi-sas-user-select>
        </nz-form-control>
      </div>

    </div>

    <div nz-col [nzXs]="0" [nzSm]="0" [nzMd]="1" [nzLg]="1" [nzXl]="1" [nzXXl]="1"></div>
    <div nz-col [nzXs]="24" [nzSm]="24" [nzMd]="5" [nzLg]="5" [nzXl]="5" [nzXXl]="5">
      <div nz-row>
        <nz-form-label>Ações</nz-form-label>
        <nz-form-control>
          <nz-select name="experience_id" [(ngModel)]="filters.experience_id" [nzLoading]="loadingExperiences" style="width: 100%;" nzAllowClear>
          <nz-option *ngFor="let experience of experiences"
            [nzLabel]="(experience?.translations | translation).title"
            [nzValue]="experience.id">
          </nz-option>
        </nz-select>
        </nz-form-control>
      </div>

    </div>

    <div nz-col [nzXs]="0" [nzSm]="0" [nzMd]="1" [nzLg]="1" [nzXl]="1" [nzXXl]="1"></div>

    <div nz-col [nzXs]="12" [nzSm]="6" [nzMd]="4" [nzLg]="3" [nzXl]="3" [nzXXl]="3" class="btn-filter">
      <button nz-button nzType="primary" class="btn" (click)="searchData(true)">Filtrar</button>
    </div>
    <div nz-col [nzXs]="12" [nzSm]="6" [nzMd]="4" [nzLg]="2" [nzXl]="1" [nzXXl]="1" class="btn-filter"
      style="text-align: left;">
      <i nz-icon nzType="delete" class="btn-delete" nzTheme="outline" (click)="listComplete()"></i>
    </div>
  </div>
</div>



<div class="container">
  <nz-table class="content-table" [nzScroll]="{ x: '240px' }" #refTable [nzData]="data"
    (nzCurrentPageDataChange)="currentPageDataChange($event)" [nzLoading]="loading" [nzFrontPagination]="false"
    [(nzPageIndex)]="pageIndex" [nzShowSizeChanger]="true" [nzTotal]="totalData" [(nzPageSize)]="pageSize"
    (nzPageIndexChange)="pagination()" (nzPageSizeChange)="pagination(true)">
    <thead (nzSortChange)="sort($event)" nzSingleSort>
      <tr class="table__header">
        <th></th>
        <th *ngFor="let column of columns" [nzShowSort]="column.sortable"
          [nzSortKey]="column.sortKey ? column.sortKey : column.key" [nzFilters]="column.filters ? column.filters : []"
          [nzFilterMultiple]="column.filterMultiple" [nzShowFilter]="column.showFilter"
          (nzFilterChange)="onColumnFilterChange($event, column.key)">
          {{ column.label }}
        </th>
        <th nzRight="0px" class="text-right">Ações</th>
      </tr>
    </thead>
    <tbody>
      <ng-template ngFor let-data [ngForOf]="refTable.data">

        <tr>
          <td nzShowExpand [(nzExpand)]="mapOfExpandData[data.id]"></td>
          <td *ngFor="let column of columns">
            <span *ngIf="!column.tag">{{ column.translation && data.translation ? (data.translations | where:
              ['language_id', 3]
              )[0][column.key] : (column.template ? column.template(data) : data[column.key])}}</span>
            <span *ngIf="column.tag">
              <fi-sas-tag [value]="data[column.key]" [results]="column.tag"></fi-sas-tag>
            </span>
          </td>
          <td nzRight="0px" class="text-right">
            <ng-container *ngIf="'volunteering:experience-user-interests:status' | hasPermission">
              <button
                nz-button
                nz-dropdown
                nzType="link"
                class="options_button"
                [nzDropdownMenu]="menu"
                nzPlacement="bottomRight"
              >
                <i nz-icon nzType="dash" nzTheme="outline"></i>
              </button>
              <nz-dropdown-menu #menu="nzDropdownMenu">
                <ul nz-menu>
                  <li nz-menu-item [nzDisabled]="!(statusMachine[data.status] | keys).length" (click)="changeStateModal(data, false)">
                    Alterar estado
                  </li>
                  <li
                    nz-menu-item
                    *ngIf="data.status === ExperienceUserInterestStatus.CANCELLED || data.status === ExperienceUserInterestStatus.CLOSED || data.status === ExperienceUserInterestStatus.WITHDRAWAL_ACCEPTED"
                    (click)="reopenModal(data.id, false)"
                  >
                    Reabrir
                  </li>
                </ul>
              </nz-dropdown-menu>
            </ng-container>
          </td>
        </tr>

        <tr [nzExpand]="mapOfExpandData[data.id]">
          <td [attr.colspan]="columns.length + 3">
            <fi-sas-view-experience-user-interests *ngIf="mapOfExpandData[data.id]" [data]="data" (itemChange)="searchData()">
            </fi-sas-view-experience-user-interests>
          </td>
        </tr>
      </ng-template>
    </tbody>
  </nz-table>
</div>

<nz-modal [(nzVisible)]="isChangeStatusModalVisible" [nzTitle]="'Mudança de estado da inscrição'" [nzWidth]="900"
  [nzFooter]="changeStatusModalFooter" (nzOnCancel)="handleChangeStatusCancel()">
  <div class="content-modal">
    <p>Mudar estado para:</p>
    <div nz-row nzType="flex" class="actions" nzGutter="24" *ngIf="isChangeStatusModalVisible && changeStatusModalExperience.status!=='DISPATCH'">
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['SUBMIT']">
        <div (click)="changeStatusModalActiveAction('submit')" class="actions__button--submit"
          [ngClass]="{ 'actions__button--submit--active': changeStatusModalExperienceAction === 'submit'}">Submeter
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['ANALYSE']">
        <div (click)="changeStatusModalActiveAction('analyse')" class="actions__button--analyse"
          [ngClass]="{ 'actions__button--analyse--active': changeStatusModalExperienceAction === 'analyse'}">Analisar
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['APPROVE']">
        <div (click)="changeStatusModalActiveAction('approve')" class="actions__button--approve"
          [ngClass]="{ 'actions__button--approve--active': changeStatusModalExperienceAction === 'approve'}">Aprovar
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['DISPATCH']">
        <div (click)="changeStatusModalActiveAction('dispatch')" class="actions__button--dispatch"
          [ngClass]="{ 'actions__button--dispatch--active': changeStatusModalExperienceAction === 'dispatch'}">
          Despacho
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['ACCEPT']">
        <div (click)="changeStatusModalActiveAction('accept')" class="actions__button--accept"
          [ngClass]="{ 'actions__button--accept--active': changeStatusModalExperienceAction === 'accept'}">Aceite
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['WAITING']">
        <div (click)="changeStatusModalActiveAction('waiting')" class="actions__button--waiting"
          [ngClass]="{ 'actions__button--waiting--active': changeStatusModalExperienceAction === 'waiting'}">Lista de
          espera
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['NOTSELECT']">
        <div (click)="changeStatusModalActiveAction('notselect')" class="actions__button--notselect"
          [ngClass]="{ 'actions__button--notselect--active': changeStatusModalExperienceAction === 'notselect'}">Não
          selecionado
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['SELECTION']">
        <div (click)="changeStatusModalActiveAction('selection')" class="actions__button--selection"
          [ngClass]="{ 'actions__button--selection--active': changeStatusModalExperienceAction === 'selection'}">Seleção
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['COLABORATION']">
        <div (click)="changeStatusModalActiveAction('colaboration')" class="actions__button--colaboration"
          [ngClass]="{ 'actions__button--colaboration--active': changeStatusModalExperienceAction === 'colaboration'}">
          Em participação
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['WITHDRAWAL']">
        <div (click)="changeStatusModalActiveAction('withdrawal')" class="actions__button--withdrawal"
          [ngClass]="{ 'actions__button--withdrawal--active': changeStatusModalExperienceAction === 'withdrawal'}">
          Desistir
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['WITHDRAWALACCEPTED']">
        <div (click)="changeStatusModalActiveAction('withdrawalaccepted')" class="actions__button--withdrawalaccepted"
          [ngClass]="{ 'actions__button--withdrawalaccepted--active': changeStatusModalExperienceAction === 'withdrawalaccepted'}">
          Aceitar desistência
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['DECLINE']">
        <div (click)="changeStatusModalActiveAction('decline')" class="actions__button--decline"
          [ngClass]="{ 'actions__button--decline--active': changeStatusModalExperienceAction === 'decline'}">Rejeitar
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['CANCEL']">
        <div (click)="changeStatusModalActiveAction('cancel')" class="actions__button--cancel"
          [ngClass]="{ 'actions__button--cancel--active': changeStatusModalExperienceAction === 'cancel'}">Cancelar
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button" *ngIf="changeStatusModalActions['CLOSE']">
        <div (click)="changeStatusModalActiveAction('close')" class="actions__button--close"
          [ngClass]="{ 'actions__button--close--active': changeStatusModalExperienceAction === 'close'}">Fechar
        </div>
      </div>
    </div>
    <div nz-row nzType="flex" class="actions" nzGutter="24" *ngIf="isChangeStatusModalVisible && changeStatusModalExperience.status==='DISPATCH'">
      <div nz-col nzSpan="8" class="actions__button">
        <div (click)="changeStatusModalActiveAction('accept')" class="actions__button--accept"
          [ngClass]="{ 'actions__button--accept--active': changeStatusModalExperienceAction === 'accept'}">
          Aceitar Despacho
        </div>
      </div>
      <div nz-col nzSpan="8" class="actions__button">
        <div (click)="changeStatusModalActiveAction('reject')" class="actions__button--decline"
          [ngClass]="{ 'actions__button--decline--active': changeStatusModalExperienceAction === 'reject'}">
          Rejeitar Despacho
        </div>
      </div>
    </div>
    <div *ngIf="changeStatusModalExperienceAction === 'dispatch'">
      <div nz-row>
        <form [formGroup]="form">
          <nz-form-item>
            <nz-form-label nzRequired [nzSpan]="24" nzFor="decision">Decisão</nz-form-label>
            <nz-form-control [nzSpan]="24" [nzErrorTip]="decisionErrorTpl">
              <nz-select formControlName="decision" name="decision" id="decision" nzAllowClear>
                <nz-option *ngFor="let decision of getDispatchActions()" [nzLabel]="decision.label" [nzValue]="decision.key"></nz-option>
              </nz-select>
              <ng-template #decisionErrorTpl let-control>
                <ng-container *ngIf="control.hasError('required')">Selecione uma decisão.</ng-container>
              </ng-template>
            </nz-form-control>
          </nz-form-item>
        </form>
      </div>
    </div>

    <nz-divider></nz-divider>
    <nz-form-item>
      <div nz-row nzType="flex" nzGutter="24">
        <div nz-col nzSpan="4">
          Observações:
        </div>
        <div nz-col nzSpan="20">
          <textarea maxlength="255" nz-input [(ngModel)]="changeStatusModalNotes" rows="4"></textarea>
        </div>
      </div>
    </nz-form-item>
    <ng-template #changeStatusModalFooter>
      <button nz-button nzType="primary" (click)="handleChangeStatusOk()"
        [ngClass]="{'disabled': changeStatusModalExperienceAction === null}" [nzLoading]="changeStatusModalLoading">
        Mudar de estado
      </button>
    </ng-template>
  </div>
</nz-modal>
