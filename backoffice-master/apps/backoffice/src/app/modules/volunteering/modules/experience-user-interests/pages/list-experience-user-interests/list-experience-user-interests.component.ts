import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import * as moment from 'moment';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ExperiencesService } from '@fi-sas/backoffice/modules/volunteering/modules/experiences/services/experiences.service';
import { ExperienceUserInterestData, ExperienceUserInterestModel, ExperienceUserInterestStatus, ExperienceUserInterestTagResult } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/models/experience-user-interest.model';
import { ExperienceUserInterestsService } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/services/experience-user-interests.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ExperienceModel } from '../../../experiences/models/experience.model';


@Component({
  selector: 'fi-sas-list-experience-user-interests',
  templateUrl: './list-experience-user-interests.component.html',
  styleUrls: ['./list-experience-user-interests.component.less'],
})
export class ListExperienceUserInterestsComponent extends TableHelper implements OnInit {
  ExperienceUserInterestStatus = ExperienceUserInterestStatus;
  status = ExperienceUserInterestData.status;
  ExperienceUserInterestTagResult = ExperienceUserInterestTagResult;
  statusMachine = ExperienceUserInterestData.statusMachine;
  experienceStatuses = [];
  languages = [];

  // FILTERS
  students_loading = false;
  students: UserModel[] = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExperience: ExperienceUserInterestModel = null;
  changeStatusModalActions = {};
  changeStatusModalExperienceAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  changeStatusRequirementNumber = null;

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });

  loadingExperiences = false
  experiences: ExperienceModel [] = [];

  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public experienceUserInterestsService: ExperienceUserInterestsService,
    public experienceService: ExperiencesService,
    private languageService: LanguagesService,
    private authService: AuthService,
    private modalService: NzModalService,
  ) {
    super(uiService, router, activatedRoute);
    this.persistentFilters = {
      withRelated: 'experience,user,history,application,historic_applications,historic_colaborations,contract_file'
    }
  }

  ngOnInit() {
    this.loadStatuses();
    this.loadLanguages();
    this.loadExperiences();
    const status: string = this.activatedRoute.snapshot.queryParamMap.get('status');

    if (status) {
      this.persistentFilters['status'] = status;
    }

    this.columns.push(
      {
        key: 'created_at',
        label: 'Data de submissão',
        template: (data) => moment(data.created_at).format('DD/MM/YYYY'),
        sortable: true,
      },
      {
        key: null,
        label: 'Nº Estudante',
        template: (data) => (data.user && data.user.student_number ? data.user.student_number : data.application? data.application.student_number :null),
        sortable: false,
      },
      {
        key: null,
        label: 'Nome',
        template: (data) => data.user ? data.user.name : null,
        sortable: false,
      },
      {
        key: null,
        label: 'Titulo da ação',
        template: (data) => data.experience.translations[0] ? data.experience.translations[0].title : null,
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: this.status,
        filters: this.experienceStatuses.map(experienceStatus => {
          return {
            text: experienceStatus.label,
            value: experienceStatus.key,
          }
        }),
        showFilter: true,
        filterMultiple: false,
      },
    );

    this.initTableData(this.experienceUserInterestsService);
  }

  loadExperiences(){
    this.loadingExperiences = true;
    this.experienceService.list(1, -1, null, null, { withRelated: "translations" }, null).pipe(first(), finalize(() => this.loadingExperiences = false))
      .subscribe(response => {
        this.experiences = response.data;
      })
  }

  loadLanguages() {
    this.loading = true;

    this.languageService
        .list(1, -1, "id", "ascend")
        .pipe(first(), finalize(() => this.loading = false))
        .subscribe(result => this.languages = result.data);
  }

  listComplete() {
    this.filters.created_at = null;
    this.filters.user_id = null;
    this.filters.experience_id = null;
    this.searchData(true)
  }

  loadStatuses() {
    this.experienceStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  deleteExperience(id: number) {
    if(!this.authService.hasPermission('volunteering:experience-user-interests:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService
        .showConfirm('Eliminar', 'Pretende eliminar esta inscrição?', 'Eliminar')
        .pipe(first())
        .subscribe((confirm) => {
          if (confirm) {
            this.experienceUserInterestsService
                .delete(id)
                .pipe(first())
                .subscribe((result) => {
                  this.uiService.showMessage(
                    MessageType.success,
                    'Inscrição eliminada com sucesso',
                  );
                  this.searchData();
                });
          }
        });
  }

  changeStateModal(experience: ExperienceUserInterestModel, disabled: boolean) {
    if(!this.authService.hasPermission('volunteering:experience-user-interests:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(!Object.keys(ExperienceUserInterestData.statusMachine[experience.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalExperience = experience;
      this.changeStatusModalActions = ExperienceUserInterestData.statusMachine[experience.status];
    }
  }

  reopenModal(id: number, disabled: boolean) {
    if(!this.authService.hasPermission('volunteering:experience-user-interests:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if (!disabled) {
      this.uiService
      .showConfirm('Reabrir inscrição', 'Pretende reabrir esta inscrição?', 'Reabrir')
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.experienceUserInterestsService
              .revertStatus(id)
              .pipe(first())
              .subscribe((result) => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Inscrição reaberta com sucesso',
                );
                this.searchData();
              });
        }
      });
    }
  }

  handleChangeStatusOk() {
    this.changeStatusModalLoading = true;

    const obj: any = {
      event: this.changeStatusModalExperienceAction.toUpperCase(),
      notes: this.changeStatusModalNotes,
    };

    if (this.changeStatusRequirementNumber != null) {
      obj.experience = {requirement_number: this.changeStatusRequirementNumber};
    }

    this.experienceService
        .read(this.changeStatusModalExperience.experience_id, { withRelated: ['users_colaboration'] })
        .subscribe((result) => {
          if (obj.event === 'APPROVE' && this.changeStatusModalExperience.experience.number_simultaneous_candidates <=  result.data[0].users_colaboration.length) {
            this.uiService
                .showConfirm('Máximo de inscrições atingido', 'Foi atingido o máximo de inscrições definidas. Tem a certeza que pretende aprovar a inscrição?', 'Aprovar inscrição')
                .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
                .subscribe((confirm) => {
                  if (confirm) {
                    this.changeCandidateStatus(obj);
                  }
                });
          } else {
            this.changeCandidateStatus(obj);
          }
        });
  }

  changeCandidateStatus(obj: any) {
    /*this.experienceUserInterestsService
        .status(this.changeStatusModalExperience.id, obj)
        .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
        .subscribe((result) => {
          this.changeStatusModalExperience.status = result.data[0].status;
          this.handleChangeStatusCancel();
        });*/   
        if(this.changeStatusModalExperience.status !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'INTERVIEW'){
          this.experienceUserInterestsService
          .status(this.changeStatusModalExperience.id, obj)
          .pipe(
            first(),
            finalize(() => (this.changeStatusModalLoading = false))
          )
          .subscribe((result) => {
            this.changeStatusModalExperience.status = result.data[0].status;
            this.uiService.showMessage(MessageType.success, 'Alteração de estado efetuada com sucesso.');
            this.handleChangeStatusCancel();
          });
        }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'DISPATCH'){
          if(this.form.valid){
            this.experienceUserInterestsService.sendDispatch(this.changeStatusModalExperience.id, {
              event: this.changeStatusModalExperienceAction.toUpperCase(),
              decision: this.form.get('decision').value,
              notes: this.changeStatusModalNotes,
            })
              .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
              .subscribe(() => {
                this.handleChangeStatusCancel();
              });
          }else{
            this.updateAndValidityForm(this.form);
            this.changeStatusModalLoading = false;
          }
        }else if(this.changeStatusModalExperience.status === 'DISPATCH'){
          this.experienceUserInterestsService
          .sendDispatch(this.changeStatusModalExperience.id, { decision_dispatch: this.changeStatusModalExperienceAction.toUpperCase() })
          .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
          .subscribe(() => this.handleChangeStatusCancel());
        }else{
          this.changeStatusModalLoading = false;
        }
    
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }


  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalExperience = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalExperienceAction = null;
    this.changeStatusModalNotes = '';
    this.form.reset();
    this.searchData();
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalExperienceAction = action;
    this.form.reset();
  }

  /*dispatchModal(userInterest: ExperienceUserInterestModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.experienceUserInterestsService.sendDispatch(userInterest.id, data),
        actions: Object.keys(ExperienceUserInterestData.statusMachine.DISPATCH).map<{
          key: ExperienceUserInterestStatus;
          label: string;
        }>((key) => ({
          key: ExperienceUserInterestData.eventToDispatchDecisionMapper[key],
          label: ExperienceUserInterestData.statusMachine.DISPATCH[key].label,
        })),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance
              .onSubmit()
              .then(() => {
                this.searchData();
                modalRef.close();
              })
              .catch(() => null);
          },
        },
      ],
    });
  }*/

  getDispatchActions = () => {
    return Object.keys(ExperienceUserInterestData.statusMachine.DISPATCH).map<{
      key: ExperienceUserInterestData;
      label: string;
    }>((key) => ({
      key: ExperienceUserInterestData.eventToDispatchDecisionMapper[key],
      label: ExperienceUserInterestData.statusMachine.DISPATCH[key].label,
    }));
  }


  dispatchChangeStatues(userInterest: ExperienceUserInterestModel, acceptDecision: boolean) {
    this.experienceUserInterestsService
      .sendDispatch(userInterest.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }
}
