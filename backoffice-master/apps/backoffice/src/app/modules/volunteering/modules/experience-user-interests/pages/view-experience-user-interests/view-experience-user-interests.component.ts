import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';

import { ApplicationModel } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';
import { ApplicationsService } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/services/applications.service';
import { ExperienceUserInterestData, ExperienceUserInterestModel, ExperienceUserInterestTagResult } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/models/experience-user-interest.model';
import { ExperienceUserInterestsService } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/services/experience-user-interests.service';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { StatusInfoModel } from '@fi-sas/backoffice/modules/volunteering/models/status-info.model';
import { ViewApplicationModalComponent } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/pages/view-application-modal/view-application-modal.component';

@Component({
  selector: 'fi-sas-view-experience-user-interests',
  templateUrl: './view-experience-user-interests.component.html',
  styleUrls: ['./view-experience-user-interests.component.less'],
})
export class ViewExperienceUserInterestComponent {
  @Input() data: ExperienceUserInterestModel = null;
  UsersInterestTagResult = ExperienceUserInterestTagResult;
  loading = false;

  experienceUserInterest: ExperienceUserInterestModel = null;
  status = ExperienceUserInterestData.status;
  statusMachine = ExperienceUserInterestData.statusMachine;
  decisionToStateMapper = ExperienceUserInterestData.decisionToStateMapper;
  modalsRefs: { name: string; modal: NzModalRef }[] = [];
  loadingCertificate = false;
  statusInfo: StatusInfoModel;

  application: ApplicationModel = null;

  @Output() itemChange = new EventEmitter<boolean>();

  constructor(
    private uiService: UiService,
    private modalService: NzModalService,
    public experienceUserInterestService: ExperienceUserInterestsService,
    public applicationsService: ApplicationsService,
  ) { }

  createTplModal(
    tplTitle: TemplateRef<{}>,
    tplContent: TemplateRef<{}>,
    tplFooter: TemplateRef<{}>,
    size: number = 900,
    padding: string = '25px'
  ): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size,
      nzBodyStyle: { padding: padding },
    });
  }

  generateModal(
    name: string,
    modalTitle: TemplateRef<{}>,
    modalContent: TemplateRef<{}>,
    modalFooter: TemplateRef<{}>,
    modalSize: number,
    padding?: string
  ) {
    this.modalsRefs.push({
      name: name,
      modal: this.createTplModal(
        modalTitle,
        modalContent,
        modalFooter,
        modalSize,
        padding
      ),
    });
  }

  getExperienceCertificate(id) {
    this.loadingCertificate = true;
    this.applicationsService.generateCertificate(id).pipe(first(), finalize(() => this.loadingCertificate = false)).subscribe((data) => {
      this.uiService.showMessage(
        MessageType.success,
        'Certificado gerado com sucesso'
      );
    })
  }

  filterSchedule(scheduleID: number) {
    const found = this.data.application.preferred_schedule.find(
      (schedule) => schedule.id === scheduleID
    );

    return found !== undefined;
  }

  viewApplicationModal(id: number) {
    this.applicationsService
      .read(id, {
        id: id,
        withRelated: "history,experience,area_interests,attendance,user,course,course_degree,school,organic_units,preferred_schedule,historic_colaborations,historic_applications,skills"
      })
      .pipe(
        first(),
        finalize(() => (true))
      )
      .subscribe((result) => {
        this.application = result.data[0];
        this.modalService.create({
          nzWidth: 1024,
          nzMaskClosable: false,
          nzClosable: true,
          nzContent: ViewApplicationModalComponent,
          nzFooter: null,
          nzComponentParams: {
            application: this.application,
          },
          nzTitle: 'Inscrição do estudante ' + this.application.name
        });
      });
  }

  dispatchChangeStatues(application: ExperienceUserInterestModel, acceptDecision: boolean) {
    this.experienceUserInterestService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.itemChange.emit(true);
      });
  }
}
