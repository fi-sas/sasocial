import { TestBed } from '@angular/core/testing';

import { ExperienceUserInterestsService } from './experience-user-interests.service';

describe('ExperiencesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExperienceUserInterestsService = TestBed.get(ExperienceUserInterestsService);
    expect(service).toBeTruthy();
  });
});
