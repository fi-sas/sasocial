import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ExperienceModel } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { ApproveRejectModel } from "@fi-sas/backoffice/modules/volunteering/models/approveReject";
import { Observable } from "rxjs";
import { SendDispatchData } from '../../../models/dispatch.model';

@Injectable({
  providedIn: 'root'
})
export class ExperienceUserInterestsService extends Repository<ExperienceModel>{

  constructor(
    resourceService: FiResourceService,
    urlService: FiUrlService
  ) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.EXPERIENCE_USER_INTERESTS';
    this.entity_url = 'VOLUNTEERING.EXPERIENCE_USER_INTERESTS_ID';
  }

  status(id: number, experience_status: ApproveRejectModel): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_INTERESTS_STATUS', {id}), experience_status);
  }

  revertStatus(id: Number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_INTEREST_LAST_STATUS', {id}), {});
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_INTERESTS_STATUS', { id }),
      data
    );
  }
}
