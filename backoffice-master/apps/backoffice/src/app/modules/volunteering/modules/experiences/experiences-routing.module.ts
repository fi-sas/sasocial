import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExperiencesComponent } from './pages/list-experiences/list-experiences.component';
import { FormExperienceComponent } from './pages/form-experience/form-experience.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ListExperiencesComponent,
    data: { breadcrumb: 'Listar', title: 'Listar', scope: 'volunteering:experiences:read' },
  },{
    path: 'create',
    component: FormExperienceComponent,
    data: { breadcrumb: 'Criar', title: 'Criar', scope: 'volunteering:experiences:create' },
  },{
    path: 'edit/:id',
    component: FormExperienceComponent,
    data: { breadcrumb: 'Editar', title: 'Editar', scope: 'volunteering:experiences:create' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExperiencesRoutingModule { }
