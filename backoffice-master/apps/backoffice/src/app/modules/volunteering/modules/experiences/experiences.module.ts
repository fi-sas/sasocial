import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExperiencesRoutingModule } from './experiences-routing.module';
import { ExperiencesService } from './services/experiences.service';
import { FormExperienceComponent } from './pages/form-experience/form-experience.component';
import { ListExperiencesComponent } from './pages/list-experiences/list-experiences.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewExperienceComponent } from './pages/view-experience/view-experience.component';
import { VSharedModule } from '../v-shared/v-shared.module';


@NgModule({
  declarations: [FormExperienceComponent, ListExperiencesComponent, ViewExperienceComponent],
  imports: [CommonModule, ExperiencesRoutingModule, SharedModule, VSharedModule],
  providers: [ExperiencesService]
})
export class ExperiencesModule { }
