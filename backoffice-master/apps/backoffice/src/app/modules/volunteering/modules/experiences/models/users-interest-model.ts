import { UsersModule } from "@fi-sas/backoffice/modules/users/users.module";

export class UsersInterestModel {
  id: number;
  experience_id: number;
  user_id: number;
  is_interest: boolean;
  user?: UsersModule;
  updated_at: string;
  created_at: string;
}
