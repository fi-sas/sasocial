import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageType, UiService } from "@fi-sas/backoffice/core/services/ui-service.service";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import {  ExperienceModel } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { OrganicUnitsService } from "@fi-sas/backoffice/modules/infrastructure/services/organic-units.service";
import { first, finalize } from "rxjs/operators";
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { VolunteeringExperiencesService } from '@fi-sas/backoffice/modules/volunteering/services/voluteeering-experiences.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { trimValidation } from '@fi-sas/backoffice/shared/validators/validators.validator';
import { NzTabSetComponent } from 'ng-zorro-antd';
import { CurrentAccountsService } from '@fi-sas/backoffice/modules/financial/services/current-accounts.service';

@Component({
  selector: 'fi-sas-form-experience',
  templateUrl: './form-experience.component.html',
  styleUrls: ['./form-experience.component.less']
})
export class FormExperienceComponent implements OnInit {
  @ViewChild('translationTabs', { static: true })
  translationTabs: NzTabSetComponent;
  @ViewChild('translationTabs2', { static: true })
  translationTabs2: NzTabSetComponent;
  @ViewChild('translationTabs3', { static: true })
  translationTabs3: NzTabSetComponent;
  idToUpdate = null;
  experienceLoading = false;
  experience: ExperienceModel = null;
  submitted = false;
  currentAcademicYear = null;

  academicYears: AcademicYearModel[] = [];
  academicYearsLoading = false;
  organicUnits: OrganicUnitsModel[] = [];
  organitUnits_loading = false;

  isScheduleModalVisible = false;
  scheduleModalBegin = null;
  scheduleModalEnd = null;
  scheduleActiveId = null;
  schedules = {
    1: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    2: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    3: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    4: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    5: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    6: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    7: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    8: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    9: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    10: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    11: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    12: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    13: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    14: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    15: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    16: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    17: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    18: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    19: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    20: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    21: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
  };
  experienceForm = new FormGroup({
    academic_year: new FormControl(null, [Validators.required]),
    publish_date: new FormControl(new Date(), [Validators.required]),
    application_deadline_date: new FormControl(moment().add(1, 'd').toDate(), [Validators.required]),
    organic_unit_id: new FormControl(null, [Validators.required]),
    address: new FormControl('', [Validators.required,trimValidation]),
    experience_responsible_id: new FormControl(null, [Validators.required]),
    number_candidates: new FormControl(1, [Validators.min(1)]),
    number_simultaneous_candidates: new FormControl(1, [Validators.min(1)]),
    interval_job: new FormControl([moment().add(1, 'd').toDate(), moment().add(3, 'd').toDate()], [Validators.required]),
    number_weekly_hours: new FormControl(0, [Validators.required, Validators.min(0)]),
    total_hours_estimation: new FormControl(0, [Validators.required, Validators.min(0)]),
    holydays_availability: new FormControl(false, [Validators.required]),
    attachment_file_id: new FormControl(null, []),
    contract_file_id: new FormControl(null, []),
    certificate_file_id: new FormControl(null, []),
    translations: new FormArray([]),
  });


  translations = this.experienceForm.get('translations') as FormArray;
  loadingLanguages = false;
  languages: LanguageModel[] = null;

  publishDate = (date) => moment(date).isBefore(new Date(), 'd');
  disabledIntervalJob = (date) => moment(date).isSameOrBefore();

  constructor(
    public experienceService: VolunteeringExperiencesService,
    private uiService: UiService,
    private router: Router,
    private location: Location,
    private activateRoute: ActivatedRoute,
    private organicUnitService: OrganicUnitsService,
    private currentAccountService: CurrentAccountsService,
    private languagesService: LanguagesService,
    private academicYearsService: AcademicYearsService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(data => {
      if (data.get('id')) {
        this.idToUpdate = data.get('id');
        this.experienceService.read(parseInt(data.get('id'), 10), {
        }).subscribe(result => {

          this.experience = result.data[0];
          this.experienceForm.patchValue({
            ...result.data[0],
            interval_job: [result.data[0].start_date, result.data[0].end_date],
          });

          if (result.data[0].schedule) {
            result.data[0].schedule.map(s => {
              this.schedules[s.schedule_id].active = true;
              this.schedules[s.schedule_id].begin = s.time_begin;
              this.schedules[s.schedule_id].end = s.time_end;
            });
          }
          this.loadLanguages();
        }, () => {
          this.location.back();
        });
      } else {
        this.loadLanguages();
      }
    });
    this.loadAcademicYears();
    this.loadOrganicUnits();
  }

  loadLanguages() {
    this.loadingLanguages = true;
    this.languagesService.list(1, -1, "order", "ascend").pipe(first(), finalize(() => this.loadingLanguages = false)).subscribe(results => {
      this.languages = results.data;
      for (const iterator of this.languages) {
        this.addTranslation(iterator.id);
      }
    });
  }
  returnButton() {
    this.router.navigate(['volunteering', 'experiences', 'list']);
  }

  getLanguageName(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }
  async addTranslation(language_id: number) {
    const translations = this.experienceForm.controls.translations as FormArray;
    if (this.experience && this.experience.translations) {
      const result = await this.experience.translations.filter(x => x.language_id === language_id);
      translations.push(new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        title: new FormControl(result[0].title, Validators.required),
        description: new FormControl(result[0].description, Validators.required),
        job: new FormControl(result[0].job, Validators.required),
        proponent_service: new FormControl(result[0].proponent_service, Validators.required),
        applicant_profile: new FormControl(result[0].applicant_profile, Validators.required),
        selection_criteria: new FormControl(result[0].selection_criteria, Validators.required)
      }));
    } else {
      translations.push(new FormGroup({
        language_id: new FormControl(language_id, Validators.required),
        title: new FormControl(null, [Validators.required,trimValidation]),
        description: new FormControl(null, [Validators.required,trimValidation]),
        job: new FormControl(null, [Validators.required,trimValidation]),
        proponent_service: new FormControl(null, [Validators.required, trimValidation]),
        applicant_profile: new FormControl(null, [Validators.required, trimValidation]),
        selection_criteria: new FormControl(null, [Validators.required, trimValidation])
      }));
    }
  }

  loadAcademicYears() {
    this.academicYearsLoading = true;

    this.academicYearsService
        .list(1, -1)
        .pipe(first(), finalize(() => this.academicYearsLoading = false))
        .subscribe(results => {
          this.academicYears = results.data
          const currentYear = results.data.filter(year => year.current === true);

          if(currentYear.length > 0) {
            this.currentAcademicYear = currentYear[0].academic_year;
            this.experienceForm.get('academic_year').setValue(currentYear[0].academic_year);
          }
        });
  }

  loadOrganicUnits() {
    this.organitUnits_loading = true;
    this.organicUnitService.list(1, -1, null, null, { withRelated: false }).pipe(first(), finalize(() => this.organitUnits_loading = false)).subscribe(results => {
      this.organicUnits = results.data;
    });
  }

  public scheduleChanged(id: number, value: boolean) {
    this.scheduleActiveId = id;
    this.scheduleModalBegin = this.schedules[this.scheduleActiveId].begin;
    this.scheduleModalEnd = this.schedules[this.scheduleActiveId].end;
    this.isScheduleModalVisible = true;
  }

  handleScheduleModalCancel() {
    this.isScheduleModalVisible = false;
  }

  handleScheduleModalOk() {
    this.isScheduleModalVisible = false;

    this.schedules[this.scheduleActiveId].begin = this.scheduleModalBegin;
    this.schedules[this.scheduleActiveId].end = this.scheduleModalEnd;
  }

  submit(valid: boolean, value: any) {
    this.submitted = true;
    const formValue = { ...value };
    let error = false;
    if(this.experienceForm.get('total_hours_estimation').value<this.experienceForm.get('number_weekly_hours').value) {
      error = true;
    }
    if (valid && !error) {
      this.submitted = false;
      this.experienceLoading = true;
      formValue.schedule = Object.keys(this.schedules).filter(s => this.schedules[s].active).map(s => {
        return {
          schedule_id: s,
          time_begin: this.schedules[s].begin,
          time_end: this.schedules[s].end,
        };
      });
      formValue.start_date = formValue.interval_job[0];
      formValue.end_date = formValue.interval_job[1];
      delete formValue.interval_job;

      // TODO: Remove this
      formValue.status = "SUBMITTED";
      if (this.experience && this.authService.hasPermission('volunteering:experiences:update')) {
        this.experienceService.update(this.idToUpdate, formValue).pipe(
          first(),
          finalize(() => this.experienceLoading = false)
        ).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Ação de voluntariado alterada com sucesso');
          this.location.back();
        });
      } else if(this.authService.hasPermission('volunteering:experiences:create')) {
        this.experienceService.create(formValue).pipe(
          first(),
          finalize(() => this.experienceLoading = false)
        ).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Ação de voluntariado inserida com sucesso');
          this.router.navigate(['volunteering', 'experiences', 'list']);
        });
      } else {
        this.uiService.showMessage(
          MessageType.warning,
          'O utilizador não tem acesso ao serviço solicitado'
        );
        return;
      }
    } else {

      let tabIndex = 0;
      for (const t in this.translations.controls) {
        if (t) {
          const tt = this.translations.get(t) as FormGroup;
          if (!tt.valid) {
            this.translationTabs.nzSelectedIndex = tabIndex;
            break;
          }
        }
        tabIndex++;
      }
      let tabIndex2 = 0;
      for (const t in this.translations.controls) {
        if (t) {
          const tt = this.translations.get(t) as FormGroup;
          if (!tt.valid) {
            this.translationTabs2.nzSelectedIndex = tabIndex;
            break;
          }
        }
        tabIndex2++;
      }
      let tabIndex3 = 0;
      for (const t in this.translations.controls) {
        if (t) {
          const tt = this.translations.get(t) as FormGroup;
          if (!tt.valid) {
            this.translationTabs3.nzSelectedIndex = tabIndex;
            break;
          }
        }
        tabIndex3++;
      }
      for (const i in this.experienceForm.controls) {
        if (this.experienceForm.controls[i]) {
          this.experienceForm.controls[i].markAsDirty();
          this.experienceForm.controls[i].updateValueAndValidity();
        }
      }
    }
  }


}
