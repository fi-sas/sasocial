import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { first, finalize } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import * as moment from 'moment';

import { ExperienceData, ExperienceModel, ExperienceStatus } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { CoursesService } from '@fi-sas/backoffice/modules/configurations/services/courses.service';
import { DispatchModalComponent } from '../../../v-shared/components';
import { ExperiencesService } from '@fi-sas/backoffice/modules/volunteering/modules/experiences/services/experiences.service';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { SendDispatchData } from '@fi-sas/backoffice/modules/volunteering/models/dispatch.model';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { VolunteeringExperiencesService } from '@fi-sas/backoffice/modules/volunteering/services/voluteeering-experiences.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'fi-sas-list-experience',
  templateUrl: './list-experiences.component.html',
  styleUrls: ['./list-experiences.component.less']
})
export class ListExperiencesComponent extends TableHelper implements OnInit {
  ExperienceStatus = ExperienceStatus;
  status = ExperienceData.status;
  statusMachine = ExperienceData.statusMachine;
  experienceStatuses = [];
  languages = [];

  // FILTERS
  academic_years_loading = false;
  organic_units_loading = false;
  academic_years: AcademicYearModel[] = [];
  organic_units: OrganicUnitsModel[] = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExperience: ExperienceModel = null;
  changeStatusModalActions = {};
  changeStatusModalExperienceAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  changeStatusRequirementNumber = null;

  courses_loading = false;
  courses: CourseModel[] = [];

  // NOTIFICATION MODAL STATUS
  isNotificationModalVisible = false;
  notificationModalExperience: ExperienceModel = null;
  notificationModalCourseYear = null;
  notificationModalCourseId = null;
  notificationModalSchoolId = null;
  notificationModalMessage = null;
  notificationModalTargetUsers = [];
  notificationModalSearchTargetsLoading = false;
  notificationModalNotifyLoading = false;

  currentAcademicYear: AcademicYearModel[];

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });
  
  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    private academicYearsService: AcademicYearsService,
    private organicUnitsService: OrganicUnitsService,
    public experienceService: VolunteeringExperiencesService,
    private authService: AuthService,
    private languageService: LanguagesService,
    public experiencesService: ExperiencesService,
    private coursesService: CoursesService,
    private modalService: NzModalService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadAcademicYears();
    this.organicUnitsLoad();
    this.loadLanguages();
    this.loadStatuses();
    this.loadCourses();

    this.persistentFilters['searchFields'] = "title,description";
    this.persistentFilters['withRelated'] = 'experience_responsible,organic_unit,translations, users_colaboration';
    let status: string = this.activatedRoute.snapshot.queryParamMap.get('status');
    if (status) {
      this.persistentFilters['status'] = status;
    }
    let academicYear: string = this.activatedRoute.snapshot.queryParamMap.get('academicYear');
    if (academicYear) {
      this.filters.academic_year = academicYear;
    } else {
      this.loadCurrentAcademicYear();
    }
    this.initTableData(this.experienceService);
  }

  checkTooltip(experience){
    return experience.publish_date && moment(new Date(), "YYYY-MM-DD").isSameOrAfter(moment(experience.publish_date).format('YYYY-MM-DD')) && (experience.status === 'SUBMITTED' || experience.status === 'SELECTION') ? true : false;
  }

  getWarning(publish_date){
    return moment(publish_date).format("DD/MM/YYYY") + String.fromCodePoint(0x2757)
  }

  getTooltip(publish_date){
    if(moment(moment(publish_date).format("YYYY-MM-DD")).isSame(moment(new Date()).format('YYYY-MM-DD'))){
      return "Data próxima de expiração!"
    }else if(moment(new Date(), "YYYY-MM-DD").isAfter(moment(publish_date).format('YYYY-MM-DD'))){
      return "Data expirada!"
    }
    
  }

  loadLanguages() {
    this.loading = true;
    this.languageService.list(1, -1).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.languages = result.data;
    });
  }

  loadStatuses() {
    this.experienceStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  loadCourses() {
    this.courses_loading = true;

    this.coursesService
      .list(1, -1)
      .pipe(first(), finalize(() => this.courses_loading = false))
      .subscribe(results => this.courses = results.data);
  }

  getStatusArray() {
    return Object.keys(this.status).map(k => {
      return {
        key: k,
        ...this.status[k]
      }
    })
  }

  loadAcademicYears() {
    this.academic_years_loading = true;
    this.academicYearsService
      .list(1, -1, 'start_date', 'descend')
      .pipe(first(), finalize(() => this.academic_years_loading = false))
      .subscribe(results => this.academic_years = results.data);
  }

  loadCurrentAcademicYear() {
    this.academicYearsService
      .getCurrentAcademicYear()
      .pipe(first())
      .subscribe((result) => {
        this.currentAcademicYear = result.data;
        if (this.currentAcademicYear.length !== 0) {
          this.filters.academic_year = this.currentAcademicYear[0].academic_year;
          this.searchData(true);
        }
      });
  }


  listComplete() {
    this.filters.start_date = null;
    this.filters.academic_year = null;
    this.filters.organic_unit_id = null;
    this.searchData(true)
  }

  organicUnitsLoad() {
    this.organic_units_loading = true;
    this.organicUnitsService.list(1, -1).pipe(
      first(),
      finalize(() => this.organic_units_loading = false)
    )
      .subscribe(results => {
        this.organic_units = results.data;
      })
  }

  changeStateModal(experience: ExperienceModel, disabled: boolean) {
    if (!this.authService.hasPermission('volunteering:experiences:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(!Object.keys(ExperienceData.statusMachine[experience.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalExperience = experience;
      this.changeStatusModalActions = ExperienceData.statusMachine[experience.status];
    }
  }

  edit(experience: ExperienceModel, disabled: boolean) {
    if (!disabled) {
      this.router.navigateByUrl('/volunteering/experiences/edit/' + experience.id);
    }
   }

  handleChangeStatusOk() {
    this.changeStatusModalLoading = true;

    const data: any = {
      event: this.changeStatusModalExperienceAction.toUpperCase(),
      notes: this.changeStatusModalNotes
    };

    if (this.changeStatusModalExperienceAction.toUpperCase() === 'RETURN') {
      data.experience = { return_reason: this.changeStatusModalNotes };
    }

    if(this.changeStatusModalExperience.status !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'DISPATCH'){
      this.experienceService
      .status(this.changeStatusModalExperience.id, data)
      .pipe(
        first(),
        finalize(() => (this.changeStatusModalLoading = false))
      )
      .subscribe((result) => {
        this.changeStatusModalExperience.status = result.data[0].status;
        this.handleChangeStatusCancel();
        this.searchData();
      });
    }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'DISPATCH'){
      if(this.form.valid){
        this.experienceService.sendDispatch(this.changeStatusModalExperience.id, {
          event: this.changeStatusModalExperienceAction.toUpperCase(),
          decision: this.form.get('decision').value,
          notes: this.changeStatusModalNotes,
        })
        .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
        .subscribe(() => {
          this.handleChangeStatusCancel();
          this.searchData();
        });
      }else{
        this.updateAndValidityForm(this.form);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalExperience.status === 'DISPATCH'){
      this.experienceService
      .sendDispatch(this.changeStatusModalExperience.id, { decision_dispatch: this.changeStatusModalExperienceAction.toUpperCase() })
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe(() => {
        this.handleChangeStatusCancel();
        this.searchData();
      });
    }else{
      this.changeStatusModalLoading = false;
    }
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalExperience = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalExperienceAction = null;
    this.changeStatusModalNotes = '';
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalExperienceAction = action;
  }

  notificationModal(experience: ExperienceModel, disabled: boolean) {
    if (!disabled) {
      this.isNotificationModalVisible = true;
      this.notificationModalExperience = experience;
    }
  }

  handleNotificationSearchTargets() {
    this.notificationModalSearchTargetsLoading = true;
    this.experiencesService
      .targetUsers(
        this.notificationModalExperience.id,
        this.notificationModalCourseYear,
        this.notificationModalCourseId,
        this.notificationModalSchoolId,
      )
      .pipe(first(), finalize(() => this.notificationModalSearchTargetsLoading = false))
      .subscribe((result) => this.notificationModalTargetUsers = result.data);
  }

  handleNotificationCancel() {
    this.isNotificationModalVisible = false;
    this.notificationModalExperience = null;
    this.notificationModalTargetUsers = [];
    this.notificationModalCourseYear = null;
    this.notificationModalCourseId = null;
    this.notificationModalSchoolId = null;
    this.notificationModalMessage = null;
  }

  handleNotificationOk() {
    this.notificationModalNotifyLoading = true;
    this.experiencesService
      .sendNotifications(
        this.notificationModalExperience.id,
        this.notificationModalTargetUsers.map(user => user.id),
      )
      .pipe(first(), finalize(() => this.notificationModalNotifyLoading = false))
      .subscribe(() => {
        this.handleNotificationCancel();
        this.uiService.showMessage(
          MessageType.success,
          'Notificação(ões) enviada(s) com sucesso',
        );
      });
  }

  reopenModal(id: number, disabled: boolean) {
    if (!this.authService.hasPermission('volunteering:experiences:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if (!disabled) {
      this.uiService
        .showConfirm('Reabrir ação', 'Pretende reabrir esta ação?', 'Reabrir')
        .pipe(first())
        .subscribe((confirm) => {
          if (confirm) {
            this.experienceService
              .revertStatus(id)
              .pipe(first())
              .subscribe((result) => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Reaberta reaberta com sucesso',
                );
                this.searchData();
              });
          }
        });
    }
  }

  /*dispatchModal(experience: ExperienceModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.experienceService.sendDispatch(experience.id, data),
        actions: Object.keys(ExperienceData.statusMachine.DISPATCH).map<{ key: ExperienceStatus; label: string }>(
          (key) => ({
            key: ExperienceData.eventToDispatchDecisionMapper[key],
            label: ExperienceData.statusMachine.DISPATCH[key].label,
          })
        ),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.onSubmit().then(() => {
              this.searchData();
              modalRef.close();
            });
          },
        },
      ],
    });
  }*/

  getDispatchActions = () => {
    return Object.keys(ExperienceData.statusMachine.DISPATCH).map<{ key: ExperienceStatus; label: string }>(
      (key) => ({
        key: ExperienceData.eventToDispatchDecisionMapper[key],
        label: ExperienceData.statusMachine.DISPATCH[key].label,
      }));
  }

  dispatchChangeStatues(experience: ExperienceModel, acceptDecision: boolean) {
    this.experienceService
      .sendDispatch(experience.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }
}
