import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { finalize, first } from 'rxjs/operators';

import { ExperienceData, ExperienceModel, ExperienceTagResult } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { ExperiencesService } from '../../services/experiences.service';
import { ExperienceUserInterestData, ExperienceUserInterestModel, ExperienceUserInterestTagResult } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/models/experience-user-interest.model';
import { ExperienceUserInterestsService } from '@fi-sas/backoffice/modules/volunteering/modules/experience-user-interests/services/experience-user-interests.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { VolunteeringExperiencesService } from '@fi-sas/backoffice/modules/volunteering/services/voluteeering-experiences.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'fi-sas-view-experience',
  templateUrl: './view-experience.component.html',
  styleUrls: ['./view-experience.component.less']
})
export class ViewExperienceComponent implements OnInit {
  @Input() data: ExperienceModel;

  @Output() itemChange = new EventEmitter<boolean>();

  experience: ExperienceModel;
  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExperience: ExperienceUserInterestModel = null;
  changeStatusModalActions = {};
  changeStatusModalExperienceAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  changeStatusRequirementNumber = null;

  ExperienceTagResult = ExperienceTagResult;
  UsersInterestTagResult = ExperienceUserInterestTagResult;

  @Input() languages: LanguageModel[];
  loading = false;

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });
  
  constructor(
    public experienceUserInterestsService: ExperienceUserInterestsService,
    public experienceService: ExperiencesService,
    public volunteeringExperiencesService: VolunteeringExperiencesService,
    private uiService: UiService) { }

  schedule = {
    1: { active: false, start: null, end: null },
    2: { active: false, start: null, end: null },
    3: { active: false, start: null, end: null },
    4: { active: false, start: null, end: null },
    5: { active: false, start: null, end: null },
    6: { active: false, start: null, end: null },
    7: { active: false, start: null, end: null },
    8: { active: false, start: null, end: null },
    9: { active: false, start: null, end: null },
    10: { active: false, start: null, end: null },
    11: { active: false, start: null, end: null },
    12: { active: false, start: null, end: null },
    13: { active: false, start: null, end: null },
    14: { active: false, start: null, end: null },
    15: { active: false, start: null, end: null },
    16: { active: false, start: null, end: null },
    17: { active: false, start: null, end: null },
    18: { active: false, start: null, end: null },
    19: { active: false, start: null, end: null },
    20: { active: false, start: null, end: null },
    21: { active: false, start: null, end: null },
  };

  status = ExperienceData.status;
  statusMachine = ExperienceData.statusMachine;
  decisionToStateMapper = ExperienceData.decisionToStateMapper;

  ngOnInit() {
    this.loading = true;
    this.experienceService
      .read(this.data.id, {
        id: this.data.id,
        withRelated: "history,applications,experience_responsible,organic_unit,attachment_file,contract_file,certificate_file,users_interest,translations,users_colaboration"
      })
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.loading = false;
        this.experience = result.data[0];
        if (this.experience.schedule) {
          this.experience.schedule.map(sch => {
            if (this.schedule[sch.schedule_id]) {
              this.schedule[sch.schedule_id].active = true;
              this.schedule[sch.schedule_id].start = sch.time_begin;
              this.schedule[sch.schedule_id].end = sch.time_end;
            }
          });
        }
      });
  }

  changeStateModal(experience: ExperienceUserInterestModel, disabled: boolean) {
    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalExperience = experience;
      this.changeStatusModalActions = ExperienceUserInterestData.statusMachine[experience.status];
      console.log(this.changeStatusModalActions);
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalExperience = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalExperienceAction = null;
    this.changeStatusModalNotes = '';
    this.form.reset();
  }

  changeStatusModalActiveAction(action: string) {
    this.changeStatusModalExperienceAction = action;
    this.form.reset();
  }

  handleChangeStatusOk() {
    this.changeStatusModalLoading = true;

    const obj: any = {
      event: this.changeStatusModalExperienceAction.toUpperCase(),
      notes: this.changeStatusModalNotes,
    };

    if (obj.event === 'APPROVE' && this.experience.number_simultaneous_candidates <= this.experience.users_colaboration.length) {
      this.uiService
        .showConfirm('Máximo de inscrições atingido', 'Foi atingido o máximo de inscrições definidas. Tem a certeza que pretende aprovar a inscrição?', 'Aprovar inscrição')
        .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
        .subscribe((confirm) => {
          if (confirm) {
            this.changeCandidateStatus(obj);
          }
        });
      return;
    } else {
      this.changeCandidateStatus(obj);
    }
  }

  changeCandidateStatus(obj: any) {
    if (this.changeStatusRequirementNumber != null) {
      obj.experience = { requirement_number: this.changeStatusRequirementNumber };
    }

    /*this.experienceUserInterestsService
      .status(this.changeStatusModalExperience.id, obj)
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe((result) => {
        this.changeStatusModalExperience.status = result.data[0].status;
        this.handleChangeStatusCancel();
      });*/

      if(this.changeStatusModalExperience.status !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'DISPATCH' && this.changeStatusModalExperienceAction.toUpperCase() !== 'INTERVIEW'){
        this.experienceUserInterestsService
          .status(this.changeStatusModalExperience.id, obj)
          .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
          .subscribe((result) => {
            this.changeStatusModalExperience.status = result.data[0].status;
            this.handleChangeStatusCancel();
            this.itemChange.emit(true);
          });
      }else if(this.changeStatusModalExperienceAction.toUpperCase() === 'DISPATCH'){
        if(this.form.valid){
          this.experienceUserInterestsService.sendDispatch(this.changeStatusModalExperience.id, {
            event: this.changeStatusModalExperienceAction.toUpperCase(),
            decision: this.form.get('decision').value,
            notes: this.changeStatusModalNotes,
          })
            .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
            .subscribe(() => {
              this.handleChangeStatusCancel();
              this.itemChange.emit(true);
            });
        }else{
          this.updateAndValidityForm(this.form);
          this.changeStatusModalLoading = false;
        }
      }else if(this.changeStatusModalExperience.status === 'DISPATCH'){
        this.experienceUserInterestsService
        .sendDispatch(this.changeStatusModalExperience.id, { decision_dispatch: this.changeStatusModalExperienceAction.toUpperCase() })
        .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
        .subscribe(() => {
          this.itemChange.emit(true);
        });
      }else{
        this.changeStatusModalLoading = false;
      }
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  getLanguageTitle(language_id: number) {
    const language = this.languages.find(l => l.id === language_id);
    return language ? language.name : 'Sem informação';
  }

  dispatchChangeStatues(application: ExperienceModel, acceptDecision: boolean) {
    this.volunteeringExperiencesService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.itemChange.emit(true);
      });
  }

  getDispatchActions = () => {
    return Object.keys(ExperienceUserInterestData.statusMachine.DISPATCH).map<{
      key: ExperienceUserInterestData;
      label: string;
    }>((key) => ({
      key: ExperienceUserInterestData.eventToDispatchDecisionMapper[key],
      label: ExperienceUserInterestData.statusMachine.DISPATCH[key].label,
    }));
  }
}
