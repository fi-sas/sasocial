import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { ExperienceModel } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { ApproveRejectModel } from "@fi-sas/backoffice/modules/volunteering/models/approveReject";
import { Observable } from "rxjs";
import { first } from 'rxjs/operators';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExperiencesService extends Repository<ExperienceModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.EXPERIENCES';
    this.entity_url = 'VOLUNTEERING.EXPERIENCES_ID';
  }

  status(id: number, experience_status: ApproveRejectModel): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCES_ID_STATUS', {id}), experience_status);
  }

  students(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string, params?: {}): Observable<Resource<UserModel>> {
    return this.resourceService.list<UserModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCES_USER_COLLABORATION',
      { ...this.persistentUrlParams}),
      { params: this.getQuery(pageIndex,pageSize,sortKey,sortValue,params) }
    ).pipe(first());
  }

  targetUsers(id: number, course_year: number, course_id: number, school_id: number): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();

    if (course_year) {
      params = params.set('course_year', String(course_year));
    }

    if (course_id) {
      params = params.set('course_id', String(course_id));
    }

    if (school_id) {
      params = params.set('school_id', String(school_id));
    }

    return this.resourceService.list<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCES_TARGET_USERS', { id }), { params });
  }

  sendNotifications(id: number, user_ids: number[]): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCES_SEND_NOTIFICATIONS', { id }), { user_ids });
  }
}
