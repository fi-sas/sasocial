export enum OwnerStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected',
}

export class ExternalUserApplicationsModel {
  id?: number;
  full_name: string;
  identification: string;
  email: string;
  password?: string;
  pin?: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  issues_receipt: boolean;
  description: string;
  applicant_consent: boolean;
  phone: string;
  status?: string;
  external_user_id?: number;
  updated_at?: string;
  created_at?: string;
  history?: HistoryModel[] = [];
  external_user: ExternalUserModel;
}

class HistoryModel {
  id: number;
  external_user_application_id: number;
  status: string;
  user_id: number;
  notes: string;
  updated_at: string;
  created_at: string;
}

class ExternalUserModel {
  id: number;
  user_id: number;
  application?: ExternalUserApplicationsModel;
  updated_at: string;
  created_at: string;
}

export class ExternalUserRejectAndApprove {
  notes: string;
}
