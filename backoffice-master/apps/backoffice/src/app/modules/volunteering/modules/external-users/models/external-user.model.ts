import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';

export enum ExternalUserStatus {
  Pending = 'Pending',
  Approved = 'Approved',
  Rejected = 'Rejected'
}

export class ExternalUserModel {
  id?: number;
  user_id: number;
  status: string;
  description: string;
  function_description: string;
  active: boolean;
  updated_at?: string;
  created_at?: string;
  user?: UserModel;
  file_id: number;
  file?: FileModel;
  name: string;
  email: string;
  gender: string;
  address: string;
  city: string;
  country: string;
  phone: string;
  tin: string;
  postal_code: string;
}

export const ExternalUserTagStatus = {
  CREATED: { color: '#f8ca00', label: 'Não validado' },
  VALIDATED: { color: '#1ba974', label: 'Validado' },
};

export const Gender = {
  M: { value: 'M', label: 'Masculino' },
  F: { value: 'F', label: 'Feminino' },
  I: { value: 'U', label: 'Outro' },
}

