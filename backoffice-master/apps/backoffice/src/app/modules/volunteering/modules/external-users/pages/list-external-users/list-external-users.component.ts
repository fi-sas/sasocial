import { ExternalUserModel, ExternalUserTagStatus } from './../../models/external-user.model';
import { Component, OnInit } from '@angular/core';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import {
  UiService,
  MessageType,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ExternalUserService } from '../../services/external-user.service';
import { first, finalize } from 'rxjs/operators';
import { TagComponent } from '@fi-sas/backoffice/shared/components/tag/tag.component';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';

@Component({
  selector: 'fi-sas-list-external-users',
  templateUrl: './list-external-users.component.html',
  styleUrls: ['./list-external-users.component.less'],
})
export class ListExternalUsersComponent extends TableHelper implements OnInit {

  ExternalUserTagStatus = ExternalUserTagStatus;

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalExternalUser: ExternalUserModel = null;
  changeStatusModalActions = {};
  changeStatusModalApplicationAction = null;
  changeStatusModalNotes = null;
  changeStatusModalLoading = false;

  constructor(
    uiService: UiService,
    router: Router,
    activatedRoute: ActivatedRoute,
    public externalUserService: ExternalUserService,
    private authService: AuthService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.columns.push(
      {
        key: 'name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: 'email',
        label: 'Email',
        sortable: false,
      },
      {
        key: 'tin',
        label: 'NIF',
        sortable: false,
      },
      {
        key: 'status',
        label: 'Estado',
        tag: ExternalUserTagStatus,
        sortable: true,
      },
      {
        key: 'active',
        label: 'Ativo',
        tag: TagComponent.YesNoTag,
        sortable: true,
      }
    );

    this.persistentFilters['withRelated'] = 'user,file';
    this.initTableData(this.externalUserService);
  }

  changeActive(externalUserId: number, active: boolean) {
    if(!this.authService.hasPermission('volunteering:external_entities:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );

      return;
    }

    this.uiService
      .showConfirm(
        active ? 'Desativar' : 'Ativar',
        active
          ? 'Pretende desativar este utilizador externo?'
          : 'Pretende ativar este utilizador externo?',
        active ? 'Desativar' : 'Ativar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.externalUserService
            .changeActive(externalUserId, active)
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                `Estado da conta alterado para  ${
                  active ? 'inativo' : 'ativo'
                }`
              );
                this.searchData();
            });
        }
      });
  }

  validate(externalUserId: number) {
    if(!this.authService.hasPermission('volunteering:external_entities:update')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );

      return;
    }

      this.uiService
      .showConfirm(
        'Validar utilizador externo',
        'Pretende validar este utilizador externo?',
        'Validar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.externalUserService
            .validate(externalUserId)
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Utilizador externo validado com sucesso'
              );
                this.searchData();
            });
        }
      });
  }

  deleteExternalUser(id: number) {
    if(!this.authService.hasPermission('volunteering:external_entities:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );

      return;
    }

    this.uiService
      .showConfirm(
        'Eliminar utilizador externo',
        'Pretende eliminar este utilizador externo ?',
        'Eliminar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.externalUserService
            .delete(id)
            .pipe(first())
            .subscribe(() => {
              this.uiService.showMessage(
                MessageType.success,
                'Utilizador externo eliminado com sucesso'
              );
              this.searchData();
            });
        }
      });
  }
}
