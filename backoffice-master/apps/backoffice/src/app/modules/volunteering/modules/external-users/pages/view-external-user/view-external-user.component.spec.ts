import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExternalUserComponent } from './view-external-user.component;

describe('ViewExternalUserComponent', () => {
  let component: ViewExternalUserComponent;
  let fixture: ComponentFixture<ViewExternalUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewExternalUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExternalUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
