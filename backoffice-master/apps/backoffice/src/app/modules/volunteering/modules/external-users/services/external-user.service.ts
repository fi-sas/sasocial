import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ExternalUserModel } from '../models/external-user.model';

@Injectable({
  providedIn: 'root'
})
export class ExternalUserService extends Repository<ExternalUserModel> {

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.EXTERNAL_USERS';
    this.entity_url = 'VOLUNTEERING.EXTERNAL_USERS_ID';
  }

  changeActive(id: number, status: boolean) {
    if(status) {
      return this.deactivate(id)
    } else {
      return this.activate(id)
    }
  }

  deactivate(id: number): Observable<any> {
    return this.resourceService
      .create(this.urlService.get('VOLUNTEERING.EXTERNAL_USERS_DEACTIVATE', { id }), {})
      .pipe(first());
  }

  activate(id: number) {
    return this.resourceService
      .create(this.urlService.get('VOLUNTEERING.EXTERNAL_USERS_ACTIVATE', { id }), {})
      .pipe(first());
  }

  validate(id: number) {
    return this.resourceService
      .create(this.urlService.get('VOLUNTEERING.EXTERNAL_USERS_VALIDATE', { id }), {})
      .pipe(first());
  }
}
