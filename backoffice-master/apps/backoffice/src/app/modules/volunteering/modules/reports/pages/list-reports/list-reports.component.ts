import { Component, Input, OnInit } from '@angular/core';
import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { first, finalize } from "rxjs/operators";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ReportsService } from '@fi-sas/backoffice/modules/volunteering/modules/reports/services/reports.service';

@Component({
  selector: 'fi-sas-list-reports',
  templateUrl: './list-reports.component.html',
  styleUrls: ['./list-reports.component.less']
})
export class ListReportsComponent implements OnInit {

   loading= false;

    //FILTERS
    academic_years: AcademicYearModel[] = [];
    load_academic_years = false;  

    reportForm = new FormGroup({
      academic_year: new FormControl('', [Validators.required])
    });

  constructor(
    private academicYearsService: AcademicYearsService,
    private reportsService: ReportsService,
    private uiService: UiService,
  ) {}

  ngOnInit() {
    this.loadAcademicYears();
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(0, -1)
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  submitForm(event: any, value: any) {
    if(this.reportForm.valid) {
      this.loading = true;
      this.reportsService.createReport(value.academic_year).pipe(first(), finalize(() => this.loading = false)).subscribe(result => {
        this.uiService.showMessage(MessageType.success, 'Relatório gerado com sucesso');
      });
    }
  }
}
