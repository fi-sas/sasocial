import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListReportsComponent } from './pages/list-reports/list-reports.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ReportsRoutingModule } from './reports-routing.module';

@NgModule({
  declarations: [
    ListReportsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingModule
  ],
  providers: [
  ]
})
export class ReportsModule { }
