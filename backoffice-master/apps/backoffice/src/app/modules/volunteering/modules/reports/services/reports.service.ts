import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ReportModel } from "@fi-sas/backoffice/modules/volunteering/modules/reports/models/report.model";
import { Observable } from "rxjs";
import { Repository } from "@fi-sas/backoffice/shared/repository/repository.class";

@Injectable({
  providedIn: 'root'
})
export class ReportsService extends Repository<ReportModel>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
  }

  createReport(academic_year: string): Observable<Resource<ReportModel>> {
    return this.resourceService.create<ReportModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS_LIST_REPORTS'), {
      academic_year: academic_year
    });
  }

}
