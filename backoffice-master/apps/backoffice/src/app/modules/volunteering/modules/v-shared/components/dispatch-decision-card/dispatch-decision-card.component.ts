import { Component, EventEmitter, Input, Output } from '@angular/core';

interface IAction {
  fn: () => void,
  label: string,
  permission?: string;
  title: string;
  btnType: 'danger' | 'primary';
  cancelText: string;
  okText: string;
}

@Component({
  selector: 'fi-sas-dispatch-decision-card',
  templateUrl: './dispatch-decision-card.component.html',
  styleUrls: ['./dispatch-decision-card.component.less']
})
export class DispatchDecisionCardComponent {
  private _decision: string;
  @Input() set decision(decision: string) {
    this._decision = decision;
  }
  get decision() {
    return this._decision || 'Sem Informação';
  }

  @Output() onReject = new EventEmitter<boolean>();
  @Output() onApprove = new EventEmitter<boolean>();

  readonly actions: IAction[] = [
    {
      fn: () => this.reject(),
      label: 'Rejeitar',
      title: 'Pretende rejeitar esta decisão?',
      btnType: 'danger',
      cancelText: 'Cancelar',
      okText: 'Rejeitar',
    },
    {
      fn: () => this.approve(),
      label: 'Aceitar',
      title: 'Pretende aceitar esta decisão?',
      btnType: 'primary',
      cancelText: 'Cancelar',
      okText: 'Aceitar',
    }
  ];

  constructor() { }

  reject() {
    this.onReject.emit(true);
  }

  approve() {
    this.onApprove.emit(true);
  }
}
