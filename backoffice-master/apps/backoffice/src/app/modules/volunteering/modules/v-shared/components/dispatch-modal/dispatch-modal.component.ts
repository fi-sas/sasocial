import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs';

import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { SendDispatchData } from '@fi-sas/backoffice/modules/volunteering/models/dispatch.model';


@Component({
  selector: 'fi-sas-dispatch-modal',
  templateUrl: './dispatch-modal.component.html',
  styleUrls: ['./dispatch-modal.component.less'],
})
export class DispatchModalComponent {
  @Input() serviceFn: (data: SendDispatchData) => Observable<Resource<any>>;
  @Input() actions: { key: string; label: string; }[];

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
    notes: new FormControl(null),
  });

  constructor() { }

  onSubmit() {
    if (!this.isFormValid()) {
      return Promise.reject();
    }

    return this.serviceFn({ event: 'DISPATCH', decision: this.form.get('decision').value, notes: this.form.get('notes').value }).pipe(first()).toPromise();
  }

  private isFormValid() {
    for (const controlName in this.form.controls) {
      if (this.form.controls[controlName]) {
        this.form.controls[controlName].markAsDirty();
        this.form.controls[controlName].updateValueAndValidity();
      }
    }

    return this.form.valid;
  }
}
