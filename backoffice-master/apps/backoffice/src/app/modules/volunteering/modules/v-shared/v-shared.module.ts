import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DispatchDecisionCardComponent, DispatchModalComponent } from './components';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [DispatchDecisionCardComponent, DispatchModalComponent],
  imports: [CommonModule, SharedModule],
  exports: [DispatchDecisionCardComponent, DispatchModalComponent],
  entryComponents: [DispatchModalComponent],
})
export class VSharedModule { }
