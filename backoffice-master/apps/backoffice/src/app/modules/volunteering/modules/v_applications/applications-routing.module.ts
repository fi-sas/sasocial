import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListApplicationsComponent } from './pages/list-applications/list-applications.component';
import { PanelStatsComponent } from './pages/panel-stats/panel-stats.component';

const routes: Routes = [
  { path: '', redirectTo: 'stats', pathMatch: 'full' },
  {
    path: 'stats',
    component: PanelStatsComponent,
    data: { breadcrumb: 'Painel', title: 'Painel', scope: 'volunteering:applications:read' },
    resolve: {}
  },
  {
    path: 'app-all',
    component: ListApplicationsComponent,
    data: { breadcrumb: 'Todas', title: 'Candidaturas', scope: 'volunteering:applications:read', applicationsStatus: '', allApplications: true },
    resolve: {}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
