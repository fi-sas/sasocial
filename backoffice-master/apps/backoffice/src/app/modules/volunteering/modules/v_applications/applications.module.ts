import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationsService } from './services/applications.service';
import { ListApplicationsComponent } from './pages/list-applications/list-applications.component';
import { PanelStatsComponent } from './pages/panel-stats/panel-stats.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';
import { ViewApplicationComponent } from './pages/view-application/view-application.component';
import { ViewApplicationModalComponent } from './pages/view-application-modal/view-application-modal.component';
import { VSharedModule } from '../v-shared/v-shared.module';

@NgModule({
  declarations: [
    ListApplicationsComponent,
    PanelStatsComponent,
    ViewApplicationComponent,
    ViewApplicationModalComponent,
  ],
  imports: [ApplicationsRoutingModule, CommonModule, SharedModule, VSharedModule],
  entryComponents: [ViewApplicationModalComponent],
  providers: [ApplicationsService],
})
export class ApplicationsModule { }
