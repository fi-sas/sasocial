import { AreaInterestModel } from './area-interest';

export class ApplicationAreaInteresModel {
    id: number;
    application_id: number;
    area_interest_id: number
    created_at: string;
    updated_at: string
    area_interest: AreaInterestModel;
}