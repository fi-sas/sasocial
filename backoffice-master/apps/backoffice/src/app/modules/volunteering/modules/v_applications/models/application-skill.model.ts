import { SkillModel } from './skill.model';

export class ApplicationSkillModel {
    id: number;
    application_id: number;
    other_skill: string;
    skill_id: number
    created_at: string;
    updated_at: string
    skill: SkillModel;
}