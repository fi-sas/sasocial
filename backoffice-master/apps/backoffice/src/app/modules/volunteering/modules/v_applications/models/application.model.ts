import { HistoryModel } from "@fi-sas/backoffice/modules/volunteering/models/history.model";
import { ExperienceModel } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { MonthlyReportsModel } from "@fi-sas/backoffice/modules/volunteering/models/monthly-reports.model";
import { AttendancesModel } from "@fi-sas/backoffice/modules/volunteering/models/attendances.model";
import { OrganicUnitsModel } from "@fi-sas/backoffice/modules/infrastructure/models/organic-units.model";
import { UserModel } from "@fi-sas/backoffice/modules/users/modules/users_users/models/user.model";
import { FileModel } from "@fi-sas/backoffice/modules/medias/models/file.model";
import { CourseModel } from "@fi-sas/backoffice/modules/configurations/models/course.model";
import { SchoolModel } from "@fi-sas/backoffice/modules/configurations/models/school.model";
import { ApplicationHistoricModel } from "./application-historic.model";
import { CollaborationHistoricModel } from "./collaboration-historic.model";
import { ApplicationSkillModel } from "./application-skill.model";
import { ApplicationAreaInteresModel } from "./application-area-interest.model";

export enum ApplicationStatus {
  ACCEPTED = 'ACCEPTED',
  ANALYSED = 'ANALYSED',
  CANCELLED = 'CANCELLED',
  DECLINED = 'DECLINED',
  DISPATCH = 'DISPATCH',
  EXPIRED = 'EXPIRED',
  SUBMITTED = 'SUBMITTED',
}

export enum ApplicationStatusEvent {
  ACCEPT = 'ACCEPT',
  ANALYSE = 'ANALYSE',
  CANCEL = 'CANCEL',
  DECLINE = 'DECLINE',
  DISPATCH = 'DISPATCH',
  EXPIRE = 'EXPIRE',
}

export enum ApplicationDispatchDecision {
  ACCEPTED = 'ACCEPTED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
}

export const ApplicationTagResult = {
  ACCEPTED: { color: '#9DBBAE', label: 'Aprovado' },
  ACKNOWLEDGED: { color: '#076743', label: 'Aceite' },
  ANALYSED: { color: '#7768AE', label: 'Em Análise' },
  CANCELLED: { color: '#A4A4A4', label: 'Cancelado' },
  DECLINED: { color: '#D0021B', label: 'Rejeitado' },
  DISPATCH: { color: 'orange', label: 'Despacho' },
  EXPIRED: { color: '#ff8000', label: 'Expirado' },
  INTERVIEWED: { color: '#88ccf1', label: 'Entrevista' },
  PUBLISHED: { color: '#9dbbae', label: 'Em Colaboração' },
  SUBMITTED: { color: '#4d9de0', label: 'Submetido' },
}

export class ApplicationModel {
  id?: number;
  academic_year: string;
  mentor_or_mentee: string;
  course_year: string;
  experience_id: number;
  name: string;
  student_number: string;
  course_id: number;
  course?: CourseModel;
  birthdate: string;
  nationality: string;
  tin: string;
  identification_number: string;
  iban: string;
  address: string;
  code_postal: string;
  city: string;
  email: string;
  phone_number: string;
  has_scholarship: boolean;
  has_applied_scholarship: boolean;
  scholarship_monthly_value: number;
  has_emergency_fund: boolean;
  has_profissional_experience: boolean;
  type_of_company: string;
  job_at_company: string;
  job_description: string;
  organic_unit_id: number;
  school?: SchoolModel;
  organic_units?: number[] | OrganicUnitsModel[];
  has_collaborated_last_year: boolean;
  previous_activity_description: string;
  preferred_schedule: PreferredScheduleModel[];
  language_skills: boolean;
  which_languages: string;
  cv_file_id: number;
  cv_file?: FileModel;
  has_holydays_availability: boolean;
  observations: string;
  user_id?: number;
  user?: UserModel;
  country: string;
  status?: string;
  updated_at?: string;
  created_at?: string;
  experience?: ExperienceModel;
  history?: HistoryModel[];
  monthly_reports?: MonthlyReportsModel[];
  attendance?: AttendancesModel[];
  historic_applications?: ApplicationHistoricModel[];
  historic_colaborations?: CollaborationHistoricModel[];
  skills: ApplicationSkillModel[];
  location: string;
  phone: string;
  mobile_phone: string;
  area_interests?: ApplicationAreaInteresModel[];
  decision?: string;
}

export class PreferredScheduleModel {
  id: never;
  day_week: string;
  day_period: string;
}

export class ApplicationData {

  static subject = [
    { value: 'BC', label: 'Bolsa Colaboradores', active: true },
    { value: 'V', label: 'Voluntariado', active: true },
    { value: 'M', label: 'Mentoria', active: true }
  ];

  static mentor_or_mentee = [
    { value: 'MENTOR', label: 'Mentor', active: true },
    { value: 'MENTEE', label: 'Mentorado', active: true }
  ];

  static status: {
    value: keyof typeof ApplicationStatus;
    label: string;
    active: boolean;
    color: string;
    order: number;
  }[] = [
    { value: 'ACCEPTED', label: 'Aprovado', active: true, color: '#9DBBAE', order: 4 },
    { value: 'ANALYSED', label: 'Em Análise', active: true, color: '#7768AE', order: 2 },
    { value: 'CANCELLED', label: 'Cancelado', active: true, color: '#A4A4A4', order: -1 },
    { value: 'DECLINED', label: 'Rejeitado', active: true, color: '#D0021B', order: -1 },
    { value: 'DISPATCH', label: 'Despacho', active: true, color: ApplicationTagResult.DISPATCH.color, order: -1 },
    { value: 'EXPIRED', label: 'Expirado', active: true, color: '#ff8000', order: -1 },
    { value: 'SUBMITTED', label: 'Submetido', active: true, color: '#4d9de0', order: 0 },
  ];

  static statusMachine: Record<ApplicationStatus, Partial<Record<ApplicationStatusEvent, { label: string; color: string; }>>> = {
    SUBMITTED: {
      ANALYSE: { label: 'Analisar', color: ApplicationTagResult.ANALYSED.color },
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
    },
    ANALYSED: {
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
      DISPATCH: { label: 'Despacho', color: ApplicationTagResult.DISPATCH.color },
    },
    DISPATCH: {
      ACCEPT: { label: 'Aceitar', color: ApplicationTagResult.ACCEPTED.color },
      DECLINE: { label: 'Rejeitar', color: ApplicationTagResult.DECLINED.color },
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
    },
    DECLINED: {
      ANALYSE: { label: 'Analisar', color: ApplicationTagResult.ANALYSED.color },
    },
    ACCEPTED: {
      CANCEL: { label: 'Cancelar', color: ApplicationTagResult.CANCELLED.color },
      EXPIRE: { label: 'Expirar', color: ApplicationTagResult.EXPIRED.color },
    },
    EXPIRED: {},
    CANCELLED: {},
  };

  static eventToDispatchDecisionMapper: Partial<Record<ApplicationStatusEvent, ApplicationDispatchDecision>> = {
    ACCEPT: ApplicationDispatchDecision.ACCEPTED,
    CANCEL: ApplicationDispatchDecision.CANCELLED,
    DECLINE: ApplicationDispatchDecision.REJECTED,
  }

  static decisionToStateMapper: Record<ApplicationDispatchDecision, ApplicationStatus> = {
    ACCEPTED: ApplicationStatus.ACCEPTED,
    CANCELLED: ApplicationStatus.CANCELLED,
    REJECTED: ApplicationStatus.DECLINED,
  }
}

