export class AreaInterestModel {
    id: number;
    created_at: string;
    updated_at: string
    translations?: AreaInterestTranslationModel[];
}

export class AreaInterestTranslationModel {
    id: number;
    skill_id: number;
    language_id: number;
    description: string;
    created_at: string;
    updated_at: string
}