import { TranslationModel } from "../../experiences/models/experience.model";

export class CollaborationHistoricModel {
    academic_year: string;
    experience_translations: TranslationModel[];
    user_manifest_id: number;
}