export class SkillModel {
    id: number;
    created_at: string;
    updated_at: string
    translations?: SkillTranslationModel[];
}

export class SkillTranslationModel {
    id: number;
    skill_id: number;
    language_id: number;
    description: string;
    created_at: string;
    updated_at: string
}