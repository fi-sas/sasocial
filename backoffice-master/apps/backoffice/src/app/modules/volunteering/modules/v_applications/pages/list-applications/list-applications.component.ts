import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { first, finalize } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { ApplicationsService } from '../../services/applications.service';
import { ApplicationTagResult, ApplicationModel, ApplicationData, ApplicationStatus } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { CourseModel } from '@fi-sas/backoffice/modules/configurations/models/course.model';
import { CoursesService } from '@fi-sas/backoffice/modules/configurations/services/courses.service';
import { ExperienceModel } from '../../../experiences/models/experience.model';
import { ExperiencesService } from '../../../experiences/services/experiences.service';
import { OrganicUnitsModel } from '@fi-sas/backoffice/modules/infrastructure/models/organic-units.model';
import { OrganicUnitsService } from '@fi-sas/backoffice/modules/infrastructure/services/organic-units.service';
import { TableHelper } from '@fi-sas/backoffice/shared/helpers/table.helper';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { VolunteeringService } from './../../../../services/volunteering.service';
import { DispatchModalComponent } from '../../../v-shared/components';
import { SendDispatchData } from '@fi-sas/backoffice/modules/volunteering/models/dispatch.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'fi-sas-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less']
})
export class ListApplicationsComponent extends TableHelper implements OnInit {
  ApplicationStatus = ApplicationStatus;

  status = ApplicationData.status;
  statusMachine = ApplicationData.statusMachine;
  allApplications = false;
  currentAcademicYear: AcademicYearModel[];

  //FILTERS
  academic_years: AcademicYearModel[] = [];
  load_academic_years = false;
  courses: CourseModel[] = [];
  load_courses = false;
  schools: OrganicUnitsModel[] = [];
  load_schools = false;
  applicationStatuses = [];
  yes_no_options = [];

  // MODAL CHANGE STATUS
  isChangeStatusModalVisible = false;
  changeStatusModalApplication: ApplicationModel = null;
  changeStatusModalActions = {};
  changeStatusModalApplicationAction = null;
  changeStatusModalNotes = '';
  changeStatusModalLoading = false;
  published_experiences: ExperienceModel[] = [];
  published_experiences_loaded = false;
  published_experiences_loading = false;
  changeStatusModalSelectedExperience = null;

  form = new FormGroup({
    decision: new FormControl(null, Validators.required),
  });
  
  constructor(
    activatedRoute: ActivatedRoute,
    router: Router,
    uiService: UiService,
    public experiencesService: ExperiencesService,
    public applicationService: ApplicationsService,
    private volunteeringService: VolunteeringService,
    private coursesService: CoursesService,
    private academicYearsService: AcademicYearsService,
    private organicUnitsService: OrganicUnitsService,
    private authService: AuthService,
    private modalService: NzModalService,
  ) {
    super(uiService, router, activatedRoute);
  }

  ngOnInit() {
    this.loadSchools();
    this.loadAcademicYears();
    this.loadStatuses();
    this.loadYesNoOptions();
    this.persistentFilters['withRelated'] = 'course';

    let status: string = this.activatedRoute.snapshot.queryParamMap.get('status');
    if (status) {
      this.persistentFilters['status'] = status;
    }
    let academicYear: string = this.activatedRoute.snapshot.queryParamMap.get('academicYear');
    if (academicYear) {
      this.filters.academic_year = academicYear;
    } else {
      this.loadCurrentAcademicYear();
    }

    this.activatedRoute.data.subscribe((data) => {
      this.status = data.applicationsStatus ? data.applicationsStatus : '';
      this.allApplications = data.allApplications ? data.allApplications : false;

      if (this.status) {
        this.persistentFilters['status'] = this.status;
        this.persistentFilters['status'] = this.persistentFilters['status'].toUpperCase();
      }
    });

    this.columns.push(
      {
        key: 'student_number',
        label: 'Nº Estudante',
        sortable: false,
      },
      {
        key: 'name',
        label: 'Nome',
        sortable: false,
      },
      {
        key: 'email',
        label: 'Email',
        sortable: false,
      },
      {
        key: 'course.name',
        label: 'Curso',
        sortable: false,
      },
      {
        key: 'academic_year',
        label: 'Ano Académico',
        sortable: true,
      },
      {
        key: 'status',
        label: 'Estado',
        sortable: true,
        tag: ApplicationTagResult,
        filters: this.applicationStatuses.map(status => {
          return {
            text: status.label,
            value: status.value,
          }
        }),
        showFilter: true,
        filterMultiple: false,
      }
    );

    this.initTableData(this.applicationService);
  }

  loadCurrentAcademicYear() {
    this.academicYearsService
      .getCurrentAcademicYear()
      .pipe(first())
      .subscribe((result) => {
        this.currentAcademicYear = result.data;
        if (this.currentAcademicYear.length !== 0) {
          this.filters.academic_year = this.currentAcademicYear[0].academic_year;
          this.searchData(true);
        }
      });
  }

  loadStatuses() {
    this.applicationStatuses = Object.keys(this.status).map((k) => {
      return {
        key: k,
        ...this.status[k],
      };
    });
  }

  loadYesNoOptions() {
    this.yes_no_options = [
      {
        label: 'Sim',
        value: true
      },
      {
        label: 'Não',
        value: false
      }
    ]
  }

  getStatusArray() {
    return Object.keys(this.status).map(k => {
      return {
        key: k,
        ...this.status[k]
      }
    })
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(1, -1, 'start_date', 'descend')
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  loadSchools() {
    this.load_schools = true;

    this.organicUnitsService
      .list(1, -1, 'name', 'ascend', { withRelated:false, is_school: true })
      .pipe(
        first(),
        finalize(() => (this.load_schools = false))
      )
      .subscribe((results) => {
        this.schools = results.data;
      });
  }

  loadCourses(schoolId) {
    if (schoolId === null) {
      this.courses = null;

      return false;
    }

    this.load_courses = true;
    this.coursesService
      .list(1, -1, 'name', 'ascend', { organic_unit_id: schoolId })
      .pipe(
        first(),
        finalize(() => (this.load_courses = false))
      )
      .subscribe((result) => {
        this.courses = result.data;
      });
  }

  listComplete() {
    this.filters.created_at = null;
    this.filters.academic_year = null;
    this.filters.school_id = null;
    this.filters.course_id = null;
    this.filters.student_number = null;
    this.filters.has_holydays_availability = null;
    this.filters.has_collaborated_last_year = null;
    this.filters.has_profissional_experience = null;
    this.searchData(true);
  }

  deleteApplication(id: number) {
    if (!this.authService.hasPermission('volunteering:applications:delete')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    this.uiService.showConfirm('Eliminar', 'Pretende eliminar esta candidatura?', 'Eliminar').pipe(first()).subscribe(confirm => {
      if (confirm) {
        this.applicationService.delete(id).pipe(first()).subscribe(result => {
          this.uiService.showMessage(MessageType.success, 'Candidatura eliminada com sucesso');
          this.searchData();
        });
      }
    });
  }

  changeStateModal(application: ApplicationModel, disabled: boolean) {
    if (!this.authService.hasPermission('volunteering:applications:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    if(!Object.keys(ApplicationData.statusMachine[application.status]).length){
      return;
    }

    if (!disabled) {
      this.isChangeStatusModalVisible = true;
      this.changeStatusModalApplication = application;
      this.changeStatusModalActions =
        ApplicationData.statusMachine[application.status];

      if (application.experience_id) {
        this.changeStatusModalSelectedExperience = application.experience_id;
      }
    }
  }

  handleChangeStatusOk() {
    let statusObj: any = {
      event: this.changeStatusModalApplicationAction.toUpperCase(),
      notes: this.changeStatusModalNotes
    };

    if (
      this.changeStatusModalApplicationAction === 'publish' &&
      !this.changeStatusModalSelectedExperience
    ) {
      this.uiService.showMessage(MessageType.warning, 'Selecione uma ação de voluntariado');
      return;
    } else {
      statusObj = {
        ...statusObj,
        application: {
          experience_id: this.changeStatusModalSelectedExperience
        }
      };
    }

    if(this.changeStatusModalApplication.status !== 'DISPATCH' && this.changeStatusModalApplicationAction.toUpperCase() !== 'DISPATCH'){
      this.changeStatusModalLoading = true;
      this.applicationService
        .status(this.changeStatusModalApplication.id, statusObj)
        .pipe(
          first(),
          finalize(() => this.changeStatusModalLoading = false)
        )
        .subscribe(result => {
          if (this.allApplications) {
            this.changeStatusModalApplication.status = result.data[0].status;
            const appIndex = this.data.findIndex(
              (a) => a.id === this.changeStatusModalApplication.id
            );
            if (appIndex !== undefined) {
              this.data[appIndex].status = result.data[0].status;
              this.data = [...this.data];
            }
          } else {
            this.searchData();
          }
          this.volunteeringService.updateApplicationsStats();
          this.handleChangeStatusCancel();
        });
    }else if(this.changeStatusModalApplicationAction.toUpperCase() === 'DISPATCH'){
      if(this.form.valid){
        this.applicationService.sendDispatch(this.changeStatusModalApplication.id, {
          event: this.changeStatusModalApplicationAction.toUpperCase(),
          decision: this.form.get('decision').value,
          notes: this.changeStatusModalNotes,
        })
          .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
          .subscribe(() => {
            this.handleChangeStatusCancel();
            this.searchData();
          });
      }else{
        this.updateAndValidityForm(this.form);
        this.changeStatusModalLoading = false;
      }
    }else if(this.changeStatusModalApplication.status === 'DISPATCH'){
      this.applicationService
      .sendDispatch(this.changeStatusModalApplication.id, { decision_dispatch: this.changeStatusModalApplicationAction.toUpperCase() })
      .pipe(first(), finalize(() => this.changeStatusModalLoading = false))
      .subscribe(() => {
        this.handleChangeStatusCancel();
        this.searchData();
      });
    }else{
      this.changeStatusModalLoading = false;
    }
  }

  updateAndValidityForm(form){
    for (const i in form.controls) {
      if (form.controls[i]) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
      }
    }
  }

  handleChangeStatusCancel() {
    this.isChangeStatusModalVisible = false;
    this.changeStatusModalApplication = null;
    this.changeStatusModalActions = {};
    this.changeStatusModalApplicationAction = null;
    this.changeStatusModalSelectedExperience = null;
    this.changeStatusModalNotes = '';
    this.form.reset();
  }

  changeStatusModalActiveAction(action: string) {
    /*if (action === 'accept') {
      this.loadPublishedExperiences();
    }*/

    this.changeStatusModalApplicationAction = action;
    this.form.reset();
  }

  loadPublishedExperiences() {
    if (!this.published_experiences_loaded) {
      this.published_experiences_loading = true;
      this.experiencesService.list(1, -1, null, null, {
        status: 'published',
        withRelated: 'organic_unit'
      }).pipe(
        first(),
        finalize(() => this.published_experiences_loading = false)
      ).subscribe(results => {
        this.published_experiences = results.data;
        this.published_experiences_loaded = true;
      });
    }
  }

  restoreApplication(application: ApplicationModel) {
    if (!this.authService.hasPermission('volunteering:applications:status')) {
      this.uiService.showMessage(
        MessageType.warning,
        'O utilizador não tem acesso ao serviço solicitado'
      );
      return;
    }

    this.loading = true;
    this.uiService
      .showConfirm(
        'Recuperar candidatura',
        'Pretende recuperar a candidatura do estudante ' + application.name + '?',
        'Recuperar'
      )
      .pipe(first())
      .subscribe((confirm) => {
        if (confirm) {
          this.applicationService
            .changeToLastStatus(application.id)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe((result) => {
              this.uiService.showMessage(
                MessageType.success,
                'Candidatura recuperada.'
              );

              this.searchData();
              this.volunteeringService.updateApplicationsStats();
            });
        }
        this.loading = false
      });
  }

  /*dispatchModal(application: ApplicationModel) {
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: 'Estado',
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.applicationService.sendDispatch(application.id, data),
        actions: Object.keys(ApplicationData.statusMachine.DISPATCH).map<{ key: ApplicationStatus; label: string }>(
          (key) => ({
            key: ApplicationData.eventToDispatchDecisionMapper[key],
            label: ApplicationData.statusMachine.DISPATCH[key].label,
          })
        ),
      },
      nzFooter: [
        { label: 'Sair', onClick: (_) => modalRef.close() },
        {
          label: 'Gravar',
          type: 'primary',
          onClick: (componentInstance) => {
              componentInstance.onSubmit().then(() => {
                this.searchData();
                modalRef.close();
              });

          },
        },
      ],
    });
  }*/

  getDispatchActions = () => {
    return Object.keys(ApplicationData.statusMachine.DISPATCH).map<{ key: ApplicationStatus; label: string }>(
      (key) => ({
        key: ApplicationData.eventToDispatchDecisionMapper[key],
        label: ApplicationData.statusMachine.DISPATCH[key].label,
      })
    );
  }

  dispatchChangeStatues(application: ApplicationModel, acceptDecision: boolean) {
    this.applicationService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.searchData();
      });
  }
}
