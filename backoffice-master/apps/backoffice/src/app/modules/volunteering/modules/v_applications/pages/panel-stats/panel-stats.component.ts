import { Component, OnInit } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { ApplicationsService } from '../../services/applications.service';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { Router } from '@angular/router';
import { ApplicationStatsModel } from '../../models/application-stats';
import { UserInterestStatsModel } from '@fi-sas/backoffice/modules/volunteering/models/user-interest-stats.model';
import { ExperienceStatsModel } from '@fi-sas/backoffice/modules/volunteering/models/experience-stats.model';

@Component({
  selector: 'fi-sas-panel-stats',
  templateUrl: './panel-stats.component.html',
  styleUrls: ['./panel-stats.component.less'],
})
export class PanelStatsComponent implements OnInit {
  applicationStats: ApplicationStatsModel = {
    SUBMITTED: 0,
    WAITING: 0,
    APPROVED: 0,
    INTERVIEWED: 0,
    PUBLISHED: 0,
    CHANGED: 0,
    ACCEPTED: 0,
    CANCELLED: 0,
    CLOSED: 0,
    ACKNOWLEDGED: 0,
    ANALYSED: 0,
    DECLINED: 0,
    WITHDRAWAL: 0,
    EXPIRED: 0,
    DISPATCH: 0,
  };

  userInteresStats: UserInterestStatsModel = {
    SUBMITTED: 0,
    WAITING: 0,
    APPROVED: 0,
    INTERVIEWED: 0,
    ACCEPTED: 0,
    CANCELLED: 0,
    CLOSED: 0,
    ANALYSED: 0,
    DECLINED: 0,
    WITHDRAWAL: 0,
    COLABORATION: 0,
    NOT_SELECTED: 0,
    DISPATCH: 0,
  };

  experienceStats: ExperienceStatsModel = {
    SUBMITTED: 0,
    RETURNED: 0,
    ANALYSED: 0,
    APPROVED: 0,
    PUBLISHED: 0,
    REJECTED: 0,
    SEND_SEEM: 0,
    EXTERNAL_SYSTEM: 0,
    SELECTION: 0,
    IN_COLABORATION: 0,
    CLOSED: 0,
    CANCELED: 0,
    CONFIRMED: 0,
    DISPATCH: 0,
  };

  grid = {
    nzXs: 24,
    nzSm: 24,
    nzMd: 12,
    nzLg: 8,
    nzXl: 6,
    nzXXl: 4,
  };

  loadingStats = false;
  academic_years: AcademicYearModel[] = [];
  selectedAcademicYear;

  constructor(
    private academicYearService: AcademicYearsService,
    private applicationsService: ApplicationsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadAcademicYears();
    this.currentAcademicYear();
  }

  getStats(academic_year) {
    this.loadingStats = true;
    this.applicationsService
      .applicationsStats(academic_year)
      .pipe(first())
      .subscribe(
        (stats) => {
          this.applicationStats = stats.data[0];
          this.loadingStats = false;
        },
        () => {
          this.loadingStats = false;
        }
      );
  }

  getExperienceUserInterest(academic_year) {
    this.loadingStats = true;
    this.applicationsService
      .experienceUserInterest(academic_year)
      .pipe(first())
      .subscribe(
        (experience) => {
          this.userInteresStats = experience.data[0];
          this.loadingStats = false;
        },
        () => {
          this.loadingStats = false;
        }
      );
  }

  getExperienceStats(academic_year) {
    this.loadingStats = true;
    this.applicationsService
      .experienceStats(academic_year)
      .pipe(first(), finalize(() => this.loadingStats = false))
      .subscribe((experience) => this.experienceStats = experience.data[0]);
  }

  currentAcademicYear() {
    this.academicYearService
      .getCurrentAcademicYear()
      .pipe(first())
      .subscribe((data) => {
        this.selectedAcademicYear = data.data[0].academic_year;
        this.getStats(data.data[0].academic_year);
        this.getExperienceStats(data.data[0].academic_year);
        this.getExperienceUserInterest(data.data[0].academic_year);
      });
  }

  loadAcademicYears() {
    this.academicYearService
      .list(1, -1, 'start_date', 'descend')
      .pipe(first())
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  search() {
    this.getStats(this.selectedAcademicYear);
    this.getExperienceStats(this.selectedAcademicYear);
    this.getExperienceUserInterest(this.selectedAcademicYear);
  }

  listApplications(status: string) {
    this.router.navigate(['/volunteering/applications/app-all'], { queryParams: { status, academicYear: this.selectedAcademicYear } });
  }

  listExperiences(status: string) {
    this.router.navigate(['/volunteering/experiences'], { queryParams: { status, academicYear: this.selectedAcademicYear } });
  }

  listUserInterests(status: string) {
    this.router.navigate(['/volunteering/experience-user-interests/list'], { queryParams: { status, academicYear: this.selectedAcademicYear } });
  }
}
