import { Component, Input, OnInit } from '@angular/core';
import { ApplicationModel } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';

@Component({
  selector: 'fi-sas-view-application-modal',
  templateUrl: './view-application-modal.component.html',
  styleUrls: ['./view-application-modal.component.less'],
})
export class ViewApplicationModalComponent implements OnInit {
  @Input() application: ApplicationModel;

  constructor(
  ) {}

  ngOnInit() { }
}
