import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import {
  ApplicationData,
  ApplicationModel,
} from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { first, finalize } from 'rxjs/operators';
import { ApplicationTagResult } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model';
import { ApplicationsService } from '@fi-sas/backoffice/modules/volunteering/modules/v_applications/services/applications.service';
import { ViewApplicationModalComponent } from './../view-application-modal/view-application-modal.component';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
@Component({
  selector: 'fi-sas-view-application',
  templateUrl: './view-application.component.html',
  styleUrls: ['./view-application.component.less'],
})
export class ViewApplicationComponent implements OnInit {
  private _data: ApplicationModel = null;
  @Input() set data(data: ApplicationModel) {
    this._data = data;
    if (data && data.decision) {
      const dispatchState = this.decisionToStateMapper[data.decision];
      const state = this.status.find((s) => s.value === dispatchState);
      if (state) {
        this.dispatchDecision = state.label
      }
    }
  }
  get data() {
    return this._data;
  }

  @Output() itemChange = new EventEmitter<boolean>();

  ApplicationTagResult = ApplicationTagResult;

  @Input() subject: string;
  loadingCertificate = false;
  application: ApplicationModel = null;
  status = ApplicationData.status;
  statusMachine = ApplicationData.statusMachine;
  decisionToStateMapper = ApplicationData.decisionToStateMapper;
  modalsRefs: { name: string; modal: NzModalRef }[] = [];
  loading = false;

  dispatchDecision: string;

  constructor(
    private modalService: NzModalService,
    private uiService: UiService,
    private applicationsService: ApplicationsService
  ) { }

  createTplModal(
    tplTitle: TemplateRef<{}>,
    tplContent: TemplateRef<{}>,
    tplFooter: TemplateRef<{}>,
    size: number = 900,
    padding: string = '25px'
  ): NzModalRef {
    return this.modalService.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzWidth: size,
      nzBodyStyle: { padding: padding },
    });
  }

  generateModal(
    name: string,
    modalTitle: TemplateRef<{}>,
    modalContent: TemplateRef<{}>,
    modalFooter: TemplateRef<{}>,
    modalSize: number,
    padding?: string
  ) {
    this.modalsRefs.push({
      name: name,
      modal: this.createTplModal(
        modalTitle,
        modalContent,
        modalFooter,
        modalSize,
        padding
      ),
    });
  }

  filterStatus() {
    return this.status.filter((s) => s.order >= 0);
  }

  filterSchedule(scheduleID: number) {
    let found = this.application.preferred_schedule.find(
      (schedule) => schedule.id === scheduleID
    );

    return found !== undefined;
  }

  viewApplicationModal(id: number) {
    this.applicationsService
      .read(id, {
        id: id,
        withRelated: "history,experience,area_interests,attendance,user,course,course_degree,school,organic_units,preferred_schedule,historic_colaborations,historic_applications,skills"
      })
      .pipe(
        first(),
        finalize(() => (true))
      )
      .subscribe((result) => {
        this.application = result.data[0];
        this.modalService.create({
          nzWidth: 1024,
          nzMaskClosable: false,
          nzClosable: true,
          nzContent: ViewApplicationModalComponent,
          nzFooter: null,
          nzComponentParams: {
            application: this.application,
          },
          nzTitle: 'Inscrição do estudante ' + this.application.name
        });
      });
  }

  ngOnInit() {

    this.loading = true;
    this.applicationsService
      .read(this.data.id, {
        id: this.data.id,
        withRelated: "history,preferred_activities,user,course,course_degree,school,cv_file,organic_units,preferred_schedule,historic_colaborations,interviews,historic_applications"
      })
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((result) => {
        this.loading = false;
        this.application = result.data[0];
      });

  }

  getExperienceCertificate(id) {
    this.loadingCertificate = true;
    this.applicationsService.generateCertificate(id).pipe(first(), finalize(() => this.loadingCertificate = false)).subscribe((data) => {
      this.uiService.showMessage(
        MessageType.success,
        'Certificado gerado com sucesso'
      );
    })
  }

  dispatchChangeStatues(application: ApplicationModel, acceptDecision: boolean) {
    this.applicationsService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, `Decisão ${acceptDecision ? 'approvada' : 'rejeitada'}`);
        this.itemChange.emit(true);
      });
  }
}
