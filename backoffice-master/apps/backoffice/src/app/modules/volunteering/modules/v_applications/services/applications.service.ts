import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from "rxjs";

import { ApplicationModel } from "@fi-sas/backoffice/modules/volunteering/modules/v_applications/models/application.model";
import { ApplicationStatsModel } from '../models/application-stats';
import { ApproveRejectModel } from "@fi-sas/backoffice/modules/volunteering/models/approveReject";
import { CertificateModel } from "@fi-sas/backoffice/modules/volunteering/models/certificate.model";
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SendDispatchData } from '../../../models/dispatch.model';
import { ExperienceStatsModel } from '../../../models/experience-stats.model';
import { UserInterestStatsModel } from '../../../models/user-interest-stats.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService extends Repository<ApplicationModel>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.APPLICATIONS';
    this.entity_url = 'VOLUNTEERING.APPLICATIONS_ID';
  }

  status(id: number, application_status: ApproveRejectModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS_STATUS_ID', { id }), application_status);
  }

  certificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS_CERTIFICATE', { id }), {});
  }

  generateCertificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(
      this.urlService.get('VOLUNTEERING.GENERATE_COLLABORATION_CERTIFICATE', { id }),
      {}
    );
  }

  stats(subject?: string): Observable<Resource<ApplicationStatsModel>> {
    let params = new HttpParams();

    if (subject) {
      params = params.append('subject', subject);
    }

    return this.resourceService.read<ApplicationStatsModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_STATS', {}), { params }
    );
  }

  changeToLastStatus(id: number): Observable<Resource<ApplicationStatsModel>> {
    return this.resourceService.create<ApplicationStatsModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_LAST_STATUS', { id }),
      {}
    );
  }

  experienceStats(academic_year): Observable<Resource<ExperienceStatsModel>> {
    let params = new HttpParams();

    params = params.append('academic_year', academic_year);

    return this.resourceService.read<ExperienceStatsModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCE_STATS', {}),
      { params }
    );
  }

  experienceUserInterest(academic_year): Observable<Resource<UserInterestStatsModel>> {
    let params = new HttpParams();

    params = params.append('academic_year', academic_year);
    return this.resourceService.read<UserInterestStatsModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_INTEREST_STATS', {}),
      { params }
    );
  }


  applicationsStats(academic_year: string): Observable<Resource<ApplicationStatsModel>> {
    let params = new HttpParams();
    params = params.append('academic_year', academic_year);
    return this.resourceService.read<ApplicationStatsModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_STATS', {}),
      { params }
    );
  }

  sendDispatch(id: number, data: SendDispatchData): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_STATUS_ID', { id }),
      data
    );
  }
}
