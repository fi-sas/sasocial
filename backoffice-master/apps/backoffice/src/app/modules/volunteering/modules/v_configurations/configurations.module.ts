import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { FormConfigurationsComponent } from './pages/form-configurations/form-configurations.component';
import { ConfigurationsService } from './services/configurations.service';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    FormConfigurationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigurationsRoutingModule
  ],
  providers: [
    ConfigurationsService
  ]
})
export class VolunteeringConfigurationsModule { }
