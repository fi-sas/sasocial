import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { first, finalize, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AcademicYearModel } from '@fi-sas/backoffice/modules/configurations/models/academic-year.model';
import { AcademicYearsService } from '@fi-sas/backoffice/modules/configurations/services/academic-years.service';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationsService } from '../../services/configurations.service';
import { ProfileModel } from '@fi-sas/backoffice/modules/users/modules/profiles/models/profile.model';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { ProfilesService } from '@fi-sas/backoffice/modules/users/modules/profiles/services/profiles.service';

@Component({
  selector: 'fi-sas-form-configurations',
  templateUrl: './form-configurations.component.html',
  styleUrls: ['./form-configurations.component.less'],
})
export class FormConfigurationsComponent implements OnInit {
  loading = false;

  //FILTERS
  academic_years: AcademicYearModel[] = [];
  load_academic_years = false;
  submit = false;
  closeApplications = new FormGroup({
    close_academic_year: new FormControl('', [Validators.required]),
  });

  profiles$: Observable<ProfileModel[]> = this.getProfiles();

  configurationsForm = new FormGroup({
    profile_id: new FormControl(null, [Validators.required]),
  });
  configurationsFormLoading: boolean;

  constructor(
    private academicYearsService: AcademicYearsService,
    private authService: AuthService,
    private profilesService: ProfilesService,
    private uiService: UiService,
    public configurationsService: ConfigurationsService
  ) {}

  ngOnInit() {
    this.loadAcademicYears();
    this.getConfigurations();
  }

  loadAcademicYears() {
    this.load_academic_years = true;
    this.academicYearsService
      .list(0, -1)
      .pipe(
        first(),
        finalize(() => (this.load_academic_years = false))
      )
      .subscribe((result) => {
        this.academic_years = result.data;
      });
  }

  submitFormCloseApplications(event: any, value: any) {
    if (!this.authService.hasPermission('volunteering:configurations:update')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não tem acesso ao serviço solicitado');
      return;
    }
    this.submit = true;
    if (this.closeApplications.valid) {
      this.submit = false;
      this.loading = true;
      this.configurationsService
        .saveCloseApplication(value.close_academic_year)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((result) => {
          this.uiService.showMessage(MessageType.success, 'Configurações atualizadas com sucesso');
        });
    }
  }

  submitConfigurations() {
    if (this.configurationsForm.valid) {
      this.configurationsService
        .setExternalProfileId(this.configurationsForm.get('profile_id').value)
        .subscribe(() => this.uiService.showMessage(MessageType.success, 'Perfil externo configurado com sucesso.'));
    }
  }

  private getProfiles() {
    return this.profilesService.list(1, -1, null, null, { withRelated: false, sort: 'name' }).pipe(
      first(),
      map((result) => result.data)
    );
  }

  private getConfigurations() {
    this.configurationsService.getExternalProfileId().subscribe((result) => {
      const item = result.data.find((config) => config.key === 'external_entity_profile_id');
      if (item) {
        this.configurationsForm.get('profile_id').setValue(parseInt(item.value));
      }
    });
  }
}
