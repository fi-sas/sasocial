import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ConfigurationModel } from '@fi-sas/backoffice/modules/volunteering/modules/v_configurations/models/configuration.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationsService extends Repository<ConfigurationModel> {
  constructor(resourceService: FiResourceService, urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.CONFIGURATIONS_CLOSE_APPLICATIONS';
  }

  saveCloseApplication(close_academic_year: string): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.create<ConfigurationModel>(
      this.urlService.get('VOLUNTEERING.CONFIGURATIONS_CLOSE_APPLICATIONS'),
      { academic_year: close_academic_year }
    );
  }

  setExternalProfileId(id: number) {
    return this.resourceService.create<any>(this.urlService.get('VOLUNTEERING.CONFIGURATIONS_EXTERNAL_PROFILE_ID'), {
      value: id,
    });
  }

  getExternalProfileId() {
    return this.resourceService.list<ConfigurationModel>(this.urlService.get('VOLUNTEERING.CONFIGURATIONS_EXTERNAL_PROFILE_ID'));
  }
}
