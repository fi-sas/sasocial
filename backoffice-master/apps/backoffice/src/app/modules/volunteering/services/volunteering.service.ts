import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApplicationsService } from '../modules/v_applications/services/applications.service';
import { first, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VolunteeringService {

  loading = false;

  private _applicationStats: Subject<any> = new Subject<any>();

  constructor(private applicationsService: ApplicationsService) { }

  applicationStatsObservable() {
    return this._applicationStats.asObservable();
  }

  updateApplicationsStats() {

    if (this.loading) {
      return;
    }
    this.loading = true;

    this.applicationsService.stats('V')
      .pipe(
        first(),
        finalize(() => this.loading = false)
      )
      .subscribe((result) => {
        this._applicationStats.next(result.data[0]);
      });
  }
}
