import { Injectable } from '@angular/core';

import { Observable } from "rxjs";

import { ApproveRejectModel } from "@fi-sas/backoffice/modules/volunteering/models/approveReject";
import { ExperienceModel } from "@fi-sas/backoffice/modules/volunteering/modules/experiences/models/experience.model";
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { SendDispatchData } from '../models/dispatch.model';

@Injectable({
  providedIn: 'root'
})
export class VolunteeringExperiencesService extends Repository<ExperienceModel>{

  constructor(resourceService: FiResourceService,
    urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'VOLUNTEERING.EXPERIENCES';
    this.entity_url = 'VOLUNTEERING.EXPERIENCES_ID';
  }

  status(id: number, experience_status: ApproveRejectModel): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCES_ID_STATUS', { id }), experience_status);
  }

  userManifest(id: number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.read<ExperienceModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_MANIFEST', {
        id
      })
    );
  }

  userManifestSelection(id: number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.read<ExperienceModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCE_USER_MANIFEST_SELECTION', {
        id
      })
    );
  }

  revertStatus(id: Number): Observable<Resource<ExperienceModel>> {
    return this.resourceService.create<ExperienceModel>(this.urlService.get('VOLUNTEERING.EXPERIENCE_LAST_STATUS', { id }), {});
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<ExperienceModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCES_ID_STATUS', { id }),
      data
    );
  }
}
