import { VolunteeringComponent } from './volunteering.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { InitialPageComponent } from '@fi-sas/backoffice/shared/components/initial-page/initial-page.component';

const routes: Routes = [
  {
    path: '',
    component: VolunteeringComponent,
    children: [
      {
        path: 'applications',
        loadChildren: '../volunteering/modules/v_applications/applications.module#ApplicationsModule',
        data: { breadcrumb: 'Candidaturas', title: 'Candidaturas' },
      },
      {
        path: 'experiences',
        loadChildren: '../volunteering/modules/experiences/experiences.module#ExperiencesModule',
        data: { breadcrumb: 'Ações', title: 'Ações' },
      },
      {
        path: 'experience-user-interests',
        loadChildren: '../volunteering/modules/experience-user-interests/experience-user-interests.module#ExperienceUserInterestsModule',
        data: { breadcrumb: 'Inscrições', title: 'Inscrições'},
      },
      {
        path: 'collaborations-list',
        loadChildren: '../volunteering/modules/collaborations-lists/collaborations-lists.module#CollaborationsListsVolunteeringModule',
        data: { breadcrumb: 'Participações', title: 'Participações'},
      },
      {
        path: 'external-users',
        loadChildren: '../volunteering/modules/external-users/external-users.module#ExternalUsersModule',
        data: { breadcrumb: 'Utilizadores Externos', title: 'Utilizadores Externos'},
      },
      {
        path: 'complains',
        loadChildren: '../volunteering/modules/complains/complains.module#ComplainsModule',
        data: { breadcrumb: 'Constestações / Reclamações', title: 'Constestações / Reclamações'},
      },
      {
        path: 'reports',
        loadChildren: '../volunteering/modules/reports/reports.module#ReportsModule',
        data: { breadcrumb: 'Relatórios', title: 'Relatórios'},
      },
      {
        path: 'configurations',
        loadChildren: '../volunteering/modules/v_configurations/configurations.module#VolunteeringConfigurationsModule',
        data: { breadcrumb: 'Configurações', title: 'Configurações'},
      },
      {
        path: 'initial-page',
        component: InitialPageComponent,

      },
      { path: '', redirectTo: 'initial-page', pathMatch: 'full' },
      {
        path: '**',
        component: PageNotFoundComponent,
        data: {
          breadcrumb: 'Página não encontrada',
          title: 'Página não encontrada',
        },
      },
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolunteeringRoutingModule {}
