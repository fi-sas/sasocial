import { VolunteeringService } from './services/volunteering.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { SiderItem } from '@fi-sas/backoffice/core/models/sider-item';
import { Subscription, BehaviorSubject } from 'rxjs';
import { SiderItemBadge } from '@fi-sas/backoffice/core/models/sider-item-badge';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';

@Component({
  selector: 'fi-sas-volunteering',
  template: '<router-outlet></router-outlet>'
})
export class VolunteeringComponent implements OnInit, OnDestroy {

  totalSubmitted = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#4d9de0',
  });
  totalAnalyzed = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#7768ae',
  });
  totalInterviewed = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#88ccf1',
  });
  totalWaiting = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#d4b483',
  });
  totalAccepted = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#076743',
  });
  totalAcknowledged = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#1ba974',
  });
  totalPublished = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#9dbbae',
  });
  totalDeclined = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#f75c03',
  });
  totalCancelled = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#d0021b',
  });
  totalClosed = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: '#000000',
  });
  totalWithdrawal = new BehaviorSubject<SiderItemBadge>({
    value: 0,
    color: 'rgb(138, 44, 55)',
  });

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }
  dataConfiguration: any;

  constructor(private uiService: UiService,
    private volunteeringService: VolunteeringService, private authService: AuthService, private configurationsService: ConfigurationGeralService) {
        this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
    }
    updateBadges(stats: any) {
      this.totalSubmitted.next({
        value: stats.submitted ? stats.submitted : '-',
        color: '#4d9de0',
      });
      this.totalAnalyzed.next({
        value: stats.analysed ? stats.analysed : '-',
        color: '#7768ae',
      });
      this.totalInterviewed.next({
        value: stats.interviewed ? stats.interviewed : '-',
        color: '#88ccf1',
      });
      this.totalWaiting.next({
        value: stats.waiting ? stats.waiting : '-',
        color: '#d4b483',
      });
      this.totalAccepted.next({
        value: stats.accepted ? stats.accepted : '-',
        color: '#076743',
      });
      this.totalAcknowledged.next({
        value: stats.acknowledged ? stats.acknowledged : '-',
        color: '#1ba974',
      });
      this.totalPublished.next({
        value: stats.published ? stats.published : '-',
        color: '#9dbbae',
      });
      this.totalDeclined.next({
        value: stats.declined ? stats.declined : '-',
        color: '#f75c03',
      });
      this.totalCancelled.next({
        value: stats.cancelled ? stats.cancelled : '-',
        color: '#d0021b',
      });
      this.totalClosed.next({
        value: stats.closed ? stats.closed : '-',
        color: '#000000',
      });
      this.totalWithdrawal.next({
        value: stats.withdrawal ? stats.withdrawal : '-',
        color: 'rgb(138, 44, 55)',
      });
    }

    ngOnDestroy() {
      this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
    }

  ngOnInit() {

    this.uiService.setContentWrapperActive(true);
    this.uiService.removeSiderItems();

    this.uiService.setSiderTitle(this.validTitleTraductions(28), 'team');

    const stats = new SiderItem(
      'Painel',
      '',
      '/volunteering/applications/stats',
      'volunteering:applications:read'
    );
    this.uiService.addSiderItem(stats);


    const applications = new SiderItem('Candidaturas', '', '', 'volunteering:applications');
    applications.addChild(
      new SiderItem(
        'Listar',
        '',
        '/volunteering/applications/app-all',
        'volunteering:applications:read',
        null,
        null
      )
    );

    this.uiService.addSiderItem(applications);

    const experiences = new SiderItem('Ações', '', '', 'volunteering:experiences');
    experiences.addChild(new SiderItem('Criar', '', '/volunteering/experiences/create', 'volunteering:experiences:create'));
    experiences.addChild(new SiderItem('Listar', '', '/volunteering/experiences/list', 'volunteering:experiences:read'));
    this.uiService.addSiderItem(experiences);

    const experienceUserInterests = new SiderItem('Inscrições', '', '', 'volunteering:experience-user-interests');
    experienceUserInterests.addChild(
      new SiderItem(
        'Listar',
        '',
        '/volunteering/experience-user-interests/list',
        'volunteering:experience-user-interests:read'
      )
    );
    this.uiService.addSiderItem(experienceUserInterests);

    const complains = new SiderItem('Contestações / Reclamações');
    complains.addChild(
      new SiderItem(
        'Geral',
        '',
        '/volunteering/complains/list',
        'volunteering:general-complains:read'
      )
    );
    /*complains.addChild(
      new SiderItem(
        'Ações',
        '',
        '/volunteering/complains/list-experiences',
        'volunteering:complain-experiences:read'
      )
    );
    complains.addChild(
      new SiderItem(
        'Candidaturas',
        '',
        '/volunteering/complains/list-applications',
        'volunteering:complain-applications:read'
      )
    );*/
    complains.addChild(
      new SiderItem(
        'Inscrições',
        '',
        '/volunteering/complains/list-user-interests',
        'volunteering:complain-user-interests:read'
      )
    );


    this.uiService.addSiderItem(complains);

    const collaborations = new SiderItem('Participações', '', '', 'volunteering:experiences');
    collaborations.addChild(
      new SiderItem(
        'Lista por Estudante',
        '',
        '/volunteering/collaborations-list/student',
        'volunteering:experiences:read'
      ),

    );
    collaborations.addChild(
      new SiderItem(
        'Lista por Ação',
        '',
        '/volunteering/collaborations-list/action',
        'volunteering:experiences:read'
      ),
    );

    this.uiService.addSiderItem(collaborations);

    const externalUsers = new SiderItem('Utilizadores Externos');
    externalUsers.addChild(
      new SiderItem(
        'Criar',
        '',
        '/volunteering/external-users/create',
        ''
      )
    );
    externalUsers.addChild(
      new SiderItem(
        'Listar',
        '',
        '/volunteering/external-users/list',
        ''
      )
    );
    this.uiService.addSiderItem(externalUsers);

    const configurations = new SiderItem(
      'Configurações',
      '',
      '/volunteering/configurations/form',
      'volunteering:configurations'
    );
    this.uiService.addSiderItem(configurations);

    this.uiService.setSiderActive(true);

    const reports = new SiderItem(
      'Relatórios',
      '',
      '/volunteering/reports/list',
      'volunteering:general-reports'
    );
    this.uiService.addSiderItem(reports);

  }


  validTitleTraductions(id: number) {
    return this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).translations : [];
  }


}
