import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VolunteeringRoutingModule } from './volunteering-routing.module';
import { VolunteeringComponent } from './volunteering.component';
import { SharedModule } from '@fi-sas/backoffice/shared/shared.module';

@NgModule({
  declarations: [
    VolunteeringComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    VolunteeringRoutingModule
  ]
})
export class VolunteeringModule { }
