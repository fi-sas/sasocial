import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoUploadFileComponent } from './auto-upload-file.component';

describe('AutoUploadFileComponent', () => {
  let component: AutoUploadFileComponent;
  let fixture: ComponentFixture<AutoUploadFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoUploadFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoUploadFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
