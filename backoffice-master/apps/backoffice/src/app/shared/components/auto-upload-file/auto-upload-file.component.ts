import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  forwardRef,
} from '@angular/core';
import {
  MessageType,
  UiService,
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { UploadFile, UploadXHRArgs, UploadFilter } from 'ng-zorro-antd';
import { EMPTY, Observable, Observer } from 'rxjs';
import { first } from 'rxjs/operators';
import { FiConfigurator } from '@fi-sas/configurator';
import { FilesService } from '@fi-sas/backoffice/modules/medias/services/files.service';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FileMessageModel } from '../../models/file-message.model';

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

@Component({
  selector: 'fi-sas-auto-upload-file',
  templateUrl: './auto-upload-file.component.html',
  styleUrls: ['./auto-upload-file.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutoUploadFileComponent),
      multi: true,
    },
  ],
})
export class AutoUploadFileComponent implements OnInit {
  public _value;
  categoryId: number;
  languageId: number;
  disableUploadButton = false;
  filesUploaded = [];
  loadingFiles = false;

  previewImage: string | undefined = '';
  previewVisible = false;

  filesIDS: {
    file_id: number;
    uid: string;
  }[] = [];

  @Input() isPublic = 'true';
  @Input() weight = 1;
  @Input() multiUploads = false;
  @Input() listType = 'text';
  @Input() buttonName = 'Carregar anexo';
  /* Files Types:
   *   image/png
   *   image/jpeg
   *   image/bmp
   *   application/pdf
   *   application/doc
   *   image/*
   *   audio/*
   *   video/*
   *   */

  @Input() limit = 0;
  @Input() filterTypes = [];
  @Input() messages: FileMessageModel = {
    upload: {
      success: 'Ficheiro carregado com sucesso',
      error: 'Erro ao carregar o ficheiro',
    },
    delete: {
      success: 'Ficheiro eliminado com sucesso',
      error: 'Erro ao eliminar o ficheiro',
    },
    load: {
      error: 'Erro ao carregar o ficheiro',
    },
    invalid: {
      error: 'Tipo de ficheiro inválido',
    },
  };
  @Input() filesList = [];
  typeFile: UploadFile = null;

  filters: UploadFilter[] = [
    {
      name: 'type',
      fn: (fileList: UploadFile[]) => {
        if (this.filterTypes.length !== 0) {
          const filterFiles = fileList.filter(
            (w) => ~this.filterTypes.indexOf(w.type)
          );
          if (filterFiles.length !== fileList.length) {
            this.uiService.showMessage(
              MessageType.error,
              this.messages.invalid.error
            );
            return filterFiles;
          }
        }
        return fileList;
      },
    },
    {
      name: 'async',
      fn: (fileList: UploadFile[]) => {
        return new Observable((observer: Observer<UploadFile[]>) => {
          // doing
          observer.next(fileList);
          observer.complete();
        });
      },
    },
  ];

  @Output() fileAdded = new EventEmitter();
  @Output() fileDeleted = new EventEmitter();

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    private uiService: UiService,
    private filesServices: FilesService,
    private configurator: FiConfigurator
  ) {
    if (!this.languageId) {
      this.languageId = this.configurator.getOption('DEFAULT_LANG_ID');
    }

    if (!this.categoryId) {
      this.categoryId = this.configurator.getOption('DEFAULT_CATEGORY_ID');
    }
  }

  ngOnInit() {
    this.loadingFiles = true;
    this.filesList = this.filesList.filter(f => f !== null);
    if (this.filesList.length !== 0) {
      this.loadFiles();
    } else {
      this.loadingFiles = false;
    }
  }

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(this._value);
    this.onTouched();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.value = value;
      this.onChange(this.value);
    }
  }

  uploadFile = (item: UploadXHRArgs) => {
    const formData = new FormData();
  /*   formData.append('name', item.file.name);
    formData.append('weight', this.weight.toString());
    formData.append('public', this.isPublic);
    formData.append('file_category_id', this.categoryId.toString());
    formData.append('language_id', this.languageId.toString());
    formData.append('file', item.file as any, item.file.name); */

    formData.append('file', item.file as any, item.file.name);
    formData.append('name', item.file.name);
    formData.append('weight', this.weight.toString());
    formData.append('public', this.isPublic);
    formData.append('file_category_id', this.categoryId.toString());
    formData.append('language_id', this.languageId.toString());

    const req = this.filesServices.createFile2(formData);

    return req.subscribe(
      (file) => {
        this.value = file['data'][0].id;
        this.fileAdded.emit(this.value);
        item.onSuccess!({}, item.file!, event);
        this.filesUploaded.push({
          uid: item.file.uid,
          name: file.data[0].filename,
          status: "done",
          url: file.data[0].url,
          thumbUrl: file.data[0].url,
        });
        this.multiUploads
          ? (this.disableUploadButton = false)
          : (this.disableUploadButton = true);
        this.filesIDS.push({ file_id: file['data'][0].id, uid: item.file.uid });
        if (this.limit !== 0) {
          this.disableUploadButton = this.limit === this.filesIDS.length;
        }
        this.uiService.showMessage(
          MessageType.success,
          this.messages.upload.success
        );
      },
      (err) => {
        item.onError!(err, item.file!);
        this.uiService.showMessage(
          MessageType.error,
          this.messages.upload.error
        );
      }
    );
  };

  removeFile = (file: UploadFile) => {
    if (this.filesIDS.length !== 0) {
      return this.remove(
        this.filesIDS.find((fileID) => fileID.uid === file.uid).file_id
      );
    } else {
      return true;
    }
  };

  remove(fileID: number): Observable<boolean> {
    return Observable.create(
      (o) => {
        this.filesServices
          .delete(fileID)
          .pipe(first())
          .subscribe(
            () => {
              // this.fileDeleted.emit(this.value);

              this.filesIDS.splice(
                this.filesIDS.findIndex((id) => id.file_id === fileID),
                1
              );

              this.fileDeleted.emit(fileID);
              this.uiService.showMessage(
                MessageType.success,
                this.messages.delete.success
              );
              this.disableUploadButton = false;
              o.next(true);
              o.complete();
            },
            () => {
              this.uiService.showMessage(
                MessageType.error,
                this.messages.upload.error
              );
              o.error(false);
            }
          );
      },
      () => {
        return EMPTY;
      }
    );
  }

  handlePreview = (file: UploadFile) => {
    this.typeFile = file ? file : null;
    if (!file.url && !file.preview) {
      file.preview = getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
    if(this.typeFile.type != '') {
      window.open(this.getURL());
    }else if(this.typeFile.url != '') {
      window.open(this.typeFile.url);
    }
  };

  getURL() {
    return this.filesUploaded.find((fileID) => fileID.uid === this.typeFile.uid).url;
  }

  loadFiles(): void {
    this.filesServices
      .listFilesByIDS(this.filesList)
      .pipe(first())
      .subscribe(
        (files) => {
          files.data.forEach((file) => {
            this.filesUploaded.push({
              uid: file.id.toString(),
              name: file.filename,
              status: 'done',
              url: file.url,
              thumbUrl: file.url,
            });
            this.filesIDS.push({ file_id: file.id, uid: file.id.toString() });
            if (this.limit !== 0) {
              this.disableUploadButton = this.limit === this.filesIDS.length;
            }
          });
          this.loadingFiles = false;
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.messages.load.error
          );
          this.loadingFiles = false;
        }
      );
  }
}
