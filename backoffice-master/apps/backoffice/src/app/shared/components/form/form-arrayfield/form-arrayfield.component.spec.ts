import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormArrayfieldComponent } from './form-arrayfield.component';

describe('FormArrayfieldComponent', () => {
  let component: FormArrayfieldComponent;
  let fixture: ComponentFixture<FormArrayfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormArrayfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormArrayfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
