import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { FormField } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';

@Component({
  selector: 'fi-sas-form-arrayfield',
  templateUrl: './form-arrayfield.component.html',
  styleUrls: ['./form-arrayfield.component.less']
})
export class FormArrayfieldComponent implements OnInit {

  @Input()
  formGroup: FormGroup = null;

  @Input()
  field: FormField = null;

  constructor() { }

  ngOnInit() {
  }

  getFormArray(): FormArray {
    return this.formGroup.get(this.field.key) as FormArray;
  }

  addLine() {
    if(!this.field.fields)
      return;
    const fg = new FormGroup({});
    this.field.fields.map(f => {
      fg.addControl(f.key, new FormControl(f.defaultValue, f.templateOptions.validators ? f.templateOptions.validators : []));
    });
    this.getFormArray().push(fg);
  }

  removeLine(index: number) {
    this.getFormArray().removeAt(index);
  }
}
