import { FormField } from './../interfaces/form-field.interface';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.less']
})
export class FormGroupComponent implements OnInit {
  @Input() formGroup: FormGroup = null;

  @Input() field: FormField = null;

  @Input() model: any = null;

  constructor() {}

  ngOnInit() {}
}
