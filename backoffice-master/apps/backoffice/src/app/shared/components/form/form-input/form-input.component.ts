import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup, AbstractControl } from '@angular/forms';
import { FormField, InputType } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';

@Component({
  selector: 'fi-sas-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.less']
})
export class FormInputComponent implements OnInit {
  InputType = InputType;

  @Input()
  showLabel = true;

  @Input()
  field: FormField = null;

  @Input()
  formGroup: FormGroup = null;

  formatterDefault = (value: number) => value;
  parserDefault = (value: number) => value;

  constructor() { }

  ngOnInit() {
  }

  getInputError() {

    return this.formGroup.controls[this.field.key].errors.required  ? `Campo ${this.field.templateOptions.name} é obrigatório` :
      this.formGroup.controls[this.field.key].errors.maxlength  ? `Campo ${this.field.templateOptions.name} não pode contar mais que ${this.formGroup.controls[this.field.key].errors.maxlength.requiredLength} caracteres` :
      this.formGroup.controls[this.field.key].errors.minlength  ? `Campo ${this.field.templateOptions.name} deve contar mais que  ${this.formGroup.controls[this.field.key].errors.minlength.requiredLength} caracteres` :
      this.formGroup.controls[this.field.key].errors.email  ? `Campo ${this.field.templateOptions.name} não é um email valido` :
      this.formGroup.controls[this.field.key].errors.pattern  ? `Campo ${this.field.templateOptions.name} não é uma expressão valida` :
      this.formGroup.controls[this.field.key].errors.jsonParseError ? `Campo ${this.field.templateOptions.name} não é um json valido`
      : '';
  }
}
