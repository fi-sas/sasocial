import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormJsonfieldComponent } from './form-jsonfield.component';

describe('FormJsonfieldComponent', () => {
  let component: FormJsonfieldComponent;
  let fixture: ComponentFixture<FormJsonfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormJsonfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormJsonfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
