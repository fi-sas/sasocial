import { FormControl, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { ControlValueAccessor, Validator } from '@angular/forms';
import { Component, OnInit, forwardRef } from '@angular/core';

@Component({
  selector: 'fi-sas-form-jsonfield',
  templateUrl: './form-jsonfield.component.html',
  styleUrls: ['./form-jsonfield.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormJsonfieldComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => FormJsonfieldComponent),
      multi: true
    }
  ]
})
export class FormJsonfieldComponent implements ControlValueAccessor, Validator {
  public jsonString: string;
  private parseError: boolean;
  private data: any;

  // this is the initial value set to the component
  public writeValue(obj: any) {
    if (obj) {
      this.data = obj;
      // this will format it with 4 character spacing
      this.jsonString = JSON.stringify(this.data, undefined, 4);
    }
  }

  // registers 'fn' that will be fired wheb changes are made
  // this is how we emit the changes back to the form
  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  // validates the form, returns null when valid else the validation object
  // in this case we're checking if the json parsing has passed or failed from the onChange method
  public validate(c: FormControl) {
    return !this.parseError
      ? null
      : {
          jsonParseError: {
            valid: false
          }
        };
  }

  // not used, used for touch input
  public registerOnTouched() {}

  // change events from the textarea
  public onChange(event) {
    // get value from text area
    const newValue = event.target.value;

    try {
      // parse it to json
      this.data = JSON.parse(newValue);
      this.parseError = false;
    } catch (ex) {
      // set parse error if it fails
      this.parseError = true;
    }

    // update the form
    this.propagateChange(this.data);
  }

  // the method set in registerOnChange to emit changes back to the form
  private propagateChange = (_: any) => {};
}
