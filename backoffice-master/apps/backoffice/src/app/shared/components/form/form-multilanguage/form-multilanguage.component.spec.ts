import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMultilanguageComponent } from './form-multilanguage.component';

describe('FormMultilanguageComponent', () => {
  let component: FormMultilanguageComponent;
  let fixture: ComponentFixture<FormMultilanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMultilanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMultilanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
