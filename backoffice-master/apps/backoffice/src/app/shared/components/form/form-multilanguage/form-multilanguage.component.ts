import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { first } from 'rxjs/operators';
import { FiConfigurator } from '@fi-sas/configurator';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormField } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';

@Component({
  selector: 'fi-sas-form-multilanguage',
  templateUrl: './form-multilanguage.component.html',
  styleUrls: ['./form-multilanguage.component.less']
})
export class FormMultilanguageComponent implements OnInit, OnChanges {

  constructor(private languagesService: LanguagesService,
              private configurator: FiConfigurator) {
    this.languagesService.list(0, -1).pipe(first()).subscribe(value => {
      this.languages = value.data;
    });

    this.default_language = this.configurator.getOption('DEFAULT_LANG_ID');
  }


  languages: LanguageModel[] = [];
  selected_language: LanguageModel = null;
  default_language = null;
  @Input()
  formGroup: FormGroup = null;

  @Input()
  field: FormField = null;

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formGroup) {
      if (changes.formGroup.currentValue.value[this.field.key].length === 0) {
        this.newLanguage(this.default_language, {});
      }
    }
  }

  initTranslation(language_id: number, obj: any) {
    const fg = new FormGroup({
      language_id: new FormControl(language_id, [Validators.required]),
    });

    this.field.fields.map(f => {
      fg.addControl(f.key, new FormControl('', f.templateOptions.validators))
    });

    return fg;
  }

  newLanguage(language_id: number, obj: any) {
    this.selected_language = null;
    this.getFormLanguages().push(this.initTranslation(language_id, obj));
  }

  deleteTranslation(i: number) {
    const language_id = this.getFormLanguages().controls[i].get('language_id')
      .value;
    this.selected_language = null;
    this.getFormLanguages().removeAt(i);
  }

  getLanguageName(language_id: number): string {
    const result = this.languages.find(language => language.id === language_id);
    if(result) {
      return result.name;
    } else {
      return '(Não existente)';
    }

  }

  availableLanguages(): LanguageModel[] {
    const languagesUsed = this.getFormLanguages().controls.map(ctr => ctr.value.language_id);
    return this.languages.filter(l => !languagesUsed.find(l1 => l1 === l.id))
  }

  getFormLanguages(): FormArray {
    // TODO CHANGE THIS THIS LATER FOR PERFORMANCE
   return this.formGroup.get(this.field.key) as FormArray;
  }

}
