import { BaseRepositoryInterface } from '@fi-sas/backoffice/shared/repository/base-repository.interface';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { FetchFromObject } from './../../../utils/fetchFromObject.class';
import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  OnChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormField } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';
import { BehaviorSubject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap
} from 'rxjs/operators';
import { FiConfigurator } from '@fi-sas/configurator';

@Component({
  selector: 'fi-sas-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.less']
})
export class FormSelectComponent implements OnInit, OnChanges {
  isLoading = false;

  @Input() formGroup: FormGroup;

  @Input() field: FormField;

  default_language: number;

  public dataSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private searchSubject: BehaviorSubject<string> = new BehaviorSubject('');

  private repository: BaseRepositoryInterface<any> = null;
  private lastRepoDynParams = {};

  constructor(
    private ref: ChangeDetectorRef,
    private configurator: FiConfigurator
  ) {
    this.default_language = this.configurator.getOption('DEFAULT_LANG_ID');
  }

  ngOnInit() {


    if (this.field.templateOptions.repository) {
      this.field.templateOptions.options = [];
      this.dataSubject.next(this.field.templateOptions.options);
      this.repository = this.field.templateOptions.repository;
      this.isLoading = true;

      this.formGroup.valueChanges.subscribe(changes => {
        const repoDynParams =
          this.field.templateOptions.repositoryDynamicParams !== undefined
            ? this.field.templateOptions.repositoryDynamicParams(changes)
            : {};

        if (JSON.stringify(this.lastRepoDynParams) !== JSON.stringify(repoDynParams)) {
          this.lastRepoDynParams = repoDynParams;
            this.loadTotal();
        }
      });

      if (this.field.templateOptions.serverSearch) {
        this.serverSearch();
      } else {
        this.loadTotal();
      }
    } else {
      this.dataSubject.next(this.field.templateOptions.options);
    }
  }

  serverSearch() {
    if (!this.field.templateOptions.searchKey) {
      throw Error(
        `'searchKey' has to be defined on serverSearch equals true on input ${
          this.field.key
        }`
      );
    }

    const repoParams = this.field.templateOptions.repositoryParams
      ? this.field.templateOptions.repositoryParams
      : {};

    this.searchSubject
      .asObservable()
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => (this.isLoading = true)),
        switchMap(term =>
          this.repository.list(1, 10, null, null, {
            name: term,
            ...repoParams,
            ...this.lastRepoDynParams
          })
        ),
        tap(() => (this.isLoading = false))
      )
      .subscribe(values => {
        this.field.templateOptions.options = [];
        this.dataSubject.next(this.field.templateOptions.options);

        let valueKey = 'id';

        if (this.field.templateOptions.selectValueKey) {
          valueKey = this.field.templateOptions.selectValueKey;
        }
        values.data.map(value => {
          this.field.templateOptions.options.push({
            value: value[valueKey],
            label: this.processLabel(value)
          });
        });
        this.dataSubject.next(this.field.templateOptions.options);
      });
  }

  loadTotal() {
    const repoParams = this.field.templateOptions.repositoryParams
      ? this.field.templateOptions.repositoryParams
      : {};

    this.repository
      .list(1, -1, null, null, {
        ...repoParams,
        ...this.lastRepoDynParams
      })
      .subscribe(values => {
        this.field.templateOptions.options = [];
        let valueKey = 'id';

        if (this.field.templateOptions.selectValueKey) {
          valueKey = this.field.templateOptions.selectValueKey;
        }

        values.data.map(value => {
          this.field.templateOptions.options.push({
            value: value[valueKey],
            label: this.processLabel(value)
          });
        });
        this.dataSubject.next(this.field.templateOptions.options);
        this.isLoading = false;
      });
  }

  processLabel(obj: {}): string {
    let labelKey = 'name';

    if (this.field.templateOptions.selectLabelKey) {
      labelKey = this.field.templateOptions.selectLabelKey;
    }
    let label = '(Sem Nome)';
    let sufix = '';
    let prefix = '';
    if (this.field.templateOptions.sufix) {
      sufix = FetchFromObject.fetchFromObject(
        obj,
        this.field.templateOptions.sufix
      );
      if (!sufix) {
        sufix = this.field.templateOptions.sufix;
      }
      sufix = ' [' + sufix + ']';
    }

    if (this.field.templateOptions.prefix) {
      prefix = FetchFromObject.fetchFromObject(
        obj,
        this.field.templateOptions.prefix
      );
      if (!prefix) {
        prefix = this.field.templateOptions.prefix;
      }
      prefix = '[' + sufix + '] ';
    }

    if (labelKey === 'translations') {
      label = obj['translations'].find(
        t => t.language_id === this.default_language
      ).name;
    } else {
      label = obj[labelKey];
    }

    return prefix + label + sufix;
  }

  ngOnChanges() {
    this.ref.detectChanges();
  }

  onSearch(search: string) {
    this.searchSubject.next(search);
  }
}
