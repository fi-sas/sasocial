import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTransferFieldComponent } from './form-transfer-field.component';

describe('FormTransferFieldComponent', () => {
  let component: FormTransferFieldComponent;
  let fixture: ComponentFixture<FormTransferFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTransferFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTransferFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
