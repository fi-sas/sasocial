import { FormField } from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fi-sas-form-transfer-field',
  templateUrl: './form-transfer-field.component.html',
  styleUrls: ['./form-transfer-field.component.less']
})
export class FormTransferFieldComponent implements OnInit {

  @Input() formGroup: FormGroup = null;
  @Input() field: FormField = null;

  constructor() { }

  ngOnInit() {
  }

}
