import { Observable, throwError } from 'rxjs';
import { Component, Input, OnDestroy, OnInit, ViewRef, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BaseRepositoryInterface } from '@fi-sas/backoffice/shared/repository/base-repository.interface';
import { finalize, first, tap, catchError } from 'rxjs/operators';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { ListService } from '@fi-sas/backoffice/shared/services/list.service';
import {
  FormField,
  InputType
} from '@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface';
import * as moment from 'moment';
import {colorSets} from "@swimlane/ngx-charts/release/utils";

@Component({
  selector: 'fi-sas-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less']
})
export class FormComponent implements OnInit, OnDestroy {
  InputType = InputType;
  loaded = false;
  preloadedModel: any = null;

  /**
   * String of base permission
   * @default false
   * @memberof FormComponent
   */
  @Input() basePermission = null;

  /**
   *  Flag to active patch method update
   * @default false
   * @memberof FormComponent
   */
  @Input() formPatch = false;

  /**
   *  Flag to active padding arround the form
   * @default true
   * @memberof FormComponent
   */
  @Input() formPadding = true;

  /**
   *  Flag to active the scroll on the form
   *
   * @default true
   * @memberof FormComponent
   */
  @Input() formScroll = true;

  /**
   * Entity name is the string that represents the name of the entity
   * that show on the message when the entity is created, updated, deleted
   *
   * @memberof FormComponent
   */
  @Input() entityName = null;

  /**
   * Flag that when enable te model is update in consequence of a item selected
   * on a ListComponent
   *
   * @memberof FormComponent
   */
  @Input() isListUpdate = false;

  /**
   *  Flag to active/desactive the top toolstrip of the form
   *
   * @memberof FormComponent
   */
  @Input() showToolstrip = true;

  _model: any = {};

  /**
   * To change directly to FormGroup (not recommended)
   *
   * @memberof FormComponent
   */
  @Input() formGroup = new FormGroup({});

  /**
   *
   * Set the model (the entity repesented on the form) and convert / load the
   * values to the form
   * @memberof FormComponent
   */
  @Input()
  set model(model: any) {
    if (!this.loaded) {
      this.preloadedModel = model;
      return;
    }

    // ADD THE # to the color
    const colors = this.getFormFields(InputType.color);
    colors.map(c => {
      this.formGroup
        .get(c.key)
        .patchValue(this.modelToFormControlValueConverter(model, c));
    });

    // PATCH SELECTS
    const selects = this.getFormFields(InputType.select);
    selects.map(select => {
      this.formGroup
        .get(select.key)
        .patchValue(this.modelToFormControlValueConverter(model, select));
    });

    this._model = model;
    if (model) {
      // PATCH INPUTS
      const inputs = this.getFormFields([
        InputType.checkbox,
        InputType.number,
        InputType.input,
        InputType.switch,
        InputType.textarea,
        InputType.json,
        InputType.uploadImage,
        InputType.uploadFile
      ]);

      const tObjP = {};
      inputs.map(i => {
        tObjP[i.key] = this.modelToFormControlValueConverter(model, i);
      });
      this.formGroup.patchValue(tObjP);

      // PACH TIME PICKERS
      const dates = this.getFormFields(InputType.date);
      dates.map(t => {
        const obj = {};
        obj[t.key] = this.modelToFormControlValueConverter(model, t);
        this.formGroup.patchValue(obj);
      });

      // PACH TIME PICKERS
      const times = this.getFormFields(InputType.time);
      times.map(t => {
        const obj = {};
        obj[t.key] = this.modelToFormControlValueConverter(model, t);
        this.formGroup.patchValue(obj);
      });

      // PATCH DATE TIME PICKERS
      const dateranges = this.getFormFields(InputType.daterange);
      dateranges.map(dr => {
        const obj = {};
        obj[dr.key] = this.modelToFormControlValueConverter(model, dr);
        this.formGroup.patchValue(obj);
      });

      // PACTH TRANSLATIONS
      const translations = this.getFormFields(InputType.multilanguage);
      translations.map(trn => {
        if (this.model.hasOwnProperty(trn.key)) {
          const formArray = this.formGroup.get(trn.key) as FormArray;
          while (formArray.length) {
            formArray.removeAt(formArray.length - 1);
          }
          this.model[trn.key].map(trn_model => {
            const formTrnGroup = new FormGroup({});
            formTrnGroup.addControl(
              'language_id',
              new FormControl(trn_model.language_id, [Validators.required])
            );
            trn.fields.map(trn_fld => {
              formTrnGroup.addControl(
                trn_fld.key,
                new FormControl(
                  this.modelToFormControlValueConverter(trn_model, trn_fld),
                  trn_fld.templateOptions.validators
                    ? trn_fld.templateOptions.validators
                    : []
                )
              );
            });
            formArray.push(formTrnGroup);
          });
        }
      });

      //PATCH ARRAY FIELDS
      const arrayFields = this.getFormFields(InputType.arrayfield);
      arrayFields.map(af => {
        if (this.model.hasOwnProperty(af.key)) {
          const formArray = this.formGroup.get(af.key) as FormArray;
          while (formArray.length) {
            formArray.removeAt(formArray.length - 1);
          }

          this.model[af.key].map(af_model => {
            const formAfGroup = new FormGroup({});
            af.fields.map(af_fld => {
              formAfGroup.addControl(
                af_fld.key,
                new FormControl(
                  this.modelToFormControlValueConverter(af_model, af_fld),
                  af_fld.templateOptions.validators
                    ? af_fld.templateOptions.validators
                    : []
                )
              );
            });
            formArray.push(formAfGroup);
          });
        }
      });

      //PATCH IMAGE GALLERY
      const galleryFields = this.getFormFields(InputType.uploadImageGallery);
      galleryFields.map(gf => {
        if (this.model.hasOwnProperty(gf.key) || this.model.hasOwnProperty(gf.model_name)) {
          const formArray = this.formGroup.get(gf.key) as FormArray;
          while (formArray.length) {
            formArray.removeAt(formArray.length - 1);
          }

          (this.modelToFormControlValueConverter(this.model, gf) as number[]).map(file_id => {
            const formGfGroup = new FormGroup({});
            formGfGroup.addControl(
              'file_id',
              new FormControl(
                file_id,
                [Validators.required]
              )
            );
            formArray.push(formGfGroup);
          });
        }
      });

      // PATCH DATE TIME PICKERS
      const monthPicker = this.getFormFields(InputType.monthPicker);
      monthPicker.map(my => {
        const obj = {};
        obj[my.key] = this.modelToFormControlValueConverter(model, my);
        this.formGroup.patchValue(obj);
      });
    }

    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }
  }

  @Input() fields: Array<FormField> = [];

  @Input() service: BaseRepositoryInterface<any>;

  isLoading = false;

  constructor(private listService: ListService, private uiService: UiService, private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.fields.map(field => {
      switch (field.type) {
        case InputType.multilanguage:
          this.formGroup.addControl(field.key, new FormArray([]));
          break;
        case InputType.arrayfield:
          this.formGroup.addControl(field.key, new FormArray([]));
          break;
        case InputType.uploadImageGallery:
          this.formGroup.addControl(field.key, new FormArray([]));
          break;
        case InputType.group:
          this.formGroup.addControl(field.key, new FormGroup({}));
          break;
        default:
          this.formGroup.addControl(
            field.key,
            new FormControl(
              field.defaultValue,
              field.templateOptions.validators
                ? field.templateOptions.validators
                : []
            )
          );
      }
    });

    if (this.isListUpdate) {
      this.listService.selectedItemObservable().subscribe(item => {
        if (item) {
          this.model = item;
        }
      });
    }

    this.loaded = true;
    if (this.preloadedModel) {
      this.model = this.preloadedModel;
      this.preloadedModel = null;
    }
  }

  ngOnDestroy() {}

  /**
   *
   *  Return the value of the model (the original entity sent to the component)
   * @readonly
   * @type {*}
   * @memberof FormComponent
   */
  get model(): any {
    return this._model;
  }

  /**
   *
   * Return the value of the Form
   * @returns {*}
   * @memberof FormComponent
   */
  getValue(): any {
    return this.formGroup.value;
  }

  /**
   *
   *  Return a boolean with if the form as passed all validators
   * @returns {boolean}
   * @memberof FormComponent
   */
  isValid(): boolean {
    return this.formGroup.valid;
  }

  /**
   *
   *  Return the status of loading flag
   * @returns {boolean}
   * @memberof FormComponent
   */
  getIsLoading(): boolean {
    return this.isLoading;
  }

  onSubmitClickButton() {
    this.submit().subscribe();
  }

  /**
   *
   *  Prepare the value and sent the request to the micro service POST/PUT
   *    if already exist (presence of id)
   * @returns
   * @memberof FormComponent
   */
  submit(): Observable<boolean> {
    return Observable.create(observer => {
      if (!this.formGroup.valid) {
        this.uiService.showMessage(
          MessageType.warning,
          'Existe campos do formulário não validos'
        );
        this.formGroup.markAsDirty();
        this.formGroup.markAsTouched();
        observer.next(false);
        observer.complete();
        return;
      }

      if (!this.service) {
        observer.next(false);
        observer.complete();
        throw new Error('No service provided!');
      }

      const value = this.formValuesConversor(this.formGroup.value);

      this.isLoading = true;
      this.formGroup.disable();
      if (this.model['id']) {
        if (!this.formPatch) {
          this.service
            .update(this.model.id, value)
            .pipe(
              catchError(err => {
                observer.next(false);
                observer.complete();
                return throwError(err);
              }),
              finalize(() => {
                this.isLoading = false;
                this.formGroup.enable();
              })
            )
            .subscribe(result => {
              this.model = result.data[0];
              this.uiService.showMessage(
                MessageType.success,
                this.entityName
                  ? `${this.entityName} alterada/o com sucesso`
                  : `Alteração efetuada`
              );

              if (this.isListUpdate) {
                this.listService.itemUpdated(this.model);
              }

              observer.next(true);
              observer.complete();
            });
        } else {
          this.service
          .patch(this.model.id, value)
          .pipe(
            catchError(err => {
              observer.next(false);
              observer.complete();
              return throwError(err);
            }),
            finalize(() => {
              this.isLoading = false;
              this.formGroup.enable();
            })
          )
          .subscribe(result => {
            this.model = result.data[0];
            this.uiService.showMessage(
              MessageType.success,
              this.entityName
                ? `${this.entityName} alterada/o com sucesso`
                : `Alteração efetuada`
            );

            if (this.isListUpdate) {
              this.listService.itemUpdated(this.model);
            }

            observer.next(true);
            observer.complete();
          });
        }
      } else {
        this.service
          .create(value)
          .pipe(
            catchError(err => {
              observer.next(false);
              observer.complete();
              return throwError(err);
            }),
            finalize(() => {
              this.isLoading = false;
              this.formGroup.enable();
            })
          )
          .subscribe(result => {
            this.model = result.data[0];
            this.uiService.showMessage(
              MessageType.success,
              this.entityName
                ? `${this.entityName} criada/o com sucesso`
                : `Inserção efetuada`
            );

            observer.next(true);
            observer.complete();
          });
      }
    });
  }

  /**
   *
   *  Send the request to delete a entity
   * @memberof FormComponent
   */
  deleteEntity() {
    this.uiService
      .showConfirm(
        'Eliminar',
        this.entityName
          ? `Pretende eliminar este/a ${this.entityName}`
          : 'Pretende eliminar?',
        'Eliminar'
      )
      .subscribe(result => {
        if (result) {
          this.service
            .delete(this.model.id)
            .pipe(first())
            .subscribe(() => {
              this.listService.deleteItem(this.model);
            });
        }
      });
  }

  /**
   *
   *  Returns the fields of a type or types sent on the param
   * @param {(InputType | InputType[])} [type]
   * @param {FormField[]} [fields]
   * @returns {FormField[]}
   * @memberof FormComponent
   */
  getFormFields(
    type?: InputType | InputType[],
    fields?: FormField[]
  ): FormField[] {
    if (type) {
      if (Array.isArray(type)) {
        return fields
          ? fields.filter(f => type.includes(f.type))
          : this.fields.filter(f => type.includes(f.type));
      } else {
        return fields
          ? fields.filter(f => f.type === type)
          : this.fields.filter(f => f.type === type);
      }
    }

    return this.fields;
  }

  /**
   *
   * Retun all fields except the type sent on the params
   * @param {(InputType | InputType[])} [type]
   * @param {FormField[]} [fields]
   * @returns {FormField[]}
   * @memberof FormComponent
   */
  getFormFieldsExcept(
    type?: InputType | InputType[],
    fields?: FormField[]
  ): FormField[] {
    if (type) {
      if (Array.isArray(type)) {
        return fields
          ? fields.filter(f => !type.includes(f.type))
          : this.fields.filter(f => !type.includes(f.type));
      } else {
        return fields
          ? fields.filter(f => f.type !== type)
          : this.fields.filter(f => f.type !== type);
      }
    }

    return this.fields;
  }

  /**
   *
   * This function prepare the values before the submit function request to the micro service
   * @param {*} value
   * @param {FormField[]} [fields]
   * @returns {*}
   * @memberof FormComponent
   */
  formValuesConversor(value: any, fields?: FormField[]): any {
    //Update color type values
    const colors = this.getFormFields(InputType.color, fields);
    colors.map(c => {
      value[c.key] = <string>value[c.key].replace('#', '');
    });

    //Update date type values
    const dates = this.getFormFields(InputType.date, fields);
    dates.map(d => {
      value[d.key] = moment(value[d.key]).format('YYYY-MM-DDTHH:mm:ss.000');
    });

    //Update image gallery
    const images = this.getFormFields(InputType.uploadImageGallery, fields);
    images.map(i => {
      value[i.key] = value[i.key].map(v => v.file_id);
    });

    //Update daterange typevalues
    const daterange = this.getFormFields(InputType.daterange, fields);
    daterange.map(dr => {
      value[dr.key2] = moment(value[dr.key][1]).format(
        'YYYY-MM-DDTHH:mm:ss.000'
      );
      value[dr.key] = moment(value[dr.key][0]).format(
        'YYYY-MM-DDTHH:mm:ss.000'
      );
    });

    const groups = this.getFormFields(InputType.group, fields);
    groups.map(gp => {
      value[gp.key] = this.formValuesConversor(value[gp.key], gp.fields);
    });

    //Update monthYear typevalues
    const monthYear = this.getFormFields(InputType.monthPicker, fields);
    monthYear.map(my => {
      //Key is Month
      value[my.key] = parseInt(moment(value[my.key]).format('MM'), 10);
      //Key 2 is Year
      value[my.key2] = parseInt(moment(value[my.key]).format('YYYY'), 10);
    });

    return value;
  }

  /**
   *
   *  Returns a value suitable to the form input component
   * @param {*} model
   * @param {FormField} field
   * @returns {(string | string[] | Date | Date[] | number)}
   * @memberof FormComponent
   */
  modelToFormControlValueConverter(
    model: any,
    field: FormField
  ): string | string[] | Date | Date[] | number | number[] {
    switch (field.type) {
      case InputType.color:
        if (!(<string>model[field.key].startsWith('#')))
          model[field.key] = '#' + model[field.key];
        return model[field.key];
      case InputType.select:
        if (field.model_name && model[field.model_name]) {
          let value = null;
          if (field.templateOptions.multiSelect) {
            value = model[field.model_name].map(e => e.id);
          } else {
            value = model[field.model_name].id;
          }
          return value;
        } else {
          return model[field.key];
        }
      case InputType.checkbox:
        return model[field.key];
      case InputType.date:
        return moment(model[field.key]).isValid() ? model[field.key] : null;
      case InputType.number:
        return model[field.key];
      case InputType.input:
        return model[field.key];
      case InputType.switch:
        return model[field.key];
      case InputType.textarea:
        return model[field.key];
      case InputType.json:
        return model[field.key];
      case InputType.uploadImage:
        return model[field.key];
      case InputType.uploadFile:
        return model[field.key];
      case InputType.time:
        const obj = {};
        if (model[field.key]) {
          return new Date('1979-01-01T' + model[field.key]);
        } else {
          return null;
        }
      case InputType.daterange:
        if (
          model.hasOwnProperty(field.key) &&
          model.hasOwnProperty(field.key2)
        ) {
          return [new Date(model[field.key]), new Date(model[field.key2])];
        } else {
          return null;
        }
      case InputType.monthPicker:
        if (
          model.hasOwnProperty(field.key) &&
          model.hasOwnProperty(field.key2)
        ) {
          //Key is Month
          //Key 2 is Year
          return new Date(model[field.key2], model[field.key]);
        } else {
          return null;
        }
      case InputType.uploadImageGallery:
        if (field.model_name && model[field.model_name]) {
          return model[field.model_name].map(e => e.id);
        } else {
          return model[field.key];
        }
      default:
        return null;
    }
  }
}
