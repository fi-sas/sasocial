import { TemplateOptions } from '@fi-sas/backoffice/shared/components/form/interfaces/template-options.interface';

export enum InputType {
  transfer = 'transfer',
  json = 'json',
  group = 'group',
  input = 'input',
  select = 'select',
  number = 'number',
  checkbox = 'checkbox',
  switch = 'switch',
  date = 'date',
  daterange = 'daterange',
  time = 'time',
  color = 'color',
  textarea = 'textarea',
  uploadFile = 'uploadfile',
  uploadImage = 'uploadimage',
  uploadImageGallery = 'uploadimagegallery',
  multilanguage = 'multilanguage',
  arrayfield = 'arrayfield',
  monthPicker = 'monthPicker'
}

export interface FormField {
  key: string;
  key2?: string; // For date range input
  model_name?: string; // On input update in case of the modal name is different of the form name, happens on selects
  type: InputType;
  defaultValue?: any;
  fields?: FormField[];
  templateOptions: TemplateOptions;
  row: number;
  hidden?: boolean; // flag to condition field visibility
  span?: number;
  condition?: (form_value: any) => boolean;
}
