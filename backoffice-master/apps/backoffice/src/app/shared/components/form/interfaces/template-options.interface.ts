import { ValidatorFn } from '@angular/forms';
import { BaseRepositoryInterface } from '@fi-sas/backoffice/shared/repository/base-repository.interface';
import { OptionsModel } from '@fi-sas/backoffice/shared/components/form/interfaces/options-model.interface';

export interface TemplateOptions {
  label: string;
  name?: string;
  mask?: string; // A - for letters and 0 - for numbers
  tip?: string;
  maskSufix?: string;
  maskPrefix?: string;
  sufix?: string;
  prefix?: string;
  options?: OptionsModel[]; // for select input
  selectValueKey?: string; // for select input key for the value
  selectLabelKey?: string;// for select input key for the label
  validators?: ValidatorFn[];
  repository?:  BaseRepositoryInterface<any>; // for select input
  repositoryParams?: {}; // params for the request of the repository
  repositoryDynamicParams?: (form_value) => { [key: string]: string }; // params for the request of the repository
  multiSelect?: boolean; // for select input
  serverSearch?: boolean; // for select input
  searchKey?: string; // for select input server search
  max?: number // for input type number
  min?: number // for input type number
  step?: number // for input type number
  formatter?: (value: number) => string | number; // for input type number
  parser?: (value: any) => any; // for input type number
  disableDate?: (date) => boolean;
}
