import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadImageGalleryComponent } from './upload-image-gallery.component';

describe('UploadImageGalleryComponent', () => {
  let component: UploadImageGalleryComponent;
  let fixture: ComponentFixture<UploadImageGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadImageGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadImageGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
