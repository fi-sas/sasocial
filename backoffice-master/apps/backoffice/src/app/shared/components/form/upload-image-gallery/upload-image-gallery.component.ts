import { Validators, FormControl, FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import {FormField} from "@fi-sas/backoffice/shared/components/form/interfaces/form-field.interface";

@Component({
  selector: 'fi-sas-upload-image-gallery',
  templateUrl: './upload-image-gallery.component.html',
  styleUrls: ['./upload-image-gallery.component.less']
})
export class UploadImageGalleryComponent implements OnInit {

  @Input()
  formGroup: FormGroup = null;

  @Input()
  field: FormField;

  constructor() { }

  ngOnInit() {
  }

  getFormImages(): FormArray {
    return this.formGroup.get(this.field.key) as FormArray;
   }

  addImage() {
    this.getFormImages().push(new FormGroup({
      file_id: new FormControl(null, Validators.required)
    }));
  }

  closeImage(index: number) {
    this.getFormImages().removeAt(index);
  }
}
