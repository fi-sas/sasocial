import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { UiService, MessageType } from '@fi-sas/backoffice/core/services/ui-service.service';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { first } from 'rxjs/operators';

@Component({
    selector: 'fi-sas-image',
    templateUrl: './image.component.html',
    styleUrls: ['./image.component.less']
})
export class ImageComponent implements OnInit, OnChanges {

    @Input()
    height = 100;

    @Input()
    width = 100;

    @Input() file_id: number = null;
    isLoading = false;
    mediaObj: FileModel = null;
    mediaUrl: string = null;
    constructor(
        private resourceService: FiResourceService,
        private urlService: FiUrlService,
        private uiService: UiService,
    ) { }

    ngOnInit() {
    }

    ngOnChanges(changes: any) {
        if (changes.file_id) {
            this.file_id = changes.file_id.currentValue;
            this.loadImage();
        }
    }

    loadImage() {

        this.isLoading = true;
        this.resourceService
            .read<FileModel>(this.urlService.get('FILES_ID', { id: this.file_id }))
            .pipe(first())
            .subscribe(
                result => {
                    this.mediaObj = result.data[0];

                    if (this.mediaObj) {
                        this.mediaUrl = result.data[0].url;
                    }
                    this.isLoading = false;
                },
                err => {
                    this.uiService.showMessage(
                        MessageType.error,
                        'Não foi possivel carregar a imagem'
                    );
                    this.isLoading = false;
                }
            );
    }

}
