import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';

@Component({
  selector: 'fi-sas-initial-page',
  templateUrl: './initial-page.component.html',
  styleUrls: ['./initial-page.component.less']
})
export class InitialPageComponent implements OnInit {

  menu = [];
  constructor(private uiService: UiService) { }



  ngOnInit() {
   // this.menu = this.uiService.getSiderItems();
  }

}
