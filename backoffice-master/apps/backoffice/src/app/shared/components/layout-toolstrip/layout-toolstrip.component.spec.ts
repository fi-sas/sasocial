import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutToolstripComponent } from './layout-toolstrip.component';

describe('LayoutToolstripComponent', () => {
  let component: LayoutToolstripComponent;
  let fixture: ComponentFixture<LayoutToolstripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutToolstripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutToolstripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
