export interface ArrayField {
  label: string;
  /***
   *  Example:
   *  [ FEILD -->'a', VALUE --> 1]
   */
  where: any[];
}
