import { TagResult } from "../../tag/tag.component";

export interface ColorField {
  color: string;
}
