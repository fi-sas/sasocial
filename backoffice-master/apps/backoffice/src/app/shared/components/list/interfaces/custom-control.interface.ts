import { BehaviorSubject } from 'rxjs';
import { BaseRepositoryInterface } from '@fi-sas/backoffice/shared/repository/base-repository.interface';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
export enum CustomControlOption {
  SELECT = 'SELECT',
  SWITCH = 'SWITCH',
  CHECKBOX = 'CHECKBOX',
  RADIOGROUP = 'RADIOGROUP',
  DATE = 'DATE'
};

export interface CustomControl {
  name: string,
  label: string,
  type: CustomControlOption,
  values?: { [key: string]: string },
  repository?: BaseRepositoryInterface<any>
  searchOnServer?: boolean;
  searchKey?: string;
}
