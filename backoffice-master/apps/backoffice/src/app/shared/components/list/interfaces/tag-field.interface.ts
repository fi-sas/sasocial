import { TagResult } from "../../tag/tag.component";

export interface TagField {
  results: { [key: string]: TagResult };
  label: string;
}
