import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSelectInputComponent } from './list-select-input.component';

describe('ListSelectInputComponent', () => {
  let component: ListSelectInputComponent;
  let fixture: ComponentFixture<ListSelectInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSelectInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSelectInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
