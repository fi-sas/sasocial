import { debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { CustomControl } from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'fi-sas-list-select-input',
  templateUrl: './list-select-input.component.html',
  styleUrls: ['./list-select-input.component.less']
})
export class ListSelectInputComponent implements OnInit {
  value = 0;
  private searchSubject: BehaviorSubject<string> = new BehaviorSubject('');

  @Input()
  get ngModel() {
    return this.value;
  }

  set ngModel(value) {
    this.value = value;
  }

  @Output() ngModelChange = new EventEmitter();

  @Input() control: CustomControl = null;

  isLoading = false;

  constructor() {}

  ngOnInit() {
    if (this.control.repository) {
      this.control.values = {};

      if (this.control.searchOnServer) {
        if (!this.control.searchKey) {
          throw Error(
            `'searchKey' has to be defined on serverSearch equals true on custom control ${
              this.control.name
            }`
          );
        }

        this.searchSubject.asObservable().pipe(
          debounceTime(500),
          distinctUntilChanged(),
          tap(() => this.isLoading = true),
          switchMap(
            term => this.control.repository.list(1,10, null, null, { name: term })
          ),
          tap(() => this.isLoading = false)).subscribe( values => {
            this.control.values = {};
            values.data.map(value => {
              this.control.values[value.id] = value.name;
            });
          });
      } else {
        this.control.repository.list(1, -1).subscribe(values => {
          values.data.map(value => {
            this.control.values[value.id] = value.name;
          });
          this.control.values = {...this.control.values};
          this.isLoading = false;
        });
      }
    }
  }

  onSelectSearch(event: string) {
    this.searchSubject.next(event);
  }

  onChange(event) {
    this.ngModelChange.emit(event);
  }
}
