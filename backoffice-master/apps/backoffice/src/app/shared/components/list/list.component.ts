import { FiConfigurator } from '@fi-sas/configurator';
import {
  CustomControlOption,
  CustomControl
} from '@fi-sas/backoffice/shared/components/list/interfaces/custom-control.interface';
import { ArrayField } from './interfaces/array-field.interface';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { BaseRepositoryInterface } from '@fi-sas/backoffice/shared/repository/base-repository.interface';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { Subject } from 'rxjs';
import { ListService } from '@fi-sas/backoffice/shared/services/list.service';
import { TagField } from './interfaces/tag-field.interface';
import { UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { TranslationField } from './interfaces/translation-field.interface';

@Component({
  selector: 'fi-sas-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  CustomControlOption = CustomControlOption;

  /***
   * Array of strings for request related entities to be included in the data objects
   */
  @Input() withRelated: string[] = [];

  /***
   * Enable the Toolstrip
   */
  @Input() showToolstrip = true;

  /***
   * Enable the multiselect option on list (checkbox)
   */
  @Input() multiSelect = false;

  /***
   * Repository of the entity
   */
  @Input() repoService: BaseRepositoryInterface<any>;

  /***
   * Objecto with the keys of possible sort fields
   */
  @Input() sortKeys: { [key: string]: string } = {};

  /***
   * Object with the keys of possible search fields
   */
  @Input() searchKeys: { [key: string]: string } = {};

  /***
   * Object with the keys of that will be showing in the list
   */
  @Input()
  fieldKeys: {
    [key: string]: string | ArrayField | TagField | TranslationField;
  } = {};

  /***
   * Custom inputs for search
   */
  @Input() customSearch: CustomControl[] = [];

  /***
   * Object with values that will b always sent on the request
   */
  @Input() persistentFilter: { [key: string]: string } = {};

  @ViewChild('scroller', { static: false}) scroller: ElementRef<any>;
  @ViewChild(PerfectScrollbarDirective, { static: false})
  directiveRef?: PerfectScrollbarDirective;

  pageIndex = 1;
  pageSize = 10;
  total = 0;
  hasMore = true;

  customSearchValues: any[] = [];

  searchKey = null;
  searchValue = null;

  sortValue = null;
  sortKey = null;

  seletectedItem : any = null;
  data: any[] = [];
  loading = false;
  dataChange: Subject<any[]> = new Subject();

  checkboxSelectedItems = [];

  default_language_id = 1;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private uiService: UiService,
    private listService: ListService,
    private ref: ChangeDetectorRef,
    configurator: FiConfigurator
  ) {
    this.default_language_id = configurator.getOption('DEFAULT_LANG_ID', 1);
  }

  ngOnInit() {
    if (!this.repoService) {
      throw new Error('No service provided!');
    }

    this.customSearch.map(value => {
      this.customSearchValues[value.name] = null;
    });

    this.listService.itemUpdatedObservable().subscribe(value => {
      this.updateSelectedItem(value);
    });

    this.listService.itemDeletedObservable().subscribe(value => {
      this.deleteSelectedItem(value);
    });

    this.listService.clearSelectedItemsObservable().subscribe(value => {
      this.deselectAll();
    });
  }

  ngAfterViewInit(): void {
    this.dataChange.next(this.data);
    this.loadUntilScroll();
  }

  ngOnDestroy() {
    this.listService.updateSelectedItem(null);
    this.ref.detach();
  }

  loadUntilScroll() {
    if (this.scroller) {
      const hasScroll =
        this.scroller.nativeElement.scrollHeight >
        this.scroller.nativeElement.clientHeight;
      if (!hasScroll) {
        this.onScroll();
      }
    }
  }

  onScroll(event?: any) {
    if (this.loading) return;

    if (!this.hasMore) {
      //this.uiService.showMessage(MessageType.warning, 'Sem mais dados a apresentar');
      this.hasMore = false;
      this.loading = false;
      return;
    }

    this.searchData();
  }

  selectItem(item: any) {
    this.listService.updateSelectedItem(item);
    this.seletectedItem = item;
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }
  }

  searchData(reset = false) {
    if (this.loading) {
      return;
    }

    this.loading = true;

    if (reset) {
      this.pageIndex = 1;
      this.data = [];
      this.dataChange.next(this.data);
    }

    const params = {};

    if (this.searchKey) {
      params[this.searchKey] = this.searchValue;
    }

    if (this.withRelated.length > 0) {
      params['withRelated'] = this.withRelated.join();
    }

    this.customSearch.map(custom => {
      if (this.customSearchValues[custom.name]) {
        if (custom.type === 'DATE') {
          params[custom.name] = new Date(
            this.customSearchValues[custom.name]
          ).toISOString();
        } else {
          params[custom.name] = this.customSearchValues[custom.name];
        }
      }
    });

    if (this.persistentFilter) {
      const pfKeys = Object.keys(this.persistentFilter);
      pfKeys.map(key => {
        params[key] = this.persistentFilter[key];
      });
    }

    this.repoService
      .list(this.pageIndex, this.pageSize, this.sortKey, this.sortValue, params)
      .pipe(first())
      .subscribe(
        result => {
          this.total = result.link.total;

          if (result.data.length > 0) {
            this.pageIndex++;
            this.data = [...this.data, ...result.data];
            this.dataChange.next(this.data);

            if (!this.seletectedItem && this.data.length > 0) {
              this.selectItem(this.data[0]);
            }
          }

          this.hasMore = result.data.length >= this.pageSize;

          this.loading = false;
          if (!this.ref['destroyed']) {
            this.ref.detectChanges();
          }
        },
        err => {
          this.loading = false;
        }
      );
  }

  updateSelectedItem(item: any) {
      const index = this.data.findIndex(t => t.id === item.id);
      if (index >= 0) {
        this.data[index] = item;
        this.data = [...this.data];
        this.dataChange.next(this.data);
        if (!this.ref['destroyed']) {
          this.ref.detectChanges();
        }
      }
  }

  private deleteSelectedItem(item: any) {
    const index = this.data.findIndex(t => t.id === item.id);

    if (index > -1) {
      this.data.splice(this.data.findIndex(t => t.id === item.id), 1);
      this.data = [...this.data];
      this.dataChange.next(this.data);

      if (this.data[index]) {
        this.selectItem(this.data[index]);
      } else {
        if (this.data[index + 1]) {
          this.selectItem(this.data[index + 1]);
        } else {
          if (this.data[index - 1]) {
            this.selectItem(this.data[index - 1]);
          } else {
            this.selectItem(null);
          }
        }
      }

      this.checkboxSelectedClick();

      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }
    }
  }

  returnValueFromItem(key: string, item: any) {
    return this.derefrecursive(item, key);
  }

  returnValueFromArray(key: string, feildKey: ArrayField, item: any) {
    const deepKey = key.split('.');
    if (deepKey.length > 1) {
      const parent = item[deepKey[0]];
      if (parent) {
        if (parent.length > 0) {
          const whereKey = feildKey.where[0];
          const whereCond = feildKey.where[1];
          const result = parent.find(r => r[whereKey] === whereCond);

          return result ? result[deepKey[1]] : '-';
        } else {
          return '-';
        }
      } else {
        throw Error(`key ${key} dont exist on the object`);
      }
    } else {
      const whereKey = feildKey.where[0];
      const whereCond = feildKey.where[1];
      const result = item[key].find(r => r[whereKey] === whereCond);
      return result ? result : '-';
    }
  }

  returnValueFromTranslation(
    key: string,
    feildKey: TranslationField,
    item: any
  ) {
    const deepKey = key.split('.');
    if (deepKey.length > 1) {
      const parent = item[deepKey[0]];
      if (parent) {
        if (parent.length > 0) {
          const whereKey = 'language_id';
          const whereCond = this.default_language_id;
          const result = parent.find(r => r[whereKey] === whereCond);

          return result ? result[deepKey[1]] : '-';
        } else {
          return '-';
        }
      } else {
        throw Error(`key ${key} dont exist on the object`);
      }
    } else {
      const whereKey = 'language_id';
      const whereCond = this.default_language_id;
      const result = item[key].find(r => r[whereKey] === whereCond);
      return result ? result : '-';
    }
  }

  isStringFeild(item: any) {
    return typeof item === 'string' ? true : false;
  }

  isArrayFeild(item: any) {
    return item.label && item.where ? true : false;
  }

  isTranslationFeild(item: any) {
    return item.translation ? true : false;
  }

  isTagFeild(item: any) {
    return item.label && item.results ? true : false;
  }

  isColorFeild(item: any) {
    return item.color ? true : false;
  }

  colorItemHashtag(color: string) {
    if (!color.startsWith('#')) {
      color = '#' + color;
    }

    return color;
  }

  derefrecursive(obj: any, s: string, i?: number) {
    const s1 = s.split('.');
    if (i === undefined) i = 0;
    if (i < s1.length) return this.derefrecursive(obj[s1[i]], s, i + 1);
    return obj;
  }

  deselectAll() {
    this.checkboxSelectedItems = [];
    this.listService.updateSelectedList([]);
  }

  checkboxSelectedClick() {

    const checkedItems =
    this.checkboxSelectedItems
      .map((d, i) => d === true ? i : 0)
      .filter(d => d > 0)
      .map(d => this.data.find(da => da.id === d))
      .filter( d => d !== undefined);

    this.listService.updateSelectedList(checkedItems);

    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }
  }
}
