import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolstripButtonComponent } from './toolstrip-button.component';

describe('ToolstripButtonComponent', () => {
  let component: ToolstripButtonComponent;
  let fixture: ComponentFixture<ToolstripButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolstripButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolstripButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
