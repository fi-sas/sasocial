import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export enum ToolStripButtonType {
  WARNING = 'warning',SUCCESS = 'success',FAIL = 'fail'
}

@Component({
  selector: 'fi-sas-toolstrip-button',
  templateUrl: './toolstrip-button.component.html',
  styleUrls: ['./toolstrip-button.component.less']
})
export class ToolstripButtonComponent implements OnInit {
  @Input() permission = null;

  @Input() disabled = false;

  @Input() tooltip = null;

  @Input() iconType = null;

  @Input() type: ToolStripButtonType = null;

  @Output() onClick = new EventEmitter();

  @Input() isLoading = false;

  constructor() {}

  ngOnInit() {}

  color() {
    switch(this.type) {
      case ToolStripButtonType.WARNING :
        return '#BA4A00';
        break;
      case ToolStripButtonType.SUCCESS:
        return '#229954';
        break;
      case ToolStripButtonType.FAIL :
        return '#A93226'
        break;
      default:
        return null;
    }
  }

  emitEvent(event: any) {
    this.onClick.emit(event);
  }
}
