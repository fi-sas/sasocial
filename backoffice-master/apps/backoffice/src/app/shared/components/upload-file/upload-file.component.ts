import { Component, forwardRef, Input, OnInit, ElementRef, ViewChild } from '@angular/core';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { Observable } from 'rxjs';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { FiConfigurator } from '@fi-sas/configurator';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'fi-sas-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadFileComponent),
      multi: true
    }
  ]
})
export class UploadFileComponent implements OnInit, ControlValueAccessor {
  public _value;

  @ViewChild('upload_input', { static: false}) upload_input: ElementRef;

  @Input() disabled = false;

  // MAX SIZE in MB
  @Input() maxSize: number = null;

  @Input() types = '';

  @Input() category_id: number = null;

  @Input() language_id: number = null;

  @Input() isPublic = true;

  @Input() weight = 1;

  mediaObj: FileModel = null;
  mediaUrl = '';
  isLoading = false;
  uploading = false;
  isSavedOnServer = false;
  hasSaved = false;
  file: File = null;
  fileChangedEvent: any = '';
  isFileLoaded = false;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    private uiService: UiService,
    private resourceService: FiResourceService,
    private configurator: FiConfigurator,
    private urlService: FiUrlService
  ) {
    if (!this.language_id) {
      this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
    }

    if (!this.category_id) {
      this.category_id = this.configurator.getOption('DEFAULT_CATEGORY_ID');
    }
  }

  ngOnInit() {}

  get value() {
    return this._value;
  }

  set value(val) {
      if(!val){  
        this.isSavedOnServer = false;
        this.isFileLoaded = false;
        this.mediaObj = null;
        this.file = null;
      }
      this._value = val;
      this.onChange(this._value);
      this.onTouched();
  }

  fileChangeEvent(event) {
    this.isSavedOnServer = false;
    this.hasSaved = false;

    this.fileChangedEvent = event;
    const file: File = event.target.files[0];

    this.file = file;
    this.isFileLoaded = true;

    if (this.maxSize) {
      if (file.size >= (this.maxSize * 1024 * 1024)) {
        this.uiService.showMessage(MessageType.warning, `O tamanho não pode exceder os ${this.maxSize}mb`);
        this.file = null;
        this.isFileLoaded = false;
      }
    }
  }

  confirm() {
    this.isLoading = true;
    this.upload().subscribe(
      result => {
        this.mediaUrl = result.data[0].url;
        this.mediaObj = result.data[0];
        this.value = result.data[0].id;
        this.isLoading = false;
        this.hasSaved = true;
        this.isSavedOnServer = true;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  upload(): Observable<FileModel | any> {
    const that = this;
    return Observable.create(function(observer) {
      const formData = new FormData();

      formData.append('name', that.file.name);
      formData.append('weight', that.weight.toString());
      formData.append('public', String(that.isPublic));
      formData.append('file_category_id', String(that.category_id));
      formData.append('language_id', String(that.language_id));
      formData.append('file', that.file, that.file.name);

      that.uploading = true;
      // You can use any AJAX library you like
      that.resourceService
        .create(that.urlService.get('FILES'), formData, {
          // headers: { 'Content-Type': 'multipart/form-data' }
        })
        .pipe(first())
        .subscribe(
          value => {
            that.uploading = false;
            observer.next(value);
            that.uiService.showMessage(
              MessageType.success,
              'Ficheiro enviado com sucesso'
            );
          },
          err => {
            that.uploading = false;
            observer.error(err);
            that.uiService.showMessage(
              MessageType.error,
              'Ocorreu um erro ao enviar o ficheiro'
            );
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  deleteFile() {
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este imagem ?', 'Eliminar')
      .pipe(first())
      .subscribe(result => {
        if (result) {
          this.resourceService
            .delete(this.urlService.get('FILES_ID', { id: this.value }))
            .pipe(first())
            .subscribe(
              deleted => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Ficheiro eliminado com sucesso'
                );
                this.mediaObj = null;
                this.mediaUrl = null;
                this.isSavedOnServer = false;
                this.hasSaved = false;
                this.upload_input.nativeElement.value = '';
                this.value = null;

              },
              err => {
                this.uiService.showMessage(
                  MessageType.error,
                  'Ocorreu um erro ao eliminar o ficheiro'
                );
              }
            );
        }
      });
  }

  loadFileFromServer(id: number) {
    if (id) {
      this.isLoading = true;
      this.resourceService
        .read<FileModel>(this.urlService.get('FILES_ID', { id }))
        .pipe(first())
        .subscribe(
          result => {

            this.mediaObj = result.data[0];

            if (this.mediaObj.path === '') {
              this.mediaObj = null;
              this.value = null;
              this.isSavedOnServer = false;
              this.isLoading = false;
            } else {
              this.mediaUrl = result.data[0].url;
              this.isSavedOnServer = true;
              this.isLoading = false;
            }
          },
          err => {
            this.value = null;
            this._value = null;
            this.uiService.showMessage(
              MessageType.error,
              'Não foi possivel carregar o ficheiro'
            );
            this.isLoading = false;
          }
        );
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.loadFileFromServer(obj);

    if (obj !== undefined) {
      this.value = obj;
      this.onChange(this.value);
    }
  }
}
