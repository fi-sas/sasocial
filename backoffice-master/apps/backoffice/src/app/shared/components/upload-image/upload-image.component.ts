import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnInit,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FileModel } from '@fi-sas/backoffice/modules/medias/models/file.model';
import Cropper from 'cropperjs';
import { FiConfigurator } from '@fi-sas/configurator';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'fi-sas-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadImageComponent),
      multi: true
    }
  ]
})
export class UploadImageComponent implements OnInit, ControlValueAccessor {
  public _value;
  @ViewChild('image', { static: false}) image: ElementRef;

  @ViewChild('upload_input', { static: false}) upload_input: ElementRef;

  @Input() disabled = false;
  mediaObj: FileModel = null;
  @Input() aspectRatio: number = null;
  @Input() language_id: number = null;
  @Input() category_id: number = null;
  @Input() isPublic = true;
  cropper: Cropper;
  isModalVisible = false;
  @Input() weight = 1;
  name: string;
  mime_type: string;
  originalImage: string = null; // URL ORIGINAL IMAGE
  mediaUrl: string = null; // URL CROPPED IMAGE
  fileToUpload: Blob = null;
  isImageLoaded = false; // WHEN IMAGE IS LOADED O BROWSER
  isLoading = false; // WHEN IS SAVING ON THE SERVER
  imageChangedEvent: any = '';
  cropperReady = false;
  isCropped = false; // IF THE IMAGE AS ALREADY CROPPED
  hasSaved = false; // IF THE IMAGE AS ALREADY CROPPED
  isSavedOnServer = false; // WHEN THE IMAGE IS ALREADY SAVED ON THE SERVER
  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private uiService: UiService,
    private configurator: FiConfigurator,
    private ref: ChangeDetectorRef,
  ) {
    if (!this.language_id) {
      this.language_id = this.configurator.getOption('DEFAULT_LANG_ID');
    }

    if (!this.category_id) {
      this.category_id = this.configurator.getOption('DEFAULT_CATEGORY_ID');
    }
  }

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(this._value);
    this.onTouched();
  }

  ngOnInit() {
    if (this.mediaObj) {
      this.mediaUrl = this.mediaObj.url;
      this.isImageLoaded = true;
    }
  }

  fileChangeEvent(event: any): void {

    this.isCropped = false;
    this.isSavedOnServer = false;
    this.hasSaved = false;
    this.isImageLoaded = false;

    this.imageChangedEvent = event;
    const file: File = event.target.files[0];
    this.mime_type = file.type;
    this.weight = this.weight;
    this.name = file.name;

    const reader = new FileReader();
    reader.onload = (pe: ProgressEvent) => {
      this.mediaUrl = (<FileReader>pe.target).result.toString();
      this.originalImage = this.mediaUrl;
      this.isImageLoaded = true;
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  confirm() {
    if ((this.aspectRatio && this.isCropped) || !this.aspectRatio) {
      this.isLoading = true;
      this.upload().subscribe(
        result => {
          this.mediaObj = result.data[0];
          this.value = result.data[0].id;
          this.isLoading = false;
          this.isImageLoaded = false;
          this.hasSaved = true;
          this.isSavedOnServer = true;
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }

  upload(): Observable<FileModel | any> {
    const that = this;
    return Observable.create(function(observer) {
      const value: FormData = new FormData();
      value.append("file", that.fileToUpload, that.name);
      value.append("mime_type", that.mime_type);
      value.append("weight", that.weight.toString());
      value.append("file_category_id", that.category_id.toString());
      value.append("public", that.isPublic ? "true" : "false");
      value.append("filename", that.name);
      value.append("language_id", that.language_id.toString());

      that.resourceService
        .create(that.urlService.get('FILES'), value)
        .pipe(first())
        .subscribe(
          result => {
            that.uiService.showMessage(
              MessageType.success,
              'Upload efetuado com sucesso.'
            );
            observer.next(result);
          },
          err => {
            observer.error(err);
          },
          () => {
            observer.complete();
          }
        );
    });
  }

  showEditingModal() {
    this.isModalVisible = true;
    this.cropper = new Cropper(this.image.nativeElement, {
      aspectRatio: this.aspectRatio,
      initialAspectRatio: this.aspectRatio,
      autoCropArea: 1,
      autoCrop: true,
      ready(e) {
        if (!this.aspectRatio) {
          this.cropper.setCropBoxData({
            top: 0,
            left: 0,
            width: this.cropper.getImageData().width,
            height: this.cropper.getImageData().height
          });
        }
      },
      crop(e) {}
    });
  }

  confirmEditing() {
    this.cropper.getCroppedCanvas().toBlob((file) => {
      this.blobToBase64(file).then((base64)=>{
        this.mediaUrl = base64;
      } );
      this.fileToUpload = file;
      this.isCropped = true;
      this.cropper.destroy();
      this.cropperReady = false;
      this.isModalVisible = false;
    });//.toDataURL();
  }

  rejectEditing() {
    if (this.cropper) {
    this.cropper.destroy();
    this.cropperReady = false;
    this.isModalVisible = false;
    }
  }

 blobToBase64(blob): Promise<string> {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result.toString());
      reader.readAsDataURL(blob);
    });
  }

  loadImageFromServer(id: number) {
    if (id) {
      this.isLoading = true;
      this.resourceService
        .read<FileModel>(this.urlService.get('FILES_ID', { id }))
        .pipe(first())
        .subscribe(
          result => {
            this.mediaObj = result.data[0];
            this.mediaUrl = result.data[0].url;
            this.isSavedOnServer = true;
            this.isLoading = false;

            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
          },
          err => {
            this.uiService.showMessage(MessageType.error,'Não foi possivel carregar a imagem');
            this.mediaObj = null;
            this.mediaUrl = null;
            this.isSavedOnServer = false;
            this.isLoading = false;

            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
          }
        );
    } else {
            this.mediaObj = null;
            this.mediaUrl = null;
            this.isSavedOnServer = false;
            this.isLoading = false;
    }
  }

  deleteImage() {
    this.uiService
      .showConfirm('Eliminar', 'Pretende eliminar este imagem ?', 'Eliminar')
      .pipe(first())
      .subscribe(result => {
        if (result) {
          this.resourceService
            .delete(this.urlService.get('FILES_ID', { id: this.value }))
            .pipe(first())
            .subscribe(
              deleted => {
                this.uiService.showMessage(
                  MessageType.success,
                  'Imagem eliminada com sucesso'
                );
                this.isSavedOnServer = false;
                this.hasSaved = false;
                this.isCropped = false;
                this.isImageLoaded = false;
                this.mediaUrl = null;
                this.upload_input.nativeElement.value = ''
                this.value = null;
              },
              err => {
                this.uiService.showMessage(
                  MessageType.error,
                  'Ocorreu um erro ao eliminar a imagem'
                );
              }
            );
        }
      });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.loadImageFromServer(obj);

    if (obj !== undefined) {
      this.value = obj;
      this.onChange(this.value);
    }
  }
}
