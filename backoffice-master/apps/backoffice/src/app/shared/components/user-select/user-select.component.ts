import { Component, OnInit, ChangeDetectionStrategy, forwardRef, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { first, finalize, debounceTime, switchMap, tap, distinctUntilChanged } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Resource } from '@fi-sas/core';
import { UsersService } from '@fi-sas/backoffice/modules/users/modules/users_users/services/users.service';

@Component({
  selector: 'fi-sas-user-select',
  templateUrl: './user-select.component.html',
  styleUrls: ['./user-select.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSelectComponent),
      multi: true
    }
  ]
})
export class UserSelectComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @Input() returnID = true;

  @Input() profilesIds: number[] = [];

  value = null;

  searchChange$ = new BehaviorSubject('');
  isDisabled = false;
  loading = false;
  users: UserModel[] = [];
  optionListSubscrition: Subscription = null;
  constructor(
    private usersService: UsersService,
    private ref: ChangeDetectorRef,
  ) { }

  onSearch(value: string): void {
    //this.loading = true;
    this.searchChange$.next(value);
  }
  onChanged: any = () => { }
  onTouched: any = () => { }

  ngOnInit() {

    const optionList$: Observable<Resource<UserModel>> = this.searchChange$
      .asObservable()
      .pipe(
        tap(() => (this.loading = true)),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((search) => {
          const data = {
            withRelated: false,
            search,
            searchFields: 'name,student_number,email,identification,tin',
            sort: 'name'
          };

          if ((this.profilesIds || []).length) {
            data['profile_id'] = this.profilesIds;
          }

          return this.usersService.list(0, 20, null, null, data).pipe(first(), finalize(() => {
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
          }))
        }),
      );

    this.optionListSubscrition = optionList$.subscribe(results => {
      this.users = [...results.data];
      this.loading = false;
    });
    this.loading = false;
  }

  loadUser(user_id: number) {
    this.loading = true;
    this.usersService.read(user_id, {
      withRelated: false
    }).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(results => {
      this.users = [...results.data];
      this.value = user_id;
      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }

    });
  }

  ngOnDestroy() {
    this.ref.detach();
    this.optionListSubscrition.unsubscribe();
  }

  writeValue(obj: any): void {
    if (obj) {
      this.loadUser(obj);
    } else {
      this.value = obj;
    }
  }
  registerOnChange(fn: any): void {
    this.onChanged = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  valueChange() {
    if (!this.isDisabled) {
      //this.value = ;
      this.onChanged(this.value);
      this.onTouched();
    }

  }

}
