import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit, SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { NzToolTipComponent } from 'ng-zorro-antd';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective implements OnInit, OnChanges {

  @HostBinding('disabled') disable;
  @Input('buttonDisable') disabled;
  @Input() hasPermission: string | string[];
  private permited = true;

  constructor(
    private el: ElementRef,
    private authService: AuthService) {

  }

  ngOnInit(): void {

    if(this.hasPermission) {
      if (Array.isArray(this.hasPermission)) {
        this.disable = this.disabled;

        this.hasPermission.map(permission => {
          if (!this.authService.hasPermission(permission)) {
            this.permited= false;
            this.disable = !this.permited && this.disabled;
          }
        });
      } else {
        this.permited = this.authService.hasPermission(this.hasPermission);
        this.disable = !this.permited || this.disabled;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.disable = this.permited && this.disabled;
  }
}
