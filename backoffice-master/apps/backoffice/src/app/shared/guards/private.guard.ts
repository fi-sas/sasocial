import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '@fi-sas/backoffice/shared/services/auth.service';
import { Observable } from 'rxjs';
import {
  MessageType,
  UiService
} from '@fi-sas/backoffice/core/services/ui-service.service';
import { finalize, first, tap } from 'rxjs/operators';

@Injectable()
export class PrivateGuard implements CanActivate, CanActivateChild {
  objKey: string;

  constructor(
    private router: Router,
    private uiService: UiService,
    private authService: AuthService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.tokenVerification(route, state);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return Observable.of(!!this.authService.getToken());
  }

  tokenVerification(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return Observable.create(observable => {

          if (this.authService.isLogged) {

            // IF TOKEN IS VALID
            if (route.data.hasOwnProperty('scope')) {
              // IF HAS SCOPE PROPRIETY ON ROUTING
              const hasPermission = this.authService.hasPermission(
                route.data.scope
              );
              if (hasPermission) {
                // IF USER HAS THE SCOPE OF THE ROUTING
                observable.next(true);
              } else {
                // IF USER DONT HAVE THE SCOPE OF ROUTING
                this.router.navigate(['unauthorized']);
                observable.next(false);
              }
            } else {
              // IF IS ONLY CHECK OF VALID TOKEN
              observable.next(true);
            }
          } else {
            // IF TOKEN IS NOT VALID
            this.authService.logout();
            /*this.uiService.showMessage(
              MessageType.error,
              'A sua autenticação não é valida'
            );*/
            this.router.navigate(['auth', 'login'], {
              queryParams: { returnUrl: state.url }
            });
            observable.next(false);
          }
          observable.complete();
    });
  }
}
