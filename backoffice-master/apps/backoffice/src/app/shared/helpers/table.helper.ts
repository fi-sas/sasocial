import { TagResult } from './../components/tag/tag.component';
import { first, finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from './../../core/services/ui-service.service';
import { Repository } from './../repository/repository.class';
import * as moment from 'moment';
export interface Column {
  key: string,
  label: string,
  template?: (data: any) => string,
  translation?: boolean,
  sortKey?: string,
  sortable?: boolean,
  tag?: boolean | { [key: string]: TagResult },
  filters?: any[],
  showFilter?: boolean,
  filterMultiple?: boolean,
  onFilterChange?: (data: any, key: string) => void,
}
export class TableHelper {
  isAllDisplayDataChecked = false;
  isIndeterminate = false;
  mapOfCheckedId: { [key: string]: boolean } = {};
  numberOfChecked = 0;
  mapOfExpandData: { [key: string]: boolean } = {};
  pageIndex = 1;
  pageSize = 30;
  idKey = 'id';
  sortKey = 'id';
  sortValue = 'descend';
  loading = false;
  totalData = 0;
  displayData: any[] = [];
  data: any[] = [];
  filters: any = {};
  extraFilters: any = {};
  persistentFilters = {};
  columns: Column[] = [];
  repository: Repository<any> = null;
  constructor(
    public uiService: UiService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  initTableData(repository: Repository<any>) {
    this.repository = repository;
    if (!this.repository) {
      throw new Error('No service provided!');
    }
    this.uiService.setContentWrapperActive(false);
    this.searchData(true);
  }

  pagination(reset = false) {
    this.searchData(reset);
  }

  sort(sort: { key: string; value: string }): void {
    this.sortKey = sort.key;
    this.sortValue = sort.value;
    this.searchData(true);
  }

  searchData(reset = false) {
    this.loading = true;
    if (reset) {
      this.pageIndex = 1;
    }
    this.updateUrl();
    this.repository
      .list(this.pageIndex, this.pageSize, this.sortKey, this.sortValue,
        this.filterObject(), this.extraFiltersObject())
      .pipe(
        first(),
        finalize(() => this.loading = false)
      )
      .subscribe(res => {
        this.totalData = res.link.total;
        this.data = res.data;
      })
  }

  extraFiltersObject(): Object {
    const tempFilters = this.extraFilters;
    for (const propName in tempFilters) {
      if (tempFilters[propName] === null || tempFilters[propName] === undefined) {
        delete tempFilters[propName];
      }
      if (moment.isDate(tempFilters[propName])) {
        tempFilters[propName] = moment(tempFilters[propName]).format('YYYY-MM-DD');
      }
    }
    return tempFilters;
  }

  filterObject(): Object {
    const tempFilters = Object.assign(this.persistentFilters, this.filters);
    for (const propName in tempFilters) {
      if (tempFilters[propName] === null || tempFilters[propName] === undefined) {
        delete tempFilters[propName];
      }
      if (moment.isDate(tempFilters[propName])) {
        tempFilters[propName] = moment(tempFilters[propName]).format('YYYY-MM-DD');
      }
    }
    return tempFilters;
  }

  currentPageDataChange(event: any[]): void {
    this.displayData = event;
    this.refreshStatus();
  }

  refreshStatus(): void {
    this.numberOfChecked = this.data.filter(item => this.mapOfCheckedId[item[this.idKey]]).length;
    if (this.displayData.length > 0) {
      this.isAllDisplayDataChecked = this.displayData.every(item => this.mapOfCheckedId[item.id] === true);
      this.isIndeterminate = this.displayData.some(item => this.mapOfCheckedId[item.id] === true) &&
        !this.isAllDisplayDataChecked;
    } else {
      this.isAllDisplayDataChecked = false;
      this.isIndeterminate = false;
    }
  }

  checkAll(value: boolean): void {
    this.displayData.forEach(item => (this.mapOfCheckedId[item[this.idKey]] = value));
    this.refreshStatus();
  }

  updateUrl() {
    const queryParams = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
    };
    if (this.sortKey) {
      queryParams['sortKey'] = this.sortKey;
    }
    if (this.sortValue) {
      queryParams['sortValue'] = this.sortValue;
    }
    this.router.navigate([], {
      queryParams,
      relativeTo: this.activatedRoute
    });
  }

  clearSelected() {
    this.mapOfCheckedId = {};
  }

  getSelectedData(): any[] {
    return Object.keys(this.mapOfCheckedId).map(key => this.data.find(app => app[this.idKey] == key));
  }

  onColumnFilterChange(data, key) {
    this.filters[key] = data;
    this.searchData();
  }
}

/* HTML EXAMPLE
<div class="content-table">
  <nz-table class="table"
  #refTable
  [nzData]="data"
  (nzCurrentPageDataChange)="currentPageDataChange($event)"
  [nzLoading]="loading"
  [nzFrontPagination]="false"
  [(nzPageIndex)]="pageIndex"
  [nzShowSizeChanger]="true"
  [nzTotal]="totalData"
  [(nzPageSize)]="pageSize"
  (nzPageIndexChange)="pagination()"
  (nzPageSizeChange)="pagination(true)"
  >
    <thead (nzSortChange)="sort($event)" nzSingleSort>
      <tr class="table__header">
        <th
        nzShowCheckbox
        [(nzChecked)]="isAllDisplayDataChecked"
        [nzIndeterminate]="isIndeterminate"
        (nzCheckedChange)="checkAll($event)"
        ></th>
        <th nzShowExpand></th>
        <th
        *ngFor="let column of columns"
        [nzShowSort]="column.sortable"
        [nzSortKey]="column.sortKey ? column.sortKey : column.key"
        > {{ column.label }}</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>
      <ng-template ngFor let-data [ngForOf]="refTable.data">
        <tr>
          <td
            nzShowCheckbox
            [(nzChecked)]="mapOfCheckedId[data.id]"
            (nzCheckedChange)="refreshStatus()"
          ></td>
          <td nzShowExpand [(nzExpand)]="mapOfExpandData[data.id]" ></td>
          <td *ngFor="let column of columns" >
            <span *ngIf="!column.tag">{{ column.translation ? (data.translations | where: ['language_id', 3] )[0][column.key] : (data | flatObject)[column.key]}}</span>
            <span *ngIf="column.tag">
              <fi-sas-tag [value]="data[column.key]" [results]="column.tag"></fi-sas-tag>
            </span>
          </td>
          <td>
            <button nz-button nz-dropdown nzType="link" class="options_button" [nzDropdownMenu]="menu" nzPlacement="bottomRight"><i nz-icon nzType="dash" nzTheme="outline"></i></button>
            <nz-dropdown-menu #menu="nzDropdownMenu">
              <ul nz-menu>
                <li nz-menu-item *ngFor="let menuItem of menuItems" (click)="menuItem.action(data)">{{ menuItem.label }}</li>
              </ul>
            </nz-dropdown-menu>
          </td>
        </tr>
        <tr [nzExpand]="mapOfExpandData[data.id]">
          <td [attr.colspan]="columns.length + 3">
            <ng-container *ngTemplateOutlet="rowExpand;context : {rowData : data}"></ng-container>
          </td>
        </tr>
      </ng-template>
    </tbody>
  </nz-table>
</div>
*/
/* LESS EXAMPLE
@import "theme";
.content-table {
    margin: 8px;
    nz-table {
    }
    .table {
        &__header {
            th {
                background: white;
                color: var(--primary-6);
                font-weight: 700;
            }
            & > th:first-child {
                border-top-left-radius: 2px !important;
            }
            & > th:last-child {
                border-top-right-radius: 2px !important;
            }
        }
        tr {
            td {
                color: var(--text-color);
            }
            background: white !important;
        }
    }
    .options_button {
        color: var(--text-color);
        font-size: 22px;
    }
}
*/
/* COMPNENT EXAMPLE
 export class XXXX extends TableHelper  implements OnInit {
  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    uiService: UiService,
    private templateService: TemplatesService
  ) {
    super(uiService, router, activatedRoute);
  }
  ngOnInit() {
    this.columns.push({
      key: 'name',
      label: 'Nome',
      sortable: true
    })
    this.initTableData(this.templateService);
  }
  deleteTemplate(id: number) {
    this.uiService.showConfirm('Eliminar modelo', 'Pretende eliminar este modelo', 'Eliminar').pipe(first()).subscribe(confirm => {
      if(confirm) {
        this.templateService.delete(id).pipe(first()).subscribe(result => {
          this.searchData();
          this.uiService.showMessage(MessageType.success, 'Modelo eliminado com sucesso');
        });
      }
    });
  }
}
*/