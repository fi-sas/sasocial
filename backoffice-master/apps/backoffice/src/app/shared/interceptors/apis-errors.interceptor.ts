import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FiConfigurator } from '@fi-sas/configurator';
import { MessageType, UiService } from '@fi-sas/backoffice/core/services/ui-service.service';
import { NzModalService } from 'ng-zorro-antd';
import { NoConnectionComponent } from '@fi-sas/backoffice/shared/components/no-connection/no-connection.component';
import { AuthService } from '../services/auth.service';

@Injectable()
export class ApisErrorsInterceptor implements HttpInterceptor {
  constructor(
    private uiService: UiService,
    private modalService: NzModalService,
    private router: Router,
    private config: FiConfigurator,
    private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let noError = false;

    if (request.headers.has('no-error')) {
      noError = true;
      request = request.clone({ headers: request.headers.delete('no-error') });
    }

    // if the call fails it retry until 5 times before explodes
    return next.handle(request).pipe(catchError(error => {

      if (error instanceof HttpErrorResponse) {
        if (!navigator.onLine) {
          const modal = this.modalService.create({
            nzWidth: 300,
            nzMaskClosable: false,
            nzClosable: false,
            nzContent: NoConnectionComponent,
            nzFooter: [{
              label: 'Verificar',
              onClick: (componentInstance) => {
                componentInstance.verifyConnection();
              }
            }]
          });
        } else {
          const httpErrorCode = error.status;

          if(error && error.error && error.error.errors) {
            const errors = error.error.errors;

            errors.map(apiError => {
              let message = 'An error has occurred.';
              if(apiError.translations && apiError.translations.find(trans => trans.language_id == 3)) {
                message = apiError.translations.find(trans => trans.language_id == 3).message;
              } else {
                message = apiError.message;
              }

              if (!noError) {
                this.uiService.showMessage(MessageType.error, message, {})
              }
            });
          }

          if(noError) {
            switch (httpErrorCode) {
              case 401:
                this.authService.logout();
                this.router.navigateByUrl('/auth/login');
                break;
              case 403:
                this.router.navigateByUrl('/unauthorized');
                break;
              case 503:
                this.showError(this.config.getOption<string>('ERROR_TRANSLATOR.SERVICE_DOWN', 'Serviço temporariamente indisponível'));
                break;
              case 0:
                this.showError(this.config.getOption<string>('ERROR_TRANSLATOR.SERVICE_DOWN', 'Serviço temporariamente indisponível'));
                break;
              default:
            }
          }
        }

        return throwError(error);
      }
    }));
  }

  private showError(message: string) {

    this.uiService.showMessage(MessageType.error, message, {});
  }
}
