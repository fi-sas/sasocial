import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  AuthService
} from '@fi-sas/backoffice/shared/services/auth.service';
import * as moment from 'moment';
import {  filter, switchMap } from 'rxjs/operators';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    if (this.authService.getAuth() && this.authService.getAuth().token && !request.headers.has('no-token')) {

      if(moment(this.authService.getAuth().expires_in).subtract(5, 'minutes').isBefore() && !this.authService.isRefreshing) {
        return this.authService.refreshToken().pipe(switchMap(() => {
          const cloned = request.clone({
            headers: request.headers.append('Authorization', `Bearer ${this.authService.getAuth().token}`)
          });
          return next.handle(cloned);
        }));
      } else {
        return this.authService.$isRefreshing.pipe(
          filter((v) => v === false), switchMap(() => {
          const cloned = request.clone({
            headers: request.headers.append('Authorization', `Bearer ${this.authService.getAuth().token}`)
          });

          return next.handle(cloned);
        }));

      }

    } else {
      return next.handle(request);
    }
  }
}
