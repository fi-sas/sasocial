export class Assignature<M> {
  status: AssignatureStatus;
  data: M[];
  error: ErrorAPI[];
}

export enum AssignatureStatus {
  SUCCESS = 'success',
  FAIL = 'fail',
  ERROR = 'error'
}

export class ErrorAPI {
  code: number;
  message: string;
}
