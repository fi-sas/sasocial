export class CCSamlRequest {
  id: string;
  context: string;
  relayState: string;
  entityEndpoint: string;
  type: string;
}
