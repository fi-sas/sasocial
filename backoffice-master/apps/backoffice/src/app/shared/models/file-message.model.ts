export class FileMessageModel {
  upload: Message;
  delete: Message;
  load: Message;
  invalid: Message;
}

class Message {
  success?: string;
  error: string;
}
