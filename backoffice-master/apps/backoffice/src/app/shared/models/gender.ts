export type Gender = 'F' | 'M' | 'U';

export const GenderOptions: { value: Gender, label: string }[] = [
  { value: 'F', label: 'Feminino' },
  { value: 'M', label: 'Masculino' },
  { value: 'U', label: 'Outro' },
];
