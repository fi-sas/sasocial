import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterMonth'
})
export class FilterMonthPipe implements PipeTransform {

  transform(monthArray: any, month_number: number): any {
    return monthArray.find(month => month.value === month_number);
  }

}
