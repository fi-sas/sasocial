import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPaymentModel'
})
export class FilterPaymentModelPipe implements PipeTransform {

  transform(paymentModelArray: any, payment_model_name: string): any {
    const pm = paymentModelArray.find(payment_model => payment_model.value === payment_model_name)
    return pm ? pm : { label: '-'};
  }


}
