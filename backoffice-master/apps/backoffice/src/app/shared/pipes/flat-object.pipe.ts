import { flatten } from '@fi-sas/utils';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flatObject'
})
export class FlatObjectPipe implements PipeTransform {

  
  transform(value: object, args?: any): any {
    return flatten(value);
  }

}
