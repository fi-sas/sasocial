import { Pipe } from '@angular/core';

import { GetFromObjectsArrayPipe } from './get-from-objects-array.pipe';
import { Gender, GenderOptions } from '../models/gender';

@Pipe({
  name: 'genderBeautify'
})
export class GenderBeautifyPipe extends GetFromObjectsArrayPipe {
  transform(key: Gender): string {
    return super.transform(key, 'value', GenderOptions, 'label');
  }
}
