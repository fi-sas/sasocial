import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'HourConvert',
})
export class HourConvertPipe implements PipeTransform {
  transform(hour: number): string {
    if (isNaN(hour)) {
      return '';
    }

    if (!hour) {
      return '0';
    }

    let minutes: string;
    let hourString = hour.toString();
    const hourSplitter = hourString.split('.');

    if (hourSplitter.length === 2) {
      minutes = hourSplitter[1].length === 2 ? hourSplitter[1] : `${hourSplitter[1]}0`;
      return `${hourSplitter[0]}h${minutes}`;
    }

    return `${hourSplitter[0]}h`;
  }
}

