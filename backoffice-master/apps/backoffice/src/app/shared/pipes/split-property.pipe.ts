import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitProperty',
})
export class SplitPropertyPipe implements PipeTransform {
  transform(data: any, path: string): any {
    const paths = path.split('.');
    for (let i = 0; i < paths.length - 1; i++) data = data[paths[i]];
    return data[paths[paths.length-1]];
  }
}
