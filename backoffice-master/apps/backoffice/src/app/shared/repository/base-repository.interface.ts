import { Observable } from 'rxjs';
import { Resource } from '@fi-sas/core';

export interface BaseRepositoryInterface<T> {
  list(pageIndex: number, pageSize: number, sortKey?: string, sortValue?: string, params?: {}): Observable<Resource<T>>;
  read(id : number, params?: {}): Observable<Resource<T>>;
  create(entity: T): Observable<Resource<T>>;
  update(id: number, entity: T): Observable<Resource<T>>;
  patch(id: number, entity: {}): Observable<Resource<T>>;
  delete(id: number): any;
}
