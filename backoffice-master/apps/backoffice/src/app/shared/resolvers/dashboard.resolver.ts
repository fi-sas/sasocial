import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { ConfigurationGeralService } from '@fi-sas/backoffice/shared/services/configuration.service';
import { first, tap } from 'rxjs/operators';

@Injectable()
export class DashboardResolver
  implements Resolve<Observable<Resource<any>>> {
  constructor(private configurationGeralService: ConfigurationGeralService) {}

  resolve() {
    localStorage.removeItem("configuraction_active_menu_bo");
    return this.configurationGeralService.list(1, -1).pipe(
        first(), 
        tap(results => {
            localStorage.setItem("configuraction_active_menu_bo", JSON.stringify(results.data[0].services));
          })
    );
  }
}
