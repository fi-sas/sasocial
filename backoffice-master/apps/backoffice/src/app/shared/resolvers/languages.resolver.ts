import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { EMPTY } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { LanguageModel } from '@fi-sas/backoffice/modules/configurations/models/language.model';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';

@Injectable()
export class LanguagesResolver
  implements Resolve<Observable<Resource<LanguageModel>>> {
  constructor(private languagesService: LanguagesService) { }

  resolve() {
    return this.languagesService.list(0, -1, "order", "ascend").catch(() => {
      return EMPTY;
    });
  }
}
