import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { EMPTY } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { GroupModel } from '@fi-sas/backoffice/modules/configurations/models/group.model';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Injectable()
export class ListAllGroupsResolver
  implements Resolve<Observable<Resource<GroupModel>>> {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
    ) {}

  resolve() {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    return this.resourceService
    .list<GroupModel>(this.urlService.get('CONFIGURATIONS.GROUPS'), {
      params
    })
    .pipe(first())
    .catch(() => {
      return EMPTY;
    });
  }
}
