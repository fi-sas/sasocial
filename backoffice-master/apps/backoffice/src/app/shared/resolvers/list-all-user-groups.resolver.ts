import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { EMPTY } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Resource } from '@fi-sas/core';
import { UserGroupModel } from '@fi-sas/backoffice/modules/users/modules/users-groups/models/user-group.model';
import { UserGroupsService } from '@fi-sas/backoffice/modules/users/modules/users-groups/services/user-groups.service';

@Injectable()
export class ListAllUserGroupsResolver
  implements Resolve<Observable<Resource<UserGroupModel>>> {
  constructor(private userGroupsService: UserGroupsService) {}

  resolve() {
    return this.userGroupsService.list(0, -1).catch(() => {
      return EMPTY;
    });
  }
}
