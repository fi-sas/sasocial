import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { FiShareModule } from '@fi-sas/share';
import { FiCoreModule, FiResourceService, FiUrlService } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FiShareModule, FiCoreModule, HttpClientTestingModule],
      providers: [FiResourceService, FiUrlService]
    });
  });

  it(
    'should be created',
    inject([AuthService], (service: AuthService) => {
      expect(service).toBeTruthy();
    })
  );
});
