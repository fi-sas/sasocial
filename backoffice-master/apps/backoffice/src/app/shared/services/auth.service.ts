import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginSuccessModel } from '@fi-sas/backoffice/components/login/model/login-success.model';
import { UserModel } from '@fi-sas/backoffice/modules/users/modules/users_users/models/user.model';
import { filter, finalize, first, tap } from 'rxjs/operators';
import { SSOUrl } from '../models/sso-url.model';
import { HttpHeaders } from '@angular/common/http';
import { CCSamlRequest } from '../models/cc-saml-request.model';
import * as moment  from 'moment';

export class AuthObject {
  user: UserModel;
  scopes: string[];
  token: string;
  expires_in: Date;
  auth_date: Date;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _authObj: AuthObject;

  isLogged = false;
  isLoggedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  isRefreshing = false;
  $isRefreshing: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  userSubject: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(
    new UserModel()
  );

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
  ) {
    this.changeIsLogged(false);

  }

  /***
   * Check's the credentials and add to the storage the login data
   * @param email
   * @param password
   */
  login(email: string, password: string): Observable<AuthObject> {
    return new Observable<AuthObject>(observer => {
      this.resourceService
        .create<LoginSuccessModel>(this.urlService.get('LOGIN', { id: 1 }), {
          email,
          password
        },
        {
          //headers: new HttpHeaders().append("no-error", "true"),
          headers: new HttpHeaders().append("no-token", "true"),
          withCredentials: true,
        }).pipe(first())
        .subscribe(
          value => {

            this.createAuthObject(value.data[0]).pipe(first()).subscribe(
              (res) => {
                observer.next(this.getAuth());
                observer.complete();
              },
              (err) => {
                observer.error(err);
                observer.complete();
              }
            );

          },
          error => {
            observer.error(error);
          }
        );
    });
  }


  private setRefreshingToken(isRefreshing: boolean) {
    this.isRefreshing = isRefreshing;
    this.$isRefreshing.next(this.isRefreshing);
  }

  refreshToken(): Observable<any> {
    return new Observable(observer => {
      this.setRefreshingToken(true);
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("REFRESH_TOKEN", { type: 'BO'}),
          {},
          {
            headers: new HttpHeaders().append("no-error", "true").append("no-token", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {

            this.createAuthObject(value.data[0]).pipe(first()).subscribe(
              (res) => {
                observer.next(res);
                observer.complete();
              },
              (err) => {
                observer.error(err);
                observer.complete();
              }
            );
          },
          (err) => {
            this.logout();
            observer.next(false);
            observer.complete();
            this.setRefreshingToken(false);
          }
        );
    });
  }

    /***
   * Creates the auth objects after authentication
   */
  createAuthObject(result: LoginSuccessModel): Observable<AuthObject> {
    return new Observable<AuthObject>(observer => {
      const authObj = new AuthObject();

      authObj.auth_date = new Date();
      authObj.token = result.token;
      if (result.expires_in_ms) {
        authObj.expires_in = new Date( Date.now() + result.expires_in_ms);
      } else {
        authObj.expires_in = new Date(result.expires_in);
      }
      authObj.scopes = (this._authObj && this._authObj.scopes) ? this._authObj.scopes :  [];
      this._authObj = authObj;

      // ALERT TOKEN REFRESH
      this.setRefreshingToken(false);

      this.resourceService
      .read<UserModel>(this.urlService.get('LOGIN_USER'), {
      }).pipe(first())
      .subscribe(
        value1 => {

          this.setUser(value1.data[0]);
          this.changeIsLogged(true);
          this.userSubject.next(authObj.user);

            this.validateToken().pipe(first()).subscribe(() => {
              observer.next(authObj);
              observer.complete();
            }, (error) => {
              observer.error(error);
              observer.complete();
            });
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  private changeIsLogged(isLogged: boolean) {
    this.isLogged = isLogged;
    this.isLoggedSubject.next(this.isLogged);
  }

  getIsLoggedObservable() {
    return this.isLoggedSubject.asObservable();
  }

  /***
   * Remove the data of the login
   */
  logout() {
    this._authObj = null;
    this.userSubject.next(null);

    this.changeIsLogged(false);

    this.resourceService
    .read<any>(
      this.urlService.get("LOGOUT", { type: 'BO'}),
      {
        headers: new HttpHeaders().append("no-error", "true"),
        withCredentials: true,
      }
      )
      .pipe(first())
      .subscribe((value) => {});
  }

  /***
   * Return the user data of the login data
   */
  getAuth(): AuthObject {
    return this._authObj;
  }

  /***
   * Return the user data of the login data
   */
  getUser(): UserModel {
    if (this.getAuth() !== null) {
      return this.getAuth().user;
    } else {
      return null;
    }
  }

  /***
   * Update the user object
   * @param user
   */
  setUser(user: UserModel): UserModel {
    if (this.getAuth() !== null) {
      this._authObj.user = user;
      return this.getAuth().user;
    } else {
      return null;
    }
  }

  /***
   * Return the scopes of the user
   */
  getScopes(): string[] {
    if (this.getAuth() !== null) {
      return this.getAuth().scopes;
    } else {
      return null;
    }
  }

  /***
   * Update the scopes of the user
   */
  setScopes(scopes: string[]): string[] {
      this.getAuth().scopes = scopes;
      return scopes;
  }

  /***
   * Return true if the user have the scope send by param
   * if send only scope microservice:entity checks if have all
   * permissions if send microservice:entity:(crud operation) check if has
   * this permission
   * @param scope
   */
  hasPermission(scope: string): boolean {
    if(!scope) {
      return false;
    }
    
    if(this.getScopes()) {
      return this.getScopes().includes(scope);
    } else {
      return false;
    }
  }

  /***
   * Return a Observable of the user object
   */
  getUserObservable(): Observable<UserModel> {
    return this.userSubject.asObservable();
  }

  /***
   * Returns the token of the user logged
   */
  getToken(): string {
     return this.getAuth() ? this.getAuth().token : null;
  }

  /***
   * Validate the token and updates the scopes of the user
   */
  validateToken(): Observable<boolean> {
    return Observable.create(observable => {


      if(moment(this.getAuth().expires_in).isBefore() && !this.isRefreshing) {
        this.refreshToken().pipe(first()).subscribe();
      }

      return this.$isRefreshing.pipe(filter(v => v === false)).subscribe(() => { // WAITING FOR THE NEW TOKEN

        if (!this.getToken()) {
          observable.next(false);
          observable.complete();
        } else {
        this.resourceService
          .create<{ user: UserModel; scopes: string[] }>(
            this.urlService.get('VALIDATE_TOKEN', {}), { token: this.getToken() }
          )
          .pipe(first())
          .subscribe(
            value => {
              if (value.data.length === 0) {
                observable.next(false);
                observable.complete();
              } else {
                this.setUser(value.data[0].user);
                this.setScopes(value.data[0].scopes);
                observable.next(true);
                observable.complete();
              }
            },
            err => {
              observable.error(err);
              observable.complete();
            }
          );
        }
      });

    });
  }

    /***
   * Get url to reddirect to the login page of the idp
   */
  ssoLoginUrl(): Observable<Resource<SSOUrl>> {
    return this.resourceService.read<SSOUrl>(this.urlService.get('SSO_URL')).pipe(first());
  }

  ssoCCLoginSAMLRequest(): Observable<Resource<CCSamlRequest>> {
    return this.resourceService.read<CCSamlRequest>(this.urlService.get('SSO_CC_URL')).pipe(first());
  }
}
