import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Repository } from '@fi-sas/backoffice/shared/repository/repository.class';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ConfigurationGeralService extends Repository<any>{

  constructor(resourceService: FiResourceService,
              urlService: FiUrlService) {
    super(resourceService, urlService);
    this.entities_url = 'CONFIGURATIONS.GERAL';
  }


  getLocalStorageService(id: number){
    let data = JSON.parse(localStorage.getItem('configuraction_active_menu_bo'));
    if(data.find((service) => service.id == id) == null || data.find((service) => service.id == id) == undefined){
      return [];
    }
    return data.find((service) => service.id == id);
  }

  getLocalStorageTotalConfiguration(){
    let data = JSON.parse(localStorage.getItem('configuraction_active_menu_bo'));
    return data;
  }

  getSectionsOrder(): Observable<Resource<any>> {
    let params = new HttpParams();
    params = params.set('sort', 'priority');
    params = params.set('withRelated', 'services');
    return this.resourceService
      .list(this.urlService.get('CONFIGURATIONS.SECTIONS'),{
        params
      });
  }

}


