import { distinctUntilChanged, debounce } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private selectedItemSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private itemUpdatedSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private itemDeleteSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private selectedItemsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
  private selectedItems: any[] = [];
  private clearSelectedItemsSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor() { 
  }

  /**
   * Observable triggered when the selected item is changed
   */
  selectedItemObservable(): Observable<any> {
    return this.selectedItemSubject.pipe(distinctUntilChanged());
  }

  /**
   * Changes the item selected in the list
   * @param item
   */
  updateSelectedItem(item: any) {
    this.selectedItemSubject.next(item);
  }

  /**
   * Observable triggered when a item is update in the list
   */
  itemUpdatedObservable(): Observable<any> {
    return this.itemUpdatedSubject.asObservable();
  }

  /**
   * Updates a item loaded in the list
   * @param item
   */
  itemUpdated(item: any) {
    this.itemUpdatedSubject.next(item);
  }

  /**
   * Observable triggered whem a element is deleted
   */
  itemDeletedObservable(): Observable<any> {
    return this.itemDeleteSubject.asObservable();
  }

  /**
   * Deletes a item from the list
   * @param item
   */
  deleteItem(item: any) {
    this.itemDeleteSubject.next(item);
  }

  /** Returns a observable with the selected items of the list  */
  selectedItemsObservable(): Observable<any[]> {
    return this.selectedItemsSubject.pipe(distinctUntilChanged());
  }

  /** Updates the list of selected items */
  updateSelectedList(items: any []) {
    this.selectedItems = items;
    this.selectedItemsSubject.next(items);
  }

  /** REturns the last values of the selected items */
  getSelecteItems() {
    return this.selectedItems;
  }

  /** Clear the selected items list */
  clearSelectedItems() {
    this.selectedItems = [];
    this.clearSelectedItemsSubject.next(true);
  }

  /** Returns  the observable for the action to clean the list*/
  clearSelectedItemsObservable() {
    return this.clearSelectedItemsSubject.asObservable();
  }
}
