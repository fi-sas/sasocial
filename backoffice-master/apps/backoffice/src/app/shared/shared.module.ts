import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LOCALE_ID, NgModule } from '@angular/core';
import { NgZorroAntdModule, NzAutocompleteModule, NZ_I18N, pt_PT } from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
import localePt from '@angular/common/locales/pt';

import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';
import { NgArrayPipesModule, NgBooleanPipesModule, NgMathPipesModule, NgObjectPipesModule } from 'angular-pipes';
import { NgxMaskModule } from 'ngx-mask';
import * as moment from 'moment';

import { AutoUploadFileComponent } from './components/auto-upload-file/auto-upload-file.component';
import { BO_CONFIGURATION } from '@fi-sas/backoffice/app.config';
import { DashboardResolver } from './resolvers/dashboard.resolver';
import { FiConfiguratorModule, OPTIONS_TOKEN } from '@fi-sas/configurator';
import { FilterMonthPipe } from './pipes/filter-month.pipe';
import { FilterPaymentModelPipe } from './pipes/filter-payment-model.pipe';
import { FiShareModule } from '@fi-sas/share';
import { FlatObjectPipe } from './pipes/flat-object.pipe';
import { FormArrayfieldComponent } from './components/form/form-arrayfield/form-arrayfield.component';
import { FormComponent } from './components/form/form.component';
import { FormGroupComponent } from './components/form/form-group/form-group.component';
import { FormInputComponent } from './components/form/form-input/form-input.component';
import { FormJsonfieldComponent } from './components/form/form-jsonfield/form-jsonfield.component';
import { FormMultilanguageComponent } from './components/form/form-multilanguage/form-multilanguage.component';
import { FormSelectComponent } from './components/form/form-select/form-select.component';
import { FormTransferFieldComponent } from './components/form/form-transfer-field/form-transfer-field.component';
import { GenderBeautifyPipe } from './pipes/gender-beautify.pipe';
import { GetFromObjectsArrayPipe } from './pipes/get-from-objects-array.pipe';
import { GroupsService } from '@fi-sas/backoffice/modules/configurations/services/groups.service';
import { HasPermissionDirective } from './directives/has-permission.directive';
import { HasPermissionPipe } from './pipes/has-permission.pipe';
import { HourConvertPipe } from './pipes/hour-convert.pipe';
import { ImageComponent } from './components/image/image.component';
import { InitialPageComponent } from './components/initial-page/initial-page.component';
import { LanguagesResolver } from '@fi-sas/backoffice/shared/resolvers/languages.resolver';
import { LanguagesService } from '@fi-sas/backoffice/modules/configurations/services/languages.service';
import { LayoutToolstripComponent } from '@fi-sas/backoffice/shared/components/layout-toolstrip/layout-toolstrip.component';
import { ListAllGroupsResolver } from '@fi-sas/backoffice/shared/resolvers/list-all-groups.resolver';
import { ListAllUserGroupsResolver } from '@fi-sas/backoffice/shared/resolvers/list-all-user-groups.resolver';
import { ListComponent } from './components/list/list.component';
import { ListSelectInputComponent } from './components/list/list-select-input/list-select-input.component';
import { NoConnectionComponent } from './components/no-connection/no-connection.component';
import { PageNotFoundComponent } from '@fi-sas/backoffice/components/page-not-found/page-not-found.component';
import { SplitPropertyPipe } from './pipes/split-property.pipe';
import { TagComponent } from './components/tag/tag.component';
import { ToolstripButtonComponent } from './components/toolstrip-button/toolstrip-button.component';
import { TranslationPipe } from './pipes/translation.pipe';
import { TruncatePipe } from '@fi-sas/backoffice/shared/pipes/truncate.pipe';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { UploadImageComponent } from './components/upload-image/upload-image.component';
import { UploadImageGalleryComponent } from './components/form/upload-image-gallery/upload-image-gallery.component';
import { UserGroupsService } from '../modules/users/modules/users-groups/services/user-groups.service';
import { UserSelectComponent } from './components/user-select/user-select.component';

registerLocaleData(localePt, 'pt');
moment.locale('pt');

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  imports: [
    CommonModule,
    FiConfiguratorModule,
    FiShareModule,
    FormsModule,
    NgArrayPipesModule,
    NgBooleanPipesModule,
    NgMathPipesModule,
    NgObjectPipesModule,
    NgxMaskModule,
    NgZorroAntdModule,
    NzAutocompleteModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    AutoUploadFileComponent,
    FilterMonthPipe,
    FilterPaymentModelPipe,
    FlatObjectPipe,
    FormArrayfieldComponent,
    FormComponent,
    FormGroupComponent,
    FormInputComponent,
    FormJsonfieldComponent,
    FormMultilanguageComponent,
    FormSelectComponent,
    FormTransferFieldComponent,
    FormTransferFieldComponent,
    GenderBeautifyPipe,
    GetFromObjectsArrayPipe,
    HasPermissionDirective,
    HasPermissionPipe,
    HourConvertPipe,
    ImageComponent,
    InitialPageComponent,
    LayoutToolstripComponent,
    ListComponent,
    ListSelectInputComponent,
    NoConnectionComponent,
    PageNotFoundComponent,
    SplitPropertyPipe,
    TagComponent,
    ToolstripButtonComponent,
    TranslationPipe,
    TruncatePipe,
    UploadFileComponent,
    UploadImageComponent,
    UploadImageGalleryComponent,
    UserSelectComponent,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-Pt' },
    { provide: NZ_I18N, useValue: pt_PT },
    { provide: OPTIONS_TOKEN, useValue: BO_CONFIGURATION },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    DashboardResolver,
    GroupsService,
    LanguagesResolver,
    LanguagesService,
    ListAllGroupsResolver,
    ListAllUserGroupsResolver,
    UserGroupsService,
  ],
  exports: [
    AutoUploadFileComponent,
    FiConfiguratorModule,
    FilterMonthPipe,
    FilterPaymentModelPipe,
    FiShareModule,
    FlatObjectPipe,
    FormComponent,
    FormJsonfieldComponent,
    FormsModule,
    GenderBeautifyPipe,
    GetFromObjectsArrayPipe,
    HasPermissionDirective,
    HasPermissionPipe,
    HourConvertPipe,
    ImageComponent,
    InitialPageComponent,
    LayoutToolstripComponent,
    ListComponent,
    NgArrayPipesModule,
    NgBooleanPipesModule,
    NgMathPipesModule,
    NgObjectPipesModule,
    NgxMaskModule,
    NgZorroAntdModule,
    PageNotFoundComponent,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    RouterModule,
    SplitPropertyPipe,
    TagComponent,
    ToolstripButtonComponent,
    TranslationPipe,
    TruncatePipe,
    UploadFileComponent,
    UploadImageComponent,
    UploadImageGalleryComponent,
    UserSelectComponent,
  ],
  entryComponents: [NoConnectionComponent],
})
export class SharedModule {}
