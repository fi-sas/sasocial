export class FetchFromObject {
  static fetchFromObject(obj, prop) {
    if (typeof obj === 'undefined') {
      return false;
    }

    const _index = prop.indexOf('.');
    if (_index > -1) {
      return this.fetchFromObject(
        obj[prop.substring(0, _index)],
        prop.substr(_index + 1)
      );
    }

    return obj[prop];
  }
}
