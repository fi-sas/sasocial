import { AbstractControl, FormGroup } from '@angular/forms';

export const IBAN_REGEX = /^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/;

export function compareTwoFieldsValidation(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.match) {
      // return if another validator has already found an error on the matchingControl
      return;
    }
    if (!matchingControl.value) return;
    if (control.value == matchingControl.value) {
      matchingControl.setErrors({ match: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function trimValidation(
  control: AbstractControl
): { [key: string]: boolean } | null {
  if (control.value) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { required: true };
  }
}

export function ibanValidator(
  control: AbstractControl
): { [s: string]: boolean } {
  if (control.value && !control.value.match(IBAN_REGEX)) {
    return { invalidIban: true };
  }
}

export function validNIF(
  control: AbstractControl
): { [key: string]: boolean } | null {
  if (control.value) {
    const nif = control.value.toString();

    const regexp = new RegExp(/[0-9]{9}/);

    if (!regexp.test(nif)) {
      return { invalidNIF: true };
    }

    const firstChar = nif[0];
    if (
      firstChar === '1' ||
      firstChar === '2' ||
      firstChar === '5' ||
      firstChar === '6' ||
      firstChar === '8' ||
      firstChar === '9'
    ) {
      let checkDigit = parseInt(firstChar) * 9;
      for (let i = 2; i <= 8; ++i) {
        checkDigit += parseInt(nif[i - 1]) * (10 - i);
      }

      checkDigit = 11 - (checkDigit % 11);
      if (checkDigit >= 10) {
        checkDigit = 0;
      }

      if (checkDigit.toString() === nif[8]) {
        return null;
      }
    }

    return { invalidNIF: true };
  }
}

export function validNumber(
  control: AbstractControl
): { [key: string]: boolean } | null {
  if (control.value) {
    const number = control.value.toString();
    const regexp = new RegExp(/\d{6,14}$/);

    console.log(number, '->', regexp.test(number));

    if (!regexp.test(number)) {
      return { invalidPhone: true };
    }

    return null;
  }
}
