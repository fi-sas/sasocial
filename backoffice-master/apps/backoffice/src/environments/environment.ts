// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const gateway_url = 'http://localhost:4200';

export const environment = {
  production: false,
  version: require('../../../../package.json').version,
  institute: 'DEV',
  hasSSO: true,
  version_check_url: gateway_url.concat('/backoffice/assets/version.json'),
  sasocial_api_url: gateway_url + '/api/sasocial',
  auth_api_url: gateway_url + '/api/authorization',
  conf_api_url: gateway_url + '/api/configuration',
  media_api_url: gateway_url + '/api/media',
  media_url: gateway_url + '/media',
  communication_api_url: gateway_url + '/api/communication',
  alimentation_api_url: gateway_url + '/api/alimentation',
  //alimentation_api_url: 'http://0.0.0.0:7080/api',
  bus_api_url: gateway_url + '/api/bus',
  private_accommodation_api_url: gateway_url + '/api/private_accommodation',
  accommodation_api_url: gateway_url + '/api/accommodation',
  infrastructure_api_url: gateway_url + '/api/infrastructure',
  u_bike_api_url: gateway_url + '/api/u_bike',
  queue_api_url: gateway_url + '/api/queue',
  sport_api_url: gateway_url + '/api/sport',
  calendar_api_url: gateway_url + '/api/calendar',
  current_account_api_url: gateway_url + '/api/current_account',
  payments_api_url: gateway_url + '/api/payments',
  notifications_api_url: gateway_url + '/api/notifications',
  gamification_api_url: gateway_url + '/api/gamification',
  social_support_api_url: gateway_url + '/api/social_scholarship',
  volunteering_api_url: gateway_url + '/api/volunteering',
  mentoring_api_url: gateway_url + '/api/mentoring',
  reports_api_url: gateway_url + '/api/reports',
  health_api_url: gateway_url + '/api/health',
  emergency_fund_api_url: gateway_url + '/api/emergency_fund',
  reports_url: gateway_url + '',
  measurements_monitorization_api_url:
    gateway_url + '/api/measurements_monitorization',
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
