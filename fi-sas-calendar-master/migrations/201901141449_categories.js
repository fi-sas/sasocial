module.exports.up = async db => db.schema.createTable('category', (table) => {
  // category database table
  table.increments();
  table.string('color', 6).notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
})
  // category translations database table
  .createTable('category_translation', (table) => {
    table.increments();
    table.integer('category_id').notNullable().unsigned().references('id')
      .inTable('category');
    table.integer('language_id').notNullable().unsigned();
    table.string('name', 120).notNullable();
    table.string('description', 500).notNullable();
  });


module.exports.down = async db => db.schema.dropTable('category_translation').dropTable('category');

module.exports.configuration = { transaction: true };
