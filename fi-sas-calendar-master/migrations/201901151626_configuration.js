module.exports.up = async db => db.schema.createTable('configuration', (table) => {
  table.increments();
  table.string('key', 120).notNullable();
  table.string('value', 250).notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique('key');
});

module.exports.down = async db => db.schema.dropTable('configuration');

module.exports.configuration = { transaction: true };
