module.exports.up = async db => db.schema.createTable('form', (table) => {
  table.increments();
  table.string('name', 255).notNullable();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
})
  .createTable('form_element', (table) => {
    table.increments();
    table.integer('form_id').notNullable().unsigned().references('id')
      .inTable('form');
    table.enu('type', ['Text', 'Numeric', 'Radio', 'DropDown', 'MultipleDropDown', 'Checkbox']);
    table.integer('required').notNullable();
    table.string('name', 255).notNullable();
    table.string('caption', 255).notNullable();
    table.text('values');
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  });


module.exports.down = async db => db.schema
  .dropTable('form_element')
  .dropTable('form');

module.exports.configuration = { transaction: true };
