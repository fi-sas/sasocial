module.exports.up = async db => db.schema.createTable('event', (table) => {
  table.increments();
  table.integer('service_id').notNullable();
  table.integer('category_id').notNullable();
  table.datetime('publish_start_date').notNullable();
  table.datetime('publish_end_date');
  table.specificType('notifications_enabled', 'tinyint', 1).notNullable();
  table.integer('notification_target_id');
  table.datetime('notification_start_date');
  table.integer('number_of_notifications');
  table.enu('notification_recurrence', ['None', 'Daily', 'Weekly', 'Monthly', 'Annually']);
  table.float('longitude', 10, 6).notNullable();
  table.float('latitude', 10, 6).notNullable();
  table.string('address', 250).notNullable();
  table.string('address_no', 25).notNullable();
  table.string('city', 250).notNullable();
  table.specificType('allow_interest', 'tinyint', 1).notNullable();
  table.specificType('allow_registration', 'tinyint', 1).notNullable();
  table.integer('max_registrations');
  table.specificType('registration_approval_needed', 'tinyint', 1);
  table.integer('registration_form_id').unsigned().references('id').inTable('form');
  table.float('registration_fee').defaultTo(0);
  table.integer('registration_tax_id');
  table.string('registration_code', 120);
  table.specificType('cancelled', 'tinyint', 1).notNullable();
  table.enu('status', ['Pending', 'Approved', 'Rejected']);
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
})
  .createTable('event_recurrence', (table) => {
    table.increments();
    table.integer('event_id').unsigned().references('id').inTable('event');
    table.datetime('start_date').notNullable();
    table.datetime('end_date');
    table.enu('recurrence', ['None', 'Daily', 'Weekly', 'Monthly', 'Annually']);
    table.string('repeat_month', 2);
    table.string('repeat_day', 2);
    table.string('repeat_week', 2);
    table.string('repeat_weekday', 2);
  })
  .createTable('event_translation', (table) => {
    table.increments();
    table.integer('event_id').unsigned().references('id').inTable('event');
    table.integer('language_id').notNullable().unsigned();
    table.string('name', 120).notNullable();
    table.string('description', 500).notNullable();
    table.string('url', 500).notNullable();
  })
  .createTable('event_interest', (table) => {
    table.increments();
    table.integer('event_id').unsigned().references('id').inTable('event');
    table.integer('user_id').notNullable();
    table.unique(['event_id', 'user_id']);
  })
  .createTable('event_media', (table) => {
    table.increments();
    table.integer('event_id').unsigned().references('id').inTable('event');
    table.integer('file_id');
  })
  .createTable('event_contact', (table) => {
    table.increments();
    table.integer('event_id').unsigned().references('id').inTable('event');
    table.enum('type', ['Email', 'Phone']).notNullable();
    table.string('value');
  });

module.exports.down = async db => db.schema.dropTable('event_contact').dropTable('event_media').dropTable('event_interest')
  .dropTable('event_translation')
  .dropTable('event_recurrence')
  .dropTable('event');


module.exports.configuration = { transaction: true };
