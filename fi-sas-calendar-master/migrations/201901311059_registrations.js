module.exports.up = async db => db.schema.createTable('registration', (table) => {
  table.increments();
  table.integer('event_id').notNullable().unsigned().references('id')
    .inTable('event');
  table.integer('user_id').notNullable().unsigned();
  table.enu('status', ['Pending', 'Approved', 'Rejected']);
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
})
  .createTable('registration_answer', (table) => {
    table.increments();
    table.integer('registration_id').notNullable().unsigned().references('id')
      .inTable('registration');
    table.integer('element_id').notNullable().unsigned().references('id')
      .inTable('form_element');
    table.string('value', 255).notNullable();
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  });


module.exports.down = async db => db.schema
  .dropTable('registration_answer')
  .dropTable('registration');

module.exports.configuration = { transaction: true };
