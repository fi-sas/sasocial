module.exports.up = async db => db.schema.table('event', (table) => {
  table.string('notification_alert_type_key', 120).after('notification_target_id');
});

module.exports.down = async db => db.schema.table('event', (table) => {
  table.dropColumn('notification_alert_type_key');
});

module.exports.configuration = { transaction: true };
