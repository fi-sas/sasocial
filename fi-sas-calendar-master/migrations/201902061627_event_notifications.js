module.exports.up = async db => db.schema.createTable('event_notification', (table) => {
  table.increments();
  table.integer('event_id').notNullable().unsigned().references('id')
    .inTable('event');
  table.integer('target_id').unsigned();
  table.string('alert_type_key', 255).notNullable();
  table.datetime('date').notNullable();
  table.text('data');
  table.integer('success').notNullable();
  table.text('response');
});

module.exports.down = async db => db.schema.dropTable('event_notification');

module.exports.configuration = { transaction: true };
