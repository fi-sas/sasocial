module.exports.up = async db => db.schema.table('event', (table) => {
  table.datetime('start_date').notNullable().defaultTo(db.raw('CURRENT_TIMESTAMP'));
  table.datetime('end_date');
  table.enu('recurrence', ['None', 'Daily', 'Weekly', 'Monthly', 'Annually']).defaultTo('None');
  table.string('repeat_month', 2);
  table.string('repeat_day', 2);
  table.string('repeat_weekday', 2);
}).dropTable('event_recurrence');

module.exports.down = async db => db.schema.table('event', (table) => {
  table.dropColumn('start_date');
  table.dropColumn('end_date');
  table.dropColumn('recurrence');
  table.dropColumn('repeat_month');
  table.dropColumn('repeat_day');
  table.dropColumn('repeat_weekday');
}).createTable('event_recurrence', (table) => {
  table.increments();
  table.integer('event_id').unsigned().references('id').inTable('event');
  table.datetime('start_date').notNullable();
  table.datetime('end_date');
  table.enu('recurrence', ['None', 'Daily', 'Weekly', 'Monthly', 'Annually']);
  table.string('repeat_month', 2);
  table.string('repeat_day', 2);
  table.string('repeat_week', 2);
  table.string('repeat_weekday', 2);
});

module.exports.configuration = { transaction: true };
