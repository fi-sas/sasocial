import express from 'express';
import path from 'path';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import log4js from 'log4js';
import cron from 'node-cron';

import setValidators from './custom_validations';
import apiComposerValidations from './middleware/apiComposerValidations';
import responseEnhancer from './middleware/response-formatter';
import requestParameters from './middleware/request-parameters';
import { errorHandler, logErrors } from './middleware/error-handlers';
import tokenInfo from './middleware/tokenInfo';
import internalRequest from './middleware/internal-request';
import logger from './logger';
import sendEventNotifications from './helpers/eventNotifications';

import requireAuth from './middleware/auth';
import eventsRouter from './routes/events';
import categoriesRouter from './routes/categories';
import registrationFormsRouter from './routes/registration_forms';
import registrationsRouter from './routes/registrations';
import configurationsRouter from './routes/configurations';

import { NotFoundError } from './errors';

// Swagger Specs
import linkSpecs from './config/components/schemas.link.spec.json';
import parametersSpecs from './config/components/parameters.spec.json';
import responsesSpecs from './config/components/responses.spec.json';
import securitySchemesSpecs from './config/components/securitySchemes.bearerAuth.json';
import securitySpecs from './config/security.json';


//    __________  ____  _   __       ______  ____ _____
//   / ____/ __ \/ __ \/ | / /      / / __ \/ __ ) ___/
//  / /   / /_/ / / / /  |/ /  __  / / / / / __  \__ \
// / /___/ _, _/ /_/ / /|  /  / /_/ / /_/ / /_/ /__/ /
// \____/_/ |_|\____/_/ |_/   \____/\____/_____/____/

// Every 15 minutes, check the notifications that need to be sent
cron.schedule('*/15 * * * *', async () => sendEventNotifications());


//     __  ________    ____  ____  ____  _____________________  ___    ____
//    /  |/  / ___/   / __ )/ __ \/ __ \/_  __/ ___/_  __/ __ \/   |  / __ \
//   / /|_/ /\__ \   / __  / / / / / / / / /  \__ \ / / / /_/ / /| | / /_/ /
//  / /  / /___/ /  / /_/ / /_/ / /_/ / / /  ___/ // / / _, _/ ___ |/ ____/
// /_/  /_//____/  /_____/\____/\____/ /_/  /____//_/ /_/ |_/_/  |_/_/
setValidators();

/**
 * Routes
 */
const app = express();

// Use logged middleware to log all requests
app.use(log4js.connectLogger(logger, { level: 'auto' }));

// Set default render engine
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

// Add CORS headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Language-ID, X-Language-Acronym');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD');
  next();
});

// Helmet helps you secure your Express apps by setting various HTTP headers.
// It's not a silver bullet, but it can help!
// https://github.com/helmetjs/helmet
app.use(helmet());

/**
 * Use response enhancer (formatter)
 * Use request parameters to access ordering, filters and pagination configurations
 */
app.use(responseEnhancer());
app.use(requestParameters());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: '5mb' }));
app.use(express.static(path.join(__dirname, '..', 'public')));

/**
 * Use internal request detection
 */
app.use(internalRequest);

// API composer validations -- used for validating resources from other MS
app.use(apiComposerValidations);
app.use(tokenInfo);

/*
 * Assign Routes to Application
 */
app.use('/api/v1/events', requireAuth, eventsRouter);
app.use('/api/v1/event-categories', requireAuth, categoriesRouter);
app.use('/api/v1/registration-forms', requireAuth, registrationFormsRouter);
app.use('/api/v1/registrations', requireAuth, registrationsRouter);
app.use('/api/v1/configurations', requireAuth, configurationsRouter);

// Usage of auth middleware:
// Up top: import requireAuth from './middleware/auth'
//
// app.use('/users', requireAuth, usersRouter);
//
// Inside your routes, you will have available in req.authInfo the information
// decoded from the token


/*
 * Swagger documentation route
 */
const swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'FI@SAS Calendar API',
      version: '0.0.0',
      description: `Microservice API for the Calendar FI@SAS project.<br><br>
        Allows the creation and management of (possibly recurring) Events, as well as the management
        of Event Categories and Registration Forms.<br><br>
        This microservice is enhanced with a set of Configurations endpoints to manage the configurations in use.
        `,
    },
    components: {
      schemas: {
        Link: linkSpecs,
      },
      parameters: parametersSpecs,
      responses: responsesSpecs,
      securitySchemes: securitySchemesSpecs,
    },
    security: securitySpecs,
    tags: [
      {
        name: 'Events',
        description: 'Management of the events in the calendar.<br><b>Scope</b> <code>calendar:events</code>',
      },
      {
        name: 'Registrations',
        description: 'Managing of the registrations in Events.<br><b>Scope</b> <code>calendar:registrations</code>',
      },
      {
        name: 'Registration Forms',
        description: 'Management of the forms that users have to fill to register in Events.<br><b>Scope</b> <code>calendar:registration-forms</code>',
      },
      {
        name: 'Event Categories',
        description: 'Management of the Event Categories.<br><b>Scope</b> <code>calendar:event-categories</code>',
      },
      {
        name: 'Configurations',
        description: 'General configurations for the calendar.<br><b>Scope</b> <code>calendar:configurations</code>',
      },
    ],
  },
  apis: ['./routes/*'],
});

delete swaggerSpec.swagger;
swaggerSpec.openapi = '3.0.n';

swaggerSpec.components.schemas = Object.assign(swaggerSpec.components.schemas, require('./config/components/schemas.errors.spec.json'));

swaggerSpec.components.responses = require('./config/components/responses.spec.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// catch 404 and forward to error handler
app.use(() => {
  throw new NotFoundError();
});

app.use(logErrors);
app.use(errorHandler);

export default app;
