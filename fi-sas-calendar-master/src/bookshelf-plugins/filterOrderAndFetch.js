/**
 * filterOrderAndFetch bookshelf plugin
 *
 * Extends bookshelf Model with filterOrderAndFetch method.
 * filterOrderAndFetch receives parameters and options arguments. The first is used to gather the
 * filters, ordering and pagination information to be applied to the model querying process. The
 * second argument is directly passed to fetch method.
 */
/* eslint-disable no-param-reassign, func-names */


module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Filters, orders and fetches all or paginated data of the current model.
     * Receives parameters object with filters, ordering and pagination properties to apply
     * to the model. The second options argument is sent to fetchAll or fetchPage method.
     *
     * @param {Object} parameters Parameters to be applied.
     * @param {Array=} parameters.filters Array of filters to be applied (each filter contains
     * field (string) and value (string) properties).
     * @param {Array=} parameters.ordering Array of ordering parameters to be applied (each
     * parameter contains field (string) and order ('ASC' or 'DESC') properties).
     * @param {Array=} parameters.withRelated Array of requested relations to be included in the
     * fetch options.
     * @param {Object|boolean=} parameters.pagination Optinal pagination object with limit and
     * offset properties.
     * @param {Object=} opts Options of fetch method
     *
     * @returns {Promise} Returned by fetchAll or fetchPage methods
     */
    filterOrderAndFetch(parameters, opts = {}) {
      const filters = parameters.filters || [];
      const ordering = parameters.ordering || [];
      const pagination = parameters.pagination || false;
      const joins = parameters.joins || [];
      const groupBys = parameters.groupBys || [];
      const withRelatedOpts = { withRelated: parameters.withRelated };

      /*
       * Apply filters from query parameters (supporting multiple or single value)
       */
      filters.forEach((filter) => {
        const { field, value } = filter;
        if (value instanceof Array) {
          const num = value.filter(i => isNaN(i)).length === 0;
          // If numeric element, use where IN
          if (num) {
            this.where(field, 'IN', value);
          } else {
            // Otherwise, use where LIKE connected by OR operator
            this.where((q) => {
              value.forEach((v) => {
                q.orWhere(field, 'LIKE', v);
              });
            });
          }
        } else {
          this.where(field, 'LIKE', value);
        }
      });

      /*
       * Apply joins
       */
      joins.forEach((join) => {
        this.query((qb) => {
          qb.leftJoin(`${join.table} as ${join.alias}`, join.condition);
        });
      });

      /*
       * Apply ordering
       */
      ordering.forEach((orderingParameter) => {
        this.orderBy(orderingParameter.field, orderingParameter.order);
      });

      /*
       * Apply group by clauses
       */
      groupBys.forEach((gb) => {
        this.query(qb => qb.groupBy(gb));
      });

      /*
       * Gather fetch options (from received opts, pagination and withRelated options)
       */
      const fetchOptions = Object.assign(withRelatedOpts, pagination, opts);

      /*
       * Fetch paginated or all data
       */
      return pagination
        ? this.fetchPage(fetchOptions)
        : this.fetchAll(fetchOptions);
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
