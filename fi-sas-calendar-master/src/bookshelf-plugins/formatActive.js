/**
 * formatActive bookshelf plugin
 *
 * Extends bookshelf Model with formatActive and parseActive methods that formats and parses
 * "active" field, and the more generic methods formatBooleans and parseBooleans that do the same
 * for the array of received fields.
 */
/* eslint-disable no-param-reassign, func-names */

module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Formats received attributes, turning boolean (string) active value into a bit value.
     * Example: { "active": "true" } formatted into { "active": 1 }
     *
     * @param {Object} attributes
     * @returns {Object} Attributes with formatted active field
     */
    formatActive(attributes) {
      return this.formatBooleans(attributes, ['active']);
    },

    /**
     * Formats received attributes, turning boolean (string) values of the received fields
     * into bit values.
     * Example: { "bool": "true" } formatted into { "bool": 1 }
     *
     * @param {Object} attributes
     * @param {Array} fields
     * @returns {Object} Attributes with formatted active field
     */
    formatBooleans(attributes, fields = []) {
      const formattedAttributes = attributes;
      fields.forEach((field) => {
        if (['true', true, 'false', false].includes(attributes[field])) {
          formattedAttributes[field] = (formattedAttributes[field] === true || formattedAttributes[field] === 'true') ? 1 : 0;
        }
      });

      return formattedAttributes;
    },

    /**
     * Parses received response, turning active value from bit to boolean.
     * Example: { "active": 1 } formatted into { "active": true }
     *
     * @param {Object} attributes
     * @returns {Object} Attributes with formatted active field
     */
    parseActive(response) {
      return this.parseBooleans(response, ['active']);
    },

    /**
     * Parses received response, turning values of the received fields from bit to boolean.
     * Example: { "bool": 1 } formatted into { "bool": true }
     *
     * @param {Object} attributes
     * @param {Array} fields
     * @returns {Object} Attributes with formatted active field
     */
    parseBooleans(response, fields = []) {
      fields.forEach((field) => {
        if ([1, 0].includes(response[field])) {
          response[field] = (response[field] === 1);
        }
      });

      return response;
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
