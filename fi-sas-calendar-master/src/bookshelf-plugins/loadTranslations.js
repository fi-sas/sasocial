/**
 * loadTranslations bookshelf plugin
 *
 * Extends bookshelf Model with a loadTranslations method to be called before fetching data.
 * loadTranslations method uses the received models and parameters to include in withRelated option
 * the appropriate translation models. The method requires the existence of translations - which
 * means that it will either filter the current model by rows with translations or change the
 * withRelated loading query of related models to filter by rows with translataions.
 * The received models to translate may include filter fields - in this case, translations will
 * be filtered according to its filterable fields and the filters present in parameters.
 *
 * Usage:
 * 1 - Simply require translations for 'modelA' and its related 'modelB':
 *   const parameters = { withRelated: 'modelB' };
 *   ModelA.forge()
 *    .loadTranslations(['modelA', 'modelB'], parameters)
 *    .fetchAll(parameters);
 * // This will load 'translations' into modelA and modelB items. If a related modelB does not have
 * // translations, it will not be returned.
 *
 * 2 - Require translations for 'modelA' and its related 'modelB', with possible filters on
 * 'active' field of modelA and 'name' and 'title' fields of modelA translations:
 *   req.parameters.validate(['active'] + ['name', 'title'], ['id'], ['modelB']);
 *   ModelA.forge()
 *    .loadTranslations([{ name: 'modelA', filters: ['name', 'title'] }, 'modelB'], req.parameters)
 *    .filterOrderAndFetch(req.parameters)
 *
 * Notes:
 * It is assumed that model names match table names. It is also assumed that translation tables use
 * model+'_translation' name and reference the model 'id' key in a model+'_id' field.
 *
 * Known limitation: load of translations for related models in a *-n relationship (example:
 * 'categories' instead of 'category') are currently not supported.
 *
 */
/* eslint-disable no-param-reassign, func-names */

import _ from 'lodash';
import got from 'got';

module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Returns a function to filter a received query, requiring the received model's
     * (possibly filtered) translations.
     *
     * Expects model to be referenced by a 'model_translation' table via 'model_id' field.
     *
     * @param {string} model Name of model's database table
     * @param {Array} translationsFilters Array of filters to apply ({field, value} objects)
     * @private
     */
    filterAndRequireTranslation(model, translationsFilters) {
      return (qb) => {
        if (translationsFilters && translationsFilters.length) {
          // May be optimized - count from translations where filters and model_id = model.id > 0
          const subquery = bookshelf.knex.select(`${model}_id`).from(`${model}_translation`);
          translationsFilters.forEach((filter) => {
            const { field, value } = filter;
            if (_.isArray(value)) {
              subquery.whereIn(field, value);
            } else {
              subquery.where(field, 'LIKE', value);
            }
          });
          qb.whereIn(`${model}.id`, subquery);
        }

        return qb;
      };
    },

    /**
     * Talks to MS Configuration to find the active languages.
     *
     * @returns {Array} Returns an array with the current active languages.
     */
    async fetchActiveLanguages() {
      // Fetch active languages, to be used if no language id was provided
      try {
        const result = await got(`${process.env.MS_CONFIG_ENDPOINT}api/v1/languages/?active=true`);
        if (result.statusCode === 200) {
          const body = JSON.parse(result.body);
          const { data } = body;
          if (data instanceof Array) {
            return data;
          }
        }
        return [];
      } catch (err) {
        console.log('Could not fecth active languages!', err);
      }

      return [];
    },

    /**
     * Includes required translations of the received models if they are currently in use.
     * Received models which are not included in parameters.withRelated and do not match the
     * current model table name are discarded.
     * Models array argument may contain model names or objects with name and optional filters
     * properties.
     * When a model object with filters value is received, parameters.filters will be used and
     * updated so that filters are applied in the model's translation table.
     * If language ID is defined in received parameters, translations will be filtered accordingly,
     * otherwise, all translations will be loaded.
     * Received parameters.withRelated is updated to include translations models and to only load
     * models with translations.
     *
     * Expects received model names to match its database table, and its translations table name
     * to be 'model_translation' using 'model_id' foreign key.
     *
     * @param {Array} models Array of model table names which include (require) translations or
     * model object containing name and optional filters property
     * @param {Object} parameters Request parameters to use and update
     * @param {number=} languageId Language id to be used
     */
    async loadTranslations(models, parameters, languageId) {
      const withRelated = parameters.withRelated || [];
      const withRelatedTranslations = [];
      let filters = parameters.filters || [];
      let languages = [];
      if (!languageId) {
        languages = await this.fetchActiveLanguages();
      }

      /*
       * Iterate over received models and include translations of the models in use
       */
      models.forEach((model) => {
        const modelName = (typeof model === 'string') ? model : model.name;
        const modelFilters = (model.filters) ? model.filters : [];

        let relationship = false;
        const relatedTranslation = {};

        /*
         * Gather filters for translations
         */
        const translationsFilters = filters.filter(f => modelFilters.includes(f.field));
        if (languageId) {
          translationsFilters.push({ field: 'language_id', value: languageId });
        } else {
          translationsFilters.push({ field: 'language_id', value: languages.map(l => l.id) });
        }

        /*
         * Update filters to contain unused filters only
         */
        filters = filters.filter(f => !modelFilters.includes(f.field));

        /*
         * Start including process:
         * Check wether requesting translations for current model
         */
        const currentModel = modelName === this.tableName;
        if (currentModel) {
          /*
           * If so, set 'translations' as relationship name
           * and filter current query to require filtered translations
           */
          relationship = 'translations';
          this.query(this.filterAndRequireTranslation(modelName, translationsFilters));
        } else {
          /*
           * Otherwise, check if model is present in withRelated parameter and procceed to include
           * its translations if so
           */
          for (let i = 0; i < withRelated.length && !relationship; i += 1) {
            const related = withRelated[i];
            const isObject = typeof related === 'object';
            const isString = typeof related === 'string';

            if ((isString && related === modelName) || (isObject && related[modelName])) {
              /*
               * If the model is being loaded, set 'model.translations' as relationship name
               * and filter the model loading query to require its translations
               */
              relationship = `${modelName}.translations`;

              // Replace found withRelated item to filter query for required translations
              let newRelated = {};
              if (isString) {
                newRelated[modelName] = this.filterAndRequireTranslation(
                  modelName,
                  translationsFilters,
                );
              } else {
                // Keep existing behavior and add the filter to require its translations
                newRelated = _.cloneDeep(related);
                delete newRelated[modelName];
                newRelated[modelName] = (qb) => {
                  related[modelName](qb);
                  this.filterAndRequireTranslation(modelName, translationsFilters)(qb);
                };
              }
              withRelated[i] = newRelated;
            }
          }
        }

        /*
         * If a translation relationship was found, add it with appropriate translation filters
         * to a withRelated variable
         */
        if (relationship) {
          relatedTranslation[relationship] = (query) => {
            translationsFilters.forEach((filter) => {
              const { field, value } = filter;
              if (_.isArray(value)) {
                query.whereIn(field, value);
              } else {
                query.where(field, 'LIKE', value);
              }
            });
          };

          withRelatedTranslations.push(relatedTranslation);
        }
      });

      /*
       * Replace appropriate parameters:
       * Set withRelated parameter with new withRelatedTranslations and updated withRelated
       * Update filters parameter with updated filters
       */
      parameters.withRelated = withRelated.concat(withRelatedTranslations);
      parameters.filters = filters;

      return this;
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
