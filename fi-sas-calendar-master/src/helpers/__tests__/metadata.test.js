import { getPaginationMetaData } from '../metadata';

describe('Metadata helper test', () => {
  describe('getPaginationMetaData', () => {
    const expectedProps = ['self', 'prev', 'next', 'first', 'last', 'total'];
    const request = {
      protocol: 'htpp', originalUrl: '/e?sort=-active', get: e => e, query: {},
    };

    it('returns empty object when no pagination is sent', () => {
      const link = getPaginationMetaData(null, request);
      expect(link).toEqual({});
    });

    it('returns JSON object with expected pagination metadata when some pagination is sent', () => {
      const link = getPaginationMetaData({}, request);
      expect(link).toBeInstanceOf(Object);
      expectedProps.forEach((key) => {
        expect(link[key]).toBeDefined();
      });
      expect(typeof link.total).toBe('number');
      expect(typeof link.self).toBe('string');
      expect(typeof link.prev).toBe('string');
      expect(typeof link.next).toBe('string');
      expect(typeof link.first).toBe('string');
      expect(typeof link.last).toBe('string');
    });

    it('returns first=last, empty prev, empty next and zero total when no items in pagination', () => {
      const pag = {
        page: 1,
        offset: 0,
        pageSize: 10,
        limit: 10,
        rowCount: 0,
      };

      const link = getPaginationMetaData(pag, request);
      expect(link.last).toBe(link.last);
      expect(link.prev).toHaveLength(0);
      expect(link.next).toHaveLength(0);
      expect(link.total).toBe(0);
    });
  });
});
