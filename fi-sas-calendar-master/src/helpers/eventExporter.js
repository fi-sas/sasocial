
import { createEvent } from 'ics';
import path from 'path';
import { accessSync, writeFileSync, unlinkSync } from 'fs';
import glob from 'glob';
import { hashCode } from './hashCode';

import { PROJECT_DIR } from '../settings';

/**
 * Internal usage only
 */
function hashInt(id) {
  return hashCode(parseInt(id, 10));
}

/**
 * Creates ICS file content from received Event instance.
 * Assumes the received event is a valid Event instance and has loaded translations and categories.
 * Returns string with ICS content.
*/
async function createICS(event) {
  const title = event.related('translations').at(0).get('name');
  const description = event.related('translations').at(0).get('description');
  const url = event.related('translations').at(0).get('url');
  const geo = { lat: event.get('latitude'), lon: event.get('longitude') };
  const categories = (event.related('category') && event.related('category').related('translations'))
    ? [event.related('category').related('translations').at(0).get('name')] : [];
  const startDate = event.get('start_date');
  let endDate = event.get('end_date');
  if (!endDate) {
    // Assume 1 hour duration by default
    endDate = new Date(startDate.getTime() + 60 * 60 * 1000);
  }

  const start = [
    startDate.getFullYear(),
    startDate.getMonth() + 1,
    startDate.getDate(),
    startDate.getHours(),
    startDate.getMinutes(),
  ];
  const end = [
    endDate.getFullYear(),
    endDate.getMonth() + 1,
    endDate.getDate(),
    endDate.getHours(),
    endDate.getMinutes(),
  ];

  // Save event to export
  const dataToExport = {
    start,
    end,
    title,
    description,
    url,
    geo,
    status: 'CONFIRMED',
    categories,
  };

  createEvent(dataToExport, (error, value) => {
    if (error) {
      throw new Error(error);
    }

    // Manually insert recurrence RRULE:FREQ=() (ics library limitation)
    const strEvents = value.split('END:VEVENT');
    const completeEvent = strEvents.map((strE, index) => {
      // Ignore last piece (contains calendar closing annotation)
      if (index === strEvents.length - 1) {
        return strE;
      }
      // Include RRULE if needed
      return `${strE}${(event.get('recurrence') !== 'None')
        ? `RRULE:FREQ=${event.get('recurrence').toUpperCase()}\r\n`
        : ''}END:VEVENT`;
    }).join('');

    return completeEvent;
  });
}

/**
* Gets (newly created or already existing) ICS filename for the received Event instance.
* Assumes the received event is a valid Event instance and has loaded translations and categories.
* Checks if the appropriate file already exists - if so, the existing filename is returned.
* Otherwise, a new file is created with the content returned by createICS function.
* Returns string with ICS filename, or empty string if not successful.
*/
async function getICS(event, languageId) {
  const eventFileName = `${hashInt(event.get('id'))}_l${languageId ? hashInt(languageId) : ''}.ics`;
  const publicPath = path.join(PROJECT_DIR, 'public');
  const eventPath = `${publicPath}/${eventFileName}`;

  const fileExists = (filePath) => {
    let exist = true;
    try {
      accessSync(filePath);
    } catch (error) {
      exist = false;
    }
    return exist;
  };

  // Check if event file already exists, if it does not, create it
  if (!fileExists(eventPath)) {
    const ICSEvent = await createICS(event);
    writeFileSync(eventPath, ICSEvent);
  }

  return eventFileName;
}

/**
* Removes the existing ICS files for the received Event instance.
*/
async function deleteICS(event) {
  const publicPath = path.join(PROJECT_DIR, 'public');
  const eventFileNamePrefix = hashInt(event.get('id'));
  const filesPattern = `${publicPath}/${eventFileNamePrefix}_l*.ics`;
  const silentUnlink = (fp) => {
    try {
      unlinkSync(fp);
    } catch (e) {
      console.log('Caught error on deleting');
      console.log(e);
    }
  };

  // Remove resized files
  glob.sync(filesPattern).forEach(silentUnlink);
}

export { getICS, createICS, deleteICS };
