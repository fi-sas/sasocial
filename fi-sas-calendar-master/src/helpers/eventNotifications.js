import _ from 'lodash';
import moment from 'moment';

import { access, alert } from './communication';
import Event from '../models/event';
import EventNotification from '../models/event_notification';

/**
 * Checks the event notification recurrence type to see if the next
 * notification should be sent.
 *
 * @param {String} lastDate The date of the sending of the last notification
 * (ISO formatted string).
 * @param {String} recurrence One of 'Annualy', 'Monthly', 'Weekly', 'Daily' or 'None'.
 *
 * @returns {Boolean} If the next notification should be sent or not.
 */
const shouldSendNextNotification = (lastDate, recurrence) => {
  switch (recurrence) {
    case 'Annualy':
      return moment(lastDate).add(1, 'y') <= moment();

    case 'Monthly':
      return moment(lastDate).add(1, 'M') <= moment();

    case 'Weekly':
      return moment(lastDate).add(1, 'w') <= moment();

    case 'Daily':
      return moment(lastDate).add(1, 'd') <= moment();

    // Catches 'None' recurrence
    default:
      return true;
  }
};

/**
 * Returns true or false whether a notification should be send for the provided event.
 *
 * @param {Object} ev The data related with the event.
 *
 * @returns {Boolean} A boolean indicating if a notification should be sent for this event.
 */
const shouldSendNotification = (ev) => {
  // Count the number of successfully sent notifications for this event
  const successfullySentNotifications = ev.notifications.filter(n => !!n.success).length;
  if (successfullySentNotifications >= ev.number_of_notifications) {
    return false;
  }

  // If no notification has been sent yet, then it is time to send it
  if (successfullySentNotifications === 0) {
    return true;
  }

  // Check the event notification recurrence to see if the next notification should be sent
  return shouldSendNextNotification(ev.notifications.pop().date, ev.notification_recurrence);
};

export default async function sendEventNotifications() {
  // Find the events that require sending notifications
  const notifiableEvents = await Event.forge().where((qb) => {
    qb.where('notifications_enabled', true)
      .andWhere('notification_start_date', '<=', new Date());
  }).fetchAll({ withRelated: ['translations', 'notifications'] });

  // Find the notifications that need to be sent
  const toBeSent = notifiableEvents.toJSON()
    // Filter the events that have already (successfully) sent all the required notifications
    .filter(ev => shouldSendNotification(ev))

    // eslint-disable-next-line no-param-reassign
    .map((ev) => { delete ev.notifications; return ev; })

    // Return an object to be used to send the notification
    .map(ev => ({
      event_id: ev.id,
      target_id: ev.notification_target_id,
      service_id: ev.service_id,
      alert_type_key: _.isEmpty(ev.notification_alert_key_type)
        ? 'CALENDAR_EVENT_NOTIFICATION' : ev.notification_alert_key_type,
      data: ev,
    }));

  if (toBeSent.length > 0) {
    await Promise.all(toBeSent.map(async (notif) => {
      let success;
      let response;

      try {
        // Check if target id is defined in the event - otherwise, use the target id of the
        // service id associated with the event
        let targetId = notif.target_id;
        if (_.isEmpty(targetId)) {
          const result = await access(null, 'CONFIG', `services/${notif.service_id}`);
          targetId = result[0].target_id;
        }

        if (targetId === null) {
          throw new Error('Please configure the target id associated with the event service id.');
        }

        // Find users in the provided target
        const targetUsersRes = await access(null, 'AUTH', `targets/${targetId}/users`);

        // Send notifications for each user of the provided target
        const results = await Promise.all(targetUsersRes.map(async user => alert(
          null,
          notif.alert_type_key,
          { event: notif.data },
          user.id,
          [],
          user,
        )));

        success = 1;
        response = JSON.stringify(results);
      } catch (err) {
        success = 0;
        response = JSON.stringify(err);

        // eslint-disable-next-line no-console
        console.log(err);
      }

      // Save a log of the sent notification
      return EventNotification.forge().save({
        event_id: notif.event_id,
        target_id: notif.target_id,
        alert_type_key: notif.alert_type_key,
        date: new Date(),
        data: JSON.stringify(notif.data),
        success,
        response,
      });
    }));
  }
}
