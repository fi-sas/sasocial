import { URL } from 'url';
/* eslint-disable import/prefer-default-export */

/**
 * Returns object with pagination metadata to be used as link response value,
 * according to pagination object received.
 * Returned object includes total number of items and self, prev, next, first
 * and last link properties.
 * If no previous link or no next link exist, their values are an empty sring.
 * If no pagination object is received, an empty object is returned.
 *
 * @param {Object} pagination
 * @param {Object} req
 * @returns {Object}
 */
function getPaginationMetaData(pagination, req) {

  if (!pagination) {
    return {};
  }

  const { limit, offset } = pagination;
  const total = typeof pagination.rowCount !== 'undefined' ? pagination.rowCount : 0;

  const self = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
  const url = new URL(self);

  const lastOffset = Math.floor(total / limit) * limit;
  const prevOffset = offset > 0 ? offset - limit : false;

  let prev = '';
  if (prevOffset !== false) {
    url.searchParams.set('offset', prevOffset);
    prev = url.toString();
  }

  url.searchParams.set('offset', 0);
  const first = url.toString();

  url.searchParams.set('offset', lastOffset);
  const last = url.toString();

  let next = '';
  if (offset < lastOffset) {
    url.searchParams.set('offset', offset + limit);
    next = url.toString();
  }

  return {
    self,
    prev,
    next,
    first,
    last,
    total,
  };
}

export { getPaginationMetaData };
