import got from 'got';
import _ from 'lodash';

import validations from './validations';
import * as Errors from '../errors';

export default async function apiComposerValidations(req, res, next) {
  const promises = [];
  validations.forEach(async ({
    resource, path, singlePath, arrayPath, indexPath, targetMS, targetResource, endpoints, code,
    message,
  }) => {
    // Check if this validation should be applied
    if (!resource.test(req.originalUrl) || !endpoints.includes(req.method)) {
      return;
    }

    // Find the IDs to validate
    let ids = [];
    const getFromArray = (data, index) => data.map((e) => {
      if (index === null || e === null) {
        return e;
      }
      return (typeof e[index] !== 'undefined') ? e[index] : null;
    });

    if (!_.isUndefined(arrayPath)) {
      ids = getFromArray(_.get(req.body, arrayPath) || [], indexPath);
    } else if (singlePath) {
      ids = getFromArray([req.body], singlePath) || [];
    } else if (path) {
      ids = path.split('.').reduce((previous, current) => {
        const isArray = (current.startsWith('[') && current.endsWith(']'));
        const index = isArray ? current.slice(1, -1) : current;
        // Flat results:
        return [].concat(...getFromArray(previous instanceof Array ? previous : [previous], index));
      }, req.body);
    }

    // Filter undefined ids
    ids = ids.filter(id => id !== undefined && id !== null);

    // Prepare to perform GETs by id using the ids to validate
    promises.push(
      ...ids.map(id => got(`${targetMS}api/v1/${targetResource}/${id}`, { headers: { Authorization: req.headers.authorization } })
        .catch((err) => {
          console.log(err);

          if (err.statusCode === 404) {
            throw new Errors.ValidationError([{ code, message }]);
          } else {
            throw new Errors.SimpleError({ code, message: `[status: ${err.statusCode}] ${message}` });
          }
        })),
    );
  });

  try {
    await Promise.all(promises);
    return next();
  } catch (e) {
    if (e instanceof Errors.ValidationError) {
      return res.formatter.badRequest(e.errors);
    }
    return res.formatter.serverError({ code: e.code, message: e.message });
  }
}
