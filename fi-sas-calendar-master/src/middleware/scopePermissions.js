/**
 * Scope Permissions Middleware: requires scope permission to execute the request.
 *
 * Throws Forbidden Error when request does not contain the necessary permission.
 * Uses decoded info in req.authInfo.
 *
 * Exceptional behavior for internal requests: no validation is made unless check-scope
 * header is sent to true.
 *
 * @public
 */
import url from 'url';
import * as Codes from '../error_codes';

/**
 * Converts the provided HTTP method into the scope corresponding action.
 * For example, 'GET' is converted to 'read' and PUT is converted to 'update'.
 *
 * @param {string} method GET, PUT, PATCH, POST or DELETE.
 * @returns {string} 'create', 'read', 'update' or 'delete', according to the HTTP method provided.
 */
function convertMethod(method) {
  switch (method) {
    case 'POST':
      return 'create';
    case 'DELETE':
      return 'delete';
    case 'PUT':
    case 'PATCH':
      return 'update';
    default:
      return 'read';
  }
}

/**
 * Parses the resource that should be taken into account from the route URL.
 * Removes the usual '/api/v1/' part of the route name, and finds the first keyword
 * after this - for instance, /api/v1/users is transformed to 'users', but
 * /api/v1/users/1 is also transformed to 'users'.
 *
 * @param {string} reqUrl The URL to be parsed - usually req.originalUrl
 * @returns {string} The resource that should be used for permission validation.
 */
function parseResourceFromRoute(reqUrl) {
  return url.parse(reqUrl).pathname.split('/')[3];
}

/**
 * Parses the resource param that is currently being requested. Typically, this is represented by
 * an ID parameter that is provided after the resource identifier, such as the :id in
 * api/v1/users/:id
 *
 * This value is returned if existing, undefined otherwise.
 *
 * @param {string} reqUrl The URL to be parsed - usually req.originalUrl
 * @returns {string|undefined} The ID of the resource that should be used for ownership validation.
 */
function parseIdentifierFromRoute(reqUrl) {
  return url.parse(reqUrl).pathname.split('/')[4];
}

/**
 * Validates if the provided token can execute the provided request.
 * The request can be executed if user has scope to it, or if the request is internal
 * and no specific action is taken to check the scope permission.
 *
 * @param {Array} scopesToConsider Array of scopes to be considered.
 * @param {*} req Request information, from which will be parsed the action that is being executed.
 */
function canExecuteAction(scopesToConsider, req) {
  if (req.internalRequest && !req.headers['check-scope']) {
    return true;
  }

  const service = process.env.MICROSERVICE_IDENTIFIER;
  const resource = parseResourceFromRoute(req.originalUrl);
  const method = convertMethod(req.method);
  const resourceScope = `${service}:${resource}`;
  const methodScope = `${service}:${resource}:${method}`;

  return scopesToConsider.includes(resourceScope) || scopesToConsider.includes(methodScope);
}

/**
 * Validates if the user provided is the owner of the resource being edited. If this is the case,
 * then permission schema should be ignored and the user should be allowed to perform the action.
 *
 * @param {*} authInfo User information.
 * @param {*} req Request information, from which will be parsed the action that is being executed.
 */
function isOwnerOfData(authInfo, req) {
  const identifier = parseIdentifierFromRoute(req.originalUrl);
  const resource = parseResourceFromRoute(req.originalUrl);
  return identifier && resource === 'users' && (identifier === req.authInfo.user.id.toString());
}

export default function requireAuth(req, res, next) {
  // Ignore token validation for OPTIONS requests
  if (req.method === 'OPTIONS') {
    next();
    return true;
  }

  const canExecute = canExecuteAction(req.authInfo && req.authInfo.scopes ? req.authInfo.scopes : [], req);
  const isOwner = isOwnerOfData(req.authInfo && req.authInfo.scopes ? req.authInfo.scopes : [], req);

  if (
    // Valid user information or token was provided
    req.authInfo
    // User can perform the action OR user is not the owner of the resource being requested
    && (canExecute || isOwner)
  ) {
    req.canExecuteAction = canExecute;
    req.isOwnerOfData = isOwner;
    next();
    return true;
  }

  return res.formatter.forbidden({
    code: Codes.AuthenticationLackOfPermissions,
    message: 'The token provided does not have permission to execute the requested action.',
  });
}
