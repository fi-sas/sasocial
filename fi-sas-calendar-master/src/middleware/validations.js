import * as Codes from '../error_codes';

export default [
  // Validation for language id inside event category translations
  {
    resource: /api\/v1\/event-categories(.*)?$/m,
    path: 'translations.[language_id]',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'languages',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidLanguageId,
    message: 'The language id provided is not valid.',
  },
  // Validation for language id inside event translations
  {
    resource: /api\/v1\/events(.*)?$/m,
    path: 'translations.[language_id]',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'languages',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidLanguageId,
    message: 'The language id provided is not valid.',
  },
  // Validation for file id inside event medias
  {
    resource: /api\/v1\/events(.*)?$/m,
    path: 'medias.[file_id]',
    targetMS: process.env.MS_MEDIA_ENDPOINT,
    targetResource: 'files',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidFileId,
    message: 'The file id provided is not valid.',
  },
  // Validation for service id inside event
  {
    resource: /api\/v1\/events(.*)?$/m,
    path: 'service_id',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'services',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidServiceId,
    message: 'The service id provided is not valid.',
  },
  // Validation for target id inside event
  {
    resource: /api\/v1\/events(.*)?$/m,
    path: 'notification_target_id',
    targetMS: process.env.MS_AUTH_ENDPOINT,
    targetResource: 'targets',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.ValidationMissingRelatedEntity,
    message: 'The target id provided is not valid.',
  },
  // Validation for tax id inside event
  {
    resource: /api\/v1\/events(.*)?$/m,
    path: 'registration_tax_id',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'taxes',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.ValidationMissingRelatedEntity,
    message: 'The tax id provided is not valid.',
  },
];
