/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import { uniqBy } from 'lodash';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import categorySchema from '../schemas/category-schema';

import CategoryTranslation from './category_translation';

const Category = Bookshelf.Model.extend({
  tableName: 'category',
  hasTimestamps: true,

  events() {
    return this.hasMany('Event', 'category_id');
  },

  translations() {
    return this.hasMany(CategoryTranslation, 'category_id');
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.validateDestroy);
    this.on('destroying', this.destroyCascade);
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, categorySchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },

  async validateDestroy(model, options) {
    const opts = { transacting: options.transacting };

    // // Check if there are related events and prevent delete if so
    const it = await this.load('events', opts);

    if (it.related('events').length > 0) {
      throw new Errors.ValidationError({
        code: Codes.InvalidDeleteDueToRelatedEntityExistence,
        message: 'The Category can not be deleted because it has associated events',
      });
    }
  },

  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    // Cascade delete category translations and events
    return this.load('translations', opts)
      .then(it => it.related('translations').invokeThen('destroy', opts));
  },

  saveWithRelated(data, translations, opts = {}) {
    const withRelated = ['translations'];
    const partial = (opts.patch === true);
    let uniqueTranslations = false;

    /*
     * Validate translations format and existence (if needed)
     */
    if (translations && !(translations instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'translations',
        message: 'translations must be an array',
      }]);
    }

    const needsTranslations = opts.method === 'insert' || (opts.patch === false);
    if (needsTranslations && !(translations && translations.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'translations',
        message: 'Missing required translations',
      }]);
    }

    /*
     * Consider unique translations
     */
    if (translations && translations instanceof Array) {
      uniqueTranslations = uniqBy(translations.reverse(), 'language_id');
    }

    return Bookshelf.transaction(t => this.save(data, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        if (uniqueTranslations) {
          // Load translations
          return item.load('translations', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations && !partial) {
          // Remove existing translations unless partial update
          return item.related('translations').invokeThen('destroy', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations) {
          if (!partial) {
            // Directly insert received translations
            return Promise.all(uniqueTranslations.map((tr) => {
              const translationData = Object.assign(tr, { category_id: item.get('id') });
              return item.related('translations').create(translationData, { transacting: t });
            }));
          }
          // If partial, since existing translations were not remove, handle received
          // translations to update or insert each
          return Promise.all(uniqueTranslations.map((tr) => {
            const translationData = Object.assign(tr, { category_id: item.get('id') });
            const translation = item.related('translations').find(el => el.get('language_id') === tr.language_id);
            if (!translation) {
              return item.related('translations').create(translationData, { transacting: t });
            }
            return translation.save(translationData, { transacting: t });
          }));
        }
        return true;
      }))
      .tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Category not found');
      });
  },
}, {
  filterFields: ['id', 'color', 'created_at', 'updated_at'],
  sortFields: ['id', 'color', 'created_at', 'updated_at'],
  defaultWithRelated: ['translations'],
  withRelated: ['events', 'translations'],
});

export default Bookshelf.model('Category', Category);
