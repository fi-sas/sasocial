/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import categoryTranslationSchema from '../schemas/category-translation-schema';

import Category from './category';

const CategoryTranslation = Bookshelf.Model.extend({
  tableName: 'category_translation',
  hidden: ['id', 'category_id'],

  category() {
    return this.belongsTo(Category);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, categoryTranslationSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
}, {
  filterFields: ['name'],
  sortFields: ['translations.id', 'translations.name', 'translations.description'],
});

export default Bookshelf.model('CategoryTranslation', CategoryTranslation);
