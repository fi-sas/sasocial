/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import { uniqBy } from 'lodash';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import eventSchema from '../schemas/event-schema';
import recurrences from './values/recurrences';

import { deleteICS } from '../helpers/eventExporter';

import EventCategory from './category';
import Form from './form';
import Registration from './registration';
import EventTranslation from './event_translation';
import EventNotification from './event_notification';
import EventMedia from './event_media';
import EventContact from './event_contact';
import EventInterest from './event_interest';
import EventIsInterested from './event_is_interested';
import EventIsRegistered from './event_is_registered';

const Event = Bookshelf.Model.extend({
  tableName: 'event',
  hasTimestamps: true,
  hidden: ['repeat_month', 'repeat_day', 'repeat_weekday'],

  translations() {
    return this.hasMany('EventTranslation', 'event_id');
  },

  contacts() {
    return this.hasMany('EventContact', 'event_id');
  },

  medias() {
    return this.hasMany(EventMedia);
  },

  interested() {
    return this.hasMany('EventInterest', 'event_id');
  },

  category() {
    return this.belongsTo('Category');
  },

  registration_form() {
    return this.belongsTo('Form', 'registration_form_id');
  },

  registrations() {
    return this.hasMany('Registration');
  },

  notifications() {
    return this.hasMany('EventNotification');
  },

  is_interested() {
    // Expected to be used in withRelated with additional filter by user id
    // as: is_interested: qb => qb.where('event_interest.user_id', userId);
    return this.hasOne('EventIsInterested', 'event_id');
  },

  is_registered() {
    // Expected to be used in withRelated with additional filter by user id
    // as: is_registered: qb => qb.where('registration.user_id', userId);
    return this.hasOne('EventIsRegistered', 'event_id').where({ status: 'Approved' });
  },

  parse(response) {
    return this.parseBooleans(response, ['notifications_enabled', 'allow_interest',
      'allow_registration', 'cancelled', 'registration_approval_needed']);
  },

  format(attributes) {
    const formatted = this.formatModelTimestamps(attributes, ['publish_start_date', 'publish_end_date',
      'notification_start_date', 'start_date', 'end_date', 'created_at', 'updated_at']);
    return this.formatBooleans(formatted, ['notifications_enabled', 'allow_interest',
      'allow_registration', 'cancelled', 'registration_approval_needed']);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.assignPendingStatus);
    this.on('saving', this.assignRepeatValues);
    this.on('saving', this.validateSave);
    this.on('saved', this.removeExportFiles);
    this.on('destroying', this.validateDestroy);
    this.on('destroying', this.destroyCascade);
    this.on('destroyed', this.removeExportFiles);
  },

  /**
   * Assign Pending status to instance being saved, unless
   * options contain approve or reject indication.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  assignPendingStatus(model, attrs, options) {
    if (this.isNew() || !(options.approve || options.reject)) {
      // Set Pending status
      this.set('status', 'Pending');
    }
  },

  /**
   * Assigns repeat values according to the instance's start date and recurrence value.
   * Assigns values for repeat_day, repeat_month and repeat_weekday attributes.
   */
  assignRepeatValues() {
    if (this.hasChanged('recurrence') || this.hasChanged('start_date')) {
      const { startDate } = this.formatModelTimestamps({ startDate: this.get('start_date') }, ['startDate']);
      const r = this.get('recurrence');

      // If everything looks OK, assign repeat values
      if (startDate instanceof Date && recurrences.includes(r)) {
        const eventDay = startDate.getDate();
        const eventMonth = startDate.getMonth() + 1;
        const eventWeekDay = startDate.getDay();

        switch (r) {
          case 'Daily':
            this.set('repeat_day', '*');
            this.set('repeat_month', '*');
            this.set('repeat_weekday', '*');
            break;
          case 'Weekly':
            this.set('repeat_day', '*');
            this.set('repeat_month', '*');
            this.set('repeat_weekday', eventWeekDay);
            break;
          case 'Monthly':
            this.set('repeat_day', eventDay);
            this.set('repeat_month', '*');
            this.set('repeat_weekday', '*');
            break;
          case 'Anually':
            this.set('repeat_day', eventDay);
            this.set('repeat_month', eventMonth);
            this.set('repeat_weekday', '*');
            break;
          case 'None':
          default:
            this.set('repeat_day', null);
            this.set('repeat_month', null);
            this.set('repeat_weekday', null);
        }
      }
    }
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, eventSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate event category and registration form
    const errors = [];
    if (this.hasChanged('category_id')) {
      try {
        await EventCategory.validateExistence(this.get('category_id'));
      } catch (e) {
        errors.push({
          code: Codes.ValidationMissingRelatedEntity,
          field: 'category_id',
          message: 'The category id provided is not valid.',
        });
      }
    }
    if (this.hasChanged('registration_form_id') && this.get('registration_form_id')) {
      try {
        await Form.validateExistence(this.get('registration_form_id'));
      } catch (e) {
        errors.push({
          code: Codes.ValidationMissingRelatedEntity,
          field: 'registration_form_id',
          message: 'The form id provided is not valid.',
        });
      }
    }

    if (errors.length) {
      throw new Errors.ValidationError(errors);
    }
  },

  /**
   * Validate destroy of model instance.
   * Only events which are not in an Approved status may be removed. Otherwise, a
   * ValidationError is thrown.
   *
   */
  async validateDestroy() {
    if (this.get('status') === 'Approved') {
      throw new Errors.ValidationError({
        code: Codes.InvalidOperation,
        message: 'The event can not be deleted because it is currently in an Approved status',
      });
    }
  },

  /**
   * Destroy cascade by deleting related entities.
   *
   * @param {Object} model
   * @param {Object} options
   */
  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    // Cascade delete event translations
    const promiseTranslationsDestroy = this.load('translations', opts)
      .then(it => it.related('translations').invokeThen('destroy', opts));
    const promiseContactsDestroy = this.load('contacts', opts)
      .then(it => it.related('contacts').invokeThen('destroy', opts));
    const promiseMediasDestroy = this.load('medias', opts)
      .then(it => it.related('medias').invokeThen('destroy', opts));
    const promiseInterestsDestroy = this.load('interested', opts)
      .then(it => it.related('interested').invokeThen('destroy', opts));
    const promiseRegistrationsDestroy = this.load('registrations', opts)
      .then(it => it.related('registrations').invokeThen('destroy', opts));

    return Promise.all([
      promiseTranslationsDestroy,
      promiseContactsDestroy,
      promiseMediasDestroy,
      promiseInterestsDestroy,
      promiseRegistrationsDestroy,
    ]);
  },

  /**
   * Returns true if current Event instance is published, according to its status and
   * publishing start and end date, and false otherwise.
   *
   * @returns {boolean} published
   */
  isPublished() {
    const now = new Date();
    return this.get('status') === 'Approved' && this.get('publish_start_date') <= now
      && (this.get('publish_end_date') >= now || this.get('publish_end_date') === null);
  },

  /**
   * Queries models according to received published parameter.
   * If null is received, nothing is done.
   * Otherwise, uses current date and publishing start and end date PLUS
   * status value of Approved to filter items which are currently active or not.
   *
   * @param {mixed} published null or trueish/falsish value
   */
  published(published) {
    if (published === null) {
      return this;
    }

    this.query((q) => {
      const now = new Date();
      if (published === 'true' || published === true) {
        q.where('publish_start_date', '<=', now)
          .where((q2) => {
            q2.where('publish_end_date', '>=', now).orWhereNull('publish_end_date');
          })
          .where('status', '=', 'Approved');
      } else {
        q.where('publish_start_date', '>=', now)
          .orWhere('publish_end_date', '<=', now)
          .orWhere('status', '!=', 'Approved');
      }
    });

    return this;
  },

  /**
   * Returns an array of week days between the two received dates.
   * If only one date is received, an array with all weekdays is returned (0..6).
   *
   * @param {Date} minDate
   * @param {Date} maxDate
   */
  getWeekDaysBetween(minDate = null, maxDate = null) {
    if (!minDate && !maxDate) {
      return [];
    }

    const msInAWeek = 1000 * 60 * 60 * 24 * 7;
    const minDateToConsider = minDate || new Date(maxDate.getTime() - msInAWeek);
    const maxDateToConsider = maxDate || new Date(minDate.getTime() + msInAWeek);
    const sDiff = Math.abs(maxDateToConsider - minDateToConsider) / 1000;
    const daysDiff = Math.floor(sDiff / 86400);
    const minWDay = minDateToConsider.getDay();

    const weekDays = [];
    if (daysDiff < 6) {
      for (let i = 0; i <= daysDiff; i++) {
        weekDays.push((minWDay + i) % 7);
      }
    } else {
      weekDays.push(...[0, 1, 2, 3, 4, 5, 6]);
    }
    return weekDays;
  },

  /**
   * Queries models according to received min and max date,
   * search for unique or recurring events active between both.
   *
   * @param {Date} minDate
   * @param {Date} maxDate
   */
  between(minDate, maxDate) {
    if (minDate || maxDate) {
      // Gather data from dates
      const minMonth = minDate ? minDate.getMonth() : 0;
      const maxMonth = maxDate ? maxDate.getMonth() : 0;
      const weekDays = this.getWeekDaysBetween(minDate, maxDate);

      // Prepare subquery producer
      const sameMonthSubquery = (date, validWeekDays, op = '>=') => {
        const month = date.getMonth();
        const day = date.getDate();

        return (qb) => {
          qb.where((qbMonth) => {
            // In the same month or every month
            qbMonth.where('event.repeat_month', '=', month);
            qbMonth.orWhere('event.repeat_month', '=', '*');
          });
          qb.where((qbDays) => {
            // After min days or every day
            qbDays.where('event.repeat_day', op, day);
            qbDays.orWhere('event.repeat_day', '=', '*');
          });
          qb.where((qbWeekDays) => {
            // Inside valid week days or every week day
            qbWeekDays.whereIn('event.repeat_weekday', validWeekDays);
            qbWeekDays.orWhere('event.repeat_weekday', '=', '*');
          });
        };
      };

      // Query model, join with recurrences and apply appropriate filters
      this.query((qb) => {
        if (minDate && maxDate) {
          qb.where((qbBetween) => {
            // Unique event between min and max dates
            qbBetween.where((qbUnique) => {
              qbUnique.where('event.start_date', '>=', minDate);
              qbUnique.where('event.start_date', '<=', maxDate);
              qbUnique.where('event.recurrence', '=', 'None');
            });

            // Or recurring event with repetition between min and max dates
            qbBetween.orWhere((qbRec) => {
              // Recurrence started before max
              qbRec.where('event.start_date', '<=', maxDate);

              if (minMonth === maxMonth) {
                // Recurrence happens in same month after min date and before max date
                qbRec.where(sameMonthSubquery(minDate, weekDays, '>='));
                qbRec.where(sameMonthSubquery(maxDate, weekDays, '<='));
              }

              if (minMonth === maxMonth - 1) {
                // Recurrence happens in the same month as min date (or in every month) and after it,
                // or in the same month as max date (or in every month) and before it
                qbRec.where((qbRecMonths) => {
                  qbRecMonths.where(sameMonthSubquery(minDate, weekDays, '>='));
                  qbRecMonths.orWhere(sameMonthSubquery(maxDate, weekDays, '<='));
                });
              }

              if (Math.abs(minMonth - maxMonth) > 1) {
                qbRec.where((qbRecMonths) => {
                  // Recurrence happens in the same month as min date (or in every month) and after
                  // it, or in the same month as max date (or in every month) and before it, or in
                  // the months inbetween
                  qbRecMonths.where(sameMonthSubquery(minDate, weekDays, '>='));
                  qbRecMonths.orWhere(sameMonthSubquery(maxDate, weekDays, '<='));
                  qbRecMonths.orWhere((qbOtherMonth) => {
                    qbOtherMonth.whereBetween('event.repeat_month', [minMonth, maxMonth]);
                    qbOtherMonth.orWhere('event.repeat_month', '=', '*');
                  });
                });
              }
            });
          });
        } else {
          if (minDate) {
            qb.where((qbAfter) => {
              // Unique event after min date
              qbAfter.where((qbUnique) => {
                qbUnique.where('event.start_date', '>=', minDate);
                qbUnique.where('event.recurrence', '=', 'None');
              });

              // Or simply recurring event (eventually will repeat)
              qbAfter.orWhere((qbRec) => {
                qbRec.where('event.recurrence', '!=', 'None');
              });

              // Note: these conditions are both needed! Unique events must be after min date; and
              // recurring events can  start before or after min date (both cases should be caught)
            });
          }

          if (maxDate) {
            qb.where((qbBefore) => {
              // Event that started before max date
              qbBefore.where('event.start_date', '<=', maxDate);
            });
          }
        }
      });
    }

    return this;
  },

  /**
   * Save with related entities.
   * Receives attributes data, array of translations, contacts and medias and options
   * object.
   * Returns a saved Event instance (new or updated), refreshed with default withRelated.
   * If received options contain userId, is_interested and is_registered will use received value
   * (otherwise both will always be false).
   *
   * @param {Object} data
   * @param {Array} translations
   * @param {Array} contacts
   * @param {Array} medias
   * @param {Object*} opts
   */
  saveWithRelated(data, translations, contacts, medias, opts = {}) {
    const partial = (opts.patch === true);
    let uniqueTranslations = false;
    const withRelated = Event.defaultWithRelated.slice();

    /*
     * Add withRelated to fetch is_interested and is_registered
     */
    withRelated.unshift({
      is_interested: qb => qb.where('event_interest.user_id', opts.userId ? opts.userId : -1),
      is_registered: qb => qb.where('registration.user_id', opts.userId ? opts.userId : -1),
    });

    /*
     * Validate translations format and existence (if needed)
     */
    if (translations && !(translations instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'translations',
        message: 'translations must be an array',
      }]);
    }

    const needsTranslations = opts.method === 'insert' || (opts.patch === false);
    if (needsTranslations && !(translations && translations.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'translations',
        message: 'Missing required translations',
      }]);
    }

    /*
     * Consider unique translations
     */
    if (translations && translations instanceof Array) {
      uniqueTranslations = uniqBy(translations.reverse(), 'language_id');
    }

    /*
     * Validate medias format, length and existence (if needed)
     */
    if (medias && !(medias instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'medias',
        message: 'medias must be an array',
      }]);
    }

    if (medias && medias instanceof Array && medias.length > 6) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'medias',
        message: 'medias must be an array with 1-6 elements',
      }]);
    }

    const needsMedias = opts.method === 'insert' || (opts.patch === false);
    if (needsMedias && !(medias && medias.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'medias',
        message: 'Missing required medias',
      }]);
    }

    return Bookshelf.transaction(t => this.save(data, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        if (medias) {
          // Load medias, remove existing medias, insert received medias
          return item.load('medias', { transacting: t })
            .tap(i => i.related('medias').invokeThen('destroy', { transacting: t }))
            .tap(i => Promise.all(medias.map((tr) => {
              const mediaData = { file_id: tr, event_id: i.get('id') };
              return i.related('medias').create(mediaData, { transacting: t });
            })));
        }
        return true;
      })
      .tap((item) => {
        if (contacts) {
          // Load contacts, remove existing contacts, insert received contacts
          return item.load('contacts', { transacting: t })
            .tap(i => i.related('contacts').invokeThen('destroy', { transacting: t }))
            .tap(i => Promise.all(contacts.map((tr) => {
              const contactData = Object.assign(tr, { event_id: i.get('id') });
              return i.related('contacts').create(contactData, { transacting: t });
            })));
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations) {
          if (!partial) {
            // Load translations, remove existing translations, insert received translations
            // === Complete update
            return item.load('translations', { transacting: t })
              .tap(i => i.related('translations').invokeThen('destroy', { transacting: t }))
              .tap(i => Promise.all(uniqueTranslations.map((tr) => {
                const translationData = Object.assign(tr, { event_id: i.get('id') });
                return i.related('translations').create(translationData, { transacting: t });
              })));
          }
          // If partial update, load translations and handle received
          // translations to update or insert each
          return item.load('translations', { transacting: t })
            .tap(i => Promise.all(uniqueTranslations.map((tr) => {
              const translationData = Object.assign(tr, { event_id: i.get('id') });
              const translation = item.related('translations').find(el => el.get('language_id') === tr.language_id);
              if (!translation) {
                return item.related('translations').create(translationData, { transacting: t });
              }
              return translation.save(translationData, { transacting: t });
            })));
        }
        return true;
      }))
      .tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Event not found');
      });
  },

  async removeExportFiles(model) {
    return deleteICS(model);
  },

}, {
  filterFields: ['id', 'address', 'service_id', 'category_id', 'min_start_date', 'max_start_date',
    'published', 'cancelled', 'updated_at', 'created_at'],
  sortFields: ['id', 'address', 'service_id', 'category_id', 'created_at', 'updated_at'],
  defaultWithRelated: ['translations', 'contacts', 'medias'],
  withRelated: ['translations', 'contacts', 'medias', 'category', 'registration_form',
    'registrations', 'interested', 'registration_form.elements'],
});

export default Bookshelf.model('Event', Event);
