/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import eventInterestSchema from '../schemas/event-interest-schema';

import Event from './event';

const EventInterest = Bookshelf.Model.extend({
  tableName: 'event_interest',
  hidden: ['id', 'event_id'],

  event() {
    return this.belongsTo(Event);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateUniqueness);
    this.on('saving', this.validateSave);
  },

  /**
   * When serializing an event interest, only return the user id.
   *
   * @param {Object*} options
   */
  toJSON() {
    return this.get('user_id');
  },

  /**
   * Validate unique index of user_id and event_id.
   * If an instance already exists with same values, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateUniqueness(model, attrs, options) {
    // Validate uniqueness of event and user ids
    const opts = (options.transacting) ? { transacting: options.transacting } : {};
    if (this.isNew() || this.hasChanged('user_id') || this.hasChanged('event_id')) {
      const n = await EventInterest.query((qb) => {
        qb.where({ user_id: this.get('user_id'), event_id: this.get('event_id') });
        if (this.get('id')) {
          qb.where('id', '!=', this.get('id'));
        }
      }).count(opts);
      if (n && n > 0) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The user is already interested in the event',
        }]);
      }
    }
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, eventInterestSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
});

export default Bookshelf.model('EventInterest', EventInterest);
