/* eslint import/no-cycle: 0 */
import Bookshelf from '../bookshelf';

import Event from './event';

/**
 * Additional model to represent boolean value of is interested.
 * Uses event_interest table and the serializing of an EventIsInterested model instance
 * returns true or false.
 * No save or destroy actions are allowed in this model - use EventInterest instead.
 * This model is used as relation to Event model - please check is_interested relation.
 */
const EventIsInterested = Bookshelf.Model.extend({
  tableName: 'event_interest',

  event() {
    return this.belongsTo(Event);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('destroying', () => { throw new Error(); });
    this.on('saving', () => { throw new Error(); });
  },

  /**
   * When serializing an EventIsInterest instance, either return true or false.
   * If the instance holds an user id value, returns true. Otherwise, no row was found on table,
   * so return false.
   *
   * @param {Object*} options
   */
  toJSON() {
    return !!this.get('user_id');
  },
});

export default Bookshelf.model('EventIsInterested', EventIsInterested);
