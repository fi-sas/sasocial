/* eslint import/no-cycle: 0 */
import Bookshelf from '../bookshelf';
import Event from './event';

const EventNotification = Bookshelf.Model.extend({
  tableName: 'event_notification',

  event() {
    return this.belongsTo(Event);
  },
});

export default Bookshelf.model('EventNotification', EventNotification);
