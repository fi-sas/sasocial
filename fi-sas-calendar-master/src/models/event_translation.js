/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import eventTranslationSchema from '../schemas/event-translation-schema';

import Event from './event';

const EventTranslation = Bookshelf.Model.extend({
  tableName: 'event_translation',
  hidden: ['id', 'event_id'],

  event() {
    return this.belongsTo(Event);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, eventTranslationSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
}, {
  filterFields: ['name', 'description', 'url'],
  sortFields: ['translations.id', 'translations.name', 'translations.description', 'translations.url'],
});

export default Bookshelf.model('EventTranslation', EventTranslation);
