/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import formSchema from '../schemas/form-schema';

import Event from './event';
import FormElement from './form_element';

const Form = Bookshelf.Model.extend({
  tableName: 'form',
  hasTimestamps: true,

  events() {
    return this.hasMany(Event, 'registration_form_id');
  },

  elements() {
    return this.hasMany(FormElement);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, formSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },

  destroyCascade(model, o) {
    const opts = { transacting: o.transacting };
    return this.load('elements', opts)
      .then(it => it.related('elements').invokeThen('destroy', opts));
  },

  saveWithRelated(data, elements, opts = {}) {
    const withRelated = ['elements'];
    const partial = (opts.patch === true);

    /*
     * Validate elements format and existence (if needed)
     */
    if (elements && !(elements instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'elements',
        message: 'elements must be an array',
      }]);
    }

    const needsElements = opts.method === 'insert' || (opts.patch === false);
    if (needsElements && !(elements && elements.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'elements',
        message: 'Missing required elements',
      }]);
    }

    return Bookshelf.transaction(t => this.save(data, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        if (elements) {
          // Load elements
          return item.load('elements', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (elements && !partial) {
          // Remove existing elements unless partial update
          return item.related('elements').invokeThen('destroy', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (elements) {
          if (!partial) {
            // Directly insert received elements
            return Promise.all(elements.map((tr) => {
              const elData = Object.assign(tr, { form_id: item.get('id') });
              return item.related('elements').create(elData, { transacting: t });
            }));
          }
          // If partial, since existing elements were not remove, handle received
          // elements to update or insert each
          return Promise.all(elements.map((tr) => {
            const elData = Object.assign(tr, { form_id: item.get('id') });
            const element = item.related('elements').find(el => el.get('name') === tr.name);
            if (!element) {
              return item.related('elements').create(elData, { transacting: t });
            }
            return element.save(elData, { transacting: t });
          }));
        }
        return true;
      }))
      .tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Form not found');
      });
  },
}, {
  filterFields: ['id', 'name', 'created_at', 'updated_at'],
  sortFields: ['id', 'name', 'created_at', 'updated_at'],
  defaultWithRelated: ['elements'],
  withRelated: ['events', 'elements'],
});

export default Bookshelf.model('Form', Form);
