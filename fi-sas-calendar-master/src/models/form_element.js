/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import formElementSchema from '../schemas/form-element-schema';

import Form from './form';

const FormElement = Bookshelf.Model.extend({
  tableName: 'form_element',
  hasTimestamps: true,

  form() {
    return this.belongsTo(Form);
  },

  parse(response) {
    const result = this.parseBooleans(response, ['required']);
    if (result.values) result.values = JSON.parse(result.values);
    return result;
  },

  format(attributes) {
    const result = this.formatBooleans(attributes, ['required']);
    if (result.values) result.values = JSON.stringify(result.values);
    return result;
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(
      data,
      formElementSchema,
      options.patch === true ? {} : { presence: 'required' },
    );
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
}, {
  filterFields: ['name', 'caption'],
  sortFields: [
    'elements.id',
    'elements.type',
    'elements.required',
    'elements.name',
    'elements.caption',
  ],
});

export default Bookshelf.model('FormElement', FormElement);
