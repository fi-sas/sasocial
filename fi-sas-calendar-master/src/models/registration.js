/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import registrationSchema from '../schemas/registration-schema';

import Event from './event';
import Form from './form';
import FormElement from './form_element';
import RegistrationAnswer from './registration_answer';

const Registration = Bookshelf.Model.extend({
  tableName: 'registration',
  hasTimestamps: true,

  events() {
    return this.belongsTo(Event);
  },

  answers() {
    return this.hasMany(RegistrationAnswer);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.assignPendingStatus);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  /**
   * Assign the Pending status to instance being saved.
   */
  assignPendingStatus() {
    if (this.isNew()) this.set('status', 'Pending');
  },

  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, registrationSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate the event id provided, if it has changed
    if (this.isNew() || this.hasChanged('event_id')) {
      try {
        await Event.validateExistence(data.event_id, 'Event');
      } catch (err) {
        throw new Errors.ValidationError([{
          code: Codes.MissingRelatedEntity,
          message: 'The event you are trying to register does not exist.',
        }]);
      }

      // Find the event
      const event = await Event.forge().where({ id: data.event_id }).fetch();

      // Validate if event allows registrations
      if (!event.get('allow_registration')) {
        throw new Errors.ValidationError([{
          code: Codes.InvalidOperation,
          message: 'The event you are trying to register to does not allow registrations.',
        }]);
      }

      // Validate event max registrations
      const maxRegistrations = event.get('max_registrations');
      if (maxRegistrations > 0) {
        const currRegistrations = Registration.forge().where({
          status: 'Approved',
          event_id: data.event_id,
        }).count();

        if (currRegistrations >= maxRegistrations) {
          throw new Errors.ValidationError([{
            code: Codes.InvalidOperation,
            message: 'The event you are trying to register has already reached its maximum of registrations.',
          }]);
        }
      }

      // Check if event requires the approval of the registration - if not,
      // automativally approve the registration
      const eventNeedsApproval = event.get('registration_approval_needed');
      if (!eventNeedsApproval) this.set({ status: 'Approved' });

      // Find the id of the form associated with the Event
      const formId = event.get('registration_form_id');
      if (formId) {
        const form = await Form.forge().where({ id: formId }).fetch();
        if (!form) {
          throw new Errors.ValidationError([{
            code: Codes.InvalidOperation,
            message: `The Form (${formId}) associated with the Event (${event.get('id')}) does not exist.`,
          }]);
        }

        // Find form elements
        const elements = await FormElement.forge().where({ form_id: formId }).fetchAll();

        // Validate required fields of the Form
        elements.forEach((el) => {
          const elId = el.get('id');
          const answer = options.answers.find(ans => ans.element_id === elId);
          if (el.get('required') && !answer) {
            throw new Errors.ValidationError([{
              code: Codes.InvalidOperation,
              message: `The Form Element "${el.get('name')}" requires a value.`,
            }]);
          }
        });
      }
    }
  },

  destroyCascade(model, o) {
    const opts = { transacting: o.transacting };
    return this.load('answers', opts)
      .then(it => it.related('answers').invokeThen('destroy', opts));
  },

  async saveWithRelated(data, answers, opts = {}) {
    const withRelated = ['answers'];
    const partial = (opts.patch === true);

    /*
     * Validate answers format and existence (if needed)
     */
    if (answers && !(answers instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'answers',
        message: 'answers must be an array',
      }]);
    }

    return Bookshelf.transaction(t => this.save(
      data,
      Object.assign(opts, { transacting: t, answers }),
    ).tap((item) => {
      if (answers) {
        // Load answers
        return item.load('answers', { transacting: t });
      }
      return true;
    }).tap((item) => {
      if (answers && !partial) {
        // Remove existing answers unless partial update
        return item.related('answers').invokeThen('destroy', { transacting: t });
      }
      return true;
    }).tap((item) => {
      if (answers) {
        if (!partial) {
          // Directly insert received answers
          return Promise.all(answers.map((tr) => {
            const elData = Object.assign(tr, { registration_id: item.get('id') });
            return item.related('answers').create(elData, { transacting: t });
          }));
        }
        // If partial, since existing answers were not remove, handle received
        // answers to update or insert each
        return Promise.all(answers.map((tr) => {
          const elData = Object.assign(tr, { registration_id: item.get('id') });
          const element = item.related('answers').find(el => el.get('name') === tr.name);
          if (!element) {
            return item.related('answers').create(elData, { transacting: t });
          }
          return element.save(elData, { transacting: t });
        }));
      }
      return true;
    })).tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Registration not found');
      });
  },
}, {
  filterFields: ['id', 'event_id', 'created_at', 'updated_at'],
  sortFields: ['id', 'event_id', 'created_at', 'updated_at'],
  defaultWithRelated: ['answers'],
  withRelated: ['event', 'answers'],
});

export default Bookshelf.model('Registration', Registration);
