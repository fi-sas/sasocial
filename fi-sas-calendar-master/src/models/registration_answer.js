/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import registrationAnswerSchema from '../schemas/registration-answer-schema';

import Registration from './registration';
import FormElement from './form_element';

const RegistrationAnswer = Bookshelf.Model.extend({
  tableName: 'registration_answer',
  hasTimestamps: true,

  registration() {
    return this.belongsTo(Registration);
  },

  form_element() {
    return this.belongsTo(FormElement, 'element_id');
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(
      data,
      registrationAnswerSchema,
      options.patch === true ? {} : { presence: 'required' },
    );
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    try {
      await FormElement.validateExistence(this.get('element_id'), 'Form Element');
    } catch (err) {
      throw new Errors.ValidationError([{
        code: Codes.MissingRelatedEntity,
        message: 'The Form Element does not exist.',
      }]);
    }

    // Validate the existance of the
  },
}, {
  filterFields: ['name', 'caption'],
  sortFields: [
    'elements.id',
    'elements.type',
    'elements.required',
    'elements.name',
    'elements.caption',
  ],
});

export default Bookshelf.model('RegistrationAnswer', RegistrationAnswer);
