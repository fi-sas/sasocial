import supertestRequest from 'supertest';
import auth from '../../middleware/auth';
import scopesAuth from '../../middleware/scopePermissions';
import app from '../../app';

jest.mock('../../middleware/auth');
jest.mock('../../middleware/scopePermissions');

describe('GET index routes', () => {
  const request = supertestRequest(app);

  const validateErrorResponseFormat = (res) => {
    expect(res.body.status).toBe('error');
    expect(res.body.link).toBeDefined();
    expect(res.body.link).toEqual({});
    expect(res.body.data).toBeInstanceOf(Array);
    expect(res.body.data).toHaveLength(0);
    expect(res.body.errors).toBeInstanceOf(Array);
  };

  describe('GET / - get things', () => {
    const expectedThingProps = ['id', 'message'];

    it('should return JSON array', () => request.get('/')
      .expect(200)
      .then((res) => {
        // check that it sends back an array
        expect(res.body.data).toBeInstanceOf(Array);
      }));

    it('should return objs w/ correct props', () => request.get('/')
      .expect(200)
      .then((res) => {
        // check for the expected language properties
        const sampleKeys = Object.keys(res.body.data[0]);
        expectedThingProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
        });
      }));
  });

  describe('GET /created', () => {
    it('should return 201 Created', () => request.get('/created')
      .expect(201));
  });

  describe('GET /no-content', () => {
    it('should return 204 No Content', () => request.get('/no-content')
      .expect(204));
  });

  describe('GET /bad', () => {
    it('should return 400 Bad Request', () => request.get('/bad')
      .expect(400)
      .then(res => validateErrorResponseFormat(res)));
  });

  describe('GET /unauthorized', () => {
    it('should return 401 Unauthorized', () => request.get('/unauthorized')
      .expect(401)
      .then(res => validateErrorResponseFormat(res)));
  });

  describe('GET /onlyauth', () => {
    it('should return 401 Unauthorized if not authenticated', () => {
      auth.mockImplementationOnce(require.requireActual('../../middleware/auth').default);
      return request.get('/onlyauth')
        .expect(401)
        .then(res => validateErrorResponseFormat(res));
    });

    it('should return 200 OK if authenticated', () => request.get('/onlyauth')
      .expect(200));
  });

  describe('GET /onlyallowed', () => {
    it('should return 403 Forbidden if not autorized by scope permissions', () => {
      scopesAuth.mockImplementationOnce(require.requireActual('../../middleware/scopePermissions').default);
      return request.get('/onlyallowed')
        .expect(403)
        .then(res => validateErrorResponseFormat(res));
    });

    it('should return 200 OK if authorized by scope permissions', () => request.get('/onlyallowed')
      .expect(200));
  });

  describe('GET /forbidden', () => {
    it('should return 403 Forbidden', () => request.get('/forbidden')
      .expect(403)
      .then(res => validateErrorResponseFormat(res)));
  });

  describe('GET /not-found', () => {
    it('should return 404 Not Found', () => request.get('/not-found')
      .expect(404)
      .then(res => validateErrorResponseFormat(res)));
  });

  describe('GET /error', () => {
    it('should return 500 Server Error', () => request.get('/error')
      .expect(500)
      .then(res => validateErrorResponseFormat(res)));
  });
});
