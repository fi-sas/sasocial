/**
 * @swagger
 *
 * components:
 *  parameters:
 *    keyParameter:
 *      in: query
 *      name: key
 *      required: false
 *      schema:
 *        type: string
 *        enum: [admin_notification_email]
 *      description: Filter by Key.
 *  schemas:
 *    Configuration:
 *      type: object
 *      description: |
 *        Represents a configuration setting.<br>
 *        A setting is defined by its key and used in different contexts. For more information, check the documentation of each endpoint.
 *      properties:
 *        key:
 *          type: string
 *          enum: [admin_notification_email]
 *          nullable: false
 *          readOnly: true
 *          description: The Configuration key.
 *        value:
 *          anyOf:
 *            - type: string
 *            - type: number
 *            - type: integer
 *            - type: boolean
 *            - type: array
 *              items: {}
 *          nullable: false
 *          description: The Configuration value.
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update.
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation.
 */

import { Router } from 'express';

import { getPaginationMetaData } from '../helpers/metadata';
import Configuration from '../models/configuration';

const configsRouter = new Router();

/**
 * @swagger
 * /api/v1/configurations:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *      Returns the list of Configurations.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/keyParameter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configsRouter.get('/', (req, res, next) => {
  /*
   * Parse filters from boolean to integer and add partial filter behaviour
   */
  req.parameters.parseFilters(
    {
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  /*
   * Validate filters, sorting and withRelated parameters
   */
  req.parameters.validate(Configuration.filterFields, Configuration.sortFields, []);

  Configuration.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 * /api/v1/configurations/admin_notification_email:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *      Returns the list of configured notification email addresses, used to notify of events
 *      awaiting approval (in case of a new or edited Event).
 *     produces:
 *       - application/json
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    allOf:
 *                    - $ref: '#/components/schemas/Configuration'
 *                    - type: object
 *                      properties:
 *                        value:
 *                          type: array
 *                          items:
 *                            type: string
 *                          example: ['admin@admin.com']
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '404':
 *        $ref: '#/components/responses/NotFound'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configsRouter.get('/admin_notification_email', (req, res, next) => Configuration.getConfigInstance('admin_notification_email', true)
  .then(conf => res.formatter.ok(conf.toJSON()))
  .catch(next));

/**
 * @swagger
 *  /api/v1/configurations/admin_notification_email:
 *    post:
 *      tags:
 *        - Configurations
 *      description: >
 *        Sets a list of email addresses to be used to notify of events
 *        awaiting approval (in case of a new or edited Event)
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Configuration Value data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *                - $ref: '#/components/schemas/Configuration'
 *                - type: object
 *                  properties:
 *                    value:
 *                      type: array
 *                      items:
 *                        type: string
 *                      example: ['admin@admin.com']
 *      responses:
 *        '201':
 *         description: Created Configuration object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
configsRouter.post('/admin_notification_email', (req, res, next) => {
  const { value } = req.body;

  return Configuration.getConfigInstance('admin_notification_email')
    .then(c => c.save({ value }))
    .then(c => res.formatter.created(c.toJSON({ omitPivot: true })))
    .catch(next);
});

export default configsRouter;
