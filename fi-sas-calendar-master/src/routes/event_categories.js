/**
 * @swagger
 * components:
 *   schemas:
 *     EventCategory:
 *      description: Represents a Category of calendarized events.
 *      allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              nullable: false
 *              description: The Event Category ID.
 *              example: 1
 *        - $ref: '#/components/schemas/NewEventCategory'
 *        - type: object
 *          properties:
 *            updated_at:
 *              type: string
 *              format: date
 *              nullable: false
 *              description: The date of the last update on the Event Category.
 *              example: 2018-09-01T09:00:00.000Z
 *            created_at:
 *              type: string
 *              format: date
 *              nullable: false
 *              description: The date of creation of the Event Category.
 *              example: 2018-09-01T09:00:00.000Z
 *
 *     NewEventCategory:
 *       type: object
 *       properties:
 *        color:
 *          type: string
 *          nullable: false
 *          description: The hexidecimal color code of the Event Category.
 *          example: 1
 *        translations:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/EventCategoryTranslation'
 *          nullable: false
 *          description: The Event translations.
 *
 *     EventCategoryTranslation:
 *      type: object
 *      properties:
 *        language_id:
 *          type: integer
 *          description: The ID of the language for this EventCategoryTranslation.
 *          example: 1
 *        name:
 *          type: string
 *          nullable: false
 *          description: Translated name of the Event Category.
 *          example: Cultura
 *        description:
 *          type: string
 *          nullable: false
 *          description: Translated description of the Event Category.
 *          example: Eventos culturais.
 */

import { Router } from 'express';

// import Bookshelf from '../bookshelf';
import asyncMiddleware from '../middleware/async';
// import { NotFoundError, ValidationError } from '../errors';
// import * as Codes from '../error_codes';
// import { getPaginationMetaData } from '../helpers/metadata';
// import { getData } from '../helpers/common';
// import { access } from '../helpers/communication';

// import Alert from '../models/alert';
// import AlertType from '../models/alert_type';


const categoryRouter = new Router();

/**
 * @swagger
 * /api/v1/event-categories:
 *   get:
 *     tags:
 *       - Event Categories
 *     description: |
 *      Returns a list of Event Categories for the specified language in headers.
 *      When no language is sent, all categories translations in all languages are returned,
 *      otherwise, only the categories of the specified language are returned.
 *
 *      -----
 *
 *      Sorting of the results is possible via query param. Category endpoints support sorting by
 *      fields of the main model (such as **created_at**, **updated_at** and **id**), as well as
 *      translation fields (referred to by **translations.name**, for instance).
 *       **NOTE: Sorting by translation attributes is only allowed if a language is provided
 *      (either via X-Language-ID or x-Language-Acronym). If no language is provided, sorting is
 *      only allowed by the category main attributes.**
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/languageIdHeader'
 *       - $ref: '#/components/parameters/languageAcronHeader'
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - in: query
 *         name: color
 *         required: false
 *         schema:
 *          type: string
 *         description: Filter by Category color.
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/EventCategory'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
categoryRouter.get('/', asyncMiddleware(async (req, res, next) => {
  // // Parse filters to add partial filter behaviour
  // req.parameters.parseFilters(
  //   {
  //     active: v => (v === 'true' ? 1 : 0),
  //     name: v => `%${v}%`,
  //     updated_at: v => `%${v}%`,
  //     created_at: v => `%${v}%`,
  //   },
  // );

  // // Find current context languageId
  // const languageId = await getLanguageId(req);

  // // Allowed sort fields depend on if a language was provided
  // const sortFields = (languageId === null)
  //   ? Category.sortFields : Category.sortFields + CategoryTranslation.sortFields;

  // // Add join with category translations table to allow sorting by its attributes
  // if (languageId !== null) {
  //   req.parameters.joins = [{
  //     table: 'category_translation',
  //     alias: 'translations',
  //     condition: (qb) => {
  //       qb.on('category.id', '=', 'translations.category_id')
  //         .andOn('translations.language_id', '=', languageId);
  //     },
  //   }];
  // }

  // // Validate filters and sorting parameters
  // req.parameters.validate(Category.filterFields + CategoryTranslation.filterFields, sortFields);

  // // Set default withRelated parameter
  // req.parameters.withRelated = req.parameters.withRelated || ['translations'];

  // const translations = await Category.forge()
  //   .loadTranslations([{ name: 'category', filters: CategoryTranslation.filterFields }], req.parameters, languageId);

  // translations.filterOrderAndFetch(req.parameters)
  //   .then((collection) => {
  //     const data = collection ? collection.toJSON() : [];
  //     const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
  //     return res.formatter.ok(data, link);
  //   })
  //   .catch(next);
}));

/**
 * @swagger
 *  /api/v1/event-categories:
 *    post:
 *      tags:
 *        - Event Categories
 *      description: >
 *        Creates new Event Category.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Event Category data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/NewEventCategory'
 *      responses:
 *        '201':
 *         description: Created EventCategory object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/EventCategory'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
categoryRouter.post('/', asyncMiddleware(async (req, res, next) => {
  // // Category data that will be stored in the database
  // const data = {
  //   active: req.body.active,
  //   weight: req.body.weight,
  //   created_at: new Date(),
  //   updated_at: new Date(),
  // };

  // const category = Category.forge(data);
  // const translations = (req.body.translations || []);

  // Bookshelf.transaction(async (trx) => {
  //   const item = await category.save(data, { method: 'insert', transacting: trx });

  //   await Promise.all(translations.map(
  //     async tr => item.related('translations').create(tr, { transacting: trx }),
  //   ));

  //   return item;
  // })
  //   .tap(item => item.refresh(['translations']))
  //   .then(item => res.formatter.created(item.toJSON({ omitPivot: true })))
  //   .catch(err => next(err));
}));

/**
 * @swagger
 *  /api/v1/event-categories/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Event Category.
 *    get:
 *      tags:
 *        - Event Categories
 *      description: >
 *        Returns the Event Category with the provided ID for the specified language in headers.
 *        When no language is sent, all translations in all languages are returned,
 *        otherwise, only the Event in the specified language is returned.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/EventCategory'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
categoryRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  // const languageId = await getLanguageId(req);

  // const translations = await Category.forge().where('id', req.params.id)
  //   .loadTranslations(['category'], req.parameters, languageId);
  // translations.fetch({ require: true, withRelated: req.parameters.withRelated })
  //   .then(item => res.formatter.ok(item.toJSON()))
  //   .catch(Bookshelf.NotFoundError, () => { throw new NotFoundError('Category not found.'); })
  //   .catch(next);
}));

/**
 * @swagger
 *  /api/v1/event-categories/{id}:
 *    put:
 *      tags:
 *        - Event Categories
 *      description: >
 *        Updates an Event Category.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Event Category updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/NewEventCategory'
 *      responses:
 *        '200':
 *          description: Updated EventCategory object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/EventCategory'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
categoryRouter.put('/:id', async (req, res, next) => {
  // // Category data that will be stored in the database
  // const data = {
  //   active: req.body.active,
  //   weight: req.body.weight,
  //   updated_at: new Date(),
  // };

  // const category = Category.forge({ id: req.params.id });
  // const translations = req.body.translations || [];

  // Bookshelf.transaction(t => category.save(data, { method: 'update', patch: false, transacting: t })
  //   .tap(item => item.load('translations', { transacting: t }))
  //   .tap(item => item.related('translations').invokeThen('destroy'))
  //   .tap(async (item) => {
  //     await Promise.all(translations.map(
  //       translation => item.related('translations').create(translation, { transacting: t }),
  //     ));
  //     return item;
  //   }))
  //   .tap(item => item.refresh(['translations']))
  //   .then((item) => {
  //     res.formatter.ok(item.toJSON());
  //   })
  //   .catch(Bookshelf.NoRowsUpdatedError, () => {
  //     throw new NotFoundError('Category not found');
  //   })
  //   .catch(next);
});

/**
 * @swagger
 *  /api/v1/event-categories/{id}:
 *    patch:
 *      tags:
 *        - Event Categories
 *      description: >
 *        Partially updates an Event Category.
 *
 *
 *        **NOTE:** Translations will be updated/incremented with the received translations.
 *        That means that, for instance, if the Category had 2 associated translations,
 *         and the PATCH body provides only one (existing) translation,
 *        then the returned result will be two translations - the one that existed previously, and
 *        the one that was provided with the values that were provided in the request body.
 *        If you don't want this behavior, please use PUT endpoint
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Event Category data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/NewEventCategory'
 *      responses:
 *        '200':
 *          description: Updated EventCategory object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/EventCategory'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
categoryRouter.patch('/:id', async (req, res, next) => {
  // // Category data that will be stored in the database
  // const data = req.body;
  // data.updated_at = new Date();

  // const category = Category.forge({ id: req.params.id });
  // const { translations } = req.body;

  // // Remove non editable category data
  // delete data.id;
  // delete data.translations;

  // Bookshelf.transaction(t => category.save(data, { method: 'update', patch: true, transacting: t })
  //   .tap(item => item.load('translations', { transacting: t }))
  //   .tap((item) => { // eslint-disable-line consistent-return
  //     if (translations) { return item.related('translations').invokeThen('destroy'); }
  //   })
  //   .tap(async (item) => { // eslint-disable-line consistent-return
  //     if (translations) {
  //       return Promise.all(translations.map((tr) => {
  //         const translationData = Object.assign(tr, { category_id: item.get('id') });
  //         const translation = item.related('translations').find(el => el.get('category_id') === tr.category_id);
  //         if (!translation) {
  //           return item.related('translations').create(translationData, { transacting: t });
  //         }
  //         return translation.save(translationData, { transacting: t });
  //       }));
  //     }
  //   }))
  //   .tap(item => item.refresh(['translations']))
  //   .then((item) => {
  //     res.formatter.ok(item.toJSON());
  //   })
  //   .catch(Bookshelf.NoRowsUpdatedError, () => {
  //     throw new NotFoundError('Category not found');
  //   })
  //   .catch(next);
});

/**
 * @swagger
 *  /api/v1/event-categories/{id}:
 *    delete:
 *      tags:
 *        - Event Categories
 *      description: >
 *        Deletes an Event Category. If the specified
 *        category contains associated events, a Bad Request is sent.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
categoryRouter.delete('/:id', (req, res, next) => {
  // Bookshelf.transaction(t => Category.forge()
  //   .where('id', req.params.id)
  //   .fetch({ require: true })
  //   .then(item => item.destroy({ transacting: t })))
  //   .then(() => res.formatter.noContent())
  //   .catch(Bookshelf.NotFoundError, () => { throw new NotFoundError('Category not found.'); })
  //   .catch(next);
});

export default categoryRouter;
