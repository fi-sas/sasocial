  /**
 * @swagger
 * definitions:
 *  EventRecurrence:
 *   type: string
 *   enum: &RECURRENCE
 *     - None
 *     - Daily
 *     - Weekly
 *     - Monthly
 *     - Annually
 *
 * components:
 *   parameters:
 *    idFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific IDs.
 *    nameFilter:
 *      in: query
 *      name: name
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial translated name.
 *    descriptionFilter:
 *      in: query
 *      name: description
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial translated description.
 *    urlFilter:
 *      in: query
 *      name: url
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial translated url.
 *    addressFilter:
 *      in: query
 *      name: address
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial address.
 *    serviceIdFilter:
 *      in: query
 *      name: service_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by service ID.
 *    categoryIdFilter:
 *      in: query
 *      name: category_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by category ID.
 *    minStartDateFilter:
 *      in: query
 *      name: min_start_date
 *      required: false
 *      schema:
 *        type: string
 *      description: |
 *        Filter by minimum start date (complete date value is required).
 *      example: 2018-12-01T23:59:59.000Z
 *    maxStartDateFilter:
 *      in: query
 *      name: max_start_date
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by maximum start date (complete date value is required).
 *      example: 2018-12-31T00:00:00.000Z
 *    publishedFilter:
 *      in: query
 *      name: published
 *      required: false
 *      schema:
 *        type: boolean
 *      description: |
 *        Filter by published elements (active according to its publishing dates and currently in
 *        Approved status).
 *    cancelledFilter:
 *      in: query
 *      name: cancelled
 *      required: false
 *      schema:
 *        type: boolean
 *      description: Filter by cancelled Events.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedEvent:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, 'translations', 'contacts' and 'medias'
 *        are requested. Value of false disables the load of related entities. Possible values
 *        include 'translations', 'contacts', 'medias', 'category', 'registration_form',
 *        'registrations', 'interested' and 'registration_form.elements'.
 *   schemas:
 *     Event:
 *      description: >
 *        Represents a calendarized event from a MS.
 *
 *
 *        A new or edited Event is automatically assigned to a Pending status, waiting to be
 *        approved or rejected by specific command.
 *
 *
 *        An Event is only visible as a search result when its status is Approved and the publishing
 *        start and end date match the current server date.
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Event numeric identifier.
 *          example: 1
 *        service_id:
 *          type: number
 *          nullable: false
 *          description: The ID of the service (MS) responsible for the Event.
 *          example: 1
 *        category_id:
 *          type: number
 *          nullable: false
 *          description: The ID of the Event Category to which the Event belongs.
 *          example: 1
 *        publish_start_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: >
 *            The date from which the Event is to be visible (published), as long as it is in
 *            an Approved status.
 *          example: 2018-12-05T19:00:00.000Z
 *        publish_end_date:
 *          type: string
 *          format: date
 *          nullable: true
 *          description: >
 *            The date from which the Event is to be not visible (unpublished).
 *          example: 2018-12-10T19:00:00.000Z
 *        start_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: >
 *            The starting date of the Event.
 *          example: 2018-12-05T19:00:00.000Z
 *        end_date:
 *          type: string
 *          format: date
 *          nullable: true
 *          description: >
 *            The (optional) expected ending date of the Event.
 *          example: 2018-12-10T19:00:00.000Z
 *        recurrence:
 *          type: string
 *          enum: *RECURRENCE
 *          nullable: false
 *          description: The frequency of the recurrence of the Event.
 *        notifications_enabled:
 *          type: boolean
 *          nullable: false
 *          description: True if Event has enabled notifications.
 *          example: false
 *        notification_target_id:
 *          type: integer
 *          nullable: true
 *          description: >
 *            The target ID used for the Event's notifications. If null and Event has enabled
 *            notifications, target ID of service will be used to issue the notifications.
 *          example: 1
 *        notification_alert_type_key:
 *          type: string
 *          nullable: true
 *          description: >
 *            The key of the alert type used as Event notification. If not defined, the key for
 *            "CALENDAR_EVENT_NOTIFICATION" will be assumed. If a diferent key is specified,
 *            the corresponding Alert Type must be properly configured in MS Notifications.
 *          example: ""
 *        notification_start_date:
 *          type: string
 *          format: date
 *          nullable: true
 *          description: >
 *            The starting date for notifications of the Event. If "notifications_enabled" is true,
 *            start date cannot be null.
 *          example: 2019-12-00T00:00:00.000Z
 *        number_of_notifications:
 *          type: integer
 *          nullable: true
 *          description: |
 *            Number of notifications of the Event. If "notifications_enabled" is true,
 *            number of notifications cannot be null.
 *          example: 1
 *        notification_recurrence:
 *          type: string
 *          enum: *RECURRENCE
 *          nullable: true
 *          description: The recurrence for notifications of the Event.
 *          example: 'None'
 *        longitude:
 *          type: string
 *          nullable: false
 *          description: The Event's locale's longitude.
 *          example: 46.414382
 *        latitude:
 *          type: string
 *          nullable: false
 *          description: The Event's locale's latitude.
 *          example: 10.013988
 *        address:
 *          type: string
 *          nullable: false
 *          description: The  Event's locale's address.
 *          example: Largo do Cais
 *        address_no:
 *          type: string
 *          nullable: false
 *          description: The  Event's locale's address number.
 *          example: 120 2ºD
 *        city:
 *          type: string
 *          nullable: false
 *          description: The Event's locale's city.
 *          example: Lisboa
 *        allow_interest:
 *          type: boolean
 *          nullable: false
 *          description: True if Event has enabled interest marking.
 *          example: true
 *        allow_registration:
 *          type: boolean
 *          nullable: false
 *          description: True if Event has enabled registration.
 *          example: false
 *        max_registrations:
 *          type: number
 *          nullable: true
 *          description: |
 *            Maximum number of registrations (assuming Event allows registration).
 *          example: 20
 *        registration_approval_needed:
 *          type: boolean
 *          nullable: true
 *          description: |
 *            True if registration manual approval is required. False if every registration is
 *            automatically approved (assigned to an Approved status). If "allow_registration"
 *            is true, "registration_approval_needed" cannot be null.
 *          example: false
 *        registration_form_id:
 *          type: number
 *          nullable: true
 *          description: ID of the RegistrationForm used by the Event.
 *          example: 1
 *        registration_fee:
 *          type: number
 *          nullable: true
 *          description: |
 *            Event registration fee. Must be not be null when "allow_registration" is true.
 *          example: 10
 *        registration_tax_id:
 *          type: number
 *          nullable: true
 *          description: |
 *            Event registration fee's tax ID. If "registration_fee" holds a positive value, tax
 *            ID cannot be null.
 *          example: 1
 *        registration_code:
 *          type: string
 *          nullable: true
 *          description: |
 *            Code for Event registration (in case payment is needed, will be used as article
 *            code).
 *          example: ''
 *        cancelled:
 *          type: boolean
 *          nullable: false
 *          description: True if Event has been cancelled (but still in Approved status).
 *          example: false
 *        status:
 *          type: string
 *          enum: [Pending, Approved, Rejected]
 *          readOnly: true
 *          description: The current status of the Event.
 *          example: Pending
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update on the Event.
 *          example: 2018-09-01T09:00:00.000Z
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Event.
 *          example: 2018-09-01T09:00:00.000Z
 *        contacts:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/EventContact'
 *          nullable: false
 *          description: The Event contacts.
 *        translations:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/EventTranslation'
 *          nullable: false
 *          description: The Event translations.
 *        medias:
 *          type: array
 *          nullable: false
 *          description: The list of media of the Event.
 *        is_interested:
 *          type: boolean
 *          nullable: true
 *          readOnly: true
 *          description: |
 *            True if current authenticated User marked interest in the Event.
 *        is_registered:
 *          type: boolean
 *          nullable: true
 *          readOnly: true
 *          description: |
 *            True if current authenticated User is registered in the Event (Registration may
 *            or not be Pending).
 *        category:
 *          type: object
 *          nullable: false
 *          readOnly: true
 *          description: The Event Category.
 *          allOf:
 *            - $ref: '#/components/schemas/EventCategory'
 *        registration_form:
 *          type: object
 *          nullable: true
 *          readOnly: true
 *          description: Registration Form for the Event.
 *          allOf:
 *            - $ref: '#/components/schemas/RegistrationForm'
 *        registrations:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/Registration'
 *          nullable: false
 *          readOnly: true
 *          description: List of registrations of the Event.
 *        interested:
 *          type: array
 *          items:
 *            type: number
 *          nullable: false
 *          readOnly: true
 *          description: List of IDs of the users that marked interest in the Event.
 *
 *     EventTranslation:
 *      type: object
 *      properties:
 *        language_id:
 *          type: integer
 *          description: The ID of the language for this EventTranslation.
 *          example: 1
 *        name:
 *          type: string
 *          nullable: false
 *          description: Translated name of the Event.
 *          example: Encontro mensal
 *        description:
 *          type: string
 *          nullable: false
 *          description: Translated description of the Event.
 *          example: Encontro mensal - descrição.
 *        url:
 *          type: string
 *          nullable: false
 *          description: External link to event's page.
 *          example: ""
 *
 *     EventMedia:
 *      type: object
 *      properties:
 *        file_id:
 *          type: integer
 *          nullable: false
 *          description: The file ID of the configured media.
 *
 *     EventContact:
 *      type: object
 *      properties:
 *        type:
 *          type: string
 *          enum: [Email, Phone]
 *          nullable: false
 *          description: The Contact type.
 *          example: Email
 *        value:
 *          type: string
 *          nullable: false
 *          description: The Contact value.
 *          example: admin@mail.com
 */

import { Router } from 'express';
import Joi from 'joi';

import Bookshelf from '../bookshelf';
import asyncMiddleware from '../middleware/async';
import { NotFoundError, ParameterError, ValidationError } from '../errors';
import * as Codes from '../error_codes';
import { getPaginationMetaData } from '../helpers/metadata';
import { getData, getLanguageId } from '../helpers/common';
import { alert } from '../helpers/communication';
import { getICS } from '../helpers/eventExporter';
import backoffice from '../middleware/backoffice';

import Event from '../models/event';
import Configuration from '../models/configuration';
import EventTranslation from '../models/event_translation';
import EventInterest from '../models/event_interest';


const eventsRouter = new Router();

/**
 * @swagger
 * /api/v1/events:
 *   get:
 *     tags:
 *       - Events
 *     description: |
 *      Returns a list of Events for the specified language in headers.<br>
 *      When no language is sent, all events translations in all languages are returned,
 *      otherwise, only the events of the specified language are returned.<br>
 *      By default, no filter is applied to the returned Events. In order to fetch current
 *      Events, filters as `published` and `min_start_date` and `max_start_date` are specially
 *      relevant.<br>
 *
 *      -----
 *
 *      Sorting of the results is possible via query param. Event endpoints support sorting by
 *      fields of the main model (such as `created_at`, `updated_at` and `id`), as well as
 *      translation fields (referred to by `translations.name`, for instance).<br>
 *       **Note: Sorting by translation attributes is only allowed if a language is provided
 *      (either via `X-Language-ID` or `X-Language-Acronym`). If no language is provided, sorting is
 *      only allowed by the Event main attributes.**
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/languageIdHeader'
 *       - $ref: '#/components/parameters/languageAcronHeader'
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/addressFilter'
 *       - $ref: '#/components/parameters/serviceIdFilter'
 *       - $ref: '#/components/parameters/categoryIdFilter'
 *       - $ref: '#/components/parameters/minStartDateFilter'
 *       - $ref: '#/components/parameters/maxStartDateFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/descriptionFilter'
 *       - $ref: '#/components/parameters/urlFilter'
 *       - $ref: '#/components/parameters/publishedFilter'
 *       - $ref: '#/components/parameters/cancelledFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/IdFilter'
 *       - $ref: '#/components/parameters/withRelatedEvent'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
eventsRouter.get('/', asyncMiddleware(async (req, res, next) => {
  /*
   * Find current context languageId
   */
  const languageId = await getLanguageId(req);

  /*
   * Parse filters to add partial filter behaviour
   */
  req.parameters.parseFilters(
    {
      address: v => `%${v}%`,
      name: v => `%${v}%`,
      description: v => `%${v}%`,
      url: v => `%${v}%`,
      cancelled: v => ((v === 'true') ? 1 : 0),
      updated_at: v => `%${v}%`,
      created_at: v => `%${v}%`,
    },
  );

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated || Event.defaultWithRelated.slice();

  /*
   * Allowed sort fields depend on if a language was provided
   */
  const sortFields = (languageId === null)
    ? Event.sortFields : EventTranslation.sortFields + Event.sortFields;

  /*
   * Validate filters, sorting and withRelated parameters
   */
  req.parameters.validate(Event.filterFields + EventTranslation.filterFields, sortFields,
    Event.withRelated);

  const published = req.parameters.getFilterValue('published', true);
  const min = req.parameters.getFilterValue('min_start_date', true);
  const max = req.parameters.getFilterValue('max_start_date', true);

  /*
   * Further validate filters: min and max must be valid iso dates
   */
  const errors = [];

  let v = Joi.validate(min, Joi.date().iso().optional());
  if (min && v.error) {
    errors.push({
      code: Codes.ValidationInvalidFormat,
      message: '"min_start_date" filter parameter must be a valid iso date',
      parameter: 'min_start_date',
    });
  }
  v = Joi.validate(max, Joi.date().iso().optional());
  if (max && v.error) {
    errors.push({
      code: Codes.ValidationInvalidFormat,
      message: '"max_start_date" filter parameter must be a valid iso date',
      parameter: 'max_start_date',
    });
  }

  if (errors.length) {
    throw new ParameterError(errors);
  }

  /*
   * Add join with category translations table to allow sorting by its attributes
   */
  if (languageId !== null) {
    req.parameters.joins = [{
      table: 'category_translation',
      alias: 'translations',
      condition: (qb) => {
        qb.on('category.id', '=', 'translations.category_id')
          .andOn('translations.language_id', '=', languageId);
      },
    }];
  }

  /*
   * Add withRelated to fetch is_interested and is_registered
   */
  if (!(req.parameters.withRelated instanceof Array)) {
    req.parameters.withRelated = [];
  }

  req.parameters.withRelated.unshift({
    is_interested: qb => qb.where('event_interest.user_id', req.authInfo.user.id),
    is_registered: qb => qb.where('registration.user_id', req.authInfo.user.id),
  });

  return Event.forge().published(published)
    .between(min ? new Date(min) : false, max ? new Date(max) : false)
    .loadTranslations([{ name: 'event', filters: EventTranslation.filterFields }], req.parameters, languageId)
    .then(translations => translations.filterOrderAndFetch(req.parameters))
    .then((collection) => {
      const data = collection ? collection.toJSON() : [];
      const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
      return res.formatter.ok(data, link);
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events:
 *    post:
 *      tags:
 *        - Events
 *      description: >
 *        Creates a new Event.<br>
 *        An Event is created in a Pending status, waiting to be approved into an Approved status,
 *        or rejected into a Rejected status by an authorized user. <br>
 *        When a new Event is inserted, the configured admin email addresses
 *        (`admin_notification_email` configuration) are are notified of a
 *        "CALENDAR_EVENT_APPROVAL_NEEDED" alert.<br>
 *        Until the Event is approved, it will not be visible as a "published" result.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Event data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Event'
 *      responses:
 *        '201':
 *         description: Created Event object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.post('/', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, ['service_id', 'category_id', 'publish_start_date',
    'publish_end_date', 'start_date', 'end_date', 'recurrence', 'notifications_enabled',
    'notification_target_id', 'notification_alert_type_key', 'notification_start_date', 'cancelled',
    'number_of_notifications', 'allow_interest', 'notification_recurrence', 'latitude',
    'longitude', 'address', 'address_no', 'city', 'allow_registration', 'max_registrations',
    'registration_approval_needed', 'registration_form_id', 'registration_fee',
    'registration_tax_id', 'registration_code'], false);

  const translations = !(req.body.translations && req.body.translations instanceof Array)
    ? null
    : req.body.translations.map(m => getData(m, ['language_id', 'name', 'description', 'url'], false));
  const contacts = !(req.body.contacts && req.body.contacts instanceof Array)
    ? null
    : req.body.contacts.map(m => getData(m, ['type', 'value'], false));
  /*const medias = !(req.body.medias && req.body.medias instanceof Array)
    ? null
    : req.body.medias.map(m => getData(m, ['file_id'], false));*/

  const adminEmailAddresses = await Configuration.getConfig('admin_notification_email', true) || [];
  return Event.forge()
    .saveWithRelated(data, translations, contacts, medias, { userId: req.authInfo.user.id, method: 'insert' })
    .then((item) => {
      if (adminEmailAddresses && adminEmailAddresses.length) {
        // Alert admins of approval needed
        adminEmailAddresses.forEach((adminEmailAddress) => {
          try {
            alert(req, 'CALENDAR_EVENT_APPROVAL_NEEDED',
              { event: item }, null, [], { email: adminEmailAddress });
          } catch (e) {
            console.log(e);
          }
        });
      }
      return item;
    })
    .then((item) => {
      res.formatter.created(item.toJSON());
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Event.
 *    get:
 *      tags:
 *        - Events
 *      description: >
 *        Returns the Event with the provided ID for the specified language in headers.<br>
 *        When no language is sent, all translations in all languages are returned,
 *        otherwise, only the Event in the specified language is returned.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *        - $ref: '#/components/parameters/withRelatedEvent'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated || Event.defaultWithRelated.slice();

  /*
   * Validate withRelated parameter
   */
  req.parameters.validate(null, null, Event.withRelated);

  const languageId = await getLanguageId(req);
  const options = { require: true, withRelated: req.parameters.withRelated };

  /*
   * Add withRelated to fetch is_interested and is_registered
   */
  if (!(options.withRelated instanceof Array)) {
    options.withRelated = [];
  }

  options.withRelated.unshift({
    is_interested: qb => qb.where('event_interest.user_id', req.authInfo.user.id),
    is_registered: qb => qb.where('registration.user_id', req.authInfo.user.id),
  });

  return Event.forge()
    .where('id', req.params.id)
    .loadTranslations(['event'], options, languageId)
    .then(tr => tr.fetch(options))
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch((e) => {
      if (e instanceof Bookshelf.NotFoundError) {
        throw new NotFoundError('Event not found');
      }
      throw e;
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events/{id}/export:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Event.
 *    get:
 *      tags:
 *        - Events
 *      description: >
 *        Returns the URL for an ICS file of the Event with the provided ID for the specified language in headers.
 *        When no language is sent, a default translation will be used.<br>
 *        Only export of published Events are allowed - unless the request is Backoffice. If the Event cannot be exported,
 *        a Bad Request is returned.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      url:
 *                        type: string
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.get('/:id/export', backoffice, asyncMiddleware(async (req, res, next) => {
  /**
   * Get Event
   */
  const languageId = await getLanguageId(req);
  const options = { require: true, withRelated: ['category'] };

  const event = await Event.forge()
    .where('id', req.params.id)
    .loadTranslations(['event', 'category'], options, languageId)
    .then(tr => tr.fetch(options))
    .catch((e) => {
      if (e instanceof Bookshelf.NotFoundError) {
        throw new NotFoundError('Event not found');
      }
      throw e;
    })
    .catch(next);

  // Only allow export of published events outside backoffice
  if (!req.backoffice) {
    if (!event.isPublished()) {
      throw new ValidationError([{
        code: Codes.InvalidOperation,
        message: 'Only export of published Events is allowed.',
      }]);
    }
  }

  try {
    const base = process.env.EVENT_URL ? process.env.EVENT_URL : null;
    const file = await getICS(event, languageId);
    return res.formatter.ok({ url: `${base}/${file}` });
  } catch (e) {
    console.log(e);
    return res.formatter.serverError();
  }
}));

/**
 * @swagger
 *  /api/v1/events/{id}/interest:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Event.
 *    post:
 *      tags:
 *        - Events
 *      description: >
 *        Marks interest from the current authenticated User in the Event with the provided ID, as long as the
 *        Event allows interest and is currently published. If this is not the case, a Bad Response is returned.<br>
 *        Only available for users who are not Guest - if a Guest User tries to mark interest in an Event,
 *        a Bad Response is returned.
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *        - $ref: '#/components/parameters/withRelatedEvent'
 *      produces:
 *        - application/json
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.post('/:id/interest', asyncMiddleware(async (req, res, next) => {
  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated || Event.defaultWithRelated.slice();

  /**
   * Validate event existence
   */
  await Event.where({ id: req.params.id })
    .fetch({ require: true })
    .then((i) => {
      if (!i.isPublished()) {
        throw new ValidationError([{
          code: Codes.InvalidOperation,
          message: 'Event is not published and so it cannnot support interests.',
        }]);
      }
      if (i.get('allow_interest') === false) {
        throw new ValidationError([{
          code: Codes.InvalidOperation,
          message: 'Event does not allow interests.',
        }]);
      }
      return true;
    })
    .catch(Bookshelf.NoRowsUpdatedError, () => {
      throw new NotFoundError('Event not found');
    });

  /*
   * Validate withRelated parameter
   */
  req.parameters.validate(null, null, Event.withRelated);

  /**
   * Validate user (not Guest)
   */
  if (req.authInfo.isGuest) {
    throw new ValidationError([{
      code: Codes.ValidationRequiredValue,
      message: 'Guest user can not mark interest in Events',
    }]);
  }

  const languageId = await getLanguageId(req);
  const options = { require: true, withRelated: req.parameters.withRelated };

  /*
   * Add withRelated to fetch is_interested and is_registered
   */
  if (!(options.withRelated instanceof Array)) {
    options.withRelated = [];
  }

  options.withRelated.unshift({
    is_interested: qb => qb.where('event_interest.user_id', req.authInfo.user.id),
    is_registered: qb => qb.where('registration.user_id', req.authInfo.user.id),
  });

  return EventInterest.forge({ event_id: req.params.id, user_id: req.authInfo.user.id })
    .save()
    .then(() => Event.forge().where('id', req.params.id)
      .loadTranslations(['event'], options, languageId)
      .then(tr => tr.fetch(options)))
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events/{id}/interest:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Event.
 *    delete:
 *      tags:
 *        - Events
 *      description: >
 *        Removes interest of the current authenticated User in the Event with the provided ID.<br>
 *        Only available for users who are not Guest - if a Guest User tries to delete interest in an Event,
 *        a Bad Response is returned. Additionally, if no interest exist between the current User and the
 *        specified Event, a Bad Response is returned.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.delete('/:id/interest', asyncMiddleware(async (req, res, next) => {
  /**
   * Validate event existence
   */
  await Event.validateExistence(req.params.id, 'Event');

  /**
   * Validate user (not Guest)
   */
  if (req.authInfo.isGuest) {
    throw new ValidationError([{
      code: Codes.ValidationRequiredValue,
      message: 'Guest user can not delete interest in Events',
    }]);
  }

  return EventInterest.forge({ event_id: req.params.id, user_id: req.authInfo.user.id })
    .fetch({ require: true })
    .catch(Bookshelf.NotFoundError, () => {
      throw new ValidationError([{
        code: Codes.ResourceNotFound,
        message: 'The user is not interested in the event',
      }]);
    })
    .then(i => i.destroy())
    .then(() => res.formatter.noContent())
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events/{id}:
 *    put:
 *      tags:
 *        - Events
 *      description: >
 *        Updates an Event.<br>
 *        After an edition, the Event is updated into a Pending status, waiting to be approved
 *        into an Approved status, or rejected into a Rejected status by an authorized user.<br>
 *        When a new Event is edited, the configured admin email addresses
 *        (`admin_notification_email` configuration) are are notified of a
 *        "CALENDAR_EVENT_APPROVAL_NEEDED" alert.<br>
 *        Until the Event is approved, it will not be visible as a "published" result.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Event updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Event'
 *      responses:
 *        '200':
 *          description: Updated Event object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.put('/:id', async (req, res, next) => {
  const data = getData(req.body, ['service_id', 'category_id', 'publish_start_date',
    'publish_end_date', 'start_date', 'end_date', 'recurrence', 'notifications_enabled',
    'notification_target_id', 'notification_alert_type_key', 'notification_start_date', 'cancelled',
    'number_of_notifications', 'allow_interest', 'notification_recurrence', 'latitude',
    'longitude', 'address', 'address_no', 'city', 'allow_registration', 'max_registrations',
    'registration_approval_needed', 'registration_form_id', 'registration_fee',
    'registration_tax_id', 'registration_code'], false);

  const translations = !(req.body.translations && req.body.translations instanceof Array)
    ? null
    : req.body.translations.map(m => getData(m, ['language_id', 'name', 'description', 'url'], false));
  const contacts = !(req.body.contacts && req.body.contacts instanceof Array)
    ? null
    : req.body.contacts.map(m => getData(m, ['type', 'value'], false));
  const medias = !(req.body.medias && req.body.medias instanceof Array)
    ? null
    : req.body.medias.map(m => getData(m, ['file_id'], false));

  const adminEmailAddresses = await Configuration.getConfig('admin_notification_email', true) || [];

  // Set Pending status after edition
  data.status = 'Pending';

  return Event.forge({ id: req.params.id })
    .saveWithRelated(data, translations, contacts, medias, { userId: req.authInfo.user.id, method: 'update', patch: false })
    .then((item) => {
      if (adminEmailAddresses && adminEmailAddresses.length) {
        // Alert admins of approval needed
        adminEmailAddresses.forEach((adminEmailAddress) => {
          try {
            alert(req, 'CALENDAR_EVENT_APPROVAL_NEEDED',
              { event: item }, null, [], { email: adminEmailAddress });
          } catch (e) {
            console.log(e);
          }
        });
      }
      return item;
    })
    .then((item) => {
      res.formatter.ok(item.toJSON({ omitPivot: true }));
    })
    .catch(Bookshelf.NoRowsUpdatedError, () => {
      throw new NotFoundError('Event not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/events/{id}:
 *    patch:
 *      tags:
 *        - Events
 *      description: >
 *        Partially updates an Event.<br>
 *        After an edition, the Event is updated into a Pending status, waiting to be approved
 *        into an Approved status, or rejected into a Rejected status by an authorized user.<br>
 *        When a new Event is edited, the configured admin email addresses
 *        (`admin_notification_email` configuration) are are notified of a
 *        "CALENDAR_EVENT_APPROVAL_NEEDED" alert.<br>
 *        Until the Event is approved, it will not be visible as a "published" result.
 *        <br><br>
 *        **Note:** when PATCHing objects, all received relationships are deleted
 *        before adding the new provided values, except for translations.<br>
 *        Translations will be updated/incremented with the received translations.
 *        That means that, for instance, if the Event had 2 associated translations,
 *        and the PATCH body provides only one (existing) translation, then the returned
 *        result will be two translations - the one that existed previously, and the one
 *        that was provided with the values that were provided in the request body.
 *        If you don't want this behavior, please use PUT endpoint
 *        Updates an Event.<br>
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Event Category data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Event'
 *      responses:
 *        '200':
 *          description: Updated Event object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.patch('/:id', async (req, res, next) => {
  const data = getData(req.body, ['service_id', 'category_id', 'publish_start_date',
    'publish_end_date', 'start_date', 'end_date', 'recurrence', 'notifications_enabled',
    'notification_target_id', 'notification_alert_type_key', 'notification_start_date', 'cancelled',
    'number_of_notifications', 'allow_interest', 'notification_recurrence', 'latitude',
    'longitude', 'address', 'address_no', 'city', 'allow_registration', 'max_registrations',
    'registration_approval_needed', 'registration_form_id', 'registration_fee',
    'registration_tax_id', 'registration_code'], true);

  const translations = !(req.body.translations && req.body.translations instanceof Array)
    ? null
    : req.body.translations.map(m => getData(m, ['language_id', 'name', 'description', 'url'], false));
  const contacts = !(req.body.contacts && req.body.contacts instanceof Array)
    ? null
    : req.body.contacts.map(m => getData(m, ['type', 'value'], false));
  const medias = !(req.body.medias && req.body.medias instanceof Array)
    ? null
    : req.body.medias.map(m => getData(m, ['file_id'], false));

  const adminEmailAddresses = await Configuration.getConfig('admin_notification_email', true) || [];

  // Set Pending status after edition
  data.status = 'Pending';

  return Event.forge({ id: req.params.id })
    .saveWithRelated(data, translations, contacts, medias, { userId: req.authInfo.user.id, method: 'update', patch: true })
    .then((item) => {
      if (adminEmailAddresses && adminEmailAddresses.length) {
        // Alert admins of approval needed
        adminEmailAddresses.forEach((adminEmailAddress) => {
          try {
            alert(req, 'CALENDAR_EVENT_APPROVAL_NEEDED',
              { event: item }, null, [], { email: adminEmailAddress });
          } catch (e) {
            console.log(e);
          }
        });
      }
      return item;
    })
    .then((item) => {
      res.formatter.ok(item.toJSON({ omitPivot: true }));
    })
    .catch(Bookshelf.NoRowsUpdatedError, () => {
      throw new NotFoundError('Event not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/events/{id}:
 *    delete:
 *      tags:
 *        - Events
 *      description: >
 *        Deletes an Event. If the specified the Event is currently in
 *        Approved status, a Bad Request is returned.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
eventsRouter.delete('/:id', (req, res, next) => {
  Bookshelf.transaction(t => Event.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => { throw new NotFoundError('Event not found.'); })
    .catch(next);
});


/**
 * @swagger
 *  /api/v1/events/{id}/approve:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Event to approve.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Events
 *      description: |
 *        The Event may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation or edition
 *          - *Approved*: approved event after creation or edition
 *          - *Rejected*: rejected event after creation or edition
 *
 *        ------
 *
 *        This endpoint approves the specified Event, setting it to an Approved status.
 *        If the Event is not in a Pending status, a Bad Request is returned.
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
eventsRouter.post('/:id/approve', asyncMiddleware(async (req, res, next) => {
  /**
   * Get Event
   */
  const event = await Event.forge({ id: req.params.id })
    .fetch({ require: true })
    .tap((item) => {
      if (item.get('status') !== 'Pending') {
        throw new ValidationError([
          {
            code: Codes.InvalidOperation,
            message: 'Event is not in a Pending status and so it cannot be approved',
            field: 'id',
            index: 0,
          },
        ]);
      }
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Event not found');
    });

  /**
   * Approve Event and returned updated version
   */
  return event.saveWithRelated({ status: 'Approved' }, null, null, null,
    {
      userId: req.authInfo.user.id, method: 'update', patch: true, approve: true,
    })
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/events/{id}/reject:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Event to reject.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Events
 *      description: |
 *        The Event may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation or edition
 *          - *Approved*: approved event after creation or edition
 *          - *Rejected*: rejected event after creation or edition
 *
 *        ------
 *
 *        This endpoint rejects the specified Event, setting it to a Rejected status. The
 *        Event must be in Pending status, otherwise, a Bad Request is returned.
 *
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
eventsRouter.post('/:id/reject', asyncMiddleware(async (req, res, next) => {
  /**
   * Get Event and validate rejection
   */
  const event = await Event.forge({ id: req.params.id })
    .fetch({ require: true })
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Event not found');
    })
    .tap((item) => {
      if (item.get('status') !== 'Pending') {
        throw new ValidationError([
          {
            code: Codes.InvalidOperation,
            message: 'Event not in a Pending status, and so it cannot be rejected',
            field: 'id',
            index: 0,
          },
        ]);
      }
    });

  /**
   * Reject and return updated Event
   */
  return event.saveWithRelated({ status: 'Rejected' }, null, null, null,
    {
      userId: req.authInfo.user.id, method: 'update', patch: true, reject: true,
    })
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(next);
}));

export default eventsRouter;
