/**
 * @swagger
 * definitions:
 *  ElementTypes:
 *   type: string
 *   enum: &ELTYPE
 *     - Text
 *     - Numeric
 *     - Radio
 *     - DropDown
 *     - MultipleDropDown
 *     - Checkbox
 * components:
 *   parameters:
 *    elementTypeFilter:
 *      in: query
 *      name: type
 *      required: false
 *      schema:
 *        type: string
 *        enum: *ELTYPE
 *      description: Filter by element type.
 *    captionFilter:
 *      in: query
 *      name: caption
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial caption.
 *    withRelatedRegistrationForm:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, 'elements' is requested. Value of false disables
 *        the load of related entities. Possible values include 'events' and 'elements'.
 *      example: elements,events
 *   schemas:
 *     RegistrationForm:
 *      description: >
 *        Represents a form for events registration.
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Registration Form numeric identifier.
 *          example: 1
 *        name:
 *          type: string
 *          nullable: false
 *          description: >
 *            The Registration Form name.
 *          example: Básico
 *        elements:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/RegistrationFormElement'
 *          nullable: false
 *          description: The list of the Form Elements.
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update.
 *          example: 2018-09-01T09:00:00.000Z
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation.
 *          example: 2018-09-01T09:00:00.000Z
 *
 *     RegistrationFormElement:
 *      description: >
 *        Represents a form element.
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Registration Form Element numeric identifier.
 *          example: 1
 *        type:
 *          type: string
 *          enum: *ELTYPE
 *          nullable: false
 *          description: The type of the Form Element.
 *        required:
 *          type: boolean
 *          nullable: false
 *          description: True if Element is required.
 *          example: true
 *        name:
 *          type: string
 *          nullable: false
 *          description: Name for the Element.
 *          example: age
 *        caption:
 *          type: string
 *          nullable: false
 *          description: Display caption for the Element.
 *          example: Idade.
 *        values:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *                nullable: true
 *                description: Display name for the Value.
 *              value:
 *                type: string
 *                nullable: true
 *                description: Actual Value.
 *          nullable: false
 *          description: |
 *            The list of the Form Element possible values (used for Elements of Radio, DropDown
 *            or MultipleDropDown type).
 */

import { Router } from 'express';

import Bookshelf from '../bookshelf';
import asyncMiddleware from '../middleware/async';
import { NotFoundError } from '../errors';
import { getPaginationMetaData } from '../helpers/metadata';
import { getData } from '../helpers/common';

import Form from '../models/form';
import FormElement from '../models/form_element';

const formsRouter = new Router();

/**
 * @swagger
 * /api/v1/registration-forms:
 *   get:
 *     tags:
 *       - Registration Forms
 *     description: Returns the list of Registration Forms.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idFilter'
 *       - $ref: '#/components/parameters/withRelatedRegistrationForm'
 *     responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationForm'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '500':
 *        $ref: '#/components/responses/ServerError'
 */
formsRouter.get('/', asyncMiddleware(async (req, res) => {
  // Parse filters from boolean to integer and add partial filter behaviour
  req.parameters.parseFilters({
    name: v => `%${v}%`,
    description: v => `%${v}%`,
    created_at: v => `%${v}%`,
    updated_at: v => `%${v}%`,
  });

  // Assign default value for withRelated
  req.parameters.withRelated = req.parameters.withRelated || Form.defaultWithRelated.slice();

  // Validate filters, sorting and withRelated parameters
  req.parameters.validate(Form.filterFields, Form.sortFields, Form.withRelated);

  // Add withRelated to fetch is_interested and is_registered if 'events' is requested
  if (req.parameters.withRelated instanceof Array && req.parameters.withRelated.includes('events')) {
    req.parameters.withRelated.unshift({
      'events.is_interested': qb => qb.where('event_interest.user_id', req.authInfo.user.id),
      'events.is_registered': qb => qb.where('registration.user_id', req.authInfo.user.id),
    });
  }

  // Use request parameters to filter, order and fetch all or paginated data
  const collection = await Form.forge().filterOrderAndFetch(req.parameters);
  const data = collection ? collection.toJSON({ omitPivot: true }) : [];
  const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
  return res.formatter.ok(data, link);
}));

/**
 * @swagger
 *  /api/v1/registration-forms/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration Form to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Registration Forms
 *      description: Returns the information for the RegistrationForm with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedRegistrationForm'
 *      responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationForm'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
formsRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  // Assign default value for withRelated
  req.parameters.withRelated = req.parameters.withRelated || Form.defaultWithRelated.slice();

  // Validate withRelated parameter
  req.parameters.validate(null, null, Form.withRelated);

  // Add withRelated to fetch is_interested and is_registered if 'events' is requested
  if (req.parameters.withRelated instanceof Array && req.parameters.withRelated.includes('events')) {
    req.parameters.withRelated.unshift({
      'events.is_interested': qb => qb.where('event_interest.user_id', req.authInfo.user.id),
      'events.is_registered': qb => qb.where('registration.user_id', req.authInfo.user.id),
    });
  }
  Form.forge({ id: req.params.id })
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Form not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/registration-forms/{id}/elements:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration Form to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Registration Forms
 *      description: |
 *        Returns the list of Registration Form Elements associated with the RegistrationForm
 *        with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/elementTypeFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/captionFilter'
 *      responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationFormElement'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
formsRouter.get('/:id/elements', asyncMiddleware(async (req, res) => {
  // First validate the existance of the form
  await Form.validateExistence(req.params.id, 'Form');
  const collection = await FormElement.forge({ form_id: req.params.id }).fetchAll();
  return res.formatter.ok(collection.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registration-forms:
 *    parameters:
 *    post:
 *      tags:
 *        - Registration Forms
 *      description: |
 *        This endpoint is used for creating RegistrationForms.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Registration Form data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/RegistrationForm'
 *      responses:
 *       '201':
 *        description: Created RegistrationForm object.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationForm'
 *       '400':
 *        $ref: '#/components/responses/BadRequest'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '500':
 *        $ref: '#/components/responses/ServerError'
 */
formsRouter.post('/', asyncMiddleware(async (req, res) => {
  const item = await Form.forge().saveWithRelated(
    getData(req.body, ['name'], false),
    req.body.elements,
    { method: 'insert' },
  );
  return res.formatter.created(item.toJSON());
}));

/**
 * @swagger
 *  /api/v1/registration-forms/{id}:
 *    put:
 *      tags:
 *        - Registration Forms
 *      description: |
 *        Updates a Registration Form.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Registration Form updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/RegistrationForm'
 *      responses:
 *        '200':
 *          description: Updated RegistrationForm object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationForm'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
formsRouter.put('/:id', asyncMiddleware(async (req, res) => {
  const item = await Form.forge({ id: req.params.id }).saveWithRelated(
    getData(req.body, ['name'], false),
    req.body.elements,
    { method: 'update', patch: false },
  );
  return res.formatter.ok(item.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registration-forms/{id}:
 *    patch:
 *      tags:
 *        - Registration Forms
 *      description: |
 *        Partially updates a Registration Form.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Registration Form updated data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/RegistrationForm'
 *      responses:
 *        '200':
 *          description: Updated RegistrationForm object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationForm'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
formsRouter.patch('/:id', asyncMiddleware(async (req, res) => {
  const item = await Form.forge({ id: req.params.id }).saveWithRelated(
    getData(req.body, ['name'], true),
    req.body.elements,
    { method: 'update', patch: true },
  );

  return res.formatter.ok(item.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registration-forms/{id}:
 *    delete:
 *      tags:
 *        - Registration Forms
 *      description: >
 *        Deletes a Registration Form. If the specified
 *        form contains associated events, a Bad Request is sent.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
formsRouter.delete('/:id', (req, res, next) => {
  Bookshelf.transaction(t => Form.forge().where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Form not found');
    })
    .catch(next);
});

export default formsRouter;
