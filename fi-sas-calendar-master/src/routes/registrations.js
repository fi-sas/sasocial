/**
 * @swagger
 * definitions:
 *  RegistrationStatus:
 *   type: string
 *   enum: &REGSTATUS
 *     - Pending
 *     - Approved
 *     - Rejected
 * components:
 *   parameters:
 *    userIdFilter:
 *      in: query
 *      name: user_id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by user ID.
 *    eventIdFilter:
 *      in: query
 *      name: event_id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by event ID.
 *    elementIdFilter:
 *      in: query
 *      name: element_id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by Registration Form Element ID.
 *    registrationStatusFilter:
 *      in: query
 *      name: status
 *      required: false
 *      schema:
 *        type: string
 *        enum: *REGSTATUS
 *      description: Filter by registration status.
 *    withRelatedRegistration:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, 'answers' are requested. Value of false disables
 *        the load of related entities. Possible values include 'answers'.
 *      example: answers
 *   schemas:
 *     Registration:
 *      description: Represents an Event Registration.
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          readOnly: true
 *          nullable: false
 *          description: The Event Registration numeric identifier.
 *          example: 1
 *        user_id:
 *          type: integer
 *          readOnly: true
 *          nullable: false
 *          description: The ID of the User to which the Event Registration belongs to.
 *          example: 1
 *        status:
 *          type: string
 *          readOnly: true
 *          enum: *REGSTATUS
 *          description: |
 *            The current status of the Even Registration. If Event dows not need manual
 *            approval, all registrations are automatically Approved. Otherwise, registrations
 *            are created in Pending status.
 *          example: Approved
 *        event_id:
 *          type: number
 *          nullable: false
 *          description: The ID of the Event to which the Registration belongs to.
 *          example: 1
 *        created_at:
 *          type: string
 *          readOnly: true
 *          format: date
 *          nullable: false
 *          description: The date of creation.
 *          example: 2018-09-01T09:00:00.000Z
 *        updated_at:
 *          type: string
 *          readOnly: true
 *          format: date
 *          nullable: false
 *          description: The date of the last update.
 *          example: 2018-09-01T09:00:00.000Z
 *        answers:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/RegistrationAnswer'
 *          nullable: false
 *          description: The answers to the Registrations Form.
 *     RegistrationAnswer:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          readOnly: true
 *          nullable: false
 *          description: The Registration Answer numeric identifier.
 *          example: 1
 *        registration_id:
 *          type: integer
 *          readOnly: true
 *          nullable: false
 *          description: |
 *            The ID of the Registration to which the Event Registration Answer
 *            belongs to.
 *          example: 1
 *        element_id:
 *          type: integer
 *          description: The ID of the Registration Element.
 *          example: 1
 *        value:
 *          type: string
 *          nullable: false
 *          description: Value for the Element.
 *          example: Example value
 *        created_at:
 *          type: string
 *          readOnly: true
 *          format: date
 *          nullable: false
 *          description: The date of creation.
 *          example: 2018-09-01T09:00:00.000Z
 *        updated_at:
 *          type: string
 *          readOnly: true
 *          format: date
 *          nullable: false
 *          description: The date of the last update.
 *          example: 2018-09-01T09:00:00.000Z
 */

import { Router } from 'express';

import Bookshelf from '../bookshelf';
import asyncMiddleware from '../middleware/async';
import { NotFoundError, ValidationError } from '../errors';
import * as Codes from '../error_codes';
import { getPaginationMetaData } from '../helpers/metadata';
import { getData } from '../helpers/common';

import Registration from '../models/registration';
import RegistrationAnswer from '../models/registration_answer';

const registrationsRouter = new Router();

/**
 * @swagger
 * /api/v1/registrations:
 *   get:
 *     tags:
 *       - Registrations
 *     description: Returns the list of Registrations.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idFilter'
 *       - $ref: '#/components/parameters/userIdFilter'
 *       - $ref: '#/components/parameters/eventIdFilter'
 *       - $ref: '#/components/parameters/registrationStatusFilter'
 *       - $ref: '#/components/parameters/withRelatedRegistration'
 *     responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Registration'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '500':
 *        $ref: '#/components/responses/ServerError'
 */
registrationsRouter.get('/', asyncMiddleware(async (req, res) => {
  // Parse filters from boolean to integer and add partial filter behaviour
  req.parameters.parseFilters({
    created_at: v => `%${v}%`,
    updated_at: v => `%${v}%`,
  });

  // Assign default value for withRelated
  req.parameters.withRelated = req.parameters.withRelated || Registration.defaultWithRelated.slice();

  // Validate filters, sorting and withRelated parameters
  req.parameters.validate(
    Registration.filterFields,
    Registration.sortFields,
    Registration.withRelated,
  );

  // Use request parameters to filter, order and fetch all or paginated data
  const collection = await Registration.forge().filterOrderAndFetch(req.parameters);
  const data = collection ? collection.toJSON({ omitPivot: true }) : [];
  const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
  return res.formatter.ok(data, link);
}));

/**
 * @swagger
 *  /api/v1/registrations/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Registrations
 *      description: Returns the information for the Registration with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedRegistration'
 *      responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Registration'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
registrationsRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  // Assign default value for withRelated
  req.parameters.withRelated = req.parameters.withRelated || Registration.defaultWithRelated.slice();

  // Validate withRelated parameter
  req.parameters.validate(null, null, Registration.withRelated);

  Registration.forge({ id: req.params.id })
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Registration not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/registrations/{id}/answers:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Registrations
 *      description: |
 *        Returns the list of Registration Answer associated with the Registration
 *        with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/elementIdFilter'
 *      responses:
 *       '200':
 *        description: Successful operation.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/RegistrationAnswer'
 *       '400':
 *        $ref: '#/components/responses/BadRequestSimpleValidation'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
registrationsRouter.get('/:id/answers', asyncMiddleware(async (req, res) => {
  // First validate the existance of the registration
  await Registration.validateExistence(req.params.id, 'Registration');
  const collection = await RegistrationAnswer.forge({ registration_id: req.params.id }).fetchAll();
  return res.formatter.ok(collection.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registrations:
 *    parameters:
 *    post:
 *      tags:
 *        - Registrations
 *      description: |
 *        This endpoint is used for creating Registrations.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Registration data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Registration'
 *      responses:
 *       '201':
 *        description: Created Registration object.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Registration'
 *       '400':
 *        $ref: '#/components/responses/BadRequest'
 *       '401':
 *        $ref: '#/components/responses/Unauthorized'
 *       '403':
 *        $ref: '#/components/responses/Forbidden'
 *       '500':
 *        $ref: '#/components/responses/ServerError'
 */
registrationsRouter.post('/', asyncMiddleware(async (req, res) => {
  const item = await Registration.forge().saveWithRelated(
    Object.assign({}, getData(req.body, ['event_id'], false), { user_id: req.authInfo.user.id }),
    req.body.answers,
    { method: 'insert' },
  );
  return res.formatter.created(item.toJSON());
}));

/**
 * @swagger
 *  /api/v1/registrations:
 *    put:
 *      tags:
 *        - Registrations
 *      description: |
 *        Updates a Registration.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Registration updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Registration'
 *      responses:
 *        '200':
 *          description: Updated RegistrationForm object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Registration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
registrationsRouter.put('/', asyncMiddleware(async (req, res) => {
  const item = await Registration.forge({ id: req.params.id }).saveWithRelated(
    Object.assign({}, getData(req.body, ['event_id'], false), { user_id: req.authInfo.user.id }),
    req.body.answers,
    { method: 'update', patch: false },
  );
  return res.formatter.ok(item.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registrations:
 *    patch:
 *      tags:
 *        - Registrations
 *      description: |
 *        Partially updates a Registration.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Registration updated data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Registration'
 *      responses:
 *        '200':
 *          description: Updated RegistrationForm object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Registration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
registrationsRouter.patch('/', asyncMiddleware(async (req, res) => {
  const item = await Registration.forge({ id: req.params.id }).saveWithRelated(
    Object.assign({}, getData(req.body, ['event_id'], true), { user_id: req.authInfo.user.id }),
    req.body.answers,
    { method: 'update', patch: true },
  );

  return res.formatter.ok(item.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/registrations/{id}:
 *    delete:
 *      tags:
 *        - Registrations
 *      description: >
 *        Deletes a Registration and its associated answers.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
registrationsRouter.delete('/:id', (req, res, next) => {
  Bookshelf.transaction(t => Registration.forge().where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => {
      throw new NotFoundError('Registration not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/registrations/{id}/approve:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration to approve.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Registrations
 *      description: |
 *        The Registration may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation or edition
 *          - *Approved*: approved event after creation or edition
 *          - *Rejected*: rejected event after creation or edition
 *
 *        ------
 *
 *        This endpoint approves the specified Registration, setting it to an Approved status.
 *        If the Registration is not in a Pending status, a Bad Request is returned.
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
registrationsRouter.post('/:id/approve', asyncMiddleware(async (req, res) => {
  // First validate the existance of the registration
  await Registration.validateExistence(req.params.id, 'Registration');

  // Registration must be at Pending status in order to be approved
  const reg = await Registration.forge({ id: req.params.id }).fetch({ require: true });
  if (reg.get('status') !== 'Pending') {
    throw new ValidationError([{
      code: Codes.InvalidOperation,
      message: 'Registration is not in a Pending status and so it cannot be approved.',
      field: 'id',
      index: 0,
    }]);
  }

  // Save and return the updated registration
  const item = await reg.saveWithRelated({ status: 'Approved' }, null, { patch: true });
  return res.formatter.ok(item.toJSON());
}));

/**
 * @swagger
 *  /api/v1/registrations/{id}/reject:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Registration to reject.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Registrations
 *      description: |
 *        The Registration may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation or edition
 *          - *Approved*: approved event after creation or edition
 *          - *Rejected*: rejected event after creation or edition
 *
 *        ------
 *
 *        This endpoint rejects the specified Registration, setting it to a Rejected status. The
 *        Registration must be in Pending status, otherwise, a Bad Request is returned.
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Event'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
registrationsRouter.post('/:id/reject', asyncMiddleware(async (req, res) => {
  // First validate the existance of the registration
  await Registration.validateExistence(req.params.id, 'Registration');

  // Registration must be at Pending status in order to be approved
  const reg = await Registration.forge({ id: req.params.id }).fetch({ require: true });
  if (reg.get('status') !== 'Pending') {
    throw new ValidationError([{
      code: Codes.InvalidOperation,
      message: 'Registration is not in a Pending status and so it cannot be rejected.',
      field: 'id',
      index: 0,
    }]);
  }

  // Save and return the updated registration
  const item = await reg.saveWithRelated({ status: 'Rejected' }, null, { patch: true });
  return res.formatter.ok(item.toJSON());
}));

export default registrationsRouter;
