import Joi from 'joi';

module.exports = Joi.object().keys({
  color: Joi.string().hex(),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
