import Joi from 'joi';
import ConfigurationKeys from '../models/values/configuration_keys';

module.exports = Joi.object().keys({
  key: Joi.valid(ConfigurationKeys),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
