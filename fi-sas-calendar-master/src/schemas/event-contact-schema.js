import Joi from 'joi';
import contactTypes from '../models/values/contact_types';

module.exports = Joi.object().keys({
  event_id: Joi.number().integer(),
  type: Joi.valid(contactTypes),
  value: Joi.string().when('type',
    {
      is: 'Email',
      then: Joi.string().email(),
      otherwise: Joi.string().max(120).regex(/^(\+|00)\d{1,3}\d+$/),
    }),
});
