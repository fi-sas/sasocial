import Joi from 'joi';

module.exports = Joi.object().keys({
  event_id: Joi.number().integer(),
  file_id: Joi.number().integer(),
});
