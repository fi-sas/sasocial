import Joi from 'joi';
import eventStatuses from '../models/values/event_statuses';
import recurrences from '../models/values/recurrences';

module.exports = Joi.object().keys({
  service_id: Joi.number().integer().positive(),
  category_id: Joi.number().integer().positive(),
  start_date: Joi.date().iso(),
  end_date: Joi.date().iso().allow(null).optional(),
  recurrence: Joi.valid(recurrences),
  repeat_month: Joi.alternatives().try(
    Joi.number().integer().allow(null).min(0)
      .max(11),
    Joi.string().valid('*'),
  ),
  repeat_day: Joi.alternatives().try(
    Joi.number().integer().allow(null).min(1)
      .max(28),
    Joi.string().valid('*'),
  ),
  repeat_weekday: Joi.alternatives().try(
    Joi.number().integer().allow(null).min(0)
      .max(6),
    Joi.string().valid('*'),
  ),
  publish_start_date: Joi.date().iso(),
  publish_end_date: Joi.date().iso().allow(null).optional(),
  notifications_enabled: Joi.boolean(),
  notification_target_id: Joi.number().integer().allow(null),
  notification_alert_type_key: Joi.string().max(120).allow('', null),
  notification_start_date: Joi.any().when(
    'notifications_enabled',
    {
      is: Joi.valid(true),
      then: Joi.date().iso(),
      otherwise: Joi.date().iso().allow(null),
    },
  ),
  number_of_notifications: Joi.any().when(
    'notifications_enabled',
    {
      is: Joi.valid(true),
      then: Joi.number().integer(),
      otherwise: Joi.number().integer().allow(null),
    },
  ),
  allow_interest: Joi.boolean(),
  cancelled: Joi.boolean(),
  notification_recurrence: Joi.valid(recurrences),
  latitude: Joi.number().min(-90).max(90),
  longitude: Joi.number().min(-180).max(180),
  address: Joi.string().min(1).max(250),
  address_no: Joi.string().min(1).max(25),
  city: Joi.string().min(1).max(250),
  allow_registration: Joi.boolean(),
  max_registrations: Joi.number().integer().allow(null).optional(),
  registration_approval_needed: Joi.any().when(
    'allow_registration',
    {
      is: Joi.valid(true),
      then: Joi.boolean(),
      otherwise: Joi.boolean().allow(null),
    },
  ),
  registration_form_id: Joi.number().integer().positive().allow(null),
  registration_fee: Joi.any().when(
    'allow_registration',
    {
      is: Joi.valid(true),
      then: Joi.number().error(() => ({
        message: '"registration_fee" must be a number when "allow_registration" is true',
      })),
      otherwise: Joi.number().allow(null),
    },
  ),
  registration_tax_id: Joi.any().when(
    'registration_fee',
    {
      is: Joi.valid([null, 0]),
      then: Joi.number().integer().positive().allow(null),
      otherwise: Joi.number().integer().positive().error(() => ({
        message: '"registration_tax_id" must be a positive integer when "registration_fee" is used',
      })),
    },
  ),
  registration_code: Joi.string().max(120).allow(null, ''),
  status: Joi.valid(eventStatuses),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
