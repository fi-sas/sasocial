import Joi from 'joi';

module.exports = Joi.object().keys({
  language_id: Joi.number().integer(),
  event_id: Joi.number().integer(),
  name: Joi.string().max(120),
  description: Joi.string().max(500).allow(''),
  url: Joi.string().max(500).allow(''),
});
