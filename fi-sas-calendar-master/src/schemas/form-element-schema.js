import Joi from 'joi';

module.exports = Joi.object().keys({
  form_id: Joi.number().integer(),
  type: Joi.string().valid([
    'Text', 'Numeric', 'Radio', 'DropDown', 'MultipleDropDown', 'Checkbox',
  ]),
  required: Joi.boolean(),
  name: Joi.string().max(255),
  caption: Joi.string().max(255),
  values: Joi.array().items(Joi.object().keys({
    name: Joi.string().max(255),
    value: Joi.string().max(255),
  })).optional(),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
