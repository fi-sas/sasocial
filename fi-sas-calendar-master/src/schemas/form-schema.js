import Joi from 'joi';

module.exports = Joi.object().keys({
  name: Joi.string().max(255),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
