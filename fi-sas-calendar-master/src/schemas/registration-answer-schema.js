import Joi from 'joi';

module.exports = Joi.object().keys({
  registration_id: Joi.number().integer(),
  element_id: Joi.number().integer(),
  value: Joi.string().max(255),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
