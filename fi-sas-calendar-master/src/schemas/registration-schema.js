import Joi from 'joi';

module.exports = Joi.object().keys({
  user_id: Joi.number().integer(),
  status: Joi.string().valid(['Pending', 'Approved', 'Rejected']),
  event_id: Joi.number().integer(),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
