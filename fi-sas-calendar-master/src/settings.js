import path from 'path';

module.exports = {
  PROJECT_DIR: path.join(__dirname, '..'),
};
