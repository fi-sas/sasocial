module.exports.up = async db => db.schema.createTable('team', (table) => {
  table.increments();
  table.string('name', 120).notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique('name');
});

module.exports.down = async db => db.schema.dropTable('team');

module.exports.configuration = { transaction: true };
