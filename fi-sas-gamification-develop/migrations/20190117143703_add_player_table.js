module.exports.up = async db => db.schema.createTable('player', (table) => {
  table.increments();
  table.string('nickname', 60).notNullable();
  table.integer('user_id').unsigned().notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique(['nickname', 'user_id']);
});

module.exports.down = async db => db.schema.dropTable('player');

module.exports.configuration = { transaction: true };
