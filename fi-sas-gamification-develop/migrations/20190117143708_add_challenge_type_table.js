module.exports.up = async db => db.schema.createTable('challenge_type', (table) => {
  table.increments();
  table.string('tag', 120).notNullable();
  table.string('description', 250);
  table.unique('tag');
});

module.exports.down = async db => db.schema.dropTable('challenge_type');

module.exports.configuration = { transaction: true };
