module.exports.up = async db => db.schema.createTable('challenge', (table) => {
  table.increments();
  table
    .integer('challenge_type_id')
    .unsigned()
    .references('id')
    .inTable('challenge_type');
  table.integer('points').unsigned().notNullable();
  table.boolean('automated').notNullable().defaultTo(0);
  table.string('tag', 120).notNullable();
  table.integer('service_id');
  table.datetime('start_date');
  table.datetime('end_date');
  table.integer('winners').unsigned();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique('tag');
});

module.exports.down = async db => db.schema.dropTable('challenge');

module.exports.configuration = { transaction: true };
