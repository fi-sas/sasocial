module.exports.up = async db => db.schema.createTable('team_player', (table) => {
  table.increments();
  table
    .integer('team_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('team');
  table
    .integer('player_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('player');
  table.boolean('leader').notNullable().defaultTo(0);
  table.enum('status', ['Invite', 'Member', 'Request']);
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique('player_id');
});

module.exports.down = async db => db.schema.dropTable('team_player');

module.exports.configuration = { transaction: true };
