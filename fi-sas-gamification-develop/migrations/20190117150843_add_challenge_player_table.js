module.exports.up = async db => db.schema.createTable('challenge_player', (table) => {
  table.increments();
  table
    .integer('challenge_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('challenge');
  table
    .integer('player_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('player');
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
  table.unique(['challenge_id', 'player_id']);
});

module.exports.down = async db => db.schema.dropTable('challenge_player');

module.exports.configuration = { transaction: true };
