module.exports.up = async db => db.schema.createTable('points', (table) => {
  table.increments();
  table
    .integer('challenge_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('challenge');
  table
    .integer('player_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('player');
  table.integer('points');
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('points');

module.exports.configuration = { transaction: true };
