module.exports.up = async db => db.schema.createTable('competition', (table) => {
  table.increments();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('competition');

module.exports.configuration = { transaction: true };
