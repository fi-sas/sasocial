module.exports.up = async db => db.schema.createTable('competition_challenge', (table) => {
  table.increments();
  table
    .integer('challenge_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('challenge');
  table
    .integer('competition_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('competition');
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('competition_challenge');

module.exports.configuration = { transaction: true };
