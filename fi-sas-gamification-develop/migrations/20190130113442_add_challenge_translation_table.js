module.exports.up = async db => db.schema.createTable('challenge_translation', (table) => {
  table.increments();
  table
    .integer('challenge_id')
    .unsigned()
    .references('id')
    .inTable('challenge');
  table.integer('language_id').notNullable().unsigned();
  table.string('name', 120).notNullable();
  table.text('description');
  table.text('rules').notNullable();
  table.string('team_prize', 250);
  table.string('player_prize', 250);
  table.unique(['challenge_id', 'language_id']);
});

module.exports.down = async db => db.schema.dropTable('challenge_translation');

module.exports.configuration = { transaction: true };
