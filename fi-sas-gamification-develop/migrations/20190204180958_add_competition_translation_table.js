module.exports.up = async db => db.schema.createTable('competition_translation', (table) => {
  table.increments();
  table
    .integer('competition_id')
    .unsigned()
    .references('id')
    .inTable('competition');
  table.integer('language_id').notNullable().unsigned();
  table.string('name', 120).notNullable();
  table.unique(['competition_id', 'language_id']);
});

module.exports.down = async db => db.schema.dropTable('competition_translation');

module.exports.configuration = { transaction: true };
