exports.seed = async (knex, Promise) => {
  const data = [
    {
      id: 1,
      tag: 'Voluntariado',
      description: 'Ações de Voluntariado',
    },
    {
      id: 2,
      tag: 'Eventos',
      description: 'Inscrição em Eventos',
    },
    {
      id: 3,
      tag: 'Serviços',
      description: 'Adesão a serviços',
    },
  ];

  return Promise.all(data.map(async (d) => {
    // Check if challenge_type exists
    const rows = await knex('challenge_type').select().where('id', d.id);
    if (rows.length === 0) {
      await knex('challenge_type').insert(d);
    }
    return true;
  }));
};
