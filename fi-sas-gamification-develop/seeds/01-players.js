module.exports.seed = async knex => knex('player').del().then(() => knex('player').insert([
  {
    id: 1,
    user_id: 1,
    nickname: 'Hightower',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 2,
    user_id: 2,
    nickname: 'Indestructible Potato',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 3,
    user_id: 3,
    nickname: 'Spitfire',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 4,
    user_id: 4,
    nickname: 'Digital Moonshine',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 5,
    user_id: 5,
    nickname: 'Rooster',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 6,
    user_id: 6,
    nickname: 'Fast Draw',
    created_at: new Date(),
    updated_at: new Date(),
  },
]));
