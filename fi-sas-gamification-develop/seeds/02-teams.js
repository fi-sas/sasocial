module.exports.seed = async knex => knex('team').del().then(() => knex('team').insert([
  {
    id: 1,
    name: 'The Great Firewall',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 2,
    name: 'Necromancers',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 3,
    name: 'Highlanders',
    created_at: new Date(),
    updated_at: new Date(),
  },
]));
