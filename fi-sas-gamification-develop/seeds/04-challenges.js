exports.seed = (knex) => {
  // Deletes ALL existing entries
  return knex('challenge').del().then(() => {
    // Inserts challenge seed entries
    return knex('challenge')
      .insert([
        {
          id: 1,
          challenge_type_id: 1,
          points: 3,
          automated: 0,
          tag: 'WELCOME_CHALLENGE',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: 2,
          challenge_type_id: 1,
          points: 5,
          automated: 0,
          tag: 'TEST_CHALLENGE',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ])
      .then(() => {
        // Inserts challenge_translation seed entries
        return knex('challenge_translation').insert([
          {
            language_id: 1,
            challenge_id: 1,
            name: 'Desafio de boas vindas.',
            description: 'Desafio para mostrar como funcionam os desafios.',
            rules: 'Completar o tutorial',
            player_prize: '3 pontos quando completar o tutorial.',
          },
          {
            language_id: 2,
            challenge_id: 1,
            name: 'Welcome challenge.',
            description: 'A challenge to show how challenges work.',
            rules: 'Finish tutorial',
            player_prize: '3 points when tutorial is finished.',
          },
          {
            language_id: 1,
            challenge_id: 2,
            name: 'Desafio Teste.',
            description: 'Desafio Teste.',
            rules: 'Teste',
            player_prize: 'Teste',
            team_prize: 'Teste',
          },
        ]);
      });
  });
};
