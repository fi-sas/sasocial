module.exports.seed = async knex => knex('challenge_player').del().then(() => knex('challenge_player').insert([
  {
    id: 1,
    challenge_id: 1,
    player_id: 1,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 2,
    challenge_id: 1,
    player_id: 2,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 3,
    challenge_id: 1,
    player_id: 3,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 4,
    challenge_id: 1,
    player_id: 4,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 5,
    challenge_id: 1,
    player_id: 5,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 6,
    challenge_id: 1,
    player_id: 6,
    created_at: new Date(),
    updated_at: new Date(),
  },
]));
