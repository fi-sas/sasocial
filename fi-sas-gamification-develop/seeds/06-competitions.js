exports.seed = (knex) => {
  // Deletes ALL existing entries
  return knex('competition').del().then(() => {
    // Inserts competition seed entries
    return knex('competition')
      .insert([
        {
          id: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ])
      .then(() => {
        // Inserts competition_translation seed entries
        return knex('competition_translation').insert([
          {
            language_id: 1,
            competition_id: 1,
            name: 'Competição Tutorial Gamification.',
          },
          {
            language_id: 2,
            competition_id: 1,
            name: 'Gamification Tutorial Competition.',
          },
          {
            language_id: 1,
            competition_id: 2,
            name: 'Nova Competição.',
          },
          {
            language_id: 2,
            competition_id: 2,
            name: 'New Competition.',
          },
        ]);
      });
  });
};
