module.exports.seed = async knex => knex('competition_challenge').del().then(() => knex('competition_challenge').insert([
  {
    id: 1,
    competition_id: 1,
    challenge_id: 1,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 2,
    competition_id: 1,
    challenge_id: 2,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 3,
    competition_id: 2,
    challenge_id: 2,
    created_at: new Date(),
    updated_at: new Date(),
  },
]));
