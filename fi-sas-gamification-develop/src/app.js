import express from 'express';
import path from 'path';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import log4js from 'log4js';

import setValidators from './custom_validations';
import apiComposerValidations from './middleware/apiComposerValidations';
import responseEnhancer from './middleware/response-formatter';
import requestParameters from './middleware/request-parameters';
import { errorHandler, logErrors } from './middleware/error-handlers';
import tokenInfo from './middleware/tokenInfo';
import internalRequest from './middleware/internal-request';
import logger from './logger';

import requireAuth from './middleware/auth';
import { NotFoundError } from './errors';

// Swagger Specs
import linkSpecs from './config/components/schemas.link.spec.json';
import parametersSpecs from './config/components/parameters.spec.json';
import responsesSpecs from './config/components/responses.spec.json';
import securitySchemesSpecs from './config/components/securitySchemes.bearerAuth.json';
import securitySpecs from './config/security.json';

import TeamsRouter from './routes/teams';
import ChallengesRouter from './routes/challenges';
import PlayersRouter from './routes/players';
import CompetitionsRouter from './routes/competitions';
import ChallengeTypesRouter from './routes/challenge_types';

//    __________  ____  _   __       ______  ____ _____
//   / ____/ __ \/ __ \/ | / /      / / __ \/ __ ) ___/
//  / /   / /_/ / / / /  |/ /  __  / / / / / __  \__ \
// / /___/ _, _/ /_/ / /|  /  / /_/ / /_/ / /_/ /__/ /
// \____/_/ |_|\____/_/ |_/   \____/\____/_____/____/

// Uncomment to use cron jobs

// import cron from 'node-cron';

// cron.schedule('* * * * *', () => {
//   console.log('running a task every minute');
// });


//     __  ________    ____  ____  ____  _____________________  ___    ____
//    /  |/  / ___/   / __ )/ __ \/ __ \/_  __/ ___/_  __/ __ \/   |  / __ \
//   / /|_/ /\__ \   / __  / / / / / / / / /  \__ \ / / / /_/ / /| | / /_/ /
//  / /  / /___/ /  / /_/ / /_/ / /_/ / / /  ___/ // / / _, _/ ___ |/ ____/
// /_/  /_//____/  /_____/\____/\____/ /_/  /____//_/ /_/ |_/_/  |_/_/
setValidators();

/**
 * Routes
 */
const app = express();

// Use logged middleware to log all requests
app.use(log4js.connectLogger(logger, { level: 'auto' }));

// Set default render engine
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

// Add CORS headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Language-ID, X-Language-Acronym');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD');
  next();
});

// Helmet helps you secure your Express apps by setting various HTTP headers.
// It's not a silver bullet, but it can help!
// https://github.com/helmetjs/helmet
app.use(helmet());

/**
 * Use response enhancer (formatter)
 * Use request parameters to access ordering, filters and pagination configurations
 */
app.use(responseEnhancer());
app.use(requestParameters());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: '5mb' }));
app.use(express.static(path.join(__dirname, '..', 'public')));

/**
 * Use internal request detection
 */
app.use(internalRequest);

// API composer validations -- used for validating resources from other MS
app.use(apiComposerValidations);
app.use(tokenInfo);

/*
 * Assign Routes to Application
 */
app.use('/api/v1/teams', requireAuth, TeamsRouter);
app.use('/api/v1/challenges/types', requireAuth, ChallengeTypesRouter);
app.use('/api/v1/challenges', requireAuth, ChallengesRouter);
app.use('/api/v1/players', requireAuth, PlayersRouter);
app.use('/api/v1/competitions', requireAuth, CompetitionsRouter);

// Usage of auth middleware:
// Up top: import requireAuth from './middleware/auth'
//
// app.use('/users', requireAuth, usersRouter);
//
// Inside your routes, you will have available in req.authInfo the information
// decoded from the token


/*
 * Swagger documentation route
 */
const swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'FI@SAS Gamification Microservice',
      version: '0.1.0',
      description: `Gamification Microservice API for the FI@SAS project.
## Summary
The main goal of this micro service is to use gaming techniques and mechanisms
to encourage users to do something and increase user engagement with the platform.
It's important to encourage the user interaction with services in a game logic
and reward them according to the achievement of specific objectives.
To achieve this, the MS must be transversal to the other framework micro services,
where they can add challenges and assign, in a automatic way, scores to the player
that accomplish some action.
## Description
This micro service has the following entities:
1. Players, a user registered (by a nickname) in this micro service;
2. Teams, a group of players, up to 20;
3. Challenges, which a player must join to be able to obtain the score, can be automatized,
through other micro services, or via BackOffice.
4. Competitions, a group of Challenges.
### Players
A player is a direct map of a framework user, is defined by a nickname and a Player ID.
The player can create, join or be invited to Teams.
### Teams
A Team is composed by a group of players and can have up to 20 players and the player
who creates the team is the team leader, a player is also able to leave the team when desired.
When a player creates a new Team, he can invite other player to his team via their
nickname, then the player can accept/reject the invite. If the player doesn't belong
to any team he can send a request to join the team, which can be accepted/rejected
by the team leader.
### Challenges
Challenges can be fully automatized or created and maintained manually via BackOffice.
Other framework micro services can create challenges and give a Player the challenge points
when it's completed. All this can also be done manually in the BackOffice. A challenge is defined
by a set of translatable fields that describe the challenge: _name_, _description_, _rules_,
_team_ and _player prizes_. The challenge is also described by a unique _tag_, whether
it is _automated_ or not, the _service id_ of the micro service that creates the challenge
(if needed), the quantity of _winners_, the number of _points_ and the _start/end date_.
The challenge can be categorized by a _challenge type_ which is an entity manageable in
the BackOffice.
### Competitions
Competitions are composed by a challenge or multiple challenges and a translatable
_name_. When challenges join the competition the team and player ranking is provided
to the competition.
`,
    },
    components: {
      schemas: {
        Link: linkSpecs,
      },
      parameters: parametersSpecs,
      responses: responsesSpecs,
      securitySchemes: securitySchemesSpecs,
    },
    security: securitySpecs,
  },
  apis: ['./routes/*'],
});

delete swaggerSpec.swagger;
swaggerSpec.openapi = '3.0.n';

swaggerSpec.components.schemas = Object.assign(swaggerSpec.components.schemas, require('./config/components/schemas.errors.spec.json'));

swaggerSpec.components.responses = require('./config/components/responses.spec.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// catch 404 and forward to error handler
app.use(() => {
  throw new NotFoundError();
});

app.use(logErrors);
app.use(errorHandler);

export default app;
