/**
 * deviceGroupScope bookshelf plugin
 *
 * Extends bookshelf Model with a applyDeviceGroupScopes method to be called before fetching data.
 * applyDeviceGroupScope method uses the received model table names, parameters and device group
 * ids to update the model query and/or parameters to respect group device scopes.
 * The method ensures that the specified models (wether it is the current model, or a related model
 * present in withRelated parameter), if loaded, will be filtered according to its group devices
 * relations: either the fetched rows are related with one of the received group devices or are
 * related with all groups.
 *
 * Usage:
 *   const deviceGroups = [1, 2, 3];
 *   ModelA.forge()
 *    .applyDeviceGroupScope(['modelA'], req.parameters, deviceGroups)
 *    .fetchAll(parameters);
 * // This will filter modelA items to include only the ones related with received deviceGroups
 * // or the ones that are not related with any group at all
 *
 * Notes:
 * It is assumed that models' "id" keys are referenced by a 'group_'+model table via model+'_id'
 * field.
 *
 * Known limitation: apply device group scopes for related models in a *-n relationship (example:
 * 'categories' instead of 'category') are currently not supported.
 */
/* eslint-disable no-param-reassign, func-names */

import _ from 'lodash';

module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Returns a function to filter a received query, requiring the received model's
     * relation with received groups.
     *
     * Expects model.id to be referenced by a 'group_model' table via 'model_id' field. This
     * table must also contain a 'group_id' field.
     *
     * @param {string} model Name of model's database table
     * @param {Array} deviceGroups Array of device group ids
     * @private
     */
    filterAndRequireGroupMatch(model, deviceGroups) {
      return (qb) => {
        const subqueryMatch = bookshelf.knex.count().from(`group_${model}`)
          .whereIn('group_id', deviceGroups)
          .whereRaw(`\`${model}_id\` = \`${model}\`.\`id\``);
        const subqueryNone = bookshelf.knex.count().from(`group_${model}`)
          .whereRaw(`\`${model}_id\` = \`${model}\`.\`id\``);

        qb.whereRaw(`( (${subqueryMatch}) > 0 OR (${subqueryNone}) = 0)`);
      };
    },

    /**
     * Applies device group filters to the received models if they are currently in use.
     * Received models which are not included in parameters.withRelated and do not match the
     * current model table name are discarded.
     * Models array argument contain the name of model database tables. It is assumed that
     * the table 'id' key is referenced by a 'group_model' table via 'model_id' field.
     * Received parameters.withRelated is updated to include translations models and to only load
     * models with translations.
     *
     *
     * @param {Array} models Array of models' database table names
     * @param {Object} parameters Parameters object to be used in fetch
     * @param {Array} deviceGroups Array of Device Group ids
     */
    applyDeviceGroupScopes(models, parameters, deviceGroups) {
      const withRelated = parameters.withRelated || [];

      /*
       * Iterate over received models and include filter by device groups of the models in use
       */
      models.forEach((modelName) => {
        /*
         * Check wether filtering current model
         */
        const currentModel = modelName === this.tableName;
        if (currentModel) {
          /*
           * If so, filter current query by device groups
           */
          this.query(this.filterAndRequireGroupMatch(modelName, deviceGroups));
        } else {
          /*
           * Otherwise, check if model is present in withRelated parameter
           */
          for (let i = 0; i < withRelated.length; i += 1) {
            const related = withRelated[i];
            const isObject = typeof related === 'object';
            const isString = typeof related === 'string';

            if ((isString && related === modelName) || (isObject && related[modelName])) {
              /*
               * If the model is being loaded, replace withRelated item to include behavior
               * of filtering the model loading query by device groups
               */
              let newRelated = {};
              if (isString) {
                newRelated[modelName] = this.filterAndRequireGroupMatch(modelName, deviceGroups);
              } else {
                // Keep existing behavior and add the filter to require its translations
                newRelated = _.cloneDeep(related);
                delete newRelated[modelName];
                newRelated[modelName] = (qb) => {
                  related[modelName](qb);
                  this.filterAndRequireGroupMatch(modelName, deviceGroups)(qb);
                };
              }
              withRelated[i] = newRelated;
            }
          }
        }
      });

      /*
       * Replace withRelated parameters with updated value
       */
      parameters.withRelated = withRelated;
      return this;
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
