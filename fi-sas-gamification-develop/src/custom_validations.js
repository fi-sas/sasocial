import checkIt from 'checkit';

export default function setValidators() {
  checkIt.Validator.prototype.date = function date(value) {
    return value instanceof Date && (!Number.isNaN(value.getTime()));
  };

  /**
   * oneOf validator for enum validations.
   * Usage example: { type: `oneOf:${JSON.stringify(Types)}` }
   * @param {} value 
   * @param {*} options 
   */
  checkIt.Validator.prototype.oneOf = function oneOf(value, options) {
    return (JSON.parse(options)).includes(value);
  };
}
