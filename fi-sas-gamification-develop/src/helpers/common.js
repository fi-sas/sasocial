/**
 * Helper to access common data from request.
 */
/* eslint-disable import/prefer-default-export */
import got from 'got';

/**
 * Returns device id from authInfo of received request.
 *
 * @param {Object} req Request object
 */
function getDeviceId(req) {
  return req.authInfo && req.authInfo.deviceId ? req.authInfo.deviceId : null;
}

/**
 * Promises an array of device group ids.
 * Uses authorization token in headers of received request and device id in authInfo from
 * received request.
 *
 * @param {Object} req Request object
 */
async function getDeviceGroups(req) {
  let groupIds = [];

  if (!req.authInfo || !req.authInfo.deviceId) {
    return groupIds;
  }

  const { deviceId } = req.authInfo;
  const auth = req.headers.authorization;

  try {
    const result = await got(`${process.env.MS_CONFIG_ENDPOINT}api/v1/devices/${deviceId}/groups`, {
      headers: {
        Authorization: auth,
      },
    });

    if (result.statusCode === 200) {
      const body = JSON.parse(result.body);
      const { data } = body;
      if (data instanceof Array) {
        groupIds = data.reduce((p, c) => { p.push(c.id); return p; }, []);
      }
    }
  } catch (error) {
    // TODO logging
    console.log(error);
  }

  return groupIds;
}

/**
 * Returns the language id to be used in translatable models - null if none is specified in headers
 * and -1 if specified language is not valid.
 *
 * Uses X-Language-ID header as first effort to get current language. If no header is sent, uses
 * X-Language-Acronym header and MS Configurations to find specified language. If no language can be
 * find with specified acronym, returns -1.
 * If no headers are sent, returns null.
 *
 * Gives no guarantee about the validity of the retuned id.
 *
 * @param {Object} req Request object
 * @returns {number=} languageId null if no headers are specified, -1 if no valid id is sent
 */
async function getLanguageId(req) {
  const error = -1;
  let languageId = null;

  if (req.headers && req.headers['x-language-id'] && req.headers['x-language-id'] !== null) {
    languageId = !isNaN(req.headers['x-language-id'])
      ? parseInt(req.headers['x-language-id'], 10)
      : error;
  } else if (req.headers && req.headers['x-language-acronym']
    && req.headers['x-language-acronym'] !== null && req.headers['x-language-acronym'].length > 0
  ) {
    const acronym = req.headers['x-language-acronym'];
    try {
      const auth = req.headers.authorization;
      const result = await got(`${process.env.MS_CONFIG_ENDPOINT}api/v1/languages?acronym=${acronym}`, {
        headers: {
          Authorization: auth,
        },
      });
      const body = (result.statusCode === 200) ? JSON.parse(result.body) : [];
      languageId = (body.data && body.data.length > 0)
        ? body.data[0].id
        : error;
    } catch (e) {
      languageId = error;
    }
  }

  return languageId;
}

/**
 * Checks the provided object for the existance of file ids. The found file ids are added to the
 * fileIds array.
 *
 * @param {Object} obj The object to search for file ids.
 * @param {Array} fileIds The array of file ids at the moment.
 */
function gatherFileIdsInObject(obj, field, fileIds) {
  // Try to search field f in the original object p
  if (obj[field] && !fileIds.includes(obj[field])) {
    fileIds.push(obj[field]);
  }
}

/**
 * Checks the provided data structure to find the ids of the files that should be fetched from
 * the Medias MS.
 *
 * @param {*} data Data that will be analyzed.
 * @param {*} relatedToFields The names of the fields that should be searched for.
 */
function gatherFileIdsInData(data, relatedToFields) {
  const fileIds = [];
  if (data instanceof Array) {
    data.forEach((p) => {
      Object.values(relatedToFields).forEach((f) => {
        gatherFileIdsInObject(p, f, fileIds);

        // Try also to search the field f inside the translations key of object p
        if (p.translations && p.translations instanceof Array) {
          p.translations.forEach((tr) => {
            gatherFileIdsInObject(tr, f, fileIds);
          });
        }
      });
    });
  } else {
    Object.values(relatedToFields).forEach((f) => {
      gatherFileIdsInObject(data, f, fileIds);

      // Try also to search the field f inside the translations key of data
      if (data.translations && data.translations instanceof Array) {
        data.translations.forEach((tr) => {
          gatherFileIdsInObject(tr, f, fileIds);
        });
      }
    });
  }

  return fileIds;
}

/**
 * Returns promise of received data with added media files in received related properties.
 * Uses mapRelatedToFields object to find the field used to reference a related entity.
 *
 * Usage:
 * Model.forge()
 *   .filterOrderAndFetch(req.parameters)
 *   .then((collection) => {
 *     const data = collection ? collection.toJSON() : [];
 *     const link = (collection.pagination) ?
 *       getPaginationMetaData(collection.pagination, req) :
 *       {};
 *     const mapRelatedToFields = {
 *       media: 'media_id'
 *     };
 *     loadMediaFiles(req, data, ['media'], mapRelatedToFields)
 *       .then(data => res.formatter.ok(data, link));
 *   })
 *
 * @param {Object} req Request object
 * @param {Object|Array} data Array of objects or single object
 * @param {Array} related Array with related entities to be loaded (must me mapped in
 * mapRelatedToFields argument)
 * @param {Object} mapRelatedToFields Map between related entity names and the field that
 * references them in (each) data object
 */
async function loadMediaFiles(req, data, related, mapRelatedToFields = {}) {
  const auth = req.headers.authorization;

  /*
   * Find mapping between related entities currently in use and fields
   */
  const relatedToFields = Object.keys(mapRelatedToFields)
    .filter(rel => related.includes(rel))
    .reduce((obj, key) => ({
      ...obj,
      [key]: mapRelatedToFields[key],
    }), {});

  const fileIds = gatherFileIdsInData(data, relatedToFields);

  /*
   * Fetch all media files from MS
   */
  let files = [];
  if (fileIds.length) {
    const idsStr = fileIds.map(id => `id=${id}`).join('&');
    // TODO update withRelated=false as soon as medias MS uses loadTranslations
    const link = `${process.env.MS_MEDIA_ENDPOINT}api/v1/files?${idsStr}&withRelated=translations`;
    try {
      const result = await got(link, {
        headers: {
          Authorization: auth,
        },
      });

      if (result.statusCode === 200) {
        const body = JSON.parse(result.body);
        files = body.data;
      }
    } catch (error) {
      // TODO logging
      console.log(link);
      console.log(error);
    }
  }

  /*
   * Transform files array into filesById
   */
  const filesById = (array => array.reduce((obj, item) => {
    const newObj = obj;
    newObj[item.id] = item;
    return newObj;
  }, {}))(files);

  /*
   * Iterate over data to load related medias with received files or empty object
   */
  let newData;
  if (data instanceof Array) {
    newData = [];
    data.forEach((item) => {
      const newItem = item;
      related.forEach((r) => {
        if (mapRelatedToFields[r]) {
          const idField = mapRelatedToFields[r];
          if (typeof item[idField] !== 'undefined') {
            const idValue = item[idField];
            const relatedValue = (filesById[idValue]) ? filesById[idValue] : {};
            newItem[r] = relatedValue;
          } else if (item.translations) {
            item.translations.forEach((tr) => {
              const newTr = tr;
              const idValue = tr[idField];
              newTr[r] = (filesById[idValue]) ? filesById[idValue] : {};
            });
          }
        }
      });
      newData.push(newItem);
    });
  } else {
    newData = data;
    related.forEach((r) => {
      if (mapRelatedToFields[r]) {
        const idField = mapRelatedToFields[r];
        if (typeof data[idField] !== 'undefined') {
          const idValue = data[idField];
          const relatedValue = (filesById[idValue]) ? filesById[idValue] : {};
          newData[r] = relatedValue;
        } else if (data.translations) {
          data.translations.forEach((tr) => {
            const newTr = tr;
            const idValue = tr[idField];
            newTr[r] = (filesById[idValue]) ? filesById[idValue] : {};
          });
        }
      }
    });
  }

  return newData;
}

/**
 * Gathers IDs present in received data used in the received related entities.
 * Related entities contains path property used to fetch the ids.
 *
 * @param {Object} data
 * @param {*} relatedEntities
 */
function gatherIds(data, relatedEntities) {
  const getFromArray = (d, index) => d.map(e => e[index]);
  const ids = [];

  Object.keys(relatedEntities).forEach((r) => {
    const { path } = relatedEntities[r];
    const idsFromPath = path.split('.').reduce((previous, current) => {
      const isArray = (current.startsWith('[') && current.endsWith(']'));
      const index = isArray ? current.slice(1, -1) : current;
      // Flat results:
      return [].concat(...getFromArray(previous instanceof Array ? previous : [previous], index));
    }, data instanceof Array ? data : [data]);
    ids.push(...idsFromPath);
  });

  return ids;
}

/**
 * Inserts related entity in the received object, in the received destination.
 * The related entity is fetched from dataById and the id in the received path inside object.
 *
 * This is a recursive function.
 * The path and destination contain properties separated by '.'. If a property is surrounded by '['
 * and ']', it means that the property is read from an array.
 * To successfully insert the related entity into the received object, the path and destination
 * untill an array property are found.
 * - If no path at all is received, then we stop the recursion. In this case, the first obj argument
 * is used as id value and the entity with same id from dataById is returned (or empty object if
 * none is defined).
 * - If path untill array is found, then the object with found destination will have the value of a
 * recursive call with the object with found path.
 * - If path untill array property is not found, we are facing an array property in the current
 * call. This means that obj is actually an array, and each of its elements will be updated with the
 * result of a recursive call with the element and the remaining path and destination.
 *
 *
 * @param {Object|Array} obj Object into which related entity will be inserted (or related ID if
 * last recursion call)
 * @param {String} path Path to access related id from object (empty if last recursion call)
 * @param {String} destination Path used to insert the related entity in the object
 * @param {Object} dataById Data of related entities by id
 */
function insertRelated(obj, path, destination, dataById) {
  // Find paths and index to the first array path
  const paths = path ? path.split('.') : [];
  let firstArrayPathIndex = -1;
  paths.forEach((p, index) => {
    if (firstArrayPathIndex === -1 && p.startsWith('[') && p.endsWith(']')) {
      firstArrayPathIndex = index;
    }
  });

  // Find destinations and index to the first array destination
  const destinations = destination ? destination.split('.') : [];
  let firstArrayDestinationIndex = -1;
  destinations.forEach((p, index) => {
    if (firstArrayDestinationIndex === -1 && p.startsWith('[') && p.endsWith(']')) {
      firstArrayDestinationIndex = index;
    }
  });

  // If no paths, return data
  if (!path.length) {
    // Halt
    return (dataById[obj]) ? dataById[obj] : {};
  }

  // If no destination, return recursive call
  if (!destination.length) {
    // We are going without destination, simply run the recursion as needed
    let pathIndex = paths.shift();
    if (obj instanceof Array) {
      // If array, iterate over its elements and run recursive call with next path
      pathIndex = pathIndex.slice(1, -1);
      return obj.map((element) => {
        let newElement = element;
        newElement = insertRelated(element[pathIndex], paths.join('.'), destinations.join('.'), dataById);
        return newElement;
      });
    }
    // If object, iterate over its elements and run recursive call with next path
    return insertRelated(obj[pathIndex], paths.join('.'), destinations.join('.'), dataById);
  }

  // If there is path untill first array path, assign it to recursive call
  let newObject = null;
  // Aux function to update an original object in the destinations array with final value.
  const update = (original, dests, finalValue) => {
    const current = dests.shift();
    const updated = original;

    if (!dests.length) {
      // Last destination, use final value
      updated[current] = finalValue;
    } else {
      // Continue to next destination
      updated[current] = update(original[current], dests, finalValue);
    }
    return updated;
  };

  if (firstArrayDestinationIndex !== 0) {
    const directPaths = firstArrayPathIndex === -1
      ? paths : paths.slice(0, firstArrayPathIndex);
    const directDestinations = firstArrayDestinationIndex === -1
      ? destinations : destinations.slice(0, firstArrayDestinationIndex);

    const remainingPath = firstArrayPathIndex === -1
      ? '' : paths.slice(firstArrayPathIndex).join('.');
    const remainingDestination = firstArrayDestinationIndex === -1
      ? '' : destinations.slice(firstArrayDestinationIndex).join('.');

    // Recursive call insertRelated using object from direct paths and remaining path
    const objectFromPath = directPaths.reduce((previous, current) => previous[current], obj);
    const value = insertRelated(objectFromPath, remainingPath, remainingDestination, dataById);

    // Use recursive result to update object from direct destinations
    newObject = update(obj, directDestinations, value);
  } else {
    // Facing an array path!
    const pathIndex = paths.shift().slice(1, -1);
    const destinationIndex = destinations.shift().slice(1, -1);
    newObject = obj.map((element) => {
      const newElement = element;
      newElement[destinationIndex] = insertRelated(element[pathIndex], paths.join('.'), destinations.join('.'), dataById);
      return newElement;
    });
  }

  return newObject;
}

/**
 * Returns promise of received data with included related entities from external services.
 *
 * Usage:
 * Model.forge()
 *   .filterOrderAndFetch(req.parameters)
 *   .then((collection) => {
 *     const data = collection ? collection.toJSON() : [];
 *     const link = (collection.pagination) ?
 *       getPaginationMetaData(collection.pagination, req) :
 *       {};
 *     const map = {
 *       groups: {
 *        targetMS: 'MS_CONFIG_ENDPOINT',
 *        targetResource: 'groups',
 *        path: 'groups.[group_id]',
 *         destination: 'groups',
 *       },
 *       media: {
 *         targetMS: 'MS_MEDIA_ENDPOINT',
 *         targetResource: 'files',
 *         path: 'media_id',
 *         destination: 'media',
 *       },
 *     };
 *     loadRelated(req, data, req.withRelated, mapRelatedToFields)
 *       .then(data => res.formatter.ok(data, link));
 *   })
 *
 * @param {Object} req Request object
 * @param {Object|Array} data Array of objects or single object
 * @param {Array} withRelated Array with related entities to be loaded
 * @param {Object} map Map between related entities and their target MS and target resource,
 * destination on the data object for the related entity and path from which to read their ID
 */
async function loadRelated(req, data, withRelated, map = {}) {
  const auth = req.headers.authorization;

  /*
   * Find mapping between related entities currently in use and group them by resource
   */
  const relatedMapping = Object.keys(map)
    .filter(rel => withRelated.includes(rel))
    .reduce((previous, current) => {
      const reduced = previous;
      const { targetResource } = map[current];
      const targetMS = process.env[map[current].targetMS];
      const target = `${targetMS}api/v1/${targetResource}`;

      if (!previous[target]) {
        reduced[target] = {};
      }

      reduced[target][current] = map[current];
      return reduced;
    }, {});

  /*
   * Now, for each target in related mapping, gather the ids and collect the data from target
   */
  const relatedDataById = {};
  await Promise.all(Object.keys(relatedMapping).map(async (target) => {
    const relatedOfTarget = relatedMapping[target];

    // Gather ids
    const ids = gatherIds(data, relatedOfTarget);

    // Fetch related data
    let relatedData = [];
    if (ids.length) {
      const idsStr = ids.map(id => `id=${id}`).join('&');
      const url = `${target}?${idsStr}&withRelated=false`;
      try {
        const result = await got(url, {
          headers: {
            Authorization: auth,
          },
        });

        if (result.statusCode === 200) {
          const body = JSON.parse(result.body);
          relatedData = body.data;
        }
      } catch (error) {
        // TODO logging
        console.log(url);
        console.log(error);
      }
    }

    /*
     * Transform related data array into relatedDataById
     */
    relatedDataById[target] = (array => array.reduce((obj, item) => {
      const newObj = obj;
      newObj[item.id] = item;
      return newObj;
    }, {}))(relatedData);

    return true;
  }));

  /*
   * Finally, for each target in related mapping, insert the collected data as related entities
   */
  let newData = data;
  Object.keys(relatedMapping).forEach((target) => {
    const relatedMappingOfTarget = relatedMapping[target];
    const byID = relatedDataById[target];

    /*
     * Iterate over data to load related entities with collected data or empty object
     */
    if (data instanceof Array) {
      newData = newData.map((item) => {
        let newItem = item;
        withRelated.forEach((r) => {
          if (relatedMappingOfTarget[r]) {
            const { path, destination } = relatedMappingOfTarget[r];
            newItem = insertRelated(newItem, path, destination, byID);
          }
        });
        return newItem;
      });
    } else {
      withRelated.forEach((r) => {
        if (relatedMappingOfTarget[r]) {
          const { path, destination } = relatedMappingOfTarget[r];
          newData = insertRelated(newData, path, destination, byID);
        }
      });
    }
  });

  return newData;
}

/**
 * Helper function to fetch data from raw object.
 *
 * Example:
 * // patching: data will contain only the defined values
 * const data = getData(req.body, ['name', 'name2', 'name3'], true);
 * // putting: data will contain all the fields (possibly with undefined values)
 * const data = getData(req.body, ['name', 'name2', 'name3'], false);
 *
 * @param {Object} raw Raw data object
 * @param {Array} fields Array of fields to get
 * @param {boolean} onlyIfPresent If true, returned data will only contain the keys for which
 * there is a value defined (patch way). If false, data will contain all received fields (put way).
 * @returns {Object} Data object
 */
function getData(raw, fields, onlyIfPresent = true) {
  let data = {};
  if (onlyIfPresent) {
    data = Object.keys(raw).filter(key => fields.includes(key)).reduce((obj, key) => {
      /* eslint-disable no-param-reassign */
      obj[key] = raw[key];
      return obj;
    }, {});
  } else {
    fields.forEach(key => data[key] = raw[key]);
  }
  return data;
}

export {
  getDeviceId, getDeviceGroups, getLanguageId, loadMediaFiles, getData, loadRelated,
};
