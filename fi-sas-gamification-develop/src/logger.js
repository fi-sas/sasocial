import log4js from 'log4js';

const categories = {
  default: { appenders: ['logstash'], level: 'info' },
};
categories[process.env.MICROSERVICE_IDENTIFIER] = { appenders: ['logstash'], level: 'info' };

// Log configurations
log4js.configure({
  appenders: {
    logstash: {
      type: 'log4js-logstash-tcp',
      host: 'fisas_logs_logstash',
      port: 5000,
    },
  },
  categories,
});


export default log4js.getLogger(process.env.MICROSERVICE_IDENTIFIER);
