import requestParametersMiddleware from '../request-parameters';
import { ParameterError } from '../../errors';
import { InvalidSortParameter, InvalidFilterParameter, InvalidWithRelatedParameter } from '../../error_codes';

describe('RequestParameters Middleware test', () => {
  const next = jest.fn();

  describe('Initial setup on request', () => {
    const mockedRequest = { query: {} };

    it('defines req.parameters wih empty ordering and filters, default pagination properties and null withRelated', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      expect(mockedRequest.parameters).toBeDefined();
      expect(mockedRequest.parameters.pagination).toBeDefined();
      expect(mockedRequest.parameters.ordering).toHaveLength(0);
      expect(mockedRequest.parameters.filters).toBeDefined();
      expect(mockedRequest.parameters.filters).toHaveLength(0);
      expect(mockedRequest.parameters.withRelated).toBeNull();
    });

    it('defines req.parameters.validate function', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      expect(mockedRequest.parameters).toBeDefined();
      expect(mockedRequest.parameters.validate).toBeDefined();
      expect(mockedRequest.parameters.validate).toBeInstanceOf(Function);
    });

    it('defines req.parameters.parseFilters function', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      expect(mockedRequest.parameters).toBeDefined();
      expect(mockedRequest.parameters.parseFilters).toBeDefined();
      expect(mockedRequest.parameters.parseFilters).toBeInstanceOf(Function);
    });
  });

  describe('WithRelated parameter test', () => {
    const mockedRequest1 = {
      query: {
        withRelated: 'related1,related2',
      },
    };
    const mockedRequest2 = {
      query: {
        withRelated: 'false',
      },
    };
    const mockedRequest3 = {
      query: {
      },
    };

    it('captures comma-separated value into array', () => {
      requestParametersMiddleware()(mockedRequest1, {}, next);
      expect(mockedRequest1.parameters.withRelated).toBeInstanceOf(Array);
      expect(mockedRequest1.parameters.withRelated).toHaveLength(2);
      expect(mockedRequest1.parameters.withRelated).toContain('related1');
      expect(mockedRequest1.parameters.withRelated).toContain('related2');
    });

    it('captures false value into empty array', () => {
      requestParametersMiddleware()(mockedRequest2, {}, next);
      expect(mockedRequest2.parameters.withRelated).toBeInstanceOf(Array);
      expect(mockedRequest2.parameters.withRelated).toHaveLength(0);
    });

    it('uses null value if nothing is sent', () => {
      requestParametersMiddleware()(mockedRequest3, {}, next);
      expect(mockedRequest3.parameters.withRelated).toBeNull();
    });
  });

  describe('Pagination parameter test', () => {
    const mockedRequest = {
      query: {
        limit: '11',
        offset: '12',
      },
    };

    it('uses default values when a valid parameter value is not received', () => {
      const mockedRequestNo1 = {
        query: {
        },
      };
      const mockedRequestNo2 = {
        query: {
          limit: '-10',
          offset: 'a',
        },
      };
      const mockedRequestNo3 = {
        query: {
          limit: '0',
          offset: '0',
        },
      };

      requestParametersMiddleware()(mockedRequestNo1, {}, next);
      expect(mockedRequestNo1.parameters.pagination).toBeDefined();
      expect(mockedRequestNo1.parameters.pagination.offset).toBe(0);
      expect(mockedRequestNo1.parameters.pagination.limit).toBe(10);

      requestParametersMiddleware()(mockedRequestNo2, {}, next);
      expect(mockedRequestNo2.parameters.pagination).toBeDefined();
      expect(mockedRequestNo2.parameters.pagination.offset).toBe(0);
      expect(mockedRequestNo2.parameters.pagination.limit).toBe(10);

      requestParametersMiddleware()(mockedRequestNo3, {}, next);
      expect(mockedRequestNo3.parameters.pagination).toBeDefined();
      expect(mockedRequestNo3.parameters.pagination.offset).toBe(0);
      expect(mockedRequestNo3.parameters.pagination.limit).toBe(10);
    });

    it('uses received valid values values', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      expect(mockedRequest.parameters.pagination).toBeDefined();
      expect(mockedRequest.parameters.pagination.offset).toBe(12);
      expect(mockedRequest.parameters.pagination.limit).toBe(11);
    });

    it('applies no pagination when limit=-1', () => {
      const mockedRequest1 = {
        query: {
          limit: '-1',
          offset: '12',
        },
      };

      requestParametersMiddleware()(mockedRequest1, {}, next);
      expect(mockedRequest1.parameters.pagination).toBeNull();
    });
  });

  describe('Validate test', () => {
    const mockedRequest = {
      query: {
        filterExample: '1',
        sort: 'sortExample1,-sortExample2',
        withRelated: 'related',
      },
    };

    it('passes validation when no fields to validate are sent', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      const v = () => { mockedRequest.parameters.validate(); };
      expect(v).not.toThrow();
    });

    it('passes validation when valid fields are used', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      const v = () => {
        mockedRequest.parameters.validate(
          ['filterExample'],
          ['sortExample1', 'sortExample2'],
          ['related'],
        );
      };
      expect(v).not.toThrow();
    });

    it('throws ParameterError when empty fields to validate are sent', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      const v = () => { mockedRequest.parameters.validate([], [], []); };
      expect(v).toThrowError(ParameterError);
    });

    it('reports InvalidSortParameter errors', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      let error;
      try {
        mockedRequest.parameters.validate(null, ['valid1', 'valid2']);
      } catch (e) {
        error = e;
      }
      expect(error).toBeInstanceOf(ParameterError);
      expect(error.errors).toBeDefined();
      expect(error.errors).toHaveLength(2);
      expect(error.errors[0].code).toBe(InvalidSortParameter);
      expect(error.errors[0].parameter).toBe('sortExample1');
      expect(error.errors[1].code).toBe(InvalidSortParameter);
      expect(error.errors[1].parameter).toBe('sortExample2');
    });

    it('reports InvalidFilterParameter errors', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      let error;
      try {
        mockedRequest.parameters.validate(['valid1', 'valid2'], null);
      } catch (e) {
        error = e;
      }
      expect(error).toBeInstanceOf(ParameterError);
      expect(error.errors).toBeDefined();
      expect(error.errors).toHaveLength(1);
      expect(error.errors[0].code).toBe(InvalidFilterParameter);
      expect(error.errors[0].parameter).toBe('filterExample');
    });

    it('reports InvalidWithRelatedParameter errors', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      let error;
      try {
        mockedRequest.parameters.validate(null, null, ['valid1', 'valid2']);
      } catch (e) {
        error = e;
      }
      expect(error).toBeInstanceOf(ParameterError);
      expect(error.errors).toBeDefined();
      expect(error.errors).toHaveLength(1);
      expect(error.errors[0].code).toBe(InvalidWithRelatedParameter);
      expect(error.errors[0].parameter).toBe('related');
    });

    it('reports InvalidFilterParameter, InvalidFilterParameter and InvalidWithRelated errors', () => {
      requestParametersMiddleware()(mockedRequest, {}, next);
      let error;
      try {
        mockedRequest.parameters.validate(['valid1', 'valid2'], ['valid1', 'valid2'], ['valid1', 'valid2']);
      } catch (e) {
        error = e;
      }
      expect(error).toBeInstanceOf(ParameterError);
      expect(error.errors).toBeDefined();
      expect(error.errors).toHaveLength(4);
    });
  });

  describe('ParseFilters test', () => {
    const mockedRequestEmpty = {
      query: {
      },
    };
    const mockedRequestFilters = {
      query: {
        filterExample1: 'value1',
        filterExample2: 'value2',
      },
    };

    const parserNo1 = {
    };
    const parserNo2 = {
      filterExample3: () => 'parsed',
    };

    const parser1 = {
      filterExample1: () => 'parsed',
      filterExample2: () => 'parsed',
    };
    const parser2 = {
      filterExample1: () => 'parsed',
    };

    it('has no effect with empty parameter filters', () => {
      requestParametersMiddleware()(mockedRequestEmpty, {}, next);
      const previousFilters = mockedRequestEmpty.parameters.filters;
      const v = () => { mockedRequestEmpty.parameters.parseFilters(parser1); };
      const nextFilters = mockedRequestEmpty.parameters.filters;
      expect(v).not.toThrow();
      expect(previousFilters).toEqual(nextFilters);
    });

    it('has no effect with empty parser', () => {
      requestParametersMiddleware()(mockedRequestFilters, {}, next);
      const previousFilters = mockedRequestFilters.parameters.filters;
      const v = () => { mockedRequestFilters.parameters.parseFilters(parserNo1); };
      const nextFilters = mockedRequestFilters.parameters.filters;
      expect(v).not.toThrow();
      expect(previousFilters).toEqual(nextFilters);
    });

    it('has no effect with mismatched parser', () => {
      requestParametersMiddleware()(mockedRequestFilters, {}, next);
      const previousFilters = mockedRequestFilters.parameters.filters;
      const v = () => { mockedRequestFilters.parameters.parseFilters(parserNo2); };
      const nextFilters = mockedRequestFilters.parameters.filters;
      expect(v).not.toThrow();
      expect(previousFilters).toEqual(nextFilters);
    });

    it('parses filters with complete parser', () => {
      requestParametersMiddleware()(mockedRequestFilters, {}, next);
      mockedRequestFilters.parameters.parseFilters(parser1);
      const nextFilters = mockedRequestFilters.parameters.filters;
      const expectedFilters = [
        {
          field: 'filterExample1',
          value: 'parsed',
        },
        {
          field: 'filterExample2',
          value: 'parsed',
        },
      ];
      expect(nextFilters).toEqual(expectedFilters);
    });

    it('parses filters with incomplete parser', () => {
      requestParametersMiddleware()(mockedRequestFilters, {}, next);
      mockedRequestFilters.parameters.parseFilters(parser2);
      const nextFilters = mockedRequestFilters.parameters.filters;
      const expectedFilters = [
        {
          field: 'filterExample1',
          value: 'parsed',
        },
        {
          field: 'filterExample2',
          value: 'value2',
        },
      ];
      expect(nextFilters).toEqual(expectedFilters);
    });
  });
});
