/**
 * Async await routes.
 *
 * Usage:
 * import asyncMiddleware from '../middleware/async';
 * router.get('/', asyncMiddleware(async (req, res, next) => {
 *  // ...
 * });
 */
export default fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};
