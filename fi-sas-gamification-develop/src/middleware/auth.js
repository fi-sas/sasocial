/**
 * Authentication Middleware: requires valid token in request.
 *
 * Throws Unauthorized Error when invalid token is sent.
 * Uses decoded info in req.authInfo.
 * @public
 */

import * as Codes from '../error_codes';

export default async function requireAuth(req, res, next) {
  // Ignore token validation for OPTIONS requests
  if (req.method === 'OPTIONS') {
    return next();
  }

  if (req.authInfo === Codes.AuthenticationInvalidToken) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationInvalidToken,
      message: 'Failed to authenticate token.',
    });
  }

  if (req.authInfo === Codes.AuthenticationMissingToken) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationMissingToken,
      message: 'No token provided.',
    });
  }

  return next();
}
