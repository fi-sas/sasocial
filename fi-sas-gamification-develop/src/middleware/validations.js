import * as Codes from '../error_codes';

export default [
  {
    resource: /api\/v1\/challenges(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'challenge_type_id',
    targetMS: process.env.MS_GAMIFICATION_ENDPOINT,
    targetResource: 'challenge/types',
    endpoints: ['POST', 'PATCH'],
    code: Codes.InvalidChallengeType,
    message: 'The challenge type id provided is not valid.',
  },
  {
    resource: /api\/v1\/challenges(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'service_id',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'services',
    endpoints: ['POST', 'PATCH'],
    code: Codes.InvalidServiceId,
    message: 'The service id provided is not valid.',
  },
  {
    resource: /api\/v1\/(challenges|competitions)(\/[^/]*)?(\?.*)?$/m,
    arrayPath: 'translations',
    indexPath: 'language_id',
    targetMS: process.env.MS_CONFIG_ENDPOINT,
    targetResource: 'languages',
    endpoints: ['POST', 'PATCH'],
    code: Codes.InvalidLanguageId,
    message: 'The language id provided is not valid.',
  },
  {
    resource: /api\/v1\/competitions(\/[^/]*)?(\?.*)?$/m,
    arrayPath: 'challenges',
    indexPath: 'challenge_id',
    targetMS: process.env.MS_GAMIFICATION_ENDPOINT,
    targetResource: 'challenges',
    endpoints: ['POST', 'PATCH'],
    code: Codes.InvalidChallenge,
    message: 'The challenge id provided is not valid.',
  },
];
