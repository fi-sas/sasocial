import Joi from 'joi';
import { uniqBy } from 'lodash';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import ChallengeSchema from '../schemas/challenge-schema';
import ChallengeType from './challengeType';
import ChallengeTranslation from './challengeTranslation';

const Challenge = Bookshelf.Model.extend({
  tableName: 'challenge',
  hasTimestamps: true,

  format(attributes) {
    return this.formatModelTimestamps(
      attributes,
      [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
      ],
    );
  },

  translations() {
    return this.hasMany(ChallengeTranslation);
  },

  players() {
    return this.belongsToMany('Player').through('ChallengePlayer');
  },

  challengeType() {
    return this.belongsTo(ChallengeType);
  },

  points() {
    return this.hasMany('Points');
  },

  challengePlayer() {
    return this.hasMany('ChallengePlayer');
  },

  competitionChallenge() {
    return this.hasMany('CompetitionChallenge');
  },

  playerRanking(challengeId, parameters) {
    const subquery = Bookshelf.knex
      .select(Bookshelf.knex.raw('player_id, sum(points) as score'))
      .from('points')
      .where('challenge_id', challengeId)
      .groupBy('player_id')
      .orderBy('score', 'desc');

    const query = Bookshelf.knex
      .select(Bookshelf.knex.raw('player_id, nickname, score, @n := @n + 1 as position'))
      .from(Bookshelf.knex.raw(`(select @n := 0) x, (${subquery}) y`))
      .innerJoin('player', 'id', 'player_id');

    const playerId = parameters.filters.filter(i => i.field === 'player_id');
    if (playerId.length && playerId[0].value) {
      return Bookshelf.knex.select()
        .from(Bookshelf.knex.raw(`(${query}) t`))
        .where('player_id', playerId[0].value);
    }

    if (parameters.pagination) {
      const { limit, offset } = parameters.pagination;
      const wrapper = Bookshelf.knex.select().from(Bookshelf.knex.raw(`(${query}) t`));
      if (limit) {
        wrapper.limit(limit);
      }
      if (offset) {
        wrapper.offset(offset);
      }
      return wrapper;
    }

    return query;
  },

  teamRanking(challengeId, parameters) {
    const subquery = Bookshelf.knex
      .select(Bookshelf.knex.raw('team_id, sum(points) as score, name'))
      .from('points as p')
      .innerJoin('team_player as tp', 'p.player_id', 'tp.player_id')
      .innerJoin('team as t', 't.id', 'team_id')
      .where('p.challenge_id', challengeId)
      .andWhere('tp.status', 'Member')
      .groupBy('team_id')
      .orderBy('score', 'desc');

    const query = Bookshelf.knex
      .select(Bookshelf.knex.raw('team_id, name, score, @n := @n + 1 as position'))
      .from(Bookshelf.knex.raw(`(select @n := 0) x, (${subquery}) y`));

    const teamId = parameters.filters.filter(i => i.field === 'team_id');
    if (teamId.length && teamId[0].value) {
      return Bookshelf.knex.select()
        .from(Bookshelf.knex.raw(`(${query}) t`))
        .where('team_id', teamId[0].value);
    }

    if (parameters.pagination) {
      const { limit, offset } = parameters.pagination;
      const wrapper = Bookshelf.knex.select().from(Bookshelf.knex.raw(`(${query}) t`));
      if (limit) {
        wrapper.limit(limit);
      }
      if (offset) {
        wrapper.offset(offset);
      }
      return wrapper;
    }

    return query;
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    const translations = this.load('translations')
      .then(item => item.related('translations').invokeThen('destroy', opts));

    const points = this.load('points')
      .then(item => item.related('points').invokeThen('destroy', opts));

    const challengePlayer = this.load('challengePlayer')
      .then(item => item.related('challengePlayer').invokeThen('destroy', opts));

    const competitionChallenge = this.load('competitionChallenge')
      .then(item => item.related('competitionChallenge').invokeThen('destroy', opts));

    return Promise.all([translations, points, challengePlayer, competitionChallenge]);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the tag
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, ChallengeSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate unique tag
    if (this.hasChanged('tag')) {
      const tagExists = await this.query((qb) => {
        qb.where('tag', this.get('tag'));
        if (this.id) {
          qb.andWhereNot('id', this.id);
        }
      }).count();
      if (tagExists) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The tag already exists',
          field: 'tag',
          index: 0,
        }]);
      }
    }

    // challenge: start date is before end date
    if (this.hasChanged('start_date') || this.hasChanged('end_date')) {
      const start = new Date(this.get('start_date'));
      start.setHours(0, 0, 0, 0);
      const end = new Date(this.get('end_date'));
      end.setHours(23, 59, 59, 999);
      if (start > end) {
        throw new Errors.ValidationError([{
          code: Codes.InvalidChallengeDateRange,
          message: 'The start date is greater than end date',
        }]);
      }
    }

    return true;
  },

  saveWithRelated(data, translations, opts = {}) {
    const withRelated = ['translations'];
    const partial = (opts.patch === true);
    let uniqueTranslations = false;

    if (translations && !(translations instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'translations',
        message: 'translations must be an array',
      }]);
    }

    const needsTranslations = opts.method === 'insert' || (opts.patch === false);
    if (needsTranslations && !(translations && translations.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'translations',
        message: 'Missing required translations',
      }]);
    }

    if (translations && translations instanceof Array) {
      uniqueTranslations = uniqBy(translations.reverse(), 'language_id');
    }

    return Bookshelf.transaction(t => this.save(data, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        if (uniqueTranslations) {
          // Load translations
          return item.load('translations', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations && !partial) {
          // Remove existing translations unless partial update
          return item.related('translations').invokeThen('destroy', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations) {
          if (!partial) {
            // Directly insert received translations
            return Promise.all(uniqueTranslations.map((tr) => {
              const translationData = Object.assign(tr, { challenge_id: item.get('id') });
              return item.related('translations').create(translationData, { transacting: t });
            }));
          }
          // If partial, since existing translations were not remove, handle received
          // translations to update or insert each
          return Promise.all(uniqueTranslations.map((tr) => {
            const translationData = Object.assign(tr, { challenge_id: item.get('id') });
            const translation = item.related('translations').find(el => el.get('language_id') === tr.language_id);
            if (!translation) {
              return item.related('translations').create(translationData, { transacting: t });
            }
            return translation.save(translationData, { transacting: t });
          }));
        }
        return true;
      }))
      .tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Challenge not found');
      });
  },
}, {
  filterFields: [
    'id',
    'challenge_type_id',
    'points',
    'rules',
    'automated',
    'tag',
    'service_id',
    'start_date',
    'end_date',
    'winners',
    'created_at',
    'updated_at',
  ],
  sortFields: [
    'id',
    'challenge_type_id',
    'points',
    'automated',
    'service_id',
    'start_date',
    'end_date',
    'winners',
    'created_at',
    'updated_at',
  ],
  related: ['challengeType', 'translations', 'players'],
  defaultRelated: ['challengeType', 'translations'],
});

export default Bookshelf.model('Challenge', Challenge);
