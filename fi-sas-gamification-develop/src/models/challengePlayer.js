import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Challenge from './challenge';
import Player from './player';
import ChallengePlayerSchema from '../schemas/challenge-player-schema';

const ChallengePlayer = Bookshelf.Model.extend({
  tableName: 'challenge_player',
  hasTimestamps: true,

  challenge() {
    return this.belongsTo(Challenge);
  },

  player() {
    return this.belongsTo(Player);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the player_id
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(
      data,
      ChallengePlayerSchema,
      options.patch === true ? {} : { presence: 'required' },
    );
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate challenge exists
    if (this.hasChanged('challenge_id')) {
      await Challenge.forge()
        .query('where', 'id', this.get('challenge_id'))
        .fetch({ require: true })
        .catch(Bookshelf.NotFoundError, () => {
          throw new Errors.ValidationError([{
            code: Codes.InvalidChallenge,
            message: 'Could not find Challenge',
            field: 'challenge_id',
            index: 0,
          }]);
        });
    }

    // Validate player exists
    if (this.hasChanged('player_id')) {
      await Player.forge()
        .query('where', 'id', this.get('player_id'))
        .fetch({ require: true })
        .catch(Bookshelf.NotFoundError, () => {
          throw new Errors.ValidationError([{
            code: Codes.InvalidPlayer,
            message: 'Could not find Player',
            field: 'player_id',
            index: 0,
          }]);
        });
    }

    // Validate ChallengePlayer doesn't exist
    const exists = await this.query((qb) => {
      qb.where('player_id', this.get('player_id'))
        .andWhere('challenge_id', this.get('challenge_id'));
    }).count();
    if (exists) {
      throw new Errors.ValidationError({
        code: Codes.ValidationDuplicateUniqueValue,
        message: 'The challenge-player relation already exists',
      });
    }

    return true;
  },
}, {
  filterFields: ['id', 'challenge_id', 'player_id', 'created_at', 'updated_at'],
  sortFields: ['id', 'challenge_id', 'player_id', 'created_at', 'updated_at'],
  related: ['challenge', 'player'],
  defaultRelated: [],
});

export default Bookshelf.model('ChallengePlayer', ChallengePlayer);
