import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import ChallengeTypeSchema from '../schemas/challenge-type-schema';

const ChallengeType = Bookshelf.Model.extend({
  tableName: 'challenge_type',

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the name
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, ChallengeTypeSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate unique tag
    if (this.hasChanged('tag')) {
      const tag = await this.query('where', 'tag', this.get('tag')).count();
      if (tag) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The ChallengeType tag already exists',
          field: 'tag',
          index: 0,
        }]);
      }
    }

    return true;
  },
}, {
  filterFields: ['id', 'tag'],
  sortFields: ['id'],
  related: [],
  defaultRelated: [],
});

export default Bookshelf.model('ChallengeType', ChallengeType);
