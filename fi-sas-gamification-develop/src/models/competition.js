import Joi from 'joi';
import { uniqBy } from 'lodash';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import CompetitionTranslation from './competitionTranslation';
import CompetitionChallenge from './competitionChallenge';
import Challenge from './challenge';
import CompetitionSchema from '../schemas/competition-schema';

const Competition = Bookshelf.Model.extend({
  tableName: 'competition',
  hasTimestamps: true,

  challenges() {
    return this.belongsToMany(Challenge).through(CompetitionChallenge);
  },

  competitionChallenges() {
    return this.hasMany(CompetitionChallenge);
  },

  translations() {
    return this.hasMany(CompetitionTranslation);
  },

  playerRanking(competitionId, parameters) {
    const cpquery = Bookshelf.knex.select('challenge_id')
      .from('competition_challenge')
      .where('competition_id', competitionId);

    const subquery = Bookshelf.knex
      .select(Bookshelf.knex.raw('player_id, sum(points) as score, nickname'))
      .from('points')
      .innerJoin('player as p', 'p.id', 'player_id')
      .whereIn('challenge_id', cpquery)
      .groupBy('player_id')
      .orderBy('score', 'desc');

    const query = Bookshelf.knex
      .select(Bookshelf.knex.raw('player_id, score, nickname, @n := @n + 1 as position'))
      .from(Bookshelf.knex.raw(`(select @n := 0) x, (${subquery}) y`));

    const playerId = parameters.filters.filter(i => i.field === 'player_id');
    if (playerId.length && playerId[0].value) {
      return Bookshelf.knex.select()
        .from(Bookshelf.knex.raw(`(${query}) t`))
        .where('player_id', playerId[0].value);
    }

    if (parameters.pagination) {
      const { limit, offset } = parameters.pagination;
      const wrapper = Bookshelf.knex.select().from(Bookshelf.knex.raw(`(${query}) t`));
      if (limit) {
        wrapper.limit(limit);
      }
      if (offset) {
        wrapper.offset(offset);
      }
      return wrapper;
    }

    return query;
  },

  teamRanking(competitionId, parameters) {
    const cpquery = Bookshelf.knex.select('challenge_id')
      .from('competition_challenge')
      .where('competition_id', competitionId);

    const subquery = Bookshelf.knex
      .select(Bookshelf.knex.raw('team_id, sum(points) as score, name'))
      .from('points as p')
      .innerJoin('team_player as tp', 'p.player_id', 'tp.player_id')
      .innerJoin('team as t', 't.id', 'team_id')
      .whereIn('p.challenge_id', cpquery)
      .andWhere('tp.status', 'Member')
      .groupBy('team_id')
      .orderBy('score', 'desc');

    const query = Bookshelf.knex
      .select(Bookshelf.knex.raw('team_id, name, score, @n := @n + 1 as position'))
      .from(Bookshelf.knex.raw(`(select @n := 0) x, (${subquery}) y`));

    const teamId = parameters.filters.filter(i => i.field === 'team_id');
    if (teamId.length && teamId[0].value) {
      return Bookshelf.knex.select()
        .from(Bookshelf.knex.raw(`(${query}) t`))
        .where('team_id', teamId[0].value);
    }

    if (parameters.pagination) {
      const { limit, offset } = parameters.pagination;
      const wrapper = Bookshelf.knex.select().from(Bookshelf.knex.raw(`(${query}) t`));
      if (limit) {
        wrapper.limit(limit);
      }
      if (offset) {
        wrapper.offset(offset);
      }
      return wrapper;
    }

    return query;
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the name
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, CompetitionSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    return true;
  },

  async saveWithRelated(id, translations, challenges, opts = {}) {
    const withRelated = ['translations', 'challenges.translations'];
    const partial = (opts.patch === true);
    let uniqueTranslations = false;

    if (challenges && !(challenges instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'challenges',
        message: 'challenges must be an array',
      }]);
    }

    const needsChallenges = opts.method === 'insert' || (opts.patch === false);
    if (needsChallenges && !(challenges && challenges.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'challenges',
        message: 'Missing required challenges',
      }]);
    }

    if (translations && !(translations instanceof Array)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationInvalidFormat,
        field: 'translations',
        message: 'translations must be an array',
      }]);
    }

    const needsTranslations = opts.method === 'insert' || (opts.patch === false);
    if (needsTranslations && !(translations && translations.length)) {
      throw new Errors.ValidationError([{
        code: Codes.ValidationRequiredValue,
        field: 'translations',
        message: 'Missing required translations',
      }]);
    }

    if (translations && translations instanceof Array) {
      uniqueTranslations = uniqBy(translations.reverse(), 'language_id');
    }

    return Bookshelf.transaction(t => this.save({}, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        // only POST & PATCH are available (no PUT exists) to change Competition data.
        // removing existing competition_challenges, but only when PATCH'ing data.
        if (challenges && partial) {
          CompetitionChallenge
            .forge()
            .where('competition_id', item.get('id'))
            .destroy();
        }
        return true;
      })
      .tap((item) => {
        // insert received challenges
        if (challenges) {
          return Promise.all(challenges.map((cc) => {
            return CompetitionChallenge.forge().save({
              competition_id: item.get('id'),
              challenge_id: cc.challenge_id,
            }, { method: 'insert', transacting: t });
          }));
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations) {
          // Load translations
          return item.load('translations', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations && !partial) {
          // Remove existing translations unless partial update
          return item.related('translations').invokeThen('destroy', { transacting: t });
        }
        return true;
      })
      .tap((item) => {
        if (uniqueTranslations) {
          if (!partial) {
            // Directly insert received translations
            return Promise.all(uniqueTranslations.map((tr) => {
              const translationData = Object.assign(tr, { competition_id: item.get('id') });
              return item.related('translations').create(translationData, { transacting: t });
            }));
          }
          // If partial, since existing translations were not remove, handle received
          // translations to update or insert each
          return Promise.all(uniqueTranslations.map((tr) => {
            const translationData = Object.assign(tr, { competition_id: item.get('id') });
            const translation = item.related('translations').find(el => el.get('language_id') === tr.language_id);
            if (!translation) {
              return item.related('translations').create(translationData, { transacting: t });
            }
            return translation.save(translationData, { transacting: t });
          }));
        }
        return true;
      }))
      .tap(item => item.refresh({ withRelated }))
      .catch(Bookshelf.NoRowsUpdatedError, () => {
        throw new Errors.NotFoundError('Competition not found');
      });
  },

  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    const translations = this.load('translations')
      .then(item => item.related('translations').invokeThen('destroy', opts));

    const competitionChallenges = this.load('competitionChallenges')
      .then(item => item.related('competitionChallenges').invokeThen('destroy', opts));

    return Promise.all([translations, competitionChallenges]);
  },
}, {
  filterFields: ['id', 'created_at', 'updated_at'],
  sortFields: ['id', 'created_at', 'updated_at'],
  related: ['challenges', 'challenges.translations', 'translations'],
  defaultRelated: ['translations'],
});

export default Bookshelf.model('Competition', Competition);
