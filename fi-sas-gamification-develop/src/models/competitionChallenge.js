import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Competition from './competition';
import Challenge from './challenge';
import CompetitionChallengeSchema from '../schemas/competition-challenge-schema';

const CompetitionChallenge = Bookshelf.Model.extend({
  tableName: 'competition_challenge',
  hasTimestamps: true,

  competition() {
    return this.hasOne(Competition);
  },

  challenge() {
    return this.hasOne(Challenge);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the player_id
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(
      data,
      CompetitionChallengeSchema,
      options.patch === true ? {} : { presence: 'required' },
    );
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    const opts = Object.assign({ require: true }, options);

    // Validate competition exists
    if (this.hasChanged('competition_id')) {
      await Competition.forge()
        .query('where', 'id', this.get('competition_id'))
        .fetch(opts)
        .catch(Bookshelf.NotFoundError, () => {
          throw new Errors.ValidationError([{
            code: Codes.InvalidCompetition,
            message: 'Could not find Competition',
            field: 'competition_id',
            index: 0,
          }]);
        });
    }

    // Validate challenge exists
    if (this.hasChanged('challenge_id')) {
      await Challenge.forge()
        .query('where', 'id', this.get('challenge_id'))
        .fetch(opts)
        .catch(Bookshelf.NotFoundError, () => {
          throw new Errors.ValidationError([{
            code: Codes.InvalidChallenge,
            message: 'Could not find Challenge',
            field: 'challenge_id',
            index: 0,
          }]);
        });
    }

    // Validate CompetitionChallenge doesn't exist
    const exists = await this.query((qb) => {
      qb.where('competition_id', this.get('competition_id'))
        .andWhere('challenge_id', this.get('challenge_id'));
    }).count();
    if (exists) {
      throw new Errors.ValidationError({
        code: Codes.ValidationDuplicateUniqueValue,
        message: 'The challenge-player relation already exists',
      });
    }

    return true;
  },
}, {
  filterFields: ['id', 'competition_id', 'challenge_id', 'created_at', 'updated_at'],
  sortFields: ['id', 'competition_id', 'challenge_id', 'created_at', 'updated_at'],
  related: ['competition', 'challenge'],
  defaultRelated: [],
});

export default Bookshelf.model('CompetitionChallenge', CompetitionChallenge);
