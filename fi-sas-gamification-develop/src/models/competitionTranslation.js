import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import CompetitionTranslationSchema from '../schemas/competition-translation-schema';

const CompetitionTranslation = Bookshelf.Model.extend({
  tableName: 'competition_translation',
  hidden: ['id', 'competition_id'],

  competition() {
    return this.belongsTo('Competition');
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    // Validate data according to model schema
    const result = Joi.validate(data, CompetitionTranslationSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
    return true;
  },
}, {
  filterFields: ['name'],
  defaultRelated: [],
  sortFields: [],
  related: [],
});

export default Bookshelf.model('CompetitionTranslation', CompetitionTranslation);
