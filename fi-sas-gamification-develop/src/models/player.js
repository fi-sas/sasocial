import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Team from './team';
import TeamPlayer from './teamPlayer';
import PlayerSchema from '../schemas/player-schema';

const Player = Bookshelf.Model.extend({
  tableName: 'player',
  hasTimestamps: true,

  team() {
    return this.belongsToMany(Team).through(TeamPlayer);
  },

  teamPlayer() {
    return this.belongsTo(TeamPlayer, 'id', 'player_id');
  },

  challengePlayer() {
    return this.hasMany('ChallengePlayer');
  },

  points() {
    return this.hasMany('Points');
  },

  teamInvites() {
    return this.belongsToMany(Team).through(TeamPlayer)
      .query({ where: { status: 'Invite' } });
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    const teamPlayer = this.load('teamPlayer', opts)
      .then(it => it.related('teamPlayer').destroy(opts));

    const challengePlayer = this.load('challengePlayer')
      .then(item => item.related('challengePlayer').invokeThen('destroy', opts));

    const points = this.load('points')
      .then(item => item.related('points').invokeThen('destroy', opts));

    return Promise.all([teamPlayer, challengePlayer, points]);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the nickname
   * and user uniqueness.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, PlayerSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate unique nickname
    if (this.hasChanged('nickname')) {
      const nickname = await this.query('where', 'nickname', this.get('nickname')).count();
      if (nickname) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The nickname already exists',
          field: 'nickname',
          index: 0,
        }]);
      }
    }

    // Validate unique user id
    if (this.hasChanged('user_id')) {
      const user = await this.query('where', 'user_id', this.get('user_id')).count();
      if (user) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The user_id already exists',
          field: 'user_id',
          index: 0,
        }]);
      }
    }

    return true;
  },
}, {
  filterFields: ['id', 'user_id', 'nickname', 'created_at', 'updated_at'],
  sortFields: ['id', 'user_id', 'created_at', 'updated_at'],
  related: ['team', 'teamInvites'],
  defaultRelated: ['team'],

  /**
   * Find a Player through a user_id.
   * @param {integer} id The id of the User.
   */
  findByUser(id) {
    return this.where('user_id', id).fetch();
  },
});

export default Bookshelf.model('Player', Player);
