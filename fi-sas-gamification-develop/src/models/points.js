import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Player from './player';
import Challenge from './challenge';
import PointsSchema from '../schemas/points-schema';

const Points = Bookshelf.Model.extend({
  tableName: 'points',
  hasTimestamps: true,

  challenge() {
    return this.belongsTo(Challenge);
  },

  player() {
    return this.belongsTo(Player);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the tag
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, PointsSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    return true;
  },

  async saveScore(data, opts = {}) {
    const challenge = await Challenge.forge()
      .query('where', 'id', data.challenge_id)
      .fetch({ require: true })
      .catch(Bookshelf.NotFoundError, () => {
        throw new Errors.NotFoundError('Challenge not found');
      });

    const now = new Date();
    now.setHours(0, 0, 0, 0);

    if (challenge.get('start_date')) {
      const start = new Date(challenge.get('start_date'));
      start.setHours(0, 0, 0, 0);
      if (now < start) {
        throw new Errors.ValidationError([{
          code: Codes.InvalidChallengeDateRange,
          message: 'Challenge start date out of range',
        }]);
      }
    }

    if (challenge.get('end_date')) {
      const end = new Date(challenge.get('end_date'));
      end.setHours(0, 0, 0, 0);
      if (now > end) {
        throw new Errors.ValidationError([{
          code: Codes.InvalidChallengeDateRange,
          message: 'Challenge end date out of range',
        }]);
      }
    }

    await Player.forge()
      .query('where', 'id', data.player_id)
      .fetch({ require: true })
      .catch(Bookshelf.NotFoundError, () => {
        throw new Errors.ValidationError([{
          code: Codes.InvalidPlayer,
          message: 'Could not find Player',
          field: 'player_id',
          index: 0,
        }]);
      });

    return this.save(Object.assign(data, { points: challenge.get('points') }), opts);
  },
}, {
  filterFields: ['id', 'challenge_id', 'player_id', 'points', 'created_at', 'updated_at'],
  sortFields: ['id', 'challenge_id', 'player_id', 'points', 'created_at', 'updated_at'],
  related: ['challenge', 'player'],
  defaultRelated: [],
});

export default Bookshelf.model('Points', Points);
