import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import TeamSchema from '../schemas/team-schema';
import TeamPlayer from './teamPlayer';
import Player from './player';

const Team = Bookshelf.Model.extend({
  tableName: 'team',
  hasTimestamps: true,

  players() {
    return this.belongsToMany(Player, 'team_player')
      .query({ where: { status: 'Member' } });
  },

  invites() {
    return this.belongsToMany(Player, 'team_player')
      .query({ where: { status: 'Invite' } });
  },

  requests() {
    return this.belongsToMany(Player, 'team_player')
      .query({ where: { status: 'Request' } });
  },

  teamPlayers() {
    return this.hasMany(TeamPlayer);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);
  },

  destroyCascade(model, options) {
    // Cascade delete associated elements
    const opts = { transacting: options.transacting };
    return Promise.all([
      this.load('teamPlayers', opts).then(it => it.related('teamPlayers').invokeThen('destroy', opts)),
    ]);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the name
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(data, TeamSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate unique name
    if (this.hasChanged('name')) {
      const name = await this.query('where', 'name', this.get('name')).count();
      if (name) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The Team name already exists',
          field: 'name',
          index: 0,
        }]);
      }
    }

    return true;
  },

  /**
   * Saves Team and TeamPlayer association in a single transaction.
   * Only a Player can create a Team, the Player that creates the Team is the leader.
   * The Player cannot be part of any other Team. The Team name must be unique.
   *
   * @param {Object} data
   * @param {Object=} opts
   */
  async saveWithLeader(data, opts = {}) {
    const player = await Player.forge().where('user_id', data.user_id).fetch();
    if (!player) {
      throw new Errors.ValidationError([{
        code: Codes.InvalidPlayer,
        message: 'Could not find Player',
        field: 'user_id',
        index: 0,
      }]);
    }

    const teamPlayer = {
      player_id: player.get('id'),
      leader: true,
      status: 'Member',
    };
    const team = { name: data.name };

    return Bookshelf.transaction(t => this.save(team, Object.assign(opts, { transacting: t }))
      .tap((item) => {
        Object.assign(teamPlayer, { team_id: item.get('id') });
        return TeamPlayer.forge().save(teamPlayer, { transacting: t });
      }));
  },
}, {
  filterFields: ['id', 'name', 'created_at', 'updated_at'],
  sortFields: ['id', 'created_at', 'updated_at'],
  related: ['players', 'teamPlayers', 'invites', 'requests'],
  defaultRelated: ['players'],
});

export default Bookshelf.model('Team', Team);
