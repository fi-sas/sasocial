import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Team from './team';
import Player from './player';
import TeamPlayerSchema from '../schemas/team-player-schema';

const TeamPlayer = Bookshelf.Model.extend({
  tableName: 'team_player',
  hasTimestamps: true,

  format(attributes) {
    return this.formatBooleans(attributes, ['leader']);
  },

  team() {
    return this.hasOne(Team);
  },

  player() {
    return this.hasOne(Player);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate model instance being saved according to the Joi schema and the player_id
   * must be unique.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    // Validate data according to model schema
    const result = Joi.validate(
      data,
      TeamPlayerSchema,
      options.patch === true ? {} : { presence: 'required' },
    );
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate player exists and player_id is unique
    if (this.hasChanged('player_id')) {
      await Player.forge()
        .query('where', 'id', this.get('player_id'))
        .fetch({ require: true })
        .catch(Bookshelf.NotFoundError, () => {
          throw new Errors.ValidationError([{
            code: Codes.InvalidPlayer,
            message: 'Could not find Player',
            field: 'player_id',
            index: 0,
          }]);
        });

      const player = await this.query('where', 'player_id', this.get('player_id')).count();
      if (player) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          message: 'The Team Player\'s already exists',
          field: 'player_id',
          index: 0,
        }]);
      }
    }

    // Teams have a limit of 20 Players
    const playersQuantity = await this.query((qb) => {
      qb.where('team_id', this.get('team_id'))
        .andWhere('status', 'Member');
    }).count();
    if (playersQuantity >= 20) {
      throw new Errors.ValidationError([{
        code: Codes.TeamPlayerLimit,
        message: 'Team limit reached',
      }]);
    }

    return true;
  },
}, {
  filterFields: ['id', 'team_id', 'player_id', 'status', 'leader', 'created_at', 'updated_at'],
  sortFields: ['id', 'team_id', 'player_id', 'leader', 'created_at', 'updated_at'],
  related: ['team', 'player'],
  defaultRelated: [],
});

export default Bookshelf.model('TeamPlayer', TeamPlayer);
