/**
 * @swagger
 *
 * components:
 *   parameters:
 *    idsFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific IDs.
 *    challengeTypeFilter:
 *      in: query
 *      name: tag
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by Challenge Type Tag.
 *   schemas:
 *     ChallengeType:
 *       description: Challenge Type
 *       allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The Challenge Type ID.
 *              example: 1
 *            tag:
 *              type: string
 *              nullable: false
 *              description: The Tag description of the Challenge Type, must be unique.
 *            description:
 *              type: string
 *              nullable: true
 *              description: A Challenge Type description.
 */
import { Router } from 'express';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';

import { getPaginationMetaData } from '../helpers/metadata';
import { getData } from '../helpers/common';

import ChallengeType from '../models/challengeType';

const challengeTypesRouter = new Router();

/**
 * @swagger
 * /api/v1/challenges/types:
 *   get:
 *     tags:
 *       - Challenge Types
 *     description: Returns a list of Challenge Types.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idsFilter'
 *       - $ref: '#/components/parameters/challengeTypeFilter'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/ChallengeType'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
challengeTypesRouter.get('/', (req, res, next) => {
  req.parameters.parseFilters({ tag: v => `%${v}%` });

  req.parameters.validate(
    ChallengeType.filterFields,
    ChallengeType.sortFields,
    ChallengeType.defaultRelated,
  );

  ChallengeType.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/types:
 *    parameters:
 *    post:
 *      tags:
 *        - Challenge Types
 *      description: >
 *        This endpoint is used to create a new Challenge Type. The request must
 *        be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Challenge Type data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ChallengeType'
 *      responses:
 *        201:
 *          description: Created Challenge Type object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/ChallengeType'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengeTypesRouter.post('/', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const data = getData(req.body, ['tag', 'description'], true);

  ChallengeType.forge(data)
    .save()
    .then((item) => {
      res.formatter.created(item.toJSON({ omitPivot: true }));
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/types/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge Type.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Challenge Types
 *      description: Returns information of the Challenge Type with the provided ID.
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/ChallengeType'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengeTypesRouter.get('/:id', (req, res, next) => {
  ChallengeType.forge({ id: req.params.id })
    .fetch({ require: true })
    .then((item) => {
      res.formatter.ok(item.toJSON({ omitPivot: true }));
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Challenge Type not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/types/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge Type.
 *        example: 1
 *        schema:
 *          type: integer
 *    patch:
 *      tags:
 *        - Challenge Types
 *      description: >
 *        Update Challenge Type name or description. The request must be
 *        authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      requestBody:
 *        name: feed
 *        description: The Challenge Type data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ChallengeType'
 *      responses:
 *        200:
 *          description: The updated Challenge Type object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/ChallengeType'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengeTypesRouter.patch('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const data = getData(req.body || {}, ['name', 'description'], true);

  ChallengeType.forge({ id: req.params.id })
    .fetch({ require: true })
    .then(item => item.save(data, { method: 'update', patch: true }))
    .tap(item => item.refresh())
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Challenge Type not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/types/{id}:
 *    delete:
 *      tags:
 *        - Challenge Types
 *      description: >
 *        Delete a Challenge Type. The request must be authenticated and only
 *        authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengeTypesRouter.delete('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  Bookshelf.transaction(t => ChallengeType.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => { res.formatter.noContent(); })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Challenge Type not found');
    })
    .catch(next);
});

export default challengeTypesRouter;
