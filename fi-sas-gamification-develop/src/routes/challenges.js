/**
 * @swagger
 *
 * components:
 *   parameters:
 *    idsFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific Challenge IDs.
 *    nameFilter:
 *      in: query
 *      name: name
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial name.
 *    automatedFilter:
 *      in: query
 *      name: automated
 *      required: false
 *      schema:
 *        type: boolean
 *      description: Filter by Automated Challenges.
 *    challengeTagFilter:
 *      in: query
 *      name: tag
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter Challenges by their Tag.
 *    serviceIDFilter:
 *      in: query
 *      name: service_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter Challenges by Service ID.
 *    challengeStartDateFilter:
 *      in: query
 *      name: start_date
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial Challenge's start date.
 *    challengeEndDateFilter:
 *      in: query
 *      name: end_date
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial Challenge's end date.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedChallenge:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Allowed value: 'challengeType'.
 *      example: challengeType
 *    withRelatedChallengePlayers:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Allowed values: 'challengeType' and 'players'.
 *      example: challengeType,players
 *    playerIdRankingFilter:
 *      in: query
 *      name: player_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter Challenge Ranking by Player ID.
 *    teamIdRankingFilter:
 *      in: query
 *      name: team_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter Challenge Ranking by Team ID.
 *   schemas:
 *     Challenge:
 *       description: A Challenge.
 *       allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The Challenge ID.
 *              example: 1
 *            challenge_type_id:
 *              type: integer
 *              nullable: true
 *              description: The Challenge Type ID.
 *              example: 1
 *            points:
 *              type: integer
 *              nullable: false
 *              description: The number of points awarded by this Challenge.
 *              example: 5
 *            automated:
 *              type: boolean
 *              nullable: false
 *              description: >
 *                Indicates if the Challenge is automated or not. An automated challenge
 *                is created by other micro services. If the Challenge is created
 *                in the BackOffice, then it is not automated.
 *              example: true
 *            tag:
 *              type: string
 *              nullable: true
 *              description: The Challenge unique identifier string.
 *              example: UBIKE_100KM_CHALLENGE
 *            service_id:
 *              type: integer
 *              nullable: true
 *              description: >
 *                The origin micro service that created the Challenge. Only use this
 *                field for automated Challenges.
 *              example: 1
 *            start_date:
 *              type: string
 *              format: date
 *              nullable: false
 *              description: The start date of the Challenge.
 *              example: 2019-01-01T09:00:00.000Z
 *            end_date:
 *              type: string
 *              format: date
 *              nullable: false
 *              description: The end date of the Challenge.
 *              example: 2019-01-01T09:00:00.000Z
 *            winners:
 *              type: integer
 *              nullable: false
 *              description: The number of Player prize winners.
 *              example: 3
 *            updated_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of the last update on the Challenge.
 *              example: 2019-01-01T09:00:00.000Z
 *            created_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of creation of the Challenge.
 *              example: 2019-01-01T09:00:00.000Z
 *            translations:
 *              type: array
 *              required: true
 *              description: The translations associated with this Challenge.
 *              items:
 *                $ref: '#/components/schemas/ChallengeTranslation'
 *            players:
 *              readOnly: true
 *              type: array
 *              description: The Players inscribed in the Challenge.
 *              items:
 *                $ref: '#/components/schemas/Player'
 *     ChallengeTranslation:
 *       type: object
 *       properties:
 *         language_id:
 *           type: integer
 *           description: The ID of the language for this ChallengeTranslation.
 *           example: 1
 *         name:
 *           type: string
 *           nullable: false
 *           description: The name of the Challenge.
 *         description:
 *           type: string
 *           nullable: false
 *           description: A description of the Challenge.
 *         rules:
 *           type: string
 *           nullable: false
 *           description: Text explaining the Challenge rules.
 *         team_prize:
 *           type: string
 *           nullable: true
 *           description: A text describing the Challenge team prizes.
 *         player_prize:
 *           type: string
 *           nullable: true
 *           description: A description of the Challenge player prizes.
 *     ChallengePlayer:
 *       type: object
 *       properties:
 *         player_id:
 *           type: integer
 *           description: The ID of the Player joining the Challenge.
 *           example: 1
 *         challenge_id:
 *           type: integer
 *           description: The ID of the Challenge the Player is going to join.
 *           example: 1
 *         updated_at:
 *           type: string
 *           readOnly: true
 *           format: date
 *           nullable: false
 *           description: The date of the last update on the ChallengePlayer.
 *           example: 2019-01-01T09:00:00.000Z
 *         created_at:
 *           type: string
 *           readOnly: true
 *           format: date
 *           nullable: false
 *           description: The date of creation of the ChallengePlayer.
 *           example: 2019-01-01T09:00:00.000Z
 *     PlayersRanking:
 *       type: object
 *       properties:
 *         player_id:
 *           readOnly: true
 *           type: integer
 *           description: The ID of the Player.
 *           example: 1
 *         score:
 *           readOnly: true
 *           type: integer
 *           description: The total points scored in this challenge.
 *           example: 1
 *         position:
 *           readOnly: true
 *           type: integer
 *           description: The position of the Player in this Challenge ranking.
 *           example: 1
 *         nickname:
 *           readOnly: true
 *           type: string
 *           description: The nickname of the Player.
 *     TeamsRanking:
 *       type: object
 *       properties:
 *         team_id:
 *           readOnly: true
 *           type: integer
 *           description: The ID of the Team.
 *           example: 1
 *         score:
 *           readOnly: true
 *           type: integer
 *           description: The total points scored in this challenge.
 *           example: 1
 *         position:
 *           readOnly: true
 *           type: integer
 *           description: The position of the Team in this Challenge ranking.
 *           example: 1
 *         name:
 *           readOnly: true
 *           type: string
 *           description: The name of the Team.
 *     Points:
 *       type: object
 *       properties:
 *         challenge_id:
 *           readOnly: true
 *           type: integer
 *           description: The ID of the Challenge.
 *           example: 1
 *         player_id:
 *           type: integer
 *           description: The ID of the Player.
 *           example: 1
 *         points:
 *           readOnly: true
 *           type: integer
 *           nullable: false
 *           description: The number of points scored.
 *           example: 1
 *         updated_at:
 *           readOnly: true
 *           type: string
 *           format: date
 *           description: The date of the last update on the Point.
 *           example: 2019-01-01T09:00:00.000Z
 *         created_at:
 *           readOnly: true
 *           type: string
 *           format: date
 *           description: The date of creation of the Point.
 *           example: 2019-01-01T09:00:00.000Z
 */
import { Router } from 'express';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import asyncMiddleware from '../middleware/async';
import { getLanguageId, getData } from '../helpers/common';
import { getPaginationMetaData } from '../helpers/metadata';

import Player from '../models/player';
import Points from '../models/points';
import Challenge from '../models/challenge';
import ChallengePlayer from '../models/challengePlayer';
import ChallengeTranslation from '../models/challengeTranslation';

const challengesRouter = new Router();

/**
 * @swagger
 * /api/v1/challenges:
 *   get:
 *     tags:
 *       - Challenges
 *     description: >
 *       Returns a list of Challenges.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/languageIdHeader'
 *       - $ref: '#/components/parameters/languageAcronHeader'
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idsFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/automatedFilter'
 *       - $ref: '#/components/parameters/challengeTagFilter'
 *       - $ref: '#/components/parameters/serviceIDFilter'
 *       - $ref: '#/components/parameters/challengeStartDateFilter'
 *       - $ref: '#/components/parameters/challengeEndDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/withRelatedChallenge'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Challenge'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
challengesRouter.get('/', asyncMiddleware(async (req, res, next) => {
  req.parameters.withRelated = req.parameters.withRelated || Challenge.defaultRelated.slice();

  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      automated: v => ((v === 'true') ? 1 : 0),
      start_date: v => `%${v}%`,
      end_date: v => `%${v}%`,
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  req.parameters.validate(
    Challenge.filterFields + ChallengeTranslation.filterFields,
    Challenge.sortFields,
    Challenge.defaultRelated,
  );

  const languageId = await getLanguageId(req);

  Challenge.forge()
    .loadTranslations(
      [{ name: 'challenge', filters: ChallengeTranslation.filterFields }],
      req.parameters,
      languageId,
    )
    .then(tr => tr.filterOrderAndFetch(req.parameters))
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/challenges:
 *    parameters:
 *    post:
 *      tags:
 *        - Challenges
 *      description: >
 *        This endpoint is used to create a new Challenge. The request must
 *        be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Challenge data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Challenge'
 *      responses:
 *        201:
 *          description: Created Challenge object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Challenge'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.post('/', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const data = getData(
    req.body,
    [
      'challenge_type_id',
      'points',
      'automated',
      'tag',
      'service_id',
      'start_date',
      'end_date',
      'winners',
    ],
    true,
  );

  const { translations } = req.body;

  return Challenge.forge()
    .saveWithRelated(data, translations, { method: 'insert' })
    .then(item => res.formatter.created(item.toJSON()))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Challenges
 *      description: >
 *        Returns information of the Challenge with the provided ID.
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *        - $ref: '#/components/parameters/withRelatedChallengePlayers'
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Challenge'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  const languageId = await getLanguageId(req);

  req.parameters.validate(null, null, Challenge.related);

  const translations = await Challenge.forge()
    .where('id', req.params.id)
    .loadTranslations(['challenge'], req.parameters, languageId);

  translations
    .fetch({ require: true, withRelated: req.parameters.withRelated || Challenge.related.slice() })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Challenge not found');
    })
    .catch(next);
}));

/**
 * @swagger
 * /api/v1/challenges/{id}:
 *   parameters:
 *     - in: path
 *       name: id
 *       schema:
 *         type: integer
 *       required: true
 *       description: Numeric ID of the Challenge.
 *   put:
 *     tags:
 *       - Challenges
 *     description: >
 *       Update a Challenge data. The request must be authenticated and only authorized
 *       to BackOffice users.<br>
 *       **NOTE:** if `translations` are received, Challenge's translations will be replaced.
 *       That means that, for instance, if your Challenge has 2 associated translations, and the
 *       PUT body provides only one translation, then the returned result will be only one
 *       translation - the one that was provided in the body.
 *     produces:
 *       - application/json
 *     requestBody:
 *       name: translation
 *       description: The complete Challenge data to update.
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Challenge'
 *     responses:
 *       201:
 *         description: The Challenge successfully updated.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Challenge'
 *       400:
 *         $ref: '#/components/responses/BadRequest'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
challengesRouter.put('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const data = getData(
    req.body,
    [
      'challenge_type_id',
      'points',
      'automated',
      'tag',
      'service_id',
      'start_date',
      'end_date',
      'winners',
    ],
    false,
  );

  const { translations } = req.body;

  return Challenge.forge({ id: req.params.id })
    .saveWithRelated(data, translations, { method: 'update', patch: false })
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch(next);
});

/**
 * @swagger
 * /api/v1/challenges/{id}:
 *   parameters:
 *     - in: path
 *       name: id
 *       schema:
 *         type: integer
 *       required: true
 *       description: Numeric ID of the Challenge.
 *   patch:
 *     tags:
 *       - Challenges
 *     description: >
 *       Partially update a Challenge data. The request must be authenticated and
 *       only authorized to BackOffice users. <br>
 *       **NOTE:** Translations will be updated/incremented with the received translations.
 *       That means that, for instance, if the challenge had 2 associated translations,
 *       and the PATCH body provides only one (existing) translation, then the returned
 *       result will be two translations - the one that existed previously, and the one
 *       that was provided with the values that were provided in the request body.
 *     produces:
 *       - application/json
 *     requestBody:
 *       name: category
 *       description: The partial Challenge data to update.
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Challenge'
 *     responses:
 *       200:
 *         description: The Challenge successfully updated.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Challenge'
 *       400:
 *         $ref: '#/components/responses/BadRequest'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
challengesRouter.patch('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const data = getData(
    req.body,
    [
      'challenge_type_id',
      'points',
      'automated',
      'tag',
      'service_id',
      'start_date',
      'end_date',
      'winners',
    ],
    true,
  );

  const { translations } = req.body;

  return Challenge.forge({ id: req.params.id })
    .saveWithRelated(data, translations, { method: 'update', patch: true })
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}:
 *    delete:
 *      tags:
 *        - Challenges
 *      description: >
 *        Delete Challenge and all related data like Translations. The request must
 *        be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.delete('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  return Bookshelf.transaction(t => Challenge.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Challenge not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}/join:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge to join.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Challenges
 *      description: >
 *        Adds a Player (the logged user) to the Challenge.<br>
 *        The logged user must be a registered player, and cannot have joined the challenge
 *        previously.
 *      produces:
 *        - application/json
 *      responses:
 *        201:
 *          description: The Challenge the player has joined.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/ChallengePlayer'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.post('/:id/join', (req, res, next) => {
  ChallengePlayer.forge({
    challenge_id: req.params.id,
    player_id: req.authInfo.user.id,
  })
    .save()
    .then(item => res.formatter.created(item.toJSON()))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}/player/{playerId}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge.
 *        example: 1
 *        schema:
 *          type: integer
 *      - in: path
 *        name: playerId
 *        required: true
 *        description: Numeric ID of the Player.
 *        example: 1
 *        schema:
 *          type: integer
 *    delete:
 *      tags:
 *        - Challenges
 *      description: >
 *        Removes a Player from a Challenge, and all the Player score in the Challenge.
 *        Only a BackOffice user or the Player (with the same user as in the request)
 *        can remove from the Challenge.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.delete('/:id/player/:playerId', asyncMiddleware(async (req, res, next) => {
  if (!req.backoffice) {
    const player = await Player.forge().where('id', req.params.playerId).fetch();
    if (!player || player.get('user_id') !== req.authInfo.user.id) {
      return res.formatter.unauthorized({
        code: Codes.AuthenticationLackOfPermissions,
        message: 'Unauthorized. Backoffice or the Player can remove from Challenge.',
      });
    }
  }

  return Bookshelf.transaction(t => ChallengePlayer.query((qb) => {
    qb.where('challenge_id', req.params.id)
      .andWhere('player_id', req.params.playerId);
  })
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('ChallengePlayer not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/challenges/{id}/score:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Challenges
 *      description: Add new Player's score in the Challenge.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new score data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Points'
 *      responses:
 *        201:
 *          description: The Challenge the player has scored.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Points'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.post('/:id/score', (req, res, next) => {
  const data = getData(req.body, ['player_id'], true);

  Points.forge()
    .saveScore(Object.assign(data, { challenge_id: req.params.id }), { method: 'insert' })
    .then(item => res.formatter.created(item.toJSON()))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}/points:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Challenges
 *      description: >
 *        Lists all Player Points in the Challenge with the provided ID. The request
 *        must be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Points'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.get('/:id/points', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  return Points.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON() : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/point/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Point.
 *        example: 1
 *        schema:
 *          type: integer
 *    delete:
 *      tags:
 *        - Challenges
 *      description: >
 *        Removes the Player's points from the Challenge. The request must be
 *        authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.delete('/point/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  return Bookshelf.transaction(t => Points.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Point not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}/ranking/teams:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge to fetch the Teams Ranking.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Challenges
 *      description: >
 *        Returns the Teams Ranking for the Challenge with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/pageParameter'
 *        - $ref: '#/components/parameters/perPageParemeter'
 *        - $ref: '#/components/parameters/teamIdRankingFilter'
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/TeamsRanking'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.get('/:id/ranking/teams', (req, res, next) => {
  Challenge.forge()
    .teamRanking(req.params.id, req.parameters)
    .then(ranking => res.formatter.ok(ranking))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/challenges/{id}/ranking/players:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Challenge to fetch the Players Ranking.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Challenges
 *      description: >
 *        Returns the Players Ranking for the Challenge with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/pageParameter'
 *        - $ref: '#/components/parameters/perPageParemeter'
 *        - $ref: '#/components/parameters/playerIdRankingFilter'
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/PlayersRanking'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
challengesRouter.get('/:id/ranking/players', (req, res, next) => {
  Challenge.forge()
    .playerRanking(req.params.id, req.parameters)
    .then(ranking => res.formatter.ok(ranking))
    .catch(next);
});

export default challengesRouter;
