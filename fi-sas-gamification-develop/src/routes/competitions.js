/**
 * @swagger
 *
 * components:
 *   parameters:
 *    idsFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific IDs.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedCompetition:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Allowed values are: 'translations', 'challenges' & 'challenges.translations'.
 *      example: translations,challenges
 *   schemas:
 *     Competition:
 *       description: >
 *         A Competition is a game mode in which the Player receive points upon
 *         completing one or more (group of) Challenges.
 *       allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The Competition ID.
 *              example: 1
 *            translations:
 *              type: array
 *              description: The translated name for the Competition.
 *              items:
 *                $ref: '#/components/schemas/CompetitionTranslation'
 *            challenges:
 *              type: array
 *              description: The Challenges (IDs) belonging to the Competition.
 *              items:
 *                $ref: '#/components/schemas/CompetitionChallenge'
 *            updated_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of the last update on the Competition.
 *              example: 2019-01-01T09:00:00.000Z
 *            created_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of creation of the Competition.
 *              example: 2019-01-01T09:00:00.000Z
 *     CompetitionTranslation:
 *       type: object
 *       properties:
 *         language_id:
 *           type: integer
 *           description: The ID of the language for this CompetitionTranslation.
 *           example: 1
 *         name:
 *           type: string
 *           nullable: false
 *           description: The name of the Competition.
 *     CompetitionChallenge:
 *       type: object
 *       properties:
 *         challenge_id:
 *           type: integer
 *           description: The ID of the Challenge.
 *           example: 1
 *         competition_id:
 *           readOnly: true
 *           type: integer
 *           description: The ID of the Competition.
 *           example: 1
 */
import { Router } from 'express';
import Bookshelf from '../bookshelf';

import asyncMiddleware from '../middleware/async';
import * as Errors from '../errors';
import * as Codes from '../error_codes';
import { getPaginationMetaData } from '../helpers/metadata';
import { getLanguageId, getData } from '../helpers/common';


import Competition from '../models/competition';
import CompetitionTranslation from '../models/competitionTranslation';

const competitionsRouter = new Router();

/**
 * @swagger
 * /api/v1/competitions:
 *   get:
 *     tags:
 *       - Competitions
 *     description: >
 *       Returns a list of Competitions. The request must be authenticated.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/languageIdHeader'
 *       - $ref: '#/components/parameters/languageAcronHeader'
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idsFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/withRelatedCompetition'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Competition'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
competitionsRouter.get('/', asyncMiddleware(async (req, res, next) => {
  req.parameters.withRelated = req.parameters.withRelated || Competition.related.slice();

  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  req.parameters.validate(
    Competition.filterFields + CompetitionTranslation.filterFields,
    Competition.sortFields,
    Competition.related,
  );

  const languageId = await getLanguageId(req);

  Competition.forge()
    .loadTranslations(
      [{ name: 'competition', filters: CompetitionTranslation.filterFields }],
      req.parameters,
      languageId,
    )
    .then(tr => tr.filterOrderAndFetch(req.parameters))
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/competitions:
 *    parameters:
 *    post:
 *      tags:
 *        - Competitions
 *      description: >
 *        This endpoint is used to create a new Competition. The request must
 *        be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Competition data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Competition'
 *      responses:
 *        201:
 *          description: Created Competition object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Competition'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
competitionsRouter.post('/', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const translations = !(req.body.translations && req.body.translations instanceof Array)
    ? null
    : req.body.translations.map(m => getData(m, ['language_id', 'name'], false));

  const challenges = !(req.body.challenges && req.body.challenges instanceof Array)
    ? null
    : req.body.challenges.map(m => getData(m, ['challenge_id'], false));

  return Competition.forge()
    .saveWithRelated(null, translations, challenges, { method: 'insert' })
    .then(item => res.formatter.created(item.toJSON({ omitPivot: true })))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/competitions/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Competition.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Competitions
 *      description: >
 *        Returns the information of the Competition with the provided ID. The request
 *        must be authenticated.
 *      produces:
 *        - application/json
 *      parameters:
 *        - $ref: '#/components/parameters/languageIdHeader'
 *        - $ref: '#/components/parameters/languageAcronHeader'
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Competition'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
competitionsRouter.get('/:id', asyncMiddleware(async (req, res, next) => {
  const languageId = await getLanguageId(req);

  const translations = await Competition.forge()
    .where('id', req.params.id)
    .loadTranslations(['competition'], req.parameters, languageId);

  translations.fetch({
    require: true,
    withRelated: Competition.related.slice(),
  })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Competition not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/competitions/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Competition.
 *        example: 1
 *        schema:
 *          type: integer
 *    patch:
 *      tags:
 *        - Competitions
 *      description: >
 *        Update Competition's name (in translations object). Challenges are also
 *        accepted, note, when passing the challenges, all the challenges in the
 *        competition are replaced by the ones sent in the request body.
 *        The request must be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      requestBody:
 *        name: feed
 *        description: The Competition new name translations.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Competition'
 *      responses:
 *        200:
 *          description: The updated Competitions object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Competition'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
competitionsRouter.patch('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  const translations = !(req.body.translations && req.body.translations instanceof Array)
    ? null
    : req.body.translations.map(m => getData(m, ['language_id', 'name'], false));

  const challenges = !(req.body.challenges && req.body.challenges instanceof Array)
    ? null
    : req.body.challenges.map(m => getData(m, ['challenge_id'], false));

  return Competition.forge({ id: req.params.id })
    .saveWithRelated({ id: req.params.id }, translations, challenges, { method: 'update', patch: true })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/competitions/{id}:
 *    delete:
 *      tags:
 *        - Competitions
 *      description: >
 *        Delete Competition and all related data like Ranking and Points Scores.
 *        The request must be authenticated and only authorized to BackOffice users.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
competitionsRouter.delete('/:id', (req, res, next) => {
  if (!req.backoffice) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice use only.',
    });
  }

  return Bookshelf.transaction(t => Competition.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => { res.formatter.noContent(); })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Competition not found');
    })
    .catch(next);
});

/**
 * @swagger
 * /api/v1/competitions/{id}/ranking/team:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Competition.
 *        schema:
 *          type: integer
 *    get:
 *     tags:
 *       - Competitions
 *     description: >
 *       Returns the Teams ranking list for all Challenges in the Competition.
 *       The request must be authenticated.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/teamIdRankingFilter'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/PlayersRanking'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
competitionsRouter.get('/:id/ranking/team', (req, res, next) => {
  Competition.forge()
    .teamRanking(req.params.id, req.parameters)
    .then(ranking => res.formatter.ok(ranking))
    .catch(next);
});

/**
 * @swagger
 * /api/v1/competitions/{id}/ranking/player:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Competition.
 *        schema:
 *          type: integer
 *    get:
 *     tags:
 *       - Competitions
 *     description: >
 *       Returns the Players ranking list for all Challenges in the Competition.
 *       The request must be authenticated.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/playerIdRankingFilter'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/PlayersRanking'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
competitionsRouter.get('/:id/ranking/player', (req, res, next) => {
  Competition.forge()
    .playerRanking(req.params.id, req.parameters)
    .then(ranking => res.formatter.ok(ranking))
    .catch(next);
});

export default competitionsRouter;
