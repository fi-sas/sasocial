/**
 * @swagger
 *
 * components:
 *   parameters:
 *    idsFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific IDs.
 *    nicknameFilter:
 *      in: query
 *      name: nickname
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by Player nickname.
 *    userIDFilter:
 *      in: query
 *      name: user_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by User ID.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedPlayersTeam:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Allowed value: 'team'.
 *      example: team
 *    withRelatedTeamInvites:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Allowed value: 'teamInvites'.
 *      example: teamInvites
 *   schemas:
 *     Player:
 *       description: >
 *         A player is a direct map of an user, it has a nickname.
 *       allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The Team ID.
 *              example: 1
 *            nickname:
 *              type: string
 *              nullable: false
 *              description: The nickname of the Player, must be unique.
 *            user_id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The ID of the user associated with the Player.
 *              example: 1
 *            updated_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of the last update on the Team.
 *              example: 2019-01-01T09:00:00.000Z
 *            created_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of creation of the Team.
 *              example: 2019-01-01T09:00:00.000Z
 */
import { Router } from 'express';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import * as Codes from '../error_codes';
import { getPaginationMetaData } from '../helpers/metadata';
import { getData } from '../helpers/common';
import asyncMiddleware from '../middleware/async';

import Player from '../models/player';

const playersRouter = new Router();

async function validateUser(req) {
  if (!req.backoffice) {
    const player = await Player.forge().where('id', req.params.playerId).fetch();
    if (!player || player.get('user_id') !== req.authInfo.user.id) {
      return false;
    }
  }
  return true;
}

/**
 * @swagger
 * /api/v1/players:
 *   get:
 *     tags:
 *       - Players
 *     description: >
 *       Returns a list of Players. The request must be authenticated.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idsFilter'
 *       - $ref: '#/components/parameters/nicknameFilter'
 *       - $ref: '#/components/parameters/userIDFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/withRelatedPlayersTeam'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Player'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
playersRouter.get('/', (req, res, next) => {
  req.parameters.withRelated = req.parameters.withRelated || Player.defaultRelated.slice();

  req.parameters.parseFilters(
    {
      nickname: v => `%${v}%`,
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  req.parameters.validate(Player.filterFields, Player.sortFields, Player.related);

  Player.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/players:
 *    parameters:
 *    post:
 *      tags:
 *        - Players
 *      description: >
 *        This endpoint is used to create a new Player. The request must be authenticated.
 *        and the user is associated with the new Player.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Player data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Player'
 *      responses:
 *        201:
 *          description: Created Player object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Player'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
playersRouter.post('/', (req, res, next) => {
  const data = getData(req.body, ['nickname'], true);

  // user_id
  data.user_id = req.authInfo.user.id;

  Player.forge(data)
    .save()
    .then((item) => {
      res.formatter.created(item.toJSON({ omitPivot: true }));
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/players/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Player to fetch.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Players
 *      description: >
 *        Returns the information of the Player with the provided ID. The request
 *        must be authenticated.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedTeamInvites'
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Player'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
playersRouter.get('/:id', (req, res, next) => {
  req.parameters.validate(null, null, Player.related);

  Player.forge({ id: req.params.id })
    .fetch({ require: true, withRelated: req.parameters.withRelated || Player.related })
    .then((item) => {
      res.formatter.ok(item.toJSON({ omitPivot: true }));
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Player not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/players/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Player to update the nickname.
 *        example: 1
 *        schema:
 *          type: integer
 *    patch:
 *      tags:
 *        - Players
 *      description: >
 *        Update Player's nickname. The request must be authenticated and only
 *        authorized to BackOffice users or the Player.
 *      produces:
 *        - application/json
 *      requestBody:
 *        name: feed
 *        description: The Player new nickname, must be unique.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Player'
 *      responses:
 *        200:
 *          description: The updated Player's object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Player'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
playersRouter.patch('/:id', asyncMiddleware(async (req, res, next) => {
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice or the Player can perform this action.',
    });
  }

  const data = getData(req.body || {}, ['nickname'], true);

  Player.forge({ id: req.params.id })
    .fetch({ require: true })
    .then(item => item.save(data, { method: 'update', patch: true }))
    .tap(item => item.refresh({ withRelated: Player.defaultRelated }))
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Player not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/players/{id}:
 *    delete:
 *      tags:
 *        - Players
 *      description: >
 *        Delete Player and all related data like Ranking and Points Scores.
 *        The request must be authenticated and only authorized to BackOffice users
 *        or the Player.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
playersRouter.delete('/:id', asyncMiddleware(async (req, res, next) => {
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice or the Player can perform this action.',
    });
  }

  return Bookshelf.transaction(t => Player.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => { res.formatter.noContent(); })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Player not found');
    })
    .catch(next);
}));

export default playersRouter;
