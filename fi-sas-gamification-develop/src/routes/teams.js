/**
 * @swagger
 *
 * components:
 *   parameters:
 *    idFilter:
 *      in: query
 *      name: id
 *      required: false
 *      schema:
 *        type: array
 *        items:
 *          type: string
 *      description: Filter by specific IDs.
 *    teamNameFilter:
 *      in: query
 *      name: name
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by Team name.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedTeam:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Possible values include 'players'.
 *      example: players
 *    withRelatedTeamId:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *        type: string
 *      description: >
 *          Request related entities to be included in the data objects (comma-separated list).
 *          Possible values include 'players', 'invites', 'requests' and 'teamPlayers'.
 *      example: players
 *   schemas:
 *     Team:
 *       description: >
 *         A Team is a group Players, it can have a max of 20 players and one player
 *         is the leader (the user that created the team). To join the team a player
 *         can request access to the team or can be invited by the team leader, in
 *         this case, the team access is immediately granted. The team leader has
 *         the power to remove players from the team and a player can also leave the
 *         team at any time.
 *       allOf:
 *        - type: object
 *          properties:
 *            id:
 *              type: integer
 *              readOnly: true
 *              nullable: false
 *              description: The Team ID.
 *              example: 1
 *            name:
 *              type: string
 *              nullable: false
 *              description: The name of the Team, must be unique.
 *            updated_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of the last update on the Team.
 *              example: 2019-01-01T09:00:00.000Z
 *            created_at:
 *              type: string
 *              readOnly: true
 *              format: date
 *              nullable: false
 *              description: The date of creation of the Team.
 *              example: 2019-01-01T09:00:00.000Z
 */
import { Router } from 'express';
import Bookshelf from '../bookshelf';

import * as Codes from '../error_codes';
import * as Errors from '../errors';
import { getData } from '../helpers/common';
import asyncMiddleware from '../middleware/async';
import { getPaginationMetaData } from '../helpers/metadata';

import Team from '../models/team';
import Player from '../models/player';
import TeamPlayer from '../models/teamPlayer';

const teamsRouter = new Router();

async function validateUser(req, ignoreLeader = false) {
  if (!req.backoffice) {
    const player = await Player.findByUser(req.authInfo.user.id);
    if (!player) {
      return false;
    }

    const teamPlayer = await TeamPlayer.forge()
      .query((qb) => {
        qb.where('player_id', player.get('id'))
          .andWhere('team_id', req.params.id);
      })
      .fetch();
    if (!teamPlayer) {
      return false;
    }

    if (!ignoreLeader && !teamPlayer.get('leader')) {
      return false;
    }
  }

  return true;
}

/**
 * @swagger
 * /api/v1/teams:
 *   get:
 *     tags:
 *       - Teams
 *     description: >
 *       Returns the list of Teams. The request must be authenticated.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/idFilter'
 *       - $ref: '#/components/parameters/teamNameFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/withRelatedTeam'
 *     responses:
 *       200:
 *         description: Successful operation.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: string
 *                   enum: [success]
 *                 link:
 *                   $ref: '#/components/schemas/Link'
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Error'
 *                   maxItems: 0
 *                   example: []
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Team'
 *       400:
 *         $ref: '#/components/responses/BadRequestSimpleValidation'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
teamsRouter.get('/', (req, res, next) => {
  req.parameters.withRelated = req.parameters.withRelated || Team.defaultRelated.slice();

  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  req.parameters.validate(Team.filterFields, Team.sortFields, Team.defaultRelated);

  Team.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/teams:
 *    parameters:
 *    post:
 *      tags:
 *        - Teams
 *      description: >
 *        This endpoint is used for creating Teams. The request must be authenticated
 *        and the user who creates the team (needs to be a Player) is the leader.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The new Team data.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Team'
 *      responses:
 *       '201':
 *        description: Created Team object.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Team'
 *       400:
 *         $ref: '#/components/responses/BadRequest'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         $ref: '#/components/responses/Forbidden'
 *       500:
 *         $ref: '#/components/responses/ServerError'
 */
teamsRouter.post('/', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, ['name'], false);
  data.user_id = req.authInfo.user.id;

  Team.forge()
    .saveWithLeader(data, 'user_id', { method: 'insert' })
    .then(item => item.refresh({ withRelated: Team.defaultRelated }))
    .then(item => res.formatter.created(item.toJSON()))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/teams/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        schema:
 *          type: integer
 *    get:
 *      tags:
 *        - Teams
 *      description: Returns the information for the Team with the provided ID.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedTeamId'
 *      responses:
 *        200:
 *          description: Successful operation.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.get('/:id', (req, res, next) => {
  req.parameters.validate(null, null, Team.related);

  Team.forge({ id: req.params.id })
    .fetch({ require: true, withRelated: req.parameters.withRelated || Team.related })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Team not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/teams/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    patch:
 *      tags:
 *        - Teams
 *      description: Update Team name. The request must be authenticated.
 *      produces:
 *        - application/json
 *      requestBody:
 *        name: feed
 *        description: The Team new name, must be unique.
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Team'
 *      responses:
 *        200:
 *          description: The updated Team object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.patch('/:id', asyncMiddleware(async (req, res, next) => {
  // validate user doing this action, ok for backoffice and team leader
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice and Team Leader only.',
    });
  }

  // only the Team _name_ can be updated
  const data = getData(req.body || {}, ['name'], true);

  Team.forge({ id: req.params.id })
    .fetch({ require: true })
    .then(item => item.save(data, { method: 'update', patch: true }))
    .tap(item => item.refresh({ withRelated: Team.defaultRelated }))
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Team not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/teams/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    delete:
 *      tags:
 *        - Teams
 *      description: >
 *        Delete Team and all related data like Ranking and Points Scores.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.delete('/:id', asyncMiddleware(async (req, res, next) => {
  // validate user doing this action, ok for backoffice and team leader
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice and Team Leader only.',
    });
  }

  return Bookshelf.transaction(t => Team.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => { res.formatter.noContent(); })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Team not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/teams/{id}/join:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Teams
 *      description: >
 *        A player can request to join a determined team. The authenticated user
 *        must be a Player to join (needs a nickname) and this request must be approved
 *        by the team leader. Only after that the player will be listed in the team
 *        players list.
 *      produces:
 *        - application/json
 *      responses:
 *        201:
 *          description: Players Team request sent.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.post('/:id/join', asyncMiddleware(async (req, res, next) => {
  const team = await Team.forge({ id: req.params.id })
    .fetch({ withRelated: Team.defaultRelated });
  if (!team) {
    throw new Errors.NotFoundError('Team not found');
  }

  const player = await Player.forge({ user_id: req.authInfo.user.id }).fetch();
  if (!player) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find Player',
      field: 'user_id',
      index: 0,
    }]);
  }

  const data = {
    player_id: player.get('id'),
    team_id: team.get('id'),
    leader: false,
    status: 'Request',
  };

  await TeamPlayer.forge(data).save().catch(next);

  return res.formatter.created(team.toJSON());
}));

/**
 * @swagger
 *  /api/v1/teams/{id}/invite:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Teams
 *      description: >
 *        This endpoint can only be used by the team leader to invite other players
 *        to join their team. As soon as the player accepts the invite, the player
 *        will be listed in the team players list.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The nickname of the player to invite.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                nickname:
 *                  type: string
 *                  description: A unique string that identifies the player.
 *      responses:
 *        201:
 *          description: Players Team invite sent.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.post('/:id/invite', asyncMiddleware(async (req, res, next) => {
  // validate user doing this action, ok for backoffice and team leader
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice and Team Leader only.',
    });
  }

  const team = await Team.forge({ id: req.params.id })
    .fetch({ withRelated: Team.defaultRelated });
  if (!team) {
    throw new Errors.NotFoundError('Team not found');
  }

  const playerData = getData(req.body, ['nickname'], true);
  const player = await Player.forge(playerData).fetch();
  if (!player) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find Player',
      field: 'nickname',
      index: 0,
    }]);
  }

  const data = {
    player_id: player.get('id'),
    team_id: req.params.id,
    leader: false,
    status: 'Invite',
  };

  await TeamPlayer.forge(data).save().catch(next);

  return res.formatter.created(team.toJSON());
}));

/**
 * @swagger
 *  /api/v1/teams/{id}/accept-invite:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Teams
 *      description: >
 *        This endpoint can only be used by the Player who was invited by the team
 *        leader to the Team. As soon as the player accepts the invite, he will be
 *        listed in the team players list.
 *      produces:
 *        - 'application/json'
 *      responses:
 *        201:
 *          description: Successful operation. Player accepts the team leader invite to the team.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.post('/:id/accept-invite', asyncMiddleware(async (req, res) => {
  // validate user doing this action, ok for Player with the user id in req
  const team = await Team.forge({ id: req.params.id })
    .fetch({ withRelated: Team.defaultRelated });
  if (!team) {
    throw new Errors.NotFoundError('Team not found');
  }

  const player = await Player.forge({ user_id: req.authInfo.user.id }).fetch();
  if (!player) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find Player',
      field: 'nickname',
      index: 0,
    }]);
  }

  const teamPlayer = await TeamPlayer.forge().query((qb) => {
    qb.where('player_id', player.get('id'))
      .andWhere('team_id', req.params.id)
      .andWhere('status', 'Invite');
  }).fetch();
  if (!teamPlayer) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find invited Player to Team',
    }]);
  }

  try {
    await teamPlayer.save({ status: 'Member' }, { method: 'update', patch: true });
  } catch (err) {
    return res.formatter.serverError({ code: err.code, message: err.message });
  }

  await team.refresh({ withRelated: Team.defaultRelated });

  return res.formatter.created(team.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/teams/{id}/accept-request:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Teams
 *      description: >
 *        This endpoint can only be used by the team leader to accept the player
 *        request to join their team. As soon as the team leader accepts the invite,
 *        the player will be listed in the team players list.
 *      produces:
 *        - 'application/json'
 *      requestBody:
 *        description: The Id of the Player to accept request to join Team.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                player_id:
 *                  type: integer
 *                  description: The ID of the Player to join the team.
 *                  example: 1
 *      responses:
 *        200:
 *          description: Successful operation. Accept the Players request to join the team.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  status:
 *                    type: string
 *                    enum: [success]
 *                  link:
 *                    $ref: '#/components/schemas/Link'
 *                  errors:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Error'
 *                    maxItems: 0
 *                    example: []
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Team'
 *        400:
 *          $ref: '#/components/responses/BadRequest'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.post('/:id/accept-request', asyncMiddleware(async (req, res, next) => {
  const valid = await validateUser(req);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice and Team Leader only.',
    });
  }

  const team = await Team.forge({ id: req.params.id }).fetch();
  if (!team) {
    throw new Errors.NotFoundError('Team not found');
  }

  const player = await Player.forge().where('user_id', req.authInfo.user.id).fetch();
  if (!player) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find Player',
    }]);
  }

  // to accept the request the logged user must be this team (:id) leader
  const leader = await TeamPlayer.forge().query((qb) => {
    qb.where('player_id', player.get('id'))
      .andWhere('team_id', req.params.id)
      .andWhere('leader', 1);
  }).fetch();
  if (!leader) {
    throw new Errors.ValidationError([{
      code: Codes.InvalidPlayer,
      message: 'Could not find Team Leader',
    }]);
  }

  const requestMembership = await TeamPlayer.forge().query((qb) => {
    qb.where('player_id', req.body.player_id)
      .andWhere('status', 'Request')
      .andWhere('team_id', req.params.id);
  }).fetch();
  if (!requestMembership) {
    throw new Errors.NotFoundError('Request not found');
  }

  try {
    await requestMembership.save({ status: 'Member' }, { method: 'update', patch: true })
  } catch (err) {
    return res.formatter.serverError({ code: err.code, message: err.message });
  }

  await team.refresh({ withRelated: Team.defaultRelated });

  return res.formatter.created(team.toJSON({ omitPivot: true }));
}));

/**
 * @swagger
 *  /api/v1/teams/{id}/player/{playerId}:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *      - in: path
 *        name: playerId
 *        required: true
 *        description: Numeric ID of the Player to remove from the Team.
 *        example: 1
 *        schema:
 *          type: integer
 *    delete:
 *      tags:
 *        - Teams
 *      description: >
 *        Removes the Player from the Team. This can be done by the player itself
 *        or by the team leader.
 *      produces:
 *        - application/json
 *      responses:
 *        204:
 *          description: Successful operation.
 *        400:
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        401:
 *          $ref: '#/components/responses/Unauthorized'
 *        403:
 *          $ref: '#/components/responses/Forbidden'
 *        404:
 *          $ref: '#/components/responses/NotFound'
 *        500:
 *          $ref: '#/components/responses/ServerError'
 */
teamsRouter.delete('/:id/player/:playerId', asyncMiddleware(async (req, res, next) => {
  // validate user doing this action, ok for backoffice, team leader and player himself
  const valid = await validateUser(req, true);
  if (!valid) {
    return res.formatter.unauthorized({
      code: Codes.AuthenticationLackOfPermissions,
      message: 'Unauthorized. Backoffice and Team Leader only.',
    });
  }

  return Bookshelf.transaction(t => TeamPlayer.forge()
    .query((qb) => {
      qb.where('team_id', req.params.id)
        .andWhere('player_id', req.params.playerId);
    })
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t })))
    .then(() => { res.formatter.noContent(); })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Team Player not found');
    })
    .catch(next);
}));

export default teamsRouter;
