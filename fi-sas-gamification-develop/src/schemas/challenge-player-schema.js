import Joi from 'joi';

module.exports = Joi.object().keys({
  challenge_id: Joi.number().integer(),
  player_id: Joi.number().integer(),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
