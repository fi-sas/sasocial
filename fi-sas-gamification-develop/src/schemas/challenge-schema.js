import Joi from 'joi';

module.exports = Joi.object().keys({
  challenge_type_id: Joi.number().integer(),
  points: Joi.number().integer(),
  automated: Joi.boolean(),
  tag: Joi.string().min(3).max(120),
  service_id: Joi.number().integer().allow(null).optional(),
  start_date: Joi.date().iso().optional(),
  end_date: Joi.date().iso().optional(),
  winners: Joi.number().integer().allow(null).optional(),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
