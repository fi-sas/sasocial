import Joi from 'joi';

module.exports = Joi.object().keys({
  challenge_id: Joi.number().integer(),
  language_id: Joi.number().integer(),
  name: Joi.string().min(1).max(120),
  description: Joi.string().allow(null).optional(),
  rules: Joi.string(),
  team_prize: Joi.string().max(250).allow(null).optional(),
  player_prize: Joi.string().max(250).allow(null).optional(),
});
