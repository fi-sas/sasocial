import Joi from 'joi';

module.exports = Joi.object().keys({
  tag: Joi.string().min(1).max(120),
  description: Joi.string().min(1).max(250),
});
