import Joi from 'joi';

module.exports = Joi.object().keys({
  competition_id: Joi.number().integer(),
  challenge_id: Joi.number().integer(),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
