import Joi from 'joi';

module.exports = Joi.object().keys({
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
