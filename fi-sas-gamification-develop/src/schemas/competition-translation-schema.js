import Joi from 'joi';

module.exports = Joi.object().keys({
  competition_id: Joi.number().integer(),
  language_id: Joi.number().integer(),
  name: Joi.string().min(1).max(120),
});
