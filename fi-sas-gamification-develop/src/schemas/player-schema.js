import Joi from 'joi';

module.exports = Joi.object().keys({
  user_id: Joi.number().integer(),
  nickname: Joi.string().min(1).max(60),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
