import Joi from 'joi';
import TeamPlayerStatuses from '../models/values/team_player_statuses';

module.exports = Joi.object().keys({
  team_id: Joi.number().integer(),
  player_id: Joi.number().integer(),
  leader: Joi.bool(),
  status: Joi.valid(TeamPlayerStatuses),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
