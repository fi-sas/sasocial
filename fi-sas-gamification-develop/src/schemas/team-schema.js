import Joi from 'joi';

module.exports = Joi.object().keys({
  name: Joi.string().min(1).max(120),
  updated_at: Joi.date().iso(),
  created_at: Joi.date().iso().optional(),
});
