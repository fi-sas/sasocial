# 1.0.0 (2021-05-17)


### Bug Fixes

* add path to metrics on cadadvisor ([40dc1a6](https://gitlab.com/fi-sas/fi-sas-logs/commit/40dc1a6ee0eb744dd0a1dea840c1f123868944d2))
* **docker-compose:** add always restart in all containers ([2228e52](https://gitlab.com/fi-sas/fi-sas-logs/commit/2228e521de66758ef608061a677ba7afec8fb332))


### Features

* update gateway service name ([6228828](https://gitlab.com/fi-sas/fi-sas-logs/commit/6228828bde4c0ebc856b5f7c287dd6d1adf707f6))
* update the prometheus scrapping ([b9f0d05](https://gitlab.com/fi-sas/fi-sas-logs/commit/b9f0d059aaf83c625f7c417775a6a74a2b1a2aa5))
