module.exports.up = async db => db.schema.createTable('modality', (table) => {
  table.increments();
  table.string('name', 255).notNullable();
  table.integer('active').notNullable();
  table.integer('file_id').notNullable().unsigned();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('modality');

module.exports.configuration = { transaction: true };
