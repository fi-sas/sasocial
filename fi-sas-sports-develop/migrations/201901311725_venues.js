module.exports.up = async db => db.schema.createTable('venue', (table) => {
  table.increments();
  table.string('name', 255).notNullable();
  table.integer('active').notNullable();
  table.float('longitude', 10, 6).notNullable();
  table.float('latitude', 10, 6).notNullable();
  table.string('address', 250).notNullable();
  table.string('address_no', 25).notNullable();
  table.string('city', 250).notNullable();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('venue');

module.exports.configuration = { transaction: true };
