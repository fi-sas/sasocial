module.exports.up = async db => db.schema.createTable('reservation', (table) => {
  table.increments();
  table.integer('venue_id').notNullable().unsigned().references('id')
    .inTable('venue');
  table.integer('user_id').unsigned();
  table.integer('user_profile_id').notNullable().unsigned();
  table.string('user_profile_name', 255).notNullable();
  table.string('name', 255).notNullable();
  table.string('student_number', 255).notNullable();
  table.string('identification_number', 255).notNullable();
  table.string('tin', 9).notNullable();
  table.string('address', 255).notNullable();
  table.string('postal_code', 15).notNullable();
  table.string('city', 255).notNullable();
  table.string('email', 255).notNullable();
  table.string('phone_number', 255).notNullable();
  table.datetime('start_date').notNullable();
  table.datetime('end_date').notNullable();
  table.enu('recurrence', ['None', 'Daily', 'Weekly']).defaultTo('None');
  table.string('usage_description', 255);
  table.integer('file_id').unsigned();
  table.enu('status', ['Pending', 'Approved', 'Rejected', 'Cancelled']);
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('reservation');

module.exports.configuration = { transaction: true };
