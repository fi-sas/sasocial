module.exports.up = async db => db.schema.createTable('activity', (table) => {
  table.increments();
  table.enu('type', ['GENERAL ACTIVITY', 'REGULAR TRAINING', 'COMPETITION']).notNullable();
  table.integer('modality_id').notNullable().unsigned().references('id')
    .inTable('modality');
  table.integer('venue_id').notNullable().unsigned().references('id')
    .inTable('venue');
  table.integer('event_id').nullable().unsigned();
  table.string('name', 255).notNullable();
  table.string('description', 500).notNullable();
  table.datetime('start_date').nullable();
  table.datetime('end_date').nullable();
  table.enu('training_day', [
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY',
    'SUNDAY',
  ]).nullable();
  table.string('start_time', 255).nullable();
  table.string('end_time', 255).nullable();
  table.integer('active').notNullable();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
});

module.exports.down = async db => db.schema.dropTable('activity');

module.exports.configuration = { transaction: true };
