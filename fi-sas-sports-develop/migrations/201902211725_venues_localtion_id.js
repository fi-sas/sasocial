module.exports.up = async db => db.schema.table('venue', (table) => {
  table.integer('location_id').nullable().after('city');
});

module.exports.down = async db => db.schema.table('venue', (table) => {
  table.dropColumn('location_id');
});

module.exports.configuration = { transaction: true };
