module.exports.up = async db => db.schema.createTable('reservation_history', (table) => {
  table.increments();
  table.integer('reservation_id').notNullable().unsigned().references('id')
    .inTable('reservation');
  table.string('recurrence');
  table.datetime('start_date');
  table.datetime('end_date');
  table.string('status');
  table.integer('user_id').unsigned();
  table.string('action');
  table.string('notes', 500);
  table.datetime('created_at');
  table.datetime('updated_at');
});

module.exports.down = async db => db.schema.dropTable('reservation_history');

module.exports.configuration = { transaction: true };
