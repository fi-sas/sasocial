import express from 'express';
import path from 'path';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import log4js from 'log4js';

import requireAuth from './middleware/auth';
import setValidators from './custom_validations';
import apiComposerValidations from './middleware/apiComposerValidations';
import responseEnhancer from './middleware/response-formatter';
import requestParameters from './middleware/request-parameters';
import { errorHandler, logErrors } from './middleware/error-handlers';
import tokenInfo from './middleware/tokenInfo';
import internalRequest from './middleware/internal-request';
import logger from './logger';
import { NotFoundError } from './errors';

// MS Routes
import activitiesRouter from './routes/activities';
import modalitiesRouter from './routes/modalities';
import reservationsRouter from './routes/reservations';
import venuesRouter from './routes/venues';
import configurationsRouter from './routes/configurations';

// Swagger Specs
import linkSpecs from './config/components/schemas.link.spec.json';
import parametersSpecs from './config/components/parameters.spec.json';
import responsesSpecs from './config/components/responses.spec.json';
import securitySchemesSpecs from './config/components/securitySchemes.bearerAuth.json';
import securitySpecs from './config/security.json';


//    __________  ____  _   __       ______  ____ _____
//   / ____/ __ \/ __ \/ | / /      / / __ \/ __ ) ___/
//  / /   / /_/ / / / /  |/ /  __  / / / / / __  \__ \
// / /___/ _, _/ /_/ / /|  /  / /_/ / /_/ / /_/ /__/ /
// \____/_/ |_|\____/_/ |_/   \____/\____/_____/____/

// Uncomment to use cron jobs

// import cron from 'node-cron';

// cron.schedule('* * * * *', () => {
//   console.log('running a task every minute');
// });


//     __  ________    ____  ____  ____  _____________________  ___    ____
//    /  |/  / ___/   / __ )/ __ \/ __ \/_  __/ ___/_  __/ __ \/   |  / __ \
//   / /|_/ /\__ \   / __  / / / / / / / / /  \__ \ / / / /_/ / /| | / /_/ /
//  / /  / /___/ /  / /_/ / /_/ / /_/ / / /  ___/ // / / _, _/ ___ |/ ____/
// /_/  /_//____/  /_____/\____/\____/ /_/  /____//_/ /_/ |_/_/  |_/_/
setValidators();

/**
 * Routes
 */
const app = express();

// Use logged middleware to log all requests
app.use(log4js.connectLogger(logger, { level: 'auto' }));

// Set default render engine
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

// Add CORS headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Language-ID, X-Language-Acronym');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD');
  next();
});

// Helmet helps you secure your Express apps by setting various HTTP headers.
// It's not a silver bullet, but it can help!
// https://github.com/helmetjs/helmet
app.use(helmet());

/**
 * Use response enhancer (formatter)
 * Use request parameters to access ordering, filters and pagination configurations
 */
app.use(responseEnhancer());
app.use(requestParameters());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: '5mb' }));
app.use(express.static(path.join(__dirname, '..', 'public')));

/**
 * Use internal request detection
 */
app.use(internalRequest);

// API composer validations -- used for validating resources from other MS
app.use(apiComposerValidations);
app.use(tokenInfo);

/*
 * Assign Routes to Application
 */
app.use('/api/v1/activities', requireAuth, activitiesRouter);
app.use('/api/v1/modalities', requireAuth, modalitiesRouter);
app.use('/api/v1/reservations', requireAuth, reservationsRouter);
app.use('/api/v1/venues', requireAuth, venuesRouter);
app.use('/api/v1/configurations', requireAuth, configurationsRouter);

const form = `{
  "name": "Registo em Actividades Desportivos",
  "elements": [
    {
      "type": "Text",
      "required": true,
      "name": "name",
      "caption": "Nome"
    },
    {
      "type": "Text",
      "required": true,
      "name": "student_number",
      "caption": "Nº Aluno"
    },
    {
      "type": "Text",
      "required": true,
      "name": "course",
      "caption": "Curso"
    },
    {
      "type": "Text",
      "required": true,
      "name": "school",
      "caption": "Escola"
    },
    {
      "type": "Numeric",
      "required": true,
      "name": "course_year",
      "caption": "Ano de Frequência"
    },
    {
      "type": "Text",
      "required": true,
      "name": "identification_number",
      "caption": "Cartão Cidadão"
    },
    {
      "type": "Text",
      "required": true,
      "name": "tin",
      "caption": "NIF"
    },
    {
      "type": "Text",
      "required": true,
      "name": "birth_date",
      "caption": "Data Nascimento"
    },
    {
      "type": "Text",
      "required": true,
      "name": "nationality",
      "caption": "Nacionalidade"
    },
    {
      "type": "Text",
      "required": true,
      "name": "address",
      "caption": "Morada"
    },
    {
      "type": "Text",
      "required": true,
      "name": "postal_code",
      "caption": "Código Postal"
    },
    {
      "type": "Text",
      "required": true,
      "name": "city",
      "caption": "Localidade"
    },
    {
      "type": "Text",
      "required": true,
      "name": "email",
      "caption": "E-mail"
    },
    {
      "type": "Text",
      "required": true,
      "name": "phone_number",
      "caption": "N.º de telemóvel/telefone"
    },
    {
      "type": "Checkbox",
      "required": true,
      "name": "federate",
      "caption": "Federado"
    },
    {
      "type": "Text",
      "required": true,
      "name": "club_name",
      "caption": "Clube"
    },
    {
      "type": "Text",
      "required": true,
      "name": "sports_license_number",
      "caption": "Nº Licença Desportiva"
    },
    {
      "type": "Numeric",
      "required": true,
      "name": "file_id",
      "caption": "Anexo"
    }
  ]
}`;
/*
 * Swagger documentation route
 */
const swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'FI@SAS MS Sports',
      version: '0.0.0',
      description: `Microservice API for the Sports activites of the FI@SAS project.<br>
        Allows the creation and management of Venues, Modalities and Activities of different types:
        Regular Trainings, Competitions and General Activities.<br>
        The Calendar Microservice is used to create Events associated with Activities.
        The Calendar is used during the creation of a new Activity and after an update of its
        start or end date.<br> Further configuration of associated Events and edition of other details (such
        as locale related info) may be done in the Calendar Microservice.<br>
        This microservice is enhanced with a set of Configurations endpoints to manage the configurations in use.
        <br><br>
        <b>Note:</b> The configuration for "activity_registration_form_id" assumes the existence of a relevant Registration
        Form in Calendar Microservice. As basic example of such form, the following data can be used (POST data as
        new Registration Form and use its ID in the configuration mentioned).
        <details>
          <summary><small>Registration Form for Activities (example)</small></summary>
          <p><small>${`\n\`\`\`json\n${form}\n\`\`\`\n`}</small></p>
        </details>`,
    },
    components: {
      schemas: {
        Link: linkSpecs,
      },
      parameters: parametersSpecs,
      responses: responsesSpecs,
      securitySchemes: securitySchemesSpecs,
    },
    security: securitySpecs,
    tags: [
      {
        name: 'Modalities',
        description: 'Manage the sports Modalities avaliable in this MS.',
      },
      {
        name: 'Venues',
        description: 'Manage Venues where Activities can be scheduled.',
      },
      {
        name: 'Activities',
        description: 'Manage Activities on which users can create Registrations.',
      },
      {
        name: 'Reservations',
        description: 'Manage Reservations (one-time or recurring) made on the Venues available.',
      },
      {
        name: 'Configurations',
        description: 'General configurations for the sports.',
      },
    ],
  },
  apis: ['./routes/*'],
});

delete swaggerSpec.swagger;
swaggerSpec.openapi = '3.0.n';

swaggerSpec.components.schemas = Object.assign(swaggerSpec.components.schemas, require('./config/components/schemas.errors.spec.json'));

swaggerSpec.components.responses = require('./config/components/responses.spec.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// catch 404 and forward to error handler
app.use(() => {
  throw new NotFoundError();
});

app.use(logErrors);
app.use(errorHandler);

export default app;
