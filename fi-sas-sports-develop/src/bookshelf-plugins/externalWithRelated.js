/**
 * externalWithRelated bookshelf plugin to remove external withRelate before fetch
 *
 * Extends bookshelf Model with attachExternalWithRelated method that attaches handlers to
 * fetching events (model and collection).
 * These handlers will use getExternalWithRelated() function and Model.externalWithRelated
 * static property to find the model's
 * external relationships. Before fetching model instances, the withRelated option will be
 * filtered to remove the external occurrences.
 *
 * ------ INSTRUCTIONS ------
 *
 * 1 - Required implementation on model:
 * getExternalWithRelated() function, externalWithRelated property and this.excludeExternal()
 * call on initialize.
 *
 * Example:
 * const MyModel = Bookshelf.Model.extend({
 *  tableName: 'mytable',
 *  initialize(...args) {
 *    this.constructor.__super__.initialize.apply(this, args);
 *    this.attachExternalWithRelated();
 *  },
 *  getExternalWithRelated() {
 *    return MyModel.externalWithRelated;
 *  },
 * }, {
 *  externalWithRelated: {
 *    file: {
 *      targetMS: 'MS_MEDIA_ENDPOINT',
 *      targetResource: 'files',
 *      path: 'file_id',
 *      destination: 'file',
 *    },
 *  },
 * });
 *
 * 2 - Required implementation on route:
 * clone original withRelated parameters and use them later on loadRelated call.
 *
 * Example 1: GET /id/
 *  // Set default withRelated parameter
 *  req.parameters.withRelated = req.parameters.withRelated || MyModel.defaultWithRelated.slice();
 *
 *  // Validate withRelated parameters
 *  req.parameters.validate(null, null, MyModel.withRelated);
 *
 *  // Store original withRelated
 *  const originalWithRelated = _.cloneDeep(req.parameters.withRelated);
 *
 *  MyModel.forge().where('id', req.params.id)
 *    .fetch({ require: true, withRelated: req.parameters.withRelated })
 *    .then(i => loadRelated(req, i.toJSON(), originalWithRelated, MyModel.externalWithRelated)
 *      .then(newItem => res.formatter.ok(newItem)))
 *    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('MyModel not found.'); })
 *    .catch(next);
 *
 * Example 2: GET /
 *   // Set default withRelated parameter
 *  req.parameters.withRelated = req.parameters.withRelated || MyModel.defaultWithRelated.slice();
 *
 *  // Validate filters, sorting and withRelated parameters
 *  req.parameters.validate(MyModel.filterFields, MyModel.sortFields, MyModel.withRelated);
 *
 *  // Store original withRelated
 *  const originalWithRelated = _.cloneDeep(req.parameters.withRelated);
 *
 *  MyModel.forge().filterOrderAndFetch(req.parameters)
 *    .then((collect) => {
 *      const data = collect ? collect.toJSON({ omitPivot: true }) : [];
 *      const link = (collect.pagination) ? getPaginationMetaData(collect.pagination, req): {};
 *      loadRelated(req, data, originalWithRelated, MyModel.externalWithRelated)
 *        .then(newData => res.formatter.ok(newData, link));
 *    })
 *    .catch(next);
 */
/* eslint-disable no-param-reassign, func-names */
import { cloneDeep } from 'lodash';

module.exports = function (bookshelf) {
  /**
   * Helper function to return a new withRelated parameter value.
   * Removes the received object of external relationship from the received withRelated parameter
   * and returns new value.
   *
   * @param {*} external
   * @param {*} withRelated
   */
  const excludeExternal = (external, withRelated) => {
    let newWithRelated = cloneDeep(withRelated);

    if (withRelated instanceof String) {
      if (external.includes(withRelated)) {
        newWithRelated = false;
      }
    }

    if (withRelated instanceof Array) {
      newWithRelated = withRelated.map((r) => {
        if (typeof r === 'string') {
          if (!external.includes(r)) {
            return r;
          }
          return false;
        } if (r instanceof Object) {
          const clean = r;
          external.forEach((e) => { if (clean[e]) { delete clean[e]; } });
          return clean;
        }
      }).filter(f => f !== false);
    }

    if (withRelated instanceof Object) {
      external.forEach((e) => { if (newWithRelated[e]) { delete newWithRelated[e]; } });
    }
    return newWithRelated;
  };

  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Attaches handlers to fetching and fetching:collection events.
     */
    attachExternalWithRelated() {
      this.on('fetching', this.excludeExternalWithRelated);
      this.on('fetching:collection', this.excludeExternalWithRelatedOnCollection);
    },

    /**
     * Removes from options.withRelated the external relationships of the received model.
     * Uses getExternalWithRelated method on model.
     *
     * @param {*} model
     * @param {*} columns
     * @param {*} options
     */
    excludeExternalWithRelated(model, columns, options) {
      if (model.getExternalWithRelated && typeof model.getExternalWithRelated === 'function'
        && model.getExternalWithRelated() instanceof Object && options.withRelated
      ) {
        const external = Object.keys(model.getExternalWithRelated());
        options.withRelated = excludeExternal(external, options.withRelated);
      }
    },

    /**
     * Removes from options.withRelated the external relationships of the received collection
     * model.
     * Uses externalWithRelated static property on model.
     *
     * @param {*} collection
     * @param {*} columns
     * @param {*} options
     */
    excludeExternalWithRelatedOnCollection(collection, columns, options) {
      if (collection.model && collection.model.externalWithRelated
        && typeof collection.model.externalWithRelated !== 'undefined'
        && collection.model.externalWithRelated instanceof Object && options.withRelated
      ) {
        const external = Object.keys(collection.model.externalWithRelated);
        options.withRelated = excludeExternal(external, options.withRelated);
      }
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
