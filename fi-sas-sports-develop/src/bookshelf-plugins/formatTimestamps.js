/**
 * formatTimestamps bookshelf plugin
 *
 * Extends bookshelf Model with formatModelTimestamps method.
 */
/* eslint-disable no-param-reassign, func-names */


module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Formats received attributes, turning dates in string format into date values.
     * By default, formats created_at and updated_at attributes.
     *
     * @param {Object} attributes
     * @returns {Object} Attributes with formatted active field
     */
    formatModelTimestamps(attributes, date_attributes = []) {
      if (date_attributes.length === 0) {
        date_attributes = this.getTimestampKeys();
      }
      date_attributes.forEach((attr) => {
        if (attributes[attr]) {
          const value = attributes[attr];
          const d = new Date(value);
          if (d instanceof Date && !isNaN(d) && d.toISOString() === value) {
            attributes[attr] = d;
          }
        }
      });
      return attributes;
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
