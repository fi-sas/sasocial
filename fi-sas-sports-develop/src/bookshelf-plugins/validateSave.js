/**
 * ValidateSave bookshelf plugin
 *
 * Extends bookshelf Model with validateSave and checkValidations methods.
 * validateSave should be set as a 'saving' event handler on the model's initialize method. This
 * collects the array of errors returned from checkValidations and throws a ValidationError if
 * needed. Additional validations to the model can be included by overwritting validateSave method
 * (keeping the existing behavior).
 *
 * checkValidations method validates the current model's attributes against the model's validations
 * property (should be constructed based on Checkit validators - see
 * https://github.com/tgriesser/checkit). The failed validations are returned in the form of an object
 *  with { error, message } format.
 *
 */
/* eslint-disable no-param-reassign, func-names */

import Checkit from 'checkit';
import { ValidationError } from '../errors';
import { ValidationRequiredValue, ValidationInvalidFormat } from '../error_codes';

module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({

    /**
     * Validates a save operation on the model, according to model's validations property.
     * Expects to be set as a 'saving' event handler on initialize method:
     * this.on('saving', this.validateSave);.
     * Uses _checkValidations to gather array of errors - if any error is returned, throws
     * ValidationError with array of found errors.
     * If any additional validation is desired, this method should be overwritten.
     *
     * @param {Model} model The model firing the event
     * @param {Object} attrs Attributes that will be inserted or updated
     * @param {Object} options  Options object passed to save method
     * @returns {Promise}
     */
    validateSave(model, attrs, options) {
      const errors = this.checkValidations(model, attrs, options);

      if (errors.length) {
        throw new ValidationError(errors);
      }
    },

    /**
     * Checks validations on current model according to its validations property
     * (see https://github.com/tgriesser/checkit).
     * If received options include patch set to true, only the validations of present
     * attributes are considered.
     * Returns array of errors with failed validations ( with ValidationRequiredValue
     * or ValidationInvalidFormat error code and Checkit original error message).
     *
     * @param {Model} model The model firing the event
     * @param {Object} attrs Attributes that will be inserted or updated
     * @param {Object} options  Options object passed to save method
     * @returns {Array} errors Array of found errors
     */
    checkValidations(model, attrs, options) {
      const validations = Object.assign({}, this.validations);
      const errors = [];

      if (options.patch === true) {
        Object.keys(validations).forEach((field) => {
          if (!(field in this.attributes)) {
            delete validations[field];
          }
        });
      }

      const [err] = Checkit(validations).runSync(this.attributes);

      if (err) {
        Object.keys(err.errors).forEach((field) => {
          const fieldErrors = err.errors[field].errors;
          fieldErrors.forEach((fieldError) => {
            const { message } = fieldError;
            const code = fieldError.rule === 'required'
              ? ValidationRequiredValue
              : ValidationInvalidFormat;
            errors.push({
              code, message, field, index: 0,
            });
          });
        });
      }

      return errors;
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
