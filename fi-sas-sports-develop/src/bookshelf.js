import bookshelf from 'bookshelf';
import db from './db';

const Bookshelf = bookshelf(db);
Bookshelf.plugin('pagination');
Bookshelf.plugin('visibility');
Bookshelf.plugin('registry');
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/formatActive`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/formatTimestamps`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/filterOrderAndFetch`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/validateSave`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/validateExistence`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/loadTranslations`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/deviceGroupScope`);
Bookshelf.plugin(`${__dirname}/bookshelf-plugins/externalWithRelated`);

export default Bookshelf;
