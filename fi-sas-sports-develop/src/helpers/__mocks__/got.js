module.exports = jest.fn((l, opts) => {
  if (!opts || !opts.headers || !opts.headers.Authorization) {
    return new Promise(resolve => resolve({
      statusCode: 401,
    }));
  }
  return new Promise(resolve => resolve({
    statusCode: 200,
    body: '{"data":[{"id":1}, {"id":2}]}',
  }));
});
