import got from 'got';
import { getLanguageId, getDeviceGroups, loadMediaFiles } from '../common';

describe('Common helper test', () => {
  afterEach(() => {
    got.mockClear();
  });

  describe('getLanguageId', () => {
    const mockedRequest = { headers: { 'x-language-id': 2 } };
    const mockedRequest2 = { headers: { 'x-language-acronym': 'EN' } };
    const mockedRequest3 = { headers: { 'x-language-id': null, 'x-language-acronym': 'EN' } };
    const mockedRequestNull = { headers: { 'x-language-id': null } };
    const mockedRequestNull1 = { headers: { 'x-language-acronym': null } };
    const mockedRequestError = { headers: { 'x-language-id': 'a' } };
    const mockedRequestError3 = { headers: { 'x-language-acronym': '9999' } };
    const mockedRequestError4 = { headers: { 'x-language-acronym': 'a' } };

    it('returns the language id from x-language-id header', async () => {
      let languageId = await getLanguageId(mockedRequest);
      expect(languageId).toEqual(2);
      languageId = await getLanguageId(mockedRequestNull);
      expect(languageId).toBeNull();
    });

    it('returns -1 when x-language-id is not number', async () => {
      const languageId = await getLanguageId(mockedRequestError);
      expect(languageId).toEqual(-1);
    });

    it('returns the language id from x-language-acronym header', async () => {
      got.mockImplementationOnce(() => ({
        statusCode: 200,
        body: '{"data":[{"id":2}]}',
      }));
      let languageId = await getLanguageId(mockedRequest2);
      expect(languageId).toEqual(2);
      got.mockImplementationOnce(() => ({
        statusCode: 200,
        body: '{"data":[{"id":2}]}',
      }));
      languageId = await getLanguageId(mockedRequest3);
      expect(languageId).toEqual(2);

      languageId = await getLanguageId(mockedRequestNull1);
      expect(languageId).toBeNull();
    });

    it('returns -1 if language id from x-language-acronym header does not exist', async () => {
      let languageId = await getLanguageId(mockedRequestError3);
      expect(languageId).toEqual(-1);
      languageId = await getLanguageId(mockedRequestError4);
      expect(languageId).toEqual(-1);
    });

    it('returns null if no headers are sent', async () => {
      const languageId1 = await getLanguageId({});
      expect(languageId1).toBeNull();
      const languageId2 = await getLanguageId({ headers: {} });
      expect(languageId2).toBeNull();
    });
  });

  describe('getDeviceGroups', () => {
    const mockedRequest = { headers: { authorization: 'Bearer ' }, authInfo: { deviceId: 2 } };
    const mockedRequestNull = { headers: {}, authInfo: { deviceId: null } };
    const mockedRequestUnauthorized = { headers: {}, authInfo: { deviceId: 2 } };

    it('returns group ids array', async () => {
      const groups1 = await getDeviceGroups(mockedRequest);
      expect(got).toHaveBeenCalled();
      expect(groups1).toBeInstanceOf(Array);
      expect(groups1).toHaveLength(2);
      expect(groups1).toContain(1);
      expect(groups1).toContain(2);
    });

    it('returns empty groups if authInfo or device are not defined', async () => {
      const groups1 = await getDeviceGroups({});
      expect(groups1).toBeInstanceOf(Array);
      expect(groups1).toHaveLength(0);
      const groups2 = await getDeviceGroups(mockedRequestNull);
      expect(groups2).toBeInstanceOf(Array);
      expect(groups2).toHaveLength(0);
    });

    it('returns empty groups if request to Configuration MS without authorization header', async () => {
      const groups1 = await getDeviceGroups(mockedRequestUnauthorized);
      expect(got).toHaveBeenCalled();
      expect(groups1).toBeInstanceOf(Array);
      expect(groups1).toHaveLength(0);
    });
  });

  describe('loadMediaFiles', () => {
    const mockedRequest = { headers: { authorization: 'Bearer ' }, authInfo: { deviceId: 2 } };
    const data = { id: 1, media_id: 1, media2_id: 2 };
    const dataArray = [{ id: 1, media_id: 1, media2_id: 2 }, { id: 2, media_id: 2, media2_id: 1 }];
    const mapRelatedToFields = { media: 'media_id', media2: 'media2_id' };
    const related = ['media', 'media2'];

    it('returns received data if no related entities are received', async () => {
      const newData = await loadMediaFiles(mockedRequest, data, [], mapRelatedToFields);
      expect(got).not.toHaveBeenCalled();
      expect(newData).toEqual(data);
    });

    it('returns received data if there is no mapping of related entities to fields', async () => {
      const newData = await loadMediaFiles(mockedRequest, data, related, { otherRel: 'otherRel_id' });
      expect(got).not.toHaveBeenCalled();
      expect(newData).toEqual(data);
      got.mockClear();
      const newData2 = await loadMediaFiles(mockedRequest, data, related, {});
      expect(got).not.toHaveBeenCalled();
      expect(newData2).toEqual(data);
      got.mockClear();
      const newData3 = await loadMediaFiles(mockedRequest, data, related);
      expect(got).not.toHaveBeenCalled();
      expect(newData3).toEqual(data);
    });

    it('loads related medias from single data object', async () => {
      const expectedData = {
        id: 1, media_id: 1, media2_id: 2, media: { id: 1 }, media2: { id: 2 },
      };
      const newData = await loadMediaFiles(mockedRequest, data, related, mapRelatedToFields);
      expect(got).toHaveBeenCalledWith(
        `${process.env.MS_MEDIA_ENDPOINT}api/v1/files?id=1&id=2&withRelated=translations`,
        { headers: { Authorization: 'Bearer ' } },
      );
      expect(newData).toEqual(expectedData);
    });

    it('loads empty medias from single data object when medias do not exist', async () => {
      const dataNoMedia = { id: 1, media_id: null, media2_id: null };
      const expectedDataNoMedia = {
        id: 1, media_id: null, media2_id: null, media: {}, media2: {},
      };
      const newData = await loadMediaFiles(mockedRequest, dataNoMedia, related, mapRelatedToFields);
      expect(newData).toEqual(expectedDataNoMedia);

      const dataNoMedia2 = { id: 1, media_id: 1, media2_id: null };
      const expectedDataNoMedia2 = {
        id: 1, media_id: 1, media2_id: null, media: { id: 1 }, media2: {},
      };
      const newData2 = await loadMediaFiles(mockedRequest, dataNoMedia2, related, mapRelatedToFields);
      expect(newData2).toEqual(expectedDataNoMedia2);
    });

    it('loads related medias from data array', async () => {
      const expectedDataArray = [
        {
          id: 1, media_id: 1, media2_id: 2, media: { id: 1 }, media2: { id: 2 },
        },
        {
          id: 2, media_id: 2, media2_id: 1, media: { id: 2 }, media2: { id: 1 },
        },
      ];
      const newData = await loadMediaFiles(mockedRequest, dataArray, related, mapRelatedToFields);
      expect(got).toHaveBeenCalledWith(
        `${process.env.MS_MEDIA_ENDPOINT}api/v1/files?id=1&id=2&withRelated=translations`,
        { headers: { Authorization: 'Bearer ' } },
      );
      expect(newData).toEqual(expectedDataArray);
    });

    it('loads empty medias from data array when medias do not exist', async () => {
      const dataArrayNoMedia = [
        { id: 1, media_id: null, media2_id: null },
        { id: 2, media_id: null, media2_id: null },
      ];
      const expectedDataArrayNoMedia = [
        {
          id: 1, media_id: null, media2_id: null, media: {}, media2: {},
        },
        {
          id: 2, media_id: null, media2_id: null, media: {}, media2: {},
        },
      ];
      const newData = await loadMediaFiles(mockedRequest, dataArrayNoMedia, related, mapRelatedToFields);
      expect(newData).toEqual(expectedDataArrayNoMedia);

      const dataArrayNoMedia2 = [
        { id: 1, media_id: 1, media2_id: null },
        { id: 2, media_id: null, media2_id: 1 },
      ];
      const expectedDataArrayNoMedia2 = [
        {
          id: 1, media_id: 1, media2_id: null, media: { id: 1 }, media2: {},
        },
        {
          id: 2, media_id: null, media2_id: 1, media: {}, media2: { id: 1 },
        },
      ];
      const newData2 = await loadMediaFiles(mockedRequest, dataArrayNoMedia2, related, mapRelatedToFields);
      expect(newData2).toEqual(expectedDataArrayNoMedia2);
    });

    it('gets media objects from Medias MS', async () => {
      await loadMediaFiles(mockedRequest, data, related, mapRelatedToFields);
      expect(got).toHaveBeenCalledWith(
        `${process.env.MS_MEDIA_ENDPOINT}api/v1/files?id=1&id=2&withRelated=translations`,
        { headers: { Authorization: 'Bearer ' } },
      );
      got.mockClear();
      await loadMediaFiles(mockedRequest, dataArray, related, mapRelatedToFields);
      expect(got).toHaveBeenCalledWith(
        `${process.env.MS_MEDIA_ENDPOINT}api/v1/files?id=1&id=2&withRelated=translations`,
        { headers: { Authorization: 'Bearer ' } },
      );
    });

    it('does not try to get medias if not needed', async () => {
      const dataNoMedia = { id: 1, media_id: null, media2_id: null };
      await loadMediaFiles(mockedRequest, dataNoMedia, related, mapRelatedToFields);
      expect(got).not.toHaveBeenCalledWith();
      const dataArrayNoMedia = [
        { id: 1, media_id: null, media2_id: null },
        { id: 2, media_id: null, media2_id: null },
      ];
      got.mockClear();
      await loadMediaFiles(mockedRequest, dataArrayNoMedia, related, mapRelatedToFields);
      expect(got).not.toHaveBeenCalledWith();
    });
  });
});
