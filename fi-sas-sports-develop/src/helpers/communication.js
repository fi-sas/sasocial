/**
 * Helper to access common data from request.
 */
/* eslint-disable import/prefer-default-export */
import got from 'got';

/**
 * Internal helper method to request a token on the MS Auth.
 *
 * @param {*} req
 * @param {String} MS Micro service name (AUTH, CONFIG, MEDIA, CURRENT_ACCOUNT, etc)
 * @param {*} endpoint
 */
async function token(body = {}) {
  const response = await got(`${process.env.MS_AUTH_ENDPOINT}api/v1/authorize/device/1`, {
    method: 'POST',
    json: true,
    body,
  });

  return response.body;
}

/**
 * Access the received endpoint from the received micro service.
 * Uses authorization headers to make the request. If no request is received,
 * (should only happen when function is not called in a request scope), a GUEST
 * token is constructed and used.
 * If request can be decoded, its data is returned. Otherwise, an exception is thrown.
 *
 * @param {*} req
 * @param {String} MS Micro service name (AUTH, CONFIG, MEDIA, CURRENT_ACCOUNT, etc)
 * @param {*} endpoint
 */
async function access(req, MS, endpoint) {
  if (req === null) {
    const res = await token();
    const authToken = res.data[0].token;
    // eslint-disable-next-line no-param-reassign
    req = { headers: { authorization: `Bearer ${authToken}` } };
  }

  const auth = req.headers.authorization;
  const url = (process.env[`MS_${MS}_ENDPOINT`]) ? (process.env[`MS_${MS}_ENDPOINT`]) : '';

  let result = null;
  const response = await got(`${url}api/v1/${endpoint}`, {
    headers: {
      Authorization: auth,
    },
  });
  if (response.statusCode === 200) {
    const body = JSON.parse(response.body);
    result = body.data;
  }

  return result;
}

/**
 * Executes a POST request to the received endpoint from the received micro service.
 * Uses authorization headers to make the request. If no request is received,
 * (should only happen when function is not called in a request scope), a GUEST
 * token is constructed and used.
 * If request can be decoded, its data is returned. Otherwise, an exception is thrown.
 *
 * @param {*} req Current req object (hopefully containing authorization headers).
 * @param {String} MS Micro service name (AUTH, CONFIG, MEDIA, CURRENT_ACCOUNT, etc).
 * @param {*} endpoint The endpoint where the post should be executed.
 * @param {Object} body The data to send in the body of the POST request.
 */
async function post(req, MS, endpoint, body) {
  if (req === null) {
    const res = await token();
    const authToken = res.data[0].token;
    // eslint-disable-next-line no-param-reassign
    req = { headers: { authorization: `Bearer ${authToken}` } };
  }

  const auth = req.headers.authorization;
  const url = (process.env[`MS_${MS}_ENDPOINT`]) ? (process.env[`MS_${MS}_ENDPOINT`]) : '';

  let result = null;
  const response = await got(`${url}api/v1/${endpoint}`, {
    headers: {
      Authorization: auth,
      'Internal-Network': true,
    },
    method: 'POST',
    json: true,
    body,
  });


  if (response.statusCode === 201) {
    result = response.body;
  }

  return result;
}

/**
 * Executes a PATCH request to the received endpoint from the received micro service.
 * Uses authorization headers to make the request. If no request is received,
 * (should only happen when function is not called in a request scope), a GUEST
 * token is constructed and used.
 * If request can be decoded, its data is returned. Otherwise, an exception is thrown.
 *
 * @param {*} req Current req object (hopefully containing authorization headers).
 * @param {String} MS Micro service name (AUTH, CONFIG, MEDIA, CURRENT_ACCOUNT, etc).
 * @param {*} endpoint The endpoint where the post should be executed.
 * @param {Object} body The data to send in the body of the POST request.
 */
async function patch(req, MS, endpoint, body) {
  if (req === null) {
    const res = await token();
    const authToken = res.data[0].token;
    // eslint-disable-next-line no-param-reassign
    req = { headers: { authorization: `Bearer ${authToken}` } };
  }

  const auth = req.headers.authorization;
  const url = (process.env[`MS_${MS}_ENDPOINT`]) ? (process.env[`MS_${MS}_ENDPOINT`]) : '';

  let result = null;
  const response = await got(`${url}api/v1/${endpoint}`, {
    headers: {
      Authorization: auth,
      'Internal-Network': true,
    },
    method: 'PATCH',
    json: true,
    body,
  });

  if (response.statusCode === 200) {
    result = response.body;
  }

  return result;
}

/**
 * Executes a DELETE request to the received endpoint from the received micro service.
 * Uses authorization headers to make the request. If no request is received,
 * (should only happen when function is not called in a request scope), a GUEST
 * token is constructed and used.
 * If request is successful (HTTP status code 204 is returned), true is returned.
 * Otherwise, returns false.
 *
 * @param {*} req Current req object (hopefully containing authorization headers).
 * @param {String} MS Micro service name (AUTH, CONFIG, MEDIA, CURRENT_ACCOUNT, etc).
 * @param {*} endpoint The endpoint where the post should be executed.
 */
async function remove(req, MS, endpoint) {
  if (req === null) {
    const res = await token();
    const authToken = res.data[0].token;
    // eslint-disable-next-line no-param-reassign
    req = { headers: { authorization: `Bearer ${authToken}` } };
  }

  const auth = req.headers.authorization;
  const url = (process.env[`MS_${MS}_ENDPOINT`]) ? (process.env[`MS_${MS}_ENDPOINT`]) : '';
  const response = await got(`${url}api/v1/${endpoint}`, {
    headers: {
      Authorization: auth,
      'Internal-Network': true,
    },
    method: 'DELETE',
    json: true,
  });

  return (response.statusCode === 204);
}

/**
 * Creates an alert of the received alert key with the receive data.
 * Sends a POST request to the Notifications MS, using authorization headers
 * to make the request. If no request is received,
 * (should only happen when function is not called in a request scope), a GUEST
 * token is constructed and used.
 * If request can be decoded, its data is returned. Otherwise, an exception is thrown.
 *
 * @param {*} req Current req object (hopefully containing authorization headers).
 * @param {String} alertKey Type's Key of the Alert to be created.
 * @param {Object} data Alert data.
 * @param {number} userId User Id of the Alert recepient (if recipient is a registered User).
 * @param {Array} medias Array of medias to be sent with Alert. Defaults to empty array.
 * @param {Object} user_data User data of the Alert recipient (used to reach them).
 */
async function alert(req, alertKey, data, userId, medias = [], user_data = null) {
  if (req === null) {
    const res = await token();
    const authToken = res.data[0].token;
    // eslint-disable-next-line no-param-reassign
    req = { headers: { authorization: `Bearer ${authToken}` } };
  }

  const auth = req.headers.authorization;
  const url = process.env.MS_NOTIFICATIONS_ENDPOINT;
  const body = {
    user_id: userId,
    user_data,
    data,
    medias,
  };

  let result = null;
  const response = await got(`${url}api/v1/alerts/${alertKey}`, {
    headers: {
      Authorization: auth,
    },
    method: 'POST',
    json: true,
    body,
  });


  if (response.statusCode === 201) {
    result = response.body;
  }

  return result;
}

export {
  access, post, patch, alert, remove,
};
