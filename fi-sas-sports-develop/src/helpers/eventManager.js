/**
 * Helper to create and edit events from sport entities.
 */
import { access, post, patch, remove } from './communication';
import { ValidationError } from '../errors';
import * as Codes from '../error_codes';

import Configuration from '../models/configuration';

/**
 * Helper function to create event, receiving needed data and request.
 *
 * @param {Object} req
 * @param {Object} data
 */
async function createEvent(
  req,
  data,
) {
  let response = false;
  try {
    response = await post(req, 'CALENDAR', 'events', data);
  } catch (e) {
    const errors = (e.response && e.response.body && e.response.body.errors)
      ? e.response.body.errors : [];
    errors.unshift({ code: Codes.ValidationRequiredValue, message: 'Could not create Event' });
    throw new ValidationError(errors);
  }

  return response && response.status === 'success' && response.data instanceof Array && response.data.length === 1
    ? response.data[0].id : false;
}

/**
 * Helper function to patch event, receiving needed data, id and request.
 *
 * @param {Object} req
 * @param {Int} eventId
 * @param {Object} data
 */
async function editEvent(req, eventId, data) {
  let response = false;
  try {
    response = await patch(req, 'CALENDAR', `events/${eventId}`, data);
  } catch (e) {
    const errors = (e.response && e.response.body && e.response.body.errors)
      ? e.response.body.errors : [];
    errors.unshift({ code: Codes.ValidationRequiredValue, message: 'Could not update Event' });
    throw new ValidationError(errors);
  }

  return response && response.status === 'success' && response.data instanceof Array && response.data.length === 1
    ? response.data[0].id : false;
}

// --------------------------------------------------------------------------------------------

/**
 * Creates a new Event from received Activity instance.
 * Assumes venue and modality relationships are loaded.
 * Uses "regular_training_event_category_id", "competition_event_category_id" or
 * "general_activity_event_category_id" configuration.
 *
 * @param {Object} req
 * @param {Object} training
 */
async function createEventFromActivity(req, activity) {
  const activityType = activity.get('type');

  // (try) Fetch event category id
  const configurationMapping = {
    'REGULAR TRAINING': 'regular_training_event_category_id',
    COMPETITION: 'competition_event_category_id',
    'GENERAL ACTIVITY': 'general_activity_event_category_id',
  };
  const configurationKey = configurationMapping[activityType];
  const categoryId = await Configuration.getConfig(configurationKey);
  if (!categoryId) {
    throw new ValidationError([{
      code: Codes.ValidationRequiredValue,
      message: `Missing required "${configurationKey}" configuration`,
    }]);
  }

  // (try) Fetch registration form id
  const registrationFormId = await Configuration.getConfig('activity_registration_form_id');
  if (!registrationFormId) {
    throw new ValidationError([{
      code: Codes.ValidationRequiredValue,
      message: 'Missing required "activity_registration_form_id" configuration',
    }]);
  }

  // Fetch venue and modality
  const venue = activity.related('venue');
  const modality = activity.related('modality');

  // (try) Fetch default language id
  let defaultLanguageId = null;
  try {
    const defaultLanguageR = await access(req, 'CONFIG', 'languages?acronym=PT');
    defaultLanguageId = defaultLanguageR[0].id;
  } catch (e) {
    throw new ValidationError([{
      code: Codes.ValidationRequiredValue,
      message: 'Could not fetch default Language to insert in Event',
    }]);
  }

  const data = {
    service_id: process.env.MS_CALENDAR_SERVICE_ID,
    category_id: categoryId,
    publish_start_date: new Date(),
    publish_end_date: null,
    start_date: activity.get('start_date'),
    end_date: activity.get('end_date'),
    recurrence: activityType === 'REGULAR TRAINING' ? 'Weekly' : 'None',
    notifications_enabled: false,
    notification_target_id: null,
    notification_alert_type_key: null,
    notification_start_date: null,
    number_of_notifications: 0,
    notification_recurrence: 'None',
    longitude: venue.get('longitude'),
    latitude: venue.get('latitude'),
    address: venue.get('address'),
    address_no: venue.get('address_no'),
    city: venue.get('city'),
    allow_interest: true,
    allow_registration: true,
    max_registrations: null,
    registration_approval_needed: true,
    registration_form_id: registrationFormId,
    registration_fee: 0,
    registration_tax_id: null,
    registration_code: null,
    cancelled: false,
    contacts: [],
    translations: [
      {
        language_id: defaultLanguageId,
        name: activity.get('name'),
        description: activity.get('description'),
        url: '',
      },
    ],
    medias: [
      {
        file_id: modality.get('file_id'),
      },
    ],
  };

  return createEvent(req, data);
}

/**
 * Edits an Event from received Activity instance.
 * Returns true if successful, or throws a ValidationError if not.
 *
 * Assumes start date or end date were changed.
 * Directly updates the Event with current dates.
 *
 * @param {Object} req
 * @param {Object} activity
 */
async function editEventFromActivity(req, activity) {
  const data = {
    start_date: activity.get('start_date'),
    end_date: activity.get('end_date'),
  };

  const errors = [];
  const edited = await editEvent(req, activity.get('event_id'), data);
  if (!edited) {
    errors.unshift({ code: Codes.ValidationRequiredValue, message: 'Could not update Event' });
    throw new ValidationError(errors);
  }

  return true;
}

/**
 * Deletes an Event from received Event id.
 * Assumes start date or end date were changed.
 * Returns true if successful or throws a ValidationError if not.
 *
 * @param {Object} req
 * @param {Object} eventId
 */
async function deleteEventFromId(req, eventId) {
  let success = false;
  let errors = [];
  try {
    success = await remove(req, 'CALENDAR', `events/${eventId}`);
  } catch (e) {
    success = false;
    errors = (e.response && e.response.body && e.response.body.errors)
      ? e.response.body.errors : [];
  }

  if (!success) {
    errors.unshift({ code: Codes.InvalidDeleteDueToRelatedEntityExistence, message: 'Could not delete Event' });
    throw new ValidationError(errors);
  }

  return true;
}


export { createEventFromActivity, editEventFromActivity, deleteEventFromId };
