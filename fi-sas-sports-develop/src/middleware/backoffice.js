/**
 * Express middleware for detection of backoffice requests.
 * Usage: const isBackoffice = req.backoffice // true/false
 * @public
 */
function backoffice(req, res, next) {
  req.backoffice = (req.authInfo && req.authInfo.device && req.authInfo.device.name === 'BO');
  next();
}

export default backoffice;
