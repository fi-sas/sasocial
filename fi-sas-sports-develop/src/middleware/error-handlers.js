/*
 * TODO: Improve error handler and logging
 */
import { ValidationError, ParameterError, NotFoundError } from '../errors';

/* eslint-disable no-console, no-unused-vars */
export const errorHandler = (err, req, res, next) => {
  if (err instanceof ValidationError || err instanceof ParameterError) {
    res.formatter.badRequest(err.errors);
    return;
  }

  if (err instanceof NotFoundError) {
    res.formatter.notFound({ code: err.code, message: err.message });
    return;
  }

  const status = err.status || 500;
  const code = req.app.get('env') === 'development'
    ? err.code || status : status;
  const message = req.app.get('env') === 'development'
    ? err.message : 'Oopps!';
  const error = { code, message };

  if (status === 500) {
    res.formatter.serverError(error);
    return;
  }

  if (status === 404) {
    res.formatter.notFound(error);
    return;
  }

  res.status(status).json({
    status: 'error',
    link: {},
    data: [],
    errors: [error],
  });
};

export const logErrors = (err, req, res, next) => {
  if (req.app.get('env') === 'development') {
    console.log(err.stack);
  }

  next(err);
};
