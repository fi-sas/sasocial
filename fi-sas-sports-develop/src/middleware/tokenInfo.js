/**
 * Token Info Middleware: decodes token information.
 *
 * Updates req.authInfo with validated data.
 * @public
 */

import jwt from 'jsonwebtoken';
import got from 'got';
import _ from 'lodash';

import * as Codes from '../error_codes';

export default async function tokenInfo(req, res, next) {
  // Ignore token validation for OPTIONS requests
  if (req.method === 'OPTIONS') {
    return next();
  }

  req.authInfo = null;

  // Token MUST be provided as query param or as an header
  let token = req.query.token || req.headers.authorization;

  if (token) {
    token = token.replace('Bearer ', '');
    // Validate token
    try {
      const result = await got(`${process.env.MS_AUTH_ENDPOINT}api/v1/authorize/validate-token/${token}`);
      if (_.isUndefined(result.statusCode) || result.statusCode !== 200) {
        req.authInfo = Codes.AuthenticationInvalidToken;
        return next();
      }

      const parsedBody = JSON.parse(result.body);
      req.authInfo = jwt.verify(token, process.env.JWT_SECRET_KEY);

      if (!_.isUndefined(parsedBody.data) && !_.isUndefined(parsedBody.data[0])) {
        req.authInfo.user = parsedBody.data[0].user;
        req.authInfo.scopes = parsedBody.data[0].scopes;
      }

      return next();
    } catch (err) {
      req.authInfo = Codes.AuthenticationInvalidToken;
      return next();
    }
  } else {
    req.authInfo = Codes.AuthenticationMissingToken;
    return next();
  }
}
