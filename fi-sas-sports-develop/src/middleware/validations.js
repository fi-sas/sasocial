import * as Codes from '../error_codes';

export default [
  // Validation for file id inside modality
  {
    resource: /api\/v1\/modalities(\/?[^/]*)?$/m,
    path: 'file_id',
    targetMS: process.env.MS_MEDIA_ENDPOINT,
    targetResource: 'files',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidFileId,
    message: 'The file id provided is not valid.',
  },
  // Validation for file id inside reservation
  {
    resource: /api\/v1\/reservations(\/?[^/]*)?$/m,
    path: 'file_id',
    targetMS: process.env.MS_MEDIA_ENDPOINT,
    targetResource: 'files',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: Codes.InvalidFileId,
    message: 'The file id provided is not valid.',
  },
  // Validation for location id inside venue
  {
    resource: /api\/v1\/venues(\/?[^/]*)?$/m,
    path: 'location_id',
    targetMS: process.env.MS_INFRASTRUCTURE_ENDPOINT,
    targetResource: 'locations',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: 1,
    message: 'The location id provided is not valid.',
  },
  {
    resource: /api\/v1\/configurations\/regular_training_event_category_id(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'value',
    targetMS: process.env.MS_CALENDAR_ENDPOINT,
    targetResource: 'event-categories',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: 1,
    message: 'The event category Id provided is not valid.',
  },
  {
    resource: /api\/v1\/configurations\/general_activity_event_category_id(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'value',
    targetMS: process.env.MS_CALENDAR_ENDPOINT,
    targetResource: 'event-categories',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: 1,
    message: 'The event category Id provided is not valid.',
  },
  {
    resource: /api\/v1\/configurations\/competition_event_category_id(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'value',
    targetMS: process.env.MS_CALENDAR_ENDPOINT,
    targetResource: 'event-categories',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: 1,
    message: 'The event category Id provided is not valid.',
  },
  {
    resource: /api\/v1\/configurations\/activity_registration_form_id(\/[^/]*)?(\?.*)?$/m,
    singlePath: 'value',
    targetMS: process.env.MS_CALENDAR_ENDPOINT,
    targetResource: 'registration-forms',
    endpoints: ['POST', 'PUT', 'PATCH'],
    code: 1,
    message: 'The registration form Id provided is not valid.',
  },
];
