/* eslint import/no-cycle: 0 */
import Joi from 'joi';

import Bookshelf from '../bookshelf';
import * as Errors from '../errors';
import * as Codes from '../error_codes';
import activitySchema from '../schemas/activity-schema';

import Modality from './modality';
import Venue from './venue';

const Activity = Bookshelf.Model.extend({
  tableName: 'activity',
  hasTimestamps: true,

  modality() {
    return this.belongsTo(Modality);
  },

  venue() {
    return this.belongsTo(Venue);
  },

  parse(response) {
    return this.parseBooleans(response, ['active']);
  },

  format(attributes) {
    const formatted = this.formatModelTimestamps(attributes, ['start_date', 'end_date', 'created_at', 'updated_at']);
    return this.formatBooleans(formatted, ['active']);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.assignTimes);
    this.on('saving', this.validateSave);
    this.on('saving', this.assignUpdatedDates);
  },

  /**
   * Assigns start and end time and training day.
   */
  assignTimes() {
    const startDate = new Date(this.get('start_date'));
    const endDate = new Date(this.get('end_date'));

    if (this.hasChanged('start_date')) {
      const startTime = this.get('start_date')
        ? `${startDate.getHours().toString().padStart(2, '0')}:${startDate.getMinutes().toString().padStart(2, '0')}`
        : null;
      this.set('start_time', startTime);
    }

    if (this.hasChanged('end_date')) {
      const endTime = this.get('end_date')
        ? `${endDate.getHours().toString().padStart(2, '0')}:${endDate.getMinutes().toString().padStart(2, '0')}`
        : null;
      this.set('end_time', endTime);
    }

    if (this.hasChanged('type') || this.hasChanged('start_date')) {
      const days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
      this.set('training_day', this.get('type') === 'REGULAR TRAINING' ? days[startDate.getDay()] : null);
    }
  },

  /**
   * Checks if start date or end date of current instance has changed
   * and assigns updated_dates property accordingly.
   */
  assignUpdatedDates() {
    this.updated_dates = this.hasChanged('start_date') || this.hasChanged('end_date');
  },

  /**
   * Validate save of model instance with Joi schema, and FK relationships.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, activitySchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate training modality and venue
    const errors = [];
    if (this.hasChanged('modality_id')) {
      try {
        await Modality.validateExistence(this.get('modality_id'));
      } catch (e) {
        errors.push({
          code: Codes.ValidationMissingRelatedEntity,
          field: 'modality_id',
          message: 'The modality id provided is not valid.',
        });
      }
    }
    if (this.hasChanged('venue_id')) {
      try {
        await Venue.validateExistence(this.get('venue_id'));
      } catch (e) {
        errors.push({
          code: Codes.ValidationMissingRelatedEntity,
          field: 'venue_id',
          message: 'The venue id provided is not valid.',
        });
      }
    }

    // Block type edition
    if (this.hasChanged('type') && !this.isNew()) {
      errors.push({
        code: Codes.InvalidOperation,
        field: 'type',
        message: 'The Activity type cannot be updated.',
      });
    }

    if (errors.length) {
      throw new Errors.ValidationError(errors);
    }
  },
}, {
  filterFields: [
    'id',
    'type',
    'modality_id',
    'venue_id',
    'event_id',
    'name',
    'description',
    'start_date',
    'end_date',
    'training_day',
    'active',
    'updated_at',
    'created_at',
  ],
  sortFields: [
    'id',
    'type',
    'modality_id',
    'venue_id',
    'event_id',
    'name',
    'description',
    'start_date',
    'end_date',
    'training_day',
    'start_time',
    'end_time',
    'active',
    'updated_at',
    'created_at',
  ],
  defaultWithRelated: ['modality', 'venue'],
  withRelated: ['modality', 'venue'],
});

export default Activity;
