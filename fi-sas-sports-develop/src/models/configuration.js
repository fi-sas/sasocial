import Joi from 'joi';
import Bookshelf from '../bookshelf';

import * as Errors from '../errors';
import ConfigurationSchema from '../schemas/configuration-schema';

const Configuration = Bookshelf.Model.extend({
  tableName: 'configuration',
  hasTimestamps: true,
  hidden: ['id'],

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  format(attributes) {
    const formatted = Object.assign({}, attributes);
    if (typeof formatted.value === 'object' || formatted.value instanceof Array) {
      formatted.value = JSON.stringify(formatted.value);
    }

    return this.formatModelTimestamps(formatted);
  },

  parse(response) {
    try {
      response.value = JSON.parse(response.value);
    } catch (e) {}

    return response;
  },

  /**
   * Validate save of model instance with Joi schema.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;

    const valueSchemas = {
      regular_training_event_category_id: Joi.number().integer().positive()
        .error(() => ({
          message: 'value for "regular_training_event_category_id" configuration must be a positive integer',
        })),
      competition_event_category_id: Joi.number().integer().positive()
        .error(() => ({
          message: 'value for "regular_training_event_category_id" configuration must be a positive integer',
        })),
      general_activity_event_category_id: Joi.number().integer().positive()
        .error(() => ({
          message: 'value for "regular_training_event_category_id" configuration must be a positive integer',
        })),
      activity_registration_form_id: Joi.number().integer().positive()
        .error(() => ({
          message: 'value for "activity_registration_form_id" configuration must be a positive integer',
        })),
    };

    let schema = ConfigurationSchema;
    if (valueSchemas[this.get('key')]) {
      schema = ConfigurationSchema.keys({
        value: valueSchemas[this.get('key')],
      });
    } else {
      schema = ConfigurationSchema.keys({
        value: Joi.any(),
      });
    }

    // Validate data according to model schema
    const result = Joi.validate(data, schema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    return true;
  },
}, {
  filterFields: ['id', 'key', 'created_at', 'updated_at'],
  sortFields: ['id', 'key', 'created_at', 'updated_at'],
  related: [],
  defaultRelated: [],

  /**
   * Returns a Configuration model instance for the provided configuration key, creating a new one
   * if it does not exist yet - unless throwNotFoundError is sent as true. If a configuration instance
   * for the received key does not exist and throwNotFoundError is received as true, a NotFounError is
   * thrown.
   *
   * @param {String} key The configuration key.
   * @param {Boolean} throwNotFoundError If true and instance does not exist, throws NotFoundError.
   * @returns {Configuration} The Configuration model instance.
   */
  async getConfigInstance(key, throwNotFoundError = false) {
    const conf = await Configuration.forge().where({ key }).fetch();

    if (conf) {
      return conf;
    }

    if (throwNotFoundError) {
      throw new Errors.NotFoundError('Configuration not found');
    } else {
      return Configuration.forge({ key });
    }
  },

  /**
   * Returns the configuration value for the received key, or null if none is defined.
   *
   * @param {string} key Configuration key
   * @returns {string*} Configuration value or null
   */
  async getConfig(key) {
    const c = await Configuration.where('key', key).fetch();
    return c ? c.get('value') : null;
  },
});

export default Configuration;
