/* eslint import/no-cycle: 0 */
import Joi from 'joi';
import _ from 'lodash';

import Bookshelf from '../bookshelf';
import * as Errors from '../errors';
import * as Codes from '../error_codes';
import modalitySchema from '../schemas/modality-schema';

import Activity from './activity';

const Modality = Bookshelf.Model.extend({
  tableName: 'modality',
  hasTimestamps: true,

  activities() {
    return this.hasMany(Activity);
  },

  general_activities() {
    return this.hasMany(Activity)
      .query({ where: { type: 'GENERAL ACTIVITY' } });
  },

  regular_trainings() {
    return this.hasMany(Activity)
      .query({ where: { type: 'REGULAR TRAINING' } });
  },

  competitions() {
    return this.hasMany(Activity)
      .query({ where: { type: 'COMPETITION' } });
  },

  parse(response) {
    return this.parseBooleans(response, ['active']);
  },

  format(attributes) {
    return this.formatBooleans(attributes, ['active']);
  },

  getExternalWithRelated() {
    return Modality.externalWithRelated;
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('destroying', this.validateDestroy);
    this.on('saving', this.validateSave);
    // Remove external with related before fetch
    this.attachExternalWithRelated();
  },

  async validateDestroy(model, options) {
    const opts = { transacting: options.transacting };

    // Check if there are related activities and prevent delete if so
    await this.load('activities', opts);

    if (this.related('activities').length > 0) {
      throw new Errors.ValidationError({
        code: Codes.InvalidDeleteDueToRelatedEntityExistence,
        message: 'The Modality can not be deleted because it has associated activities',
      });
    }
  },

  /**
   * Validate save of model instance with Joi schema, and FK relationships.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, modalitySchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
}, {
  filterFields: [
    'id',
    'file_id',
    'name',
    'active',
    'updated_at',
    'created_at',
  ],
  sortFields: [
    'id',
    'file_id',
    'name',
    'active',
    'updated_at',
    'created_at',
  ],
  defaultWithRelated: [],
  withRelated: ['activities', 'general_activities', 'regular_trainings', 'competitions', 'file'],
  externalWithRelated: {
    file: {
      targetMS: 'MS_MEDIA_ENDPOINT',
      targetResource: 'files',
      path: 'file_id',
      destination: 'file',
    },
  },
});

export default Modality;
