/* eslint import/no-cycle: 0 */
import Joi from 'joi';

import Bookshelf from '../bookshelf';
import Venue from './venue';
import * as Errors from '../errors';
import * as Codes from '../error_codes';
import reservationSchema from '../schemas/reservation-schema';

import ReservationHistory from './reservation_history';

const Reservation = Bookshelf.Model.extend({
  tableName: 'reservation',
  hasTimestamps: true,

  venue() {
    return this.belongsTo(Venue);
  },

  history() {
    return this.hasMany(ReservationHistory);
  },

  format(attributes) {
    return this.formatModelTimestamps(attributes, ['start_date', 'end_date', 'created_at', 'updated_at']);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.prepareCreation); // actually, 'creating' would be better, but would invert the order
    this.on('saving', this.validateSave);
    this.on('destroying', this.destroyCascade);

    this.on('saved', this.trackHistory);
  },

  /**
   * Prepare create action by setting Pending status.
   */
  prepareCreation() {
    if (this.isNew()) {
      // Set new status
      this.set('status', 'Pending');
    }
  },

  /**
   * Validate save of model instance with Joi schema, and FK relationships.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, reservationSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate venue
    const errors = [];
    if (this.hasChanged('venue_id')) {
      try {
        await Venue.validateExistence(this.get('venue_id'));
      } catch (e) {
        errors.push({
          code: Codes.ValidationMissingRelatedEntity,
          field: 'venue_id',
          message: 'The venue id provided is not valid.',
        });
      }
    }

    // Validate start and end date of the same day
    // ! If this is removed, the availability check must be updated (day of week validations)
    if (this.hasChanged('start_date') || this.hasChanged('end_date')) {
      if (new Date(this.get('start_date')).getDate() !== new Date(this.get('end_date')).getDate()) {
        errors.push({
          code: Codes.ValidationInvalidFormat,
          field: 'end_date',
          message: 'The end date must be the same day as start date.',
        });
      }
    }

    // Validate availability if any date or status has changed and reservation is not being
    // cancelled or rejected
    if ((this.hasChanged('status') || this.hasChanged('start_date') || this.hasChanged('end_date'))
      && !(this.get('status') === 'Cancelled' || this.get('status') === 'Rejected')) {
      const ok = await this.checkAvailability();
      if (!ok) {
        errors.push({
          code: Codes.InvalidOperation,
          field: 'start_date',
          message: 'The venue is not available for the provided dates.',
        });
      }
    }

    if (errors.length) {
      throw new Errors.ValidationError(errors);
    }
  },

  /**
   * Destroys cascade the reservation history.
   *
   * @param {Object} model
   * @param {Object} options
   */
  destroyCascade(model, options) {
    const opts = { transacting: options.transacting };

    // Cascade delete group_devices instances and components instances
    const history = this.load('history', opts).then(it => it.related('history').invokeThen('destroy', opts));

    return Promise.all([history]);
  },

  /**
   * Track Reservation history after every save.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async trackHistory(model, attrs, options) {
    const opts = (options.transacting) ? { transacting: options.transacting } : {};
    if (!options.dontTrackHistory) {
      return ReservationHistory.forge({
        reservation_id: this.id,
        recurrence: this.get('recurrence'),
        start_date: this.get('start_date'),
        end_date: this.get('end_date'),
        status: this.get('status'),
        user_id: this.historyUser,
        notes: this.historyNotes,
        action: this.historyAction,
      }).save(null, opts);
    }
    return true;
  },

  /**
   * Checks if current Reservation is viable according to its venue availability.
   * Uses overlappingReservations function as helper.
   *
   * @returns boolean True if reservation is possible, false otherwise.
   */
  async checkAvailability() {
    const overlappingReservations = await this.overlappingReservations();
    return overlappingReservations === 0;
  },

  /**
   * Returns number of overlapping reservations.
   *
   * @returns number
   */
  async overlappingReservations() {
    // Gather start and end dates, times and day of week
    const startDate = new Date(this.get('start_date'));
    const startTime = `${startDate.getHours().toString().padStart(2, '0')}:${startDate.getMinutes().toString().padStart(2, '0')}:00`;
    const endDate = new Date(this.get('end_date'));
    const endTime = `${endDate.getHours().toString().padStart(2, '0')}:${endDate.getMinutes().toString().padStart(2, '0')}:00`;
    const dayOfWeek = startDate.getDay() + 1; // prepare to compare with MySQL day of week

    // Prepare helper function to determine overlap of times
    const overlapTimes = (partialQ) => {
      // start time or end time of entries falls between the entry range
      partialQ.where((partialQOverlapStart) => {
        partialQOverlapStart.whereRaw('TIME(start_date) >= ?', [startTime]);
        partialQOverlapStart.whereRaw('TIME(start_date) < ?', [endTime]);
      });
      partialQ.orWhere((partialQOverlapEnd) => {
        partialQOverlapEnd.whereRaw('TIME(end_date) > ?', [startTime]);
        partialQOverlapEnd.whereRaw('TIME(end_date) <= ?', [endTime]);
      });
      // or the start or end time of entry falls between entries range
      partialQ.orWhere((partialQOverlapStart) => {
        partialQOverlapStart.whereRaw('CAST(? AS TIME) >= TIME(start_date)', [startTime]);
        partialQOverlapStart.whereRaw('CAST(? AS TIME) < TIME(end_date)', [startTime]);
      });
      partialQ.orWhere((partialQOverlapEnd) => {
        partialQOverlapEnd.whereRaw('CAST(? AS TIME) > TIME(start_date)', [endTime]);
        partialQOverlapEnd.whereRaw('CAST(? AS TIME) <= TIME(end_date)', [endTime]);
      });
    };

    const overlappingReservations = await Reservation.forge()
      .where((qb) => {
        // Relevant registrations for the case
        qb.where('venue_id', this.get('venue_id'));
        qb.where('status', 'Approved');
        if (!this.isNew()) {
          qb.where('id', '!=', this.get('id'));
        }

        // And it goes like this:
        switch (this.get('recurrence')) {
          // 0 - If Saving None recurrence reservation:
          case 'None':
            // Count entries with overlap times and...
            qb.where(overlapTimes);
            qb.where((qbNone) => {
              // ... overlap times and non recurring reservations on the same day
              qbNone.where((qbNoneNone) => {
                qbNoneNone.where('recurrence', 'None');
                qbNoneNone.whereRaw('DATEDIFF(start_date, ?) = 0', [startDate]);
              });
              // ... overlap times and daily reservations
              qbNone.orWhere((qbNoneDaily) => {
                qbNoneDaily.where('recurrence', 'Daily');
                qbNoneDaily.where('start_date', '<=', startDate);
              });
              // ... overlap times and weekly reservations on the same day of week
              qbNone.orWhere((qbNoneWeekly) => {
                qbNoneWeekly.where('recurrence', 'Weekly');
                qbNoneWeekly.whereRaw('DAYOFWEEK(start_date) = ?', [dayOfWeek]);
                qbNoneWeekly.where('start_date', '<=', startDate);
              });
            });
            break;
          // 1 - If saving Daily reservation:
          case 'Daily':
            // Count entries with overlap time and ...
            qb.where(overlapTimes);
            qb.where((qbDaily) => {
              // ... overlap times and non recurring reservations later on
              qbDaily.where((qbDailyNone) => {
                qbDailyNone.where('recurrence', 'None');
                qbDailyNone.where('start_date', '>=', startDate);
              });
              // ... overlap times and daily reservations
              qbDaily.orWhere((qbDailyDaily) => {
                qbDailyDaily.where('recurrence', 'Daily');
              });
              // ... overlap times and weekly reservations on the same day of week
              qbDaily.orWhere((qbDailyWeekly) => {
                qbDailyWeekly.where('recurrence', 'Weekly');
              });
            });
            break;
          // 2 - If saving Weekly reservation:
          case 'Weekly':
            // Count entries with overlap times and...
            qb.where(overlapTimes);
            qb.where((qbWeekly) => {
              // ... overlap times and non recurring reservations on the same day of week
              qbWeekly.where((qbWeeklyNone) => {
                qbWeeklyNone.where('recurrence', 'None');
                qbWeeklyNone.whereRaw('DAYOFWEEK(start_date) = ?', [dayOfWeek]);
                qbWeeklyNone.where('start_date', '>=', startDate);
              });
              // ... overlap times and daily reservations
              qbWeekly.orWhere('recurrence', 'Daily');
              // ... overlap times and  weekly reservations on the same day of week
              qbWeekly.orWhere((qbWeeklyWeekly) => {
                qbWeeklyWeekly.where('recurrence', 'Weekly');
                qbWeeklyWeekly.whereRaw('DAYOFWEEK(start_date) = ?', [dayOfWeek]);
              });
            });
            break;
          default:
        }
      })
      .count();

    return parseInt(overlappingReservations, 10);
  },
}, {
  filterFields: [
    'id',
    'venue_id',
    'user_id',
    'user_profile_id',
    'user_profile_name',
    'name',
    'student_number',
    'identification_number',
    'tin',
    'address',
    'postal_code',
    'city',
    'email',
    'phone_number',
    'recurrence',
    'status',
    'usage_description',
    'file_id',
    'start_date',
    'end_date',
    'created_at',
    'updated_at',
  ],
  sortFields: [
    'id',
    'venue_id',
    'user_id',
    'user_profile_id',
    'user_profile_name',
    'name',
    'student_number',
    'identification_number',
    'tin',
    'address',
    'postal_code',
    'city',
    'email',
    'phone_number',
    'recurrence',
    'status',
    'usage_description',
    'file_id',
    'start_date',
    'end_date',
    'created_at',
    'updated_at',
  ],
  defaultWithRelated: [],
  withRelated: ['venue', 'history'],
});

export default Reservation;
