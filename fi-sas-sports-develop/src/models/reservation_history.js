/* eslint import/no-cycle: 0 */
import Joi from 'joi';

import Bookshelf from '../bookshelf';
import Reservation from './reservation';
import * as Errors from '../errors';
import reservationHistorySchema from '../schemas/reservation-history-schema';

const ReservationHistory = Bookshelf.Model.extend({
  tableName: 'reservation_history',
  hasTimestamps: true,
  hidden: ['id', 'reservation_id', 'updated_at'],

  reservation() {
    return this.belongsTo(Reservation);
  },

  format(attributes) {
    return this.formatModelTimestamps(attributes, ['start_date', 'end_date', 'created_at', 'updated_at']);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('saving', this.validateSave);
  },

  /**
   * Validate save of model instance with Joi schema, and FK relationships.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, reservationHistorySchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }
  },
}, {
  filterFields: [],
  sortFields: [],
  defaultWithRelated: ['reservation'],
});

export default ReservationHistory;
