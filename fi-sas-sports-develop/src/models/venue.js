/* eslint import/no-cycle: 0 */
import Joi from 'joi';

import Bookshelf from '../bookshelf';
import * as Errors from '../errors';
import * as Codes from '../error_codes';
import venueSchema from '../schemas/venue-schema';

import Activity from './activity';
import Reservation from './reservation';

const Venue = Bookshelf.Model.extend({
  tableName: 'venue',
  hasTimestamps: true,

  activities() {
    return this.hasMany(Activity);
  },

  general_activities() {
    return this.hasMany(Activity)
      .query({ where: { type: 'GENERAL ACTIVITY' } });
  },

  regular_trainings() {
    return this.hasMany(Activity)
      .query({ where: { type: 'REGULAR TRAINING' } });
  },

  competitions() {
    return this.hasMany(Activity)
      .query({ where: { type: 'COMPETITION' } });
  },

  reservations() {
    return this.hasMany(Reservation);
  },

  parse(response) {
    return this.parseBooleans(response, ['active']);
  },

  format(attributes) {
    return this.formatBooleans(attributes, ['active']);
  },

  initialize(...args) {
    /* eslint no-underscore-dangle: 0 */
    this.constructor.__super__.initialize.apply(this, args);
    this.on('destroying', this.validateDestroy);
    this.on('saving', this.validateSave);
  },

  async validateDestroy(model, options) {
    const opts = { transacting: options.transacting };

    // Check if there are related activities and prevent delete if so
    await this.load('activities', opts);

    if (this.related('activities').length > 0) {
      throw new Errors.ValidationError({
        code: Codes.InvalidDeleteDueToRelatedEntityExistence,
        message: 'The Venue can not be deleted because it has associated activities',
      });
    }

    // Check if there are related reservations and prevent delete if so
    await this.load('reservations', opts);

    if (this.related('reservations').length > 0) {
      throw new Errors.ValidationError({
        code: Codes.InvalidDeleteDueToRelatedEntityExistence,
        message: 'The Venue can not be deleted because it has associated reservations',
      });
    }
  },

  /**
   * Validate save of model instance with Joi schema, and FK relationships.
   * If validation is not successful, a ValidationError is thrown.
   *
   * @param {Object} model
   * @param {Object} attrs
   * @param {Object} options
   */
  async validateSave(model, attrs, options) {
    const data = Object.assign({}, this.attributes);
    delete data.id;
    const result = Joi.validate(data, venueSchema, options.patch === true ? {} : { presence: 'required' });
    if (result.error !== null) {
      throw new Errors.JoiValidationError(result.error);
    }

    // Validate unique (existing) value of location id
    if (this.hasChanged('location_id') && this.get('location_id')) {
      const used = await Venue.forge().where((q) => {
        q.where('location_id', this.get('location_id'));
        if (!this.isNew()) {
          q.where('id', '!=', this.get('id'));
        }
      }).count();

      if (used) {
        throw new Errors.ValidationError([{
          code: Codes.ValidationDuplicateUniqueValue,
          field: 'location_id',
          message: 'The location is already referenced by another Venue',
        }]);
      }
    }
  },
}, {
  filterFields: [
    'id',
    'name',
    'active',
    'latitude',
    'longitude',
    'address',
    'address_no',
    'city',
    'updated_at',
    'created_at',
  ],
  sortFields: [
    'id',
    'name',
    'active',
    'latitude',
    'longitude',
    'address',
    'address_no',
    'city',
    'updated_at',
    'created_at',
  ],
  defaultWithRelated: [],
  withRelated: ['reservations', 'activities', 'regular_trainings', 'general_activities', 'competitions'],
});

export default Venue;
