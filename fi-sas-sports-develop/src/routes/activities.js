/**
 * @swagger
 * definitions:
 *  TrainingDay:
 *    type: string
 *    enum: &TDAY
 *      - MONDAY
 *      - TUESDAY
 *      - WEDNESDAY
 *      - THURSDAY
 *      - FRIDAY
 *      - SATURDAY
 *      - SUNDAY
 *
 * components:
 *  parameters:
 *    typeFilter:
 *      in: query
 *      name: type
 *      required: false
 *      schema:
 *        type: string
 *        enum:
 *          - GENERAL ACTIVITY
 *          - REGULAR TRAINING
 *          - COMPETITION
 *      description: Filter by Activity type.
 *    activeFilter:
 *      in: query
 *      name: active
 *      required: false
 *      schema:
 *        type: boolean
 *      description: Filter by active status (true/false).
 *    modalityIdFilter:
 *      in: query
 *      name: modality_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by modality ID.
 *    venueIdFilter:
 *      in: query
 *      name: venue_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by venue ID.
 *    eventIdFilter:
 *      in: query
 *      name: event_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by event ID.
 *    nameFilter:
 *      in: query
 *      name: name
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial name.
 *    descriptionFilter:
 *      in: query
 *      name: description
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial description.
 *    trainingDayFilter:
 *      in: query
 *      name: training_day
 *      required: false
 *      schema:
 *        type: string
 *        enum: *TDAY
 *      description: Filter by training day (only relevant for Regular Trainings).
 *    startDateFilter:
 *      in: query
 *      name: start_date
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial start date.
 *    endDateFilter:
 *      in: query
 *      name: end_date
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial end date.
 *    updateDateFilter:
 *      in: query
 *      name: updated_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of last update.
 *    creationDateFilter:
 *      in: query
 *      name: created_at
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial date of creation.
 *    withRelatedActivity:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *       type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, 'modality' and 'venue' are requested. Value of false
 *        disables the load of related entities. Possible values include 'modality' and 'venue'.
 *      example: modality,venue
 *  schemas:
 *    Activity:
 *      description: |
 *        An Activity may be a Regular Training, a Competition or a General Activity - according
 *        to its "type" value.
 *        It has an associated modality, venue and Event (created based on the Activity data).<br>
 *        For its management, several configurations are used ("activity_registration_form_id",
 *        "general_activity_event_category_id", "regular_training_event_category_id"
 *        and "competition_event_category_id"). When one configuration is missing,
 *        the creation of new Activities will not be successful.
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Activity id.
 *          example: 1
 *        type:
 *          type: string
 *          enum:
 *            - GENERAL ACTIVITY
 *            - REGULAR TRAINING
 *            - COMPETITION
 *          nullable: false
 *          description: The type of this Activity, it cannot be updated after creation.
 *          example: GENERAL ACTIVITY
 *        name:
 *          type: string
 *          nullable: false
 *          description: The name of this Activity.
 *          example: Campeonato Universitário de Andebol
 *        description:
 *          type: string
 *          nullable: false
 *          description: The description of this Activity.
 *          example: Campeonato Universitário de Andebol.
 *        venue_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the Venue where this Activity will take place.
 *          example: 1
 *        modality_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the Modality to which this Activity is associated.
 *          example: 1
 *        event_id:
 *          type: integer
 *          nullable: true
 *          readOnly: true
 *          description: The id of the Event associated with this Activity (in MS Calendar).
 *          example: 1
 *        start_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: The starting date of the Activity.
 *          example: 2019-01-01T00:00:00.000Z
 *        end_date:
 *          type: string
 *          format: date
 *          nullable: true
 *          description: |
 *            The ending date of the Activity. For Regular Training Activities it cannot be null.
 *          example: 2019-01-01T00:00:00.000Z
 *        start_time:
 *          type: string
 *          nullable: false
 *          readOnly: true
 *          description: |
 *            The calculated start time of the Activity.
 *            The start time will follow the format HH:MM and is calculated from the start date of
 *            the Activity.
 *          example: "20:00"
 *        end_time:
 *          type: string
 *          nullable: true
 *          readOnly: true
 *          description: |
 *            The calculated end time of the Activity.
 *            The start time will follow the format HH:MM (or null) and is calculated from the end
 *            date of the Activity.
 *        training_day:
 *          type: string
 *          enum: *TDAY
 *          nullable: true
 *          readOnly: true
 *          description: |
 *            The name of the day of the week of the Activity - only used for Regular Training
 *            Activitivies. For other Activities, this property will be null. The training day
 *            is read from the Activity's start date.
 *        active:
 *          type: boolean
 *          nullable: false
 *          description: The Activity active status.
 *          example: true
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Activity.
 *          example: 2019-01-01T00:00:00.000Z
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update on the Activity.
 *          example: 2019-01-01T00:00:00.000Z
 */

import { Router } from 'express';

import Bookshelf from '../bookshelf';
import asyncMiddleware from '../middleware/async';
import { getData } from '../helpers/common';
import { getPaginationMetaData } from '../helpers/metadata';
import * as Errors from '../errors';

import Activity from '../models/activity';

import { createEventFromActivity, editEventFromActivity, deleteEventFromId } from '../helpers/eventManager';

const activityRouter = new Router();

/**
 * @swagger
 * /api/v1/activities:
 *   get:
 *     tags:
 *       - Activities
 *     description: Returns a list of Activities.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/typeFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/descriptionFilter'
 *       - $ref: '#/components/parameters/activeFilter'
 *       - $ref: '#/components/parameters/modalityIdFilter'
 *       - $ref: '#/components/parameters/venueIdFilter'
 *       - $ref: '#/components/parameters/eventIdFilter'
 *       - $ref: '#/components/parameters/startDateFilter'
 *       - $ref: '#/components/parameters/endDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/trainingDayFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/IdFilter'
 *       - $ref: '#/components/parameters/withRelatedActivity'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Activity'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
activityRouter.get('/', (req, res, next) => {
  /*
   * Parse filters to add boolean and partial filter
   */
  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      description: v => `%${v}%`,
      active: v => (v === 'true' ? 1 : 0),
      start_date: v => `%${v}%`,
      end_date: v => `%${v}%`,
      updated_at: v => `%${v}%`,
      created_at: v => `%${v}%`,
    },
  );

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Activity.defaultWithRelated.slice();

  /*
   * Validate filters, sorting and withRelated parameters
   */
  req.parameters.validate(
    Activity.filterFields,
    Activity.sortFields,
    Activity.withRelated,
  );

  Activity.forge().filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
      return res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/activities:
 *    post:
 *      tags:
 *        - Activities
 *      description: >
 *        Creates a new Activity and a new associated Event in MS Calendar.<br>
 *        The Activity can be a Regular Training, a Competition, or a General Activity - according to
 *        its "type" value.<br>
 *        Each Activity will have an associated Event. If the Event creation in MS Calendar is not
 *        successfull, a Bad Request will be returned and the Activity will not be created.<br><br>
 *        The associated Event will be created in MS Calendar to be published from the current date, and will hold the Activity
 *        start and end dates. Events for Regular Training Activities will have Weekly recurrence,
 *        and Events for other Activities will have no recurrence.<br>
 *        The Event will have disabled notifications and will use the associated Venue geographical details.<br>
 *        As display information, the Activity name and description and its Modality file id will be used.<br>
 *        Both interests and registrations will be enabled in the Event.
 *        Registrations will need approval and will use as registration form
 *        id the configuration value of "activity_registration_form_id".<br>
 *        The Event Category will be set according to the configuration value of
 *        "general_activity_event_category_id", "regular_training_event_category_id"
 *        or "competition_event_category_id", according to the type of the Activity.<br>
 *        The Event will be created using the specified values and can be further configured in MS
 *        Calendar.<br><br>
 *        <b>Note:</b> An Event is always created in a Pending status. An approval is needed before
 *        the Event is publicly available in MS Calendar.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Activity data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Activity'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedActivity'
 *      responses:
 *        '201':
 *         description: Created Activity object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Activity'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
activityRouter.post('/', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, ['type', 'modality_id', 'name', 'description', 'venue_id', 'active', 'start_date', 'end_date'], false);

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Activity.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Activity.withRelated);

  const assignEvent = (item, t) => item.refresh({ transacting: t, withRelated: ['venue', 'modality'] })
    .tap(i => createEventFromActivity(req, i, t)
      .then(eventId => i.save({ event_id: eventId }, { method: 'update', patch: true, transacting: t })));

  return Bookshelf.transaction(t => Activity.forge()
    .save(data, { method: 'insert', transacting: t })
    .tap(item => assignEvent(item, t)))
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then((item) => {
      res.formatter.created(item.toJSON());
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/activities/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Activity.
 *    get:
 *      tags:
 *        - Activities
 *      description: >
 *        Returns an Activity object.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedActivity'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Activity'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
activityRouter.get('/:id', (req, res, next) => {
  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Activity.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Activity.withRelated);

  Activity.forge()
    .where('id', req.params.id)
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Activity not found.'); })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/activities/{id}:
 *    put:
 *      tags:
 *        - Activities
 *      description: >
 *        Updates an Activity object.
 *        If any date related value is updated, the associated Event will also be updated.<br>
 *        Update of the value for the "type" property is not allowed.<br><br>
 *        <b>Note 1:</b> Changes to any other values are not reflected in the Event.<br>
 *        <b>Note 2:</b> If the Event is updated, it will automatically be set into a Pending status. An
 *        approval is needed before the Event is publicly available in MS Calendar.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Activity updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Activity'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedActivity'
 *      responses:
 *        '200':
 *          description: Updated Activity object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Activity'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
activityRouter.put('/:id', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, ['type', 'modality_id', 'name', 'description', 'venue_id', 'active', 'start_date', 'end_date'], false);

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Activity.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Activity.withRelated);

  const editEvent = (item, t) => {
    if (item.updated_dates && item.get('event_id')) {
      return editEventFromActivity(req, item, t);
    }
    return true;
  };

  return Bookshelf.transaction(t => Activity.forge({ id: req.params.id })
    .fetch({ require: true })
    .then(item => item.save(data, { method: 'update', patch: false, transacting: t }))
    .tap(item => editEvent(item, t)))
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch(Bookshelf.NoRowsUpdatedError, () => {
      throw new Errors.NotFoundError('Activity not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/activities/{id}:
 *    patch:
 *      tags:
 *        - Activities
 *      description: >
 *        Partially updates an Activity object.
 *        If any date related value is updated, the associated Event will also be updated.<br>
 *        Update of the value for the "type" property is not allowed.<br><br>
 *        <b>Note 1:</b> Changes to any other values are not reflected in the Event.<br>
 *        <b>Note 2:</b> If the Event is updated, it will automatically be set into a Pending status. An
 *        approval is needed before the Event is publicly available in MS Calendar.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Activity data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Activity'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedActivity'
 *      responses:
 *        '200':
 *          description: Updated Activity object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Activity'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
activityRouter.patch('/:id', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, ['type', 'modality_id', 'name', 'description', 'venue_id', 'active', 'start_date', 'end_date'], true);

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Activity.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Activity.withRelated);

  const editEvent = (item, t) => {
    if (item.updated_dates && item.get('event_id')) {
      return editEventFromActivity(req, item, t);
    }
    return true;
  };

  return Bookshelf.transaction(t => Activity.forge({ id: req.params.id })
    .fetch({ require: true })
    .then(item => item.save(data, { method: 'update', patch: true, transacting: t }))
    .tap(item => editEvent(item, t)))
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then((item) => {
      res.formatter.ok(item.toJSON());
    })
    .catch(Bookshelf.NoRowsUpdatedError, () => {
      throw new Errors.NotFoundError('Activity not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/activities/{id}:
 *    delete:
 *      tags:
 *        - Activities
 *      description: |
 *        Deletes an Activity object and the associated Event (if needed). If the Event
 *        cannot be deleted, a BadRequest is returned.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
activityRouter.delete('/:id', (req, res, next) => {
  let eventId = false;
  Bookshelf.transaction(t => Activity.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then((item) => {
      eventId = item.get('event_id');
      return item.destroy({ transacting: t });
    })
    .tap(() => {
      if (eventId) {
        return deleteEventFromId(req, eventId);
      }
      return true;
    }))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Activity not found.'); })
    .catch(next);
});

export default activityRouter;
