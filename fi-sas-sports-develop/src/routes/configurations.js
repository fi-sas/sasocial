/**
 * @swagger
 * components:
 *  parameters:
 *    keyParameter:
 *      in: query
 *      name: key
 *      required: false
 *      schema:
 *        type: string
 *        enum: [regular_training_event_category_id,competition_event_category_id,general_activity_event_category_id,activity_registration_form_id]
 *      description: Filter by Key.
 *  schemas:
 *    Configuration:
 *      description: |
 *        Represents a configuration setting.<br>
 *        A setting is defined by its key and used in different contexts. For more information, check the documentation of each endpoint.
 *      properties:
 *        key:
 *          type: string
 *          enum: [regular_training_event_category_id,competition_event_category_id,general_activity_event_category_id,activity_registration_form_id]
 *          nullable: false
 *          readOnly: true
 *          description: The Configuration key.
 *        value:
 *          anyOf:
 *            - type: string
 *            - type: number
 *            - type: integer
 *            - type: boolean
 *            - type: array
 *              items: {}
 *          nullable: false
 *          description: The Configuration value.
 *          example: admin@mail.com
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update.
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation.
 */

import { Router } from 'express';

import { getPaginationMetaData } from '../helpers/metadata';

import Configuration from '../models/configuration';

const configurationsRouter = new Router();

/**
 * @swagger
 * /api/v1/configurations:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *      Returns the list of Configurations.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/keyParameter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configurationsRouter.get('/', (req, res, next) => {
  /*
   * Parse filters from boolean to integer and add partial filter behaviour
   */
  req.parameters.parseFilters(
    {
      created_at: v => `%${v}%`,
      updated_at: v => `%${v}%`,
    },
  );

  /*
   * Validate filters, sorting and withRelated parameters
   */
  req.parameters.validate(Configuration.filterFields, Configuration.sortFields, []);

  Configuration.forge()
    .filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination)
        ? getPaginationMetaData(collection.pagination, req)
        : {};
      res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 * /api/v1/configurations/regular_training_event_category_id:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *        Returns the ID of the configured Category for Events of Regular Trainings Activities.<br>
 *        This configuration will be used when an Activity of type REGULAR TRAINING is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *     produces:
 *       - application/json
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    allOf:
 *                    - $ref: '#/components/schemas/Configuration'
 *                    - type: object
 *                      properties:
 *                        value:
 *                          type: integer
 *                          example: 1
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '404':
 *        $ref: '#/components/responses/NotFound'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configurationsRouter.get('/regular_training_event_category_id', (req, res, next) => Configuration.getConfigInstance('regular_training_event_category_id', true)
  .then(conf => res.formatter.ok(conf.toJSON()))
  .catch(next));

/**
 * @swagger
 *  /api/v1/configurations/regular_training_event_category_id:
 *    post:
 *      tags:
 *        - Configurations
 *      description: >
 *        Sets the Category ID for Events of Regular Trainings Activities.<br>
 *        This configuration will be used when an Activity of type REGULAR TRAINING is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Configuration Value data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *                - $ref: '#/components/schemas/Configuration'
 *                - type: object
 *                  properties:
 *                    value:
 *                      type: integer
 *                      example: 1
 *      responses:
 *        '201':
 *         description: Created Configuration object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
configurationsRouter.post('/regular_training_event_category_id', (req, res, next) => {
  const { value } = req.body;

  return Configuration.getConfigInstance('regular_training_event_category_id')
    .then(c => c.save({ value }))
    .then(c => res.formatter.created(c.toJSON({ omitPivot: true })))
    .catch(next);
});

/**
 * @swagger
 * /api/v1/configurations/competition_event_category_id:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *        Returns the ID of the configured Category for Events of Competition Activities.<br>
 *        This configuration will be used when an Activity of type COMPETITION is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *     produces:
 *       - application/json
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    allOf:
 *                    - $ref: '#/components/schemas/Configuration'
 *                    - type: object
 *                      properties:
 *                        value:
 *                          type: integer
 *                          example: 1
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '404':
 *        $ref: '#/components/responses/NotFound'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configurationsRouter.get('/competition_event_category_id', (req, res, next) => Configuration.getConfigInstance('competition_event_category_id', true)
  .then(conf => res.formatter.ok(conf.toJSON()))
  .catch(next));

/**
 * @swagger
 *  /api/v1/configurations/competition_event_category_id:
 *    post:
 *      tags:
 *        - Configurations
 *      description: >
 *        Sets the Category ID for Events of Competition Activities.<br>
 *        This configuration will be used when an Activity of type COMPETITION is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Configuration Value data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *                - $ref: '#/components/schemas/Configuration'
 *                - type: object
 *                  properties:
 *                    value:
 *                      type: integer
 *                      example: 1
 *      responses:
 *        '201':
 *         description: Created Configuration object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
configurationsRouter.post('/competition_event_category_id', (req, res, next) => {
  const { value } = req.body;

  return Configuration.getConfigInstance('competition_event_category_id')
    .then(c => c.save({ value }))
    .then(c => res.formatter.created(c.toJSON({ omitPivot: true })))
    .catch(next);
});

/**
 * @swagger
 * /api/v1/configurations/general_activity_event_category_id:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *        Returns the ID of the configured Category for Events of General Activities.<br>
 *        This configuration will be used when an Activity of type GENERAL ACTIVITY is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *     produces:
 *       - application/json
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    allOf:
 *                    - $ref: '#/components/schemas/Configuration'
 *                    - type: object
 *                      properties:
 *                        value:
 *                          type: integer
 *                          example: 1
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '404':
 *        $ref: '#/components/responses/NotFound'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configurationsRouter.get('/general_activity_event_category_id', (req, res, next) => Configuration.getConfigInstance('general_activity_event_category_id', true)
  .then(conf => res.formatter.ok(conf.toJSON()))
  .catch(next));

/**
 * @swagger
 *  /api/v1/configurations/general_activity_event_category_id:
 *    post:
 *      tags:
 *        - Configurations
 *      description: >
 *        Sets the Category ID for Events of General Activities.<br>
 *        This configuration will be used when an Activity of type GENERAL ACTIVITY is created.
 *        If no value is configured, the Event creation cannot be completed and, thus,
 *        so cannot the Activity creation.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Configuration Value data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *                - $ref: '#/components/schemas/Configuration'
 *                - type: object
 *                  properties:
 *                    value:
 *                      type: integer
 *                      example: 1
 *      responses:
 *        '201':
 *         description: Created Configuration object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
configurationsRouter.post('/general_activity_event_category_id', (req, res, next) => {
  const { value } = req.body;

  return Configuration.getConfigInstance('general_activity_event_category_id')
    .then(c => c.save({ value }))
    .then(c => res.formatter.created(c.toJSON({ omitPivot: true })))
    .catch(next);
});

/**
 * @swagger
 * /api/v1/configurations/activity_registration_form_id:
 *   get:
 *     tags:
 *       - Configurations
 *     description: >
 *        Returns the ID of the configured Form for Registration on Events of Activities.
 *        This configuration will be used when an Activity is created.
 *        If no value is configured, the Event creation cannot
 *        be completed and, thus, so cannot the Activity creation.
 *     produces:
 *       - application/json
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    allOf:
 *                    - $ref: '#/components/schemas/Configuration'
 *                    - type: object
 *                      properties:
 *                        value:
 *                          type: integer
 *                          example: 1
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '404':
 *        $ref: '#/components/responses/NotFound'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
configurationsRouter.get('/activity_registration_form_id', (req, res, next) => Configuration.getConfigInstance('activity_registration_form_id', true)
  .then(conf => res.formatter.ok(conf.toJSON()))
  .catch(next));

/**
 * @swagger
 *  /api/v1/configurations/activity_registration_form_id:
 *    post:
 *      tags:
 *        - Configurations
 *      description: >
 *        Sets the Registration Form ID for Events of Activities.
 *        This configuration will be used when an Activity is created.
 *        If no value is configured, the Event creation cannot
 *        be completed and, thus, so cannot the Activity creation.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Configuration Value data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *                - $ref: '#/components/schemas/Configuration'
 *                - type: object
 *                  properties:
 *                    value:
 *                      type: integer
 *                      example: 1
 *      responses:
 *        '201':
 *         description: Created Configuration object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Configuration'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
configurationsRouter.post('/activity_registration_form_id', (req, res, next) => {
  const { value } = req.body;

  return Configuration.getConfigInstance('activity_registration_form_id')
    .then(c => c.save({ value }))
    .then(c => res.formatter.created(c.toJSON({ omitPivot: true })))
    .catch(next);
});

export default configurationsRouter;
