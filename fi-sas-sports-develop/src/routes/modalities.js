/**
 * @swagger
 *
 * components:
 *  parameters:
 *    fileIdFilter:
 *      in: query
 *      name: file_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by file ID.
 *    withRelatedModality:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *       type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, no entity is requested. Value of false disables the
 *        load of related entities. Possible values include 'file', 'activities',
 *        'regular_trainings' (activities of Regular Training type),
 *        'general_activities' (activities of General Activity type) and
 *        'competitions' (activities of Competition type).
 *  schemas:
 *    Modality:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Modality id.
 *          example: 1
 *        name:
 *          type: string
 *          nullable: false
 *          description: The Modality name.
 *          example: Andebol
 *        file_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the Media File associated with the Modality (ID from MS Media).
 *          example: 1
 *        active:
 *          type: boolean
 *          nullable: false
 *          description: The Modality active status.
 *          example: true
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Modality.
 *          example: 2019-01-01T00:00:00.000Z
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update on the Modality.
 *          example: 2019-01-01T00:00:00.000Z
 */

import { Router } from 'express';
import { cloneDeep } from 'lodash';

import Bookshelf from '../bookshelf';
import Modality from '../models/modality';
import { getPaginationMetaData } from '../helpers/metadata';
import { loadRelated } from '../helpers/common';
import asyncMiddleware from '../middleware/async';
import * as Errors from '../errors';

const modalityRouter = new Router();

/**
 * @swagger
 * /api/v1/modalities:
 *   get:
 *     tags:
 *       - Modalities
 *     description: Returns a list of Modalities.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/fileIdFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/activeFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/IdFilter'
 *       - $ref: '#/components/parameters/withRelatedModality'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Modality'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
modalityRouter.get('/', (req, res, next) => {
  /*
   * Parse filters to add boolean and partial filter
   */
  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      active: v => (v === 'true' ? 1 : 0),
      updated_at: v => `%${v}%`,
      created_at: v => `%${v}%`,
    },
  );

  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Modality.defaultWithRelated.slice();

  // Validate filters, sorting and withRelated parameters
  req.parameters.validate(Modality.filterFields, Modality.sortFields, Modality.withRelated);

  // Store original withRelated
  const originalWithRelated = cloneDeep(req.parameters.withRelated);

  Modality.forge().filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
      loadRelated(req, data, originalWithRelated, Modality.externalWithRelated)
        .then(newData => res.formatter.ok(newData, link));
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/modalities:
 *    post:
 *      tags:
 *        - Modalities
 *      description: >
 *        Creates new Modality object.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Modality data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Modality'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedModality'
 *      responses:
 *        '201':
 *         description: Created Modality object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Modality'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
modalityRouter.post('/', asyncMiddleware(async (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Modality.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Modality.withRelated);

  // Store original withRelated
  const originalWithRelated = cloneDeep(req.parameters.withRelated);

  return Modality.forge()
    .save(req.body, { method: 'insert' })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => loadRelated(req, item.toJSON(), originalWithRelated, Modality.externalWithRelated)
      .then(newItem => res.formatter.created(newItem)))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/modalities/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Modality.
 *    get:
 *      tags:
 *        - Modalities
 *      description: >
 *        Returns a Modality object.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedModality'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Modality'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
modalityRouter.get('/:id', (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Modality.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Modality.withRelated);

  // Store original withRelated
  const originalWithRelated = cloneDeep(req.parameters.withRelated);

  Modality.forge()
    .where('id', req.params.id)
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then(item => loadRelated(req, item.toJSON(), originalWithRelated, Modality.externalWithRelated)
      .then(newItem => res.formatter.ok(newItem)))
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Modality not found.'); })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/modalities/{id}:
 *    put:
 *      tags:
 *        - Modalities
 *      description: >
 *        Update a Modality object.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Modality updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Modality'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedModality'
 *      responses:
 *        '200':
 *          description: Updated Modality object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Modality'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
modalityRouter.put('/:id', asyncMiddleware(async (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Modality.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Modality.withRelated);

  // Store original withRelated
  const originalWithRelated = cloneDeep(req.parameters.withRelated);

  return Modality.forge({ id: req.params.id })
    .save(req.body, { method: 'update', patch: false })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => loadRelated(req, item.toJSON(), originalWithRelated, Modality.externalWithRelated)
      .then(newItem => res.formatter.ok(newItem)))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/modalities/{id}:
 *    patch:
 *      tags:
 *        - Modalities
 *      description: >
 *        Partially update a Modality object.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Modality data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Modality'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedModality'
 *      responses:
 *        '200':
 *          description: Updated Modality object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Modality'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
modalityRouter.patch('/:id', asyncMiddleware(async (req, res, next) => {
  // Store original withRelated
  const originalWithRelated = cloneDeep(req.parameters.withRelated);

  return Modality.forge({ id: req.params.id })
    .save(req.body, { method: 'update', patch: true })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => loadRelated(req, item.toJSON(), originalWithRelated, Modality.externalWithRelated)
      .then(newItem => res.formatter.ok(newItem)))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/modalities/{id}:
 *    delete:
 *      tags:
 *        - Modalities
 *      description: |
 *        Deletes a Modality object. If it has associated trainings, activities or competities
 *        a BadRequest is returned.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
modalityRouter.delete('/:id', (req, res, next) => {
  Bookshelf.transaction(t => Modality.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t }))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Modality not found.'); })
    .catch(next));
});

export default modalityRouter;
