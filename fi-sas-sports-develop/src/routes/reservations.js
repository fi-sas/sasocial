/**
 * @swagger
 *
 * components:
 *  parameters:
 *    userFilter:
 *      in: query
 *      name: user_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by User ID (only available for Backoffice).
 *    userProfileIdFilter:
 *      in: query
 *      name: user_profile_id
 *      required: false
 *      schema:
 *        type: integer
 *      description: Filter by User Profile ID.
 *    userProfileNameFilter:
 *      in: query
 *      name: user_profile_name
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial User Profile name.
 *    studentNumberFilter:
 *      in: query
 *      name: student_number
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial student number.
 *    idNumberFilter:
 *      in: query
 *      name: identification_number
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial identification number.
 *    tinFilter:
 *      in: query
 *      name: tin
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial TIN.
 *    addressFilter:
 *      in: query
 *      name: address
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial address.
 *    postalCodeFilter:
 *      in: query
 *      name: postal_code
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial postal code.
 *    cityFilter:
 *      in: query
 *      name: city
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial city.
 *    emailFilter:
 *      in: query
 *      name: email
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial email.
 *    phoneFilter:
 *      in: query
 *      name: phone_number
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial phone number.
 *    usageFilter:
 *      in: query
 *      name: usage_description
 *      required: false
 *      schema:
 *        type: string
 *      description: Filter by partial usage description.
 *    recurrenceFilter:
 *      in: query
 *      name: recurrence
 *      required: false
 *      schema:
 *        type: string
 *        enum: [None, Daily, Weekly]
 *      description: Filter by Reservation recurrence.
 *    reservationStatusFilter:
 *      in: query
 *      name: status
 *      required: false
 *      schema:
 *        type: string
 *        enum: [Pending, Approved, Rejected, Cancelled]
 *      description: Filter by Reservation status.
 *    withRelatedReservation:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *       type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, no entity is requested. Value of false disables the
 *        load of related entities. Possible values include 'history' and 'venue'.
 *      example:
 *  schemas:
 *    Reservation:
 *      description: |
 *        A Reservation of a specific space in a specific date.<br>
 *        Reservations may be recurring (example: Weekly reservation) or non-recurring (with None
 *        recurrence).<br>
 *        Reservations are always created in a Pending status, waiting to be
 *        approved or rejected by specific command. An edition of a Reservation also
 *        automatically assigns it to a Pending status.
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Reservation id.
 *          example: 1
 *        venue_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the Venue where this Reservation is being made.
 *          example: 1
 *        user_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the User who belonging tois Reservation.
 *          example: 1
 *        user_profile_id:
 *          type: string
 *          nullable: false
 *          description: The id of the profile of the user making this Reservation.
 *          example: 1
 *        user_profile_name:
 *          type: string
 *          nullable: false
 *          description: The name of the profile of the user making this Reservation.
 *          example: Aluno
 *        name:
 *          type: string
 *          nullable: false
 *          description: The name of the User creating the Reservation.
 *          example: Tiago Brandão Rodrigues
 *        student_number:
 *          type: string
 *          nullable: false
 *          description: The student number of the user creating the Reservation.
 *          example: "a22222"
 *        identification_number:
 *          type: string
 *          nullable: false
 *          description: |
 *            The official identification number (CC in Portugal) of the user creating
 *            the Reservation.
 *          example: "999999990"
 *        tin:
 *          type: string
 *          nullable: false
 *          description: |
 *            The tax identification number (NIF in Portugal) of the user creating
 *            the Reservation.
 *          example: "999999990"
 *        address:
 *          type: string
 *          nullable: false
 *          description: The address of the user creating the Reservation.
 *          example: Largo do Cais 120
 *        postal_code:
 *          type: string
 *          nullable: false
 *          description: The postal code of the user creating the Reservation.
 *          example: 1000-000
 *        city:
 *          type: string
 *          nullable: false
 *          description: The city of the user creating the Reservation.
 *          example: Lisboa
 *        email:
 *          type: string
 *          nullable: false
 *          description: The contact email of the user creating the Reservation.
 *          example: a22222@alunos.ipvc.pt
 *        phone_number:
 *          type: string
 *          nullable: false
 *          description: |
 *            The phone number of the user creating the Reservation (the country calling
 *            code is required, being formatted as +351 or 00351 (taking the PT country
 *            calling code as example).
 *          example: "+351919999999"
 *        start_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: The start date of this Reservation.
 *          example: 2019-01-01T20:00:00.000Z
 *        end_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: The end date of this Reservation.
 *          example: 2019-01-01T21:00:00.000Z
 *        recurrence:
 *          type: string
 *          enu: [None, Daily, Weekly]
 *          nullable: false
 *          description: |
 *            Recurrence of this Reservation (applicable untill it is cancelled).
 *          example: None
 *        usage_description:
 *          type: string
 *          nullable: false
 *          description: |
 *            The Reservation descrption (what the Venue will be used for during
 *            the time of the Reservation).
 *          example: Reserva para jogo de furebol entre amigos.
 *        file_id:
 *          type: integer
 *          nullable: true
 *          description: The id of an attachment file for this Reservation (optional).
 *          example: 1
 *        status:
 *          type: string
 *          enum: [Pending, Approved, Rejected, Cancelled]
 *          readOnly: true
 *          description: The current status of this Reservation.
 *          example: Pending
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Reservation.
 *          example: 2019-01-01T00:00:00.000Z
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update on the Reservation.
 *          example: 2019-01-01T00:00:00.000Z
 *    ReservationHistory:
 *      description: |
 *        A Reservation history record.
 *      type: object
 *      properties:
 *        recurrence:
 *          type: string
 *          enu: [None, Daily, Weekly]
 *          nullable: false
 *          description: |
 *            Recurrence of the Reservation History record.
 *        start_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: The start date of the Reservation History record.
 *          example: 2019-01-01T20:00:00.000Z
 *        end_date:
 *          type: string
 *          format: date
 *          nullable: false
 *          description: The end date of the Reservation History record.
 *          example: 2019-01-01T21:00:00.000Z
 *        status:
 *          type: string
 *          enum: [Pending, Approved, Rejected, Cancelled]
 *          readOnly: true
 *          description: The current status of the Reservation History record.
 *          example: Pending
 *        user_id:
 *          type: integer
 *          nullable: false
 *          description: The id of the User who authored the Reservation History record.
 *          example: 1
 *        action:
 *          type: string
 *          nullable: false
 *          description: |
 *            The action associated with the Reservation History record. Example: create, edit, approve, cancel, reject.
 *          example: edit.
 *        notes:
 *          type: string
 *          nullable: false
 *          description: |
 *            The optional notes associated with the Reservation History record.
 *          example: Cancelado por falta de comparências.
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Reservation.
 *          example: 2019-01-01T00:00:00.000Z
 */

import { Router } from 'express';

import Bookshelf from '../bookshelf';
import { getPaginationMetaData } from '../helpers/metadata';
import { getData } from '../helpers/common';
import asyncMiddleware from '../middleware/async';
import backoffice from '../middleware/backoffice';
import * as Errors from '../errors';
import * as Codes from '../error_codes';

import Reservation from '../models/reservation';

const reservationRouter = new Router();

/**
 * @swagger
 * /api/v1/reservations:
 *   get:
 *     tags:
 *       - Reservations
 *     description: |
 *       Returns a list of Reservations. If not Backoffice, only
 *       Reservations belonging to the current user are accessible.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/userFilter'
 *       - $ref: '#/components/parameters/venueIdFilter'
 *       - $ref: '#/components/parameters/userProfileIdFilter'
 *       - $ref: '#/components/parameters/userProfileNameFilter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/studentNumberFilter'
 *       - $ref: '#/components/parameters/idNumberFilter'
 *       - $ref: '#/components/parameters/tinFilter'
 *       - $ref: '#/components/parameters/addressFilter'
 *       - $ref: '#/components/parameters/cityFilter'
 *       - $ref: '#/components/parameters/postalCodeFilter'
 *       - $ref: '#/components/parameters/emailFilter'
 *       - $ref: '#/components/parameters/phoneFilter'
 *       - $ref: '#/components/parameters/fileIdFilter'
 *       - $ref: '#/components/parameters/usageFilter'
 *       - $ref: '#/components/parameters/startDateFilter'
 *       - $ref: '#/components/parameters/endDateFilter'
 *       - $ref: '#/components/parameters/recurrenceFilter'
 *       - $ref: '#/components/parameters/reservationStatusFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/IdFilter'
 *       - $ref: '#/components/parameters/withRelatedReservation'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
reservationRouter.get('/', backoffice, (req, res, next) => {
  /*
   * Parse filters to add boolean and partial filter
   */
  req.parameters.parseFilters(
    {
      user_profile_name: v => `%${v}%`,
      name: v => `%${v}%`,
      student_number: v => `%${v}%`,
      identification_number: v => `%${v}%`,
      tin: v => `%${v}%`,
      address: v => `%${v}%`,
      postal_code: v => `%${v}%`,
      city: v => `%${v}%`,
      email: v => `%${v}%`,
      phone_number: v => `%${v}%`,
      usage_description: v => `%${v}%`,
      start_date: v => `%${v}%`,
      end_date: v => `%${v}%`,
      updated_at: v => `%${v}%`,
      created_at: v => `%${v}%`,
    },
  );

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Reservation.defaultWithRelated.slice();

  /*
   * Validate filters, sorting and withRelated parameters
   */
  req.parameters.validate(
    Reservation.filterFields,
    Reservation.sortFields,
    Reservation.withRelated,
  );

  if (!req.backoffice) {
    req.parameters.addFilter('user_id', req.authInfo.user.id);
  }

  Reservation.forge().filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
      return res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/reservations:
 *    post:
 *      tags:
 *        - Reservations
 *      description: >
 *        Creates a new Reservation.<br>
 *        A Reservation is created in a Pending status, waiting to be approved into an Approved
 *        status, or rejected into a Rejected status by an authorized user. <br>
 *        A Reservation can also be cancelled via backoffice or by specific command of the User
 *        that belonging to the Reservation. <br>
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Reservation data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Reservation'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedReservation'
 *      responses:
 *        '201':
 *         description: Created Reservation object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
reservationRouter.post('/', asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, [
    'venue_id',
    'user_id',
    'user_profile_id',
    'user_profile_name',
    'name',
    'student_number',
    'identification_number',
    'tin',
    'address',
    'postal_code',
    'city',
    'email',
    'phone_number',
    'recurrence',
    'usage_description',
    'file_id',
    'start_date',
    'end_date',
  ], false);

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Reservation.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Reservation.withRelated);

  const reservation = Reservation.forge();
  reservation.historyNotes = 'New reservation';
  reservation.historyAction = 'create';
  reservation.historyUser = req.authInfo.user.id;

  return Bookshelf.transaction(t => reservation
    .save(data, { method: 'insert', transacting: t }))
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then((item) => {
      res.formatter.created(item.toJSON());
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Reservation.
 *    get:
 *      tags:
 *        - Reservations
 *      description: >
 *        Returns the Reservation with the provided ID. If not Backoffice, only
 *        Reservations belonging to the current user are accessible.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedReservation'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
reservationRouter.get('/:id', backoffice, (req, res, next) => {
  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Reservation.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Reservation.withRelated);

  const opts = !req.backoffice
    ? { id: req.params.id, user_id: req.authInfo.user.id }
    : { id: req.params.id };

  Reservation.forge(opts)
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then((item) => {
      res.formatter.ok(item.toJSON({ omitPivot: true }));
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/reservations/{id}:
 *    put:
 *      tags:
 *        - Reservations
 *      description: >
 *        Update the Reservation with the provided ID. If not Backoffice, only
 *        Reservations belonging to the current user are accessible.<br>
 *        After an edition, the Reservation is updated into a Pending status, waiting to be approved
 *        into an Approved status, or rejected into a Rejected status by an authorized user.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Reservation updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Reservation'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedReservation'
 *      responses:
 *        '200':
 *          description: Updated Reservation object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
reservationRouter.put('/:id', backoffice, asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, [
    'venue_id',
    'user_id',
    'user_profile_id',
    'user_profile_name',
    'name',
    'student_number',
    'identification_number',
    'tin',
    'address',
    'postal_code',
    'city',
    'email',
    'phone_number',
    'recurrence',
    'usage_description',
    'file_id',
    'start_date',
    'end_date',
  ], false);

  // Set pending status
  data.status = 'Pending';

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Reservation.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Reservation.withRelated);

  const opts = !req.backoffice
    ? { id: req.params.id, user_id: req.authInfo.user.id }
    : { id: req.params.id };

  Reservation.forge(opts)
    .fetch({ require: true })
    .then((item) => {
      const reservation = item;
      reservation.historyNotes = 'Reservation edition';
      reservation.historyAction = 'edit';
      reservation.historyUser = req.authInfo.user.id;
      return reservation.save(data, { method: 'update' });
    })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}:
 *    patch:
 *      tags:
 *        - Reservations
 *      description: >
 *        Partially updates the Reservation with the provided ID. If not Backoffice, only
 *        Reservations belonging to the current user are accessible.<br>
 *        After an edition, the Reservation is updated into a Pending status, waiting to be approved
 *        into an Approved status, or rejected into a Rejected status by an authorized user.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Reservation data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Reservation'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedReservation'
 *      responses:
 *        '200':
 *          description: Updated Reservation object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
reservationRouter.patch('/:id', backoffice, asyncMiddleware(async (req, res, next) => {
  const data = getData(req.body, [
    'venue_id',
    'user_id',
    'user_profile_id',
    'user_profile_name',
    'name',
    'student_number',
    'identification_number',
    'tin',
    'address',
    'postal_code',
    'city',
    'email',
    'phone_number',
    'recurrence',
    'usage_description',
    'file_id',
    'start_date',
    'end_date',
  ], true);

  // Set pending status
  data.status = 'Pending';

  /*
   * Set default withRelated parameter
   */
  req.parameters.withRelated = req.parameters.withRelated
    || Reservation.defaultWithRelated.slice();

  /*
   * Validate withRelated parameters
   */
  req.parameters.validate(null, null, Reservation.withRelated);

  const opts = !req.backoffice
    ? { id: req.params.id, user_id: req.authInfo.user.id }
    : { id: req.params.id };

  Reservation.forge(opts)
    .fetch({ require: true })
    .then((item) => {
      const reservation = item;
      reservation.historyNotes = 'Reservation edition';
      reservation.historyAction = 'edit';
      reservation.historyUser = req.authInfo.user.id;
      return reservation.save(data, { method: 'update' });
    })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    })
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}/cancel:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Reservation to cancel.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Reservations
 *      description: |
 *        Cancels the Reservation with the provided ID. If not Backoffice, only
 *        Reservations belonging to the current user are accessible.<br>
 *        The Reservation must be in Pending or Approved status, otherwise, a Bad Request
 *        is returned.
 *      requestBody:
 *        description: |
 *          The provided notes will be used for tracking the history of the Reservation (optional).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                notes:
 *                  type: string
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
reservationRouter.post('/:id/cancel', backoffice, asyncMiddleware(async (req, res, next) => {
  const opts = !req.backoffice
    ? { id: req.params.id, user_id: req.authInfo.user.id }
    : { id: req.params.id };

  /**
   * Get Reservation and validate cancelation
   */
  const reservation = await Reservation.forge(opts)
    .fetch({ require: true })
    .tap((item) => {
      if (!(item.get('status') === 'Pending' || item.get('status') === 'Approved')) {
        throw new Errors.ValidationError([
          {
            code: Codes.InvalidOperation,
            message: 'Reservation not in a Pending or Approved status, and so it cannot be cancelled',
            field: 'id',
            index: 0,
          },
        ]);
      }
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    });

  /**
   * Cancel and return updated Reservation
   */
  reservation.historyNotes = (req.body && req.body.notes) ? req.body.notes : '';
  reservation.historyAction = 'cancel';
  reservation.historyUser = req.authInfo.user.id;
  return reservation.save({ status: 'Cancelled' }, {
    method: 'update', patch: true,
  })
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}/approve:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Reservation to approve.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Reservations
 *      description: |
 *        The Reservation may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation
 *          - *Approved*: approved reservation after creation
 *          - *Rejected*: rejected reservation after creation
 *          - *Cancelled*: cancelled reservation (previously in Pending or Approved status)
 *
 *        ------
 *
 *        This endpoint approves the specified Reservation, setting it to an Approved status.
 *        If the Reservation is not in a Pending status, a Bad Request is returned.
 *      requestBody:
 *        description: |
 *          The provided notes will be used for tracking the history of the Reservation (optional).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                notes:
 *                  type: string
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
reservationRouter.post('/:id/approve', asyncMiddleware(async (req, res, next) => {
  /**
   * Get Reservation
   */
  const reservation = await Reservation.forge({ id: req.params.id })
    .fetch({ require: true })
    .tap((item) => {
      if (item.get('status') !== 'Pending') {
        throw new Errors.ValidationError([
          {
            code: Codes.InvalidOperation,
            message: 'Reservation is not in a Pending status and so it cannot be approved',
            field: 'id',
            index: 0,
          },
        ]);
      }
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    });

  /**
   * Approve Reservation and returned updated version
   */
  reservation.historyNotes = (req.body && req.body.notes) ? req.body.notes : '';
  reservation.historyAction = 'approve';
  reservation.historyUser = req.authInfo.user.id;
  return reservation.save({ status: 'Approved' },
    {
      method: 'update', patch: true,
    })
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}/reject:
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of the Reservation to reject.
 *        example: 1
 *        schema:
 *          type: integer
 *    post:
 *      tags:
 *        - Reservations
 *      description: |
 *        The Reservation may be in one of the following statuses:
 *          - *Pending*: waiting for approval - after its creation or edition
 *          - *Approved*: approved reservation after credition or edition
 *          - *Rejected*: rejected reservation after credition or edition
 *          - *Cancelled*: cancelled reservation (previously in Pending or Approved status)
 *
 *        ------
 *
 *        This endpoint rejects the specified Reservation, setting it to a Rejected status. The
 *        Reservation must be in Pending status, otherwise, a Bad Request is returned.
 *
 *      requestBody:
 *        description: |
 *          The provided notes will be used for tracking the history of the Reservation (optional).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                notes:
 *                  type: string
 *      produces:
 *        - application/json
 *      responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Reservation'
 *       '400':
 *         $ref: '#/components/responses/BadRequest'
 *       '401':
 *         $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         $ref: '#/components/responses/Forbidden'
 *       '404':
 *         $ref: '#/components/responses/NotFound'
 *       '500':
 *         $ref: '#/components/responses/ServerError'
 */
reservationRouter.post('/:id/reject', asyncMiddleware(async (req, res, next) => {
  /**
   * Get Reservation and validate rejection
   */
  const reservation = await Reservation.forge({ id: req.params.id })
    .fetch({ require: true })
    .tap((item) => {
      if (item.get('status') !== 'Pending') {
        throw new Errors.ValidationError([
          {
            code: Codes.InvalidOperation,
            message: 'Reservation not in a Pending status, and so it cannot be rejected',
            field: 'id',
            index: 0,
          },
        ]);
      }
    })
    .catch(Bookshelf.NotFoundError, () => {
      throw new Errors.NotFoundError('Reservation not found');
    });

  /**
   * Reject and return updated Reservation
   */
  reservation.historyNotes = (req.body && req.body.notes) ? req.body.notes : '';
  reservation.historyAction = 'reject';
  reservation.historyUser = req.authInfo.user.id;
  return reservation.save({ status: 'Rejected' },
    {
      method: 'update', patch: true, reject: true,
    })
    .then(item => res.formatter.ok(item.toJSON()))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/reservations/{id}:
 *    delete:
 *      tags:
 *        - Reservations
 *      description: |
 *        Deletes the Reservation with the provided ID. If not Backoffice, only
 *        Reservations belonging to the current user are accessible.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
reservationRouter.delete('/:id', backoffice, (req, res, next) => {
  const opts = !req.backoffice
    ? { id: req.params.id, user_id: req.authInfo.user.id }
    : { id: req.params.id };

  Bookshelf.transaction(t => Reservation.forge()
    .where(opts)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t }))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Reservation not found.'); })
    .catch(next));
});

export default reservationRouter;
