/**
 * @swagger
 *
 * components:
 *  parameters:
 *    withRelatedVenue:
 *      in: query
 *      name: withRelated
 *      required: false
 *      schema:
 *       type: string
 *      description: >
 *        Request related entities to be included in the data objects (comma-separated list).
 *        If no value is specified, no entity is requested. Value of false disables the
 *        load of related entities. Possible values include 'reservations', 'activities',
 *        'regular_trainings' (activities of Regular Training type),
 *        'general_activities' (activities of General Activity type) and
 *        'competitions' (activities of Competition type).
 *  schemas:
 *    Venue:
 *      description: |
 *        A sport venue for Activities.<br>
 *        May exist standalone or reference a Location from MS Infraestructure.<br>
 *        If the Location is later on deleted, the Venue will still exist.
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          readOnly: true
 *          description: The Venue id.
 *          example: 1
 *        name:
 *          type: string
 *          nullable: false
 *          description: The Venue name.
 *          example: Pavilhão Multidesportivo do IPVC
 *        active:
 *          type: boolean
 *          nullable: false
 *          description: The Venue active status.
 *          example: true
 *        longitude:
 *          type: string
 *          nullable: false
 *          description: The Venue's longitude.
 *          example: 46.414382
 *        latitude:
 *          type: string
 *          nullable: false
 *          description: The Venue's latitude.
 *          example: 10.013988
 *        address:
 *          type: string
 *          nullable: false
 *          description: The  Venue's address.
 *          example: Largo do Cais
 *        address_no:
 *          type: string
 *          nullable: false
 *          description: The  Venue's address number.
 *          example: 120 2ºD
 *        city:
 *          type: string
 *          nullable: false
 *          description: The Venue's city.
 *          example: Lisboa
 *        location_id:
 *          type: integer
 *          nullable: true
 *          description: |
 *            The associated Location ID (from MS Infraestructure). Optional.
 *          example: 1
 *        created_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of creation of the Venue.
 *          example: 2019-01-01T00:00:00.000Z
 *        updated_at:
 *          type: string
 *          format: date
 *          nullable: false
 *          readOnly: true
 *          description: The date of the last update on the Venue.
 *          example: 2019-01-01T00:00:00.000Z
 */

import { Router } from 'express';

import Bookshelf from '../bookshelf';
import Venue from '../models/venue';
import { getPaginationMetaData } from '../helpers/metadata';
import asyncMiddleware from '../middleware/async';
import * as Errors from '../errors';

const venueRouter = new Router();

/**
 * @swagger
 * /api/v1/venues:
 *   get:
 *     tags:
 *       - Venues
 *     description: Returns a list of Venues.
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - $ref: '#/components/parameters/nameFilter'
 *       - $ref: '#/components/parameters/activeFilter'
 *       - $ref: '#/components/parameters/updateDateFilter'
 *       - $ref: '#/components/parameters/creationDateFilter'
 *       - $ref: '#/components/parameters/IdFilter'
 *       - $ref: '#/components/parameters/withRelatedVenue'
 *     responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Venue'
 *      '400':
 *        $ref: '#/components/responses/BadRequestParameters'
 *      '401':
 *        $ref: '#/components/responses/Unauthorized'
 *      '403':
 *        $ref: '#/components/responses/Forbidden'
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
venueRouter.get('/', (req, res, next) => {
  /*
   * Parse filters to add boolean and partial filter
   */
  req.parameters.parseFilters(
    {
      name: v => `%${v}%`,
      active: v => (v === 'true' ? 1 : 0),
      updated_at: v => `%${v}%`,
      created_at: v => `%${v}%`,
    },
  );

  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Venue.defaultWithRelated.slice();

  // Validate filters, sorting and withRelated parameters
  req.parameters.validate(Venue.filterFields, Venue.sortFields, Venue.withRelated);

  Venue.forge().filterOrderAndFetch(req.parameters)
    .then((collection) => {
      const data = collection ? collection.toJSON({ omitPivot: true }) : [];
      const link = (collection.pagination) ? getPaginationMetaData(collection.pagination, req) : {};
      return res.formatter.ok(data, link);
    })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/venues:
 *    post:
 *      tags:
 *        - Venues
 *      description: >
 *        Creates new Venue object.
 *      produces:
 *        - application/json
 *      requestBody:
 *        description: The Venue data
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Venue'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedVenue'
 *      responses:
 *        '201':
 *         description: Created Venue object
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Venue'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
venueRouter.post('/', asyncMiddleware(async (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Venue.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Venue.withRelated);

  return Venue.forge()
    .save(req.body, { method: 'insert' })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => res.formatter.created(item))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/venues/{id}:
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *        description: Numeric ID of the Venue.
 *    get:
 *      tags:
 *        - Venues
 *      description: >
 *        Returns a Venue object.
 *      produces:
 *        - application/json
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedVenue'
 *      responses:
 *        '200':
 *         description: Successful operation
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Venue'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
venueRouter.get('/:id', (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Venue.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Venue.withRelated);

  Venue.forge()
    .where('id', req.params.id)
    .fetch({ require: true, withRelated: req.parameters.withRelated })
    .then(item => res.formatter.ok(item.toJSON({ omitPivot: true })))
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Venue not found.'); })
    .catch(next);
});

/**
 * @swagger
 *  /api/v1/venues/{id}:
 *    put:
 *      tags:
 *        - Venues
 *      description: >
 *        Update a Venue object.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Venue updated data.
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Venue'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedVenue'
 *      responses:
 *        '200':
 *          description: Updated Venue object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Venue'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
venueRouter.put('/:id', asyncMiddleware(async (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Venue.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Venue.withRelated);

  return Venue.forge({ id: req.params.id })
    .save(req.body, { method: 'update', patch: false })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => res.formatter.ok(item))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/venues/{id}:
 *    patch:
 *      tags:
 *        - Venues
 *      description: >
 *        Partially update a Venue object.
 *      produces:
 *        - application/
 *      requestBody:
 *        description: The Venue data (may be partial).
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Venue'
 *      parameters:
 *       - $ref: '#/components/parameters/withRelatedVenue'
 *      responses:
 *        '200':
 *          description: Updated Venue object
 *          content:
 *            application/json:
 *             schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Venue'
 *        '400':
 *          $ref: '#/components/responses/BadRequestSimpleValidation'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
venueRouter.patch('/:id', asyncMiddleware(async (req, res, next) => {
  // Set default withRelated parameter
  req.parameters.withRelated = req.parameters.withRelated || Venue.defaultWithRelated.slice();

  // Validate withRelated parameters
  req.parameters.validate(null, null, Venue.withRelated);

  return Venue.forge({ id: req.params.id })
    .save(req.body, { method: 'update', patch: true })
    .then(item => item.refresh({ withRelated: req.parameters.withRelated }))
    .then(item => res.formatter.ok(item))
    .catch(next);
}));

/**
 * @swagger
 *  /api/v1/venues/{id}:
 *    delete:
 *      tags:
 *        - Venues
 *      description: |
 *        Deletes a Venue object. If it has associated trainings, activities or competities
 *        a BadRequest is returned.
 *      produces:
 *        - application/json
 *      responses:
 *        '204':
 *          description: Successful operation
 *        '400':
 *          $ref: '#/components/responses/BadRequest'
 *        '401':
 *          $ref: '#/components/responses/Unauthorized'
 *        '403':
 *          $ref: '#/components/responses/Forbidden'
 *        '404':
 *          $ref: '#/components/responses/NotFound'
 *        '500':
 *          $ref: '#/components/responses/ServerError'
 */
venueRouter.delete('/:id', (req, res, next) => {
  Bookshelf.transaction(t => Venue.forge()
    .where('id', req.params.id)
    .fetch({ require: true })
    .then(item => item.destroy({ transacting: t }))
    .then(() => res.formatter.noContent())
    .catch(Bookshelf.NotFoundError, () => { throw new Errors.NotFoundError('Venue not found.'); })
    .catch(next));
});

export default venueRouter;
