import Joi from 'joi';
import trainingDays from '../models/values/training_days';

module.exports = Joi.object().keys({
  type: Joi.string().valid('REGULAR TRAINING', 'GENERAL ACTIVITY', 'COMPETITION'),
  modality_id: Joi.number().integer().positive(),
  venue_id: Joi.number().integer().positive(),
  event_id: Joi.number().integer().positive().allow(null)
    .optional(),
  name: Joi.string().max(255).allow(''),
  description: Joi.string().max(500).allow(''),
  active: Joi.boolean(),
  start_date: Joi.date().iso(),
  end_date: Joi.any().when('type',
    {
      is: 'REGULAR TRAINING',
      then: Joi.date().iso(),
      otherwise: Joi.date().iso().allow(null),
    }),
  training_day: Joi.string().when('type',
    {
      is: 'REGULAR TRAINING',
      then: Joi.string().valid(trainingDays),
      otherwise: Joi.valid(null),
    }),
  start_time: Joi.string().regex(/^\d{2}:\d{2}$/),
  end_time: Joi.string().regex(/^\d{2}:\d{2}$/).allow(null),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
