import Joi from 'joi';

module.exports = Joi.object().keys({
  file_id: Joi.number().integer().positive(),
  name: Joi.string().max(255),
  active: Joi.boolean(),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
