import Joi from 'joi';
import recurrences from '../models/values/reservation_recurrences';

module.exports = Joi.object().keys({
  reservation_id: Joi.number().integer().min(1),
  recurrence: Joi.valid(recurrences),
  start_date: Joi.date().iso(),
  end_date: Joi.date().iso(),
  status: Joi.string().max(255),
  user_id: Joi.number().integer().min(1),
  notes: Joi.string().max(500).allow('', null).optional(),
  action: Joi.string().optional(),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
