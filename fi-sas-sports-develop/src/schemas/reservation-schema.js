import Joi from 'joi';
import statuses from '../models/values/reservation_statuses';
import recurrences from '../models/values/reservation_recurrences';

module.exports = Joi.object().keys({
  venue_id: Joi.number().integer().positive(),
  user_id: Joi.number().integer().positive(),
  user_profile_id: Joi.number().integer().positive(),
  user_profile_name: Joi.string().max(255).allow('', null),
  name: Joi.string().max(255),
  student_number: Joi.string().max(255),
  identification_number: Joi.string().max(255),
  tin: Joi.string().regex(/[0-9]{9}/),
  address: Joi.string().max(255),
  postal_code: Joi.string().max(15).regex(/^\d{4}-\d{3}$/),
  city: Joi.string().max(255),
  email: Joi.string().max(255).email(),
  phone_number: Joi.string().max(255).regex(/^(\+|00)\d{1,3}\d+$/),
  recurrence: Joi.valid(recurrences),
  status: Joi.valid(statuses),
  usage_description: Joi.string().max(255).allow(''),
  file_id: Joi.number().integer().positive().allow(null),
  start_date: Joi.date().iso(),
  end_date: Joi.date().iso().greater(Joi.ref('start_date')),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
