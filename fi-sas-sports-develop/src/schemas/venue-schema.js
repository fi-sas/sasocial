import Joi from 'joi';

module.exports = Joi.object().keys({
  name: Joi.string().max(255),
  active: Joi.boolean(),
  latitude: Joi.number().min(-90).max(90),
  longitude: Joi.number().min(-180).max(180),
  address: Joi.string().min(1).max(250),
  address_no: Joi.string().min(1).max(25),
  city: Joi.string().min(1).max(250),
  location_id: Joi.number().integer().positive().allow(null),
  created_at: Joi.date().iso().optional(),
  updated_at: Joi.date().iso(),
});
