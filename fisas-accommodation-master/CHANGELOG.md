## [1.57.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.57.0...v1.57.1) (2022-06-28)


### Bug Fixes

* **accommodation:** fix extra query contract changes ([9dfa553](https://gitlab.com/fi-sas/fisas-accommodation/commit/9dfa5530e91352e008ee9e6705efa06d1d1d15fc))
* **accommodation:** range dates by academic year ([2fa8b0c](https://gitlab.com/fi-sas/fisas-accommodation/commit/2fa8b0cf7df33d181fc6e4c628706b330b8e5893))

# [1.57.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.56.0...v1.57.0) (2022-06-28)


### Features

* **accommodation:** get phase by academic year ([85d6282](https://gitlab.com/fi-sas/fisas-accommodation/commit/85d6282c4437854b6cf6911cd688abd4bc7ddb33))

# [1.56.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.55.1...v1.56.0) (2022-06-27)


### Features

* **accommodation:** show phases since today ([2886b8f](https://gitlab.com/fi-sas/fisas-accommodation/commit/2886b8fb42639e2455e58f7448cd061243bbb05c))

## [1.55.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.55.0...v1.55.1) (2022-06-21)


### Bug Fixes

* **accommodation:** allow exit and entry in date range ([a9502e5](https://gitlab.com/fi-sas/fisas-accommodation/commit/a9502e522541c6188faa1820f2d429946382f8ff))

# [1.55.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.54.0...v1.55.0) (2022-06-20)


### Bug Fixes

* **accommodation:** table name fix ([e4bd5e3](https://gitlab.com/fi-sas/fisas-accommodation/commit/e4bd5e34280f3b6132f23546aacb3f041b90e7a0))


### Features

* **accommodation:** add file_id to extension table ([0551171](https://gitlab.com/fi-sas/fisas-accommodation/commit/055117132b85088f75f09d7389606b142cb645b0))

# [1.54.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.53.2...v1.54.0) (2022-06-06)


### Features

* **accommodation:** allow name sort ([8385ab9](https://gitlab.com/fi-sas/fisas-accommodation/commit/8385ab946ea89be9d577e0be6843486e6bdb252e))

## [1.53.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.53.1...v1.53.2) (2022-06-02)


### Bug Fixes

* **accommodation:** fix discount value ([037ba4c](https://gitlab.com/fi-sas/fisas-accommodation/commit/037ba4cbe2de20d8cdd3b7f32d595e214a033dca))

## [1.53.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.53.0...v1.53.1) (2022-06-01)


### Bug Fixes

* **accommodation:** fix billings proccess ([ed3bfe2](https://gitlab.com/fi-sas/fisas-accommodation/commit/ed3bfe2718d87722175313fbe2da36e0c95148b5))

# [1.53.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.52.0...v1.53.0) (2022-05-31)


### Features

* **configs:** add ALLOW_APPLICATION_RENEW option ([feb4f98](https://gitlab.com/fi-sas/fisas-accommodation/commit/feb4f98797e432fa818289b0a06b710de938df88))

# [1.52.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.51.0...v1.52.0) (2022-05-30)


### Features

* **accommodation:** academic year added to responses ([68f9679](https://gitlab.com/fi-sas/fisas-accommodation/commit/68f96792ffd72babec128aed892694fb608c220e))
* **accommodation:** new entry and exit hour in communications ([339cdaa](https://gitlab.com/fi-sas/fisas-accommodation/commit/339cdaab27e618e1eac002fe616b6db1508302f3))

# [1.51.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.50.1...v1.51.0) (2022-05-27)


### Features

* **accommodation:** show last user tariff in tariff report ([0cdc674](https://gitlab.com/fi-sas/fisas-accommodation/commit/0cdc6743d91357ae5ad03d8631a626ac793ead62))

## [1.50.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.50.0...v1.50.1) (2022-05-24)


### Bug Fixes

* **extensions:** change period-limit to academic_year limit ([509c444](https://gitlab.com/fi-sas/fisas-accommodation/commit/509c4447dffe900f7af2e83453e94972edc729e1))

# [1.50.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.49.2...v1.50.0) (2022-05-16)


### Bug Fixes

* **reports:** fix regime reprots get current data ([5027b2d](https://gitlab.com/fi-sas/fisas-accommodation/commit/5027b2dc724efbaf481d6065fbeece1a70406232))


### Features

* **accommodation:** add renew status to can apply action ([4409a1d](https://gitlab.com/fi-sas/fisas-accommodation/commit/4409a1db4bbcb74f48c50059d964124832013eee))

## [1.49.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.49.1...v1.49.2) (2022-05-10)


### Performance Improvements

* **applications:** improve performance of  notifications action ([c47718b](https://gitlab.com/fi-sas/fisas-accommodation/commit/c47718b341fa5709bf717c8678be1c766f9471fd))

## [1.49.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.49.0...v1.49.1) (2022-05-10)


### Bug Fixes

* **accommodation:** fix tariff report bug ([0be05e3](https://gitlab.com/fi-sas/fisas-accommodation/commit/0be05e3be7127d1df6154d81b826ef645dc5b670))

# [1.49.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.48.1...v1.49.0) (2022-05-06)


### Features

* **accommodation:** improve billings response ([234c402](https://gitlab.com/fi-sas/fisas-accommodation/commit/234c40207b25bd51a1f16861894351a1708111fe))

## [1.48.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.48.0...v1.48.1) (2022-05-04)


### Bug Fixes

* **application:** change application already exist validation ([2ccc86f](https://gitlab.com/fi-sas/fisas-accommodation/commit/2ccc86fcc9a72cd9b7b55698fea2ca3af7f2598c))

# [1.48.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.47.0...v1.48.0) (2022-04-20)


### Features

* **accommodation:** block billing without items ([e0d5fd1](https://gitlab.com/fi-sas/fisas-accommodation/commit/e0d5fd142e5e0fca0acfaec387e2bb60dfee20f6))

# [1.47.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.46.0...v1.47.0) (2022-04-20)


### Bug Fixes

* **billings:** change status machine of billings ([eab9c32](https://gitlab.com/fi-sas/fisas-accommodation/commit/eab9c32f76d2a2a8f11b1680d719b38570eaa884))


### Features

* **accommodation:** change unit price for processing ([9663a6e](https://gitlab.com/fi-sas/fisas-accommodation/commit/9663a6eaa2a56d57fb9114587a444a8bfc1ad108))
* **accommodation:** prevent item price 0 ([be763c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/be763c9860ec25283f8dc5c83251e6cd856e666d))
* **accommodation:** unit price billing items corrected ([78bae4d](https://gitlab.com/fi-sas/fisas-accommodation/commit/78bae4dc71b62277ef6e31c72bbb8515ffbbab0a))
* **accommodation:** unit price extra corrected ([39e3f4a](https://gitlab.com/fi-sas/fisas-accommodation/commit/39e3f4a61811e5f69a0865afca6fe32847b3e2d2))

# [1.46.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.45.0...v1.46.0) (2022-04-19)


### Bug Fixes

* **accommodation:** change billing from cancelled to processed ([0e18fd8](https://gitlab.com/fi-sas/fisas-accommodation/commit/0e18fd853b74f2dc04746d3f480eb6f144ef0bea))
* **accommodation:** check null for residence and room ([b2246dd](https://gitlab.com/fi-sas/fisas-accommodation/commit/b2246dd2c2b86e17a0401ec6c172c3b30af7f40b))
* **accommodation:** fix and improve billing service ([fd393c0](https://gitlab.com/fi-sas/fisas-accommodation/commit/fd393c039fd29a3f843bdbc9a739f15abd1a5e26))
* **accommodation:** fix extensions calculation ([493ad0f](https://gitlab.com/fi-sas/fisas-accommodation/commit/493ad0fb3ee31ce1ba762bd6c20a9b9e73005624))
* **accommodation:** get proccessed billings ([7407bf4](https://gitlab.com/fi-sas/fisas-accommodation/commit/7407bf45468e0351f07cdfe40b2a3a0af121721a))
* **accommodation:** removing commented code ([d456f5e](https://gitlab.com/fi-sas/fisas-accommodation/commit/d456f5e3306c0b9b0599fe0f0875969239a45247))


### Features

* **accommodation:** block extension and period change by accomomodation period dates ([a19cbd7](https://gitlab.com/fi-sas/fisas-accommodation/commit/a19cbd7953a66028e3bbb3a1668d409b5341f70c))

# [1.45.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.44.2...v1.45.0) (2022-04-06)


### Features

* **accommodation:** report map tariffs ([808dd47](https://gitlab.com/fi-sas/fisas-accommodation/commit/808dd47f41d5ff14da66941d0a80b746052e7610))

## [1.44.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.44.1...v1.44.2) (2022-03-31)


### Bug Fixes

* **accommodation:** fix contract-changes by allowed residences ([19caf63](https://gitlab.com/fi-sas/fisas-accommodation/commit/19caf63eb532bbd5457b26efe3287fe521d88f52))
* **accommodation:** fix order by name and list applications ([992bfb9](https://gitlab.com/fi-sas/fisas-accommodation/commit/992bfb9a42f2ebcc56d8f4390f4bf1f603e51a73))

## [1.44.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.44.0...v1.44.1) (2022-03-29)


### Bug Fixes

* **accommodation:** fix bug discount fields and new field in billing_item ([1645dac](https://gitlab.com/fi-sas/fisas-accommodation/commit/1645dac42ac4e17639446d75ebfc8698ae3e0851))
* **accommodation:** processing billing items ([a634e2f](https://gitlab.com/fi-sas/fisas-accommodation/commit/a634e2fe01e5e9531580054bee85a384adead7cc))
* **accommodation:** sort billing list by date ([1192d85](https://gitlab.com/fi-sas/fisas-accommodation/commit/1192d856dcf2a76f98357e554b210eb602c3d0c5))

# [1.44.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.43.0...v1.44.0) (2022-03-28)


### Bug Fixes

* **accommodation:** fix lint issues ([dd823a9](https://gitlab.com/fi-sas/fisas-accommodation/commit/dd823a91da2f0d92a0e0477cb48eb19dd2211d16))


### Features

* **accommodation:** add residences validation to billings ([f9553b6](https://gitlab.com/fi-sas/fisas-accommodation/commit/f9553b6b6e8766b0aeb530880e9ba1d0fbaf15dc))
* **accommodation:** added residences allow checking ([ef7ee7f](https://gitlab.com/fi-sas/fisas-accommodation/commit/ef7ee7fbedd5eacc16abe4a12d7ed03f1d7474bb))
* **accommodation:** allow changing accommodation period ([042c713](https://gitlab.com/fi-sas/fisas-accommodation/commit/042c7133b483e79ef4095be13834558a2a5b7601))
* **accomodation:** notifications by status ([6858746](https://gitlab.com/fi-sas/fisas-accommodation/commit/685874697a9c4e1feb219d16b21272e2c13da5a9))

# [1.43.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.42.4...v1.43.0) (2022-03-28)


### Features

* **accommodation:** new field to report dados em bruto ([5807343](https://gitlab.com/fi-sas/fisas-accommodation/commit/58073433f5227178865d75beede9389b0d854e30))

## [1.42.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.42.3...v1.42.4) (2022-03-18)


### Bug Fixes

* **regime-changes:** add application currentRegime related ([e9f948a](https://gitlab.com/fi-sas/fisas-accommodation/commit/e9f948a2d4e03874dd3016ca420cb98d49c7eae1))

## [1.42.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.42.2...v1.42.3) (2022-03-11)


### Bug Fixes

* **accommodation:** fix deleting rooms regimes typologies and extras ([e3a30d5](https://gitlab.com/fi-sas/fisas-accommodation/commit/e3a30d55c13027867b7d41481268243b9dca70b1))

## [1.42.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.42.1...v1.42.2) (2022-03-10)


### Bug Fixes

* **migrations:** add default to discounts values ([87eb689](https://gitlab.com/fi-sas/fisas-accommodation/commit/87eb6893f645caf1de938728597fcf84817b2c8c))

## [1.42.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.42.0...v1.42.1) (2022-03-03)


### Bug Fixes

* **application:** add current regime field ([5527ea0](https://gitlab.com/fi-sas/fisas-accommodation/commit/5527ea01457e60018fbb6f41472b2812d39c758c))

# [1.42.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.41.0...v1.42.0) (2022-03-03)


### Bug Fixes

* **accommodation:** fix processing and reprocessing billings ([f571828](https://gitlab.com/fi-sas/fisas-accommodation/commit/f571828f2547ccf60336615918a3653b9f703b06))
* **accommodation:** month reprocessing discount ([8e3a632](https://gitlab.com/fi-sas/fisas-accommodation/commit/8e3a6327f43668e48ce911dc3e51aa98543d898c))
* **accommodation:** processing and reprocessing issue ([5915444](https://gitlab.com/fi-sas/fisas-accommodation/commit/5915444fd4a924004e7d9ad19a7200cd4aed4af0))
* **accommodation:** reprocess_date fields and fix processing bugs ([3596afc](https://gitlab.com/fi-sas/fisas-accommodation/commit/3596afcaa45c07a1227ba786c5e5a0040397aece))
* **accommodation:** show active residences ([b712df8](https://gitlab.com/fi-sas/fisas-accommodation/commit/b712df8d5d98200eb8b233a2159618a9a9ae50bb))


### Features

* **accommodation:** billings remove reapeated code ([b0b10e9](https://gitlab.com/fi-sas/fisas-accommodation/commit/b0b10e9a635aed036cc0a640fd263db6b9e638cb))
* **accommodation:** discount regime, extra and typology feature ([d64b6e7](https://gitlab.com/fi-sas/fisas-accommodation/commit/d64b6e7cf5c71774c5667cd6518433fae24584f7))
* **accommodation:** discount tables/services ([f241cef](https://gitlab.com/fi-sas/fisas-accommodation/commit/f241cef1f3806873a62d442b81c72c5580707001))
* **accommodation:** hide decision while not contracted ([4373d34](https://gitlab.com/fi-sas/fisas-accommodation/commit/4373d341ecac2086f7b0236235a3eb809ba9f618))
* **accommodation:** improve tables load ([9fd382d](https://gitlab.com/fi-sas/fisas-accommodation/commit/9fd382d19f682402c4c8c2bef8b4b31503e9fa4d))

# [1.41.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.40.0...v1.41.0) (2022-02-10)


### Features

* remove limit 255 chars from fields reason ([c10488b](https://gitlab.com/fi-sas/fisas-accommodation/commit/c10488bd7f56db58ed3bb51695f42ae129fee16e))

# [1.40.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.39.0...v1.40.0) (2022-02-07)


### Features

* **accommodation:** show dispatch by permission ([7187271](https://gitlab.com/fi-sas/fisas-accommodation/commit/718727170f955cea302a21050c96d9055a3aefbf))

# [1.39.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.12...v1.39.0) (2022-02-01)


### Features

* **accommodation:** see rooms map ([a7b0e78](https://gitlab.com/fi-sas/fisas-accommodation/commit/a7b0e7892fe6410186c7f1e882936e9edb7169d0))
* **accommodation:** show typologies and improvment room map ([4279ec0](https://gitlab.com/fi-sas/fisas-accommodation/commit/4279ec0a59f18aa18ce7fb2672831a20a8ca76a5))

## [1.38.12](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.11...v1.38.12) (2022-01-20)


### Bug Fixes

* **room-changes:** add missing withRelated to typology ([923127d](https://gitlab.com/fi-sas/fisas-accommodation/commit/923127dd9d01680e8af82d0d07e336970d1636f3))

## [1.38.11](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.10...v1.38.11) (2022-01-17)

### Bug Fixes

- **application-room-changes:** fix withRelated history ([0dd7570](https://gitlab.com/fi-sas/fisas-accommodation/commit/0dd7570c7d16dff7cb8d736b638f22f8145cf0ca))

## [1.38.10](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.9...v1.38.10) (2022-01-14)

### Bug Fixes

- **application-room-changes:** remove withRelateds of withRelateds ([54050a4](https://gitlab.com/fi-sas/fisas-accommodation/commit/54050a42c70efe5aee4f2e26f26e0886d7798667))

## [1.38.9](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.8...v1.38.9) (2022-01-14)

### Bug Fixes

- **billings:** get sepa_id_debit_auth from iban_changes ([4942bb9](https://gitlab.com/fi-sas/fisas-accommodation/commit/4942bb985da894d95ed2f9cff1d15d1746b3e4ac))

## [1.38.8](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.7...v1.38.8) (2022-01-08)

### Bug Fixes

- **contract-changes:** include residence and academic-year into query ([22b21d4](https://gitlab.com/fi-sas/fisas-accommodation/commit/22b21d46d47e69a8fe4ffeb629e9eda77e433b91))

## [1.38.7](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.6...v1.38.7) (2022-01-08)

### Bug Fixes

- **billing:** prevent billing create at same month ([a275a95](https://gitlab.com/fi-sas/fisas-accommodation/commit/a275a95880357737162654eb6faeef44daf32abd))

## [1.38.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.5...v1.38.6) (2021-12-16)

### Bug Fixes

- increase table fields length observation and notes ([8b74ae5](https://gitlab.com/fi-sas/fisas-accommodation/commit/8b74ae575b8dc1f0fd4b492d7efb125f824db24f))

## [1.38.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.4...v1.38.5) (2021-12-13)

### Bug Fixes

- **applications:** missepeled where ([0f11263](https://gitlab.com/fi-sas/fisas-accommodation/commit/0f11263528e928a394b20eb201f8dd7a5de4acfc))

## [1.38.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.3...v1.38.4) (2021-12-13)

### Bug Fixes

- **applications:** can user apply validation ([64b729d](https://gitlab.com/fi-sas/fisas-accommodation/commit/64b729d655957a30bed6d6e4faff5aa14e530e35))
- **applications:** fix applications create validation ([efe5204](https://gitlab.com/fi-sas/fisas-accommodation/commit/efe5204f79435ee3640404f666679128fa7ff88f))

## [1.38.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.2...v1.38.3) (2021-12-10)

### Bug Fixes

- add banks ctt and bankinter ([6009a75](https://gitlab.com/fi-sas/fisas-accommodation/commit/6009a755fb53959d03cadea37ad372feb73cd928))

## [1.38.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.1...v1.38.2) (2021-12-06)

### Bug Fixes

- billings prevent owner application ([7b10cfb](https://gitlab.com/fi-sas/fisas-accommodation/commit/7b10cfba4a6c968cb420e8c97cfc6d3957e79740))

## [1.38.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.38.0...v1.38.1) (2021-12-06)

### Bug Fixes

- prevent billing application owner ([96ba92c](https://gitlab.com/fi-sas/fisas-accommodation/commit/96ba92c1ee8771ea7d4a5b22337ae9c9ebd72551))

# [1.38.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.37.2...v1.38.0) (2021-12-03)

### Bug Fixes

- **application_communications:** filter by application fields ([289d575](https://gitlab.com/fi-sas/fisas-accommodation/commit/289d57532c974025f2c18abca50fa7bacb60be1b))

### Features

- **billing:** order by application.full_name ([209f9ff](https://gitlab.com/fi-sas/fisas-accommodation/commit/209f9fff0d08526490047b1fd72ccc851c06f3b6))

## [1.37.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.37.1...v1.37.2) (2021-12-02)

### Bug Fixes

- **billings:** state-machine function wrong case ([00d7ee4](https://gitlab.com/fi-sas/fisas-accommodation/commit/00d7ee47281fa72a0a9398d25dc8f8989432fe8c))

## [1.37.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.37.0...v1.37.1) (2021-11-29)

### Bug Fixes

- **sepa:** fix ctrlSum to 2 decimal places and add sepa_created_by ([5dc66af](https://gitlab.com/fi-sas/fisas-accommodation/commit/5dc66afe3641f48cca98b721f9a4dc9c5de4cd6a))

# [1.37.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.36.4...v1.37.0) (2021-11-26)

### Features

- **contract-changes:** add generic search fields ([2ea7c40](https://gitlab.com/fi-sas/fisas-accommodation/commit/2ea7c40ae3aa7e1692d1424068fe771b3cf37bf3))

## [1.36.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.36.3...v1.36.4) (2021-11-24)

### Bug Fixes

- **application:** per_capita_income not calculated when update ([d3a3601](https://gitlab.com/fi-sas/fisas-accommodation/commit/d3a36017d1d9110420d05f96a45d07aeca3b160d))

## [1.36.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.36.2...v1.36.3) (2021-11-22)

### Performance Improvements

- **reports:** fixed schollar applications report withRelateds ([a841b0c](https://gitlab.com/fi-sas/fisas-accommodation/commit/a841b0c126eedc45e90e843a2c2dbf34ec27e5eb))

## [1.36.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.36.1...v1.36.2) (2021-11-19)

### Bug Fixes

- **sepa:** report sepa prevent no residence_id ([c6e1e1e](https://gitlab.com/fi-sas/fisas-accommodation/commit/c6e1e1e047dd5656339669aed8cc56edb8fc0eb4))

## [1.36.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.36.0...v1.36.1) (2021-11-19)

### Bug Fixes

- **movement:** add due_date to movement.expiration_at ([728b59b](https://gitlab.com/fi-sas/fisas-accommodation/commit/728b59bf6f789d6847489c9de2f1dd9b340e121f))

# [1.36.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.5...v1.36.0) (2021-11-17)

### Features

- **sepa:** add sepa report ([a214e39](https://gitlab.com/fi-sas/fisas-accommodation/commit/a214e397cd92e15f71670d76589f4597d858a31c))

## [1.35.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.4...v1.35.5) (2021-11-16)

### Bug Fixes

- **billings:** report timeout ([aecffc1](https://gitlab.com/fi-sas/fisas-accommodation/commit/aecffc16c8f8f789f232fddd5dc4164e6dcc3a2a))

## [1.35.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.3...v1.35.4) (2021-11-16)

### Bug Fixes

- **occurrences:** applicationSearchFields length ([9b4262e](https://gitlab.com/fi-sas/fisas-accommodation/commit/9b4262ee75a2732506cde066b7d694e56531738e))

## [1.35.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.2...v1.35.3) (2021-11-16)

### Bug Fixes

- **occurrences:** text search ([6b2d025](https://gitlab.com/fi-sas/fisas-accommodation/commit/6b2d025d4ea048cb70546148f900008ff2d62612))

## [1.35.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.1...v1.35.2) (2021-11-15)

### Bug Fixes

- **occurrences:** fix search be text ([5829518](https://gitlab.com/fi-sas/fisas-accommodation/commit/58295188688cf0666431079cd0535ded0b1bc96f))

## [1.35.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.35.0...v1.35.1) (2021-11-15)

### Bug Fixes

- **occurrences:** freeze search fields ([a2f355c](https://gitlab.com/fi-sas/fisas-accommodation/commit/a2f355ca5ea9e887634629d4eae6796e857422b6))

# [1.35.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.34.4...v1.35.0) (2021-11-15)

### Features

- **occurences:** search by applications fields ([2bf1eac](https://gitlab.com/fi-sas/fisas-accommodation/commit/2bf1eac3891494f88eccd1d4e0ab17dca6f5e1ff))

### Performance Improvements

- **occurrences:** remove unused withRelateds ([fd9d90b](https://gitlab.com/fi-sas/fisas-accommodation/commit/fd9d90b59a5c7b2e985a019e60c677d5b74da094))

## [1.34.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.34.3...v1.34.4) (2021-11-15)

### Bug Fixes

- **configuration:** add config enable external validate or paid ([1881b59](https://gitlab.com/fi-sas/fisas-accommodation/commit/1881b59e21513f7d1c8f71b510212769a568a4dd))

## [1.34.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.34.2...v1.34.3) (2021-11-15)

### Bug Fixes

- **applications:** add regimes and extras to assignedResidence ([7f9aa15](https://gitlab.com/fi-sas/fisas-accommodation/commit/7f9aa155e8ac0ba3e8a32e600c00206f81050396))

## [1.34.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.34.1...v1.34.2) (2021-11-12)

### Bug Fixes

- **sepa:** add bussiness days to request collection date ([3d9a2e6](https://gitlab.com/fi-sas/fisas-accommodation/commit/3d9a2e649b9feb324c788c96cab4f80efba5d048))
- **sepa:** prevent accented chars ([4849b8b](https://gitlab.com/fi-sas/fisas-accommodation/commit/4849b8bcbde8a48898a00b4d0664bdd708750f60))

## [1.34.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.34.0...v1.34.1) (2021-11-10)

### Bug Fixes

- **billing_item:** change fields float to decimal 8,3 ([70dd90d](https://gitlab.com/fi-sas/fisas-accommodation/commit/70dd90ded364ace301b1f343c4c2e512a4089221))

# [1.34.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.33.3...v1.34.0) (2021-11-10)

### Features

- **residences:** stats by typology ([f6a5ce9](https://gitlab.com/fi-sas/fisas-accommodation/commit/f6a5ce916f6b809c9187c9d84ff59f67eddb4766))

## [1.33.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.33.2...v1.33.3) (2021-11-10)

### Bug Fixes

- **sepa:** fix check iban changes ([94bc0c7](https://gitlab.com/fi-sas/fisas-accommodation/commit/94bc0c7a210712107a36d95d82bae501823a8967))

## [1.33.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.33.1...v1.33.2) (2021-11-09)

### Bug Fixes

- **billings:** add to state-machine external validated and paid ([4cb4171](https://gitlab.com/fi-sas/fisas-accommodation/commit/4cb4171c9a82cd54a38f42a8b12462f0bb0817d4))
- **reports:** add iban and email to reports raw and billings ([7b7263b](https://gitlab.com/fi-sas/fisas-accommodation/commit/7b7263ba9f65e691506ae49f8caf21b5c989e84b))

## [1.33.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.33.0...v1.33.1) (2021-11-05)

### Bug Fixes

- **applications:** change default withRelated ([06317e2](https://gitlab.com/fi-sas/fisas-accommodation/commit/06317e2bbabc93dfa64ad2ddfc430d9cedc0df3d))

# [1.33.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.9...v1.33.0) (2021-11-05)

### Features

- addressed publications ([8fca437](https://gitlab.com/fi-sas/fisas-accommodation/commit/8fca437ac23420b6e0f93d41e89257e310f8cc2d))

## [1.32.9](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.8...v1.32.9) (2021-11-04)

### Bug Fixes

- **application:** change start and end date to date type for prevent timezone issues ([00f1cc4](https://gitlab.com/fi-sas/fisas-accommodation/commit/00f1cc4b235c88c54abb1ea26727029e23b14a93))

## [1.32.8](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.7...v1.32.8) (2021-10-29)

### Bug Fixes

- **contract-changes-stats:** missing criteria by type on room-changes ([338925f](https://gitlab.com/fi-sas/fisas-accommodation/commit/338925f16173b3fe4dd33ae63aee18f5dfb2ed65))

## [1.32.7](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.6...v1.32.7) (2021-10-29)

### Bug Fixes

- **contract-changes-stats:** remove wrong filter by residence_id ([ac2ab70](https://gitlab.com/fi-sas/fisas-accommodation/commit/ac2ab70366e83ee47fcf04873cf41fc682f0604f))

## [1.32.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.5...v1.32.6) (2021-10-28)

### Bug Fixes

- **application:** counters contract-changes ([4e64537](https://gitlab.com/fi-sas/fisas-accommodation/commit/4e645379e17a5104fbbf8d6ece98b0b4b693520a))

## [1.32.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.4...v1.32.5) (2021-10-27)

### Bug Fixes

- **applications:** wrong index set on contract changes stats ([48c9a0b](https://gitlab.com/fi-sas/fisas-accommodation/commit/48c9a0b6533a21757e12905424146b606974308b))

## [1.32.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.3...v1.32.4) (2021-10-27)

### Bug Fixes

- add endpoint to count contracted changes ([0a6867e](https://gitlab.com/fi-sas/fisas-accommodation/commit/0a6867e0d0bc96dac877c934e18d4f3c0655f93e))

## [1.32.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.2...v1.32.3) (2021-10-27)

### Bug Fixes

- **iban_change:** add filter by application_id to Expired ibans ([ad8c758](https://gitlab.com/fi-sas/fisas-accommodation/commit/ad8c75884891878a5e4166c46eb9bcc79bd3f238))

## [1.32.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.1...v1.32.2) (2021-10-26)

### Bug Fixes

- **application:** add swift validation ([9d18638](https://gitlab.com/fi-sas/fisas-accommodation/commit/9d18638de1a3ddea9e15870218c1b5529d655af1))
- **billings:** iban change ([a2cfbcb](https://gitlab.com/fi-sas/fisas-accommodation/commit/a2cfbcbd70f36dbbeb19fed0d65d4b2868acf003))
- **iban_change:** allow_direct_debit to be false ([d0aae10](https://gitlab.com/fi-sas/fisas-accommodation/commit/d0aae106d62c7a6ee9dd063ae9ff3a1d0835df66))
- **iban_change:** remove validation allow_direct_debit as false ([ed6fb76](https://gitlab.com/fi-sas/fisas-accommodation/commit/ed6fb7625cf9f94057604a707651d77dd8b9b05a))
- **resume-list:** add missing filters ([977e97f](https://gitlab.com/fi-sas/fisas-accommodation/commit/977e97f2fa0048d23cc83ea516fc36d3a1bfa10e))
- **sepa:** consider billings_ids set on form ([27fbc94](https://gitlab.com/fi-sas/fisas-accommodation/commit/27fbc941cc9c5590aea9340091df085e64ba17d9))
- **sepa:** iban change ([84857ee](https://gitlab.com/fi-sas/fisas-accommodation/commit/84857ee1a6f85e437ee1db2c0fcb020d4d72d2fc))
- **sepa:** recur_first ([98e2dcc](https://gitlab.com/fi-sas/fisas-accommodation/commit/98e2dcccc4f042abfbed49f80c3c0af9464c0b39))
- **sepa:** update sepa auth_id ([6501260](https://gitlab.com/fi-sas/fisas-accommodation/commit/6501260d5510a0e58b59e22848ad67156baa4536))

## [1.32.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.32.0...v1.32.1) (2021-10-22)

### Bug Fixes

- **billings:** report billing passed by stream ([71d4326](https://gitlab.com/fi-sas/fisas-accommodation/commit/71d4326f511221ec7fff9155bbf468c71be866b1))

# [1.32.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.7...v1.32.0) (2021-10-21)

### Bug Fixes

- **sepa:** add default false to billing allow_debit_direct field ([54ec26f](https://gitlab.com/fi-sas/fisas-accommodation/commit/54ec26f89544a374ca772a3d7873ef4e01c5a054))
- **sepa:** remove logs and uncessary data ([5f32ee3](https://gitlab.com/fi-sas/fisas-accommodation/commit/5f32ee3f6b9812eec6ade051ced1253208d41a7c))

### Features

- **sepa:** sepa file generation ([4af0919](https://gitlab.com/fi-sas/fisas-accommodation/commit/4af0919d9f9feb22837289cf0f0d59d19c2e7dba))

## [1.31.7](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.6...v1.31.7) (2021-10-18)

### Bug Fixes

- add status withdrawal to stats endpoint ([2aff958](https://gitlab.com/fi-sas/fisas-accommodation/commit/2aff9586167ad068f359ba74bb43267936d3e3ca))

## [1.31.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.5...v1.31.6) (2021-10-12)

### Bug Fixes

- **reports:** add stream method to others reports ([131bf55](https://gitlab.com/fi-sas/fisas-accommodation/commit/131bf55865e2042dd5d50d3c79c0e1731835b6b5))

## [1.31.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.4...v1.31.5) (2021-10-07)

### Bug Fixes

- **lint:** fix lint errors ([2a2a68b](https://gitlab.com/fi-sas/fisas-accommodation/commit/2a2a68bddc47d63544958a6b0be2b4e78b0c43d4))
- **residences:** fix room map query ([3e73909](https://gitlab.com/fi-sas/fisas-accommodation/commit/3e739092465b9e51ffe0c395021b60add6dfc2ba))

## [1.31.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.3...v1.31.4) (2021-10-07)

### Bug Fixes

- **applications:** fix applications filter by residence ([e7465e0](https://gitlab.com/fi-sas/fisas-accommodation/commit/e7465e0d64281d24c810a0581eeef54c94ffa1c7))

## [1.31.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.2...v1.31.3) (2021-10-06)

### Bug Fixes

- **billings:** allow 0 at price + reprocess contracted applications ([1507d68](https://gitlab.com/fi-sas/fisas-accommodation/commit/1507d686ddd9b45144a4178029d179c0914471d4))

## [1.31.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.1...v1.31.2) (2021-10-04)

### Bug Fixes

- **applications:** add room occupation validation on select a room ([601fd8f](https://gitlab.com/fi-sas/fisas-accommodation/commit/601fd8f85abbe0aa38626c35e7b2b2ea9c06fd84))

## [1.31.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.31.0...v1.31.1) (2021-09-29)

### Bug Fixes

- **extras:** remove is_deposit field ([24303b7](https://gitlab.com/fi-sas/fisas-accommodation/commit/24303b7c786a080d41e7196b4dd08a254b02f4f0))

# [1.31.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.30.2...v1.31.0) (2021-09-29)

### Features

- **billings:** create report request ([9af5447](https://gitlab.com/fi-sas/fisas-accommodation/commit/9af54476f8d6e0333f1f946cad19ac374527d6e6))

## [1.30.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.30.1...v1.30.2) (2021-09-24)

### Bug Fixes

- **application:** add student cancel, and unassigned opposition limit days ([49c416c](https://gitlab.com/fi-sas/fisas-accommodation/commit/49c416ce8f155872ec09a954ada11c95d5fe6b55))

## [1.30.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.30.0...v1.30.1) (2021-09-22)

### Bug Fixes

- **reports:** add phase filter to application raw report ([b63f496](https://gitlab.com/fi-sas/fisas-accommodation/commit/b63f496b260c356e0d504d57c83fa75a45e4a685))

# [1.30.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.29.3...v1.30.0) (2021-09-20)

### Bug Fixes

- **lint:** resolve lint errors ([59e79d2](https://gitlab.com/fi-sas/fisas-accommodation/commit/59e79d2a75072253d82891af96a2b2159e47fbb6))

### Features

- **applications:** validate unreviewed billings on close applications ([809dcdd](https://gitlab.com/fi-sas/fisas-accommodation/commit/809dcdd699bffdc3bbede8e7c243f317bb300272))

## [1.29.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.29.2...v1.29.3) (2021-09-20)

### Bug Fixes

- **applicaiton-room-change:** disable start-date validtion on BO change ([abadb67](https://gitlab.com/fi-sas/fisas-accommodation/commit/abadb670c0b2e7ee6720f93c675034f8c4899f5a))
- **application:** add withdrawal options to state-machine ([fe6bfdf](https://gitlab.com/fi-sas/fisas-accommodation/commit/fe6bfdf935a1a2098b74e456b0127d1fdc4fcd3d))

## [1.29.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.29.1...v1.29.2) (2021-09-17)

### Bug Fixes

- **various:** fix lint and missing applciation\ filters ([ad9dedc](https://gitlab.com/fi-sas/fisas-accommodation/commit/ad9dedc50032e4cd8975c8047d215c0bd963e63c))

## [1.29.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.29.0...v1.29.1) (2021-09-16)

### Bug Fixes

- **application:** add withdrawal to open application validation ([0586732](https://gitlab.com/fi-sas/fisas-accommodation/commit/0586732460ec88b3a795e435982a19078dfd01fb))
- **various:** add application filters on application change requests ([4bad5ff](https://gitlab.com/fi-sas/fisas-accommodation/commit/4bad5ffc16f5a98139e60fe378d2daa57edb3b32))

# [1.29.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.28.0...v1.29.0) (2021-09-14)

### Features

- **application-room-changes:** addd application room change option ([3c1b08e](https://gitlab.com/fi-sas/fisas-accommodation/commit/3c1b08ee3450119a46238dfb339f4073bfa67136))

# [1.28.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.27.1...v1.28.0) (2021-09-13)

### Bug Fixes

- **applications:** fix /changes request order ([0d4753a](https://gitlab.com/fi-sas/fisas-accommodation/commit/0d4753aa7bf708da32de08dd10c41f8bb281f82a))

### Features

- **configurations:** add show iban configuration ([4bab445](https://gitlab.com/fi-sas/fisas-accommodation/commit/4bab445c9a7a88c1284800ff4cf72ee4cd5334a6))

## [1.27.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.27.0...v1.27.1) (2021-09-10)

### Bug Fixes

- **various:** various small fixs ([74d9624](https://gitlab.com/fi-sas/fisas-accommodation/commit/74d9624906ad58a028997677e3e8ec7ae27877d2))

# [1.27.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.26.1...v1.27.0) (2021-09-07)

### Features

- **application-communications:** create application communication forms ([1ed2f3e](https://gitlab.com/fi-sas/fisas-accommodation/commit/1ed2f3ef20033ebd7ce76a108e98700471458dac))
- **withdrawals:** add new withdrawal status, and cron to change application status ([f46f872](https://gitlab.com/fi-sas/fisas-accommodation/commit/f46f87220fa3fe77cf2967d4e34a081c543d9e82))

## [1.26.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.26.0...v1.26.1) (2021-09-06)

### Bug Fixes

- **reports:** fix reports requests filters ([4006f9d](https://gitlab.com/fi-sas/fisas-accommodation/commit/4006f9d17d281922750296a448ec7ef8ae107e38))

# [1.26.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.25.3...v1.26.0) (2021-09-06)

### Bug Fixes

- **applications:** return document_type_id on applicant-info ([8ff03c6](https://gitlab.com/fi-sas/fisas-accommodation/commit/8ff03c6d6dd9b0f764312a70a2f39ef10d03f8ea))

### Features

- **applications:** add rolback status service ([531b49a](https://gitlab.com/fi-sas/fisas-accommodation/commit/531b49a2e3ac316553b93ca0a4dccf04bae4404c))

## [1.25.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.25.2...v1.25.3) (2021-09-02)

### Bug Fixes

- **applications:** change concat method of changes endpoint ([e092f58](https://gitlab.com/fi-sas/fisas-accommodation/commit/e092f583c40dbbd7491671f8aa9d4c9dada584c4))

## [1.25.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.25.1...v1.25.2) (2021-09-02)

### Bug Fixes

- **application:** remove application validation by academic year ([527c53e](https://gitlab.com/fi-sas/fisas-accommodation/commit/527c53ef201626cba273bebf8e2ee96e99382ede))
- **reports:** applications result report order by applciant name ([3807c99](https://gitlab.com/fi-sas/fisas-accommodation/commit/3807c99597acb59faa954c56a945ce20b27590b6))

## [1.25.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.25.0...v1.25.1) (2021-09-01)

### Bug Fixes

- **applications-files:** get files query ([d8ea04f](https://gitlab.com/fi-sas/fisas-accommodation/commit/d8ea04fa47be39c4a59fff1c7d6dfae69013e1e4))

# [1.25.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.24.0...v1.25.0) (2021-08-31)

### Bug Fixes

- **applications:** change parameter name ([57bf251](https://gitlab.com/fi-sas/fisas-accommodation/commit/57bf251796d635d9442feaf347e99190db960185))

### Features

- **application-history:** save decision, residence and room on status change history ([58a8ebf](https://gitlab.com/fi-sas/fisas-accommodation/commit/58a8ebf1d3fd68eb926a87c5682ea1d1d86bffa0))
- **applications:** create application by BO on closed application phases ([e57f6ff](https://gitlab.com/fi-sas/fisas-accommodation/commit/e57f6ff86aab31082e20aa6150ccd150455130ec))

# [1.24.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.23.0...v1.24.0) (2021-08-30)

### Bug Fixes

- **lint:** remove lint warnings ([e18c55b](https://gitlab.com/fi-sas/fisas-accommodation/commit/e18c55bf1ad7e7d673c4adff2f0a0d0e87fa5078))

### Features

- **application:** add application files ([379a2c4](https://gitlab.com/fi-sas/fisas-accommodation/commit/379a2c4c02d4fb1ae45d7ed2589e201af6b6bbaa))
- **configuration:** add automatic change status to analysed after x days ([ae59972](https://gitlab.com/fi-sas/fisas-accommodation/commit/ae5997288e243039090f3a1d15c8ecabe5978eb5))

# [1.23.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.22.1...v1.23.0) (2021-08-27)

### Bug Fixes

- **billings:** remove billing month unique constraint ([33c3097](https://gitlab.com/fi-sas/fisas-accommodation/commit/33c3097ee92d6d39dfdd12e55b57cc9bb365829c))

### Features

- **application:** add alow direct debit flag to application ([a5d9a62](https://gitlab.com/fi-sas/fisas-accommodation/commit/a5d9a62e10a6e66c5bb49967104392ba2fb03f75))
- **application:** add per capita income calculation ([0486ea6](https://gitlab.com/fi-sas/fisas-accommodation/commit/0486ea64819e8cc30c9dad94f7855535ba020ae7))
- **application_phases:** add applciation phase out of date validation ([d38d326](https://gitlab.com/fi-sas/fisas-accommodation/commit/d38d326646a83cf3fab837dc1bd580882a2f1f12))

## [1.22.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.22.0...v1.22.1) (2021-08-19)

### Bug Fixes

- **application:** missing has_opposed frag change ([4a8afe2](https://gitlab.com/fi-sas/fisas-accommodation/commit/4a8afe2eb1f167cef7338403d9afea63395dc941))
- **lint:** fix lint errors ([e4bf67b](https://gitlab.com/fi-sas/fisas-accommodation/commit/e4bf67b4c71c016e6c4853dc80040c9e6f1816fe))
- **various:** several minor fixs ([eb6743f](https://gitlab.com/fi-sas/fisas-accommodation/commit/eb6743f7eef580a2a01bcb957507b295739509b9))

# [1.22.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.21.2...v1.22.0) (2021-08-19)

### Bug Fixes

- **maintenance-requests:** ass maintenance requests history ([01befa6](https://gitlab.com/fi-sas/fisas-accommodation/commit/01befa67e3e194c1c06fc58122900a9230217843))
- **various:** add file_id to change applicaiton requests ([7a68bd3](https://gitlab.com/fi-sas/fisas-accommodation/commit/7a68bd354c5c80977783747815b0516a36b7a758))
- **various:** add regime and extra changes history logs ([96e8047](https://gitlab.com/fi-sas/fisas-accommodation/commit/96e804751017000fa557c534928d4bfca7a46919))
- **variuos:** fix cache and withrelated problems ([2314748](https://gitlab.com/fi-sas/fisas-accommodation/commit/2314748d474714bae3fa2d09d3b2213f65b40c4d))
- **varius:** reprocess billings on changing prices ([7dccf4b](https://gitlab.com/fi-sas/fisas-accommodation/commit/7dccf4bf23709fe3b930e0f3819cd474624218f1))

### Features

- **tariff-change-request:** create tariff change request ([d5e1a8e](https://gitlab.com/fi-sas/fisas-accommodation/commit/d5e1a8eab99a8f5b653413f27c574907db051b66))
- **variuos:** add room change requests history and some state machine fixs ([74bd544](https://gitlab.com/fi-sas/fisas-accommodation/commit/74bd544f3e6cb96754581ded58d60db288664d13))

## [1.21.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.21.1...v1.21.2) (2021-08-04)

### Bug Fixes

- **billings:** fix error on due_date calculation ([1bdac1e](https://gitlab.com/fi-sas/fisas-accommodation/commit/1bdac1ed775694261ec07bb90d80526cb7464fcd))

## [1.21.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.21.0...v1.21.1) (2021-08-03)

### Bug Fixes

- **billings:** fix search and add cancel option to billed billings ([4c5b8fa](https://gitlab.com/fi-sas/fisas-accommodation/commit/4c5b8fa74c49d82d93d9127f8c8d912fe0776e41))

# [1.21.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.20.0...v1.21.0) (2021-08-02)

### Bug Fixes

- **absences:** add abcenses history ([d09b21b](https://gitlab.com/fi-sas/fisas-accommodation/commit/d09b21bab09ebbe26f1cfae45007a15ba250922e))
- **extensions:** fix extensions history ([084d985](https://gitlab.com/fi-sas/fisas-accommodation/commit/084d9855f25f16c85ffb74b7a6ec5218b6104e6e))
- **extensions:** update extensions state machine ([fd55f44](https://gitlab.com/fi-sas/fisas-accommodation/commit/fd55f44a2021078169946d2e9f633fc1a76149a9))
- **various:** some small errors fix ([1998004](https://gitlab.com/fi-sas/fisas-accommodation/commit/199800481a8af10656ea283936f75b97fd88353b))
- **withdrawals:** fix withdrawals history ([50c394d](https://gitlab.com/fi-sas/fisas-accommodation/commit/50c394d899280adf3a925c839b48107dceb6b5a4))

### Features

- **occurrences:** add application ocurrences ([bdbcbe2](https://gitlab.com/fi-sas/fisas-accommodation/commit/bdbcbe2c73c6f5d4f4d2a1cd5d16325a6fd80b62))

# [1.20.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.19.0...v1.20.0) (2021-07-27)

### Bug Fixes

- **billings:** fix lint ([75853a4](https://gitlab.com/fi-sas/fisas-accommodation/commit/75853a4655eeed758635fa61daf45febb7302123))
- **various:** add observations field to application change requests ([c9ca8ff](https://gitlab.com/fi-sas/fisas-accommodation/commit/c9ca8ff00c19e036ef7bcad383f45d88e55dc03b))
- **various:** change scopes ([7d3bf53](https://gitlab.com/fi-sas/fisas-accommodation/commit/7d3bf53af6da7460f5f0031ce531c9bc76232cf6))

### Features

- **applciations:** add change residence request ([7c9f2b2](https://gitlab.com/fi-sas/fisas-accommodation/commit/7c9f2b23b19c0ab0526f4818f1b7dd3b77da4083))
- **application:** application billing processment ([ffbb8e3](https://gitlab.com/fi-sas/fisas-accommodation/commit/ffbb8e3ecd1db568e4f05d4b66931279948eb1c2))
- **application-room-changes:** create request to change typology, residence and permute room ([ac0c253](https://gitlab.com/fi-sas/fisas-accommodation/commit/ac0c2533fa0f786c1ffaecb863257c3fbe455a71))
- **application-typology-change:** add application typology change request loginc and processment ([2e08394](https://gitlab.com/fi-sas/fisas-accommodation/commit/2e083942943ee167981e52e346ed4344fa2c54ab))
- **billing:** automatic update application fields ([fec13e1](https://gitlab.com/fi-sas/fisas-accommodation/commit/fec13e13b7c677afee95e2c03adf669c4a3ae1bb))
- **billings:** add due date configuration params ([76f6c60](https://gitlab.com/fi-sas/fisas-accommodation/commit/76f6c60bfb8ac207447685dc72e0d713eb8621f3))
- **billings:** apply extras/regime changes in processment afte approves requests ([9dbc26c](https://gitlab.com/fi-sas/fisas-accommodation/commit/9dbc26c546d17bdd6dfc706e5c45eed52bc7d502))
- **billings:** change price lines and process withdrawal requests ([6dec6f6](https://gitlab.com/fi-sas/fisas-accommodation/commit/6dec6f61a8cb22b3f1b55293c394d0caa784f86d))
- **billings:** filter billings by application fields ([ec57af6](https://gitlab.com/fi-sas/fisas-accommodation/commit/ec57af6d04855aae38168cd4b486cb483c00c6e3))
- **billings:** process application extension requests ([5a90cf6](https://gitlab.com/fi-sas/fisas-accommodation/commit/5a90cf64151034d0ef429d25f3ebd3f48832fc32))
- **billings:** reprocesse billings when pricelines are changed ([03a48a2](https://gitlab.com/fi-sas/fisas-accommodation/commit/03a48a207b2253147bec664ae4d394bd2e33f373))
- **maintenance_requests:** create maintenance requests ([9d165a9](https://gitlab.com/fi-sas/fisas-accommodation/commit/9d165a9ecb6589ea28b8ecd3435a06ac66c2cea2))

# [1.19.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.18.2...v1.19.0) (2021-07-19)

### Features

- **various:** add search on entities translations ([ebef08b](https://gitlab.com/fi-sas/fisas-accommodation/commit/ebef08bf60dfdf22f434d9edc1325591699d916f))

## [1.18.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.18.1...v1.18.2) (2021-07-16)

### Bug Fixes

- **reports:** parse data to applications raw data report ([b688099](https://gitlab.com/fi-sas/fisas-accommodation/commit/b68809977d99a50a46c05928479a8a4a97e6be7b))

## [1.18.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.18.0...v1.18.1) (2021-07-12)

### Bug Fixes

- **applications:** fixx error in extras withrelated ([5cdbe45](https://gitlab.com/fi-sas/fisas-accommodation/commit/5cdbe4524cf7756df4d187bcd67469bc5f5ce437))

# [1.18.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.6...v1.18.0) (2021-07-12)

### Features

- **reports:** raw report now accepts all residences and seend by stream the data ([7725413](https://gitlab.com/fi-sas/fisas-accommodation/commit/7725413223e814368ed5b45fb608f921d5d0d79d))

## [1.17.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.5...v1.17.6) (2021-07-05)

### Bug Fixes

- **applications:** fix cron auto status change and rejected status ([f87eacb](https://gitlab.com/fi-sas/fisas-accommodation/commit/f87eacb0663f82c7888912b5cf7de928d5babe1f))

## [1.17.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.4...v1.17.5) (2021-07-01)

### Bug Fixes

- **reports:** add filter by residence to raw report ([1ba12dc](https://gitlab.com/fi-sas/fisas-accommodation/commit/1ba12dca6ba2c04d7e70dbadd30d77e565d2520f))

## [1.17.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.3...v1.17.4) (2021-06-30)

### Bug Fixes

- **applications:** add new fields to applicant info ([9e75446](https://gitlab.com/fi-sas/fisas-accommodation/commit/9e75446b18272f53be10760efb549ad3d6d45d34))

## [1.17.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.2...v1.17.3) (2021-06-28)

### Bug Fixes

- **rroms:** occupants status change filtering ([57a79db](https://gitlab.com/fi-sas/fisas-accommodation/commit/57a79db302770811ceb1cc587b95ddbfe8a5ef97))

## [1.17.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.1...v1.17.2) (2021-06-28)

### Bug Fixes

- **rooms:** fix occupants withrelated ([41451a8](https://gitlab.com/fi-sas/fisas-accommodation/commit/41451a8ddd7fd226816d45bde11f1ebbd62c2ab6))

## [1.17.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.17.0...v1.17.1) (2021-06-16)

### Bug Fixes

- **rooms:** remove inactive rooms from occupation calculations ([6c0fca1](https://gitlab.com/fi-sas/fisas-accommodation/commit/6c0fca157286dd7449de7c206335798a4a131974))

# [1.17.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.16.2...v1.17.0) (2021-06-15)

### Bug Fixes

- **applications:** remove /cancel endpoint ([dd073f6](https://gitlab.com/fi-sas/fisas-accommodation/commit/dd073f64bbd4f9aceb9c0e248e2520bbbca7247d))
- **reports:** add OTHER validiation ([4142bce](https://gitlab.com/fi-sas/fisas-accommodation/commit/4142bce2c220167d5da363a78f6c0366ce97f0b5))
- **reports:** error on income_source undefined ([6b4838f](https://gitlab.com/fi-sas/fisas-accommodation/commit/6b4838f4fe49a38504217e8d34348bcbeaf90215))
- **residences:** remove free-rooms withrelateds ([f2bdab8](https://gitlab.com/fi-sas/fisas-accommodation/commit/f2bdab88a59835fc1591832a56c6261f39dd6301))
- **rooms:** fix reported errors by IPCoimbra ([fa5c5ab](https://gitlab.com/fi-sas/fisas-accommodation/commit/fa5c5ab5140163ab49e78650db77a62a8527f8e1))

### Features

- **applications:** add student cancel enpoint ([0d81e6c](https://gitlab.com/fi-sas/fisas-accommodation/commit/0d81e6cd93d68f22c53c617d27b1df7ab91094f2))

## [1.16.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.16.1...v1.16.2) (2021-06-14)

### Bug Fixes

- **application:** change automatic status change after X days ([e25815e](https://gitlab.com/fi-sas/fisas-accommodation/commit/e25815eaf96cdb6c8f9b3aff95c1ef9994625459))
- **application:** change close status change ([6b7c25e](https://gitlab.com/fi-sas/fisas-accommodation/commit/6b7c25e4ad3e42bf903fd931053a96ad4fbe5e65))
- **application:** missing change on last commit ([c9ffdfa](https://gitlab.com/fi-sas/fisas-accommodation/commit/c9ffdfae696aa933d66cd73e68f0e1eb0424badd))
- **applications:** fix notes validation ([5733d93](https://gitlab.com/fi-sas/fisas-accommodation/commit/5733d9379895e72a8de5dbf57bed6bfb8a615046))

## [1.16.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.16.0...v1.16.1) (2021-06-09)

### Bug Fixes

- **rooms:** bad search field on occupants related ([bcfdbfa](https://gitlab.com/fi-sas/fisas-accommodation/commit/bcfdbfa3f31bd12b3c6c69722893d8a6f44e1974))

# [1.16.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.15.1...v1.16.0) (2021-06-07)

### Features

- **applications:** optimize applcation extras related ([dcb044f](https://gitlab.com/fi-sas/fisas-accommodation/commit/dcb044f72037bef43892417120193f1f048b28a8))

## [1.15.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.15.0...v1.15.1) (2021-05-31)

### Bug Fixes

- **reports:** add missing fields in report ([77f4912](https://gitlab.com/fi-sas/fisas-accommodation/commit/77f4912c76942302ed5230c3ecb5b06d897277bd))
- **reports:** fix error ([08325df](https://gitlab.com/fi-sas/fisas-accommodation/commit/08325df3fe32cdd13376adc37fe5625211bcca3e))

# [1.15.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.6...v1.15.0) (2021-05-28)

### Bug Fixes

- **applications:** add preferred_typology_id validation ([3e71286](https://gitlab.com/fi-sas/fisas-accommodation/commit/3e7128699cc31368c385564f09013886eb9941a9))
- **applications:** optimize withrelated fields ([a8dbfb6](https://gitlab.com/fi-sas/fisas-accommodation/commit/a8dbfb6f6adc578e70f5890c75c99d220c842aac))

### Features

- **applications:** add preferred_typology and withrelated ([beb8f19](https://gitlab.com/fi-sas/fisas-accommodation/commit/beb8f19c90a73a40454c8545acbc0abc60085be4))

## [1.14.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.5...v1.14.6) (2021-05-28)

### Bug Fixes

- **applications:** add endpoint stats params ([684134e](https://gitlab.com/fi-sas/fisas-accommodation/commit/684134edfe75be7b1ec19f3691740ff22cd59f72))

## [1.14.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.4...v1.14.5) (2021-05-26)

### Bug Fixes

- **applications:** add automatic change status on 5 days in assigned status ([02e3145](https://gitlab.com/fi-sas/fisas-accommodation/commit/02e314509d470b03a9ac2770a0a390ec494009aa))

## [1.14.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.3...v1.14.4) (2021-05-26)

### Bug Fixes

- **applications:** change scopes of change status ([2e1d65f](https://gitlab.com/fi-sas/fisas-accommodation/commit/2e1d65f52519a1db0b781df1c73a71419aca1626))

## [1.14.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.2...v1.14.3) (2021-05-25)

### Bug Fixes

- **patrimony_categories:** validate type on hook ([d83b44b](https://gitlab.com/fi-sas/fisas-accommodation/commit/d83b44b24c49fbe6402b83d6efc041fbaf87e0d8))

## [1.14.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.1...v1.14.2) (2021-05-24)

### Bug Fixes

- **application:** change opposition data types ([3579082](https://gitlab.com/fi-sas/fisas-accommodation/commit/3579082b4d8c4e5c7e11d199e772669a06f81f23))

## [1.14.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.14.0...v1.14.1) (2021-05-24)

### Bug Fixes

- **applications:** add course_degree_id and remove unused fields ([48af9db](https://gitlab.com/fi-sas/fisas-accommodation/commit/48af9dbb7ac2b9cd2fbb4f7683f77c331cc55beb))

# [1.14.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.13.0...v1.14.0) (2021-05-19)

### Bug Fixes

- **applications:** add gender to applicant-info ([a2b2921](https://gitlab.com/fi-sas/fisas-accommodation/commit/a2b29214b10a40ad6cd7c42737edfb3d92fcc569))
- **residences:** add bindings to raw queries ([b13d1e3](https://gitlab.com/fi-sas/fisas-accommodation/commit/b13d1e3af9023eefa85409ab5bafc1f68e25beb7))

### Features

- **residences:** fix occupation map request ([2bb8eae](https://gitlab.com/fi-sas/fisas-accommodation/commit/2bb8eaebac2722869119fcbc7705c4bf5a4d795e))

# [1.13.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.12.3...v1.13.0) (2021-05-17)

### Features

- **residences:** add field visible for users to residences ([280b21f](https://gitlab.com/fi-sas/fisas-accommodation/commit/280b21fc562d6222b79e06ecae12dd956cc0e888))

## [1.12.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.12.2...v1.12.3) (2021-05-07)

### Bug Fixes

- **application:** remove icome_source required ([7e7a221](https://gitlab.com/fi-sas/fisas-accommodation/commit/7e7a221a35c806927e2b6de9d54f9f36f98df6f2))
- **reports:** change application report unformatted fields ([f36da0b](https://gitlab.com/fi-sas/fisas-accommodation/commit/f36da0bdee4cb9d92735816d36ae1fc622ebcbb4))

## [1.12.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.12.1...v1.12.2) (2021-05-06)

### Bug Fixes

- **residences:** add decision to free rooms query ([3baff40](https://gitlab.com/fi-sas/fisas-accommodation/commit/3baff400c2a89a2c930ce2e59484f7e190d17871))

## [1.12.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.12.0...v1.12.1) (2021-05-06)

### Bug Fixes

- **reportes:** change document type ([b3f9524](https://gitlab.com/fi-sas/fisas-accommodation/commit/b3f952439b85f542588c14f85928288bec755e74))

# [1.12.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.11.0...v1.12.0) (2021-05-03)

### Bug Fixes

- **applications:** change premute room scope ([e61f82f](https://gitlab.com/fi-sas/fisas-accommodation/commit/e61f82f63a98e574de2fa011b48ec0ce1c00524b))
- **residences:** change free rooms query ([0e63a63](https://gitlab.com/fi-sas/fisas-accommodation/commit/0e63a6304b06e5840a937fb3054760558d8612ca))

### Features

- **applications:** add premute room request ([90e127c](https://gitlab.com/fi-sas/fisas-accommodation/commit/90e127c1fabfa6c65c5947973f53f818978a7699))

# [1.11.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.10.4...v1.11.0) (2021-04-30)

### Features

- **application-phases:** add fixed period flag ([03b08c0](https://gitlab.com/fi-sas/fisas-accommodation/commit/03b08c00ba91256fa978a9375d5b1f1da88a18fa))

## [1.10.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.10.3...v1.10.4) (2021-04-30)

### Bug Fixes

- **application:** change stats scope ([236d0c6](https://gitlab.com/fi-sas/fisas-accommodation/commit/236d0c6d7856f426c64880fbc826fabe8f2bbb55))

## [1.10.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.10.2...v1.10.3) (2021-04-30)

### Bug Fixes

- **application:** add missing scope ([59441ca](https://gitlab.com/fi-sas/fisas-accommodation/commit/59441ca390e040a6f8f65350bd9b1937efca88b9))
- **applications:** change document_type to document_type_id ([00518e1](https://gitlab.com/fi-sas/fisas-accommodation/commit/00518e1a1741ce12065f8ab6421779c0988ec51e))

## [1.10.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.10.1...v1.10.2) (2021-04-28)

### Bug Fixes

- **configurations:** fix configuration change validation ([b75bdd6](https://gitlab.com/fi-sas/fisas-accommodation/commit/b75bdd662aed8fecadf12b9e3ff9f54d67acfc4b))

## [1.10.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.10.0...v1.10.1) (2021-04-28)

### Bug Fixes

- **applications:** change user_id on backoffice request ([b653362](https://gitlab.com/fi-sas/fisas-accommodation/commit/b6533627716575b99dfbfdaf6abe3458d3ab6fdb))

# [1.10.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.9.0...v1.10.0) (2021-04-27)

### Bug Fixes

- **applciations:** cancel application dependencies on close/cancel ([cbdc137](https://gitlab.com/fi-sas/fisas-accommodation/commit/cbdc137fc666cc3fa47da5995eb4fc03bfadd3bf))

### Features

- **reports:** application extra reports generation ([3202489](https://gitlab.com/fi-sas/fisas-accommodation/commit/3202489e9bef2d9fa8533b1f05c4472f7fd3f07a))
- **reports:** application regimes and social scholarship reports ([fb611db](https://gitlab.com/fi-sas/fisas-accommodation/commit/fb611db7aeae1e3b2ca5599256fbb31c0454299f))

# [1.9.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.8.0...v1.9.0) (2021-04-26)

### Bug Fixes

- **accommodation-period:** add visibility to delete ([b44123a](https://gitlab.com/fi-sas/fisas-accommodation/commit/b44123adafc38707e8c5692c5932a92316b0ed60))
- **application:** add descritive comment ([89073b7](https://gitlab.com/fi-sas/fisas-accommodation/commit/89073b7deddd0e19337a214fb55ac19fd096155d))
- **applications:** add backoffice validation ([6a31503](https://gitlab.com/fi-sas/fisas-accommodation/commit/6a31503e6a861be67771563bd058bf2d151ed4dc))
- **applications:** change path variable to param ([88f29f1](https://gitlab.com/fi-sas/fisas-accommodation/commit/88f29f1f987e8257b56d0ceb2654e796e0edd003))
- **configuration:** fix find query params ([f2a70e7](https://gitlab.com/fi-sas/fisas-accommodation/commit/f2a70e7a7df934ad7a496d28d34181181bcacd8b))

### Features

- **applications:** add can apply request ([c2c7ebc](https://gitlab.com/fi-sas/fisas-accommodation/commit/c2c7ebc8a8d7a7eeab66b370f9de7fe2e577cb65))
- **applications:** add income_source and other_income_source ([2322f48](https://gitlab.com/fi-sas/fisas-accommodation/commit/2322f48ff9fc2bfd5eba41f0ae085947a81dd223))

# [1.8.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.7.3...v1.8.0) (2021-04-26)

### Bug Fixes

- **application:** change user_previous_activities withrelateds ([3fac8bf](https://gitlab.com/fi-sas/fisas-accommodation/commit/3fac8bfcef50fe7b728ca5d148b0e3b80bfe0327))
- **application:** fix user open applications validation ([6247535](https://gitlab.com/fi-sas/fisas-accommodation/commit/62475353ef60ef18d8ce3e95091417b22f0384ad))
- **configurations:** add validation ([7e34f0b](https://gitlab.com/fi-sas/fisas-accommodation/commit/7e34f0bd67186cf92686268e8526054fdf574219))

### Features

- **accommodation_period:** add accommodation period service ([57533f9](https://gitlab.com/fi-sas/fisas-accommodation/commit/57533f9ee33d60dbe32d2f44fd41c83acfdde12b))

## [1.7.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.7.2...v1.7.3) (2021-04-23)

### Bug Fixes

- **application:** add international_student validations ([ba8c4a0](https://gitlab.com/fi-sas/fisas-accommodation/commit/ba8c4a0305a94bb53b7044001d5a73a16a7b945c))
- **application:** add patrimony_category and remove patrimony_category \_id/registry_validity ([3b0d9c2](https://gitlab.com/fi-sas/fisas-accommodation/commit/3b0d9c257b9cfe4959fa57893bae79877571d8df))
- **application:** improve international user validation ([4d84b5f](https://gitlab.com/fi-sas/fisas-accommodation/commit/4d84b5f86373058f935d27db75cf6b0d4e769955))

## [1.7.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.7.1...v1.7.2) (2021-04-20)

### Bug Fixes

- **report:** add extras to application report ([2b93472](https://gitlab.com/fi-sas/fisas-accommodation/commit/2b93472018dbc7f329181bac3b37c76cce7b115f))

## [1.7.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.7.0...v1.7.1) (2021-04-19)

### Bug Fixes

- **applications:** allow to submit multiple income statement files ([66c4584](https://gitlab.com/fi-sas/fisas-accommodation/commit/66c4584578754155544b225dfb2fa37aa929ef87))

# [1.7.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.6.0...v1.7.0) (2021-04-19)

### Bug Fixes

- **patrimony-categories:** cganhe functions name ([7ef4d05](https://gitlab.com/fi-sas/fisas-accommodation/commit/7ef4d05e4fc78a3657ff103b9a6da6505a8f5c0b))
- **patrimony-categories:** validate code on create and remove ([0023421](https://gitlab.com/fi-sas/fisas-accommodation/commit/00234210de15217721812f0e72a17b8b740a0924))

### Features

- **applications:** add patrimony categories and remove household_accounts_ballence ([4e6e9af](https://gitlab.com/fi-sas/fisas-accommodation/commit/4e6e9af3f8e59a0cec8dc3154cdc1bc641456afc))

# [1.6.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.5.0...v1.6.0) (2021-04-19)

### Features

- **applications:** add document type to applications ([aa575a9](https://gitlab.com/fi-sas/fisas-accommodation/commit/aa575a9bc0cb1d3da79ddb483f7fe9f519d3745c))

# [1.5.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.4.0...v1.5.0) (2021-04-16)

### Bug Fixes

- **residences:** required at least one regime ([2768879](https://gitlab.com/fi-sas/fisas-accommodation/commit/27688796b78f0566037e451d36c2aae0fa6197d6))

### Features

- **configurations:** add configurations ([e006223](https://gitlab.com/fi-sas/fisas-accommodation/commit/e00622336e5a9324846003d08766bd3180f996b0))
- **configurations:** remove unused code ([7ab0f82](https://gitlab.com/fi-sas/fisas-accommodation/commit/7ab0f8231619fc5e00876359f7410645a81ed772))

# [1.4.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.3.2...v1.4.0) (2021-04-16)

### Features

- **residences:** add avalilable for application and assign flags ([8a86a53](https://gitlab.com/fi-sas/fisas-accommodation/commit/8a86a537c90cf917c157f88d00bbf3f4d81d5839))

## [1.3.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.3.1...v1.3.2) (2021-04-16)

### Bug Fixes

- **application:** add missing scope ([3bcf039](https://gitlab.com/fi-sas/fisas-accommodation/commit/3bcf039b92d567978892e3f9209fba042d9b475d))
- **application:** change admission_year to admission_date ([fd8ded0](https://gitlab.com/fi-sas/fisas-accommodation/commit/fd8ded0f40bd147b68ba72a66e27e67e426b6eef))
- **applications:** add new field to applications ([3717875](https://gitlab.com/fi-sas/fisas-accommodation/commit/371787553a92590436bdc96548e21be0bf7ca1b4))

## [1.3.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.3.0...v1.3.1) (2021-04-14)

### Bug Fixes

- **application-phases:** add academic year dates validation ([bca47c1](https://gitlab.com/fi-sas/fisas-accommodation/commit/bca47c1ba51eca7e1f22d5da2600750b3cc82858))
- **application-phases:** remove between academic year validation ([501c6cb](https://gitlab.com/fi-sas/fisas-accommodation/commit/501c6cbf8dd1fd285c7c95195d2716cb4f3305b8))

# [1.3.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.2.0...v1.3.0) (2021-04-13)

### Bug Fixes

- **application:** remove wait from user_previous_applications withRelated ([08723bc](https://gitlab.com/fi-sas/fisas-accommodation/commit/08723bc54f4ca47015f59c7e9fc2582f5f6adea0))

### Features

- **applications:** add user_previous_applications withrelated ([cce733d](https://gitlab.com/fi-sas/fisas-accommodation/commit/cce733d642dfc3e709580ac9b3c4d54c0b284c79))

# [1.2.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.1.1...v1.2.0) (2021-04-13)

### Bug Fixes

- **application:** second_option_residence withrelated ([becf705](https://gitlab.com/fi-sas/fisas-accommodation/commit/becf70554502055b0815cdc9ba488fd5d4d60fef))
- change transition name to analyse ([2595ec5](https://gitlab.com/fi-sas/fisas-accommodation/commit/2595ec507487574bb9930178d5aa0a9f71f55500))

### Features

- **absences:** add state machine and /status requests ([4bfb421](https://gitlab.com/fi-sas/fisas-accommodation/commit/4bfb421c900df91b2467f82ad6558633108f605d))

## [1.1.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.1.0...v1.1.1) (2021-04-07)

### Bug Fixes

- remove cronjobs and validate application ([538d081](https://gitlab.com/fi-sas/fisas-accommodation/commit/538d081fdf4ca30f91b336989b2ccae2322088ef))

# [1.1.0](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.1...v1.1.0) (2021-04-07)

### Bug Fixes

- add regime change and extras change state machine ([d6c5b95](https://gitlab.com/fi-sas/fisas-accommodation/commit/d6c5b95dad7984db23f08153721c8e1b306ecdb0))
- **applications:** add cronjobs to update application extras and regimes ([153478f](https://gitlab.com/fi-sas/fisas-accommodation/commit/153478ff35d4acc8e08ecd98f07c4c136419c189))
- change fields names ([0aa8521](https://gitlab.com/fi-sas/fisas-accommodation/commit/0aa8521221b6c00cdc8f7a39e977c675d82cc23b))
- **applications:** cancel pending extensions and withdrawals when cancel/close application ([dda602f](https://gitlab.com/fi-sas/fisas-accommodation/commit/dda602f6bda43e95aaebcefc79cc2e6822fbfcc7))
- **residences:** add residences extras and regimes ([6758318](https://gitlab.com/fi-sas/fisas-accommodation/commit/6758318dab68d8f3847817ab5ed63a40bba0fd1b))
- **withdrawals:** resolve error on withdrawal application ([162a21a](https://gitlab.com/fi-sas/fisas-accommodation/commit/162a21a45a8634efd752154457451a808d275615))

### Features

- **applications:** add regime and extras application change request ([67f2172](https://gitlab.com/fi-sas/fisas-accommodation/commit/67f21723d645d2742ceabae296c808c42695c3ca))

## [1.0.1](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0...v1.0.1) (2021-03-31)

### Bug Fixes

- **migration:** fix migrations error rejected value already exists ([02dee9e](https://gitlab.com/fi-sas/fisas-accommodation/commit/02dee9e5cee10c68cf5f80fc43068023ba78a683))

# 1.0.0 (2021-03-09)

### Bug Fixes

- **application-phases:** add validation to date ranges ([311e575](https://gitlab.com/fi-sas/fisas-accommodation/commit/311e57507926a9e73d967c63124a98a220c27193))
- **applications:** add update methods ([b87fea5](https://gitlab.com/fi-sas/fisas-accommodation/commit/b87fea53da9c67664477b62bc8f5764d563d12e6))
- absences extensions widthrawls fill applicatioon_id ([4b76b7f](https://gitlab.com/fi-sas/fisas-accommodation/commit/4b76b7f576769f41ef0aa628d45ab25de681a820))
- accommodation add cancelled status ([159c3c0](https://gitlab.com/fi-sas/fisas-accommodation/commit/159c3c0e3f135aa4ade64bcb4ef969667864d843))
- add new withRelates de extensions absences withdrawals ([0c21f89](https://gitlab.com/fi-sas/fisas-accommodation/commit/0c21f8936301afbba07ea50ae2fb3aafd06136db))
- application validation ([0c002a8](https://gitlab.com/fi-sas/fisas-accommodation/commit/0c002a8a9f2c1b4deff8a2c3422ef8dc16bc25c6))
- change the knex to suport secrets ([f033258](https://gitlab.com/fi-sas/fisas-accommodation/commit/f033258748334982020a99566edc86071709e7a0))
- fix validatios of withdrawals ([e44680d](https://gitlab.com/fi-sas/fisas-accommodation/commit/e44680d20bfa262367a1211dc00a016282809441))
- geral minor fixs ([d2071fe](https://gitlab.com/fi-sas/fisas-accommodation/commit/d2071fe09b5226d0509c5e4a8292d8cf725c9ec6))
- minor fix on contract changes ([1fa5d4f](https://gitlab.com/fi-sas/fisas-accommodation/commit/1fa5d4ff6fdbf9f68542c51a6ed1214154588d4a))
- **/billings:** fix billings query ([eb66e28](https://gitlab.com/fi-sas/fisas-accommodation/commit/eb66e28234fbf9ef631001f158b0fa11b48cad14))
- **applcations:** fix openapplication condition ([7eafaa3](https://gitlab.com/fi-sas/fisas-accommodation/commit/7eafaa33d1607757076d7dc432c3640c3a562654))
- **application:** add condition to filter max permission by academic year ([ca44aba](https://gitlab.com/fi-sas/fisas-accommodation/commit/ca44aba70ba2498e7600dca79d9462c815d5b94c))
- **application:** get room_id from body on contract ([6f656d8](https://gitlab.com/fi-sas/fisas-accommodation/commit/6f656d87f69c1f3215cc760751f69b2cb2ef1ad9))
- **application:** minor fixs ([6331bfe](https://gitlab.com/fi-sas/fisas-accommodation/commit/6331bfe49bbbb9ff3e30aa69cf0b10ec5d57146e))
- **application:** remove admission_date validation ([21bd8e6](https://gitlab.com/fi-sas/fisas-accommodation/commit/21bd8e60be4c473cca7ee90b08c23e0aebd967eb))
- **application:** set has scholarship to true by default ([bb6edb0](https://gitlab.com/fi-sas/fisas-accommodation/commit/bb6edb0152b14b1c82169b2ef2fb1b406042b5b6))
- **applicationPhases:** remove old residenceApplicationsPhases references ([5c724c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/5c724c91f42d66c2843f74fcce2a43d24466f010))
- **applications:** add condition for only 1 opposition ([fc00b91](https://gitlab.com/fi-sas/fisas-accommodation/commit/fc00b91b162ade86b4aeb14c96320572b9184109))
- **applications:** change migration to nullable ([886a529](https://gitlab.com/fi-sas/fisas-accommodation/commit/886a5296a873894b9849f26df37ff61b684ecc8a))
- **applications:** fix change status withut extra data ([e4250c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/e4250c98457206ec1b6808c2c6fbed3b4d283a83))
- **applications:** fix creation of first billings ([a47f1f3](https://gitlab.com/fi-sas/fisas-accommodation/commit/a47f1f3e7c81e7df03497ecfd6231576d35b07d4))
- **applications:** fix stats filters ([ecea283](https://gitlab.com/fi-sas/fisas-accommodation/commit/ecea283c27f368fb235d79407a24604c8d4dc094))
- **applications:** generate first billings ([1bd41fd](https://gitlab.com/fi-sas/fisas-accommodation/commit/1bd41fd76d8192209409d1b97f9414d2eb1384a6))
- **applications:** minor fix ([b3b727a](https://gitlab.com/fi-sas/fisas-accommodation/commit/b3b727aca5a419627e6714ef05cd96d8d427c1c0))
- **billing:** fix billing service ([7a9f2bf](https://gitlab.com/fi-sas/fisas-accommodation/commit/7a9f2bfe116e939fb2af5fa7712bf1e721419f59))
- **billingItem:** fix start of month date ([c6d01b7](https://gitlab.com/fi-sas/fisas-accommodation/commit/c6d01b7ddd40d8430fef7f560357c4b86b899242))
- **billings:** add billing review enpoint ([f797bb7](https://gitlab.com/fi-sas/fisas-accommodation/commit/f797bb71ab7af929e433cdc3fb9f84096a568c58))
- **billings:** add import to billings service ([660e86e](https://gitlab.com/fi-sas/fisas-accommodation/commit/660e86efb566c1912b884d264a5915ae000de439))
- **billings:** add residence filter to reviewInvoices ([9d16581](https://gitlab.com/fi-sas/fisas-accommodation/commit/9d1658139bd8b78fc6cf90ae600408fc8e9ad837))
- **billings:** addd async to reviewInvoices ([585cdbd](https://gitlab.com/fi-sas/fisas-accommodation/commit/585cdbde87085049187bd1f37c2b3ec688af61cf))
- **billings:** billing ite service ([0d36841](https://gitlab.com/fi-sas/fisas-accommodation/commit/0d3684199e5344c3b2bce6f726f8a2e68a0baee7))
- **billings:** fix review billings query ([08e0a84](https://gitlab.com/fi-sas/fisas-accommodation/commit/08e0a84fbe9527cab54a0063f4753910e4f13d4c))
- **billings:** fix reviewinvoices endpoint ([3d74ceb](https://gitlab.com/fi-sas/fisas-accommodation/commit/3d74ceb2ad38445c49671dad367df42172e32324))
- **billings:** review billings by residence ([ddeb2b9](https://gitlab.com/fi-sas/fisas-accommodation/commit/ddeb2b93f94f3b925a17dd52fc528ea6de3e8a6a))
- **billings:** save deposit amount ([ee7ac14](https://gitlab.com/fi-sas/fisas-accommodation/commit/ee7ac14928c2e974aa1a3a433f04cd2575f8bf8d))
- **withdrawals:** add start date validation ([45189eb](https://gitlab.com/fi-sas/fisas-accommodation/commit/45189eb2f5f50a2ad20feb24e17f14f8ba472a84))
- minor issues ([49da576](https://gitlab.com/fi-sas/fisas-accommodation/commit/49da576b77292fbb85c4bd119eae0101b7c5a9a4))
- **app:** remove index route ([96f0ece](https://gitlab.com/fi-sas/fisas-accommodation/commit/96f0ece463746ce0be1fd2ce97db56d6f3a1a943))
- **applcations:** user permissions ([457f3b4](https://gitlab.com/fi-sas/fisas-accommodation/commit/457f3b4fb398f892a1a53a27a4cb9478a073b322))
- **application:** add backoffice middleware ([2a197c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/2a197c9aceafc69574925a1715644f5a8249ee49))
- **application:** add backoffice middleware to status endpoint ([cdef349](https://gitlab.com/fi-sas/fisas-accommodation/commit/cdef349c8bd7be078dad875e8fd345f799a3cce6))
- **application:** add backoffice middleware to status endpoint ([5c9a633](https://gitlab.com/fi-sas/fisas-accommodation/commit/5c9a63390d7e5730c4401f2620def777343407ad))
- **application:** add course_id to application ([832b74b](https://gitlab.com/fi-sas/fisas-accommodation/commit/832b74b4cc4aaceee3767771c36c57b95176a47b))
- **application:** allow null on decision ([1d2c5c4](https://gitlab.com/fi-sas/fisas-accommodation/commit/1d2c5c4f9c300900b7ebb11c630e13a4080d5baa))
- **application:** applicant-infobirth_date field name ([c8fc495](https://gitlab.com/fi-sas/fisas-accommodation/commit/c8fc495688c3d2abaab1bb41b4f3a1c3a39e8483))
- **application:** check user permission ([b80aa19](https://gitlab.com/fi-sas/fisas-accommodation/commit/b80aa194cfbe27d5270f4721e079557063b622c8))
- **application:** course and coruse_degree nullable on db ([27aa16f](https://gitlab.com/fi-sas/fisas-accommodation/commit/27aa16fb192e5751a16595b7e4c31e2da4affd53))
- **application:** course and coruse_degree nullable on db ([cf68339](https://gitlab.com/fi-sas/fisas-accommodation/commit/cf68339a3f47d5da0bc39de115b9c7587af7a09b))
- **application:** fix application validation ([cda55bc](https://gitlab.com/fi-sas/fisas-accommodation/commit/cda55bcc9247bfbb068e82a92b2d88c90986bfab))
- **application:** fix application validation of dates and files ([4f89977](https://gitlab.com/fi-sas/fisas-accommodation/commit/4f89977dcde58d6c1ddb352c314b9038e812bbc0))
- **application:** schema of schollarship value can be 0 ([8bbe55b](https://gitlab.com/fi-sas/fisas-accommodation/commit/8bbe55b2bf54de3e43b974c752a732bb581fd3d3))
- **applications:** add assigned_residence_id to backoffice data ([e145cc8](https://gitlab.com/fi-sas/fisas-accommodation/commit/e145cc8e9747f301218b32b2ea02d42080a2cb14))
- **applications:** add backoffice middleware to patch ([5081d83](https://gitlab.com/fi-sas/fisas-accommodation/commit/5081d83e6117add9f638fbb25efdc2ab3a38b8a2))
- **applications:** add documentation and add validation ([a00d8be](https://gitlab.com/fi-sas/fisas-accommodation/commit/a00d8be29e830627a5e1f6edbcdca16c385a8a78))
- **applications:** add extra info to applicant-info endpoint ([1878160](https://gitlab.com/fi-sas/fisas-accommodation/commit/18781609dead58b6b9ab9235ae339d38590baa7b))
- **applications:** allow observations field to be null ([135dd39](https://gitlab.com/fi-sas/fisas-accommodation/commit/135dd39f07a2f1f1736257ae9e506ba59f9bca00))
- **applications:** fix error on application post on application phase closed ([eaa0159](https://gitlab.com/fi-sas/fisas-accommodation/commit/eaa01592126fd87fe7ac740e0ed971ae0a30f166))
- **applications:** fix error on application post on application phase closed ([2b60a75](https://gitlab.com/fi-sas/fisas-accommodation/commit/2b60a750f3aa1347625273af5bfffb6aa48ae3c4))
- **applications:** fix minor isseus ([3546e19](https://gitlab.com/fi-sas/fisas-accommodation/commit/3546e197185ed9d588b28c5bf17592ec9ab6a884))
- **applications:** fix reopend bypass to analysed ([682f2a4](https://gitlab.com/fi-sas/fisas-accommodation/commit/682f2a436fac483a05b154464db6b5050d2d39dd))
- **applications:** fix swagger ([75b4336](https://gitlab.com/fi-sas/fisas-accommodation/commit/75b433665777748af3ac82fc41dc30184eaa1de1))
- **applications:** has_scholarship is not required ([4545d6b](https://gitlab.com/fi-sas/fisas-accommodation/commit/4545d6b7a7b2c40c88349c34d62494f794538844))
- **applications:** stop checking disponibility of residence on ENQUEUED ([1de3534](https://gitlab.com/fi-sas/fisas-accommodation/commit/1de35348e8ba7a9820ec28d03c9e342b8e0b4368))
- **applications:** stop checking disponibility of residence on ENQUEUED ([920fc28](https://gitlab.com/fi-sas/fisas-accommodation/commit/920fc284135febd47143237d622536384aa695a6))
- **biilingItem:** fix moment start value ([c75813f](https://gitlab.com/fi-sas/fisas-accommodation/commit/c75813ffd2a888033b0578e52582f0a1f554f3ad))
- **billingItem:** add the req to generateAllInvoices ([8ebee51](https://gitlab.com/fi-sas/fisas-accommodation/commit/8ebee5107d6580dd9738365a66d8621cfa5bd5ec))
- **billingItem:** minor fixs ([b25e958](https://gitlab.com/fi-sas/fisas-accommodation/commit/b25e9583c1f4eb20e17b6c9a760c24b10a861b91))
- **billingItem:** minor tweaks ([9a281db](https://gitlab.com/fi-sas/fisas-accommodation/commit/9a281db121bb360fea17d28aa09fc8de7286e542))
- **billings:** fix services update deposit paid ([f73b1cb](https://gitlab.com/fi-sas/fisas-accommodation/commit/f73b1cb48901e0768db801c309ca43c28568e280))
- **billings:** minor date fix ([5826218](https://gitlab.com/fi-sas/fisas-accommodation/commit/58262185e8243e591b35b859faa58e295f114b57))
- **billings:** mispeeled async ([996ccde](https://gitlab.com/fi-sas/fisas-accommodation/commit/996ccde7548a8216f5587982b59a9885be95f632))
- **billintItem:** add cancelled field and inicial development for paid event ([3cfc044](https://gitlab.com/fi-sas/fisas-accommodation/commit/3cfc044e4d7bcf0854d8d2a879bbb638c259f6b8))
- **billlingItem:** minor fixs ([30e8d24](https://gitlab.com/fi-sas/fisas-accommodation/commit/30e8d24dd338549d45554bac8fe2e531e694f308))
- **common:** remove limits on request of relateds ([7f42c41](https://gitlab.com/fi-sas/fisas-accommodation/commit/7f42c41a5d4688bdff650bb14c5c599ad041536f))
- **docker-compose:** create docker-compose to prod and dev ([995d829](https://gitlab.com/fi-sas/fisas-accommodation/commit/995d8299565a472a1c07e52fd2b74bbb7abcb0b6))
- **extras:** add visible to filters ([44a8429](https://gitlab.com/fi-sas/fisas-accommodation/commit/44a84293ab7995adca1603441ed24f3d4288866c))
- **helpers:** add withRelated to withReloteds function ([c266fdc](https://gitlab.com/fi-sas/fisas-accommodation/commit/c266fdca017bc8a2998217d8044f4a921a7b7346))
- **package:** fix the name of the microservice in docker-bash ([678d4e9](https://gitlab.com/fi-sas/fisas-accommodation/commit/678d4e982dda42533c0f90a0be50985f1586e463))
- **regimes:** fix update priceLines ([363ca30](https://gitlab.com/fi-sas/fisas-accommodation/commit/363ca305b1ede9667c4ebe002405ecc77ba3ee90))
- **residences:** occupation from id sent on request ([6940187](https://gitlab.com/fi-sas/fisas-accommodation/commit/69401876b1fb236166cf2c6d1df895cd5c21dafa))
- **residences:** occupation values in case of no room inserted ([6ea89db](https://gitlab.com/fi-sas/fisas-accommodation/commit/6ea89db7e9d26e6577fde5595f29633693855a0b))
- **residences:** remove unused import ([41d6df2](https://gitlab.com/fi-sas/fisas-accommodation/commit/41d6df27d74360badcef869574488dce54ac00a0))
- **rooms:** fix sql error on availables negative value ([7d30e36](https://gitlab.com/fi-sas/fisas-accommodation/commit/7d30e367cf3e5e0bae1d37430c1ee471e712ee0c))
- **typologies:** acept field max_occupants_number ([07a60d8](https://gitlab.com/fi-sas/fisas-accommodation/commit/07a60d8345cb2584957ce7adae0f7d4cf38f33d8))
- **typologies:** acept field max_occupants_number ([dd03492](https://gitlab.com/fi-sas/fisas-accommodation/commit/dd0349288cd2124d2e8cbecf1971a74497240851))
- **withdrawals:** get the active application to withdrawal ([f149f92](https://gitlab.com/fi-sas/fisas-accommodation/commit/f149f928291b4945094a8855938f5ae85890a96d))

### Features

- add house hould elements to application ([798d6d6](https://gitlab.com/fi-sas/fisas-accommodation/commit/798d6d6f8b8ff8cea38c37ceb7c8b29e8dd63a7f))
- add ms infrastructure connection ([14fea0e](https://gitlab.com/fi-sas/fisas-accommodation/commit/14fea0eb4d48061b34d15c1ef7afe8e56a0cb8ff))
- **application:** add the limit of 1 active application academic year ([e06bccc](https://gitlab.com/fi-sas/fisas-accommodation/commit/e06bccc87d8d0052d7c2bdde2a196035e41bbb2c))
- **applicationPhases:** add applicationPhases entity ([162c5a3](https://gitlab.com/fi-sas/fisas-accommodation/commit/162c5a35e6b210df2326c3208920f74ae2db2233))
- **applications:** add final result endpoint ([a8cc8fe](https://gitlab.com/fi-sas/fisas-accommodation/commit/a8cc8fe6994543f7bbea9befd076571fa2c85bfc))
- **billings:** add option to fixed payment_method ([78839f7](https://gitlab.com/fi-sas/fisas-accommodation/commit/78839f75cd9ec7bc1ebd82933a5c98068f845ade))
- **billings:** add possible retroative pre calculation ([bc29b20](https://gitlab.com/fi-sas/fisas-accommodation/commit/bc29b2076bf985f762562c972bc3213359d76073))
- **billings:** add review billing ([8b1a5e8](https://gitlab.com/fi-sas/fisas-accommodation/commit/8b1a5e837ee9eba9d74ad12ed20f83e9493c1aa4))
- **billings:** initial version of billings ([c5923af](https://gitlab.com/fi-sas/fisas-accommodation/commit/c5923af5d398a82b500b25310c5b6b2d2fd84a1b))
- **residences:** add gallery of images to residences ([bae69d7](https://gitlab.com/fi-sas/fisas-accommodation/commit/bae69d707602ab38855cec4cee9ba6dab58ab71d))
- **residences:** add occupations management to rooms ([fddcbdb](https://gitlab.com/fi-sas/fisas-accommodation/commit/fddcbdb51001f9768be9f5c403b1dbd13e518016))
- **typology:** add max_occupants_number field ([3c4a1d6](https://gitlab.com/fi-sas/fisas-accommodation/commit/3c4a1d6ddcac17b5943fd443d8d8da1098e6d33c))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-03-04)

### Bug Fixes

- **application-phases:** add validation to date ranges ([311e575](https://gitlab.com/fi-sas/fisas-accommodation/commit/311e57507926a9e73d967c63124a98a220c27193))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-03-01)

### Bug Fixes

- **applications:** add update methods ([b87fea5](https://gitlab.com/fi-sas/fisas-accommodation/commit/b87fea53da9c67664477b62bc8f5764d563d12e6))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-02-24)

### Bug Fixes

- geral minor fixs ([d2071fe](https://gitlab.com/fi-sas/fisas-accommodation/commit/d2071fe09b5226d0509c5e4a8292d8cf725c9ec6))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-02-09)

### Bug Fixes

- change the knex to suport secrets ([f033258](https://gitlab.com/fi-sas/fisas-accommodation/commit/f033258748334982020a99566edc86071709e7a0))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-27)

### Features

- add house hould elements to application ([798d6d6](https://gitlab.com/fi-sas/fisas-accommodation/commit/798d6d6f8b8ff8cea38c37ceb7c8b29e8dd63a7f))
- add ms infrastructure connection ([14fea0e](https://gitlab.com/fi-sas/fisas-accommodation/commit/14fea0eb4d48061b34d15c1ef7afe8e56a0cb8ff))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-14)

### Bug Fixes

- fix validatios of withdrawals ([e44680d](https://gitlab.com/fi-sas/fisas-accommodation/commit/e44680d20bfa262367a1211dc00a016282809441))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-14)

### Bug Fixes

- add new withRelates de extensions absences withdrawals ([0c21f89](https://gitlab.com/fi-sas/fisas-accommodation/commit/0c21f8936301afbba07ea50ae2fb3aafd06136db))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-14)

### Bug Fixes

- absences extensions widthrawls fill applicatioon_id ([4b76b7f](https://gitlab.com/fi-sas/fisas-accommodation/commit/4b76b7f576769f41ef0aa628d45ab25de681a820))
- minor fix on contract changes ([1fa5d4f](https://gitlab.com/fi-sas/fisas-accommodation/commit/1fa5d4ff6fdbf9f68542c51a6ed1214154588d4a))
- **withdrawals:** add start date validation ([45189eb](https://gitlab.com/fi-sas/fisas-accommodation/commit/45189eb2f5f50a2ad20feb24e17f14f8ba472a84))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-01-12)

### Bug Fixes

- accommodation add cancelled status ([159c3c0](https://gitlab.com/fi-sas/fisas-accommodation/commit/159c3c0e3f135aa4ade64bcb4ef969667864d843))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-01-12)

### Bug Fixes

- **applications:** fix change status withut extra data ([e4250c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/e4250c98457206ec1b6808c2c6fbed3b4d283a83))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-accommodation/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-12)

### Bug Fixes

- application validation ([0c002a8](https://gitlab.com/fi-sas/fisas-accommodation/commit/0c002a8a9f2c1b4deff8a2c3422ef8dc16bc25c6))

# 1.0.0-rc.1 (2021-01-04)

### Bug Fixes

- minor issues ([49da576](https://gitlab.com/fi-sas/fisas-accommodation/commit/49da576b77292fbb85c4bd119eae0101b7c5a9a4))
- **/billings:** fix billings query ([eb66e28](https://gitlab.com/fi-sas/fisas-accommodation/commit/eb66e28234fbf9ef631001f158b0fa11b48cad14))
- **app:** remove index route ([96f0ece](https://gitlab.com/fi-sas/fisas-accommodation/commit/96f0ece463746ce0be1fd2ce97db56d6f3a1a943))
- **applcations:** fix openapplication condition ([7eafaa3](https://gitlab.com/fi-sas/fisas-accommodation/commit/7eafaa33d1607757076d7dc432c3640c3a562654))
- **applcations:** user permissions ([457f3b4](https://gitlab.com/fi-sas/fisas-accommodation/commit/457f3b4fb398f892a1a53a27a4cb9478a073b322))
- **application:** add backoffice middleware ([2a197c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/2a197c9aceafc69574925a1715644f5a8249ee49))
- **application:** add backoffice middleware to status endpoint ([cdef349](https://gitlab.com/fi-sas/fisas-accommodation/commit/cdef349c8bd7be078dad875e8fd345f799a3cce6))
- **application:** add backoffice middleware to status endpoint ([5c9a633](https://gitlab.com/fi-sas/fisas-accommodation/commit/5c9a63390d7e5730c4401f2620def777343407ad))
- **application:** add condition to filter max permission by academic year ([ca44aba](https://gitlab.com/fi-sas/fisas-accommodation/commit/ca44aba70ba2498e7600dca79d9462c815d5b94c))
- **application:** add course_id to application ([832b74b](https://gitlab.com/fi-sas/fisas-accommodation/commit/832b74b4cc4aaceee3767771c36c57b95176a47b))
- **application:** allow null on decision ([1d2c5c4](https://gitlab.com/fi-sas/fisas-accommodation/commit/1d2c5c4f9c300900b7ebb11c630e13a4080d5baa))
- **application:** applicant-infobirth_date field name ([c8fc495](https://gitlab.com/fi-sas/fisas-accommodation/commit/c8fc495688c3d2abaab1bb41b4f3a1c3a39e8483))
- **application:** check user permission ([b80aa19](https://gitlab.com/fi-sas/fisas-accommodation/commit/b80aa194cfbe27d5270f4721e079557063b622c8))
- **application:** course and coruse_degree nullable on db ([27aa16f](https://gitlab.com/fi-sas/fisas-accommodation/commit/27aa16fb192e5751a16595b7e4c31e2da4affd53))
- **application:** course and coruse_degree nullable on db ([cf68339](https://gitlab.com/fi-sas/fisas-accommodation/commit/cf68339a3f47d5da0bc39de115b9c7587af7a09b))
- **application:** fix application validation ([cda55bc](https://gitlab.com/fi-sas/fisas-accommodation/commit/cda55bcc9247bfbb068e82a92b2d88c90986bfab))
- **application:** fix application validation of dates and files ([4f89977](https://gitlab.com/fi-sas/fisas-accommodation/commit/4f89977dcde58d6c1ddb352c314b9038e812bbc0))
- **application:** get room_id from body on contract ([6f656d8](https://gitlab.com/fi-sas/fisas-accommodation/commit/6f656d87f69c1f3215cc760751f69b2cb2ef1ad9))
- **application:** minor fixs ([6331bfe](https://gitlab.com/fi-sas/fisas-accommodation/commit/6331bfe49bbbb9ff3e30aa69cf0b10ec5d57146e))
- **application:** remove admission_date validation ([21bd8e6](https://gitlab.com/fi-sas/fisas-accommodation/commit/21bd8e60be4c473cca7ee90b08c23e0aebd967eb))
- **application:** schema of schollarship value can be 0 ([8bbe55b](https://gitlab.com/fi-sas/fisas-accommodation/commit/8bbe55b2bf54de3e43b974c752a732bb581fd3d3))
- **application:** set has scholarship to true by default ([bb6edb0](https://gitlab.com/fi-sas/fisas-accommodation/commit/bb6edb0152b14b1c82169b2ef2fb1b406042b5b6))
- **applicationPhases:** remove old residenceApplicationsPhases references ([5c724c9](https://gitlab.com/fi-sas/fisas-accommodation/commit/5c724c91f42d66c2843f74fcce2a43d24466f010))
- **applications:** add assigned_residence_id to backoffice data ([e145cc8](https://gitlab.com/fi-sas/fisas-accommodation/commit/e145cc8e9747f301218b32b2ea02d42080a2cb14))
- **applications:** add backoffice middleware to patch ([5081d83](https://gitlab.com/fi-sas/fisas-accommodation/commit/5081d83e6117add9f638fbb25efdc2ab3a38b8a2))
- **applications:** add condition for only 1 opposition ([fc00b91](https://gitlab.com/fi-sas/fisas-accommodation/commit/fc00b91b162ade86b4aeb14c96320572b9184109))
- **applications:** add documentation and add validation ([a00d8be](https://gitlab.com/fi-sas/fisas-accommodation/commit/a00d8be29e830627a5e1f6edbcdca16c385a8a78))
- **applications:** add extra info to applicant-info endpoint ([1878160](https://gitlab.com/fi-sas/fisas-accommodation/commit/18781609dead58b6b9ab9235ae339d38590baa7b))
- **applications:** allow observations field to be null ([135dd39](https://gitlab.com/fi-sas/fisas-accommodation/commit/135dd39f07a2f1f1736257ae9e506ba59f9bca00))
- **applications:** change migration to nullable ([886a529](https://gitlab.com/fi-sas/fisas-accommodation/commit/886a5296a873894b9849f26df37ff61b684ecc8a))
- **applications:** fix creation of first billings ([a47f1f3](https://gitlab.com/fi-sas/fisas-accommodation/commit/a47f1f3e7c81e7df03497ecfd6231576d35b07d4))
- **applications:** fix error on application post on application phase closed ([eaa0159](https://gitlab.com/fi-sas/fisas-accommodation/commit/eaa01592126fd87fe7ac740e0ed971ae0a30f166))
- **applications:** fix error on application post on application phase closed ([2b60a75](https://gitlab.com/fi-sas/fisas-accommodation/commit/2b60a750f3aa1347625273af5bfffb6aa48ae3c4))
- **applications:** fix minor isseus ([3546e19](https://gitlab.com/fi-sas/fisas-accommodation/commit/3546e197185ed9d588b28c5bf17592ec9ab6a884))
- **applications:** fix reopend bypass to analysed ([682f2a4](https://gitlab.com/fi-sas/fisas-accommodation/commit/682f2a436fac483a05b154464db6b5050d2d39dd))
- **applications:** fix stats filters ([ecea283](https://gitlab.com/fi-sas/fisas-accommodation/commit/ecea283c27f368fb235d79407a24604c8d4dc094))
- **applications:** fix swagger ([75b4336](https://gitlab.com/fi-sas/fisas-accommodation/commit/75b433665777748af3ac82fc41dc30184eaa1de1))
- **applications:** generate first billings ([1bd41fd](https://gitlab.com/fi-sas/fisas-accommodation/commit/1bd41fd76d8192209409d1b97f9414d2eb1384a6))
- **applications:** has_scholarship is not required ([4545d6b](https://gitlab.com/fi-sas/fisas-accommodation/commit/4545d6b7a7b2c40c88349c34d62494f794538844))
- **applications:** minor fix ([b3b727a](https://gitlab.com/fi-sas/fisas-accommodation/commit/b3b727aca5a419627e6714ef05cd96d8d427c1c0))
- **applications:** stop checking disponibility of residence on ENQUEUED ([1de3534](https://gitlab.com/fi-sas/fisas-accommodation/commit/1de35348e8ba7a9820ec28d03c9e342b8e0b4368))
- **applications:** stop checking disponibility of residence on ENQUEUED ([920fc28](https://gitlab.com/fi-sas/fisas-accommodation/commit/920fc284135febd47143237d622536384aa695a6))
- **biilingItem:** fix moment start value ([c75813f](https://gitlab.com/fi-sas/fisas-accommodation/commit/c75813ffd2a888033b0578e52582f0a1f554f3ad))
- **billing:** fix billing service ([7a9f2bf](https://gitlab.com/fi-sas/fisas-accommodation/commit/7a9f2bfe116e939fb2af5fa7712bf1e721419f59))
- **billingItem:** add the req to generateAllInvoices ([8ebee51](https://gitlab.com/fi-sas/fisas-accommodation/commit/8ebee5107d6580dd9738365a66d8621cfa5bd5ec))
- **billingItem:** fix start of month date ([c6d01b7](https://gitlab.com/fi-sas/fisas-accommodation/commit/c6d01b7ddd40d8430fef7f560357c4b86b899242))
- **billingItem:** minor fixs ([b25e958](https://gitlab.com/fi-sas/fisas-accommodation/commit/b25e9583c1f4eb20e17b6c9a760c24b10a861b91))
- **billingItem:** minor tweaks ([9a281db](https://gitlab.com/fi-sas/fisas-accommodation/commit/9a281db121bb360fea17d28aa09fc8de7286e542))
- **billings:** add billing review enpoint ([f797bb7](https://gitlab.com/fi-sas/fisas-accommodation/commit/f797bb71ab7af929e433cdc3fb9f84096a568c58))
- **billings:** add import to billings service ([660e86e](https://gitlab.com/fi-sas/fisas-accommodation/commit/660e86efb566c1912b884d264a5915ae000de439))
- **billings:** add residence filter to reviewInvoices ([9d16581](https://gitlab.com/fi-sas/fisas-accommodation/commit/9d1658139bd8b78fc6cf90ae600408fc8e9ad837))
- **billings:** addd async to reviewInvoices ([585cdbd](https://gitlab.com/fi-sas/fisas-accommodation/commit/585cdbde87085049187bd1f37c2b3ec688af61cf))
- **billings:** billing ite service ([0d36841](https://gitlab.com/fi-sas/fisas-accommodation/commit/0d3684199e5344c3b2bce6f726f8a2e68a0baee7))
- **billings:** fix review billings query ([08e0a84](https://gitlab.com/fi-sas/fisas-accommodation/commit/08e0a84fbe9527cab54a0063f4753910e4f13d4c))
- **billings:** fix reviewinvoices endpoint ([3d74ceb](https://gitlab.com/fi-sas/fisas-accommodation/commit/3d74ceb2ad38445c49671dad367df42172e32324))
- **billings:** fix services update deposit paid ([f73b1cb](https://gitlab.com/fi-sas/fisas-accommodation/commit/f73b1cb48901e0768db801c309ca43c28568e280))
- **billings:** minor date fix ([5826218](https://gitlab.com/fi-sas/fisas-accommodation/commit/58262185e8243e591b35b859faa58e295f114b57))
- **billings:** mispeeled async ([996ccde](https://gitlab.com/fi-sas/fisas-accommodation/commit/996ccde7548a8216f5587982b59a9885be95f632))
- **billings:** review billings by residence ([ddeb2b9](https://gitlab.com/fi-sas/fisas-accommodation/commit/ddeb2b93f94f3b925a17dd52fc528ea6de3e8a6a))
- **billings:** save deposit amount ([ee7ac14](https://gitlab.com/fi-sas/fisas-accommodation/commit/ee7ac14928c2e974aa1a3a433f04cd2575f8bf8d))
- **billintItem:** add cancelled field and inicial development for paid event ([3cfc044](https://gitlab.com/fi-sas/fisas-accommodation/commit/3cfc044e4d7bcf0854d8d2a879bbb638c259f6b8))
- **billlingItem:** minor fixs ([30e8d24](https://gitlab.com/fi-sas/fisas-accommodation/commit/30e8d24dd338549d45554bac8fe2e531e694f308))
- **common:** remove limits on request of relateds ([7f42c41](https://gitlab.com/fi-sas/fisas-accommodation/commit/7f42c41a5d4688bdff650bb14c5c599ad041536f))
- **docker-compose:** create docker-compose to prod and dev ([995d829](https://gitlab.com/fi-sas/fisas-accommodation/commit/995d8299565a472a1c07e52fd2b74bbb7abcb0b6))
- **extras:** add visible to filters ([44a8429](https://gitlab.com/fi-sas/fisas-accommodation/commit/44a84293ab7995adca1603441ed24f3d4288866c))
- **helpers:** add withRelated to withReloteds function ([c266fdc](https://gitlab.com/fi-sas/fisas-accommodation/commit/c266fdca017bc8a2998217d8044f4a921a7b7346))
- **package:** fix the name of the microservice in docker-bash ([678d4e9](https://gitlab.com/fi-sas/fisas-accommodation/commit/678d4e982dda42533c0f90a0be50985f1586e463))
- **regimes:** fix update priceLines ([363ca30](https://gitlab.com/fi-sas/fisas-accommodation/commit/363ca305b1ede9667c4ebe002405ecc77ba3ee90))
- **residences:** occupation from id sent on request ([6940187](https://gitlab.com/fi-sas/fisas-accommodation/commit/69401876b1fb236166cf2c6d1df895cd5c21dafa))
- **residences:** occupation values in case of no room inserted ([6ea89db](https://gitlab.com/fi-sas/fisas-accommodation/commit/6ea89db7e9d26e6577fde5595f29633693855a0b))
- **residences:** remove unused import ([41d6df2](https://gitlab.com/fi-sas/fisas-accommodation/commit/41d6df27d74360badcef869574488dce54ac00a0))
- **rooms:** fix sql error on availables negative value ([7d30e36](https://gitlab.com/fi-sas/fisas-accommodation/commit/7d30e367cf3e5e0bae1d37430c1ee471e712ee0c))
- **typologies:** acept field max_occupants_number ([07a60d8](https://gitlab.com/fi-sas/fisas-accommodation/commit/07a60d8345cb2584957ce7adae0f7d4cf38f33d8))
- **typologies:** acept field max_occupants_number ([dd03492](https://gitlab.com/fi-sas/fisas-accommodation/commit/dd0349288cd2124d2e8cbecf1971a74497240851))
- **withdrawals:** get the active application to withdrawal ([f149f92](https://gitlab.com/fi-sas/fisas-accommodation/commit/f149f928291b4945094a8855938f5ae85890a96d))

### Features

- **application:** add the limit of 1 active application academic year ([e06bccc](https://gitlab.com/fi-sas/fisas-accommodation/commit/e06bccc87d8d0052d7c2bdde2a196035e41bbb2c))
- **applicationPhases:** add applicationPhases entity ([162c5a3](https://gitlab.com/fi-sas/fisas-accommodation/commit/162c5a35e6b210df2326c3208920f74ae2db2233))
- **applications:** add final result endpoint ([a8cc8fe](https://gitlab.com/fi-sas/fisas-accommodation/commit/a8cc8fe6994543f7bbea9befd076571fa2c85bfc))
- **billings:** add option to fixed payment_method ([78839f7](https://gitlab.com/fi-sas/fisas-accommodation/commit/78839f75cd9ec7bc1ebd82933a5c98068f845ade))
- **billings:** add possible retroative pre calculation ([bc29b20](https://gitlab.com/fi-sas/fisas-accommodation/commit/bc29b2076bf985f762562c972bc3213359d76073))
- **billings:** add review billing ([8b1a5e8](https://gitlab.com/fi-sas/fisas-accommodation/commit/8b1a5e837ee9eba9d74ad12ed20f83e9493c1aa4))
- **billings:** initial version of billings ([c5923af](https://gitlab.com/fi-sas/fisas-accommodation/commit/c5923af5d398a82b500b25310c5b6b2d2fd84a1b))
- **residences:** add gallery of images to residences ([bae69d7](https://gitlab.com/fi-sas/fisas-accommodation/commit/bae69d707602ab38855cec4cee9ba6dab58ab71d))
- **residences:** add occupations management to rooms ([fddcbdb](https://gitlab.com/fi-sas/fisas-accommodation/commit/fddcbdb51001f9768be9f5c403b1dbd13e518016))
- **typology:** add max_occupants_number field ([3c4a1d6](https://gitlab.com/fi-sas/fisas-accommodation/commit/3c4a1d6ddcac17b5943fd443d8d8da1098e6d33c))
