module.exports.up = async (db) =>
	db.schema
		.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
		.createTable("service", (table) => {
			// service
			table.increments();
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("service_translation", (table) => {
			table.increments();
			table
				.integer("service_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("service");
			table.integer("language_id").notNullable().unsigned();
			table.string("name", 255).notNullable();
			table.unique(["service_id", "language_id"]);
		})
		// typology
		.createTable("typology", (table) => {
			table.increments();
			table
				.integer("max_occupants_number")
				.after("id")
				.unsigned()
				.notNullable()
				.defaultTo(0);
			table.string("product_code", 120).notNullable();
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("typology_translation", (table) => {
			table.increments();
			table
				.integer("typology_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("typology");
			table.integer("language_id").notNullable().unsigned();
			table.string("name", 255).notNullable();
			table.unique(["typology_id", "language_id"]);
		})

		.createTable("application_phase", (table) => {
			table.increments();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.datetime("out_of_date_from").nullable();
			table.string("academic_year", 10).notNullable();
			table.integer("application_phase").unsigned().notNullable().defaultTo(0);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		//EXTRA
		.createTable("extra", (table) => {
			table.increments();
			table.string("product_code", 120).notNullable();
			table.string("billing_name", 120).notNullable();
			table.float("price").notNullable();
			table.integer("vat_id").notNullable();
			table.boolean("is_deposit").default(0).notNullable();
			table.boolean("visible").notNullable();
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		// EXTRA TRANSLATION
		.createTable("extra_translation", (table) => {
			table.increments();
			table
				.integer("extra_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("extra");
			table.integer("language_id").notNullable().unsigned();
			table.string("name", 255).notNullable();
			table.unique(["extra_id", "language_id"]);
		})
		// REGIME
		.createTable("regime", (table) => {
			table.increments();
			table.string("product_code", 120).notNullable();
			table.string("billing_name", 120).notNullable();
			table.boolean("appear_billing").notNullable();
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		// REGIME TRANSLATION
		.createTable("regime_translation", (table) => {
			table.increments();
			table.integer("regime_id").unsigned().references("id").inTable("regime").onDelete("CASCADE");
			table.integer("language_id").notNullable().unsigned();
			table.string("name", 50).notNullable();
			table.text("description");
			table.unique(["regime_id", "language_id"]);
		})
		// residence
		.createTable("residence", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("address", 255).notNullable();
			table.string("city", 255).notNullable();
			table.string("postal_code", 255).notNullable();
			table.string("phone_1", 255).notNullable();
			table.string("phone_2", 255).notNullable();
			table.string("email", 255).notNullable();
			table.integer("building_id").notNullable();
			table.integer("regulation_file_id").notNullable();
			table.integer("contract_file_id").notNullable();
			table.boolean("charge_deposit").notNullable().defaultTo(0);
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// RESIDENCE MEDIA
		.createTable("residence_media", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("file_id").unsigned();
			table.unique(["residence_id", "file_id"]);
		})

		// RESIDENCE REGIME
		.createTable("residence_regime", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("regime_id").unsigned().references("id").inTable("regime");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique(["residence_id", "regime_id"]);
		})

		// RESIDENCE EXTRA
		.createTable("residence_extra", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("extra_id").unsigned().references("id").inTable("extra");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique(["residence_id", "extra_id"]);
		})

		// residence - typology many-to-many relationship
		.createTable("residence_typology", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology");
			table.unique(["residence_id", "typology_id"]);
		})

		// residence - service many-to-many relationship
		.createTable("residence_service", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table
				.integer("service_id")
				.unsigned()
				.references("id")
				.inTable("service");
			table.unique(["residence_id", "service_id"]);
		})

		// residence - responsible user many-to-many relationship
		.createTable("residence_responsible", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("user_id").unsigned();
			table.unique(["residence_id", "user_id"]);
		})

		// residence - wing responsible user many-to-many relationship
		.createTable("residence_wing_responsible", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("user_id").unsigned();
			table.unique(["residence_id", "user_id"]);
		})

		// residence - kitchen responsible user many-to-many relationship
		.createTable("residence_kitchen_responsible", (table) => {
			table.increments();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("user_id").unsigned();
			table.unique(["residence_id", "user_id"]);
		})

		// room
		.createTable("room", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.integer("room_id").notNullable();
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology");
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.boolean("allow_booking").notNullable().defaultTo(true);
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// booking
		.createTable("booking", (table) => {
			table.increments();
			table.string("customer_full_name", 255).notNullable();
			table.string("customer_address", 255).notNullable();
			table.string("customer_city", 255).notNullable();
			table.string("customer_postal_code", 255).notNullable();
			table.string("customer_phone_1", 255).notNullable();
			table.string("customer_phone_2", 255).notNullable();
			table.string("customer_email", 255).notNullable();
			table
				.integer("pretended_residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.datetime("check_in_date").notNullable();
			table.datetime("check_out_date").notNullable();
			table.integer("n_rooms").notNullable();
			table.integer("n_adults").notNullable();
			table.integer("n_children").notNullable();
			table.string("booking_regime").notNullable();
			table.string("additional_info");
			table.string("estimated_check_in_time").notNullable();
			table.string("status", 45).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// price line
		.createTable("price_line", (table) => {
			table.increments();
			table
				.enu("period", ["DAY", "WEEK", "MONTH"], {
					useNative: true,
					enumName: "price_line_periods",
				})
				.notNullable();
			table.float("price_scholarship").notNullable();
			table.integer("vat_id").notNullable();
			table
				.integer("regime_id")
				.unsigned()
				.references("id")
				.inTable("regime")
				.nullable();
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology")
				.nullable();
			table.float("price").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		// application
		.createTable("application", (table) => {
			table.increments();
			table.uuid("uuid").default(db.raw("uuid_generate_v4()")).notNullable();
			table.string("full_name", 255).notNullable();
			table.string("identification", 255).notNullable();
			table.string("tin", 255).notNullable();
			table.string("student_number", 255).notNullable();
			table.integer("course_id").notNullable();
			table.string("course", 255).nullable();
			table.string("course_degree", 255).nullable();
			table.string("academic_year", 10).notNullable().after("tin");
			table.integer("course_year").notNullable();
			table.datetime("registry_validity").notNullable();
			table.datetime("admission_date").notNullable();
			table.string("email", 255).notNullable();
			table.string("phone_1", 255).notNullable();
			table.string("phone_2", 255).nullable();
			table.string("address", 255).notNullable();
			table.string("city", 255).notNullable();
			table.datetime("start_date");
			table.datetime("end_date");
			table.integer("room_id").unsigned().references("id").inTable("room");
			table.integer("user_id").unsigned();
			table.integer("signed_contract_file_id").unsigned();
			table.integer("liability_term_file_id").unsigned();
			table.string("postal_code", 255).notNullable();
			table.datetime("applicant_birth_date");
			table.string("parent_profession_father", 255).nullable(); // HOUSEHOULD ELEMENTS
			table.string("parent_profession_mother", 255).nullable(); // HOUSEHOULD ELEMENTS
			table.integer("household_elements_number").unsigned();
			table.float("household_total_income");
			table.float("household_accounts_balance");
			table.integer("household_distance").notNullable().defaultTo(0);
			table.integer("income_statement_file_id").unsigned().nullable(); // APPLICATIONS FILES
			table.integer("payment_method_id").unsigned();
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table
				.integer("second_option_residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.datetime("submission_date");
			table.boolean("student_draft").notNullable().defaultTo(0);
			table.boolean("submitted_out_of_date").notNullable().defaultTo(0);
			table
				.integer("assigned_residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table.integer("regime_id").unsigned().references("id").inTable("regime");
			table.boolean("has_used_residences_before").notNullable();
			table.boolean("has_scholarship").notNullable().defaultTo(0);
			table
				.integer("which_residence_id")
				.unsigned()
				.references("id")
				.inTable("residence");
			table
				.integer("which_room_id")
				.unsigned()
				.references("id")
				.inTable("room");
			table.boolean("applied_scholarship").notNullable();
			table.float("scholarship_value");
			table.string("iban", 255);
			table.string("swift", 255);
			table.boolean("applicant_consent").notNullable();
			table.integer("application_phase").unsigned().notNullable().defaultTo(0);
			table
				.integer("application_phase_id")
				.unsigned()
				.references("id")
				.inTable("application_phase");
			table.datetime("consent_date").notNullable();
			table
				.enum(
					"status",
					[
						"draft",
						"submitted",
						"analysed",
						"pending",
						"assigned",
						"unassigned",
						"queued",
						"confirmed",
						"rejected",
						"opposition",
						"contracted",
						"cancelled",
						"closed",
					],
					{ useNative: true, enumName: "applications_status" }
				)
				.defaultTo("submitted")
				.notNullable();
			table
				.enum("decision", ["ASSIGN", "UNASSIGN", "ENQUEUE"], {
					useNative: true,
					enumName: "applications_decision",
				})
				.nullable();
			table.string("opposition_request").nullable();
			table.string("opposition_answer").nullable();
			table.boolean("has_opposed").notNullable().defaultTo(false);
			table.text("observations");
			table
				.enu(
					"billing_status",
					[
						"waiting",
						"proccessing",
						"proccessed",
						"verifieing",
						"verified",
						"error_in_proccessing",
						"error_in_verifieing",
					],
					{ useNative: true, enumName: "billing_status" }
				)
				.defaultTo("waiting")
				.notNullable();
			table.float("total_unbilled").defaultTo(0).notNullable();
			table.float("total_unpaid").defaultTo(0).notNullable();
			table.date("next_period").nullable();
			table.float("charged_deposit").nullable();
			table.float("paid_deposit").nullable();
			table.float("available_deposit").nullable();
			table.float("available_retroactive").nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("absence", (table) => {
			// absence
			table.increments();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.string("reason");
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application");
			table.boolean("allow_booking").notNullable().defaultTo(true);
			table.integer("user_id").unsigned().notNullable();
			table.integer("file_id").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// withdrawal
		.createTable("withdrawal", (table) => {
			table.increments();
			table.datetime("end_date").notNullable();
			table.string("reason");
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application");
			table.boolean("allow_booking").notNullable().defaultTo(true);
			table.integer("file_id").unsigned();
			table.integer("user_id").unsigned().notNullable();
			table.string("status");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("booking_history", (table) => {
			// booking status history
			table.increments();
			table
				.integer("booking_id")
				.unsigned()
				.references("id")
				.inTable("booking");
			table.string("status").notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// application status history
		.createTable("application_history", (table) => {
			table.increments();
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application");
			table.string("status").notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("withdrawal_history", (table) => {
			table.increments();
			table
				.integer("withdrawal_id")
				.unsigned()
				.references("id")
				.inTable("withdrawal");
			table.string("status").notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// extension table
		.createTable("extension", (table) => {
			table.increments();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.string("reason");
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application");
			table.integer("user_id").unsigned();
			table.string("status");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// extension status history
		.createTable("extension_history", (table) => {
			table.increments();
			table
				.integer("extension_id")
				.unsigned()
				.references("id")
				.inTable("extension");
			table.string("status").notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("booking_room", (table) => {
			table.increments();
			table
				.integer("booking_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("booking");
			table
				.integer("room_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("room");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique(["booking_id", "room_id"]);
		})
		// billingItem
		.createTable("billing_item", (table) => {
			table.increments();
			table.string("product_code", 120).notNullable();
			table.string("name", 200).notNullable();
			table.string("description", 255).notNullable().defaultTo("-");
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application")
				.notNullable();
			table
				.integer("room_id")
				.unsigned()
				.references("id")
				.inTable("room")
				.nullable();
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology")
				.nullable();
			table
				.integer("regime_id")
				.unsigned()
				.references("id")
				.inTable("regime")
				.nullable();
			table
				.integer("extra_id")
				.unsigned()
				.references("id")
				.inTable("extra")
				.nullable();
			table.float("quantity").notNullable();
			table.float("unit_price").notNullable();
			table.float("price").notNullable();
			table.integer("vat_id").notNullable();
			table.float("vat_value").notNullable();
			table
				.enu("period", ["DAY", "WEEK", "MONTH"], {
					useNative: true,
					existingType: true,
					enumName: "price_line_periods",
				})
				.notNullable();
			table.integer("month").notNullable().unsigned();
			table.integer("year").notNullable().unsigned();
			table.boolean("paid").notNullable().defaultTo(0);
			table.boolean("billed").notNullable().defaultTo(0);
			table.boolean("cancelled").default(0).notNullable();
			table.datetime("paid_at").nullable();
			table.datetime("cancelled_at").nullable();
			table.boolean("is_deposit").notNullable().defaultTo(0);
			table.integer("payment_method_id").nullable();
			table.integer("billed_month").nullable();
			table.integer("billed_year").nullable();
			table.integer("order_id").nullable();
			table.float("possible_retroactive").nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("application_extra", (table) => {
			table.increments();
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application");
			table.integer("extra_id").unsigned().references("id").inTable("extra");
			table.unique(["application_id", "extra_id"]);
		})
		.createTable("metrics", (table) => {
			table.increments();
			table.date("date").notNullable();
			table.float("occupation_rate").notNullable().unsigned().defaultTo(0);
			table.float("maximum_capacity").notNullable().unsigned().defaultTo(0);
			table.float("beds_occupation").notNullable().unsigned().defaultTo(0);
			table.float("diary_receipt").notNullable().unsigned().defaultTo(0);
			table.float("adr").notNullable().unsigned().defaultTo(0);
			table.float("revpab").notNullable().unsigned().defaultTo(0);
			table.float("trevpab").notNullable().unsigned().defaultTo(0);
			table
				.integer("residence_id")
				.unsigned()
				.references("id")
				.inTable("residence")
				.nullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("service")
		.dropTable("service_translation")
		.dropTable("price_line")
		.dropTable("typology")
		.dropTable("typology_translation")
		.dropTable("residence")
		.dropTable("residence_media")
		.dropTable("residence_regime")
		.dropTable("residence_extra")
		.dropTable("residence_typology")
		.dropTable("residence_service")
		.dropTable("residence_responsible")
		.dropTable("residence_wing_responsible")
		.dropTable("residence_kitchen_responsible")
		.dropTable("room")
		.dropTable("booking")
		.dropTable("regime")
		.dropTable("regime_translation")
		.dropTable("application")
		.dropTable("absence")
		.dropTable("withdrawal")
		.dropTable("booking_history")
		.dropTable("application_history")
		.dropTable("withdrawal_history")
		.dropTable("extension")
		.dropTable("extension_history")
		.dropTable("booking_room")
		.dropTable("extra")
		.dropTable("extra_translation")
		.dropTable("billing_item")
		.dropTable("application_extra")
		.dropTable("metrics")
		.dropTable("application_phase");

module.exports.configuration = { transaction: true };
