
exports.up = function (knex) {
	return knex.schema
		.alterTable("application", (table) => {
			table.dropColumn("payment_method_id");
		})
		.alterTable("billing_item", (table) => {
			table.dropColumn("payment_method_id");
		});
};

exports.down = function (knex) {
	return knex.schema
		.alterTable("application", (table) => {
			table.integer("payment_method_id").unsigned();
		})
		.alterTable("billing_item", (table) => {
			table.integer("payment_method_id").nullable();
		});
};
