
exports.up = function (knex) {
	return knex.schema
		.alterTable("billing_item", (table) => {
			table.uuid("payment_method_id").nullable();
		});
};

exports.down = function (knex) {
	return knex.schema
		.alterTable("billing_item", (table) => {
			table.dropColumn("payment_method_id");
		});
};
