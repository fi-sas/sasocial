module.exports.up = async (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("parent_profession_father");
			table.dropColumn("parent_profession_mother");
		})
		.createTable("house_hould_element", (table) => {
			table.increments();
			table
				.integer("application_id")
				.unsigned()
				.references("id")
				.inTable("application")
				.notNullable();
			table.string("kinship").notNullable();
			table.string("profession").notNullable();
		});
module.exports.down = async (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.string("parent_profession_father", 255).nullable();
			table.string("parent_profession_mother", 255).nullable();
		})
		.dropTable("house_hould_element");
module.exports.configuration = { transaction: true };
