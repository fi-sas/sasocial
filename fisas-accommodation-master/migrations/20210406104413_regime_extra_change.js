module.exports.up = (db) =>
	db.schema
		.createTable("application_regime_change", (table) => {
			table.increments();
			table.integer("regime_id").unsigned().references("id").inTable("regime").notNullable();
			table.string("reason");
			table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
			table.date("start_date");
			table.enum("decision", ["APPROVE", "REJECT"]);
			table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"], { useNative: true, enumName: "regime_change_status" }).defaultTo("SUBMITTED").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("application_extra_change", (table) => {
			table.increments();
			table.string("reason");
			table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
			table.date("start_date");
			table.enum("decision", ["APPROVE", "REJECT"]);
			table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"], { useNative: true, enumName: "application_extra_change_status" }).defaultTo("SUBMITTED").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("extra_change", (table) => {
			table.increments();
			table.integer("application_extra_change_id").unsigned().references("id").inTable("application_extra_change").notNullable();
			table.integer("extra_id").unsigned().references("id").inTable("extra").notNullable();
		});
module.exports.down = (db) =>
	db.schema
		.dropTable("application_regime_change")
		.dropTable("application_extra_change")
		.dropTable("extra_change");
module.exports.configuration = { transaction: true };
