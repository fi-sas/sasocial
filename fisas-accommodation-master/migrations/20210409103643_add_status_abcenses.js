module.exports.up = (db) =>
	db.schema
		.alterTable("absence", (table) => {
			table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED_WITH_SUSPENSION", "APPROVED_WITHOUT_SUSPENSION", "CANCELLED"]).defaultTo("SUBMITTED").notNullable();
			table.enum("decision", ["APPROVE_WITH_SUSPENSION", "APPROVE_WITHOUT_SUSPENSION"]);
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("absence", (table) => {
			table.dropColumn("status");
			table.dropColumn("decision");
		});
module.exports.configuration = { transaction: true };
