module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.integer("organic_unit_id");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("organic_unit_id");
		});
module.exports.configuration = { transaction: true };
