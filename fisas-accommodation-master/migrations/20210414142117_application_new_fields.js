module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.string("nationality");
			table.enum("gender", ["M", "F", "U"]);
			table.string("mother_name");
			table.string("father_name");
			table.boolean("incapacity");
			table.string("incapacity_degree");
			table.string("incapacity_type");
			table.string("country");
			table.integer("househould_elements_in_university");
			table.string("secundary_email");

		});
module.exports.down = (db) =>
	db.schema

		.alterTable("application", (table) => {
			table.dropColumn("nationality");
			table.dropColumn("gender");
			table.dropColumn("mother_name");
			table.dropColumn("father_name");
			table.dropColumn("incapacity");
			table.dropColumn("incapacity_degree");
			table.dropColumn("incapacity_type");
			table.dropColumn("country");
			table.dropColumn("househould_elements_in_university");
			table.dropColumn("secundary_email");
		});
module.exports.configuration = { transaction: true };
