module.exports.up = (db) =>
	db.schema
		.alterTable("residence", (table) => {
			table.boolean("available_for_application");
			table.boolean("available_for_assign");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("residence", (table) => {
			table.dropColumn("available_for_application");
			table.dropColumn("available_for_assign");
		});
module.exports.configuration = { transaction: true };
