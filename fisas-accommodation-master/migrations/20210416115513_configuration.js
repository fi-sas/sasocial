module.exports.up = (db) =>
	db.schema
		.createTable("configuration", (table) => {
			table.increments();
			table.string("key", 120).notNullable();
			table.json("value");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("key");
		});
module.exports.down = (db) =>
	db.schema
		.droptable("configuration");
module.exports.configuration = { transaction: true };
