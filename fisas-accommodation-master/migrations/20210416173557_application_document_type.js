module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.string("document_type");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("document_type");
		});
module.exports.configuration = { transaction: true };
