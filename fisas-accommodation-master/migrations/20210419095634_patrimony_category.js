module.exports.up = (db) =>
	db.schema
		.createTable("patrimony_category", (table) => {
			table.increments();
			table.integer("code").notNullable();
			table.string("description").notNullable();
			table.boolean("active").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.alterTable("application", (table) => {
			table.dropColumn("household_accounts_balance");
			table.integer("patrimony_category_id").unsigned().references("id").inTable("patrimony_category");

		});

module.exports.down = (db) =>
	db.schema
		.dropTable("patrimony_category")
		.alterTable("application", (table) => {
			table.dropColumn("patrimony_category_id");
			table.float("household_accounts_balance");
		})
	;
module.exports.configuration = { transaction: true };
