module.exports.up = (db) =>
	db.schema
		.createTable("application_income_statement_file", (table) => {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("application");
			table.integer("file_id");
		})
		.alterTable("application", (table) => {
			table.dropColumn("income_statement_file_id");
			table.boolean("income_statement_delivered");
		});

module.exports.down = (db) =>
	db.schema
		.dropTable("application_income_statement_file")
		.alterTable("application", (table) => {
			table.integer("income_statement_file_id").unsigned().nullable();
			table.dropColumn("income_statement_delivered");
		});

module.exports.configuration = { transaction: true };
