module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("patrimony_category_id");
			table.dropColumn("registry_validity");
			table.string("patrimony_category");
			table.boolean("international_student");
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.integer("patrimony_category_id").unsigned().references("id").inTable("patrimony_category");
			table.datetime("registry_validity");
			table.dropColumn("patrimony_category");
			table.dropColumn("international_student");
		})
	;
module.exports.configuration = { transaction: true };
