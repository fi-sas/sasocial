module.exports.up = (db) =>
	db.schema
		.createTable("accommodation_period", (table) => {
			table.increments();
			table.string("academic_year").notNullable().unique();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.alterTable("application", (table) => {
			table.integer("patrimony_category_id").unsigned().references("id").inTable("patrimony_category");
		});
module.exports.down = (db) =>
	db.schema
		.dropTable("accommodation_period")
		.alterTable("application", (table) => {
			table.dropColumn("patrimony_category_id");
		});
module.exports.configuration = { transaction: true };
