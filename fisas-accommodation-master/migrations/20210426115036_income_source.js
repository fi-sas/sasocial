module.exports.up = (db) =>
	db.schema
		.raw(`CREATE TYPE income_source AS ENUM ('DEPENDENT_WORKER', 'INDEPENDENT_WORK', 'PENSIONS','UNEMPLOYMENT_ALLOWANCE','RSI','OTHER');`)
		.alterTable("application", (table) => {
			table.specificType("income_source", "income_source[]");
			table.string("other_income_source");
		});
module.exports.down = (db) =>
	db.schema
		.raw("DROP TYPE income_source")
		.alterTable("application", (table) => {
			table.dropColumn("income_source");
			table.dropColumn("other_income_source");
		});

module.exports.configuration = { transaction: true };
