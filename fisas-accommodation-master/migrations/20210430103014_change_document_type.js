module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("document_type");
			table.integer("document_type_id").unsigned();
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("document_type_id");
			table.string("document_type");
		});
module.exports.configuration = { transaction: true };
