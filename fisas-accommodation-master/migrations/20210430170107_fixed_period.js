
module.exports.up = (db) =>
	db.schema
		.alterTable("application_phase", (table) => {
			table.boolean("fixed_accommodation_period");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application_phase", (table) => {
			table.dropColumn("fixed_accommodation_period");
		});
module.exports.configuration = { transaction: true };
