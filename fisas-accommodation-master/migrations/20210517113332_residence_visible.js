
module.exports.up = (db) =>
	db.schema
		.alterTable("residence", (table) => {
			table.boolean("visible_for_users");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("residence", (table) => {
			table.dropColumn("visible_for_users");
		});
module.exports.configuration = { transaction: true };
