
module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("course");
			table.dropColumn("course_degree");
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.string("course", 255).nullable();
			table.string("course_degree", 255).nullable();
		});
module.exports.configuration = { transaction: true };
