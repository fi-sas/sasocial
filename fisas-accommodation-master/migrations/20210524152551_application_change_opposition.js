exports.up = function (knex) {
	return knex.schema.alterTable("application", (table) => {
		table.text("opposition_request").alter();
		table.text("opposition_answer").alter();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("application", (table) => {
		table.string("opposition_request").alter();
		table.string("opposition_answer").alter();
	});
};
