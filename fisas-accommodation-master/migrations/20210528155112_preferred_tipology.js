
module.exports.up = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table
				.integer("preferred_typology_id")
				.unsigned()
				.references("id")
				.inTable("typology")
				.nullable();
		});
module.exports.down = (db) =>
	db.schema
		.alterTable("application", (table) => {
			table.dropColumn("preferred_typology_id");
		});
module.exports.configuration = { transaction: true };
