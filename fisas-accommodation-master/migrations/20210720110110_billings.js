
module.exports.up = (db) =>
    db.schema
        .createTable("billing", (table) => {
            table.increments();
            table.integer("application_id").unsigned().references("id").inTable("application");
            table.integer("month");
            table.integer("year");
            table.date("billing_start_date");
            table.date("billing_end_date");
            table.enu("status", ["PROCESSED", "REVIEWED", "BILLED", "PAID", "CANCELLED"]);
            table.float("possible_retroactive", 4, 2);
            table.datetime("due_date");
            table.uuid("movement_id");
            table.datetime("paid_at").nullable();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();

            table.unique(["application_id", "month", "year"]);
        })
        .dropTableIfExists("billing_item")
        .createTable("billing_item", (table) => {
            table.increments();
            table.string("product_code", 120).notNullable();
            table.string("name", 200).notNullable();
            table.string("description", 255).notNullable().defaultTo("-");
            table.integer("room_id").unsigned().references("id").inTable("room").nullable();
            table.integer("typology_id").unsigned().references("id").inTable("typology").nullable();
            table.integer("regime_id").unsigned().references("id").inTable("regime").nullable();
            table.integer("extra_id").unsigned().references("id").inTable("extra").nullable();

            table.float("quantity").notNullable();
            table.float("unit_price", 4, 2).notNullable();
            table.float("price", 4, 2).notNullable();
            table.integer("vat_id").notNullable();
            table.float("vat_value").notNullable();
            table
                .enu("period", ["DAY", "WEEK", "MONTH"], {
                    useNative: true,
                    existingType: true,
                    enumName: "price_line_periods",
                })
                .notNullable();
            table.float("possible_retroactive", 4, 2).nullable();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
            table.integer("billing_id").unsigned().references("id").inTable("billing");
        })
        .alterTable("residence", (table) => {
            table.integer("current_account_id");
        });


;
module.exports.down = (db) =>
    db.schema
        .dropTableIfExists('billing')
        .dropTableIfExists("billing_item")
        // billingItem
        .createTable("billing_item", (table) => {
            table.increments();
            table.string("product_code", 120).notNullable();
            table.string("name", 200).notNullable();
            table.string("description", 255).notNullable().defaultTo("-");
            table
                .integer("application_id")
                .unsigned()
                .references("id")
                .inTable("application")
                .notNullable();
            table
                .integer("room_id")
                .unsigned()
                .references("id")
                .inTable("room")
                .nullable();
            table
                .integer("typology_id")
                .unsigned()
                .references("id")
                .inTable("typology")
                .nullable();
            table
                .integer("regime_id")
                .unsigned()
                .references("id")
                .inTable("regime")
                .nullable();
            table
                .integer("extra_id")
                .unsigned()
                .references("id")
                .inTable("extra")
                .nullable();
            table.float("quantity").notNullable();
            table.float("unit_price").notNullable();
            table.float("price").notNullable();
            table.integer("vat_id").notNullable();
            table.float("vat_value").notNullable();
            table
                .enu("period", ["DAY", "WEEK", "MONTH"], {
                    useNative: true,
                    existingType: true,
                    enumName: "price_line_periods",
                })
                .notNullable();
            table.integer("month").notNullable().unsigned();
            table.integer("year").notNullable().unsigned();
            table.boolean("paid").notNullable().defaultTo(0);
            table.boolean("billed").notNullable().defaultTo(0);
            table.boolean("cancelled").default(0).notNullable();
            table.datetime("paid_at").nullable();
            table.datetime("cancelled_at").nullable();
            table.boolean("is_deposit").notNullable().defaultTo(0);
            table.integer("payment_method_id").nullable();
            table.integer("billed_month").nullable();
            table.integer("billed_year").nullable();
            table.integer("order_id").nullable();
            table.float("possible_retroactive").nullable();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .alterTable("residence", (table) => {
            table.dropColumn("current_account_id");
        });


module.exports.configuration = { transaction: true };
