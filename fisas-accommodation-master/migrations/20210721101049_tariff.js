module.exports.up = (db) =>
    db.schema
        .createTable("tariff", (table) => {
            table.increments();
            table.string("name");
            table.boolean("active");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .alterTable("price_line", (table) => {
            table.integer("tariff_id").unsigned().references("id").inTable("tariff").nullable();
            table.dropColumn("price_scholarship");
        })
        .alterTable("application", (table) => {
            table.integer("tariff_id").unsigned().references("id").inTable("tariff").nullable();
            table.date("updated_end_date").nullable();
        })
    ;
module.exports.down = (db) =>
    db.schema
        .alterTable("price_line", (table) => {
            table.float("price_scholarship");
            table.dropColumn("tariff_id");
        })
        .dropTableIfExists('tariff')
        .alterTable("application", (table) => {
            table.dropColumn("tariff_id");
            table.dropColumn("updated_end_date");
        });
module.exports.configuration = { transaction: true };
