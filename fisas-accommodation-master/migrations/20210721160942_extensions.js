module.exports.up = (db) =>
    db.schema
        .alterTable("extension", (table) => {
            table.dropColumn("start_date");
            table.string("observations");
        })
    ;
module.exports.down = (db) =>
    db.schema
        .alterTable("extension", (table) => {
            table.datetime("start_date").notNullable();
            table.dropColumn("observations");
        });
module.exports.configuration = { transaction: true };
