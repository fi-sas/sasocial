module.exports.up = (db) =>
    db.schema
        .createTable("application_room_change", (table) => {
            table.increments();
            table.integer("typology_id").unsigned().references("id").inTable("typology");
            table.integer("residence_id").unsigned().references("id").inTable("residence");
            table.string("permute_user_tin");
            table.integer("room_id").unsigned().references("id").inTable("room");
            table.string("reason");
            table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
            table.date("start_date");
            table.enum("type", ["TYPOLOGY", "RESIDENCE", "PERMUTE"]).notNullable();
            table.enum("decision", ["APPROVE", "REJECT"]);
            table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"]).defaultTo("SUBMITTED").notNullable();
            table.string("observations");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .alterTable("application_extra_change", (table) => {
            table.string("observations");
        })
        .alterTable("application_regime_change", (table) => {
            table.string("observations");
        })
        .alterTable("withdrawal", (table) => {
            table.string("observations");
        })
        .alterTable("absence", (table) => {
            table.string("observations");
        })

    ;
module.exports.down = (db) =>
    db.schema
        .dropTable("application_room_change")
        .alterTable("application_extra_change", (table) => {
            table.dropColumn("observations");
        })
        .alterTable("application_regime_change", (table) => {
            table.dropColumn("observations");
        })
        .alterTable("withdrawal", (table) => {
            table.dropColumn("observations");
        })
        .alterTable("absence", (table) => {
            table.dropColumn("observations");
        });
module.exports.configuration = { transaction: true };
