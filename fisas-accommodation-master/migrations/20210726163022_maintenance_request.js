module.exports.up = (db) =>
    db.schema
        .createTable("maintenance_request", (table) => {
            table.increments();
            table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
            table.string("description");
            table.string("local")
            table.enum("status", ["SUBMITTED", "RESOLVING", "RESOLVED", "REJECTED", "CANCELLED"]).defaultTo("SUBMITTED").notNullable();
            table.string("observations")
            table.integer("file_id");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        });
module.exports.down = (db) =>
    db.schema
        .dropTable("maintenance_request")
    ;

module.exports.configuration = { transaction: true };
