module.exports.up = (db) =>
    db.schema
        .alterTable("extension", (table) => {
            table.enum("decision", ["APPROVE", "REJECT"]);
        })
        .alterTable("withdrawal", (table) => {
            table.enum("decision", ["APPROVE", "REJECT"]);
        })

    ;
module.exports.down = (db) =>
    db.schema
        .alterTable("extension", (table) => {
            table.dropColumn("decision");
        })
        .alterTable("withdrawal", (table) => {
            table.dropColumn("decision");
        });
module.exports.configuration = { transaction: true };
