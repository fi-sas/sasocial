module.exports.up = (db) =>
    db.schema
        .createTable("absence_history", (table) => {
            table.increments();
            table
                .integer("absence_id")
                .unsigned()
                .references("id")
                .inTable("absence");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        });

module.exports.down = (db) =>
    db.schema.dropTable("absence_history");
module.exports.configuration = { transaction: true };
