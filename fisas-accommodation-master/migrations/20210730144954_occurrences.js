module.exports.up = (db) =>
    db.schema
        .createTable("occurrence", (table) => {
            table.increments();
            table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
            table.datetime("date");
            table.string("subject");
            table.string("description");
            table.integer("file_id");
            table.integer("created_by_id");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        });
module.exports.down = (db) =>
    db.schema
        .dropTable("occurrence");

module.exports.configuration = { transaction: true };
