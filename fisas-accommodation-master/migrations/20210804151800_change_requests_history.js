module.exports.up = (db) =>
    db.schema
        .createTable("application_extra_change_history", (table) => {
            table.increments();
            table
                .integer("application_extra_change_id")
                .unsigned()
                .references("id")
                .inTable("application_extra_change");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("application_regime_change_history", (table) => {
            table.increments();
            table
                .integer("application_regime_change_id")
                .unsigned()
                .references("id")
                .inTable("application_regime_change");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("application_room_change_history", (table) => {
            table.increments();
            table
                .integer("application_room_change_id")
                .unsigned()
                .references("id")
                .inTable("application_room_change");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("maintenance_request_history", (table) => {
            table.increments();
            table
                .integer("maintenance_request_id")
                .unsigned()
                .references("id")
                .inTable("maintenance_request");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
    ;

module.exports.down = (db) =>
    db.schema
        .dropTable("application_extra_change_history")
        .dropTable("application_regime_change_history")
        .dropTable("application_room_change_history")
        .dropTable("maintenance_request_history");
module.exports.configuration = { transaction: true };
