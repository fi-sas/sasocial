module.exports.up = (db) =>
    db.schema
        .alterTable("application_room_change", (table) => {
            table.integer("file_id");
        })
        .alterTable("application_extra_change", (table) => {
            table.integer("file_id");
        })
        .alterTable("application_regime_change", (table) => {
            table.integer("file_id");
        })
        .createTable("application_tariff_change", (table) => {
            table.increments();
            table.integer("old_tariff_id").unsigned().references("id").inTable("tariff");
            table.integer("tariff_id").unsigned().references("id").inTable("tariff");
            table.boolean("has_scholarship").notNullable();
            table.string("reason");
            table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
            table.date("start_date").notNullable();
            table.integer("file_id")
            table.enum("decision", ["APPROVE", "REJECT"]);
            table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"]).defaultTo("SUBMITTED").notNullable();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("application_tariff_change_history", (table) => {
            table.increments();
            table.integer("application_tariff_change_id").unsigned().references("id").inTable("application_tariff_change");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.string("notes");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .alterTable("billing", (table) => {
            table.integer("tariff_id");
        })
    ;

module.exports.down = (db) =>
    db.schema
        .alterTable("application_room_change", (table) => {
            table.dropColumn("file_id");
        })
        .alterTable("application_extra_change", (table) => {
            table.dropColumn("file_id");
        })
        .alterTable("application_regime_change", (table) => {
            table.dropColumn("file_id");
        })
        .dropTable("application_tariff_change_history")
        .dropTable("application_tariff_change")
        .alterTable("billing", (table) => {
            table.dropColumn("tariff_id");
        })
    ;

module.exports.configuration = { transaction: true };
