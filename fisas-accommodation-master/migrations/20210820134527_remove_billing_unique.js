
module.exports.up = (db) =>
    db.schema.alterTable("billing", (table) => {
        table.dropUnique(["application_id", "month", "year"]);
    });

module.exports.down = (db) =>
    db.schema.alterTable("billing", (table) => {
        table.unique(["application_id", "month", "year"]);
    });

module.exports.configuration = { transaction: true };
