module.exports.up = (db) =>
    db.schema
        .alterTable("patrimony_category", (table) => {
            table.float("reference_value", 4, 2);
        })
        .alterTable("application", (table) => {
            table.float("per_capita_income", 4, 2);
        });

module.exports.down = (db) =>
    db.schema
        .alterTable("patrimony_category", (table) => {
            table.dropColumn("reference_value");
        })
        .alterTable("application", (table) => {
            table.dropColumn("per_capita_income");
        });
module.exports.configuration = { transaction: true };
