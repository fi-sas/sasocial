
exports.up = function (knex) {
    return knex.schema
        .alterTable("application_phase", (table) => {
            table.boolean("out_of_date_validation").notNullable().defaultTo(false);
            table.integer("out_of_date_validation_days").unsigned();
        });
};

exports.down = function (knex) {
    return knex.schema
        .alterTable("application_phase", (table) => {
            table.dropColumn("out_of_date_validation");
            table.dropColumn("out_of_date_validation_days");
        });
};
