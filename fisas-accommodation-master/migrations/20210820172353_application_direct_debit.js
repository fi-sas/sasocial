
module.exports.up = (db) =>
    db.schema
        .alterTable("application", (table) => {
            table.boolean("allow_direct_debit");
        });
module.exports.down = (db) =>
    db.schema
        .alterTable("application", (table) => {
            table.dropColumn("allow_direct_debit");
        });
module.exports.configuration = { transaction: true };
