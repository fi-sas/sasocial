
exports.up = (db) => {
    return db.schema.createTable("application_file", (table) => {
        table.increments();
        table.integer("application_id").unsigned().references("id").inTable("application");
        table.integer("file_id").unsigned();
    })
};

exports.down = (db) => {
    return db.schema.dropTable("application_file");
};
