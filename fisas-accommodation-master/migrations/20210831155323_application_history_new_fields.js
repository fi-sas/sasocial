
exports.up = (db) => {
	return db.schema.alterTable("application_history", (table) => {
		table.integer("room_id").unsigned().references("id").inTable("room");
		table.integer("residence_id").unsigned().references("id").inTable("residence");
		table.string("decision");
	})
};

exports.down = (db) => {
	return db.schema.alterTable("application_history", (table) => {
		table.dropTable("room_id");
		table.dropTable("residence_id");
		table.dropTable("decision");
	});
};
