
exports.up = (db) => {
	return db.schema.createTable("application_communication", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
		table.date("start_date");
		table.date("end_date");
		table.string("observations");
		table.string("response");
		table.enum("status", ["SUBMITTED", "CLOSED"]).defaultTo("SUBMITTED").notNullable();
		table.enum("type", ["ENTRY", "EXIT"]).defaultTo("SUBMITTED").notNullable();
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})
};

exports.down = (db) => {
	db.schema
		.dropTable("application_communication");
};
