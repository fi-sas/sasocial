
exports.up = (knex) => {
	return knex.schema
		.raw(`ALTER TYPE applications_status ADD VALUE 'withdrawal';`);
};

exports.down = (knex) => {
	return knex.schema.raw(``);
};
