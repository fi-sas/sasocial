
exports.up = function (knex) {
	return knex.schema.raw(`
	ALTER TABLE "application_room_change"
	DROP CONSTRAINT "application_room_change_type_check",
	ADD CONSTRAINT "application_room_change_type_check"
	CHECK (type IN ('TYPOLOGY', 'RESIDENCE', 'PERMUTE', 'ROOM'))
  `);

};

exports.down = function (knex) {
	return knex.schema.raw(`
	ALTER TABLE "application_room_change"
	DROP CONSTRAINT "application_room_change_type_check",
	ADD CONSTRAINT "application_room_change_type_check"
	CHECK (type IN ('TYPOLOGY', 'RESIDENCE', 'PERMUTE'))
  `);

};
