
exports.up = (db) => {
	return db.schema.createTable("application_iban_change", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
		table.string("iban");
		table.boolean("allow_direct_debit");
		table.enum("status", ["SUBMITTED", "EXPIRED"]).defaultTo("SUBMITTED").notNullable();
		table.integer("file_id");
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})
};

exports.down = (db) => {
	return db.schema
		.dropTable("application_iban_change");
};
