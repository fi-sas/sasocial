
exports.up = (db) => {
	return db.schema.alterTable("application_history", (table) => {
		table.integer("tariff_id").unsigned().references("id").inTable("tariff");
	})
};

exports.down = (db) => {
	return db.schema.alterTable("application_history", (table) => {
		table.dropTable("tariff_id");
	});
};
