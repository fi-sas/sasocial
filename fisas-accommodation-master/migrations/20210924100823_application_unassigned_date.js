
exports.up = (db) => {
	return db.schema.alterTable("application", (table) => {
		table.datetime("unassigned_date");
	});
};

exports.down = (db) => {
	return db.schema.alterTable("application", (table) => {
		table.dropTable("unassigned_date");
	});
};
