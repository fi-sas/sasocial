
exports.up = function (db) {
	return db.schema.alterTable("extra", (table) => {
		table.dropColumn("is_deposit");
	});
};

exports.down = function (db) {
	return db.schema.alterTable("extra", (table) => {
		table.boolean("is_deposit").default(0).notNullable();
	});

};
