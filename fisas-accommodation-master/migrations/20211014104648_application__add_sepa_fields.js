module.exports.up = (db) =>
	db.schema.alterTable("application", (table) => {
		table.string("sepa_id_debit_auth", 18);
		table.datetime("sepa_signtr_date").nullable();
	});
module.exports.down = (db) =>
	db.schema.alterTable("application", (table) => {
		table.dropColumn("sepa_id_debit_auth");
		table.dropColumn("sepa_signtr_date");
	});
module.exports.configuration = { transaction: true };
