module.exports.up = (db) =>
	db.schema.alterTable("application_iban_change", (table) => {
		table.string("swift", 11);
		table.string("sepa_id_debit_auth", 18);
		table.datetime("sepa_signtr_date").nullable();
	});
module.exports.down = (db) =>
	db.schema.alterTable("application_iban_change", (table) => {
		table.dropColumn("swift");
		table.dropColumn("sepa_id_debit_auth");
		table.dropColumn("sepa_signtr_date");
	});
module.exports.configuration = { transaction: true };
