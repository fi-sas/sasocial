module.exports.up = (db) =>
	db.schema.alterTable("billing", (table) => {
		table.boolean("allow_direct_debit").notNullable().defaultTo(0);
		table.string("sepa_id_debit_auth", 18);
		table.datetime("sepa_signtr_date").nullable();
		table.string("sepa_first_recur", 8);
	});
module.exports.down = (db) =>
	db.schema.alterTable("billing", (table) => {
		table.dropColumn("allow_direct_debit");
		table.dropColumn("sepa_id_debit_auth");
		table.dropColumn("sepa_signtr_date");
		table.dropColumn("sepa_first_recur");
	});
module.exports.configuration = { transaction: true };
