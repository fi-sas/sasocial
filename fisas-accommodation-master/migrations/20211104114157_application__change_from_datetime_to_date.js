module.exports.up = (db) =>
    db.schema
        .raw('alter TABLE application ALTER COLUMN start_date TYPE date')
        .raw('alter TABLE application ALTER COLUMN end_date TYPE date');

module.exports.down = (db) =>
    db.schema
        .raw('alter TABLE application ALTER COLUMN start_date TYPE timestamp with time zone')
        .raw('alter TABLE application ALTER COLUMN end_date TYPE timestamp with time zone');

module.exports.configuration = { transaction: true };

