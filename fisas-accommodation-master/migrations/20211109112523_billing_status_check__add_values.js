
exports.up = function (knex) {
	return knex.schema.raw(`
	ALTER TABLE "billing"
	DROP CONSTRAINT "billing_status_check",
	ADD CONSTRAINT "billing_status_check"
	CHECK (status IN ('PROCESSED', 'REVIEWED', 'BILLED', 'PAID', 'CANCELLED', 'EXTERNAL_VALIDATED', 'EXTERNAL_PAID'))
  `);

};

exports.down = function (knex) {
	return knex.schema.raw(`
	ALTER TABLE "billing"
	DROP CONSTRAINT "billing_status_check",
	ADD CONSTRAINT "billing_status_check"
	CHECK (status IN ('PROCESSED', 'REVIEWED', 'BILLED', 'PAID', 'CANCELLED'))
  `);

};