module.exports.up = async (db) =>
    db.schema
        .alterTable("billing_item", (table) => {
            table.decimal("quantity", 8, 3).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.decimal("unit_price", 8, 3).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.decimal("price", 8, 3).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.decimal("vat_value", 8, 3).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.decimal("possible_retroactive", 8, 3).notNullable().alter();
        });

module.exports.down = async (db) =>
    db.schema
        .alterTable("billing_item", (table) => {
            table.float("quantity", 4).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.float("unit_price", 4).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.float("price", 4).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.float("vat_value", 4).notNullable().alter();
        })
        .alterTable("billing_item", (table) => {
            table.float("possible_retroactive", 4).notNullable().alter();
        });

module.exports.configuration = { transaction: true };