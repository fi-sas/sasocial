module.exports.up = (db) =>
    db.schema.alterTable("billing", (table) => {
        table.datetime("sepa_created_at").nullable();
        table.string("sepa_iban", 34);
    });
module.exports.down = (db) =>
    db.schema.alterTable("billing", (table) => {
        table.dropColumn("sepa_created_at");
        table.dropColumn("sepa_iban");
    });
module.exports.configuration = { transaction: true };