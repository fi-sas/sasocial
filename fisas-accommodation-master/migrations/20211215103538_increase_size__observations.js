module.exports.up = (db) =>
	db.schema
		.alterTable("withdrawal", (table) => {
			table.text("observations").alter();
		})
		.alterTable("extension", (table) => {
			table.text("observations").alter();
		})
		.alterTable("application_room_change", (table) => {
			table.text("observations").alter();
		})
		.alterTable("application_extra_change", (table) => {
			table.text("observations").alter();
		})
		.alterTable("application_regime_change", (table) => {
			table.text("observations").alter();
		})
		.alterTable("absence", (table) => {
			table.text("observations").alter();
		})
		.alterTable("application_communication", (table) => {
			table.text("observations").alter();
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("withdrawal", (table) => {
			table.string("observations").alter();
		})
		.alterTable("extension", (table) => {
			table.string("observations").alter();
		})
		.alterTable("application_room_change", (table) => {
			table.string("observations").alter();
		})
		.alterTable("application_extra_change", (table) => {
			table.string("observations").alter();
		})
		.alterTable("application_regime_change", (table) => {
			table.string("observations").alter();
		})
		.alterTable("absence", (table) => {
			table.string("observations").alter();
		})
		.alterTable("application_communication", (table) => {
			table.string("observations").alter();
		});

module.exports.configuration = { transaction: true };
