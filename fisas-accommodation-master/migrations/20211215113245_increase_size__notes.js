module.exports.up = (db) =>
	db.schema
		.alterTable("booking_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("application_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("withdrawal_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("extension_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("absence_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("application_extra_change_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("application_regime_change_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("application_room_change_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("maintenance_request_history", (table) => {
			table.text("notes").alter();
		})
		.alterTable("application_tariff_change_history", (table) => {
			table.text("notes").alter();
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("booking_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("application_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("withdrawal_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("extension_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("absence_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("application_extra_change_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("application_regime_change_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("application_room_change_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("maintenance_request_history", (table) => {
			table.string("notes").alter();
		})
		.alterTable("application_tariff_change_history", (table) => {
			table.string("notes").alter();
		});

module.exports.configuration = { transaction: true };
