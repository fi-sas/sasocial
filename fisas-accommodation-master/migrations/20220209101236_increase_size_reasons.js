module.exports.up = (db) =>
	db.schema
		.alterTable("absence", (table) => {
			table.text("reason").alter();
		})
		.alterTable("withdrawal", (table) => {
			table.text("reason").alter();
		})
		.alterTable("extension", (table) => {
			table.text("reason").alter();
		})
		.alterTable("application_regime_change", (table) => {
			table.text("reason").alter();
		})
		.alterTable("application_extra_change", (table) => {
			table.text("reason").alter();
		})
		.alterTable("application_room_change", (table) => {
			table.text("reason").alter();
		})
		.alterTable("application_tariff_change", (table) => {
			table.text("reason").alter();
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("absence", (table) => {
			table.string("reason").alter();
		})
		.alterTable("withdrawal", (table) => {
			table.string("reason").alter();
		})
		.alterTable("extension", (table) => {
			table.string("reason").alter();
		})
		.alterTable("application_regime_change", (table) => {
			table.string("reason").alter();
		})
		.alterTable("application_extra_change", (table) => {
			table.string("reason").alter();
		})
		.alterTable("application_room_change", (table) => {
			table.string("reason").alter();
		})
		.alterTable("application_tariff_change", (table) => {
			table.string("reason").alter();
		});

module.exports.configuration = { transaction: true };
