module.exports.up = (db) =>
    db.schema
        .createTable("extra_discount", (table) => {
            table.increments();
            table.integer("extra_id").unsigned().references("id").inTable("extra");
            table.integer("month").notNullable().unsigned();
            table.integer("year").notNullable().unsigned();
            table.decimal("discount_value", 12, 5);
            table.timestamps(true, true);
        })
        .createTable("typology_discount", (table) => {
            table.increments();
            table.integer("typology_id").unsigned().references("id").inTable("typology");
            table.integer("month").notNullable().unsigned();
            table.integer("year").notNullable().unsigned();
            table.decimal("discount_value", 12, 5);
            table.timestamps(true, true);
        })
        .createTable("regime_discount", (table) => {
            table.increments();
            table.integer("regime_id").unsigned().references("id").inTable("regime");
            table.integer("month").notNullable().unsigned();
            table.integer("year").notNullable().unsigned();
            table.decimal("discount_value", 12, 5);
            table.timestamps(true, true);
        });

module.exports.down = (db) =>
    db.schema
        .dropTable("extra_discount")
        .dropTable("typology_discount")
        .dropTable("regime_discount");