module.exports.up = (db) =>
	db.schema
		.alterTable("extra", (table) => {
			table.datetime("reprocess_since").nullable();
		})
		.alterTable("regime", (table) => {
			table.datetime("reprocess_since").nullable();
		})
		.alterTable("typology", (table) => {
			table.datetime("reprocess_since").nullable();
		})
		.alterTable("billing_item", (table) => {
			table.decimal("discount", 8, 3).notNullable().defaultTo(0);
			table.decimal("discount_value", 8, 3).notNullable().defaultTo(0);
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("extra", (table) => {
			table.dropColumn("reprocess_since");
		})
		.alterTable("regime", (table) => {
			table.dropColumn("reprocess_since");
		})
		.alterTable("typology", (table) => {
			table.dropColumn("reprocess_since");
		})
		.alterTable("billing_item", (table) => {
			table.dropColumn("discount");
			table.dropColumn("discount_value");
		});

module.exports.configuration = { transaction: true };
