module.exports.up = (db) =>
	db.schema.alterTable("application", (table) => {
		table.integer("current_regime_id").unsigned().references("id").inTable("regime").nullable();
	});

module.exports.down = (db) =>
	db.schema.alterTable("application", (table) => {
		table.dropColumn("current_regime_id");
	});
module.exports.configuration = { transaction: true };
