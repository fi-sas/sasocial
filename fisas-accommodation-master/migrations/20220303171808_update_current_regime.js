module.exports.up = (db) => db.raw("UPDATE application SET current_regime_id = regime_id");

module.exports.down = (db) => db.raw("UPDATE application SET current_regime_id = null");
module.exports.configuration = { transaction: true };
