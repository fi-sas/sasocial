module.exports.up = (db) =>
    db.schema
        .createTable("application_period_change", (table) => {
            table.increments();
            table.text("reason");
            table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
            table.date("start_date");
            table.date("end_date");
            table.enum("decision", ["APPROVE", "REJECT"]);
            table.enum("status", ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"], { useNative: true, enumName: "application_period_change_status" }).defaultTo("SUBMITTED").notNullable();
            table.timestamps(true, true);
        })
        .createTable("application_period_change_history", (table) => {
            table.increments();
            table.integer("application_period_change_id").unsigned().references("id").inTable("application_period_change");
            table.string("status").notNullable();
            table.integer("user_id").unsigned();
            table.text("notes");
            table.timestamps(true, true);
        });

module.exports.down = (db) =>
    db.schema
        .dropTable("application_period_change")
        .dropTable("application_period_change_history");