module.exports.up = (db) =>
    db.schema.alterTable("billing_item", (table) => {
        table.boolean("processed_manually").nullable().default(false);
    });

module.exports.down = (db) =>
    db.schema.alterTable("billing_item", (table) => {
        table.dropColumn("processed_manually");
    });
module.exports.configuration = { transaction: true };