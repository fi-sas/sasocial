
module.exports.up = (db) =>
	db.schema.alterTable("application_communication", (table) => {
		table.specificType("entry_hour", "time");
		table.specificType("exit_hour", "time");
	});

module.exports.down = (db) =>
	db.schema.alterTable("application_communication", (table) => {
		table.dropColumn("entry_hour");
		table.dropColumn("exit_hour");
	});
module.exports.configuration = { transaction: true };
