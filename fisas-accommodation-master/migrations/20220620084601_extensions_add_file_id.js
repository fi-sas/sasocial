
module.exports.up = (db) =>
	db.schema.alterTable("extension", (table) => {
		table.integer("file_id").unsigned();
	});

module.exports.down = (db) =>
	db.schema.alterTable("extension", (table) => {
		table.dropColumn("file_id");
	});
module.exports.configuration = { transaction: true };
