module.exports.seed = async (knex) => {
  return knex("regime")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("regime").insert({
          id: 1,
          product_code: "ALOJAMENTO",
          billing_name: "ALOJAMENTO",
          appear_billing: false,
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        await knex("regime_translation").insert({
          regime_id: 1,
          language_id: 3,
          name: "Alojamento",
          description: "Seriço basico de alojamento",
        });
        await knex("regime_translation").insert({
          regime_id: 1,
          language_id: 4,
          name: "Accommodation",
          description: "Basic service of accommodation",
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
