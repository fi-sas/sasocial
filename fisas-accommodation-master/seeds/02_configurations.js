exports.seed = async (knex) => {
	const data = [
		{
			key: "ALLOW_OPTIONAL_RESIDENCE",
			value: "true",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "ALLOW_APPLICATION_RENEW",
			value: "true",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "ASSIGNED_STATUS_AUTO_CHANGE",
			value: "true",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "STATUS_AUTO_CHANGE_DAYS",
			value: "5",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "EVENT_FOR_AUTO_CHANGE",
			value: JSON.stringify("CONFIRM"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "BILLING_OUT_OF_DATE_TYPE",
			value: JSON.stringify("MONTH_DAY"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "BILLING_OUT_OF_DATE_DAY",
			value: "10",
			updated_at: new Date(),
			created_at: new Date(),
		},

		{
			key: "CHANGE_STATUS_ON_CREATE",
			value: "true",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "CHANGE_STATUS_ON_CREATE_DAYS",
			value: "10",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "SHOW_IBAN_ON_APPLICATION_FORM",
			value: "false",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "UNASSIGNED_OPPOSITION_LIMIT",
			value: "30",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "INSTITUTE_IBAN",
			value: JSON.stringify("PT"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "INSTITUTE_SWIFT",
			value: JSON.stringify("XYZ"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "INSTITUTE_SEPA_NAME",
			value: JSON.stringify("SERV ACCAO SOCIAL INST POL XYZ"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "INSTITUTE_SEPA_PRIVATE_ID",
			value: JSON.stringify("PT49ZZZ123456"),
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "SEPA_REQUESTED_COLLECTION_DATE_DAYS_ADD",
			value: 3,
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "ENABLE_STATUS_EXTERNAL_VALIDATE_OR_PAID",
			value: "false",
			updated_at: new Date(),
			created_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			const rows = await knex("configuration").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("configuration").insert(d);
			}
			return true;
		}),
	);
};
