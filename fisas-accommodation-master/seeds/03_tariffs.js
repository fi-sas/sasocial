exports.seed = async (knex) => {
    const data = [
        {
            id: 1,
            name: "Aluno bolseiro",
            active: true,
            updated_at: new Date(),
            created_at: new Date(),
        },
        {
            id: 2,
            name: "Aluno não bolseiro",
            active: true,
            updated_at: new Date(),
            created_at: new Date(),
        },
        {
            id: 3,
            name: "Aluno internacional",
            active: true,
            updated_at: new Date(),
            created_at: new Date(),
        },
        {
            id: 4,
            name: "Outro",
            active: true,
            updated_at: new Date(),
            created_at: new Date(),
        },
    ];

    return Promise.all(
        data.map(async (d) => {
            const rows = await knex("tariff").select().where("id", d.id);
            if (rows.length === 0) {
                await knex("tariff").insert(d);
            }
            return true;
        }),
    );
};
