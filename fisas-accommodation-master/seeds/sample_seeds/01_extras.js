module.exports.seed = async (knex) => {
  return knex("extra")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("extra").insert({
          id: 1,
          product_code: "CAUCAO",
          billing_name: "CAUÇÃO",
          price: 1,
          vat_id: 1,
          //is_deposit: true,
          visible: false,
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        await knex("extra_translation").insert({
          extra_id: 1,
          language_id: 3,
          name: "Caução",
        });
        await knex("extra_translation").insert({
          extra_id: 1,
          language_id: 4,
          name: "Deposit",
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
