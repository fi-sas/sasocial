module.exports.seed = async (knex) => {
  return knex("service")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("service").insert({
          id: 1,
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        await knex("service_translation").insert({
          service_id: 1,
          language_id: 3,
          name: "WIFI",
        });
        await knex("service_translation").insert({
          service_id: 1,
          language_id: 4,
          name: "WIFI",
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
