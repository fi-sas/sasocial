module.exports.seed = async (knex) => {
  return knex("typology")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("typology").insert({
          id: 1,
          max_occupants_number: 1,
          product_code: "QUARTOIND",
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        await knex("typology_translation").insert({
          typology_id: 1,
          language_id: 3,
          name: "Quarto individual",
        });
        await knex("typology_translation").insert({
          typology_id: 1,
          language_id: 4,
          name: "Single room",
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
