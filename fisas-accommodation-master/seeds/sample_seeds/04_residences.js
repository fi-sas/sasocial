module.exports.seed = async (knex) => {
  return knex("residence")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("residence").insert({
          id: 1,
          name: "Master Residence",
          address: "Morada",
          city: "Cidade",
          postal_code: "1234-123",
          phone_1: "123123123",
          phone_2: "123123123",
          email: "email@sasocial.pt",
          building_id: 1,
          regulation_file_id: 1,
          contract_file_id: 1,
          charge_deposit: false,
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
