module.exports.seed = async (knex) => {
  return knex("room")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("room").insert({
          id: 1,
          name: "Quarto 1",
          room_id: 1,
          typology_id: 1,
          residence_id: 1,
          allow_booking: false,
          active: true,
          updated_at: new Date(),
          created_at: new Date(),
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
