module.exports.seed = async (knex) => {
  return knex("application_phase")
    .select()
    .where("id", 1)
    .then(async (rows) => {
      if (rows.length === 0) {
        // no matching records found
        // Insert alert type and example related alert template
        const regime = await knex("application_phase").insert({
          id: 1,
          start_date: new Date(new Date().getFullYear(), 0, 1),
          end_date: new Date(new Date().getFullYear() + 1, 0, 1),
          out_of_date_from: new Date(),
          academic_year: `${new Date().getFullYear()}-${
            new Date().getFullYear() + 1
          }`,
          application_phase: 1,
          updated_at: new Date(),
          created_at: new Date(),
        });

        return true;
      }
      return true;
    })
    .catch(() => {
      // you can find errors here.
    });
};
