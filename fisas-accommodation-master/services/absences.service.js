"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { MoleculerClientError } = require("moleculer").Errors;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const absenceStateMachine = require("./state-machines/absence.machine");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.absences",
	table: "absence",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"start_date",
			"end_date",
			"reason",
			"application_id",
			"allow_booking",
			"user_id",
			"file_id",
			"status",
			"decision",
			"observations",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["history", "file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},

			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "accommodation.absence-history", "history", "id", "absence_id");
			},
		},
		entityValidator: {
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
			reason: { type: "string" },
			application_id: { type: "number", convert: true },
			allow_booking: { type: "boolean" },
			user_id: { type: "number", convert: true },
			file_id: { type: "number", convert: true, optional: true },
			status: {
				type: "enum",
				values: [
					"SUBMITTED",
					"ANALYSIS",
					"DISPATCH",
					"APPROVED_WITH_SUSPENSION",
					"APPROVED_WITHOUT_SUSPENSION",
					"CANCELLED",
				],
				optional: true,
			},
			decision: {
				type: "enum",
				values: ["APPROVE_WITH_SUSPENSION", "APPROVE_WITHOUT_SUSPENSION"],
				optional: true,
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			observations: { type: "string", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
				},
				function getActiveApplication(ctx) {
					return ctx
						.call("accommodation.applications.find", {
							query: {
								user_id: ctx.meta.user.id,
								status: "contracted",
							},
						})
						.then((applications) => {
							if (applications.length > 0) {
								ctx.params.application_id = applications[0].id;
							} else {
								throw new MoleculerClientError(
									"No active application finded in accommodation",
									404,
									"ACCOMMODATION_APPLICAION_NOT_FOUND",
									{},
								);
							}
						});
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}

					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "absence.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				async function sendNotification(ctx, response) {
					const app = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_ABSENCES_SUBMITTED",
						user_id: app[0].user_id,
						user_data: {},
						data: {
							application: app[0],
						},
						variables: {},
						external_uuid: app[0].uuid,
					});
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		get_status: {
			rest: "GET /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			handler(ctx) {
				return this._get(ctx, ctx.params).then((absence) => {
					const stateMachine = absenceStateMachine.createStateMachine(absence[0].status, ctx);
					return stateMachine.transitions();
				});
			},
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["ANALYSE", "DISPATCH", "CANCEL"] },
				absence: {
					type: "object",
					props: {
						decision: {
							type: "enum",
							values: ["APPROVE_WITH_SUSPENSION", "APPROVE_WITHOUT_SUSPENSION"],
							optional: true,
						},
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const absence = await ctx.call("accommodation.absences.get", ctx.params);
				const stateMachine = absenceStateMachine.createStateMachine(absence[0].status, ctx);

				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.absence) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.absence.decision)
				) {
					throw new ValidationError(
						"Decision is required to change status to dispatch",
						"ACCOMMODATION_ABSENCE_DECISION_NOT_FOUND",
						{},
					);
				}
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_ABSENCE_STATUS_ERROR",
						{},
					);
				}
			},
		},
		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const absence = await ctx.call("accommodation.absences.get", ctx.params);
				const stateMachine = absenceStateMachine.createStateMachine(absence[0].status, ctx);
				if (stateMachine.can(absence[0].decision)) {
					const action =
						absence[0].decision == "APPROVE_WITH_SUSPENSION"
							? "approveWithSuspension"
							: "approveWithoutSuspension";
					return stateMachine[action]().then((result) => {
						// TODO: IF(RESULT[0].STATUS==APPROVE_WITH_SUSPENSION) BILLING RECALCULATION
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_ABSENCE_STATUS",
						{},
					);
				}
			},
		},
		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_regime_change = await ctx.call("accommodation.absences.get", ctx.params);
				const stateMachine = absenceStateMachine.createStateMachine(
					app_regime_change[0].status,
					ctx,
				);

				if (stateMachine.can("REJECT")) {
					return stateMachine["reject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_REGIME_CHANGE_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
