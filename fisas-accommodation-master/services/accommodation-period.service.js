"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.accommodation-periods",
	table: "accommodation_period",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "accommodation-periods")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"academic_year",
			"start_date",
			"end_date",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			academic_year: { type: "string", max: 9, pattern: "^\\d{4}-\\d{4}" },
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateAcademicYear"
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateAcademicYear(ctx) {
			const academic_year = await ctx.call("configuration.academic_years.count", { query: { academic_year: ctx.params.academic_year } });
			if (academic_year == 0) {
				throw new ValidationError(
					"Academic year not founD",
					"ACCOMMODATION_ACADEMIC_YEAR_NOT_FOUND",
					{},
				);
			}
			const count = await this._count(ctx, { query: { academic_year: ctx.params.academic_year } });
			if (count > 0) {
				throw new ValidationError(
					"Already exists one configuration to provided academic year",
					"ACCOMMODATION_DUPLICATE_ACADEMIC_YEAR",
					{},
				);
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
