"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application_extras",
	table: "application_extra",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications_extras")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "application_id", "extra_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			application_id: { type: "number" },
			extra_id: { type: "number" },
		},
	},
	hooks: {
		before: {
			list: [],
			create: [],
			update: [],
		},
		after: {
			create: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_extras_of_application: {
			params: {
				application_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				extra_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id,
				});
				this.clearCache();

				const entities = ctx.params.extra_ids.map((extra_id) => ({
					application_id: ctx.params.application_id,
					extra_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
		find: {
			cache: {
				enabled: true,
				keys: ["application_id"],
			},
			params: {
				query: {
					type: "object",
					props: {
						application_id: [{
							type: "number",
							convert: true,
						},
						{
							type: "array", item: { type: "number", positive: true, convert: true }
						}],
					}
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						application_id: ctx.params.query ? ctx.params.query.application_id : null,
					},
				}).then((app_extras) => {

					return ctx.call("accommodation.extras.find", {
						id: app_extras.map((r) => r.extra_id),
					}).then(extras => {
						return app_extras.map(app_extra => {
							const extra = _.cloneDeep(extras.find(e => e.id === app_extra.extra_id));
							extra.application_id = app_extra.application_id;
							return extra;
						});
					});

				});
			},
		},

	},

	/**
	 * Events
	 */
	events: {
		"accommodation.extras.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
