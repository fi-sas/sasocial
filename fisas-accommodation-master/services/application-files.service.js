"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application_files",
	table: "application_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "application_files")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "application_id", "file_id"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			application_id: { type: "number" },
			file_id: { type: "number" },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_application_files: {
			params: {
				application_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				file_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id,
				});
				this.clearCache();
				const entities = ctx.params.file_ids.map((file_id) => ({
					application_id: ctx.params.application_id,
					file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
		find: {
			cache: {
				enabled: true,
				keys: ["application_id"],
			},
			params: {
				query: {
					type: "object",
					props: {
						application_id: [
							{
								type: "number",
								convert: true,
							},
							{
								type: "array",
								item: { type: "number", positive: true, convert: true },
							},
						],
					},
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						application_id: ctx.params.query ? ctx.params.query.application_id : null,
					},
				}).then((app_files) => {
					return ctx
						.call("media.files.find", {
							query: {
								id: app_files.map((r) => r.file_id),
							},
						})
						.then((files) => {
							return app_files
								.map((app_file) => {
									const file = _.cloneDeep(files.find((e) => e.id === app_file.file_id));

									// GOD KNOWS
									if (file) file.application_id = app_file.application_id;
									return file;
								})
								.filter((f) => !!f);
						});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
