"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.applications-history",
	table: "application_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications-history")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "status", "notes", "user_id", "application_id", "residence_id", "room_id", "decision", "tariff_id", "created_at", "updated_at"],
		defaultWithRelateds: ["user", "residence", "room", "tariff"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			residence(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.residences", "residence", "residence_id", "id", {}, "name", false);
			},
			room(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.rooms", "room", "room_id", "id", {}, "name", false);
			},
			tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "tariff", "tariff_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			status: { type: "string" },
			notes: { type: "string", allowEmpty: true },
			user_id: { type: "number", positive: true, convert: true, optional: true },
			application_id: { type: "number", positive: true, convert: true },
			room_id: { type: "number", positive: true, convert: true, optional: true },
			residence_id: { type: "number", positive: true, convert: true, optional: true },
			decision: { type: "string", allowEmpty: true, optional: true },
			tariff: { type: "number", positive: true, convert: true, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
