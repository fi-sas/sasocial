"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const moment = require("moment");
const { database } = require("pg/lib/defaults");
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const periodStateMachine = require("./state-machines/application-period-changes.machine");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-period-changes",
	table: "application_period_change",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"reason",
			"application_id",
			"start_date",
			"end_date",
			"decision",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["application", "history"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
									res[0].extras = await ctx.call("accommodation.extras.find", {
										application_id: res[0].id
									});
								}
								doc.application = res[0];
							});
					}),
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application-period-change-history",
					"history",
					"id",
					"application_period_change_id",
				);
			},
		},
		entityValidator: {
			reason: { type: "string", convert: true, optional: true },
			application_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optinal: true,
			},
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
			decision: {
				type: "enum",
				values: ["APPROVE", "REJECT"],
				optional: true,
			},
			status: {
				type: "enum",
				values: ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"],
				default: "SUBMITTED",
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateActiveApplication",
				"haveOpenPeriodChangeRequests",
				"validateAcademicYear",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}

					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "application_period_change.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				async function saveHistory(ctx, response) {
					ctx.call("accommodation.application-period-change-history.create", {
						application_period_change_id: response[0].id,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
						status: response[0].status,
					});
					return response;
				},
				async function sendNotification(ctx, response) {
					const application = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_APPLICATION_PERIOD_CHANGE_SUBMITTED",
						user_id: application[0].user_id,
						user_data: {},
						data: {
							application: application,
						},
						variables: {},
						external_uuid: null,
					});
					return response;
				},
			],
			admin_approve: [
				async function updateApplicationBilling(ctx, res) {
					if (res[0].decision == "APPROVE") {
						ctx.call("accommodation.applications.patch", {
							id: res[0].application_id,
							start_date: res[0].start_date,
							end_date: res[0].end_date,
							updated_end_date: res[0].end_date
						}).then(response => {
							ctx.call("accommodation.billings.processBillingPeriodChanges", {
								application_id: response[0].id,
								start_date: response[0].start_date,
								end_date: response[0].updated_end_date
							});
						});
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		get_status: {
			rest: "GET /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			handler(ctx) {
				return ctx
					.call("accommodation.application-period-changes.get", ctx.params)
					.then((application_period_change) => {
						const stateMachine = periodStateMachine.createStateMachine(
							application_period_change[0].status,
							ctx,
						);
						return stateMachine.transitions();
					});
			},
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "string" },
				application_period_change: {
					type: "object",
					props: {
						reason: { type: "string", convert: true, optional: true },
						application_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						start_date: { type: "date", convert: true, optional: true },
						end_date: { type: "date", convert: true, optional: true },
						decision: {
							type: "enum",
							values: ["APPROVE", "REJECT"],
							optional: true,
						},
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const app_period_change = await ctx.call(
					"accommodation.application-period-changes.get",
					ctx.params,
				);
				const stateMachine = periodStateMachine.createStateMachine(app_period_change[0].status, ctx);

				if (["APPROVE", "REJECT"].includes(ctx.params.event)) {
					throw new ValidationError(
						"The events ['APPROVED', 'REJECTED'] cant be executed in this endpoint",
						"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_BAD_STATUS",
						{},
					);
				}
				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.application_period_change) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.application_period_change.decision)
				) {
					throw new ValidationError(
						"Decision is required to to change status to dispatch",
						"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_DECISION_NOT_FOUND",
						{},
					);
				}

				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_extra_change = await ctx.call(
					"accommodation.application-period-changes.get",
					ctx.params,
				);
				const stateMachine = periodStateMachine.createStateMachine(app_extra_change[0].status, ctx);
				if (stateMachine.can(app_extra_change[0].decision)) {
					return stateMachine[app_extra_change[0].decision.toLowerCase()]().then((result) => {
						// TODO: MISSING BILLING RECALCULATION
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_extra_change = await ctx.call(
					"accommodation.application-period-changes.get",
					ctx.params,
				);
				const stateMachine = periodStateMachine.createStateMachine(app_extra_change[0].status, ctx);

				if (stateMachine.can("ADMIN_REJECT")) {
					return stateMachine["adminReject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async haveOpenPeriodChangeRequests(ctx) {
			const open_changes = await this._count(ctx, {
				query: {
					application_id: ctx.params.application_id,
					status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				},
			});
			if (open_changes > 0)
				throw new ValidationError(
					"User already have open extras change requests",
					"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_ALREADY_EXISTS",
				);
		},
		async validateActiveApplication(ctx) {
			const application = await ctx.call("accommodation.applications.get", {
				id: ctx.params.application_id,
				withRelated: false
			});
			if (application[0].status != "contracted") {
				throw new ValidationError(
					"No active application finded in accommodation",
					"ACCOMMODATION_APPLICATION_NOT_FOUND",
				);
			}
		},

		async validateAcademicYear(ctx) {
			const application = await ctx.call("accommodation.applications.get", {
				id: ctx.params.application_id,
				withRelated: false
			});
			if (application.length) {
				const currentAcademicDates = await ctx.call("configuration.academic_years.find", {
					query: {
						academic_year: application[0].academic_year
					}
				});
				if (currentAcademicDates.length && !moment(new Date(ctx.params.start_date), "YYYY-MM-DD").isBetween(moment(currentAcademicDates[0].start_date, "YYYY-MM-DD"), moment(currentAcademicDates[0].end_date, "YYYY-MM-DD"), null, "[]")
					|| !moment(new Date(ctx.params.end_date), "YYYY-MM-DD").isBetween(moment(currentAcademicDates[0].start_date, "YYYY-MM-DD"), moment(currentAcademicDates[0].end_date, "YYYY-MM-DD"), null, "[]")) {
					throw new ValidationError(
						"Dates out of range defined in academic year",
						"ACCOMMODATION_DATES_OUT_OF_RANGE",
					);
				}
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
