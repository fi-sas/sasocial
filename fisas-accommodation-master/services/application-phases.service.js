"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const moment = require("moment");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-phases",
	table: "application_phase",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "application-phases")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"start_date",
			"end_date",
			"out_of_date_from",
			"academic_year",
			"application_phase",
			"fixed_accommodation_period",
			"out_of_date_validation",
			"out_of_date_validation_days",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
			out_of_date_from: { type: "date", convert: true, optional: true },
			application_phase: { type: "number" },
			academic_year: { type: "string" },
			fixed_accommodation_period: { type: "boolean", convert: true },
			out_of_date_validation: { type: "boolean", convert: true },
			out_of_date_validation_days: { type: "number", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					if (ctx.params.out_of_date_validation && !ctx.params.out_of_date_validation_days) {
						throw new Errors.ValidationError(
							"out_of_date_validation_days field is required",
							"ACCOMMODATION_OUT_DATE_VALIDATION_DAY_REQUIRED",
							{},
						);
					}
				},
				async function checkdisponibility(ctx) {
					if (ctx.params.start_date && ctx.params.end_date) {
						const applicationsPhase = await this._count(ctx, {
							query: (qb) => {
								qb.whereRaw("tstzrange(start_date,end_date, '[]') && tstzrange(?, ?, '[]')", [
									ctx.params.start_date,
									ctx.params.end_date,
								]);
							},
						});

						if (applicationsPhase > 0) {
							throw new Errors.ValidationError(
								"The start date and end date overlap with another application phase",
								"ACCOMMODATION_PHASE_OVERLAP",
								{},
							);
						}

						if (ctx.params.out_of_date_from && !moment(ctx.params.out_of_date_from).isBetween(
							ctx.params.start_date,
							ctx.params.end_date,
						)
						) {
							throw new Errors.ValidationError(
								"The out of date must be between start date and end date",
								"ACCOMMODATION_PHASE_INVALID_OUTOFDATE",
								{},
							);
						}
					}

					if (ctx.params.academic_year && ctx.params.application_phase) {
						const applicationsPhase = await this._count(ctx, {
							query: {
								academic_year: ctx.params.academic_year,
								application_phase: ctx.params.application_phase,
							},
						});
						if (applicationsPhase > 0) {
							throw new Errors.ValidationError(
								"Already exist a application phase for this academic year",
								"ACCOMMODATION_PHASE_ALREADY_EXIST",
								{},
							);
						}
					}
				},
				"validateAcademicYear"
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					if (ctx.params.out_of_date_validation && !ctx.params.out_of_date_validation_days) {
						throw new Errors.ValidationError(
							"out_of_date_validation_days field is required",
							"ACCOMMODATION_OUT_DATE_VALIDATION_DAY_REQUIRED",
							{},
						);
					}
				},
				"validateAcademicYear"
			],
			remove: ["isPossibleRemove"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getOpenPhase: {
			rest: "GET /openPhase",
			visibility: "published",
			scope: "accommodation:application-phases:open",
			handler(ctx) {
				return this._find(ctx, {
					query: (qb) => {
						qb.whereRaw("NOW() BETWEEN start_date AND end_date");
					},
					limit: 1,
				});
			},
		},

		getPhasesAfterToday: {
			rest: "GET /getPhasesAfterToday",
			visibility: "published",
			scope: "accommodation:application-phases:read",
			handler(ctx) {
				return ctx.call("configuration.academic_years.getCurrentAndFutureAcademicYear").then(response => {
					if (response.length) {
						return this._find(ctx, {
							query: {
								academic_year: response.map(r => r.academic_year),
							}
						});
					}
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countApplication = await ctx.call("accommodation.applications.count", {
					query: {
						application_phase_id: ctx.params.id,
					},
				});

				if (countApplication > 0) {
					throw new Errors.ValidationError(
						"You have applications with this application phase associated",
						"APPLICATION_PHASE_WITH_APPLICATIONS",
					);
				}
			}
		},
		async validateAcademicYear(ctx) {
			const academic_year = await ctx.call("configuration.academic_years.find", { query: { academic_year: ctx.params.academic_year } });
			if (academic_year.length == 0) {
				throw new Errors.ValidationError(
					"Academic year not found",
					"APPLICATION_PHASE_ACADEMIC_YEAR_NOT_FOUND",
				);
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
