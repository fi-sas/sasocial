"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
const roomStateMachine = require("./state-machines/application-room-changes.machine");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-room-changes",
	table: "application_room_change",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"typology_id",
			"residence_id",
			"permute_user_tin",
			"room_id",
			"reason",
			"application_id",
			"start_date",
			"type",
			"decision",
			"observations",
			"status",
			"created_at",
			"updated_at",
			"file_id",
		],
		defaultWithRelateds: ["history", "file"],
		withRelateds: {
			typology(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.typologies",
					"typology",
					"typology_id",
					"id",
					{},
					null,
					"translations",
				);
			},
			application(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.applications",
					"application",
					"application_id",
					"id",
					{},
					null,
					false,
				);
			},
			room(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.rooms", "room", "room_id", "id", {}, null, false);
			},
			residence(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.residences",
					"residence",
					"residence_id",
					"id",
					{},
					null,
					false,
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application-room-change-history",
					"history",
					"id",
					"application_room_change_id",
					{},
					null,
					false,
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id", "id", {}, null, false);
			},
		},
		entityValidator: {
			typology_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			residence_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			permute_user_tin: { type: "string", convert: true, optional: true },
			room_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			reason: { type: "string", convert: true, optional: true },
			application_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			start_date: { type: "date", convert: true },
			decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
			status: {
				type: "enum",
				values: ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"],
				default: "SUBMITTED",
			},
			type: { type: "enum", values: ["TYPOLOGY", "RESIDENCE", "PERMUTE", "ROOM"] },
			observations: { type: "string", convert: true, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			file_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateActiveApplication",
				"haveOpenRoomChangeRequests",
				"validateStartDate",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validatePermuteUserTin",
			],
			update: [
				"validateStartDate",
				"validatePermuteUserTin",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validateStartDate",
				"validatePermuteUserTin",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}
					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "application_room_change.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				async function sendNotification(ctx, response) {
					const application = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_APPLICATION_" + response[0].type + "_CHANGE_SUBMITTED",
						user_id: application[0].user_id,
						user_data: {},
						data: { application: application },
						variables: {},
						external_uuid: null,
					});
					return response;
				},
			],
			admin_approve: [
				async function updateApplicationBilling(ctx, res) {
					if (res[0].decision == "APPROVE") {
						if (res[0].type == "PERMUTE") {
							const permuted_application = await ctx.call("accommodation.applications.find", {
								query: { tin: res[0].permute_user_tin, status: "contracted" },
							});
							if (permuted_application.length > 0) {
								const application = await ctx.call("accommodation.applications.get", {
									id: res[0].application_id,
								});
								// application =>  res[0].room_id;
								// permuted_application =>  application[0].room_id;
								const id = permuted_application[0].id;
								const createdRoomChange = await this._create(ctx, {
									application_id: id,
									room_id: application[0].room_id,
									residence_id: application[0].assigned_residence_id,
									permute_user_tin: application[0].tin,
									start_date: res[0].start_date,
									reason: "Gerado automáticamente após aprovação de um pedido de permuta",
									type: "PERMUTE",
									decision: "APPROVE",
									status: "APPROVED",
									updated_at: new Date(),
									created_at: new Date(),
								});
								ctx.call("accommodation.billings.processExtrasAndRegimeAndTypologyChange", {
									application_id: createdRoomChange[0].application_id,
									start_date: createdRoomChange[0].start_date,
								});
							}
						}

						const next_month_first_day = moment(new Date(), "YYYY-MM-DD")
							.add(1, "M")
							.startOf("month");

						if (moment(res[0].start_date).isBefore(next_month_first_day)) {
							res[0].start_date = next_month_first_day;
						}
						ctx.call("accommodation.billings.processExtrasAndRegimeAndTypologyChange", {
							application_id: res[0].application_id,
							start_date: res[0].start_date,
						});
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		get_status: {
			rest: "GET /:id/status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			handler(ctx) {
				return ctx
					.call("accommodation.application-room-changes.get", ctx.params)
					.then((application_room_change) => {
						const stateMachine = roomStateMachine.createStateMachine(
							application_room_change[0].status,
							ctx,
						);
						return stateMachine.transitions();
					});
			},
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "string" },
				application_room_change: {
					type: "object",
					props: {
						typology_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						residence_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						room_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						reason: { type: "string", convert: true, optional: true },
						application_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						start_date: { type: "date", convert: true, optional: true },
						decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const app_room_change = await ctx.call(
					"accommodation.application-room-changes.get",
					ctx.params,
				);
				const stateMachine = roomStateMachine.createStateMachine(app_room_change[0].status, ctx);

				if (["APPROVE", "REJECT"].includes(ctx.params.event)) {
					throw new ValidationError(
						"The events ['APPROVED', 'REJECTED'] cant be executed in this endpoint",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_BAD_STATUS",
						{},
					);
				}
				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.application_room_change) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.application_room_change.decision) ||
					(ctx.params.event == "DISPATCH" &&
						ctx.params.application_room_change.decision == "APPROVE" &&
						app_room_change[0].type != "PERMUTE" &&
						!ctx.params.application_room_change.room_id)
				) {
					throw new ValidationError(
						"Decision and room are required to to change status to dispatch",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_DECISION_NOT_FOUND",
						{},
					);
				}

				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_room_change = await ctx.call(
					"accommodation.application-room-changes.get",
					ctx.params,
				);
				const stateMachine = roomStateMachine.createStateMachine(app_room_change[0].status, ctx);
				if (stateMachine.can(app_room_change[0].decision)) {
					return stateMachine[app_room_change[0].decision.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_room_change = await ctx.call(
					"accommodation.application-room-changes.get",
					ctx.params,
				);
				const stateMachine = roomStateMachine.createStateMachine(app_room_change[0].status, ctx);

				if (stateMachine.can("ADMIN_REJECT")) {
					return stateMachine["adminReject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async haveOpenRoomChangeRequests(ctx) {
			const open_changes = await this._count(ctx, {
				query: {
					application_id: ctx.params.application_id,
					status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				},
			});
			if (open_changes > 0)
				throw new ValidationError(
					"User already have ongoing room changes requests",
					"ACCOMMODATION_APPLICATION_ROOM_CHANGE_ALREADY_EXISTS",
				);
		},
		async validateStartDate(ctx) {
			if (ctx.params.start_date && !ctx.meta.isBackoffice) {
				ctx.params.start_date = moment(ctx.params.start_date).set("date", "1").format("YYYY-MM-DD");
				const start_date = moment(ctx.params.start_date).set("date", "1");
				const next_month_first_day = moment(new Date(), "YYYY-MM-DD").add(1, "M").startOf("month");
				if (start_date.isBefore(next_month_first_day)) {
					throw new ValidationError(
						"User only can change contract on next or more months",
						"ACCOMMODATION_APPLICATION_ROOM_CHANGE_INVALID_DATE",
					);
				}
				ctx.params.start_date = start_date.format("YYYY-MM-DD");
			}
		},
		async validateActiveApplication(ctx) {
			const application = await ctx.call("accommodation.applications.get", {
				id: ctx.params.application_id,
			});
			if (application[0].status != "contracted") {
				throw new ValidationError(
					"No active application finded in accommodation",
					"ACCOMMODATION_APPLICATION_NOT_FOUND",
				);
			}
		},
		async validatePermuteUserTin(ctx) {
			if (ctx.params.type == "PERMUTE" && ctx.params.permute_user_tin) {
				const validPermuteUser = await ctx.call("accommodation.applications.find", {
					query: {
						tin: ctx.params.permute_user_tin,
						status: "contracted",
					},
				});
				if (validPermuteUser.length == 0) {
					throw new ValidationError(
						"User with provided tin dont have a application in provided room",
						"ACCOMMODATION_PERMUTE_USER_TIN_NOT_FOUND",
					);
				}
				ctx.params.room_id = validPermuteUser[0].room_id;
				ctx.params.residence_id = validPermuteUser[0].assigned_residence_id;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
