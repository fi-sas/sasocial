"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-tariff-change-history",
	table: "application_tariff_change_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],


	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_tariff_change_id",
			"status",
			"user_id",
			"notes",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			application_tariff_change_id: { type: "number" },
			status: {
				type: "enum",
				values: [
					"SUBMITTED",
					"ANALYSIS",
					"DISPATCH",
					"APPROVED",
					"REJECTED",
					"CANCELLED"
				], optional: true,
			},
			user_id: { type: "number", optional: true },
			notes: { type: "string", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
