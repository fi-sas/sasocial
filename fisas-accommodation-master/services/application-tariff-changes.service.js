"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
const tariffStateMachine = require("./state-machines/application-tariff-changes.machine");
const Cron = require("moleculer-cron");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-tariff-changes",
	table: "application_tariff_change",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications"), Cron],
	crons: [
		{
			name: "automaticChangeApplicationTariff",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("accommodation.application-tariff-changes")
					.actions.automatic_change_application_tariff()
					.then(() => { });
			},
			runOnInit: function () { },
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"tariff_id",
			"old_tariff_id",
			"reason",
			"application_id",
			"start_date",
			"decision",
			"status",
			"created_at",
			"updated_at",
			"file_id",
			"has_scholarship",
		],
		defaultWithRelateds: ["tariff", "old_tariff", "history", "file"],
		withRelateds: {
			tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "tariff", "tariff_id");
			},
			old_tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "old_tariff", "old_tariff_id");
			},
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application-tariff-change-history",
					"history",
					"id",
					"application_tariff_change_id",
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			tariff_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			old_tariff_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			reason: { type: "string", convert: true, optional: true },
			application_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			start_date: { type: "date", convert: true },
			decision: {
				type: "enum",
				values: ["APPROVE", "REJECT"],
				optional: true,
			},
			status: {
				type: "enum",
				values: ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"],
				default: "SUBMITTED",
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			file_id: { type: "number", positive: true, integer: true, convert: true, optional: true },
			has_scholarship: { type: "boolean", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateActiveApplication",
				"haveOpenTariffsChangeRequests",
				"validateStartDate",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					if (ctx.params.start_date) {
						ctx.params.start_date = moment(ctx.params.start_date)
							.set("date", "1")
							.format("YYYY-MM-DD");
					}
				},
			],
			update: [
				"validateStartDate",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validateStartDate",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}
					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "application_tariff_change.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				async function sendNotification(ctx, response) {
					ctx.call("accommodation.application-tariff-change-history.create", {
						application_tariff_change_id: response[0].id,
						user_id: ctx.meta.user.id,
						notes: "",
						status: response[0].status,
					});
					const application = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_APPLICATION_TARIFF_CHANGE_SUBMITTED",
						user_id: application[0].user_id,
						user_data: {},
						data: {
							application: application,
						},
						variables: {},
						external_uuid: null,
					});
					return response;
				},
			],
			admin_approve: [
				async function updateApplicationBilling(ctx, res) {
					if (res[0].decision == "APPROVE") {
						ctx.call("accommodation.billings.processExtrasAndRegimeAndTypologyChange", {
							application_id: res[0].application_id,
							start_date: res[0].start_date,
						});
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		automatic_change_application_tariff: {
			async handler(ctx) {
				return this._find(ctx, {
					withRelated: false,
					query: (qb) => {
						qb.select(
							"application_tariff_change.id",
							"application_tariff_change.application_id",
							"application_tariff_change.status",
							"application_tariff_change.decision",
							"application_tariff_change.start_date",
							"application_tariff_change.tariff_id",
						);
						qb.innerJoin(
							"application",
							"application.id",
							"application_tariff_change.application_id",
						);
						qb.where("application.status", "=", "contracted");
						qb.where("application_tariff_change.start_date", "<=", new Date());
						qb.where("application_tariff_change.status", "=", "APPROVED");
					},
				}).then((tariff_change) => {
					for (const tc of tariff_change) {
						ctx.call("accommodation.applications.patch", {
							id: tc.application_id,
							tariff_id: tc.tariff_id,
						});
					}
				});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		get_status: {
			rest: "GET /:id/status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			handler(ctx) {
				return ctx
					.call("accommodation.application-tariff-changes.get", ctx.params)
					.then((application_tariff_change) => {
						const stateMachine = tariffStateMachine.createStateMachine(
							application_tariff_change[0].status,
							ctx,
						);
						return stateMachine.transitions();
					});
			},
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "string" },
				application_tariff_change: {
					type: "object",
					props: {
						tariff_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						reason: { type: "string", convert: true, optional: true },
						application_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: true,
						},
						start_date: { type: "date", convert: true, optional: true },
						decision: {
							type: "enum",
							values: ["APPROVE", "REJECT"],
							optional: true,
						},
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const app_tariff_change = await ctx.call(
					"accommodation.application-tariff-changes.get",
					ctx.params,
				);
				const stateMachine = tariffStateMachine.createStateMachine(
					app_tariff_change[0].status,
					ctx,
				);

				if (["APPROVE", "REJECT"].includes(ctx.params.event)) {
					throw new ValidationError(
						"The events ['APPROVED', 'REJECTED'] cant be executed in this endpoint",
						"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_BAD_STATUS",
						{},
					);
				}
				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.application_tariff_change) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.application_tariff_change.decision)
				) {
					throw new ValidationError(
						"Decision is required to to change status to dispatch",
						"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_DECISION_NOT_FOUND",
						{},
					);
				}

				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_tariff_change = await ctx.call(
					"accommodation.application-tariff-changes.get",
					ctx.params,
				);
				const stateMachine = tariffStateMachine.createStateMachine(
					app_tariff_change[0].status,
					ctx,
				);
				if (stateMachine.can(app_tariff_change[0].decision)) {
					return stateMachine[app_tariff_change[0].decision.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const app_tariff_change = await ctx.call(
					"accommodation.application-tariff-changes.get",
					ctx.params,
				);
				const stateMachine = tariffStateMachine.createStateMachine(
					app_tariff_change[0].status,
					ctx,
				);

				if (stateMachine.can("ADMIN_REJECT")) {
					return stateMachine["adminReject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async haveOpenTariffsChangeRequests(ctx) {
			const open_changes = await this._count(ctx, {
				query: {
					application_id: ctx.params.application_id,
					status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				},
			});
			if (open_changes > 0)
				throw new ValidationError(
					"User already have ongoing tariff changes requests",
					"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_ALREADY_EXISTS",
				);
		},
		async validateStartDate(ctx) {
			if (ctx.params.start_date) {
				const start_date = moment(ctx.params.start_date).set("date", "1");
				const next_month_first_day = moment(new Date(), "YYYY-MM-DD").add(1, "M").startOf("month");
				if (start_date.isBefore(next_month_first_day)) {
					throw new ValidationError(
						"User only can change extras on next or more months",
						"ACCOMMODATION_APPLICATION_EXTRA_CHANGE_INVALID_DATE",
					);
				}
				ctx.params.start_date = start_date.format("YYYY-MM-DD");
			}
		},
		async validateActiveApplication(ctx) {
			const application = await ctx.call("accommodation.applications.get", {
				id: ctx.params.application_id,
			});
			if (application[0].status != "contracted") {
				throw new ValidationError(
					"No active application finded in accommodation",
					"ACCOMMODATION_APPLICATION_NOT_FOUND",
				);
			}
			ctx.params.old_tariff_id = application[0].tariff_id;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
