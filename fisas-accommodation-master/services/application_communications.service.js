"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Validator = require("moleculer").Validator;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const moment = require("moment");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-communications",
	table: "application_communication",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"start_date",
			"end_date",
			"observations",
			"response",
			"status",
			"type",
			"created_at",
			"updated_at",
			"entry_hour",
			"exit_hour"
		],
		defaultWithRelateds: [],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},
		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			start_date: { type: "date", convert: true, optional: true },
			end_date: { type: "date", convert: true, optional: true },
			observations: { type: "string", optional: true },
			response: { type: "string", optional: true },
			status: { type: "enum", values: ["SUBMITTED", "CLOSED"], optional: true },
			type: { type: "enum", values: ["ENTRY", "EXIT"] },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			entry_hour: { type: "string", optional: true },
			exit_hour: { type: "string", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";
				},
				function validateRequiredFields(ctx) {
					const v = new Validator();
					if (ctx.params.type == "ENTRY") {
						const schema = {
							start_date: { type: "date", convert: true },
							entry_hour: { type: "string", convert: true },
						};
						const check = v.compile(schema);
						const res = check(ctx.params);
						if (res !== true)
							return Promise.reject(new ValidationError("Entity validation error!", null, res));
					} else if (ctx.params.type == "EXIT") {
						const schema = {
							end_date: { type: "date", convert: true },
							exit_hour: { type: "string", convert: true },
						};
						const check = v.compile(schema);
						const res = check(ctx.params);
						if (res !== true)
							return Promise.reject(new ValidationError("Entity validation error!", null, res));
					}
				},
				async function validateIfAlreadyHaveOneCommunication(ctx) {
					const communications = await this._find(ctx, {
						query: { type: ctx.params.type, application_id: ctx.params.application_id },
					});
					if (communications.length > 0) {
						throw new ValidationError(
							"Already have one communication",
							"ACCOMMODATION_COMMUNICATION_ALREADY_EXISTS",
							{},
						);
					}
				},
				async function validateApplication(ctx) {
					const application = await ctx.call("accommodation.applications.get", {
						id: ctx.params.application_id,
					});
					if (application[0].status != "contracted") {
						throw new ValidationError(
							"Application must be in contracted status",
							"ACCOMMODATION_APPLICATION_NOT_IN_CONTRACTED_STATUS",
							{},
						);
					}
					if (
						ctx.params.start_date &&
						!moment(ctx.params.start_date).isBetween(
							moment(application[0].start_date),
							moment(application[0].end_date),
							null,
							"[]"
						)
					) {
						throw new ValidationError(
							"Start date must be between accommodation period",
							"ACCOMMODATION_APPLICATION_INVALID_START_DATE",
							{},
						);
					}
					if (
						ctx.params.end_date &&
						!moment(ctx.params.end_date).isBetween(
							moment(application[0].start_date),
							moment(application[0].end_date),
							null,
							"[]"
						)
					) {
						throw new ValidationError(
							"End date must be between accommodation period",
							"ACCOMMODATION_APPLICATION_INVALID_END_DATE",
							{},
						);
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}

					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "application_communication.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: ["sendNotification"],
			close: ["sendNotification"],
			reply: [
				async function sendReplyNotification(ctx, res) {
					const application = await ctx.call("accommodation.applications.get", {
						id: res[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_APPLICATION_" + res[0].type + "_COMMUNICATION_REPLY",
						user_id: application[0].user_id,
						user_data: {},
						data: {
							application: application,
							response: res[0].response,
						},
						variables: {},
						external_uuid: null,
					});
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		reply: {
			visibility: "published",
			rest: "POST /:id/reply",
			scope: "accommodation:applications:dispatch",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				response: { type: "string", convert: true, optional: true },
			},
			async handler(ctx) {
				return this._update(ctx, { id: ctx.params.id, response: ctx.params.response }, true);
			},
		},
		close: {
			visibility: "published",
			rest: "POST /:id/close",
			scope: "accommodation:applications:dispatch",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				observations: { type: "string", convert: true, optional: true },
			},
			async handler(ctx) {
				return this._update(
					ctx,
					{ id: ctx.params.id, status: "CLOSED", observations: ctx.params.observations },
					true,
				);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async sendNotification(ctx, res) {
			const application = await ctx.call("accommodation.applications.get", {
				id: res[0].application_id,
			});
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key:
					"ACCOMMODATION_APPLICATION_" + res[0].type + "_COMMUNICATION_" + res[0].status,
				user_id: application[0].user_id,
				user_data: {},
				data: {
					application: application,
				},
				variables: {},
				external_uuid: null,
			});
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
