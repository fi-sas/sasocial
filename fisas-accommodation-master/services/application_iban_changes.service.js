"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { swifts } = require("./helpers/bic_swift.js");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-iban-changes",
	table: "application_iban_change",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"iban",
			"allow_direct_debit",
			"status",
			"file_id",
			"created_at",
			"updated_at",
			"swift",
			"sepa_id_debit_auth",
			"sepa_signtr_date",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			iban: { type: "string", convert: true, optional: true },
			allow_direct_debit: { type: "boolean", convert: true },
			status: { type: "enum", values: ["SUBMITTED", "EXPIRED"], optional: true },
			file_id: { type: "number", optional: true, integer: true, convert: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";
				},
				async function validateApplication(ctx) {
					const application = await ctx.call("accommodation.applications.get", {
						id: ctx.params.application_id,
					});
					if (application[0].status != "contracted") {
						throw new ValidationError(
							"Application must be in contracted status",
							"ACCOMMODATION_APPLICATION_NOT_IN_CONTRACTED_STATUS",
							{},
						);
					}
				},
				function sanatizeIbanSwift(ctx) {
					if (ctx.params.iban) {
						ctx.params.iban = ctx.params.iban.replace(/\s/g, "").toUpperCase();

						let oBicSwift = swifts.find((o) => o.cod_bdp === ctx.params.iban.substr(4, 4));

						if (oBicSwift) {
							ctx.params.swift = oBicSwift.bic_swift;
						}
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);
						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}
					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "application_iban_change.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				"sendNotification",
				async function cancelOtherIbanChangeRequest(ctx, res) {
					const existents = await this._find(ctx, {
						withRelated: false,
						query: (qb) => {
							qb.where("id", "!=", res[0].id);
							qb.where("application_id", "=", res[0].application_id);
							qb.where("status", "=", "SUBMITTED");
						},
					});
					for (const e of existents) {
						await this._update(ctx, { id: e.id, status: "EXPIRED" }, true);
					}
					return res;
				},
				async function updateAuthID(ctx, res) {
					if (!res[0].allow_direct_debit) return res;

					const application = await ctx.call("accommodation.applications.get", {
						id: res[0].application_id,
					});

					const existents = await this._find(ctx, {
						withRelated: false,
						query: (qb) => {
							qb.where("id", "!=", res[0].id);
							qb.where("application_id", "=", res[0].application_id);
							qb.where("status", "=", "EXPIRED");
						},
						sort: "-created_at",
					});

					let newSepaIdDebitAuth = application[0].sepa_id_debit_auth;
					let newSepaSigntrDate = res[0].created_at;

					if (existents.length) {
						if (existents[0].sepa_id_debit_auth) {
							const currAuthOrderNum = (parseInt(existents[0].sepa_id_debit_auth.substr(0, 2)) + 1)
								.toString()
								.padStart(2, "0");
							const currAuthTin = existents[0].sepa_id_debit_auth.substr(2);
							newSepaIdDebitAuth = currAuthOrderNum + currAuthTin;
						}
					} else {
						if (newSepaIdDebitAuth) {
							const currAuthOrderNum = (parseInt(newSepaIdDebitAuth.substr(0, 2)) + 1)
								.toString()
								.padStart(2, "0");
							const currAuthTin = newSepaIdDebitAuth.substr(2);
							newSepaIdDebitAuth = currAuthOrderNum + currAuthTin;
						} else {
							newSepaIdDebitAuth = "00" + application[0].tin;
						}
					}
					await this._update(
						ctx,
						{
							id: res[0].id,
							sepa_id_debit_auth: newSepaIdDebitAuth,
							sepa_signtr_date: newSepaSigntrDate,
						},
						true,
					);

					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async sendNotification(ctx, res) {
			const application = await ctx.call("accommodation.applications.get", {
				id: res[0].application_id,
			});
			await ctx
				.call("notifications.alerts.create_alert", {
					alert_type_key: "ACCOMMODATION_APPLICATION_IBAN_CHANGE_SUBMITTED",
					user_id: application[0].user_id,
					user_data: {},
					data: {
						application: application,
					},
					variables: {},
					external_uuid: null,
				})
				.catch((err) => {
					this.logger.info("ERR");
					this.logger.info(err);
					//throw err;
				});
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
