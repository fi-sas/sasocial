"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError, ForbiddenError } = require("@fisas/ms_core/src/helpers/errors");
const moment = require("moment");
const _ = require("lodash");
const ApplicationStateMachine = require("./state-machines/application.machine");
const pg = require("pg");
const Cron = require("moleculer-cron");
const Validator = require("moleculer").Validator;
const { swifts } = require("./helpers/bic_swift.js");
const { STATS } = require("./utils/stats");
const {
	checkAllowedResidencesQuery,
	checkAllowedResidencesParams,
} = require("./utils/residences-scope");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.applications",
	table: "application",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications"), Cron],

	/*
	 * Crons
	 */
	crons: [
		{
			name: "automaticChangeAssignedStatusAfter5Days",
			cronTime: "0 * * * *",
			onTick: function () {
				this.getLocalService("accommodation.applications")
					.actions.automatic_change_status()
					.then(() => {});
			},
			runOnInit: function () {},
		},
		{
			// Cron to update room_id in application
			// when room change requests start_date <= now
			name: "applyRoomChangesOnApplication",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("accommodation.applications")
					.actions.applyRoomChangesOnApplication()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"full_name",
			"identification",
			"tin",
			"student_number",
			"course_id",
			"academic_year",
			"course_year",
			"admission_date",
			"email",
			"phone_1",
			"phone_2",
			"address",
			"city",
			"start_date",
			"end_date",
			"room_id",
			"user_id",
			"signed_contract_file_id",
			"liability_term_file_id",
			"postal_code",
			"applicant_birth_date",
			"parent_profession_father",
			"parent_profession_mother",
			"household_elements_number",
			"household_total_income",
			"household_distance",
			"income_statement_delivered",
			"payment_method_id",
			"residence_id",
			"second_option_residence_id",
			"submission_date",
			"submitted_out_of_date",
			"assigned_residence_id",
			"regime_id",
			"current_regime_id",
			"has_used_residences_before",
			"has_scholarship",
			"which_residence_id",
			"which_room_id",
			"applied_scholarship",
			"scholarship_value",
			"payment_method",
			"iban",
			"swift",
			"applicant_consent",
			"application_phase",
			"application_phase_id",
			"consent_date",
			"decision",
			"status",
			"opposition_request",
			"opposition_answer",
			"has_opposed",
			"observations",
			"billing_status",
			"total_unbilled",
			"total_unpaid",
			"next_period",
			"charged_deposit",
			"paid_deposit",
			"available_deposit",
			"available_retroactive",
			"updated_at",
			"created_at",
			"organic_unit_id",
			"nationality",
			"gender",
			"mother_name",
			"father_name",
			"incapacity",
			"incapacity",
			"incapacity_degree",
			"incapacity_type",
			"country",
			"househould_elements_in_university",
			"secundary_email",
			"document_type_id",
			"patrimony_category",
			"patrimony_category_id",
			"international_student",
			"income_source",
			"other_income_source",
			"preferred_typology_id",
			"tariff_id",
			"updated_end_date",
			"per_capita_income",
			"allow_direct_debit",
			"unassigned_date",

			"sepa_id_debit_auth",
			"sepa_signtr_date",
		],
		defaultWithRelateds: [
			"residence",
			"assignedResidence",
			"regime",
			"course",
			"liability_term_file",
			"signed_contract_file",
			"income_statement_files",
			"history",
			"whichResidence",
			"whichRoom",
			"room",
			"extras",
			"house_hould_elements",
			"second_option_residence",
			"organic_unit",
			"document_type",
			"preferred_typology",
			"user",
			"tariff",
			"files",
		],
		withRelateds: {
			billings(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "accommodation.billings", "billings", "id", "application_id");
			},
			assignedResidence(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.residences",
					"assignedResidence",
					"assigned_residence_id",
					"id",
					{},
					"name,current_account_id",
					"regimes,extras",
				);
			},
			whichResidence(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.residences",
					"whichResidence",
					"which_residence_id",
					"id",
					{},
					"name",
					false,
				);
			},
			second_option_residence(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.residences",
					"second_option_residence",
					"second_option_residence_id",
					"id",
					{},
					"name",
					false,
				);
			},
			whichRoom(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.rooms",
					"whichRoom",
					"which_room_id",
					"id",
					{},
					"name",
					false,
				);
			},
			room(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.rooms",
					"room",
					"room_id",
					"id",
					{},
					"name,typology",
					"typology",
				);
			},
			residence(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.residences",
					"residence",
					"residence_id",
					"id",
					{},
					"name",
					false,
				);
			},
			regime(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.regimes", "regime", "regime_id");
			},
			currentRegime(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.regimes", "currentRegime", "current_regime_id");
			},
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			liability_term_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "liability_term_file", "liability_term_file_id");
			},
			signed_contract_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "signed_contract_file", "signed_contract_file_id");
			},
			income_statement_files(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application-income-statement-files",
					"income_statement_files",
					"id",
					"application_id",
				);
			},
			extras(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application_extras",
					"extras",
					"id",
					"application_id",
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.applications-history",
					"history",
					"id",
					"application_id",
				);
			},
			house_hould_elements(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.house-hould-elements",
					"house_hould_elements",
					"id",
					"application_id",
				);
			},
			organic_unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organic_unit", "organic_unit_id");
			},
			async user_previous_applications(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return this.adapter
							.find({
								query: (qb) => {
									qb.select(
										"application.id",
										"application.submission_date",
										"application.status",
										"application.start_date",
										"application.end_date",
										"application.observations",
										"residence.name",
									);
									qb.leftJoin("residence", "application.assigned_residence_id", "residence.id");
									qb.where("application.user_id", "=", doc.user_id);
									qb.where("application.id", "!=", doc.id);
									qb.orderBy("submission_date", "desc");
									qb.limit(10);
								},
							})
							.then((res) => (doc.user_previous_applications = res));
					}),
				);
			},
			document_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.document-types",
					"document_type",
					"document_type_id",
				);
			},
			preferred_typology(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"accommodation.typologies",
					"preferred_typology",
					"preferred_typology_id",
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id",
					"id",
					{},
					"name,profile_id,email,phone,address,postal_code,city,tin,identification",
					false,
				);
			},
			tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "tariff", "tariff_id", "id");
			},
			occurrences(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.occurrences",
					"occurrences",
					"id",
					"application_id",
				);
			},
			files(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.application_files",
					"files",
					"id",
					"application_id",
				);
			},
			iban_changes(ids, docs, rule, ctx) {
				//hasMany with query
				return hasMany(
					docs,
					ctx,
					"accommodation.application-iban-changes", //serviceName
					"iban_changes", //newField
					"id", //identificatorField
					"application_id", //searchField
					{
						status: "SUBMITTED", //additionalQuery
					},
					null, //fields to return or null for all
					false, //withRelated
				).then((res) => {
					return docs.map((doc) => {
						if (doc.iban_changes.length) {
							doc.allow_direct_debit = doc.iban_changes[0].allow_direct_debit;
							doc.iban = doc.iban_changes[0].iban;
							doc.swift = doc.iban_changes[0].swift;
							doc.sepa_id_debit_auth = doc.iban_changes[0].sepa_id_debit_auth;
							doc.sepa_signtr_date = doc.iban_changes[0].sepa_signtr_date;
						}
					});
				});
			},
		},
		entityValidator: {
			// Personal data
			full_name: { type: "string" },
			applicant_birth_date: { type: "date", convert: true },
			identification: { type: "string" },
			document_type_id: { type: "number", convert: true, positive: true },
			tin: { type: "string" },
			nationality: { type: "string" },
			gender: {
				type: "enum",
				values: ["M", "F", "U"],
			},
			incapacity: { type: "boolean" },
			incapacity_degree: { type: "string", optional: true },
			incapacity_type: { type: "string", optional: true },
			mother_name: { type: "string" },
			father_name: { type: "string" },
			// Contacts
			email: { type: "email" },
			secundary_email: { type: "email", optional: true },
			phone_1: { type: "string" },
			phone_2: { type: "string" },
			// Residence
			residence_id: { type: "number", convert: true, optional: false },
			second_option_residence_id: { type: "number", min: 0, optional: true, convert: true },
			regime_id: { type: "number" },
			current_regime_id: { type: "number", optional: true },
			extra_ids: { type: "array", items: "number" },
			has_used_residences_before: { type: "boolean" },
			which_residence_id: { type: "number", optional: true, convert: true },
			which_room_id: { type: "number", optional: true, convert: true },
			// Academic data
			international_student: { type: "boolean", convert: true },
			organic_unit_id: { type: "number", positive: true, convert: true },
			course_id: { type: "number" },
			course_year: { type: "number", convert: true },
			student_number: { type: "string" },
			admission_date: { type: "date", convert: true },
			// HouseOld
			country: { type: "string" },
			city: { type: "string" },
			address: { type: "string" },
			postal_code: { type: "string" },
			household_distance: { type: "number" },
			household_elements_number: { type: "number", optional: true },
			household_total_income: { type: "number", optional: true },
			income_statement_delivered: { type: "boolean", convert: true },
			income_statement_files_ids: { type: "array", items: "number", optional: true },
			income_source: {
				type: "array",
				items: {
					type: "enum",
					values: [
						"DEPENDENT_WORKER",
						"INDEPENDENT_WORK",
						"PENSIONS",
						"UNEMPLOYMENT_ALLOWANCE",
						"RSI",
						"OTHER",
					],
				},
				optional: true,
			},
			other_income_source: { type: "string", optional: true },
			house_hould_elements: {
				type: "array",
				empty: true,
				optional: true,
				items: {
					type: "object",
					props: {
						kinship: { type: "string" },
						profession: { type: "string" },
					},
				},
			},
			househould_elements_in_university: { type: "number", convert: true, optional: true },
			patrimony_category_id: { type: "number", convert: true, optional: true },
			// Socialschollarship
			has_scholarship: { type: "boolean", optional: true },
			applied_scholarship: { type: "boolean" },
			scholarship_value: { type: "number", optional: true, convert: true, min: 0 },
			// Observations
			observations: { type: "string", empty: true },

			academic_year: { type: "string" },
			start_date: { type: "date", optional: true, convert: true },
			end_date: { type: "date", optional: true, convert: true },
			room_id: { type: "number", optional: true },
			user_id: { type: "number" },
			signed_contract_file_id: { type: "number", optional: true },
			liability_term_file_id: { type: "number", optional: true },
			// payment_method_id: { type: "number" },
			submission_date: { type: "date", convert: true },
			submitted_out_of_date: { type: "boolean" },
			assigned_residence_id: { type: "number", optional: true },
			iban: { type: "string", empty: true, optional: true },
			allow_direct_debit: { type: "boolean", optional: true },
			swift: { type: "string", optional: true },
			applicant_consent: { type: "boolean", value: true, strict: true },
			application_phase: { type: "number" },
			application_phase_id: { type: "number" },
			student_draft: { type: "boolean", optional: true },
			consent_date: { type: "date", convert: true },
			decision: {
				type: "enum",
				values: ["ASSIGN", "UNASSIGN", "ENQUEUE"],
				optional: true,
			},
			status: {
				type: "enum",
				values: [
					"cancelled",
					"draft",
					"submitted",
					"analysed",
					"pending",
					"assigned",
					"unassigned",
					"queued",
					"confirmed",
					"opposition",
					"contracted",
					"closed",
					"rejected",
					"withdrawal",
				],
			},
			opposition_request: { type: "string", optional: true },
			opposition_answer: { type: "string", optional: true },
			has_opposed: { type: "boolean", optional: true },
			billing_status: {
				type: "enum",
				values: [
					"waiting",
					"proccessing",
					"proccessed",
					"verifieing",
					"verified",
					"error_in_proccessing",
					"error_in_verifieing",
				],
			},
			total_unbilled: { type: "number", optional: true },
			total_unpaid: { type: "number", optional: true },
			next_period: { type: "date", convert: true, optional: true },
			charged_deposit: { type: "number", optional: true },
			paid_deposit: { type: "number", optional: true },
			available_deposit: { type: "number", optional: true },
			available_retroactive: { type: "number", optional: true },
			preferred_typology_id: { type: "number", optional: true },
			tariff_id: { type: "number", optional: true },
			updated_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
			per_capita_income: { type: "number", convert: true, optional: true },
			file_ids: { type: "array", items: "number", optional: true },
			unassigned_date: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			find: [
				function checkQuery(ctx) {
					if (ctx.params.query) {
						if (
							ctx.params.query.debit_direct_active !== null &&
							ctx.params.query.debit_direct_active !== undefined
						) {
							const debit_direct = ctx.params.query.debit_direct_active;
							delete ctx.params.query.debit_direct_active;
							ctx.params.extraQuery = (qb) => {
								qb.select("application.*");
								qb.leftJoin(
									this.adapter.raw(`(select * from
								application_iban_change aict1
								where
									id in (select max(id) from application_iban_change aict2
											where
											aict2.application_id = aict1.application_id )) as aic`),
									"aic.application_id ",
									"application.id",
								);
								qb.where(
									this.adapter.raw(
										"COALESCE(aic.allow_direct_debit, application.allow_direct_debit)",
									),
									debit_direct,
								);
							};
						}
					}
				},
			],
			list: [
				checkAllowedResidencesQuery,
				function checkQuery(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.user_id = ctx.meta.user.id;
					}
					if (ctx.params.query) {
						if (ctx.params.query.application_residence_id) {
							const residence_id = ctx.params.query.application_residence_id;
							ctx.params.extraQuery = (qb) => {
								qb.where((qB) => {
									qB.where("assigned_residence_id", "=", residence_id);
									qB.orWhere((qbb) => {
										qbb.whereNull("assigned_residence_id");
										qbb.andWhere("residence_id", "=", residence_id);
									});
								});
							};
						}
						delete ctx.params.query.application_residence_id;
					}
				},
			],
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "submitted";
					ctx.params.consent_date = new Date();
					ctx.params.submission_date = new Date();
					ctx.params.billing_status = "waiting";
					ctx.params.current_regime_id = ctx.params.regime_id;
					ctx.params.user_id = ctx.meta.isBackoffice ? ctx.params.user_id : ctx.meta.user.id;

					const current = moment();
					// If default start date has not been provided, set it as the next September 1st
					if (!ctx.params.start_date) {
						ctx.params.start_date = current.toISOString();
					}

					// If default end date has not been provided, set it as the June 30th after the start date
					if (!ctx.params.end_date) {
						const target = moment(current).month(5).endOf("month");
						ctx.params.end_date = target.isAfter(current)
							? target.toISOString()
							: target.add(1, "year").toISOString();
					}
					ctx.params.updated_end_date = ctx.params.end_date;

					ctx.params.submitted_out_of_date = false;
					if (ctx.params.patrimony_category_id) {
						const patrimony_category = await ctx.call("accommodation.patrimony-categories.get", {
							id: ctx.params.patrimony_category_id,
						});
						const reference_value = patrimony_category[0].reference_value
							? patrimony_category[0].reference_value
							: 0;
						ctx.params.per_capita_income =
							(ctx.params.household_total_income + reference_value) /
							ctx.params.house_hould_elements.length;
					}
				},
				"validateExternalEntities",
				function fillUserData(ctx) {
					if (!ctx.meta.isBackoffice) {
						const user = ctx.meta.user;

						ctx.params = _.defaults(ctx.params, {
							user_id: user.id,
							email: user.email,
							phone_1: user.phone,
							address: user.address,
							city: user.city,
							postal_code: user.postal_code,
							applicant_birth_date: user.birth_date,
						});
					}
					return Promise.resolve();
				},
				function validatioApplicationPhase(ctx) {
					if (ctx.meta.isBackoffice && ctx.params.application_phase_id) {
						return ctx
							.call("accommodation.application-phases.get", { id: ctx.params.application_phase_id })
							.then((phase) => {
								ctx.params.academic_year = phase[0].academic_year;
								ctx.params.application_phase = phase[0].application_phase;
								ctx.params.application_phase_id = phase[0].id;
								if (phase[0].out_of_date_validation) {
									const days = moment().diff(moment(ctx.params.admission_date), "days");
									ctx.params.submitted_out_of_date = days > phase[0].out_of_date_validation_days;
								} else {
									ctx.params.submitted_out_of_date = phase[0].out_of_date_from
										? moment(phase[0].out_of_date_from).isAfter(new Date())
											? false
											: true
										: false;
								}
							});
					} else {
						return ctx.call("accommodation.application-phases.getOpenPhase").then((phase) => {
							if (phase.length > 0) {
								ctx.params.academic_year = phase[0].academic_year;
								ctx.params.application_phase = phase[0].application_phase;
								ctx.params.application_phase_id = phase[0].id;
								if (phase[0].out_of_date_validation) {
									const days = moment().diff(moment(ctx.params.admission_date), "days");
									ctx.params.submitted_out_of_date = days > phase[0].out_of_date_validation_days;
								} else {
									ctx.params.submitted_out_of_date = phase[0].out_of_date_from
										? moment(phase[0].out_of_date_from).isAfter(new Date())
											? false
											: true
										: false;
								}
							} else {
								throw new ValidationError(
									"No open application phase",
									"NO_OPEN_APPLICATION_PHASE",
									{},
								);
							}
						});
					}
				},
				async function checkIfuserAlreadyHaveApplication(ctx) {
					const configurations = await ctx.call("accommodation.configurations.list", {});
					return this.activeApplications(ctx, ctx.params.user_id, ctx.params.academic_year).then(
						(existApplication) => {
							if (existApplication > 0) {
								throw new ValidationError(
									"User already have a active application this academic year",
									"ACCOMMODATION_APPLICATION_ALREADY_EXIST",
									{},
								);
							}
						},
					);
				},
				"checkIfContactsAreEquals",
				"validateDocumentType",
				"validateIncomeStatementFiles",
				"getPatrimonyCategory",
				"notInternationalStudentValidations",
				"validateOtherIncomeSource",
				"sanatizeIbanSwift",
			],
			update: [
				async function sanatizeParams(ctx) {
					if (ctx.params.patrimony_category_id) {
						const patrimony_category = await ctx.call("accommodation.patrimony-categories.get", {
							id: ctx.params.patrimony_category_id,
						});
						const reference_value = patrimony_category[0].reference_value
							? patrimony_category[0].reference_value
							: 0;
						ctx.params.per_capita_income =
							(ctx.params.household_total_income + reference_value) /
							ctx.params.house_hould_elements.length;
					}
					ctx.params.updated_at = new Date();
					ctx.params.updated_end_date = ctx.params.end_date;
				},
				"validateExternalEntities",
				async function fillDataToUpdateAndCheckPermission(ctx) {
					const application = await ctx.call("accommodation.applications.get", {
						id: ctx.params.id,
					});
					if (!ctx.meta.isBackoffice && application[0].user_id != ctx.meta.user.id) {
						throw new ForbiddenError(
							"This application id doesnt belong to the user logged",
							"ACCOMMODATION_APPLICATION_FORBIDDEN",
						);
					}

					ctx.params = Object.assign(ctx.params, {
						user_id: application[0].user_id,
						academic_year: application[0].academic_year,
						submission_date: application[0].submission_date,
						submitted_out_of_date: application[0].submitted_out_of_date,
						application_phase: application[0].application_phase,
						application_phase_id: application[0].application_phase_id,
						status: application[0].status,
						billing_status: application[0].billing_status,
					});
				},
				"checkIfContactsAreEquals",
				"validateDocumentType",
				"validateIncomeStatementFiles",
				"getPatrimonyCategory",
				"notInternationalStudentValidations",
				"validateOtherIncomeSource",
				"sanatizeIbanSwift",
			],
			contract_changes_stats: [checkAllowedResidencesParams],
		},
		after: {
			create: [
				async function saveHistoryAndSendNotification(ctx, res) {
					let app = await ctx.call("accommodation.applications.change_status", {
						id: res[0].id,
						event: "SUBMIT",
						notes: "Candidatura submetida",
						application: {},
					});

					const configuration_enable = await ctx.call("accommodation.configurations.list");
					if (
						configuration_enable["CHANGE_STATUS_ON_CREATE"] == true &&
						configuration_enable["CHANGE_STATUS_ON_CREATE_DAYS"] == 0
					) {
						app = await ctx.call("accommodation.applications.change_status", {
							id: res[0].id,
							event: "ANALYSE",
							notes: "[Automatico] Candidatura enviada para análise",
							application: {},
						});
					}
					return app;
				},
				"saveExtras",
				"saveHouseElements",
				"saveIncomeStatmentFiles",
				"saveApplicationFiles",
			],
			get: [
				function checkIfIsUser(ctx, res) {
					if (!ctx.meta.isBackoffice && res[0].user_id != ctx.meta.user.id) {
						throw new ForbiddenError(
							"This application id doesnt belong to the user logged",
							"ACCOMMODATION_APPLICATION_FORBIDDEN",
						);
					}
					return res;
				},
			],
			update: [
				"saveExtras",
				"saveHouseElements",
				"saveIncomeStatmentFiles",
				"saveApplicationFiles",
			],
			patch: ["saveExtras", "saveHouseElements", "saveIncomeStatmentFiles", "saveApplicationFiles"],
			// backoffice_data: ["reprocessBilling"],
			change_status: [
				async function processApplication(ctx, res) {
					if (ctx.params.event == "CONTRACT") {
						await ctx.call("accommodation.billings.processAllApplicationMonths", {
							application_id: res[0].id,
						});
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		user_can_apply: {
			rest: "GET /can-apply",
			visibility: "published",
			params: {
				user_id: { type: "number", positive: true, convert: true, optional: true },
				application_phase_id: { type: "number", positive: true, convert: true, optional: true },
			},
			async handler(ctx) {
				let open_phase = null;
				if (ctx.meta.isBackoffice && ctx.params.application_phase_id) {
					open_phase = await ctx.call("accommodation.application-phases.get", {
						id: ctx.params.application_phase_id,
					});
				} else {
					open_phase = await ctx.call("accommodation.application-phases.getOpenPhase", {});
					if (open_phase.length == 0) {
						throw new ValidationError("No open application phase", "NO_OPEN_APPLICATION_PHASE", {});
					}
				}
				const user_id = ctx.meta.isBackoffice ? ctx.params.user_id : ctx.meta.user.id;
				if (!user_id) {
					throw new ValidationError(
						"user_id is required",
						"APPLICATIONS_CAN_APPLY_USER_ID_NOT_FOUND",
						{},
					);
				}

				return this.activeApplications(ctx, user_id, open_phase[0].academic_year).then(
					(applications) => {
						if (applications > 0)
							return {
								can: false,
								reason: "ALREADY_APPLIED",
							};
						else {
							return ctx
								.call("accommodation.configurations.list")
								.then((configuration_enable) => {
									// CHECK CONFIGURATION IF RENEW IS ACTIVE
									if (configuration_enable["ALLOW_APPLICATION_RENEW"] == true) {
										return true;
									}
									return false;
								})
								.then((can_renew) => {
									return this.adapter.db
										.raw(
											"SELECT a.id FROM application as a WHERE a.user_id = ? and status in ('closed', 'contracted') and a.academic_year != ? order by a.created_at desc limit 1",
											[user_id, open_phase[0].academic_year],
										)
										.then((result) => {
											return {
												can: true,
												reason: null,
												renew: can_renew ? result.rows.length > 0 : false,
												renew_application_id:
													can_renew && result.rows.length > 0 ? result.rows[0].id : null,
											};
										});
								});
						}
					},
				);
			},
		},
		// findApplicationsForBilling: {
		// 	visibility: "public",
		// 	params: {
		// 		residence_id: { type: "number", optional: true },
		// 		application_ids: { type: "array", item: { type: "number", positive: true, convert: true }, convert: true, optional: true, },
		// 		start_date: { type: "date", convert: true },
		// 		end_date: { type: "date", convert: true },
		// 	},
		// 	handler(ctx) {
		// 		return this._find(ctx, {
		// 			withRelated: ["extras", "regime", "user"],
		// 			query: (qb) => {
		// 				if (ctx.params.residence_id) {
		// 					qb.where("assigned_residence_id", "=", ctx.params.residence_id);
		// 				}
		// 				if (ctx.params.application_ids) {
		// 					qb.whereIn("id", ctx.params.application_ids);
		// 				}
		// 				qb.whereRaw(`billing_status != 'proccessing' AND status = 'contracted' AND (
		// 		  (start_date BETWEEN '${moment(ctx.params.start_date).toISOString()}' and '${moment(
		// 					ctx.params.end_date,
		// 				).toISOString()}') or
		// 		  (end_date BETWEEN '${moment(ctx.params.start_date).toISOString()}' AND '${moment(
		// 					ctx.params.end_date,
		// 				).toISOString()}') or
		// 		  (start_date <= '${moment(ctx.params.start_date).toISOString()}' AND end_date >= '${moment(
		// 					ctx.params.end_date,
		// 				).toISOString()}'))`);
		// 			},
		// 		});
		// 	},
		// },
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#isBackoffice", "#user.id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		backoffice_data: {
			rest: "PATCH /:id/backoffice-data",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
				// has_scholarship: { type: "boolean" },
				signed_contract_file_id: { type: "number", positive: true, convert: true, optional: true },
				liability_term_file_id: { type: "number", positive: true, convert: true, optional: true },
				per_capita_income: { type: "number", positive: true, convert: true, optional: true },
				// reprocess_since: { type: "date", convert: true },
			},
			async handler(ctx) {
				return ctx.call("accommodation.applications.patch", ctx.params);
			},
		},
		get_status: {
			rest: "GET /:id/status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);

				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);

				return stateMachine.transitions();
				/*{
	"current_status": "analysed",
	"available_events": [
		"CANCEL",
		"PENDING"
	],
	"available_status": [
		"cancelled",
		"pending"
	]
	}*/
			},
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "string" },
				notes: { type: "string", optional: true },
				application: { type: "object", optional: true },
			},
			visibility: "published",
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);

				if (["ENQUEUE", "ASSIGN", "UNASSIGN"].includes(ctx.params.event)) {
					throw new ValidationError(
						"The events ['ENQUEUE', 'ASSIGN', 'UNASSIGN'] cant be executed in this endpoint",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS_WRONG",
						{},
					);
				}

				if (ctx.params.event == "PENDING" && ctx.params.application.room_id) {
					const room = await ctx.call("accommodation.rooms.get", {
						id: ctx.params.application.room_id,
						academic_year: application[0].academic_year,
						withRelated: "occupants,typology",
					});
					if (room[0].occupants.length >= room[0].typology.max_occupants_number) {
						throw new ValidationError(
							"Room is already full",
							"ACCOMMODATION_APPLICATION_ROOM_ALREADY_FULL",
							{},
						);
					}
				}

				if (ctx.params.event == "CLOSE") {
					await this.validatePendingProcessments(ctx, application[0]);
				}
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then(() => {
						return ctx
							.call("accommodation.applications.get", {
								id: ctx.params.id,
							})
							.then((result) => {
								if (["CLOSE", "CANCEL"].includes(ctx.params.event)) {
									this.cancelPendingApplicationDependencies(ctx, ctx.params.id);
								}
								return result;
							});
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		rollback_to_previous_state: {
			rest: "POST /:id/status/rollback",
			visibility: "published",
			scope: "accommodation:applications:rollback",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: [
						"ROLLBACK_TO_SUBMITTED",
						"ROLLBACK_TO_ANALYSED",
						"ROLLBACK_TO_PENDING",
						"ROLLBACK_TO_ASSIGNED",
					],
				},
				notes: { type: "string", convert: true, optional: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", { id: ctx.params.id });
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);

				if (["closed", "cancelled"].includes(application[0].status)) {
					const configurations = await ctx.call("accommodation.configurations.list", {});
					const count = await this._count(ctx, {
						query: (qb) => {
							qb.where("user_id", "=", application[0].user_id);
							qb.where("status", "not in", ["cancelled", "closed", "withdrawal", "unassigned"]);
							qb.orWhere("user_id", "=", application[0].user_id);
							qb.where("status", "=", "unassigned");
							qb.whereNotNull("unassigned_date");
							qb.where(
								"unassigned_date",
								">=",
								moment(new Date())
									.subtract(configurations.UNASSIGNED_OPPOSITION_LIMIT, "days")
									.format("YYYY-MM-DD"),
							);
							return qb;
						},
					});
					if (count > 0) {
						throw new ValidationError(
							"User already have a active application",
							"ACCOMMODATION_APPLICATION_ALREADY_EXIST",
							{},
						);
					}
				}
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[_.camelCase(ctx.params.event)]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_approve_status: {
			rest: "POST /:id/admin/approve-status",
			visibility: "published",
			scope: "accommodation:applications:dispatch",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (application[0].status !== "pending") {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);

				if (application[0].decision == "UNASSIGN") {
					ctx.params.application = ctx.params.application ? ctx.params.application : {};
					ctx.params.application.unassigned_date = new Date();
				}

				if (application[0].decision) {
					return stateMachine[application[0].decision.toLowerCase()]();
				} else {
					throw new ValidationError(
						"NO DECISION DEFINED",
						"ACCOMMODATION_APPLICATION_NO_DECISION_DEFINED",
						{},
					);
				}
			},
		},
		admin_reject_status: {
			rest: "POST /:id/admin/reject-status",
			visibility: "published",
			scope: "accommodation:applications:dispatch",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (application[0].status !== "pending") {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}

				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);

				return stateMachine.analyse();
			},
		},
		student_approve_status: {
			rest: "POST /:id/student/approve-status",
			visibility: "published",
			scope: "accommodation:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (application[0].status !== "assigned" && application[0].user_id === ctx.meta.user.id) {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);
				return stateMachine.confirm();
			},
		},
		student_reject_status: {
			rest: "POST /:id/student/reject-status",
			visibility: "published",
			scope: "accommodation:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (application[0].status !== "assigned" || application[0].user_id !== ctx.meta.user.id) {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);
				return stateMachine.reject();
			},
		},
		student_oppose_status: {
			rest: "POST /:id/student/oppose-status",
			visibility: "published",
			scope: "accommodation:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (
					!["assigned", "unassigned"].includes(application[0].status) ||
					application[0].user_id !== ctx.meta.user.id
				) {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);
				ctx.params.application = ctx.params.application ? ctx.params.application : {};
				ctx.params.application.has_opposed = true;
				return stateMachine.oppose();
			},
		},
		student_cancel_status: {
			rest: "POST /:id/student/cancel",
			visibility: "published",
			scope: "accommodation:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", ctx.params);
				if (
					!["submitted", "analysed", "pending", "queued", "confirmed"].includes(
						application[0].status,
					) ||
					application[0].user_id !== ctx.meta.user.id
				) {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
				const stateMachine = ApplicationStateMachine.createStateMachine(application[0].status, ctx);
				return stateMachine.cancel();
			},
		},
		application_info: {
			rest: "GET /applicant-info",
			visibility: "published",
			async handler(ctx) {
				let course_id = null;
				let course_degree_id = null;
				if (ctx.meta.user.course_id) {
					const course = await ctx.call("configuration.courses.get", {
						id: ctx.meta.user.course_id,
					});
					course_id = course[0].id;
					course_degree_id = course[0].course_degree_id;
				}

				return {
					full_name: ctx.meta.user.name,
					applicant_birth_date: ctx.meta.user.birth_date || new Date(),
					document_type_id: ctx.meta.user.document_type_id || null,
					identification: ctx.meta.user.identification || "",
					tin: ctx.meta.user.tin || "",
					email: ctx.meta.user.email || "",
					phone_1: ctx.meta.user.phone || "",
					student_number: ctx.meta.user.student_number || "",
					nationality: ctx.meta.user.nationality || null,
					address: ctx.meta.user.address || "",
					city: ctx.meta.user.city || "",
					country: ctx.meta.user.country || "",
					postal_code: ctx.meta.user.postal_code || "",
					course_id,
					course_degree_id,
					course_year: ctx.meta.user.course_year || 0,
					organic_unit_id: ctx.meta.user.organic_unit_id,
					admission_date: ctx.meta.user.admission_date,
					gender: ctx.meta.user.gender,
				};
			},
		},
		stats: {
			rest: "GET /stats",
			visibility: "published",
			scope: "accommodation:applications:stats",
			params: {
				residence_id: { type: "number", convert: true, optional: true },
				academic_year: { type: "string", optional: true },
				application_phase_id: { type: "number", convert: true, optional: true },
			},
			handler(ctx) {
				let q1 = this.adapter
					.db(this.adapter.table)
					.select("status")
					.count("*")
					.whereIn("status", ["draft", "submitted", "cancelled", "analysed"])
					.groupBy("status");

				let q2 = this.adapter
					.db(this.adapter.table)
					.select("status")
					.count("*")
					.whereIn("status", [
						"pending",
						"assigned",
						"unassigned",
						"rejected",
						"queued",
						"confirmed",
						"opposition",
						"contracted",
						"closed",
						"withdrawal",
					])
					.groupBy("status");

				if (ctx.params.residence_id) {
					q1.andWhere("residence_id", ctx.params.residence_id);
					q2.andWhere("assigned_residence_id", ctx.params.residence_id);
				}

				if (ctx.params.academic_year) {
					q1.andWhere("academic_year", ctx.params.academic_year);
					q2.andWhere("academic_year", ctx.params.academic_year);
				}

				if (ctx.params.application_phase_id) {
					q1.andWhere("application_phase_id", ctx.params.application_phase_id);
					q2.andWhere("application_phase_id", ctx.params.application_phase_id);
				}

				return Promise.all([q1, q2]).then((result) => {
					let allStatus = {
						draft: 0,
						submitted: 0,
						analysed: 0,
						pending: 0,
						cancelled: 0,
						assigned: 0,
						unassigned: 0,
						rejected: 0,
						queued: 0,
						reopened: 0,
						confirmed: 0,
						opposition: 0,
						contracted: 0,
						closed: 0,
						withdrawal: 0,
					};
					result[0].map((r) => (allStatus[r.status] = r.count));
					result[1].map((r) => (allStatus[r.status] = r.count));
					return allStatus;
				});
			},
		},
		contract_changes_stats: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
			rest: "GET /contract-changes-stats",
			visibility: "published",
			scope: "accommodation:applications:stats",
			params: {
				residence_id: { type: "number", convert: true, optional: true },
				academic_year: { type: "string", optional: true },
				application_phase_id: { type: "number", convert: true, optional: true },
			},
			async handler(ctx) {
				let q1 = this.adapter.db(this.adapter.table).select("id");

				if (ctx.params.residence_id) {
					//q1.andWhere("residence_id", ctx.params.residence_id);
					q1.whereIn(
						"assigned_residence_id",
						Array.isArray(ctx.params.residence_id)
							? ctx.params.residence_id
							: [ctx.params.residence_id],
					);
				}

				if (_.has(ctx.params, "academic_year")) {
					q1.andWhere("academic_year", ctx.params.academic_year);
				}

				if (_.has(ctx.params, "application_phase_id")) {
					q1.andWhere("application_phase_id", ctx.params.application_phase_id);
				}

				return Promise.all([q1]).then(async (result) => {
					let found_application_ids = result[0].map((x) => x.id);

					let query_application_id;
					if (!found_application_ids.length) {
						query_application_id = { application_id: 0 };
					} else {
						query_application_id = { application_id: found_application_ids };
					}

					return ctx
						.call("authorization.scopes.userHasScope", { scope: "sasocial:is_sas_admnistrator" })
						.then(async (user) => {
							let allStatus = {};
							for (const status of STATS) {
								if (user) {
									const params = status.query.type
										? Object.assign(
												{ status: { in: ["DISPATCH"] }, type: status.query.type },
												query_application_id,
										  )
										: Object.assign({ status: { in: ["DISPATCH"] } }, query_application_id);
									const count = await ctx.call(status.action, {
										query: params,
									});
									allStatus[status.name] = {
										value: count,
										color: "orange",
									};
								} else {
									const params = status.query.type
										? Object.assign(
												{ status: { in: status.query.status }, type: status.query.type },
												query_application_id,
										  )
										: Object.assign({ status: { in: status.query.status } }, query_application_id);
									const count = await ctx.call(status.action, {
										query: params,
									});
									allStatus[status.name] = {
										value: count,
										color: "red",
									};
								}
							}
							return allStatus;
						});
				});
			},
		},
		premute_room: {
			rest: "POST /:id/premute-room",
			visibility: "published",
			scope: "accommodation:applications:update",
			params: {
				id: { type: "number", integer: true, convert: true },
				premute_with_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				// STEP 1:  check if the application is in contract
				const application = await this._get(ctx, { id: ctx.params.id });
				if (application[0].status != "contracted") {
					throw new ValidationError(
						"The application is not in contracted status.",
						"ACCOMMODATION_APPLICATION_NOT_IN_CONTRACT",
						{},
					);
				}
				// STEP 2:  check if the application to premute is in contract
				const premute_application = await this._get(ctx, { id: ctx.params.premute_with_id });
				if (premute_application[0].status != "contracted") {
					throw new ValidationError(
						"The premute application is not in contracted status.",
						"ACCOMMODATION_PREMUTE_APPLICATION_NOT_IN_CONTRACT",
						{},
					);
				}
				// STEP 3:  check if both applications is in the same residence
				if (application[0].assigned_residence_id != premute_application[0].assigned_residence_id) {
					throw new ValidationError(
						"The applications are not assigned to the same residence.",
						"ACCOMMODATION_APPLICATION_DIFERENT_RESIDENCE",
						{},
					);
				}
				// STEP 4: check if isn't in the same room
				if (application[0].room_id == premute_application[0].room_id) {
					throw new ValidationError(
						"The applications are not assigned to the same residence.",
						"ACCOMMODATION_APPLICATION_SAME_ROOM",
						{},
					);
				}
				await ctx.call("accommodation.applications.patch", {
					id: premute_application[0].id,
					room_id: application[0].room_id,
				});
				return await ctx.call("accommodation.applications.patch", {
					id: application[0].id,
					room_id: premute_application[0].room_id,
				});
			},
		},
		automatic_change_status: {
			visibility: "public",
			async handler(ctx) {
				const configuration_enable = await ctx.call("accommodation.configurations.list");
				if (
					configuration_enable["ASSIGNED_STATUS_AUTO_CHANGE"] == true &&
					configuration_enable["STATUS_AUTO_CHANGE_DAYS"] &&
					configuration_enable["EVENT_FOR_AUTO_CHANGE"]
				) {
					// Assigned applications more than 5 days
					const applications = await this.adapter.db.raw(
						`select a.id, a.status, a.user_id FROM application_history ah
							JOIN application a ON a.id=ah.application_id
							WHERE a.status = 'assigned' AND ah.status ='assigned'
							and DATE_PART('day', AGE(current_timestamp,ah.created_at)) >=  ?
							and ah.id = (select max(ah1.id) FROM application_history ah1 where ah1.application_id = a.id);`,
						[configuration_enable["STATUS_AUTO_CHANGE_DAYS"]],
					);
					for (const app of applications.rows) {
						ctx.params = {};
						ctx.params.id = app.id;
						try {
							const stateMachine = ApplicationStateMachine.createStateMachine(app.status, ctx);
							if (configuration_enable["EVENT_FOR_AUTO_CHANGE"] == "CONFIRM")
								ctx.params.notes =
									"[Automático] Candidatura confirmada automáticamente após " +
									configuration_enable["STATUS_AUTO_CHANGE_DAYS"] +
									" dias";
							else if (configuration_enable["EVENT_FOR_AUTO_CHANGE"] == "REJECT")
								ctx.params.notes =
									"[Automático] Candidatura rejeitada automáticamente após " +
									configuration_enable["STATUS_AUTO_CHANGE_DAYS"] +
									" dias";
							await stateMachine[configuration_enable["EVENT_FOR_AUTO_CHANGE"].toLowerCase()]();
						} catch (err) {
							this.logger.info(err);
						}
					}
				}

				if (configuration_enable["CHANGE_STATUS_ON_CREATE"] == true) {
					const applications = await this.adapter.find({
						query: (qb) => {
							qb.where("status", "=", "submitted");
							qb.whereRaw("DATE_PART('day', AGE(current_timestamp,created_at))>=?", [
								configuration_enable["CHANGE_STATUS_ON_CREATE_DAYS"],
							]);
						},
					});
					for (const app of applications) {
						ctx.params = {};
						ctx.params.id = app.id;
						try {
							const stateMachine = ApplicationStateMachine.createStateMachine(app.status, ctx);
							ctx.params.notes =
								"[Automático] Candidatura em análise automáticamente após " +
								configuration_enable["CHANGE_STATUS_ON_CREATE_DAYS"] +
								" dias";
							await stateMachine["analyse"]();
						} catch (err) {
							this.logger.info(err);
						}
					}
				}
			},
		},
		applyRoomChangesOnApplication: {
			visibility: "public",
			async handler(ctx) {
				// Returns most recent room change request who have a room_id!= application.room_id
				const room_change_requests = await this.adapter.raw(
					`select arc.start_date, arc.application_id, arc.room_id, arc.typology_id, arc.residence_id from application_room_change arc
					join application a on a.id=arc.application_id
					where arc.id = (
						select id from application_room_change
						where application_room_change.application_id=arc.application_id
						order by application_room_change.created_at desc
						limit 1) and
					arc.start_date <= now() and
					a.room_id!=arc.room_id
				`,
				);
				for (const rcr of room_change_requests.rows) {
					const changes = {};
					changes.id = rcr.application_id;
					changes.room_id = rcr.room_id;
					if (rcr.residence_id) changes.assigned_residence_id = rcr.residence_id;
					if (rcr.typology_id) changes.typology_id = rcr.typology_id;
					await ctx.call("accommodation.applications.patch", changes);
				}
			},
		},
		getAllApplicationChanges: {
			rest: "GET /:id/changes",
			visibility: "published",
			scope: "accommodation:applications:read",
			cache: false,
			params: {
				id: { type: "number", convert: true },
			},
			async handler(ctx) {
				const withdrawals = await ctx.call("accommodation.withdrawals.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history",
				});
				const absences = await ctx.call("accommodation.absences.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history",
				});
				const extensions = await ctx.call("accommodation.extensions.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history",
				});
				const extras = await ctx.call("accommodation.application-extra-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history,extras",
				});
				const regime = await ctx.call("accommodation.application-regime-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history,regime",
				});
				const room_change = await ctx.call("accommodation.application-room-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history,typology,room,residence",
				});
				const maintenance = await ctx.call("accommodation.maintenance-requests.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history,file",
				});
				const tariff = await ctx.call("accommodation.application-tariff-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history,tariff,old_tariff,file",
				});
				const communications = await ctx.call("accommodation.application-communications.find", {
					query: { application_id: ctx.params.id },
				});
				const iban_changes = await ctx.call("accommodation.application-iban-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "file",
				});

				const periods_changes = await ctx.call("accommodation.application-period-changes.find", {
					query: { application_id: ctx.params.id },
					withRelated: "history",
				});

				const joined = [].concat(
					withdrawals.map((obj) => ({ ...obj, type: "WITHDRAWAL" })),
					absences.map((obj) => ({ ...obj, type: "ABSENCE" })),
					extensions.map((obj) => ({ ...obj, type: "EXTENSION" })),
					extras.map((obj) => ({ ...obj, type: "EXTRA" })),
					regime.map((obj) => ({ ...obj, type: "REGIME" })),
					room_change,
					maintenance.map((obj) => ({ ...obj, type: "MAINTENANCE" })),
					tariff.map((obj) => ({ ...obj, type: "TARIFF" })),
					communications.map((obj) => ({ ...obj, type: obj.type + "_COMMUNICATION" })),
					iban_changes.map((obj) => ({ ...obj, type: "IBAN_CHANGE" })),
					periods_changes.map((obj) => ({ ...obj, type: "PERIODS" })),
				);
				return _.sortBy(joined, [
					function (o) {
						return new Date().getTime() - new Date(o.updated_at);
					},
				]);
			},
		},
		getApplicationsListResume: {
			rest: "GET /resume-list",
			visibility: "published",
			scope: "accommodation:applications:resume-list",
			cache: false,
			params: {},
			handler(ctx) {
				const params = ctx.params;
				ctx.params.withRelated =
					"preferred_typology,residence,regime,regime.translations,room,course,extras,assignedResidence,whichRoom,whichResidence,document_type,occurrences,tariff,user";
				return ctx.call("accommodation.applications.list", params);
			},
		},

		get_users_by_contracted_app_and_residence: {
			//rest: "GET /get-users-by-contracted-app-and-residence",
			//visibility: "published",
			visibility: "public",
			cache: { keys: ["residence_ids"] },
			params: {
				residence_ids: { type: "string", max: 255, optional: true },
			},
			async handler(ctx) {
				const result = await this.adapter.find({
					query: (q) => {
						q.distinct("user_id as id");
						q.where("status ", "=", "contracted");

						if (ctx.params.residence_ids !== null)
							q.whereRaw(' "residence_id" IN (' + ctx.params.residence_ids + ") ");

						return q;
					},
					withRelated: false,
				});

				return result;
			},
		},
		get_residences_by_contracted_app: {
			rest: "GET /get-residences-by-contracted-app",
			visibility: "published",
			params: {},
			async handler(ctx) {
				const result = await this.adapter.find({
					query: (q) => {
						q.distinct("residence_id");
						q.where("status ", "=", "contracted");
						return q;
					},
				});

				return result;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"accommodation.extras.*"() {
			this.clearCache();
		},
		"accommodation.regimes.*"() {
			this.clearCache();
		},
		"accommodation.typologies.*"() {
			this.clearCache();
		},
		"accommodation.residences.*"() {
			this.clearCache();
		},
		"accommodation.rooms.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		validateExternalEntities(ctx) {
			return this.validateExternalIds(ctx, {
				course_id: { action: "configuration.courses.get" },
				room_id: { action: "accommodation.rooms.get" },
				application_phase_id: {
					action: "accommodation.application-phases.get",
				},
				residence_id: { action: "accommodation.residences.get" },
				assigned_residence_id: { action: "accommodation.residences.get" },
				which_residence_id: { action: "accommodation.residences.get" },
				second_option_residence_id: {
					action: "accommodation.residences.get",
				},
				which_room_id: { action: "accommodation.rooms.get" },
				regime_id: { action: "accommodation.regimes.get" },
				income_statement_file_id: { action: "media.files.get" },
				payment_method_id: { action: "payments.methods.get" },
				user_id: { action: "authorization.users.get" },
				organic_unit_id: { action: "infrastructure.organic-units.get" },
				preferred_typology_id: { action: "accommodation.typologies.get" },
			});
		},
		async validateExternalIds(ctx, opts) {
			const opts_fields = _.keys(opts);

			let promises = [];
			_.forEach(opts_fields, (field) => {
				this.logger.info(opts[field].action, {
					id: ctx.params[field],
				});
				if (ctx.params[field]) {
					promises.push(
						ctx.call(opts[field].action, {
							id: ctx.params[field],
						}),
					);
				}
			});

			return await Promise.all(promises)
				.then((res) => {
					this.logger.info("RES");
					this.logger.info(res);
					return res;
				})
				.catch((err) => {
					this.logger.info("ERR");
					this.logger.info(err);
					throw err;
				});
		},
		async saveExtras(ctx, response) {
			if (ctx.params.extra_ids && Array.isArray(ctx.params.extra_ids)) {
				const extra_ids = [...new Set(ctx.params.extra_ids)];
				await ctx.call("accommodation.application_extras.save_extras_of_application", {
					application_id: response[0].id,
					extra_ids,
				});
				response[0].extras = await ctx.call("accommodation.application_extras.find", {
					query: {
						application_id: response[0].id,
					},
				});
			}
			return response;
		},
		async saveHouseElements(ctx, response) {
			if (ctx.params.house_hould_elements && ctx.params.house_hould_elements.length !== 0) {
				const elements_list = await ctx.call("accommodation.house-hould-elements.save_elements", {
					application_id: response[0].id,
					elements: ctx.params.house_hould_elements,
				});
				response[0].house_hould_elements = elements_list;
			}
			return response;
		},
		async saveApplicationFiles(ctx, response) {
			if (Array.isArray(ctx.params.file_ids)) {
				const file_ids = [...new Set(ctx.params.file_ids)];
				await ctx.call("accommodation.application_files.save_application_files", {
					application_id: response[0].id,
					file_ids,
				});
				response[0].files = await ctx.call("accommodation.application_files.find", {
					query: {
						application_id: response[0].id,
					},
				});
			}
			return response;
		},
		cancelPendingApplicationDependencies(ctx, application_id) {
			ctx
				.call("accommodation.extensions.find", {
					query: { application_id: application_id, status: ["WAITING_APPROVAL"] },
				})
				.then((extensions) => {
					extensions.forEach((extension) => {
						extension.status = "CANCELLED";
						ctx.call("accommodation.extensions.patch", {
							id: extension.id,
							status: "CANCELLED",
						});
					});
				});
			ctx
				.call("accommodation.withdrawals.find", {
					query: { application_id: application_id, status: ["WAITING_APPROVAL"] },
				})
				.then((withdrawals) => {
					withdrawals.forEach((extension) => {
						extension.status = "CANCELLED";
						ctx.call("accommodation.withdrawals.patch", {
							id: extension.id,
							status: "CANCELLED",
						});
					});
				});
			ctx
				.call("accommodation.absences.find", {
					query: { application_id: application_id, status: ["SUBMITTED", "ANALYSIS", "DISPATCH"] },
				})
				.then((absences) => {
					absences.forEach((absence) => {
						absence.status = "CANCELLED";
						ctx.call("accommodation.absences.patch", {
							id: absence.id,
							status: "CANCELLED",
						});
					});
				});
			ctx
				.call("accommodation.application-extra-changes.find", {
					query: { application_id: application_id, status: ["SUBMITTED", "ANALYSIS", "DISPATCH"] },
				})
				.then((extra_chg) => {
					extra_chg.forEach((chg) => {
						chg.status = "CANCELLED";
						ctx.call("accommodation.application-extra-changes.patch", {
							id: chg.id,
							status: "CANCELLED",
						});
					});
				});
			ctx
				.call("accommodation.application-regime-changes.find", {
					query: { application_id: application_id, status: ["SUBMITTED", "ANALYSIS", "DISPATCH"] },
				})
				.then((regime_chg) => {
					regime_chg.forEach((chg) => {
						chg.status = "CANCELLED";
						ctx.call("accommodation.application-regime-changes.patch", {
							id: chg.id,
							status: "CANCELLED",
						});
					});
				});
			ctx
				.call("accommodation.application-period-changes.find", {
					query: { application_id: application_id, status: ["SUBMITTED", "ANALYSIS", "DISPATCH"] },
				})
				.then((extra_chg) => {
					extra_chg.forEach((chg) => {
						chg.status = "CANCELLED";
						ctx.call("accommodation.application-period-changes.patch", {
							id: chg.id,
							status: "CANCELLED",
						});
					});
				});
		},
		checkIfContactsAreEquals(ctx) {
			if (ctx.params.phone_1 == ctx.params.phone_2) {
				throw new ValidationError(
					"Contact and emergency contact cannot be equals",
					"ACCOMMODATION_APPLICATION_EQUAL_CONTACTS",
					{},
				);
			}
		},
		async validateDocumentType(ctx) {
			if (ctx.params.document_type_id) {
				await ctx.call("authorization.document-types.get", { id: ctx.params.document_type_id });
			}
		},
		async saveIncomeStatmentFiles(ctx, response) {
			if (
				ctx.params.income_statement_files_ids &&
				Array.isArray(ctx.params.income_statement_files_ids)
			) {
				const file_ids = [...new Set(ctx.params.income_statement_files_ids)];
				response[0].income_statement_files = await ctx.call(
					"accommodation.application-income-statement-files.save_statment_files",
					{
						application_id: response[0].id,
						file_ids,
					},
				);
			}
			return response;
		},
		async validateIncomeStatementFiles(ctx) {
			if (ctx.params.income_statement_delivered == true) {
				if (this.checkIfExistsAndHaveElements(ctx.params.income_statement_files_ids)) {
					throw new ValidationError(
						"Its required at least one income_statement_file",
						"ACCOMMODATION_APPLICATION_STATEMENT_FILE_NOT_FOUND",
						{},
					);
				} else {
					for (const id of ctx.params.income_statement_files_ids) {
						await ctx.call("media.files.get", { id });
					}
				}
			}
		},
		checkIfExistsAndHaveElements(arr) {
			return !arr || !Array.isArray(arr) || arr.length == 0;
		},
		async getPatrimonyCategory(ctx) {
			if (ctx.params.patrimony_category_id) {
				const patrimony_category = await ctx.call("accommodation.patrimony-categories.get", {
					id: ctx.params.patrimony_category_id,
				});
				ctx.params.patrimony_category = patrimony_category[0].description;
			}
		},
		async notInternationalStudentValidations(ctx) {
			if (ctx.params.international_student == false) {
				const v = new Validator();
				const schema = {
					house_hould_elements: {
						type: "array",
						empty: true,
						items: {
							type: "object",
							props: {
								kinship: { type: "string" },
								profession: { type: "string" },
							},
						},
					},
					household_total_income: { type: "number" },
					househould_elements_in_university: { type: "number" },
					patrimony_category_id: { type: "number" },
					income_source: {
						type: "array",
						items: {
							type: "enum",
							values: [
								"DEPENDENT_WORKER",
								"INDEPENDENT_WORK",
								"PENSIONS",
								"UNEMPLOYMENT_ALLOWANCE",
								"RSI",
								"OTHER",
							],
						},
						min: 1,
					},
					other_income_source: { type: "string", optional: true },
				};
				const check = v.compile(schema);
				const res = check(ctx.params);
				if (res !== true)
					return Promise.reject(new ValidationError("Entity validation error!", null, res));
			}
		},
		async validateOtherIncomeSource(ctx) {
			if (
				Array.isArray(ctx.params.income_source) &&
				ctx.params.income_source.includes("OTHER") &&
				!ctx.params.other_income_source
			) {
				throw new ValidationError(
					"Its required provide other income source description",
					"ACCOMMODATION_APPLICATION_OTHER_INCOME_SOURCE_NOT_FOUND",
					{},
				);
			}
		},
		async reprocessBilling(ctx, res) {
			const month = moment(ctx.params.reprocess_since).month() + 1;
			const year = moment(ctx.params.reprocess_since).year();
			const billings = await ctx.call("accommodation.billings.find", {
				query: { year: { gte: year }, application_id: res[0].id },
			});
			for (const bill of billings) {
				if (
					(bill.status == "PROCESSED" && bill.month >= month && bill.year == year) ||
					(bill.status == "PROCESSED" && bill.month <= month && bill.year > year)
				) {
					ctx.call("accommodation.billings.reprocessApplicationBilling", { id: bill.id });
				}
			}
			return res;
		},
		async validatePendingProcessments(ctx, application) {
			const billings = await ctx.call("accommodation.billings.find", {
				query: { status: "PROCESSED", application_id: application.id },
			});
			if (billings.length > 0) {
				throw new ValidationError(
					"Cannot close one application with unprocessed billings",
					"ACCOMMODATION_APPLICATION_UNPROCESSED_BILLINGS",
					{},
				);
			}
		},
		sanatizeIbanSwift(ctx) {
			if (ctx.params.allow_direct_debit && ctx.params.iban) {
				ctx.params.iban = ctx.params.iban.replace(/\s/g, "").toUpperCase();

				let oBicSwift = swifts.find((o) => o.cod_bdp === ctx.params.iban.substr(4, 4));

				if (oBicSwift) {
					ctx.params.swift = oBicSwift.bic_swift;
				}

				ctx.params.sepa_id_debit_auth = "00" + ctx.params.tin;
				ctx.params.sepa_signtr_date = ctx.params.updated_at;
			}
		},
		async activeApplications(ctx, user_id, academic_year) {
			const configurations = await ctx.call("accommodation.configurations.list", {});

			return this._count(ctx, {
				query: (qb) => {
					qb.where("user_id", "=", user_id);
					qb.where("status", "not in", [
						"cancelled",
						"closed",
						"withdrawal",
						"unassigned",
						"rejected",
					]);
					qb.where("academic_year", "=", academic_year);
					qb.orWhere((qb1) => {
						qb1.where("user_id", "=", user_id);
						qb1.where("academic_year", "=", academic_year);
						qb1.where("status", "=", "unassigned");
						qb1.whereNotNull("unassigned_date");
						qb1.where(
							"unassigned_date",
							">=",
							moment(new Date())
								.subtract(configurations.UNASSIGNED_OPPOSITION_LIMIT, "days")
								.format("YYYY-MM-DD"),
						);
					});
					return qb;
				},
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		// Change default PostgreSQL array return format. Applied on custom income_source data type.
		// Before: { DEPENDENT_WORKER,INDEPENDENT_WORK" }
		// After: [	"DEPENDENT_WORKER","INDEPENDENT_WORK" ]
		this.adapter
			.raw("SELECT oid::regtype, oid FROM pg_type  where oid::regtype::varchar = 'income_source[]'")
			.then((data) => {
				pg.types.setTypeParser(data.rows[0].oid, (value) => {
					return value.match(/[\w.-]+/g).map(String);
				});
			});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
