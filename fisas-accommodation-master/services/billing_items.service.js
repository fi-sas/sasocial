"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.billing_items",
	table: "billing_item",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "billings")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"product_code",
			"name",
			"description",
			"room_id",
			"typology_id",
			"regime_id",
			"extra_id",
			"quantity",
			"unit_price",
			"price",
			"vat_id",
			"vat_value",
			"period",
			"possible_retroactive",
			"billing_id",
			"discount",
			"discount_value",
			"processed_manually",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["room", "typology", "extra"],
		withRelateds: {
			room(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.rooms", "room", "room_id");
			},
			typology(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.typologies", "typology", "typology_id");
			},
			extra(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.extras", "extra", "extra_id");
			},

		},
		entityValidator: {
			product_code: { type: "string" },
			name: { type: "string" },
			description: { type: "string", empty: true },
			room_id: { type: "number", convert: true, optional: true },
			typology_id: { type: "number", convert: true, optional: true },
			regime_id: { type: "number", convert: true, optional: true },
			extra_id: { type: "number", convert: true, optional: true },
			quantity: { type: "number", convert: true },
			unit_price: { type: "number", convert: true },
			price: { type: "number", convert: true },
			vat_id: { type: "number", convert: true },
			vat_value: { type: "number", convert: true },
			period: { type: "enum", values: ["DAY", "WEEK", "MONTH"] },
			possible_retroactive: { type: "number", convert: true, optional: true },
			billing_id: { type: "number", convert: true },
			discount: { type: "number", convert: true, optional: true, default: 0 },
			discount_value: { type: "number", convert: true, optional: true, default: 0 },
			processed_manually: { type: "boolean", optional: true, default: false },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				async function validateBillingStatus(ctx) {
					if (ctx.params.billing_id) {
						const billing = await ctx.call("accommodation.billings.get", { id: ctx.params.billing_id });
						if (billing[0].status != "PROCESSED" && billing[0].status != "CANCELLED" && billing[0].status != "REVIEWED")
							throw new ValidationError(
								"Only can add itens to unbilled processements",
								"ACCOMMODATION_BILLING_ITEMS_ALREADY_BILLED",
								{},
							);
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
