"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const moment = require("moment");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const billingStateMachine = require("./state-machines/billings.machine");
const QueueService = require("moleculer-bull");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const Stream = require("stream");

const { swifts } = require("./helpers/bic_swift.js");
const _ = require("lodash");
const { upperFirst } = require("lodash");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.billings",
	table: "billing",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "billings"), QueueService(process.env.REDIS_QUEUE)],
	/**
	 * QUEUES
	 */
	queues: {
		"billings.bill"(job) {
			this.logger.info("NEW JOB RECEIVED!");
			return this.currentAccountProcessBilling(job.data.billing_id)
				.then(() => {
					this.logger.info("========================");
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.info("========================");
					this.logger.info("ANTES DE MANDAR O REJECT");
					this.logger.info(err);
					return Promise.reject(err);
				});
		},
		"billings.process"(job) {
			this.logger.info("NEW JOB RECEIVED!");
			return this.processApplication(job.data.application)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.error("ANTES DE MANDAR O REJECT");
					this.logger.error(err);
					return Promise.reject(err);
				});
		},
	},
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"month",
			"year",
			"billing_start_date",
			"billing_end_date",
			"status",
			"possible_retroactive",
			"due_date",
			"paid_at",
			"movement_id",
			"updated_at",
			"created_at",
			"tariff_id",

			"allow_direct_debit",
			"sepa_id_debit_auth",
			"sepa_signtr_date",
			"sepa_first_recur",
			"sepa_created_at",
			"sepa_iban",
		],
		defaultWithRelateds: ["billing_items", "application", "tariff"],
		withRelateds: {
			billing_items(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.billing_items", //serviceName
					"billing_items", //newField
					"id", //identificatorField
					"billing_id", //searchField
					{},
					null, //fields to return or null for all
					false, //withRelated
				).then(async (res) => {
					const extras = await ctx.call("accommodation.extras.list", { withRelated: false });
					const rooms = await ctx.call("accommodation.rooms.list", { withRelated: false });
					const typologies = await ctx.call("accommodation.typologies.list", { withRelated: false });

					return docs.map((doc) => {
						return doc.billing_items.map((item) => {
							if (item.room_id) {
								item.room = rooms.rows.find(r => r.id === item.room_id);
							}
							if (item.typology_id) {
								item.typology = typologies.rows.find(t => t.id === item.typology_id);
							}
							if (item.extra_id) {
								item.extra = extras.rows.find(e => e.id === item.extra_id);
							}
						});
					});
				});



				/*return hasMany(
					docs,
					ctx,
					"accommodation.billing_items",
					"billing_items",
					"id",
					"billing_id",
				);*/
			},
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.applications", "application", "application_id", "id", {}, null, "assignedResidence, regime, extras, room");
			},

			tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "tariff", "tariff_id", "id");
			},
		},
		entityValidator: {
			application_id: { type: "number", convert: true },
			month: { type: "number", convert: true },
			year: { type: "number", convert: true },
			billing_start_date: { type: "date", convert: true },
			billing_end_date: { type: "date", convert: true },
			status: {
				type: "enum",
				values: [
					"PROCESSED",
					"REVIEWED",
					"BILLED",
					"PAID",
					"CANCELLED",
					"EXTERNAL_VALIDATED",
					"EXTERNAL_PAID",
				],
				default: "PROCESSED",
			},
			possible_retroactive: { type: "number", convert: true, optional: true },
			due_date: { type: "date", convert: true, optional: true },
			movement_id: { type: "uuid", version: 4, optional: true },
			paid_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			status: [
				async function validateRequiredParams(ctx) {
					if (
						!ctx.params.billing_ids &&
						(!ctx.params.residence_id || !ctx.params.month || !ctx.params.year)
					) {
						throw new ValidationError(
							"Required array of billing_ids or residence_id, month and year params",
							"ACCOMMODATION_BILLING_STATUS_REQUIRED_PARAMS",
							{},
						);
					}
				},
				async function validateBillingIds(ctx) {
					if (Array.isArray(ctx.params.billing_ids)) {
						ctx.params.billing_ids.forEach((id) => {
							ctx.call("accommodation.billings.get", { id });
						});
					}
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function checkQuery(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						let applications_ids = null;

						let _query = {
							user_id: ctx.meta.user.id,
						};

						if (ctx.params.query.application_id) {
							Object.assign(_query, { id: ctx.params.query.application_id });
						}
						applications_ids = await ctx.call("accommodation.applications.find", {
							query: _query,
							fields: "id",
							withRelated: false,
						});

						ctx.params.query.application_id = applications_ids.map((x) => x.id);
					}
				},
				async function searchInApplication(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.query.application) {
						applications_ids = await ctx.call("accommodation.applications.find", {
							query:
								ctx.params.query && ctx.params.query.application
									? ctx.params.query.application
									: {},
							fields: "id",
							withRelated: false,
							search: ctx.params.search,
							searchFields: ctx.params.searchFields,
						});
					} else if (ctx.params.search) {
						applications_ids = await ctx.call("accommodation.applications.find", {
							fields: "id",
							withRelated: false,
							search: ctx.params.search,
							searchFields: ctx.params.searchFields,
						});
					}
					if (applications_ids) ctx.params.query.application_id = applications_ids.map((x) => x.id);
				},
				function sortBy(ctx) {
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.sort && ctx.params.sort.includes("application."))
							qb.innerJoin("application", "billing.application_id", "application.id");
					};
				},
				async function removeFields(ctx) {
					delete ctx.params.query.application;
					delete ctx.params.search;
					delete ctx.params.searchFields;
				},
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		processAllApplicationMonths: {
			visibility: "public", //"published", //<--debug
			//rest: "GET /processAllApplicationMonths", //<--debug
			params: {
				application_id: { type: "number", convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", {
					id: ctx.params.application_id,
					withRelated: ["extras", "regime", "user"],
				});

				//Just for debug
				//return this.processApplicationCtx(ctx, application);

				this.createJob(
					"billings.process",
					{
						application: application,
					},
					{
						attempts: 1,
						removeOnComplete: true,
						removeOnFail: true,
					},
				);

				return [];
			},
		},
		status: {
			visibility: "published",
			params: {
				billing_ids: { type: "array", items: "number", optional: true },
				action: {
					type: "enum",
					values: ["REVIEW", "CANCEL", "EXTERNAL_VALIDATE", "EXTERNAL_PAID"],
				},
				residence_id: { type: "number", positive: true, convert: true, optional: true },
				month: { type: "number", positive: true, convert: true, optional: true },
				year: { type: "number", positive: true, convert: true, optional: true },
			},
			rest: "POST /status",
			async handler(ctx) {
				let billings = [];
				if (ctx.params.billing_ids) {
					billings = await ctx.call("accommodation.billings.find", {
						query: { id: ctx.params.billing_ids },
						withRelated: false,
					});
				} else {
					billings = await this._find(ctx, {
						query: (qb) => {
							qb.select(
								"billing.status",
								"billing.id",
								"billing.application_id",
								"billing.month",
								"billing.year",
							);
							qb.innerJoin("application", "billing.application_id", "application.id");
							qb.where("application.assigned_residence_id", ctx.params.residence_id);
							qb.andWhere("billing.month", ctx.params.month);
							qb.andWhere("billing.year", ctx.params.year);
						},
						withRelated: false,
					});
				}
				// TODO: Put cicle in one job
				for (const billing of billings) {
					const stateMachine = billingStateMachine.createStateMachine(billing.status, ctx);
					ctx.params.id = billing.id;
					if (ctx.params.action == "REVIEW" && stateMachine.can("REVIEW")) {
						await stateMachine.review();
						this.createJob(
							"billings.bill",
							{
								billing_id: billing.id,
							},
							{
								attempts: 1,
								removeOnComplete: true,
								removeOnFail: true,
							},
						);
						// await this.currentAccountProcessBilling(billing.id)
					} else if (
						ctx.params.action == "CANCEL" &&
						stateMachine.can("CANCEL") &&
						(billing.status == "PAID" || billing.status == "BILLED")
					) {
						// TODO: Arranjar novo estado
						await stateMachine.cancel();
						await ctx.call("current_account.movements.cancel", {
							movement_id: billing.movement_id,
						});
						await this.updateApplicationValues(billing.application_id);
					} else if (
						ctx.params.action == "EXTERNAL_VALIDATE" &&
						stateMachine.can("EXTERNAL_VALIDATE")
					) {
						await stateMachine.externalValidate();
						await this.updateApplicationValues(billing.application_id);
					} else if (ctx.params.action == "EXTERNAL_PAID" && stateMachine.can("EXTERNAL_PAID")) {
						await stateMachine.externalPaid();
						await this.updateApplicationValues(billing.application_id);
					} else if (stateMachine.can(ctx.params.action.toUpperCase())) {
						await stateMachine[ctx.params.action.toLowerCase()]();
						await this.updateApplicationValues(billing.application_id);
					}
				}
				return true;
			},
		},
		current_account_confirm: {
			visibility: "public",
			async handler(ctx) {
				if (Array.isArray(ctx.params.items) && ctx.params.items.length > 0) {
					const billing = await ctx.call("accommodation.billings.get", {
						id: ctx.params.items[0].extra_info.billing_id,
					});
					if (billing[0].status == "BILLED") {
						await ctx.call("accommodation.billings.patch", {
							id: billing[0].id,
							status: "PAID",
							paid_at: new Date(),
							updated_at: new Date(),
						});
						this.updateApplicationValues(billing[0].application_id);
					}
				}
			},
		},
		current_account_cancel: {
			visibility: "public",
			async handler(ctx) {
				if (Array.isArray(ctx.params.items) && ctx.params.items.length > 0) {
					const billing = await ctx.call("accommodation.billings.get", {
						id: ctx.params.items[0].extra_info.billing_id,
					});
					if (billing[0].status == "BILLED") {
						await ctx.call("accommodation.billings.patch", {
							id: billing[0].id,
							status: "CANCELLED",
							updated_at: new Date(),
						});
						this.updateApplicationValues(billing[0].application_id);
					}
				}
			},
		},
		reprocessApplicationBilling: {
			visibility: "published",
			params: {
				id: { type: "number", convert: true, positive: true },
			},
			rest: "POST /:id/reprocess",
			async handler(ctx) {
				const billing = await ctx.call("accommodation.billings.get", {
					id: ctx.params.id,
					cache: false,
				});
				if (billing[0].status != "PROCESSED" && billing[0].status != "CANCELLED" && billing[0].status != "REVIEWED") {
					throw new ValidationError(
						"Cant process a billed billing",
						"ACCOMMODATION_BILLING_ALREADY_BILLED",
						{},
					);
				}
				return this.reprocessApplicationBilling(billing[0]);
			},
		},
		reprocessApplicationsMonth: {
			visibility: "published",
			params: {
				billing_ids: { type: "array", items: "number", optional: true },
				action: { type: "enum", values: ["REVIEW", "CANCEL"] },
				residence_id: { type: "number", positive: true, convert: true, optional: true },
				month: { type: "number", positive: true, convert: true, optional: true },
				year: { type: "number", positive: true, convert: true, optional: true },
			},
			rest: "POST /month/reprocess",
			async handler(ctx) {
				let billings = [];
				if (ctx.params.billing_ids) {
					billings = await ctx.call("accommodation.billings.find", {
						query: { id: ctx.params.billing_ids },
					});
				} else {
					billings = await this._find(ctx, {
						query: (qb) => {
							qb.select(
								"billing.status",
								"billing.id",
								"billing.application_id",
								"billing.month",
								"billing.year",
							);
							qb.innerJoin("application", "billing.application_id", "application.id");
							qb.where("application.assigned_residence_id", ctx.params.residence_id);
							qb.andWhere("billing.month", ctx.params.month);
							qb.andWhere("billing.year", ctx.params.year);
						},
						withRelated: false,
					});
				}
				for (const billing of billings) {
					await this.reprocessApplicationBilling(billing);
				}
			},
		},
		/*
		- Reprocess month of the new application end_date
		- Cancel all processed next months
		*/
		processWithdrawal: {
			visibility: "public",
			params: {
				application_id: { type: "number", convert: true, positive: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const month = moment(ctx.params.end_date).month() + 1;
				const year = moment(ctx.params.end_date).year();
				const billings = await ctx.call("accommodation.billings.find", {
					query: { year: { gte: year }, application_id: ctx.params.application_id },
				});
				for (const bill of billings) {
					if (
						(bill.status == "PROCESSED" && bill.month >= month && bill.year == year) ||
						(bill.status == "PROCESSED" && bill.month <= month && bill.year > year)
					) {
						if (bill.month == month) {
							await ctx.call("accommodation.billings.reprocessApplicationBilling", { id: bill.id });
						} else {
							await ctx.call("accommodation.billings.patch", { id: bill.id, status: "CANCELLED" });
						}
					}
				}
				await this.updateApplicationValues(ctx.params.application_id);
			},
		},

		/*
		- Reprocess months of the new application dates
		- Cancel all processed months
		*/
		processBillingPeriodChanges: {
			visibility: "public",
			params: {
				application_id: { type: "number", convert: true, positive: true },
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {

				const billings = await ctx.call("accommodation.billings.find", {
					query: { application_id: ctx.params.application_id },
				});

				const lastBilling = billings.reduce(function (a, b) { return moment(a.billing_end_date).isAfter(moment(b.billing_end_date)) ? a : b; });

				await this.processBillingsByDates(ctx, ctx.params.start_date, ctx.params.end_date, lastBilling, billings);

				for (const bill of billings) {
					const billingStartDate = moment(bill.billing_start_date, "YYYY-MM-DD");
					const billingEndDate = moment(bill.billing_end_date, "YYYY-MM-DD");

					if (!billingStartDate.isBetween(moment(ctx.params.start_date, "YYYY-MM-DD"), moment(ctx.params.end_date, "YYYY-MM-DD"), null, "[]")
						&& !billingEndDate.isBetween(moment(ctx.params.start_date, "YYYY-MM-DD"), moment(ctx.params.end_date, "YYYY-MM-DD"), null, "[]")
						&& bill.status === "PROCESSED") {
						await ctx.call("accommodation.billings.patch", { id: bill.id, status: "CANCELLED" });
					}
				}
				await this.updateApplicationValues(ctx.params.application_id);

			}
		},

		/*
		- Reprocess last processed month (to process complete month)
		- Process next months until new application end_date
		*/
		processExtension: {
			visibility: "public",
			params: {
				application_id: { type: "number", convert: true, positive: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {

				const billings = await ctx.call("accommodation.billings.find", {
					query: { application_id: ctx.params.application_id },
				});
				const firstBilling = billings.reduce(function (a, b) { return moment(a.billing_start_date).isBefore(moment(b.billing_start_date)) ? a : b; });
				const lastBilling = billings.reduce(function (a, b) { return moment(a.billing_end_date).isAfter(moment(b.billing_end_date)) ? a : b; });

				await this.processBillingsByDates(ctx, firstBilling.billing_start_date, ctx.params.end_date, lastBilling, billings);

				for (const bill of billings) {
					const billingStartDate = moment(bill.billing_start_date, "YYYY-MM-DD");
					const billingEndDate = moment(bill.billing_end_date, "YYYY-MM-DD");

					if ((billingStartDate.isAfter(moment(ctx.params.end_date, "YYYY-MM-DD")) || billingEndDate.isAfter(moment(ctx.params.end_date, "YYYY-MM-DD")))
						&& bill.status === "PROCESSED") {
						await ctx.call("accommodation.billings.patch", { id: bill.id, status: "CANCELLED" });
					}
				}
				await this.updateApplicationValues(ctx.params.application_id);
			},
		},

		processExtrasAndRegimeAndTypologyChange: {
			visibility: "public",
			params: {
				application_id: { type: "number", convert: true, positive: true },
				start_date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const billings = await ctx.call("accommodation.billings.find", {
					query: {
						billing_start_date: { gte: moment(ctx.params.start_date).format("yyyy-MM-DD") },
						application_id: ctx.params.application_id,
					},
				});
				for (const bill of billings) {
					if (bill.status == "PROCESSED") {
						try {
							await ctx.call("accommodation.billings.reprocessApplicationBilling", { id: bill.id });
						} catch (error) {
							this.logger.info(error);
						}
					}
				}
				await this.updateApplicationValues(ctx.params.application_id);
			},
		},
		report: {
			visibility: "published",
			params: {
				residence_id: { type: "number", positive: true, convert: true, optional: true },
				academic_year: { type: "string", convert: true, optional: true },
				month: { type: "number", positive: true, convert: true, optional: true },
				year: { type: "number", positive: true, convert: true, optional: true },
			},
			rest: "POST /report",
			async handler(ctx) {
				const billings = await this._find(ctx, {
					query: (qb) => {
						qb.select("billing.*");
						qb.innerJoin("application", "application.id", "billing.application_id");

						if (ctx.params.residence_id) {
							qb.where("application.assigned_residence_id", "=", ctx.params.residence_id);
						}
						if (ctx.params.academic_year) {
							qb.where("application.academic_year", "=", ctx.params.academic_year);
						}

						if (ctx.params.month) {
							qb.where("billing.month", "=", ctx.params.month);
						}

						if (ctx.params.year) {
							qb.where("billing.year", "=", ctx.params.year);
						}
					},
					withRelated: "billing_items",
				});

				const applications = await ctx.call("accommodation.applications.find", {
					query: {
						id: billings.map((b) => b.application_id),
					},
					withRelated: "room,assignedResidence",
				});

				for (const b of billings) {
					b.application = applications.find((a) => a.id === b.application_id);
					if (b.application.room.typology && b.application.room.typology.translations) {
						const typology_translation = b.application.room.typology.translations.find(
							(x) => x.language_id == 3,
						);
						b.application.room.typology.name = typology_translation.name;
					}
					b.total = this.getBillingTotalPrice(b.billing_items);
				}

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "ACCOMMODATION_BILLING_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							billings: billings,
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		generateSepaFile: {
			visibility: "public",
			params: {
				billing_ids: { type: "array", items: "number", optional: true },
				month: { type: "number", positive: true, convert: true, optional: false },
				year: { type: "number", positive: true, convert: true, optional: false },
			},
			async handler(ctx) {
				this.logger.info();
				this.logger.info("  -> # billings.generateSepaFile ctx.params: ");
				this.logger.info(ctx.params);

				let billingIds = "";
				if (ctx.params.billing_ids && ctx.params.billing_ids.length) {
					billingIds = " and b.id in (" + ctx.params.billing_ids.join(",") + ")";
				}

				// GetBillings
				const rawData = await this.adapter.raw(
					`
				select
				a.id
				,unaccent(a.full_name) "full_name"
				,a.created_at
				,a.tin
				,b.id "billing_id"
				,b.application_id
				,COALESCE(aic.allow_direct_debit, a.allow_direct_debit) as allow_direct_debit
				,COALESCE(aic.iban, a.iban) as iban
				,COALESCE(aic.swift, a.swift) as swift
				,(select sum(bi.price) from billing_item bi where bi.billing_id=b.id) "total"
				,b.sepa_first_recur
				,aic.sepa_id_debit_auth
				,aic.sepa_signtr_date
				from billing b
				inner join application a on a.id = b.application_id
				left join (select * from
						application_iban_change aict1
					where
						id in(
						select
							max(id)
						from
							application_iban_change aict2
						where
							aict2.application_id = aict1.application_id )) as aic on aic.application_id = a.id
				where
				(1=1)
				and COALESCE(b.sepa_first_recur, '') not in ('FRST','RCUR')
				and b."month" = ?
				and b."year" = ?
				and (b.status = 'VALIDATED' or b.status = 'BILLED')
				and COALESCE(aic.allow_direct_debit, a.allow_direct_debit) = true
				${billingIds}
				order by a.full_name
				`,
					[ctx.params.month, ctx.params.year],
				);
				// /GetBillings

				const billings = rawData.rows;

				let sepaFrstBillings = "";
				let sepaRcurBillings = "";

				let ctrlSumFRST = 0; //<-- will increment when FRST
				let ctrlSumRCUR = 0; //<-- will increment when RCUR
				let nbOfTxsFRST = 0; //<-- will increment when FRST
				let nbOfTxsRCUR = 0; //<-- will increment when RCUR

				let nbOfTxs = billings.length; //Quantidade de transações incluídas na mensagem.//<NbOfTxs>3</NbOfTxs>
				let ctrlSum = ctrlSumRCUR; //Montante total das transações incluídas na mensagem. (somatório do Instructed Amount). Admite até 2 decimais.//<CtrlSum>230.37</CtrlSum>

				for (const b of billings) {
					if (!b.sepa_id_debit_auth) {
						b.sepa_id_debit_auth = "00" + b.tin;
					}
					if (!b.sepa_signtr_date) {
						b.sepa_signtr_date = b.created_at;
					}
					if (!b.swift) {
						let oBicSwift = swifts.find((o) => o.cod_bdp === b.iban.substr(4, 4));

						if (oBicSwift) {
							b.swift = oBicSwift.bic_swift;
						}
					}

					// Chek if exists any FRST with current sepa_id_debit_auth
					const countFrstRcur = await this.broker.call("accommodation.billings.count", {
						query: {
							application_id: b.application_id,
							sepa_first_recur: "FRST",
							sepa_id_debit_auth: b.sepa_id_debit_auth,
						},
					});

					const isFirstSendWithCurrentSepaIdDebitAuth = countFrstRcur == 0;

					// Update SEPA info into billing
					/*await ctx.call("accommodation.billings.patch", {
						id: b.billing_id,
						allow_direct_debit: true,
						sepa_id_debit_auth: b.sepa_id_debit_auth,
						sepa_signtr_date: b.sepa_signtr_date,
						sepa_first_recur: isFirstSendWithCurrentSepaIdDebitAuth ? "FRST" : "RCUR",
						sepa_created_at: new Date(),
						sepa_iban: b.iban
					});*/
					const rawPatch = await this.adapter.raw(
						`
						update billing set
							allow_direct_debit = ?,
							sepa_id_debit_auth = ?,
							sepa_signtr_date = ?,
							sepa_first_recur = ?,
							sepa_created_at = ?,
							sepa_iban = ?
						where id = ?
					`,
						[
							true,
							b.sepa_id_debit_auth,
							b.sepa_signtr_date,
							isFirstSendWithCurrentSepaIdDebitAuth ? "FRST" : "RCUR",
							new Date(),
							b.iban,
							b.billing_id,
						],
					);
					// /Update SEPA

					const _regime = "ALOJAMENTO"; //b.application.regime.billing_name;

					if (isFirstSendWithCurrentSepaIdDebitAuth) {
						nbOfTxsFRST += 1;
						ctrlSumFRST += b.total;
						sepaFrstBillings += `
						<DrctDbtTxInf>
							<PmtId>
								<EndToEndId>${_regime + "-" + ctx.params.year + "" + ctx.params.month}</EndToEndId>
							</PmtId>
							<InstdAmt Ccy="EUR">${b.total}</InstdAmt>
							<DrctDbtTx>
								<MndtRltdInf>
									<MndtId>${b.sepa_id_debit_auth}</MndtId>
									<DtOfSgntr>${moment(b.sepa_signtr_date).format("YYYY-MM-DD")}</DtOfSgntr>
								</MndtRltdInf>
							</DrctDbtTx>
							<DbtrAgt>
								<FinInstnId>
									<BIC>${b.swift}</BIC>
								</FinInstnId>
							</DbtrAgt>
							<Dbtr>
								<Nm>${b.full_name}</Nm>
							</Dbtr>
							<DbtrAcct>
								<Id>
									<IBAN>${b.iban}</IBAN>
								</Id>
							</DbtrAcct>
							<Purp>
								<Cd>OTHR</Cd>
							</Purp>
						</DrctDbtTxInf>
						`;
					} else {
						nbOfTxsRCUR += 1;
						ctrlSumRCUR += b.total;
						sepaRcurBillings += `
						<DrctDbtTxInf>
							<PmtId>
								<EndToEndId>${_regime + "-" + ctx.params.year + "" + ctx.params.month}</EndToEndId>
							</PmtId>
							<InstdAmt Ccy="EUR">${b.total}</InstdAmt>
							<DrctDbtTx>
								<MndtRltdInf>
									<MndtId>${b.sepa_id_debit_auth}</MndtId>
									<DtOfSgntr>${moment(b.sepa_signtr_date).format("YYYY-MM-DD")}</DtOfSgntr>
								</MndtRltdInf>
							</DrctDbtTx>
							<DbtrAgt>
								<FinInstnId>
									<BIC>${b.swift}</BIC>
								</FinInstnId>
							</DbtrAgt>
							<Dbtr>
								<Nm>${b.full_name}</Nm>
							</Dbtr>
							<DbtrAcct>
								<Id>
									<IBAN>${b.iban}</IBAN>
								</Id>
							</DbtrAcct>
							<Purp>
								<Cd>OTHR</Cd>
							</Purp>
						</DrctDbtTxInf>
						`;
					}
				} // /billings

				const configurations = await ctx.call("accommodation.configurations.list", {});

				const msgId = moment().unix();
				const creDtTm = moment().toISOString();

				const businessDays = configurations.SEPA_REQUESTED_COLLECTION_DATE_DAYS_ADD || 5; // "RequestedCollectionDate" bussiness days to add
				const days =
					businessDays + Math.floor((Math.min(moment().day(), 5) + businessDays) / 6) * 2;
				const reqdColltnDt = moment().add(days, "days").format("YYYY-MM-DD");

				const cdtrAcct_IBAN = configurations.INSTITUTE_IBAN; //<IBAN>PT50003500000000000000000</IBAN>
				const cdtrAgt_BIC = configurations.INSTITUTE_SWIFT; //<BIC>CGDIPTPL</BIC>
				const initgPty_Nm = configurations.INSTITUTE_SEPA_NAME; //<Nm>SERV ACCAO SOCIAL INST POL LEIRIA</Nm>
				const initgPty_prvtId = configurations.INSTITUTE_SEPA_PRIVATE_ID; //<Id>PT49ZZZ103906</Id>

				let seqTP = "RCUR"; //Valores válidos: “FRST”, “OOFF”,“RCUR”, ”FNAL”
				let sepaFilename = `ALO__${ctx.params.year}_${ctx.params.month}__${cdtrAgt_BIC}_${msgId}.xml`;

				let PmtInf_FRST = `
				<PmtInf>
					<PmtInfId>ALOJAMENTO-FRST</PmtInfId>
					<PmtMtd>DD</PmtMtd>
					<NbOfTxs>${nbOfTxsFRST}</NbOfTxs>
					<CtrlSum>${parseFloat(ctrlSumFRST).toFixed(2)}</CtrlSum>
					<PmtTpInf>
						<SvcLvl>
							<Cd>SEPA</Cd>
						</SvcLvl>
						<SeqTp>FRST</SeqTp>
					</PmtTpInf>
					<ReqdColltnDt>${reqdColltnDt}</ReqdColltnDt>
					<Cdtr>
						<Nm>${initgPty_Nm}</Nm>
					</Cdtr>
					<CdtrAcct>
						<Id>
							<IBAN>${cdtrAcct_IBAN}</IBAN>
						</Id>
					</CdtrAcct>
					<CdtrAgt>
						<FinInstnId>
							<BIC>${cdtrAgt_BIC}</BIC>
						</FinInstnId>
					</CdtrAgt>
					<CdtrSchmeId>
						<Id>
							<PrvtId>
								<Othr>
									<Id>${initgPty_prvtId}</Id>
								</Othr>
							</PrvtId>
						</Id>
					</CdtrSchmeId>
					${sepaFrstBillings}
				</PmtInf>
				`;
				if (ctrlSumFRST == 0) {
					PmtInf_FRST = "";
				}

				let PmtInf_RCUR = `
				<PmtInf>
					<PmtInfId>ALOJAMENTO-RCUR</PmtInfId>
					<PmtMtd>DD</PmtMtd>
					<NbOfTxs>${nbOfTxsRCUR}</NbOfTxs>
					<CtrlSum>${parseFloat(ctrlSumRCUR).toFixed(2)}</CtrlSum>
					<PmtTpInf>
						<SvcLvl>
							<Cd>SEPA</Cd>
						</SvcLvl>
						<SeqTp>RCUR</SeqTp>
					</PmtTpInf>
					<ReqdColltnDt>${reqdColltnDt}</ReqdColltnDt>
					<Cdtr>
						<Nm>${initgPty_Nm}</Nm>
					</Cdtr>
					<CdtrAcct>
						<Id>
							<IBAN>${cdtrAcct_IBAN}</IBAN>
						</Id>
					</CdtrAcct>
					<CdtrAgt>
						<FinInstnId>
							<BIC>${cdtrAgt_BIC}</BIC>
						</FinInstnId>
					</CdtrAgt>
					<CdtrSchmeId>
						<Id>
							<PrvtId>
								<Othr>
									<Id>${initgPty_prvtId}</Id>
								</Othr>
							</PrvtId>
						</Id>
					</CdtrSchmeId>
					${sepaRcurBillings}
				</PmtInf>
				`;
				if (ctrlSumRCUR == 0) {
					PmtInf_RCUR = "";
				}

				let sepaFile = `<?xml version="1.0" encoding="utf-8"?>
				<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.008.001.02">
					<CstmrDrctDbtInitn>
						<GrpHdr>
							<MsgId>${msgId}</MsgId>
							<CreDtTm>${creDtTm}</CreDtTm>
							<NbOfTxs>${nbOfTxs}</NbOfTxs>
							<CtrlSum>${parseFloat(ctrlSumFRST + ctrlSumRCUR).toFixed(2)}</CtrlSum>
							<InitgPty>
								<Nm>${initgPty_Nm}</Nm>
								<Id>
									<PrvtId>
										<Othr>
											<Id>${initgPty_prvtId}</Id>
										</Othr>
									</PrvtId>
								</Id>
							</InitgPty>
						</GrpHdr>
						${PmtInf_FRST}
						${PmtInf_RCUR}
					</CstmrDrctDbtInitn>
				</Document>`;

				ctx.meta.$responseType = "application/xml";
				/*ctx.meta.$responseHeaders = {
					"Content-Disposition": `attachment; filename="ALO_${cdtrAgt_BIC}_${msgId}.xml"`,
				};*/
				ctx.meta.$responseHeaders = {
					"Content-Disposition": `attachment; filename="${sepaFilename}"`,
				};

				if (nbOfTxs) {
					this.logger.info();
					this.logger.info("    └─ # clearCache");
					this.logger.info("    └─ # nbOfTxs: ", nbOfTxs);
					this.clearCache();
				}

				return sepaFile;
			},
		},
		sepaMap: {
			visibility: "published",
			params: {
				residence_id: { type: "number", positive: true, convert: true, optional: true },
				month: { type: "number", positive: true, convert: true, optional: true },
				year: { type: "number", positive: true, convert: true, optional: true },
			},
			rest: "POST /sepa-map",
			async handler(ctx) {
				// ctx.params.query = ctx.params.query ? ctx.params.query : {};
				let query = {};
				if (ctx.params.residence_id) {
					query["assigned_residence_id"] = ctx.params.residence_id;
				}
				if (ctx.params.academic_year) {
					query["academic_year"] = ctx.params.academic_year;
				}
				if (ctx.params.residence_id || ctx.params.academic_year) {
					const applications_ids = await ctx.call("accommodation.applications.find", {
						query,
						withRelated: false,
					});
					ctx.params.application_id = applications_ids.map((x) => x.id);
					delete ctx.params.residence_id;
					delete ctx.params.academic_year;
				}

				let billingsQuery = ctx.params; //
				if (!ctx.params.residence_id) {
					delete ctx.params.residence_id;
				}
				if (!ctx.params.academic_year) {
					delete ctx.params.academic_year;
				}

				let sepa_first_recur = { in: ["FRST", "RCUR"] };
				billingsQuery.sepa_first_recur = sepa_first_recur;
				const billings = await this._find(ctx, { query: billingsQuery });

				for (const b of billings) {
					if (b.application.room.typology && b.application.room.typology.translations) {
						const typology_translation = b.application.room.typology.translations.find(
							(x) => x.language_id == 3,
						);
						b.application.room.typology.name = typology_translation.name;
					}
					b.total = this.getBillingTotalPrice(b.billing_items);

					if (b.movement_id) {
						const movement = await ctx.call("current_account.movements.get", {
							id: b.movement_id,
							fields: ["id", "operation", "doc_type_acronym", "seq_doc_num"],
							withRelated: false,
						});
						b.movement = movement[0];
					}
				}

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "ACCOMMODATION_BILLING_SEPA_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							billings: billings,
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"accommodation.billing_items.*"(ctx) {
			this.clearCache();
		},
		"accommodation.regimes.*"(ctx) {
			this.clearCache();
		},
		"accommodation.extras.*"(ctx) {
			this.clearCache();
		},
		"accommodation.typologies.*"(ctx) {
			this.clearCache();
		},
		"accommodation.residences.*"(ctx) {
			this.clearCache();
		},
	},

	/* Methods*/
	methods: {
		getBillingTotalPrice(billing_items) {
			let total = 0;
			for (const bi of billing_items) {
				total += bi.price;
			}
			return parseFloat(total).toFixed(2);
		},
		/*
		Return current in use extras. Valid if exist's approved change extra requests
		*/
		async getApplicationCurrentExtras(application, start_of_month) {
			const extra_changes = await this.broker.call("accommodation.application-extra-changes.find", {
				query: {
					application_id: application.id,
					status: "APPROVED",
					start_date: { lte: moment(start_of_month).format("yyyy-MM-DD") },
				},
				sort: "-created_at",
				limit: 1,
			});

			let valid_extras = [];
			if (extra_changes.length > 0) {
				for (const ec of extra_changes[0].extras) {
					valid_extras.push(ec.extra);
				}
			}
			valid_extras = extra_changes.length > 0 ? valid_extras : application.extras;
			return valid_extras;
		},
		/*
		Return current in use regime. Valid if exist's approved change regime requests
		*/
		async getApplicationCurrentRegime(application, start_of_month) {
			const regime_changes = await this.broker.call(
				"accommodation.application-regime-changes.find",
				{
					query: {
						application_id: application.id,
						status: "APPROVED",
						start_date: { lte: moment(start_of_month).format("yyyy-MM-DD") },
					},
					sort: "-created_at",
					limit: 1,
				},
			);

			if (regime_changes.length > 0) {
				return regime_changes[0].regime;
			} else {
				return application.regime;
			}
		},
		/*
		Return current in use regime. Valid if exist's approved change regime requests
		*/
		async getApplicationCurrentRoom(application, start_of_month) {
			const room_changes = await this.broker.call("accommodation.application-room-changes.find", {
				query: {
					application_id: application.id,
					status: "APPROVED",
					start_date: { lte: moment(start_of_month).format("yyyy-MM-DD") },
				},
				sort: "-created_at",
				limit: 1,
			});
			if (room_changes.length > 0) {
				return room_changes[0].room_id;
			} else {
				return application.room_id;
			}
		},
		/*
		Return current in use extras. Valid if exist's approved change extra requests
		*/
		async getApplicationCurrentTariff(application, start_of_month) {
			const tariff_changes = await this.broker.call(
				"accommodation.application-tariff-changes.find",
				{
					query: {
						application_id: application.id,
						status: "APPROVED",
						start_date: { lte: moment(start_of_month).format("yyyy-MM-DD") },
					},
					sort: "-created_at",
					limit: 1,
				},
			);

			return tariff_changes.length > 0 ? tariff_changes[0].tariff_id : application.tariff_id;
		},

		async processApplication(application) {

			await this.broker.call("accommodation.applications.patch", {
				id: application[0].id,
				billing_status: "proccessing",
			});

			const months = await this.getMonthsForProcessement(
				moment(application[0].updated_end_date),
				moment(application[0].start_date),
			);

			for (const m of months) {
				const hasAlreadyBillingInThisMonthAndYear = await this.broker.call(
					"accommodation.billings.count",
					{
						query: {
							application_id: application[0].id,
							month: m.month,
							year: m.year,
						},
					},
				);
				if (hasAlreadyBillingInThisMonthAndYear) {
					//If already exists an billing in this month and year dont create
					this.logger.warn("");
					this.logger.warn(
						"Already exists a billing for this month/year: ",
						m.month + "/" + m.year,
					);
					this.logger.warn(" --> billing creation ignored!");
					continue;
				} else {
					this.logger.info("");
					this.logger.info(" --> create billing --> ", m.month + "/" + m.year);
				}

				await this.processBillingByMonth(m, application[0], application[0].tariff_id);

			}
			await this.updateApplicationValues(application[0].id);
			await this.broker.call("accommodation.applications.patch", {
				id: application[0].id,
				billing_status: "proccessed",
			});

		},
		//Just for debugging purposes
		/*async processApplicationCtx(ctx, application) {
			this.logger.warn("");
			this.logger.warn(" ### ################## #### ");
			this.logger.warn(" ### processApplication #### # id: ", application[0].id);

			try {
				await ctx.call("accommodation.applications.patch", {
					id: application[0].id,
					billing_status: "proccessing",
				});

				const months = await this.getMonthsForProcessement(
					moment(application[0].updated_end_date),
					moment(application[0].start_date),
				);

				this.logger.warn("");
				this.logger.warn(months);

				for (const m of months) {
					const hasAlreadyBillingInThisMonthAndYear = await ctx.call(
						"accommodation.billings.count",
						{
							query: {
								application_id: application[0].id,
								month: m.month,
								year: m.year,
							},
						},
					);
					this.logger.warn("");
					this.logger.warn(" --> hasAlreadyBillingInThisMonthAndYear:");
					this.logger.warn(hasAlreadyBillingInThisMonthAndYear);
					if (hasAlreadyBillingInThisMonthAndYear) {
						//If exists in this month and year a billing so dont create the billing

						this.logger.warn("");
						this.logger.warn(
							"Already exists a billing for this month/year: ",
							m.month + "/" + m.year,
						);
						this.logger.warn(" --> billing creation ignored!");

						continue;
					} else {
						this.logger.warn("");
						this.logger.warn(" --> create billing --> ", m.month + "/" + m.year);
					}

					this.logger.warn("");
					this.logger.warn(" XYZ ");
					this.logger.warn({
						application_id: application[0].id,
						month: m.month,
						year: m.year,
						billing_start_date:
							moment(application[0].start_date).month() == m.month - 1 &&
								moment(application[0].start_date).year() == m.year
								? moment(application[0].start_date).toDate()
								: moment()
									.set("year", m.year)
									.set("month", m.month - 1)
									.set("date", 1)
									.toDate(),
						billing_end_date:
							moment(application[0].updated_end_date).month() == m.month - 1 &&
								moment(application[0].updated_end_date).year() == m.year
								? moment(application[0].updated_end_date).toDate()
								: moment()
									.set("year", m.year)
									.set("month", m.month - 1)
									.endOf("month")
									.toDate(),
						status: "PROCESSED",
						possible_retroactive: null,
						paid_at: null,
						tariff_id: application[0].tariff_id,
					});

					const billing = await ctx.call("accommodation.billings.create", {
						application_id: application[0].id,
						month: m.month,
						year: m.year,
						billing_start_date:
							moment(application[0].start_date).month() == m.month - 1 &&
								moment(application[0].start_date).year() == m.year
								? moment(application[0].start_date).toDate()
								: moment()
									.set("year", m.year)
									.set("month", m.month - 1)
									.set("date", 1)
									.toDate(),
						billing_end_date:
							moment(application[0].updated_end_date).month() == m.month - 1 &&
								moment(application[0].updated_end_date).year() == m.year
								? moment(application[0].updated_end_date).toDate()
								: moment()
									.set("year", m.year)
									.set("month", m.month - 1)
									.endOf("month")
									.toDate(),
						status: "PROCESSED",
						possible_retroactive: null,
						paid_at: null,
						tariff_id: application[0].tariff_id,
					});
					let retroactive = 0;

					try {
						const room_billing = await this.processApplicationRoom(
							application[0],
							m.month,
							m.year,
							billing[0].id,
						);
						retroactive += room_billing[0].possible_retroactive;

						await this.processApplicationExtras(
							application[0],
							billing[0].billing_start_date,
							billing[0].billing_end_date,
							billing[0].id,
						);

						const regime_billing = await this.processApplicationRegime(
							application[0],
							m.month,
							m.year,
							billing[0].id,
						);
						retroactive += regime_billing[0].possible_retroactive;
					} catch (err) {
						this.logger.error(
							"Error occurred on procement (Maybe non definited prices). Please reprocess.",
						);
					}

					await ctx.call("accommodation.billings.patch", {
						id: billing[0].id,
						possible_retroactive: retroactive,
					});
				}
				await this.updateApplicationValues(application[0].id);
				await ctx.call("accommodation.applications.patch", {
					id: application[0].id,
					billing_status: "proccessed",
				});
			} catch (err) {
				await ctx.call("accommodation.applications.patch", {
					id: application[0].id,
					billing_status: "error_in_proccessing",
				});
				this.logger.fatal("DEU STRESS");
				this.logger.fatal(err);
				return err;
			}
		},*/

		getMonthsForProcessement(end_date, start_date) {
			let months_of_processment = [];
			while (end_date.isSameOrAfter(start_date, "M")) {
				months_of_processment.push({
					month: start_date.format("M"),
					year: start_date.format("YYYY"),
				});
				start_date.add(1, "month");
			}

			return months_of_processment;
		},

		async processApplicationRoom(application, month, year, billing_id) {
			const room_id = await this.getApplicationCurrentRoom(
				application,
				moment()
					.set("month", month - 1)
					.set("year", year)
					.set("date", 1),
			);

			let room = await this.broker.call("accommodation.rooms.get", {
				withRelated: ["typology"],
				id: room_id,
			});
			room = room[0];

			const priceLines = await this.broker.call("accommodation.price_lines.find", {
				query: {
					typology_id: room.typology_id,
				},
			});

			const current_tariff_id = await this.getApplicationCurrentTariff(
				application,
				moment()
					.set("month", month - 1)
					.set("year", year)
					.set("date", 1),
			);
			let start_of_month = moment()
				.set("year", year)
				.set("month", month - 1)
				.set("date", 1);
			const end_of_month = start_of_month.clone().endOf("month");

			if (priceLines.length === 0 || priceLines.length && !priceLines.find(p => p.tariff_id === current_tariff_id)) {
				//set price to 0
				this.logger.warn("NO PRICE LINES FOR TYPOLOGY");
				return await this.broker.call("accommodation.billing_items.create", {
					product_code: room.typology.product_code,
					name:
						"Alojamento no Quarto ".concat(room.name).concat(" - Mensalidade [") +
						moment(start_of_month).format("YYYY-MM-DD") +
						" / " +
						moment(end_of_month).format("YYYY-MM-DD") +
						"]",
					description: "",
					room_id: room.id,
					typology_id: room.typology.id,
					regime_id: null,
					extra_id: null,
					quantity: 1,
					unit_price: 0,
					price: 0,
					vat_id: 2,
					vat_value: 0,
					discount: 0,
					discount_value: 0,
					period: "MONTH",
					billing_id: billing_id,
					possible_retroactive: 0,
				});
			} else {
				const discount = await this.broker.call("accommodation.typologies-discount.find", {
					query: {
						typology_id: room.typology_id,
						month: month,
						year: year
					}
				});
				const most_cheap = await this.getCheaperPrice(priceLines, current_tariff_id, start_of_month, end_of_month, application);
				//missing quantity count
				const unit_price = discount.length && discount[0].discount_value > 0 && most_cheap.unit_price > 0
					? (most_cheap.unit_price - ((most_cheap.unit_price * discount[0].discount_value) / 100))
					: most_cheap.unit_price;
				const discount_value = discount.length ? (most_cheap.price * discount[0].discount_value) / 100 : 0;

				return await this.broker.call("accommodation.billing_items.create", {
					product_code: room.typology.product_code,
					name: "Alojamento no Quarto "
						.concat(room.name)
						.concat(" - ")
						.concat(most_cheap.period_name),
					description: "-",
					application_id: application.id,
					room_id: room.id,
					typology_id: room.typology.id,
					regime_id: null,
					extra_id: null,
					quantity: most_cheap.quantity,
					unit_price: unit_price,
					price: unit_price * most_cheap.quantity,
					discount: discount.length ? discount[0].discount_value : 0,
					discount_value: discount_value,
					vat_id: most_cheap.vat.id,
					vat_value: most_cheap.vat.tax_value,
					period: most_cheap.period,
					billing_id: billing_id,
					possible_retroactive: most_cheap.possibleRetroactive,
				});
			}

		},

		async getCheaperPrice(priceLines, current_tariff_id, start_of_month, end_of_month, application) {
			let dates_of_use = [start_of_month, end_of_month];
			let weeks_of_use = 0;
			let days_of_use = 0;
			const pl_month = priceLines.find(
				(pl) => pl.period === "MONTH" && pl.tariff_id === current_tariff_id,
			);
			const price_month = pl_month ? pl_month.price : null;
			const possible_retroactive_month = await this.possibleRetroactive(
				priceLines,
				"MONTH",
				current_tariff_id,
			);

			const pl_week = priceLines.find(
				(pl) => pl.period === "WEEK" && pl.tariff_id === current_tariff_id,
			);
			const price_week = pl_week ? pl_week.price : null;
			const possible_retroactive_week = await this.possibleRetroactive(
				priceLines,
				"WEEK",
				current_tariff_id,
			);

			let pl_day = priceLines.find(
				(pl) => pl.period === "DAY" && pl.tariff_id === current_tariff_id,
			);
			const price_day = pl_day ? pl_day.price : null;
			const possible_retroactive_day = await this.possibleRetroactive(
				priceLines,
				"DAY",
				current_tariff_id,
			);


			// Process complete month
			if (
				!moment(application.start_date).isBetween(start_of_month, end_of_month, "day") &&
				!moment(application.updated_end_date).isBetween(start_of_month, end_of_month, "day")
			) {
				weeks_of_use = moment(end_of_month).diff(start_of_month, "week") + 1;
				days_of_use = moment(end_of_month).diff(start_of_month, "day") + 1;
				dates_of_use = [start_of_month, end_of_month];
			}
			// Application inits and didn't end in middle of month
			else if (
				moment(application.start_date).isBetween(start_of_month, end_of_month) &&
				!moment(application.updated_end_date).isBetween(start_of_month, end_of_month)
			) {
				weeks_of_use = moment(end_of_month).diff(application.start_date, "week") + 1;
				days_of_use = moment(end_of_month).diff(application.start_date, "day") + 1;
				dates_of_use = [application.start_date, end_of_month];
			}
			// Application ends in middle of month
			else if (
				moment(application.updated_end_date).isBetween(start_of_month, end_of_month) &&
				!moment(application.start_date).isBetween(start_of_month, end_of_month)
			) {
				weeks_of_use = moment(application.updated_end_date).diff(start_of_month, "week") + 1;
				days_of_use = moment(application.updated_end_date).diff(start_of_month, "day") + 1;
				dates_of_use = [start_of_month, application.updated_end_date];
			}
			// Applications inits and ends in middle of month
			else if (
				moment(application.start_date).isBetween(start_of_month, end_of_month) &&
				moment(application.updated_end_date).isBetween(start_of_month, end_of_month)
			) {
				weeks_of_use =
					moment(application.updated_end_date).diff(application.start_date, "week") + 1;
				days_of_use = moment(application.updated_end_date).diff(application.start_date, "day") + 1;
				dates_of_use = [application.start_date, application.updated_end_date];
			}
			// Calculate cheaper billing period (DAY, WEEK, MONTH)
			let prices_by_period = [];
			// Price calculated per day
			if (pl_day) {
				prices_by_period.push({
					priceLine: pl_day,
					possibleRetroactive: possible_retroactive_day * days_of_use,
					unit_price: price_day,
					price: price_day * days_of_use,
					vat: pl_day.vat,
					quantity: days_of_use,
					period: "DAY",
					period_name:
						days_of_use +
						" Dias [" +
						moment(dates_of_use[0]).format("YYYY-MM-DD") +
						"/" +
						moment(dates_of_use[1]).format("YYYY-MM-DD") +
						"]",
				});
			}
			// Price calculated per week
			if (pl_week) {
				prices_by_period.push({
					priceLine: pl_week,
					possibleRetroactive: possible_retroactive_week * weeks_of_use,
					unit_price: price_week,
					price: price_week * weeks_of_use,
					vat: pl_week.vat,
					quantity: weeks_of_use,
					period: "WEEK",
					period_name:
						weeks_of_use +
						" Semanas [" +
						moment(dates_of_use[0]).format("YYYY-MM-DD") +
						"/" +
						moment(dates_of_use[1]).format("YYYY-MM-DD") +
						"]",
				});
			}
			// Price calculated per month
			if (pl_month) {
				prices_by_period.push({
					priceLine: pl_month,
					possibleRetroactive: possible_retroactive_month,
					unit_price: price_month,
					price: price_month,
					vat: pl_month.vat,
					quantity: 1,
					period: "MONTH",
					period_name:
						"Mensalidade [" +
						moment(dates_of_use[0]).format("YYYY-MM-DD") +
						" / " +
						moment(dates_of_use[1]).format("YYYY-MM-DD") +
						"]",
				});
			}

			return prices_by_period.reduce(
				(c, p) => (p.priceLine && c.price > p.price ? p : c),
				prices_by_period[0],
			);

		},

		async processApplicationExtras(application, start_of_month, end_of_month, billing_id) {
			const extras = [];
			const current_extras = await this.getApplicationCurrentExtras(application, start_of_month);
			if (current_extras) {
				for (const extra of current_extras) {
					extras.push(this.createExtraBilling(extra, start_of_month, end_of_month, billing_id));
				}
			}
			return extras;
		},

		async createExtraBilling(extra, start_of_month, end_of_month, billing_id) {
			const discount = await this.broker.call("accommodation.extras-discount.find", {
				query: {
					extra_id: extra.id,
					month: moment(start_of_month).format("MM").valueOf(),
					year: moment(start_of_month).format("YYYY").valueOf()
				}
			});
			const price = discount.length ? extra.price - ((extra.price * discount[0].discount_value) / 100) : extra.price;
			const discount_value = discount.length ? (extra.price * discount[0].discount_value) / 100 : 0;

			const item = await this.broker.call("accommodation.billing_items.create", {
				product_code: extra.product_code,
				name:
					"".concat(extra.billing_name).concat(" - Mensalidade [") +
					moment(start_of_month).format("YYYY-MM-DD") +
					" / " +
					moment(end_of_month).format("YYYY-MM-DD") +
					"]",
				description: "",
				room_id: null,
				typology_id: null,
				regime_id: null,
				extra_id: extra.id,
				quantity: 1,
				unit_price: price,
				price: price,
				discount: discount.length ? discount[0].discount_value : 0,
				discount_value: discount_value,
				vat_id: extra.vat_id,
				vat_value: extra.vat.tax_value,
				period: "MONTH",
				billing_id: billing_id,
				possible_retroactive: 0,
			});

			return item;
		},

		async processApplicationRegime(application, month, year, billing_id) {
			const current_regime = await this.getApplicationCurrentRegime(
				application,
				moment()
					.set("month", month - 1)
					.set("year", year)
					.set("date", 1),
			);

			const priceLines = current_regime.priceLines;

			const current_tariff_id = await this.getApplicationCurrentTariff(
				application,
				moment()
					.set("month", month - 1)
					.set("year", year)
					.set("date", 1),
			);
			let start_of_month = moment()
				.set("year", year)
				.set("month", month - 1)
				.set("date", 1);
			const end_of_month = start_of_month.clone().endOf("month");

			if (priceLines.length === 0 || priceLines.length && !priceLines.find(p => p.tariff_id === current_tariff_id)) {
				//set price to 0
				this.logger.warn("NO PRICE LINES FOR REGIME");
				return await this.broker.call("accommodation.billing_items.create", {
					product_code: current_regime.product_code,
					name: current_regime.billing_name.concat(" - Mensalidade [") +
						moment(start_of_month).format("YYYY-MM-DD") +
						" / " +
						moment(end_of_month).format("YYYY-MM-DD") +
						"]",
					description: "",
					room_id: null,
					typology_id: null,
					regime_id: current_regime.id,
					extra_id: null,
					quantity: 1,
					unit_price: 0,
					price: 0,
					vat_id: 2,
					vat_value: 0,
					discount: 0,
					discount_value: 0,
					period: "MONTH",
					billing_id: billing_id,
					possible_retroactive: 0,
				});
			} else {

				const most_cheap = await this.getCheaperPrice(priceLines, current_tariff_id, start_of_month, end_of_month, application);

				const discount = await this.broker.call("accommodation.regimes-discount.find", {
					query: {
						regime_id: current_regime.id,
						month: month,
						year: year
					}
				});
				const unit_price = discount.length && discount[0].discount_value > 0 && most_cheap.unit_price > 0
					? (most_cheap.unit_price - ((most_cheap.unit_price * discount[0].discount_value) / 100))
					: most_cheap.unit_price;
				const discount_value = discount.length ? (most_cheap.price * discount[0].discount_value) / 100 : 0;

				return await this.broker.call("accommodation.billing_items.create", {
					product_code: current_regime.product_code,
					name: current_regime.billing_name.concat(" - ").concat(most_cheap.period_name),
					description: "",
					room_id: null,
					typology_id: null,
					regime_id: current_regime.id,
					extra_id: null,
					quantity: most_cheap.quantity,
					unit_price: unit_price,
					price: unit_price * most_cheap.quantity,
					discount: discount.length ? discount[0].discount_value : 0,
					discount_value: discount_value,
					vat_id: most_cheap.vat.id,
					vat_value: most_cheap.vat.tax_value,
					payment_method_id: application.payment_method_id,
					period: most_cheap.period,
					billing_id: billing_id,
					possible_retroactive: most_cheap.possibleRetroactive,
				});
			}
		},

		async generateMovement(billing) {
			/*TODO: Missing refund (negatice prices)*/
			const items = [];
			for (const bi of billing.billing_items) {
				if (bi.unit_price > 0) {
					items.push({
						service_id: 1,
						product_code: bi.product_code,
						name: bi.name,
						description: bi.description,
						quantity: bi.quantity,
						vat_id: bi.vat_id,
						unit_value: bi.unit_price,
						//price: bi.price,
						discount_value: bi.discount_value,
						location: billing.application.assignedResidence.name,
						article_type: "accommodation",
						extra_info: { billing_item_id: bi.id, billing_id: bi.billing_id },
						service_confirm_path: "accommodation.billings.current_account_confirm",
						service_cancel_path: "accommodation.billings.current_account_cancel",
					});
				}
			}
			if (items.length) {
				return await this.broker.call("current_account.movements.create", {
					account_id: billing.application.assignedResidence.current_account_id,
					operation: "INVOICE",
					user_id: billing.application.user_id,
					description: "Alojamento",
					expiration_at: billing.due_date,
					items,
				});
			}
			return Promise.reject(new Error("Billing without items"));
		},

		async currentAccountProcessBilling(billing_id) {
			const billing = await this.broker.call("accommodation.billings.get", { id: billing_id });
			const movement = await this.generateMovement(billing[0]);
			const configurations = await this.broker.call("accommodation.configurations.list", {});
			let due_date = null;
			if (configurations["BILLING_OUT_OF_DATE_TYPE"] == "MONTH_DAY") {
				due_date = moment();
				if (due_date.day() + 1 < configurations["BILLING_OUT_OF_DATE_DAY"]) {
					due_date = due_date.day(configurations["BILLING_OUT_OF_DATE_DAY"] - 1);
				} else {
					due_date = due_date
						.day(configurations["BILLING_OUT_OF_DATE_DAY"] - 1)
						.month(moment().month() + 1);
				}
			} else {
				due_date = moment()
					.add(configurations["BILLING_OUT_OF_DATE_DAY"], "day")
					.format("yyyy-MM-DD");
			}
			await this.broker.call("accommodation.billings.patch", {
				id: billing[0].id,
				status: "BILLED",
				movement_id: movement[0].id,
				due_date,
			});
			await this.updateApplicationValues(billing[0].application_id);
		},

		/* REVIEW */
		async reprocessApplicationBilling(billing) {
			const month = moment(billing.billing_start_date).month() + 1;
			const year = moment(billing.billing_start_date).year();

			// billing.application.updated_end_date = await this.getCurrentApplicationEndDate(billing.application);

			const billing_start_date =
				moment(billing.application.start_date).month() == month - 1 &&
					moment(billing.application.start_date).year() == year
					? moment(billing.application.start_date)
					: moment()
						.set("year", year)
						.set("month", month - 1)
						.set("date", 1);
			const billing_end_date =
				moment(billing.application.updated_end_date).month() == month - 1 &&
					moment(billing.application.updated_end_date).year() == year
					? moment(billing.application.updated_end_date)
					: moment()
						.set("year", year)
						.set("month", month - 1)
						.endOf("month");

			const oldItems = [];
			const current_extras = await this.getApplicationCurrentExtras(billing.application, billing_start_date);
			for (const item of billing.billing_items) {
				if (item.processed_manually) {
					oldItems.push(item);
				} else {
					if (item.extra) {
						if (item.extra.reprocess_since && moment(moment(item.extra.reprocess_since).format("YYYY-MM").valueOf()).isAfter(billing_start_date.format("YYYY-MM"))) {
							if (current_extras.length && current_extras.find(current => current.id === item.extra.id) && oldItems.find(old => old.extra_id !== item.extra.id)) {
								oldItems.push(item);
							} else {
								await this.broker.call("accommodation.billing_items.remove", { id: item.id });
							}
						}
						else {
							await this.broker.call("accommodation.billing_items.remove", { id: item.id });
						}
					} else if (item.regime_id) {
						const regime = await this.broker.call("accommodation.regimes.get", { id: item.regime_id });
						if (regime.length) {
							if (regime[0].reprocess_since && moment(moment(regime[0].reprocess_since).format("YYYY-MM").valueOf()).isAfter(billing_start_date.format("YYYY-MM"))) {
								oldItems.push(item);
							} else {
								await this.broker.call("accommodation.billing_items.remove", { id: item.id });
							}
						} else {
							await this.broker.call("accommodation.billing_items.remove", { id: item.id });
						}
					} else if (item.typology_id) {
						const typology = await this.broker.call("accommodation.typologies.get", { id: item.typology_id });
						if (typology.length) {
							if (typology[0].reprocess_since && moment(moment(typology[0].reprocess_since).format("YYYY-MM").valueOf()).isAfter(billing_start_date.format("YYYY-MM"))) {
								oldItems.push(item);
							} else {
								await this.broker.call("accommodation.billing_items.remove", { id: item.id });
							}
						} else {
							await this.broker.call("accommodation.billing_items.remove", { id: item.id });
						}

					}
				}
			}

			let retroactive = 0;
			//Prevent calculate billing after billing_end_date, all values will set to 0 (zero)
			//let current_tariff_id = null;
			if (
				moment(billing_start_date, "YYYY-MM").isSameOrBefore(moment(billing.application.updated_end_date).format("YYYY-MM"), "month")
			) {

				if (!oldItems.filter(oldItem => oldItem.typology !== null && !oldItem.processed_manually).length) {
					if (billing.application.room_id) {
						const room_billing = await this.processApplicationRoom(
							billing.application,
							month,
							year,
							billing.id,
						);
						retroactive += room_billing[0].possible_retroactive;
					}
				} else {
					const items = oldItems.filter(oldItem => oldItem.typology);
					for (const item of items) {
						retroactive += item.possible_retroactive;
					}
				}

				const oldExtras = oldItems.filter(oldItem => oldItem.extra);
				if (!oldExtras.length) {
					await this.processApplicationExtras(
						billing.application,
						billing_start_date,
						billing_end_date,
						billing.id,
					);
				} else {
					for (const current of current_extras) {
						await this.createExtraBilling(current, billing_start_date, billing_end_date, billing.id);
					}
				}

				if (!oldItems.filter(oldItem => oldItem.regime_id !== null && !oldItem.processed_manually).length) {
					if (billing.application.regime_id) {
						const regime_billing = await this.processApplicationRegime(
							billing.application,
							month,
							year,
							billing.id,
						);
						retroactive += regime_billing[0].possible_retroactive;
					}
				} else {
					const items = oldItems.filter(oldItem => oldItem.regime_id);
					for (const item of items) {
						retroactive += item.possible_retroactive;
					}
				}
			}

			let current_tariff_id = null;
			if (billing.application.tariff_id) {
				current_tariff_id = await this.getApplicationCurrentTariff(
					billing.application,
					moment()
						.set("month", month - 1)
						.set("year", year)
						.set("date", 1),
				);
			}

			//check total billing
			const totalBilling = await this.getBillingItemsSum(billing.id);
			let billingStatus = "CANCELLED";
			if (totalBilling && totalBilling > 0) {
				billingStatus = "PROCESSED";
			}
			return await this.broker.call("accommodation.billings.patch", {
				id: billing.id,
				possible_retroactive: retroactive,
				billing_start_date,
				billing_end_date,
				tariff_id: current_tariff_id,
				status: billingStatus,
			});
		},

		// Calculate retroactive between students with/without social scholarship
		async possibleRetroactive(priceLines, period, application_tariff_id) {
			const scholarshp_price = priceLines.find((pl) => pl.period === period && pl.tariff_id === 1);
			const without_scholarshp_price = priceLines.find(
				(pl) => pl.period === period && pl.tariff_id === 2,
			);
			if (application_tariff_id == 1 && scholarshp_price && without_scholarshp_price) {
				return +scholarshp_price.price - +without_scholarshp_price.price;
			} else if (application_tariff_id == 2 && scholarshp_price && without_scholarshp_price) {
				return +without_scholarshp_price.price - +scholarshp_price.price;
			}
			return 0;
		},

		// Calculate sum of billing items
		async getBillingItemsSum(billing_id) {
			let sum = await this.adapter
				.getDB("billing_item")
				.innerJoin("billing", "billing.id", "billing_item.billing_id")
				.sum("price")
				.where("billing.id", billing_id);
			return sum.length ? sum[0].sum : 0;
		},

		// Update application total_unbilled and total_unpaid fields
		// Total_unbilled = Billings in processed status
		// Total_unpaid = Billings in billed status (billed and not paid)
		async updateApplicationValues(application_id) {
			let total_unbilled = await this.adapter
				.getDB("billing_item")
				.innerJoin("billing", "billing.id", "billing_item.billing_id")
				.sum("price")
				.where("billing.application_id", application_id)
				.andWhere("billing.status", "PROCESSED");

			let total_unpaid = await this.adapter
				.getDB("billing_item")
				.innerJoin("billing", "billing.id", "billing_item.billing_id")
				.sum("price")
				.where("billing.application_id", application_id)
				.andWhere("billing.status", "BILLED");

			return await this.broker.call("accommodation.applications.patch", {
				id: application_id,
				total_unbilled: total_unbilled[0].sum ? total_unbilled[0].sum : 0,
				total_unpaid: total_unpaid[0].sum ? total_unpaid[0].sum : 0,
			});
		},

		/* PROCESS BILLING BY DATES - USED FOR EXTENSION AND ACCOMODATION PERIOD CHANGE */
		async processBillingsByDates(ctx, start_date, end_date, last_billing, billings) {
			const monthsYears = await this.getMonthsForProcessement(moment(end_date), moment(start_date));
			for (const m of monthsYears) {
				const checkIfBillingExists = billings.filter(billing => billing.month === Number(m.month) && billing.year === Number(m.year));
				if (checkIfBillingExists.length) {
					//Para o caso de haver duplicados
					for (const bill of checkIfBillingExists) {
						if (bill.status === "PROCESSED" || bill.status === "CANCELLED") {
							this.logger.info("");
							this.logger.info(" *********** REPROCESS BILLING PROCESS ************* ");
							this.logger.info(" --> Month / Year : " + m.month + " / " + m.year);
							this.logger.info(" --> Application ID : " + bill.application_id);
							await ctx.call("accommodation.billings.reprocessApplicationBilling", { id: bill.id });
						}
					}
				} else {
					await this.processBillingByMonth(m, last_billing.application, last_billing.tariff_id);
				}
			}
		},

		/* CREATE BILLING */
		async processBillingByMonth(m, application, tariff_id) {
			try {
				this.logger.info("");
				this.logger.info(" *********** CREATE BILLING PROCESS ************* ");
				this.logger.info(" --> Month / Year : " + m.month + " / " + m.year);
				this.logger.info(" --> Application ID : " + application.id);

				const billing = await this.broker.call("accommodation.billings.create", {
					application_id: application.id,
					month: m.month,
					year: m.year,
					billing_start_date:
						moment(application.start_date).month() == m.month - 1 &&
							moment(application.start_date).year() == m.year
							? moment(application.start_date)
							: moment()
								.set("year", m.year)
								.set("month", m.month - 1)
								.set("date", 1),
					billing_end_date:
						moment(application.updated_end_date).month() == m.month - 1 &&
							moment(application.updated_end_date).year() == m.year
							? moment(application.updated_end_date)
							: moment()
								.set("year", m.year)
								.set("month", m.month - 1)
								.endOf("month"),
					status: "PROCESSED",
					possible_retroactive: null,
					paid_at: null,
					tariff_id: tariff_id,
				});
				let retroactive = 0;

				if (application.room_id) {
					const room_billing = await this.processApplicationRoom(
						application,
						m.month,
						m.year,
						billing[0].id,
					);
					retroactive += room_billing[0].possible_retroactive;
				}
				await this.processApplicationExtras(
					application,
					billing[0].billing_start_date,
					billing[0].billing_end_date,
					billing[0].id,
				);

				if (application.regime_id) {
					const regime_billing = await this.processApplicationRegime(
						application,
						m.month,
						m.year,
						billing[0].id,
					);
					retroactive += regime_billing[0].possible_retroactive;
				}
				await this.broker.call("accommodation.billings.patch", {
					id: billing[0].id,
					possible_retroactive: retroactive,
				});

			} catch (e) {
				await this.broker.call("accommodation.applications.patch", {
					id: application.id,
					billing_status: "error_in_proccessing",
				});
				this.logger.fatal("DEU STRESS");
				this.logger.fatal(e);
				return e;
			}
		},



	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
