"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const Validator = require("moleculer").Validator;
const QueueService = require("moleculer-bull");
const { swifts } = require("./helpers/bic_swift.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "configurations"), QueueService(process.env.REDIS_QUEUE)],
	/**
	 * QUEUES
	 */
	queues: {
		// "configurations.processUnprocessedApplications"(job) {
		// 	this.logger.info("NEW JOB RECEIVED!");
		// 	return this.processUnprocessedApplicationsInContract()
		// 		.then(() => {
		// 			this.logger.info("ANTES DE MANDAR O RESOLVE");
		// 			return Promise.resolve({
		// 				done: true,
		// 				id: job.data.id,
		// 				worker: process.pid,
		// 			});
		// 		})
		// 		.catch(err => {
		// 			this.logger.info("ANTES DE MANDAR O REJECT");
		// 			this.logger.info(err);
		// 			return Promise.reject(err);
		// 		});
		// },
	},

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				function validateRequiredFields(ctx) {
					const v = new Validator();

					if (ctx.params.ASSIGNED_STATUS_AUTO_CHANGE == true) {
						const schema = {
							STATUS_AUTO_CHANGE_DAYS: { type: "number", positive: true, convert: true },
							EVENT_FOR_AUTO_CHANGE: { type: "enum", values: ["CONFIRM", "REJECT"] },
						};
						const check = v.compile(schema);
						const res = check(ctx.params);
						if (res !== true)
							return Promise.reject(new ValidationError("Entity validation error!", null, res));
					}

					if (ctx.params.CHANGE_STATUS_ON_CREATE == true) {
						const schema = {
							CHANGE_STATUS_ON_CREATE_DAYS: { type: "number", convert: true },
						};
						const check = v.compile(schema);
						const res = check(ctx.params);
						if (res !== true)
							return Promise.reject(new ValidationError("Entity validation error!", null, res));
					}
				},
				"sanatizeIbanSwift",
			],
			update: ["sanatizeIbanSwift"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});
				return result;
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			params: {
				ALLOW_OPTIONAL_RESIDENCE: { type: "boolean" },
				ALLOW_APPLICATION_RENEW: { type: "boolean" },
				ASSIGNED_STATUS_AUTO_CHANGE: { type: "boolean" },
				STATUS_AUTO_CHANGE_DAYS: { type: "number", positive: true, optional: true },
				EVENT_FOR_AUTO_CHANGE: { type: "enum", values: ["CONFIRM", "REJECT"], optional: true },
				BILLING_OUT_OF_DATE_TYPE: { type: "enum", values: ["MONTH_DAY", "DAYS"] },
				BILLING_OUT_OF_DATE_DAY: { type: "number", positive: true },
				CHANGE_STATUS_ON_CREATE: { type: "boolean" },
				CHANGE_STATUS_ON_CREATE_DAYS: { type: "number", convert: true, optional: true },
				SHOW_IBAN_ON_APPLICATION_FORM: { type: "boolean" },
				UNASSIGNED_OPPOSITION_LIMIT: { type: "number", positive: true },

				INSTITUTE_IBAN: { type: "string" },
				INSTITUTE_SWIFT: { type: "string" },
				INSTITUTE_SEPA_NAME: { type: "string" },
				INSTITUTE_SEPA_PRIVATE_ID: { type: "string" },
				SEPA_REQUESTED_COLLECTION_DATE_DAYS_ADD: { type: "number", positive: true, optional: true },
				ENABLE_STATUS_EXTERNAL_VALIDATE_OR_PAID: { type: "boolean" },

				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key: key } }).then((config) => {
						if (config.length > 0 && ctx.params[config[0].key] != null) {
							return this._update(
								ctx,
								{ id: config[0].id, value: JSON.stringify(ctx.params[config[0].key]) },
								true,
							);
						}
					});
				});
				return Promise.all(promisses).then(() => ctx.call("accommodation.configurations.list", {}));
			},
		},
		processUnprocessedApplications: {
			visibility: "published",
			rest: "POST /applications/process",
			async handler(ctx) {
				this.processUnprocessedApplicationsInContract(ctx);
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async processUnprocessedApplicationsInContract(ctx) {
			const applications = await this.broker.call("accommodation.applications.find", {
				query: { status: "contracted" },
			});
			for (const app of applications) {
				const billings = await this.broker.call("accommodation.billings.count", {
					query: { application_id: app.id },
				});
				if (billings == 0) {
					this.createJob(
						"billings.process",
						{
							application: [app],
						},
						{
							attempts: 1,
							removeOnComplete: true,
							removeOnFail: true,
						},
					);
				}
			}
		},
		sanatizeIbanSwift(ctx) {
			if (ctx.params.INSTITUTE_IBAN) {
				ctx.params.INSTITUTE_IBAN = ctx.params.INSTITUTE_IBAN.replace(/\s/g, "").toUpperCase();

				let oBicSwift = swifts.find((o) => o.cod_bdp === ctx.params.INSTITUTE_IBAN.substr(4, 4));

				if (oBicSwift) {
					ctx.params.INSTITUTE_SWIFT = oBicSwift.bic_swift;
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
