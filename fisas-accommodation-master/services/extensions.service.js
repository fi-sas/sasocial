"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const extensionStateMachine = require("./state-machines/extensions.machine");
const moment = require("moment");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.extensions",
	table: "extension",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "extensions")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"end_date",
			"reason",
			"application_id",
			"user_id",
			"decision",
			"status",
			"observations",
			"created_at",
			"updated_at",
			"file_id"
		],
		parseBoolean: ["active"],
		defaultWithRelateds: ["application", "history", "file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.extension-history",
					"history",
					"id",
					"extension_id",
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			end_date: { type: "date", convert: true },
			reason: { type: "string" },
			application_id: { type: "number", convert: true },
			user_id: { type: "number", convert: true },
			decision: {
				type: "enum",
				values: ["APPROVE", "REJECT"],
				optional: true,
			},
			status: {
				type: "enum",
				values: ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"],
				optional: true,
			},
			observations: { type: "string", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
			file_id: { type: "number", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "SUBMITTED";
				},
				function getActiveApplication(ctx) {
					return ctx
						.call("accommodation.applications.find", {
							query: {
								user_id: ctx.meta.user.id,
								status: "contracted",
							},
						})
						.then((applications) => {
							if (applications.length > 0) {
								ctx.params.application_id = applications[0].id;
								if (moment(ctx.params.end_date).isSameOrBefore(applications[0].end_date)) {
									throw new ValidationError(
										"End date cannot be before application end date",
										"ACCOMMODATION_EXTENSION_INVALID_DATE",
										{},
									);
								}
							} else {
								throw new ValidationError(
									"No active application finded in accommodation",
									"ACCOMMODATION_APPLICAION_NOT_FOUND",
									{},
								);
							}
						});
				},
				function checkIfAlreadyExist(ctx) {
					return ctx
						.call("accommodation.extensions.count", {
							query: {
								application_id: ctx.params.application_id,
								status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
							},
						})
						.then((count) => {
							if (count > 0) {
								throw new ValidationError(
									"Already exist a active extension",
									"ACCOMMODATION_AREADY_EXIST_ACTIVE_EXTENSION",
									{},
								);
							}
						});
				},
				"validateAcademicYear"
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}

					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "extension.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			admin_approve: [
				async function processApplicationBilling(ctx, res) {
					if (res[0].decision == "APPROVE") {
						await ctx.call("accommodation.applications.patch", {
							id: res[0].application_id,
							updated_end_date: res[0].end_date,
							end_date: res[0].end_date,
						});
						ctx.call("accommodation.billings.processExtension", {
							application_id: res[0].application_id,
							end_date: res[0].end_date,
						});
					}
					return res;
				},
			],
			create: [
				async function sendNotification(ctx, response) {
					const application = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_EXTENSION_SUBMITTED",
						user_id: application[0].user_id,
						user_data: {},
						data: { application: application },
						variables: {},
						external_uuid: null,
					});
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["ANALYSE", "CANCEL", "DISPATCH"] },
				extension: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						application_id: { type: "number", convert: true, optional: true },
						user_id: { type: "number", convert: true, optional: true },
						decision: {
							type: "enum",
							values: ["APPROVE", "REJECT"],
							optional: true,
						},
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const extension = await ctx.call("accommodation.extensions.get", { id: ctx.params.id });
				const stateMachine = extensionStateMachine.createStateMachine(extension[0].status, ctx);

				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.extension) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.extension.decision)
				) {
					throw new ValidationError(
						"Decision are required to to change status to dispatch",
						"ACCOMMODATION_EXTENSIONS_DECISION_NOT_FOUND",
						{},
					);
				}
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_EXTENSIONS_CHANGE_STATUS",
						{},
					);
				}
			},
		},

		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				extension: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						application_id: { type: "number", convert: true, optional: true },
						user_id: { type: "number", convert: true, optional: true },
						decision: {
							type: "enum",
							values: ["APPROVE", "REJECT"],
							optional: true,
						},
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const extension = await ctx.call("accommodation.extensions.get", { id: ctx.params.id });
				const stateMachine = extensionStateMachine.createStateMachine(extension[0].status, ctx);
				if (stateMachine.can(extension[0].decision)) {
					return stateMachine[extension[0].decision.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_EXTENSIONS_STATUS",
						{},
					);
				}
			},
		},

		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				extension: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						application_id: { type: "number", convert: true, optional: true },
						user_id: { type: "number", convert: true, optional: true },
						decision: {
							type: "enum",
							values: ["APPROVE", "REJECT"],
							optional: true,
						},
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const extension = await ctx.call("accommodation.extensions.get", { id: ctx.params.id });
				const stateMachine = extensionStateMachine.createStateMachine(extension[0].status, ctx);
				if (stateMachine.can("ADMIN_REJECT")) {
					return stateMachine["adminReject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_EXTENSIONS_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateAcademicYear(ctx) {
			const application = await ctx.call("accommodation.applications.get", {
				id: ctx.params.application_id,
				withRelated: false
			});
			if (application.length) {
				const currentAcademicDates = await ctx.call("configuration.academic_years.find", {
					query: {
						academic_year: application[0].academic_year
					}
				});
				if (currentAcademicDates.length && !moment(ctx.params.end_date, "YYYY-MM-DD").isBetween(moment(currentAcademicDates[0].start_date, "YYYY-MM-DD"), moment(currentAcademicDates[0].end_date, "YYYY-MM-DD"), null, "[]")) {
					throw new ValidationError(
						"Dates out of range defined in academic year",
						"ACCOMMODATION_DATES_OUT_OF_RANGE",
					);
				}
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
