"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.extra-changes",
	table: "extra_change",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "extra-changes")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_extra_change_id",
			"extra_id"
		],
		defaultWithRelateds: ["extra"],
		withRelateds: {
			extra(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.extras", "extra", "extra_id");
			},
			application_extra_change(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.applications-regime-changes", "application_extra_change", "application_extra_change_id");
			},

		},
		entityValidator: {
			application_extra_change_id: { type: "number", positive: true, integer: true, convert: true },
			extra_id: { type: "number", positive: true, integer: true, convert: true }
		},
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		save_extras: {
			params: {
				application_extra_change_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				extra_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_extra_change_id: ctx.params.application_extra_change_id,
				});
				this.clearCache();

				const entities = ctx.params.extra_ids.map((extra_id) => ({
					application_extra_change_id: ctx.params.application_extra_change_id,
					extra_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
