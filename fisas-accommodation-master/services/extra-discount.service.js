"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.extras-discount",
	table: "extra_discount",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "extras-discount")],

	/**
     * Settings
    */
	settings: {
		fields: [
			"id",
			"extra_id",
			"month",
			"year",
			"discount_value",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			extra_id: { type: "number", convert: true, integer: true, positive: true },
			month: { type: "number", convert: true },
			year: { type: "number", convert: true },
			discount_value: { type: "number", convert: true, max: 100 },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		}
	},
	hooks: {
		before: {
			create: [
				"checkIfMonthYearExists",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				"checkIfMonthYearExists",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
	},
	/**
     * Dependencies
     */
	dependencies: [],

	/**
     * Actions
     */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		save_discounts: {
			params: {
				extra_id: { type: "number", positive: true, integer: true, convert: true },
				discounts: {
					type: "array",
					item: {
						date: { type: "date", convert: true },
						discount_value: { type: "number", convert: true },
					}
				}
			},
			async handler(ctx) {
				if (ctx.params.discounts && Array.isArray(ctx.params.discounts)) {
					await this.adapter.removeMany({
						extra_id: ctx.params.extra_id,
					});
					this.clearCache();
					const entities = ctx.params.discounts.map((discount) => ({
						extra_id: ctx.params.extra_id,
						month: moment(discount.date).month() + 1,
						year: moment(discount.date).year(),
						discount_value: discount.discount_value,
					}));
					return this._insert(ctx, { entities });
				}
			}
		},
		extras_discounts: {
			cache: {
				keys: ["extra_id"],
			},
			params: {
				extra_id: { type: "number", convert: true },
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						extra_id: ctx.params.extra_id,
					},
				}).then((response) => {
					return ctx.call("accommodation.extras-discount.get", {
						withRelated: null,
						id: response.map((r) => r.id),
					});
				});
			},
		},
	},

	/**
     * Events
     */
	events: {

	},

	/**
     * Methods
     */
	methods: {
		async checkIfMonthYearExists(ctx, res) {
			this._find(ctx, {
				query: {
					month: ctx.params.month,
					year: ctx.params.year
				}
			}).then(response => {
				if (response.length) {
					throw new Errors.ValidationError(
						"Discount already exists for this month and year",
						"ACCOMMODATION_DISCOUNT_EXISTS",
					);
				}
				return res;
			});
		},
	},

	/**
     * Service created lifecycle event handler
     */
	created() {

	},

	/**
     * Service started lifecycle event handler
     */
	async started() {

	},

	/**
     * Service stopped lifecycle event handler
     */
	async stopped() {

	}
};
