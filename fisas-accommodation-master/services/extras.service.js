"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const QueueService = require("moleculer-bull");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.extras",
	table: "extra",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "extras"), QueueService(process.env.REDIS_QUEUE)],
	/**
	 * QUEUES
	 */
	queues: {
		"extras.update.reprocessBillings"(job) {
			this.logger.info("NEW JOB RECEIVED!");
			return this.updateProcessedBillings(job.data.id, job.data.reprocess_since)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch(err => {
					this.logger.info("ANTES DE MANDAR O REJECT");
					this.logger.info(err);
					return Promise.reject(err);
				});
		}
	},
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"product_code",
			"billing_name",
			"price",
			"vat_id",
			"visible",
			"active",
			"reprocess_since",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: ["translations", "vat", "discounts"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.extras_translations",
					"translations",
					"id",
					"extra_id",
				);
			},
			vat(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "vat", "vat_id");
			},
			discounts(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.extras-discount",
					"discounts",
					"id",
					"extra_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
			product_code: { type: "string" },
			billing_name: { type: "string" },
			price: { type: "number", positive: true, convert: true },
			vat_id: { type: "string", positive: true, integer: true, convert: true },
			visible: { type: "boolean" },
			active: { type: "boolean" },
			discounts: {
				type: "array",
				item: {
					date: { type: "date", convert: true },
					discount_value: { type: "number", convert: true, max: 100 },
				}
			},
			reprocess_since: { type: "date", convert: true, optional: true }
		},
	},
	hooks: {
		before: {
			create: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"isPossibleRemove"
			],
			list: [
				function addFilterByTranslations(ctx) {
					return addSearchRelation(ctx, ["name"], "accommodation.extras_translations", "extra_id",);
				},
			],
		},
		after: {
			create: ["saveTranslations", "saveDiscounts"],
			update: ["saveTranslations", "saveDiscounts",
				function updatePriceOnProcessedBillings(ctx, res) {
					this.createJob(
						"extras.update.reprocessBillings",
						{
							id: res[0].id,
							reprocess_since: ctx.params.reprocess_since
						},
						{
							attempts: 1,
							removeOnComplete: true,
							removeOnFail: true,
						},
					);
					return res;
				},],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);

				return this.adapter.db
					.transaction(async (trx) => {

						return this.adapter.db("extra_translation").transacting(trx).where("extra_id", id).del()
							.then(() => this.adapter.db("extra_discount").transacting(trx).where("extra_id", id).del())
							.then(() => this.adapter.db("extra").transacting(trx).where("id", id).del())
							.then(doc => {
								if (!doc)
									return Promise.reject(new Errors.EntityNotFoundError("extra", params.id));
								return this.transformDocuments(ctx, {}, doc)
									.then(json => this.entityChanged("removed", json, ctx).then(() => json));
							});
					});

			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"accommodation.extras.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveTranslations(ctx, res) {
			await ctx.call("accommodation.extras_translations.save_translations", {
				extra_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("accommodation.extras_translations.find", {
				query: {
					extra_id: res[0].id,
				},
			});

			return res;
		},

		async saveDiscounts(ctx, res) {
			if (ctx.params.discounts && Array.isArray(ctx.params.discounts)) {
				await ctx.call("accommodation.extras-discount.save_discounts", {
					extra_id: res[0].id,
					discounts: ctx.params.discounts,
				});
				res[0].discounts = await ctx.call("accommodation.extras-discount.extras_discounts", { extra_id: res[0].id });
			}
			return res;
		},

		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countApplication = await ctx.call("accommodation.application_extras.count", {
					query: {
						extra_id: ctx.params.id
					}
				});

				if (countApplication > 0) {
					throw new Errors.ValidationError("You have applications with this extra associated", "EXTRA_WITH_APPLICATIONS");
				}

				const countBillingsItems = await ctx.call("accommodation.billing_items.count", {
					query: {
						extra_id: ctx.params.id
					}
				});

				if (countBillingsItems > 0) {
					throw new Errors.ValidationError("You have billings with this extra associated", "EXTRA_WITH_BILLINGS");
				}
			}
		},
		async updateProcessedBillings(id) {
			let query = `
			select billing.id
			from application_extra as application_extra
			inner join application as application
			on application_extra.application_id  = application.id
			inner join billing as billing
			on billing.application_id = application.id
			inner join extra as extra 
			on extra.id = application_extra.extra_id 
			where extra.id = ? and  extra.active = true
			and billing.status in ('PROCESSED')
			group by billing.id`;
			this.adapter.raw(query, id).then(async (data) => {
				for (const row of data.rows) {
					try {
						await this.broker.call("accommodation.billings.reprocessApplicationBilling", { id: row.id });
					} catch (error) {
						this.logger.info(error);
					}
				}
			});
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
