const _ = require("lodash");
/**
 * Taxa de ocupação do seu hotel.
 * Para calcular:
 * Divida o número de camas reservados pelo número total de camas.
 */
async function occupation_rate(ctx, residence_id) {
	const occupation = await ctx.call("accommodation.residences.occuppation", {
		id: residence_id,
	});
	return occupation.occupationRate;
}

/**
 * Numero de camas ocupadas.
 * Para calcular:
 * Numero total de camas ocupada com candidatura em estado 'contracted'
 */
async function beds_occupation(ctx, residence_id) {
	return ctx.call("accommodation.residences.bedOccupancy", {
		id: residence_id,
	});
}

/**
 * Numero de camas ocupadas.
 * Para calcular:
 * Numero total de camas ocupada com candidatura em estado 'contracted'
 */
async function maximum_capacity(ctx, residence_id) {
	return ctx.call("accommodation.residences.maximumCapacity", {
		id: residence_id,
	});
}

/** RECEITA DIARIA
 * Total de ganho por dia considerando que os utilizadores vão pagar o valor mais baixo
 */
async function diary_receipt(ctx, residence_id) {
	return ctx.call("accommodation.residences.dailyReceipt", {
		id: residence_id,
	});
}

/**
 * ADR - Tarifa média diária.
 * Para calcular:
 * Para determinar a ADR do seu hotel, divida a receita obtida com os
 * seus quartos pela quantidade de quartos vendidos.
 * Por exemplo. 3850€/35 quartos vendidos numa noite = ADR de 110€
 * Calcular a sua ADR por um maior período de tempo é ligeiramente
 * mais complicado. Por exemplo, se quiser saber qual a sua ADR num
 * determinado período mensal, deve dividir a receita gerada pelos
 * seus quartos nesse período, pelo número total de quartos vendidos
 * durante o mesmo e o número de dias desse mês.
 */
async function adr(ctx, residence_id) {
	const occupation = await ctx.call("accommodation.residences.bedOccupancy", {
		id: residence_id,
	});
	if (occupation) {
		const receipt_day = await diary_receipt(ctx, residence_id);
		return _.round(receipt_day / occupation, 2);
	} else {
		return 0;
	}
}

/**
 * RevPAB - Receita por cama disponível.
 * Para calcular:
 * Basta multiplicar a sua tarifa média diária (ADR - para mais informação
 * consulte a pág. 2) pela sua taxa de ocupação.
 * Por exemplo, se o seu hotel tiver uma ocupação de 70% com uma ADR de
 * 100€, a sua RevPAR será de 70€.
 * Deverá usar a RevPar para determinar a melhor forma de maximizar
 * a receita gerada por cama. Se a RevPar do seu estabelecimento
 * aumentar, isto significa que a sua tarifa média por cama ou a sua taxa
 * de ocupação estão a aumentar - ou ambas!
 */
async function revpab(ctx, residence_id) {
	const adr_value = await adr(ctx, residence_id);
	if (adr_value) {
		const occup_rate = (await occupation_rate(ctx, residence_id)) / 100;
		return _.round(adr_value * occup_rate, 2);
	} else {
		return 0;
	}
}

/**
 * TrevPAB - Receita total por cama disponível.
 * Para calcular:
 * A TrevPAR é calculada dividindo a receita total pelo número total de
 * camas.
 * Logo, se a receita diária do seu hotel for de, por exemplo, 15 000€, e o
 * seu hotel tiver 110 camas, a TrevPAR seria de 136€.
 * Dado que a TrevPAR inclui todas as receitas do hotel e interliga-as com
 * o número de camas, podemos afirmar que oferece uma visão geral
 * melhor do que a RevPAR. No entanto, a TrevPAB não tem em conta
 * quaisquer custos incorridos ou a ocupação real do seu hotel.
 */
async function trevpab(ctx, residence_id) {
	const occupation = await ctx.call("accommodation.residences.occuppation", {
		id: residence_id,
	});
	if (occupation.maximumCapacity) {
		const receipt_day = await diary_receipt(ctx, residence_id);
		return _.round(receipt_day / occupation.maximumCapacity, 2);
	} else {
		return 0;
	}
}

module.exports = {
	revpab,
	trevpab,
	diary_receipt,
	maximum_capacity,
	occupation_rate,
	beds_occupation,
	adr,
};
