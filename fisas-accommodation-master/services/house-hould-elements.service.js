"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.house-hould-elements",
	table: "house_hould_element",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "house_hould_element")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "application_id", "kinship", "profession"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			application_id: { type: "number", integer: true, convert: true },
			kinship: { type: "string" },
			profession: { type: "string" },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_elements: {
			params: {
				application_id: { type: "number", integer: true, convert: true },
				elements: {
					type: "array",
					items: {
						type: "object",
						props: {
							kinship: { type: "string" },
							profession: { type: "string" },
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id,
				});
				let resp = [];
				for (const element of ctx.params.elements) {
					element.application_id = ctx.params.application_id;
					const element_created = await this._create(ctx, element);
					resp.push(element_created[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
