"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.application-income-statement-files",
	table: "application_income_statement_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "application-income-statement-files")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"file_id"
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.applications", "application", "application_id");
			},

		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			file_id: { type: "number", positive: true, integer: true, convert: true }
		},
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		save_statment_files: {
			params: {
				application_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				file_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id,
				});
				this.clearCache();

				const entities = ctx.params.file_ids.map((file_id) => ({
					application_id: ctx.params.application_id,
					file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
