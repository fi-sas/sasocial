"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const maintenanceStateMachine = require("./state-machines/maintenance_requests.machine");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.maintenance-requests",
	table: "maintenance_request",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"description",
			"local",
			"application_id",
			"file_id",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["application", "file", "history"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id", "id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.maintenance-request-history",
					"history",
					"id",
					"maintenance_request_id",
				);
			},
		},
		entityValidator: {
			description: { type: "string" },
			local: { type: "string" },
			application_id: { type: "number", convert: true },
			status: {
				type: "enum",
				values: ["SUBMITTED", "RESOLVING", "RESOLVED", "REJECTED", "CANCELLED"],
			},
			file_id: { type: "number", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}
					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "maintenance_request.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			create: [
				function notify(ctx, res) {
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_MAINTENANCE_REQUEST_SUBMITTED",
						user_id: res[0].application.user_id,
						user_data: {},
						data: {
							application: res[0].application,
						},
						variables: {},
						external_uuid: null,
					});
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["CANCEL", "RESOLVING", "REJECT", "RESOLVE"] },
				notes: { type: "string", optional: true },
				maintenance_request: {
					type: "object",
					props: {
						description: { type: "string", optional: true },
						local: { type: "string", optional: true },
						application_id: { type: "number", convert: true, optional: true },
						status: {
							type: "enum",
							values: ["SUBMITTED", "RESOLVING", "RESOLVED", "REJECTED", "CANCELLED"],
							optional: true,
						},
						file_id: { type: "number", convert: true, optional: true },
					},
					optional: true,
				},
			},
			visibility: "published",
			async handler(ctx) {
				const maintenance_request = await ctx.call("accommodation.maintenance-requests.get", {
					id: ctx.params.id,
				});
				const stateMachine = maintenanceStateMachine.createStateMachine(
					maintenance_request[0].status,
					ctx,
				);
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_MAINTENANCE_REQUEST_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		get_status: {
			rest: "GET /:id/status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				const maintenance_request = await ctx.call("accommodation.maintenance-requests.get", {
					id: ctx.params.id,
				});

				const stateMachine = maintenanceStateMachine.createStateMachine(
					maintenance_request[0].status,
					ctx,
				);
				return stateMachine.transitions();
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
