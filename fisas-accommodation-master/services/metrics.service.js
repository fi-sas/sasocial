"use strict";
const Knex = require("knex");
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {
	adr,
	beds_occupation,
	diary_receipt,
	maximum_capacity,
	occupation_rate,
	revpab,
	trevpab,
} = require("./helpers/metrics.helper");
const Cron = require("moleculer-cron");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.metrics",
	table: "metrics",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "metrics"), Cron],

	/*
	 * Crons
	 */
	crons: [
		{
			name: "saveDailyMetrics",
			cronTime: "0 0 * * *",
			onTick: function() {
				//this.logger.info("cron saveDaylyMetrics ticked!");
				this.getLocalService("accommodation.metrics")
					.actions.saveMetrics()
					.then(() => {});
			},
			runOnInit: function() {
				//this.logger.info("cron saveDaylyMetrics created!");
			},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"date",
			"year",
			"week",
			"occupation_rate",
			"maximum_capacity",
			"beds_occupation",
			"diary_receipt",
			"adr",
			"revpab",
			"trevpab",
			"residence_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			date: { type: "date" },
			occupation_rate: { type: "number", convert: true },
			maximum_capacity: { type: "number", convert: true },
			beds_occupation: { type: "number", convert: true },
			diary_receipt: { type: "number", convert: true },
			adr: { type: "number", convert: true },
			revpab: { type: "number", convert: true },
			trevpab: { type: "number", convert: true },
			residence_id: { type: "number", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			list: [
				function sanatizeParams(ctx) {
					const query = ctx.params.query || {};
					ctx.params.query = (qb) => {
						let select = [];

						switch (query.operation) {
							case "avg":
								qb.avg({
									occupation_rate: "occupation_rate",
									beds_occupation: "beds_occupation",
									maximum_capacity: "maximum_capacity",
									diary_receipt: "diary_receipt",
									adr: "adr",
									revpab: "revpab",
									trevpab: "trevpab",
									residence_id: "residence_id",
								});
								break;
							case "min":
								qb.min({
									occupation_rate: "occupation_rate",
									beds_occupation: "beds_occupation",
									maximum_capacity: "maximum_capacity",
									diary_receipt: "diary_receipt",
									adr: "adr",
									revpab: "revpab",
									trevpab: "trevpab",
									residence_id: "residence_id",
								});
								break;
							case "max":
								qb.max({
									occupation_rate: "occupation_rate",
									beds_occupation: "beds_occupation",
									maximum_capacity: "maximum_capacity",
									diary_receipt: "diary_receipt",
									adr: "adr",
									revpab: "revpab",
									trevpab: "trevpab",
									residence_id: "residence_id",
								});
								break;
							default:
								select.push(
									"occupation_rate",
									"beds_occupation",
									"maximum_capacity",
									"diary_receipt",
									"adr",
									"revpab",
									"trevpab",
									"residence_id",
								);
						}

						switch (query.period) {
							case "week":
								select.push(
									Knex.raw("date_part('year', \"date\"::date) as year"),
									Knex.raw(" date_part('week', \"date\"::date) AS week"),
								);
								qb.groupBy("year", "week");
								break;
							case "month":
								select.push(
									Knex.raw("date_part('year', \"date\"::date) as year"),
									Knex.raw("date_part('month', \"date\"::date) AS month"),
								);
								qb.groupBy("year", "month");
								break;
							default:
								qb.groupBy("date");
								select.push("date");
						}

						qb.select(select);

						if (query.start_date && query.end_date) {
							qb.whereBetween(Knex.raw("\"date\"::date"), [
								moment(query.start_date).format("YYYY-MM-DD"),
								moment(query.end_date).format("YYYY-MM-DD"),
							]);
						} else {
							qb.whereBetween(Knex.raw("\"date\"::date"), [
								moment(moment().subtract(10, "day").format("YYYY-MM-DD")).format("YYYY-MM-DD"),
								moment(moment().format("YYYY-MM-DD")).format("YYYY-MM-DD"),
							]);
						}

						if (query.residence_id) {
							qb.andWhere("residence_id", query.residence_id);
						}

						return qb;
					};
				},
			],
			create: [
				function sanatizeParams(ctx) {
					ctx.params.date = new Date();
				},
			],
			update: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			scope: "accommodation:billings:list",
		},
		saveMetrics: {
			handler(ctx) {
				return Promise.all([
					Promise.all([
						occupation_rate(ctx),
						beds_occupation(ctx),
						diary_receipt(ctx),
						adr(ctx),
						revpab(ctx),
						trevpab(ctx),
						maximum_capacity(ctx),
					]).then((result) =>
						ctx.call("accommodation.metrics.create", {
							date: new Date(),
							occupation_rate: result[0],
							maximum_capacity: result[6],
							beds_occupation: result[1],
							diary_receipt: result[2],
							adr: result[3],
							revpab: result[4],
							trevpab: result[5],
							residence_id: null,
						}),
					),
					ctx.call("accommodation.residences.find").then((residences) => {
						return Promise.all(
							residences.map((residence) =>
								Promise.all([
									occupation_rate(ctx, residence.id),
									beds_occupation(ctx, residence.id),
									diary_receipt(ctx, residence.id),
									adr(ctx, residence.id),
									revpab(ctx, residence.id),
									trevpab(ctx, residence.id),
									maximum_capacity(ctx, residence.id),
								]).then((result) =>
									ctx.call("accommodation.metrics.create", {
										date: new Date(),
										occupation_rate: result[0],
										maximum_capacity: result[6],
										beds_occupation: result[1],
										diary_receipt: result[2],
										adr: result[3],
										revpab: result[4],
										trevpab: result[5],
										residence_id: residence.id,
									}),
								),
							),
						);
					}),
				]);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
