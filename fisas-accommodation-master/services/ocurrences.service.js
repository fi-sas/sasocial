"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.occurrences",
	table: "occurrence",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"date",
			"subject",
			"description",
			"file_id",
			"created_by_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["created_by", "file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},

			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id", "id");
			},
			created_by(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"created_by",
					"created_by_id",
					"id",
					{},
					"name",
					false,
				);
			},
		},

		entityValidator: {
			application_id: { type: "number", convert: true },
			date: { type: "date", convert: true },
			subject: { type: "string" },
			description: { type: "string" },
			file_id: { type: "number", convert: true, optional: true },
			created_by_id: { type: "number" },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.created_by_id = ctx.meta.user.id;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function searchInApplication(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;

					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}
						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}
					if (ctx.params.query || applications_ids) {
						if (
							ctx.params.query.residence_id ||
							ctx.params.query.academic_year ||
							applications_ids
						) {
							const queryResidenceId = ctx.params.query.residence_id
								? ctx.params.query.residence_id
								: null;
							const queryAcademicYear = ctx.params.query.academic_year
								? ctx.params.query.academic_year
								: null;
							ctx.params.extraQuery = (qb) => {
								//qb.select("application.id");

								if (applications_ids)
									qb.andWhere(
										"application_id",
										"in",
										applications_ids.map((x) => x.id),
									);

								qb.innerJoin(
									"application",
									"application.id",
									"occurrence.application_id",
								);
								if (queryResidenceId)
									qb.where("application.assigned_residence_id", "in", queryResidenceId);
								if (queryAcademicYear)
									qb.where("application.academic_year", "=", queryAcademicYear);
							};

							delete ctx.params.query.residence_id;
							delete ctx.params.query.academic_year;
						}
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
