"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const Validator = require("fastest-validator");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.patrimony-categories",
	table: "patrimony_category",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "patrimony-categories")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"code",
			"description",
			"reference_value",
			"active",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			code: { type: "number", convert: true, positive: true },
			description: { type: "string" },
			active: { type: "boolean", convert: true },
			reference_value: { type: "number", convert: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateUniqueCode",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function validateRequestOrigin(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.active = true;
					}
				},
			],
			remove: [
				"validateIfCanRemove"
			]
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateUniqueCode(ctx) {
			const v = new Validator();
			const schema = { code: { type: "number", convert: true, positive: true }, };
			const check = v.compile(schema);
			const res = check({ code: ctx.params.code });
			if (res == true) {
				const count = await ctx.call("accommodation.patrimony-categories.count", { query: { code: ctx.params.code } });
				if (count > 0) {
					throw new Errors.ValidationError(
						"Already exists one patrimony catgory with provided code",
						"ACCOMMODATION_PATRIMONY_CATEGORY_CODE_ALREADY_EXISTS",
						{},
					);
				}
			}
		},
		async validateIfCanRemove(ctx) {
			const count = await ctx.call("accommodation.applications.count", { query: { patrimony_category_id: ctx.params.id } });
			if (count > 0) {
				throw new Errors.ValidationError(
					"There are applications associated with this patrimony category",
					"ACCOMMODATION_PATRIMONY_CATEGORY_REMOVE_ERROR",
					{},
				);
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
