"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.price_lines",
	table: "price_line",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "price-lines")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"period",
			"price",
			"vat_id",
			"regime_id",
			"typology_id",
			"tariff_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["vat", "tariff"],
		withRelateds: {
			vat(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "vat", "vat_id");
			},
			tariff(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.tariffs", "tariff", "tariff_id");
			},
		},
		entityValidator: {
			period: { type: "enum", values: ["DAY", "WEEK", "MONTH"] },
			price: { type: "number", convert: true },
			regime_id: {
				type: "number",
				positive: true,
				convert: true,
				optional: true,
			},
			typology_id: {
				type: "number",
				positive: true,
				convert: true,
				optional: true,
			},
			vat_id: { type: "number", positive: true, convert: true },
			tariff_id: { type: "number", positive: true, convert: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_priceLines: {
			params: {
				regime_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				typology_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				priceLines: {
					type: "array",
					item: {
						period: { type: "enum", values: ["DAY", "WEEK", "MONTH"] },
						price: { type: "number", positive: true, convert: true },
						vat_id: { type: "number", positive: true, convert: true },
						tariff_id: { type: "number", positive: true, convert: true },
					},
				},
			},
			async handler(ctx) {
				const query = {};

				if (ctx.params.typology_id) query.typology_id = ctx.params.typology_id;

				if (ctx.params.regime_id) query.regime_id = ctx.params.regime_id;

				await this.adapter.removeMany(query);
				this.clearCache();

				const entities = ctx.params.priceLines.map((priceLine) => ({
					...query,
					period: priceLine.period,
					price: priceLine.price,
					vat_id: priceLine.vat_id,
					tariff_id: priceLine.tariff_id,
					created_at: new Date(),
					updated_at: new Date(),
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
