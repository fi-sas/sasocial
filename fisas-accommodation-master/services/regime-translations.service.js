"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.regimes_translations",
	table: "regime_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "regime-translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"language_id",
			"regime_id",
			"name",
			"description"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			regime_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
			name: { type: "string" },
			description: { type: "string" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				regime_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
						description: { type: "string" },
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					regime_id: ctx.params.regime_id
				});
				this.clearCache();

				const entities = ctx.params.translations.map(translation => ({
					regime_id: ctx.params.regime_id,
					language_id: translation.language_id,
					name: translation.name,
					description: translation.description,
				}));
				return this._insert(ctx, { entities });
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
