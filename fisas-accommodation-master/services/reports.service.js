"use strict";
const moment = require("moment");
const Stream = require("stream");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.reports",
	mixins: [],

	/**
	 * Settings
	 */
	settings: {},
	hooks: {
		before: {},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		applicationReport: {
			visibility: "published",
			rest: "POST /application",
			params: {
				id: { type: "number", min: 0, optional: false, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("accommodation.applications.get", { id: ctx.params.id });
				application[0].extras = this.concatExtrasIntoString(application);
				const pt_translations = application[0].document_type
					? application[0].document_type.translations.find((x) => x.language_id == 3)
					: null;
				application[0].document_type = pt_translations ? pt_translations.description : "--";
				application[0].applicant_birth_date = moment(application[0].applicant_birth_date).format(
					"YYYY-MM-DD",
				);
				application[0].start_date = moment(application[0].start_date).format("YYYY-MM-DD");
				application[0].end_date = moment(application[0].end_date).format("YYYY-MM-DD");
				application[0].submission_date = moment(application[0].submission_date).format(
					"YYYY-MM-DD",
				);
				application[0].consent_date = moment(application[0].consent_date).format("YYYY-MM-DD");
				application[0].date = moment().format("YYYY-MM-DD");
				application[0].other_income_source = application[0].other_income_source
					? application[0].other_income_source
					: "--";
				application[0].decision = application[0].status === "contracted" ? application[0].decision : null;
				if (application[0].income_source) {
					for (let index = 0; index < application[0].income_source.length; index++) {
						switch (application[0].income_source[index]) {
							case "DEPENDENT_WORKER":
								application[0].income_source[index] = "Trabalho dependente";
								break;
							case "INDEPENDENT_WORK":
								application[0].income_source[index] = "Trabalho independente";
								break;
							case "PENSIONS":
								application[0].income_source[index] = "Pensão";
								break;
							case "UNEMPLOYMENT_ALLOWANCE":
								application[0].income_source[index] = "Subsídio de desemprego";
								break;
							case "RSI":
								application[0].income_source[index] = "RSI";
								break;
							case "OTHER":
								application[0].income_source[index] = "Outro";
								break;
							default:
								"Outros";
								break;
						}
					}
					application[0].income_source = application[0].income_source.join();
				}

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "ACCOMMODATION_APPLICATION_REPORT",
						options: {
							convertTo: "pdf",
						},
						data: {
							...application[0],
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		applicationsResults: {
			visibility: "published",
			rest: "POST /applications-results",
			params: {
				residence_id: { type: "number", min: 0, convert: true },
				application_phase_id: { type: "number", min: 0, convert: true },
			},
			async handler(ctx) {
				const residence = await ctx.call("accommodation.residences.get", {
					id: ctx.params.residence_id,
				});
				const application_phase = await ctx.call("accommodation.application-phases.get", {
					id: ctx.params.application_phase_id,
				});

				return ctx
					.call("accommodation.applications.find", {
						query: {
							residence_id: ctx.params.residence_id,
							application_phase_id: ctx.params.application_phase_id,
						},
						sort: "full_name",
					})
					.then((applications) => {
						const readable = new Stream.Readable();
						readable.push(
							JSON.stringify({
								key: "ALO_RES_CAN",
								options: {
									convertTo: "pdf",
								},
								data: {
									residence_name: residence[0].name,
									application_phase: application_phase[0].application_phase,
									academic_year: application_phase[0].academic_year,
									applications,
								},
							}),
						);
						// no more data
						readable.push(null);
						return ctx.call("reports.templates.printFromStream", readable);
					});
			},
		},
		applicationsRawData: {
			visibility: "published",
			rest: "POST /application-raw",
			params: {
				academic_year: { type: "string" },
				residence_id: { type: "number", positive: true, optional: true },
				application_phase_id: { type: "number", positive: true, optional: true },
			},
			async handler(ctx) {
				let query = {};
				if (ctx.params.application_phase_id) {
					query = {
						id: ctx.params.application_phase_id,
					};
				} else {
					query = {
						academic_year: ctx.params.academic_year,
					};
				}

				const application_phases = await ctx.call("accommodation.application-phases.find", {
					query,
				});

				const query1 = {
					application_phase_id: application_phases.map((ap) => ap.id),
					status: ["draft", "cancelled", "submitted", "analysed"],
				};

				const query2 = {
					application_phase_id: application_phases.map((ap) => ap.id),
					status: {
						nin: ["draft", "cancelled", "submitted", "analysed"],
					},
				};

				if (ctx.params.residence_id) {
					query1.residence_id = ctx.params.residence_id;
					query2.assigned_residence_id = ctx.params.residence_id;
				}

				const withRelated =
					"residence,assignedResidence,regime,course,liability_term_file,signed_contract_file,income_statement_files,history,whichResidence,whichRoom,room,extras,house_hould_elements,second_opinion_residence,organic_unit,document_type,preferred_typology,user,tariff,files,iban_changes";

				return Promise.all([
					ctx.call("accommodation.applications.find", {
						query: query1,
						withRelated,
					}),
					ctx.call("accommodation.applications.find", {
						query: query2,
						withRelated,
					}),
				]).then((data) => {
					const applications = [...data[0], ...data[1]];
					applications.forEach((app) => {
						if (app.regime)
							app.regime.name =
								app.regime && app.regime.translations.find((x) => (x.language_id = 3))
									? app.regime.translations.find((x) => (x.language_id = 3)).name
									: null;
						if (app.preferred_typology)
							app.preferred_typology.name = app.preferred_typology.translations.find(
								(x) => (x.language_id = 3),
							)
								? app.preferred_typology.translations.find((x) => (x.language_id = 3)).name
								: null;
						const extras_names = [];
						for (const extra of app.extras) {
							extras_names.push(extra.translations.find((x) => (x.language_id = 3)).name);
						}
						app.extras = extras_names.join(",");
						app.last_status_date = moment(app.history.reduce((a, b) => moment(a.updated_at).isAfter(moment(b.updated_at)) ? a : b).updated_at).format("YYYY-MM-DD");
					});
					const readable = new Stream.Readable();
					readable.push(
						JSON.stringify({
							key: "ALO_CAN_RAW",
							options: {
								convertTo: "xlsx",
							},
							data: {
								academic_year: ctx.params.academic_year,
								applications,
							},
						}),
					);
					// no more data
					readable.push(null);
					return ctx.call("reports.templates.printFromStream", readable);
				});
			},
		},
		clothingControlMap: {
			visibility: "published",
			rest: "POST /clothing-map",
			params: {
				residence_id: { type: "number", min: 0, convert: true },
			},
			async handler(ctx) {
				const rooms = await ctx.call("accommodation.rooms.find", {
					withRelated: "occupants",
					query: {
						residence_id: ctx.params.residence_id,
					},
				});
				const residence = await ctx.call("accommodation.residences.get", {
					withRelated: false,
					id: ctx.params.residence_id,
				});

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "ALO_CLOTHES_MAP",
						options: {
							convertTo: "pdf",
						},
						data: {
							residence: residence[0],
							rooms,
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		schollarshipApplicationsMap: {
			visibility: "published",
			rest: "POST /schollarship-applications-map",
			params: {
				application_phase_id: { type: "number", min: 0, convert: true },
				residence_id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return ctx
					.call("accommodation.application-phases.get", { id: ctx.params.application_phase_id })
					.then((application_phase) => {
						const query = {};
						query.application_phase_id = ctx.params.application_phase_id;
						if (ctx.params.residence_id) query.residence_id = ctx.params.residence_id;

						return ctx
							.call("accommodation.applications.find", {
								query,
								withRelated: "residence,assignedResidence,document_type,organic_unit",
							})
							.then((applications) => {
								for (const app of applications) {
									app.start_date = moment(app.start_date).format("YYYY-MM-DD");
									app.end_date = moment(app.end_date).format("YYYY-MM-DD");
									const pt_translations = app.document_type
										? app.document_type.translations.find((x) => x.language_id == 3)
										: null;
									app.document_type = pt_translations ? pt_translations.description : "--";
								}
								const readable = new Stream.Readable();
								readable.push(
									JSON.stringify({
										key: "ACCOMMODATION_APPLICATION_SCHOOLARSHIP_REPORT",
										options: {
											convertTo: "xlsx",
										},
										data: {
											applications,
										},
									}),
								);
								// no more data
								readable.push(null);
								return ctx.call("reports.templates.printFromStream", readable);
							});
					});
			},
		},
		regimeApplicationsMap: {
			visibility: "published",
			rest: "POST /regime-applications-map",
			params: {
				application_phase_id: { type: "number", min: 0, convert: true },
				residence_id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				return ctx
					.call("accommodation.application-phases.get", { id: ctx.params.application_phase_id })
					.then((application_phase) => {
						const query = {};
						query.application_phase_id = ctx.params.application_phase_id;
						if (ctx.params.residence_id) query.residence_id = ctx.params.residence_id;

						return ctx
							.call("accommodation.applications.find", {
								query,
								withRelated: "currentRegime,document_type"
							})
							.then((applications) => {
								for (const app of applications) {
									app.start_date = moment(app.start_date).format("YYYY-MM-DD");
									app.end_date = moment(app.end_date).format("YYYY-MM-DD");
									const regime = app.currentRegime.translations.find((x) => x.language_id == 3);
									app.regime = regime ? regime.name : "--";
									const pt_translations = app.document_type
										? app.document_type.translations.find((x) => x.language_id == 3)
										: null;
									app.document_type = pt_translations ? pt_translations.description : "--";
								}
								const readable = new Stream.Readable();
								readable.push(
									JSON.stringify({
										key: "ACCOMMODATION_APPLICATION_REGIME_REPORT",
										options: {
											convertTo: "xlsx",
										},
										data: {
											applications,
										},
									}),
								);
								// no more data
								readable.push(null);
								return ctx.call("reports.templates.printFromStream", readable);
							});
					});
			},
		},
		extrasApplicationsMap: {
			visibility: "published",
			rest: "POST /extras-applications-map",
			params: {
				application_phase_id: { type: "number", min: 0, convert: true },
				residence_id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				return ctx
					.call("accommodation.application-phases.get", { id: ctx.params.application_phase_id })
					.then((application_phase) => {
						const query = {};
						query.application_phase_id = ctx.params.application_phase_id;
						if (ctx.params.residence_id) query.residence_id = ctx.params.residence_id;

						return ctx
							.call("accommodation.applications.find", {
								query,
							})
							.then((applications) => {
								for (const app of applications) {
									app.start_date = moment(app.start_date).format("YYYY-MM-DD");
									app.end_date = moment(app.end_date).format("YYYY-MM-DD");
									app.extras = app.extras.length > 0;
									const pt_translations = app.document_type
										? app.document_type.translations.find((x) => x.language_id == 3)
										: null;
									app.document_type = pt_translations ? pt_translations.description : "--";
								}

								const readable = new Stream.Readable();
								readable.push(
									JSON.stringify({
										key: "ACCOMMODATION_APPLICATION_EXTRAS_REPORT",
										options: {
											convertTo: "xlsx",
										},
										data: {
											applications,
										},
									}),
								);
								// no more data
								readable.push(null);
								return ctx.call("reports.templates.printFromStream", readable);
							});
					});
			},
		},
		generateSepaFile: {
			visibility: "published",
			rest: "POST /generate-sepa-file",
			params: {
				billing_ids: { type: "array", items: "number", optional: true },
				month: { type: "number", positive: true, convert: true, optional: false },
				year: { type: "number", positive: true, convert: true, optional: false },
			},
			async handler(ctx) {
				const params = ctx.params;

				this.logger.info();
				this.logger.info(" #### reports.generateSepaFile ctx.params: ");
				this.logger.info(ctx.params);

				return ctx.call("accommodation.billings.generateSepaFile", params);
			},
		},

		tariffsApplicationsMap: {
			visibility: "published",
			rest: "POST /tariffs-applications-map",
			params: {
				residence_id: { type: "number", positive: true, optional: true },
				academic_year: { type: "string", optional: true },
				tariff_id: { type: "number", min: 0, convert: true, optional: true },
				month: { type: "number", positive: true, convert: true, optional: true },
				year: { type: "number", positive: true, convert: true, optional: true },
			},
			async handler(ctx) {
				let queryApplications = {};
				let queryBillings = {};

				if (ctx.params.academic_year) {
					queryApplications.academic_year = ctx.params.academic_year;
				}
				if (ctx.params.residence_id) {
					queryApplications.residence_id = ctx.params.residence_id;
					queryApplications.assigned_residence_id = ctx.params.residence_id;
				}

				if (ctx.params.tariff_id) {
					queryBillings.tariff_id = ctx.params.tariff_id;
				}
				if (ctx.params.month && ctx.params.year) {
					queryBillings.year = ctx.params.year;
					queryBillings.month = ctx.params.month;
				}
				const withRelated = "residence,assignedResidence,user";

				return ctx.call("accommodation.applications.find", {
					query: queryApplications,
					withRelated
				}).then(async applicationsResponse => {
					const allTarifs = await ctx.call("accommodation.tariffs.list", {});
					const applications = [];
					for (let application of applicationsResponse) {
						queryBillings.application_id = application.id;
						const tariffBilling = await ctx.call("accommodation.billings.find", {
							query: queryBillings,
							withRelated: false
						});

						if (tariffBilling.length) {
							let tariff = null;
							if (ctx.params.month && ctx.params.year && tariffBilling.length) {
								tariff = allTarifs.rows.find(t => t.id === tariffBilling[0].tariff_id);
							} else if (ctx.params.tariff_id) {
								tariff = allTarifs.rows.find(t => t.id === ctx.params.tariff_id);
							} else if (!ctx.params.month && !ctx.params.year && tariffBilling.length) {
								const lastTariff = tariffBilling.reduce((a, b) => (moment(a.billing_start_date).isAfter(moment(b.billing_start_date)) ? a : b));
								tariff = lastTariff ? allTarifs.rows.find(t => t.id === lastTariff.tariff_id) : null;
							} else {
								tariff = application.tariff_id ? allTarifs.rows.find(t => t.id === application.tariff_id) : null;
							}
							application.tariff = tariff ? tariff : "";
							application.month = ctx.params.month ? new Date(ctx.params.year, ctx.params.month - 1, 1).toLocaleString("default", { month: "long" }) : "";
							applications.push(application);
						}

					}

					const readable = new Stream.Readable();
					readable.push(
						JSON.stringify({
							key: "ACCOMMODATION_APPLICATION_TARIFFS_REPORT",
							options: {
								convertTo: "xlsx",
							},
							data: {
								applications
							},
						}),
					);
					// no more data
					readable.push(null);
					return ctx.call("reports.templates.printFromStream", readable);
				});

			}
		},

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		concatExtrasIntoString(application) {
			const extras = [];
			if (application[0].extras) {
				for (const extra of application[0].extras) {
					const pt = extra.translations.find((t) => t.language_id == 3);
					if (pt) {
						extras.push(pt.name);
					}
				}
			}

			return extras.length == 0 ? "--" : extras.join(", ");
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
