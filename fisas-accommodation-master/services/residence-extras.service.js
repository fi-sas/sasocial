"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.residence-extras",
	table: "residence_extra",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "residence-extras")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"residence_id",
			"extra_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["extra"],
		withRelateds: {
			residence(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.residences", "residence", "residence_id");
			},
			extra(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.extras", "extra", "extra_id");
			},
		},
		entityValidator: {
			residence_id: { type: "number", convert: true, integer: true, positive: true },
			extra_id: { type: "number", convert: true, integer: true, positive: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		save_extras_of_residences: {
			params: {
				residence_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				extra_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					residence_id: ctx.params.residence_id,
				});
				this.clearCache();
				const entities = ctx.params.extra_ids.map((extra_id) => ({
					residence_id: ctx.params.residence_id,
					extra_id,
					created_at: new Date(),
					updated_at: new Date()
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
