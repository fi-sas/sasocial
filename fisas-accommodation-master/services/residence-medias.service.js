"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.residences_medias",
	table: "residence_media",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "residences-medias")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "residence_id", "file_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			residence_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			file_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		medias_of_residence: {
			cache: {
				keys: ["residence_id"],
				ttl: 60,
			},
			visibility: "public",
			rest: {
				path: "GET /residences/:residence_id/medias",
			},
			params: {
				residence_id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						residence_id: ctx.params.residence_id,
					},
				}).then((res) => {
					return ctx.call("media.files.get", {
						id: res.map((r) => r.file_id),
					});
				});
			},
		},
		save_media_of_residences: {
			params: {
				residence_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				media_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					residence_id: ctx.params.residence_id,
				});
				this.clearCache();

				const entities = ctx.params.media_ids.map((file_id) => ({
					residence_id: ctx.params.residence_id,
					file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
