"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.residence_reponsibles",
	table: "residence_responsible",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "residence-reponsibles")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"residence_id",
			"user_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			residence_id: { type: "number", positive: true },
			user_id: { type: "number", positive: true }
		}
	},
	hooks: {
		before: {
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		responsibles_of_residence: {
			cache: {
				keys: ["residence_id"],
			},
			rest: {
				path: "GET /residences/:residence_id/responsibles"
			},
			params: {
				residence_id: {
					type: "number",
					convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						residence_id: ctx.params.residence_id
					}
				}).then(res => {
					return ctx.call("authorization.users.get", {
						withRelated: null,
						id: res.map(r => r.user_id)
					});
				});
			}
		},
		save_responsibles_of_residences: {
			params: {
				residence_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				residence_responsible_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					residence_id: ctx.params.residence_id,
				});
				this.clearCache();

				const entities = ctx.params.residence_responsible_ids.map((user_id) => ({
					residence_id: ctx.params.residence_id,
					user_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
