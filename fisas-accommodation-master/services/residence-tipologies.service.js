"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.residence_tipologies",
	table: "residence_typology",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "residence-tipologies")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"residence_id",
			"typology_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			residence_id: { type: "number", positive: true },
			typology_id: { type: "number", positive: true }
		}
	},
	hooks: {
		before: {
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		typologies_of_residence: {
			cache: {
				keys: ["residence_id"],
			},
			rest: {
				path: "GET /residences/:residence_id/typologies"
			},
			params: {
				residence_id: {
					type: "number",
					convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						residence_id: ctx.params.residence_id
					}
				}).then(res => {
					return ctx.call("accommodation.typologies.get", {
						id: res.map(r => r.typology_id)
					});
				});
			}
		},
		save_typologies_of_residences: {
			params: {
				residence_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				typology_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					residence_id: ctx.params.residence_id,
				});
				this.clearCache();

				const entities = ctx.params.typology_ids.map((typology_id) => ({
					residence_id: ctx.params.residence_id,
					typology_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
