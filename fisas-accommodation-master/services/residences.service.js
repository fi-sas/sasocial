"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { maximum_capacity, beds_occupation } = require("./helpers/metrics.helper");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.residences",
	table: "residence",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "residences")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"address",
			"city",
			"postal_code",
			"phone_1",
			"phone_2",
			"email",
			"building_id",
			"regulation_file_id",
			"contract_file_id",
			"charge_deposit",
			"available_for_application",
			"available_for_assign",
			"visible_for_users",
			"active",
			"current_account_id",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [
			"typologies",
			"services",
			"mediaIds",
			"regulationFile",
			"residenceResponsibleIds",
			"wingResponsibleIds",
			"kitchenResponsibleIds",
			"regimes",
			"extras",
			"current_account",
		],
		withRelateds: {
			typologies(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.residence_tipologies.typologies_of_residence", {
								residence_id: doc.id,
							})
							.then((res) => {
								doc.typologies = res;
							});
					}),
				);
			},
			services(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.residence_services.services_of_residence", {
								residence_id: doc.id,
							})
							.then((res) => {
								doc.services = res;
							});
					}),
				);
			},
			residenceResponsibleIds(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.residence_reponsibles.responsibles_of_residence", {
								residence_id: doc.id,
							})
							.then((res) => {
								doc.residenceResponsibleIds = res;
							});
					}),
				);
			},
			wingResponsibleIds(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.residence_wing_reponsibles.wing_responsibles_of_residence", {
								residence_id: doc.id,
							})
							.then((res) => {
								doc.wingResponsibleIds = res;
							});
					}),
				);
			},
			kitchenResponsibleIds(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call(
								"accommodation.residence_kitchen_reponsibles.kitchen_responsibles_of_residence",
								{
									residence_id: doc.id,
								},
							)
							.then((res) => {
								doc.kitchenResponsibleIds = res;
							});
					}),
				);
			},
			mediaIds(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.residences_medias.medias_of_residence", {
								residence_id: doc.id,
							})
							.then((res) => {
								doc.mediaIds = res;
							});
					}),
				);
			},
			regulationFile(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "regulationFile", "regulation_file_id");
			},
			contractFile(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "contractFile", "contract_file_id");
			},
			building(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.buildings", "building", "building_id");
			},
			regimes(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.residence-regimes",
					"regimes",
					"id",
					"residence_id",
				);
			},
			extras(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "accommodation.residence-extras", "extras", "id", "residence_id");
			},
			current_account(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"current_account.accounts",
					"current_account",
					"current_account_id",
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			address: { type: "string" },
			city: { type: "string" },
			postal_code: { type: "string" },
			phone_1: { type: "string" },
			phone_2: { type: "string" },
			email: { type: "email" },
			building_id: { type: "number" },
			regulation_file_id: { type: "number" },
			contract_file_id: { type: "number" },
			charge_deposit: { type: "boolean" },
			active: { type: "boolean" },
			media_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
			},
			regime_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				min: 1,
			},
			extra_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				optional: true,
			},
			available_for_application: { type: "boolean", convert: true },
			available_for_assign: { type: "boolean", convert: true },
			visible_for_users: { type: "boolean", convert: true },
			current_account_id: { type: "number" },
		},
	},
	hooks: {
		before: {
			create: [
				"validations",
				"validateResidenceRegimes",
				"validateResidenceExtras",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validations",
				"validateResidenceRegimes",
				"validateResidenceExtras",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validations",
				"validateResidenceRegimes",
				"validateResidenceExtras",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["isPossibleRemove"],
			list: [
				async function checkResidencePermissions(ctx) {
					if (ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						const user = await ctx.call("authorization.scopes.userHasScope", { scope: "accommodation:is_residence_admnistrator" });
						if (!user) {
							const residences_ids = await ctx.call("accommodation.residence_reponsibles.find", {
								query: {
									user_id: ctx.meta.user.id
								},
								withRelated: false,
							});
							ctx.params.query.id = residences_ids.map((x) => x.residence_id);
						}
					}
				},
			]
		},
		after: {
			create: [
				"saveMedias",
				"saveServices",
				"saveTypologies",
				"saveResponsibles",
				"saveWingResponsibles",
				"saveKitchenResponsibles",
				"saveRegimes",
				"saveExtras",
			],
			update: [
				"saveMedias",
				"saveServices",
				"saveTypologies",
				"saveResponsibles",
				"saveWingResponsibles",
				"saveKitchenResponsibles",
				"saveRegimes",
				"saveExtras",
			],
			patch: [
				"saveMedias",
				"saveServices",
				"saveTypologies",
				"saveResponsibles",
				"saveWingResponsibles",
				"saveKitchenResponsibles",
				"saveRegimes",
				"saveExtras",
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" },
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);

				return this.adapter.db.transaction(async (trx) => {
					return this.adapter
						.db("residence_media")
						.transacting(trx)
						.where("residence_id", id)
						.del()
						.then(() =>
							this.adapter.db("residence_regime").transacting(trx).where("residence_id", id).del(),
						)
						.then(() =>
							this.adapter.db("residence_extra").transacting(trx).where("residence_id", id).del(),
						)
						.then(() =>
							this.adapter
								.db("residence_typology")
								.transacting(trx)
								.where("residence_id", id)
								.del(),
						)
						.then(() =>
							this.adapter.db("residence_service").transacting(trx).where("residence_id", id).del(),
						)
						.then(() =>
							this.adapter
								.db("residence_responsible")
								.transacting(trx)
								.where("residence_id", id)
								.del(),
						)
						.then(() =>
							this.adapter
								.db("residence_wing_responsible")
								.transacting(trx)
								.where("residence_id", id)
								.del(),
						)
						.then(() =>
							this.adapter
								.db("residence_kitchen_responsible")
								.transacting(trx)
								.where("residence_id", id)
								.del(),
						)
						.then(() => this.adapter.db("metrics").transacting(trx).where("residence_id", id).del())
						.then(() => this.adapter.db("residence").transacting(trx).where("id", id).del())
						.then((doc) => {
							if (!doc)
								return Promise.reject(new Errors.EntityNotFoundError("residence", params.id));
							return this.transformDocuments(ctx, {}, doc).then((json) =>
								this.entityChanged("removed", json, ctx).then(() => json),
							);
						});
				});
			},
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		occupation_map: {
			rest: "GET /occupation-map/:id",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				academic_year: { type: "string", optional: true },
			},
			async handler(ctx) {
				await this._get(ctx, { id: ctx.params.id });
				let queryAcademic_year = ctx.params.academic_year ? `AND app.academic_year = '${ctx.params.academic_year}' ` : "";

				const active_applications = await ctx.call("accommodation.applications.count", {
					query: {
						academic_year: ctx.params.academic_year,
						status: "contracted",
						assigned_residence_id: ctx.params.id,
					},
				});

				const absences = await this.adapter.db.raw(
					`select count(*) from absence a
				join application app on app.id=a.application_id
				where app.assigned_residence_id=? and
				app.status='contracted' ${queryAcademic_year};`,
					[ctx.params.id],
				);

				const bookings = await ctx.call("accommodation.bookings.count", {
					query: {
						pretended_residence_id: ctx.params.id,
					},
				});

				const withdrawals = await this.adapter.db.raw(
					`select  count(*) from withdrawal w
				join application app on app.id=w.application_id
				where app.assigned_residence_id=? and
				app.status='contracted' ${queryAcademic_year};`,
					[ctx.params.id],
				);

				const rooms = await ctx.call("accommodation.rooms.count", {
					query: {
						residence_id: ctx.params.id,
						active: true,
					},
				});
				return {
					absences: absences.rows[0].count,
					bookings: bookings,
					withdrawals: withdrawals.rows[0].count,
					rooms: rooms,
					applications: active_applications,
				};
			},
		},
		/**/
		rooms_map_v2: {
			rest: "GET /rooms-map",
			visibility: "published",
			params: {
				residence_id: { type: "number", min: 0, convert: true, optional: true },
				academic_year: { type: "string", optional: true },
			},
			async handler(ctx) {
				let residences = [];
				let queryAcademic_year = ctx.params.academic_year ? `AND ap.academic_year = '${ctx.params.academic_year}' ` : "";
				let query = ctx.params.residence_id
					? `
				select re.id as residence_id, ro.id as room_id, ro.name, sum(ty.max_occupants_number) as max_occupants_number
				FROM residence AS re
				INNER JOIN room AS ro
					ON ro.residence_id = re.id
				INNER JOIN typology AS ty
					ON ty.id = ro.typology_id
				where re.id = ${ctx.params.residence_id} and re.active = true
				group by re.id, ro.id, ro.name
				order by unaccent(ro.name) ASC`
					: `select re.id as residence_id, re.name, sum(ty.max_occupants_number) as max_occupants_number
				FROM residence AS re
				INNER JOIN room AS ro
					ON ro.residence_id = re.id
				INNER JOIN typology AS ty
					ON ty.id = ro.typology_id
				WHERE re.active = true
				group by re.id`;
				return this.adapter.raw(query).then(async (data) => {
					residences = data.rows;
					for (let residence of residences) {
						residence.occupants = 0;
						residence.available = residence.max_occupants_number - residence.occupants;
					}
					if (ctx.params.residence_id) {
						let query2 = `
                        SELECT ap.assigned_residence_id, ap.room_id, COALESCE(count(ap.id), 0) as totalOccupied
                        FROM application AS ap
						WHERE(ap.status in ('assigned', 'confirmed', 'contracted') OR(ap.status = 'pending' AND ap.decision = 'ASSIGN')) ${queryAcademic_year}
                        AND ap.assigned_residence_id = ${ctx.params.residence_id} AND ap.room_id = ANY(?) GROUP BY ap.assigned_residence_id, ap.room_id`;
						await this.adapter.raw(query2, [data.rows.map((r) => r.room_id)]).then((infos) => {
							for (const info of infos.rows) {
								const infoResi = residences.find(
									(r) =>
										r.residence_id === info.assigned_residence_id && r.room_id === info.room_id,
								);
								if (infoResi) {
									infoResi.occupants = info.totaloccupied || 0;
									infoResi.available = infoResi.max_occupants_number - infoResi.occupants;
								}
							}
						});
					} else {
						let query2 = `
                        SELECT ap.assigned_residence_id, COALESCE(count(ap.id), 0) as totalOccupied
                        FROM application AS ap
						WHERE(ap.status in ('assigned', 'confirmed', 'contracted') OR(ap.status = 'pending' AND ap.decision = 'ASSIGN')) ${queryAcademic_year}
                        AND ap.assigned_residence_id = ANY(?) GROUP BY ap.assigned_residence_id`;
						await this.adapter.raw(query2, [data.rows.map((r) => r.residence_id)]).then((infos) => {
							for (const info of infos.rows) {
								const infoResi = residences.find(
									(r) => r.residence_id === info.assigned_residence_id,
								);
								if (infoResi) {
									infoResi.occupants = info.totaloccupied || 0;
									infoResi.available = infoResi.max_occupants_number - infoResi.occupants;
								}
							}
						});
					}
					return residences;
				});
			},
		},
		occuppation_v2: {
			rest: "GET /occupation",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				academic_year: { type: "string", optional: true },
			},
			async handler(ctx) {
				let query1 = ctx.params.id ? `AND re.id = ${ctx.params.id} ` : "";
				let query2 = ctx.params.id ? `AND ap.assigned_residence_id = ${ctx.params.id} ` : "";
				let queryAcademic_year = ctx.params.academic_year ? `AND ap.academic_year = '${ctx.params.academic_year}' ` : "";

				const maximumCapacity = `SELECT COALESCE(sum(ty.max_occupants_number), 0) as maximumCapacity FROM residence AS re INNER JOIN room AS ro ON ro.residence_id = re.id INNER JOIN typology AS ty ON ty.id = ro.typology_id WHERE ro.active = true AND re.active = true  ${query1} `;
				const totalOccupied = `SELECT COALESCE(count(ap.id), 0) as totalOccupied FROM application AS ap  INNER JOIN residence AS re ON re.id = ap.assigned_residence_id WHERE(ap.status in ('assigned', 'confirmed', 'contracted') OR(ap.status = 'pending' AND ap.decision = 'ASSIGN')) ${queryAcademic_year} AND re.active = true ${query2} `;
				return Promise.all([
					this.adapter.raw(maximumCapacity),
					this.adapter.raw(totalOccupied),
				]).then((result) => {
					const data = {
						...result[0].rows[0],
						...result[1].rows[0],
					};
					data.occupationRate = data.maximumcapacity
						? parseFloat(((data.totaloccupied / data.maximumcapacity) * 100).toFixed(2))
						: 0;
					data.totalAvailable = data.maximumcapacity - data.totaloccupied;
					return data;
				});
			},
		},
		typology_map_v2: {
			rest: "GET /typology-map",
			visibility: "published",
			params: {
				residence_id: { type: "number", min: 0, convert: true, optional: true },
				academic_year: { type: "string", optional: true },
			},
			async handler(ctx) {
				let typologies = [];
				let queryResidence = ctx.params.residence_id
					? `and ro.residence_id = ${ctx.params.residence_id} `
					: "";
				let queryAcademic_year = ctx.params.academic_year ? `AND ap.academic_year = '${ctx.params.academic_year}' ` : "";
				let query = `
					select ty.id, tyt."name", sum(ty.max_occupants_number) as max_occupants_number
					from
					room as ro
					inner join typology as ty on
						ty.id = ro.typology_id
					inner join residence as re on
						re.id = ro.residence_id
					left join typology_translation as tyt
						on tyt.typology_id = ty.id and tyt.language_id = 3
					where
						ro.active = true and re.active = true
						${queryResidence}
					group by
						ty.id, tyt."name"`;
				return this.adapter.raw(query).then(async (data) => {
					typologies = data.rows;
					for (let typology of typologies) {
						typology.occupants = 0;
						typology.available = typology.max_occupants_number - typology.occupants;
					}
					let query2 = `
						SELECT ro.typology_id, COALESCE(count(ap.id), 0) as totalOccupied
                        FROM application AS ap
						inner join room as ro
						on ro.id = ap.room_id
						inner join residence as re
						on re.id = ap.assigned_residence_id
						WHERE(ap.status in ('assigned', 'confirmed', 'contracted') OR(ap.status = 'pending' AND ap.decision = 'ASSIGN')) ${queryAcademic_year}
                        AND ro.typology_id = ANY(?) AND re.active = true
						${queryResidence}
                        GROUP BY ro.typology_id`;
					await this.adapter.raw(query2, [data.rows.map((r) => r.id)]).then((infos) => {
						for (const info of infos.rows) {
							const infoTypologies = typologies.find((r) => r.id === info.typology_id);
							if (infoTypologies) {
								infoTypologies.occupants = info.totaloccupied || 0;
								infoTypologies.available =
									infoTypologies.max_occupants_number - infoTypologies.occupants;
							}
						}
					});
					return typologies;
				});
			},
		},

		free_rooms: {
			rest: "GET /:id/free-rooms",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				application_id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				let rooms = [];
				await ctx.call("accommodation.residences.get", { id: ctx.params.id });
				let application = null;
				if (ctx.params.application_id) {
					application = await ctx.call("accommodation.applications.get", {
						id: ctx.params.application_id,
						withRelated: false,
					});
				}
				if (application.length) {
					const roomsMapQuery = `select
					ro.id,
						ro.name,
						ty.max_occupants_number,
						COUNT(ap.id) as occupants,
						ro.active
					 from room as ro
					 inner join typology as ty on ty.id = ro.typology_id
					 inner join residence as re
					 on re.id =  ro.residence_id
					 left join application as ap on ap.room_id = ro.id and ap.status in ('contracted') 
					 	and ap.decision in ('ASSIGN')
						 and ap.academic_year = '${application[0].academic_year}'
					 where ro.residence_id = ${ctx.params.id} and
						ro.active = true and re.active = true
					 group by ro.id, ty.max_occupants_number
					 HAVING COUNT(ap.id) < ty.max_occupants_number; `;

					return this.adapter.raw(roomsMapQuery).then(async (data) => {
						rooms = data.rows;
						if (application && application[0].residence_id === ctx.params.id) {
							rooms.push({ id: application[0].room_id });
						}

						return ctx.call("accommodation.rooms.get", {
							id: rooms.map((r) => r.id),
							academic_year: application[0].academic_year,
							withRelated: "occupants,typology",
						});
					});
				}
			},
		},
		bedOccupancy: {
			visibility: "protected",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				if (ctx.params.id) {
					await ctx.call("accommodation.residences.get", ctx.params);
				}

				const totalOccupied =
					"SELECT COALESCE(count(ap.id), 0) as \"totalOccupied\" FROM application AS ap WHERE (ap.status in ('assigned', 'confirmed', 'contracted') OR (ap.status = 'pending' AND ap.decision = 'ASSIGN'))" +
					(ctx.params.id ? ` AND ap.assigned_residence_id = ${ctx.params.id} ` : "");
				return this.adapter.raw(totalOccupied).then((data) => {
					return data.rows[0].totalOccupied;
				});
			},
		},
		maximumCapacity: {
			visibility: "protected",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				if (ctx.params.id) {
					await ctx.call("accommodation.residences.get", ctx.params);
				}

				const maximumCapacity =
					"SELECT COALESCE(sum(ty.max_occupants_number), 0) as \"maximumCapacity\" FROM residence AS re INNER JOIN room AS ro ON ro.residence_id = re.id INNER JOIN typology AS ty ON ty.id = ro.typology_id " +
					(ctx.params.id ? `WHERE re.id = ${ctx.params.id}; ` : "");
				return this.adapter.raw(maximumCapacity).then((data) => {
					return data.rows[0].maximumCapacity;
				});
			},
		},
		dailyReceipt: {
			visibility: "protected",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				if (ctx.params.id) {
					await ctx.call("accommodation.residences.get", ctx.params);
				}

				const adr = `SELECT coalesce(SUM(pl.price) / DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH':: interval - '1 DAY':: interval), 0) as adr FROM application as a
				INNER JOIN room as r ON a.room_id = r.id
				INNER JOIN typology as t ON r.typology_id = t.id
				INNER JOIN price_line as pl ON pl.typology_id = t.id and pl.period = 'MONTH'
				WHERE a.status = 'contracted' ${ctx.params.id ? ` AND a.assigned_residence_id = ${ctx.params.id}` : ""} `;
				return this.adapter.raw(adr).then((data) => {
					return data.rows[0].adr;
				});
			},
		},

		/*	getEntityByUser: {
				rest: "GET /user",
				scope: "accommodation:residences",
				visibility: "published",
				async handler(ctx) {
					const hasPermission = await ctx.call("authorization.scopes.userHasScope", {
						scope: "accommodation:is_residence_admnistrator",
					});
					this.logger.info("--> Scope result: ", hasPermission);
					if (hasPermission) {
						return ctx.call("accommodation.residences.find", {});
					} else {
						const residences_ids = await ctx.call("accommodation.residence_reponsibles.find", {
							query: {
								user_id: ctx.meta.user.id
							},
							withRelated: false,
						});
						return this._find(ctx, {
							query: {
								id: residences_ids.map((al) => al.id),
							},
						});
					}
				},
			},
					async function checkResidencePermissions(ctx) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						const user = await ctx.call("authorization.scopes.userHasScope", { scope: "accommodation:is_residence_admnistrator" });
						if (!user) {
							const residences_ids = await ctx.call("accommodation.residence_reponsibles.find", {
								query: {
									user_id: ctx.meta.user.id
								},
								withRelated: false,
							});
							ctx.params.query.id = residences_ids.map((x) => x.id);
						}
					},
			*/
	},

	/**
	 * Events
	 */
	events: {
		"accommodation.typologies.*"() {
			this.clearCache();
		},
		"accommodation.regimes.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async saveMedias(ctx, response) {
			if (ctx.params.media_ids && Array.isArray(ctx.params.media_ids)) {
				const media_ids = [...new Set(ctx.params.media_ids)];
				await ctx.call("accommodation.residences_medias.save_media_of_residences", {
					residence_id: response[0].id,
					media_ids,
				});
				response[0].mediaIds = await ctx.call(
					"accommodation.residences_medias.medias_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveServices(ctx, response) {
			if (ctx.params.service_ids && Array.isArray(ctx.params.service_ids)) {
				const service_ids = [...new Set(ctx.params.service_ids)];
				await ctx.call("accommodation.residence_services.save_services_of_residences", {
					residence_id: response[0].id,
					service_ids,
				});
				response[0].mediaIds = await ctx.call(
					"accommodation.residence_services.services_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveTypologies(ctx, response) {
			if (ctx.params.typology_ids && Array.isArray(ctx.params.typology_ids)) {
				const typology_ids = [...new Set(ctx.params.typology_ids)];
				await ctx.call("accommodation.residence_tipologies.save_typologies_of_residences", {
					residence_id: response[0].id,
					typology_ids,
				});
				response[0].services = await ctx.call(
					"accommodation.residence_tipologies.typologies_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveResponsibles(ctx, response) {
			if (
				ctx.params.residence_responsible_ids &&
				Array.isArray(ctx.params.residence_responsible_ids)
			) {
				const residence_responsible_ids = [...new Set(ctx.params.residence_responsible_ids)];
				await ctx.call("accommodation.residence_reponsibles.save_responsibles_of_residences", {
					residence_id: response[0].id,
					residence_responsible_ids,
				});
				response[0].residenceResponsibleIds = await ctx.call(
					"accommodation.residence_reponsibles.responsibles_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveWingResponsibles(ctx, response) {
			if (ctx.params.wing_responsible_ids && Array.isArray(ctx.params.wing_responsible_ids)) {
				const wing_responsible_ids = [...new Set(ctx.params.wing_responsible_ids)];
				await ctx.call(
					"accommodation.residence_wing_reponsibles.save_wing_responsibles_of_residences",
					{
						residence_id: response[0].id,
						wing_responsible_ids,
					},
				);
				response[0].wingResponsibleIds = await ctx.call(
					"accommodation.residence_wing_reponsibles.wing_responsibles_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveKitchenResponsibles(ctx, response) {
			if (ctx.params.kitchen_responsible_ids && Array.isArray(ctx.params.kitchen_responsible_ids)) {
				const kitchen_responsible_ids = [...new Set(ctx.params.kitchen_responsible_ids)];
				await ctx.call(
					"accommodation.residence_kitchen_reponsibles.save_wing_responsibles_of_residences",
					{
						residence_id: response[0].id,
						kitchen_responsible_ids,
					},
				);
				response[0].kitchenResponsibleIds = await ctx.call(
					"accommodation.residence_kitchen_reponsibles.kitchen_responsibles_of_residence",
					{
						residence_id: response[0].id,
					},
				);
			}
			return response;
		},
		async saveRegimes(ctx, response) {
			if (ctx.params.regime_ids && Array.isArray(ctx.params.regime_ids)) {
				const regime_ids = [...new Set(ctx.params.regime_ids)];
				await ctx.call("accommodation.residence-regimes.save_regimes_of_residences", {
					residence_id: response[0].id,
					regime_ids,
				});
				response[0].regimes = await ctx.call("accommodation.residence-regimes.find", {
					query: {
						residence_id: response[0].id,
					},
				});
			}
			return response;
		},
		async saveExtras(ctx, response) {
			if (ctx.params.extra_ids && Array.isArray(ctx.params.extra_ids)) {
				const extra_ids = [...new Set(ctx.params.extra_ids)];
				await ctx.call("accommodation.residence-extras.save_extras_of_residences", {
					residence_id: response[0].id,
					extra_ids,
				});
				response[0].extras = await ctx.call("accommodation.residence-extras.find", {
					query: {
						residence_id: response[0].id,
					},
				});
			}
			return response;
		},
		async validations(ctx) {
			if (ctx.params.building_id)
				await ctx.call("infrastructure.buildings.get", { id: ctx.params.building_id });
		},
		async validateResidenceRegimes(ctx) {
			if (ctx.params.regime_ids && Array.isArray(ctx.params.regime_ids))
				for (const id of ctx.params.regime_ids) {
					await ctx.call("accommodation.regimes.get", { id: id });
				}
		},
		async validateResidenceExtras(ctx) {
			if (ctx.params.extra_ids && Array.isArray(ctx.params.extra_ids))
				for (const id of ctx.params.extra_ids) {
					await ctx.call("accommodation.extras.get", { id: id });
				}
		},
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countAllApplication = await Promise.all([
					ctx.call("accommodation.applications.count", {
						query: {
							residence_id: ctx.params.id,
						},
					}),
					ctx.call("accommodation.applications.count", {
						query: {
							second_option_residence_id: ctx.params.id,
						},
					}),
					ctx.call("accommodation.applications.count", {
						query: {
							assigned_residence_id: ctx.params.id,
						},
					}),
					ctx.call("accommodation.applications.count", {
						query: {
							which_residence_id: ctx.params.id,
						},
					}),
				]);

				if (countAllApplication.reduce((a, b) => a + b, 0) > 0) {
					throw new Errors.ValidationError(
						"You have applications with this residence associated",
						"RESIDENCE_WITH_APPLICATIONS",
					);
				}

				const countRoomsItems = await ctx.call("accommodation.rooms.count", {
					query: {
						residence_id: ctx.params.id,
					},
				});

				if (countRoomsItems > 0) {
					throw new Errors.ValidationError(
						"You have rooms with this residence associated",
						"RESIDENCE_WITH_ROOMS",
					);
				}

				const countBookingItems = await ctx.call("accommodation.bookings.count", {
					query: {
						pretended_residence_id: ctx.params.id,
					},
				});

				if (countBookingItems > 0) {
					throw new Errors.ValidationError(
						"You have booking with this residence associated",
						"RESIDENCE_WITH_BOOKINGS",
					);
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
