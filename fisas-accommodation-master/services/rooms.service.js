"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.rooms",
	table: "room",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "rooms")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"room_id",
			"typology_id",
			"residence_id",
			"allow_booking",
			"active",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["typology", "residence"],
		withRelateds: {
			typology(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.typologies", "typology", "typology_id");
			},
			residence(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.residences", "residence", "residence_id", "id", {}, "id,name", false);
			},
			room(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.rooms", "room", "room_id");
			},
			occupants(ids, docs, rule, ctx) {
				const query = {
					room_id: docs.map(d => d.id),
					status: ["contracted", "confirmed", "pending", "assigned"],
				};
				if (ctx.params.academic_year) {
					query.academic_year = ctx.params.academic_year;
				};

				return ctx.call("accommodation.applications.find", {
					query,
					fields: "room_id,id,full_name",
					withRelated: false
				}).then(response => {
					docs.forEach(doc => {
						doc.occupants = response.filter(r => r.room_id === doc.id);
					});
					return docs;
				});
			},
		},
		entityValidator: {
			name: { type: "string" },
			room_id: { type: "number" },
			typology_id: { type: "number" },
			residence_id: { type: "number" },
			allow_booking: { type: "boolean" },
			active: { type: "boolean" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validations",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["isPossibleRemove"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		find: {
			cache: {
				keys: [
					"academic_year",
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#language_id"]
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"academic_year",
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query"
				]
			},

		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: [
					"id",
					"academic_year",
					"withRelated",
					"fields",
					"mapping"
				]
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

		"accommodation.applications.*"(ctx) {
			this.clearCache();
		},

	},

	/**
	 * Methods
	 */
	methods: {
		async validations(ctx) {
			await ctx.call("infrastructure.rooms.get", { id: ctx.params.room_id });
		},
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				if (ctx.params.id) {
					const countAllApplication = await Promise.all([
						ctx.call("accommodation.applications.count", {
							query: {
								residence_id: ctx.params.id,
							},
						}),
						ctx.call("accommodation.applications.count", {
							query: {
								room_id: ctx.params.id,
							},
						}),
						ctx.call("accommodation.applications.count", {
							query: {
								which_room_id: ctx.params.id,
							},
						}),
					]);

					if (countAllApplication.reduce((a, b) => a + b, 0) > 0) {
						throw new Errors.ValidationError(
							"You have applications with this room associated",
							"ROOM_WITH_APPLICATIONS",
						);
					}

					const countBillingsItems = await ctx.call("accommodation.billing_items.count", {
						query: {
							room_id: ctx.params.id
						}
					});

					if (countBillingsItems > 0) {
						throw new Errors.ValidationError(
							"You have billing items with this room associated",
							"ROOM_WITH_BILLINGS",
						);
					}

					// TODO Booking table must be implemented first
					/*const countBookingRooms = await ctx.call("accommodation.booking-rooms.count", {
						query: {
							room_id: ctx.params.id
						}
					});

					if (countBookingRooms > 0) {
						throw new Errors.ValidationError("You have bookings with this room associated", "ROOM_WITH_BOOKINGS");
					}*/
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
