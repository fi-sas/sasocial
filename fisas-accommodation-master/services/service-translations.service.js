"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.services_translations",
	table: "service_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "service-translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"service_id",
			"language_id"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			service_id: { type: "number", positive: true, integer: true },
			language_id: { type: "number", positive: true, integer: true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				service_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					service_id: ctx.params.service_id
				});
				this.clearCache();

				const entities = ctx.params.translations.map(translation => ({
					service_id: ctx.params.service_id,
					language_id: translation.language_id,
					name: translation.name,
				}));
				return this._insert(ctx, { entities });
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
