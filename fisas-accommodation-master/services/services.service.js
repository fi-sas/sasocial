"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.services",
	table: "service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "services")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "created_at", "updated_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.services_translations",
					"translations",
					"id",
					"service_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
			active: { type: "boolean" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"isPossibleRemove"
			],
			list: [
				function addFilterByTranslations(ctx) {
					return addSearchRelation(ctx, ["name"], "accommodation.services_translations", "service_id",);
				},
			]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);

				return this.adapter.db
					.transaction(async (trx) => {
						return this.adapter.db("service_translation").transacting(trx).where("service_id", id).del()
							.then(() => this.adapter.db("service").transacting(trx).where("id", id).del())
							.then(doc => {
								if (!doc)
									return Promise.reject(new Errors.EntityNotFoundError("service", params.id));
								return this.transformDocuments(ctx, {}, doc)
									.then(json => this.entityChanged("removed", json, ctx).then(() => json));
							});
					});

			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveTranslations(ctx, res) {
			await ctx.call("accommodation.services_translations.save_translations", {
				service_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("accommodation.services_translations.find", {
				query: {
					service_id: res[0].id,
				},
			});

			return res;
		},
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countRooms = await ctx.call("accommodation.residence_services.count", {
					query: {
						service_id: ctx.params.id
					}
				});

				if (countRooms > 0) {
					throw new Errors.ValidationError("You have residences with this service associated", "SERVICE_WITH_RESIDENCES");
				}
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
