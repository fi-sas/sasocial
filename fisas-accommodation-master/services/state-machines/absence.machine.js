let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSIS" },

			{ name: "DISPATCH", from: "ANALYSIS", to: "DISPATCH" },

			{ name: "APPROVE_WITH_SUSPENSION", from: "DISPATCH", to: "APPROVED_WITH_SUSPENSION" },
			{ name: "APPROVE_WITHOUT_SUSPENSION", from: "DISPATCH", to: "APPROVED_WITHOUT_SUSPENSION" },

			{ name: "REJECT", from: "DISPATCH", to: "ANALYSIS" },

			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSIS", to: "CANCELLED" },
			{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" },
		],
		methods: {
			onAfterTransition: function(lifecycle) {
				if (lifecycle.from === "none") {
					return;
				}
				// SAVE STATUS
				return ctx
					.call("accommodation.absences.patch", {
						id: ctx.params.id,
						status: lifecycle.to,
						...ctx.params.absence,
					})
					.then((absence) => {
						ctx.call("accommodation.absence-history.create", {
							absence_id: absence[0].id,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
							status: absence[0].status,
						});
						// SEND NOTIFICATION
						ctx
							.call("accommodation.applications.get", { id: absence[0].application_id })
							.then((application) => {
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: `ACCOMMODATION_ABSENCES_${absence[0].status.toUpperCase()}`,
									user_id: application[0].user_id,
									user_data: {},
									data: {
										application: application[0],
									},
									variables: {},
									external_uuid: application[0].uuid,
								});
							});
						return absence;
					});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
