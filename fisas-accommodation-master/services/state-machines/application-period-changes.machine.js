let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSIS" },

			{ name: "DISPATCH", from: "ANALYSIS", to: "DISPATCH" },

			{ name: "APPROVE", from: "DISPATCH", to: "APPROVED" },
			{ name: "REJECT", from: "DISPATCH", to: "REJECTED" },
			{ name: "ADMIN_REJECT", from: "DISPATCH", to: "ANALYSIS" },

			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSIS", to: "CANCELLED" },
			{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" },
		],
		methods: {
			onAfterTransition: function(lifecycle) {
				if (lifecycle.from === "none") {
					return;
				}
				// SAVE STATUS
				return ctx
					.call("accommodation.application-period-changes.patch", {
						id: ctx.params.id,
						status: lifecycle.to,
						...ctx.params.application_period_change,
					})
					.then((application_period_change) => {
						ctx.call("accommodation.application-period-change-history.create", {
							application_period_change_id: application_period_change[0].id,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
							status: application_period_change[0].status,
						});

						// SEND NOTIFICATION
						ctx
							.call("accommodation.applications.get", {
								id: application_period_change[0].application_id,
							})
							.then((application) => {
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: `ACCOMMODATION_APPLICATION_PERIOD_CHANGE_${application_period_change[0].status.toUpperCase()}`,
									user_id: application[0].user_id,
									user_data: {},
									data: {
										application: application[0],
									},
									variables: {},
									external_uuid: application[0].uuid,
								});
							});
						return application_period_change;
					});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
