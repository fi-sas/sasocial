let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "SUBMIT", from: "drafted", to: "submitted" },
			{ name: "SUBMIT", from: "submitted", to: "submitted" },

			{ name: "ANALYSE", from: "submitted", to: "analysed" },

			{ name: "CANCEL", from: "submitted", to: "cancelled" },
			{ name: "CANCEL", from: "analysed", to: "cancelled" },

			{ name: "PENDING", from: "analysed", to: "pending" },
			{ name: "CANCEL", from: "analysed", to: "cancelled" },

			{ name: "ANALYSE", from: "pending", to: "analysed" },
			{ name: "ENQUEUE", from: "pending", to: "queued" },
			{ name: "ASSIGN", from: "pending", to: "assigned" },
			{ name: "CANCEL", from: "pending", to: "cancelled" },

			{ name: "UNASSIGN", from: "pending", to: "unassigned" },

			{ name: "CLOSE", from: "unassigned", to: "closed" },
			{ name: "OPPOSE", from: "unassigned", to: "opposition" },

			{ name: "CANCEL", from: "queued", to: "cancelled" },
			{ name: "PENDING", from: "queued", to: "pending" },
			{ name: "OPPOSE", from: "queued", to: "opposition" },

			{ name: "CONFIRM", from: "assigned", to: "confirmed" },
			{ name: "REJECT", from: "assigned", to: "rejected" },
			{ name: "OPPOSE", from: "assigned", to: "opposition" },

			{ name: "CLOSE", from: "opposition", to: "closed" },
			{ name: "PENDING", from: "opposition", to: "pending" },

			{ name: "CONTRACT", from: "confirmed", to: "contracted" },
			{ name: "CANCEL", from: "confirmed", to: "cancelled" },

			{ name: "ANALYSE", from: "rejected", to: "analysed" },

			{ name: "REOPEN", from: "contracted", to: "reopened" },
			{ name: "CLOSE", from: "contracted", to: "closed" },

			{ name: "ANALYSE", from: "reopened", to: "analysed" },

			{ name: "CONTRACT", from: "closed", to: "contracted" },
			{ name: "REOPEN", from: "closed", to: "reopened" },

			{ name: "ROLLBACK_TO_SUBMITTED", from: "analysed", to: "submitted" },
			{ name: "ROLLBACK_TO_SUBMITTED", from: "cancelled", to: "submitted" },
			{ name: "ROLLBACK_TO_ANALYSED", from: "cancelled", to: "analysed" },
			{ name: "ROLLBACK_TO_ANALYSED", from: "pending", to: "analysed" },
			{ name: "ROLLBACK_TO_PENDING", from: "cancelled", to: "pending" },
			{ name: "ROLLBACK_TO_PENDING", from: "assigned", to: "pending" },
			{ name: "ROLLBACK_TO_PENDING", from: "unassigned", to: "pending" },
			{ name: "ROLLBACK_TO_ASSIGNED", from: "confirmed", to: "assigned" },
			{ name: "ROLLBACK_TO_ASSIGNED", from: "rejected", to: "assigned" },
			{ name: "ROLLBACK_TO_ANALYSED", from: "withdrawal", to: "analysed" },
		],
		methods: {
			onCONFIRM: function() {},
			onREJECT: function() {},
			onOPPOSE: function() {},
			onAfterTransition: function(lifecycle) {
				if (lifecycle.from === "none") {
					return;
				}

				// SAVE STATUS
				return ctx
					.call("accommodation.applications.patch", {
						id: ctx.params.id,
						status: lifecycle.to,
						...ctx.params.application,
					})
					.then((application) => {
						// SAVE HISTORY
						ctx.call("accommodation.applications-history.create", {
							application_id: ctx.params.id,
							user_id: ctx.meta.user ? ctx.meta.user.id : null,
							status: lifecycle.to,
							notes: ctx.params.notes ? ctx.params.notes : "",
							decision: ctx.params.application ? ctx.params.application.decision : null,
							room_id: ctx.params.application ? ctx.params.application.room_id : null,
							residence_id: ctx.params.application
								? ctx.params.application.assigned_residence_id
								: null,
							tariff_id: ctx.params.application ? ctx.params.application.tariff_id : null,
						});

						// SEND NOTIFICATION
						ctx.call("notifications.alerts.create_alert", {
							alert_type_key: `ACCOMMODATION_APPLICATION_STATUS_${application[0].status.toUpperCase()}`,
							user_id: application[0].user_id,
							user_data: {},
							data: {
								application: application[0],
							},
							variables: {},
							external_uuid: application[0].uuid,
						});

						return application;
					});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
