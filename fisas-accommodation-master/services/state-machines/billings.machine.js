let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "REVIEW", from: "PROCESSED", to: "REVIEWED" },
			{ name: "REVIEW", from: "REVIEWED", to: "REVIEWED" },
			{ name: "CANCEL", from: "PROCESSED", to: "CANCELLED" },
			{ name: "CANCEL", from: "PAID", to: "CANCELLED" },
			{ name: "CANCEL", from: "BILLED", to: "CANCELLED" },
			{ name: "EXTERNAL_VALIDATE", from: "PROCESSED", to: "EXTERNAL_VALIDATED" },
			{ name: "EXTERNAL_PAID", from: "EXTERNAL_VALIDATED", to: "EXTERNAL_PAID" },
		],
		methods: {
			onAfterTransition: function(lifecycle) {
				if (lifecycle.from === "none") {
					return;
				}
				return ctx.call("accommodation.billings.patch", {
					id: ctx.params.id,
					status: lifecycle.to,
					...ctx.params.billing,
				});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
