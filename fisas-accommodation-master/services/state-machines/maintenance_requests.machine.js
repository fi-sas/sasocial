let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "RESOLVING", from: "SUBMITTED", to: "RESOLVING" },
			{ name: "REJECT", from: "SUBMITTED", to: "REJECTED" },
			{ name: "RESOLVE", from: "RESOLVING", to: "RESOLVED" },
		],
		methods: {
			onAfterTransition: function(lifecycle) {
				if (lifecycle.from === "none") {
					return;
				}
				// SAVE STATUS
				return ctx
					.call("accommodation.maintenance-requests.patch", {
						id: ctx.params.id,
						status: lifecycle.to,
						...ctx.params.maintenance_request,
					})
					.then((maintenance_request_change) => {
						ctx.call("accommodation.maintenance-request-history.create", {
							maintenance_request_id: maintenance_request_change[0].id,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
							status: maintenance_request_change[0].status,
						});

						// SEND NOTIFICATION
						ctx
							.call("accommodation.applications.get", {
								id: maintenance_request_change[0].application_id,
							})
							.then((application) => {
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: `ACCOMMODATION_MAINTENANCE_REQUEST_${maintenance_request_change[0].status.toUpperCase()}`,
									user_id: application[0].user_id,
									user_data: {},
									data: {
										application: application[0],
									},
									variables: {},
									external_uuid: application[0].uuid,
								});
							});
						return maintenance_request_change;
					});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
