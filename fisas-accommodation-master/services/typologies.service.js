"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const _ = require("lodash");
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const QueueService = require("moleculer-bull");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.typologies",
	table: "typology",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "typologies"), QueueService(process.env.REDIS_QUEUE)],
	/**
	 * QUEUES
	 */
	queues: {
		"typology.update.reprocessBillings"(job) {
			this.logger.info("NEW JOB RECEIVED!");
			return this.updateProcessedBillings(job.data.id)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch(err => {
					this.logger.info("ANTES DE MANDAR O REJECT");
					this.logger.info(err);
					return Promise.reject(err);
				});
		}
	},
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "max_occupants_number", "product_code", "reprocess_since", "created_at", "updated_at"],
		defaultWithRelateds: ["translations", "priceLines", "discounts"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.typology_translations",
					"translations",
					"id",
					"typology_id",
				);
			},
			priceLines(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "accommodation.price_lines", "priceLines", "id", "typology_id");
			},
			discounts(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.typologies-discount",
					"discounts",
					"id",
					"typology_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
			priceLines: {
				type: "array",
				item: {
					price_scholarship: { type: "number", positive: true, convert: true },
					price: { type: "number", positive: true, convert: true },
					period: { type: "enum", values: ["WEEK", "DAY", "MONTH"] },
					vat_id: { type: "number", positive: true, convert: true },
				},
			},
			max_occupants_number: { type: "number", positive: true, convert: true },
			product_code: { type: "string" },
			active: { type: "boolean" },
			discounts: {
				type: "array",
				item: {
					date: { type: "date", convert: true },
					discount_value: { type: "number", convert: true, max: 100 },
				}
			},
			reprocess_since: { type: "date", convert: true, optional: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				function validateEntities(ctx) {
					return this.validateExternalIds(ctx, {
						"configuration.languages.get": {
							value: ctx.params.translations.map((t) => t.language_id),
						},
						"configuration.taxes.get": {
							value: ctx.params.priceLines.map((t) => t.vat_id),
						},
					});
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"isPossibleRemove"
			],
			list: [
				function addFilterByTranslations(ctx) {
					return addSearchRelation(ctx, ["name"], "accommodation.typology_translations", "typology_id",);
				},
			]
		},
		after: {
			create: ["savePriceLines", "saveTranslations", "saveDiscounts"],
			update: [
				"savePriceLines",
				"saveTranslations",
				"saveDiscounts",
				function updatePriceOnProcessedBillings(ctx, res) {
					this.createJob(
						"typology.update.reprocessBillings",
						{
							id: res[0].id,
						},
						{
							attempts: 1,
							removeOnComplete: true,
							removeOnFail: true,
						},
					);
					return res;
				},],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);

				return this.adapter.db
					.transaction(async (trx) => {
						return this.adapter.db("typology_translation").transacting(trx).where("typology_id", id).del()
							.then(() => this.adapter.db("price_line").transacting(trx).where("typology_id", id).del())
							.then(() => this.adapter.db("typology_discount").transacting(trx).where("typology_id", id).del())
							.then(() => this.adapter.db("typology").transacting(trx).where("id", id).del())
							.then(doc => {
								if (!doc)
									return Promise.reject(new Errors.EntityNotFoundError("typology", params.id));
								return this.transformDocuments(ctx, {}, doc)
									.then(json => this.entityChanged("removed", json, ctx).then(() => json));
							});
					});

			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"accommodation.typologies.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateExternalIds(ctx, opts) {
			const actions = _.keys(opts);

			this.logger.info(opts);

			let promises = [];
			_.forEach(actions, (action) => {
				this.logger.info(action, {
					id: opts[action].value,
				});
				if (opts[action].value) {
					promises.push(
						ctx.call(action, {
							id: opts[action].value,
						}),
					);
				}
			});

			return await Promise.all(promises)
				.then((res) => {
					this.logger.info("RES");
					this.logger.info(res);
					return res;
				})
				.catch((err) => {
					this.logger.info("ERR");
					this.logger.info(err);
					throw err;
				});
		},
		async saveTranslations(ctx, res) {
			await ctx.call("accommodation.typology_translations.save_translations", {
				typology_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("accommodation.typology_translations.find", {
				query: {
					typology_id: res[0].id,
				},
			});

			return res;
		},

		async saveDiscounts(ctx, res) {
			if (ctx.params.discounts && Array.isArray(ctx.params.discounts)) {
				await ctx.call("accommodation.typologies-discount.save_discounts", {
					typology_id: res[0].id,
					discounts: ctx.params.discounts,
				});
				res[0].discounts = await ctx.call("accommodation.typologies-discount.typologies_discounts", { typology_id: res[0].id });
			}
			return res;
		},

		async savePriceLines(ctx, res) {
			await ctx.call("accommodation.price_lines.save_priceLines", {
				typology_id: res[0].id,
				priceLines: ctx.params.priceLines,
			});

			res[0].priceLines = await ctx.call("accommodation.price_lines.find", {
				query: {
					typology_id: res[0].id,
				},
			});

			return res;
		},
		async isPossibleRemove(ctx) {
			if (ctx.params.id) {
				const countRooms = await ctx.call("accommodation.rooms.count", {
					query: {
						typology_id: ctx.params.id
					}
				});

				if (countRooms > 0) {
					throw new Errors.ValidationError("You have rooms with this typology associated", "TYPOLOGY_WITH_ROOMS");
				}

				const countResidences = await ctx.call("accommodation.residence_tipologies.count", {
					query: {
						typology_id: ctx.params.id
					}
				});

				if (countResidences > 0) {
					throw new Errors.ValidationError("You have residences with this typology associated", "TYPOLOGY_WITH_RESIDENCES");
				}

				const countBillingsItems = await ctx.call("accommodation.billing_items.count", {
					query: {
						typology_id: ctx.params.id
					}
				});

				if (countBillingsItems > 0) {
					throw new Errors.ValidationError("You have billing items with this typology associated", "TYPOLOGY_WITH_BILLINGS");
				}
			}
		},
		async updateProcessedBillings(id) {
			let query = `
			select billing.id
			from room as room
			inner join typology as typology
			on room.typology_id = typology.id
			inner join application as application 
			on application.room_id = room.id 
			inner join billing as billing
			on billing.application_id = application.id
			where typology.id = ? and room.active = true
			and billing.status in ('PROCESSED')
			group by billing.id`;
			this.adapter.raw(query, id).then(async (data) => {
				for (const row of data.rows) {
					try {
						await this.broker.call("accommodation.billings.reprocessApplicationBilling", { id: row.id });
					} catch (error) {
						this.logger.info(error);
					}
				}
			});
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
