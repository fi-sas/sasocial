"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.typology_translations",
	table: "typology_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "typology-translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "typology_id", "language_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			typology_id: { type: "number", positive: true, convert: true },
			language_id: { type: "number", positive: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [],
			update: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				typology_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				translations: {
					type: "array",
					item: {
						language_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
						},
						name: { type: "string" },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					typology_id: ctx.params.typology_id,
				});
				this.clearCache();

				const entities = ctx.params.translations.map((translation) => ({
					typology_id: ctx.params.typology_id,
					language_id: translation.language_id,
					name: translation.name,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
