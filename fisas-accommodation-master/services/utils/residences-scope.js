async function checkAllowedResidencesQuery(ctx) {
	ctx.params.query = ctx.params.query ? ctx.params.query : {};
	if (ctx.meta.isBackoffice && !ctx.params.query.residence_id) {
		await ctx.call("authorization.scopes.userHasScope", { scope: "accommodation:is_residence_admnistrator" })
			.then(async user => {
				if (!user) {
					const residence_responsables = await ctx.call("accommodation.residence_reponsibles.find", {
						query: {
							user_id: ctx.meta.user.id
						},
						withRelated: false,
					});
					ctx.params.query.residence_id = residence_responsables.map((r) => r.residence_id);
				}
			});
	} else if (ctx.params.query.residence_id) {
		ctx.params.query.residence_id = Array.isArray(ctx.params.query.residence_id) ? ctx.params.query.residence_id : [ctx.params.query.residence_id];
	}
}

async function checkAllowedResidencesParams(ctx) {
	if (ctx.meta.isBackoffice && !ctx.params.residence_id) {
		await ctx.call("authorization.scopes.userHasScope", { scope: "accommodation:is_residence_admnistrator" })
			.then(async user => {
				if (!user) {
					const residence_responsables = await ctx.call("accommodation.residence_reponsibles.find", {
						query: {
							user_id: ctx.meta.user.id
						},
						withRelated: false,
					});
					ctx.params.residence_id = residence_responsables.map((r) => r.residence_id);
				}
			});
	} else if (ctx.params.residence_id) {
		ctx.params.residence_id = Array.isArray(ctx.params.residence_id) ? ctx.params.residence_id : [ctx.params.residence_id];
	}
}

module.exports = {
	checkAllowedResidencesQuery,
	checkAllowedResidencesParams
};