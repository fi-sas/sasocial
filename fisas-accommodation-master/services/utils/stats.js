module.exports = {
	STATS: [
		{
			name: "withdrawals",
			action: "accommodation.withdrawals.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "absences",
			action: "accommodation.absences.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "extensions",
			action: "accommodation.extensions.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "application_extra_changes",
			action: "accommodation.application-extra-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "application_regime_changes",
			action: "accommodation.application-regime-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "application_room_typology_changes",
			action: "accommodation.application-room-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: "TYPOLOGY",
			},
		},
		{
			name: "application_room_residence_changes",
			action: "accommodation.application-room-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: "RESIDENCE",
			},
		},
		{
			name: "application_room_permute_changes",
			action: "accommodation.application-room-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: "PERMUTE",
			},
		},
		{
			name: "application_room_room_changes",
			action: "accommodation.application-room-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: "ROOM",
			},
		},
		{
			name: "application_tariff_changes",
			action: "accommodation.application-tariff-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
		{
			name: "maintenance_requests_resolving",
			action: "accommodation.maintenance-requests.count",
			query: {
				status: ["SUBMITTED", "RESOLVING"],
				type: null,
			},
		},
		{
			name: "application_communications_entry",
			action: "accommodation.application-communications.count",
			query: {
				status: ["SUBMITTED"],
				type: "ENTRY",
			},
		},
		{
			name: "application_communications_exit",
			action: "accommodation.application-communications.count",
			query: {
				status: ["SUBMITTED"],
				type: "EXIT",
			},
		},
		{
			name: "application_iban_changes",
			action: "accommodation.application-iban-changes.count",
			query: {
				status: ["SUBMITTED"],
				type: null,
			},
		},
		{
			name: "application_period_changes",
			action: "accommodation.application-period-changes.count",
			query: {
				status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
				type: null,
			},
		},
	],
};
