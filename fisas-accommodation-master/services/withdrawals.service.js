"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const withdrawalsStateMachine = require("./state-machines/withdrawals.machine");
const moment = require("moment");
const Cron = require("moleculer-cron");
const { checkAllowedResidencesQuery } = require("./utils/residences-scope");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accommodation.withdrawals",
	table: "withdrawal",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("accommodation", "withdrawals"), Cron],
	crons: [
		{
			name: "automaticChangeApplicationStatusToWithdrawal",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("accommodation.withdrawals")
					.actions.automatic_change_to_withdrawal_status()
					.then(() => { });
			},
			runOnInit: function () { },
		},
	],
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"end_date",
			"reason",
			"application_id",
			"allow_booking",
			"file_id",
			"decision",
			"status",
			"user_id",
			"updated_at",
			"created_at",
			"observations",
		],
		defaultWithRelateds: ["application", "file", "history"],
		withRelateds: {
			/*application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "accommodation.applications", "application", "application_id");
			},*/

			application(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("accommodation.applications.get", {
								id: doc.application_id,
								withRelated: false,
							})
							.then(async (res) => {
								if (res.length) {
									if (res[0].assigned_residence_id) {
										res[0].assignedResidence = await ctx
											.call("accommodation.residences.get", {
												id: res[0].assigned_residence_id,
												withRelated: false,
											})
											.then((residence) => (residence.length ? residence[0] : []));
									} else {
										res[0].assignedResidence = [];
									}
									if (res[0].room_id) {
										res[0].room = await ctx
											.call("accommodation.rooms.get", {
												id: res[0].room_id,
												withRelated: false,
											})
											.then((room) => (room.length ? room[0] : []));
									} else {
										res[0].room = [];
									}
								}
								doc.application = res[0];
							});
					}),
				);
			},

			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"accommodation.withdrawal-history",
					"history",
					"id",
					"withdrawal_id",
				);
			},
		},
		entityValidator: {
			end_date: { type: "date", convert: true },
			reason: { type: "string" },
			allow_booking: { type: "boolean", optional: true },
			application_id: { type: "number", optional: false },
			file_id: { type: "number", integer: true, optional: true, convert: true },
			user_id: { type: "number", integer: true, convert: true },
			decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
			status: {
				type: "enum",
				values: ["SUBMITTED", "ANALYSIS", "DISPATCH", "APPROVED", "REJECTED", "CANCELLED"],
				default: "SUBMITTED",
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
			observations: { type: "string", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					const applicationData = await ctx.call("accommodation.applications.get", {
						id: ctx.params.application_id,
						withRelated: false,
					});
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = applicationData[0].user_id;
					ctx.params.status = "SUBMITTED";
				},
				function getActiveApplication(ctx) {
					return ctx
						.call("accommodation.applications.get", {
							id: ctx.params.application_id,
							withRelated: false,
						})
						.then((application) => {
							return ctx.call("accommodation.applications.find", {
								query: {
									user_id: application[0].user_id,
									status: "contracted",
								},
							});
						})
						.then((applications) => {
							if (applications.length > 0) {
								ctx.params.application_id = applications[0].id;
								if (moment(applications[0].end_date).isSameOrBefore(ctx.params.end_date)) {
									throw new ValidationError(
										"End date cannot be before application end date",
										"ACCOMMODATION_WITHDRAWALS_INVALID_DATE",
										{},
									);
								}
							} else {
								throw new ValidationError(
									"No active application finded in accommodation",
									"ACCOMMODATION_APPLICAION_NOT_FOUND",
									{},
								);
							}
						});
				},
				function checkIfAlreadyExist(ctx) {
					return ctx
						.call("accommodation.withdrawals.count", {
							query: {
								application_id: ctx.params.application_id,
								status: ["SUBMITTED", "ANALYSIS", "DISPATCH"],
							},
						})
						.then((count) => {
							if (count > 0) {
								throw new ValidationError(
									"Already exist a active withdrawal",
									"ACCOMMODATION_AREADY_EXIST_ACTIVE_WITHDRAWAL",
									{},
								);
							}
						});
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				checkAllowedResidencesQuery,
				async function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					let applications_ids = null;
					if (ctx.params.search && ctx.params.searchFields) {
						let applicationSearchFields = ctx.params.searchFields
							.split(",")
							.filter((sf) =>
								["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							);

						if (applicationSearchFields.length) {
							applications_ids = await ctx.call("accommodation.applications.find", {
								fields: "id",
								withRelated: false,
								search: ctx.params.search,
								searchFields: applicationSearchFields.join(","),
							});
						}

						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter(
								(sf) =>
									!["full_name", "email", "tin", "identification", "student_number"].includes(sf),
							)
							.join(",");
					}

					const queryResidenceId = ctx.params.query.residence_id
						? ctx.params.query.residence_id
						: null;
					const queryAcademicYear = ctx.params.query.academic_year
						? ctx.params.query.academic_year
						: null;
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.query || applications_ids || (ctx.params.sort && ctx.params.sort.includes("application."))) {
							qb.innerJoin("application", "application.id", "withdrawal.application_id");
						}

						if (ctx.params.query || applications_ids) {
							if (applications_ids)
								qb.andWhere(
									"application_id",
									"in",
									applications_ids.map((x) => x.id),
								);
							if (queryResidenceId)
								qb.andWhere("application.assigned_residence_id", "in", queryResidenceId);
							if (queryAcademicYear)
								qb.andWhere("application.academic_year", "=", queryAcademicYear);
						}
					};
					delete ctx.params.query.residence_id;
					delete ctx.params.query.academic_year;
				},
			],
		},
		after: {
			admin_approve: [
				async function processApplicationBilling(ctx, res) {
					if (res[0].decision == "APPROVE") {
						await ctx.call("accommodation.applications.patch", {
							id: res[0].application_id,
							end_date: res[0].end_date,
							updated_end_date: res[0].end_date,
						});
						ctx.call("accommodation.billings.processWithdrawal", {
							application_id: res[0].application_id,
							end_date: res[0].end_date,
						});
					}
					return res;
				},
			],
			create: [
				async function sendNotification(ctx, response) {
					const application = await ctx.call("accommodation.applications.get", {
						id: response[0].application_id,
					});
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "ACCOMMODATION_WITHDRAWAL_SUBMITTED",
						user_id: application[0].user_id,
						user_data: {},
						data: { application: application },
						variables: {},
						external_uuid: null,
					});
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		automatic_change_to_withdrawal_status: {
			async handler(ctx) {
				return this._find(ctx, {
					withRelated: false,
					query: (qb) => {
						qb.select(
							"withdrawal.id",
							"withdrawal.application_id",
							"withdrawal.status",
							"withdrawal.decision",
							"withdrawal.end_date",
						);
						qb.innerJoin("application", "application.id", "withdrawal.application_id");
						qb.where("application.status", "=", "contracted");
						qb.where("withdrawal.end_date", "<=", new Date());
						qb.where("withdrawal.status", "=", "APPROVED");
					},
				}).then((withdrawals) => {
					for (const w of withdrawals) {
						ctx
							.call("accommodation.applications.patch", {
								id: w.application_id,
								status: "withdrawal",
							})
							.then((app) => {
								ctx.call("accommodation.applications-history.create", {
									application_id: w.application_id,
									status: "withdrawal",
									user_id: null,
									notes:
										"[Automático] Passagem de estado automática devido a um pedido de desistência",
								});
							});
					}
				});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		change_status: {
			rest: "POST /:id/status",
			scope: "accommodation:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["ANALYSE", "CANCEL", "DISPATCH"] },
				withdrawal: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						allow_booking: { type: "boolean", optional: true },
						application_id: { type: "number", optional: true },
						file_id: { type: "number", integer: true, optional: true, convert: true },
						user_id: { type: "number", integer: true, convert: true, optional: true },
						decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const withdrawal = await ctx.call("accommodation.withdrawals.get", { id: ctx.params.id });
				const stateMachine = withdrawalsStateMachine.createStateMachine(withdrawal[0].status, ctx);

				if (
					(ctx.params.event == "DISPATCH" && !ctx.params.withdrawal) ||
					(ctx.params.event == "DISPATCH" && !ctx.params.withdrawal.decision)
				) {
					throw new ValidationError(
						"Decision are required to to change status to dispatch",
						"ACCOMMODATION_WITHDRAWALS_DECISION_NOT_FOUND",
						{},
					);
				}
				if (stateMachine.can(ctx.params.event)) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_WITHDRAWALS_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		admin_approve: {
			rest: "POST /:id/admin/approve",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				withdrawal: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						allow_booking: { type: "boolean", optional: true },
						application_id: { type: "number", optional: true },
						file_id: { type: "number", integer: true, optional: true, convert: true },
						user_id: { type: "number", integer: true, convert: true, optional: true },
						decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const withdrawal = await ctx.call("accommodation.withdrawals.get", { id: ctx.params.id });
				const stateMachine = withdrawalsStateMachine.createStateMachine(withdrawal[0].status, ctx);
				if (stateMachine.can(withdrawal[0].decision)) {
					return stateMachine[withdrawal[0].decision.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_WITHDRAWALS_STATUS",
						{},
					);
				}
			},
		},
		admin_reject: {
			rest: "POST /:id/admin/reject",
			scope: "accommodation:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
				withdrawal: {
					type: "object",
					props: {
						end_date: { type: "date", convert: true, optional: true },
						reason: { type: "string", optional: true },
						allow_booking: { type: "boolean", optional: true },
						application_id: { type: "number", optional: true },
						file_id: { type: "number", integer: true, optional: true, convert: true },
						user_id: { type: "number", integer: true, convert: true, optional: true },
						decision: { type: "enum", values: ["APPROVE", "REJECT"], optional: true },
						observations: { type: "string", optional: true },
					},
					optional: true,
				},
			},
			async handler(ctx) {
				const withdrawal = await ctx.call("accommodation.withdrawals.get", { id: ctx.params.id });
				const stateMachine = withdrawalsStateMachine.createStateMachine(withdrawal[0].status, ctx);

				if (stateMachine.can("ADMIN_REJECT")) {
					return stateMachine["adminReject"]();
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"ACCOMMODATION_WITHDRAWALS_STATUS",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
