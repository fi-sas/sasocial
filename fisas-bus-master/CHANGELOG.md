## [1.12.5](https://gitlab.com/fi-sas/fisas-bus/compare/v1.12.4...v1.12.5) (2022-05-18)


### Bug Fixes

* **mobility:** change from meta to params ([01b47d0](https://gitlab.com/fi-sas/fisas-bus/commit/01b47d0e8f0225abbb774c3d7a206902ae27d0b3))

## [1.12.4](https://gitlab.com/fi-sas/fisas-bus/compare/v1.12.3...v1.12.4) (2022-01-10)


### Bug Fixes

* **tickets_bougth:** change dashboard query ([a25d696](https://gitlab.com/fi-sas/fisas-bus/commit/a25d696cf8c7fe71c49af936f3bdbc16680ae1ad))

## [1.12.3](https://gitlab.com/fi-sas/fisas-bus/compare/v1.12.2...v1.12.3) (2022-01-07)


### Bug Fixes

* **route_search:** calculation of prices ([f9010a4](https://gitlab.com/fi-sas/fisas-bus/commit/f9010a451040333cc54115cd6399bb6fb0edd6b2))

## [1.12.2](https://gitlab.com/fi-sas/fisas-bus/compare/v1.12.1...v1.12.2) (2022-01-05)


### Bug Fixes

* **route_search:** repreated routes ([17880f5](https://gitlab.com/fi-sas/fisas-bus/commit/17880f5796e0e06cb73f7e78cb2044c62e5f5ee4))

## [1.12.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.12.0...v1.12.1) (2022-01-05)


### Bug Fixes

* **route_search:** remove repeat routes ([86c7e99](https://gitlab.com/fi-sas/fisas-bus/commit/86c7e99bc38fa65cc95f1c91ae98acccd75bc7f4))

# [1.12.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.11.1...v1.12.0) (2022-01-03)


### Features

* **tickets:** add new ticket system ([94174a0](https://gitlab.com/fi-sas/fisas-bus/commit/94174a07594e4867622e241945378eb920b690b0))

## [1.11.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.11.0...v1.11.1) (2021-09-24)


### Bug Fixes

* declaration_file_id optional ([0494908](https://gitlab.com/fi-sas/fisas-bus/commit/049490812098d48995f657fe7e6649f71b093160))

# [1.11.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.10.0...v1.11.0) (2021-09-24)


### Features

* send notification on declaration ready ([28ba79d](https://gitlab.com/fi-sas/fisas-bus/commit/28ba79d3fcd748e9212cf3b9417f006e38c048f9))

# [1.10.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.9.4...v1.10.0) (2021-09-24)


### Features

* **applications:** save declaration file to user ([8b697f0](https://gitlab.com/fi-sas/fisas-bus/commit/8b697f0ae12dde7a7382ff1396bfb1d8dd7c5db2))

## [1.9.4](https://gitlab.com/fi-sas/fisas-bus/compare/v1.9.3...v1.9.4) (2021-09-24)


### Bug Fixes

* send application notification of approve and not approve ([c6d8dec](https://gitlab.com/fi-sas/fisas-bus/commit/c6d8dec4648a3eb338149ce719569f59ca1165cc))

## [1.9.3](https://gitlab.com/fi-sas/fisas-bus/compare/v1.9.2...v1.9.3) (2021-09-24)


### Bug Fixes

* **revert:** revert date field ([f16d0e0](https://gitlab.com/fi-sas/fisas-bus/commit/f16d0e0fe412780c07abbac3c4d3dc0f1392b525))

## [1.9.2](https://gitlab.com/fi-sas/fisas-bus/compare/v1.9.1...v1.9.2) (2021-09-24)


### Bug Fixes

* **sub23:** add date of document ([f8c38e7](https://gitlab.com/fi-sas/fisas-bus/commit/f8c38e7e60cee102829afa3cd553b9b5ab3e5848))

## [1.9.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.9.0...v1.9.1) (2021-09-22)


### Bug Fixes

* **applications:** change query params filter ([9dae2a1](https://gitlab.com/fi-sas/fisas-bus/commit/9dae2a13d16616e8e5b879a13dc240fc383a28f1))

# [1.9.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.8.0...v1.9.0) (2021-09-17)


### Features

* **applications:** add academic year ([1f227de](https://gitlab.com/fi-sas/fisas-bus/commit/1f227de8cfb0f582a7a8bf4f9efcc592ec26244c))
* **declarations:** add cc emited in ([645ad72](https://gitlab.com/fi-sas/fisas-bus/commit/645ad724ac3c3a09ad4f84ebcc9b0396977edc9d))

# [1.8.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.7.0...v1.8.0) (2021-09-15)


### Bug Fixes

* **enums:** update status declaration sub23 ([b66359e](https://gitlab.com/fi-sas/fisas-bus/commit/b66359eeed269eb9cec65c3149701a959c36dabb))
* **sub23-declaration:** add validations, reject_reason and new status ([ddcbf71](https://gitlab.com/fi-sas/fisas-bus/commit/ddcbf716723c5daf8118a11dbb4417185b193f5d))
* **sub23-machine:** update and add new status ([1c82e6d](https://gitlab.com/fi-sas/fisas-bus/commit/1c82e6dee1f99a2423247dfea2ea0d77611cdca9))


### Features

* **migrations:** update table declarations sub 23 ([44ccb9c](https://gitlab.com/fi-sas/fisas-bus/commit/44ccb9c055d4a96150ed115268643e551d656626))
* **sub23_declarations:** add generate report lista declarations ([1ab76c6](https://gitlab.com/fi-sas/fisas-bus/commit/1ab76c6abd43c85573da74afee281a8dd1748616))

# [1.7.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.6.1...v1.7.0) (2021-09-06)


### Bug Fixes

* **route_search:** update tickets_prices_total ([10fe1bb](https://gitlab.com/fi-sas/fisas-bus/commit/10fe1bb11747e8bf2b067a53d51e5c27f1755740))
* **timetables:** remove all hours timetable ([a3d928e](https://gitlab.com/fi-sas/fisas-bus/commit/a3d928e0d139fdd3fdcbb8b9be7175991f2c9c1e))
* **timetables:** update validation hours ([c6ab5c9](https://gitlab.com/fi-sas/fisas-bus/commit/c6ab5c92fda31b8fec7b38e7fe538627eb049fae))


### Features

* **hours:** add delete hours by timetable ([015d518](https://gitlab.com/fi-sas/fisas-bus/commit/015d5189ab80a9713298d5a4e5a9732de8394695))
* **route_search:** add count stops ([4dc5159](https://gitlab.com/fi-sas/fisas-bus/commit/4dc5159780259fc9969e3dac427f076f98d08932))

## [1.6.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.6.0...v1.6.1) (2021-09-02)


### Bug Fixes

* **route-search:** update response ([246f152](https://gitlab.com/fi-sas/fisas-bus/commit/246f152e219f8153fe0db21bf74ec49d0c3ec128))

# [1.6.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.5.0...v1.6.0) (2021-08-25)


### Bug Fixes

* **application.machine:** update change status ([53de88b](https://gitlab.com/fi-sas/fisas-bus/commit/53de88b8a352b09aa407905368d0297ca64aa137))
* **sub23_declaration:** remove accept_procedure ([97dcf01](https://gitlab.com/fi-sas/fisas-bus/commit/97dcf01657ad1d4106765fa7b774138fa6564c69))


### Features

* **migration:** update table sub23_declaration ([e143bd5](https://gitlab.com/fi-sas/fisas-bus/commit/e143bd55ec96a56cdd791e3c8835012c9cda6647))

# [1.5.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.4.1...v1.5.0) (2021-08-24)


### Bug Fixes

* **applications.machine:** udpate events ([d89e968](https://gitlab.com/fi-sas/fisas-bus/commit/d89e9688f2198d7c56c8a6bdc473eb0ead06e936))
* **configurations:** add optional values ([e77db1e](https://gitlab.com/fi-sas/fisas-bus/commit/e77db1e23c35bcac656481264c296026509ca3cf))
* **enum:** update status ([273417c](https://gitlab.com/fi-sas/fisas-bus/commit/273417cf3d9bd7e56aa1bc2b9a5bc9f5372ba0d4))
* **migrations:** applications add reject reason ([c00005e](https://gitlab.com/fi-sas/fisas-bus/commit/c00005e764d1937b853fe97db4869a7554d90262))
* **sub_23_declaration:** add status validated ([8690964](https://gitlab.com/fi-sas/fisas-bus/commit/86909649efcd5dac8c53e546aa797e008954fa64))
* **sub_23_machine:** update status ([6211eec](https://gitlab.com/fi-sas/fisas-bus/commit/6211eecc0dfa1f66f3852d5ef7de3de78aad4e38))


### Features

* **applications:** add cron job ana accept/reject action ([1df11fd](https://gitlab.com/fi-sas/fisas-bus/commit/1df11fdb174d4a750b11adfb21b56716a2623093))
* **configurations:** add new configurations ([f9a4531](https://gitlab.com/fi-sas/fisas-bus/commit/f9a4531db07e327658942f8e6a11e9f37339cc6e))
* **migrations:** sub23 update status ([04f2a5a](https://gitlab.com/fi-sas/fisas-bus/commit/04f2a5ab6e02fe56f07380307f9401e66e92845c))
* **migrations:** update status application ([704a75c](https://gitlab.com/fi-sas/fisas-bus/commit/704a75c2c3fd3400f4be659986c9170ab19fb4b1))

## [1.4.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.4.0...v1.4.1) (2021-08-05)


### Bug Fixes

* **routes:** add clearCache ([0f57265](https://gitlab.com/fi-sas/fisas-bus/commit/0f57265d89502f08366de4e82599ba639f43b642))
* **timetables:** reorganize clear cache ([2168421](https://gitlab.com/fi-sas/fisas-bus/commit/2168421ed94fd21b13f730afbdfd6c6391fdfb39))

# [1.4.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.3.0...v1.4.0) (2021-07-28)


### Bug Fixes

* **days_group:** optimize code ([dc442d1](https://gitlab.com/fi-sas/fisas-bus/commit/dc442d1924d0aa018fe64c1f5108f8e1370f55ca))
* **days_groups:** add group_id to response ([57ba578](https://gitlab.com/fi-sas/fisas-bus/commit/57ba57844f4f28b863d4f30409f29665fb24242d))
* **hours:** add delete validations ([6496ce7](https://gitlab.com/fi-sas/fisas-bus/commit/6496ce7a13f33d539c42904875301041ce38acc2))
* **local_zones:** remove all local zones ([f2587a0](https://gitlab.com/fi-sas/fisas-bus/commit/f2587a0501178a88583fff29738172111d25ce14))
* **timetables:** remove validation ([370db5d](https://gitlab.com/fi-sas/fisas-bus/commit/370db5df7f951d8bc0137cff7d383024a5fbe911))
* **type_days:** add validation remove day ([8c5336a](https://gitlab.com/fi-sas/fisas-bus/commit/8c5336a99b61b70d86ceeb9ea3fb49a0d1037bf0))
* **zones:** remove action ([b8d4b75](https://gitlab.com/fi-sas/fisas-bus/commit/b8d4b75251aec17f4e60e2ae170e6fa84ffde40c))


### Features

* **type_days:** add clearcache ([6bb2e39](https://gitlab.com/fi-sas/fisas-bus/commit/6bb2e39e24cdd81933a6bb29dedf6ad547335315))

# [1.3.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.2.0...v1.3.0) (2021-07-23)


### Bug Fixes

* **links:** error message ([dcf876a](https://gitlab.com/fi-sas/fisas-bus/commit/dcf876ae4189319e564525e6ceddf36d11ca766f))
* **route_search:** problema pesquisa por rotas ([9510eb0](https://gitlab.com/fi-sas/fisas-bus/commit/9510eb0c2ea1b96bfced59ed419acb67af9b3f6a))
* **search_logs:** remove positive validation ([2b6bcdf](https://gitlab.com/fi-sas/fisas-bus/commit/2b6bcdfc037b371f33f87aa881bc34d403cda440))


### Features

* **links:** add validation same link ([2cfb4db](https://gitlab.com/fi-sas/fisas-bus/commit/2cfb4db6a69e0251a8547a5bd7f77f35ecb87406))

# [1.2.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.1.0...v1.2.0) (2021-07-07)


### Features

* **absences:** add absences requests to suspend application ([9f010ce](https://gitlab.com/fi-sas/fisas-bus/commit/9f010cec448169c955c869fff0f43d8fab3109aa))

# [1.1.0](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.11...v1.1.0) (2021-07-01)


### Bug Fixes

* **sub23_declarations:** change sub23 applications to sub23 declarations ([af55206](https://gitlab.com/fi-sas/fisas-bus/commit/af552060538e40a7bfd23fc24cbc2969624bc418))


### Features

* **sub23_applications:** add sub 23 applications ([1d6c614](https://gitlab.com/fi-sas/fisas-bus/commit/1d6c61492bf8b4f307b0f2ca55adbceff32de8e0))

## [1.0.11](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.10...v1.0.11) (2021-06-30)


### Bug Fixes

* **prices:** change scopes of service ([6801628](https://gitlab.com/fi-sas/fisas-bus/commit/6801628b0ecb8f185d82e2d9d6d28e47f36edd96))
* **withdrawals:** fix scope ([e65ec89](https://gitlab.com/fi-sas/fisas-bus/commit/e65ec89bf3dc1dce1e1fddeaa52b09cb681608d1))

## [1.0.10](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.9...v1.0.10) (2021-06-24)


### Bug Fixes

* **applications:** fix application states and add notifications ([8ed6b26](https://gitlab.com/fi-sas/fisas-bus/commit/8ed6b26c3f537b550121b6a375eec2c09b0210ae))

## [1.0.9](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.8...v1.0.9) (2021-06-23)


### Bug Fixes

* **withdrawals:** change withdrawals ([9d0a7f8](https://gitlab.com/fi-sas/fisas-bus/commit/9d0a7f8a4d7268a330ebdc903a78147e8df1e266))

## [1.0.8](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.7...v1.0.8) (2021-06-22)


### Bug Fixes

* **applications:** add applicaitons document_type_id ([4c5c123](https://gitlab.com/fi-sas/fisas-bus/commit/4c5c12328c114f0a2bb29e96a80c014abfa9e19e))

## [1.0.7](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.6...v1.0.7) (2021-06-18)


### Bug Fixes

* **applications:** generate applicaiton declaration ([b12ecb5](https://gitlab.com/fi-sas/fisas-bus/commit/b12ecb56ac5e1c4f722f4564dec9f21a8c51026b))

## [1.0.6](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.5...v1.0.6) (2021-06-18)


### Bug Fixes

* **prices:** change clearCache method ([929b106](https://gitlab.com/fi-sas/fisas-bus/commit/929b10611b22491f20e91904260d8655a5df7c5f))
* **prices:** remove required account_id and clear cache ([35efad2](https://gitlab.com/fi-sas/fisas-bus/commit/35efad2c7e9e2c35016410be25c204de90c90ed4))

## [1.0.5](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.4...v1.0.5) (2021-06-15)


### Bug Fixes

* **applications:** add /cancel endpoint ([0ebcd3f](https://gitlab.com/fi-sas/fisas-bus/commit/0ebcd3f3be1c173ed7d2df3a5736da4465800783))

## [1.0.4](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.3...v1.0.4) (2021-05-05)


### Bug Fixes

* **application:** fix withdrawal create ([65bee15](https://gitlab.com/fi-sas/fisas-bus/commit/65bee153cac3340e15380dfb99462cd2782cc8c9))

## [1.0.3](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.2...v1.0.3) (2021-05-04)


### Bug Fixes

* **applications:** filter applications by user_id on webpage requests ([49754bb](https://gitlab.com/fi-sas/fisas-bus/commit/49754bb6104dad7ac42994dd92c8a0efe49e7084))

## [1.0.2](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.1...v1.0.2) (2021-04-06)


### Bug Fixes

* **route_search:** fix array first in getRoutesInTrees ([35ee955](https://gitlab.com/fi-sas/fisas-bus/commit/35ee955c8ccdeb310252d82dd4fefe1d366c9751))
* **route_search:** fix undefined findRouteInTreeBranches ([184f950](https://gitlab.com/fi-sas/fisas-bus/commit/184f950ae642a3fe3485843d65b605c10599a8a5))

## [1.0.1](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0...v1.0.1) (2021-04-06)


### Bug Fixes

* **applications:** get user id from meta ([8cf31ec](https://gitlab.com/fi-sas/fisas-bus/commit/8cf31ec11f201cf41d1b7cd077956db57c5e0b5a))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **application:** fix menssage error ([1a3fe23](https://gitlab.com/fi-sas/fisas-bus/commit/1a3fe2384f4ff3d19d7b767fb711e2b19e39ae92))
* **application_history:** add field ([60c994a](https://gitlab.com/fi-sas/fisas-bus/commit/60c994a8b669d0490acc1935a3f58312f2ce00df))
* **applications:** add course id ([93a520d](https://gitlab.com/fi-sas/fisas-bus/commit/93a520d6ade58305d726cb64e441f3443216d1a7))
* **applications:** add validations for enternal data ([78d7bd0](https://gitlab.com/fi-sas/fisas-bus/commit/78d7bd0e77d6a82df060615f6512c9ce9ea40037))
* **applications:** fix withRelateds ([5afcf5c](https://gitlab.com/fi-sas/fisas-bus/commit/5afcf5c74e7905f60e32cb48521382c7b2481e4a))
* **applications:** withRelated ([389a0de](https://gitlab.com/fi-sas/fisas-bus/commit/389a0de2b63da0c6a9fa365895a83ed10c6948ff))
* **bus:** fix bugs in services ([ca8fcea](https://gitlab.com/fi-sas/fisas-bus/commit/ca8fcea136c99ec547bfe345936e655f5af2455a))
* **dockerfile:** change node version for 12 buster slim ([6dd6a67](https://gitlab.com/fi-sas/fisas-bus/commit/6dd6a6750efa0857f0753be695a76eb5f91af09a))
* **env:** change debugger port for 59082 ([be7df19](https://gitlab.com/fi-sas/fisas-bus/commit/be7df1936d673a90fb8a33049f38f91a31b18cbf))
* **hours:** fix find hours of timtable ([d949eae](https://gitlab.com/fi-sas/fisas-bus/commit/d949eae97ec228bd209eb398a513e864608b6f17))
* **links:** fix link action ([a3ed45b](https://gitlab.com/fi-sas/fisas-bus/commit/a3ed45bbdf05c7c615522c1c7dbe756b0f26db35))
* **linter:** fix linter errors ([75f2ef1](https://gitlab.com/fi-sas/fisas-bus/commit/75f2ef1369bc4f76556e8d4e67f99b2d5aeb16a2))
* **linter:** fix linter in configurations seeds ([46d75a2](https://gitlab.com/fi-sas/fisas-bus/commit/46d75a2fb5d1ab44728fc053853e14c7a56648e4))
* **local_zone:** save zone id ([92ef48c](https://gitlab.com/fi-sas/fisas-bus/commit/92ef48c4195428be181de396c3348fdce32e2af9))
* **migarations:** fix configurations ([faa8e05](https://gitlab.com/fi-sas/fisas-bus/commit/faa8e059bc48f31dff09e89056681b117520c066))
* **migrations:** add table hours ([c1ea82b](https://gitlab.com/fi-sas/fisas-bus/commit/c1ea82bbc4dd1c85fcac3c3bab28dc60d3518175))
* **migrations:** fix price tables ([3ec1726](https://gitlab.com/fi-sas/fisas-bus/commit/3ec172674582797b60f65ae9b31c5506f05dc18f))
* **migrations:** update database ([c4a3d85](https://gitlab.com/fi-sas/fisas-bus/commit/c4a3d858a8cfc237d844e96ab83c25a715462065))
* **package:** update fisa core ([9e8b64d](https://gitlab.com/fi-sas/fisas-bus/commit/9e8b64d7cd42cefe826d6d45b16a392ce1b3e5ad))
* **payment_month:** add validation for month and year ([517eebb](https://gitlab.com/fi-sas/fisas-bus/commit/517eebb7214cf009390bd5b0f1f6df099a20a702))
* **price_tables:** set validation and withRelated for account ([cb40339](https://gitlab.com/fi-sas/fisas-bus/commit/cb403393771f8bfaff2dc682f45ddd9e6ab030e4))
* **route_search:** add routes total ([c4528ad](https://gitlab.com/fi-sas/fisas-bus/commit/c4528ad475b8e2009a2d3df5af632021b996b2be))
* **route_search:** fix issues in search routes ([c704ef9](https://gitlab.com/fi-sas/fisas-bus/commit/c704ef9c3e2bf608429b1b1d3655bbbfe3ad761a))
* **route_search:** fix trace route ([0dfdcc1](https://gitlab.com/fi-sas/fisas-bus/commit/0dfdcc1bcc7c8772c6c69fbf8d4a032636ff53c2))
* **seeds:** add updated_at and created_at ([208607e](https://gitlab.com/fi-sas/fisas-bus/commit/208607e2980458d59eb4ed4496b08170a47fa0c8))
* **seeds:** check if items exist in seeds ([812d013](https://gitlab.com/fi-sas/fisas-bus/commit/812d013e6337064fe6a5acc37e8c6f4e3b401895))
* **seeds:** fix configurations primary key ([c3c4b8e](https://gitlab.com/fi-sas/fisas-bus/commit/c3c4b8e58ba25c5da1633d7eca4b3a528e29f55b))
* **tickets_bought:** remove lint errors ([6f2cdcd](https://gitlab.com/fi-sas/fisas-bus/commit/6f2cdcd9f96371f26d016c9c62f766c4ae6de0e4))
* add usr validation to tickets bought ([24580ed](https://gitlab.com/fi-sas/fisas-bus/commit/24580edb08abac381f8900c43725943afdd555c4))
* knexfile ([812b578](https://gitlab.com/fi-sas/fisas-bus/commit/812b578db119946b5ebb179331d1dde21ab67229))
* validation school_id ([955dc10](https://gitlab.com/fi-sas/fisas-bus/commit/955dc1002e82c68eb26c6b27266177ea3362bc5b))
* **routes:** add withRelated for links ([0650fc6](https://gitlab.com/fi-sas/fisas-bus/commit/0650fc635db69dce2266cfcafd3f797c4e1d1562))
* **routes:** fix withRelateds ([64422e8](https://gitlab.com/fi-sas/fisas-bus/commit/64422e8964f7820d4f3e0a9b277433cd235b4fcd))
* **services:** remove validation for created_at and updated_at ([5c03162](https://gitlab.com/fi-sas/fisas-bus/commit/5c03162da47c426cddf2d0ed338f02c6ea8b10ad))
* **ticket-config:** remove created at ([9cd66aa](https://gitlab.com/fi-sas/fisas-bus/commit/9cd66aa0cedc7bba73298a5f47accdab71d4a3f0))
* **tickets_bought:** fix the fields and add withRelateds ([d77f49f](https://gitlab.com/fi-sas/fisas-bus/commit/d77f49fce7d98ab7c088f478e0f766ec0ce4ce57))
* **tickets_bought:** validation for tickets ([8098e57](https://gitlab.com/fi-sas/fisas-bus/commit/8098e5731cae684bdc30f4fdf2e46ef13873accd))
* **timetable:** fix create ([e335b5b](https://gitlab.com/fi-sas/fisas-bus/commit/e335b5b50a20b956a7a532201f9ddbe39ebb2411))
* **timetable:** validation for array hours order ([2e64a4e](https://gitlab.com/fi-sas/fisas-bus/commit/2e64a4e7fdd4e410ac3a8a6c2acf0f2ba02e5058))
* **type_day:** fix add day and remove day ([2465b45](https://gitlab.com/fi-sas/fisas-bus/commit/2465b456122991bb55d0d5792bde33779eeabdce))
* **zones:** add withRelateds ([4d7451a](https://gitlab.com/fi-sas/fisas-bus/commit/4d7451a5e78597d0303ca98bd4fc675784195283))
* **zones:** remove route zone service ([cb78314](https://gitlab.com/fi-sas/fisas-bus/commit/cb78314bc566fa42d9c1a5df43bf019a670d5a80))


### Features

* **bus:** add new features ([761deee](https://gitlab.com/fi-sas/fisas-bus/commit/761deeeb0b76860450cb6a85b8f003e74b83bb52))
* **bus:** create locals route and apllications ([c9a45f4](https://gitlab.com/fi-sas/fisas-bus/commit/c9a45f41ad79e1d6b654e698477ed1f6e4a6e96b))
* **configuration:** add endpoint to update and get configuration ([10e521f](https://gitlab.com/fi-sas/fisas-bus/commit/10e521f6b43f7aa5ddab1e0a6d16b94a74f6da69))
* **configurations:** add service for configurations ([fe5c57a](https://gitlab.com/fi-sas/fisas-bus/commit/fe5c57a9e7f43e51aa5a8bb224dfc6198a58c89f))
* **database:** set migrations and seeds ([0e5c611](https://gitlab.com/fi-sas/fisas-bus/commit/0e5c611989dd6138b34a72ad5952672f497f9111))
* **editorconfig:** add configurations ([9636b31](https://gitlab.com/fi-sas/fisas-bus/commit/9636b3120adee9e4d1ef7152e943ff44bf34f050))
* **hours:** add service for hours ([e374e85](https://gitlab.com/fi-sas/fisas-bus/commit/e374e85e21c320c8117a0a98a6edfbb86a5bc0a4))
* **links:** create service for links ([c52da41](https://gitlab.com/fi-sas/fisas-bus/commit/c52da41a641df1ee02f7a5b3e8f3221dd1d6f2a5))
* **logs_search:** create service for log search ([538065f](https://gitlab.com/fi-sas/fisas-bus/commit/538065f6e16d121581e055a9e87f23bf693010ca))
* **package:** add new commands ([09853a8](https://gitlab.com/fi-sas/fisas-bus/commit/09853a8f6695c12b83ff9cebb5d00287adaf891c))
* **places:** create service for places ([94977e7](https://gitlab.com/fi-sas/fisas-bus/commit/94977e7d88496b7019de5724d0c741e6624309bb))
* **prettie:** add configurations ([6c46921](https://gitlab.com/fi-sas/fisas-bus/commit/6c46921396a42dcf418800d269af76a60690a69c))
* **route order:** add new action for validate local in route ([344a910](https://gitlab.com/fi-sas/fisas-bus/commit/344a91047b68fef0378fc6d96cf798c84928127e))
* **route search:** add search for link routes ([e2babab](https://gitlab.com/fi-sas/fisas-bus/commit/e2babab3f7aa2ee05231c55c0d3c775c3144dbd2))
* **route_search:** add calc prices ([2134105](https://gitlab.com/fi-sas/fisas-bus/commit/2134105ca9b5985899aef65c26c3f11b86a80356))
* **route_search:** save route log ([2ee4094](https://gitlab.com/fi-sas/fisas-bus/commit/2ee4094349c6ac1dff6f63cd0536cdcb6742ccc8))
* **route_zone:** create service for route_zone ([a6a8c2d](https://gitlab.com/fi-sas/fisas-bus/commit/a6a8c2d98db264169d6f6246771aab68d1315b60))
* **routes:** add new withRelated linkOut and linkIn ([7db2f34](https://gitlab.com/fi-sas/fisas-bus/commit/7db2f348b325a80ba03467fbf92202c3c86de0f2))
* **seasons:** create service for seasons ([9e0896e](https://gitlab.com/fi-sas/fisas-bus/commit/9e0896ed6ccd9e27bd4a9659f26c7b5e747b6dd1))
* **tickets_bought:** add webpage widget endpoint ([6a60e8c](https://gitlab.com/fi-sas/fisas-bus/commit/6a60e8c8c11b28e9ed38baa19e54ae9848c0e5f1))
* **timetable:** add service to create timetable ([0000f5d](https://gitlab.com/fi-sas/fisas-bus/commit/0000f5d7560988a49c44eea80386dd3825394431))
* **timetable:** insert, delete line and update hour ([7f9cbfb](https://gitlab.com/fi-sas/fisas-bus/commit/7f9cbfb51830822f55c96d10a1995d7bc5977289))
* **types_days:** add day and remove ([4fd044c](https://gitlab.com/fi-sas/fisas-bus/commit/4fd044c1d5a8e742b71d560a41a135efa792f8c3))
* add applications withdrawals and state-machine ([c539772](https://gitlab.com/fi-sas/fisas-bus/commit/c5397723f6546a33133660651fc55022f6b43847))
* add state-machine ([795f10a](https://gitlab.com/fi-sas/fisas-bus/commit/795f10a6c7ef897c58727b2a6d991eaf266e6b5d))
* **type-days:** create services for type days ([6ae3a01](https://gitlab.com/fi-sas/fisas-bus/commit/6ae3a0179b2c9804ff4cab10931ca0134f219edd))
* **zones:** create service for zones ([e606c21](https://gitlab.com/fi-sas/fisas-bus/commit/e606c21efb12779d58212ac6e1c6181588a1545e))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-03-09)


### Bug Fixes

* **tickets_bought:** remove lint errors ([6f2cdcd](https://gitlab.com/fi-sas/fisas-bus/commit/6f2cdcd9f96371f26d016c9c62f766c4ae6de0e4))


### Features

* **tickets_bought:** add webpage widget endpoint ([6a60e8c](https://gitlab.com/fi-sas/fisas-bus/commit/6a60e8c8c11b28e9ed38baa19e54ae9848c0e5f1))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-02-10)


### Bug Fixes

* **seeds:** fix configurations primary key ([c3c4b8e](https://gitlab.com/fi-sas/fisas-bus/commit/c3c4b8e58ba25c5da1633d7eca4b3a528e29f55b))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-02-09)


### Bug Fixes

* **seeds:** check if items exist in seeds ([812d013](https://gitlab.com/fi-sas/fisas-bus/commit/812d013e6337064fe6a5acc37e8c6f4e3b401895))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-02-08)


### Bug Fixes

* validation school_id ([955dc10](https://gitlab.com/fi-sas/fisas-bus/commit/955dc1002e82c68eb26c6b27266177ea3362bc5b))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-02-05)


### Bug Fixes

* knexfile ([812b578](https://gitlab.com/fi-sas/fisas-bus/commit/812b578db119946b5ebb179331d1dde21ab67229))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-01-13)


### Bug Fixes

* **route_search:** fix issues in search routes ([c704ef9](https://gitlab.com/fi-sas/fisas-bus/commit/c704ef9c3e2bf608429b1b1d3655bbbfe3ad761a))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-bus/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-05)


### Bug Fixes

* add usr validation to tickets bought ([24580ed](https://gitlab.com/fi-sas/fisas-bus/commit/24580edb08abac381f8900c43725943afdd555c4))

# 1.0.0-rc.1 (2020-12-18)


### Bug Fixes

* **application:** fix menssage error ([1a3fe23](https://gitlab.com/fi-sas/fisas-bus/commit/1a3fe2384f4ff3d19d7b767fb711e2b19e39ae92))
* **application_history:** add field ([60c994a](https://gitlab.com/fi-sas/fisas-bus/commit/60c994a8b669d0490acc1935a3f58312f2ce00df))
* **applications:** add course id ([93a520d](https://gitlab.com/fi-sas/fisas-bus/commit/93a520d6ade58305d726cb64e441f3443216d1a7))
* **applications:** add validations for enternal data ([78d7bd0](https://gitlab.com/fi-sas/fisas-bus/commit/78d7bd0e77d6a82df060615f6512c9ce9ea40037))
* **applications:** fix withRelateds ([5afcf5c](https://gitlab.com/fi-sas/fisas-bus/commit/5afcf5c74e7905f60e32cb48521382c7b2481e4a))
* **applications:** withRelated ([389a0de](https://gitlab.com/fi-sas/fisas-bus/commit/389a0de2b63da0c6a9fa365895a83ed10c6948ff))
* **bus:** fix bugs in services ([ca8fcea](https://gitlab.com/fi-sas/fisas-bus/commit/ca8fcea136c99ec547bfe345936e655f5af2455a))
* **dockerfile:** change node version for 12 buster slim ([6dd6a67](https://gitlab.com/fi-sas/fisas-bus/commit/6dd6a6750efa0857f0753be695a76eb5f91af09a))
* **env:** change debugger port for 59082 ([be7df19](https://gitlab.com/fi-sas/fisas-bus/commit/be7df1936d673a90fb8a33049f38f91a31b18cbf))
* **hours:** fix find hours of timtable ([d949eae](https://gitlab.com/fi-sas/fisas-bus/commit/d949eae97ec228bd209eb398a513e864608b6f17))
* **links:** fix link action ([a3ed45b](https://gitlab.com/fi-sas/fisas-bus/commit/a3ed45bbdf05c7c615522c1c7dbe756b0f26db35))
* **linter:** fix linter errors ([75f2ef1](https://gitlab.com/fi-sas/fisas-bus/commit/75f2ef1369bc4f76556e8d4e67f99b2d5aeb16a2))
* **linter:** fix linter in configurations seeds ([46d75a2](https://gitlab.com/fi-sas/fisas-bus/commit/46d75a2fb5d1ab44728fc053853e14c7a56648e4))
* **local_zone:** save zone id ([92ef48c](https://gitlab.com/fi-sas/fisas-bus/commit/92ef48c4195428be181de396c3348fdce32e2af9))
* **migarations:** fix configurations ([faa8e05](https://gitlab.com/fi-sas/fisas-bus/commit/faa8e059bc48f31dff09e89056681b117520c066))
* **migrations:** add table hours ([c1ea82b](https://gitlab.com/fi-sas/fisas-bus/commit/c1ea82bbc4dd1c85fcac3c3bab28dc60d3518175))
* **migrations:** fix price tables ([3ec1726](https://gitlab.com/fi-sas/fisas-bus/commit/3ec172674582797b60f65ae9b31c5506f05dc18f))
* **migrations:** update database ([c4a3d85](https://gitlab.com/fi-sas/fisas-bus/commit/c4a3d858a8cfc237d844e96ab83c25a715462065))
* **package:** update fisa core ([9e8b64d](https://gitlab.com/fi-sas/fisas-bus/commit/9e8b64d7cd42cefe826d6d45b16a392ce1b3e5ad))
* **payment_month:** add validation for month and year ([517eebb](https://gitlab.com/fi-sas/fisas-bus/commit/517eebb7214cf009390bd5b0f1f6df099a20a702))
* **price_tables:** set validation and withRelated for account ([cb40339](https://gitlab.com/fi-sas/fisas-bus/commit/cb403393771f8bfaff2dc682f45ddd9e6ab030e4))
* **route_search:** add routes total ([c4528ad](https://gitlab.com/fi-sas/fisas-bus/commit/c4528ad475b8e2009a2d3df5af632021b996b2be))
* **route_search:** fix trace route ([0dfdcc1](https://gitlab.com/fi-sas/fisas-bus/commit/0dfdcc1bcc7c8772c6c69fbf8d4a032636ff53c2))
* **routes:** add withRelated for links ([0650fc6](https://gitlab.com/fi-sas/fisas-bus/commit/0650fc635db69dce2266cfcafd3f797c4e1d1562))
* **routes:** fix withRelateds ([64422e8](https://gitlab.com/fi-sas/fisas-bus/commit/64422e8964f7820d4f3e0a9b277433cd235b4fcd))
* **seeds:** add updated_at and created_at ([208607e](https://gitlab.com/fi-sas/fisas-bus/commit/208607e2980458d59eb4ed4496b08170a47fa0c8))
* **services:** remove validation for created_at and updated_at ([5c03162](https://gitlab.com/fi-sas/fisas-bus/commit/5c03162da47c426cddf2d0ed338f02c6ea8b10ad))
* **ticket-config:** remove created at ([9cd66aa](https://gitlab.com/fi-sas/fisas-bus/commit/9cd66aa0cedc7bba73298a5f47accdab71d4a3f0))
* **tickets_bought:** fix the fields and add withRelateds ([d77f49f](https://gitlab.com/fi-sas/fisas-bus/commit/d77f49fce7d98ab7c088f478e0f766ec0ce4ce57))
* **tickets_bought:** validation for tickets ([8098e57](https://gitlab.com/fi-sas/fisas-bus/commit/8098e5731cae684bdc30f4fdf2e46ef13873accd))
* **timetable:** fix create ([e335b5b](https://gitlab.com/fi-sas/fisas-bus/commit/e335b5b50a20b956a7a532201f9ddbe39ebb2411))
* **timetable:** validation for array hours order ([2e64a4e](https://gitlab.com/fi-sas/fisas-bus/commit/2e64a4e7fdd4e410ac3a8a6c2acf0f2ba02e5058))
* **type_day:** fix add day and remove day ([2465b45](https://gitlab.com/fi-sas/fisas-bus/commit/2465b456122991bb55d0d5792bde33779eeabdce))
* **zones:** add withRelateds ([4d7451a](https://gitlab.com/fi-sas/fisas-bus/commit/4d7451a5e78597d0303ca98bd4fc675784195283))
* **zones:** remove route zone service ([cb78314](https://gitlab.com/fi-sas/fisas-bus/commit/cb78314bc566fa42d9c1a5df43bf019a670d5a80))


### Features

* **bus:** add new features ([761deee](https://gitlab.com/fi-sas/fisas-bus/commit/761deeeb0b76860450cb6a85b8f003e74b83bb52))
* **bus:** create locals route and apllications ([c9a45f4](https://gitlab.com/fi-sas/fisas-bus/commit/c9a45f41ad79e1d6b654e698477ed1f6e4a6e96b))
* **configuration:** add endpoint to update and get configuration ([10e521f](https://gitlab.com/fi-sas/fisas-bus/commit/10e521f6b43f7aa5ddab1e0a6d16b94a74f6da69))
* **configurations:** add service for configurations ([fe5c57a](https://gitlab.com/fi-sas/fisas-bus/commit/fe5c57a9e7f43e51aa5a8bb224dfc6198a58c89f))
* **database:** set migrations and seeds ([0e5c611](https://gitlab.com/fi-sas/fisas-bus/commit/0e5c611989dd6138b34a72ad5952672f497f9111))
* **editorconfig:** add configurations ([9636b31](https://gitlab.com/fi-sas/fisas-bus/commit/9636b3120adee9e4d1ef7152e943ff44bf34f050))
* **hours:** add service for hours ([e374e85](https://gitlab.com/fi-sas/fisas-bus/commit/e374e85e21c320c8117a0a98a6edfbb86a5bc0a4))
* **links:** create service for links ([c52da41](https://gitlab.com/fi-sas/fisas-bus/commit/c52da41a641df1ee02f7a5b3e8f3221dd1d6f2a5))
* **logs_search:** create service for log search ([538065f](https://gitlab.com/fi-sas/fisas-bus/commit/538065f6e16d121581e055a9e87f23bf693010ca))
* **package:** add new commands ([09853a8](https://gitlab.com/fi-sas/fisas-bus/commit/09853a8f6695c12b83ff9cebb5d00287adaf891c))
* **places:** create service for places ([94977e7](https://gitlab.com/fi-sas/fisas-bus/commit/94977e7d88496b7019de5724d0c741e6624309bb))
* **prettie:** add configurations ([6c46921](https://gitlab.com/fi-sas/fisas-bus/commit/6c46921396a42dcf418800d269af76a60690a69c))
* **route order:** add new action for validate local in route ([344a910](https://gitlab.com/fi-sas/fisas-bus/commit/344a91047b68fef0378fc6d96cf798c84928127e))
* **route search:** add search for link routes ([e2babab](https://gitlab.com/fi-sas/fisas-bus/commit/e2babab3f7aa2ee05231c55c0d3c775c3144dbd2))
* **route_search:** add calc prices ([2134105](https://gitlab.com/fi-sas/fisas-bus/commit/2134105ca9b5985899aef65c26c3f11b86a80356))
* **route_search:** save route log ([2ee4094](https://gitlab.com/fi-sas/fisas-bus/commit/2ee4094349c6ac1dff6f63cd0536cdcb6742ccc8))
* **route_zone:** create service for route_zone ([a6a8c2d](https://gitlab.com/fi-sas/fisas-bus/commit/a6a8c2d98db264169d6f6246771aab68d1315b60))
* **routes:** add new withRelated linkOut and linkIn ([7db2f34](https://gitlab.com/fi-sas/fisas-bus/commit/7db2f348b325a80ba03467fbf92202c3c86de0f2))
* **seasons:** create service for seasons ([9e0896e](https://gitlab.com/fi-sas/fisas-bus/commit/9e0896ed6ccd9e27bd4a9659f26c7b5e747b6dd1))
* **timetable:** add service to create timetable ([0000f5d](https://gitlab.com/fi-sas/fisas-bus/commit/0000f5d7560988a49c44eea80386dd3825394431))
* **timetable:** insert, delete line and update hour ([7f9cbfb](https://gitlab.com/fi-sas/fisas-bus/commit/7f9cbfb51830822f55c96d10a1995d7bc5977289))
* **types_days:** add day and remove ([4fd044c](https://gitlab.com/fi-sas/fisas-bus/commit/4fd044c1d5a8e742b71d560a41a135efa792f8c3))
* add applications withdrawals and state-machine ([c539772](https://gitlab.com/fi-sas/fisas-bus/commit/c5397723f6546a33133660651fc55022f6b43847))
* add state-machine ([795f10a](https://gitlab.com/fi-sas/fisas-bus/commit/795f10a6c7ef897c58727b2a6d991eaf266e6b5d))
* **type-days:** create services for type days ([6ae3a01](https://gitlab.com/fi-sas/fisas-bus/commit/6ae3a0179b2c9804ff4cab10931ca0134f219edd))
* **zones:** create service for zones ([e606c21](https://gitlab.com/fi-sas/fisas-bus/commit/e606c21efb12779d58212ac6e1c6181588a1545e))
