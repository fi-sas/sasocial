"use strict";
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/routes');

//Novas entradas
var pretty = require('express-prettify');
var cors = require('cors');
var log4js = require('log4js');
var app = express();

var swaggerUi = require('swagger-ui-express');
var YAML = require('yamljs');
var swaggerDocument = require('./api/swagger/swagger.json');

var response = require('./libs/config_response');
var logger = require('./logger');

require('dotenv').config();

app.use(log4js.connectLogger(logger, { level: 'auto' }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(pretty({ query: 'pretty' }));

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(routes);

app.use(function (err, req, res, next) {
    // if (err) {
        console.log('Invalid Request data')
        res.status(500).json(response.response_error([{"code": 500, "message": err.message}]));
    // } else {
    //     next()
    // }
});

process.on('uncaughtException', function(err) {

    console.log('Uncaught Exception-----------------------------------')
    console.error("Error:", err);
    process.exit(1) //mandatory (as per the Node docs)
})

app.use(function(req, res) {
    res.status(404).json(response.response_error(
        {code: 404,
                message: req.originalUrl + ' not found'
        }));
});

module.exports = app;
