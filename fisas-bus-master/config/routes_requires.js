const period = require('../controllers/back_office/period_controller');
const contact = require('../controllers/back_office/contact_controller');
const local = require('../controllers/back_office/local_controller');
const route = require('../controllers/back_office/route_controller');
const type_day = require('../controllers/back_office/type_day_controller');
const ticket_type = require('../controllers/back_office/ticket_type_controller');
const currency = require('../controllers/back_office/currency_controller');
const season = require('../controllers/back_office/season_controller');
const type_user = require('../controllers/back_office/type_user_controller');
const timetable = require('../controllers/back_office/timetable_controller');
const price = require('../controllers/back_office/price_controller');
const zone = require('../controllers/back_office/zone_controller');
const link = require('../controllers/back_office/link_controller');
const ticket_config = require('../controllers/back_office/ticket_config_controller');
const ticket_bought = require('../controllers/back_office/ticket_bought_controller');
const configuration = require('../controllers/back_office/configuration_controller');
const configurations = require('../controllers/back_office/configurations_controller');
const applications = require('../controllers/applications_controller');
const payment_months = require('../controllers/payment_months_controller')

module.exports = {
period,
contact,
local,
route,
type_day,
ticket_type,
currency,
season,
type_user,
timetable,
price,
zone,
link,
ticket_config,
ticket_bought,
configuration,
applications,
payment_months,
configurations
};