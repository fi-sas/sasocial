"use strict";
var applications_database = require('../models/applications_model');
var local_database = require('../models/back_office/local_model')
var response  = require('../libs/config_response');
var _ = require('lodash');
var pagination = require('../libs/get_pagination');
var params_validation =require('../libs/params_validation');
var moment = require('moment');


function parseBoolean(data, boolProps) {

  for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < boolProps.length; j++) {
        if ([1, 0].includes(data[i][boolProps[j]])) {
            data[i][boolProps[j]] = (data[i][boolProps[j]] === 1);
        }
    }
  }
  return data;
};


/**
 * 
 * @param req 
 * @param res
 * @description Mostra as candidaturas
 */
exports.get_applications = function(req, res, next) {

    applications_database.get_applications(req.limit, req.offset,req.sortProp, req.sort, req.where, req.withRelated,function(applications, total, error) {

        if (!error) {

            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                if (applications.length === 0) res.status(200).json(response.response_success(links, applications,[]));

                req.applications = parseBoolean(applications, ['renovation', 'accept_procedure']);
                req.links = links;

                next();

                /* res.status(200).json(response.response_success(links,applications,[])); */
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra uma candidatura
 */
exports.get_one_application = function(req, res, next) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    let where_user = '';

    if (!req.device !== 'BO') {
        where_user = 'AND user_id = '+ req.user.id;
    }   

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            applications_database.get_one_application(params_out.application_id, where_user,function(application, error) {
                    if (!error) {

                        if (application.length === 0) res.status(200).json(response.response_success({total:application.length},application,[]));

                        req.applications = parseBoolean([application], ['renovation', 'accept_procedure']);
                        req.links = {};
        
                        next();

                      /*   res.status(200).json(response.response_success({total:application.length},application,[])); */
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @desc Montra um desitência de uma candidatura
 */
exports.get_withdrawal_application = function(req, res, next) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            applications_database.get_withdrawal_application(params_out.application_id, function(withdrawal, error) {
                    if (!error) {
                      res.status(200).json(response.response_success({total:withdrawal.length},withdrawal,[]));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


/**
 *
 * @param req
 * @param res
 * @description Insere um candidatura
 */
exports.insert_application = function(req, res) {

   var params = [
        {
            "param_name" : "name",
            "value" : req.body.name,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "birth_date",
            "value" : req.body.birth_date,
            "required": true,
            "type": "string",
            "sub_type": "date",
            "default": ""
        },
        {
            "param_name" : "tin",
            "value" : req.body.tin,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "identification",
            "value" : req.body.identification,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "nationality",
            "value" : req.body.nationality,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "course_year",
            "value" : req.body.course_year,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "date",
            "value" : req.body.date,
            "required": false,
            "type": "string",
            "sub_type":"date",
            "default": ""
        },
        {
            "param_name" : "email",
            "value" : req.body.email,
            "required": true,
            "type": "string",
            "sub_type":"email",
            "default": ""
        },
        {
            "param_name" : "phone_1",
            "value" : req.body.phone_1,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "phone_2",
            "value" : req.body.phone_2,
            "required": false,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "course_id",
            "value" : req.body.course_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "renovation",
            "value" : req.body.renovation,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        },
        {
            "param_name" : "school_id",
            "value" : req.body.school_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "observations",
            "value" : req.body.observations,
            "required": false,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "accept_procedure",
            "value" : req.body.accept_procedure,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        },
        {
            "param_name" : "morning_local_id_from",
            "value" : req.body.morning_local_id_from,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "morning_local_id_to",
            "value" : req.body.morning_local_id_to,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "afternoon_local_id_from",
            "value" : req.body.afternoon_local_id_from,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "afternoon_local_id_to",
            "value" : req.body.afternoon_local_id_to,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "student_number",
            "value" : req.body.student_number,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
    ]

    var status = "submitted";
    var created_at = moment().toISOString();
    var updated_at = moment().toISOString();
    var user_id =  req.user.id;
   
    var locals = [];
    
    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            locals = [params_out.morning_local_id_from, params_out.morning_local_id_to, params_out.afternoon_local_id_from, params_out.afternoon_local_id_to];

            local_database.check_locals(locals, function(error) {

                if (!error) {
                    applications_database.insert_application(params_out.name, params_out.birth_date, params_out.tin, params_out.identification, params_out.nationality, params_out.course_year, params_out.date, params_out.email, params_out.phone_1, params_out.phone_2, params_out.course_id, params_out.renovation, params_out.school_id, params_out.observations, params_out.accept_procedure, params_out.morning_local_id_from, params_out.morning_local_id_to, params_out.afternoon_local_id_from, params_out.afternoon_local_id_to, params_out.student_number,user_id, status, created_at, updated_at, function (application, error) {

                        if (!error) {
                            res.status(200).json(response.response_success({total:application.length}, application, []));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }
                    });

                } else {
                    res.status(400).json(response.response_error(error));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina uma candidatura
 */
exports.delete_application = function(req, res) {

    var params = [
    {
        "param_name" : "application_id",
        "value" : parseInt(req.params.application_id),
        "required": true,
        "type": "number",
        "sub_type": "number",
        "default": ""
    }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            applications_database.delete_application(params_out.application_id, function (status, error) {

                if (!error) {
                    if (status === "DELETED") {
                        res.status(204).send();
                    } else {

                        if (status === "RELATED") {
                            res.status(400).json(response.response_error([{
                                "code": 700,
                                "message": "Application ID " +params_out.application_id + " is associated with ....."
                            }]));

                        } else {

                            res.status(400).json(response.response_error([{
                                "code": 400,
                                "message": "Application ID " +params_out.application_id + " is not exist."
                            }]));
                        }

                    }
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Actualiza uma candidatura
 */
exports.update_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "name",
            "value" : req.body.name,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "birth_date",
            "value" : req.body.birth_date,
            "required": true,
            "type": "string",
            "sub_type": "date",
            "default": ""
        },
        {
            "param_name" : "tin",
            "value" : req.body.tin,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "identification",
            "value" : req.body.identification,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "nationality",
            "value" : req.body.nationality,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "course_year",
            "value" : req.body.course_year,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "date",
            "value" : req.body.date,
            "required": false,
            "type": "string",
            "sub_type":"date",
            "default": ""
        },
        {
            "param_name" : "email",
            "value" : req.body.email,
            "required": true,
            "type": "string",
            "sub_type":"email",
            "default": ""
        },
        {
            "param_name" : "phone_1",
            "value" : req.body.phone_1,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "phone_2",
            "value" : req.body.phone_2,
            "required": false,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "course_id",
            "value" : req.body.course_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "renovation",
            "value" : req.body.renovation,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        },
        {
            "param_name" : "school_id",
            "value" : req.body.school_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "observations",
            "value" : req.body.observations,
            "required": false,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "accept_procedure",
            "value" : req.body.accept_procedure,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        },
        {
            "param_name" : "morning_local_id_from",
            "value" : req.body.morning_local_id_from,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "morning_local_id_to",
            "value" : req.body.morning_local_id_to,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "afternoon_local_id_from",
            "value" : req.body.afternoon_local_id_from,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "afternoon_local_id_to",
            "value" : req.body.afternoon_local_id_to,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "student_number",
            "value" : req.body.student_number,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
    ]

    var updated_at = moment().toISOString();

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            applications_database.update_application(params_out.application_id, params_out.name, params_out.birth_date, params_out.tin, params_out.identification, params_out.nationality, params_out.course_year, params_out.date, params_out.email, params_out.phone_1, params_out.phone_2, params_out.course_year, params_out.renovation, params_out.school_id, params_out.observations, params_out.accept_procedure, params_out.morning_local_id_from, params_out.morning_local_id_to, params_out.afternoon_local_id_from, params_out.afternoon_local_id_to, params_out.student_number, updated_at, function (application, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: application.length}, application, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};


/**
 *
 * @param req
 * @param res
 * @desc Aceita um candidatura
 */
exports.accept_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {    

                applications_database.accept_application(params_out.application_id, function(application, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, application, []));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

            } else {
                res.status(400).json(response.response_error(error));
            }
        });

}

/**
 *
 * @param req
 * @param res
 * @desc Rejeita um candidatura
 */
exports.rejected_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {    

                applications_database.reject_application(params_out.application_id, function(application, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, application, []));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

            } else {
                res.status(400).json(response.response_error(error));
            }
        });

}

/**
 *
 * @param req
 * @param res
 * @desc Cancela uma candidatura
 */
exports.cancelled_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {    

                applications_database.cancel_application(params_out.application_id, function(application, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, application, []));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

            } else {
                res.status(400).json(response.response_error(error));
            }
        });

}


/**
 *
 * @param req
 * @param res
 * @desc Desistência de uma candidatura
 */
exports.withdrawal_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "justification",
            "value" : req.body.justification,
            "required": false,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }
    ]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {   
                

                applications_database.withdrawal_application(params_out.application_id, params_out.justification, function(application, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, application, []));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

            } else {
                res.status(400).json(response.response_error(error));
            }
        });
}


/**
 *
 * @param req
 * @param res
 * @desc Mundança de estado da candidatura
 */
exports.status_application = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "event",
            "value" : req.body.event,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "notes",
            "value" : req.body.notes,
            "required": false,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            applications_database[params_out.event](params_out.application_id, params_out.event, params_out.notes, function(application, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: 1}, application, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Mundança de estado da desitência
 */
exports.status_withdrawal = function(req, res) {

    const withdrawal_status = ['accept','reject'];

    var params = [
        {
            "param_name" : "withdrawal_id",
            "value" : parseInt(req.params.withdrawal_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "event",
            "value" : req.body.event,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        console.log(withdrawal_status.includes(params_out.event));
        if (withdrawal_status.includes(params_out.event)) {

            if (!error) {

                applications_database[params_out.event + '_withdrawal'](params_out.withdrawal_id, function(application, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, application, []));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

            } else {
                res.status(400).json(response.response_error(error));
            }
        } else {
            res.status(400).json(response.response_error([{'code': '3500', 'message': "The status provided ('"+ params_out.event +"') is not valid"}]));
        }
    });

}
