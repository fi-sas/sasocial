const back_office = require('../../models/back_office/period');
const response  = require('../../libs/config_response');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os contactos
 */
exports.get_contacts = function(req, res) {

    back_office.get_contacts(function(contacts, error){

        if (!error) {
            res.status(200).json(response.response_success(contacts));
        } else {
            res.status(400).json(response.response_error(error));
        }
           
    });
};