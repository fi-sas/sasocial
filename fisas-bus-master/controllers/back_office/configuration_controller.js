var configuration_database = require('../../models/back_office/configuration_model');
var response  = require('../../libs/config_response');
var params_validation =require('../../libs/params_validation');
var _ = require('lodash');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra a configuração do micro serviço
 */
exports.get_configuration = function(req, res) {

    configuration_database.get_configuration(function(configuration, error){
        if (!error) {
            res.status(200).json(response.response_success(configuration));
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Atualiza a configuração do micro serviço
 */
exports.update_configuration = function(req, res) {

    var params = [
        {
            "param_name" : "configuration_id",
            "value" : parseInt(req.params.configuration_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "service_id",
            "value" : req.body.service_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            if (_.find(req.services, {'id': params_out.service_id}) !== undefined) {

                configuration_database.update_configuration(params_out.configuration_id, params_out.service_id, function (configuration, error) {

                    if (!error) {
                        res.status(200).json(response.response_success(configuration));
                    } else {
                        res.status(400).json(response.response_error(error));
                    }

                });

            } else {
                res.status(400).json(response.response_error([{
                    'code': '400',
                    'message': "Service id don't exist"
                }]));
            }
        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};