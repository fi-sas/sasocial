var configurations_database = require('../../models/back_office/configurations_model');
var response  = require('../../libs/config_response');
var params_validation =require('../../libs/params_validation');
var _ = require('lodash');
const { nextTick } = require('process');


const configurationsKeys = [
    'APPLICATION_REGULATION'
];

const configurationsKeysValidations = {
        'APPLICATION_REGULATION': [{
            "param_name" : "file_id",
            "value": "",
            "required": true,
            "type": "number",
            "sub_type":"positive",
            "default": ""
        }]
    };

/**
 * 
 * @param req 
 * @param res
 * @description Mostra as configuração do MS
 */
exports.get_configurations = function(req, res, next) {

    configurations_database.get_configurations(function(configuration, error){
        if (!error) {

            req.links = {total: configuration.length};
            req.configurations = configuration;

            next();
/*             res.status(200).json(response.response_success({total: configuration.length}, configuration, []));*/        
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 * 
 * @param req 
 * @param res
 * @description Mostra as configuração do MS
 */
exports.get_one_configurations = function(req, res, next) {

    var params = [
        {
            "param_name" : "configuration_key",
            "value" : req.params.configuration_key,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            if (configurationsKeys.includes(params_out.configuration_key)) {

            configurations_database.get_one_configurations(params_out.configuration_key, function(configuration, error) {
                    if (!error) {
                        req.links = {total: configuration.length};
                        req.configurations = configuration;
            
                        next();
/*                         res.status(200).json(response.response_success({total: configuration.length}, configuration, []));
 */                    } else {
                        res.status(400).json(response.response_error(error));
                    }
                });

            } else {
                res.status(400).json(response.response_error([{
                    'code': '400',
                    'message': "Configuration key don't exist"
                }]));
            }

        } else {
            res.status(400).json(response.response_error(error));
        }

});
};

/**
 *
 * @param req
 * @param res
 * @description Atualiza uma configuração do MS
 */
exports.update_configurations = function(req, res, next) {

    var params = [
        {
            "param_name" : "configuration_key",
            "value" : req.params.configuration_key,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "config",
            "value" : JSON.stringify(req.body.config),
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": null
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            if (configurationsKeys.includes(params_out.configuration_key)) {

                configurations_database.update_configurations(params_out.configuration_key, params_out.config, function (configuration, error) {

                    if (!error) {

                        configurations_database.get_one_configurations(params_out.configuration_key, function(configuration, error) {
                            if (!error) {
                                req.links = {total: configuration.length};
                                req.configurations = configuration;
                                next();
                             } else {
                                res.status(400).json(response.response_error(error));
                            }
                        });

/*                         res.status(200).json(response.response_success(configuration)); */ 
                    } else {
                        res.status(400).json(response.response_error(error));
                    }

                });

            } else {
                res.status(400).json(response.response_error([{
                    'code': '400',
                    'message': "Configuration key don't exist"
                }]));
            }
        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};