"use strict";
var contact_database = require('../../models/back_office/contact_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra os contactos
 */
exports.get_contacts = function(req, res) {

    contact_database.get_contacts(req.limit, req.offset,req.sortProp, req.sort, function(contacts, total ,error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,contacts,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


/**
 *
 * @param req
 * @param res
 * @description Mostra
 */
exports.get_one_contact = function(req, res) {

    var params = [
        {
            "param_name" : "contact_id",
            "value" : parseInt(req.params.contact_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            contact_database.get_one_contact(params_out.contact_id, function(contact, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: contact.length},contact,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 * 
 * @param req 
 * @param res 
 * @description Insere um contacto
 */
exports.insert_contact = function(req, res) {

    var params = [
        {
            "param_name" : "name",
            "value" : req.body.name,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "phone",
            "value" : req.body.phone,
            "required": true,
            "type": "number",
            "sub_type": "phone",
            "default": ""
        },
        {
            "param_name" : "email",
            "value" : req.body.email,
            "required": true,
            "type": "string",
            "sub_type": "email",
            "default": ""
        },
        {
            "param_name" : "website",
            "value" : req.body.website,
            "required": true,
            "type": "string",
            "sub_type": "link",
            "default": ""
        }

    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            contact_database.insert_contact(params_out.name, params_out.phone, params_out.email, params_out.website,function(contact,error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:contact.length},contact,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};

/**
 *
 * @param  req
 * @param  res
 * @description Elimina um contacto
 */
exports.delete_contact = function(req, res){

    var params = [
        {
            "param_name" : "contact_id",
            "value" : parseInt(req.params.contact_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                contact_database.delete_contact(params_out.contact_id, function(error)
                {
                    if (!error) {
                        res.status(200).send();
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Altera um contacto
 */
exports.update_contact = function(req, res){

    var params = [
        {
            "param_name" : "contact_id",
            "value" : parseInt(req.params.contact_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "name",
            "value" : req.body.name,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "phone",
            "value" : req.body.phone,
            "required": true,
            "type": "number",
            "sub_type": "phone",
            "default": ""
        },
        {
            "param_name" : "email",
            "value" : req.body.email,
            "required": true,
            "type": "string",
            "sub_type": "email",
            "default": ""
        },
        {
            "param_name" : "website",
            "value" : req.body.website,
            "required": true,
            "type": "string",
            "sub_type": "link",
            "default": ""
        }

    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            contact_database.update_contact(params_out.contact_id, params_out.name, params_out.phone, params_out.email, params_out.website, function(contact, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:contact.length},contact,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};