"use strict";
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
var currency_database = require('../../models/back_office/currency_model');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os tipo de moeda
 */
exports.get_currencys = function(req, res) {

    currency_database.get_currencys(req.limit, req.offset,req.sortProp, req.sort, function(currencys, total, error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,currencys,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


/**
 *
 * @param req
 * @param res
 * @description Mostra um tipo de moeda
 */
exports.get_one_currency = function(req, res) {

    var params = [
        {
            "param_name" : "currency_id",
            "value" : parseInt(req.params.currency_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            currency_database.get_one_currency(params_out.currency_id, function(currency, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: 1},currency,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Insere um tipo de moeda
 */
exports.insert_currency = function(req, res){

    var params = [
        {
            "param_name" : "currency",
            "value" : req.body.currency,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "symbol",
            "value" : req.body.symbol,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            currency_database.insert_currency(params_out.currency, params_out.symbol,function(currency, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: currency.length},currency,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 * 
 * @param req 
 * @param res 
 * @description Elimina uma moeda
 */
exports.delete_currency = function (req, res) {

    var params = [
        {
            "param_name" : "currency_id",
            "value" : parseInt(req.params.currency_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {

              currency_database.delete_currency(params_out.currency_id, function(error) {
                 if (!error) {
                     res.status(200).send();
                 } else {
                     res.status(400).json(response.response_error([error]));
                 }
             });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
 
 }

/**
 *
 * @param req
 * @param res
 * @desc Altera uma moeda
 */
exports.update_currency = function(req, res){

    var params = [
        {
            "param_name" : "currency_id",
            "value" : parseInt(req.params.currency_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "currency",
            "value" : req.body.currency,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "symbol",
            "value" : req.body.symbol,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            currency_database.update_currency(params_out.currency_id, params_out.currency, params_out.symbol, function(currency, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: currency.length},currency,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};