"use strict";
var link_database = require('../../models/back_office/link_model');
var response  = require('../../libs/config_response');
var params_validation =require('../../libs/params_validation');
var pagination = require('../../libs/get_pagination');
/**
 * 
 * @param req 
 * @param res 
 * @description Insere uma ligação entre duas rotas
 */
exports.insert_link = function(req, res){

    var params = [
        {
            "param_name" : "local_route1_id",
            "value" : req.body.local_route1_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "local_route2_id",
            "value" : req.body.local_route2_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "route1_id",
            "value" : req.body.route1_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "route2_id",
            "value" : req.body.route2_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]


    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            link_database.insert_link(params_out.local_route1_id, params_out.local_route2_id, params_out.route1_id, params_out.route2_id, function(link,error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:link.length},link,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};

/**
 *
 * @param req
 * @param res
 * @description Elimina uma ligação entre duas rotas
 */
exports.delete_link = function(req, res){

    var params = [
        {
            "param_name" : "local_route1_id",
            "value" : req.body.local_route1_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "local_route2_id",
            "value" : req.body.local_route2_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "route1_id",
            "value" : req.body.route1_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "route2_id",
            "value" : req.body.route2_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]


    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            link_database.delete_link(params_out.local_route1_id, params_out.local_route2_id, params_out.route1_id, params_out.route2_id, function(error) {
                if (!error) {
                    res.status(200).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};


/**
 *
 * @param req
 * @param res
 * @description Mostra as ligações entre rotas
 */
exports.get_links = function(req, res) {

    link_database.get_links(req.limit, req.offset,req.sortProp, req.sort, function(links_route, total ,error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,links_route,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }

    });
};
