"use strict";
var local_database = require('../../models/back_office/local_model');
var response  = require('../../libs/config_response');
var _ = require('lodash');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os locais
 */
exports.get_locals = function(req, res) {

    local_database.get_locals(req.limit, req.offset,req.sortProp, req.sort, function(locals, total, error) {

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,locals,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra um local
 */
exports.get_one_local = function(req, res) {

    var params = [
        {
            "param_name" : "local_id",
            "value" : parseInt(req.params.local_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
                local_database.get_one_local(params_out.local_id, function(local, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total:local.length},local,[]));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 * 
 * @param req 
 * @param res
 * @description Mostras os locais que se referem ao instituto
 */
exports.get_locals_institute = function(req, res) {

    local_database.get_locals_institute(req.limit, req.offset,req.sortProp, req.sort,function(locals, total,error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,locals,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


/**
 *
 * @param req
 * @param res
 * @description Insere um ponto
 */
exports.insert_local = function(req, res) {

   var params = [
        {
            "param_name" : "address",
            "value" : req.body.address,
            "required": false,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "latitude",
            "value" : req.body.latitude,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "longitude",
            "value" : req.body.longitude,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "local",
            "value" : req.body.local,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "region",
            "value" : req.body.region,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "institute",
            "value" : req.body.institute,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        }
    ]


    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            local_database.insert_local(params_out.address, params_out.latitude, params_out.longitude, params_out.local, params_out.region, params_out.institute, function (local, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total:local.length}, local, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina um ponto
 */
exports.delete_local = function(req, res) {

    var params = [
    {
        "param_name" : "local_id",
        "value" : parseInt(req.params.local_id),
        "required": true,
        "type": "number",
        "sub_type": "number",
        "default": ""
    }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            local_database.delete_local(params_out.local_id, function (status, error) {

                if (!error) {
                    if (status === "DELETED") {
                        res.status(204).send();
                    } else {

                        if (status === "RELATED") {
                            res.status(400).json(response.response_error([{
                                "code": 700,
                                "message": "Local ID " +params_out.local_id + " is associated with a route."
                            }]));

                        } else {

                            res.status(400).json(response.response_error([{
                                "code": 400,
                                "message": "Local ID " +params_out.local_id + " is not exist."
                            }]));
                        }

                    }
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Actualiza um ponto
 */
exports.update_local = function(req, res) {

    var params = [
        {
            "param_name" : "local_id",
            "value" : parseInt(req.params.local_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "address",
            "value" : req.body.address,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "latitude",
            "value" : req.body.latitude,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "longitude",
            "value" : req.body.longitude,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "local",
            "value" : req.body.local,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "region",
            "value" : req.body.region,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "institute",
            "value" : req.body.institute,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            local_database.update_local(params_out.local_id, params_out.address, params_out.latitude, params_out.longitude, params_out.local, params_out.region, params_out.institute, function (local, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: local.length}, local, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};