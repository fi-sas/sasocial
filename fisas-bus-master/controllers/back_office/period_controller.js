"use strict";
var period_database = require('../../models/back_office/period_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra as épocas 
 */
exports.get_periods = function(req, res) {

    period_database.get_periods(req.limit, req.offset,req.sortProp, req.sort,function(periods, total, error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,periods,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


/**
 *
 * @param req
 * @param res
 * @description Mosrta um periodo
 */
exports.get_one_period = function(req, res) {

    var params = [
        {
            "param_name" : "period_id",
            "value" : parseInt(req.params.period_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            period_database.get_one_period(params_out.period_id, function(period, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: period.length},period,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Insere uma época
 */
exports.insert_period = function(req,res){

    var params = [
        {
            "param_name" : "type",
            "value" : req.body.type,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            period_database.insert_period(params_out.type ,function(period, error) {
                if (!error) {
                    res.status(200).json(response.response_success(period));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param  req
 * @param  res
 * @desc Elimina uma época
 */
exports.delete_period = function(req,res){

    var params = [
        {
            "param_name" : "period_id",
            "value" : parseInt(req.params.period_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {

            period_database.delete_period(params_out.period_id,function(error) {
                if (!error) {
                    res.status(200).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};

/**
 *
 * @param req
 * @param res
 * @desc Altera a época
 */
exports.update_period = function(req,res){

    var params = [
        {
            "param_name" : "period_id",
            "value" : parseInt(req.params.period_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type",
            "value" : req.body.type,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            period_database.update_period(params_out.period_id, params_out.type, function(period, error) {
                if (!error) {
                    res.status(200).json(response.response_success(period));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });

};