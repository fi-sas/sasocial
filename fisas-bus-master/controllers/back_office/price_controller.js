"use strict";
var price_database = require('../../models/back_office/price_table_model');
var response  = require('../../libs/config_response');
var async = require('async');
var params_validation =require('../../libs/params_validation');
var _ = require('lodash');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra uma tabela de preços
 */
exports.get_price_table = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.query.route_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "ticket_type_id",
            "value" : parseInt(req.query.ticket_type_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_user_id",
            "value" : parseInt(req.query.type_user_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "currency",
            "value" : req.query.currency,
            "required": true,
            "type": "string",
            "sub_type":"number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            price_database.get_currency_id(params_out.currency, function(currency_id, error) {

            if (!error) {
                    price_database.get_price_table(params_out.route_id, params_out.ticket_type_id, params_out.type_user_id, currency_id, req.profileInfo.name,function(prices, error) {

                        if (!error) {
                            res.status(200).json(response.response_success({total: prices.length},prices,[]));
                        } else {
                            res.status(400).json(response.response_error(error));
                        }
                    });
            } else {
                res.status(400).json(response.response_error([error]));
            }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @desc Insere uma tabela de preços
 */
exports.insert_price_table = function(req,res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : req.body.route_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "type_user_id",
            "value" : req.body.type_user_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "ticket_type_id",
            "value" : req.body.ticket_type_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "prices",
            "value" : req.body.prices,
            "required": true,
            "type": "object",
            "sub_type":"",
            "items_types": [{
                "param_name" : "value",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type":"positive",
                "default": ""
            },
            {
                "param_name" : "description",
                "value": "",
                "required": true,
                "type": "string",
                "sub_type":"string",
                "default": ""
            },
            {
                "param_name" : "currency_id",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type":"number",
                "default": 0
            },
            {
                "param_name" : "number_stop",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type":"positive",
                "default": 0
            },
            {
                "param_name" : "tax_id",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type":"positive",
                "default": 0
            },
            {
                "param_name" : "account_id",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type":"positive",
                "default": 0
            }   
            ],
            "default": ""
        }
    ]


    var currency_id;

    params_validation.params_validation(params, function (params_out, error) {
        if (!error) {

            if (params_out.prices.length !== 0) {

                if (_.find(req.profiles, {'id': params_out.type_user_id}) !== undefined) {


                    if (_.uniqBy(params_out.prices, 'tax_id').length === 1 && _.uniqBy(params_out.prices, 'account_id').length === 1) {

                        if (_.find(req.taxes, {'id': params_out.prices[0].tax_id}) !== undefined) {

                            if (_.find(req.accounts, {'id': params_out.prices[0].account_id}) !== undefined) {

                                price_database.get_price_table(params_out.route_id, params_out.ticket_type_id, params_out.type_user_id, params_out.prices[0].currency_id, _.find(req.profiles, {'id': params_out.type_user_id}).profile, function (prices, error) {


                                    if (prices.length === 0) {

                                        var asyncPrices = [];

                                        async.series([

                                            //region Linhas
                                            function (callback) {

                                                params_out.prices.forEach(function (price, index, array) {

                                                    asyncPrices.push(function (callback) {

                                                        price_database.insert_price_column(params_out.route_id, params_out.ticket_type_id, params_out.type_user_id, price.value, price.description, price.currency_id, price.number_stop, price.tax_id, price.account_id, function (error) {

                                                            currency_id = price.currency_id;

                                                            if (!error) {
                                                                callback(null);
                                                            } else {
                                                                callback(error);
                                                            }
                                                        });

                                                    });

                                                    if (index === array.length - 1) {
                                                        callback(null);
                                                    }
                                                });
                                            },
                                            function (callback) {

                                                async.parallel(asyncPrices, function (err) {
                                                    console.log("Tabela de preços inserida");
                                                    callback(err);
                                                });
                                            }
                                            //endregion

                                        ], function (err) {

                                            if (!err) {

                                                price_database.get_price_table(params_out.route_id, params_out.ticket_type_id, params_out.type_user_id, currency_id, _.find(req.profiles, {'id': params_out.type_user_id}).profile, function (price_table, error) {

                                                    if (!error) {
                                                        res.status(200).json(response.response_success({}, price_table, []));
                                                    }

                                                });
                                            } else {
                                                res.status(400).json(response.response_error(error));
                                            }
                                        });

                                    } else {

                                        res.status(400).json(response.response_error([{
                                            "code": 400,
                                            "message": "This price table already exists for this user and type of ticket."
                                        }]));

                                    }

                                });

                            } else {

                                res.status(400).json(response.response_error([{
                                    'code': '400',
                                    'message': "Account id don't exist"
                                }]));
                            }
                        } else {

                            res.status(400).json(response.response_error([{
                                'code': '400',
                                'message': "Tax id don't exist"
                            }]));
                        }
                    } else {

                        res.status(400).json(response.response_error([{
                            'code': '2500',
                            'message': "Tax id or Account id are different all prices"
                        }]));
                    }
                } else {
                    res.status(400).json(response.response_error([{
                        'code': '400',
                        'message': "Type user id don't exist"
                    }]));
                }

            } else {
                res.status(400).json(response.response_error([{
                    'code': '400',
                    'message': "The prices are empty!"
                }]));
            }
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


/**
 * 
 * @param req 
 * @param res
 * @description Elimina uma tabela de preços
 */
exports.delete_price_table = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "ticket_type_id",
            "value" : parseInt(req.params.ticket_type_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_user_id",
            "value" : parseInt(req.params.type_user_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "currency_id",
            "value" : parseInt(req.params.currency_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            price_database.delete_price_table(params_out.route_id, params_out.ticket_type_id, params_out.type_user_id, params_out.currency_id , req.profiles,function(error){

                if (!error) {
                    res.status(204).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
                
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @desc Altera um preço
 */
exports.update_price_value = function(req,res){

    var params = [
        {
            "param_name" : "price_id",
            "value" : parseInt(req.params.price_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "value",
            "value" : req.body.value,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            price_database.update_price(params_out.price_id, params_out.value, function(price, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:price.length}, price, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
         });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


