"use strict";
var routes_database = require('../../models/back_office/route_model');
var response  = require('../../libs/config_response');
var _ = require('lodash');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra as rotas
 */
exports.get_routes = function(req, res) {

        routes_database.get_routes(req.limit, req.offset,req.sortProp, req.sort,function(routes, total,error){

            if (!error) {
                pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                    res.status(200).json(response.response_success(links,routes,[]));
                })
            } else {
                res.status(400).json(response.response_error([error]));
            }

        });
}

/**
 *
 * @param req
 * @param res
 * @description Mostra uma rota
 */
exports.get_one_route = function(req, res) {


    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            routes_database.get_one_route(params_out.route_id, function (routes, error) {

                if (!error) {
                   res.status(200).json(response.response_success({total:routes.length}, routes, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Mostra as tabelas de uma rota
 */
exports.get_route_timetables = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            routes_database.get_route_timetables(params_out.route_id, function (routes, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total:routes.length}, routes, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Mostras as tabelas de preços
 */
exports.get_route_price_tables = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            routes_database.get_route_prices_tables(params_out.route_id, req.profiles,function (price_tables, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total:price_tables.length}, price_tables, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Mostra as zonas de uma rota
 */
exports.get_route_zones = function(req, res) {


    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            routes_database.get_route_zones(params_out.route_id, function (zones, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: zones.length}, zones, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });

}

/**
 *
 * @param req
 * @param res
 * @description Mostra as ligações da rota com outras rotas
 */
exports.get_route_links = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                routes_database.get_route_links(req.limit, req.offset,req.sortProp, req.sort, params_out.route_id, function(route_links, total, error){

                    if (!error) {
                        pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                            res.status(200).json(response.response_success(links,route_links,[]));
                        })
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }

                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Mostra os bilhetes comprados numa rota
 */
exports.get_route_tikets = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            routes_database.get_tickets_bought_route(req.limit, req.offset,req.sortProp, req.sort, params_out.route_id, function(tickets, total, error){

                if (!error) {
                    pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                        res.status(200).json(response.response_success(links,tickets,[]));
                    })
                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}


/**
 *
 * @param req
 * @param res
 * @description Insere uma Rota
 */
exports.insert_route = function(req, res) {

    var params = [
        {
            "param_name" : "name",
            "value" : req.body.name,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "external",
            "value" : req.body.external,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        },
        {
            "param_name" : "contact_id",
            "value" : req.body.contact_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "locals",
            "value" : req.body.locals,
            "required": true,
            "type": "object",
            "sub_type":"",
            "items_types": [{
                "param_name" : "id",
                "value": "",
                "required": true,
                "type": "number",
                "sub_type": "number",
                "default": ""
            },
            {
                    "param_name" : "instruction",
                    "value": "",
                    "required": false,
                    "type": "string",
                    "sub_type":"string",
                    "default": ""
                },
                {
                    "param_name" : "distance",
                    "value": "",
                    "required": false,
                    "type": "number",
                    "sub_type":"number",
                    "default": 0
                }
            ],
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            routes_database.insert_route(params_out.name, params_out.external ,params_out.contact_id, params_out.locals, function(route_id, error) {

                if (!error) {

                    routes_database.get_one_route(route_id, function (route, error) {

                        if (!error) {
                            res.status(200).json(response.response_success({"total": route.length},route, []));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }
                    });
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina uma rota
 */
exports.delete_route = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                routes_database.delete_route(params_out.route_id,function(error) {
                    if (!error) {
                            res.status(200).send();
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Atualiza o nome da rota
 */
exports.update_route_name = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "route_name",
            "value" : req.body.route_name,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            routes_database.update_route_name(params_out.route_id, params_out.route_name, function(route, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: route.length},route,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};

/**
 *
 * @param req
 * @param res
 * @description Atualiza os dados do local da rota
 */
exports.update_route_local = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "id",
            "value" : req.body.id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "instruction",
            "value" : req.body.instruction,
            "required": true,
            "type": "string",
            "sub_type":"string",
            "default": ""
        },
        {
            "param_name" : "distance",
            "value" : req.body.distance,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            routes_database.update_route_local(params_out.route_id, params_out.id, params_out.instruction, params_out.distance,function(local, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: local.length},local,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};