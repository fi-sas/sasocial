"use strict";
var season_database = require('../../models/back_office/season_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os períodos
 */
exports.get_seasons = function(req, res) {

    season_database.get_seasons(req.limit, req.offset,req.sortProp, req.sort,function(seasons, total,error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,seasons,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};

exports.get_one_season = function(req, res) {

    var params = [
        {
            "param_name" : "season_id",
            "value" : parseInt(req.params.season_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            season_database.get_one_season(params_out.season_id ,function(season, error){

                if (!error) {

                    res.status(200).json(response.response_success({total: season.length},season,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }

            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


/**
 *
 * @param req
 * @param res
 * @description Insere um período
 */
exports.insert_season = function(req, res) {

    var params = [
        {
            "param_name" : "period_id",
            "value" : req.body.period_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "date_begin",
            "value" : req.body.date_begin,
            "required": true,
            "type": "string",
            "sub_type": "date",
            "default": ""
        },
        {
            "param_name" : "date_end",
            "value" : req.body.date_end,
            "required": true,
            "type": "string",
            "sub_type": "date",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            season_database.insert_season(params_out.period_id, params_out.date_begin, params_out.date_end, function (season, error) {

                if (!error) {
                    res.status(200).json(response.response_success(season));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina um período
 */
exports.delete_season = function(req, res){

    var params = [
        {
            "param_name" : "season_id",
            "value" : parseInt(req.params.season_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {

            season_database.delete_season(params_out.season_id,function(error) {
                if (!error) {
                    res.status(204).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Altera o período
 */
exports.update_season = function(req, res) {

    var params = [
        {
            "param_name" : "season_id",
            "value" : parseInt(req.params.season_id),
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "date_begin",
            "value" : req.body.date_begin,
            "required": true,
            "type": "string",
            "sub_type":"date",
            "default": ""
        },
        {
            "param_name" : "date_end",
            "value" : req.body.date_end,
            "required": true,
            "type": "string",
            "sub_type":"date",
            "default": ""
        },
        {
            "param_name" : "period_id",
            "value" : req.body.period_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            season_database.update_season(params_out.season_id, params_out.date_begin, params_out.date_end, params_out.period_id, function (season, error) {

                if (!error) {
                    res.status(200).json(response.response_success(season));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};