var tickets_bought_database = require('../../models/back_office/tickets_bought_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
var cypoto_prod = require('../../libs/crypto_prod');
var async = require('async');

/**
 *
 * @param req
 * @param res
 * @description Mostra as configuraçao dos bilhetes
 */
exports.get_tickets_boughts = function(req, res) {

    tickets_bought_database.get_tickets_bought(req.limit, req.offset,req.sortProp, req.sort, function(tickets_config, total,error){
        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (tickets) {

                res.status(200).json(response.response_success(tickets,tickets_config,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra uma configuraçao de um bilhete numa rota
 */
exports.get_one_ticket_bought = function(req, res) {

    var params = [
        {
            "param_name" : "ticket_id",
            "value" : parseInt(req.params.ticket_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            tickets_bought_database.get_one_ticket_bought(params_out.ticket_id,function(ticket, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: 1},ticket,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @description Mostra uma configuraçao de um bilhete numa rota
 */
exports.insert_tickets = function(req, res) {

    var asyncTickets = [] , tickets_inserted = [];

    var params = [
        {
            "param_name": "prod_compound_cod",
            "value": req.body.prod_compound_cod,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
         }
        ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

         var tickets = cypoto_prod.decrypt(params_out.prod_compound_cod);

         if (tickets !== null) {
             tickets = tickets.tickets;
             if (tickets.length !== 0) {

             async.series([

                 function (callback) {

                     tickets.forEach(function (ticket, index, array) {


                         asyncTickets.push(function (callback) {

                             tickets_bought_database.insert_ticket(ticket.prod_cod,
                                 ticket.validate_date,
                                 ticket.departure_local,
                                 ticket.arrival_local,
                                 req.user.user_name,
                                 req.user.email
                                 , function (ticket, error) {

                                     if (!error) {
                                         tickets_inserted.push(ticket);
                                         callback(null);

                                     } else {

                                         callback(error);
                                     }
                                 });
                        });
                         if (index === array.length - 1) {
                             callback();
                         }
                     });
                 },
                 function (callback) {

                     async.parallel(asyncTickets, function (error) {
                         if (!error) {
                             callback(null);
                         } else {
                             callback(error);
                         }
                     });
                 }

             ], function (error) {

                     if (!error) {
                         res.status(200).json(response.response_success({total: tickets_inserted.length}, tickets_inserted, []));
                     } else {
                         res.status(400).json(response.response_error([error]));
                     }
                 })

             } else {
                 res.status(400).json(response.response_error({'code': '400', 'message': "Don't have tickets"}));
             }
         } else {
             res.status(400).json(response.response_error({'code': '800', 'message': "Invalid product code"}));
         }


        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}


/**
 *
 * @param req
 * @param res
 * @description Ativa ou desativa a utilizaçao do bilhete na rota
 */
exports.confirm_ticket = function (req, res) {

    var params = [
        {
            "param_name" : "ticket_id",
            "value" : parseInt(req.params.ticket_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            tickets_bought_database.confirm_ticket(params_out.ticket_id,function(ticket,error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: 1},ticket,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });

}