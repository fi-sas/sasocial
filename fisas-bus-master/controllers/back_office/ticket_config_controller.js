"use strict";

var ticket_config_database = require('../../models/back_office/ticket_config_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 *
 * @param req
 * @param res
 * @description Mostra as configuraçao dos bilhetes
 */
exports.get_tickets_configs = function(req, res) {

    ticket_config_database.get_tickets_configs(req.limit, req.offset,req.sortProp, req.sort, function(tickets_config, total, error){
        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,tickets_config,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }

    });
};


/**
 *
 * @param req
 * @param res
 * @description Mostra uma configuraçao de um bilhete numa rota
 */
exports.get_one_ticket_config = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            ticket_config_database.get_one_ticket_config(params_out.route_id, function(ticket_config, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: ticket_config.length},ticket_config,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}


/**
 *
 * @param req
 * @param res
 * @description Elimina um tipo de bilhete/passe
 */
exports.enable_disable_ticket_config = function (req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "active",
            "value" : req.body.active,
            "required": true,
            "type": "boolean",
            "sub_type": "boolean",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            ticket_config_database.active_enable_ticket(params_out.route_id, params_out.active,function(ticket_config, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: 1},ticket_config,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });

}

/**
 *
 * @param req
 * @param res
 * @desc Altera um tipo de bilhete/passe
 */
exports.update_ticket_config = function(req, res){

    console.log("Máximo atualizado");

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "max_to_buy",
            "value" : req.body.max_to_buy,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            ticket_config_database.set_max(params_out.route_id, params_out.max_to_buy, function(ticket_config, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:1},ticket_config,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};