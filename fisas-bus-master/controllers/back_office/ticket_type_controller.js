"use strict";
var ticket_type_database = require('../../models/back_office/ticket_type_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra os tipos de bilhete ou passes
 */
exports.get_tickets_types = function(req, res) {

    ticket_type_database.get_tickets_types(req.limit, req.offset,req.sortProp, req.sort, function(tickets, total,error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,tickets,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


/**
 *
 * @param req
 * @param res
 * @description Mostra um tipo de bilhete
 */
exports.get_one_ticket = function(req, res) {

    var params = [
        {
            "param_name" : "ticket_type_id",
            "value" : parseInt(req.params.ticket_type_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            ticket_type_database.get_one_ticket_type(params_out.ticket_type_id, function(ticket_type, error) {

                if (!error) {
                    res.status(200).json(response.response_success({total: ticket_type.length},ticket_type,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 * 
 * @param req 
 * @param res 
 * @description Insere um tipo de bilhete/passe
 */
exports.insert_ticket_type= function(req, res){

    var params = [
        {
            "param_name" : "type",
            "value" : req.body.type,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                ticket_type_database.insert_ticket_type(params_out.type, function(ticket_type, error) {
                    if (!error) {
                        res.status(200).json(response.response_success(ticket_type));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 * 
 * @param req 
 * @param res 
 * @description Elimina um tipo de bilhete/passe
 */
exports.delete_ticket_type = function (req, res) {

    var params = [
        {
            "param_name" : "ticket_type_id",
            "value" : parseInt(req.params.ticket_type_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                  ticket_type_database.delete_ticket_type(params_out.ticket_type_id, function(error) {
                     if (!error) {
                         res.status(200).send();
                     } else {
                         res.status(400).json(response.response_error([error]));
                     }
                 });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
 
 }

/**
 *
 * @param req
 * @param res
 * @desc Altera um tipo de bilhete/passe
 */
exports.update_ticket_type = function(req, res){

    var params = [
        {
            "param_name" : "ticket_type_id",
            "value" : parseInt(req.params.ticket_type_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type",
            "value" : req.body.type,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            ticket_type_database.update_ticket_type(params_out.ticket_type_id, params_out.type, function(ticket_type, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total:ticket_type.length},ticket_type,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};