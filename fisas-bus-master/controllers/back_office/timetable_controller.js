"use strict";
var timetable_database = require('../../models/back_office/timetable_model');
var response  = require('../../libs/config_response');
var async = require('async');
var moment = require('moment');
var  _ = require('lodash');
var params_validation = require('../../libs/params_validation');
var type_day = require('../../models/back_office/type_day_model');
var route = require('../../models/back_office/route_model');
var season = require ('../../models/back_office/season_model');
var local = require('../../models/back_office/local_model');

/**
 *
 * @param  req
 * @param  res
 * @description mostra uma tabela horária
 */
exports.get_timetable = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.query.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.query.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "season_id",
            "value" : parseInt(req.query.season_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": 0
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            timetable_database.get_timetable(params_out.route_id, params_out.type_day_id, params_out.season_id, function (timetable, error) {
                if (!error) {
                    res.status(200).json(response.response_success({}, timetable, []));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};

/**
 *
 * @param req
 * @param res
 * @desc Retorna uma tabela horário de uma rota
 */
exports.insert_timetable = function(req,res) {

    var hours = req.body.hours;
    var locals = req.body.locals;

    var params = [
        {
            "param_name" : "route_id",
            "value" : req.body.route_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_day_id",
            "value" : req.body.type_day_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "season_id",
            "value" : req.body.season_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    var errors = [];

    params_validation.params_validation(params, function (params_out, error) {

         if(!error) {

         type_day.get_one_type_day(params_out.type_day_id, function (type_day, error) {

             if (error) {
                 errors.push(error);
             }

             route.get_one_route(params_out.route_id, function (route, error) {

               if (error) {
                   errors.push(error);
               }

               season.get_one_season(params_out.season_id, function (season, error) {

                   if (error) {
                       errors.push(error);
                   }

                  local.check_locals(locals,function (error) {

                       if (!error) {

                               var asyncLines = [];

                               async.series([

                                   //region Linhas
                                   function (callback) {

                                       var line_n;
                                       timetable_database.line_state(params_out.route_id, params_out.season_id, params_out.type_day_id, function (state, error) {

                                           if (!error) {

                                                   if (state === null) {
                                                       line_n = 1;
                                                   } else {
                                                       line_n = state + 1;
                                                   }


                                                   var lines = [];

                                                   for (var j = 0; j < hours.length; j++) {

                                                       for (var i = 0 ; i < hours[j].length; i++) {

                                                           lines.push({hour:hours[j][i], local_id: locals[i], type_day_id: params_out.type_day_id, line_number: j + line_n, route_id:params_out.route_id, season_id:params_out.season_id, order: i + 1});

                                                       }
                                                   }

                                                   lines.forEach(function (line, index, array) {

                                                           asyncLines.push(function (callback) {

                                                                   timetable_database.insert_line(line.hour, line.local_id, line.type_day_id, line.line_number, line.route_id, line.season_id, line.order , function (error) {

                                                                       if (error) {
                                                                           errors.push(error);
                                                                       }
                                                                       callback(null);

                                                                   });
                                                            });

                                                       if (index === array.length - 1) {
                                                           callback();
                                                       }
                                                   });
                                           } else {
                                               errors.push(error);
                                               callback();
                                           }
                                       });
                                   },
                                   function (callback) {

                                       async.parallel(asyncLines, function (error) {
                                           callback(error);
                                       });
                                   }
                                   //endregion

                               ], function (error) {

                                   if (errors.length === 0) {

                                       timetable_database.get_timetable(params_out.route_id, params_out.type_day_id, params_out.season_id, function (timetable, error) {
                                           if (!error) {
                                               res.status(200).json(response.response_success({total:1},timetable,[]));
                                           } else {
                                               res.status(400).json(response.response_error([error]));
                                           }
                                       });
                                   } else {
                                       res.status(400).json(response.response_error(errors));
                                   }
                               });
                   } else {
                      errors.push(error);
                      res.status(400).json(response.response_error(error));
                   }
                   });
               });

             });
         });
        } else {
            res.status(400).json(response.response_error(error));
        }

   });
};

/**
 *
 * @param req
 * @param res
 * @desc Elimina uma tabela
 */
exports.delete_timetable = function(req, res) {


    console.log(req.params.route_id);
    console.log(req.params.type_day_id);
    console.log(req.params.season_id);


    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "season_id",
            "value" : parseInt(req.params.season_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    var errors = [];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_day.get_one_type_day(params_out.type_day_id, function (type_day, error) {

                if (error) {
                    errors.push(error);
                }

                route.get_one_route(params_out.route_id, function (route, error) {

                    if (error) {
                        errors.push(error);
                    }

                    season.get_one_season(params_out.season_id, function (season, error) {

                        if (error) {
                            errors.push(error);
                        }

                        if (errors.length === 0) {

                            timetable_database.delete_timetable(params_out.route_id, params_out.type_day_id, params_out.season_id, function (error) {
                                if (!error) {
                                    res.status(200).send();
                                } else {
                                    res.status(400).json(response.response_error([error]));
                                }
                            });

                        } else {
                            res.status(400).json(response.response_error(errors));
                        }
                    });

                });
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });

}


/**
 *
 * @param req
 * @param res
 * @desc Elimina uma linha da tabela
 */
exports.delete_timetable_line = function(req, res) {

    var params = [
        {
            "param_name" : "route_id",
            "value" : parseInt(req.params.route_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "season_id",
            "value" : parseInt(req.params.season_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "line_number",
            "value" : parseInt(req.params.line_number),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    var errors = [];

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_day.get_one_type_day(params_out.type_day_id, function (type_day, error) {

                if (error) {
                    errors.push(error);
                }

                route.get_one_route(params_out.route_id, function (route, error) {

                    if (error) {
                        errors.push(error);
                    }

                    season.get_one_season(params_out.season_id, function (season, error) {

                        if (error) {
                            errors.push(error);
                        }

                        if (errors.length === 0) {

                            timetable_database.delete_timetable_line(params_out.route_id, params_out.type_day_id, params_out.season_id, params_out.line_number, function (error) {
                                if (!error) {
                                    res.status(200).send();
                                } else {
                                    res.status(400).json(response.response_error(error));
                                }
                            });

                        } else {
                            res.status(400).json(response.response_error(errors));
                        }
                    });

                });
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });

}

/**
 *
 * @param req
 * @param res
 * @desc Actuliza a hora de passagem
 */
exports.update_hour = function(req,res) {

    var params = [
        {
            "param_name" : "hour_id",
            "value" : parseInt(req.params.hour_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "hour",
            "value" : req.body.hour,
            "required": true,
            "type": "string",
            "sub_type": "hour",
            "default": "00:00:00"
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                timetable_database.update_hour(params_out.hour, params_out.hour_id, function (hour, error) {

                    if (!error) {
                        res.status(200).json(response.response_success({total: 1}, hour,[]));
                    } else {
                        res.status(400).json(response.response_error(error));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};
