"use strict";
var type_day_database = require('../../models/back_office/type_day_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra os tipo de dias
 */
exports.get_types_days = function(req, res) {

    type_day_database.get_types_days(req.limit, req.offset,req.sortProp, req.sort,function(types_days, total, error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,types_days,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra um tipo de dia
 */
exports.get_one_type_day = function(req, res) {

    var params = [
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                type_day_database.get_one_type_day(params_out.type_day_id, function(type_day, error){

                    if (!error) {
                        res.status(200).json(response.response_success({total: type_day.length},type_day,[]));

                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


/**
 *
 * @param req
 * @param res
 * @description Mostra os dias da semana
 */
exports.get_days_weeks = function(req, res) {

    type_day_database.get_days_weeks(function(days_weeks, error){

        if (!error) {

           res.status(200).json(response.response_success({total: days_weeks.length}, days_weeks, []));


        } else {
            res.status(400).json(response.response_error([error]));
        }
    });

};

/**
 *
 * @param req
 * @param res
 * @description Tipo de dia de uma rota
 */
exports.route_types_days = function(req, res) {

    var route_id = req.query.route_id;

    type_day_database.route_types_days(route_id, function(types_days, error) {
        if (!error) {
            res.status(200).json(response.response_success(types_days));
        } else {
            res.status(400).json(response.response_error([error]));
        }
    });

};

/**
 * 
 * @param req 
 * @param res 
 * @description Insere um tipo de dia
 */
exports.insert_type_day= function(req, res){

    var days_week_ids = req.body.days_week_ids;

    var params = [
        {
            "param_name" : "type_day",
            "value" : req.body.type_day,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    if (days_week_ids !== undefined) {

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {

                type_day_database.insert_type_day(params_out.type_day, days_week_ids, function (type_day_id, error) {

                    if (!error) {
                        type_day_database.get_one_type_day(type_day_id, function (type_day, error) {

                            if (!error) {
                                res.status(200).json(response.response_success(type_day));
                            } else {
                                res.status(400).json(response.response_error([error]));
                            }

                        });
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
            } else {
                res.status(400).json(response.response_error(error));
            }
        });

    } else {
        res.status(400).json(response.response_error({"code": 800, "message": "Invalid request missing days week ids"}));
    }
};


exports.add_day_week = function(req, res){

    var params = [
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "day_id",
            "value" : parseInt(req.params.day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_day_database.add_day_week(params_out.type_day_id, params_out.day_id,function(error)
            {
                if (!error) {
                    type_day_database.get_one_type_day(params_out.type_day_id, function (type_day, error) {

                        if (!error) {
                            res.status(200).json(response.response_success(type_day));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }

                    });
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

exports.delete_day_week = function(req, res){

    var params = [
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "day_id",
            "value" : parseInt(req.params.day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_day_database.delete_day_week(params_out.type_day_id, params_out.day_id,function(error)
            {
                if (!error) {
                    type_day_database.get_one_type_day(params_out.type_day_id, function (type_day, error) {

                        if (!error) {
                            res.status(200).json(response.response_success(type_day));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }

                    });
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}
/**
 *
 * @param  req
 * @param  res
 * @description Elimina um tipo de dia
 */
exports.delete_type_day = function(req, res){

    var params = [
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_day_database.delete_type_day(params_out.type_day_id,function(error) {
                if (!error) {
                    res.status(204).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}


/**
 *
 * @param req
 * @param res
 * @desc Altera o tipo de dia
 */
exports.update_type_day = function(req, res){

    var params = [
        {
            "param_name" : "type_day_id",
            "value" : parseInt(req.params.type_day_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "type_day",
            "value" : req.body.type_day,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
                type_day_database.update_type_day(params_out.type_day_id, params_out.type_day, function(type_day_data, error) {
                    if (!error) {
                        res.status(200).json(response.response_success(type_day_data));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

