"use strict";
var type_user_database = require('../../models/back_office/type_user_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os tipo de utilizadores
 */
exports.get_types_users = function(req, res) {

    type_user_database.get_types_users(req.limit, req.offset,req.sortProp, req.sort,function(types_users, total, error){

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,types_users,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};


exports.get_one_type_user = function(req, res) {

    var params = [
        {
            "param_name" : "type_user_id",
            "value" : parseInt(req.params.type_user_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
                type_user_database.get_one_type_user(params_out.type_user_id, function(type_user, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({},type_user,[]));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Insere um tipo de utilizador
 */
exports.insert_type_user = function(req, res){

    var params = [
        {
            "param_name" : "user",
            "value" : req.body.user,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]


    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

        type_user_database.insert_type_user(params_out.user ,function(user_out, error) {
            if (!error) {
                res.status(200).json(response.response_success({"total": user_out.length},user_out,[]));
            } else {
                res.status(400).json(response.response_error([error]));
            }
        });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina um ponto
 */
exports.delete_type_user = function(req, res) {

    var params = [
        {
            "param_name" : "type_user_id",
            "value" : parseInt(req.params.type_user_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]


    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_user_database.delete_type_user(params_out.type_user_id, function(status, error) {
                if (!error) {
                    if (status === "DELETED") {
                        res.status(204).send();
                    } else {

                        if (status === "RELATED") {
                            res.status(400).json(response.response_error([{
                                "code": 700,
                                "message": "Type User ID " +params_out.type_user_id + " is associated with a price table."
                            }]));

                        } else {

                            res.status(400).json(response.response_error([{
                                "code": 400,
                                "message": "Type User ID " +params_out.type_user_id + " is not exist."
                            }]));
                        }

                    }
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    })
}

/**
 *
 * @param req
 * @param res
 * @desc Altera o tipo de utilizador
 */
exports.update_type_user = function(req, res){

    var params = [
        {
            "param_name" : "type_user_id",
            "value" : parseInt(req.params.type_user_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "user",
            "value" : req.body.user,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            type_user_database.update_type_user(params_out.type_user_id, params_out.user, function(type_user, error) {
                if (!error) {
                    res.status(200).json(response.response_success({total: type_user.length},type_user,[]));
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    })
};

