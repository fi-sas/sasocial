"use strict";
var database_zone = require('../../models/back_office/zone_model');
var response  = require('../../libs/config_response');
var pagination = require('../../libs/get_pagination');
var params_validation =require('../../libs/params_validation');
/**
 * 
 * @param req 
 * @param res
 * @description Mostra as zonas
 */
exports.get_zones = function(req, res) {

    database_zone.get_zones(req.limit, req.offset,req.sortProp, req.sort,function(zones, total, error) {

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,zones,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra uma zona
 */
exports.get_one_zone = function(req, res) {

    var params = [
        {
            "param_name" : "zone_id",
            "value" : parseInt(req.params.zone_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            database_zone.get_one_zone(params_out.zone_id, function(zones, error) {

                if (!error) {

                    res.status(200).json(response.response_success({total: zones.length},zones,[]));

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Insere uma Zona
 */
exports.insert_zone = function(req, res) {

    var locals_ids = req.body.locals_ids;

    var params = [
        {
            "param_name" : "zone_name",
            "value" : req.body.zone_name,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "route_id",
            "value" : req.body.route_id,
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    if (locals_ids !== undefined) {

        params_validation.params_validation(params, function (params_out, error) {

            if (!error) {

                      database_zone.insert_zone(params_out.zone_name, params_out.route_id, locals_ids, function(zone_id, error) {

                          if (!error) {

                              database_zone.get_one_zone(zone_id, function (zones, error) {

                                  if (!error) {
                                      if (!error) {
                                          res.status(200).json(response.response_success({total: zones.length}, zones, []));
                                      } else {
                                          res.status(400).json(response.response_error([error]));
                                      }
                                  } else {
                                      res.status(400).json(response.response_error([error]));
                                  }
                              });
                          } else {
                              res.status(400).json(response.response_error([error]));
                          }
                     });
            } else {
                res.status(400).json(response.response_error(error));
            }
        });
    } else {
        res.status(400).json(response.response_error([{"code": 800, "message": "Invalid request missing locals_ids"}]));
    }
};


/**
 *
 * @param req
 * @param res
 * @description Elimina uma zona
 */
exports.delete_zone = function(req, res) {

    var params = [
        {
            "param_name" : "zone_id",
            "value" : parseInt(req.params.zone_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            database_zone.delete_zone(params_out.zone_id, function(error) {
                if (!error) {
                    res.status(200).send();
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina uma zona de uma rota
 */
exports.delete_zone_route = function(req, res) {

    var zone_id = req.params.zone_id;
    var route_id = req.params.route_id;


    if (zone_id !== undefined && route_id !== undefined) {
        database_zone.delete_zone_route(zone_id, route_id,function(status, error) {
            if (!error) {
                res.status(200).json(response.response_success({
                    status: status,
                }));
            } else {
                res.status(400).json(response.response_error([error]));
            }
        });
    } else {
        res.status(400).json(response.response_error(new Error("INVALID REQUEST")));
    }
};