"use strict";

var crypto_prod = require('../libs/crypto_prod');
var response  = require('../libs/config_response');


exports.cod_prod_decrypt = function (req, res) {

    var prod_cod = req.query.prod_compound_cod;

    if (prod_cod !== null || prod_cod !== undefined) {

        var prod_object = crypto_prod.decrypt(prod_cod);

        res.status(200).json(response.response_success({total: 1}, prod_object, []));

    } else {
        res.status(400).json(response.response_error([{code:350, message:"Missing product compound code."}]));
    }
};

