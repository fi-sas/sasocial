"use strict";
var payment_months_database = require('../models/payment_months_model');
var applications_database = require('../models/applications_model');
var response  = require('../libs/config_response');
var _ = require('lodash');
var pagination = require('../libs/get_pagination');
var params_validation =require('../libs/params_validation');
var moment = require('moment');


function parseBoolean(data, boolProps) {

    for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < boolProps.length; j++) {
          if ([1, 0].includes(data[i][boolProps[j]])) {
              data[i][boolProps[j]] = (data[i][boolProps[j]] === 1);
          }
      }
    }
    return data;
};

/**
 * 
 * @param req 
 * @param res
 * @description Mostra os pagamantos por mês
 */
exports.get_payment_months= function(req, res) {

    payment_months_database.get_payment_months(req.limit, req.offset,req.sortProp, req.sort, function(paymentMonths, total, error) {

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links, parseBoolean(paymentMonths, ['paid']),[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }
           
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra os pagamentos de uma candidatura
 */
exports.get_application_payment_month = function(req, res) {

    var params = [
        {
            "param_name" : "application_id",
            "value" : parseInt(req.params.application_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }] 

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            payment_months_database.get_application_payment_month(params_out.application_id, function(paymentMonths, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total:paymentMonths.length}, parseBoolean(paymentMonths, ['paid']),[]));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra um pagamento de um mês
 */
exports.get_one_payment_month = function(req, res) {

    var params = [
        {
            "param_name" : "payment_month_id",
            "value" : parseInt(req.params.payment_month_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            payment_months_database.get_one_payment_month(params_out.payment_month_id, function(paymentMonth, error) {
                    if (!error) {
                        res.status(200).json(response.response_success({total:paymentMonth.length},parseBoolean([paymentMonth], ['paid']),[]));
                    } else {
                        res.status(400).json(response.response_error([error]));
                    }
                });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};


/**
 *
 * @param req
 * @param res
 * @description Insere um pagamento por mês
 */
exports.insert_payment_month = function(req, res) {

   var params = [
        {
            "param_name" : "application_id",
            "value" : req.body.application_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "month",
            "value" : req.body.month,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "year",
            "value" : req.body.year,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "value",
            "value" : req.body.value,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "paid",
            "value" : req.body.paid,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        }
    ]

    var created_at = moment().toISOString();
    var updated_at = moment().toISOString();
    
    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            applications_database.get_one_application(params_out.application_id, '',function(application, error) {
                if (!error) {

                    payment_months_database.insert_payment_month(params_out.application_id, params_out.month, params_out.year, params_out.value, params_out.paid, created_at, updated_at, function (paymentMonth, error) {

                        if (!error) {
                            res.status(200).json(response.response_success({total:paymentMonth.length}, paymentMonth, []));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }
                    });

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Elimina um pagamanto por mês
 */
exports.delete_payment_month = function(req, res) {

    var params = [
        {
            "param_name" : "payment_month_id",
            "value" : parseInt(req.params.payment_month_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        }
    ]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {
            payment_months_database.delete_payment_month(params_out.payment_month_id, function (status, error) {

                if (!error) {
                    if (status === "DELETED") {
                        res.status(204).send();
                    } else {

                        if (status === "RELATED") {
                            res.status(400).json(response.response_error([{
                                "code": 700,
                                "message": "Payment month ID " +params_out.payment_month_id + " is associated with ....."
                            }]));

                        } else {

                            res.status(400).json(response.response_error([{
                                "code": 400,
                                "message": "Payment month ID " +params_out.payment_month_id + " is not exist."
                            }]));
                        }

                    }
                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });
        } else {
            res.status(400).json(response.response_error(error));
        }
    });
}

/**
 *
 * @param req
 * @param res
 * @desc Actualiza um pagamento por mês
 */
exports.update_payment_month = function(req, res) {

    var params = [
        {
            "param_name" : "payment_month_id",
            "value" : parseInt(req.params.payment_month_id),
            "required": true,
            "type": "number",
            "sub_type": "number",
            "default": ""
        },
        {
            "param_name" : "application_id",
            "value" : req.body.application_id,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "month",
            "value" : req.body.month,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        },
        {
            "param_name" : "year",
            "value" : req.body.year,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "value",
            "value" : req.body.value,
            "required": true,
            "type": "number",
            "sub_type":"number",
            "default": ""
        },
        {
            "param_name" : "paid",
            "value" : req.body.paid,
            "required": true,
            "type": "boolean",
            "sub_type":"boolean",
            "default": ""
        }
    ]

    var updated_at = moment().toISOString();

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            applications_database.get_one_application(params_out.application_id, '',function(application, error) {

                if (!error) {

                    payment_months_database.update_payment_month(params_out.payment_month_id, params_out.application_id, params_out.month, params_out.year, params_out.value, params_out.paid, updated_at, function (paymentMonth, error) {

                        if (!error) {
                            res.status(200).json(response.response_success({total: paymentMonth.length}, paymentMonth, []));
                        } else {
                            res.status(400).json(response.response_error([error]));
                        }
                    });

                } else {
                    res.status(400).json(response.response_error([error]));
                }
            });

        } else {
            res.status(400).json(response.response_error(error));
        }

    });
};