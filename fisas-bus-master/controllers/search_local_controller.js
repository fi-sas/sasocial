"use strict";

var search_local_database = require('../models/search_local_model');
var response  = require('../libs/config_response');
var params_validation =require('../libs/params_validation');


exports.routes_search_one_local = function (req, res) {

    var params = [
        {
            "param_name" : "local",
            "value" : req.query.local,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                 search_local_database.search_local(params_out.local, function (locals, error) {

                     if (!error) {

                         res.status(200).json(response.response_success({total: locals.length}, locals, []));
                     } else {
                         res.status(400).json(response.response_error(error));
                     }
                 });


        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

exports.search_place = function (req, res) {

    var params = [
        {
            "param_name" : "place",
            "value" : req.query.place,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

            search_local_database.search_place(params_out.local, function (locals, error) {

                if (!error) {

                    res.status(200).json(response.response_success({total: locals.length}, locals, []));
                } else {
                    res.status(400).json(response.response_error(error));
                }
            });


        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};