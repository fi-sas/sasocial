"use strict";

var search_place_database = require('../models/search_place_model');
var response  = require('../libs/config_response');
var params_validation =require('../libs/params_validation');
var pagination = require('../libs/get_pagination');

/**
 *
 * @param req
 * @param res
 * @description Mostra os concelhos
 */
exports.search_place= function (req, res) {

    var params = [
        {
            "param_name" : "place",
            "value" : req.query.place,
            "required": true,
            "type": "string",
            "sub_type": "string",
            "default": ""
        }]

    params_validation.params_validation(params, function (params_out, error) {

        if (!error) {

                 search_place_database.search_place(params_out.place, function (places, error) {

                     if (!error) {

                         res.status(200).json(response.response_success({total: places.length}, places, []));
                     } else {
                         res.status(400).json(response.response_error([error]));
                     }
                 });


        } else {
            res.status(400).json(response.response_error(error));
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @description Mostra os concelhos
 */
exports.get_places = function(req, res) {

    search_place_database.get_places(req.limit, req.offset,req.sortProp, req.sort, function(places, total, error) {

        if (!error) {
            pagination.getPaginationMetaData(total, req.limit, req.offset, req , function (links) {

                res.status(200).json(response.response_success(links,places,[]));
            })
        } else {
            res.status(400).json(response.response_error([error]));
        }

    });
};