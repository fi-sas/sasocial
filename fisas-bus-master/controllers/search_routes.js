"use strict";
/**
 * Created by afcosta on 06/08/2018.
 */

var moment  = require('moment');
var async   = require("async");

var google_apis = require('../libs/google_apis_func');
var config = require('config');
var mobility  = require('../libs/mobility_funcs');

var response  = require('../libs/config_response');
var search_database  = require('../models/search_route_model');
var _ = require('lodash');

moment.locale('pt');

function removeDups(array) {
    let unique = {};
    array.forEach(function(i) {
      if(!unique[i]) {
        unique[i] = true;
      }
    });
    return Object.keys(unique);
  }

exports.routes_search = function (req, res) {

    //region Querys Stings

    var type_day, date , season , location, location_id, mode;
    var origin_id , destination_id;

    var type_user = req.profile_id;

    var origin = req.query.origin;
    var destination = req.query.destination;

    //data e hora
    if (req.query.date == null || req.query.date === undefined) {

       season = moment().format("YYYY-MM-DD");
       date = moment().unix();

    } else {

       season = moment(req.query.date).format("YYYY-MM-DD");
       date = moment(req.query.date).unix();
    }

    //tipo de dia
    type_day = req.types_days.toString();

    //endregion

    var arrivel_place;
    var departure_place;

    // mode 0 -> Origem em texto e destino em númerico
    // mode 1 -> Origem em númerico e destino em texto
    // mode 2 -> Origem e destino em númerico

    if (isNaN(origin) && !isNaN(destination)) {

        location = origin;
        location_id = destination;
        mode = 0;

    } else if (!isNaN(origin) && isNaN(destination)) {
        location = destination;
        location_id = origin;
        mode = 1;


    } else if (!isNaN(origin) && !isNaN(destination)) {
        location = null;
        mode = 2;
    } else {
        mode = 3;
    }

    if (mode !== 3) {

        if (origin !== destination) {

            if (isNaN(origin) && !isNaN(destination) || !isNaN(origin) && isNaN(destination) || !isNaN(origin) && !isNaN(destination)) {

                var location_nearby = {
                    local_id: undefined,
                    region: undefined,
                    latitude: undefined,
                    longitude: undefined
                };

                var location_coord = {
                    latitude: null,
                    longitude: null
                };

                var routes_detected, routes_traced;

                async.series([

                        function (callback) {
                            mobility.local_arrivel_and_departure(mode, origin, destination, function (departure, arrivel, errors) {

                                if (errors.length === 0) {

                                    if (mode === 0) {
                                        departure_place = origin;
                                        arrivel_place = arrivel;
                                    } else if (mode === 1) {

                                        departure_place = departure;
                                        arrivel_place = destination;

                                    } else {

                                        departure_place = departure;
                                        arrivel_place = arrivel;
                                    }
                                    callback(null);

                                } else {
                                    res.status(400).json(response.response_error(errors));
                                }

                            })
                        },
                        function (callback) {

                            if (location !== null) {

                                google_apis.geocode(location, function (latitude, longitude, error) {

                                    if (!error) {
                                        location_coord.latitude = latitude;
                                        location_coord.longitude = longitude;
                                        callback(null);
                                    } else {

                                        if (error.message === "Geocode API: UNDEFINED_LOCATION") {

                                            callback([{code: 1000 , message: "Undefined location"}]);
                                        } else {

                                            if (error.message !== "INVALID_LOCAL") {
                                                callback([{code: 1100, message: error.message}]);
                                            } else {
                                                callback([{code: 1200, message: "Only places in portugal are allowed"}]);
                                            }
                                        }
                                    }
                                });
                            } else {
                                callback(null);
                            }
                        },

                        function (callback) {

                            if (location !== null) {

                                search_database.nearby_local(location_coord.latitude, location_coord.longitude, function (locals, error) {

                                    if (!error) {

                                        if (locals[0].distance_in_km <= config.get("RADIUS.limit")) {

                                            location_nearby.local_id = locals[0].id;
                                            location_nearby.region = locals[0].region;
                                            location_nearby.latitude = locals[0].latitude;
                                            location_nearby.longitude = locals[0].longitude;

                                            console.log("PROCESS: Nearby local find");
                                        } else {
                                            console.log("PROCESS: Nearby local not find");
                                        }


                                        callback(null);

                                    } else {
                                        callback(error);
                                    }
                                });
                            } else {
                                callback(null);
                            }
                        },

                        function (callback) {

                            //region  SetOriginAndDestination

                            if (isNaN(origin) && !isNaN(destination)) {

                                origin_id = location_nearby.local_id;
                                destination_id = parseInt(destination);

                            } else if (!isNaN(origin) && isNaN(destination)) {
                                destination_id = location_nearby.local_id;
                                origin_id = parseInt(origin)
                            } else {
                                origin_id = parseInt(origin);
                                destination_id = parseInt(destination);
                            }

                            //endregion

                            mobility.detect_routes(origin_id, destination_id, type_day, function (routes, error) {

                                if (!error) {
                                    routes_detected = routes;

                                  //console.log("Rotas Relacionadas: ", JSON.stringify(routes_detected));

                                    if (routes_detected.length !== 0) {
                                        console.log("PROCESS: Routes detects");
                                    } else {
                                        res.status(200).json(response.response_success({total: routes_detected.length},routes_detected, []));
                                    }

                                    callback(null);
                                } else {
                                    callback(error);
                                }
                            });
                        },

                        function (callback) {

                            mobility.pickTimetables(routes_detected, type_day, season, function (error) {

                                if (!error) {

                                    console.log("PROCESS: Timetables obtained");

                                    try {

                                    var routes_aux = [] , routes_index_to_remove = [];


                                    // Remove routes without hours
                                    for (var r = 0; r < routes_detected.length; r++) {

                                        for (var h = 0; h < routes_detected[r].length; h++) { 
                                            if (routes_detected[r][h].hours === null) {
                                               routes_index_to_remove.push(r);
                                            }
                                        }
                                    }
                                    routes_index_to_remove = removeDups(routes_index_to_remove);

                                    for (var i = routes_index_to_remove.length -1; i >= 0; i--)
                                    routes_detected.splice(routes_index_to_remove[i],1);



                                    for (var i = 0; i < routes_detected.length; i++) {
                                        if (routes_detected[i].length !== 0) {

                                            //Filter hours null
                                            if (mode === 1 || mode === 2) {
                                                    var local_index, indexs = [];

                                                    for (var j = 0; j < routes_detected[i].length; j++) {
                                                        local_index = routes_detected[i][j].locals_ids.indexOf(Number(origin));

                                                        for (var k = 1; k < routes_detected[i][j].hours.length; k++) {
                                                            if (routes_detected[i][j].hours[k][local_index] === null){
                                                                indexs.push(k);
                                                            }
                                                        }
                                                        _.pullAt(routes_detected[i][j].hours, indexs);
                                                    }
                                                }

                                            routes_aux.push(routes_detected[i]);

                                        }
                                    }

                                    routes_detected = routes_aux;

                                    callback(null);
                                   } catch(e) {
                                    callback({code: 400, message: e.message});
                                   }
                                } else {

                                    callback({code: 400, message:error.message});
                                }
                            });
                        },

                        function (callback) {

                            if (location_nearby.local_id === undefined && location !== null) {

                                // (Rotas encontradas, Coordenadas do ponto encontrado no nearby, Ponto com id)

                                mobility.best_station_by_route(routes_detected, location_coord.latitude + "," + location_coord.longitude, location_id, mode, function (distances, error) {

                                    if (!error) {
                                        console.log("PROCESS: The best station found");
                                        callback(null);
                                    } else {
                                        callback(error);
                                    }
                                });
                            } else {
                                callback(null);
                            }
                        },

                        function (callback) {


                            if (routes_detected.length !== 0) {
                                mobility.create_routes(routes_detected, destination_id, function (trace, error) {

                                    if (!error) {

                                        routes_traced = trace;
                                        console.log("PROCESS: Route Create");
                                        callback(null);
                                    } else {
                                        callback(error);
                                    }

                                });

                            } else {
                                routes_traced = [];
                                callback(null);
                            }

                        },

                        function (callback) {

                            if (location_nearby.local_id === undefined && location !== null) {

                                var origin_coord, destination_coord, local_id;
                                if (mode === 0) {
                                    origin_coord = location_coord.latitude + "," + location_coord.longitude;
                                    destination_coord = null;
                                    local_id = destination;
                                } else if (mode === 1) {
                                    origin_coord = null;
                                    destination_coord = location_coord.latitude + "," + location_coord.longitude;
                                    local_id = origin;
                                }

                                mobility.add_google_diretions(origin_coord, destination_coord, date, routes_traced, local_id, function (error, routes_filter) {

                                    if (!error) {

                                        routes_traced = routes_filter;
                                        console.log("PROCESS: Google route add");
                                        callback(null);
                                    } else {

                                        callback(error);
                                    }
                                });
                            } else {
                                callback(null);
                            }
                        },
                        function (callback) {

                            mobility.calculate(routes_traced, type_user, arrivel_place, departure_place, season,req.taxes,function (error) {
                                if (!error) {
                                    console.log("PROCESS: Calc price and duration success");
                                    callback(null);
                                } else {
                                    callback(error);
                                }
                            });
                        },
                        function (callback) {

                            mobility.routeFormatting(routes_traced, season,function (route_formating, error) {

                               if (!error) {

                                   routes_traced = route_formating;
                                   console.log("PROCESS: Route Formatting");
                                   callback(null);

                               } else {

                                   callback(error);
                               }

                            });
                        }
                    ],
                    function (err) {

                        if (!err) {
                            search_database.log_search(arrivel_place, departure_place, date, routes_traced.length, 0, function (error) {

                                //routes_traced = _.orderBy(routes_traced, ['departure_hour'], ['asc']);

                                // if (error) {
                                //     console.log("Log error: ", error);
                                // }
                                res.status(200).json(response.response_success({"total": routes_traced.length}, routes_traced, []));
                            });
                        } else {
                            search_database.log_search(arrivel_place, departure_place, date, 0, 1, function (error) {

                                // if (error) {
                                //     console.log("Log error: ", error);
                                // }

                                res.status(400).json(response.response_error({code: err.code, message: err.message}));
                            });
                        }
                    });
            } else {
                res.status(400).json(response.response_error([{code:350, message:"Invalid search."}]));
            }

        } else {
            res.status(400).json(response.response_error([{code:100, message:"Origin and destination are same."}]));
        }
    } else {
        res.status(400).json(response.response_error([{code:350, message:"Invalid search."}]));
    }

};
