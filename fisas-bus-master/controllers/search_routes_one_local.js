"use strict";

var moment  = require('moment');
var async   = require("async");
var mobility  = require('../libs/mobility_funcs');
var response  = require('../libs/config_response');

moment.locale('pt');

exports.routes_search_one_local = function (req, res) {

    var season, date, type_day;

    var destination = req.query.destination;

    if (req.query.date == null || req.query.date === undefined) {

        season = moment().format("YYYY-MM-DD");
        date = moment().unix();

    } else {

        season = moment(req.query.date).format("YYYY-MM-DD");
        date = moment(req.query.date).unix();
    }

    //tipo de dia
    type_day = req.types_days.toString();

    var departure_place = null;

    var routes_detected, routes_traced = [];
    
            async.series([

                    function (callback) {
                        mobility.local_arrivel_and_departure(0, null, destination, function (arrivel, departure, errors) {

                            if (errors.length === 0) {

                                departure_place = departure;

                                callback(null);

                            } else {
                                res.status(400).json(response.response_error(errors));
                            }

                        })
                    },

                    function (callback) {

                        mobility.detect_route_one_local(destination, type_day, function (routes, error) {

                            if (!error) {

                                routes_detected = routes;
                                console.log("PROCESS: Routes detects");
                                callback(null);

                            } else {
                                callback(error);
                            }
                        });

                    },

                    function (callback) {

                        mobility.pickTimetables(routes_detected, type_day, season, function (error) {

                            if (!error) {
                                console.log("PROCESS: Timetables obtained");

                                for (var i = 0; i < routes_detected.length; i++) {
                                    if (routes_detected[i].length === 0) {
                                        routes_detected.splice(i, 1);
                                    }
                                }
                                callback(null);
                            } else {
                                callback(error);
                            }
                        });
                    },

                    function (callback) {

                        mobility.set_begin_local(routes_detected, function (error) {

                            if (!error) {
                                callback(null);
                            } else {
                                callback(error);
                            }
                        })
                    },

                    function (callback) {

                        mobility.create_routes(routes_detected, destination, function (trace, error) {

                            if (!error) {

                                for (var i = 0; i < trace.length; i++) {

                                    if (trace[i].steps.length !== 1) {

                                        trace[i].arrivel_place = trace[i].steps[0].local;
                                        trace[i].departure_place = departure_place;
                                        delete trace[i].route_price_detail;
                                        routes_traced.push(trace[i]);
                                        console.log("PROCESS: Traced routes");
                                    }

                                }
                                callback(null);
                            } else {
                                callback(error);
                            }
                        })

                    },
                    function (callback) {
                        mobility.calculateOnlyDuration(routes_traced, function (error) {
                            if (!error) {
                                console.log("PROCESS: Calc duration sucess");
                                callback(null);
                            } else {
                                callback(error);
                            }
                        });
                    }

                ],
                function (error) {

                    if (!error) {
                        res.status(200).json(response.response_success({"total": routes_traced.length}, routes_traced, []));
                    } else {
                        res.status(400).json(response.response_error([{code: error.code, message: error.message}]));
                    }
                });
            
}
