CREATE DATABASE  IF NOT EXISTS `fisas_bus` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fisas_bus`;
-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: fisas_bus
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (1,2);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'IPVC',258809610,'geral@ipvc.pt','www.ipvc.pt'),(2,'AVIC',258829022,'avictransportes@avic','https://www.avic.pt/home'),(3,'IPCA',234567890,'IPCA@ipca.pt','www.ipca.pt');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(45) DEFAULT NULL,
  `symbol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Euro','€'),(2,'Dólar','$');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `day_group`
--

DROP TABLE IF EXISTS `day_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `day_group` (
  `day_id` int(11) NOT NULL,
  `type_day_id` int(11) NOT NULL,
  KEY `fk_day_group_day1_idx` (`day_id`),
  KEY `fk_day_group_type_day1_idx` (`type_day_id`),
  CONSTRAINT `fk_day_group_day1` FOREIGN KEY (`day_id`) REFERENCES `day_week` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_day_group_type_day1` FOREIGN KEY (`type_day_id`) REFERENCES `type_day` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_group`
--

LOCK TABLES `day_group` WRITE;
/*!40000 ALTER TABLE `day_group` DISABLE KEYS */;
INSERT INTO `day_group` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,2),(0,3),(4,1);
/*!40000 ALTER TABLE `day_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `day_week`
--

DROP TABLE IF EXISTS `day_week`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `day_week` (
  `id` int(11) NOT NULL,
  `day` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_week`
--

LOCK TABLES `day_week` WRITE;
/*!40000 ALTER TABLE `day_week` DISABLE KEYS */;
INSERT INTO `day_week` VALUES (0,'Domingo'),(1,'Segunda-Feira'),(2,'Terça-Feira'),(3,'Quarta-Feira'),(4,'Quinta-Feira'),(5,'Sexta-Feira'),(6,'Sábado');
/*!40000 ALTER TABLE `day_week` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distance`
--

DROP TABLE IF EXISTS `distance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local1_id` int(11) NOT NULL,
  `local2_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `distance` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_distancia_pontos1_idx` (`local1_id`),
  KEY `fk_distancia_pontos2_idx` (`local2_id`),
  KEY `fk_distancia_rota1_idx` (`route_id`),
  CONSTRAINT `fk_distancia_pontos1` FOREIGN KEY (`local1_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_distancia_pontos2` FOREIGN KEY (`local2_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_distancia_rota1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distance`
--

LOCK TABLES `distance` WRITE;
/*!40000 ALTER TABLE `distance` DISABLE KEYS */;
INSERT INTO `distance` VALUES (1,1,2,1,2),(2,2,10,1,0),(3,10,5,1,0),(4,5,6,1,0),(5,6,5,1,0),(6,5,10,1,0),(7,10,2,1,0),(8,2,1,1,0),(9,11,4,2,0),(10,4,7,2,0),(11,7,8,2,0),(12,8,9,2,0),(13,9,10,2,0),(14,10,2,2,0),(15,2,1,2,0),(18,12,13,4,0),(19,13,14,4,0),(20,14,2,4,0),(21,15,16,5,0),(22,16,17,5,0),(23,17,2,5,0),(24,11,4,6,0),(25,4,7,6,0),(26,7,8,6,0),(27,8,5,6,0),(28,5,6,6,0),(29,6,3,6,0),(30,3,18,6,0),(31,4,20,7,0),(32,20,19,7,0),(33,18,3,8,0),(34,3,6,8,0),(35,6,5,8,0),(36,5,8,8,0),(37,8,7,8,0),(38,7,4,8,0),(39,4,11,8,0),(40,2,5,9,0),(41,11,4,10,0),(42,4,7,10,0),(43,7,8,10,0),(44,8,9,10,0),(45,9,21,10,0),(46,21,22,10,0),(47,22,2,10,0),(48,1,2,11,0),(49,2,10,11,0),(50,10,6,11,0),(51,6,5,11,0),(52,5,10,11,0),(53,10,2,11,0),(54,2,1,11,0),(55,2,14,12,0),(56,14,13,12,0),(57,13,12,12,0),(64,74,17,39,5),(65,17,2,39,3),(66,75,76,40,5),(67,76,78,40,2),(68,78,77,40,3),(69,77,2,40,3),(70,79,80,41,3),(71,80,2,41,5),(72,19,20,42,3),(73,20,4,42,5),(83,81,82,44,4),(84,82,11,44,4),(85,83,4,45,3),(86,84,85,47,4),(87,85,86,47,3),(88,86,8,47,9),(89,4,11,48,4),(90,84,8,49,3),(91,8,9,49,6),(92,9,21,49,2),(93,21,2,49,6),(94,11,82,50,3),(95,82,81,50,4),(96,4,83,51,3),(97,8,86,52,2),(98,86,85,52,2),(99,85,84,52,10),(100,2,17,53,2),(101,17,16,53,2),(102,16,15,53,1),(103,15,74,53,6),(104,2,77,54,2),(105,77,78,54,2),(106,78,76,54,5),(107,76,75,54,7),(108,2,80,55,0),(109,80,79,55,0),(110,2,8,56,2),(111,8,84,56,2),(112,5,8,57,0);
/*!40000 ALTER TABLE `distance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route1_id` int(11) NOT NULL,
  `local_route1_id` int(11) NOT NULL,
  `route2_id` int(11) NOT NULL,
  `local_route2_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ligações_rota1_idx` (`route1_id`),
  KEY `fk_ligações_pontos1_idx` (`local_route1_id`),
  KEY `fk_ligações_pontos2_idx` (`local_route2_id`),
  KEY `fk_ligações_rota2_idx` (`route2_id`),
  CONSTRAINT `fk_ligações_pontos1` FOREIGN KEY (`local_route1_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ligações_pontos2` FOREIGN KEY (`local_route2_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ligações_rota1` FOREIGN KEY (`route1_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_ligações_rota2` FOREIGN KEY (`route2_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` VALUES (1,1,5,8,5),(2,6,5,11,5),(3,11,2,12,2),(4,6,4,7,4),(5,1,2,39,2),(6,1,2,40,2),(7,1,2,41,2),(8,1,4,42,4),(9,39,2,1,2),(10,39,2,1,2),(11,40,2,1,2),(12,41,2,1,2),(13,42,4,8,4),(14,45,4,8,4),(15,8,5,1,5),(16,6,11,50,11),(17,6,4,51,4),(18,6,8,52,8),(19,11,2,53,2),(20,11,2,54,2),(21,11,2,55,2);
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local`
--

DROP TABLE IF EXISTS `local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(45) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `local` varchar(30) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `institute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local`
--

LOCK TABLES `local` WRITE;
/*!40000 ALTER TABLE `local` DISABLE KEYS */;
INSERT INTO `local` VALUES (1,'Avenida do Atlântico',41.693645,-8.846397,'ESTG','Viana do Castelo',1),(2,'',41.694361,-8.833316,'Central (Viana)','Viana do Castelo',0),(3,'',41.806055,-8.415806,'Ponte da Barca','Ponte da Barca',0),(4,'',42.077167,-8.48194,'Central (Monção)','Monção',0),(5,'',41.760698,-8.579354,'Central (P. Lima)','Ponte de Lima',0),(6,'',41.792978,-8.541711,'ESA','Refóies',1),(7,'',42.031989,-8.632907,'ESCE','Valença',1),(8,'',41.985594,-8.658418,'Café Stop','S.Pedro da Torre',0),(9,'',41.943048,-8.740812,'Central (V. N. de Cerveira)','Vila Nova de Cerveira',0),(10,'',41.703424,-8.820456,'ESE','Viana do Castelo',1),(11,'',42.114489,-8.257979,' ESDL | Central ','Melgaço',1),(12,'',41.779394,-8.861063,'Afife','Afife',0),(13,'',41.747423,-8.86383,'Carreço','Carreço',0),(14,'',41.715175,-8.85392,'Areosa','Areosa',0),(15,'',41.659806,-8.762043,'Vila Fria','Vila Fria',0),(16,'',41.681101,-8.766636,'Mazarefes','Mazarefes',0),(17,'',41.684394,-8.793901,'Darque','Darque',0),(18,'',41.846582,-8.419175,'Central (Arcos)','A. Valdevez',0),(19,'',41.966175,-8.468726,'Extremo','Extremo',0),(20,'',42.02247,-8.485948,'Moreira','Moreira',0),(21,'',41.873402,-8.837865,'Caminha','Caminha',0),(22,'',41.820855,-8.852822,'Vila Praia de Âncora','Vila Praia de Âncora',0),(74,'',41.665479,-8.796719,'V.N Anha','V.N Anha',0),(75,'',41.72232,-8.780486,'Perre (Igreja)','Perre (Igreja)',0),(76,'',41.722486,-8.75864,'Samonde','Viana do Castelo',0),(77,'',41.7065,-8.805713,'Meadela','Viana do Castelo',0),(78,'',41.708385,-8.777427,'Santa Marta','Viana do Castelo',0),(79,'',41.733907,-8.788843,'Madorra','Viana do Castelo',0),(80,'',41.717035,-8.812481,'Cova','Viana do Castelo',0),(81,'',42.032744,-8.158826,'Castro Laboreiro','Viana do Castelo',0),(82,'',42.058847,-8.278662,'Pomares','Viana do Castelo',0),(83,'',41.973354,-8.37418,'Sistelo','Arcos de Valdevez',0),(84,'',41.912571,-8.562851,'Paredes de Coura','Paredes de Coura',0),(85,'',41.690156,-8.203872,'S. Bento da P.Aberta','Terras do Bouro',0),(86,'',41.769294,-8.651372,'S. Pedro da Lagoa','Ponte de Lima',0);
/*!40000 ALTER TABLE `local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_zone`
--

DROP TABLE IF EXISTS `local_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_zone` (
  `local_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  KEY `fk_pontos_da_zona_zonas1_idx` (`zone_id`),
  CONSTRAINT `fk_pontos_da_zona_zonas1` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_zone`
--

LOCK TABLES `local_zone` WRITE;
/*!40000 ALTER TABLE `local_zone` DISABLE KEYS */;
INSERT INTO `local_zone` VALUES (1,1),(2,1),(10,1),(5,2),(6,2),(8,3),(7,3),(11,4),(4,5),(3,6),(18,6),(9,7),(22,8),(21,9),(1,11),(3,11),(5,11),(7,11),(1,12),(2,12),(10,12),(22,13),(21,14),(9,15),(8,16),(7,16),(4,17),(11,18),(2,19),(6,19),(8,20);
/*!40000 ALTER TABLE `local_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_search`
--

DROP TABLE IF EXISTS `log_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin` varchar(45) DEFAULT NULL,
  `destination` varchar(45) DEFAULT NULL,
  `date_search` datetime DEFAULT NULL,
  `error` tinyint(4) DEFAULT NULL,
  `result_count` int(11) DEFAULT NULL,
  `date_search_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=943 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_search`
--

LOCK TABLES `log_search` WRITE;
/*!40000 ALTER TABLE `log_search` DISABLE KEYS */;
INSERT INTO `log_search` VALUES (1,'Porto','4','2018-09-19 00:00:00',0,5,'0000-00-00 00:00:00'),(2,'Porto','4','2018-09-19 00:00:00',0,5,'0000-00-00 00:00:00'),(3,'Porto','4','2018-09-19 00:00:00',0,5,'0000-00-00 00:00:00'),(4,'5','7','2018-09-19 00:00:00',0,1,'0000-00-00 00:00:00'),(5,'5','7','2018-09-19 00:00:00',0,1,'0000-00-00 00:00:00'),(6,'Porto','7','2018-09-20 00:00:00',0,4,'0000-00-00 00:00:00'),(7,'uuuuuuuuu','7','2018-09-20 00:00:00',1,0,'0000-00-00 00:00:00'),(8,'undefined','7','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(9,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(10,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(11,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(12,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(13,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(14,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(15,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(16,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(17,'Porto','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(18,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(19,'undefined','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(20,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(21,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(22,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(23,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(24,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(25,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(26,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(27,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(28,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(29,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(30,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(31,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(32,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(33,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(34,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(35,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(36,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(37,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(38,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(39,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(40,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(41,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(42,'undefined','undefined','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(43,'undefined','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(44,'undefined','undefined','1970-01-18 00:00:00',0,2,'0000-00-00 00:00:00'),(45,'Porto','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(46,'undefined','Porto','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(47,'Coimbra','undefined','1970-01-18 00:00:00',0,0,'0000-00-00 00:00:00'),(48,'Braga','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(49,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(50,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(51,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(52,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(53,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(54,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(55,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(56,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(57,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(58,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(59,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(60,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(61,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(62,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(63,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(64,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(65,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(66,'undefined','Braga','1970-01-18 00:00:00',0,0,'0000-00-00 00:00:00'),(67,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(68,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(69,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(70,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(71,'Braga','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(72,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(73,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(74,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(75,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(76,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(77,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(78,'undefined','Braga','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(79,'undefined','Braga','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(80,'undefined','Braga','1970-01-18 00:00:00',0,3,'0000-00-00 00:00:00'),(81,'undefined','Porto','1970-01-18 00:00:00',0,3,'0000-00-00 00:00:00'),(82,'undefined','fwdsfsdf','1970-01-18 00:00:00',1,0,'0000-00-00 00:00:00'),(83,'undefined','Madrid','1970-01-18 00:00:00',0,2,'0000-00-00 00:00:00'),(84,'Porto','undefined','1970-01-18 00:00:00',0,5,'0000-00-00 00:00:00'),(85,'Braga','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(86,'Braga','undefined','1970-01-18 00:00:00',0,1,'0000-00-00 00:00:00'),(87,'undefined','undefined','1970-01-18 00:00:00',0,4,'0000-00-00 00:00:00'),(88,'ESTG','Central (Viana)','1970-01-18 20:44:56',0,5,'2018-11-29 12:54:03'),(89,'ESTG','Central (Viana)','1970-01-18 20:44:56',0,5,'2018-11-29 12:56:53'),(90,'ESTG','Central (Viana)','1970-01-18 20:44:56',0,5,'2018-11-29 13:03:16'),(91,'Porto','Central (Viana)','1970-01-18 20:44:56',0,1,'2018-11-29 13:03:37'),(92,'ESTG','Ponte da Barca','1970-01-18 20:45:08',0,3,'2018-11-29 16:17:59'),(93,'porto','Ponte da Barca','1970-01-18 20:45:08',0,4,'2018-11-29 16:18:30'),(94,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 16:59:25'),(95,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:32'),(96,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:35'),(97,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:38'),(98,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:40'),(99,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:43'),(100,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:45'),(101,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:48'),(102,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:50'),(103,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:51'),(104,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:53'),(105,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:55'),(106,'Braga','Ponte da Barca','1970-01-18 20:45:10',0,4,'2018-11-29 17:02:58'),(107,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:16'),(108,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:24'),(109,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:25'),(110,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:26'),(111,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:27'),(112,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:27'),(113,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:29'),(114,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:29'),(115,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:32'),(116,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:34'),(117,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:36'),(118,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:38'),(119,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:39'),(120,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:40'),(121,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:41'),(122,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:42'),(123,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:43'),(124,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:45'),(125,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:47'),(126,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:04:48'),(127,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:01'),(128,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:03'),(129,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:04'),(130,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:05'),(131,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:08'),(132,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:08'),(133,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:09'),(134,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:10'),(135,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:11'),(136,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:56'),(137,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:05:58'),(138,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:01'),(139,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:22'),(140,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:24'),(141,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:26'),(142,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:31'),(143,'Braga','Ponte da Barca','1970-01-18 20:45:11',0,4,'2018-11-29 17:06:34'),(144,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:48'),(145,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:50'),(146,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:52'),(147,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:54'),(148,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:56'),(149,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:06:58'),(150,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:07:00'),(151,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:07:03'),(152,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:07:38'),(153,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:07:40'),(154,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:07:43'),(155,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:07:46'),(156,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:07:49'),(157,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:07:51'),(158,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:08:33'),(159,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:08:37'),(160,'undefined','Braga','1970-01-18 20:45:11',0,1,'2018-11-29 17:08:40'),(161,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:02'),(162,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:19'),(163,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:42'),(164,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:50'),(165,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:51'),(166,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:52'),(167,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:53'),(168,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:54'),(169,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:55'),(170,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:56'),(171,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:09:56'),(172,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:57'),(173,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:09:58'),(174,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:10:02'),(175,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:10:03'),(176,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:10:06'),(177,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:10:07'),(178,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:11:08'),(179,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:11:11'),(180,'undefined','Braga','1970-01-18 20:45:11',0,1,'2018-11-29 17:11:13'),(181,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:11:14'),(182,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:11:16'),(183,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:11:21'),(184,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:11:54'),(185,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:12:12'),(186,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:13:46'),(187,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:14:08'),(188,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:15:05'),(189,'undefined','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:15:49'),(190,'undefined','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:15:52'),(191,'undefined','Braga','1970-01-18 20:45:11',0,1,'2018-11-29 17:15:55'),(192,'null','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:16:34'),(193,'null','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:16:37'),(194,'null','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:16:39'),(195,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:17:55'),(196,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:18:03'),(197,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,3,'2018-11-29 17:18:05'),(198,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:18:08'),(199,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:18:13'),(200,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:18:13'),(201,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:18:15'),(202,'Ponte da Barca','Braga','1970-01-18 20:45:11',0,2,'2018-11-29 17:19:28'),(203,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:21:37'),(204,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:22:10'),(205,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:22:24'),(206,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:23:00'),(207,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:23:03'),(208,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:23:12'),(209,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:23:16'),(210,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:23:18'),(211,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:23:34'),(212,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:23:43'),(213,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,1,'2018-11-29 17:23:56'),(214,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:24:58'),(215,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:25:21'),(216,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:25:35'),(217,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:25:49'),(218,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:25:54'),(219,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:25:57'),(220,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:27:12'),(221,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:27:29'),(222,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:27:35'),(223,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:27:40'),(224,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,2,'2018-11-29 17:27:46'),(225,'Ponte da Barca','Braga','1970-01-18 20:45:12',0,3,'2018-11-29 17:27:50'),(226,'ESTG','Ponte da Barca','1970-01-18 20:46:14',0,3,'2018-11-30 10:35:13'),(227,'ESTG','Ponte da Barca','1970-01-18 20:46:14',0,3,'2018-11-30 10:44:08'),(228,'ESTG','Ponte da Barca','1970-01-18 20:46:14',0,3,'2018-11-30 10:44:11'),(229,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:46:48'),(230,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:47:57'),(231,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:18'),(232,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:20'),(233,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:24'),(234,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:26'),(235,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:27'),(236,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,2,'2018-11-30 10:49:29'),(237,'Ponte da Barca','Braga','1970-01-18 20:46:14',0,3,'2018-11-30 10:49:31'),(238,'Ponte da Barca','Braga','1970-01-18 20:46:15',0,3,'2018-11-30 10:51:31'),(239,'Ponte da Barca','Braga','1970-01-18 20:46:15',0,2,'2018-11-30 10:52:26'),(240,'Ponte da Barca','Braga','1970-01-18 20:46:15',0,2,'2018-11-30 10:52:46'),(241,'Ponte da Barca','Braga','1970-01-18 20:46:15',0,2,'2018-11-30 10:52:52'),(242,'Ponte da Barca','Braga','1970-01-18 20:46:15',0,3,'2018-11-30 10:53:00'),(243,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:11:53'),(244,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:12:15'),(245,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,3,'2018-11-30 11:15:07'),(246,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:15:18'),(247,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:15:27'),(248,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:15:37'),(249,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:15:49'),(250,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:16:25'),(251,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,1,'2018-11-30 11:16:28'),(252,'Ponte da Barca','Braga','1970-01-18 20:46:16',1,0,'2018-11-30 11:17:36'),(253,'Ponte da Barca','Braga','1970-01-18 20:46:16',1,0,'2018-11-30 11:17:58'),(254,'Ponte da Barca','Braga','1970-01-18 20:46:16',0,2,'2018-11-30 11:18:31'),(255,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,1,'2018-11-30 11:29:14'),(256,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,2,'2018-11-30 11:29:18'),(257,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,2,'2018-11-30 11:29:21'),(258,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,2,'2018-11-30 11:29:24'),(259,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,2,'2018-11-30 11:29:26'),(260,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,1,'2018-11-30 11:29:27'),(261,'Ponte da Barca','Braga','1970-01-18 20:46:17',0,2,'2018-11-30 11:29:28'),(262,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:31:04'),(263,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:31:33'),(264,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:34:09'),(265,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:35:36'),(266,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:38:35'),(267,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:39:15'),(268,'Ponte da Barca','Braga','1970-01-18 20:46:17',1,0,'2018-11-30 11:39:44'),(269,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:14'),(270,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:17'),(271,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:19'),(272,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:21'),(273,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:24'),(274,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:43:30'),(275,'Braga','Ponte da Barca','1970-01-18 20:46:18',0,4,'2018-11-30 11:48:28'),(276,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:48:36'),(277,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:48:43'),(278,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:48:47'),(279,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,3,'2018-11-30 11:48:50'),(280,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:48:53'),(281,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:49:03'),(282,'Ponte da Barca','Braga','1970-01-18 20:46:18',1,0,'2018-11-30 11:53:13'),(283,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,3,'2018-11-30 11:54:04'),(284,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:56:11'),(285,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,2,'2018-11-30 11:56:32'),(286,'Ponte da Barca','Braga','1970-01-18 20:46:18',0,3,'2018-11-30 11:56:35'),(287,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:15:32'),(288,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:18:31'),(289,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:18:39'),(290,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:19:13'),(291,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:20:04'),(292,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:20:16'),(293,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:20:18'),(294,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:23:07'),(295,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:23:32'),(296,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:23:53'),(297,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:23:56'),(298,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:24:02'),(299,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:05'),(300,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:09'),(301,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:11'),(302,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:12'),(303,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:13'),(304,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:14'),(305,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,1,'2018-11-30 12:24:16'),(306,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:24:17'),(307,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:24:18'),(308,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,2,'2018-11-30 12:24:23'),(309,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,3,'2018-11-30 12:24:23'),(310,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:39'),(311,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:42'),(312,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:49'),(313,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:50'),(314,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:51'),(315,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:54'),(316,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:55'),(317,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:56'),(318,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:57'),(319,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:58'),(320,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:24:59'),(321,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:25:00'),(322,'Ponte da Barca','Braga','1970-01-18 20:46:20',0,5,'2018-11-30 12:25:01'),(323,'ESTG','Central','1970-01-18 20:46:32',0,1,'2018-11-30 15:38:18'),(324,'Porto','Central','1970-01-18 20:46:32',0,1,'2018-11-30 15:38:38'),(325,'ESTG','Ponte da Barca','1970-01-18 20:46:39',0,3,'2018-11-30 17:46:22'),(326,'ESTG','Ponte da Barca','1970-01-18 20:46:40',0,3,'2018-11-30 17:53:02'),(327,'Braga','Central','1970-01-18 20:46:40',0,1,'2018-11-30 17:53:21'),(328,'Braga','Ponte da Barca','1970-01-18 20:46:40',0,4,'2018-11-30 17:54:41'),(329,'Ponte da Barca','Braga','1970-01-18 20:46:40',1,0,'2018-11-30 17:54:56'),(330,'Ponte da Barca','Braga','1970-01-18 20:46:40',0,5,'2018-11-30 17:55:40'),(331,'Ponte da Barca','Braga','1970-01-18 20:46:40',0,5,'2018-11-30 17:56:50'),(332,'Ponte da Barca','Braga','1970-01-18 20:46:40',0,5,'2018-11-30 17:59:09'),(333,'Ponte da Barca','Braga','1970-01-18 20:46:40',0,5,'2018-11-30 18:01:39'),(334,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,5,'2018-11-30 18:07:30'),(335,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,5,'2018-11-30 18:08:14'),(336,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,5,'2018-11-30 18:14:47'),(337,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,2,'2018-11-30 18:15:04'),(338,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,2,'2018-11-30 18:15:14'),(339,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,3,'2018-11-30 18:15:17'),(340,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,2,'2018-11-30 18:15:19'),(341,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,3,'2018-11-30 18:15:25'),(342,'Ponte da Barca','Braga','1970-01-18 20:46:41',0,3,'2018-11-30 18:15:27'),(343,'Ponte da Barca','Braga','1970-01-18 20:46:42',0,5,'2018-11-30 18:28:35'),(344,'Ponte da Barca','Braga','1970-01-18 20:46:42',0,5,'2018-11-30 18:29:03'),(345,'Ponte da Barca','Braga','1970-01-18 20:46:42',0,5,'2018-11-30 18:29:12'),(346,'Ponte da Barca','Braga','1970-01-18 20:46:42',0,5,'2018-11-30 18:33:13'),(347,'Ponte da Barca','Braga','1970-01-18 20:46:42',0,5,'2018-11-30 18:35:45'),(348,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,5,'2018-11-30 18:38:19'),(349,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,5,'2018-11-30 18:38:28'),(350,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,5,'2018-11-30 18:40:13'),(351,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,5,'2018-11-30 18:40:15'),(352,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,5,'2018-11-30 18:40:16'),(353,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:32'),(354,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:42'),(355,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:44'),(356,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:45'),(357,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:47'),(358,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:48'),(359,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:49'),(360,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:50'),(361,'Ponte da Barca','Braga','1970-01-18 20:46:43',0,1,'2018-11-30 18:45:51'),(362,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:47:45'),(363,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:47:50'),(364,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:04'),(365,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:06'),(366,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:06'),(367,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:07'),(368,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:08'),(369,'Braga','Central','1970-01-18 20:46:43',0,1,'2018-11-30 18:48:08'),(370,'Braga','Central','1970-01-18 20:46:44',0,1,'2018-11-30 18:54:55'),(371,'Braga','Central (Viana)','1970-01-18 20:46:44',0,1,'2018-11-30 18:54:59'),(372,'Braga','Central (Viana)','1970-01-18 20:46:44',0,1,'2018-11-30 18:55:02'),(373,'Braga','Central','1970-01-18 20:46:44',0,1,'2018-11-30 18:55:07'),(374,'Braga','Central','1970-01-18 20:46:44',0,1,'2018-11-30 18:55:08'),(375,'Braga','ESCE','1970-01-18 20:46:44',0,4,'2018-11-30 18:55:12'),(376,'Braga','ESCE','1970-01-18 20:46:44',0,4,'2018-11-30 18:55:28'),(377,'Braga','ESCE','1970-01-18 20:46:44',0,4,'2018-11-30 18:55:29'),(378,'Central (Viana)','ESCE','1970-01-18 20:46:44',0,1,'2018-11-30 18:55:32'),(379,'Central (Viana)','ESCE','1970-01-18 20:46:44',0,1,'2018-11-30 18:56:16'),(380,'ESTG','Ponte da Barca','1970-01-18 20:50:52',0,3,'2018-12-03 15:53:29'),(381,'ESTG','Ponte da Barca','1970-01-18 20:50:55',0,3,'2018-12-03 16:42:37'),(382,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:42:49'),(383,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:44:14'),(384,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:49:19'),(385,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:49:51'),(386,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:53:08'),(387,'ESTG','Central (Viana)','1970-01-18 20:50:55',0,5,'2018-12-03 16:53:10'),(388,'ESTG','Central (Viana)','1970-01-18 20:50:56',0,5,'2018-12-03 16:53:39'),(389,'ESTG','Central (Viana)','1970-01-18 20:50:56',0,5,'2018-12-03 16:54:54'),(390,'Braga','Central (Viana)','1970-01-18 20:50:56',0,1,'2018-12-03 16:59:19'),(391,'Porto','Central (Viana)','1970-01-18 20:50:56',0,1,'2018-12-03 17:01:56'),(392,'ESTG','Central (Viana)','1970-01-18 20:51:56',0,5,'2018-12-04 09:35:56'),(393,'ESTG','Central (Viana)','1970-01-18 20:52:05',0,5,'2018-12-04 12:06:16'),(394,'ESTG','Central (Viana)','1970-01-18 20:52:05',0,5,'2018-12-04 12:14:35'),(395,'ESTG','Central (Viana)','1970-01-18 20:52:05',0,5,'2018-12-04 12:17:17'),(396,'ESTG','Central (Viana)','1970-01-18 20:52:05',0,5,'2018-12-04 12:19:12'),(397,'ESTG','Central (Viana)','1970-01-18 20:52:05',0,5,'2018-12-04 12:19:39'),(398,'ESTG','Central (Viana)','1970-01-18 20:52:06',0,5,'2018-12-04 12:20:25'),(399,'ESTG','Central (Viana)','1970-01-18 20:52:06',0,5,'2018-12-04 12:20:40'),(400,'ESTG','Central (Viana)','1970-01-18 20:52:06',0,5,'2018-12-04 12:21:50'),(401,'ESTG','Central (Viana)','1970-01-18 20:52:06',0,5,'2018-12-04 12:24:34'),(402,'Braga','Central (Viana)','1970-01-18 20:52:06',0,1,'2018-12-04 12:24:40'),(403,'Braga','Central (Viana)','1970-01-18 20:52:06',0,1,'2018-12-04 12:31:05'),(404,'Braga','Central (Viana)','1970-01-18 20:52:07',1,0,'2018-12-04 12:39:15'),(405,'Braga','Central (Viana)','1970-01-18 20:52:07',0,1,'2018-12-04 12:43:34'),(406,'Braga','Central (Viana)','1970-01-18 20:52:08',0,1,'2018-12-04 12:59:24'),(407,'Central (Viana)','Braga','1970-01-18 20:52:08',0,1,'2018-12-04 12:59:50'),(408,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:01:08'),(409,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:01:19'),(410,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:05:10'),(411,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:05:23'),(412,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:06:08'),(413,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:07:55'),(414,'ESCE','Braga','1970-01-18 20:52:08',1,0,'2018-12-04 13:08:27'),(415,'ESCE','Braga','1970-01-18 20:52:09',1,0,'2018-12-04 13:12:32'),(416,'ESCE','Braga','1970-01-18 20:52:13',1,0,'2018-12-04 14:24:28'),(417,'ESCE','Braga','1970-01-18 20:52:13',1,0,'2018-12-04 14:25:46'),(418,'ESCE','Braga','1970-01-18 20:52:13',0,3,'2018-12-04 14:29:57'),(419,'ESCE','Braga','1970-01-18 20:52:14',0,3,'2018-12-04 14:39:16'),(420,'ESCE','Braga','1970-01-18 20:52:14',0,3,'2018-12-04 14:47:51'),(421,'ESCE','Braga','1970-01-18 20:52:15',0,3,'2018-12-04 14:51:45'),(422,'ESCE','Braga','1970-01-18 20:52:15',0,3,'2018-12-04 14:53:24'),(423,'ESCE','Braga','1970-01-18 20:52:15',0,3,'2018-12-04 14:56:37'),(424,'ESCE','Braga','1970-01-18 20:52:15',0,3,'2018-12-04 14:57:01'),(425,'Central (Viana)','Braga','1970-01-18 20:52:15',0,2,'2018-12-04 14:59:33'),(426,'ESTG','Braga','1970-01-18 20:52:15',0,3,'2018-12-04 14:59:39'),(427,'ESTG','Braga','1970-01-18 20:52:16',0,3,'2018-12-04 15:19:18'),(428,'ESTG','Central (Viana)','1970-01-18 20:52:21',0,5,'2018-12-04 16:46:18'),(429,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:47:16'),(430,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:49:31'),(431,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:50:40'),(432,'ESTG','Central (Viana)','1970-01-18 20:51:21',0,5,'2018-12-04 16:50:58'),(433,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:52:32'),(434,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:54:33'),(435,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:55:35'),(436,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:56:28'),(437,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:56:58'),(438,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 16:56:59'),(439,'ESTG','Central (Viana)','1970-01-18 20:52:22',0,5,'2018-12-04 17:02:39'),(440,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:03:22'),(441,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:04:13'),(442,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:05:03'),(443,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:05:17'),(444,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:06:22'),(445,'ESTG','Central (Viana)','1970-01-18 20:52:23',0,5,'2018-12-04 17:10:07'),(446,'ESTG','Central (P. Lima)','1970-01-18 20:52:23',0,7,'2018-12-04 17:17:03'),(447,'ESTG','Central (Viana)','1970-01-18 20:53:49',0,5,'2018-12-05 17:12:29'),(448,'ESTG','Central (Viana)','1970-01-18 20:53:50',0,5,'2018-12-05 17:18:23'),(449,'ESTG','Central (Viana)','1970-01-18 20:53:50',0,5,'2018-12-05 17:18:24'),(450,'ESTG','Central (P. Lima)','1970-01-18 20:53:50',0,7,'2018-12-05 17:19:23'),(451,'ESTG','Central (P. Lima)','1970-01-18 20:53:50',0,7,'2018-12-05 17:19:25'),(452,'ESTG','ESA','1970-01-18 20:53:52',0,2,'2018-12-05 17:47:30'),(453,'Porto','ESTG','1970-01-18 20:53:52',0,1,'2018-12-05 17:57:22'),(454,'Porto','Central (Viana)','1970-01-18 20:53:52',0,1,'2018-12-05 17:58:38'),(455,'ESTG','Central (P. Lima)','1970-01-18 20:54:05',0,7,'2018-12-05 21:31:34'),(456,'ESTG','Central (P. Lima)','1970-01-18 20:54:05',0,7,'2018-12-05 21:39:12'),(457,'ESTG','Central (P. Lima)','1970-01-18 20:54:05',0,7,'2018-12-05 21:39:14'),(458,'ESTG','Central (P. Lima)','1970-01-18 20:54:12',0,7,'2018-12-05 23:25:46'),(459,'ESTG','Central (P. Lima)','1970-01-18 20:54:12',0,7,'2018-12-05 23:30:16'),(460,'ESTG','Central (P. Lima)','1970-01-18 20:54:12',0,7,'2018-12-05 23:36:37'),(461,'ESTG','Central (P. Lima)','1970-01-18 20:54:13',0,7,'2018-12-05 23:37:22'),(462,'ESTG','Central (Viana)','1970-01-18 20:54:50',0,5,'2018-12-06 10:00:00'),(463,'Samonde','Central (Viana)','1970-01-18 20:54:50',0,1,'2018-12-06 10:00:47'),(464,'Samonde','Ponte da Barca','1970-01-18 20:54:50',0,0,'2018-12-06 10:07:00'),(465,'Madorra','Central (Viana)','1970-01-18 20:54:50',0,2,'2018-12-06 10:09:07'),(466,'Madorra','Central (P. Lima)','1970-01-18 20:54:50',0,1,'2018-12-06 10:09:17'),(467,'Cova','Central (P. Lima)','1970-01-18 20:54:51',0,1,'2018-12-06 10:10:21'),(468,'Santa Marta','Central (P. Lima)','1970-01-18 20:54:51',0,1,'2018-12-06 10:10:35'),(469,'Sistelo',' ESDL | Central ','1970-01-18 20:54:56',0,1,'2018-12-06 11:37:11'),(470,'Sistelo',' ESDL | Central ','1970-01-18 20:54:59',0,1,'2018-12-06 12:32:20'),(471,'Sistelo',' ESDL | Central ','1970-01-18 20:55:06',0,1,'2018-12-06 14:28:41'),(472,'Sistelo',' ESDL | Central ','1970-01-18 20:55:06',0,1,'2018-12-06 14:28:58'),(473,'Sistelo',' ESDL | Central ','1970-01-18 20:55:06',0,1,'2018-12-06 14:35:26'),(474,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:42:15'),(475,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:44:42'),(476,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:44:52'),(477,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:46:59'),(478,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:47:25'),(479,'Sistelo',' ESDL | Central ','1970-01-18 20:55:07',0,1,'2018-12-06 14:48:02'),(480,'Sistelo',' ESDL | Central ','1970-01-18 20:55:08',0,1,'2018-12-06 15:08:40'),(481,'Sistelo',' ESDL | Central ','1970-01-18 20:55:08',0,1,'2018-12-06 15:09:22'),(482,'Sistelo',' ESDL | Central ','1970-01-18 20:55:09',0,1,'2018-12-06 15:10:57'),(483,'Sistelo',' ESDL | Central ','1970-01-18 20:55:09',0,1,'2018-12-06 15:17:30'),(484,'Sistelo',' ESDL | Central ','1970-01-18 20:55:10',0,1,'2018-12-06 15:32:26'),(485,'Sistelo',' ESDL | Central ','1970-01-18 20:55:10',0,1,'2018-12-06 15:34:23'),(486,'Sistelo',' ESDL | Central ','1970-01-18 20:55:10',0,1,'2018-12-06 15:38:16'),(487,'Sistelo',' ESDL | Central ','1970-01-18 20:55:10',0,1,'2018-12-06 15:38:29'),(488,'Sistelo',' ESDL | Central ','1970-01-18 20:55:10',0,1,'2018-12-06 15:41:11'),(489,'Sistelo',' ESDL | Central ','1970-01-18 20:55:11',0,1,'2018-12-06 15:51:50'),(490,'Sistelo',' ESDL | Central ','1970-01-18 20:55:11',0,1,'2018-12-06 15:53:18'),(491,'Sistelo',' ESDL | Central ','1970-01-18 20:55:11',0,1,'2018-12-06 15:55:55'),(492,'Sistelo',' ESDL | Central ','1970-01-18 20:55:11',0,1,'2018-12-06 15:58:03'),(493,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:02:17'),(494,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:02:44'),(495,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:02:45'),(496,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:04:06'),(497,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:04:14'),(498,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:13:54'),(499,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:14'),(500,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:17'),(501,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:18'),(502,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:18'),(503,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:19'),(504,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:19'),(505,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:20'),(506,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:21'),(507,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:21'),(508,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:22'),(509,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:22'),(510,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:23'),(511,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:23'),(512,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:23'),(513,'Sistelo',' ESDL | Central ','1970-01-18 20:55:12',0,1,'2018-12-06 16:14:24'),(514,'Porto','ESCE','1970-01-18 20:55:12',0,4,'2018-12-06 16:15:47'),(515,'Porto','ESCE','1970-01-18 20:55:13',0,4,'2018-12-06 16:18:06'),(516,'Porto','ESCE','1970-01-18 20:55:13',0,4,'2018-12-06 16:18:52'),(517,'Porto','Central (Viana)','1970-01-18 20:55:13',0,2,'2018-12-06 16:29:35'),(518,'Porto','ESCE','1970-01-18 20:55:13',0,4,'2018-12-06 16:33:18'),(519,'Porto','ESCE','1970-01-18 20:55:14',0,4,'2018-12-06 16:42:22'),(520,'Porto','ESCE','1970-01-18 20:55:15',0,4,'2018-12-06 16:50:27'),(521,'Porto','ESCE','1970-01-18 20:55:15',0,4,'2018-12-06 16:50:59'),(522,'Porto','ESCE','1970-01-18 20:55:15',0,4,'2018-12-06 16:51:22'),(523,'Central (P. Lima)','ESCE','1970-01-18 20:55:15',0,2,'2018-12-06 16:51:43'),(524,'Central (P. Lima)','ESCE','1970-01-18 20:55:15',0,2,'2018-12-06 16:53:25'),(525,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:14:59'),(526,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:16:53'),(527,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:18:55'),(528,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:19:35'),(529,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:21:49'),(530,'Central (P. Lima)','ESCE','1970-01-18 20:55:16',0,2,'2018-12-06 17:22:53'),(531,'Central (P. Lima)','ESCE','1970-01-18 20:55:17',0,2,'2018-12-06 17:30:46'),(532,'Central (P. Lima)','ESCE','1970-01-18 20:55:17',0,2,'2018-12-06 17:38:08'),(533,'Central (P. Lima)','ESCE','1970-01-18 20:56:16',0,2,'2018-12-07 10:01:41'),(534,'Central (P. Lima)','ESCE','1970-01-18 20:56:16',0,2,'2018-12-07 10:02:33'),(535,'Central (Viana)','ESCE','1970-01-18 20:56:17',0,4,'2018-12-07 10:12:28'),(536,'Central (P. Lima)','ESCE','1970-01-18 20:56:18',0,2,'2018-12-07 10:23:07'),(537,'ESCE','Central (P. Lima)','1970-01-18 20:56:19',0,2,'2018-12-07 10:40:07'),(538,'ESCE','Central (P. Lima)','1970-01-18 20:56:19',0,2,'2018-12-07 10:44:01'),(539,'ESCE','Central (P. Lima)','1970-01-18 20:56:19',0,1,'2018-12-07 10:50:11'),(540,'Central (Viana)','ESCE','1970-01-18 20:56:20',0,0,'2018-12-07 10:56:31'),(541,'Central (Viana)','ESCE','1970-01-18 20:56:20',0,0,'2018-12-07 10:56:47'),(542,'Central (P. Lima)','ESCE','1970-01-18 20:56:20',0,1,'2018-12-07 10:56:56'),(543,'Samonde','Central (P. Lima)','1970-01-18 20:56:20',0,1,'2018-12-07 10:59:19'),(544,'Central (Viana)','ESCE','1970-01-18 20:56:20',0,0,'2018-12-07 10:59:55'),(545,'ESA','ESCE','1970-01-18 20:56:20',0,1,'2018-12-07 11:00:40'),(546,'ESA','Central (P. Lima)','1970-01-18 20:56:20',0,3,'2018-12-07 11:01:12'),(547,'Ponte da Barca','Central (P. Lima)','1970-01-18 20:56:20',0,1,'2018-12-07 11:03:44'),(548,'Ponte da Barca','ESTG','1970-01-18 20:56:20',0,0,'2018-12-07 11:04:12'),(549,'Ponte da Barca','ESTG','1970-01-18 20:56:20',0,0,'2018-12-07 11:04:53'),(550,'ESTG','Ponte da Barca','1970-01-18 20:56:20',0,0,'2018-12-07 11:09:33'),(551,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:20',0,0,'2018-12-07 11:09:51'),(552,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:12:14'),(553,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:13:28'),(554,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:13:57'),(555,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:15:06'),(556,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:16:41'),(557,'ESE','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:17:22'),(558,'Santa Marta','Central (P. Lima)','1970-01-18 20:56:21',0,1,'2018-12-07 11:19:14'),(559,'Central (Viana)','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:20:39'),(560,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:24:57'),(561,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:24:57'),(562,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:24:59'),(563,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:26:34'),(564,'ESTG','Ponte da Barca','1970-01-18 20:56:21',0,0,'2018-12-07 11:26:35'),(565,'ESTG','Ponte da Barca','1970-01-18 20:56:23',0,0,'2018-12-07 11:54:51'),(566,'ESTG','Ponte da Barca','1970-01-18 20:56:23',0,0,'2018-12-07 11:57:23'),(567,'ESTG','Ponte da Barca','1970-01-18 20:56:23',0,0,'2018-12-07 11:58:45'),(568,'ESTG','Ponte da Barca','1970-01-18 20:56:24',0,0,'2018-12-07 12:01:52'),(569,'ESTG','Ponte da Barca','1970-01-18 20:56:24',0,0,'2018-12-07 12:03:08'),(570,'ESTG','ESCE','1970-01-18 20:56:24',0,0,'2018-12-07 12:05:40'),(571,'Ponte da Barca','ESTG','1970-01-18 20:56:24',0,0,'2018-12-07 12:07:11'),(572,'Ponte da Barca','ESTG','1970-01-18 20:56:24',0,0,'2018-12-07 12:10:16'),(573,'ESCE','Areosa','1970-01-18 20:56:24',0,1,'2018-12-07 12:12:33'),(574,'Central (Monção)','ESTG','1970-01-18 20:56:25',0,1,'2018-12-07 12:16:52'),(575,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:17:17'),(576,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:17:43'),(577,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:39'),(578,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:41'),(579,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:41'),(580,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:42'),(581,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:43'),(582,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:44'),(583,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:44'),(584,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:19:44'),(585,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:21:58'),(586,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:23:32'),(587,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:26:28'),(588,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:27:36'),(589,'Ponte da Barca','ESTG','1970-01-18 20:56:25',0,0,'2018-12-07 12:31:28'),(590,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,0,'2018-12-07 12:33:24'),(591,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,0,'2018-12-07 12:37:52'),(592,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,0,'2018-12-07 12:38:39'),(593,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,0,'2018-12-07 12:39:51'),(594,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,0,'2018-12-07 12:40:20'),(595,'Ponte da Barca','ESTG','1970-01-18 20:56:26',0,1,'2018-12-07 12:45:00'),(596,'ESTG','Ponte da Barca','1970-01-18 20:56:26',0,0,'2018-12-07 12:45:56'),(597,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:46:45'),(598,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:47:23'),(599,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:47:48'),(600,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:48:26'),(601,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:48:37'),(602,'ESTG','ESCE','1970-01-18 20:56:26',0,0,'2018-12-07 12:48:45'),(603,'ESTG','ESCE','1970-01-18 20:56:31',0,1,'2018-12-07 14:09:50'),(604,'ESTG','ESCE','1970-01-18 20:56:32',0,1,'2018-12-07 14:15:27'),(605,'ESTG','Ponte da Barca','1970-01-18 21:00:36',0,0,'2018-12-10 10:10:44'),(606,'ESTG','Central (Viana)','1970-01-18 21:00:36',0,4,'2018-12-10 10:10:47'),(607,'ESTG','ESCE','1970-01-18 21:00:36',0,1,'2018-12-10 10:14:27'),(608,'ESTG','ESCE','1970-01-18 21:00:42',0,1,'2018-12-10 11:45:10'),(609,'ESTG','ESCE','1970-01-18 21:00:42',0,1,'2018-12-10 11:46:26'),(610,'ESTG','ESCE','1970-01-18 21:00:42',0,1,'2018-12-10 11:53:45'),(611,'ESTG','ESCE','1970-01-18 21:00:42',0,1,'2018-12-10 11:55:10'),(612,'ESTG','ESCE','1970-01-18 21:00:43',0,1,'2018-12-10 12:07:32'),(613,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:34:16'),(614,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:37:19'),(615,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:39:57'),(616,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:40:41'),(617,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:45:31'),(618,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:46:03'),(619,'ESTG','ESCE','1970-01-18 21:00:45',0,1,'2018-12-10 12:46:15'),(620,'ESTG','ESCE','1970-01-18 21:00:46',0,1,'2018-12-10 12:46:42'),(621,'ESTG','ESCE','1970-01-18 21:00:46',0,1,'2018-12-10 13:02:31'),(622,'ESTG','ESCE','1970-01-18 21:00:51',0,1,'2018-12-10 14:23:18'),(623,'Porto','ESCE','1970-01-18 21:00:52',0,4,'2018-12-10 14:33:11'),(624,'Porto','ESCE','1970-01-18 21:00:52',1,0,'2018-12-10 14:43:14'),(625,'Porto','ESCE','1970-01-18 21:00:53',0,4,'2018-12-10 14:43:51'),(626,'Porto','ESCE','1970-01-18 21:00:53',0,4,'2018-12-10 14:44:22'),(627,'Porto','ESCE','1970-01-18 21:00:53',0,4,'2018-12-10 14:57:37'),(628,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:00:54'),(629,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:04:32'),(630,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:05:28'),(631,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:05:51'),(632,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:06:39'),(633,'Porto','ESCE','1970-01-18 21:00:54',1,0,'2018-12-10 15:08:18'),(634,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:08:43'),(635,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:10:22'),(636,'Porto','ESCE','1970-01-18 21:00:54',1,0,'2018-12-10 15:10:35'),(637,'Porto','ESCE','1970-01-18 21:00:54',0,4,'2018-12-10 15:10:58'),(638,'Porto','ESCE','1970-01-18 21:00:55',0,4,'2018-12-10 15:16:50'),(639,'Porto','ESCE','1970-01-18 21:00:55',1,0,'2018-12-10 15:24:45'),(640,'Porto','ESCE','1970-01-18 21:00:55',1,0,'2018-12-10 15:25:29'),(641,'Porto','ESCE','1970-01-18 21:00:55',1,0,'2018-12-10 15:25:50'),(642,'Porto','ESCE','1970-01-18 21:00:55',1,0,'2018-12-10 15:26:56'),(643,'Porto','ESCE','1970-01-18 21:00:55',0,4,'2018-12-10 15:28:10'),(644,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:34:28'),(645,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:35:54'),(646,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:36:28'),(647,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:37:48'),(648,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:37:55'),(649,'Porto','ESCE','1970-01-18 21:00:56',0,4,'2018-12-10 15:39:23'),(650,'Porto','ESCE','1970-01-18 21:00:58',0,4,'2018-12-10 16:16:11'),(651,'Porto','ESCE','1970-01-18 21:00:58',1,0,'2018-12-10 16:23:03'),(652,'Porto','ESCE','1970-01-18 21:00:59',0,4,'2018-12-10 16:23:20'),(653,'Porto','ESCE','1970-01-18 21:00:59',0,4,'2018-12-10 16:24:07'),(654,'Porto','ESCE','1970-01-18 21:00:59',0,4,'2018-12-10 16:27:58'),(655,'Porto','ESCE','1970-01-18 21:00:59',0,4,'2018-12-10 16:36:12'),(656,'Porto','ESCE','1970-01-18 21:00:59',0,4,'2018-12-10 16:38:11'),(657,'Porto','Central (Viana)','1970-01-18 21:01:00',0,2,'2018-12-10 16:40:34'),(658,'Porto','ESTG','1970-01-18 21:01:00',0,1,'2018-12-10 16:42:44'),(659,'Porto','ESTG','1970-01-18 21:01:00',0,1,'2018-12-10 16:47:35'),(660,'ESTG','Porto','1970-01-18 21:01:01',0,1,'2018-12-10 16:59:25'),(661,'ESTG','Porto','1970-01-18 21:01:01',0,1,'2018-12-10 17:01:01'),(662,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:20:20'),(663,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:21:04'),(664,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:23:54'),(665,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:27:15'),(666,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:27:42'),(667,'ESTG','Porto','1970-01-18 21:01:02',0,1,'2018-12-10 17:28:18'),(668,'ESTG','Porto','1970-01-18 21:01:03',0,1,'2018-12-10 17:30:33'),(669,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:01',0,6,'2018-12-11 09:44:31'),(670,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:01:08'),(671,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:37'),(672,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:40'),(673,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:41'),(674,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:42'),(675,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:43'),(676,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:43'),(677,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:44'),(678,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:44'),(679,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:45'),(680,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:45'),(681,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:46'),(682,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:47'),(683,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:07:48'),(684,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:08:14'),(685,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:08:14'),(686,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:02',0,3,'2018-12-11 10:08:15'),(687,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:11:29'),(688,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:11:33'),(689,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:17:05'),(690,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:17:42'),(691,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:18:36'),(692,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:19:15'),(693,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:03',0,3,'2018-12-11 10:21:36'),(694,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:04',0,3,'2018-12-11 10:37:56'),(695,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:05',0,3,'2018-12-11 10:43:51'),(696,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:05',0,3,'2018-12-11 10:43:53'),(697,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:05',0,3,'2018-12-11 10:44:37'),(698,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:05',0,3,'2018-12-11 10:45:20'),(699,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:05',0,3,'2018-12-11 10:45:22'),(700,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:06',0,3,'2018-12-11 11:01:30'),(701,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:07',0,3,'2018-12-11 11:27:39'),(702,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:33:27'),(703,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:36:37'),(704,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:37:11'),(705,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:38:39'),(706,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:39:23'),(707,'Central (Viana)','Central (P. Lima)','1970-01-18 21:02:08',0,3,'2018-12-11 11:42:40'),(708,'Central (Viana)','ESTG','1970-01-18 21:02:09',0,4,'2018-12-11 11:52:24'),(709,'Central (Viana)','ESTG','1970-01-18 21:02:10',0,4,'2018-12-11 12:20:57'),(710,'Central (Viana)','Paredes de Coura','1970-01-18 21:02:10',0,3,'2018-12-11 12:21:36'),(711,'Central (Viana)','Paredes de Coura','1970-01-18 21:02:11',0,3,'2018-12-11 12:31:36'),(712,'Central (Viana)','Paredes de Coura','1970-01-18 21:02:11',0,3,'2018-12-11 12:32:41'),(713,'Central (Viana)','Paredes de Coura','1970-01-18 21:02:11',0,3,'2018-12-11 12:32:54'),(714,'Central (Viana)','Paredes de Coura','1970-01-18 21:02:11',0,3,'2018-12-11 12:36:06'),(715,'Ponte da Barca','ESTG','1970-01-18 21:02:25',0,0,'2018-12-11 16:21:07'),(716,'Central (Viana)','ESTG','1970-01-18 21:02:25',0,4,'2018-12-11 16:21:12'),(717,'ESCE','ESTG','1970-01-18 21:02:25',0,1,'2018-12-11 16:21:18'),(718,'ESCE','ESTG','1970-01-18 21:02:27',0,1,'2018-12-11 16:53:01'),(719,'ESCE','ESTG','1970-01-18 21:02:27',0,1,'2018-12-11 16:55:18'),(720,'ESCE','ESTG','1970-01-18 21:02:27',0,1,'2018-12-11 16:55:43'),(721,'ESCE','ESTG','1970-01-18 21:02:27',0,1,'2018-12-11 16:58:27'),(722,'ESCE','ESTG','1970-01-18 21:02:27',0,1,'2018-12-11 16:59:23'),(723,'ESCE','Porto','1970-01-18 21:02:27',0,4,'2018-12-11 17:04:01'),(724,'ESCE','Porto','1970-01-18 21:02:28',0,4,'2018-12-11 17:09:50'),(725,'ESCE','Lisboa','1970-01-18 21:02:28',0,3,'2018-12-11 17:21:10'),(726,'ESCE','Café Stop','1970-01-18 21:02:28',0,1,'2018-12-11 17:21:19'),(727,'Central (P. Lima)','ESTG','1970-01-18 21:03:28',0,3,'2018-12-12 09:55:38'),(728,'Central (P. Lima)','ESTG','1970-01-18 21:03:28',0,3,'2018-12-12 09:56:51'),(729,'ESCE','ESTG','1970-01-18 21:03:28',0,1,'2018-12-12 09:57:17'),(730,'ESCE','ESTG','1970-01-18 21:03:28',0,1,'2018-12-12 10:00:31'),(731,'ESA','Porto','1970-01-18 21:03:29',0,3,'2018-12-12 10:07:57'),(732,'ESA','Porto','1970-01-18 21:03:39',0,3,'2018-12-12 12:58:18'),(733,'ESA','Porto','1970-01-18 21:03:39',0,3,'2018-12-12 13:01:05'),(734,'ESA','Porto','1970-01-18 21:03:39',0,3,'2018-12-12 13:02:36'),(735,'ESA','Porto','1970-01-18 21:03:40',0,3,'2018-12-12 13:08:29'),(736,'ESA','Porto','1970-01-18 21:03:40',0,3,'2018-12-12 13:09:12'),(737,'ESA','Porto','1970-01-18 21:03:40',0,3,'2018-12-12 13:09:28'),(738,'ESA','Porto','1970-01-18 21:03:45',0,0,'2018-12-12 14:39:45'),(739,'ESA','Porto','1970-01-18 21:03:45',0,3,'2018-12-12 14:41:07'),(740,'ESA','Porto','1970-01-18 21:03:45',0,3,'2018-12-12 14:41:27'),(741,'ESA','Porto','1970-01-18 21:03:45',0,3,'2018-12-12 14:46:00'),(742,'ESA','Porto','1970-01-18 21:03:46',0,2,'2018-12-12 14:47:08'),(743,'Central (Viana)','ESTG','1970-01-18 21:03:50',0,4,'2018-12-12 16:05:43'),(744,'Central (Viana)','ESTG','1970-01-18 21:03:50',0,4,'2018-12-12 16:09:34'),(745,'Central (Viana)','ESTG','1970-01-18 21:03:51',0,4,'2018-12-12 16:16:55'),(746,'Central (Viana)','ESTG','1970-01-18 21:03:51',0,4,'2018-12-12 16:18:06'),(747,'Central (Viana)','ESTG','1970-01-18 21:03:51',0,4,'2018-12-12 16:18:52'),(748,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:45:49'),(749,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:47:31'),(750,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:47:48'),(751,'Central (Viana)','ESTG','1970-01-18 21:03:53',1,0,'2018-12-12 16:48:43'),(752,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:49:48'),(753,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:50:15'),(754,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:51:13'),(755,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:55:43'),(756,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:56:31'),(757,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:57:32'),(758,'Central (Viana)','ESTG','1970-01-18 21:03:53',0,4,'2018-12-12 16:59:34'),(759,'Central (Viana)','ESTG','1970-01-18 21:03:54',0,4,'2018-12-12 17:01:18'),(760,'Central (Viana)','ESTG','1970-01-18 21:03:54',0,4,'2018-12-12 17:06:59'),(761,'Central (Viana)','ESTG','1970-01-18 21:03:57',0,4,'2018-12-12 18:01:07'),(762,'Central (Viana)','ESTG','1970-01-18 21:04:54',0,4,'2018-12-13 09:43:47'),(763,'ESCE','ESTG','1970-01-18 21:04:55',0,1,'2018-12-13 10:08:23'),(764,'ESCE','ESTG','1970-01-18 21:04:56',0,1,'2018-12-13 10:22:08'),(765,'ESCE','ESTG','1970-01-18 21:04:57',0,1,'2018-12-13 10:45:41'),(766,'Central (Viana)','ESTG','1970-01-18 21:05:22',0,4,'2018-12-13 17:34:30'),(767,'Central (Viana)','ESTG','1970-01-18 21:06:28',0,4,'2018-12-14 11:54:01'),(768,'Central (Viana)','Porto','1970-01-18 21:06:30',0,0,'2018-12-14 12:24:33'),(769,'ESTG','Porto','1970-01-18 21:06:30',0,1,'2018-12-14 12:25:43'),(770,'ESTG','Porto','1970-01-18 21:06:30',0,1,'2018-12-14 12:30:21'),(771,'ESTG','Porto','1970-01-18 21:06:30',0,1,'2018-12-14 12:33:05'),(772,'ESTG','Porto','1970-01-18 21:06:30',0,1,'2018-12-14 12:34:16'),(773,'ESTG','Porto','1970-01-18 21:06:31',0,0,'2018-12-14 12:41:39'),(774,'ESTG','Porto','1970-01-18 21:06:31',0,0,'2018-12-14 12:41:47'),(775,'Central (Viana)','Porto','1970-01-18 21:06:31',0,1,'2018-12-14 12:42:15'),(776,'ESTG','Porto','1970-01-18 21:06:37',0,1,'2018-12-14 14:24:57'),(777,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:15:56'),(778,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:00'),(779,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:03'),(780,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:04'),(781,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:05'),(782,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:06'),(783,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:07'),(784,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:08'),(785,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:08'),(786,'ESTG','Porto','1970-01-18 21:06:40',0,1,'2018-12-14 15:16:09'),(787,'Central (Viana)','ESTG','1970-01-18 21:12:14',1,0,'2018-12-18 12:02:31'),(788,'Central (Viana)','ESTG','1970-01-18 21:12:15',0,4,'2018-12-18 12:24:39'),(789,'ESTG','Porto','1970-01-18 21:12:15',0,1,'2018-12-18 12:24:52'),(790,'ESTG','Madrid','1970-01-18 21:12:15',0,1,'2018-12-18 12:25:13'),(791,'ESTG','Paris','1970-01-18 21:12:15',0,0,'2018-12-18 12:25:35'),(792,'ESTG','Paris','1970-01-18 21:12:15',0,0,'2018-12-18 12:26:02'),(793,'ESCE','Paris','1970-01-18 21:12:15',0,1,'2018-12-18 12:26:10'),(794,'ESCE','Paris','1970-01-18 21:12:17',0,1,'2018-12-18 12:58:06'),(795,'ESTG','Porto','1970-01-18 21:13:37',1,0,'2018-12-19 11:12:31'),(796,'ESTG','Porto','1970-01-18 21:13:38',1,0,'2018-12-19 11:14:19'),(797,'ESTG','Porto','1970-01-18 21:13:38',1,0,'2018-12-19 11:17:18'),(798,'ESTG','Porto','1970-01-18 21:13:38',1,0,'2018-12-19 11:17:51'),(799,'ESTG','Madrid','1970-01-18 21:13:38',1,0,'2018-12-19 11:18:08'),(800,'ESTG','Madrid','1970-01-18 21:13:38',1,0,'2018-12-19 11:18:33'),(801,'ESTG','Viseu','1970-01-18 21:13:38',1,0,'2018-12-19 11:18:43'),(802,'ESTG','Aveiro','1970-01-18 21:13:38',1,0,'2018-12-19 11:18:53'),(803,'ESTG','mocovo','1970-01-18 21:13:38',0,0,'2018-12-19 11:19:10'),(804,'ESTG','Macau','1970-01-18 21:13:38',0,0,'2018-12-19 11:19:26'),(805,'ESTG','Campo, Barcelos','1970-01-18 21:13:38',0,0,'2018-12-19 11:19:45'),(806,'ESTG','Campo, Barcelos','1970-01-18 21:13:38',0,0,'2018-12-19 11:20:37'),(807,'ESTG','Campo, Barcelos','1970-01-18 21:13:39',0,0,'2018-12-19 11:37:34'),(808,'ESTG','Barcelos','1970-01-18 21:13:39',1,0,'2018-12-19 11:38:01'),(809,'ESTG','Madrid','1970-01-18 21:13:39',1,0,'2018-12-19 11:38:17'),(810,'ESTG','Rua de talhos','1970-01-18 21:13:39',0,0,'2018-12-19 11:39:05'),(811,'ESTG','Rua de talhos','1970-01-18 21:13:40',0,0,'2018-12-19 11:58:34'),(812,'ESTG','Madrid','1970-01-18 21:13:40',1,0,'2018-12-19 11:58:44'),(813,'Central (Viana)','ESTG','1970-01-18 21:15:39',1,0,'2018-12-20 21:05:45'),(814,'Central (Viana)','ESTG','1970-01-18 21:27:00',1,0,'2018-12-28 18:13:56'),(815,'Central (Viana)','ESTG','1970-01-18 21:27:00',1,0,'2018-12-28 18:15:18'),(816,'Central (Viana)','ESTG','1970-01-18 21:27:00',1,0,'2018-12-28 18:15:24'),(817,'Central (Viana)','ESTG','1970-01-18 21:27:00',1,0,'2018-12-28 18:15:25'),(818,'Central (Viana)','ESTG','1970-01-18 21:27:00',1,0,'2018-12-28 18:15:29'),(819,'Central (Viana)','ESTG','1970-01-18 21:27:01',1,0,'2018-12-28 18:18:25'),(820,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:40:25'),(821,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:41:34'),(822,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:42:56'),(823,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:42:59'),(824,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:43:00'),(825,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:45:59'),(826,'Central (Viana)','ESTG','1970-01-18 21:27:02',1,0,'2018-12-28 18:47:49'),(827,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:51:12'),(828,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:52:10'),(829,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:52:13'),(830,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:52:51'),(831,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:53:04'),(832,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:55:56'),(833,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:56:04'),(834,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:56:04'),(835,'Central (Viana)','ESTG','1970-01-18 21:27:03',1,0,'2018-12-28 18:56:04'),(836,'Central (Viana)','ESTG','1970-01-18 21:27:03',0,4,'2018-12-28 18:56:50'),(837,'Central (Viana)','ESTG','1970-01-18 21:27:03',0,4,'2018-12-28 19:00:44'),(838,'Central (Viana)','ESTG','1970-01-18 21:33:49',0,4,'2019-01-02 11:50:23'),(839,'Central (Viana)','ESTG','1970-01-18 21:35:16',0,4,'2019-01-03 12:01:23'),(840,'Darque','ESTG','1970-01-18 21:35:17',0,1,'2019-01-03 12:05:16'),(841,'Darque','ESTG','1970-01-18 21:35:18',0,1,'2019-01-03 12:24:10'),(842,'Darque','ESTG','1970-01-18 21:35:18',0,1,'2019-01-03 12:24:32'),(843,'Darque','ESTG','1970-01-18 21:35:18',0,1,'2019-01-03 12:30:50'),(844,'Darque','ESTG','1970-01-18 21:35:18',0,1,'2019-01-03 12:32:51'),(845,'Darque','ESTG','1970-01-18 21:35:28',0,1,'2019-01-03 15:08:04'),(846,'Darque','ESTG','1970-01-18 21:35:28',0,1,'2019-01-03 15:09:24'),(847,'Darque','ESTG','1970-01-18 21:35:29',0,1,'2019-01-03 15:34:20'),(848,'Darque','ESTG','1970-01-18 21:35:29',0,1,'2019-01-03 15:37:45'),(849,'Darque','ESTG','1970-01-18 21:35:31',0,1,'2019-01-03 16:02:31'),(850,'Darque','ESTG','1970-01-18 21:35:31',0,1,'2019-01-03 16:05:41'),(851,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:33:46'),(852,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:19'),(853,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:20'),(854,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:20'),(855,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:21'),(856,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:21'),(857,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:22'),(858,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:22'),(859,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:23'),(860,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:23'),(861,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:24'),(862,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:25'),(863,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:25'),(864,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:26'),(865,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:27'),(866,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:27'),(867,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:32'),(868,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:33'),(869,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:33'),(870,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:34:34'),(871,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:41'),(872,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:52'),(873,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:53'),(874,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:53'),(875,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:53'),(876,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:54'),(877,'Darque','ESTG','1970-01-18 21:35:33',0,1,'2019-01-03 16:38:55'),(878,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:02'),(879,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:06'),(880,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:07'),(881,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:07'),(882,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:08'),(883,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:08'),(884,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:09'),(885,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:09'),(886,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:09'),(887,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:54:10'),(888,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:55:41'),(889,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:56:10'),(890,'Darque','ESTG','1970-01-18 21:35:34',1,0,'2019-01-03 16:56:49'),(891,'Darque','ESTG','1970-01-18 21:35:34',0,1,'2019-01-03 16:57:08'),(892,'Darque','ESTG','1970-01-18 21:35:34',0,1,'2019-01-03 16:58:13'),(893,'Central (Viana)','ESTG','1970-01-18 21:36:37',0,4,'2019-01-04 10:25:28'),(894,'Madrid','ESTG','1970-01-18 21:36:43',1,0,'2019-01-04 12:04:47'),(895,'Central (Viana)','ESTG','1970-01-18 21:36:43',0,4,'2019-01-04 12:07:19'),(896,'Central (Viana)','ESTG','1970-01-18 21:40:55',0,0,'2019-01-07 10:00:53'),(897,'Central (Viana)','ESTG','1970-01-18 21:40:55',0,0,'2019-01-07 10:08:25'),(898,'Central (Viana)','ESTG','1970-01-18 21:42:29',0,4,'2019-01-08 12:13:24'),(899,'Central (Monção)','ESTG','1970-01-18 21:42:29',0,1,'2019-01-08 12:13:41'),(900,'Ponte da Barca','ESTG','1970-01-18 21:42:29',0,0,'2019-01-08 12:13:48'),(901,'Central (Viana)','ESTG','1970-01-18 21:42:29',0,4,'2019-01-08 12:13:53'),(902,'ESCE','Central (P. Lima)','1970-01-18 21:42:29',0,1,'2019-01-08 12:14:06'),(903,'Central (Viana)','ESTG','1970-01-18 21:42:30',0,4,'2019-01-08 12:27:08'),(904,'Central (Viana)','ESTG','1970-01-18 21:42:30',0,4,'2019-01-08 12:29:16'),(905,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:33:40'),(906,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:38:02'),(907,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:38:04'),(908,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:38:10'),(909,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:38:10'),(910,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:38:11'),(911,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:40:05'),(912,'Central (Viana)','ESTG','1970-01-18 21:56:51',0,4,'2019-01-18 11:40:32'),(913,'Central (Viana)','ESTG','1970-01-18 22:01:05',0,4,'2019-01-21 10:07:05'),(914,'Central (Viana)','ESTG','1970-01-18 22:01:06',0,4,'2019-01-21 10:25:07'),(915,'Central (Viana)','ESTG','1970-01-18 22:01:20',0,4,'2019-01-21 14:26:21'),(916,'Central (Viana)','ESTG','1970-01-18 22:01:20',0,4,'2019-01-21 14:27:36'),(917,'Central (Viana)','ESTG','1970-01-18 22:01:21',0,4,'2019-01-21 14:37:34'),(918,'Central (Viana)','ESTG','1970-01-18 22:01:28',0,4,'2019-01-21 16:34:25'),(919,'Central (Viana)','ESTG','1970-01-18 22:01:29',0,4,'2019-01-21 16:51:18'),(920,'Central (Viana)','ESTG','1970-01-18 22:02:32',0,4,'2019-01-22 10:14:07'),(921,'Central (Viana)','ESTG','1970-01-18 22:02:41',0,4,'2019-01-22 12:46:15'),(922,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:21:17'),(923,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:22:04'),(924,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:23:56'),(925,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:28:27'),(926,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:29:03'),(927,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:29:39'),(928,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:29:58'),(929,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:32:52'),(930,'Central (Viana)','ESTG','1970-01-18 22:02:54',0,4,'2019-01-22 16:34:22'),(931,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:36:45'),(932,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:42:52'),(933,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:43:32'),(934,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:47:17'),(935,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:47:53'),(936,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:07'),(937,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:07'),(938,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:07'),(939,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:08'),(940,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:08'),(941,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:08'),(942,'Central (Viana)','ESTG','1970-01-18 22:02:55',0,4,'2019-01-22 16:49:08');
/*!40000 ALTER TABLE `log_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `period`
--

LOCK TABLES `period` WRITE;
/*!40000 ALTER TABLE `period` DISABLE KEYS */;
INSERT INTO `period` VALUES (2,'Época Normal'),(3,'Época de teste'),(4,'Época de exames'),(5,'Época de férias');
/*!40000 ALTER TABLE `period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_district` int(11) DEFAULT NULL,
  `cod_place` int(11) DEFAULT NULL,
  `place` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
INSERT INTO `place` VALUES (1,1,6,'Castelo de Paiva'),(2,1,7,'Espinho'),(3,1,8,'Estarreja'),(4,1,9,'Santa Maria da Feira'),(5,1,10,'Ílhavo'),(6,1,11,'Mealhada'),(7,1,12,'Murtosa'),(8,1,13,'Oliveira de Azeméis'),(9,1,14,'Oliveira do Bairro'),(10,1,15,'Ovar'),(11,1,16,'São João da Madeira'),(12,1,17,'Sever do Vouga'),(13,1,18,'Vagos'),(14,1,19,'Vale de Cambra'),(15,1,1,'Águeda'),(16,1,2,'Albergaria-a-Velha'),(17,1,3,'Anadia'),(18,1,4,'Arouca'),(19,1,5,'Aveiro'),(20,2,6,'Castro Verde'),(21,2,7,'Cuba'),(22,2,8,'Ferreira do Alentejo'),(23,2,9,'Mértola'),(24,2,10,'Moura'),(25,2,11,'Odemira'),(26,2,12,'Ourique'),(27,2,13,'Serpa'),(28,2,14,'Vidigueira'),(29,2,1,'Aljustrel'),(30,2,2,'Almodôvar'),(31,2,3,'Alvito'),(32,2,4,'Barrancos'),(33,2,5,'Beja'),(34,3,6,'Esposende'),(35,3,7,'Fafe'),(36,3,8,'Guimarães'),(37,3,9,'Póvoa de Lanhoso'),(38,3,10,'Terras de Bouro'),(39,3,11,'Vieira do Minho'),(40,3,12,'Vila Nova de Famalicão'),(41,3,13,'Vila Verde'),(42,3,14,'Vizela'),(43,3,1,'Amares'),(44,3,2,'Barcelos'),(45,3,3,'Braga'),(46,3,4,'Cabeceiras de Basto'),(47,3,5,'Celorico de Basto'),(48,4,6,'Miranda do Douro'),(49,4,7,'Mirandela'),(50,4,8,'Mogadouro'),(51,4,9,'Torre de Moncorvo'),(52,4,10,'Vila Flor'),(53,4,11,'Vimioso'),(54,4,12,'Vinhais'),(55,4,1,'Alfândega da Fé'),(56,4,2,'Bragança'),(57,4,3,'Carrazeda de Ansiães'),(58,4,4,'Freixo de Espada à Cinta'),(59,4,5,'Macedo de Cavaleiros'),(60,5,6,'Oleiros'),(61,5,7,'Penamacor'),(62,5,8,'Proença-a-Nova'),(63,5,9,'Sertã'),(64,5,10,'Vila de Rei'),(65,5,11,'Vila Velha de Ródão'),(66,5,1,'Belmonte'),(67,5,2,'Castelo Branco'),(68,5,3,'Covilhã'),(69,5,4,'Fundão'),(70,5,5,'Idanha-a-Nova'),(71,6,6,'Góis'),(72,6,7,'Lousã'),(73,6,8,'Mira'),(74,6,9,'Miranda do Corvo'),(75,6,10,'Montemor-o-Velho'),(76,6,11,'Oliveira do Hospital'),(77,6,12,'Pampilhosa da Serra'),(78,6,13,'Penacova'),(79,6,14,'Penela'),(80,6,15,'Soure'),(81,6,16,'Tábua'),(82,6,17,'Vila Nova de Poiares'),(83,6,1,'Arganil'),(84,6,2,'Cantanhede'),(85,6,3,'Coimbra'),(86,6,4,'Condeixa-a-Nova'),(87,6,5,'Figueira da Foz'),(88,7,6,'Montemor-o-Novo'),(89,7,7,'Mora'),(90,7,8,'Mourão'),(91,7,9,'Portel'),(92,7,10,'Redondo'),(93,7,11,'Reguengos de Monsaraz'),(94,7,12,'Vendas Novas'),(95,7,13,'Viana do Alentejo'),(96,7,14,'Vila Viçosa'),(97,7,1,'Alandroal'),(98,7,2,'Arraiolos'),(99,7,3,'Borba'),(100,7,4,'Estremoz'),(101,7,5,'Évora'),(102,8,6,'Lagoa (Algarve)'),(103,8,7,'Lagos'),(104,8,8,'Loulé'),(105,8,9,'Monchique'),(106,8,10,'Olhão'),(107,8,11,'Portimão'),(108,8,12,'São Brás de Alportel'),(109,8,13,'Silves'),(110,8,14,'Tavira'),(111,8,15,'Vila do Bispo'),(112,8,16,'Vila Real de Santo António'),(113,8,1,'Albufeira'),(114,8,2,'Alcoutim'),(115,8,3,'Aljezur'),(116,8,4,'Castro Marim'),(117,8,5,'Faro'),(118,9,6,'Gouveia'),(119,9,7,'Guarda'),(120,9,8,'Manteigas'),(121,9,9,'Meda'),(122,9,10,'Pinhel'),(123,9,11,'Sabugal'),(124,9,12,'Seia'),(125,9,13,'Trancoso'),(126,9,14,'Vila Nova de Foz Côa'),(127,9,1,'Aguiar da Beira'),(128,9,2,'Almeida'),(129,9,3,'Celorico da Beira'),(130,9,4,'Figueira de Castelo Rodrigo'),(131,9,5,'Fornos de Algodres'),(132,10,6,'Caldas da Rainha'),(133,10,7,'Castanheira de Pêra'),(134,10,8,'Figueiró dos Vinhos'),(135,10,9,'Leiria'),(136,10,10,'Marinha Grande'),(137,10,11,'Nazaré'),(138,10,12,'Óbidos'),(139,10,13,'Pedrógão Grande'),(140,10,14,'Peniche'),(141,10,15,'Pombal'),(142,10,16,'Porto de Mós'),(143,10,1,'Alcobaça'),(144,10,2,'Alvaiázere'),(145,10,3,'Ansião'),(146,10,4,'Batalha'),(147,10,5,'Bombarral'),(148,11,6,'Lisboa'),(149,11,7,'Loures'),(150,11,8,'Lourinhã'),(151,11,9,'Mafra'),(152,11,10,'Oeiras'),(153,11,11,'Sintra'),(154,11,12,'Sobral de Monte Agraço'),(155,11,13,'Torres Vedras'),(156,11,14,'Vila Franca de Xira'),(157,11,15,'Amadora'),(158,11,16,'Odivelas'),(159,11,1,'Alenquer'),(160,11,2,'Arruda dos Vinhos'),(161,11,3,'Azambuja'),(162,11,4,'Cadaval'),(163,11,5,'Cascais'),(164,12,6,'Crato'),(165,12,7,'Elvas'),(166,12,8,'Fronteira'),(167,12,9,'Gavião'),(168,12,10,'Marvão'),(169,12,11,'Monforte'),(170,12,12,'Nisa'),(171,12,13,'Ponte de Sor'),(172,12,14,'Portalegre'),(173,12,15,'Sousel'),(174,12,1,'Alter do Chão'),(175,12,2,'Arronches'),(176,12,3,'Avis'),(177,12,4,'Campo Maior'),(178,12,5,'Castelo de Vide'),(179,13,6,'Maia'),(180,13,7,'Marco de Canaveses'),(181,13,8,'Matosinhos'),(182,13,9,'Paços de Ferreira'),(183,13,10,'Paredes'),(184,13,11,'Penafiel'),(185,13,12,'Porto'),(186,13,13,'Póvoa de Varzim'),(187,13,14,'Santo Tirso'),(188,13,15,'Valongo'),(189,13,16,'Vila do Conde'),(190,13,17,'Vila Nova de Gaia'),(191,13,18,'Trofa'),(192,13,1,'Amarante'),(193,13,2,'Baião'),(194,13,3,'Felgueiras'),(195,13,4,'Gondomar'),(196,13,5,'Lousada'),(197,14,6,'Cartaxo'),(198,14,7,'Chamusca'),(199,14,8,'Constância'),(200,14,9,'Coruche'),(201,14,10,'Entroncamento'),(202,14,11,'Ferreira do Zêzere'),(203,14,12,'Golegã'),(204,14,13,'Mação'),(205,14,14,'Rio Maior'),(206,14,15,'Salvaterra de Magos'),(207,14,16,'Santarém'),(208,14,17,'Sardoal'),(209,14,18,'Tomar'),(210,14,19,'Torres Novas'),(211,14,20,'Vila Nova da Barquinha'),(212,14,21,'Ourém'),(213,14,1,'Abrantes'),(214,14,2,'Alcanena'),(215,14,3,'Almeirim'),(216,14,4,'Alpiarça'),(217,14,5,'Benavente'),(218,15,6,'Moita'),(219,15,7,'Montijo'),(220,15,8,'Palmela'),(221,15,9,'Santiago do Cacém'),(222,15,10,'Seixal'),(223,15,11,'Sesimbra'),(224,15,12,'Setúbal'),(225,15,13,'Sines'),(226,15,1,'Alcácer do Sal'),(227,15,2,'Alcochete'),(228,15,3,'Almada'),(229,15,4,'Barreiro'),(230,15,5,'Grândola'),(231,16,6,'Ponte da Barca'),(232,16,7,'Ponte de Lima'),(233,16,8,'Valença'),(234,16,9,'Viana do Castelo'),(235,16,10,'Vila Nova de Cerveira'),(236,16,1,'Arcos de Valdevez'),(237,16,2,'Caminha'),(238,16,3,'Melgaço'),(239,16,4,'Monção'),(240,16,5,'Paredes de Coura'),(241,17,5,'Mondim de Basto'),(242,17,6,'Montalegre'),(243,17,7,'Murça'),(244,17,8,'Peso da Régua'),(245,17,9,'Ribeira de Pena'),(246,17,10,'Sabrosa'),(247,17,11,'Santa Marta de Penaguião'),(248,17,12,'Valpaços'),(249,17,13,'Vila Pouca de Aguiar'),(250,17,14,'Vila Real'),(251,17,1,'Alijó'),(252,17,2,'Boticas'),(253,17,3,'Chaves'),(254,17,4,'Mesão Frio'),(255,18,5,'Lamego'),(256,18,6,'Mangualde'),(257,18,7,'Moimenta da Beira'),(258,18,8,'Mortágua'),(259,18,9,'Nelas'),(260,18,10,'Oliveira de Frades'),(261,18,11,'Penalva do Castelo'),(262,18,12,'Penedono'),(263,18,13,'Resende'),(264,18,14,'Santa Comba Dão'),(265,18,15,'São João da Pesqueira'),(266,18,16,'São Pedro do Sul'),(267,18,17,'Sátão'),(268,18,18,'Sernancelhe'),(269,18,19,'Tabuaço'),(270,18,20,'Tarouca'),(271,18,21,'Tondela'),(272,18,22,'Vila Nova de Paiva'),(273,18,23,'Viseu'),(274,18,24,'Vouzela'),(275,18,1,'Armamar'),(276,18,2,'Carregal do Sal'),(277,18,3,'Castro Daire'),(278,18,4,'Cinfães'),(279,31,5,'Ponta do Sol'),(280,31,6,'Porto Moniz'),(281,31,7,'Ribeira Brava'),(282,31,8,'Santa Cruz'),(283,31,9,'Santana'),(284,31,10,'São Vicente'),(285,31,1,'Calheta (Madeira)'),(286,31,2,'Câmara de Lobos'),(287,31,3,'Funchal'),(288,31,4,'Machico'),(289,32,1,'Porto Santo'),(290,41,1,'Vila do Porto'),(291,42,5,'Ribeira Grande'),(292,42,6,'Vila Franca do Campo'),(293,42,1,'Lagoa (São Miguel)'),(294,42,2,'Nordeste'),(295,42,3,'Ponta Delgada'),(296,42,4,'Povoação'),(297,43,1,'Angra do Heroísmo'),(298,43,2,'Praia da Vitória'),(299,44,1,'Santa Cruz da Graciosa'),(300,45,1,'Calheta (São Jorge)'),(301,45,2,'Velas'),(302,46,1,'Lajes do Pico'),(303,46,2,'Madalena'),(304,46,3,'São Roque do Pico'),(305,47,1,'Horta'),(306,48,1,'Lajes das Flores'),(307,48,2,'Santa Cruz das Flores'),(308,49,1,'Corvo');
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_user_id` int(11) NOT NULL,
  `ticket_type_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `number_stop` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_preco_titulo_de_transporte1_idx` (`ticket_type_id`),
  KEY `fk_price_currency1_idx` (`currency_id`),
  CONSTRAINT `fk_preco_titulo_de_transporte1` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_currency1` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (30,5,2,1,'99','1º Paragem',0,2,1),(31,5,2,1,'100','1º Paragem',0,2,1),(36,6,2,1,'56','1º Paragem',0,2,1),(37,6,2,1,'78','2º Paragem',1,2,1),(40,5,1,1,'6','1º Paragem',0,2,1),(41,5,1,1,'8','2º Paragem',1,2,1),(57,5,2,1,'20','1º Paragem',0,2,1),(58,5,2,1,'30','1º Paragem',1,2,1),(59,6,1,1,'1.65','Trajeto Local',0,2,1),(60,6,1,1,'2.3','1º Paragem',1,2,1),(61,6,1,1,'3.6','2º Paragem',2,2,1),(62,6,1,1,'4.9','3º Paragem',3,2,1),(63,6,1,1,'6.2','4º Paragem',4,2,1),(74,5,1,1,'1.3','1º Paragem',1,2,1),(75,5,1,1,'0.65','Trajeto Local',0,2,1),(76,5,1,1,'2.6','2º Paragem',2,2,1),(77,5,1,1,'3.9','3º Paragem',3,2,1),(78,5,1,1,'5.2','4º Paragem',4,2,1),(79,5,1,1,'0.65','Trajeto Local',0,2,1),(80,5,1,1,'1.3','1º Paragem',1,2,1),(81,5,1,1,'5.2','4º Paragem',4,2,1),(82,5,1,1,'2.6','2º Paragem',2,2,1),(83,5,1,1,'3.9','3º Paragem',3,2,1),(84,6,1,1,'1.65','Trajeto Local',0,2,1),(85,6,1,1,'2.3','1º Paragem',1,2,1),(86,6,1,1,'3.6','2º Paragem',2,2,1),(87,6,1,1,'4.9','3º Paragem',3,2,1),(88,6,1,1,'6.2','4º Paragem',4,2,1),(89,5,2,1,'15','1º Paragem',1,2,1),(90,5,2,1,'25','2º Paragem',2,2,1),(91,5,2,1,'35','3º Paragem',3,2,1),(92,5,2,1,'40','4º Paragem',4,2,1),(101,6,2,1,'20','1º Paragem',1,2,1),(102,6,2,1,'30','2º Paragem',2,2,1),(103,6,2,1,'40','3º Paragem',3,2,1),(104,6,2,1,'45','4º Paragem',4,2,1),(105,5,1,1,'0.65','Trajeto Local',0,2,1),(106,5,1,1,'1.3','1º Paragem',1,2,1),(107,5,1,1,'2.6','2º Paragem',2,2,1),(108,5,1,1,'3.9','3º Paragem',3,2,1),(109,5,1,1,'5.2','4º Paragem',4,2,1),(110,6,1,1,'2.3','1º Paragem',1,2,1),(111,6,1,1,'1.65','Trajeto Local',0,2,1),(112,6,1,1,'3.6','2º Paragem',2,2,1),(113,6,1,1,'4.9','3º Paragem',3,2,1),(114,6,1,1,'6.2','4º Paragem',4,2,1),(115,5,2,1,'15','1º Paragem',1,2,1),(116,5,2,1,'25','2º Paragem',2,2,1),(117,5,2,1,'35','3º Paragem',3,2,1),(118,5,2,1,'40','4º Paragem',4,2,1),(119,6,2,1,'20','1º Paragem',1,2,1),(120,6,2,1,'30','2º Paragem',2,2,1),(121,6,2,1,'40','3º Paragem',3,2,1),(122,6,2,1,'45','4º Paragem',4,2,1),(123,5,1,1,'0.65','Trajeto Local',0,2,1),(124,5,1,1,'1.3','1º Paragem',1,2,1),(125,5,1,1,'2.6','2º Paragem',2,2,1),(126,5,1,1,'3.9','3º Paragem',3,2,1),(127,5,1,1,'5.2','4º Paragem',4,2,1),(128,6,1,1,'1.65','Trajeto Local',0,2,1),(129,6,1,1,'2.3','1º Paragem',1,2,1),(130,6,1,1,'3.6','2º Paragem',2,2,1),(131,6,1,1,'4.9','3º Paragem',3,2,1),(132,6,1,1,'6.2','4º Paragem',4,2,1),(133,5,2,1,'15','1º Paragem',1,2,1),(134,5,2,1,'25','2º Paragem',2,2,1),(135,5,2,1,'35','3º Paragem',3,2,1),(136,5,2,1,'40','4º Paragem',4,2,1),(137,6,2,1,'30','2º Paragem',2,2,1),(138,6,2,1,'20','1º Paragem',1,2,1),(139,6,2,1,'45','4º Paragem',4,2,1),(140,6,2,1,'40','3º Paragem',3,2,1),(141,5,1,1,'1.3','1º Paragem',1,2,1),(142,5,1,1,'0.65','Trajeto Local',0,2,1),(143,5,1,1,'2.6','2º Paragem',2,2,1),(144,5,1,1,'3.9','3º Paragem',3,2,1),(145,5,1,1,'5.2','4º Paragem',4,2,1),(146,6,1,1,'4.9','3º Paragem',3,2,1),(147,6,1,1,'2.3','1º Paragem',1,2,1),(148,6,1,1,'1.65','Trajeto Local',0,2,1),(149,6,1,1,'3.6','2º Paragem',2,2,1),(150,6,1,1,'6.2','4º Paragem',4,2,1),(151,5,2,1,'25','2º Paragem',2,2,1),(152,5,2,1,'15','1º Paragem',1,2,1),(153,5,2,1,'35','3º Paragem',3,2,1),(154,5,2,1,'40','4º Paragem',4,2,1),(155,6,2,1,'20','1º Paragem',1,2,1),(156,6,2,1,'30','2º Paragem',2,2,1),(157,6,2,1,'40','3º Paragem',3,2,1),(158,6,2,1,'45','4º Paragem',4,2,1),(159,5,1,1,'1.3','1º Paragem',1,2,1),(160,5,1,1,'2.6','2º Paragem',2,2,1),(161,5,1,1,'0.65','Trajeto Local',0,2,1),(162,5,1,1,'3.9','3º Paragem',3,2,1),(163,5,1,1,'5.2','4º Paragem',4,2,1),(164,6,1,1,'1.65','Trajeto Local',0,2,1),(165,6,1,1,'2.3','1º Paragem',1,2,1),(166,6,1,1,'3.6','2º Paragem',2,2,1),(167,6,1,1,'6.2','4º Paragem',4,2,1),(168,6,1,1,'4.9','3º Paragem',3,2,1),(169,5,2,1,'25','2º Paragem',2,2,1),(170,5,2,1,'15','1º Paragem',1,2,1),(171,5,2,1,'35','3º Paragem',3,2,1),(172,5,2,1,'40','4º Paragem',4,2,1),(173,6,2,1,'30','2º Paragem',2,2,1),(174,6,2,1,'20','1º Paragem',1,2,1),(175,6,2,1,'40','3º Paragem',3,2,1),(176,6,2,1,'45','4º Paragem',4,2,1),(177,5,1,1,'3.45','Teste 2',1,2,1),(178,5,1,1,'1.23','Teste',0,2,1);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_route`
--

DROP TABLE IF EXISTS `price_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_route` (
  `price_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  KEY `fk_table1_price1_idx` (`price_id`),
  KEY `fk_price_route_route1_idx` (`route_id`),
  CONSTRAINT `fk_price_route_route1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_price1` FOREIGN KEY (`price_id`) REFERENCES `price` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_route`
--

LOCK TABLES `price_route` WRITE;
/*!40000 ALTER TABLE `price_route` DISABLE KEYS */;
INSERT INTO `price_route` VALUES (59,2),(60,2),(61,2),(62,2),(63,2),(75,2),(74,2),(76,2),(78,2),(77,2),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(91,1),(90,1),(92,1),(102,1),(101,1),(103,1),(104,1),(105,8),(106,8),(107,8),(109,8),(108,8),(110,8),(112,8),(114,8),(111,8),(113,8),(115,8),(116,8),(117,8),(118,8),(119,8),(120,8),(122,8),(121,8),(124,6),(125,6),(126,6),(123,6),(127,6),(128,6),(129,6),(130,6),(131,6),(132,6),(134,6),(133,6),(136,6),(135,6),(137,6),(138,6),(139,6),(140,6),(142,11),(143,11),(141,11),(144,11),(145,11),(147,11),(146,11),(149,11),(148,11),(150,11),(151,11),(153,11),(152,11),(154,11),(155,11),(157,11),(156,11),(158,11),(159,43),(161,43),(160,43),(162,43),(163,43),(164,43),(166,43),(168,43),(165,43),(167,43),(170,43),(171,43),(172,43),(169,43),(174,43),(176,43),(175,43),(173,43);
/*!40000 ALTER TABLE `price_route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `name` varchar(70) DEFAULT NULL,
  `external` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_route_contact1_idx` (`contact_id`),
  CONSTRAINT `fk_route_contact1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (1,1,'Bus 1 Manhã',0),(2,1,'Bus 3 Manhã',0),(4,2,'Afife -> Central de Viana',1),(5,2,'Vila Fria -> Central de Viana',1),(6,1,'Bus 2 Tarde',0),(7,2,'Monção -> Extremo',1),(8,1,'Bus 2 Manhã',0),(9,2,'Viana -> Ponte de Lima',1),(10,2,'Melgaço -> Viana',1),(11,1,'Bus 1 Tarde',0),(12,2,'Viana - > Afife',1),(39,2,'V.N. Anha -> Central(Viana)',1),(40,2,'Perre - >Viana (Central)',1),(41,2,'Madorra -> Viana (Central)',1),(42,2,'Extremo -> Moreira',1),(43,1,'Bus 3 Tarde',0),(44,2,'Castro -> Melgaço',1),(45,2,'Sistelo -> Monção',1),(47,2,'P. de Coura -> S. Pedro da Torre',1),(48,2,'Monção -> Melgaço',1),(49,2,'P. Coura -> Viana',1),(50,2,'ESDL -> C. Laboiro',1),(51,2,'Monção -> Sistelo',1),(52,2,'S. Pedro da Torre -> P. Coura',1),(53,2,'Viana -> V.N. Anha',1),(54,2,'Viana -> Perre(Igreja)',1),(55,2,'Viana -> Madorra',1),(56,2,'Viana -> Paredes',1),(57,1,'Teste',1);
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_order`
--

DROP TABLE IF EXISTS `route_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `order_table` int(11) NOT NULL,
  `instruction` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ordem_da_rota_pontos1_idx` (`local_id`),
  KEY `fk_ordem_da_rota_rota1_idx` (`route_id`),
  CONSTRAINT `fk_ordem_da_rota_pontos1` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ordem_da_rota_rota1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_order`
--

LOCK TABLES `route_order` WRITE;
/*!40000 ALTER TABLE `route_order` DISABLE KEYS */;
INSERT INTO `route_order` VALUES (1,1,1,1,''),(2,2,1,2,''),(3,10,1,3,''),(4,5,1,4,''),(5,6,1,5,''),(6,5,1,6,''),(7,10,1,7,''),(8,2,1,8,''),(9,1,1,9,''),(10,11,2,1,''),(11,4,2,2,''),(12,7,2,3,''),(13,8,2,4,''),(14,9,2,5,''),(15,10,2,6,''),(16,2,2,7,''),(17,1,2,8,'eeeeee'),(21,12,4,1,''),(22,13,4,2,''),(23,14,4,3,''),(24,2,4,4,''),(25,15,5,1,''),(26,16,5,2,''),(27,17,5,3,''),(28,2,5,4,''),(29,11,6,1,''),(30,4,6,2,''),(31,7,6,3,''),(32,8,6,4,''),(33,5,6,5,''),(34,6,6,6,''),(35,3,6,7,''),(36,18,6,8,''),(37,4,7,1,''),(38,20,7,2,''),(39,19,7,3,''),(40,18,8,1,''),(41,3,8,2,''),(42,6,8,3,''),(43,5,8,4,''),(44,8,8,5,''),(45,7,8,6,''),(46,4,8,7,''),(47,11,8,8,''),(48,2,9,1,''),(49,5,9,2,''),(50,11,10,1,''),(51,4,10,2,''),(52,7,10,3,''),(53,8,10,4,''),(54,9,10,5,''),(55,21,10,6,''),(56,22,10,7,''),(57,2,10,8,''),(58,1,11,1,''),(59,2,11,2,''),(60,10,11,3,''),(61,6,11,4,''),(62,5,11,5,''),(63,10,11,6,''),(64,2,11,7,''),(65,1,11,8,''),(66,2,12,1,''),(67,14,12,2,''),(68,13,12,3,''),(69,12,12,4,''),(78,74,39,1,''),(79,17,39,2,''),(80,2,39,3,''),(81,75,40,1,''),(82,76,40,2,''),(83,78,40,3,''),(84,77,40,4,''),(85,2,40,5,''),(86,79,41,1,'Autocarro em direcção a Cova'),(87,80,41,2,''),(88,2,41,3,''),(89,19,42,1,'Autocarro em direcção a Monção'),(90,20,42,2,''),(91,4,42,3,''),(102,81,44,1,''),(103,82,44,2,''),(104,11,44,3,''),(105,83,45,1,'Autocarro em direcção a Monção'),(106,4,45,2,''),(107,84,47,1,'Autocarro em Direcção a Café Stop'),(108,85,47,2,''),(109,86,47,3,''),(110,8,47,4,''),(111,4,48,1,''),(112,11,48,2,''),(113,84,49,1,''),(114,8,49,2,''),(115,9,49,3,''),(116,21,49,4,''),(117,2,49,5,''),(118,11,50,1,''),(119,82,50,2,''),(120,81,50,3,''),(121,4,51,1,''),(122,83,51,2,''),(123,8,52,1,''),(124,86,52,2,''),(125,85,52,3,''),(126,84,52,4,''),(127,2,53,1,''),(128,17,53,2,''),(129,16,53,3,''),(130,15,53,4,''),(131,74,53,5,''),(132,2,54,1,''),(133,77,54,2,''),(134,78,54,3,''),(135,76,54,4,''),(136,75,54,5,''),(137,2,55,1,''),(138,80,55,2,''),(139,79,55,3,''),(140,2,56,1,''),(141,8,56,2,''),(142,84,56,3,''),(143,5,57,1,''),(144,8,57,2,'');
/*!40000 ALTER TABLE `route_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_ticket`
--

DROP TABLE IF EXISTS `route_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_ticket` (
  `ticket_bought_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  KEY `fk_route_ticket_tickets_boughts1_idx` (`ticket_bought_id`),
  KEY `fk_route_ticket_route1_idx` (`route_id`),
  CONSTRAINT `fk_route_ticket_route1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_ticket_tickets_boughts1` FOREIGN KEY (`ticket_bought_id`) REFERENCES `ticket_bought` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_ticket`
--

LOCK TABLES `route_ticket` WRITE;
/*!40000 ALTER TABLE `route_ticket` DISABLE KEYS */;
INSERT INTO `route_ticket` VALUES (33,1),(34,8),(35,1),(36,11),(37,1),(38,1);
/*!40000 ALTER TABLE `route_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_zone`
--

DROP TABLE IF EXISTS `route_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_zone` (
  `zone_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  KEY `fk_route_zone_zone1_idx` (`zone_id`),
  KEY `fk_route_zone_route1_idx` (`route_id`),
  CONSTRAINT `fk_route_zone_route1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_zone_zone1` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_zone`
--

LOCK TABLES `route_zone` WRITE;
/*!40000 ALTER TABLE `route_zone` DISABLE KEYS */;
INSERT INTO `route_zone` VALUES (1,1),(2,1),(6,8),(2,8),(3,8),(4,8),(5,8),(4,2),(5,2),(3,2),(7,2),(1,1),(1,11),(2,11),(4,6),(5,6),(3,6),(2,6),(6,6),(1,2),(12,43),(13,43),(14,43),(15,43),(16,43),(17,43),(18,43);
/*!40000 ALTER TABLE `route_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period_id` int(11) NOT NULL,
  `date_begin` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tabelas_horarias_tipo_de_horario1_idx` (`period_id`),
  CONSTRAINT `fk_tabelas_horarias_tipo_de_horario1` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (1,2,'2018-01-05','2019-12-05');
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season_register`
--

DROP TABLE IF EXISTS `season_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season_id` int(11) NOT NULL,
  `timetable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_registo_horario_tabela_horaria1_idx` (`timetable_id`),
  KEY `fk_registo_horario_periodos_horarias1_idx` (`season_id`),
  CONSTRAINT `season_register_ibfk_1` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`),
  CONSTRAINT `season_register_ibfk_2` FOREIGN KEY (`timetable_id`) REFERENCES `timetable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season_register`
--

LOCK TABLES `season_register` WRITE;
/*!40000 ALTER TABLE `season_register` DISABLE KEYS */;
INSERT INTO `season_register` VALUES (2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,1,14),(15,1,15),(16,1,16),(17,1,17),(18,1,18),(22,1,22),(23,1,23),(24,1,24),(25,1,25),(26,1,26),(27,1,27),(28,1,28),(29,1,29),(30,1,30),(31,1,31),(32,1,32),(33,1,33),(34,1,34),(35,1,35),(36,1,36),(37,1,37),(38,1,38),(39,1,39),(40,1,40),(41,1,41),(42,1,42),(43,1,43),(44,1,44),(45,1,45),(46,1,46),(47,1,47),(48,1,48),(49,1,49),(50,1,50),(51,1,51),(52,1,52),(55,1,55),(56,1,56),(57,1,57),(58,1,58),(59,1,59),(60,1,60),(61,1,61),(62,1,62),(63,1,63),(64,1,64),(65,1,65),(66,1,66),(67,1,67),(68,1,68),(69,1,69),(70,1,70),(71,1,71),(72,1,72),(73,1,73),(74,1,74),(75,1,75),(76,1,76),(77,1,77),(78,1,78),(79,1,79),(80,1,80),(81,1,81),(82,1,82),(101,1,101),(102,1,102),(103,1,103),(104,1,104),(105,1,106),(106,1,108),(107,1,107),(108,1,105),(109,1,109),(110,1,111),(111,1,110),(112,1,113),(113,1,114),(114,1,112),(115,1,115),(116,1,116),(117,1,117),(118,1,119),(119,1,118),(123,1,124),(124,1,125),(125,1,123),(126,1,126),(127,1,128),(128,1,127),(129,1,129),(130,1,135),(131,1,131),(132,1,132),(133,1,130),(134,1,133),(135,1,134),(136,1,136),(137,1,138),(138,1,139),(139,1,141),(140,1,140),(141,1,142),(142,1,143),(143,1,137),(144,1,145),(145,1,146),(146,1,144),(147,1,147),(148,1,148),(149,1,151),(150,1,152),(151,1,149),(152,1,150),(153,1,153),(154,1,155),(155,1,154),(156,1,156),(157,1,158),(158,1,159),(159,1,160),(160,1,157),(161,1,161),(162,1,162),(163,1,165),(164,1,164),(165,1,163),(166,1,167),(167,1,166),(168,1,168),(169,1,169),(170,1,170),(171,1,171),(172,1,173),(173,1,172),(174,1,176),(175,1,174),(176,1,175),(177,1,177),(178,1,178),(179,1,179);
/*!40000 ALTER TABLE `season_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_bought`
--

DROP TABLE IF EXISTS `ticket_bought`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_bought` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `ticket_type` varchar(45) DEFAULT NULL,
  `validate_date` date DEFAULT NULL,
  `route` varchar(45) DEFAULT NULL,
  `departure_local` varchar(45) DEFAULT NULL,
  `arrivel_local` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `user_email` varchar(45) DEFAULT NULL,
  `type_user` varchar(45) DEFAULT NULL,
  `used` tinyint(4) DEFAULT NULL,
  `bought_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_bought`
--

LOCK TABLES `ticket_bought` WRITE;
/*!40000 ALTER TABLE `ticket_bought` DISABLE KEYS */;
INSERT INTO `ticket_bought` VALUES (1,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',1,'2038-01-19 03:14:07'),(2,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2038-01-19 03:14:07'),(3,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(4,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(5,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(6,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(7,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(8,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(9,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(10,3,'€','Bilhete','2019-01-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2020-01-19 03:14:07'),(11,3,'€','Bilhete',NULL,'Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,NULL),(12,3,'€','Bilhete',NULL,'Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,NULL),(13,3,'€','Bilhete',NULL,'Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,NULL),(14,3,'€','Bilhete',NULL,'Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,NULL),(15,3,'€','Bilhete','2019-02-12','Rota','Local Teste','Locat Testeeee','Andre','andre@ipca.pt','Aluno',0,'2038-01-19 03:14:07'),(16,5,'€','Bilhete','2019-02-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',1,'2038-01-19 03:14:07'),(17,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 09:48:30'),(18,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:13:49'),(19,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:15:09'),(20,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:16:01'),(21,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:16:47'),(22,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:18:21'),(23,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 10:19:38'),(24,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:09:02'),(25,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:12:11'),(26,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:13:54'),(27,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:14:37'),(28,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:15:37'),(29,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:15:57'),(30,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:38:38'),(31,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:38:49'),(32,5,'€','Bilhete','2019-01-12','Bus Manhã','ESTG','ESE','André','andre@email.pt','Aluno',0,'2018-11-28 15:38:55'),(33,15,'€','Passe Mensal','2019-01-12','Bus 1 Manhã','ESTG','ESE','admin','admin@ipvc.pt','Aluno',0,'2018-12-04 16:30:13'),(34,1.3,'€','Bilhete','2018-12-13','Bus 2 Manhã','ESTG','undefined','admin','admin@ipvc.pt','Alunos',1,'2018-12-13 13:09:13'),(35,1.3,'€','Bilhete','2018-12-13','Bus 1 Manhã','ESTG','undefined','admin','admin@ipvc.pt','Alunos',1,'2018-12-13 13:09:13'),(36,0.65,'€','Bilhete','2018-12-28','Bus 1 Tarde','ESTG','undefined','aluno','aluno@ipvc.pt','Alunos',0,'2018-12-28 19:03:34'),(37,0.65,'€','Bilhete','2018-12-28','Bus 1 Manhã','ESTG','undefined','aluno','aluno@ipvc.pt','Alunos',0,'2018-12-28 19:03:34'),(38,0.65,'€','Bilhete','2018-12-28','Bus 1 Manhã','ESTG','undefined','aluno','aluno@ipvc.pt','Alunos',0,'2018-12-28 19:03:34');
/*!40000 ALTER TABLE `ticket_bought` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_config`
--

DROP TABLE IF EXISTS `ticket_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) NOT NULL,
  `max_to_buy` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_config_route1_idx` (`route_id`),
  CONSTRAINT `fk_ticket_config_route1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_config`
--

LOCK TABLES `ticket_config` WRITE;
/*!40000 ALTER TABLE `ticket_config` DISABLE KEYS */;
INSERT INTO `ticket_config` VALUES (2,1,50,1),(3,2,50,1),(4,4,50,0),(5,5,50,0),(6,6,50,1),(7,7,50,0),(8,8,50,1),(9,9,50,0),(10,10,50,0),(11,11,50,1),(12,12,50,0),(15,39,50,0),(16,40,50,0),(17,41,50,0),(18,42,50,0),(19,43,50,0),(20,44,50,0),(21,45,50,0),(23,47,50,0),(24,48,50,0),(25,49,50,0),(26,50,50,0),(27,51,50,0),(28,52,50,0),(29,53,50,0),(30,54,50,0),(31,55,50,0),(32,56,50,0),(33,57,50,0);
/*!40000 ALTER TABLE `ticket_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_type`
--

DROP TABLE IF EXISTS `ticket_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `purchase_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_type`
--

LOCK TABLES `ticket_type` WRITE;
/*!40000 ALTER TABLE `ticket_type` DISABLE KEYS */;
INSERT INTO `ticket_type` VALUES (1,'Bilhete','TICKET'),(2,'Passe Mensal','PASS');
/*!40000 ALTER TABLE `ticket_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetable`
--

DROP TABLE IF EXISTS `timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_id` int(11) NOT NULL,
  `type_day_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `route_order_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `hour` time DEFAULT NULL,
  `line_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_linha_do_horario_pontos1_idx` (`local_id`),
  KEY `fk_linha_do_horario_tipo_de_dia1_idx` (`type_day_id`),
  KEY `fk_linha_do_horario_rota1_idx` (`route_id`),
  KEY `fk_linha_do_horario_tabelas_horarias1_idx` (`season_id`),
  CONSTRAINT `fk_linha_do_horario_pontos1` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_linha_do_horario_rota1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_linha_do_horario_tabelas_horarias1` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_linha_do_horario_tipo_de_dia1` FOREIGN KEY (`type_day_id`) REFERENCES `type_day` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetable`
--

LOCK TABLES `timetable` WRITE;
/*!40000 ALTER TABLE `timetable` DISABLE KEYS */;
INSERT INTO `timetable` VALUES (2,1,1,1,1,1,'07:30:00',1),(3,2,1,1,2,1,'07:35:00',1),(4,10,1,1,3,1,'07:40:00',1),(5,5,1,1,4,1,'08:05:00',1),(6,6,1,1,5,1,'08:15:00',1),(7,5,1,1,6,1,'08:25:00',1),(8,10,1,1,7,1,'08:50:00',1),(9,2,1,1,8,1,'08:55:00',1),(10,1,1,1,9,1,'09:00:00',1),(11,11,1,2,10,1,'07:30:00',1),(12,4,1,2,11,1,'07:50:00',1),(13,7,1,2,12,1,'08:10:00',1),(14,8,1,2,13,1,'08:15:00',1),(15,9,1,2,14,1,'08:30:00',1),(16,10,1,2,15,1,'08:50:00',1),(17,2,1,2,16,1,'08:55:00',1),(18,1,1,2,17,1,'09:00:00',1),(22,12,1,4,21,1,'06:50:00',1),(23,13,1,4,22,1,'07:00:00',1),(24,14,1,4,23,1,'07:10:00',1),(25,2,1,4,24,1,'07:20:00',1),(26,15,1,5,25,1,'06:55:00',1),(27,16,1,5,26,1,'07:00:00',1),(28,17,1,5,27,1,'07:05:00',1),(29,2,1,5,28,1,'07:20:00',1),(30,11,1,6,29,1,'17:30:00',1),(31,4,1,6,30,1,'17:50:00',1),(32,7,1,6,31,1,'18:10:00',1),(33,8,1,6,32,1,'18:15:00',1),(34,5,1,6,33,1,'18:40:00',1),(35,6,1,6,34,1,'18:50:00',1),(36,3,1,6,35,1,'19:00:00',1),(37,18,1,6,36,1,'19:10:00',1),(38,4,1,7,37,1,'18:00:00',1),(39,20,1,7,38,1,'18:10:00',1),(40,19,1,7,39,1,'18:40:00',1),(41,18,1,8,40,1,'07:30:00',1),(42,3,1,8,41,1,'07:40:00',1),(43,6,1,8,42,1,'07:55:00',1),(44,5,1,8,43,1,'08:05:00',1),(45,8,1,8,44,1,'08:30:00',1),(46,7,1,8,45,1,'08:35:00',1),(47,4,1,8,46,1,'08:55:00',1),(48,11,1,8,47,1,'09:15:00',1),(49,2,1,9,48,1,'08:15:00',1),(50,5,1,9,49,1,'09:10:00',1),(51,2,1,9,48,1,'10:20:00',2),(52,5,1,9,49,1,'11:15:00',2),(55,11,1,10,50,1,'07:30:00',1),(56,4,1,10,51,1,'08:10:00',1),(57,7,1,10,52,1,'08:30:00',1),(58,8,1,10,53,1,'08:35:00',1),(59,9,1,10,54,1,'08:50:00',1),(60,21,1,10,55,1,'09:05:00',1),(61,22,1,10,56,1,'09:15:00',1),(62,2,1,10,57,1,'09:35:00',1),(63,1,1,11,58,1,'18:00:00',1),(64,2,1,11,59,1,'18:05:00',1),(65,10,1,11,60,1,'18:10:00',1),(66,6,1,11,61,1,'18:25:00',1),(67,5,1,11,62,1,'18:40:00',1),(68,10,1,11,63,1,'19:05:00',1),(69,2,1,11,64,1,'19:10:00',1),(70,1,1,11,65,1,'19:15:00',1),(71,2,1,12,66,1,'19:20:00',1),(72,14,1,12,67,1,'19:30:00',1),(73,13,1,12,68,1,'19:40:00',1),(74,12,1,12,69,1,'19:50:00',1),(75,11,1,10,50,1,'11:30:00',2),(76,4,1,10,51,1,'12:10:00',2),(77,7,1,10,52,1,'12:30:00',2),(78,8,1,10,53,1,'12:35:00',2),(79,9,1,10,54,1,'12:50:00',2),(80,21,1,10,55,1,'13:05:00',2),(81,22,1,10,56,1,'13:15:00',2),(82,2,1,10,57,1,'13:35:00',2),(101,74,1,39,78,1,'07:10:59',1),(102,17,1,39,79,1,'07:20:59',1),(103,2,1,39,80,1,'07:30:59',1),(104,76,1,40,82,1,'06:50:13',1),(105,78,1,40,83,1,'06:55:13',1),(106,77,1,40,84,1,'07:05:13',1),(107,75,1,40,81,1,'06:45:13',1),(108,2,1,40,85,1,'07:15:13',1),(109,79,1,41,86,1,'07:00:20',1),(110,80,1,41,87,1,'07:10:20',1),(111,2,1,41,88,1,'07:20:20',1),(112,19,1,42,89,1,'07:15:58',1),(113,20,1,42,90,1,'07:40:58',1),(114,4,1,42,91,1,'08:55:58',1),(115,82,1,44,103,1,'08:10:38',1),(116,81,1,44,102,1,'07:45:38',1),(117,11,1,44,104,1,'08:30:38',1),(118,83,1,45,105,1,'07:40:50',1),(119,4,1,45,106,1,'07:50:50',1),(123,84,1,47,107,1,'07:35:41',1),(124,85,1,47,108,1,'08:00:41',1),(125,86,1,47,109,1,'08:10:41',1),(126,8,1,47,110,1,'08:15:41',1),(127,11,1,48,112,1,'09:10:44',1),(128,4,1,48,111,1,'08:25:44',1),(129,84,1,49,113,1,'07:00:59',1),(130,8,1,49,114,1,'07:30:59',1),(131,21,1,49,116,1,'07:55:59',1),(132,8,1,49,114,1,'08:15:11',2),(133,9,1,49,115,1,'08:25:11',2),(134,21,1,49,116,1,'08:40:11',2),(135,84,1,49,113,1,'07:35:11',2),(136,2,1,49,117,1,'08:25:59',1),(137,9,1,49,115,1,'07:40:59',1),(138,84,1,49,113,1,'12:30:15',3),(139,2,1,49,117,1,'09:10:11',2),(140,8,1,49,114,1,'13:10:15',3),(141,9,1,49,115,1,'13:20:15',3),(142,21,1,49,116,1,'13:35:15',3),(143,2,1,49,117,1,'14:05:15',3),(144,82,1,50,119,1,'18:20:49',1),(145,11,1,50,118,1,'18:00:49',1),(146,81,1,50,120,1,'18:45:49',1),(147,83,1,51,122,1,'19:10:33',1),(148,4,1,51,121,1,'17:50:33',1),(149,86,1,52,124,1,'18:35:51',1),(150,85,1,52,125,1,'18:45:51',1),(151,8,1,52,123,1,'18:30:51',1),(152,84,1,52,126,1,'19:00:51',1),(153,16,1,53,129,1,'19:00:50',1),(154,2,1,53,127,1,'19:15:53',2),(155,15,1,53,130,1,'19:10:50',1),(156,17,1,53,128,1,'19:35:53',2),(157,74,1,53,131,1,'19:15:50',1),(158,2,1,53,127,1,'18:30:50',1),(159,17,1,53,128,1,'18:50:50',1),(160,74,1,53,131,1,'20:00:53',2),(161,16,1,53,129,1,'19:40:53',2),(162,15,1,53,130,1,'19:45:53',2),(163,77,1,54,133,1,'19:30:12',1),(164,2,1,54,132,1,'19:10:12',1),(165,78,1,54,134,1,'19:35:12',1),(166,76,1,54,135,1,'19:40:12',1),(167,75,1,54,136,1,'19:45:12',1),(168,80,1,55,138,1,'19:20:24',1),(169,2,1,55,137,1,'19:10:24',1),(170,79,1,55,139,1,'19:40:24',1),(171,84,1,56,142,1,'13:40:55',1),(172,8,1,56,141,1,'13:00:55',1),(173,2,1,56,140,1,'12:00:55',1),(174,2,1,56,140,1,'15:30:44',2),(175,84,1,56,142,1,'17:05:44',2),(176,8,1,56,141,1,'18:25:18',3),(177,2,1,56,140,1,'17:30:18',3),(178,8,1,56,141,1,'16:25:44',2),(179,84,1,56,142,1,'18:55:18',3);
/*!40000 ALTER TABLE `timetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_day`
--

DROP TABLE IF EXISTS `type_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_day`
--

LOCK TABLES `type_day` WRITE;
/*!40000 ALTER TABLE `type_day` DISABLE KEYS */;
INSERT INTO `type_day` VALUES (1,'Dias Uteis'),(2,'Sábado'),(3,'Domingo');
/*!40000 ALTER TABLE `type_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,'Zona A'),(2,'Zona B'),(3,'Zona C'),(4,'Zona D'),(5,'Zona E'),(6,'Zona F'),(7,'Zona G'),(8,'Zona J'),(9,'Zona H'),(11,'Zona Teste'),(12,'Zona A Bus3T'),(13,'Zona B Bus3T'),(14,'Zona C Bus3T'),(15,'Zona C Bus3T'),(16,'Zona D Bus3T'),(17,'Zona E Bus3T'),(18,'Zona F Bus3T'),(19,'Zona Teste'),(20,'Zona Teste 2');
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'fisas_bus'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_day_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_day_week`(IN `type_day_id_in` INT, IN `day_id_in` INT)
BEGIN

insert into day_group (type_day_id, day_id) values (type_day_id_in , day_id_in);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_days_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_days_week`(IN `days_week_ids_in` VARCHAR(30))
BEGIN
SET @SQLText = CONCAT('Select * from day_week where id in (',days_week_ids_in,')');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_day_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_day_group`(IN `type_day_id_in` INT, IN `day_id_in` INT)
BEGIN
 select * from day_group where type_day_id = type_day_id_in and day_id = day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_locals` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_locals`(IN `locals_ids_in` VARCHAR(30))
BEGIN
 SET @SQLText = CONCAT('Select * from local where id in (',locals_ids_in,')');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_locals_in_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_locals_in_route`(IN `route_id_in` INT)
BEGIN
select odr.local_id from route_order as odr
join local as lc on lc.id = odr.local_id
join route as rt on rt.id = odr.route_id
where rt.id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_prices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_prices`(IN `prices_ids_in` VARCHAR(30))
BEGIN
SET @SQLText = CONCAT('Select * from price where id in (',prices_ids_in,')');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_routes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_routes`(IN `routes_ids_in` VARCHAR(30))
BEGIN
 SET @SQLText = CONCAT('Select * from route where id in (',routes_ids_in,')');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_season` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_season`(IN `date_in` DATE)
    NO SQL
SELECT id FROM season WHERE date_in BETWEEN date_begin AND date_end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_timetable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_timetable`(IN `route_id_in` INT, IN `season_id_in` INT, IN `type_day_id_in` INT)
BEGIN
 select * from timetable 
 where route_id = route_id_in 
 and season_id = season_id_in 
 and type_day_id = type_day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_timetable_line` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_timetable_line`(IN `route_id_in` INT, IN `season_id_in` INT, IN `type_day_id_in` INT,IN `line_number_in` INT)
BEGIN
 select * from timetable 
 where route_id = route_id_in 
 and season_id = season_id_in 
 and type_day_id = type_day_id_in
 and line_number = line_number_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `confirm_ticket` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `confirm_ticket`(IN `ticket_bought_id_in` TIME, IN `used_id_in` INT)
    NO SQL
BEGIN    
UPDATE ticket_bought
SET used = used_id_in
WHERE id = ticket_bought_id_in;
select * from ticket_bought where id = ticket_bought_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `contacts`(IN `route_id_in` INT)
    NO SQL
SELECT ct.name, ct.email, ct.phone, ct.website FROM route AS rt 
JOIN contact AS ct ON rt.contact_id = ct.id
WHERE rt.id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `count_tickets_by_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `count_tickets_by_day`(IN `route_id_in` INT, IN `validate_date_in` DATE)
BEGIN
select Count(tb.id) as total from ticket_bought as tb 
join route_ticket as rt on tb.id = rt.ticket_bought_id
where rt.route_id = route_id_in and validate_date = validate_date_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `currency_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `currency_id`(IN `currency_in` VARCHAR(30))
    NO SQL
SELECT id FROM currency WHERE currency = currency_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_contact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_contact`(IN `contact_id_in` INT)
    NO SQL
DELETE FROM contact
WHERE id = contact_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_currency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_currency`(IN `currency_id_in` INT)
    NO SQL
DELETE FROM currency where id = currency_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_day_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_day_week`(IN `type_day_id_in` INT, IN `day_id_in` INT)
BEGIN
 delete from day_group where type_day_id = type_day_id_in and day_id = day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_link` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_link`(IN `local_route1_id_in` INT, IN `local_route2_id_in` INT, IN `route1_id_in` INT, IN `route2_id_in` INT)
    NO SQL
BEGIN
Delete from links where local_route1_id = local_route1_id_in and local_route2_id = local_route2_id_in and route1_id = route1_id_in and route2_id = route2_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_local`(IN `id_local_in` INT)
    NO SQL
DELETE FROM local
WHERE id = id_local_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_period`(IN `period_id_in` INT)
    NO SQL
DELETE FROM period
WHERE id = period_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_price` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_price`(IN `price_id_in` INT)
    NO SQL
DELETE FROM price WHERE id = price_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_price_column` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_price_column`(IN `route_id_in` INT, IN `price_id_in` INT)
    NO SQL
BEGIN

Delete From price_route
where route_id = route_id_in and price_id = price_id_in;

DELETE FROM price
WHERE id = price_id_in;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_price_table` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_price_table`(IN `prices_ids_in` varchar(30))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('DELETE from price where id in (', prices_ids_in,')');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_route`(IN `route_id_in` INT)
    NO SQL
BEGIN

Delete From route_order
where route_order.route_id=route_id_in;

DELETE FROM distance
WHERE distance.route_id = route_id_in;

DELETE FROM links
WHERE links.route1_id = route_id_in or links.route2_id = route_id_in;

DELETE FROM route
WHERE route.id = route_id_in;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_season` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_season`(IN `season_id_in` INT)
    NO SQL
DELETE FROM season
WHERE id = season_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_ticket_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_ticket_type`(IN `ticket_type_id_in` INT)
    NO SQL
DELETE FROM ticket_type WHERE id = ticket_type_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_timetable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_timetable`(IN `route_id_in` INT,IN `season_id_in` INT, IN `type_day_id_in` INT)
BEGIN
delete from timetable 
where route_id = route_id_in 
and season_id = season_id_in 
and type_day_id = type_day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_timetable_line` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_timetable_line`(IN `route_id_in` INT,IN `season_id_in` INT, IN `type_day_id_in` INT, IN `line_number_in` INT)
BEGIN
delete from timetable 
where route_id = route_id_in 
and season_id = season_id_in 
and type_day_id = type_day_id_in
and line_number = line_number_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_type_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_type_day`(IN `type_day_id_in` INT)
    NO SQL
DELETE FROM type_day
WHERE id= type_day_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_type_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_type_user`(IN `type_user_id_in` INT)
    NO SQL
DELETE FROM type_user
WHERE id = type_user_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_zone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_zone`(IN `zone_id_in` INT)
    NO SQL
BEGIN

DELETE FROM route_zone WHERE zone_id = zone_id_in;
DELETE FROM zone WHERE id = zone_id_in;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_zone_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_zone_route`(IN `zone_id_in` INT, IN `route_id_in` INT)
    NO SQL
DELETE FROM route_zone WHERE route_id = route_id_in and zone_id = zone_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `detect_route_by_one_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `detect_route_by_one_local`(IN `local_in` INT, IN `type_day_in` VARCHAR(35))
    NO SQL
select DISTINCT(ord.route_id) from route_order As ord
join timetable As td On td.route_order_id = ord.id
where ord.local_id = local_in and td.type_day_id in (type_day_in) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `detect_route_order` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `detect_route_order`(IN `ponto_in` INT, IN `tipo_dia_in` INT)
    NO SQL
select DISTINCT(ord.ordem),ord.rota_id from ordem_da_rota As ord
join tabela_horaria As th On th.ordem_da_rota_id = ord.id_ordem_da_rota
where pontos_id=ponto_in and th.tipo_de_dia_id = tipo_dia_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `distances` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `distances`(IN `route_id_in` INT)
    NO SQL
SELECT local1_id, local2_id, distance
FROM distance
WHERE route_id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `enable_disable_ticket` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `enable_disable_ticket`(IN `route_id_in` INT, IN `active_in` tinyint)
    NO SQL
BEGIN    
UPDATE ticket_config SET active = active_in
WHERE route_id = route_id_in;
SELECT * FROM ticket_config WHERE route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_configuration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_configuration`()
BEGIN
SET @conf = (select count(*) from configuration);
IF (@conf = 0) THEN
INSERT INTO configuration (id,service_id) VALUES (1,0);
select * from configuration;
ELSE
select * from configuration;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_contacts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_contacts`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN    
SET @SQLText = CONCAT('SELECT * FROM contact ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_currencys` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_currencys`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM currency ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_days_weeks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_days_weeks`()
BEGIN
select * from day_week order by id asc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_distances` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_distances`(IN `route_ids_in` VARCHAR(100))
    NO SQL
BEGIN
 SET @sql = concat('SELECT * FROM distance where route_id in (',route_ids_in,')');
 PREPARE stmt FROM @sql;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_links` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_links`(IN `limit_offset_in` VARCHAR(50) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('select lik.*, rt1.name as route1, lc1.region as region1, lc1.local as local1, rt2.name as route2, lc2.region as region2, lc2.local as local2 from links as lik 
join local as lc1 on lik.local_route1_id = lc1.id
join local as lc2 on lik.local_route2_id = lc2.id
join route as rt1 on lik.route1_id = rt1.id
join route as rt2 on lik.route2_id = rt2.id ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_locals` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_locals`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM local ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_locals_institute` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_locals_institute`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('select id,local from local WHERE institute=1 ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_contact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_contact`(IN `contact_id_in` INT)
BEGIN
  select * From contact where id = contact_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_currency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_currency`(IN `currency_id_in` INT)
BEGIN
  select * From currency where id = currency_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_day_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_day_week`(IN `day_id_in` INT)
BEGIN
select * from day_week where id = day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_local`(IN `local_id_in` INT)
    NO SQL
SELECT * FROM local where id = local_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_period`(IN `period_id_in` INT)
BEGIN
  select * From period where id = period_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_price` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_price`(IN `price_id_in` INT)
BEGIN
select * From price where id = price_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_route`(IN `route_id_in` int)
    NO SQL
select rt.*,odr.local_id,lc.local,lc.region,odr.instruction, odr.order_table from route_order as odr
join local as lc on lc.id = odr.local_id
join (SELECT * FROM route where id = route_id_in ) as rt on rt.id = odr.route_id
order by odr.order_table ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_season` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_season`(IN `season_id_in` INT)
    NO SQL
SELECT * FROM season where id = season_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_ticket_bought` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_ticket_bought`(IN `ticket_id_in` INT)
    NO SQL
SELECT * FROM ticket_bought where id = ticket_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_ticket_config` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_ticket_config`(IN `route_id_in` INT)
BEGIN
  select * From ticket_config where route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_ticket_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_ticket_type`(IN `ticket_type_id_in` INT)
    NO SQL
SELECT * FROM ticket_type where id = ticket_type_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_type_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_type_day`(IN `type_day_id_in` INT)
BEGIN
SELECT dg.type_day_id, td.name,dg.day_id, dw.day FROM day_group as dg 
JOIN type_day as td ON dg.type_day_id = td.id
JOIN day_week as dw ON dw.id = dg.day_id
where td.id = type_day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_type_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_type_user`(IN `type_user_id_in` INT)
    NO SQL
SELECT * FROM type_user where id = type_user_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_one_zone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_one_zone`(IN `zone_id_in` INT)
select lz.local_id, loc.local, loc.region, lz.zone_id, z.name  from local_zone as lz 
	join zone as z on z.id = lz.zone_id
	join local as loc on loc.id = lz.local_id
    where z.id = zone_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_periods` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_periods`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM period ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_places` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_places`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM place ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_prices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_prices`()
    NO SQL
SELECT pr.value, cr.symbol, pr.description, tk.type, tu.user, pr.number_stop FROM price AS pr
JOIN currency AS cr ON pr.currency_id = cr.id
JOIN ticket_type as tk ON pr.currency_id = tk.id
JOIN type_user as tu ON pr.currency_id = tu.id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_prices_ids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_prices_ids`(IN `route_id_in` INT, IN `ticket_type_id_in` INT, IN `type_user_id_in` INT, IN `currency_id_in` INT)
BEGIN
select price_id from price_route join price
ON price_route.price_id = price.id
where price_route.route_id = route_id_in
and price.ticket_type_id = ticket_type_id_in
and price.type_user_id = type_user_id_in 
and price.currency_id = currency_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_price_table` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_price_table`(IN `route_id_in` INT, IN `ticket_type_id_in` INT, IN `type_user_id_in` INT, IN `currency_id_in` INT)
    NO SQL
select * from price_route join price ON price.id = price_route.price_id
join currency ON currency.id = price.currency_id
join ticket_type ON ticket_type.id = price.ticket_type_id 
where route_id = route_id_in and ticket_type_id = ticket_type_id_in and type_user_id = type_user_id_in and currency_id = currency_id_in
order by value asc ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_prod` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_prod`(IN prod_cod_in INT)
BEGIN
select p.id  as prod_cod, pr.route_id, rot.name,p.value, cur.symbol ,tt.type, tu.user as type_user from price as p 
join ticket_type as tt on p.ticket_type_id = tt.id
join currency as cur on cur.id = p.currency_id
join price_route as pr on pr.price_id = p.id
join route as rot on pr.route_id = rot.id
join type_user as tu on tu.id = p.type_user_id
where p.id = prod_cod_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_routes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_routes`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN

SET @query = concat('select rt.*,odr.local_id,lc.local,lc.region,odr.instruction, odr.order_table from route_order as odr
join local as lc on lc.id = odr.local_id
join (SELECT * FROM route ORDER BY ',columnName,' ',order_by,' ',limit_offset_in,') as rt on rt.id = odr.route_id
order by odr.order_table ASC');
PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_route_links` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_route_links`(IN `route_id_in` INT,IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
BEGIN
SET @SQLText = CONCAT('select lik.*, rt1.name as route1, lc1.region as region1, lc1.local as local1, rt2.name as route2, lc2.region as region2, lc2.local as local2 from links as lik 
join local as lc1 on lik.local_route1_id = lc1.id
join local as lc2 on lik.local_route2_id = lc2.id
join route as rt1 on lik.route1_id = rt1.id
join route as rt2 on lik.route2_id = rt2.id
where route1_id = ', route_id_in ,' or route2_id = ', route_id_in ,' order by ' ,columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_route_ticket` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_route_ticket`(IN `route_id_in` INT,IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('select * from route_ticket as rt
join ticket_bought as tb on
tb.id = rt.ticket_bought_id
where rt.route_id =', route_id_in ,' order by ' ,columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_route_ticket_filter_used` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_route_ticket_filter_used`(IN `route_id_in` INT,IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20), IN `used_in` tinyint)
    NO SQL
BEGIN   
SET @SQLText = CONCAT('select * from route_ticket as rt
join ticket_bought as tb on
tb.id = rt.ticket_bought_id
where rt.route_id = ',route_id_in,' and tb.used = ',used_in,' order by ',columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_seasons` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_seasons`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN    
SET @SQLText = CONCAT('select ss.id,ss.date_begin, ss.date_end, pi.type, ss.period_id from season as ss 
   join period as pi on ss.period_id = pi.id ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tickets_boughts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_boughts`(IN `limit_offset_in` VARCHAR(50) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM ticket_bought ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tickets_configs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_configs`(IN `limit_offset_in` VARCHAR(50) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM ticket_config ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_tickets_types` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_types`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM ticket_type ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_timetable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_timetable`(IN `route_id_in` INT, IN `type_day_id_in` VARCHAR(45), IN `season_id_in` INT)
    NO SQL
    DETERMINISTIC
SELECT tb.id, tb.hour, lc.local, lc.region, tb.season_id, tb.type_day_id,tb.line_number,tb.local_id, ord.order_table, ord.instruction, rt.name as route,td.name AS type_day, CONCAT(tb.line_number, ord.order_table) AS ordination
  FROM timetable AS tb
    JOIN local AS lc ON tb.local_id = lc.id
    JOIN route AS rt ON tb.route_id = rt.id
    JOIN route_order AS ord ON tb.route_order_id = ord.id
    JOIN season_register AS sr ON sr.timetable_id = tb.id
    JOIN type_day as td on td.id = tb.type_day_id
  WHERE tb.route_id = route_id_in and tb.type_day_id in (type_day_id_in) and sr.season_id = season_id_in
  ORDER BY convert(ordination ,SIGNED) ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_types_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_types_users`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN   
SET @SQLText = CONCAT('SELECT * FROM type_user ORDER BY ', columnName,' ', order_by,' ',limit_offset_in);    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_type_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_type_day`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN    
SET @SQLText = CONCAT('SELECT dg.type_day_id, td.name,dg.day_id, dw.day FROM day_group as dg 
JOIN (select * from type_day ORDER BY ', columnName,' ', order_by,' ',limit_offset_in,') as td ON dg.type_day_id = td.id
JOIN day_week as dw ON dw.id = dg.day_id');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zones` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_zones`(IN `limit_offset_in` VARCHAR(30) , IN `columnName` varchar(50), IN `order_by` varchar(20))
    NO SQL
BEGIN
SET @SQLText = CONCAT('select lz.local_id, loc.local, loc.region, lz.zone_id, z.name  from local_zone as lz 
join (select * from zone ORDER BY ', columnName,' ', order_by,' ',limit_offset_in,') as z on z.id = lz.zone_id
join local as loc on loc.id = lz.local_id');    
    PREPARE stmt FROM @SQLText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_zones_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_zones_route`(IN `route_id_in` INT)
BEGIN
select lz.local_id, loc.local, loc.region, lz.zone_id, z.name  from local_zone as lz 
join zone as z on z.id = lz.zone_id
join route_zone as rz on rz.zone_id = z.id
join local as loc on loc.id = lz.local_id
where rz.route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_contact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_contact`(IN `name_in` VARCHAR(45), IN `phone_in` INT, IN `email_in` VARCHAR(45), IN `website_in` VARCHAR(45))
    NO SQL
BEGIN    
INSERT INTO contact (name, phone, email, website) VALUES 
(name_in, phone_in, email_in, website_in);
SELECT * from contact where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_currency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_currency`(IN `currency_in` VARCHAR(45), IN `symbol_in` VARCHAR(45))
    NO SQL
BEGIN
INSERT INTO currency (currency, symbol) VALUES (currency_in, symbol_in);
SELECT * from currency where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_day_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_day_group`(IN `type_day_id_in` INT, IN `day_id_in` INT)
    NO SQL
INSERT INTO day_group (type_day_id, day_id) 
VALUES (type_day_id_in, day_id_in) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_link` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_link`(IN `local_route1_id_in` INT, IN `local_route2_id_in` INT, IN `route1_id_in` INT, IN `route2_id_in` INT)
    NO SQL
BEGIN    
INSERT INTO links (local_route1_id, local_route2_id, route1_id, route2_id) VALUES (local_route1_id_in, local_route2_id_in, route1_id_in, route2_id_in);
SELECT * from links where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_local`(IN `address_in` VARCHAR(50), IN `latitude_in` DOUBLE, IN `longitude_in` DOUBLE, IN `local_in` VARCHAR(30), IN `region_in` VARCHAR(30), IN `institute_in` BOOLEAN)
    NO SQL
BEGIN   
INSERT INTO local (address,latitude,longitude,local, region, institute) VALUES
(address_in, latitude_in, longitude_in, local_in, region_in, institute_in);
SELECT * from local where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_log_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_log_search`(IN `origin_in` VARCHAR(50), IN `destination_in` VARCHAR(50), IN `date_search_in` DATETIME, IN `error_in` TINYINT, IN `result_count_in` INT, IN `date_search_created_in` datetime)
    NO SQL
INSERT INTO log_search (origin,destination,date_search,error, result_count, date_search_created) VALUES
(origin_in,destination_in,date_search_in,error_in,result_count_in, date_search_created_in) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_period`(IN `type_in` VARCHAR(45))
    NO SQL
BEGIN   
INSERT INTO period (type) VALUES (type_in);
SELECT * from period where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_price_column` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_price_column`(IN `type_user_id_in` INT, IN `ticket_type_id_in` INT, IN `currency_id_in` INT, IN `value_in` DOUBLE, IN `description_in` VARCHAR(45), IN `number_stop_in` INT, IN `route_id_in` INT, IN `tax_id_in` INT, IN `account_id_in` INT)
    NO SQL
BEGIN

INSERT INTO price (type_user_id, ticket_type_id, currency_id, value, description, number_stop, tax_id, account_id) VALUES
(type_user_id_in, ticket_type_id_in, currency_id_in, value_in, description_in, number_stop_in, tax_id_in, account_id_in);

INSERT INTO price_route (price_id, route_id) VALUES (LAST_INSERT_ID(), route_id_in);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_season` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_season`(IN `period_id_in` INT, IN `date_begin_in` DATE, IN `date_end_in` DATE)
    NO SQL
BEGIN

INSERT INTO season (period_id, date_begin, date_end) VALUES
(period_id_in,date_begin_in,date_end_in);
SELECT * from season where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_ticket` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_ticket`(
IN `route_id_in` int, 
IN `price_in` double, 
IN `currency_in` varchar(30), 
IN `ticket_type_in` varchar(30),
IN `validate_date_in` DATE, 
IN `route_in` VARCHAR(100), 
IN `departure_local_in` varchar(50),
IN `arrivel_local_in` varchar(50),
IN `user_name_in` varchar(50),
IN `user_email_in` varchar(50),
IN `type_user_in` varchar(50),
IN `used_in` tinyint,
IN `bought_date_in` datetime)
    NO SQL
BEGIN   
INSERT INTO ticket_bought(price, currency, ticket_type, 
validate_date, route, departure_local, 
arrivel_local, user_name, 
user_email, type_user, used, bought_date) VALUES
(price_in, currency_in, ticket_type_in,
validate_date_in, route_in, 
departure_local_in, arrivel_local_in,
user_name_in, user_email_in, type_user_in, 
used_in, bought_date_in);
insert into route_ticket(ticket_bought_id, route_id) values (LAST_INSERT_ID(), route_id_in);
SELECT * from ticket_bought where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_ticket_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_ticket_type`(IN `type_in` VARCHAR(45))
    NO SQL
BEGIN
INSERT INTO ticket_type (type) VALUES (type_in);
Select * from ticket_type where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_timetable_line` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_timetable_line`(IN `hour_in` TIME, IN `local_id_in` INT, IN `type_day_id_in` INT, IN `line_number_in` INT, IN `route_id_in` INT, IN `season_id_in` INT, IN `order_in` INT)
    NO SQL
BEGIN

INSERT INTO timetable (hour, local_id, type_day_id, line_number, route_id, route_order_id, season_id) VALUES
(hour_in, local_id_in, type_day_id_in, line_number_in, route_id_in, (SELECT ro.id FROM route_order as ro WHERE ro.route_id = route_id_in and ro.local_id = local_id_in and ro.order_table=order_in), season_id_in);

INSERT INTO season_register (season_id, timetable_id) VALUES
(season_id_in, LAST_INSERT_ID());

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_type_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_type_user`(IN `user_in` VARCHAR(45))
    NO SQL
BEGIN
INSERT INTO type_user (user) VALUES (user_in);
SELECT * from type_user where id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `locals_on_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `locals_on_route`(IN `local_begin_in` INT, IN `local_end_in` INT)
    NO SQL
SELECT ro.local_id, ro.order_table, ro.route_id FROM route_order AS ro
where ro.local_id IN (local_begin_in, local_end_in) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `local_coord` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `local_coord`(IN `local_id_in` INT)
    NO SQL
SELECT latitude, longitude, local
FROM local 
where id = local_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `nearby_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `nearby_local`(IN `latitude_in` DOUBLE, IN `longitude_in` DOUBLE)
    NO SQL
SELECT id, local, region,latitude, longitude, 111.045 * DEGREES(ACOS(COS(RADIANS(latitude_in))
 * COS(RADIANS(latitude))
 * COS(RADIANS(longitude) - RADIANS(longitude_in))
 + SIN(RADIANS(latitude_in))
 * SIN(RADIANS(latitude))))
 AS distance_in_km
FROM local
ORDER BY distance_in_km ASC
LIMIT 0,1 ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `route_price` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `route_price`(IN `type_user_id_in` INT, IN `number_stop_in` INT, IN `route_id_in` INT)
    NO SQL
select p.id  as prod_cod,p.value, p.tax_id ,cur.symbol , tt.type, p.account_id,tt.purchase_type ,p.ticket_type_id from price as p 
join ticket_type as tt on p.ticket_type_id = tt.id
join currency as cur on cur.id = p.currency_id
join price_route as pr on pr.price_id = p.id
where type_user_id = type_user_id_in and number_stop = number_stop_in and pr.route_id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `route_prices_ids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `route_prices_ids`(IN `route_id_in` INT)
BEGIN
select * from price_route where route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `route_test` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `route_test`(IN `route2_id_in` INT)
    NO SQL
select rt.*,odr.order_table, lc.id from route_order as odr
join local as lc on lc.id = odr.local_id
join route as rt on rt.id = odr.route_id
where rt.id = route2_id_in
order by odr.order_table ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `route_types_days` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `route_types_days`(IN `route_id_in` INT)
    NO SQL
select distinct(tb.type_day_id), td.name from timetable as tb
join type_day as td on td.id = tb.type_day_id  
where tb.route_id=route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_max` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_max`(IN `route_id_in` INT, IN `max_to_buy_in` INT)
    NO SQL
BEGIN    
UPDATE ticket_config SET max_to_buy = max_to_buy_in
WHERE route_id = route_id_in;
SELECT * FROM ticket_config WHERE route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `surch_types_days` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `surch_types_days`(IN `day_id_in` INT)
    NO SQL
select type_day_id FROM day_group where day_id = day_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `timetable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `timetable`(IN `route_id_in` INT, IN `types_days_ids_in` VARCHAR(35), IN `season_date_in` DATE)
    NO SQL
    DETERMINISTIC
SELECT tb.hour, lc.local,lc.region ,lc.latitude, lc.longitude, tb.line_number, ord.order_table, ord.instruction, rt.name, rt.external, CONCAT(tb.line_number, ord.order_table) AS ordination, tb.local_id
  FROM timetable AS tb
    JOIN local AS lc ON tb.local_id = lc.id
    JOIN route AS rt ON tb.route_id = rt.id
    JOIN route_order AS ord ON tb.route_order_id = ord.id
    JOIN season_register AS sr ON sr.timetable_id = tb.id
  WHERE tb.route_id = route_id_in and tb.type_day_id in (types_days_ids_in) and sr.season_id = (SELECT id FROM season WHERE season_date_in BETWEEN date_begin AND date_end)
  ORDER BY convert(ordination , SIGNED) ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `timetables_how_many` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `timetables_how_many`(IN `route_id_in` INT)
BEGIN

select tb.type_day_id, td.name, tb.season_id,sea.date_begin, sea.date_end, prd.type
from (select distinct season_id, type_day_id from timetable where route_id = route_id_in) as tb 
join type_day as td ON td.id = tb.type_day_id 
join season as sea ON sea.id = tb.season_id
join period as prd ON prd.id = sea.period_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_configuration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_configuration`(IN `configuration_id_in` INT, IN `service_id_in` INT)
BEGIN
update configuration set service_id = service_id_in where id = configuration_id_in;
select * from configuration;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_contact` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_contact`(IN `name_in` VARCHAR(45), IN `phone_in` INT, IN `email_in` VARCHAR(45), IN `website_in` VARCHAR(45), IN `contact_id_in` INT)
    NO SQL
BEGIN
update contact set name=name_in, phone = phone_in, email = email_in, website = website_in
where id = contact_id_in;
select * from contact where id = contact_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_currency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_currency`(IN `currency_id_in` INT, IN `currency_in` VARCHAR(45), IN `symbol_in` VARCHAR(45))
    NO SQL
BEGIN    
update currency set currency = currency_in, symbol = symbol_in
where id = currency_id_in;
select * from currency where id = currency_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_distance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_distance`(IN `route_id_in` INT, IN `local1_id_in` INT, IN `local2_id_in` INT, IN `distance_in` DOUBLE)
    NO SQL
update distance set distance=distance_in
where local1_id = local1_id_in and local2_id = local2_id_in and route_id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_hour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_hour`(IN `hour_in` TIME, IN `timetable_id_in` INT)
    NO SQL
BEGIN    
UPDATE timetable
SET hour=hour_in
WHERE id = timetable_id_in;
select * from timetable where id = timetable_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_instruction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_instruction`(IN `route_id_in` INT, IN `local_id_in` INT, IN `instruction_in` VARCHAR(30))
    NO SQL
update route_order set instruction=instruction_in
where route_id=route_id_in and local_id=local_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_local`(IN `address_in` VARCHAR(30), IN `latitude_in` DOUBLE, IN `longitude_in` DOUBLE, IN `local_in` VARCHAR(30), IN `region_in` VARCHAR(30), IN `local_id_in` INT, IN `institute_in` BOOLEAN)
    NO SQL
BEGIN    
UPDATE local
SET address=address_in, latitude=latitude_in, longitude=longitude_in, local=local_in,region=region_in, institute=institute_in
WHERE id=local_id_in;
SELECT * FROM local WHERE id = local_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_period` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_period`(IN `period_id_in` INT, IN `type_in` VARCHAR(30))
    NO SQL
BEGIN   
update period set type=type_in
where id=period_id_in;
select * from period where id = period_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_price` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_price`(IN `price_id_in` INT, IN `type_user_id_in` INT, IN `ticket_type_id_in` INT, IN `value_in` DOUBLE, IN `description_in` VARCHAR(45), IN `number_stop_in` INT)
    NO SQL
BEGIN    
UPDATE price SET type_user_id = type_user_id_in, ticket_type_id = ticket_type_id_in, currency_id = currency_id_in, value = value_in, description = description_in, number_stop = number_stop_in
WHERE id = price_id_in;
SELECT * FROM price WHERE id = price_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_price_value` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_price_value`(IN `price_id_in` INT, IN `value_in` VARCHAR(45))
    NO SQL
BEGIN    
update price set value = value_in
where id = price_id_in;
select * from price where id = price_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_route`(IN `route_id_in` INT, IN `name_in` VARCHAR(30), IN `contact_id_in` INT)
    NO SQL
update route set name=name_in, contact_id = contact_id_in
where id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_route_local` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_route_local`(IN `route_id_in` INT, IN `local_id_in` INT, IN `instruction_in` varchar(50), IN `distance_in` INT )
BEGIN
update route_order set instruction = instruction_in where local_id = local_id_in and route_id = route_id_in;
update distance set distance = distance_in where local1_id = local_id_in and route_id = route_id_in;
select route_order.route_id, route_order.instruction, distance.distance from route_order join distance on route_order.route_id = distance.route_id
where route_order.local_id = local_id_in and distance.local1_id = local_id_in and route_order.route_id = route_id_in; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_route_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_route_name`(IN `route_id_in` INT, IN `name_in` VARCHAR(50))
    NO SQL
Begin    
update route set name = name_in
where id = route_id_in;
select * from route where id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_season` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_season`(IN `season_id_in` INT, IN `period_id_in` INT, IN `date_begin_in` DATE, IN `date_end_in` DATE)
    NO SQL
Begin    
UPDATE season SET period_id=period_id_in, date_begin=date_begin_in,date_end=date_end_in 
WHERE id=season_id_in;
SELECT * FROM season WHERE id = season_id_in;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_ticket_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_ticket_type`(IN `ticket_type_id_in` INT, IN `type_in` VARCHAR(45))
    NO SQL
BEGIN    
update ticket_type set type = type_in
where id = ticket_type_id_in;
select * from ticket_type where id = ticket_type_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_type_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_type_day`(IN `type_day_id_in` INT, IN `name_in` VARCHAR(30))
    NO SQL
BEGIN
update type_day set name = name_in
where id = type_day_id_in;
select * from type_day where id = type_day_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_type_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_type_user`(IN `user_in` VARCHAR(45), IN `type_user_id_in` INT)
    NO SQL
BEGIN   
update type_user set user = user_in
where id = type_user_id_in;
SELECT * FROM type_user WHERE id = type_user_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_ticket_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `user_ticket_route`(IN `route_id_in` INT)
BEGIN
select distinct type_user_id, ticket_type_id, currency_id  from price_route 
join price on price.id = price_route.price_id
where route_id = route_id_in;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `zones_of_route` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `zones_of_route`(IN `route_id_in` INT)
    NO SQL
select DISTINCT(lz.zone_id), lz.local_id from route_zone as rz join local_zone as lz
on rz.zone_id = lz.zone_id
where rz.route_id = route_id_in ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-22 16:59:52
