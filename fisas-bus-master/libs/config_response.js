/**
 * Created by afcosta on 06/08/2018.
 */
 /**
  * @code 200
  */
module.exports.response_success = function (links, data, errors) {

    return {
        status: "sucess",
        link: links,
        data: data,
        errors: errors
    }
};

/**
 * 
 * @param error
 * @code  400
 */
module.exports.response_error = function (error) {

    return {
        status: "error",
        link: {
            total: 0
        },
        data: [
        ],
        errors: error
    }
};

/**
 * 
 * @code 500
 */
module.exports.failed_authenticate = function () {

    return {
        auth: false, 
        message: 'Failed to authenticate token..'
    }
};

/**
 * 
 * @code 401 
 */
module.exports.no_provided = function () {

    return {
        status: "error",
        link: {
            total: 0
        },
        data: [
        ],
        errors: [{
            code:"901",
            message: 'No token provided.'
        }]

    }
};

/**
 * 
 * @code 403 
 */
module.exports.no_authorized = function () {

    return {
        auth: false, 
        message: 'Not authorized.'
    }
};