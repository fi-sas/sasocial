"use strict";

var crypto = require('crypto'),
     algorithm ='seed',
     password = 'bus';

exports.encrypt = function (prod) {

    try {

        var prod_aux = JSON.stringify(prod);

        var cipher = crypto.createCipher(algorithm, password);

        var crypted = cipher.update(prod_aux, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    } catch (e) {
        console.log("Error: ", e);
        return null;
    }
}

exports.decrypt = function (prod) {

    try {

        var decipher = crypto.createDecipher(algorithm, password);
        var dec = decipher.update(prod, 'hex', 'utf8');
        dec += decipher.final('utf8');


        dec = JSON.parse(dec);

        return dec;

    } catch (e) {

        console.log("Error: ", e);
        return null;
    }
}
