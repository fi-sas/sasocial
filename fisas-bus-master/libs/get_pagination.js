"use strict";

const { URL } = require('url');

exports.getPaginationMetaData = function(total, limit, offset,req, callback) {

    if (limit !== -1) {

        var myURL = new URL(req.protocol + "://" + req.get('host') + req.originalUrl);

        var next, prev, first, last, self;


        self = myURL.href;

        myURL.searchParams.set('offset', '0');
        first = myURL.href;

        var offset_last = Math.floor(total / limit) * limit;
        if (offset_last > 0) {
            myURL.searchParams.set('offset', offset_last);
            last = myURL.href;
        } else {
            last = first;
        }

        var offset_next = offset + limit;
        if (offset_next > total) {
            next = "";
        } else {
            myURL.searchParams.set('offset', offset_next);
            next = myURL.href;
        }

        var offset_prev;
        if (offset === 0) {
            prev = "";
        } else {
            offset_prev = offset - limit
            myURL.searchParams.set('offset', offset_prev);
            prev = myURL.href;
        }

        callback({
            "self": self,
            "prev": prev,
            "next": next,
            "first": first,
            "last": last,
            "total": total
        });
    } else {
        callback({"total": total});
    }
}
