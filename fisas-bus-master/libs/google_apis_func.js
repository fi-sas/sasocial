"use strict";
/**
 * Created by afcosta on 27/07/2018.
 * Version 1.0
 * APIS das GOOGLE
 */

var request = require('request');
var config = require('config');
var moment  = require('moment');
var _ = require('lodash');

/**
 *
 * @param location ->  Local/Morada/Ponto/Localização
 * @param callback (latitude, longitude, error)
 * @description -> Converte um local em coordenadas
 */
"use strict";
exports.geocode = function(location, callback){

    var url = config.get('GOOGLE_GEOCODE.base_url') + "language=" + config.get('GOOGLE_GEOCODE.language')+ "&";
    var latitude, longitude;

    url = encodeURI(url + "address=" + location + "&key=" + config.get('GOOGLE_GEOCODE.api_key'));

    request(url, function (error, response, json) {

        if (!error) {
            try {
                json = JSON.parse(json);

                if (json.status === "OK") {

                    var country_find = _.find(json.results[0].address_components, {'short_name': 'PT'});

                    if (country_find !== undefined) {

                        latitude = parseFloat(json.results[0].geometry.location.lat);
                        longitude = parseFloat(json.results[0].geometry.location.lng);

                        callback(latitude, longitude, null);
                    } else {
                        callback(null, null, {message: "INVALID_LOCAL"});
                    }
                } else {
                    if (json.status === "ZERO_RESULTS") {
                        throw new Error(config.google_geocode.name+': UNDEFINED_LOCATION');
                    }
                    if (json.status === "REQUEST_DENIED") {
                        throw new Error(config.google_geocode.name+': REQUEST_DENIED');
                    }
                    if (json.status === "INVALID_REQUEST") {
                        throw new Error(config.google_geocode.name+': INVALID_REQUEST');
                    }
                    if (json.status === "OVER_QUERY_LIMIT") {
                        throw new Error(config.google_geocode.name+': OVER_QUERY_LIMIT');
                    }
                    if (json.status === "UNKNOWN_ERROR") {
                        throw new Error(config.google_geocode.name+': UNKNOWN_ERROR');
                    }
                }
            } catch (e) {
                callback(null, null, e);
            }
        } else {
            callback(null,null,error);
        }
    });
};

"use strict";
/**
 *
 * @param origin
 * @param destination
 * @param first_local
 * @param last_local
 * @param departure_time
 * @param arrival_time
 * @param date
 * @param callback
 * @description Obtém as rotas a partir do google directions
 */
exports.directions = function(origin, destination, first_local, last_local , departure_time, arrival_time, date,callback) {

    var url;
    var id = 101;

    // Se verificar esta condição é só retornado rotas do google
    if (arrival_time !== null && departure_time !== null) {

        if (origin == null) {

            arrival_time = null;
            origin = last_local;
            //departure_time = moment.unix(date.format("YYYY-MM-DD") + " " + departure_time);
            departure_time = moment.unix(date).format("YYYY-MM-DD") + ' ' + departure_time;
            departure_time = moment(departure_time).unix();

        } else {

            destination = first_local;
            departure_time = null;
            //arrival_time = moment.unix(date.format("YYYY-MM-DD") + " " + arrival_time);
            arrival_time = moment.unix(date).format("YYYY-MM-DD") + ' ' + arrival_time;
            arrival_time = moment(arrival_time).unix();

        }


        if (arrival_time !== null) {

            url = config.get('GOOGLE_DIRECTIONS.base_url')+"origin=" + origin + "&destination=" + destination + "&arrival_time=" + arrival_time + "&mode=" + config.get('GOOGLE_DIRECTIONS.mode') + "&language=" + config.get('GOOGLE_DIRECTIONS.language') + "&region="+ config.get('GOOGLE_DIRECTIONS.region') +"&key=" + config.get('GOOGLE_DIRECTIONS.api_key');

        } else {


            url = config.get('GOOGLE_DIRECTIONS.base_url')+"origin=" + origin + "&destination=" + destination + "&departure_time=" + departure_time + "&mode=" + config.get('GOOGLE_DIRECTIONS.mode') + "&language=" + config.get('GOOGLE_DIRECTIONS.language') + "&region="+ config.get('GOOGLE_DIRECTIONS.region') + "&key=" + config.get('GOOGLE_DIRECTIONS.api_key');

        }
    } else {

       url = config.get('GOOGLE_DIRECTIONS.base_url')+"origin=" + origin + "&destination=" + destination + "&alternatives=" + config.get('GOOGLE_DIRECTIONS.alternatives') + "&mode=" + config.get('GOOGLE_DIRECTIONS.mode') + "&language=" + config.get('GOOGLE_DIRECTIONS.language') + "&region="+ config.get('GOOGLE_DIRECTIONS.region') + "&key=" + config.get('GOOGLE_DIRECTIONS.api_key');
    }
    // var url = config.google_directions_api.base_url+"origin=" + origin + "&destination=" + destination + "&alternatives=" + config.google_directions_api.alternatives + "&mode=" + config.google_directions_api.mode + "&language=" + config.google_directions_api.language + "&key=" + config.google_directions_api.api_key;
    url = encodeURI(url);


    var wallking, transit;
    var line = [], routes = [];
    var route_line = '';
    var route_prices = [], route_prices_aux = [];

    request(url,function(error, response, json) {

        if (!error){
            json = JSON.parse(json);

            try {
                if (json.status === "OK") {

                        for (var r = 0; r < json.routes.length; r++) {

                            for (var i = 0; i < json.routes[r].legs[0].steps.length; i++) {

                                if (json.routes[r].legs[0].steps[i].travel_mode === "TRANSIT") {

                                    if (json.routes[r].legs[0].steps[i].transit_details.line.hasOwnProperty('name')) {
                                        route_line = json.routes[r].legs[0].steps[i].transit_details.line.name + ", " + json.routes[r].legs[0].steps[i].transit_details.line.short_name;

                                    } else {
                                        route_line = json.routes[r].legs[0].steps[i].transit_details.line.short_name;
                                    }

                                    var id_g = id++;

                                    transit = {

                                        local: json.routes[r].legs[0].steps[i].transit_details.departure_stop.name,
                                        lat: json.routes[r].legs[0].steps[i].transit_details.departure_stop.location.lat,
                                        lng: json.routes[r].legs[0].steps[i].transit_details.departure_stop.location.lng,
                                        instruction: json.routes[r].legs[0].steps[i].html_instructions,
                                        departure_time: json.routes[r].legs[0].steps[i].transit_details.departure_time.text + ':00',
                                        duration: json.routes[r].legs[0].steps[i].duration.text,
                                        contacts: json.routes[r].legs[0].steps[i].transit_details.line.agencies,
                                        route_id: id_g,
                                        route_line: route_line,
                                        external: true,
                                        vehicle: json.routes[r].legs[0].steps[i].transit_details.line.vehicle.name.toUpperCase().replace(/ /g,"_")

                                    };

                                    line.push(transit);

                                    transit = {

                                        local: json.routes[r].legs[0].steps[i].transit_details.arrival_stop.name,
                                        lat: json.routes[r].legs[0].steps[i].transit_details.arrival_stop.location.lat,
                                        lng: json.routes[r].legs[0].steps[i].transit_details.arrival_stop.location.lng,
                                        instruction: "",
                                        departure_time: json.routes[r].legs[0].steps[i].transit_details.arrival_time.text + ':00',
                                        duration: 0,
                                        contacts: json.routes[r].legs[0].steps[i].transit_details.line.agencies,
                                        route_id: id_g,
                                        route_line: route_line,
                                        external: true,
                                        vehicle: json.routes[r].legs[0].steps[i].transit_details.line.vehicle.name.toUpperCase().replace(/ /g,"_")

                                    };

                                    line.push(transit);

                                    route_prices_aux.push({
                                        "route_id": id_g,
                                        "route": route_line,
                                        "departure_local": json.routes[r].legs[0].steps[i].transit_details.departure_stop.name,
                                        "arrival_local": json.routes[r].legs[0].steps[i].transit_details.arrival_stop.name,
                                        "departure_hour": json.routes[r].legs[0].steps[i].transit_details.departure_time.text + ':00',
                                        "arrival_hour": json.routes[r].legs[0].steps[i].transit_details.arrival_time.text + ':00',
                                        "purchare_active": false,
                                        "price": 0
                                    });

                                }
                                if (json.routes[r].legs[0].steps[i].travel_mode === "WALKING") {

                                    wallking = {
                                        duration: json.routes[r].legs[0].steps[i].duration.text,
                                        instruction: json.routes[r].legs[0].steps[i].html_instructions,
                                        vehicle: "CAMINHADA"
                                    };
                                    line.push(wallking);
                                }
                            }

                            if (arrival_time === null && departure_time === null) {

                              routes.push(line);
                              route_prices.push(route_prices_aux);
                              route_prices_aux = [];
                              line = [];


                            } else {
                               route_prices = _.concat(route_prices, route_prices_aux);
                               routes = _.concat(routes,line);
                            }

                            //routes.push(line);
                        }

                        callback(routes, route_prices, null);

                } else {
                    if (json.status === "ZERO_RESULTS") {
                        callback([],[], null);
                    }
                    if (json.status === "REQUEST_DENIED") {
                        throw new Error(config.google_directions_api.name+': REQUEST_DENIED');
                    }
                    if (json.status === "INVALID_REQUEST") {
                        throw new Error(config.google_directions_api.name+': INVALID_REQUEST');
                    }
                    if (json.status === "OVER_QUERY_LIMIT") {
                        throw new Error(config.google_directions_api.name+': OVER_QUERY_LIMIT');
                    }
                    if (json.status === "UNKNOWN_ERROR") {
                        throw new Error(config.google_directions_api.name+': UNKNOWN_ERROR');
                    }
                }
            } catch (e) {

                callback([],[],e);
            }

        } else {
           callback(null,[],error);
        }
    });
};

/**
 *
 * @param latitude
 * @param longitude
 * @param radius -> Raio
 * @param callback (transit_stations, error)
 * @description -> Estações de transportes presente no google maps
 */
"use strict";
exports.transit_station = function(latitude,longitude,radius, callback){

    var transit_stations = [];
    var url = config.get('GOOGLE_PLACES.base_url') +"location=" + latitude + ", " + longitude + "&radius="+ radius +"&type=" + config.get('GOOGLE_PLACES.type') + "&language=" + config.get('GOOGLE_PLACES.language') + "&key=" + config.get('GOOGLE_PLACES.api_key');
    url = encodeURI(url);

        request(url,function(error, response, json) {

            if (!error) {

                try {

                    json = JSON.parse(json);
                    if (json.status === "OK"){
                        for (var i = 0; i < json.results.length; i++) {

                            transit_stations.push({
                                latitude: json.results[i].geometry.location.lat,
                                longitude: json.results[i].geometry.location.lng,
                                type_transit: json.results[i].types[0]
                            })
                        }
                        callback(transit_stations, null);

                    } else {
                        if (json.status === "ZERO_RESULTS") {
                            callback ([], null);
                        }
                        if (json.status === "REQUEST_DENIED") {
                            throw new Error(config.google_places.name+': REQUEST_DENIED');
                        }
                        if (json.status === "INVALID_REQUEST") {
                            throw new Error(config.google_places.name+': INVALID_REQUEST');
                        }
                        if (json.status === "OVER_QUERY_LIMIT") {
                            throw new Error(config.google_places.name+': OVER_QUERY_LIMIT');
                        }
                        if (json.status === "UNKNOWN_ERROR") {
                            throw new Error(config.google_places.name+': UNKNOWN_ERROR');
                        }
                    }
                } catch (e) {
                    callback ([], e);
                }
            } else {
                callback ([], error);
            }
        });
};

/**
 *
 * @param origin
 * @param destination
 * @param destinations
 * @param locals_ids
 * @param callback (destination, error)
 * @description -> Determina o destino com o tempo de viagem mais curto
 */
exports.distances = function (origin, destination, destinations, locals_ids,callback) {

    var values = [];
    var destinations_string;

    destinations_string = destinations.join("|");

    var url = config.get('GOOGLE_DISTANCES.base_url') +"origins=" + origin + "&destinations=" + destinations_string + "&mode=" + config.get('GOOGLE_DISTANCES.mode') + "&language=" + config.get('GOOGLE_DISTANCES.language') + "&key=" + config.get('GOOGLE_DISTANCES.api_key');
    //url = encodeURI(url);
    request(url, function(error, response, json) {

        if (!error) {

            try {

                json = JSON.parse(json);

                if (json.status === "OK") {

                    // Guarda os resultados das melhores paragens num array
                    for (var i = 0; i < json.rows[0].elements.length; i++) {

                      if (json.rows[0].elements[i].status === "OK") {
                          values.push(
                              parseInt(json.rows[0].elements[i].duration.value)
                          );
                      }
                    }

                    if (values.length !== 0) {

                        var best_station = false;


                        do {

                            var min = Math.min.apply(Math, values);


                            var index = json.rows[0].elements.map(function (e) {
                                if (e.hasOwnProperty('duration')) {
                                    return e.duration.value;
                                }
                            }).indexOf(min);

                            if (destination !== null) {
                                if (index > locals_ids.indexOf(parseInt(destination))) {
                                    _.pull(values, min);

                                } else {
                                    best_station = true;
                                }
                            } else {
                                best_station = true;
                            }

                        } while(!best_station);


                        if (index !== -1) {
                            callback(destinations[index], index,null);
                        } else {
                            callback(null, -1,null);
                        }

                    } else {
                        callback(null, -1,null);
                    }

                } else {
                    if (json.status === "MAX_ELEMENTS_EXCEEDED") {
                        throw new Error(config.google_distances.name+': MAX_ELEMENTS_EXCEEDED');
                    }
                    if (json.status === "REQUEST_DENIED") {
                        throw new Error(config.google_distances.name+': REQUEST_DENIED');
                    }
                    if (json.status === "INVALID_REQUEST") {
                        throw new Error(config.google_distances.name+': INVALID_REQUEST');
                    }
                    if (json.status === "OVER_QUERY_LIMIT") {
                        throw new Error(config.google_distances.name+': OVER_QUERY_LIMIT');
                    }
                    if (json.status === "UNKNOWN_ERROR") {
                        throw new Error(config.google_distances.name+': UNKNOWN_ERROR');
                    }
                }
            } catch (e) {
                console.log(e);
                callback ([],-1 ,e);
            }
        } else {
            callback ([], -1,error);
        }
    });
};



