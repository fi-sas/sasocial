"use strict";
/**
 * Created by afcosta on 30/01/2018.
 */

var search_route_database = require('../models/search_route_model');
var google_apis = require("./google_apis_func");
var local = require("../models/back_office/local_model");
var configuration = require("../models/back_office/configuration_model");

var crypto_prod = require("./crypto_prod");
var moment  = require('moment');
var async   = require("async");
var _ = require('lodash');


// region PICK TIMETABLES
/**
 *
 * @param routes -> Rotas
 * @param type_day -> Tipo de dia
 * @param season -> Período
 * @param callback (asyncRotas)
 * @description -> Cria um array com as funções para inserir os horários em cada rota
 */
function pickTimetable(routes, type_day, season, callback) {

    var asyncRoutes = [];
    if (routes.length !== 0) {

        routes.forEach(function (route, index, array) {

            asyncRoutes.push(function (callback) {

                search_route_database.pick_timetables(route.route, type_day, season, function (timetable, status, error) {

                    if (!error) {

                        search_route_database.distance_by_route(route.route, function (distances, error) {

                            if (!error) {

                                search_route_database.find_zones_of_route(route.route, function (stops_zones , error) {

                                    if (!error) {

                                        try {

                                            if (status === "OK") {
                                                search_route_database.contacts(timetable.route, function (contacts, error) {

                                                 if (!error) {

                                                     search_route_database.purchare_active(route.route, function (active , error) {

                                                         if (!error) {

                                                             routes[index].route_line = timetable.route_line;
                                                             routes[index].external = timetable.external === 0 ? false : true;
                                                             routes[index].locals_ids = timetable.locals_ids;
                                                             routes[index].coordinates = timetable.coord;
                                                             routes[index].hours = timetable.table;
                                                             routes[index].regions = timetable.regions;
                                                             routes[index].instructions = timetable.instructions;
                                                             routes[index].distances = distances;
                                                             routes[index].contacts = contacts;
                                                             routes[index].stops_zones = stops_zones;
                                                             routes[index].purchase_tickets = active === 0 ? false : true;

                                                             callback(null);

                                                         } else {
                                                             callback(error);
                                                         }

                                                     })

                                                 } else {
                                                     callback(error);
                                                 }
                                                });
                                            } else {
                                                if (status === "NOT_EXIST") {

                                                 /* routes.splice(index,1); */

                                                 routes[index].route_line = null
                                                 routes[index].external = null;
                                                 routes[index].locals_ids = null;
                                                 routes[index].coordinates = null;
                                                 routes[index].hours = null;
                                                 routes[index].regions = null;
                                                 routes[index].instructions = null;
                                                 routes[index].distances = null;
                                                 routes[index].contacts = null;
                                                 routes[index].stops_zones = null;
                                                 routes[index].purchase_tickets = null;

                                                 callback(null);
                                                }
                                            }
                                        } catch (e) {
                                            callback(e);
                                        }
                                    } else {
                                        callback(error);
                                    }
                                });
                            } else {
                                callback(error);
                            }
                        });

                    } else {
                        callback(error);
                    }
                });
            });
            if (index === array.length - 1) {
                callback(asyncRoutes);
            }
        });
    } else {
        callback(asyncRoutes);
    }
}

/**
 *
 * @param routes_detected -> Rota detectadas
 * @param type_day -> Tipo de dia
 * @param season -> Período
 * @param callback (error)
 * description -> Busca os horários na base de dados e os aplica em cada rota
 */
exports.pickTimetables = function (routes_detected, type_day, season, callback) {

    var asyncRoutes_r = [];

    async.series([

            function (callback) {

                routes_detected.forEach(function (route, index, array) {

                    pickTimetable(route, type_day, season, function (asyncRoutes) {

                            asyncRoutes_r = _.concat(asyncRoutes_r, asyncRoutes);

                    });
                    if (index === array.length - 1) {

                        callback();
                    }
                });
            },

            function (callback) {

                async.parallel(asyncRoutes_r, function (error) {

                    if (!error) {
                        callback(null);
                    } else {
                        callback(error);
                    }
                });
            }

        ],
        function (err) {
            callback(err);
        });

};

//endregion

//region ROUTE CREATE

/**
 *
 * @param origin_index -> indice do primeiro local
 * @param destination -> Id do destino (Fim da rota)
 * @param row_index -> Linha da tabela horária
 * @param route -> Dados da rota
 * @param trace_out -> Entra um array para guardar o trajeto
 * @param routes_price_out -> guarda o número das zonas para o cálculo do preço
 * @description -> Traça uma rota com as paragens apartir de um local de inicio e final
 */
function trace_route(origin_index, destination, row_index, route ,trace_out, routes_price_out) {

    var transit;
    var distance = 0, duration;
    var number_stops = -1, aux_zone;

        for (var j = origin_index; j < route.hours[row_index].length; j++) {

            try {

                //region DurationCalc (Calculo da duracão ponto a ponto)

                if ((j + 1) === route.hours[row_index].length) {
                    duration = "0";
                } else {
                    var b, a;
                    a = moment.duration(route.hours[row_index][j], "m");
                    if (route.hours[row_index][j + 1] !== null) {
                        b = moment.duration(route.hours[row_index][j + 1], "m");
                    } else {
                        b = moment.duration(route.hours[row_index][j + 2], "m");
                    }
                    duration = b.subtract(a).minutes();
                    duration = Math.round(duration);
                }

                //endregion

                //region Soma das distâcias
                if (route.distances.length !== 0) {

                    for (var d = 0; d < origin_index; d++) {

                        if ((j + 1) == route.locals_ids.length) {
                            distance = 0;
                        } else {
                            if (route.distances[d].local1_id === route.locals_ids[j] && route.distances[d].local2_id === route.locals_ids[j + 1]) {

                                distance = route.distances[d].distance;
                            }
                        }
                    }
                }
                //endregion

                //region Cálculo das zonas

               
                if (route.stops_zones.length !== 0) {


                    var zone = _.find(route.stops_zones, {'local_id': route.locals_ids[j]});

                    if (j == origin_index) {
                        aux_zone = zone.zone_id;
                        number_stops = 0;
                    } else {
                        if (aux_zone != zone.zone_id) {
                            number_stops++;
                            aux_zone = zone.zone_id;
                        }
                    }
                }

                //endregion

                //region Inserção dos locais
                if (route.hours[row_index][j] !== null) {

                    transit = {
                        local: route.hours[0][j],
                        local_id: route.locals_ids[j],
                        region: route.regions[j],
                        lat: route.coordinates[j][0],
                        lng: route.coordinates[j][1],
                        instruction: route.instructions[j],
                        departure_time: route.hours[row_index][j],
                        duration: duration.toString() + " mim.",
                        distance: distance + " km.",
                        contacts: route.contacts,
                        route_id: route.route,
                        route_line: route.route_line,
                        external: route.external,
                        vehicle: "AUTOCARRO"
                    };

                    // total_distance += distance;
                    trace_out.push(transit);
                }

                if (route.locals_ids[j] == destination) {

                    routes_price_out.push({route_id: route.route,
                                           route: route.route_line,
                                           departure_local: trace_out[0].local,
                                           arrival_local: trace_out[trace_out.length-1].local,
                                           departure_hour: trace_out[0].departure_time,
                                           arrival_hour: trace_out[trace_out.length-1].departure_time,
                                           purchase_active: route.purchase_tickets,
                                           price: number_stops});
                    return null;
                    break;
                }

                //endregion

            } catch (e) {
                return e;
            }
        }
};

/**
 *
 * @param route -> Conjunto de rotas para criar a interligação
 * @param destination -> id do destino final da rota
 * @param callback (trace, error)
 * @description -> Cria um trajeto com as rota inseridas
 */
function create_route(route, destination, callback) {

    var trace = [], error, departure_index, end_local_id;
    var begin, end;
    var traces = [] , route_prices = [];

    if (route.length !== 0) {

        try {

            for (var h = 1; h < route[0].hours.length; h++) {

                // Encontra o index do local da primeira rota

                departure_index = _.indexOf(route[0].locals_ids, route[0].begin_local_id);

                // Verifica se esta rota é a rota final
                if (route.length > 1) {

                    end_local_id = route[1].begin_local_id;

                } else {

                    if (route[0].hasOwnProperty('end_local_id')) {
                        end_local_id = route[0].end_local_id;
                    } else {
                        end_local_id = destination;
                    }
                }

                // Traça a Rota
                error = trace_route(departure_index, end_local_id, h, route[0], trace, route_prices);

                if (!error) {
                     // Se o trajeto inclui mais que uma rota
                    if (route.length > 1) {

                        // Ciclo para as rotas complementares
                        for (var route_index = 1; route_index < route.length; route_index++) {

                            departure_index = _.indexOf(route[route_index].locals_ids, route[route_index].begin_local_id);

                            // Ciclo para as hora de uma rota
                            for (var hh = 1; hh < route[route_index].hours.length; hh++) {

                                if (route[route_index].hours[hh][departure_index] !== null) {

                                    // Hora de ínicio da novas rota
                                    begin = moment(route[route_index].hours[hh][departure_index], 'HH:mm:ss');

                                    // Hora final da última rota traçada
                                    end = moment(trace[trace.length - 1].departure_time, 'HH:mm:ss');

                                    //Verifica se a hora é igual ou depois da hora final

                                    if (end.isBefore(begin) || end.isSame(begin)) {

                                        if (typeof route[route_index + 1] === 'undefined') {

                                            end_local_id = destination;

                                        } else {
                                            end_local_id = route[route_index + 1].begin_local_id;
                                        }

                                        trace.push({
                                            duration: Math.round(moment.duration(route[route_index].hours[hh][departure_index]).asMinutes() - moment.duration(trace[trace.length - 1].departure_time).asMinutes()),
                                            instruction: "Troca de Transporte",
                                            vehicle: "CAMINHADA"
                                        });

                                        error = trace_route(departure_index, end_local_id, hh, route[route_index], trace, route_prices);
                                        if (error) {

                                            throw new Error(error);
                                        } else {

                                            if (trace[trace.length-1].local_id === end_local_id) {

                                                break;
                                            }
                                        }
                                    } else {
                                        trace = ["SEM PERCURSO"]
                                    }
                                }
                            }
                        }

                        if (trace[trace.length-1].local_id === destination) {

                            traces.push({"steps": trace, "route_price_detail": route_prices, "stops": 0});

                        }
                        //callback(trace, null);
                    } else {
                        traces.push( {"steps":trace, "route_price_detail":route_prices, "stops": 0});
                        trace = [];
                        //callback(trace, null);
                    }
                } else {
                    throw new Error(error);
                    // callback(null, error);
                    // break;
                }
                route_prices = [];
            }// Fim do ciclo
            callback(traces);
        } catch (e) {

           callback(null, e);
        }
    } else {
        callback(null,null);
    }
}

/**
 * 
 * @param routes 
 * @param destination 
 * @param callback
 * @description Cria um conjunto de rotas
 */
exports.create_routes = function (routes, destination, callback) {

    var asyncTraces = [], traces  = [];

    async.series([

            function (callback) {

                routes.forEach(function (route, index, array) {

                    asyncTraces.push(function (callback) {
                        create_route(route, destination, function (trace, error) {

                            if (!error) {
                                traces = _.concat(traces, trace);
                                callback(null)
                            } else {
                                callback(error);
                            }
                            //callback(error, trace);
                        });
                       });

                        if (index === array.length - 1) {

                            callback();
                        }
                    });

            },
            function (callback) {

                async.parallel(asyncTraces, function (error) {

                    if (!error) {
                        callback(null);
                    } else {
                        callback(error);
                    }
                });
            }
        ],
        function (error) {

        if (!error) {

             callback(traces, null);
         } else {
            callback([], error)
        }

        });


};

//endregion

//region ROUTES DETECT
/**
 *
 * @param routes - > Rotas
 * @param callback (routes, error)
 * @description Cria várias rotas ligadas
 */
function loop_route_links(routes, origin, destination, callback) {

    var asyncRotas = [];

    if (routes.length !== 0) {

        async.waterfall([

            function (callback) {

                routes.forEach(function (route, index, array) {

                        asyncRotas.push(function (callback) {
                            search_route_database.routes_links([route], origin, destination, function (routes, error) {

                                if (!error) {
                                    callback(null, routes);
                                } else {
                                    callback(error, []);
                                }
                            });

                        });
                        if (index === array.length - 1) {
                            callback();
                        }
                });
            },
            function (callback) {

                async.parallel(asyncRotas, function (err, results) {

                    if (!err) {
                        callback(null, results);
                    } else {
                        callback(err, []);
                    }

                });
            }
        ], function (error, results) {

            if (!error) {
                callback(results, null);
            } else {
                callback([], error);
            }
        });
    } else {
        callback([], null);
    }
}

/**
 *
 * @param origin -> Origem
 * @param destination -> Destino
 * @param type_day -> Tipo de dia
 * @param callback (routes, error)
 * @description -> encontra as rotas as rotas
 */
exports.detect_routes = function (origin, destination, type_day, callback) {

    var one_local = false, local_id , begin_local_id;
    var routes_ax = [];

    local_id = origin;

    if (!isNaN(origin) && isNaN(destination) ) {
        one_local = true;
        begin_local_id = origin;
    } else if (isNaN(origin) && !isNaN(destination)) {
        local_id = destination;
        begin_local_id = null;
        one_local = true;
    }

    async.waterfall([

            function (callback) {

               search_route_database.find_routes(origin, destination, function (routes, error) {

                    if (!error) {

                        //Uma Rota detectada
                        if (routes.length === 0) { //

                            routes_ax = routes;
                            search_route_database.detect_routes_by_one_local(local_id, type_day, function (routes, error) {

                                if (!error) {
                                    callback(null, routes, false);
                                } else {
                                    callback(error, [], false);
                                }

                            });

                        //Uma Rota detectada
                        } else { //
                            callback(null, routes, true); //
                        } //

                    } else {
                        callback(error, [], false);
                    }
                });
                

            },
            function (routes, detect, callback) {

                if (!detect) { //

                    if (!one_local) {

                        loop_route_links(routes, origin, destination, function (routes_r, error) {
                            if (!error) {
                                if (routes_aux === 0) {
                                    callback(null, routes_r);
                                } else {
                                    callback(null, routes_r.concat(routes_ax));
                                }
                            } else {

                                callback(error, []);
                            }
                        });

                    } else {

                        var routes_aux = [];

                        for (var i = 0; i < routes.length; i++) {
                            //routes_aux = _.concat(routes_aux, [{route: routes[i], begin_local_id: 69}]);
                            routes_aux.push([{route: routes[i], begin_local_id: 69}]);
                        }

                        callback(null, routes_aux);
                
                }
             } else {//
                    callback(null, routes);//
             }//

            }
        ],

        function (error, results) {

            if (!error) {

                // Elimina resultados vazios do paralelo
                _.remove(results, function(r) {
                    return r.length === 0;
                });

                callback(results, null);
            } else {

                callback([], error);
            }
        });
};

//endregion

/**
 *
 * @param routes -> rotas geradas
 * @param location_coord -> origem em coordenadas
 * @param location_id -> Destino em id caso seja instituto
 * @param callback
 * @description -> Descobre o ponto mais proximo de uma estação e determina qual é o ponto com o transporte mais rápido
 */
exports.best_station_by_route = function (routes, location_coord, location_id, mode,callback) {

    var asyncRoutes = [] , last_destination;

    async.series([

        function (callback) {

            routes.forEach(function (route, index, array) {

                asyncRoutes.push(function (callback) {

                    try {

                        var coordinates = [];
                        for (var i = 0; i < route[0].coordinates.length; i++) {
                            coordinates.push(route[0].coordinates[i][0] + "," + route[0].coordinates[i][1]);
                        }

                        if (mode === 1) {
                          last_destination = null;
                        } else {
                            last_destination = location_id;
                        }

                        google_apis.distances(location_coord, last_destination, coordinates, route[0].locals_ids, function (best_destination, index_id, error) {
                            if (!error) {

                                // Verifica se a rota tem um ponto proximo de uma estação de transportes
                                if (best_destination !== null) {

                                    // Verifica se a melhor estação não é a mesma que o destino/origem
                                    if (parseInt(location_id) !== parseInt(route[0].locals_ids[index_id])) {

                                        if (mode === 0) {
                                            route[0].begin_local_id = route[0].locals_ids[index_id];
                                        } else if (mode === 1) {
                                            route[0].begin_local_id = parseInt(location_id);
                                            route[0].end_local_id = route[0].locals_ids[index_id];
                                        }
                                    } else {
                                        routes.splice(routes.indexOf(route), 1);
                                    }
                                } else {
                                    //Caso não tenha elimina a rota
                                    routes.splice(routes.indexOf(route), 1);
                                }
                              
                                callback(null, best_destination);
                            } else {
                                throw new Error(error);
                            }
                        });
                    } catch (e) {
                        callback(e, []);
                    }
                });
                if (index === array.length - 1) {
                    callback();
                }
            });
        },
        function (callback) {

            async.parallel(asyncRoutes, function (err, results) {

                if (!err) {
                    callback(null, results);
                } else {
                    callback(err, []);
                }

            });
        }
    ], function (error, results) {

        if (!error) {
            callback(results, null);
        } else {
            callback([], error);
        }
    });

}

/**
 *
 * @param origin
 * @param destination
 * @param date
 * @param routes
 * @param local_id
 * @param callback
 * @description Adiciona as rotas dos transportes no google
 */
exports.add_google_diretions = function(origin, destination, date, routes, local_id, callback) {

    var asyncRoutes = [];

    var routes_aux = [];

    if (routes.length !== 0) {

        async.series([

            function(callback) {

                routes.forEach(function (route, index, array) {

                    asyncRoutes.push(function (callback) {

                        google_apis.directions(origin,
                                               destination,
                                               route.steps[0].lat + "," + route.steps[0].lng,
                                               route.steps[route.steps.length - 1].lat + "," + route.steps[route.steps.length - 1].lng,
                                               route.steps[route.steps.length - 1].departure_time,
                                               route.steps[0].departure_time,
                                               date,
                            function (transit_route, route_prices,error) {

                            if (!error) {
                                var begin, end;
                                if (origin !== null) {

                                    if (transit_route.length !== 0) {

                                    begin = moment(route.steps[0].departure_time, 'HH:mm:ss');

                                    if (transit_route[transit_route.length -1].vehicle === "CAMINHADA") {
                                        end = moment(transit_route[transit_route.length -2].departure_time, 'HH:mm:ss');
                                    } else {
                                        end = moment(transit_route[transit_route.length -1].departure_time, 'HH:mm:ss');
                                    }

                                    if (end.isBefore(begin)) {
                                        routes[index].steps = _.concat(transit_route, route.steps);
                                        routes[index].route_price_detail =_.concat(route_prices, route.route_price_detail);
                                        routes_aux.push(routes[index]);
                                    }

                                    }

                                } else {

                                  var aux_time;

                                  if (transit_route.length !== 0) {

                                    begin = moment(route.steps[route.steps.length-1].departure_time, 'HH:mm:ss');

                                    if (transit_route[0].vehicle === "CAMINHADA") {
                                       aux_time = moment(transit_route[1].departure_time, 'HH:mm:ss').add(parseInt(transit_route[0].duration), 'minutes');
                                       end = moment(aux_time, 'HH:mm:ss');
                                    } else {
                                        end = moment(transit_route[0].departure_time, 'HH:mm:ss');
                                    }

                                    if (begin.isBefore(end)) {

                                        if (routes[index] !== undefined) {
                                            routes[index].steps = _.concat(route.steps, transit_route);
                                            routes[index].route_price_detail =_.concat(route.route_price_detail, route_prices);
                                            routes_aux.push(routes[index]);
                                        }
                                    }
                                  }
                                }
                                callback(null);
                            } else {
                                callback(error);
                            }
                        });

                    });

                    if (index === array.length - 1) {
                        callback();
                    }

                });
            },

            function (callback) {

                async.parallel(asyncRoutes, function (err) {

                    if (!err) {
                        callback(null);
                    } else {
                        callback(err);
                    }
                });
            }
        ], function (error) {

                if (!error) {

                    callback(null, routes_aux);
                } else {
                    callback(error, null);
                }
            });

    } else {
        
        search_route_database.local_coord(local_id, function(coord, error) {

            if (!error) {
            if (origin === null) {
                origin = coord[0] + "," + coord[1];
            } else {
                destination = coord[0] + "," + coord[1];
            }

            google_apis.directions(origin,
                destination,
                null,
                null,
                null,
                null,
                date,
            function (transit_route, route_prices ,error) {

                if(!error) {

                    for (var i = 0; i < transit_route.length; i++) {
                        routes.push({"steps": transit_route[i], "route_price_detail":route_prices[i], "stops": 0});
                    }

                    routes_aux = routes;

                    callback(null, routes_aux);
                } else {
                    callback(error, null);
                }
            });
        } else {
            callback(error, null);
        }
     });
    }
};

/**
 *
 * @param route -> array com as rotas
 * @param index -> Índice da rota
 * @returns {duration , departure_hour , arrival_hour}
 * @description Calcula a duração total de uma rota a hora de partida e a hora e chegada
 */
function calcDuration(route, index) {

    var t, departure_hour, arrival_hour;

    try {

        if (route[index].steps[0].departure_time === undefined && route[index].steps[route[index].steps.length - 1].departure_time === undefined) {

            departure_hour = moment.duration(route[index].steps[1].departure_time).subtract(parseInt(route[index].steps[0].duration), 'minutes');
            arrival_hour = moment.duration(route[index].steps[route[index].steps.length - 2].departure_time).add(parseInt(route[index].steps[route[index].steps.length - 1].duration), 'minutes');

            departure_hour = (departure_hour.hours().toString().length === 1 ? "0" + departure_hour.hours() : departure_hour.hours()) + ":" + (departure_hour.minutes().toString().length === 1 ? "0" + departure_hour.minutes() : departure_hour.minutes()) + ":00";
            arrival_hour = (arrival_hour.hours().toString().length === 1 ? "0" + arrival_hour.hours() : arrival_hour.hours()) + ":" + (arrival_hour.minutes().toString().length === 1 ? "0" + arrival_hour.minutes() : arrival_hour.minutes()) + ":00";

            t = moment.duration(moment.duration(route[index].steps[route[index].steps.length - 2].departure_time).asMinutes() - moment.duration(route[index].steps[1].departure_time).asMinutes() + parseInt(route[index].steps[0].duration) + parseInt(route[index].steps[route[index].steps.length - 1].duration), 'minutes');

        } else if (route[index].steps[0].departure_time === undefined) {


            departure_hour = moment.duration(route[index].steps[1].departure_time).subtract(parseInt(route[index].steps[0].duration), 'minutes');
            departure_hour = (departure_hour.hours().toString().length === 1 ? "0" + departure_hour.hours() : departure_hour.hours()) + ":" + (departure_hour.minutes().toString().length === 1 ? "0" + departure_hour.minutes() : departure_hour.minutes()) + ":00";
            arrival_hour = route[index].steps[route[index].steps.length - 1].departure_time;

            t = moment.duration(moment.duration(route[index].steps[route[index].steps.length - 1].departure_time).asMinutes() - moment.duration(route[index].steps[1].departure_time).asMinutes() + parseInt(route[index].steps[0].duration), 'minutes');
        } else if (route[index].steps[route[index].steps.length - 1].departure_time === undefined) {

            departure_hour = route[index].steps[0].departure_time;
            arrival_hour = moment.duration(route[index].steps[route[index].steps.length - 2].departure_time).add(parseInt(route[index].steps[route[index].steps.length - 1].duration), 'minutes');
            arrival_hour = (arrival_hour.hours().toString().length === 1 ? "0" + arrival_hour.hours() : arrival_hour.hours()) + ":" + (arrival_hour.minutes().toString().length === 1 ? "0" + arrival_hour.minutes() : arrival_hour.minutes()) + ":00";

            t = moment.duration(moment.duration(route[index].steps[route[index].steps.length - 2].departure_time).asMinutes() - moment.duration(route[index].steps[0].departure_time).asMinutes() + parseInt(route[index].steps[route[index].steps.length - 1].duration), 'minutes');

        } else {

            departure_hour = route[index].steps[0].departure_time;
            arrival_hour = route[index].steps[route[index].steps.length - 1].departure_time;

            t = moment.duration(moment.duration(route[index].steps[route[index].steps.length - 1].departure_time).asMinutes() - moment.duration(route[index].steps[0].departure_time).asMinutes(), 'minutes');
        }


        if (t.minutes().toString().length === 1) {


            route[index].route_duration = Math.abs(t.hours()) + "h0" + Math.abs(t.minutes());
            route[index].departure_hour = departure_hour;
            route[index].arrival_hour = arrival_hour;

            // return {
            //     route_duration: t.hours() + "h0" + t.minutes(),
            //     departure_hour: departure_hour,
            //     arrival_hour: arrival_hour
            // };

        } else {


            route[index].route_duration = Math.abs(t.hours()) + "h" + Math.abs(t.minutes());
            route[index].departure_hour = departure_hour;
            route[index].arrival_hour = arrival_hour;


            // return {
            //     route_duration: t.hours() + "h" + t.minutes(),
            //     departure_hour: departure_hour,
            //     arrival_hour: arrival_hour
            // };
        }
    } catch (error) {


        route[index].route_duration = "0h00";
        route[index].departure_hour = "";
        route[index].arrival_hour = "";

        // return {route_duration: "0h00", departure_hour:"", arrival_hour:""};

    }
}

/**
 *
 * @param route_price_detail
 * @param type_user
 * @param callback
 * @description Calcula os preços dos bilhetes da rota
 */
function calcPrice (route_price_detail, type_user, service_id, taxes,callback) {

    var asyncPrices = [];

    var ticket_price_total = 0;
    var ticket, exist_ticket = false;

    if (route_price_detail.length !== 0) {
    async.series([

        function(callback) {
            route_price_detail.forEach(function (detail, index, array) {

                asyncPrices.push(function (callback) {

                    search_route_database.route_price(type_user, detail.price , detail.route_id,function (price, error) {
                        if (!error) {

                            if (price.length !== 0) {

                                price[0].service_id = service_id;

                                if (taxes.length !== 0) {

                                    var tax_value = _.find(taxes, { 'id': price[0].tax_id}).tax_value;

                                    if ( tax_value!== undefined) {
                                        price[0].tax = tax_value;
                                    }

                                }
                            }

                            route_price_detail[index].price = price;
                            if (price.length !== 0) {
                               ticket = _.find(route_price_detail[index].price, { 'purchase_type': "TICKET"});
                               if (ticket !== undefined) {
                                  exist_ticket = true;
                                  ticket_price_total += parseFloat(ticket.value);
                               }
                            }
                            callback(null);
                        } else {
                            callback(error);
                        }
                    })
                });

                if (index === array.length - 1) {
                    callback();
                }

            });
        },
        function (callback) {


            async.parallel(asyncPrices, function (err) {

                if (!err) {
                    callback(null);
                } else {
                    callback(err);
                }
            });
        }

    ], function (error) {

        if (!error) {

            if (!exist_ticket) {
                ticket_price_total = null;
            }

            callback(route_price_detail, ticket_price_total, null);
        } else {
            callback([], null,error);
        }
    });

   } else {
       callback([], null,null);
   }

};

/**
 *
 * @param routes
 * @param type_user
 * @param arrivel_place
 * @param departure_place
 * @param callback
 * @description Calcula a duração e o preço da rota e atribui o local de destino e de origem
 */
exports.calculate = function (routes, type_user, arrivel_place, departure_place, date, taxes,callback) {

    var asyncRoutes = [];
    var tickets = [];
    var service_id = 0;
    async.series([

        function (callback) {

            configuration.get_configuration(function (configuration, error) {

                if (!error) {

                    if (configuration.length !== 0) {
                        service_id = configuration[0].service_id;
                    }
                    callback(null);
                } else {
                    callback(error);
                }
            });
        },
        function(callback) {
          if (routes.length !== 0) {

            routes.forEach(function (route, index, array) {

                asyncRoutes.push(function (callback) {

                  calcPrice(route.route_price_detail, type_user, service_id , taxes,function (price_detail, tickets_prices_total, error) {
                                     if (!error) {

                                         calcDuration(routes, index);

                                         var routes_lines = _.uniq(_.pull(_.map(routes[index].steps, 'route_line'), undefined));
                                         routes[index].stops = routes_lines.length - 1;
                                         routes[index].route_price_detail = price_detail;
                                         routes[index].arrival_place = arrivel_place;
                                         routes[index].departure_place = departure_place;
                                         routes[index].date = moment(date).utcOffset('+0100').format("YYYY-MM-DD");
                                         routes[index].tickets_total_prices = tickets_prices_total;

                                         for (var i = 0; i < price_detail.length; i++) {
                                             var ticket = _.find(price_detail[i].price, {'purchase_type': "TICKET"});
                                             if (ticket !== undefined) {
                                                 tickets.push({
                                                     prod_cod: ticket.prod_cod,
                                                     validate_date: routes[index].date,
                                                     departure_local: price_detail[i].departure_local,
                                                     arrival_local: price_detail[i].arrival_local
                                                 })
                                             }
                                         }

                                         routes[index].prod_compound_cod = crypto_prod.encrypt({tickets: tickets});
                                         callback(null);
                                     } else {
                                         callback(error);
                                     }
                                 })

                });

                if (index === array.length - 1) {
                    callback();
                }

            });
          } else {
              callback();
          }
        },
        function (callback) {

          if (routes.length !== 0) {
            async.parallel(asyncRoutes, function (err) {
                if (!err) {
                    callback(null);
                } else {
                    callback(err);
                }
            });
          } else {
              callback();
          }
        }

    ], function (error) {

        if (!error) {
            callback(null);
        } else {
            callback(error);
        }
    })
};

/**
 *
 * @param mode
 * @param origin
 * @param destination
 * @param callback
 * @description Determina o local de origem ou de destino ou ambos apartir de um ID
 */
exports.local_arrivel_and_departure = function (mode, origin, destination,callback){

    var errors = [];

    var departure_place, arrivel_place;

    if (mode === 0) {

        local.get_one_local(destination, function (local, error) {

            if (!error) {

                arrivel_place = local[0].local;
                callback(null, arrivel_place, errors);

            } else {
                errors.push({code: error.code, message: error.message})
                callback(null, null, errors);
            }

        })
    } else if (mode === 1){
        local.get_one_local(origin, function (local, error) {

            if (!error) {
                departure_place = local[0].local;
                    callback(departure_place, null, errors);
            } else {
                errors.push({code: error.code, message: error.message})
                callback(null, null, errors);
            }

        })
    } else {
        local.get_one_local(origin, function (local1, error1) {

            local.get_one_local(destination, function (local2, error2) {

                if (error1 !== null && error2 !== null) {
                    errors.push(error1);
                    errors.push(error2);
                    callback(null, null, errors);
                } else if (error1) {
                    errors.push(error1);
                    callback(null, null, errors);
                } else if (error2) {
                    errors.push(error2);
                    callback(null, null, errors);
                } else {
                    departure_place = local1[0].local;
                    arrivel_place = local2[0].local;

                    callback(departure_place, arrivel_place,errors);
                }
            })

        })
    }

}

/**
 *
 * @param local_id
 * @param type_day
 * @param callback
 * @description Detecta as rotas para um determinado destino
 */
exports.detect_route_one_local = function (local_id, type_day, callback) {

    var routes_aux  = [];

    search_route_database.detect_routes_by_one_local(local_id, type_day, function (routes, error) {

        if (!error) {

            for (var i = 0; i < routes.length; i++) {
                routes_aux.push([{route: routes[i], begin_local_id: 69}]);
            }

            callback(routes_aux, null);
        } else {
            callback([], error);
        }

    });

};

/**
 *
 * @param routes
 * @param callback
 * @description Adiciona o local de origem
 */
exports.set_begin_local = function (routes, callback) {

    try {

        for (var i = 0; i < routes.length; i++) {

            routes[i][0].begin_local_id = routes[i][0].locals_ids[0];
        }

        callback(null);
    } catch (e) {

        callback({code: e.code, message: e.message});
    }

};

/**
 *
 * @param routes
 * @param callback
 * @description Calcula só a duração da rota
 */
exports.calculateOnlyDuration = function (routes, callback) {

    var asyncRoutes = [];
    async.series([

        function(callback) {
            if (routes.length !== 0) {

                routes.forEach(function (route, index, array) {

                    asyncRoutes.push(function (callback) {

                                calcDuration(routes, index);
                                var routes_lines = _.uniq(_.pull(_.map(routes[index].steps, 'route_line'), undefined));
                                routes[index].stops = routes_lines.length -1;
                                callback(null);
                    });

                    if (index === array.length - 1) {
                        callback();
                    }

                });
            } else {
                callback();
            }
        },
        function (callback) {

            if (routes.length !== 0) {
                async.parallel(asyncRoutes, function (err) {
                    if (!err) {
                        callback(null);
                    } else {
                        callback(err);
                    }
                });
            } else {
                callback();
            }
        }

    ], function (error) {

        if (!error) {
            callback(null);
        } else {
            callback(error);
        }
    })
};


/**
 *
 * @param timeline
 * @param season
 * @returns {string}
 * @constructor
 * @description Calcula viagens com mais dias
 */
function AjustDuration (timeline,  season) {

    var value_to_compare, begin_datetime, duration = moment.duration(0), more_days = false;

    var date = moment(season);

    for (var k = 0; k < timeline.length; k++) {

        // Início
        if (k === 0) {

            value_to_compare = timeline[k];
            begin_datetime = timeline[k];

        } else {

            if (moment(value_to_compare, 'HH:mm:ss').isAfter(moment(timeline[k],'HH:mm:ss'))) {

                more_days = true;

                value_to_compare = timeline[k];
                begin_datetime = timeline[k];

                date.add(1 ,'d');

            } else {

                value_to_compare = timeline[k];
            }
        }

        if (k === timeline.length -1) {

            if (more_days) {

                var begin = moment(season +' '+timeline[0]);
                var end = moment(date.format('YYYY-MM-DD')+' '+timeline[timeline.length-1]);

                duration = moment.duration(begin.diff(end));

                if (duration.minutes().toString().length === 1) {

                    return Math.abs(duration.hours()) + "h0" + Math.abs(duration.minutes());

                } else {

                    return Math.abs(duration.hours()) + "h" + Math.abs(duration.minutes());
                }

                more_days = false;
            }
            timeline = [];
        }
    }
}


/**
 *
 * @param routes
 * @param callback
 * @description Formata a rota
 */
exports.routeFormatting = function (routes, season,callback) {

    var new_route = [], all_routes = [], routes_complete = [];
    var route_id = -1;
    var route_price, contacts, route_line, external;
    var stops = 0;

    var timeline = [], duration = null;

    try {

        for (var i = 0; i < routes.length; i++) {

            for (var j = 0; j < routes[i].steps.length; j++) {

                if (routes[i].steps[j].vehicle !== 'CAMINHADA') {
                    timeline.push(routes[i].steps[j].departure_time);
                }

                if (routes[i].steps[j].hasOwnProperty('route_id')) {

                    if (route_id === -1) {

                        route_id = routes[i].steps[j].route_id;
                        route_line = routes[i].steps[j].route_line;
                        external = routes[i].steps[j].external;
                        delete  routes[i].steps[j].external;
                        route_price = _.find(routes[i].route_price_detail, {'route_id': route_id});
                        contacts = routes[i].steps[j].contacts[0];
                        delete routes[i].steps[j].contacts;
                        new_route.push(routes[i].steps[j]);

                    } else {

                        if (route_id === routes[i].steps[j].route_id) {
                            delete routes[i].steps[j].contacts;
                            new_route.push(routes[i].steps[j]);

                            if (j === routes[i].steps.length - 1) {
                                all_routes.push({route_line: route_line,route_id: route_id, external: external,steps: new_route, prices: route_price, contacts: contacts});
                                new_route = [];
                            }

                        } else {

                            if (new_route.length !== 0) {
                                all_routes.push({route_line: route_line,route_id: route_id, external: external, steps: new_route, prices: route_price, contacts: contacts});
                                new_route = [];
                            }

                            stops++;
                            route_id = routes[i].steps[j].route_id;
                            route_line = routes[i].steps[j].route_line;
                            external = routes[i].steps[j].external;
                            delete  routes[i].steps[j].external;
                            route_price = _.find(routes[i].route_price_detail, {'route_id': route_id});
                            contacts = routes[i].steps[j].contacts[0];
                            delete routes[i].steps[j].contacts;
                            new_route.push(routes[i].steps[j]);

                        }
                    }
                } else {

                    if (new_route.length !== 0) {
                        all_routes.push({route_line: route_line, route_id: route_id, external: external, steps: new_route, prices: route_price, contacts: contacts});
                        new_route = [];
                    }
                    new_route.push(routes[i].steps[j]);
                    all_routes.push({steps: new_route});
                    new_route = [];
                }
            }

            duration = AjustDuration(timeline, season);

            timeline = [];

            routes_complete.push({
                route: all_routes,
                stops: stops,
                route_duration: duration === null? routes[i].route_duration : duration,
                departure_hour: routes[i].departure_hour,
                arrival_hour: routes[i].arrival_hour,
                arrival_place: routes[i].arrival_place,
                departure_place: routes[i].departure_place,
                date: routes[i].date,
                tickets_prices_total: routes[i].tickets_total_prices,
                prod_compound_cod: routes[i].prod_compound_cod
            });

            duration = null;
            // console.log(JSON.stringify(routes_complete));
            stops = 0;
            all_routes = [];
        }

        //console.log(route_timeline);

        callback(routes_complete, null);
    } catch (e) {
        callback([], {code: e.code, message: e.message})
    }

}