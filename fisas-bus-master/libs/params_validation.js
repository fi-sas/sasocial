
var _ = require('lodash');
var moment = require('moment');
var validator = require('validator');

"use strict";

/**
 *
 * @param numbers
 * @param strings
 * @param booleans
 * @param callback
 * @description Verifica o tipo dos parametros
 */
exports.params_validation = function(params, callback) {

    var errors = [];
    var params_aux = {};

    try {

        for (var i = 0; i < params.length; i++) {

            if (params[i].value === undefined) {

                if (params[i].required) {
                    errors.push({"code": 800, "message": "Invalid request missing " + params[i].param_name})
                } else {
                    params_aux[params[i].param_name] = params[i].default;
                }
            } else {
                if (typeof params[i].value !== params[i].type) {
                    errors.push({
                        "code": 450,
                        "message": "This " + params[i].value + " on " + params[i].param_name + " is not " + params[i].type + " type"
                    })
                } else {
                    if (params[i].type === "object") {
                        var r = array_items(params[i].items_types, params[i].value);
                        params_aux[params[i].param_name] = validation_array(r, errors);
                    } else {

                        if (params[i].sub_type === "string" || params[i].sub_type === "number" || params[i].sub_type === "boolean") {
                            params_aux[params[i].param_name] = params[i].value;
                        } else {

                            params_aux[params[i].param_name] = validate_sub_types(params[i], errors);
                        }

                    }
                }
            }
        }

        if (errors.length === 0) {
            errors = null;
        }

        callback(params_aux, errors);

    } catch (e) {
        callback(params_aux, {code: e.code, message: e.message});
    }
};


/**
 *
 * @param params
 * @param errors
 * @returns {Array}
 * @description Valida um array com objetos
 */
function validation_array(params, errors) {

    var params_aux = {};
    var items = [];

    for (var i = 0; i < params.length; i++) {
        for (var j = 0; j < params[i].length; j++) {
            if (params[i][j].value === undefined) {

                if (params[i][j].required) {
                    errors.push({"code": 800, "message": "Invalid request missing " + params[i][j].param_name})
                } else {
                    params_aux[params[i][j].param_name] = params[i][j].default;
                }
            } else {

                if (typeof params[i][j].value !== params[i][j].type) {
                    errors.push({
                        "code": 450,
                        "message": "This " + params[i][j].value + " on " + params[i][j].param_name + " is not " + params[i][j].type + " type"
                    })
                } else {


                    if (params[i][j].sub_type === "string" || params[i][j].sub_type === "number" || params[i][j].sub_type === "boolean") {
                        params_aux[params[i][j].param_name] = params[i][j].value;
                    } else {

                        params_aux[params[i][j].param_name] = validate_sub_types(params[i][j], errors);
                    }

                }
            }
        }

        items.push(params_aux);
        params_aux = {};
    }

    return items;
}


/**
 *
 * @param items_types
 * @param items
 * @returns {Array}
 * @description Prepara um array de objetos para serem validados
 */
function array_items(items_types, items) {

    var all_items = [];
    var items_aux = [];
    var obj;

    for (var j = 0 ; j < items.length; j++) {

        for (var k = 0; k < items_types.length;  k++) {

            obj = {
                "param_name" : items_types[k].param_name,
                "value": items[j][items_types[k].param_name],
                "required": items_types[k].required,
                "type": items_types[k].type,
                "sub_type":items_types[k].sub_type,
                "default": items_types[k].default
            }

            items_aux.push(obj);
        }
        all_items.push(items_aux);
        items_aux = [];
    }

    return all_items;
}

function validate_sub_types(param, errors) {

    var value = "";

    switch (param.sub_type) {
        case "date":
            var date = moment(param.value).format('YYYY-MM-DD');
            if (date !== "Invalid date") {
                value = date;
            } else {
                errors.push({"code": 250, "message": "Invalid date: " + param.value})
            }
            break;
        case "email":

            if (validator.isEmail(param.value)) {
                value = param.value;
            } else {
               errors.push({"code": 250, "message": "Invalid email: " + param.value})
            }

            break;
        case "link":

            if (validator.isURL(param.value,[{protocols: ['http','https']}])) {
                value = param.value;
            } else {
                errors.push({"code": 250, "message": "Invalid link: " + param.value})
            }
            break;
        case "positive":
            if (param.value < 0) {
                errors.push({"code": 250, "message": "Invalid value: " + param.value})
            } else {
                value = param.value;
            }
            break;
        case "phone":
            if (param.value.toString().length === 9) {
                value = parseInt(param.value);
            } else {
                errors.push({"code": 250, "message": "Invalid phone: " + param.value})
            }
            break;
        case "LatLong":

             if (validator.isLatLong(param.value)) {
                 value = param.value;
             } else {
                 errors.push({"code": 250, "message": "Invalid latitude/longitude: " + param.value})
             }
        case "hour":
            if (validator.matches(param.value,/([0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]/g)) {
                value = param.value;
            } else {

                if (validator.matches(param.value,/-/g)) {
                    value = null;
                } else {
                    errors.push({"code": 250, "message": "Invalid date: " + param.value})
                }
            }
    }

    return value;
}