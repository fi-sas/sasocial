var async   = require("async");
var applications_model = require('../models/applications_model');

/**
 * @param data
 * @param callback
 * @description Retorna as desitências das candidaturas
 */
exports.applicationsWithdrawal = function (data, callback) {

    var asyncWithdrawals  = [];

    async.series([

            function (callback) {

                data.forEach(function (dat, index, array) {

                    asyncWithdrawals.push(function (callback) {

                        applications_model.get_withdrawal_application(dat.id, function (withdrawal, error) {
                            if (!error) {
                                data[index].withdrawal = withdrawal;
                                callback(null)
                            } else {
                                data[index].withdrawal = {};
                                callback(null);
                            }
                        });
                    });
                    if (index === array.length - 1) {

                        callback();
                    }
                });
            },
            function (callback) {

                async.series(asyncWithdrawals, function (error) {

                    if (!error) {
                        callback(null);
                    } else {
                        callback(error);
                    }
                });
            }
        ],
        function (error) {

            if (!error) {
                callback(data, null);
            } else {
                callback([], error)
            }

        });
};