"use strict";

var log4js = require('log4js');
// var config = require('./config/config');
var config = require('config');
var categories = {
    default: { appenders: ['logstash'], level: 'info' },
};

categories[process.env.MICROSERVICE_IDENTIFIER] = { appenders: ['logstash'], level: 'info' };

// Log configurations
log4js.configure({
    appenders: {
        logstash: {
            type: 'log4js-logstash-tcp',
            host: config.get('LOGS_ENDPOINT.host'),
            port: config.get('LOGS_ENDPOINT.port'),
        },
    },
    categories,
});

module.exports =  log4js.getLogger(process.env.MICROSERVICE_IDENTIFIER);