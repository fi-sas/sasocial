"use strict";

var response  = require('../libs/config_response');

exports.filter_application = function(req, res, next) {

    try {

        let where = '';

        if (req.device !== 'BO') {
            where = ' WHERE user_id = '+ req.user.id;
        }
    
        if (req.query.name) {
            if (where !== '') {
                where += " and name LIKE  '%"+req.query.name+"%'";
            } else {
                where = " WHERE name LIKE '%"+req.query.name+"%'";
            }
        }
    
        if (req.query.student_number) {
            if (where !== '') {
                where += " and student_number LIKE  '%"+req.query.student_number+"%'";
            } else {
                where = " WHERE student_number LIKE '%"+req.query.student_number+"%'";
            }
        }
    
        if (req.query.identification) {
            if (where !== '') {
                where += " and identification LIKE  '%"+req.query.identification+"%'";
            } else {
                where = " WHERE identification LIKE '%"+req.query.identification+"%'";
            }
        }
    
        if (req.query.tin) {
            if (where !== '') {
                where += " and tin LIKE  '%"+req.query.tin+"%'";
            } else {
                where = " WHERE tin LIKE '%"+req.query.tin+"%'";
            }
        }

        req.where = where;
         
        next();
    } catch (e) {
        res.status(400).json(response.response_error([{"code": e.code, "message": e.message}]));
    }
}