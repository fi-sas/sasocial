var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');
var _ = require('lodash');

exports.get_courses = function(req, res, next) {

    var token = req.headers['authorization'];

    var courses = [];
    var coursesIDS = '';

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (!token) return res.status(401).send(response.no_provided());

    for (var i = 0; i < req.applications.length; i++) {

      if (i == 0) {
        coursesIDS += 'id=' + req.applications[i].course_id;
      } else {
        coursesIDS += '&id=' + req.applications[i].course_id;
      } 
      
    }

    var url = encodeURI(config.get('MS_CONG_ENDPOINT.url') + "courses?" + coursesIDS);
    request({
        headers: {
            'authorization': token
        },
        uri: url,
        method: 'GET'}, function (error, resp, json) {

        if (!error) {

          try {
              let courseFound;
              json = JSON.parse(json);

              if (json.errors.length === 0) {

                  courses = json.data;

                  for (var i = 0; i < req.applications.length; i++) {
                    courseFound = _.find(courses, ['id', req.applications[i].course_id]);
                    if (courseFound !== undefined) {
                        req.applications[i]['course'] = courseFound;
                    } else {
                        req.applications[i]['course'] = {};
                    }
                    
                  }

                  next();

                  /* res.status(200).json(response.response_success(req.links, req.applications,[])); */

              } else {
                  res.status(404).send(response.response_error(json.errors));
              }
          } catch (e) {
              res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
          }

        } else {
            res.status(500).send(response.response_error([{
                code: error.code,
                message: error.message
            }]));
        }
    });


};