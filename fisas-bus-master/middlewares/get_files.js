var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');
var _ = require('lodash');

exports.get_files = function(req, res, next) {

    var token = req.headers['authorization'];

    var files = [];
    var filesIDS = '';

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (!token) return res.status(401).send(response.no_provided());

    if (req.configurations[0].config !== null) {

        for (var i = 0; i < req.configurations.length; i++) {

            if (i == 0) {
                filesIDS += 'id=' + req.configurations[i].config.file_id;
            } else {
                filesIDS += '&id=' + req.configurations[i].config.file_id;
            } 
        
        }
    
        var url = encodeURI(config.get('MS_MEDIA_ENDPOINT.url') + "files?" + filesIDS);
        request({
            headers: {
                'authorization': token
            },
            uri: url,
            method: 'GET'}, function (error, resp, json) {

    
                
            if (!error) {

            try {
                let fileFound;
                json = JSON.parse(json);

                if (json.errors.length === 0) {

                    files = json.data;

                    for (var i = 0; i < req.configurations.length; i++) {
                        fileFound = _.find(files, ['id', req.configurations[i].config.file_id]);
                        if (fileFound !== undefined) {
                            req.configurations[i].config['file'] = fileFound;
                        } else {
                            req.configurations[i].config['file'] = {};
                        }
                        
                    }

                    /*  next(); */

                    res.status(200).json(response.response_success(req.links, req.configurations,[]));

                } else {
                    res.status(404).send(response.response_error(json.errors));
                }
            } catch (e) {
                res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
            }

            } else {
                res.status(500).send(response.response_error([{
                    code: error.code,
                    message: error.message
                }]));
            }
        });
    } else {
        res.status(200).json(response.response_success(req.links, req.configurations,[]));
    }

};