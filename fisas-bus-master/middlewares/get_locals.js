var response  = require('../libs/config_response');
var _ = require('lodash');
var locals_database = require('../models/back_office/local_model');

exports.get_locals = function(req, res, next) {

    var locals_ids = [];

    for (var i = 0; i < req.applications.length; i++) {
        locals_ids.push(req.applications[i].morning_local_id_from);
        locals_ids.push(req.applications[i].morning_local_id_to);

        locals_ids.push(req.applications[i].afternoon_local_id_from);
        locals_ids.push(req.applications[i].afternoon_local_id_to);
    }

    locals_database.get_locals_by_ids(locals_ids, function (locals, error) {

        if (!error) {

          try {

            let morningLocalFrom, morningLocalTo, afternoonLocalFrom, afternoonLocalTo;

            for (var i = 0; i < req.applications.length; i++) {

                morningLocalFrom = _.find(locals, ['id', req.applications[i].morning_local_id_from]);
                morningLocalTo = _.find(locals, ['id', req.applications[i].morning_local_id_to]);

                afternoonLocalFrom = _.find(locals, ['id', req.applications[i].afternoon_local_id_from]);
                afternoonLocalTo = _.find(locals, ['id', req.applications[i].afternoon_local_id_to]);

                req.applications[i]['morning_local_from'] = morningLocalFrom !== undefined? morningLocalFrom : {};
                req.applications[i]['morning_local_to'] = morningLocalTo !== undefined? morningLocalTo : {};

                req.applications[i]['afternoon_local_from'] = afternoonLocalFrom !== undefined? afternoonLocalFrom : {};
                req.applications[i]['afternoon_local_to'] = afternoonLocalTo !== undefined? afternoonLocalTo : {};
             
            }

            res.status(200).json(response.response_success(req.links, req.applications,[]));

            /* next(); */

            
          } catch (e) {
              res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
          }

        } else {
            res.status(400).send(response.response_error([{
                code: error.code,
                message: error.message
            }]));
        }
    });


};