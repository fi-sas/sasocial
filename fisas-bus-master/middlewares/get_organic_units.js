var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');
var _ = require('lodash');

exports.get_organic_units = function(req, res, next) {

    var token = req.headers['authorization'];

    var organic_units = [];
    /* var organicUnitsIDS = ''; */

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (!token) return res.status(401).send(response.no_provided());

  /*   for (var i = 0; i < req.applications.length; i++) {

        if (i == 0) {
            organicUnitsIDS += 'id=' + req.applications[i].organic_unit_id;
        } else {
            organicUnitsIDS += '&id=' + req.applications[i].organic_unit_id;
        } 
    } */

    var url = encodeURI(config.get('MS_INFRASTRUCTURE_ENDPOINT.url') + "organic-units?limit=-1&offset=0");
    request({
        headers: {
            'authorization': token
        },
        uri: url,
        method: 'GET'}, function (error, resp, json) {

        if (!error) {

          try {
            let organicUnitFound;
              json = JSON.parse(json);

              if (json.errors.length === 0) {

                organic_units = json.data;

                for (var i = 0; i < req.applications.length; i++) {

                    organicUnitFound = _.find(organic_units, ['id', req.applications[i].organic_unit_id]);
                    if (organicUnitFound !== undefined) {
                        req.applications[i]['organic_unit'] = organicUnitFound;
                    } else {
                        req.applications[i]['organic_unit'] = {};
                    }
                }

                /* res.status(200).json(response.response_success(req.links, req.applications,[])); */

                next();

              } else {
                  res.status(404).send(response.response_error(json.errors));
              }
          } catch (e) {
              res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
          }

        } else {
            res.status(500).send(response.response_error([{
                code: error.code,
                message: error.message
            }]));
        }
    });


};