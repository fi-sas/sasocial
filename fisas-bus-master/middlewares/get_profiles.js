var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');

exports.get_profiles = function(req, res, next) {

    var token = req.headers['authorization'];

    var profiles = [];

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (!token) return res.status(401).send(response.no_provided());

    var url = encodeURI(config.get('MS_AUTH_ENDPOINT.url') + "profiles?limit=-1&offset=0");
    request({
        headers: {
            'authorization': token
        },
        uri: url,
        method: 'GET'}, function (error, resp, json) {

        if (!error) {

          try {
              json = JSON.parse(json);

              if (json.errors.length === 0) {

                  for (var i = 0; i < json.data.length; i++) {
                      profiles.push({id: json.data[i].id,
                                     profile: json.data[i].name});
                  }

                  req.profiles = profiles;
                  next();

              } else {

                  res.status(404).send(response.response_error(json.errors));
              }
          } catch (e) {
              res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
          }

        } else {
            res.status(500).send(response.response_error([{
                code: error.code,
                message: error.message
            }]));
        }
    });


};