var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');
var _ = require('lodash');

exports.get_schools = function(req, res, next) {

    var token = req.headers['authorization'];

    var schools = [];
    var schoolsIDS = '';

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (!token) return res.status(401).send(response.no_provided());

    for (var i = 0; i < req.applications.length; i++) {

      if (i == 0) {
        schoolsIDS += 'id=' + req.applications[i].school_id;
      } else {
        schoolsIDS += '&id=' + req.applications[i].school_id;
      } 
      
    }

    var url = encodeURI(config.get('MS_CONG_ENDPOINT.url') + "schools?" + schoolsIDS);
    request({
        headers: {
            'authorization': token
        },
        uri: url,
        method: 'GET'}, function (error, resp, json) {

        if (!error) {

          try {
              let schoolFound;
              json = JSON.parse(json);

              if (json.errors.length === 0) {

                  schools = json.data;

                  for (var i = 0; i < req.applications.length; i++) {
                    schoolFound = _.find(schools, ['id', req.applications[i].school_id]);
                    if (schoolFound !== undefined) {
                        req.applications[i]['school'] = schoolFound;
                    } else {
                        req.applications[i]['school'] = {};
                    }
                    
                  }

                  next();

                  /* res.status(200).json(response.response_success(req.links, req.applications,[])); */

              } else {
                  res.status(404).send(response.response_error(json.errors));
              }
          } catch (e) {
              res.status(500).send(response.response_error([{code: e.code, message: e.message }]));
          }

        } else {
            res.status(500).send(response.response_error([{
                code: error.code,
                message: error.message
            }]));
        }
    });


};