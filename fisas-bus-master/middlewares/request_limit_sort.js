"use strict";

var response  = require('../libs/config_response');

exports.limit_and_sort = function(req, res, next) {

    try {

        var limit = req.query.limit;
        var offset = req.query.offset;

        req.withRelated = req.query.withRelated !== undefined ? req.query.withRelated : '';

        var errors = [];

        var sort = req.query.sort;

        if (sort !== undefined) {

            if (sort.charAt(0) === '-') {
                req.sort = "desc";
                req.sortProp = sort.slice(1);
            } else {
                req.sort = "asc";
                req.sortProp = sort;
            }

        } else {

            req.sort = "asc";
            req.sortProp = "id";
        }

        if (limit === undefined) {

            req.limit = 10;

        } else {

            limit = parseInt(limit);
            if (limit === NaN) {
                errors.push({
                    "code": 100,
                    "message": "Limit is not a number."
                })
            } else {

                if (limit < -1) {
                    errors.push({"code": 250, "message": "limit "+limit+ " is invalid"});
                } else {
                    req.limit = limit
                }
            }

            // } else {
            //     isPositive(limit) ? req.limit = limit : errors.push({"code": 200, "message": "limit is negative"});
            //
            // }
        }

        if (offset === undefined) {

            req.offset = 0;

        } else {

            offset = parseInt(offset);
            if (offset === NaN) {
                errors.push({
                    "code": 100,
                    "message": "Offset is not a number."
                })
            } else {
                isPositive(offset) ? req.offset = offset : errors.push({"code": 200, "message": "Offset is negative"});
            }
        }
        if (errors.length === 0) {
            next();
        } else {
            res.status(400).json(response.response_error(errors));
        }
    } catch (e) {
        console.log("Error: ", e);
        res.status(400).json(response.response_error([{"code": e.code, "message": e.message}]));
    }
}

function isPositive(x)
{
    if (x >= 0) {
        return true;
    } else {
        return false;
    }
}