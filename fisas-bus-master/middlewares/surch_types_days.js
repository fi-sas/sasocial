"use strict";
var moment  = require('moment');
var extra_functions  = require('../models/extra_functions');
var response = require('../libs/config_response');

/**
 *
 * 
 * @param req 
 * @param res 
 * @param next 
 * @description Verifica os tipos de dia
 */
exports.surch_types_days = function(req, res, next){
    
    var date = req.query.date;

    if (req.query.date === null || req.query.date === undefined) {

      date = moment().format("YYYY-MM-DD");
    }

    var day = moment(date).day();

    if (!isNaN(day)) {

        extra_functions.surch_types_days(day, function (types_days, error) {
            if (error) {
                res.status(500).json(response.response_error(error));
            } else {
                req.types_days = types_days;
                next();
            }
        });
    } else {
        res.status(400).json(response.response_error([{"code": 250, "message": "Invalid date"}]));
    }
  }