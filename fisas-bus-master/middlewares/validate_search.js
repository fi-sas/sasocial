"use strict";

exports.validate_search = function(req, res, next) {

    if (req.query.destination !== undefined) {

        if (!isNaN(req.query.destination)) {
                next();
        } else {

            res.status(400).json(response.response_error([{
                "code": 450, "message": "This destination is not a id number!"
            }]));

        }
    } else {
        res.status(400).json(response.response_error([{code:350, message:"Invalid search."}]));
    }

};