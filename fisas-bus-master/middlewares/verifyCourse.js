"use strict";

var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');


exports.verifyCourse = function(req, res, next) {

    var token = req.headers['authorization'];

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    var course_id = req.body.course_id;

    if ( course_id !== undefined) {

        if (!token) return res.status(401).send(response.no_provided());

        var url = encodeURI(config.get('MS_CONG_ENDPOINT.url') + "courses/" + course_id);
        request({
            headers: {
                'authorization': token
            },
            uri: url,
            method: 'GET'}, function (error, resp, json) {

            if (!error) {

                json = JSON.parse(json);

                if (json.errors.length === 0) {

                    next();

                } else {
                    res.status(404).send(response.response_error(json.errors));
                }

            } else {
                res.status(500).send(response.response_error([{
                    code: error.code,
                    message: error.message
                }]));
            }
        });
    } else {
        res.status(500).send(response.response_error([{
            code: 20000,
            message: 'Course id undefined!'
        }]));
    }

};


