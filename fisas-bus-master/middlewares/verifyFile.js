"use strict";

var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');


exports.verifyFile = function(req, res, next) {

    var token = req.headers['authorization'];

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    if (req.body.config !== null || req.body.config !== null) {

    var file_id = req.body.config.file_id;

    if ( file_id !== undefined) {

        if (!token) return res.status(401).send(response.no_provided());

        var url = encodeURI(config.get('MS_MEDIA_ENDPOINT.url') + "files/" + file_id);
        request({
            headers: {
                'authorization': token
            },
            uri: url,
            method: 'GET'}, function (error, resp, json) {

            if (!error) {

                json = JSON.parse(json);

                if (json.errors.length === 0) {

                    next();

                } else {
                    res.status(404).send(response.response_error(json.errors));
                }

            } else {
                res.status(500).send(response.response_error([{
                    code: error.code,
                    message: error.message
                }]));
            }
        });
    } else {
        res.status(500).send(response.response_error([{
            code: 20000,
            message: 'File id undefined!'
        }]));
    }
   } else {
    res.status(500).send(response.response_error([{
        code: 20000,
        message: 'Config is empty!'
    }]));
   }

};


