"use strict";

var request = require('request');
var config = require('config');
var response  = require('../libs/config_response');


exports.verifyOrganicUnit = function(req, res, next) {

    var token = req.headers['authorization'];

    if (token !== undefined) {

        token = token.replace('Bearer ', '');
    }

    var organic_unit_id = req.body.organic_unit_id;


    if ( organic_unit_id !== undefined) {

        if (!token) return res.status(401).send(response.no_provided());

        var url = encodeURI(config.get('MS_INFRASTRUCTURE_ENDPOINT.url') + "organic-units/" + organic_unit_id);
        request({
            headers: {
                'authorization': token
            },
            uri: url,
            method: 'GET'}, function (error, resp, json) {

            if (!error) {

                json = JSON.parse(json);

                if (json.errors.length === 0) {

                    req.profileInfo = json.data[0];
                    next();

                } else {
                    res.status(404).send(response.response_error(json.errors));
                }

            } else {
                res.status(500).send(response.response_error([{
                    code: error.code,
                    message: error.message
                }]));
            }
        });
    } else {
        res.status(500).send(response.response_error([{
            code: 20000,
            message: 'Organic Unit id undefined!'
        }]));
    }

};


