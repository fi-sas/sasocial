"use strict";

var response  = require('../libs/config_response');

exports.verifyScopes = function (req, res, next) {

           var disabledScopes = true;

           if (disabledScopes) {
            next();
           } else {

            var permission, permissionAll;

            if (req.scopes.length !== 0) {

                var path = req.path.split("/");

                permissionAll = "bus:" + path[3];
                permission = "bus:" + path[3] + ":" + convertMethod(req.method);

                if (req.scopes.includes(permissionAll)) {
                    next();
                } else if (req.scopes.includes(permissionAll)) {

                    next();
                } else {

                    res.status(500).send(response.response_error([{
                        code: 600,
                        message: "The token provided does not have permission to execute the requested action"
                    }]));
                }

            } else {
                res.status(500).send(response.response_error([{
                    code: 600,
                    message: "The token provided does not have permission to execute the requested action"
                }]));
            }

        }

}

function convertMethod(method) {
    switch (method) {
        case 'POST':
            return 'create';
        case 'DELETE':
            return 'delete';
        case 'PUT':
        case 'PATCH':
            return 'update';
        default:
            return 'read';
    }
}