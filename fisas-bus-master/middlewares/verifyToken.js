"use strict";

var config = require('config');
var response  = require('../libs/config_response');
var request = require('request');
var jwt = require('jsonwebtoken');

/**
 *
 * @param req
 * @param res
 * @param next
 * @description Verifica se o token é válido e guarda a informação do payload
 */
exports.verifyToken = function(req, res, next) {

          var token = req.headers['authorization'];

          if (token !== undefined) {

              token = token.replace('Bearer ', '');
          }

          var url = encodeURI(config.get('MS_AUTH_ENDPOINT.url') + "authorize/validate-token/" + token);

          if (!token) return res.status(401).send(response.no_provided());

          request(url, function (error, resp, json) {

              if (error) return res.status(500).send(response.response_error([{
                  "code": 500,
                  "message": "Failed to connect authorization microservice"
              }]));

              json = JSON.parse(json);

              if (json.status === "error") return res.status(400).send(response.response_error(json.errors));

              try {

                  if (json.data.length !== 0) {

                      var tokenDecoded = jwt.verify(token, config.get('MS_AUTH_ENDPOINT.secret'));

                      req.device = tokenDecoded.device.type? tokenDecoded.device.type : null;
                      
                      req.backoffice = true;
                      req.user = json.data[0].user;
                      req.scopes = json.data[0].scopes;

                      if (json.data[0].user.profile_id !== null) {
                          req.profile_id = json.data[0].user.profile_id;
                      } else {
                          req.profile_id = 0;
                      }
                  } else {
                      req.user = {id: 0, user_name: "device"};
                      req.scopes = [];
                      req.profile_id = 0;
                  }

                  next();

              } catch (e) {

                  res.status(400).send(response.response_error([{
                      code: 600,
                      message: "The token provided does not have permission to execute the requested action"
                  }]));
              }

          });
}
