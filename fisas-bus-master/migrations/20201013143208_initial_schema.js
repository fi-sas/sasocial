module.exports.up = async (db) => {

	return db.schema
		// Locals database table
		.createTable("locals", function (table) {
			table.increments();
			table.string("local", 120).notNullable();
			table.string("region", 120).notNullable();
			table.double("latitude");
			table.double("longitude");
			table.boolean("active").notNullable().defaultTo(false);
			table.boolean("institute").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("places", function (table) {
			table.increments();
			table.integer("cod_district").unsigned();
			table.integer("cod_place").unsigned();
			table.string("place", 120).notNullable();
		})
		// Routes database table
		.createTable("routes", function (table) {
			table.increments();
			table.string("name", 120).notNullable();
			table.integer("contact_id").unsigned();
			table.boolean("external").notNullable().defaultTo(false);
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("route_order", function (table) {
			table.increments();
			table.integer("local_id").unsigned().references("id").inTable("locals");
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.integer("order_table").unsigned();
			table.string("instruction", 120).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		// Days Weeks database table
		.createTable("days_weeks", function (table) {
			table.increments();
			table.string("day", 120).notNullable();
		})
		.createTable("types_days", function (table) {
			table.increments();
			table.string("name", 120).notNullable();
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("days_group", function (table) {
			table.increments();
			table.integer("day_id").unsigned().references("id").inTable("days_weeks");
			table.integer("type_day_id").unsigned().references("id").inTable("types_days");
			table.unique(["day_id", "type_day_id"]);
		})
		.createTable("zones", function (table) {
			table.increments();
			table.string("name", 120).notNullable();
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("local_zone", function (table) {
			table.increments();
			table.integer("local_id").unsigned().references("id").inTable("locals");
			table.integer("zone_id").unsigned().references("id").inTable("zones");
			table.unique(["local_id", "zone_id"]);
		})
		.createTable("distances", function (table) {
			table.increments();
			table.integer("local1_id").unsigned().references("id").inTable("locals");
			table.integer("local2_id").unsigned().references("id").inTable("locals");
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.double("distance");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("links", function (table) {
			table.increments();
			table.integer("route1_id").unsigned().references("id").inTable("routes");
			table.integer("local_route1_id").unsigned().references("id").inTable("locals");
			table.integer("route2_id").unsigned().references("id").inTable("routes");
			table.integer("local_route2_id").unsigned().references("id").inTable("locals");
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("applications", function (table) {
			table.increments();
			table.string("name", 120).notNullable();
			table.string("student_number", 120).notNullable();
			table.datetime("birth_date").notNullable();
			table.string("tin", 120).notNullable();
			table.string("identification", 120).notNullable();
			table.string("nationality", 120).notNullable();
			table.datetime("date");
			table.string("email", 120).notNullable();
			table.string("phone_1", 120).notNullable();
			table.string("phone_2", 120).notNullable();
			table.integer("course_year").unsigned();
			table.integer("course_id").unsigned();
			table.boolean("renovation").notNullable().defaultTo(false);
			table.integer("school_id").unsigned();
			table.string("observations");
			table.boolean("accept_procedure").notNullable().defaultTo(false);
			table.integer("morning_local_id_from").unsigned().references("id").inTable("locals");
			table.integer("morning_local_id_to").unsigned().references("id").inTable("locals");
			table.integer("afternoon_local_id_from").unsigned().references("id").inTable("locals");
			table.integer("afternoon_local_id_to").unsigned().references("id").inTable("locals");
			table.enu("status", ["submitted", "accepted", "rejected", "withdrawal", "closed", "canceled"]);
			table.integer("user_id").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("application_history", function (table) {
			table.increments();
			table.integer("application_id").unsigned();
			table.integer("user_id").unsigned();
			table.enu("status", ["submitted", "accepted", "rejected", "withdrawal", "closed", "canceled"]);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("configurations", (table) => {
			table.increments();
			table.string("key", 120).notNullable();
			table.string("value", 250);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("key");
		})

		.createTable("search_logs", function (table) {
			table.increments();
			table.string("origin", 120).notNullable();
			table.string("destination", 120).notNullable();
			table.datetime("date").notNullable();
			table.boolean("error").notNullable().defaultTo(false);
			table.integer("result_count").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("payment_months", function (table) {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("applications");
			table.string("month", 120).notNullable();
			table.integer("year").notNullable();
			table.decimal("value", 8, 2).notNullable();
			table.boolean("paid").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("seasons", function (table) {
			table.increments();
			table.string("name", 120).notNullable();
			table.datetime("date_begin").notNullable();
			table.datetime("date_end").notNullable();
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("price_tables", function(table) {
			table.increments();
			table.integer("profile_id").unsigned();
			table.integer("tax_id").unsigned();
			table.integer("account_id").unsigned();
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.enu("ticket_type", ["TICKET", "PASS"]);
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("prices", function (table) {
			table.increments();
			table.integer("number_stop").unsigned();
			table.decimal("value", 8, 2).notNullable();
			table.string("description", 120).notNullable();
			table.integer("price_table_id").unsigned().references("id").inTable("price_tables");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("tickets_bought", function (table) {
			table.increments();
			table.integer("price_id").unsigned().references("id").inTable("prices");
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.integer("departure_local_id").unsigned().references("id").inTable("locals");
			table.integer("arrival_local_id").unsigned().references("id").inTable("locals");
			table.integer("user_id").unsigned();
			table.datetime("validate_date");
			table.boolean("used").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("route_ticket", function (table) {
			table.increments();
			table.integer("ticket_bought_id").unsigned().references("id").inTable("tickets_bought");
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.unique(["ticket_bought_id", "route_id"]);
		})
		.createTable("ticket_config", function (table) {
			table.increments();
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.integer("max_to_buy").unsigned();
			table.boolean("active").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
		})
		.createTable("withdrawals", function (table) {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("applications");
			table.string("justification", 255).notNullable();
			table.datetime("date_withdrawal").notNullable();
			table.enu("status", ["pending", "accepted", "rejected"]);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("withdrawals_history", function (table) {
			table.increments();
			table.integer("withdrawal_id").unsigned().references("id").inTable("withdrawals");
			table.integer("user_id").unsigned();
			table.enu("status", ["pending", "accepted", "rejected"]);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("timetables", function (table) {
			table.increments();
			table.integer("type_day_id").unsigned().references("id").inTable("types_days");
			table.integer("route_order_id").unsigned().references("id").inTable("route_order");
			table.integer("route_id").unsigned().references("id").inTable("routes");
			table.integer("season_id").unsigned().references("id").inTable("seasons");
			table.boolean("active").notNullable().defaultTo(false);
		})
		.createTable("hours", function (table) {
			table.increments();
			table.integer("local_id").unsigned().references("id").inTable("locals");
			table.integer("timetable_id").unsigned().references("id").inTable("timetables");
			table.integer("route_order_id").unsigned().references("id").inTable("route_order");
			table.integer("line_number").unsigned();
			table.time("hour").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});
};

module.exports.down = async db => db.schema
	.dropTable("hours")
	.dropTable("timetables")
	.dropTable("route_order")
	.dropTable("links")
	.dropTable("days_group")
	.dropTable("withdrawals")
	.dropTable("withdrawals_history")
	.dropTable("payment_months")
	.dropTable("application_history")
	.dropTable("applications")
	.dropTable("local_zone")
	.dropTable("route_ticket")
	.dropTable("tickets_bought")
	.dropTable("prices")
	.dropTable("price_tables")
	.dropTable("ticket_config")
	.dropTable("distances")
	.dropTable("locals")
	.dropTable("places")
	.dropTable("routes")
	.dropTable("days_weeks")
	.dropTable("types_days")
	.dropTable("zones")
	.dropTable("configurations")
	.dropTable("search_logs")
	.dropTable("seasons")
	;

module.exports.configuration = { transaction: true };
