module.exports.up = (db) =>
	db.schema
		.table("applications", (table) => {
			table.integer("document_type_id");
		});

module.exports.down = (db) =>
	db.schema
		.table("applications", (table) => {
			table.dropColumn("document_type_id");
		});

module.exports.configuration = { transaction: true };
