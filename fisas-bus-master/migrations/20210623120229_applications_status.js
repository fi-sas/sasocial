module.exports.up = (db) =>
	db.schema
		.raw(`
			ALTER TABLE "applications"
			DROP CONSTRAINT "applications_status_check",
			ADD CONSTRAINT "applications_status_check"
			CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting'))
		  `)
		.raw(`
			ALTER TABLE "application_history"
			DROP CONSTRAINT "application_history_status_check",
			ADD CONSTRAINT "application_history_status_check"
			CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting'))
		  `);

module.exports.down = (db) =>
	db.schema
		.raw(`
	ALTER TABLE "applications"
	DROP CONSTRAINT "applications_status_check",
	ADD CONSTRAINT "applications_status_check"
	CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled'))
  `)
		.raw(`
	  ALTER TABLE "application_history"
	  DROP CONSTRAINT "application_history_status_check",
	  ADD CONSTRAINT "application_history_status_check"
	  CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled'))
	`);

module.exports.configuration = { transaction: true };
