module.exports.up = (db) => {

	return db.schema
		.createTable("sub23_declarations", function (table) {
			table.increments();
			table.string("academic_year", 10).notNullable();
			table.string("name", 120).notNullable();
			table.string("address", 255).notNullable();
			table.string("postal_code", 15).notNullable();
			table.string("country", 255).notNullable();
			table.integer("document_type_id").notNullable();
			table.string("identification", 120).notNullable();
			table.datetime("birth_date").notNullable();
			table.integer("organic_unit_id").notNullable();
			table.integer("course_id").notNullable();
			table.integer("course_year").notNullable();
			table.boolean("has_social_scholarship").notNullable().defaultTo(false);
			table.integer("declaration_id");
			table.string("observations");
			table.string("locality").notNullable();
			table.boolean("accept_procedure").notNullable();
			table.boolean("applied_social_scholarship").notNullable().defaultTo(false);
			table.enu("status", ["submitted", "emitted", "rejected"]).defaultTo("submitted");
			table.integer("user_id").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("sub23_declarations_history", function (table) {
			table.increments();
			table.integer("sub23_declarations_id").references("id").inTable("sub23_declarations");
			table.integer("user_id").unsigned();
			table.enu("status", ["submitted", "emitted", "rejected"]).defaultTo("submitted");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});
};

module.exports.down = db => db.schema
	.dropTable("sub23_declarations_history")
	.dropTable("sub23_declarations");

module.exports.configuration = { transaction: true };
