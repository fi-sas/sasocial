
exports.up = (db) => {
    return db.schema
        .createTable("absence", (table) => {
            table.increments();
            table.datetime("start_date").notNullable();
            table.datetime("end_date").notNullable();
            table.string("justification");
            table.integer("application_id").references("id").inTable("applications");
            table.integer("user_id").unsigned().notNullable();
            table.integer("file_id").unsigned();
            table.enu("status", ["submitted", "approved", "rejected", "ongoing", "completed"]).defaultTo("submitted");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .raw(`
        ALTER TABLE "applications"
        DROP CONSTRAINT "applications_status_check",
        ADD CONSTRAINT "applications_status_check"
        CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting','suspended'))
      `)
        .raw(`
        ALTER TABLE "application_history"
        DROP CONSTRAINT "application_history_status_check",
        ADD CONSTRAINT "application_history_status_check"
        CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting','suspended'))
      `)
};

exports.down = (db) => {
    return db.schema
        .dropTable("absence")
        .raw(`
    ALTER TABLE "applications"
    DROP CONSTRAINT "applications_status_check",
    ADD CONSTRAINT "applications_status_check"
    CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting'))
  `)
        .raw(`
    ALTER TABLE "application_history"
    DROP CONSTRAINT "application_history_status_check",
    ADD CONSTRAINT "application_history_status_check"
    CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting'))
  `);
};
