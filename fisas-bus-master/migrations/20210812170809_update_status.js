
module.exports.up = (db) =>
    db.schema
        .raw(`
        ALTER TABLE "applications"
        DROP CONSTRAINT "applications_status_check",
        ADD CONSTRAINT "applications_status_check"
        CHECK (status IN ('submitted', 'accepted', 'rejected', 'approved', 'not_approved', 'withdrawal', 'closed', 'canceled', 'quiting', 'suspended'))
      `)
        .raw(`
        ALTER TABLE "application_history"
        DROP CONSTRAINT "application_history_status_check",
        ADD CONSTRAINT "application_history_status_check"
        CHECK (status IN ('submitted', 'accepted', 'rejected', 'approved', 'not_approved', 'withdrawal', 'closed', 'canceled', 'quiting', 'suspended'))
      `);

module.exports.down = (db) =>
    db.schema
        .raw(`
ALTER TABLE "applications"
DROP CONSTRAINT "applications_status_check",
ADD CONSTRAINT "applications_status_check"
CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting', 'suspended'))
`)
        .raw(`
  ALTER TABLE "application_history"
  DROP CONSTRAINT "application_history_status_check",
  ADD CONSTRAINT "application_history_status_check"
  CHECK (status IN ('submitted', 'accepted', 'rejected', 'withdrawal', 'closed', 'canceled', 'quiting', 'suspended'))
`);

module.exports.configuration = { transaction: true };
