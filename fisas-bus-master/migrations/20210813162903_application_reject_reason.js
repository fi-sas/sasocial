
exports.up = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.string("reject_reason", 255).nullable();
        })
};

exports.down = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.dropColumn("reject_reason");
        })
};
