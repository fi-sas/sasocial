

module.exports.up = (db) =>
db.schema
    .raw(`
    ALTER TABLE "sub23_declarations"
    DROP CONSTRAINT "sub23_declarations_status_check",
    ADD CONSTRAINT "sub23_declarations_status_check"
    CHECK (status IN ('submitted', 'validated', 'emitted', 'rejected'))
  `)
    .raw(`
    ALTER TABLE "sub23_declarations_history"
    DROP CONSTRAINT "sub23_declarations_history_status_check",
    ADD CONSTRAINT "sub23_declarations_history_status_check"
    CHECK (status IN ('submitted', 'validated', 'emitted', 'rejected'))
  `);

module.exports.down = (db) =>
db.schema
.raw(`
ALTER TABLE "sub23_declarations"
DROP CONSTRAINT "sub23_declarations_status_check",
ADD CONSTRAINT "sub23_declarations_status_check"
CHECK (status IN ('submitted', 'emitted', 'rejected'))
`)
.raw(`
ALTER TABLE "sub23_declarations_history"
DROP CONSTRAINT "sub23_declarations_history_status_check",
ADD CONSTRAINT "sub23_declarations_history_status_check"
CHECK (status IN ('submitted', 'emitted', 'rejected'))
`);

module.exports.configuration = { transaction: true };