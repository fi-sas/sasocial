

exports.up = (db) => {
    return db.schema
        .alterTable("sub23_declarations", (table) => {
            table.dropColumn("accept_procedure");
        })
};

exports.down = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.boolean("accept_procedure").notNullable();
        })
};
