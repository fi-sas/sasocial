

exports.up = (db) => {
    return db.schema
        .alterTable("sub23_declarations", (table) => {
            table.string("reject_reason").nullable();
        })
};

exports.down = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.dropColumn("reject_reason");
        })
};
