
exports.up = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.string("academic_year").nullable();
        })
};

exports.down = (db) => {
    return db.schema
        .alterTable("applications", (table) => {
            table.dropColumn("academic_year");
        })
};
