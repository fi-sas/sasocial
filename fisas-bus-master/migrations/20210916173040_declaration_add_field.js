
exports.up = (db) => {
    return db.schema
        .alterTable("sub23_declarations", (table) => {
            table.string("cc_emitted_in").nullable();
        })
};

exports.down = (db) => {
    return db.schema
        .alterTable("sub23_declarations", (table) => {
            table.dropColumn("cc_emitted_in");
        })
};
