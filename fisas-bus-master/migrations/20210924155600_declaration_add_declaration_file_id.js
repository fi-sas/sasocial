exports.up = (db) => {
	return db.schema.alterTable("applications", (table) => {
		table.integer("declaration_file_id").unsigned();
	});
};

exports.down = (db) => {
	return db.schema.alterTable("applications", (table) => {
		table.dropColumn("declaration_file_id");
	});
};
