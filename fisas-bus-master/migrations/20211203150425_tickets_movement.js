module.exports.up = async (db) =>
	db.schema.alterTable("tickets_bought", (table) => {
		table.string("movement_id").nullable();
		table.boolean("has_canceled").notNullable();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("tickets_bought", (table) => {
		table.dropColumn("movement_id");
		table.dropColumn("has_canceled");
	});

module.exports.configuration = { transaction: true };
