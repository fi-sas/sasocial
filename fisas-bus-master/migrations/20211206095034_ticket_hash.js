module.exports.up = (db) => {
	return db.schema.createTable("ticket_hash", function (table) {
		table.increments();
		table.string("hash");
		table.integer("price_id").unsigned().references("id").inTable("prices");
		table.integer("departure_local_id").unsigned().references("id").inTable("locals");
		table.integer("arrival_local_id").unsigned().references("id").inTable("locals");
	});
};

module.exports.down = (db) => db.schema.dropTable("ticket_hash");

module.exports.configuration = { transaction: true };
