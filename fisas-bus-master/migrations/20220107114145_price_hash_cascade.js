exports.up = function (knex) {
	return knex.schema.alterTable("ticket_hash", (table) => {
		table.dropForeign("price_id");

		table.foreign("price_id").references("prices.id").onDelete("CASCADE");
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("ticket_hash", (table) => {
		table.dropForeign("price_id");

		table.foreign("price_id").references("prices.id").onDelete("NO ACTION");
	});
};
