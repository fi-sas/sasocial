exports.up = (db) => {
	return db.schema.alterTable("routes", (table) => {
		table.boolean("represents_connection").notNullable().defaultTo(false);
	});
};

exports.down = (db) => {
	return db.schema.alterTable("routes", (table) => {
		table.dropColumn("represents_connection");
	});
};
