'use strict';

var mysql_connection = require('./connection');
var moment = require('moment');
var withRelated_func = require('../libs/withRelated')
/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param where
 * @param callback
 * @description Mostras os locais
 */
exports.get_applications = function(limit, offset, columnOrder, order_by, where, withRelated, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM applications",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("SELECT * FROM applications "+ where +" ORDER BY " + columnOrder + " " + order_by+" "+ limit_offset, function (error, results, fields) {
                if (!error) {

                    connection.end();

                    if (withRelated === 'withdrawal') {
                      withRelated_func.applicationsWithdrawal(results, function(r , error) {
                        callback(r, total[0].total, error);
                      });
                    } else {
                       callback(results, total[0].total, error);
                    }

                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param application_id
 * @param where_user
 * @param callback
 * @description Mostra uma candidatura
 */
exports.get_one_application = function(application_id, where_user, callback) {

    var connection = mysql_connection.conncection();

    connection.query("SELECT * FROM applications where id = "+application_id +" "+ where_user, function (error, results, fields) {
 
      try {
          if (!error) {
  
              if (results.length !== 0) {
                  connection.end();
                  callback(results[0], error);
              } else {
                  connection.end();dd
                  callback(null, {'code': '400', 'message': "Application don't exist"});
              }
          } else {
              connection.end();
              callback(null, {'code': error.code, 'message': error.message});
          }
        } catch(e) {
          connection.end();
          callback(null, {'code': error.code, 'message': e.message});
        }
     });
 
 };

 /**
  * 
  * @param {*} name 
  * @param {*} birth_date 
  * @param {*} tin 
  * @param {*} identification 
  * @param {*} nationality 
  * @param {*} course_year 
  * @param {*} date 
  * @param {*} email 
  * @param {*} phone_1 
  * @param {*} phone_2 
  * @param {*} course_id 
  * @param {*} renovation 
  * @param {*} school_id
  * @param {*} observations 
  * @param {*} accept_procedure 
  * @param {*} morning_local_id_from 
  * @param {*} morning_local_id_to 
  * @param {*} afternoon_local_id_from 
  * @param {*} afternoon_local_id_to
  * @param {*} student_number
  * @param {*} user_id 
  * @param {*} status 
  * @param {*} callback 
  * @description Insere uma candidatura
  */
exports.insert_application = function (
  name,
  birth_date,
  tin,
  identification,
  nationality,
  course_year,
  date,
  email,
  phone_1,
  phone_2,
  course_id,
  renovation,
  school_id,
  observations,
  accept_procedure,
  morning_local_id_from,
  morning_local_id_to,
  afternoon_local_id_from,
  afternoon_local_id_to,
  student_number,
  user_id,
  status,
  created_at,
  updated_at,
  callback
) {
  var connection = mysql_connection.conncection();

  connection.query(
    "INSERT INTO applications (name, birth_date, tin, identification, nationality, course_year, date, email, phone_1, phone_2, course_id, renovation, school_id, observations, accept_procedure, morning_local_id_from, morning_local_id_to, afternoon_local_id_from, afternoon_local_id_to, student_number,user_id, status, created_at, updated_at) VALUES('" +
      name +
      "', '" +
      birth_date +
      "' , '" +
      tin +
      "','" +
      identification +
      "','" +
      nationality +
      "'," +
      course_year +
      ",'" +
      date +
      "','" +
      email +
      "','" +
      phone_1 +
      "','" +
      phone_2 +
      "'," +
      course_id +
      "," +
      renovation +
      "," +
      school_id +
      ",'" +
      observations +
      "'," +
      accept_procedure +
      "," +
      morning_local_id_from +
      "," +
      morning_local_id_to +
      "," +
      afternoon_local_id_from +
      "," +
      afternoon_local_id_to +
      ",'" +
      student_number +
      "'," +
      user_id +
      ",'" +
      status +
      "','" + 
      created_at +
      "','" + 
      updated_at +"')",
    function (error, results, fields) {
      try {
        if (!error) {
          connection.query("SELECT * FROM applications where id = "+ results.insertId, function (error, results, fields) {   
            if (!error) {
              connection.end();
              callback(results[0], null);
            } else {
              connection.end();
              callback(null, { code: error.code, message: error.message });
            }
          
          });
        } else {
          connection.end();
          callback(null, { code: error.code, message: error.message });
        }
      } catch (e) {
        connection.end();
        callback(null, { code: e.code, message: e.message });
      }
    }
  );
};

 /**
  * 
  * @param {*} name 
  * @param {*} birth_date 
  * @param {*} tin 
  * @param {*} identification 
  * @param {*} nationality 
  * @param {*} course_year 
  * @param {*} date 
  * @param {*} email 
  * @param {*} phone_1 
  * @param {*} phone_2 
  * @param {*} course_id 
  * @param {*} renovation 
  * @param {*} school_id 
  * @param {*} observations 
  * @param {*} accept_procedure 
  * @param {*} morning_local_id_from 
  * @param {*} morning_local_id_to 
  * @param {*} afternoon_local_id_from 
  * @param {*} afternoon_local_id_to 
  * @param {*} student_number 
  * @param {*} callback 
  * @description Atualiza uma candidatura
  */
 exports.update_application = function (
    application_id,
    name,
    birth_date,
    tin,
    identification,
    nationality,
    course_year,
    date,
    email,
    phone_1,
    phone_2,
    course_id,
    renovation,
    school_id,
    observations,
    accept_procedure,
    morning_local_id_from,
    morning_local_id_to,
    afternoon_local_id_from,
    afternoon_local_id_to,
    student_number,
    updated_at,
    callback
  ) {
    var connection = mysql_connection.conncection();
  
    connection.query(
      "UPDATE applications SET " +
      "name = '"+ name + 
      "', birth_date = '" + birth_date + 
      "', tin = '"+tin+
      "', identification = '" + identification +
      "', nationality = '"+nationality +
      "', course_year = "+course_year +
      ", date = '" + date + 
      "', email = '" + email +
      "', phone_1 = '" + phone_1 + 
      "', phone_2 = '"+ phone_2 +
      "', course_id = " + course_id + 
      ", renovation = " + renovation + 
      ", school_id = " + school_id + 
      ", observations = '" + observations + 
      "', accept_procedure = " + accept_procedure + 
      ", morning_local_id_from = " + morning_local_id_from + 
      ", morning_local_id_to = " + morning_local_id_to + 
      ", afternoon_local_id_from = " + afternoon_local_id_from + 
      ", afternoon_local_id_to = " + afternoon_local_id_to +
      ", student_number = '" + student_number +
      "', updated_at = '" + updated_at +
      "' WHERE id = "+ application_id,
      function (error, results, fields) {
        try {
          if (!error) {
            connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {  
              
              if (!error) {
                connection.end();
                callback(results[0], null);
              } else {
                connection.end();
                callback(null, { code: error.code, message: error.message });
              }
            
            })
          } else {
            connection.end();
            callback(null, { code: error.code, message: error.message });
          }
        } catch (e) {
          connection.end();
          callback(null, { code: e.code, message: e.message });
        }
      }
    );
};

/**
 *
 * @param application_id
 * @param callback
 * @description Elimina uma candidatura
 */
exports.delete_application = function(application_id, callback){

   var connection = mysql_connection.conncection();

   console.log("DELETE FROM applications WHERE id = " + application_id);

   connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {

        if (!error) {

             console.log(results);

            if (results.length !== 0) {
                connection.query("DELETE FROM applications WHERE id = " + application_id, function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback(null, error);
                    }
                    else {
                        console.log("Application Deleted!");
                        connection.end();
                        callback("DELETED", null);
                    }
                });
            } else {
             connection.end();
             callback("NOT EXIST",null);
            }
            } else {
            connection.end();
            callback(null, error)
        }
    });

};

exports.accept_application = function (
  application_id,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "accepted"

  connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {
    if (results.length !== 0) {
      if (!error) {
          if (results[0].status === 'submitted') {
            connection.query(
              "UPDATE applications SET " +
              "status='"+ status + 
              "' WHERE id="+ application_id,
              function (error, results, fields) {
                try {
                  if (!error) {
                    connection.query("SELECT * FROM applications where id = " + application_id, function (error, results, fields) {  
                
                      if (!error) {
                        connection.end();
                        callback(results[0], null);
                      } else {
                        connection.end();
                        callback(null, { code: error.code, message: error.message });
                      }
                    
                    })
                  } else {
                    connection.end();
                    callback(null, { code: error.code, message: error.message });
                  }
                } catch (e) {
                  connection.end();
                  callback(null, { code: e.code, message: e.message });
                }
              }
            );
          } else if (results[0].status === 'accepted') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is already accepted' });
          } else if (results[0].status === 'cancelled') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is cancelled' });
          } else if (results[0].status === 'rejected') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to accept the application because it is already rejected'});
          } else if (results[0].status === 'quited') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to accept the application because it is quited'});
          }
      } else {
        connection.end();
        callback(null, error);
      }
  } else {
    connection.end();
    callback(null, {'code': '400', 'message': "Application don't exist"});
  }
});
};

exports.reject_application = function (
  application_id,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "rejected"

  connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {
    if (results.length !== 0) {
      if (!error) {
          if (results[0].status === 'submitted') {
            connection.query(
              "UPDATE applications SET " +
              "status='"+ status + 
              "' WHERE id="+ application_id,
              function (error, results, fields) {
                try {
                  if (!error) {

                    connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {  
                
                      if (!error) {
                        connection.end();
                        callback(results[0], null);
                      } else {
                        connection.end();
                        callback(null, { code: error.code, message: error.message });
                      }
                    
                    });
              
                  } else {
                    connection.end();
                    callback(null, { code: error.code, message: error.message });
                  }
                } catch (e) {
                  connection.end();
                  callback(null, { code: e.code, message: e.message });
                }
              }
            );
          } else if (results[0].status === 'rejected') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is already rejected' });
          } else if (results[0].status === 'cancelled') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is cancelled' });
          }  else if (results[0].status === 'accepted') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to rejected the application because it is already accepted'});
          } else if (results[0].status === 'quited') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to rejected the application because it is quited'});
          }
      } else {
        connection.end();
        callback(null, error);
      }
  } else {
    connection.end();
    callback(null, {'code': '400', 'message': "Application don't exist"});
  }
});
};

exports.cancel_application = function (
  application_id,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "cancelled"

  connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {
    if (results.length !== 0) {
      if (!error) {
          if (results[0].status === 'submitted') {
            connection.query(
              "UPDATE applications SET " +
              "status='"+ status + 
              "' WHERE id="+ application_id,
              function (error, results, fields) {
                try {
                  if (!error) {

                    connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {  
                
                      if (!error) {
                        connection.end();
                        callback(results[0], null);
                      } else {
                        connection.end();
                        callback(null, { code: error.code, message: error.message });
                      }
                    
                    });
              
                  } else {
                    connection.end();
                    callback(null, { code: error.code, message: error.message });
                  }
                } catch (e) {
                  connection.end();
                  callback(null, { code: e.code, message: e.message });
                }
              }
            );
          } else if (results[0].status === 'rejected') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is rejected' });
          } else if (results[0].status === 'cancelled') {
            connection.end();
            callback(null, { code: 1500, message: 'The application is already cancelled' }); 
          }  else if (results[0].status === 'accepted') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to cancel the application because it is already accepted'});
          } else if (results[0].status === 'quited') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to cancel the application because it is quited'});
          }
      } else {
        connection.end();
        callback(null, error);
      }
  } else {
    connection.end();
    callback(null, {'code': '400', 'message': "Application don't exist"});
  }
});
};

/**
 * 
 * @param {*} application_id 
 * @param {*} justification 
 * @param {*} date_withdrawal 
 * @param {*} callback
 * @desc Desitência do passe
 */
exports.withdrawal_application = function (
  application_id,
  justification,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "withdrawal"
  var statusWithdrawal = "pending";
  var date_withdrawal = moment().toISOString();
  var created_at = moment().toISOString();
  var updated_at = moment().toISOString();


  connection.beginTransaction(function(err) {

    if (err) { connection.end(); callback(null, {'code': err.code, 'message': err.message}); }

    connection.query("SELECT * FROM applications where id = "+ application_id, function (error, results, fields) {
      if (results.length !== 0) {
        if (!error) {
            if (results[0].status === 'accepted') {
              connection.query(
                "UPDATE applications SET " +
                "status='"+ status + 
                "' WHERE id="+ application_id,
                function (error, results, fields) {
                  try {
                    if (!error) {
  
                      connection.query(
                        "INSERT INTO withdrawals (application_id, justification, date_withdrawal, status, created_at, updated_at) VALUES(" +
                          application_id +
                          ", '" +
                          justification +
                          "' , '" +
                          date_withdrawal +
                          "','" +
                          statusWithdrawal +
                          "','" +
                          created_at +
                          "','" + 
                          updated_at +"')",
                        function (error, results, fields) {
                          if (!error) {
                            connection.query("SELECT * FROM withdrawals where id = "+ results.insertId, function (error, results, fields) {
                              if (!error) {
                                  connection.commit(function (err) {
                                    if (err) {
                                        return connection.rollback(function () {
                                            connection.end();
                                            callback(null, {'code': err.code, 'message': err.message});
                                        });
                                    }
                                    connection.end();
                                    callback(results[0], null);
                                });
                              } else {
                                return connection.rollback(function() {
                                  connection.end();
                                  callback(null,{'code': error.code, 'message': error.message});
                              });
                              }
                            
                            });
                          } else {
                            return connection.rollback(function() {
                              connection.end();
                              callback(null,{'code': error.code, 'message': error.message});
                           });
                          }
                      });
  
                    } else {
                      return connection.rollback(function() {
                        connection.end();
                        callback(null,{'code': error.code, 'message': error.message});
                    });
                    }
                  } catch (e) {
                    return connection.rollback(function() {
                      connection.end();
                      callback(null,{'code': error.code, 'message': error.message});
                  });
                  }
                }
              );
            } else if (results[0].status === 'rejected') {
              connection.end();
              callback(null, { code: 1500, message: "The application is rejected can't withdrawal" });
            } else if (results[0].status === 'submitted') {
              connection.end();
              callback(null, { code: 1501, message: "The application is submitted can't withdrawal"});
            } else if (results[0].status === 'closed') {
              connection.end();
              callback(null, { code: 1502, message: "The application is closed can't withdrawal"});
            } else if (results[0].status === 'withdrawal') {
              connection.end();
              callback(null, { code: 1503, message: "The application is already in withdrawal"});
            } else if (results[0].status === 'quited') {
              connection.end();
              callback(null, { code: 1502, message: "The application is quited can't withdrawal"});
            }
        } else {
           return connection.rollback(function() {
                            connection.end();
                            callback(null,{'code': error.code, 'message': error.message});
           });
        }
    } else {
      connection.end();
      callback(null, {'code': '400', 'message': "Application don't exist"});
    }
  });

  });
};

/**
 *
 * @param application_id
 * @param callback
 * @description Mostra a desitência de uma candidatura
 */
exports.get_withdrawal_application = function(application_id, callback) {

  var connection = mysql_connection.conncection();

  connection.query("SELECT * FROM withdrawals where application_id = "+application_id, function (error, results, fields) {

       if (!error) {

        console.log(results);

           if (results.length !== 0) {
               connection.end();
               callback(results[0], error);
           } else {
               connection.end();
               callback(null, {'code': '400', 'message': "Application don't exist"});
           }
       } else {
           connection.end();
           callback(null, {'code': error.code, 'message': error.message});
       }
   });

};

/**
 * 
 * @param {*} withdrawal_id 
 * @param {*} callback 
 */
exports.accept_withdrawal = function (
  withdrawal_id,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "accepted"
  var applicationStatus = "quited"

  connection.query("SELECT * FROM withdrawals where id = "+ withdrawal_id, function (error, results, fields) {
    if (!error) {
      if (results.length !== 0) {
          if (results[0].status === 'pending') {
            connection.query(
              "UPDATE withdrawals SET " +
              "status='"+ status + 
              "' WHERE id="+ withdrawal_id,
              function (error, results, fields) {
                try {
                  if (!error) {
                    connection.query("SELECT * FROM withdrawals where id = " + withdrawal_id, function (error, withdrawal, fields) {  
                
                      if (!error) {
                        connection.query(
                          "UPDATE applications SET " +
                          "status='"+ applicationStatus + 
                          "' WHERE id="+ withdrawal[0].application_id,
                          function (error, results, fields) {

                          connection.end();
                          callback(withdrawal[0], null);
                        });

                      } else {
                        connection.end();
                        callback(null, { code: error.code, message: error.message });
                      }
                    
                    });
                  } else {
                    connection.end();
                    callback(null, { code: error.code, message: error.message });
                  }
                } catch (e) {
                  connection.end();
                  callback(null, { code: e.code, message: e.message });
                }
              }
            );
          } else if (results[0].status === 'accepted') {
            connection.end();
            callback(null, { code: 1500, message: 'The withdrawal is already accepted' });
          }  else if (results[0].status === 'rejected') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to accept the withdrawals because it is already rejected'});
          }
      } else {
        connection.end();
        callback(null, {'code': '400', 'message': "Withdrawal don't exist"});
      }
  } else {
    connection.end();
    callback(null, error);
  }
});
};

/**
 * 
 * @param {*} withdrawal_id 
 * @param {*} callback 
 */
exports.reject_withdrawal = function (
  withdrawal_id,
  callback
) {
  var connection = mysql_connection.conncection();
  var status = "rejected"

  connection.query("SELECT * FROM withdrawals where id = "+ withdrawal_id, function (error, results, fields) {
    if (results.length !== 0) {
      if (!error) {
          if (results[0].status === 'pending') {
            connection.query(
              "UPDATE withdrawals SET " +
              "status='"+ status + 
              "' WHERE id="+ withdrawal_id,
              function (error, results, fields) {
                try {
                  if (!error) {

                    connection.query("SELECT * FROM withdrawals where id = "+ withdrawal_id, function (error, results, fields) {  
                
                      if (!error) {
                        connection.end();
                        callback(results[0], null);
                      } else {
                        connection.end();
                        callback(null, { code: error.code, message: error.message });
                      }
                    
                    });
              
                  } else {
                    connection.end();
                    callback(null, { code: error.code, message: error.message });
                  }
                } catch (e) {
                  connection.end();
                  callback(null, { code: e.code, message: e.message });
                }
              }
            );
          } else if (results[0].status === 'rejected') {
            connection.end();
            callback(null, { code: 1500, message: 'The withdrawal is rejected' });
          }  else if (results[0].status === 'accepted') {
            connection.end();
            callback(null, { code: 1510, message: 'It is not possible to rejected the withdrawal because it is already accepted'});
          }
      } else {
        connection.end();
        callback(null, error);
      }
  } else {
    connection.end();
    callback(null, {'code': '400', 'message': "Withdrawal don't exist"});
  }
});
};


