var mysql_connection = require('../connection');

/**
 * 
 * @param callback
 * @description Mostras a confoguração do microserviço
 */
exports.get_configuration = function(callback) {

    var connection = mysql_connection.conncection();

    connection.query('CALL get_configuration()', function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], error);
        } else {
            connection.end();
            callback(null, error);
        }       
    });
};

/**
 *
 * @param service_id
 * @param configuration_id
 * @param callback
 * @descriptions Atualiza a configuração
 */
exports.update_configuration = function(configuration_id ,service_id,callback) {

    var connection = mysql_connection.conncection();

    connection.query('CALL update_configuration('+ configuration_id +',' + service_id + ')', function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], error);
        } else {
            connection.end();
            callback(null, error);
        }
    });
};