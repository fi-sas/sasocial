var mysql_connection = require('../connection');

/**
 * 
 * @param callback
 * @description Mostras todas as configurações do MS
 */
exports.get_configurations = function(callback) {

    var connection = mysql_connection.conncection();

    connection.query('SELECT * FROM configurations', function (error, results, fields) {
     try { 
         if (!error) {

                for(var i = 0; i < results.length; i++) {
                    if (results[i].config !== null) {
                        results[i].config = JSON.parse(results[i].config);
                    }
                }

                connection.end();
                callback(results, error);
            } else {
                connection.end();
                callback(null, error);
            }
        } catch (e) {
            callback(null, e);
        }       
    });
 
};

/**
 * 
 * @param callback
 * @description Mostras todas as configurações do MS
 */
exports.get_one_configurations = function(configuration_key, callback) {

    var connection = mysql_connection.conncection();

    connection.query("SELECT * FROM configurations WHERE configuration_key = '" + configuration_key +"'", function (error, results, fields) {

      try {  
        if (!error) {
            connection.end();

            if (results[0].config !== null) {
              results[0].config = JSON.parse(results[0].config);
            }

            callback(results, error);
        } else {
            connection.end();
            callback(null, error);
        }
      } catch (e) {
         callback(null, e);
      }    
    });
};

/**
 *
 * @param configuration_key
 * @param config
 * @param callback
 * @descriptions Atualiza uma configuração
 */
exports.update_configurations = function(configuration_key , config, callback) {

    var connection = mysql_connection.conncection();

    connection.query("UPDATE configurations SET configuration_key = '" + configuration_key + "', config = '" + config +"'", function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], error);
        } else {
            connection.end();
            callback(null, error);
        }
    });
};