"use strict";
var mysql_connection = require('../connection');
/**
 *
 * @limit
 * @offset
 * @columnOder
 * @order_by
 * @param callback
 * @description Mostras os contactos
 */
exports.get_contacts = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM contact",function (error, total, fields) {

        if (!error) {

            try {

                var limit_offset;
                if (limit != -1) {
                    limit_offset = "limit " + limit + " offset " + offset;
                } else {
                    limit_offset = "";
                }

            } catch (e) {
                connection.end();
                callback(null, null, {'code': e.code, 'message': e.message});
            }

            connection.query("CALL get_contacts('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {

                    try {

                        connection.end();
                       callback(results[0], total[0].total,null);

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param contact_id
 * @param callback
 * @description Mostra um contacto
 */
exports.get_one_contact = function (contact_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_contact(" + contact_id + ")", function (error, results, fields) {

        if (!error) {

            try {

                if (results[0].length !== 0) {

                    connection.end();
                    callback(results[0], null);

                } else {
                    connection.end();
                    callback(null, {'code': '400', 'message': "Contact don't exist"});
                }

            } catch (e) {
                connection.end();
                callback(null, {'code': e.code, 'message': e.message});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

}

/**
 *
 * @param type
 * @param callback (error)
 * @description Insere um contacto
 */
exports.insert_contact = function(name, phone, email, website, callback) {

  var connection = mysql_connection.conncection();

  connection.query("CALL insert_contact('" + name + "' , " + phone + " , '" + email + "','" + website + "')", function (error, results, fields) {

     try {

         if (!error) {
             connection.end();
             callback(results[0], null);
         } else {
             connection.end();
             callback(null, {'code': error.code, 'message': error.message});
         }

     } catch (e) {
         connection.end();
         callback(null, {'code': e.code, 'message': e.message});
     }

    });


};

/**
 * 
 * @param type_day_id
 * @param callback
 * @description Elimina um contacto
 */
exports.delete_contact = function(contact_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_contact("+ contact_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                    connection.query("select Distinct(contact_id) from route where contact_id=" + contact_id, function (error, results, fields) {
                        if(error) {

                            connection.end();
                            callback(null, error);
                        } else {
                            if (results.length === 0) {

                                connection.query("CALL delete_contact(" + contact_id + ")", function (error, results, fields) {

                                    if (error) {
                                        connection.end();
                                        callback({'code': error.code, 'message': error.message});
                                    }
                                    else {
                                        console.log("Contacto eliminado");

                                        connection.end();
                                        callback(null);
                                    }
                                });
                            } else {

                                connection.end();
                                callback({"code": 700, "message": "Contact ID " + contact_id + " is associated with a route."});
                            }
                        }
                    });
            } else {
                connection.end();
                callback({'code': '400', 'message': "Contact don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * @param contact_id
 * @param name
 * @param phone
 * @param email
 * @param website 
 * @param callback 
 * @description Altera um tipo de utilizador
 */
exports.update_contact = function(contact_id, name, phone, email, website, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_contact("+ contact_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                    connection.query("CALL update_contact('" + name + "'," + phone + ",'" + email + "','" + website + "'," + contact_id + ")", function (error, results, fields) {

                        if (error){
                            connection.end();
                            callback(null, {'code': error.code, 'message': error.message});
                        } else {
                           console.log("Contacto alterado");
                           connection.end();
                           callback(results[0], null);
                        }

                    });

            } else {

                connection.end();
                callback(null, {'code': '400', 'message': "Contact don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};