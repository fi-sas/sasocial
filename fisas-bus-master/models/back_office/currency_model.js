"use strict";
var mysql_connection = require('../connection');

/**
 * 
 * @param callback
 * @description Mostras os tipo de moedas
 */
exports.get_currencys = function(limit, offset, columnOrder, order_by, callback) {

  var connection = mysql_connection.conncection();
  connection.query("Select COUNT(*) as total FROM currency",function (error, total, fields) {

                if (!error) {
                    var limit_offset;
                    if (limit != -1) {
                        limit_offset = "limit "+ limit + " offset "+offset;
                    } else {
                        limit_offset = "";
                    }


            connection.query("CALL get_currencys('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {
                    connection.end();
                    callback(results[0], total[0].total, null);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param currency_id
 * @param callback
 * @description Mostra uma tipo de moeda
 */
exports.get_one_currency = function (currency_id, callback) {

    var connection = mysql_connection.conncection();
    connection.query("CALL get_one_currency(" + currency_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.end();
                callback(results[0], null);

            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Currency don't exist"});
            }
        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

}
/**
 *
 * @param currency
 * @param symbol
 * @param callback (error)
 * @description Insere um tipo de moeda
 */
exports.insert_currency = function(currency, symbol,callback) {

   var connection = mysql_connection.conncection();
   connection.query("CALL insert_currency('"+ currency +"','"+ symbol +"')", function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], null);
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }

    });

};

/**
 * 
 * @param currency_id
 * @param callback
 * @description Elimina um tipo de dia 
 */
exports.delete_currency = function(currency_id, callback){


    var connection = mysql_connection.conncection();
   connection.query("CALL get_one_currency("+ currency_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

              connection.query("select Distinct(currency_id) from price where currency_id=" + currency_id, function (error, results, fields) {

                    if(error) {
                        connection.end();
                        callback(null, error);
                    } else {
                        if (results.length === 0) {

                            connection.query("CALL delete_currency(" + currency_id + ")", function (error, results, fields) {

                                if (error) {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                }
                                else {
                                    console.log("Moeda eliminada");
                                    connection.end();
                                    callback(null);
                                }
                            });
                        } else {
                            connection.end();
                            callback({"code": 700, "message": "Currency ID " + currency_id + " is associated with a price table."});
                        }
                    }
              });

            } else {
                connection.end();
                callback({'code': '400', 'message': "Currency don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * @param currency_id
 * @param currency
 * @param symbol
 * @param callback 
 * @description Altera a moeda
 */
exports.update_currency = function(currency_id, currency, symbol, callback){


  var connection = mysql_connection.conncection();

  connection.query("CALL get_one_currency("+ currency_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

               connection.query("CALL update_currency(" + currency_id + ",'" + currency + "','" + symbol + "')", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, error);
                    } else {
                       console.log("Moeda alterada");
                       connection.end();
                       callback(results[0], null);
                    }

                });

            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Currency don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};