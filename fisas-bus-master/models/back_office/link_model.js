"use strict";
var mysql_connection = require('../connection');
var _ = require('lodash');

/**
 *
 * @param local_route1_id
 * @param local_route2_id
 * @param route1_id
 * @param route2_id
 * @param callback (error)
 * @description Insere uma ligação entre duas rotas
 */
exports.insert_link = function(local_route1_id, local_route2_id, route1_id, route2_id, callback) {

    var routes = [route1_id, route2_id];
    var locals = [local_route1_id, local_route2_id];

    var connection = mysql_connection.conncection();

    connection.query("CALL check_routes('"+ routes.toString() +"')", function (error, results, fields) {

        if (!error) {

            var routes_ids_result = [];

            for (var i = 0; i < results[0].length; i++) {

                routes_ids_result.push(results[0][i].id);
            }

            var diff = _.difference(routes, routes_ids_result);

            if (diff.length === 0) {

                connection.query("CALL check_locals('"+ locals.toString() +"')", function (error, results, fields) {

                    if (!error) {

                        var locals_ids_result = [];

                        for (var i = 0; i < results[0].length; i++) {

                            locals_ids_result.push(results[0][i].id);
                        }

                        var diff = _.difference(locals, locals_ids_result);

                        if (diff.length === 0) {


                           connection.query("CALL insert_link(" + local_route1_id + "," + local_route2_id + "," + route1_id + "," + route2_id + ")", function (error, results, fields) {

                            if (!error) {
                                connection.end();
                                callback(results[0], null);
                            } else {
                                connection.end();
                                callback(null, error);
                            }

                        });


                        } else {
                            connection.end();
                            callback(null, {'code': 400, 'message': "The locals " + diff.toString() + " don't exist"});
                        }


                    } else {
                        connection.end();
                        callback(null, {code: error.code, message: error.message});
                    }

                });

            } else {
                connection.end();
                callback(null, {'code': 400, 'message': "The routes " + diff.toString() + " don't exist"});
            }
        } else {
            connection.end();
            callback(null, {code: error.code, message: error.message });
        }
    });

};

/**
 *
 * @param local_route1_id
 * @param local_route2_id
 * @param route1_id
 * @param route2_id
 * @param callback
 * @description Elimina uma ligaçao entre duas rotas
 */
exports.delete_link = function(local_route1_id, local_route2_id, route1_id, route2_id, callback) {

    var routes = [route1_id, route2_id];
    var locals = [local_route1_id, local_route2_id];

    var connection = mysql_connection.conncection();

    connection.query("CALL check_routes('"+ routes.toString() +"')", function (error, results, fields) {

        if (!error) {

            var routes_ids_result = [];

            for (var i = 0; i < results[0].length; i++) {

                routes_ids_result.push(results[0][i].id);
            }

            var diff = _.difference(routes, routes_ids_result);

            if (diff.length === 0) {

                connection.query("CALL check_locals('"+ locals.toString() +"')", function (error, results, fields) {

                    if (!error) {

                        var locals_ids_result = [];

                        for (var i = 0; i < results[0].length; i++) {

                            locals_ids_result.push(results[0][i].id);
                        }

                        var diff = _.difference(locals, locals_ids_result);

                        if (diff.length === 0) {


                           connection.query("CALL delete_link(" + local_route1_id + "," + local_route2_id + "," + route1_id + "," + route2_id + ")", function (error, results, fields) {

                            if (!error) {
                                connection.end()
                                callback(null);
                            } else {
                                connection.end();
                                callback(error);
                            }

                        });


                        } else {
                            connection.end();
                            callback(null, {'code': 400, 'message': "The locals " + diff.toString() + " don't exist"});
                        }


                    } else {
                        connection.end();
                        callback(null, {code: error.code, message: error.message});
                    }

                });

            } else {
                connection.end();
                callback(null, {'code': 400, 'message': "The routes " + diff.toString() + " don't exist"});
            }
        } else {
            connection.end();
            callback(null, {code: error.code, message: error.message });
        }
    });

};


/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostra tudas as ligações
 */
exports.get_links = function (limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM links",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("CALL get_links('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {
                    connection.end();
                    callback(results[0], total[0].total, null);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });
}