"use strict";
var mysql_connection = require('../connection');
var _ = require('lodash');

/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostras os locais
 */
exports.get_locals = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM local",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("CALL get_locals('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {
                if (!error) {

                    connection.end();
                    callback(results[0], total[0].total, error);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param local_id
 * @param callback
 * @description Mostra um local
 */
exports.get_one_local = function(local_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_local(" + local_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.end();
                callback(results[0], error);
            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Local don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};
/**
 * @param callback
 * @description Mostras os locais que se referem ao instituto
 */
exports.get_locals_institute = function(limit, offset, columnOrder, order_by, callback) {

     var connection = mysql_connection.conncection();

     connection.query("Select COUNT(*) as total FROM local where institute = 1",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("CALL get_locals_institute('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {
                        connection.end();
                        callback(results[0], total, null);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param address
 * @param latitude
 * @param longitude
 * @param local
 * @param region
 * @param institute
 * @param callback (error)
 * @description Insere um ponto
 */
exports.insert_local = function(address, latitude, longitude, local, region, institute, callback){

    var connection = mysql_connection.conncection();
    connection.query("CALL insert_local('" + address + "','" + latitude +"'," + longitude + ",'" + local + "','" + region + "'," + institute + ")", function (error, results, fields) {

        if(!error){
            connection.end();
            callback(results[0],null);
        } else {
            connection.end();
            callback(null,{'code': error.code, 'message': error.message});
        }

    });

};

/**
 *
 * @param local_id
 * @param callback
 * @description Elimina um ponto
 */
exports.delete_local = function(local_id, callback){

    var connection = mysql_connection.conncection();

   connection.query("CALL get_one_local(" + local_id + ")", function (error, results, fields) {

        if (!error) {

            if (results.length !== 0) {

                connection.query("select Distinct(local_id) from route_order where local_id = " + local_id, function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback(null, error);
                    } else {
                        if (results.length === 0) {
                            connection.query("CALL delete_local(" + local_id + ")", function (error, results, fields) {
                                if (error) {
                                    connection.end();
                                    callback(null, error);
                                }
                                else {
                                    console.log("Local Deleted!");
                                    connection.end();
                                    callback("DELETED", null);
                                }
                            });
                        } else {
                            connection.end();
                            callback("RELATED", error)
                        }
                    }
                });
            } else {
             connection.end();
             callback("NOT EXIST",null);
            }
            } else {
            connection.end();
            callback(null, error)
        }
    });

};

/**
 *
 * @param local_id
 * @param address
 * @param latitude
 * @param longitude
 * @param local
 * @param region
 * @param institute
 * @param callback (status , error)
 * @description -> Actuliza um ponto
 */
exports.update_local = function(local_id, address, latitude, longitude, local, region, institute, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_local(" + local_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {
                connection.query("CALL update_local('"+address+"',"+latitude+","+longitude+",'"+local+"','"+region+"',"+local_id+","+institute+")", function (error, results, fields) {

                    if (!error){
                        connection.end();
                        callback(results[0], null);
                    } else {
                        connection.end();
                        callback(null,error);
                    }
                });

            } else {
                connection.end();
                callback(null,{
                    "code": 400,
                    "message": "Local ID " + local_id + " is not exist."
                });
            }
        } else {
            connection.end();
            callback(null, error)
        }
    });

};

/**
 *
 * @param locals_ids
 * @callback
 * @description Verifica se exitem os locais
 */
exports.check_locals = function (locals_ids, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL check_locals('" + locals_ids.toString() + "')", function (error, results, fields) {

        if (!error) {

            var locals_ids_result = [];

            for (var i = 0; i < results[0].length; i++) {

                locals_ids_result.push(results[0][i].id);
            }

            var diff = _.difference(locals_ids, locals_ids_result);

            if (diff.length === 0) {

                connection.end();
                callback(null);

            } else {
                connection.end();
                callback({'code': 400, 'message': "The locals " + diff.toString() + " don't exist"});
            }
        } else {
            connection.end();
            callback(error);
        }
    });

}

/**
 *
 * @param route_id
 * @param local_id
 * @param callback
 * @description Verifica se a rota tem um determinado local
 */
exports.check_locals_in_route = function (route_id, local_id,callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL check_locals_in_route(" + route_id + ")", function (error, results, fields) {

        if (!error) {

            var locals_ids_result = [];

            for (var i = 0; i < results[0].length; i++) {

                locals_ids_result.push(results[0][i].local_id);
            }

            var exist = _.indexOf(locals_ids_result, local_id);

            if (exist !== -1) {

                connection.end();
                callback(null);

            } else {
                connection.end();
                callback({'code': 400, 'message': "The local id " + local_id + " don't exist in route"});
            }
        } else {
            connection.end();
            callback(error);
        }
    });

}

/**
 *
 * @param locals_ids
 * @callback
 * @description Retornas os locais pelos ids
 */
exports.get_locals_by_ids = function (locals_ids, callback) {

    if (locals_ids.length !== 0) {

        var connection = mysql_connection.conncection();

        connection.query("Select * from local where id in (" + locals_ids.toString() + ")", function (error, results, fields) {

            if (!error) {
                connection.end();
                callback(results, null);
                
            } else {
                connection.end();
                callback([], error);
            }
        });
  } else {
        callback([], null);
  }

}