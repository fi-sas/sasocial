"use strict";
var mysql_connection = require('../connection');

/**
 * @limit
 * @offset
 * @columnOrder
 * @order_by
 * @param callback
 * @description Mostra as épocas 
 */
exports.get_periods = function(limit, offset, columnOrder, order_by, callback){

   var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM period",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }
                connection.query("CALL get_periods('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                    if (!error) {
                        connection.end();
                        callback(results[0], total, null);
                    } else {
                        if (error.code === "ER_BAD_FIELD_ERROR") {
                            connection.end();
                            callback(null, null,{
                                "code": 300,
                                "message": columnOrder + " not a exist to sort."
                            });
                        } else {
                            connection.end();
                            callback(null, null,{'code': error.code, 'message': error.message});
                        }
                    }
                });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });
};


/**
 *
 * @param period_id
 * @param callback
 * @description Mostra um periodo
 */
exports.get_one_period = function (period_id, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_period("+ period_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {
                connection.end();
                callback(results[0], null);

            } else {
                connection.end();
                callback(null,{'code': '400', 'message': "Period don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

}


/**
 *
 * @param type
 * @param callback (error)
 * @description Insere uma época
 */
exports.insert_period = function(type, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL insert_period('"+ type +"')", function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], null);
        } else {
            connection.end();
            callback(null,error);
        }

    });

};

/**
 * 
 * @param period_id 
 * @param callback 
 * @description Elimina uma época 
 */
exports.delete_period = function(period_id, callback){


   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_period("+ period_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                    connection.query("select Distinct(period_id) from season where period_id="+period_id, function (error, results, fields) {
                        if (error) {
                            connection.end();
                            callback({'code': error.code, 'message': error.message});
                        } else {

                            if (results.length === 0) {

                             connection.query("CALL delete_period("+period_id+")", function (error, results, fields) {

                                    if (error) {
                                        connection.end();
                                       callback({'code': error.code, 'message': error.message});
                                    } else {
                                       console.log("Época eliminada");
                                       connection.end();
                                       callback(null);
                             }});

                            } else {
                                connection.end();
                                callback({"code": 700, "message": "Period ID " + period_id + " is associated with a season."});
                            }
                       }
                     });
            } else {
                connection.end();
                callback({'code': '400', 'message': "Period don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * 
 * @param period_id 
 * @param type 
 * @param callback 
 * @description Altera a época
 */
exports.update_period = function(period_id, type, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_period("+ period_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL update_period("+period_id+",'"+type+"')", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, {'code': error.code, 'message': error.message});
                    } else {
                       console.log("Época alterada");
                       connection.end();
                       callback(results[0], null);
                    }
                });

            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Period type don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};