var mysql_connection = require('../connection');
var async = require('async');
var _ = require('lodash');

/**
 * @param route_id
 * @param ticket_type_id
 * @param type_user_id
 * @param currency_id
 * @param callback
 * @description Mostras uma tabela de preços
 */
exports.get_price_table = function(route_id, ticket_type_id, type_user_id, currency_id, user,callback) {

   var connection = mysql_connection.conncection();

   connection.query('CALL get_one_route('+ route_id +')', function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query('CALL get_one_ticket_type(' + ticket_type_id + ')', function (error, results, fields) {

                    if (!error) {

                        if (results[0].length !== 0) {

                                        connection.query('CALL get_price_table(' + route_id + ',' + ticket_type_id + ',' + type_user_id + ',' + currency_id + ')', function (error, results, fields) {

                                            if (!error) {

                                                if (results[0].length !== 0) {

                                                    try {

                                                        var price_table, prices = [];

                                                        for (var i = 0; i < results[0].length; i++) {

                                                            if (i === 0) {

                                                                price_table = {
                                                                    route_id: results[0][i].route_id,
                                                                    type_user_id: results[0][i].type_user_id,
                                                                    user: user,
                                                                    ticket_type_id: results[0][i].ticket_type_id,
                                                                    ticket: results[0][i].type,
                                                                    currency_id: currency_id,
                                                                    prices: ""
                                                                }
                                                            }

                                                            prices.push({
                                                                price_id: results[0][i].price_id,
                                                                value: results[0][i].value,
                                                                currency: results[0][i].symbol,
                                                                description: results[0][i].description,
                                                                number_stop: results[0][i].number_stop,
                                                                tax_id: results[0][i].tax_id,
                                                                account_id: results[0][i].account_id
                                                            })
                                                        }

                                                        price_table.prices = prices;

                                                        connection.end();
                                                        callback(price_table, error);
                                                    } catch (e) {
                                                        connection.end();
                                                        callback(null, {code: e.code, message: e.message});
                                                    }

                                                } else {
                                                    connection.end();
                                                    callback([], null);
                                                }
                                            } else {
                                                connection.end();
                                                callback(null, {code: error.code, message: error.message});
                                            }
                                        });

                        } else {
                            connection.end();
                            callback(null, {'code': '400', 'message': "Ticket type don't exist"});
                        }
                    } else {
                        connection.end();
                        callback([], {code: error.code, message: error.message});
                    }
                });

            }else {
                connection.end();
                callback(null, {'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback([], {code:error.code, message: error.message});
        }
    });

};

/**
 * 
 * @param currency 
 * @param callback
 * @description Indentifica o id do tipo de moeda
 */
exports.get_currency_id = function(currency, callback) {

    var connection = mysql_connection.conncection();

   connection.query("CALL currency_id('" + currency + "')", function (error, results, fields) {

        if (!error) {
            if (results[0].length !== 0) {
                connection.end();
                callback(results[0][0].id, error);
            } else {
                connection.end();
                callback(null, {"code": 350, "message": currency +" is invalid argument!" });
            }
        } else {
            connection.end();
            callback(null, {"code": error.code, "message": error.message });
        }       
    });

};

/**
 *
 * @param route_id
 * @param ticket_type_id
 * @param type_user_id
 * @param value
 * @param description
 * @param currency_id
 * @param number_stops
 * @param callback (error)
 * @description Insere uma coluna de um preço
 */
exports.insert_price_column = function(route_id, ticket_type_id, type_user_id, value, description, currency_id, number_stops, tax_id, account_id,callback){

    var connection = mysql_connection.conncection();
    connection.query("CALL insert_price_column(" + type_user_id + "," + ticket_type_id  + "," + currency_id + "," + value + ",'" + description + "'," + number_stops + "," + route_id + ","+ tax_id +","+ account_id +")", function (error, results, fields) {
        if(error) {
            connection.end();
            callback(error);}
        else{
            console.log("Inserido um Preço");
            connection.end();
            callback(null);
        }
    });

};

/**
 *
 * @param prices_ids
 * @param callback
 * @description Elimina uma tabela de preço de uma rota
 */
exports.delete_price_table = function(route_id, ticket_type_id, type_user_id, currency_id, profiles,callback){

 var connection = mysql_connection.conncection();

  connection.query('CALL get_one_route('+ route_id +')', function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query('CALL get_one_ticket_type(' + ticket_type_id + ')', function (error, results, fields) {

                    if (!error) {

                        if (results[0].length !== 0) {

                                    if (_.find(profiles, { 'id': type_user_id}) !== undefined) {

                                        connection.query('CALL get_prices_ids(' + route_id + ',' + ticket_type_id + ',' + type_user_id + ',' + currency_id + ')', function (error, results, fields) {

                                            if (!error) {

                                                var prices_ids = [];
                                                for (var i = 0 ; i < results[0].length; i++) {

                                                    prices_ids.push(results[0][i].price_id);
                                                }

                                                if(prices_ids.length !== 0) {

                                                    connection.query("CALL delete_price_table('" + prices_ids.toString() + "')", function (error, results, fields) {

                                                        if (!error) {
                                                            connection.end();
                                                            callback(null);
                                                        } else {
                                                            connection.end();
                                                            callback({code: error.code, message: error.message});
                                                        }

                                                    });
                                                } else {
                                                    connection.end();
                                                    callback({'code': '400', 'message': "Price table don't exist"});
                                                }

                                            } else {
                                                connection.end();
                                                callback({code: error.code, message: error.message});
                                            }
                                        });
                                    } else {
                                        connection.end();
                                        callback({'code': '400', 'message': "Type User don't exist"});
                                    }

                        } else {
                            connection.end();
                            callback({'code': '400', 'message': "Ticket type don't exist"});
                        }
                    } else {
                        connection.end();
                        callback({code: error.code, message: error.message});
                    }
                });

            }else {
                connection.end();
                callback({'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback({code:error.code, message: error.message});
        }
    });

};

/**
 *
 * @param price_id
 * @param value
 * @param callback
 * @description Altera um preço
 */
exports.update_price = function(price_id, value, callback) {
    
  var connection = mysql_connection.conncection();

  connection.query("CALL get_one_price(" + price_id + ")", function (error, results, fields) {

        if (!error) {
            if (results[0].length !== 0) {

                connection.query("CALL update_price_value(" + price_id + "," + value + ")", function (error, results, fields) {

                    if (!error) {
                        connection.end();
                        callback(results[0], null);
                    } else {
                        connection.end();
                        callback(null, {"code": error.code, "message": error.message});
                    }
                });
            } else  {
                connection.end();
                callback(null,{"code": 400, "message": "Price ID " + price_id + " is not exist."});
            }
        } else {
            connection.end();
            callback(null, {"code": error.code, "message": error.message});
        }

    });

};


function check_ids_prices(prices_ids, route_ids) {

      var arr;
      var errors = [];
      var route_ids_aux = [];

      for (var j = 0; j < route_ids.length; j++) {

          route_ids_aux.push(route_ids[j].price_id);
      }

      try {

          for (var i = 0; i < prices_ids; i++) {

              if (typeof  prices_ids[i] !== "number") {
                  errors.push({code: 450, message: "This " + prices_ids[i] + "is not number type"});
              }
          }

          if (errors.length === 0) {

              console.log("d", prices_ids);
              console.log("s", route_ids_aux);
              arr = _.difference(prices_ids, route_ids_aux);
                  if (arr.length !== 0) {

                      return null;
                  } else {
                      console.log("sddsadasd", arr);
                      return {code: 400, message: "Prices IDS " + arr.toString() + "don't exist in the route"};
                  }
          } else {
             return errors;
          }

      } catch (e) {
          return {code: e.code, message: e.message};
      }

}
