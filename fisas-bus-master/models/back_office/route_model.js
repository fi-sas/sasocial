var mysql_connection = require('../connection');
var _ = require('lodash');
var async = require('async');
var timetable = require('./timetable_model');
var type_day = require('./type_day_model');
var price_table = require('./price_table_model');
var local = require('./local_model');
var moment = require('moment');

/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostras as rotas
 */
exports.get_routes = function(limit, offset, columnOrder, order_by, callback) {

   var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM route", function (error, total, fields) {
                if (!error) {

                    var routes_ids = [], routes = [];
                    var route_info, route_index;

                    var limit_offset;
                    if (limit != -1) {
                        limit_offset = "limit " + limit + " offset " + offset;
                    } else {
                        limit_offset = "";
                    }

                    connection.query("CALL get_routes('" + limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                        if (!error) {

                            for (var i = 0; i < results[0].length; i++) {

                                if (routes_ids.indexOf(results[0][i].id) === -1) {

                                    route_info = {
                                        id: results[0][i].id,
                                        name: results[0][i].name,
                                        external: results[0][i].external === 0? false : true ,
                                        contact_id: results[0][i].contact_id,
                                        locals: [{
                                            id: results[0][i].local_id,
                                            local: results[0][i].local,
                                            region: results[0][i].region,
                                            instruction: results[0][i].instruction,
                                            distance: 0
                                        }]
                                    };

                                    routes_ids.push(results[0][i].id);
                                    routes.push(route_info);

                                } else {
                                    route_index = _.findIndex(routes, {'id': results[0][i].id});

                                    if (route_index !== -1) {

                                        routes[route_index].locals.push({
                                            id: results[0][i].local_id,
                                            local: results[0][i].local,
                                            region: results[0][i].region,
                                            instruction: results[0][i].instruction,
                                            distance: 0
                                        });
                                    }
                                }
                            }

                            if (routes_ids.length !== 0) {
                                connection.query("CALL get_distances('" + routes_ids.toString() + "')", function (error, results, fields) {

                                    if (!error) {

                                        var r;
                                        for (i = 0; i < routes.length; i++) {

                                            // Primeira rota
                                            for (var j = 0; j < routes[i].locals.length - 1; j++) {

                                                r = _.find(results[0], {
                                                    'local1_id': routes[i].locals[j].id,
                                                    'local2_id': routes[i].locals[j + 1].id,
                                                    'route_id': routes[i].id
                                                });

                                                if (r !== undefined) {

                                                    routes[i].locals[j].distance = r.distance;
                                                }
                                            }
                                        }

                                        routes = _.orderBy(routes, [columnOrder], [order_by]);

                                        connection.end();
                                        callback(routes, total[0].total, null);
                                    } else {
                                        connection.end();
                                        callback([], null, {'code': error.code, 'message': error.message});
                                    }
                                });
                            } else {
                                connection.end();
                                callback(routes, 0, null);
                            }
                        } else {
                            if (error.code === "ER_BAD_FIELD_ERROR") {
                                connection.end();
                                callback(null, null, {
                                    "code": 300,
                                    "message": columnOrder + " not a exist to sort."
                                });
                            } else {
                                connection.end();
                                callback(null, null, {'code': error.code, 'message': error.message});
                            }
                        }
                    });

                } else {
                    connection.end();
                    callback(null, null, {'code': error.code, 'message': error.message});
                }
            });
};

/**
 *
 * @param route_id
 * @param callback
 * @description Mostra uma rota
 */
exports.get_one_route = function(route_id , callback) {

    var routes_ids = [], routes = [];
    var route_info, route_index;

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_route(" + route_id + ")", function (error, results, fields) {

        if (!error) {

            for (var i = 0; i < results[0].length; i++) {

                if (routes_ids.indexOf(results[0][i].id) === -1) {

                    route_info = {
                        route_id: results[0][i].id,
                        name: results[0][i].name,
                        external: results[0][i].external === 0? false : true,
                        contact_id: results[0][i].contact_id,
                        locals : [{
                            id: results[0][i].local_id,
                            local: results[0][i].local,
                            region: results[0][i].region,
                            instruction: results[0][i].instruction,
                            distance:0
                        }]
                    };

                    routes_ids.push(results[0][i].id);
                    routes.push(route_info);

                } else {
                    route_index = _.findIndex(routes, {'route_id': results[0][i].id});

                    if (route_index !== -1) {

                        routes[route_index].locals.push({
                            id: results[0][i].local_id,
                            local: results[0][i].local,
                            region: results[0][i].region,
                            instruction: results[0][i].instruction,
                            distance:0
                        });
                    }
                }
            }


            if (routes_ids.length !== 0 ) {

                connection.query("CALL get_distances('" + routes_ids.toString() + "')", function (error, results, fields) {

                    if (!error) {

                        var r;
                        for (i = 0; i < routes.length; i++) {

                            // Primeira rota
                            for (var j = 0; j < routes[i].locals.length - 1; j++) {


                                r = _.find(results[0], {
                                    'local1_id': routes[i].locals[j].id,
                                    'local2_id': routes[i].locals[j + 1].id,
                                    'route_id': routes[i].route_id
                                });

                                if (r !== undefined) {

                                    routes[i].locals[j].distance = r.distance;
                                }
                            }
                        }

                        connection.end();
                        callback(routes, null);
                    } else {
                        connection.end();
                        callback([], error);
                    }
                });
            } else {
                connection.end();
                callback([], {'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback([], error);
        }
    });
};

/**
 *
 * @param route_id
 * @param season_date
 * @param callback
 * @description Retorna as tabelas de uma rota
 */
exports.get_route_timetables = function(route_id, callback) {


    var asyncTimetables = [];
    var timetables = [];

    var connection = mysql_connection.conncection();

    connection.query("CALL timetables_how_many(" + route_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                async.series([

                        function (callback) {
                            results[0].forEach(function (result, index, array) {

                                asyncTimetables.push(function (callback) {

                                    timetable.get_timetable(route_id, result.type_day_id, result.season_id, function (timetable_out, error) {

                                        if (!error) {
                                            timetables.push(timetable_out);

                                            callback(null);
                                        } else {

                                            callback(error);
                                        }

                                    });
                                });

                                if (index === array.length - 1) {
                                    callback();
                                }
                            });
                        },
                        function (callback) {

                            async.parallel(asyncTimetables, function (error) {

                                if (!error) {
                                    callback(null);
                                } else {
                                    callback(error);
                                }
                            });
                        }
                    ],
                    function (error) {

                        if (!error) {
                            connection.end();
                            callback(timetables, null);
                        } else {
                            connection.end();
                            callback([], {code: error.code, message: error.message})
                        }

                    });
            } else {
                connection.end();
                callback([], null);
            }
        } else {
            connection.end();
            callback([], {code: error.code, message: error.message});
        }
    });

};

/**
 *
 * @param route_id
 * @param callback
 * @description Mostras as tabelas de preços que uma rota tem
 */
exports.get_route_prices_tables = function(route_id, profiles,callback) {

    var asyncPricesTables = [];
    var price_tables = [];

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_route(" + route_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {


                connection.query("CALL user_ticket_route(" + route_id + ")", function (error, results, fields) {
                    if (!error) {

                        if (results[0].length !== 0) {

                            async.series([

                                    function (callback) {
                                        results[0].forEach(function (result, index, array) {

                                            asyncPricesTables.push(function (callback) {

                                                price_table.get_price_table(route_id, result.ticket_type_id, result.type_user_id, result.currency_id, _.find(profiles, { 'id': result.type_user_id}).profile,function (price_table_out, error) {
                                                    if (!error) {
                                                        price_tables.push(price_table_out);
                                                        callback(null);
                                                    } else {
                                                        callback(error);
                                                    }
                                                });
                                            });

                                            if (index === array.length - 1) {
                                                callback();
                                            }
                                        });
                                    },
                                    function (callback) {

                                        async.parallel(asyncPricesTables, function (error) {
                                            if (!error) {
                                                callback(null);
                                            } else {

                                                callback(error);
                                            }
                                        });
                                    }
                                ],
                                function (error) {

                                    if (!error) {

                                        connection.end();
                                        callback(price_tables, null);
                                    } else {
                                        connection.end();
                                        callback([], {code: error.code, message: error.message})
                                    }

                                });
                        } else {
                            connection.end();
                            callback([], null);
                        }
                    } else {
                        connection.end();
                        callback([], {code: error.code, message: error.message});
                    }
                });

            } else {
                connection.end();
                callback([], {'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback([], {code: error.code, message: error.message});
        }
    });

};

/**
 *
 * @param route_id
 * @param callback
 * @description Mostra as zonas das rotas
 */
exports.get_route_zones = function(route_id, callback) {

    var zones_ids = [], zones = [];
    var zone_info, zone_index;
    var connection = mysql_connection.conncection();

     connection.query("CALL get_zones_route("+ route_id + ")", function (error, results, fields) {

                if (!error) {

                    // if (results[0].length !== 0) {

                       try {

                           for (var i = 0; i < results[0].length; i++) {

                            if (zones_ids.indexOf(results[0][i].zone_id) === -1) {

                                zone_info = {
                                    zone_id: results[0][i].zone_id,
                                    name: results[0][i].name,
                                    locals: [{
                                        local_id: results[0][i].local_id,
                                        local: results[0][i].local,
                                        region: results[0][i].region
                                    }]
                                };

                                zones_ids.push(results[0][i].zone_id);
                                zones.push(zone_info);

                            } else {
                                zone_index = _.findIndex(zones, {'zone_id': results[0][i].zone_id});

                                if (zone_index !== -1) {

                                    zones[zone_index].locals.push({
                                        local_id: results[0][i].local_id,
                                        local: results[0][i].local,
                                        region: results[0][i].region
                                    });
                                }
                            }
                        }
                           connection.end();
                           callback(zones, null);

                       } catch (e) {
                           connection.end();
                           callback(null, null, {'code': e.code, 'message': e.message});
                       }

                    // } else {
                    //     connection.end();
                    //     callback(null,{'code': '400', 'message': "The route don't have zones"});
                    // }

                } else {
                    connection.end();
                    callback(null,{'code': error.code, 'message': error.message});
                }
            });

}

/**
 *
 * @limit
 * @offset
 * @columnOder
 * @order_by
 * @route_id
 * @param callback
 * @description Mostras os bilhetes comprados numa rota
 */
exports.get_tickets_bought_route = function(limit, offset, columnOrder, order_by, route_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM ticket_bought join route_ticket on route_ticket.ticket_bought_id = ticket_bought.id where route_ticket.route_id="+ route_id,function (error, total, fields) {

        if (!error) {

            try {

                var limit_offset;
                if (limit != -1) {
                    limit_offset = "limit " + limit + " offset " + offset;
                } else {
                    limit_offset = "";
                }

            } catch (e) {
                connection.end();
                callback(null, null, {'code': e.code, 'message': e.message});
            }

            connection.query("CALL get_route_ticket("+route_id+",'"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {

                    try {

                        connection.end();
                        callback(results[0], total[0].total,null);

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @limit
 * @offset
 * @columnOder
 * @order_by
 * @route_id
 * @param callback
 * @description Mostras os bilhetes comprados numa rota
 */
exports.get_route_links = function(limit, offset, columnOrder, order_by, route_id,callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM links where links.route1_id= "+ route_id + " or links.route2_id = "+ route_id,function (error, total, fields) {

        if (!error) {

            try {

                var limit_offset;
                if (limit != -1) {
                    limit_offset = "limit " + limit + " offset " + offset;
                } else {
                    limit_offset = "";
                }

            } catch (e) {
                connection.end();
                callback(null, null, {'code': e.code, 'message': e.message});
            }

            connection.query("CALL get_route_links("+ route_id +",'"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {

                    try {

                        connection.end();
                        callback(results[0], total[0].total,null);

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param route_id
 * @param used
 * @param callback
 * description Mostra o bilhetes confirmados ou não confirmados
 */
exports.get_tickets_bought_route_filter_used = function(limit, offset, columnOrder, order_by, route_id, used, callback) {


        var connection = mysql_connection.conncection();
        var used_aux = null;

        if (used === "confirmed" ) {

            used = 1;

        } else if (used ==='not confirmed') {

            used = 0;
        }

        if (used_aux !== null) {

            connection.query("Select COUNT(*) as total FROM ticket_bought join route_ticket on route_ticket.ticket_bought_id = ticket_bought.id where ticket_bought.id=" + route_id + ' and ' + used_aux, function (error, total, fields) {

                if (!error) {

                    try {

                        var limit_offset;
                        if (limit != -1) {
                            limit_offset = "limit " + limit + " offset " + offset;
                        } else {
                            limit_offset = "";
                        }

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }

                    connection.query("CALL get_route_ticket_filter_used(" + route_id + ",'" + limit_offset + "','" + columnOrder + "','" + order_by + "'," + used + ")", function (error, results, fields) {

                        if (!error) {

                            try {
                                connection.end();
                                callback(results[0], total[0].total, null);

                            } catch (e) {
                                connection.end();
                                callback(null, null, {'code': e.code, 'message': e.message});
                            }
                        } else {
                            if (error.code === "ER_BAD_FIELD_ERROR") {
                                connection.end();
                                callback(null, null, {
                                    "code": 300,
                                    "message": columnOrder + " not a exist to sort."
                                });
                            } else {
                                connection.end();
                                callback(null, null, {'code': error.code, 'message': error.message});
                            }
                        }
                    });

                } else {
                    connection.end();
                    callback(null, null, {'code': error.code, 'message': error.message});
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': '300', 'message': used_aux+" don't exist to filter"});
        }

};

/**
 *
 * @param route_name
 * @param external
 * @param contact_id
 * @param locals
 * @param instructions
 * @param distances_in
 * @param callback (error)
 * @description Insere uma rota
 */
exports.insert_route = function(route_name, external,contact_id, locals, callback) {

    var route_id;
    var max_to_buy = 50;
    var active = 0;

    var connection = mysql_connection.conncection();

    connection.beginTransaction(function(err) {

                if (err) { connection.end(); callback(null,{'code': err.code, 'message': err.message}); }

                connection.query("INSERT INTO route (name, external, contact_id) VALUES ('" + route_name + "'," + external + "," + contact_id + ")",function (error, results, fields) {

                    if (error) {
                        return connection.rollback(function() {
                            connection.end();
                            callback(null,{'code': error.code, 'message': error.message});
                        });
                    }

                    route_id = results.insertId;

                    var order = [], distances = [];
                    for (var i=0; i < locals.length; i++) {

                        order.push([locals[i].id,results.insertId,i+1, locals[i].instruction]);
                        if(i !== locals.length-1) {
                            distances.push([locals[i].id,locals[i+1].id, results.insertId,locals[i].distance]);
                        }
                    }

                    connection.query('INSERT INTO route_order (local_id,route_id,order_table,instruction) VALUES ?', [order], function (error, results, fields) {
                        if (error) {
                            return connection.rollback(function () {
                                connection.end();
                                callback(null, {'code': error.code, 'message': error.message});
                            });
                        }
                        connection.query('INSERT INTO distance (local1_id, local2_id, route_id, distance) VALUES ?',[distances],function (error, results, fields) {
                            if (error) {
                                return connection.rollback(function () {
                                    connection.end();
                                    callback(null, {'code': error.code, 'message': error.message});
                                });
                            }
                            connection.query('INSERT INTO ticket_config (route_id, max_to_buy, active) VALUES ('+ route_id +','+ max_to_buy + ',' + active + ')',function (error, results, fields) {
                                if (error) {
                                    return connection.rollback(function () {
                                        connection.end();
                                        callback(null, {'code': error.code, 'message': error.message});
                                    });
                                }
                                connection.commit(function (err) {
                                        if (err) {
                                            return connection.rollback(function () {
                                                connection.end();
                                                callback(null, {'code': err.code, 'message': err.message});
                                            });
                                        }
                                        console.log('Route inserted!');
                                        connection.end();
                                        callback(route_id, null);
                                    });
                           });
                    });
                    });
                });

            });

};

/**
 *
 * @param route_id
 * @param callback
 * @description Elimina uma rota
 */
exports.delete_route = function(route_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("select * from route where id =" + route_id, function (error, results, fields) {

        if (!error) {

            if (results.length !== 0) {

                            connection.query("CALL delete_route(" + route_id + ")", function (error, results, fields) {

                                if (error) {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                } else {
                                    console.log("Route Deleted!");

                                    connection.end();
                                    callback(null);
                                }
                            });

            } else {
                connection.end();
                callback({'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param route_id
 * @param route_name
 * @description Atualiza o nome da rota
 */
exports.update_route_name = function (route_id, route_name, callback) {

    var connection = mysql_connection.conncection();

    connection.query("select * from route where id =" + route_id, function (error, results, fields) {

                if (!error) {

                    if (results.length !== 0) {

                        connection.query("CALL update_route_name(" + route_id + ",'" + route_name + "')", function (error, results, fields) {

                            if (error) {
                                connection.end();
                                callback(null, {'code': error.code, 'message': error.message});
                            } else {
                                connection.end();
                                callback(results[0], null);
                            }
                        });

                        } else {
                        connection.end();
                        callback(null, {'code': '400', 'message': "Route don't exist"});

                }

                } else {
                    connection.end();
                    callback(null, {'code': error.code, 'message': error.message});
                }
            });
};

/**
 *
 * @param route_id
 * @param local_id
 * @param instruction
 * @param distance
 * @param callback
 * @description Atualiza os dados de um local da rota
 */
exports.update_route_local = function (route_id, local_id, instruction, distance, callback) {

   var connection = mysql_connection.conncection();

   connection.query("select * from route where id =" + route_id, function (error, results, fields) {

        if (!error) {

            if (results.length !== 0) {

               local.check_locals_in_route(route_id,local_id, function (error) {

                   if (!error) {
                       connection.query("CALL update_route_local(" + route_id + "," + local_id + ",'" + instruction + "'," + distance + ")", function (error, results, fields) {

                           if (error) {
                               connection.end();
                               callback(null, {'code': error.code, 'message': error.message});
                           } else {
                               connection.end();
                               callback(results[0], null);
                           }
                       });
                   } else {
                       connection.end();
                       callback(null, error);
                   }

                });
            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Route don't exist"});

            }

        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};