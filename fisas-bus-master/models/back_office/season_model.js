"use strict";
var mysql_connection = require('../connection');
var moment = require('moment');
/**
 *
 * @limit
 * @offset
 * @columnOrder
 * @order_by
 * @param callback
 * @description Mostras os períodos
 */
exports.get_seasons = function(limit, offset, columnOrder, order_by, callback) {

   var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM season",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }
            connection.query("CALL get_seasons('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {
                if (!error) {
                    connection.end();
                    callback(results[0], total[0].total, error);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param season_id
 * @param callback
 */
exports.get_one_season = function(season_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_season(" + season_id + ")", function (error, results, fields) {

        if (!error) {
            if (results[0].length !== 0) {
                connection.end();
                callback(results[0], error);
            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Season don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param period_id
 * @param date_begin
 * @param date_end
 * @param callback (status, error)
 * @desc Insere um período
 */
exports.insert_season = function(period_id, date_begin, date_end, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL check_season('" + date_begin + "')", function (error, results, fields) {
       
      if (!error) {
        if (results[0].length === 0) {

            connection.query("CALL check_season('" + date_end + "')", function (error, results, fields) {
              if (!error) {  
                if (results[0].length === 0) {

                   if (moment(date_begin).isBefore(date_end)) {

                       connection.query("CALL insert_season(" + period_id + ",'" + date_begin + "','" + date_end + "')", function (error, results, fields) {

                           if (!error) {
                               connection.end();
                               callback(results[0], null);
                           } else {
                               connection.end();
                               callback(null, {code: error.code, message: error.message});
                           }
                       });
                   } else {
                       connection.end();
                       callback(null, {code: 250, message: "The date begin " + date_begin +" is greater than the date end "+ date_end});
                   }
                } else {
                    connection.end();
                    callback(null, {code: 250, message: "The date " + date_begin +" is in use in another season"});
                }
             }
            });
        } else {
            connection.end();
            callback(null, {code: 250, message: "The date " + date_begin +" is in use in another season"});
        }
    } else {
        connection.end();
        callback(null, {code: error.code, message: error.message});
    }
    });

};

/**
 *
 * @param season_id
 * @param callback
 * @description Elimina um período
 */
exports.delete_season = function(season_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_season(" + season_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("select Distinct(season_id) from timetable where season_id =" + season_id, function (error, results, fields) {

                    if (error) {
                        connection.end();
                        callback({'code': error.code, 'message': error.message});
                    } else {
                        if (results.length === 0) {
                            connection.query("CALL delete_season(" + season_id + ")", function (error, results, fields) {

                                if (error) {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                } else {
                                    console.log("Período Eliminado!");
                                    connection.end();
                                    callback(null);
                                }
                            });
                        } else {
                            connection.end();
                            callback({"code": 700, "message": "Season ID " + season_id + " is associated with a timetable."});
                        }
                    }
                });
            } else {
                connection.end();
                callback({"code": 400, "message": "Season ID " + season_id + " is not exist."});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param season_id
 * @param date_begin
 * @param date_end
 * @param period_id
 * @param callback
 * @description Altera o período
 */
exports.update_season = function(season_id, date_begin, date_end, period_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_season(" + season_id + ")", function (error, results, fields) {

        if (!error) {
            if (results[0].length !== 0) {

                connection.query("CALL get_one_period(" + period_id + ")", function (error, results, fields) {

                  if (!error) {

                         if(results[0].length !== 0) {

                             connection.query("CALL update_season(" + season_id + "," + period_id + ",'" + date_begin + "','" + date_end + "')", function (error, results, fields) {

                                 if (error) {
                                     connection.end();
                                     callback(null, {'code': error.code, 'message': error.message});
                                 } else {
                                     console.log("Actualizado o período");

                                     connection.end();
                                     callback(results[0], null);
                                 }
                             });
                         } else {
                             connection.end();
                             callback({"code": 400, "message": "Period ID " + season_id + " is not exist."});
                         }
                         } else {
                      connection.end();
                      callback(null, {'code': error.code, 'message': error.message})
                  }
                });
            } else {
                connection.end();
                callback({"code": 400, "message": "Season ID " + season_id + " is not exist."});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message})
        }
    });

};