"use strict";

var mysql_connection = require('../connection');

/**
 *
 * @limit
 * @offset
 * @columnOder
 * @order_by
 * @param callback
 * @description Mostras os as configuraçoes dos bilhete nas rotas
 */
exports.get_tickets_configs = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM ticket_config",function (error, total, fields) {

        if (!error) {

            try {

                var limit_offset;
                if (limit != -1) {
                    limit_offset = "limit " + limit + " offset " + offset;
                } else {
                    limit_offset = "";
                }

            } catch (e) {
                connection.end();
                callback(null, null, {'code': e.code, 'message': e.message});
            }

            connection.query("CALL get_tickets_configs('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {

                    try {

                        connection.end();
                        callback(results[0], total[0].total,null);

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param route_id
 * @param callback
 * @description Mostra uma configuraçao de um bilhete numa rota
 */
exports.get_one_ticket_config = function (route_id, callback) {


   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_ticket_config(" + route_id + ")", function (error, results, fields) {

        if (!error) {

            try {

                if (results[0].length !== 0) {

                    connection.end();
                    callback(results[0], null);

                } else {
                    connection.end();

                    callback(null, {'code': '400', 'message': "Ticket config don't exist"});
                }

            } catch (e) {
                connection.end();
                callback(null, {'code': e.code, 'message': e.message});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

}

/**
 *
 * @param ticket_config_id
 * @param max_to_buY
 * @param callback
 * @description Atualiza a configuraçao do bilhete na rota
 */
exports.set_max = function(route_id, max_to_buy, callback) {


    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_route("+ route_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL set_max("+ route_id + "," + max_to_buy + " )", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, {'code': error.code, 'message': error.message});
                    } else {
                        connection.end();
                        callback(results[0], null);
                    }

                });

            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param ticket_config_id
 * @param active
 * @param callback
 * @description Ativa ou desativa a venda de bilhetes nesta rota
 */
exports.active_enable_ticket = function(route_id, active, callback){


    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_route("+ route_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL enable_disable_ticket(" + route_id + ","+ active +")", function (error, results, fields) {

                    if (error) {
                        connection.end();
                        callback(null,{'code': error.code, 'message': error.message});
                    }
                    else {
                        connection.end();
                        callback(results[0],null);
                    }
                });

            } else {
                connection.end();
                callback({'code': '400', 'message': "The Route don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};
