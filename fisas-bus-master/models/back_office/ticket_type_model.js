"use strict";
var mysql_connection = require('../connection');
/**
 * 
 * @param callback
 * @description Mostras os tipo de bilhete ou passes
 */
exports.get_tickets_types = function(limit, offset, columnOrder, order_by, callback) {

   var connection = mysql_connection.conncection();

   connection.query("Select COUNT(*) as total FROM ticket_type",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }
                connection.query("CALL get_tickets_types('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                    if (!error) {
                        connection.end();
                        callback(results[0], total[0].total, null);
                    } else {
                        if (error.code === "ER_BAD_FIELD_ERROR") {
                            connection.end();
                            callback(null, null,{
                                "code": 300,
                                "message": columnOrder + " not a exist to sort."
                            });
                        } else {
                            connection.end();
                            callback(null, null,{'code': error.code, 'message': error.message});
                        }
                    }
                });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });
};


exports.get_one_ticket_type = function (ticket_type_id, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_ticket_type("+ ticket_type_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.end();
                callback(results[0], null);

            } else {
                connection.end();
                callback(null,{'code': '400', 'message': "Ticket Type don't exist"});
            }
        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

}

/**
 *
 * @param type
 * @param callback (error)
 * @description Insere um tipo de bilhete/passe
 */
exports.insert_ticket_type = function(type, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL insert_ticket_type('"+ type +"')", function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], null);
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }

    });

};

/**
 * 
 * @param ticket_type_id
 * @param callback
 * @description Elimina um tipo de bilhete/passe
 */
exports.delete_ticket_type = function(ticket_type_id, callback){

  var connection = mysql_connection.conncection();

  connection.query("CALL get_one_ticket_type("+ ticket_type_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("select Distinct(ticket_type_id) from price where ticket_type_id =" + ticket_type_id, function (error, results, fields) {
                    if(error) {
                        connection.end();
                        callback({'code': error.code, 'message': error.message});
                    } else {
                        if (results.length === 0) {

                            connection.query("CALL delete_ticket_type(" + ticket_type_id+ ")", function (error, results, fields) {

                                if (error) {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                }
                                else {
                                    console.log("Tipo de bilhete eliminado");
                                    connection.end();
                                    callback(null);
                                }
                            });
                        } else {
                            connection.end();
                            callback({"code": 700, "message": "Season ID " + ticket_type_id + " is associated with a price."});
                        }
                    }
                });
            } else {
                connection.end();
                callback({'code': '400', 'message': "Ticket type don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * @param ticket_type_id
 * @param type
 * @param callback 
 * @description Altera o tipo de bilhete
 */
exports.update_ticket_type = function(ticket_type_id, type, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_ticket_type("+ ticket_type_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL update_ticket_type("+ ticket_type_id + ",'" + type + "')", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, {'code': error.code, 'message': error.message});
                    } else {
                       console.log("Tipo de bilhete alterado");
                       connection.end();
                       callback(results[0], null);
                    }

                });

            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Ticket type don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });


};

