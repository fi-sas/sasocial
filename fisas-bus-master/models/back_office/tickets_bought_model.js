var mysql_connection = require('../connection');
var moment = require('moment');

/**
 *
 * @limit
 * @offset
 * @columnOder
 * @order_by
 * @param callback
 * @description Mostras os bilhetes comprados
 */
exports.get_tickets_bought = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM ticket_bought",function (error, total, fields) {

        if (!error) {

            try {

                var limit_offset;
                if (limit != -1) {
                    limit_offset = "limit " + limit + " offset " + offset;
                } else {
                    limit_offset = "";
                }

            } catch (e) {
                connection.end();
                callback(null, null, {'code': e.code, 'message': e.message});
            }

            connection.query("CALL get_tickets_boughts('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {

                    try {
                       connection.end();
                        callback(results[0], total[0].total,null);

                    } catch (e) {
                        connection.end();
                        callback(null, null, {'code': e.code, 'message': e.message});
                    }
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param ticket_type_id
 * @param callback
 * @description Mostra um bilhete
 */
exports.get_one_ticket_bought = function (ticket_id, callback) {

  var connection = mysql_connection.conncection();

  connection.query("CALL get_one_ticket_bought("+ ticket_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.end();
                callback(results[0], null);

            } else {

                connection.end();
                callback(null,{'code': '400', 'message': "Ticket don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

}

/**
 *
 * @param route_id
 * @param price
 * @param currency
 * @param ticket_type
 * @param validate_date
 * @param route
 * @param departure_local
 * @param arrivel_local
 * @param departure_hour
 * @param arrivel_hour
 * @param user_name
 * @param user_email
 * @param type_user
 * @param callback
 * @description Insere um bilhete
 */
exports.insert_ticket = function(prod_cod, validate_date ,departure_local, arrivel_local, user_name, user_email, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_prod(" + prod_cod + ")", function (error, prod, fields) {

        if (!error) {

            console.log(prod);
            if (prod[0].length !== 0) {

                connection.query("CALL get_one_ticket_config(" + prod[0][0].route_id + ")", function (error, ticket, fields) {

                    if (!error) {
                        connection.query("CALL count_tickets_by_day(" + prod[0][0].route_id + ",'" + validate_date + "')", function (error, count, fields) {

                            if (!error) {

                                if (ticket[0].length !== 0) {

                                    if (count[0][0].total !== ticket[0][0].max_to_buy) {

                                        var bought_date = moment().format('YYYY-MM-DD HH:mm:ss');

                                        var used = false;

                                             connection.query("CALL insert_ticket(" + prod[0][0].route_id + "," + prod[0][0].value + ",'" + prod[0][0].symbol + "','" + prod[0][0].type + "','" + validate_date + "','" + prod[0][0].name + "','" + departure_local + "','" + arrivel_local + "','" + user_name + "','" + user_email + "','" + prod[0][0].type_user + "'," + used + ",'" + bought_date + "')", function (error, results, fields) {

                                                 if (!error) {
                                                     connection.end();
                                                     callback(results[0], null);
                                                 } else {

                                                     connection.end();
                                                     callback(null, {'code': error.code, 'message': error.message});
                                                 }

                                             });

                                    } else {
                                        connection.end();
                                        callback(null, {
                                            'code': '900',
                                            'message': "Ticket purchase limit for this day has exceeded"
                                        });
                                    }

                                } else {
                                    connection.end();
                                    callback(null, {'code': '400', 'message': "The route don't have ticket config"});
                                }

                            } else {
                                connection.end();
                                callback(null, {'code': error.code, 'message': error.message});
                            }
                        });
                    } else {
                        connection.end();
                        callback(null, {'code': error.code, 'message': error.message});
                    }
                });

            } else {

                connection.end();
                callback(null, {'code': '400', 'message': "The prod don't exist"});
            }
        } else {
            connection.end();
          callback(null, {'code': error.code, 'message': error.message});
        }
    });
};

/**
 *
 * @param ticket_id
 * @param callback
 * @description Confirma o bilhete
 */
exports.confirm_ticket = function (ticket_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_ticket_bought("+ ticket_id + ")", function (error, results, fields) {

        if (!error) {
            var used = true;
            connection.query("CALL confirm_ticket(" + ticket_id + "," + used + ")", function (error, results, fields) {

                if (!error) {

                    if (results[0].length !== 0) {

                        connection.end();
                        callback(results[0], null);

                    } else {
                        connection.end();
                        callback(null, {'code': '400', 'message': "Ticket don't exist"});
                    }
                } else {
                    connection.end();
                    callback(null, {'code': error.code, 'message': error.message});
                }
            });
        }
    });

}
