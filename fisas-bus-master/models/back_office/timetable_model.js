"use strict";
var mysql_connection = require('../connection');
var async = require('async');

/**
 *
 * @param route_id
 * @param types_days_ids
 * @param season_id
 * @param callback
 * @description Mostra uma tabela horária
 */
exports.get_timetable = function(route_id, types_days_ids, season_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("Select * from route where id = " + route_id, function (error, results, fields) {

        if (!error) {
            if (results.length !== 0) {
                connection.query("CALL get_timetable(" + route_id + ",'" + types_days_ids + "'," + season_id + ")", function (error, results, fields) {

                    if (!error) {

                        try {
                            var table = [], season_id, route_line, type_day, type_day_id;

                            for (var i = 0; i < results[0].length; i++) {

                                if (results[0][i].line_number == 1) {

                                    type_day = results[0][i].type_day;
                                    type_day_id = results[0][i].type_day_id;
                                    route_line = results[0][i].route;
                                    season_id = results[0][i].season_id;
                                    table.push({
                                        local_id: results[0][i].local_id,
                                        local: results[0][i].local,
                                        region: results[0][i].region,
                                        instruction: results[0][i].instruction,
                                        hours: [{hour_id: results[0][i].id, hour: results[0][i].hour, line_number: results[0][i].line_number}]
                                    });

                                } else {
                                    table[results[0][i].order_table - 1].hours.push({
                                        hour_id: results[0][i].id,
                                        hour: results[0][i].hour,
                                        line_number: results[0][i].line_number
                                    });
                                }
                            }

                            if (!(table.length == 0)) {

                                connection.end();
                                callback({
                                    route_id: route_id,
                                    route_line: route_line,
                                    type_day: type_day,
                                    type_day_id: type_day_id,
                                    season_id: season_id,
                                    locals: table
                                }, null);
                            } else {
                                connection.end();
                                callback([], {code: 400, message: "Route ID "+ route_id +" don't have timetable for type day ID "+ types_days_ids +" and Season " + season_id + "."});
                            }
                        } catch (e) {
                            connection.end();
                            callback(null, {code: e.code, message: e.message});
                        }
                    } else {
                        connection.end();
                        callback(null, {code: error.code, message: error.message});
                    }
                });
            } else {
                connection.end();
                callback([], {code: 400, message: "Route don't exist"});
            }
        } else {
            connection.end();
        callback([], {code: error.code, message: error.message});
        }
    });

};

/**
 *
 * @param hour
 * @param local
 * @param type_day
 * @param line_number
 * @param route
 * @param season
 * @param order
 * @param callback (error)
 * @description Insere uma linha na tabela horária
 */
exports.insert_line = function(hour, local, type_day, line_number, route, season, order, callback){

    var connection = mysql_connection.conncection();

    var time;
    if (hour !== null) {
        time = hour.replace(/:/g, "");
    } else {
       time = null;
    }
    connection.query("CALL insert_timetable_line("+ time + "," + local + "," + type_day + "," + line_number + "," + route + ","  + season + "," + order + ")", function (error, results, fields) {

        if(error) {
            connection.end();
            callback({code: error.code, message: error.message});
        } else {
            console.log("Inserida uma Hora");
            connection.end();
            callback(null);
        }
    });

};

/**
 *
 * @param route_id
 * @param type_day_id
 * @param season_id
 * @param callback
 * @description Elimina uma tabela
 */
exports.delete_timetable = function(route_id, type_day_id, season_id ,callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL check_timetable(" + route_id + "," + season_id + "," + type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL delete_timetable(" + route_id + "," + season_id + "," + type_day_id + ")", function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback({code: error.code, message: error.message});
                    } else {
                        connection.end();
                        callback(null);
                    }
                });

            } else {
                connection.end();
                callback({code: 400, message: "Timetable don't exist"});
            }
        } else {
            connection.end();
            callback({code: error.code, message: error.message});
        }
    })


};


/**
 *
 * @param route_id
 * @param type_day_id
 * @param season_id
 * @param line_number
 * @param callback
 * @description Elimina uma linha da tabela
 */
exports.delete_timetable_line = function(route_id, type_day_id, season_id , line_number, callback){

     var connection = mysql_connection.conncection();

     connection.query("CALL check_timetable_line(" + route_id + "," + season_id + "," + type_day_id + "," + line_number + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL delete_timetable_line(" + route_id + "," + season_id + "," + type_day_id + "," + line_number + ")", function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback({code: error.code, message: error.message});
                    } else {
                        connection.end();
                        callback(null);
                    }
                });

            } else {
                connection.end();
                callback({code: 400, message: "Timetable line don't exist"});
            }
        } else {
            connection.end();
            callback({code: error.code, message: error.message});
        }
    })

};


/**
 *
 * @param hour
 * @param timetable_id
 * @param callback (status , error)
 * @description Actuliza uma hora de passagem da tabela horária
 */
exports.update_hour = function(hour, timetable_id, callback){

    var connection = mysql_connection.conncection();

    if (hour !== null) {
        connection.query("CALL update_hour('" + hour + "'," + timetable_id + ")", function (error, results, fields) {

            if (error) {
                connection.end();
                callback(null, {code: error.code, message: error.message});
            } else {
                console.log("Actualizada a hora de passagem");

                connection.end();
                callback(results[0], null);
            }
        });
    } else {
        connection.query("CALL update_hour("+ hour + "," +timetable_id +")", function (error, results, fields) {

            if (error) {
                connection.end();
                callback(null, {code: error.code, message: error.message});
            } else {
                console.log("Actualizada a hora de passagem");
                connection.end();
                callback(results[0], null);
            }
        });
    }


};

/**
 *
 * @param route_id
 * @param season_id
 * @param type_day_id
 * @description Verifica a linha atual da tabela
 */
exports.line_state = function(route_id ,season_id, type_day_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("SELECT max(line_number) as line FROM timetable WHERE route_id = "+ route_id + " and season_id=" + season_id + " and type_day_id =" + type_day_id, function (error, results, fields) {

        if (error) {
            connection.end();
            callback(null , error);
        } else {
            if(results[0].line !== null) {
                connection.end();
                callback(parseInt(results[0].line), null);
            } else {
                connection.end();
                callback(null , null);
            }
        }
    });

}
