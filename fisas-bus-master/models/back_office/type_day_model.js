"use strict";
var mysql_connection = require('../connection');
var _ = require('lodash');

/**
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostras os tipos de dia
 */
exports.get_types_days = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    var type_day_data = [];
    var aux_type_day_id, index;

    connection.query("Select COUNT(*) as total FROM type_day",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("CALL get_type_day('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                if (!error) {
                    results[0] =_.orderBy(results[0], ['type_day_id'], [order_by]);

                    for (var i = 0; i < results[0].length; i++) {

                        if (i === 0) {
                            aux_type_day_id = results[0][i].type_day_id;
                            type_day_data.push({
                                "id": aux_type_day_id,
                                "type_day": results[0][i].name,
                                "days_week": []
                            });
                        }

                        if (aux_type_day_id === results[0][i].type_day_id) {

                            index = _.findIndex(type_day_data, ['id', aux_type_day_id]);

                            type_day_data[index].days_week.push({
                                "id": results[0][i].day_id,
                                "day": results[0][i].day
                            });
                        } else {

                            aux_type_day_id = results[0][i].type_day_id;
                            type_day_data.push({
                                "id": aux_type_day_id,
                                "type_day": results[0][i].name,
                                "days_week": [{
                                    "id": results[0][i].day_id,
                                    "day": results[0][i].day
                                }]
                            });
                        }
                    }

                    connection.end();
                    callback(type_day_data, total[0].total, null);

                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }

            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param type_day_id
 * @param callback
 * @description Mostra um tipo de dia
 */
exports.get_one_type_day = function(type_day_id, callback) {

    var type_day_data = [];
    var aux_type_day_id, index;

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_day("+ type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                for (var i = 0; i < results[0].length; i++) {

                    if (i === 0) {
                        aux_type_day_id = results[0][i].type_day_id;
                        type_day_data.push({
                            "id": aux_type_day_id,
                            "type_day": results[0][i].name,
                            "days_week": []
                        });
                    }

                    if (aux_type_day_id === results[0][i].type_day_id) {

                        index = _.findIndex(type_day_data, ['id', aux_type_day_id]);

                        type_day_data[index].days_week.push({
                            "id": results[0][i].day_id,
                            "day": results[0][i].day
                        });
                    } else {

                        aux_type_day_id = results[0][i].type_day_id;
                        type_day_data.push({
                            "id": aux_type_day_id,
                            "type_day": results[0][i].name,
                            "days_week": [{
                                "id": results[0][i].day_id,
                                "day": results[0][i].day
                            }]
                        });
                    }
                }

                connection.end();
                callback(type_day_data, null);

            } else {
                connection.end();
               callback(null,{'code': '400', 'message': "Type day don't exist"});
            }
        } else {
            connection.end();
            callback(null, null, {'code': error.code, 'message': error.message});
        }
    });

};

/**
 * @param callbakc
 * @description Mostra os dias da semena
 */
exports.get_days_weeks = function(callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL get_days_weeks()",function (error, results, fields) {

        if (!error) {

            connection.end();
            callback(results[0], null);

        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    })

};

/**
 *
 * @param route_id
 * @param callback
 * @description Tipo de dias que uma rota tem
 * Alterar
 */
exports.route_types_days = function(route_id, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL route_types_days(" + route_id + ")",function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0], null);
        } else {
            connection.end();
            callback(null, error);
        }
    })

};


/**
 *
 * @param type_day
 * @param days_week_ids
 * @param callback (error)
 * @description Insere um tipo de dia
 */
exports.insert_type_day = function(type_day, days_week_ids, callback){

    var type_day_id;

    var connection = mysql_connection.conncection();

    connection.query("CALL check_days_week('" + days_week_ids.toString() + "')",function (error, results, fields) {

        if (!error) {

            var days_ids_result = [];

            for (var i = 0; i < results[0].length; i++) {

                days_ids_result.push(results[0][i].id);
            }

            var diff =_.difference(days_week_ids, days_ids_result);

            if (diff.length === 0) {
                  connection.beginTransaction( function(err) {

        if (err) { connection.end();callback(null, err); }

        connection.query("INSERT INTO type_day (name) VALUES ('" + type_day + "')",function (error, results, fields) {
            if (error) {
                return connection.rollback(function() {
                    connection.end();
                    callback(null, error);
                });
            }

            type_day_id = results.insertId;

            var day_group = []
            for (var i = 0; i < days_week_ids.length; i++) {

                day_group.push([days_week_ids[i], results.insertId]);

            }

            connection.query('INSERT INTO day_group (day_id, type_day_id) VALUES ?',[day_group],function (error, results, fields) {
                if (error) {
                    return connection.rollback(function () {
                        connection.end();
                        callback(null ,error);
                    });
                }

                connection.commit(function (err) {
                    if (err) {
                        return connection.rollback(function () {
                            connection.end();
                            callback(null, err);
                        });
                    }
                    console.log('Tipo de dia inserido com sucesso!');
                    connection.end();
                    callback(type_day_id, null);
                });
            });
        });

    });
            } else {
                connection.end();
                callback(null,{'code': 400 , 'message': "The days week "+ diff.toString() + " don't exist"});
            }
        } else {
            connection.end();
            callback(null,{'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param type_day_id
 * @param day_id
 * @param callback
 * @description Adiciona um dia da semana no tipo de dia
 */
exports.add_day_week = function(type_day_id, day_id, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_day("+ type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL get_one_day_week("+ day_id + ")", function (error, results, fields) {

                    if (!error) {

                        if (results[0].length !== 0) {

                            connection.query("CALL check_day_group(" + type_day_id + "," + day_id + ")", function (error, results, fields) {

                                if(!error) {

                                    if (results[0].length === 0) {
                                        connection.query("CALL add_day_week(" + type_day_id + "," + day_id + ")", function (error, results, fields) {


                                            if (!error) {
                                                connection.end();
                                                callback(null);

                                            } else {
                                                connection.end();
                                                callback({'code': error.code, 'message': error.message});
                                            }
                                        });
                                    } else {
                                        connection.end();
                                        callback({'code': '150', 'message': "Already exists the day associated with a type of day"});
                                    }
                                } else {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                }

                            });
                        } else {
                            connection.end();
                            callback({'code': '400', 'message': "Day week don't exist"});
                        }
                    } else {
                        connection.end();
                        callback({'code': error.code, 'message': error.message});
                    }
                });
            } else {
                connection.end();
                callback({'code': '400', 'message': "Type day don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });
};


/**
 *
 * @param type_day_id
 * @param day_id
 * @param callback
 * @description Elimina um dia da semana
 */
exports.delete_day_week = function(type_day_id, day_id, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_day("+ type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL get_one_day_week("+ day_id + ")", function (error, results, fields) {

                    if (!error) {

                        if (results[0].length !== 0) {

                            connection.query("CALL check_day_group(" + type_day_id + "," + day_id + ")", function (error, results, fields) {

                                if(!error) {

                                    if (results[0].length !== 0) {
                                        connection.query("CALL delete_day_week(" + type_day_id + "," + day_id + ")", function (error, results, fields) {

                                            if (!error) {
                                                connection.end();
                                                callback(null);

                                            } else {
                                                connection.end();
                                                callback({'code': error.code, 'message': error.message});
                                            }
                                        });
                                    } else {
                                        connection.end();
                                        callback({'code': '150', 'message': "Not exist this day associated with a type of day"});
                                    }
                                } else {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                }

                            });
                        } else {
                            connection.end();
                            callback({'code': '400', 'message': "Day week don't exist"});
                        }
                    } else {
                        connection.end();
                        callback({'code': error.code, 'message': error.message});
                    }
                });
            } else {
                connection.end();
                callback({'code': '400', 'message': "Type day don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * 
 * @param type_day_id
 * @param callback
 * @description Elimina um tipo de dia 
 */
exports.delete_type_day = function(type_day_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_day(" + type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {
                connection.query("select Distinct(type_day_id) from timetable where type_day_id=" + type_day_id, function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback({'code': error.code, 'message': error.message});
                    } else {
                        if (results.length === 0) {

                            connection.query("CALL delete_type_day(" + type_day_id + ")", function (error, results, fields) {

                                if (error) {
                                    connection.end();
                                    callback({'code': error.code, 'message': error.message});
                                }
                                else {
                                    console.log("Tipo de dia eliminado");
                                    connection.end();
                                    callback(null);
                                }
                            });
                        } else {
                            connection.end();
                            callback({
                                "code": 700, "message": "Type Day ID " + type_day_id + " is associated with a route."
                            });
                        }
                    }
                });
            } else {
                connection.end();
                callback({"code": 400, "message": "Type day ID " + type_day_id + " is not exist."})
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 * @param type_day_id
 * @param name 
 * @param callback 
 * @description Altera o tipo de dia
 */
exports.update_type_day = function(type_day_id, name, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_day(" + type_day_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL update_type_day("+ type_day_id + ",'" + name + "')", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, error);
                    } else {
                        connection.end();
                       callback(results[0], null);
                    }

                });

            } else {
                connection.end();
                callback({'code': '400', 'message': "Type day don't exist"});
            }
        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};



