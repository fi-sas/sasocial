"use strict";
var mysql_connection = require('../connection');


/**
 * 
 * @param callback
 * @description Mostras os tipo de utilizador
 */
exports.get_types_users = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM type_user",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }
                connection.query("CALL get_types_users('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {

                    if (!error) {
                        connection.end();
                        callback(results[0], total[0].total,error);
                    } else {
                        if (error.code === "ER_BAD_FIELD_ERROR") {
                            connection.end();
                            callback(null, null,{
                                "code": 300,
                                "message": columnOrder + " not a exist to sort."
                            });
                        } else {
                            connection.end();
                            callback(null, null,{'code': error.code, 'message': error.message});
                        }
                    }
                });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param type_user_id
 * @param callback
 * @description Mostra um tipo de utilizador
 */
exports.get_one_type_user = function(type_user_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_type_user(" + type_user_id + ")", function (error, results, fields) {

        if (!error) {
            if (results[0].length !== 0) {
                connection.end();
                callback(results[0], error);
            } else {
                connection.end();
                callback(null, {'code': '400', 'message': "Type User don't exist"});
            }
        } else {
            connection.end();
            callback(null, {'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param user
 * @param callback (error)
 * @description Insere um tipo de utilizador
 */
exports.insert_type_user = function(user, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL insert_type_user('"+ user +"')", function (error, results, fields) {

        if (!error) {
            connection.end();
            callback(results[0],null);
        } else {
            connection.end();
            callback(null, error);
        }

    });
};

/**
 *
 * @param season_id
 * @param callback
 * @description Elimina um período
 */
exports.delete_type_user = function(type_user_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_type_user(" + type_user_id + ")", function (error, results, fields) {

    if (!error) {

        if (results[0].length !== 0) {

            connection.query("select Distinct(type_user_id) from price where type_user_id = " + type_user_id, function (error, results, fields) {

                if (error) {
                    connection.end();
                    callback(null ,error);
                } else {

                    if (results.length === 0) {
                        connection.query("CALL delete_type_user(" + type_user_id + ")", function (error, results, fields) {

                            if (error) {
                                connection.end();
                                callback(null, error);
                            } else {
                                console.log("Tipo de utlizador eliminado!");
                                connection.end();
                                callback("DELETED" ,null);
                            }
                        });
                    } else {
                        connection.end();
                        callback("RELATED", null);
                    }
                }
            });
        } else {
            connection.end();
            callback("NOT EXIST",null);
        }
    } else {
        connection.end();
        callback(null, error)
    }
    });

};

/**
 * @param type_user_id
 * @param user 
 * @param callback 
 * @description Altera um tipo de utilizador
 */
exports.update_type_user = function(type_user_id, user, callback){

   var connection = mysql_connection.conncection();

   connection.query("CALL get_one_type_user(" + type_user_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL update_type_user('" + user + "'," + type_user_id + ")", function (error, results, fields) {

                    if (error){
                        connection.end();
                        callback(null, error);
                    } else {
                       console.log("Tipo de user alterado");

                       connection.end();
                       callback(results[0], null);
                    }

                });

            } else {
                connection.end();
                callback(null,{
                    "code": 400,
                    "message": "Type User ID " + type_user_id + " is not exist."
                });
            }
        } else {
            connection.end();
            callback(null, error)
        }
    });

};