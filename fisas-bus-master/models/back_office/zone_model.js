"use strict";
var mysql_connection = require('../connection');
var _= require('lodash');

/**
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostras as zonas
 */
exports.get_zones = function(limit, offset, columnOrder, order_by,callback) {

    var zones_ids = [], zones = [];
    var zone_info, zone_index;

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM zone",function (error, total, fields) {

       if (!error) {
           var limit_offset;
           if (limit != -1) {
               limit_offset = "limit "+ limit + " offset "+offset;
           } else {
               limit_offset = "";
           }

            connection.query("CALL get_zones('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {


                if (!error) {

                    for (var i = 0; i < results[0].length; i++) {

                        if (zones_ids.indexOf(results[0][i].zone_id) === -1) {

                            zone_info = {
                                zone_id: results[0][i].zone_id,
                                name: results[0][i].name,
                                locals: [{local_id:results[0][i].local_id,
                                         local: results[0][i].local,
                                         region: results[0][i].region
                                         }]
                            };

                            zones_ids.push(results[0][i].zone_id);
                            zones.push(zone_info);

                        } else {
                            zone_index = _.findIndex(zones, {'zone_id': results[0][i].zone_id});

                            if (zone_index !== -1) {

                                zones[zone_index].locals.push({ local_id:results[0][i].local_id,
                                                                local: results[0][i].local,
                                                                region: results[0][i].region
                                                               });
                            }
                        }
                    }

                    connection.end();
                   callback(zones, total, null);

                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });

        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param zone_id
 * @param callback
 * @description Mostra uma zona
 */
exports.get_one_zone = function(zone_id, callback) {

    var zones_ids = [], zones = [];
    var zone_info, zone_index;

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_zone("+ zone_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                for (var i = 0; i < results[0].length; i++) {

                    if (zones_ids.indexOf(results[0][i].zone_id) === -1) {

                        zone_info = {
                            zone_id: results[0][i].zone_id,
                            name: results[0][i].name,
                            locals: [{
                                local_id: results[0][i].local_id,
                                local: results[0][i].local,
                                region: results[0][i].region
                            }]
                        };

                        zones_ids.push(results[0][i].zone_id);
                        zones.push(zone_info);

                    } else {
                        zone_index = _.findIndex(zones, {'zone_id': results[0][i].zone_id});

                        if (zone_index !== -1) {

                            zones[zone_index].locals.push({
                                local_id: results[0][i].local_id,
                                local: results[0][i].local,
                                region: results[0][i].region
                            });
                        }
                    }
                }

                connection.end();
                callback(zones, null);
            } else {
                connection.end();
                callback(null,{'code': '400', 'message': "Zone don't exist"});
            }

        } else {
            connection.end();
            callback(null,{'code': error.code, 'message': error.message});
        }
    });

};
/**
 *
 * @param zone_name
 * @param locals_ids
 * @param callback (error)
 * @description Insere uma zona
 */
exports.insert_zone = function(zone_name, route_id,locals_ids, callback){

    var zone_id;
    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_route(" + route_id + ")",function (error, results, fields) {

        if (!error) {

            if (results[0].length) {

                connection.query("CALL check_locals('" + locals_ids.toString() + "')", function (error, results, fields) {

                    if (!error) {

                        var locals_ids_result = [];


                        for (var i = 0; i < results[0].length; i++) {

                            locals_ids_result.push(results[0][i].id);
                        }

                        var diff = _.difference(locals_ids, locals_ids_result);

                        if (diff.length === 0) {

                            connection.beginTransaction(function (err) {

                                if (err) {
                                    connection.end();
                                    callback(null, {'code': err.code, 'message': err.message});
                                }

                                connection.query("INSERT INTO zone (name) VALUES ('" + zone_name + "')", function (error, results, fields) {
                                    if (error) {
                                        return connection.rollback(function () {
                                            connection.end();
                                            callback(null, {'code': error.code, 'message': error.message});
                                        });
                                    }

                                    zone_id = results.insertId;

                                    var order = [], locals_zones = [];
                                    for (var i = 0; i < locals_ids.length; i++) {

                                        order.push([locals_ids[i], results.insertId]);
                                        if (i !== locals_ids.length - 1) {
                                            locals_zones.push([locals_ids[i], results.insertId]);
                                        }
                                    }

                                    connection.query('INSERT INTO local_zone (local_id, zone_id) VALUES ?', [order], function (error, results, fields) {
                                        if (error) {
                                            return connection.rollback(function () {
                                                connection.end();
                                                callback(null, {'code': error.code, 'message': error.message});
                                            });
                                        }

                                        connection.query("INSERT INTO route_zone (zone_id, route_id) VALUES(" + zone_id + "," + route_id + ")", function (error, results, fields) {
                                            if (error) {
                                                return connection.rollback(function () {
                                                    connection.end();
                                                    callback(null, {'code': error.code, 'message': error.message});
                                                });
                                            }

                                            connection.commit(function (err) {
                                                if (err) {
                                                    return connection.rollback(function () {
                                                        connection.end();
                                                        callback(null, {'code': err.code, 'message': err.message});
                                                    });
                                                }
                                                console.log('Zona Inserida com sucesso!');
                                                connection.end();
                                                callback(zone_id, null);
                                            });
                                        });
                                    });
                                });

                            });

                        } else {
                            connection.end();
                            callback(null, {'code': 400, 'message': "The locals " + diff.toString() + " don't exist"});
                        }
                    } else {
                        connection.end()
                        callback(null, {'code': error.code, 'message': error.message});
                    }
                });
            } else {
                connection.end();
                callback(null,{'code': '400', 'message': "Route don't exist"});
            }
        } else {
            connection.end();
            callback(null,{'code': error.code, 'message': error.message});
        }
    });

};


/**
 *
 * @param zone_id
 * @param callback
 * @description Elimina uma zona
 */
exports.delete_zone = function(zone_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_zone("+ zone_id + ")", function (error, results, fields) {

        if (!error) {

            if (results[0].length !== 0) {

                connection.query("CALL delete_zone(" + zone_id + ")", function (error, results, fields) {

                        if(error) {
                            connection.end();
                            callback(error);
                        } else {
                            console.log("Zona Eliminada!");
                            connection.end();
                            callback(null);
                        }
                    });

            } else {
                connection.end();
                callback({'code': '400', 'message': "Zone don't exist"});
            }

        } else {
            connection.end();
            callback({'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param zone_id
 * @param route_id
 * @param callback
 * @description Elimina uma zona de uma rota
 */
exports.delete_zone_route = function(zone_id, route_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL delete_zone_route(" + zone_id + "," + route_id + ")", function (error, results, fields) {

        if(error) {
            connection.end();
            callback(null, error);
        } else {
            console.log("Zona Eliminada da rota!");

            connection.end();
            callback("DELETED", null);
        }
    });

};