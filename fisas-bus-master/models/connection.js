"use strict";
var mysql   = require('mysql');

/**
 * MySQL
 * @type {Connection|*}
 */

exports.conncection = function () {

    var configDB = {
        host     : process.env.MYSQL_HOST,
        user     : 'root',
        password : process.env.MYSQL_ROOT_PASSWORD,
        database : process.env.MYSQL_DATABASE,
        multipleStatements: true
    }

    var connection = mysql.createConnection(configDB);

    return connection;
};

