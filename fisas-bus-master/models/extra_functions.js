"use strict";
var mysql_connection = require('./connection');


/**
 *
 * @param day
 * @description Verifica os tipos de dias referentes consonte um dia
 */
exports.surch_types_days = function(day , callback) {

    var types_days = [];
    var connection = mysql_connection.conncection();

    connection.query("CAll surch_types_days("+ day +")", function (error, results, fields) {


        if (error) {
            connection.end();
            callback(null , error);
        } else {
            if(results[0] !== null) {

                for (var i =  0; i < results[0].length; i++) {
                    types_days.push(results[0][i].type_day_id);
                }
                connection.end();
                callback(types_days, null);
            } else {
                connection.end();
                callback(null , null);
            }
        }
    });

}