'use strict';

var mysql_connection = require('./connection');

/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostras os meses de pagamentos
 */
exports.get_payment_months = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM payment_months",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("SELECT * FROM payment_months ORDER BY " + columnOrder + " " + order_by+" "+ limit_offset, function (error, results, fields) {
                if (!error) {

                    connection.end();
                    callback(results, total[0].total, error);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};

/**
 *
 * @param application_id
 * @param callback
 * @description Mostra os pagamentos de uma candidatura
 */
exports.get_application_payment_month = function(application_id, callback) {

    var connection = mysql_connection.conncection();
 
    connection.query("SELECT * FROM payment_months where application_id = " + application_id, function (error, results, fields) {
 
         if (!error) {
 
              connection.end();
              callback(results, error);
        
         } else {
             connection.end();
             callback(null, {'code': error.code, 'message': error.message});
         }
     });
 
 };

 /**
 *
 * @param payment_month_id
 * @param callback
 * @description Mostra um pagamento
 */
exports.get_one_payment_month = function(payment_month_id, callback) {

  var connection = mysql_connection.conncection();

  connection.query("SELECT * FROM payment_months where id = " + payment_month_id, function (error, results, fields) {

       if (!error) {

        console.log(results);

           if (results.length !== 0) {

               connection.end();
               callback(results[0], error);
           } else {
               connection.end();
               callback(null, {'code': '400', 'message': "Payment month don't exist"});
           }
       } else {
           connection.end();
           callback(null, {'code': error.code, 'message': error.message});
       }
   });

};

 /**
  * 
  * @param {*} application_id 
  * @param {*} month 
  * @param {*} year 
  * @param {*} value 
  * @param {*} paid
  * @param {*} created_at 
  * @param {*} updated_at 
  * @param {*} callback 
  * @description Insere uma pagamento por mês
  */
exports.insert_payment_month = function (
  application_id,
  month,
  year,
  value,
  paid,
  created_at,
  updated_at,
  callback
) {
  var connection = mysql_connection.conncection();

  connection.query(
    "INSERT INTO payment_months (application_id, month, year, value, paid, created_at, updated_at) VALUES(" +
      application_id +
      ", '" +
      month +
      "' , " +
      year +
      "," +
      value +
      "," +
      paid +
      ",'" + 
      created_at +
      "','" + 
      updated_at +"')",
    function (error, results, fields) {
      try {
        if (!error) {
          connection.query("SELECT * FROM payment_months where id = "+ results.insertId, function (error, results, fields) {   
            if (!error) {
              connection.end();
              callback(results[0], null);
            } else {
              connection.end();
              callback(null, { code: error.code, message: error.message });
            }
          
          });
        } else {
          connection.end();
          callback(null, { code: error.code, message: error.message });
        }
      } catch (e) {
        connection.end();
        callback(null, { code: e.code, message: e.message });
      }
    }
  );
};

 /**
  * 
  * @param {*} payment_month_id 
  * @param {*} application_id 
  * @param {*} month 
  * @param {*} year 
  * @param {*} value 
  * @param {*} paid
  * @param {*} updated_at 
  * @param {*} callback 
  * @description Atualiza um pagamento mensal
  */
 exports.update_payment_month = function (
  payment_month_id,
  application_id,
  month,
  year,
  value,
  paid,
  updated_at,
  callback
  ) {
    var connection = mysql_connection.conncection();
  
    connection.query(
      "UPDATE payment_months SET " +
      "application_id = "+ application_id + 
      ", month = '" + month + 
      "', year = " + year + 
      ", value = " + value + 
      ", paid = " + paid + 
      ", updated_at = '" + updated_at +
      "' WHERE id = "+ payment_month_id,
      function (error, results, fields) {
        try {
          if (!error) {
            connection.query("SELECT * FROM payment_months where id = " + payment_month_id, function (error, results, fields) {  
              
              if (!error) {
                connection.end();
                callback(results[0], null);
              } else {
                connection.end();
                callback(null, { code: error.code, message: error.message });
              }
            
            })
          } else {
            connection.end();
            callback(null, { code: error.code, message: error.message });
          }
        } catch (e) {
          connection.end();
          callback(null, { code: e.code, message: e.message });
        }
      }
    );
};

/**
 *
 * @param payment_month_id
 * @param callback
 * @description Elimina um pagamento mensal
 */
exports.delete_payment_month = function(payment_month_id, callback){

   var connection = mysql_connection.conncection();

   connection.query("SELECT * FROM payment_months where id = "+ payment_month_id, function (error, results, fields) {

        if (!error) {

            if (results.length !== 0) {
                connection.query("DELETE FROM payment_months WHERE id =" + payment_month_id, function (error, results, fields) {
                    if (error) {
                        connection.end();
                        callback(null, error);
                    }
                    else {
                        console.log("Payment month Deleted!");
                        connection.end();
                        callback("DELETED", null);
                    }
                });
            } else {
             connection.end();
             callback("NOT EXIST",null);
            }
            } else {
            connection.end();
            callback(null, error)
        }
    });

};
