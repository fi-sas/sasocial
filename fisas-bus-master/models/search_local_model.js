"use strict";

var mysql_connection = require('./connection');


/**
 *
 * @param local
 * @param callback
 * @description Procura o local pelo texto
 */
exports.search_local = function(local, callback) {

    var connection = mysql_connection.conncection();

    connection.query("select * FROM local as loc where loc.local LIKE '"+local+"%'", function (error, results, fields) {

        try {
            if (!error) {
                connection.end();
                callback(results, null);
            } else {
                connection.enc();
                callback(null, {'code': error.code, 'message': error.message});
            }

        } catch (e) {
            connection.end();
            callback(null, {'code': e.code, 'message': e.message});
        }

    });

};

/**
 *
 * @param place
 * @param callback
 * @description Procura um concelho pelo texto
 */
exports.search_place = function(place, callback) {

    var connection = mysql_connection.conncection();

    connection.query("select * FROM place as pla where pla.local LIKE '"+place+"%'", function (error, results, fields) {

        try {
            if (!error) {
                connection.end();
                callback(results, null);
            } else {
                connection.enc();
                callback(null, {'code': error.code, 'message': error.message});
            }

        } catch (e) {
            connection.end();
            callback(null, {'code': e.code, 'message': e.message});
        }

    });

};