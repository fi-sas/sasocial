"use strict";

var mysql_connection = require('./connection');


/**
 *
 * @param place
 * @param callback
 * @description Procura um concelho pelo texto
 */
exports.search_place = function(place, callback) {


    var connection = mysql_connection.conncection();

    connection.query("select * FROM place  where place.place LIKE '"+place+"%'", function (error, results, fields) {

        try {
            if (!error) {
                connection.end();
                callback(results, null);
            } else {
                connection.end();
                callback(null, {'code': error.code, 'message': error.message});
            }

        } catch (e) {
            connection.end();
            callback(null, {'code': e.code, 'message': e.message});
        }

    });

};

/**
 *
 * @param limit
 * @param offset
 * @param columnOrder
 * @param order_by
 * @param callback
 * @description Mostra todos os concelhos
 */
exports.get_places = function(limit, offset, columnOrder, order_by, callback) {

    var connection = mysql_connection.conncection();

    connection.query("Select COUNT(*) as total FROM place",function (error, total, fields) {

        if (!error) {
            var limit_offset;
            if (limit != -1) {
                limit_offset = "limit "+ limit + " offset "+offset;
            } else {
                limit_offset = "";
            }

            connection.query("CALL get_places('"+ limit_offset + "','" + columnOrder + "','" + order_by + "')", function (error, results, fields) {
                if (!error) {

                    connection.end();
                    callback(results[0], total[0].total, error);
                } else {
                    if (error.code === "ER_BAD_FIELD_ERROR") {
                        connection.end();
                        callback(null, null,{
                            "code": 300,
                            "message": columnOrder + " not a exist to sort."
                        });
                    } else {
                        connection.end();
                        callback(null, null,{'code': error.code, 'message': error.message});
                    }
                }
            });
        } else {
            connection.end();
            callback(null, null,{'code': error.code, 'message': error.message});
        }
    });

};