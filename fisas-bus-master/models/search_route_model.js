"use strict";
var mysql_connection = require('./connection');
var moment  = require('moment');
var async   = require("async");
var _ = require('lodash');

/**
 *
 * @param latitude
 * @param longitude
 * @param callback -> (locals, error) : locals -> locais próximos ao ponto
 *                                    : error -> Erro (null por padrão)
 */
exports.nearby_local = function (latitude, longitude, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL nearby_local(" + latitude + "," + longitude + ")", function (error, locals, fields) {

        if (!error) {

             connection.end();
             callback(locals[0], null);

        } else {

            connection.end();
            callback([], error)
        }
    });

}

/**
 * 
 * @param route 
 * @param type_day 
 * @param season 
 * @param callback
 * @description Pega numa tabela horária
 */
exports.pick_timetables = function(route, type_day, season, callback) {

     var connection = mysql_connection.conncection();

     connection.query("CALL timetable(" + route + ",'" + type_day + "','" + season + "')", function (error, results, fields) {

        if (!error) {

         var header = [], rows = [], rows_aux = [], table = [], locals_ids = [], instruction = [], coordinates = [], route_line,external, regions = [];

           try {

                for (var i = 0; i < results[0].length; i++) {

                    if (results[0][i].line_number === 1) {

                        route_line = results[0][i].name;
                        external = results[0][i].external;
                        locals_ids.push(results[0][i].local_id);
                        instruction.push(results[0][i].instruction);
                        coordinates.push([results[0][i].latitude, results[0][i].longitude]);
                        regions.push(results[0][i].region);
                        header.push(results[0][i].local);
                        rows.push(results[0][i].hour);

                    } else {
                        if (header.length == results[0][i].order_table) {
                            rows_aux.push(results[0][i].hour);
                            table.push(rows_aux);
                            rows_aux = [];
                        } else {
                            rows_aux.push(results[0][i].hour);
                        }
                    }
                }
                if (!(rows.length == 0 && header.length == 0)) {

                    table.unshift(rows);
                    table.unshift(header);


                    connection.end();
                    callback({
                        route: route,
                        regions: regions,
                        locals_ids: locals_ids,
                        instructions: instruction,
                        coord: coordinates,
                        table: table,
                        route_line: route_line,
                        external: external
                    }, "OK", null);

                } else {
                    connection.end();
                    callback(null, "NOT_EXIST", null);
                }
           } catch(e) {
               connection.end();
              callback(null, "ERROR", e);
           }

        } else {
            connection.end();
            callback(null, "ERROR", error);
        }
    })

};

/**
 *
 * @param route_id -> Rota não encontrada é retornada um array vazio
 * @param callback
 * @description Encontra as distâncias de uma rota
 */
exports.distance_by_route = function(route_id, callback){


    var connection = mysql_connection.conncection();

    connection.query("CALL distances(" + route_id + ")", function (error, results, fields) {

    if (!error) {

        connection.end();
        callback(results[0], null);

    } else {

        connection.end();
        callback([], null);
    }
    });

};

/**
 *
 * @param route_id
 * @param callback
 * @description Retorna um contacto de um horário
 */
exports.contacts = function(route_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL contacts(" + route_id + ")", function (error, results, fields) {

        var contacts = [];

        if (error) {
            connection.end();
            callback(null,error);
        } else {

            contacts.push(
                {
                    "name": results[0][0].name,
                    "phone": results[0][0].phone,
                    "url": results[0][0].website
                }
            );
            connection.end();
            callback(contacts, null);
        }
    });

};

/**
 * @param route_id
 * @param callback
 * @description Retorna as zonas de um rota
 */
exports.find_zones_of_route = function (route_id, callback) {

   var connection = mysql_connection.conncection();

   connection.query("CALL zones_of_route(" + route_id + ")", function (error, zones, fields) {
        if (!error) {

            connection.end();
            callback(zones[0], null);
        } else {
            connection.end();
            callback([], error)
        }
    });

};

/**
 *
 * @param routes_ids -> ids ou id da rota(s) iniciais
 * @param destination -> destino final da rota
 * @param callback - > (route_complete, error) : route_complete -> rota completa
 *                                             : error -> Erro (null por padrão)
 * @description Encontra as ligações entre as rotas até chegar ao local de destino
 */
exports.routes_links = function (routes_ids, origin, destination, callback) {

            var is_find, head = 0, head_index;
            var i = 0, j = 0
            var aux_routes = [], aux_routes_ids = [], route_tree = [];

            var connection = mysql_connection.conncection();

              var find = false;
              async.during(
                  function (callback) {

                      /* Verifica as ligações da rota:
                          id_ route - > rota de entrada
                          rota2_id -> Rota que liga com o id_route
                          ponto_rota1_id -> id do ponto da id_route
                          ponto_rota2_id -> id da rota
                      */

                      connection.query("select route2_id, local_route1_id, local_route2_id from links where route1_id =" + routes_ids[j], function (db_error_rt, route_links, fields_rt) {

                          //Elimina a rota anterior das ligações seguintes para não voltar atrás
                          if (head !== 0) {

                              head_index =  _.findIndex(route_links, ['route2_id', head]);

                              if (head_index !== -1) {
                                  route_links.splice(head_index, 1);
                              }
                          }

                          if (!db_error_rt) {
                              if (route_links.length !== 0) {

                                  //Busca os pontos da rota
                                  connection.query("CALL route_test(" + route_links[i].route2_id + ")", function (db_error_r, route, fields_r) {

                                          if (!db_error_r) {

                                           if (route.length !== 0) {

                                               try {
                                                  // Verifica se o destino existe nesta rota
                                                  is_find = _.find(route[0], ['id', parseInt(destination)]);

                                                  // Se não exite guarda a rota
                                                  if (is_find === undefined) {

                                                      //Guarda a rota
                                                      aux_routes.push(route_links[i].route2_id);
                                                      aux_routes_ids.push({"route":route_links[i].route2_id, "begin_local_id":route_links[i].local_route1_id});

                                                      i++; // avança para outra rota

                                                      if (i < route_links.length) {
                                                          return callback(null, true);
                                                      } else {

                                                          //route_tree.push();
                                                          head = routes_ids[j];

                                                          j++;
                                                          i = 0;
                                                          if (!(j < routes_ids.length)) {
                                                              j = 0;
                                                              routes_ids = aux_routes;
                                                              route_tree.push( { "head" : head ,"branches": aux_routes});
                                                          }

                                                          return callback(null, true);
                                                      }
                                                  } else {
                                                      find = true;
                                                      aux_routes_ids.push({"route":route_links[i].route2_id,"begin_local_id":route_links[i].local_route2_id});
                                                      route_tree.push( { "head" : routes_ids[j] , "branches": [route_links[i].route2_id]});

                                                      return callback(null, false);
                                                  }
                                               } catch(e) {
                                                   return callback(e, false);
                                               }

                                          } else {
                                              return callback(db_error_r, false);
                                          }
                                      } else {
                                          return callback(null, false);
                                      }
                                  });

                                  } else {
                                      // RESULTS length === 0

                                      return callback(null, false);
                                  }

                              } else {
                                  return callback(db_error_rt, false);
                              }


                      });
                  },
                  function (callback) {
                      callback();
                  },
                  function (err) {

                     if (!err) {


                         if (route_tree.length !== 0 && find) {

                             connection.query("select route2_id, local_route1_id, local_route2_id from links where route1_id =" + route_tree[0].head, function (db_error_rt, route_links, fields_rt) {


                                 if (!db_error_rt) {

                                     var route_complete = [], index;

                                     try {

                                         for (var l = 0; l < route_links.length; l++) {

                                             aux_routes_ids.push({
                                                 "route": route_tree[0].head,
                                                 "begin_local_id": origin
                                             });
                                         }
                                         for (var i = route_tree.length - 1; i >= 0; i--) {
                                             if (route_tree.length - 1 === i) {
                                                 route_complete.push(_.find(aux_routes_ids, ['route', route_tree[i].head]), _.find(aux_routes_ids, ['route', route_tree[i].branches[0]]));
                                                 head = route_tree[i].head;
                                             } else {
                                                 index = _.indexOf(route_tree[i].branches, head);
                                                 if (index !== -1) {
                                                     route_complete.unshift(_.find(aux_routes_ids, ['route', route_tree[i].head]));
                                                     head = route_tree[i].head;
                                                 }
                                             }
                                         }

                                         connection.end();
                                         callback(route_complete, null);

                                     } catch (e) {
                                         connection.end();
                                         callback([], e);
                                     }

                                 } else {
                                     connection.end();
                                     callback([], db_error_rt);
                                 }
                             });
                         } else {
                             connection.end();
                             callback([], null);
                         }

                      } else {
                         connection.end();
                          callback([], err);
                      }
                  }
              );
  }

/**
 *
 * @param origin
 * @param destination
 * @param callback (locals, error)
 * @description Encontra as rotas que têm o local de origem e o do destino
 */
exports.find_routes = function(origin, destination, callback) {

        var locals = [], local_1 = [], local_2 = [];

        var connection = mysql_connection.conncection();

        if ( !isNaN(origin) && !isNaN(destination)) {

            connection.query("CALL locals_on_route(" + origin + "," + destination + ")", function (error, results, fields) {

                if (!error) {

                    try {
                        for (var i = 0; i < results[0].length; i++) {

                            if (results[0][i].local_id == origin) {
                                local_1.push(results[0][i]);
                            }

                            if (results[0][i].local_id == destination) {
                                local_2.push(results[0][i]);
                            }
                        }
                        var j;
                        if (local_1.length < local_2.length) {

                            for (i = 0; i < local_1.length; i++) {
                                for (j = 0; j < local_2.length; j++) {
                                    if (local_1[i].route_id == local_2[j].route_id) {
                                        if (local_1[i].order_table < local_2[j].order_table) {
                                            locals.push([{route: local_1[i].route_id, begin_local_id: origin}]);
                                        }
                                    }
                                }
                            }
                        } else {
                            for (i = 0; i < local_2.length; i++) {

                                for (j = 0; j < local_1.length; j++) {
                                    if (local_2[i].route_id == local_1[j].route_id) {
                                        if (local_2[i].order_table > local_1[j].order_table) {
                                            locals.push([{route: local_2[i].route_id, begin_local_id: origin}]);
                                        }
                                    }
                                }
                            }
                        }
                        connection.end();

                        callback(locals, null);
                    } catch (e) {
                        connection.end();
                        callback([], e);
                    }
                } else {
                    connection.end();

                    callback(locals, error);
                }
            });
        } else {
            connection.end();

            callback (locals, null);
        }

};

/**
 * 
 * @param local_id 
 * @param type_day_id 
 * @param callback
 * @description Detecta rotas a partir de um local
 */
exports.detect_routes_by_one_local = function(local_id, type_day_id, callback){

    var connection = mysql_connection.conncection();

    connection.query("CALL detect_route_by_one_local(" + local_id + ",'" + type_day_id + "')", function (error, results, fields) {

        var routes = [], i;

        if (!error) {

          try {
                 for (i = 0 ;i < results[0].length; i++) {
                           routes.push(results[0][i].route_id);
                 }

                 connection.end();
                 callback(routes, null);

              } catch (e) {
              connection.end();
                   callback([], e);
              }

        } else {
            connection.end();
            callback ([], error);
        }
    });

};

/**
 * 
 * @param type_user_id 
 * @param number_stop
 * @param route_id
 * @param callback
 * @description Mostra o preço total do bilhete e passe para um determinado utilizador
 *
 */
exports.route_price = function (type_user_id, number_stop, route_id, callback) {

    var connection = mysql_connection.conncection();

    if (number_stop !== null) {

        connection.query("CALL route_price(" + type_user_id + "," + number_stop + "," + route_id + ")", function (error, price, fields) {

            if (!error) {

                connection.end();
                callback(price[0], null);

            } else {
                connection.end();
                callback([], error)
            }
        });
    } else {
        connection.end();
        callback([] ,null);
    }
};

/**
 *
 * @param local_id
 * @param callback
 * @description Retorna as coordenadas de um determinado ponto
 */
exports.local_coord = function(local_id,callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL local_coord(" + local_id + ")", function (error, results, fields) {

        var coord = [];
        if(!error) {
            coord.push(results[0][0].latitude);
            coord.push(results[0][0].longitude);
            connection.end();
            callback(coord, null);
        } else {
            connection.end();
            callback (null, error);
        }
    });
};

/**
 *
 * @param route_id
 * @param callback
 * @description Verifica se a compra de bilhete está ativa
 */
exports.purchare_active = function(route_id, callback) {

    var connection = mysql_connection.conncection();

    connection.query("CALL get_one_ticket_config(" + route_id + ")", function (error, results, fields) {

        if(!error) {

            if (results[0].length === 0) {

                connection.end();
                callback(false, null);

            } else {
                connection.end();
                callback(results[0][0].active, null);
            }

        } else {
            connection.end();
            callback (null, error);
        }
    });

};

/**
 * 
 * @param origin 
 * @param destination 
 * @param date_search 
 * @param result_count 
 * @param error 
 * @param callback
 * @description Guarda o log da pesquisa
 */
exports.log_search = function(origin ,destination, date_search, result_count,error ,callback) {

            var date_search_created = moment().format("YYYY-MM-DD HH:mm:ss");
            var date_search_aux = moment(date_search).format("YYYY-MM-DD HH:mm:ss");

            var connection = mysql_connection.conncection();

            connection.query("CALL insert_log_search('" + origin + "','" + destination + "','" + date_search_aux + "'," + error + "," + result_count + ",'" + date_search_created + "')", function (error, results, fields) {

                if(!error) {
                    connection.end();
                    callback(null);
                } else {
                    connection.end();
                    callback (error);
                }
            });
};
