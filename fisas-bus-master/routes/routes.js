"use strict";
var express = require('express');
var router = express.Router();
var search_routes = require('../controllers/search_routes');
var search_one_local = require('../controllers/search_routes_one_local');
var search_local = require('../controllers/search_local_controller');
var search_place = require('../controllers/search_place_controller');
var cod_prod = require('../controllers/cod_prod_decrypt_controller');

var token = require('../middlewares/verifyToken');
var scopes = require('../middlewares/verifyScopes');
var limit_sort = require('../middlewares/request_limit_sort');
var mid_func = require('../middlewares/surch_types_days');
var validate = require('../middlewares/validate_search');
var back_office = require('../config/routes_requires');
var profile = require('../middlewares/verifyProfile');
var profiles = require('../middlewares/get_profiles');
var organic_unit = require('../middlewares/verifyOrganicUnit');
var organic_units = require('../middlewares/get_organic_units');
var curse = require('../middlewares/verifyCourse');
var curses = require('../middlewares/get_courses');
var files = require('../middlewares/get_files');
var file = require('../middlewares/verifyFile');
var school = require('../middlewares/verifySchool');
var schools = require('../middlewares/get_schools');
var taxes = require('../middlewares/get_taxs');
var accounts = require('../middlewares/get_accounts');
var services = require('../middlewares/get_services');
var locals = require('../middlewares/get_locals')
var filter_application = require('../middlewares/filter_application')

var hello = require('../controllers/hello_microservice');


var urlBase = "/api/v1";


router.get('/', hello.hello_microservice);

//Region Search Routes

router.get(urlBase + '/route_search', token.verifyToken, mid_func.surch_types_days, taxes.get_taxs ,search_routes.routes_search);//Pesquisa de trajetos

router.get(urlBase + '/route_search/one_local', token.verifyToken, mid_func.surch_types_days, validate.validate_search, search_one_local.routes_search_one_local);

//endregion


//region Seasons

router.get(urlBase + '/seasons',token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort,back_office.season.get_seasons);
router.get(urlBase + '/seasons/:season_id',token.verifyToken,scopes.verifyScopes,back_office.season.get_one_season);
router.post(urlBase + '/seasons',token.verifyToken,scopes.verifyScopes,back_office.season.insert_season);
router.delete(urlBase + '/seasons/:season_id',token.verifyToken,scopes.verifyScopes,back_office.season.delete_season);
router.put(urlBase + '/seasons/:season_id',token.verifyToken,scopes.verifyScopes,back_office.season.update_season);

//endregion

//Region Period

router.get(urlBase + '/periods', token.verifyToken,scopes.verifyScopes ,limit_sort.limit_and_sort,back_office.period.get_periods);
router.get(urlBase + '/periods/:period_id',token.verifyToken, scopes.verifyScopes,back_office.period.get_one_period);
router.post(urlBase + '/periods',token.verifyToken,scopes.verifyScopes ,back_office.period.insert_period);
router.delete(urlBase + '/periods/:period_id',token.verifyToken, scopes.verifyScopes,back_office.period.delete_period);
router.put(urlBase + '/periods/:period_id',token.verifyToken, scopes.verifyScopes,back_office.period.update_period);

//endregion

//Region Contacts

router.get(urlBase + '/contacts',token.verifyToken, scopes.verifyScopes,limit_sort.limit_and_sort, back_office.contact.get_contacts);
router.get(urlBase + '/contacts/:contact_id',token.verifyToken,scopes.verifyScopes,back_office.contact.get_one_contact);
router.post(urlBase + '/contacts',token.verifyToken, scopes.verifyScopes,back_office.contact.insert_contact);
router.delete(urlBase + '/contacts/:contact_id',token.verifyToken,scopes.verifyScopes,back_office.contact.delete_contact);
router.put(urlBase + '/contacts/:contact_id',token.verifyToken, scopes.verifyScopes,back_office.contact.update_contact);

//endregion

//Region Local

router.get(urlBase + '/locals',token.verifyToken,limit_sort.limit_and_sort,back_office.local.get_locals);
router.get(urlBase + '/locals/search', token.verifyToken,search_local.routes_search_one_local);
router.get(urlBase + '/locals/institute', token.verifyToken,limit_sort.limit_and_sort,back_office.local.get_locals_institute);//Locais do instituto
router.get(urlBase + '/locals/:local_id',token.verifyToken,scopes.verifyScopes, back_office.local.get_one_local);
router.post(urlBase + '/locals',token.verifyToken,scopes.verifyScopes,back_office.local.insert_local);
router.put(urlBase + '/locals/:local_id',token.verifyToken,scopes.verifyScopes,back_office.local.update_local);
router.delete(urlBase + '/locals/:local_id', token.verifyToken,scopes.verifyScopes,back_office.local.delete_local);

//endregion

//Region Place

router.get(urlBase + '/places',token.verifyToken,limit_sort.limit_and_sort, search_place.get_places);
router.get(urlBase + '/places/search', token.verifyToken, search_place.search_place);

//endregion

//Region Routes

router.get(urlBase + '/routes', token.verifyToken, limit_sort.limit_and_sort,back_office.route.get_routes);
router.post(urlBase + '/routes/link',token.verifyToken,scopes.verifyScopes,back_office.link.insert_link);
router.delete(urlBase + '/routes/link',token.verifyToken,scopes.verifyScopes,back_office.link.delete_link);
router.get(urlBase + '/routes/:route_id', token.verifyToken,scopes.verifyScopes,back_office.route.get_one_route);
router.get(urlBase + '/routes/:route_id/timetables', token.verifyToken, back_office.route.get_route_timetables);
router.get(urlBase + '/routes/:route_id/pricetables', token.verifyToken, profiles.get_profiles,back_office.route.get_route_price_tables);
router.get(urlBase + '/routes/:route_id/zones', token.verifyToken,scopes.verifyScopes,back_office.route.get_route_zones);
router.get(urlBase + '/routes/:route_id/tickets', token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort,back_office.route.get_route_tikets);
router.post(urlBase + '/routes', token.verifyToken,scopes.verifyScopes,back_office.route.insert_route);
router.delete(urlBase + '/routes/:route_id', token.verifyToken,scopes.verifyScopes,back_office.route.delete_route);
router.put(urlBase + '/routes/:route_id/name', token.verifyToken,scopes.verifyScopes,back_office.route.update_route_name);
router.put(urlBase + '/routes/:route_id/local', token.verifyToken,scopes.verifyScopes,back_office.route.update_route_local);
router.get(urlBase + '/routes/:route_id/links', token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort,back_office.route.get_route_links);

//endregion

//Region Types Days

router.get(urlBase + '/types_days',token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort ,back_office.type_day.get_types_days);
router.get(urlBase + '/types_days/days_weeks',token.verifyToken, scopes.verifyScopes,back_office.type_day.get_days_weeks);
router.get(urlBase + '/types_days/:type_day_id',token.verifyToken, scopes.verifyScopes,back_office.type_day.get_one_type_day);
router.get(urlBase + '/types_days/route_types_days',token.verifyToken,scopes.verifyScopes,back_office.type_day.route_types_days);//Tipos de dias numa rota

router.post(urlBase + '/types_days',token.verifyToken, scopes.verifyScopes,back_office.type_day.insert_type_day);
router.delete(urlBase + '/types_days/:type_day_id',token.verifyToken, scopes.verifyScopes,back_office.type_day.delete_type_day);
router.put(urlBase + '/types_days/:type_day_id',token.verifyToken, scopes.verifyScopes,back_office.type_day.update_type_day);
router.post(urlBase + '/types_days/:type_day_id/:day_id',token.verifyToken, scopes.verifyScopes,back_office.type_day.add_day_week);
router.delete(urlBase + '/types_days/:type_day_id/:day_id',token.verifyToken, scopes.verifyScopes,back_office.type_day.delete_day_week);

//endregion

//Region Ticket Type

router.get(urlBase + '/tickets_types', token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort, back_office.ticket_type.get_tickets_types);
router.get(urlBase + '/tickets_types/:ticket_type_id', token.verifyToken, scopes.verifyScopes,back_office.ticket_type.get_one_ticket);
router.post(urlBase + '/tickets_types',token.verifyToken, scopes.verifyScopes,back_office.ticket_type.insert_ticket_type);
router.delete(urlBase + '/tickets_types/:ticket_type_id', token.verifyToken,scopes.verifyScopes,back_office.ticket_type.delete_ticket_type);
router.put(urlBase + '/tickets_types/:ticket_type_id',token.verifyToken,scopes.verifyScopes,back_office.ticket_type.update_ticket_type);

//endregion

//Region Currency

router.get(urlBase + '/currencys', token.verifyToken, scopes.verifyScopes,limit_sort.limit_and_sort, back_office.currency.get_currencys);
router.get(urlBase + '/currencys/:currency_id', token.verifyToken, scopes.verifyScopes,back_office.currency.get_one_currency);
router.post(urlBase + '/currencys',token.verifyToken, scopes.verifyScopes,back_office.currency.insert_currency);
router.delete(urlBase + '/currencys/:currency_id', token.verifyToken,scopes.verifyScopes ,back_office.currency.delete_currency);
router.put(urlBase + '/currencys/:currency_id',token.verifyToken, scopes.verifyScopes,back_office.currency.update_currency);

//endregion

//Region Type User

router.get(urlBase + '/types_users',token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort ,back_office.type_user.get_types_users);
router.get(urlBase + '/types_users/:type_user_id',token.verifyToken,scopes.verifyScopes,back_office.type_user.get_one_type_user);
router.post(urlBase + '/types_users',token.verifyToken,scopes.verifyScopes,back_office.type_user.insert_type_user);
router.delete(urlBase + '/types_users/:type_user_id',token.verifyToken,scopes.verifyScopes,back_office.type_user.delete_type_user);
router.put(urlBase + '/types_users/:type_user_id',token.verifyToken,scopes.verifyScopes,back_office.type_user.update_type_user);

//endregion

//Region Timetable

router.get(urlBase + '/timetable',token.verifyToken, back_office.timetable.get_timetable);
router.post(urlBase + '/timetable',token.verifyToken,scopes.verifyScopes,back_office.timetable.insert_timetable);
router.delete(urlBase + '/timetable/:route_id/:type_day_id/:season_id',token.verifyToken,scopes.verifyScopes,back_office.timetable.delete_timetable);
router.delete(urlBase + '/timetable/line/:route_id/:type_day_id/:season_id/:line_number',token.verifyToken,scopes.verifyScopes,back_office.timetable.delete_timetable_line);
router.put(urlBase + '/timetable/hour/:hour_id',token.verifyToken,scopes.verifyScopes,back_office.timetable.update_hour);//Actualiza uma hora de passagem

//endregion

//Region Price Table

router.get(urlBase + '/price_table', token.verifyToken, profile.profile, back_office.price.get_price_table);
router.put(urlBase + '/price_table/price_value/:price_id',token.verifyToken, scopes.verifyScopes,back_office.price.update_price_value); //Atualiza um preço
router.delete(urlBase + '/price_table/:route_id/:ticket_type_id/:type_user_id/:currency_id', token.verifyToken, scopes.verifyScopes, profiles.get_profiles, back_office.price.delete_price_table);
router.post(urlBase + '/price_table',token.verifyToken, scopes.verifyScopes, profiles.get_profiles, taxes.get_taxs, accounts.get_accounts,back_office.price.insert_price_table);

//endregion

//Region Zone

router.get(urlBase + '/zones',token.verifyToken, scopes.verifyScopes,limit_sort.limit_and_sort, back_office.zone.get_zones);
router.get(urlBase + '/zones/:zone_id', token.verifyToken,scopes.verifyScopes,back_office.zone.get_one_zone);
router.post(urlBase + '/zones',token.verifyToken,scopes.verifyScopes,back_office.zone.insert_zone);
router.delete(urlBase + '/zones/:zone_id', token.verifyToken,scopes.verifyScopes,back_office.zone.delete_zone);
router.delete(urlBase + '/zones/zone_route/:zone_id/:route_id',token.verifyToken,scopes.verifyScopes, back_office.zone.delete_zone_route);//Elimina uma zona de uma rota

//endregion

//Region Link


router.get(urlBase + '/links', token.verifyToken,scopes.verifyScopes ,limit_sort.limit_and_sort, back_office.link.get_links);

//endregion

//Region TicketConfig

router.get(urlBase + '/tickets_configs', token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort, back_office.ticket_config.get_tickets_configs);
router.get(urlBase + '/tickets_configs/:route_id', token.verifyToken,scopes.verifyScopes ,back_office.ticket_config.get_one_ticket_config);
router.put(urlBase + '/tickets_configs/switch/:route_id',token.verifyToken, scopes.verifyScopes,back_office.ticket_config.enable_disable_ticket_config); //Ativa e desativa
router.put(urlBase + '/tickets_configs/max/:route_id',token.verifyToken, scopes.verifyScopes, back_office.ticket_config.update_ticket_config);

//endregion

//Region TicketBought

router.get(urlBase + '/tickets_boughts', token.verifyToken, scopes.verifyScopes,limit_sort.limit_and_sort,back_office.ticket_bought.get_tickets_boughts);
router.get(urlBase + '/tickets_boughts/:ticket_id', token.verifyToken, scopes.verifyScopes,back_office.ticket_bought.get_one_ticket_bought);
router.post(urlBase + '/tickets_boughts',token.verifyToken, back_office.ticket_bought.insert_tickets);
router.put(urlBase + '/tickets_boughts/:ticket_id/confirm',token.verifyToken, scopes.verifyScopes,back_office.ticket_bought.confirm_ticket); //Confirma bilhete


//Region Applications

router.get(urlBase + '/applications', token.verifyToken, filter_application.filter_application, limit_sort.limit_and_sort, back_office.applications.get_applications, curses.get_courses, schools.get_schools, locals.get_locals);
router.get(urlBase + '/applications/:application_id', token.verifyToken,scopes.verifyScopes ,back_office.applications.get_one_application, curses.get_courses, schools.get_schools, locals.get_locals);
router.get(urlBase + '/applications/payment-months/:application_id', token.verifyToken,scopes.verifyScopes ,back_office.payment_months.get_application_payment_month);
router.post(urlBase + '/applications',token.verifyToken,scopes.verifyScopes, curse.verifyCourse, school.verifySchool, back_office.applications.insert_application);
router.delete(urlBase + '/applications/:application_id',token.verifyToken,scopes.verifyScopes,back_office.applications.delete_application);
router.put(urlBase + '/applications/:application_id',token.verifyToken,scopes.verifyScopes, curse.verifyCourse, school.verifySchool, back_office.applications.update_application);
router.post(urlBase + '/applications/accept/:application_id',token.verifyToken, scopes.verifyScopes, back_office.applications.accept_application);
router.post(urlBase + '/applications/reject/:application_id',token.verifyToken, scopes.verifyScopes, back_office.applications.rejected_application);
router.post(urlBase + '/applications/cancel/:application_id',token.verifyToken, scopes.verifyScopes, back_office.applications.cancelled_application);
router.get(urlBase + '/applications/withdrawal/:application_id',token.verifyToken, scopes.verifyScopes, back_office.applications.get_withdrawal_application);
router.post(urlBase + '/applications/withdrawal/:application_id',token.verifyToken, scopes.verifyScopes, back_office.applications.withdrawal_application);
router.post(urlBase + '/applications/withdrawal/:withdrawal_id/status',token.verifyToken, scopes.verifyScopes, back_office.applications.status_withdrawal);
router.post(urlBase + '/applications/:application_id/status',token.verifyToken, scopes.verifyScopes, back_office.applications.status_application);


//Region Payment Months

router.get(urlBase + '/payment-months', token.verifyToken,scopes.verifyScopes,limit_sort.limit_and_sort, back_office.payment_months.get_payment_months);
router.get(urlBase + '/payment-months/:payment_month_id', token.verifyToken,scopes.verifyScopes ,back_office.payment_months.get_one_payment_month);
router.post(urlBase + '/payment-months',token.verifyToken,scopes.verifyScopes, back_office.payment_months.insert_payment_month);
router.delete(urlBase + '/payment-months/:payment_month_id',token.verifyToken,scopes.verifyScopes,back_office.payment_months.delete_payment_month);
router.put(urlBase + '/payment-months/:payment_month_id',token.verifyToken,scopes.verifyScopes, back_office.payment_months.update_payment_month);


router.get(urlBase + '/tickets/prod_cod_descrypt', token.verifyToken, scopes.verifyScopes, cod_prod.cod_prod_decrypt);
//endregion

router.get(urlBase + '/service', token.verifyToken, scopes.verifyScopes, back_office.configuration.get_configuration);
router.put(urlBase + '/service/:configuration_id', token.verifyToken, scopes.verifyScopes, services.get_services, back_office.configuration.update_configuration);

router.get(urlBase + '/configurations', token.verifyToken, scopes.verifyScopes, back_office.configurations.get_configurations, files.get_files);
router.get(urlBase + '/configurations/:configuration_key', token.verifyToken, scopes.verifyScopes, back_office.configurations.get_one_configurations, files.get_files);
router.put(urlBase + '/configurations/:configuration_key', token.verifyToken, scopes.verifyScopes, file.verifyFile, back_office.configurations.update_configurations, files.get_files);

module.exports = router;
