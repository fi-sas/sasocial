exports.seed = function(knex) {

	const data = [
		{ key: "GOOGLE_API_KEY", value: null, updated_at: new Date(), created_at: new Date() },
		{ key: "APPLICATION_REGULATION", value: null, updated_at: new Date(), created_at: new Date() },
		{ key: "SERVICE_ID", value: null, updated_at: new Date(), created_at: new Date() },
		{ key: "ASSIGNED_STATUS_AUTO_CHANGE", value: null, updated_at: new Date(), created_at: new Date() },
		{ key: "EVENT_FOR_AUTO_CHANGE", value: null, updated_at: new Date(), created_at: new Date() },
		{ key: "NUNBER_DAYS_FOR_CHANGE", value: null, updated_at: new Date(), created_at: new Date() }
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("configurations").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("configurations").insert(d);
			}
			return true;
		}),
	);
};
