exports.seed = function(knex) {

	const data = [
		{ id: 0, day: "Domingo" },
		{ id: 1, day: "Segunda-Feira" },
		{ id: 2, day: "Terça-Feira" },
		{ id: 3, day: "Quarta-Feira" },
		{ id: 4, day: "Quinta-Feira" },
		{ id: 5, day: "Sexta-Feira" },
		{ id: 6, day: "Sábado" }
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("days_weeks").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("days_weeks").insert(d);
			}
			return true;
		}),
	);
};
