exports.seed = function(knex) {
	const data = [
		{
			"id" : 1,
			"cod_district" : 1,
			"cod_place" : 6,
			"place" : "Castelo de Paiva"
		},
		{
			"id" : 2,
			"cod_district" : 1,
			"cod_place" : 7,
			"place" : "Espinho"
		},
		{
			"id" : 3,
			"cod_district" : 1,
			"cod_place" : 8,
			"place" : "Estarreja"
		},
		{
			"id" : 4,
			"cod_district" : 1,
			"cod_place" : 9,
			"place" : "Santa Maria da Feira"
		},
		{
			"id" : 5,
			"cod_district" : 1,
			"cod_place" : 10,
			"place" : "Ílhavo"
		},
		{
			"id" : 6,
			"cod_district" : 1,
			"cod_place" : 11,
			"place" : "Mealhada"
		},
		{
			"id" : 7,
			"cod_district" : 1,
			"cod_place" : 12,
			"place" : "Murtosa"
		},
		{
			"id" : 8,
			"cod_district" : 1,
			"cod_place" : 13,
			"place" : "Oliveira de Azeméis"
		},
		{
			"id" : 9,
			"cod_district" : 1,
			"cod_place" : 14,
			"place" : "Oliveira do Bairro"
		},
		{
			"id" : 10,
			"cod_district" : 1,
			"cod_place" : 15,
			"place" : "Ovar"
		},
		{
			"id" : 11,
			"cod_district" : 1,
			"cod_place" : 16,
			"place" : "São João da Madeira"
		},
		{
			"id" : 12,
			"cod_district" : 1,
			"cod_place" : 17,
			"place" : "Sever do Vouga"
		},
		{
			"id" : 13,
			"cod_district" : 1,
			"cod_place" : 18,
			"place" : "Vagos"
		},
		{
			"id" : 14,
			"cod_district" : 1,
			"cod_place" : 19,
			"place" : "Vale de Cambra"
		},
		{
			"id" : 15,
			"cod_district" : 1,
			"cod_place" : 1,
			"place" : "Águeda"
		},
		{
			"id" : 16,
			"cod_district" : 1,
			"cod_place" : 2,
			"place" : "Albergaria-a-Velha"
		},
		{
			"id" : 17,
			"cod_district" : 1,
			"cod_place" : 3,
			"place" : "Anadia"
		},
		{
			"id" : 18,
			"cod_district" : 1,
			"cod_place" : 4,
			"place" : "Arouca"
		},
		{
			"id" : 19,
			"cod_district" : 1,
			"cod_place" : 5,
			"place" : "Aveiro"
		},
		{
			"id" : 20,
			"cod_district" : 2,
			"cod_place" : 6,
			"place" : "Castro Verde"
		},
		{
			"id" : 21,
			"cod_district" : 2,
			"cod_place" : 7,
			"place" : "Cuba"
		},
		{
			"id" : 22,
			"cod_district" : 2,
			"cod_place" : 8,
			"place" : "Ferreira do Alentejo"
		},
		{
			"id" : 23,
			"cod_district" : 2,
			"cod_place" : 9,
			"place" : "Mértola"
		},
		{
			"id" : 24,
			"cod_district" : 2,
			"cod_place" : 10,
			"place" : "Moura"
		},
		{
			"id" : 25,
			"cod_district" : 2,
			"cod_place" : 11,
			"place" : "Odemira"
		},
		{
			"id" : 26,
			"cod_district" : 2,
			"cod_place" : 12,
			"place" : "Ourique"
		},
		{
			"id" : 27,
			"cod_district" : 2,
			"cod_place" : 13,
			"place" : "Serpa"
		},
		{
			"id" : 28,
			"cod_district" : 2,
			"cod_place" : 14,
			"place" : "Vidigueira"
		},
		{
			"id" : 29,
			"cod_district" : 2,
			"cod_place" : 1,
			"place" : "Aljustrel"
		},
		{
			"id" : 30,
			"cod_district" : 2,
			"cod_place" : 2,
			"place" : "Almodôvar"
		},
		{
			"id" : 31,
			"cod_district" : 2,
			"cod_place" : 3,
			"place" : "Alvito"
		},
		{
			"id" : 32,
			"cod_district" : 2,
			"cod_place" : 4,
			"place" : "Barrancos"
		},
		{
			"id" : 33,
			"cod_district" : 2,
			"cod_place" : 5,
			"place" : "Beja"
		},
		{
			"id" : 34,
			"cod_district" : 3,
			"cod_place" : 6,
			"place" : "Esposende"
		},
		{
			"id" : 35,
			"cod_district" : 3,
			"cod_place" : 7,
			"place" : "Fafe"
		},
		{
			"id" : 36,
			"cod_district" : 3,
			"cod_place" : 8,
			"place" : "Guimarães"
		},
		{
			"id" : 37,
			"cod_district" : 3,
			"cod_place" : 9,
			"place" : "Póvoa de Lanhoso"
		},
		{
			"id" : 38,
			"cod_district" : 3,
			"cod_place" : 10,
			"place" : "Terras de Bouro"
		},
		{
			"id" : 39,
			"cod_district" : 3,
			"cod_place" : 11,
			"place" : "Vieira do Minho"
		},
		{
			"id" : 40,
			"cod_district" : 3,
			"cod_place" : 12,
			"place" : "Vila Nova de Famalicão"
		},
		{
			"id" : 41,
			"cod_district" : 3,
			"cod_place" : 13,
			"place" : "Vila Verde"
		},
		{
			"id" : 42,
			"cod_district" : 3,
			"cod_place" : 14,
			"place" : "Vizela"
		},
		{
			"id" : 43,
			"cod_district" : 3,
			"cod_place" : 1,
			"place" : "Amares"
		},
		{
			"id" : 44,
			"cod_district" : 3,
			"cod_place" : 2,
			"place" : "Barcelos"
		},
		{
			"id" : 45,
			"cod_district" : 3,
			"cod_place" : 3,
			"place" : "Braga"
		},
		{
			"id" : 46,
			"cod_district" : 3,
			"cod_place" : 4,
			"place" : "Cabeceiras de Basto"
		},
		{
			"id" : 47,
			"cod_district" : 3,
			"cod_place" : 5,
			"place" : "Celorico de Basto"
		},
		{
			"id" : 48,
			"cod_district" : 4,
			"cod_place" : 6,
			"place" : "Miranda do Douro"
		},
		{
			"id" : 49,
			"cod_district" : 4,
			"cod_place" : 7,
			"place" : "Mirandela"
		},
		{
			"id" : 50,
			"cod_district" : 4,
			"cod_place" : 8,
			"place" : "Mogadouro"
		},
		{
			"id" : 51,
			"cod_district" : 4,
			"cod_place" : 9,
			"place" : "Torre de Moncorvo"
		},
		{
			"id" : 52,
			"cod_district" : 4,
			"cod_place" : 10,
			"place" : "Vila Flor"
		},
		{
			"id" : 53,
			"cod_district" : 4,
			"cod_place" : 11,
			"place" : "Vimioso"
		},
		{
			"id" : 54,
			"cod_district" : 4,
			"cod_place" : 12,
			"place" : "Vinhais"
		},
		{
			"id" : 55,
			"cod_district" : 4,
			"cod_place" : 1,
			"place" : "Alfândega da Fé"
		},
		{
			"id" : 56,
			"cod_district" : 4,
			"cod_place" : 2,
			"place" : "Bragança"
		},
		{
			"id" : 57,
			"cod_district" : 4,
			"cod_place" : 3,
			"place" : "Carrazeda de Ansiães"
		},
		{
			"id" : 58,
			"cod_district" : 4,
			"cod_place" : 4,
			"place" : "Freixo de Espada à Cinta"
		},
		{
			"id" : 59,
			"cod_district" : 4,
			"cod_place" : 5,
			"place" : "Macedo de Cavaleiros"
		},
		{
			"id" : 60,
			"cod_district" : 5,
			"cod_place" : 6,
			"place" : "Oleiros"
		},
		{
			"id" : 61,
			"cod_district" : 5,
			"cod_place" : 7,
			"place" : "Penamacor"
		},
		{
			"id" : 62,
			"cod_district" : 5,
			"cod_place" : 8,
			"place" : "Proença-a-Nova"
		},
		{
			"id" : 63,
			"cod_district" : 5,
			"cod_place" : 9,
			"place" : "Sertã"
		},
		{
			"id" : 64,
			"cod_district" : 5,
			"cod_place" : 10,
			"place" : "Vila de Rei"
		},
		{
			"id" : 65,
			"cod_district" : 5,
			"cod_place" : 11,
			"place" : "Vila Velha de Ródão"
		},
		{
			"id" : 66,
			"cod_district" : 5,
			"cod_place" : 1,
			"place" : "Belmonte"
		},
		{
			"id" : 67,
			"cod_district" : 5,
			"cod_place" : 2,
			"place" : "Castelo Branco"
		},
		{
			"id" : 68,
			"cod_district" : 5,
			"cod_place" : 3,
			"place" : "Covilhã"
		},
		{
			"id" : 69,
			"cod_district" : 5,
			"cod_place" : 4,
			"place" : "Fundão"
		},
		{
			"id" : 70,
			"cod_district" : 5,
			"cod_place" : 5,
			"place" : "Idanha-a-Nova"
		},
		{
			"id" : 71,
			"cod_district" : 6,
			"cod_place" : 6,
			"place" : "Góis"
		},
		{
			"id" : 72,
			"cod_district" : 6,
			"cod_place" : 7,
			"place" : "Lousã"
		},
		{
			"id" : 73,
			"cod_district" : 6,
			"cod_place" : 8,
			"place" : "Mira"
		},
		{
			"id" : 74,
			"cod_district" : 6,
			"cod_place" : 9,
			"place" : "Miranda do Corvo"
		},
		{
			"id" : 75,
			"cod_district" : 6,
			"cod_place" : 10,
			"place" : "Montemor-o-Velho"
		},
		{
			"id" : 76,
			"cod_district" : 6,
			"cod_place" : 11,
			"place" : "Oliveira do Hospital"
		},
		{
			"id" : 77,
			"cod_district" : 6,
			"cod_place" : 12,
			"place" : "Pampilhosa da Serra"
		},
		{
			"id" : 78,
			"cod_district" : 6,
			"cod_place" : 13,
			"place" : "Penacova"
		},
		{
			"id" : 79,
			"cod_district" : 6,
			"cod_place" : 14,
			"place" : "Penela"
		},
		{
			"id" : 80,
			"cod_district" : 6,
			"cod_place" : 15,
			"place" : "Soure"
		},
		{
			"id" : 81,
			"cod_district" : 6,
			"cod_place" : 16,
			"place" : "Tábua"
		},
		{
			"id" : 82,
			"cod_district" : 6,
			"cod_place" : 17,
			"place" : "Vila Nova de Poiares"
		},
		{
			"id" : 83,
			"cod_district" : 6,
			"cod_place" : 1,
			"place" : "Arganil"
		},
		{
			"id" : 84,
			"cod_district" : 6,
			"cod_place" : 2,
			"place" : "Cantanhede"
		},
		{
			"id" : 85,
			"cod_district" : 6,
			"cod_place" : 3,
			"place" : "Coimbra"
		},
		{
			"id" : 86,
			"cod_district" : 6,
			"cod_place" : 4,
			"place" : "Condeixa-a-Nova"
		},
		{
			"id" : 87,
			"cod_district" : 6,
			"cod_place" : 5,
			"place" : "Figueira da Foz"
		},
		{
			"id" : 88,
			"cod_district" : 7,
			"cod_place" : 6,
			"place" : "Montemor-o-Novo"
		},
		{
			"id" : 89,
			"cod_district" : 7,
			"cod_place" : 7,
			"place" : "Mora"
		},
		{
			"id" : 90,
			"cod_district" : 7,
			"cod_place" : 8,
			"place" : "Mourão"
		},
		{
			"id" : 91,
			"cod_district" : 7,
			"cod_place" : 9,
			"place" : "Portel"
		},
		{
			"id" : 92,
			"cod_district" : 7,
			"cod_place" : 10,
			"place" : "Redondo"
		},
		{
			"id" : 93,
			"cod_district" : 7,
			"cod_place" : 11,
			"place" : "Reguengos de Monsaraz"
		},
		{
			"id" : 94,
			"cod_district" : 7,
			"cod_place" : 12,
			"place" : "Vendas Novas"
		},
		{
			"id" : 95,
			"cod_district" : 7,
			"cod_place" : 13,
			"place" : "Viana do Alentejo"
		},
		{
			"id" : 96,
			"cod_district" : 7,
			"cod_place" : 14,
			"place" : "Vila Viçosa"
		},
		{
			"id" : 97,
			"cod_district" : 7,
			"cod_place" : 1,
			"place" : "Alandroal"
		},
		{
			"id" : 98,
			"cod_district" : 7,
			"cod_place" : 2,
			"place" : "Arraiolos"
		},
		{
			"id" : 99,
			"cod_district" : 7,
			"cod_place" : 3,
			"place" : "Borba"
		},
		{
			"id" : 100,
			"cod_district" : 7,
			"cod_place" : 4,
			"place" : "Estremoz"
		},
		{
			"id" : 101,
			"cod_district" : 7,
			"cod_place" : 5,
			"place" : "Évora"
		},
		{
			"id" : 102,
			"cod_district" : 8,
			"cod_place" : 6,
			"place" : "Lagoa (Algarve)"
		},
		{
			"id" : 103,
			"cod_district" : 8,
			"cod_place" : 7,
			"place" : "Lagos"
		},
		{
			"id" : 104,
			"cod_district" : 8,
			"cod_place" : 8,
			"place" : "Loulé"
		},
		{
			"id" : 105,
			"cod_district" : 8,
			"cod_place" : 9,
			"place" : "Monchique"
		},
		{
			"id" : 106,
			"cod_district" : 8,
			"cod_place" : 10,
			"place" : "Olhão"
		},
		{
			"id" : 107,
			"cod_district" : 8,
			"cod_place" : 11,
			"place" : "Portimão"
		},
		{
			"id" : 108,
			"cod_district" : 8,
			"cod_place" : 12,
			"place" : "São Brás de Alportel"
		},
		{
			"id" : 109,
			"cod_district" : 8,
			"cod_place" : 13,
			"place" : "Silves"
		},
		{
			"id" : 110,
			"cod_district" : 8,
			"cod_place" : 14,
			"place" : "Tavira"
		},
		{
			"id" : 111,
			"cod_district" : 8,
			"cod_place" : 15,
			"place" : "Vila do Bispo"
		},
		{
			"id" : 112,
			"cod_district" : 8,
			"cod_place" : 16,
			"place" : "Vila Real de Santo António"
		},
		{
			"id" : 113,
			"cod_district" : 8,
			"cod_place" : 1,
			"place" : "Albufeira"
		},
		{
			"id" : 114,
			"cod_district" : 8,
			"cod_place" : 2,
			"place" : "Alcoutim"
		},
		{
			"id" : 115,
			"cod_district" : 8,
			"cod_place" : 3,
			"place" : "Aljezur"
		},
		{
			"id" : 116,
			"cod_district" : 8,
			"cod_place" : 4,
			"place" : "Castro Marim"
		},
		{
			"id" : 117,
			"cod_district" : 8,
			"cod_place" : 5,
			"place" : "Faro"
		},
		{
			"id" : 118,
			"cod_district" : 9,
			"cod_place" : 6,
			"place" : "Gouveia"
		},
		{
			"id" : 119,
			"cod_district" : 9,
			"cod_place" : 7,
			"place" : "Guarda"
		},
		{
			"id" : 120,
			"cod_district" : 9,
			"cod_place" : 8,
			"place" : "Manteigas"
		},
		{
			"id" : 121,
			"cod_district" : 9,
			"cod_place" : 9,
			"place" : "Meda"
		},
		{
			"id" : 122,
			"cod_district" : 9,
			"cod_place" : 10,
			"place" : "Pinhel"
		},
		{
			"id" : 123,
			"cod_district" : 9,
			"cod_place" : 11,
			"place" : "Sabugal"
		},
		{
			"id" : 124,
			"cod_district" : 9,
			"cod_place" : 12,
			"place" : "Seia"
		},
		{
			"id" : 125,
			"cod_district" : 9,
			"cod_place" : 13,
			"place" : "Trancoso"
		},
		{
			"id" : 126,
			"cod_district" : 9,
			"cod_place" : 14,
			"place" : "Vila Nova de Foz Côa"
		},
		{
			"id" : 127,
			"cod_district" : 9,
			"cod_place" : 1,
			"place" : "Aguiar da Beira"
		},
		{
			"id" : 128,
			"cod_district" : 9,
			"cod_place" : 2,
			"place" : "Almeida"
		},
		{
			"id" : 129,
			"cod_district" : 9,
			"cod_place" : 3,
			"place" : "Celorico da Beira"
		},
		{
			"id" : 130,
			"cod_district" : 9,
			"cod_place" : 4,
			"place" : "Figueira de Castelo Rodrigo"
		},
		{
			"id" : 131,
			"cod_district" : 9,
			"cod_place" : 5,
			"place" : "Fornos de Algodres"
		},
		{
			"id" : 132,
			"cod_district" : 10,
			"cod_place" : 6,
			"place" : "Caldas da Rainha"
		},
		{
			"id" : 133,
			"cod_district" : 10,
			"cod_place" : 7,
			"place" : "Castanheira de Pêra"
		},
		{
			"id" : 134,
			"cod_district" : 10,
			"cod_place" : 8,
			"place" : "Figueiró dos Vinhos"
		},
		{
			"id" : 135,
			"cod_district" : 10,
			"cod_place" : 9,
			"place" : "Leiria"
		},
		{
			"id" : 136,
			"cod_district" : 10,
			"cod_place" : 10,
			"place" : "Marinha Grande"
		},
		{
			"id" : 137,
			"cod_district" : 10,
			"cod_place" : 11,
			"place" : "Nazaré"
		},
		{
			"id" : 138,
			"cod_district" : 10,
			"cod_place" : 12,
			"place" : "Óbidos"
		},
		{
			"id" : 139,
			"cod_district" : 10,
			"cod_place" : 13,
			"place" : "Pedrógão Grande"
		},
		{
			"id" : 140,
			"cod_district" : 10,
			"cod_place" : 14,
			"place" : "Peniche"
		},
		{
			"id" : 141,
			"cod_district" : 10,
			"cod_place" : 15,
			"place" : "Pombal"
		},
		{
			"id" : 142,
			"cod_district" : 10,
			"cod_place" : 16,
			"place" : "Porto de Mós"
		},
		{
			"id" : 143,
			"cod_district" : 10,
			"cod_place" : 1,
			"place" : "Alcobaça"
		},
		{
			"id" : 144,
			"cod_district" : 10,
			"cod_place" : 2,
			"place" : "Alvaiázere"
		},
		{
			"id" : 145,
			"cod_district" : 10,
			"cod_place" : 3,
			"place" : "Ansião"
		},
		{
			"id" : 146,
			"cod_district" : 10,
			"cod_place" : 4,
			"place" : "Batalha"
		},
		{
			"id" : 147,
			"cod_district" : 10,
			"cod_place" : 5,
			"place" : "Bombarral"
		},
		{
			"id" : 148,
			"cod_district" : 11,
			"cod_place" : 6,
			"place" : "Lisboa"
		},
		{
			"id" : 149,
			"cod_district" : 11,
			"cod_place" : 7,
			"place" : "Loures"
		},
		{
			"id" : 150,
			"cod_district" : 11,
			"cod_place" : 8,
			"place" : "Lourinhã"
		},
		{
			"id" : 151,
			"cod_district" : 11,
			"cod_place" : 9,
			"place" : "Mafra"
		},
		{
			"id" : 152,
			"cod_district" : 11,
			"cod_place" : 10,
			"place" : "Oeiras"
		},
		{
			"id" : 153,
			"cod_district" : 11,
			"cod_place" : 11,
			"place" : "Sintra"
		},
		{
			"id" : 154,
			"cod_district" : 11,
			"cod_place" : 12,
			"place" : "Sobral de Monte Agraço"
		},
		{
			"id" : 155,
			"cod_district" : 11,
			"cod_place" : 13,
			"place" : "Torres Vedras"
		},
		{
			"id" : 156,
			"cod_district" : 11,
			"cod_place" : 14,
			"place" : "Vila Franca de Xira"
		},
		{
			"id" : 157,
			"cod_district" : 11,
			"cod_place" : 15,
			"place" : "Amadora"
		},
		{
			"id" : 158,
			"cod_district" : 11,
			"cod_place" : 16,
			"place" : "Odivelas"
		},
		{
			"id" : 159,
			"cod_district" : 11,
			"cod_place" : 1,
			"place" : "Alenquer"
		},
		{
			"id" : 160,
			"cod_district" : 11,
			"cod_place" : 2,
			"place" : "Arruda dos Vinhos"
		},
		{
			"id" : 161,
			"cod_district" : 11,
			"cod_place" : 3,
			"place" : "Azambuja"
		},
		{
			"id" : 162,
			"cod_district" : 11,
			"cod_place" : 4,
			"place" : "Cadaval"
		},
		{
			"id" : 163,
			"cod_district" : 11,
			"cod_place" : 5,
			"place" : "Cascais"
		},
		{
			"id" : 164,
			"cod_district" : 12,
			"cod_place" : 6,
			"place" : "Crato"
		},
		{
			"id" : 165,
			"cod_district" : 12,
			"cod_place" : 7,
			"place" : "Elvas"
		},
		{
			"id" : 166,
			"cod_district" : 12,
			"cod_place" : 8,
			"place" : "Fronteira"
		},
		{
			"id" : 167,
			"cod_district" : 12,
			"cod_place" : 9,
			"place" : "Gavião"
		},
		{
			"id" : 168,
			"cod_district" : 12,
			"cod_place" : 10,
			"place" : "Marvão"
		},
		{
			"id" : 169,
			"cod_district" : 12,
			"cod_place" : 11,
			"place" : "Monforte"
		},
		{
			"id" : 170,
			"cod_district" : 12,
			"cod_place" : 12,
			"place" : "Nisa"
		},
		{
			"id" : 171,
			"cod_district" : 12,
			"cod_place" : 13,
			"place" : "Ponte de Sor"
		},
		{
			"id" : 172,
			"cod_district" : 12,
			"cod_place" : 14,
			"place" : "Portalegre"
		},
		{
			"id" : 173,
			"cod_district" : 12,
			"cod_place" : 15,
			"place" : "Sousel"
		},
		{
			"id" : 174,
			"cod_district" : 12,
			"cod_place" : 1,
			"place" : "Alter do Chão"
		},
		{
			"id" : 175,
			"cod_district" : 12,
			"cod_place" : 2,
			"place" : "Arronches"
		},
		{
			"id" : 176,
			"cod_district" : 12,
			"cod_place" : 3,
			"place" : "Avis"
		},
		{
			"id" : 177,
			"cod_district" : 12,
			"cod_place" : 4,
			"place" : "Campo Maior"
		},
		{
			"id" : 178,
			"cod_district" : 12,
			"cod_place" : 5,
			"place" : "Castelo de Vide"
		},
		{
			"id" : 179,
			"cod_district" : 13,
			"cod_place" : 6,
			"place" : "Maia"
		},
		{
			"id" : 180,
			"cod_district" : 13,
			"cod_place" : 7,
			"place" : "Marco de Canaveses"
		},
		{
			"id" : 181,
			"cod_district" : 13,
			"cod_place" : 8,
			"place" : "Matosinhos"
		},
		{
			"id" : 182,
			"cod_district" : 13,
			"cod_place" : 9,
			"place" : "Paços de Ferreira"
		},
		{
			"id" : 183,
			"cod_district" : 13,
			"cod_place" : 10,
			"place" : "Paredes"
		},
		{
			"id" : 184,
			"cod_district" : 13,
			"cod_place" : 11,
			"place" : "Penafiel"
		},
		{
			"id" : 185,
			"cod_district" : 13,
			"cod_place" : 12,
			"place" : "Porto"
		},
		{
			"id" : 186,
			"cod_district" : 13,
			"cod_place" : 13,
			"place" : "Póvoa de Varzim"
		},
		{
			"id" : 187,
			"cod_district" : 13,
			"cod_place" : 14,
			"place" : "Santo Tirso"
		},
		{
			"id" : 188,
			"cod_district" : 13,
			"cod_place" : 15,
			"place" : "Valongo"
		},
		{
			"id" : 189,
			"cod_district" : 13,
			"cod_place" : 16,
			"place" : "Vila do Conde"
		},
		{
			"id" : 190,
			"cod_district" : 13,
			"cod_place" : 17,
			"place" : "Vila Nova de Gaia"
		},
		{
			"id" : 191,
			"cod_district" : 13,
			"cod_place" : 18,
			"place" : "Trofa"
		},
		{
			"id" : 192,
			"cod_district" : 13,
			"cod_place" : 1,
			"place" : "Amarante"
		},
		{
			"id" : 193,
			"cod_district" : 13,
			"cod_place" : 2,
			"place" : "Baião"
		},
		{
			"id" : 194,
			"cod_district" : 13,
			"cod_place" : 3,
			"place" : "Felgueiras"
		},
		{
			"id" : 195,
			"cod_district" : 13,
			"cod_place" : 4,
			"place" : "Gondomar"
		},
		{
			"id" : 196,
			"cod_district" : 13,
			"cod_place" : 5,
			"place" : "Lousada"
		},
		{
			"id" : 197,
			"cod_district" : 14,
			"cod_place" : 6,
			"place" : "Cartaxo"
		},
		{
			"id" : 198,
			"cod_district" : 14,
			"cod_place" : 7,
			"place" : "Chamusca"
		},
		{
			"id" : 199,
			"cod_district" : 14,
			"cod_place" : 8,
			"place" : "Constância"
		},
		{
			"id" : 200,
			"cod_district" : 14,
			"cod_place" : 9,
			"place" : "Coruche"
		},
		{
			"id" : 201,
			"cod_district" : 14,
			"cod_place" : 10,
			"place" : "Entroncamento"
		},
		{
			"id" : 202,
			"cod_district" : 14,
			"cod_place" : 11,
			"place" : "Ferreira do Zêzere"
		},
		{
			"id" : 203,
			"cod_district" : 14,
			"cod_place" : 12,
			"place" : "Golegã"
		},
		{
			"id" : 204,
			"cod_district" : 14,
			"cod_place" : 13,
			"place" : "Mação"
		},
		{
			"id" : 205,
			"cod_district" : 14,
			"cod_place" : 14,
			"place" : "Rio Maior"
		},
		{
			"id" : 206,
			"cod_district" : 14,
			"cod_place" : 15,
			"place" : "Salvaterra de Magos"
		},
		{
			"id" : 207,
			"cod_district" : 14,
			"cod_place" : 16,
			"place" : "Santarém"
		},
		{
			"id" : 208,
			"cod_district" : 14,
			"cod_place" : 17,
			"place" : "Sardoal"
		},
		{
			"id" : 209,
			"cod_district" : 14,
			"cod_place" : 18,
			"place" : "Tomar"
		},
		{
			"id" : 210,
			"cod_district" : 14,
			"cod_place" : 19,
			"place" : "Torres Novas"
		},
		{
			"id" : 211,
			"cod_district" : 14,
			"cod_place" : 20,
			"place" : "Vila Nova da Barquinha"
		},
		{
			"id" : 212,
			"cod_district" : 14,
			"cod_place" : 21,
			"place" : "Ourém"
		},
		{
			"id" : 213,
			"cod_district" : 14,
			"cod_place" : 1,
			"place" : "Abrantes"
		},
		{
			"id" : 214,
			"cod_district" : 14,
			"cod_place" : 2,
			"place" : "Alcanena"
		},
		{
			"id" : 215,
			"cod_district" : 14,
			"cod_place" : 3,
			"place" : "Almeirim"
		},
		{
			"id" : 216,
			"cod_district" : 14,
			"cod_place" : 4,
			"place" : "Alpiarça"
		},
		{
			"id" : 217,
			"cod_district" : 14,
			"cod_place" : 5,
			"place" : "Benavente"
		},
		{
			"id" : 218,
			"cod_district" : 15,
			"cod_place" : 6,
			"place" : "Moita"
		},
		{
			"id" : 219,
			"cod_district" : 15,
			"cod_place" : 7,
			"place" : "Montijo"
		},
		{
			"id" : 220,
			"cod_district" : 15,
			"cod_place" : 8,
			"place" : "Palmela"
		},
		{
			"id" : 221,
			"cod_district" : 15,
			"cod_place" : 9,
			"place" : "Santiago do Cacém"
		},
		{
			"id" : 222,
			"cod_district" : 15,
			"cod_place" : 10,
			"place" : "Seixal"
		},
		{
			"id" : 223,
			"cod_district" : 15,
			"cod_place" : 11,
			"place" : "Sesimbra"
		},
		{
			"id" : 224,
			"cod_district" : 15,
			"cod_place" : 12,
			"place" : "Setúbal"
		},
		{
			"id" : 225,
			"cod_district" : 15,
			"cod_place" : 13,
			"place" : "Sines"
		},
		{
			"id" : 226,
			"cod_district" : 15,
			"cod_place" : 1,
			"place" : "Alcácer do Sal"
		},
		{
			"id" : 227,
			"cod_district" : 15,
			"cod_place" : 2,
			"place" : "Alcochete"
		},
		{
			"id" : 228,
			"cod_district" : 15,
			"cod_place" : 3,
			"place" : "Almada"
		},
		{
			"id" : 229,
			"cod_district" : 15,
			"cod_place" : 4,
			"place" : "Barreiro"
		},
		{
			"id" : 230,
			"cod_district" : 15,
			"cod_place" : 5,
			"place" : "Grândola"
		},
		{
			"id" : 231,
			"cod_district" : 16,
			"cod_place" : 6,
			"place" : "Ponte da Barca"
		},
		{
			"id" : 232,
			"cod_district" : 16,
			"cod_place" : 7,
			"place" : "Ponte de Lima"
		},
		{
			"id" : 233,
			"cod_district" : 16,
			"cod_place" : 8,
			"place" : "Valença"
		},
		{
			"id" : 234,
			"cod_district" : 16,
			"cod_place" : 9,
			"place" : "Viana do Castelo"
		},
		{
			"id" : 235,
			"cod_district" : 16,
			"cod_place" : 10,
			"place" : "Vila Nova de Cerveira"
		},
		{
			"id" : 236,
			"cod_district" : 16,
			"cod_place" : 1,
			"place" : "Arcos de Valdevez"
		},
		{
			"id" : 237,
			"cod_district" : 16,
			"cod_place" : 2,
			"place" : "Caminha"
		},
		{
			"id" : 238,
			"cod_district" : 16,
			"cod_place" : 3,
			"place" : "Melgaço"
		},
		{
			"id" : 239,
			"cod_district" : 16,
			"cod_place" : 4,
			"place" : "Monção"
		},
		{
			"id" : 240,
			"cod_district" : 16,
			"cod_place" : 5,
			"place" : "Paredes de Coura"
		},
		{
			"id" : 241,
			"cod_district" : 17,
			"cod_place" : 5,
			"place" : "Mondim de Basto"
		},
		{
			"id" : 242,
			"cod_district" : 17,
			"cod_place" : 6,
			"place" : "Montalegre"
		},
		{
			"id" : 243,
			"cod_district" : 17,
			"cod_place" : 7,
			"place" : "Murça"
		},
		{
			"id" : 244,
			"cod_district" : 17,
			"cod_place" : 8,
			"place" : "Peso da Régua"
		},
		{
			"id" : 245,
			"cod_district" : 17,
			"cod_place" : 9,
			"place" : "Ribeira de Pena"
		},
		{
			"id" : 246,
			"cod_district" : 17,
			"cod_place" : 10,
			"place" : "Sabrosa"
		},
		{
			"id" : 247,
			"cod_district" : 17,
			"cod_place" : 11,
			"place" : "Santa Marta de Penaguião"
		},
		{
			"id" : 248,
			"cod_district" : 17,
			"cod_place" : 12,
			"place" : "Valpaços"
		},
		{
			"id" : 249,
			"cod_district" : 17,
			"cod_place" : 13,
			"place" : "Vila Pouca de Aguiar"
		},
		{
			"id" : 250,
			"cod_district" : 17,
			"cod_place" : 14,
			"place" : "Vila Real"
		},
		{
			"id" : 251,
			"cod_district" : 17,
			"cod_place" : 1,
			"place" : "Alijó"
		},
		{
			"id" : 252,
			"cod_district" : 17,
			"cod_place" : 2,
			"place" : "Boticas"
		},
		{
			"id" : 253,
			"cod_district" : 17,
			"cod_place" : 3,
			"place" : "Chaves"
		},
		{
			"id" : 254,
			"cod_district" : 17,
			"cod_place" : 4,
			"place" : "Mesão Frio"
		},
		{
			"id" : 255,
			"cod_district" : 18,
			"cod_place" : 5,
			"place" : "Lamego"
		},
		{
			"id" : 256,
			"cod_district" : 18,
			"cod_place" : 6,
			"place" : "Mangualde"
		},
		{
			"id" : 257,
			"cod_district" : 18,
			"cod_place" : 7,
			"place" : "Moimenta da Beira"
		},
		{
			"id" : 258,
			"cod_district" : 18,
			"cod_place" : 8,
			"place" : "Mortágua"
		},
		{
			"id" : 259,
			"cod_district" : 18,
			"cod_place" : 9,
			"place" : "Nelas"
		},
		{
			"id" : 260,
			"cod_district" : 18,
			"cod_place" : 10,
			"place" : "Oliveira de Frades"
		},
		{
			"id" : 261,
			"cod_district" : 18,
			"cod_place" : 11,
			"place" : "Penalva do Castelo"
		},
		{
			"id" : 262,
			"cod_district" : 18,
			"cod_place" : 12,
			"place" : "Penedono"
		},
		{
			"id" : 263,
			"cod_district" : 18,
			"cod_place" : 13,
			"place" : "Resende"
		},
		{
			"id" : 264,
			"cod_district" : 18,
			"cod_place" : 14,
			"place" : "Santa Comba Dão"
		},
		{
			"id" : 265,
			"cod_district" : 18,
			"cod_place" : 15,
			"place" : "São João da Pesqueira"
		},
		{
			"id" : 266,
			"cod_district" : 18,
			"cod_place" : 16,
			"place" : "São Pedro do Sul"
		},
		{
			"id" : 267,
			"cod_district" : 18,
			"cod_place" : 17,
			"place" : "Sátão"
		},
		{
			"id" : 268,
			"cod_district" : 18,
			"cod_place" : 18,
			"place" : "Sernancelhe"
		},
		{
			"id" : 269,
			"cod_district" : 18,
			"cod_place" : 19,
			"place" : "Tabuaço"
		},
		{
			"id" : 270,
			"cod_district" : 18,
			"cod_place" : 20,
			"place" : "Tarouca"
		},
		{
			"id" : 271,
			"cod_district" : 18,
			"cod_place" : 21,
			"place" : "Tondela"
		},
		{
			"id" : 272,
			"cod_district" : 18,
			"cod_place" : 22,
			"place" : "Vila Nova de Paiva"
		},
		{
			"id" : 273,
			"cod_district" : 18,
			"cod_place" : 23,
			"place" : "Viseu"
		},
		{
			"id" : 274,
			"cod_district" : 18,
			"cod_place" : 24,
			"place" : "Vouzela"
		},
		{
			"id" : 275,
			"cod_district" : 18,
			"cod_place" : 1,
			"place" : "Armamar"
		},
		{
			"id" : 276,
			"cod_district" : 18,
			"cod_place" : 2,
			"place" : "Carregal do Sal"
		},
		{
			"id" : 277,
			"cod_district" : 18,
			"cod_place" : 3,
			"place" : "Castro Daire"
		},
		{
			"id" : 278,
			"cod_district" : 18,
			"cod_place" : 4,
			"place" : "Cinfães"
		},
		{
			"id" : 279,
			"cod_district" : 31,
			"cod_place" : 5,
			"place" : "Ponta do Sol"
		},
		{
			"id" : 280,
			"cod_district" : 31,
			"cod_place" : 6,
			"place" : "Porto Moniz"
		},
		{
			"id" : 281,
			"cod_district" : 31,
			"cod_place" : 7,
			"place" : "Ribeira Brava"
		},
		{
			"id" : 282,
			"cod_district" : 31,
			"cod_place" : 8,
			"place" : "Santa Cruz"
		},
		{
			"id" : 283,
			"cod_district" : 31,
			"cod_place" : 9,
			"place" : "Santana"
		},
		{
			"id" : 284,
			"cod_district" : 31,
			"cod_place" : 10,
			"place" : "São Vicente"
		},
		{
			"id" : 285,
			"cod_district" : 31,
			"cod_place" : 1,
			"place" : "Calheta (Madeira)"
		},
		{
			"id" : 286,
			"cod_district" : 31,
			"cod_place" : 2,
			"place" : "Câmara de Lobos"
		},
		{
			"id" : 287,
			"cod_district" : 31,
			"cod_place" : 3,
			"place" : "Funchal"
		},
		{
			"id" : 288,
			"cod_district" : 31,
			"cod_place" : 4,
			"place" : "Machico"
		},
		{
			"id" : 289,
			"cod_district" : 32,
			"cod_place" : 1,
			"place" : "Porto Santo"
		},
		{
			"id" : 290,
			"cod_district" : 41,
			"cod_place" : 1,
			"place" : "Vila do Porto"
		},
		{
			"id" : 291,
			"cod_district" : 42,
			"cod_place" : 5,
			"place" : "Ribeira Grande"
		},
		{
			"id" : 292,
			"cod_district" : 42,
			"cod_place" : 6,
			"place" : "Vila Franca do Campo"
		},
		{
			"id" : 293,
			"cod_district" : 42,
			"cod_place" : 1,
			"place" : "Lagoa (São Miguel)"
		},
		{
			"id" : 294,
			"cod_district" : 42,
			"cod_place" : 2,
			"place" : "Nordeste"
		},
		{
			"id" : 295,
			"cod_district" : 42,
			"cod_place" : 3,
			"place" : "Ponta Delgada"
		},
		{
			"id" : 296,
			"cod_district" : 42,
			"cod_place" : 4,
			"place" : "Povoação"
		},
		{
			"id" : 297,
			"cod_district" : 43,
			"cod_place" : 1,
			"place" : "Angra do Heroísmo"
		},
		{
			"id" : 298,
			"cod_district" : 43,
			"cod_place" : 2,
			"place" : "Praia da Vitória"
		},
		{
			"id" : 299,
			"cod_district" : 44,
			"cod_place" : 1,
			"place" : "Santa Cruz da Graciosa"
		},
		{
			"id" : 300,
			"cod_district" : 45,
			"cod_place" : 1,
			"place" : "Calheta (São Jorge)"
		},
		{
			"id" : 301,
			"cod_district" : 45,
			"cod_place" : 2,
			"place" : "Velas"
		},
		{
			"id" : 302,
			"cod_district" : 46,
			"cod_place" : 1,
			"place" : "Lajes do Pico"
		},
		{
			"id" : 303,
			"cod_district" : 46,
			"cod_place" : 2,
			"place" : "Madalena"
		},
		{
			"id" : 304,
			"cod_district" : 46,
			"cod_place" : 3,
			"place" : "São Roque do Pico"
		},
		{
			"id" : 305,
			"cod_district" : 47,
			"cod_place" : 1,
			"place" : "Horta"
		},
		{
			"id" : 306,
			"cod_district" : 48,
			"cod_place" : 1,
			"place" : "Lajes das Flores"
		},
		{
			"id" : 307,
			"cod_district" : 48,
			"cod_place" : 2,
			"place" : "Santa Cruz das Flores"
		},
		{
			"id" : 308,
			"cod_district" : 49,
			"cod_place" : 1,
			"place" : "Corvo"
		}
	];
	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("places").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("places").insert(d);
			}
			return true;
		}),
	);
};
