"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const absenceStateMachine = require("../state-machine/absence.machine");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const Cron = require("moleculer-cron");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.absences",
	table: "absence",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "absences"), Cron],
	crons: [
		{
			name: "changeApplicationStatusByAbsences",
			cronTime: "* * * * *",
			onTick: function() {
				this.getLocalService("bus.absences")
					.actions.automaticSuspendApplications();
				this.getLocalService("bus.absences")
					.actions.automaticResumeApplications();
			},
			runOnInit: function() {
			},
		},
	],
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"start_date",
			"end_date",
			"justification",
			"application_id",
			"user_id",
			"file_id",
			"status",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.applications", "application", "application_id");
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
			justification: { type: "string", max: 255 },
			application_id: { type: "number", convert: true },
			user_id: { type: "number", convert: true },
			file_id: { type: "number", convert: true, optional: true },
			status: { type: "enum", values: ["submitted", "approved", "rejected", "ongoing", "completed"], optional: true, },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "submitted";
					ctx.params.start_date = moment(ctx.params.start_date).hours(0).minutes(0).seconds(0);
					ctx.params.end_date = moment(ctx.params.end_date).hours(22).minutes(59).seconds(59);
				},
				async function validateApplciation(ctx) {
					if (ctx.params.application_id) {
						await ctx.call("bus.applications.get", { id: ctx.params.application_id });
					}
				},
				async function validateAbsencesOverlaps(ctx) {
					if (ctx.params.start_date && ctx.params.end_date) {
						const overlap_absences = await this._count(ctx, {
							query: (qb) => {
								qb.where("user_id", ctx.params.user_id);
								qb.whereIn("status", ["approved", "ongoing", "completed"]);
								qb.whereRaw("tstzrange(start_date,end_date, '[]') && tstzrange(?, ?, '[]')", [
									ctx.params.start_date,
									ctx.params.end_date,
								]);
							},
						});

						if (overlap_absences > 0) {
							throw new ValidationError(
								"The start date and end date overlap with another absence request",
								"BUS_ABSENCE_OVERLAP",
								{},
							);
						}
					}
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: [
				async function sendNotification(ctx, response) {
					const app = await ctx.call("bus.applications.get", { id: response[0].application_id });
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "MOBILITY_ABSENCE_SUBMITTED",
						user_id: app[0].user_id,
						user_data: {},
						data: {
							application: app[0],
						},
						variables: {},
						external_uuid: null
					});
					return response;
				},
			],
			status: [
				async function validateDatesRange(ctx, res) {
					if (res[0].status == "approved") {
						if (moment().isAfter(res[0].end_date)) {
							await ctx.call("bus.absences.patch", { id: res[0].id, status: "completed" });
						} else if (moment().isAfter(res[0].start_date) && moment().isBefore(res[0].end_date)) {
							await ctx.call("bus.absences.patch", { id: res[0].id, status: "ongoing" });
							ctx.params = {};
							ctx.params.id = res[0].application_id;
							await ctx.call("bus.applications.status", { id: res[0].application_id, event: "suspend" });
							ctx.params = {};
						}
					}
					return res;
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		status: {
			rest: "POST /:id/status",
			scope: "bus:absences:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["approve", "reject"] },
			},
			async handler(ctx) {
				const absence = await ctx.call("bus.absences.get", ctx.params);
				const stateMachine = absenceStateMachine.createStateMachine(absence[0].status, ctx);
				if (!ctx.meta.user.can_access_BO) {
					throw new ValidationError(
						"You dont have permissions to change absence status",
						"BUS_ABSENCE_STATUS_UNAUTHORIZED",
						{},
					);
				}
				if (stateMachine.can(ctx.params.event.toUpperCase())) {
					return stateMachine[ctx.params.event.toLowerCase()]().then((result) => {
						return result;
					});
				} else {
					throw new ValidationError(
						"Is not possible to change to the state",
						"BUS_ABSENCE_STATUS_ERROR",
						{},
					);
				}


			},
		},
		automaticSuspendApplications: {
			visibility: "private",
			async handler(ctx) {
				const approved_absences = await this.adapter.find({
					query: (q) => {
						q.select("absence.id", "absence.application_id", "applications.status");
						q.innerJoin("applications", "absence.application_id", "applications.id");
						q.whereIn("applications.status", ["accepted", "suspended"]);
						q.where("absence.status", "approved");
						q.where("absence.start_date", "<=", new Date());
						q.where("absence.end_date", ">", new Date());
						return q;
					}
				});
				for (const absence of approved_absences) {
					await ctx.call("bus.absences.patch", { id: absence.id, status: "ongoing" });
					if (absence.status == "accepted") {
						await ctx.call("bus.applications.status", { id: absence.application_id, event: "suspend" });
					}
				}
			}
		},
		automaticResumeApplications: {
			visibility: "private",
			async handler(ctx) {
				const ongoing_absences = await this.adapter.find({
					query: (q) => {
						q.select("absence.id", "absence.application_id");
						q.innerJoin("applications", "absence.application_id", "applications.id");
						q.where("applications.status", "suspended");
						q.where("absence.status", "ongoing");
						q.where("absence.end_date", "<=", new Date());
						return q;
					}
				});
				for (const absence of ongoing_absences) {
					await ctx.call("bus.absences.patch", { id: absence.id, status: "completed" });
					await ctx.call("bus.applications.status", { id: absence.application_id, event: "resume" });
				}
			}
		},



	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
