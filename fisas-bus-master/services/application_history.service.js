"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { applicationsStatus } = require("../values/enums");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.application_history",
	table: "application_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "application_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"user_id",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, optional: true },
			application_id: { type: "number", positive: true },
			status: { type: "enum", values: applicationsStatus },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
