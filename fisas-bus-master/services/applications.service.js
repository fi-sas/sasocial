"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { applicationsStatus } = require("../values/enums");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const ApplicationStateMachine = require("../state-machine/application.machine");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");

/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.applications",
	table: "applications",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "applications")],

	/*
	 * Crons
	 */
	crons: [
		{
			name: "automaticChangeAssignedStatusAfter5Days",
			cronTime: "0 * * * *",
			onTick: function () {
				this.getLocalService("bus.applications")
					.actions.automatic_change_status()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"student_number",
			"birth_date",
			"tin",
			"identification",
			"document_type_id",
			"nationality",
			"date",
			"email",
			"phone_1",
			"phone_2",
			"course_year",
			"course_id",
			"renovation",
			"school_id",
			"observations",
			"accept_procedure",
			"morning_local_id_from",
			"morning_local_id_to",
			"afternoon_local_id_from",
			"afternoon_local_id_to",
			"user_id",
			"status",
			"declaration_file_id",
			"reject_reason",
			"academic_year",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"course",
			"school",
			"morning_local_from",
			"morning_local_to",
			"afternoon_local_from",
			"afternoon_local_to",
			"document_type",
			"declaration",
		],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			school(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "school", "school_id");
			},
			morning_local_from(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "morning_local_from", "morning_local_id_from");
			},
			morning_local_to(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "morning_local_to", "morning_local_id_to");
			},
			afternoon_local_from(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "afternoon_local_from", "afternoon_local_id_from");
			},
			afternoon_local_to(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "afternoon_local_to", "afternoon_local_id_to");
			},
			document_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.document-types",
					"document_type",
					"document_type_id",
				);
			},
			declaration(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "declaration", "declaration_file_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			student_number: { type: "string" },
			birth_date: { type: "date", convert: true },
			tin: { type: "string" },
			nationality: { type: "string" },
			identification: { type: "string" },
			document_type_id: { type: "number", convert: true },
			date: { type: "date", optional: true, convert: true },
			email: { type: "string" },
			phone_1: { type: "string" },
			phone_2: { type: "string", optional: true },
			course_year: { type: "number", positive: true },
			course_id: { type: "number", positive: true },
			renovation: { type: "boolean" },
			school_id: { type: "number", positive: true },
			observations: { type: "string", optional: true },
			accept_procedure: { type: "boolean" },
			morning_local_id_from: { type: "number", positive: true },
			morning_local_id_to: { type: "number", positive: true },
			afternoon_local_id_from: { type: "number", positive: true },
			afternoon_local_id_to: { type: "number", positive: true },
			declaration_file_id: { type: "number", positive: true, optional: true },
			user_id: { type: "number", positive: true },
			status: { type: "enum", values: applicationsStatus },
			reject_reason: { type: "string", optional: true, nullable: true },
			academic_year: { type: "string" },
		},
	},
	hooks: {
		before: {
			create: [
				async function validateAlreadyApplication(ctx) {
					this.logger.info("BATATA", ctx.params.academic_year);
					const application = await this._find(ctx, {
						query: {
							academic_year: ctx.params.academic_year,
							user_id: ctx.meta.user.id,
							status: ["submitted", "accepted", "quiting"],
						},
					});
					if (application.length !== 0) {
						throw new Errors.ValidationError(
							"The user already have application for this academic year",
							"BUS_APPLICATION_ALREADY_EXIST",
							{},
						);
					}
				},
				async function validateUser(ctx) {
					ctx.params.user_id = ctx.meta.user.id;
				},
				async function validateSchool(ctx) {
					if (ctx.params.school_id) {
						await ctx.call("infrastructure.organic-units.get", { id: ctx.params.school_id });
					} else {
						throw new Errors.ValidationError("'school_id' is required!", "SCHOOL_ID_REQUIRED", {});
					}
				},
				async function validateCourse(ctx) {
					if (ctx.params.course_id) {
						await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
					} else {
						throw new Errors.ValidationError("'course_id' is required!", "COURSE_ID_REQUIRED", {});
					}
				},
				async function validateMorningLocalFrom(ctx) {
					if (ctx.params.morning_local_id_from) {
						await ctx.call("bus.locals.get", { id: ctx.params.morning_local_id_from });
					} else {
						throw new Errors.ValidationError(
							"'morning_local_id_from' is required!",
							"MORNING_LOCAL_ID_FROM_REQUIRED",
							{},
						);
					}
				},
				async function validateMorningLocalTo(ctx) {
					if (ctx.params.morning_local_id_to) {
						await ctx.call("bus.locals.get", { id: ctx.params.morning_local_id_to });
					} else {
						throw new Errors.ValidationError(
							"'morning_local_id_to' is required!",
							"MORNING_LOCAL_ID_TO_FROM_REQUIRED",
							{},
						);
					}
				},
				async function validateAfternoonLocalFrom(ctx) {
					if (ctx.params.afternoon_local_id_from) {
						await ctx.call("bus.locals.get", { id: ctx.params.afternoon_local_id_from });
					} else {
						throw new Errors.ValidationError(
							"'afternoon_local_id_from' is required!",
							"AFTERNOON_LOCAL_ID_FROM_FROM_REQUIRED",
							{},
						);
					}
				},
				async function validateAfternoonLocalTo(ctx) {
					if (ctx.params.afternoon_local_id_to) {
						await ctx.call("bus.locals.get", { id: ctx.params.afternoon_local_id_to });
					} else {
						throw new Errors.ValidationError(
							"'AFTERNOON_LOCAL_ID_TO' is required!",
							"AFTERNOON_LOCAL_ID_TO_FROM_FROM_REQUIRED",
							{},
						);
					}
				},
				async function validateDocumenttype(ctx) {
					if (ctx.params.document_type_id) {
						await ctx.call("authorization.document-types.get", { id: ctx.params.document_type_id });
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "submitted";
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			createWithdrawal: [
				async function validateApplicationId(ctx) {
					const application = await this._get(ctx, { id: ctx.params.id });
					if (application[0].status != "accepted") {
						throw new ValidationError(
							"Cannot withdrawal application on " + application[0].status + " status",
							"BUS_APPLICATION_WRONG_STATUS",
							{},
						);
					}
				},
				// Verify if application already have oppening withdrawals
				async function validatePendingWithdrawals(ctx) {
					if (
						(await ctx.call("bus.withdrawals.find", {
							query: { application_id: ctx.params.id, status: "pending" },
						}).length) > 0
					) {
						throw new ValidationError(
							"Already have one pending withdrawal",
							"BUS_WITHDRAWAL_ALREADY_ON_PROGRESS",
							{},
						);
					}
				},
			],
			list: [
				function checkRequestOrigin(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.user_id = ctx.meta.user.id;
					}
				},
			],
		},
		after: {
			create: [
				function sendNotification(ctx, res) {
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "MOBILITY_APPLICATION_SUBMITTED",
						user_id: res[0].user_id,
						user_data: {},
						data: {
							application: res[0],
						},
						variables: {},
						external_uuid: null,
					});
					return res;
				},
			],
			status: [
				async function updateOngoingAbsence(ctx, res) {
					if (ctx.params.event == "resume") {
						const absence = await ctx.call("bus.absences.find", {
							query: { status: "ongoing", application_id: res[0].id },
						});
						for (const ab of absence) {
							await ctx.call("bus.absences.patch", { id: ab.id, status: "completed" });
						}
						return res;
					}
				},
			],
			resume: [
				async function updateOngoingAbsence(ctx, res) {
					const absence = await ctx.call("bus.absences.find", {
						query: { status: "ongoing", application_id: res[0].id },
					});
					for (const ab of absence) {
						await ctx.call("bus.absences.patch", { id: ab.id, status: "completed" });
					}
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		automatic_change_status: {
			visibility: "public",
			async handler(ctx) {
				const configuration = await ctx.call("bus.configurations.list");
				if (
					configuration["ASSIGNED_STATUS_AUTO_CHANGE"] == "true" &&
					configuration["EVENT_FOR_AUTO_CHANGE"] &&
					configuration["NUNBER_DAYS_FOR_CHANGE"]
				) {
					let startdate = moment()
						.subtract(configuration["NUNBER_DAYS_FOR_CHANGE"], "days")
						.format("YYYY-MM-DD");
					const applications = await this._find(ctx, {
						query: (qb) => {
							qb.where("status", "=", "approved");
							qb.where("updated_at", "<=", startdate);
							return qb;
						},
						withRelated: false,
					});
					for (const application of applications) {
						ctx.params = {};
						ctx.params.id = application.id;
						try {
							const stateMachine = ApplicationStateMachine.createStateMachine(
								application.status,
								ctx,
							);
							ctx.params.notes =
								"[Automático] Estado da candidatura alterado após " +
								configuration["STATUS_AUTO_CHANGE_DAYS"] +
								" dias";
							const status = configuration["EVENT_FOR_AUTO_CHANGE"].toLowerCase();
							await stateMachine[status]();
						} catch (err) {
							this.logger.info(err);
						}
					}
				}
			},
		},
		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "bus:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: [
						"approve",
						"notapprove",
						"accept",
						"reject",
						"close",
						"cancel",
						"withdrawal",
						"quit",
						"withdrawalreject",
						"suspend",
						"resume",
					],
				},
			},
			async handler(ctx) {
				const application = await ctx.call("bus.applications.get", ctx.params);
				const stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				if (stateMachine.can(ctx.params.event.toUpperCase())) {
					const result = await stateMachine[ctx.params.event.toLowerCase()](application[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		cancel: {
			rest: "POST /:id/cancel",
			visibility: "published",
			scope: "bus:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("bus.applications.get", ctx.params);
				const stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				if (application[0].user_id != ctx.meta.user.id) {
					throw new ValidationError(
						"Don't have permissions to change state",
						"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
						{},
					);
				}
				if (stateMachine.can("CANCEL")) {
					const result = await stateMachine["cancel"](application[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},

		accept: {
			rest: "POST /accept/:id",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "bus:applications:create",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				const stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				if (application[0].user_id != ctx.meta.user.id) {
					throw new ValidationError(
						"Don't have permissions to change state",
						"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
						{},
					);
				}
				if (stateMachine.can("ACCEPT")) {
					const result = await stateMachine["accept"](application[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},

		reject: {
			rest: "POST /reject/:id",
			params: {
				id: { type: "number", integer: true, convert: true },
				reject_reason: { type: "string", max: 255, optional: true, nullable: true },
			},
			visibility: "published",
			scope: "bus:applications:create",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				const stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				if (application[0].user_id != ctx.meta.user.id) {
					throw new ValidationError(
						"Don't have permissions to change state",
						"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
						{},
					);
				}
				if (stateMachine.can("REJECT")) {
					const result = await stateMachine["reject"](application[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					if (ctx.params.reject_reason) {
						await ctx.call("bus.applications.patch", {
							id: application[0].id,
							reject_reason: ctx.params.reject_reason,
						});
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},

		resume: {
			rest: "POST /:id/resume",
			visibility: "published",
			scope: "bus:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await ctx.call("bus.applications.get", ctx.params);
				const stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				if (application[0].user_id != ctx.meta.user.id) {
					throw new ValidationError(
						"Don't have permissions to change state",
						"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
						{},
					);
				}
				if (stateMachine.can("RESUME")) {
					const result = await stateMachine["resume"](application[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_APPLICATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_APPLICATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},

		getWithdrawalsByApplicationId: {
			visibility: "published",
			rest: "GET /withdrawal/:id",
			scope: "bus:applications:read",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				return await ctx.call("bus.withdrawals.find", { query: { application_id: ctx.params.id } });
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		generateDeclaration: {
			visibility: "published",
			rest: "POST /:id/declaration",
			scope: "bus:applications:create",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				if (application[0].status != "accepted") {
					throw new ValidationError(
						"Only can generate a declaration on 'accepted' applciation status",
						"BUS_DECLARATION_INVALID_APPLICATION_STATUS",
						{},
					);
				}
				application[0].validate_date = moment().add(60, "days").format("DD-MM-YYYY");
				application[0].date = moment().format("DD-MM-YYYY");

				return ctx.call("reports.templates.print", {
					key: "MOBILITY_APPLICATION_DECLARATION",
					options: {
						convertTo: "pdf",
					},
					confirmation_path: "bus.applications.confirm_application_declaration",
					extra_info: {
						id: application[0].id,
					},
					data: {
						...application[0],
					},
				});
			},
		},
		print_list_applications: {
			rest: "GET /reports-list",
			visibility: "published",
			params: {
				academic_year: { type: "string", optional: true, nullable: true },
				organic_unit: { type: "number", integer: true, convert: true, optional: true },
			},
			async handler(ctx) {
				const query = {};
				if (ctx.params.academic_year) {
					query.academic_year = ctx.params.academic_year;
				}
				if (ctx.params.organic_unit) {
					query.school_id = ctx.params.organic_unit;
				}
				const applications = await ctx.call("bus.applications.find", {
					query,
				});
				return ctx.call("reports.templates.print", {
					key: "MOBILITY_APPLICATION_LIST",
					data: { applications: applications },
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"bus.applications.confirm_application_declaration"(ctx) {
			return ctx
				.call("bus.applications.patch", {
					id: ctx.params.extra_info.id,
					declaration_file_id: ctx.params.file_id,
				})
				.then((application) => {
					this.sendDeclarationReadyNotification(ctx, application);
				});
		},
	},

	/**
	 * Methods
	 */
	methods: {
		sendDeclarationReadyNotification(ctx, application) {
			ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "MOBILITY_APPLICATION_DECLARATION_READY",
				user_id: application[0].user_id,
				user_data: {},
				data: {
					application: application[0],
				},
				variables: {},
				external_uuid: null,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
