"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { configurationsKeys } = require("../values/enums");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.configurations",
	table: "configurations",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "enum", values: configurationsKeys },
			value: [{ type: "string", optional: true,  nullable: true }, { type: "number", optional: true,  nullable: true  }, { type: "object", optional: true,  nullable: true  }],
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {

				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach(conf => {
					result[conf.key] = conf.value;
				});
				return result;
			}
		},

		getByKey: {
			cache: {
				keys: ["key"],
			},
			visibility: "published",
			rest: "GET /:key",
			scope: "bus:configurations:read",
			params: {
				key: { type: "enum", values: configurationsKeys },
			},
			handler(ctx) {
				return this.getConfigurationByKey(ctx.params.key);
			},
		},
		updateKeyValue: {
			visibility: "published",
			rest: "PUT /:key",
			scope: "bus:configurations:update",
			params: {
				value: [{ type: "string", optional: true,  nullable: true }, { type: "number", optional: true,  nullable: true }, { type: "object", optional: true,  nullable: true }],
			},
			async handler(ctx) {
				let configuration;
				let params = ctx.params;

				configuration = await ctx.call("bus.configurations.getByKey", {
					key: ctx.params.key,
				});

				if (configuration[0].id) params.id = configuration[0].id;
				else throw new Errors.ValidationError("Empty configuration object", "EMPTY_CONFIGURATION", {});
				return this._update(ctx, params, true);
			},
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		getConfigurationByKey(key) {
			return this.adapter.find({
				query: (qb) => {
					qb.where("key", key);
					return qb;
				},
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
