"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.days_group",
	table: "days_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "days_group")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"day_id",
			"type_day_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			day_id: { type: "number" },
			type_day_id: { type: "number", positive: true },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		findDaysOfTypeDay: {
			params: {
				type_day_id: { type: "number" },
			},
			async handler(ctx) {

				const days_groups_lines = await this.findDaysWeekOfTypeDay(ctx.params.type_day_id);



				const ids = days_groups_lines.map(app => {
					const container = {};
					container.id = app.day_id;
					container.group_id = app.id;
					return container;
				});


				return ctx.call("bus.days_weeks.find", {
					query: {
						id: ids.map((elemnt)=> elemnt.id )
					}
				}).then((days_weeks) => {
					return days_weeks.map((day_week) => {
						day_week.group_day = ids.find((elemnt) => elemnt.id === day_week.id ).group_id;
						return day_week;
					});
				});

			}
		},

		save_days: {
			params: {
				type_day_id: { type: "number", positive: true },
				days_week_ids: {
					type: "array",
					items: {
						type: "number"
					}
				},
			},
			async handler(ctx) {

				const entities = ctx.params.days_week_ids.map((day_week_id) => ({
					type_day_id: ctx.params.type_day_id,
					day_id: day_week_id,
				}));
				return this._insert(ctx, { entities });
			}
		},

		remove_day: {
			params: {
				id: { type: "number", positive: true },
			},
			async handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				return this._remove(ctx, params).then(() => { });
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		findDaysWeekOfTypeDay(id) {
			return this.adapter.find( { query: { type_day_id: id } });
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
