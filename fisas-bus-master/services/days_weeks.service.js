"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.days_weeks",
	table: "days_weeks",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "days_weeks")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"day",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			day: { type: "string" },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
