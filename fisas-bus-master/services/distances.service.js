"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const { MoleculerError } = require("moleculer").Errors;

module.exports = {
	name: "bus.distances",
	table: "distances",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "distances")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"local1_id",
			"local2_id",
			"route_id",
			"distance",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			local1_id: { type: "number", positive: true },
			local2_id: { type: "number", positive: true },
			route_id: { type: "number", positive: true },
			distance: { type: "number" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_distances: {
			params: {
				distances: {
					type: "array",
					items: { type: "object", props: {
						local1_id: { type: "number", positive: true },
						local2_id: { type: "number", positive: true },
						route_id: { type: "number", positive: true },
						distance: { type: "number" },
						created_at: { type: "date" },
						updated_at: { type: "date" },
					} },
				}
			},
			async handler(ctx) {
				return this.saveDistances(ctx);
			}
		},
		getRouteDistanceBetweenTwoLocals: {
			params: {
				route_id: { type: "number", positive: true },
				local1_id: { type: "number", positive: true },
				local2_id: { type: "number", positive: true }
			},
			async handler(ctx) {
				return this.getDistanceBetweenTwoLocals(ctx);
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		getDistanceBetweenTwoLocals(ctx) {
			return this.adapter.find({ query: { local1_id: ctx.params.local1_id, local2_id: ctx.params.local2_id, route_id: ctx.params.route_id } });
		},
		async saveDistances(ctx) {
			if (ctx.params.distances && Array.isArray(ctx.params.distances)) {
				ctx.params["entities"] = ctx.params.distances;
				return this._insert(ctx, ctx.params);
			} else {
				throw new MoleculerError("Not found distances", 400);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
