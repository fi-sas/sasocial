"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

const _ = require("lodash");

module.exports = {
	name: "bus.hours",
	table: "hours",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "hours")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"hour",
			"local_id",
			"timetable_id",
			"route_order_id",
			"line_number",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["local"],
		withRelateds: {
			local(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "local", "local_id");
			},
		},
		entityValidator: {
			hour: { type: "string" },
			local_id: { type: "number", positive: true },
			timetable_id: { type: "number", positive: true },
			route_order_id: { type: "number", positive: true },
			line_number: { type: "number", positive: true }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_hours: {
			params: {
				timetable_id: { type: "number", positive: true },
				locals: {
					type: "array",
					items: {
						type: "number" } },
				hours: {
					type: "array",
					items: {
						type: "array", items: {
							type: "string"
						}
					},
				}
			},
			async handler(ctx) {
				const entities = await this.createHoursLines(ctx);
				return this._insert(ctx, { entities });
			}
		},
		save_line_hours: {
			params: {
				timetable_id: { type: "number", positive: true },
				line_number: { type: "number", positive: true },
				route_id: { type: "number", positive: true },
				locals: {
					type: "array",
					items: {
						type: "number" } },
				hours: {
					type: "array",
					items: {
						type: "array", items: {
							type: "string"
						}
					},
				}
			},
			async handler(ctx) {
				const entities = await this.createHoursLines(ctx, ctx.params.line_number);
				await this._insert(ctx, { entities });
			}
		},
		findHoursForTimetable: {
			params: {
				timetable_id: { type: "number", positive: true },
			},
			async handler(ctx) {
				let hours = await this.findHoursOfTimetable(ctx);
				let grouped = _.groupBy(hours, (hour) => {
					return hour.line_number;
				});

				return Object.values(grouped);
			}
		},
		deleteLineOfTimetable: {
			params: {
				timetable_id: { type: "number", positive: true },
				line_number: { type: "number", positive: true },
			},
			async handler(ctx) {
				await this.deleteLineOfTimetable(ctx, ctx.params.timetable_id, ctx.params.line_number);
				return null;
			}
		},
		delete_hours: {
			params:{
				id: { type: "number", integer: true, convert: true }
			},
			async handler(ctx){
				await this.adapter.removeMany({
					timetable_id: ctx.params.id
				});
				this.clearCache();
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async createHoursLines(ctx, line_number = null) {
			return new Promise((resolve, reject) => {
				let lines = [];
				let order;
				try {
					ctx.params.hours.forEach(async (column, c_index) => {
						ctx.params.hours[c_index].forEach(async (hour, h_index) => {
							order = await ctx.call("bus.route_order.find", {
								query: {
									route_id: ctx.params.route_id,
									local_id: ctx.params.locals[h_index],
									order_table: h_index + 1
								}
							});
							lines.push({
								hour:ctx.params.hours[c_index][h_index],
								local_id: ctx.params.locals[h_index],
								timetable_id: ctx.params.timetable_id,
								route_order_id: order.length !== 0 ? order[0].id : null,
								line_number: line_number === null ? c_index + 1: line_number,
								created_at: new Date(),
								updated_at: new Date(),
							});
							if (c_index === ctx.params.hours.length -1 && h_index === ctx.params.hours[c_index].length -1) {
								resolve(lines); }
						});

					});
				} catch(error) {
					reject(error);
				}
			});
		},
		async findHoursOfTimetable(ctx) {
			return ctx.call("bus.hours.find", { query: { timetable_id: ctx.params.timetable_id }, sort: "route_order_id", withRelated: ["local"] });
		},
		async deleteLineOfTimetable(ctx, timetable_id, line_number) {

			const hours_deleted = await this.adapter.removeMany({
				timetable_id: timetable_id,
				line_number: line_number
			});
			this.clearCache();
			const hours_time_table = await this._find(ctx, {
				query: {
					timetable_id: timetable_id,
				}
			});
			if (hours_time_table.length === 0) {
				return await ctx.call("bus.timetables.remove", { id: timetable_id });
			}else {
				return hours_deleted;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
