"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "bus.links",
	table: "links",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "links")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"local_route1_id",
			"local_route2_id",
			"route1_id",
			"route2_id",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			local_route1(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "local_route1", "local_route1_id");
			},
			local_route2(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "local_route2", "local_route2_id");
			},
			route1(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "route1", "route1_id");
			},
			route2(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "route2", "route2_id");
			},
		},
		entityValidator: {
			local_route1_id: { type: "number", positive: true },
			local_route2_id: { type: "number", positive: true },
			route1_id: { type: "number", positive: true },
			route2_id: { type: "number", positive: true },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				"validateEqualLink",
				async function validateIsDifferentRoutes(ctx) {
					if (ctx.params.route1_id && ctx.params.route2_id) {
						if(ctx.params.route1_id === ctx.params.route2_id) {
							throw new Errors.ValidationError("You cannot create a link to the same route", "LINK_SAME_ROUTES", {});
						}
					} else {
						throw new Errors.ValidationError("'route2_id' and 'route2_id' is required!", "ROUTE_ID_ONE_TWO_REQUIRED", {});					}
				},
				async function validateLocalRoute1(ctx) {
					if (ctx.params.local_route1_id) {
						await ctx.call("bus.locals.get", { id: ctx.params.local_route1_id });
					} else {
						throw new Errors.ValidationError("'local_route1_id' is required!", "LOCAL_ROUTE_ONE_ID_REQUIRED", {});
					}
				},
				async function validateLocalRoute1(ctx) {
					if (ctx.params.local_route2_id) {
						await ctx.call("bus.locals.get", { id: ctx.params.local_route2_id });
					} else {
						throw new Errors.ValidationError("'local_route2_id' is required!", "LOCAL_ROUTE_TWO_ID_REQUIRED", {});
					}
				},
				async function validateRoute1(ctx) {
					if (ctx.params.route1_id) {
						await ctx.call("bus.routes.get", { id: ctx.params.route1_id });
					} else {
						throw new Errors.ValidationError("'route1_id' is required!", "ROUTE_ONE_ID_REQUIRED", {});
					}
				},
				async function validateRoute2(ctx) {
					if (ctx.params.route2_id) {
						await ctx.call("bus.routes.get", { id: ctx.params.route2_id });
					} else {
						throw new Errors.ValidationError("'local_route2_id' is required!", "ROUTE_TWO_ID_REQUIRED", {});
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findLinksOfRoute: {
			params: {
				route_id: { type: "number" },
			},
			async handler(ctx) {
				return await this.findLinksOfRoute(ctx.params.route_id);
			}
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		findLinksOfRoute(id) {
			return this.adapter.find({
				query: (qb) => {
					qb.where("route1_id", id);
					qb.orWhere("route2_id", id);
					return qb;
				}
			});
		},
		findNextIntersectionsForOtherRoute(id) {
			return this.adapter.find({
				query: (qb) => {
					qb.where("route1_id", id);
					return qb;
				}
			});
		},

		async validateEqualLink(ctx){
			const link = await this._find(ctx, { query : {
				local_route1_id: ctx.params.local_route1_id,
				local_route2_id: ctx.params.local_route2_id,
				route1_id: ctx.params.route1_id,
				route2_id: ctx.params.route2_id,
				active: true
			} });
			if(link.length > 0){
				throw new Errors.ValidationError("Router link alreday exist", "BUS_ROUTE_LINK_ALREADY_EXIST", {});
			}

		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
