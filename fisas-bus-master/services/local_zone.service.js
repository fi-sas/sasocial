"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.local_zone",
	table: "local_zone",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "local_zone")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"local_id",
			"zone_id"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			local_id: { type: "number", positive: true },
			zone_id: { type: "number", positive: true },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		},
		after: {
			create: [],
			update: [],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findLocalsOfZone: {
			params: {
				zone_id: { type: "number", positive: true  },
			},
			async handler(ctx) {
				const locals_in_zone = await this.findLocalsOfZone(ctx.params.zone_id);

				if (locals_in_zone.length !== 0) {
					const ids = locals_in_zone.map(d => d["local_id"]);
					return  Promise.all(
						ids.map(async id => {
							const query = {};
							query["id"] = id;
							let locals = await ctx.call("bus.locals.find", {
								query
							});
							locals[0].zone_id = ctx.params.zone_id;
							return locals[0];
						})
					);
				} else {
					return [];
				}
			}
		},
		save_locals: {
			params: {
				zone_id: { type: "number", positive: true },
				locals_ids: {
					type: "array",
					items: {
						type: "number", positive: true
					}
				}
			},
			async handler(ctx) {

				const entities = ctx.params.locals_ids.map((local_id) => ({
					zone_id: ctx.params.zone_id,
					local_id: local_id,
				}));
				return this._insert(ctx, { entities });
			}
		},
		removeLocalZones: {
			params: {
				zone_id: { type: "number", integer: true, convert: true }
			},
			async handler(ctx) {
				const local_zones = await this.adapter.removeMany({
					zone_id: ctx.params.zone_id
				});
				this.clearCache();
				return local_zones;

			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		findLocalsOfZone(id) {
			return this.adapter.find({ query: { zone_id: id } });
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
