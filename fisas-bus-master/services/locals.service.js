"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.locals",
	table: "locals",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "locals")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"local",
			"region",
			"latitude",
			"longitude",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			routes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.route_order.findRoutesOfLocal", {
								local_id: doc.id,
							})
							.then((res) => (doc.routes = res));
					}),
				);
			},
		},
		entityValidator: {
			local: { type: "string" },
			region: { type: "string" },
			latitude: { type: "number" },
			longitude: { type: "number" },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async nearby(latitude, longitude) {
			return this.adapter.db(this.table).raw(`SELECT id, local, region, latitude, longitude, 111.045 * DEGREES(ACOS(COS(RADIANS(${latitude}))
		   * COS(RADIANS(latitude))
		   * COS(RADIANS(longitude) - RADIANS(${longitude}))
		   + SIN(RADIANS(${latitude}))
		   * SIN(RADIANS(latitude))))
		   AS distance_in_km
		  FROM locals
		  ORDER BY distance_in_km ASC
		  LIMIT 1'`);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
