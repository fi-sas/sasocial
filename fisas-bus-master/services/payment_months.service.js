"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.payment_months",
	table: "payment_months",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "payment_months")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"month",
			"year",
			"value",
			"paid",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.applications", "application", "application_id");
			},
		},
		entityValidator: {
			application_id: { type: "number", positive: true },
			month: { type: "string" },
			year: { type: "number" },
			value: { type: "number" },
			paid: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				async function validateApplication(ctx) {
					if (ctx.params.application_id) {
						await ctx.call("bus.applications.get", { id: ctx.params.application_id });
					} else {
						throw new Errors.ValidationError("'application_id' is required!", "APPLICANTION_ID_REQUIRED", {});
					}
				},
				async function validateMonth(ctx) {
					if (ctx.params.application_id) {
						let payment_month = await ctx.call("bus.payment_months.find", { query : { application_id: ctx.params.application_id, month: ctx.params.month, year: ctx.params.year } });
						if (payment_month.length !== 0) {
							throw new Errors.ValidationError("There is already a payment this month", "ALREADY_PATMENT_FOR_MONTH", {});
						}
					} else {
						throw new Errors.ValidationError("'application_id' is required!", "APPLICANTION_ID_REQUIRED", {});
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
		after: {
			create: [],
			update: [],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
