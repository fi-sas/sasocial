"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.places",
	table: "places",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "places")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"cod_district",
			"cod_place",
			"place",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			cod_district: { type: "number", positive: true },
			cod_place: { type: "number", positive: true },
			place: { type: "string" },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
