"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ticketTypes } = require("../values/enums");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.price_tables",
	table: "price_tables",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "price_tables")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"route_id",
			"profile_id",
			"tax_id",
			"account_id",
			"ticket_type",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["prices"],
		withRelateds: {
			prices(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.prices.findPriceOfPriceTable", { price_table_id: doc.id })
							.then((res) => {
								doc.prices = res;
							});
					}),
				);
			},
			route(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.routes", "route", "route_id");
			},
			profile(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.profiles", "profile", "profile_id");
			},
			tax(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "tax", "tax_id");
			},
			account(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "current_account.accounts", "account", "account_id");
			},
		},
		entityValidator: {
			route_id: { type: "number", positive: true },
			profile_id: { type: "number", positive: true },
			tax_id: { type: "number", positive: true },
			account_id: { type: "number", positive: true, optional: true },
			ticket_type: { type: "enum", values: ticketTypes },
			active: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			create: [
				async function validatePricesArray(ctx) {
					if (ctx.params.prices) {
						if (ctx.params.prices.length !== 0) {
							/*  #TODO Fazer a validação dos campos dentro do prices */
						} else {
							throw new Errors.ValidationError("Prices is empty", "PRICES_EMPTY", {});
						}
					} else {
						throw new Errors.ValidationError("'prices' is required!", "PRICES_REQUIRED", {});
					}
				},
				async function validateRoute(ctx) {
					if (ctx.params.route_id) {
						await ctx.call("bus.routes.get", { id: ctx.params.route_id });
					} else {
						throw new Errors.ValidationError("'route_id' is required!", "ROUTE_ID_REQUIRED", {});
					}
				},
				async function validateTax(ctx) {
					if (ctx.params.tax_id) {
						await ctx.call("configuration.taxes.get", { id: ctx.params.tax_id });
					} else {
						throw new Errors.ValidationError("'tax_id' is required!", "TAX_ID_REQUIRED", {});
					}
				},
				async function validateProfile(ctx) {
					if (ctx.params.profile_id) {
						await ctx.call("authorization.profiles.get", { id: ctx.params.profile_id });
					} else {
						throw new Errors.ValidationError("'profile_id' is required!", "PROFILE_ID_REQUIRED", {});
					}
				},
				async function validateAccount(ctx) {
					if (ctx.params.account_id) {
						await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["removePrices"],
		},
		after: {
			create: ["savePrices"],
			update: ["savePrices"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"bus.prices.updated"() {
			this.clearCache();
		}
	},

	/**
	 * Methods
	 */
	methods: {
		async savePrices(ctx, res) {
			await ctx.call("bus.prices.save_prices", {
				price_table_id: res[0].id,
				prices: ctx.params.prices,
			});

			res[0].prices = await ctx.call("bus.prices.findPriceOfPriceTable", {
				price_table_id: res[0].id,
			});

			return res;
		},
		async removePrices(ctx, res) {
			typeof res;
			return ctx
				.call("bus.prices.find", {
					query: {
						price_table_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(async (priceRel) => await ctx.call("bus.prices.remove", { id: priceRel.id }));
				})
				.catch((err) => this.logger.error("Unable to delete Price relation!", err));
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
