"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.prices",
	table: "prices",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "price_tables")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"number_stop",
			"value",
			"description",
			"price_table_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			priceTable(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.price_tables", "priceTable", "price_table_id");
			},
		},
		entityValidator: {
			number_stop: { type: "number" },
			value: { type: "number", convert: true },
			description: { type: "string" },
			price_table_id: { type: "number", positive: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: [],
			patch: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findPriceOfPriceTable: {
			params: {
				price_table_id: { type: "number", positive: true },
			},
			async handler(ctx) {
				const prices_in_price_table = await this.findPriceOfPriceTable(ctx.params.price_table_id);
				const ids = prices_in_price_table.map((d) => d["id"]);
				return Promise.all(
					ids.map(async (id) => {
						const query = {};
						query["id"] = id;
						let prices = await ctx.call("bus.prices.find", {
							query,
						});
						return prices[0];
					}),
				);
			},
		},
		save_prices: {
			params: {
				price_table_id: { type: "number", positive: true },
				prices: {
					type: "array",
					items: {
						type: "object",
						props: {
							number_stop: { type: "number" },
							value: { type: "number" },
							description: { type: "string" },
						},
					},
				},
			},
			async handler(ctx) {
				const entities = await this.createPriceLines(ctx);
				return this._insert(ctx, { entities });
			},
		},
		patch: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			visibility: "public",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async createPriceLines(ctx) {
			return new Promise((resolve, reject) => {
				let lines = [];
				try {
					ctx.params.prices.forEach((price, p_index) => {
						lines.push({
							price_table_id: ctx.params.price_table_id,
							number_stop: price.number_stop,
							value: price.value,
							description: price.description,
							created_at: new Date(),
							updated_at: new Date(),
						});
						if (p_index === ctx.params.prices.length - 1) {
							resolve(lines);
						}
					});
				} catch (error) {
					reject(error);
				}
			});
		},
		findPriceOfPriceTable(id) {
			return this.adapter.find({ query: { price_table_id: id }, sort: "id" });
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
