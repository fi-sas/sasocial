"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.route_order",
	table: "route_order",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "route_order")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"local_id",
			"route_id",
			"order_table",
			"instruction",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			local_id: { type: "number", positive: true, integer: true },
			route_id: { type: "number", positive: true, integer: true },
			order_table: { type: "number", positive: true, integer: true },
			instruction: { type: "string" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findRoutesOfLocal: {
			params: {
				local_id: { type: "number" },
			},
			async handler(ctx) {
				const routes_in_route_order = await this.findRoutesOfLocal(ctx.params.local_id);
				const ids = routes_in_route_order.map((d) => d["route_id"]);
				return Promise.all(
					ids.map(async (id) => {
						const query = {};
						query["id"] = id;
						let route = await ctx.call("bus.routes.find", {
							query,
						});
						return _.first(route);
					}),
				);
			},
		},
		findLocalOfRoute: {
			params: {
				route_id: { type: "number" },
			},
			async handler(ctx) {
				let locals_in_route_order = await this.findLocalsOfRoute(ctx.params.route_id);
				const ids = locals_in_route_order.map((route_order) => route_order["local_id"]);
				return Promise.all(
					ids.map(async (id, index) => {
						const query = {};
						query["id"] = id;
						let local = await ctx.call("bus.locals.find", {
							query,
						});
						if (local.length !== 0) {
							let found = locals_in_route_order.find(
								(route_order) => _.first(local).id === route_order.local_id,
							);
							if (found !== undefined) {
								if (found.instruction) {
									local[0].instruction = found.instruction;
								} else {
									local[0].instruction = "";
								}
							} else {
								local[0].instruction = "";
							}

							if (ids.length !== index + 1) {
								let distance = await ctx.call("bus.distances.getRouteDistanceBetweenTwoLocals", {
									route_id: ctx.params.route_id,
									local1_id: id,
									local2_id: ids[index + 1],
								});
								local[0].distance = distance.length !== 0 ? _.first(distance).distance : 0;
							} else {
								local[0].distance = 0;
							}
						}
						return local[0];
					}),
				);
			},
		},
		findLocalIdsOfRoute: {
			params: {
				route_id: { type: "number" },
			},
			async handler(ctx) {
				let locals_in_route_order = await this.findLocalsOfRoute(ctx.params.route_id);
				return locals_in_route_order.map((route_order) => route_order["local_id"]);
			},
		},
		isLocalContainInRoute: {
			params: {
				local_id: { type: "number" },
				route_id: { type: "number" },
			},
			async handler(ctx) {
				let route_order = await ctx.call("bus.route_order.find", {
					query: { local_id: ctx.params.local_id, route_id: ctx.params.route_id },
				});
				return route_order.length !== 0;
			},
		},
		save_locals: {
			params: {
				route_id: { type: "number", positive: true },
				locals: {
					type: "array",
					items: {
						type: "object",
						props: {
							id: { type: "number", positive: true },
							instruction: { type: "string" },
						},
					},
				},
			},
			async handler(ctx) {
				const entities = ctx.params.locals.map((local, order) => ({
					route_id: ctx.params.route_id,
					local_id: local.id,
					order_table: order + 1,
					instruction: local.instruction,
					created_at: new Date(),
					updated_at: new Date(),
				}));
				return this._insert(ctx, { entities });
			},
		},

		findRoutesByLocals: {
			params: {
				departure_local_id: {
					type: "number",
				},
				arrival_local_id: {
					type: "number",
				},
				types_days_ids: {
					type: "array",
					items: {
						type: "number",
					},
				},
				seasons_ids: {
					type: "array",
					items: {
						type: "number",
					},
				},
			},
			async handler(ctx) {
				const routes_ids = await this.findRoutesByLocals(ctx);
				return Promise.all(
					routes_ids.map((id) => {
						const query = {};
						let withRelated = {};
						query["id"] = id;
						withRelated = "locals,timetables,linksIn,links,contact,zones,price_tables";
						ctx.meta.withRelateds = {
							timetables: {
								type_day_id: ctx.params.types_days_ids,
								season_id: ctx.params.seasons_ids,
							},
						};
						return ctx
							.call("bus.routes.find", {
								query,
								withRelated,
							})
							.then((r) => r[0]);
					}),
				);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		findRoutesOfLocal(id) {
			return this.adapter.find({ query: { local_id: id } });
		},
		findLocalsOfRoute(id) {
			return this.adapter.find({ query: { route_id: id } });
		},
		findRoutesByLocals(ctx) {
			return this.adapter
				.raw(
					`select ro.route_id from route_order ro
			inner join route_order ro1 on ro1.local_id = ? and ro1.route_id = ro.route_id
			inner join route_order ro2 on ro2.local_id = ? and ro2.route_id = ro.route_id
			where ro1.order_table < ro2.order_table
			group by ro.route_id`,
					[ctx.params.departure_local_id, ctx.params.arrival_local_id],
				)
				.then((raw) => raw.rows.map((r) => r.route_id));
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
