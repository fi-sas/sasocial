"use strict";
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const { MoleculerError } = require("moleculer").Errors;
const moment = require("moment");
const _ = require("lodash");
const { Errors } = require("@fisas/ms_core").Helpers;

module.exports = {
	name: "bus.route_search",

	mixins: [],

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		addToCart: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/addCart",
			},
			params: {
				hash: { type: "string" },
			},
			async handler(ctx) {
				const hash_data = await ctx.call("bus.tickets_hashs.find", {
					query: {
						hash: ctx.params.hash,
					},
				});

				if (hash_data.length === 0) {
					return [];
				}

				const price = await ctx.call("bus.prices.get", {
					id: hash_data[0].price_id,
					withRelated: "priceTable",
				});

				const departure_local = await ctx.call("bus.locals.get", {
					id: hash_data[0].departure_local_id,
				});

				const arrival_local = await ctx.call("bus.locals.get", {
					id: hash_data[0].arrival_local_id,
				});

				const entity = {
					account_id: price[0].priceTable.account_id,
					service_id: 3,
					product_code: "BUS_TICKET",
					article_type: "BUS_TICKET",
					quantity: 1,
					unit_value: price[0].value,
					vat_id: price[0].priceTable.tax_id,
					name: `Bilhete ${departure_local[0].local} -> ${arrival_local[0].local}`,
					description: `Bilhete ${departure_local[0].local} -> ${arrival_local[0].local}`,
					location: "Bus " + "Nome Autocarro",
					max_quantity: 30,
					service_confirm_path: "bus.tickets_bought.create_order",
					service_cancel_path: "bus.tickets_bought.cancel_order",
					extra_info: {
						price_id: price[0].id,
						route_id: price[0].priceTable.route_id,
						departure_local_id: hash_data[0].departure_local_id,
						arrival_local_id: hash_data[0].arrival_local_id,
					},
					expires_in: null,
				};
				return ctx.call("payments.cart.addCartItem", entity);
			},
		},
		list: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/",
			},
			cache: false, // TODO: REMOVE THIS IN PRODUCTION ?
			params: {
				origin: [{ type: "number", convert: true }, { type: "string" }],
				destination: [{ type: "number", convert: true }, { type: "string" }],
				date: { type: "string", optional: true },
			},
			async handler(ctx) {
				let mode;
				let types_days;
				let routes = [];
				let locals_ids = [];
				let seasons = [];
				let date;
				let locals = [];

				mode = await this.getModeForSearch(ctx);
				types_days = await this.getTypesDays(ctx);
				seasons = await this.getCurrentSeasonsIDS(ctx);
				date = this.getDate(ctx);
				switch (mode) {
					case 0:
						locals_ids.push(ctx.params.destination);
						locals = await this.getLocals(locals_ids, ctx);
						break;
					case 1:
						locals_ids.push(ctx.params.origin);
						locals = await this.getLocals(locals_ids, ctx);
						break;
					case 2:
						locals_ids.push(ctx.params.origin);
						locals_ids.push(ctx.params.destination);
						locals = await this.getLocals(locals_ids, ctx);
						break;
					default:
						locals_ids = [];
				}

				const traces = [];

				routes = await ctx.call("bus.route_order.findRoutesByLocals", {
					departure_local_id: ctx.params.origin,
					arrival_local_id: ctx.params.destination,
					types_days_ids: types_days,
					seasons_ids: seasons,
				});

				this.logger.info("findRoutesByLocals total: " + routes.length);
				this.logger.info(routes.map((r) => r.id).join(","));

				for (let route of routes) {
					this.logger.info("forEachRota ID:" + route.id);

					const locals_in_route = [
						...new Set(route.locals.filter((l) => locals_ids.includes(l.id)).map((l) => l.id)),
					];

					if (locals_in_route.length > 1) {
						// ROUTE HAS THE TWO LOCALS

						let routeInfo = {
							route: [],
							stops: 0,
							departure_hour: null,
							departure_local_id: null,
							departure_place: null,
							arrival_hour: null,
							arrival_local_id: null,
							arrival_place: null,
							date: null,
							duration: 0,
							represents_connection: route.represents_connection,
							tickets_prices_total: 0,
						};

						const route_trace = {
							route_id: route.id,
							route_line: route.name,
							external: route.external,
							active: route.active,
							steps: [],
							departure_hour: null,
							arrival_hour: null,
							contacts: [],
							prices: [],
						};

						route_trace.steps = this.getSteps(route, locals_ids[0], locals_ids[1]);
						this.logger.info("route_trace.steps");
						this.logger.info(route_trace.steps);
						if (route_trace.steps.length > 0) {
							route_trace.departure_hour = route_trace.steps[0].departure_time;
							route_trace.arrival_hour =
								route_trace.steps[route_trace.steps.length - 1].departure_time;
							route_trace.prices = await this.calcPrices(
								ctx,
								route_trace.steps,
								route.zones,
								route.price_tables,
								ctx.meta.user,
							);

							// ROUTE INFO
							routeInfo.departure_hour = route_trace.steps[0].departure_time;
							routeInfo.arrival_hour =
								route_trace.steps[route_trace.steps.length - 1].departure_time;
							routeInfo.departure_local_id = route_trace.steps[0].local_id;
							routeInfo.departure_place = route_trace.steps[0].local;
							routeInfo.arrival_local_id = route_trace.steps[route_trace.steps.length - 1].local_id;
							routeInfo.arrival_place = route_trace.steps[route_trace.steps.length - 1].local;
							routeInfo.stops = route_trace.steps.length;
							routeInfo.duration = this.calcDuration(
								routeInfo.departure_hour,
								routeInfo.arrival_hour,
							);

							routeInfo.route.push(route_trace);
							traces.push(routeInfo);
						}
					} else {
						// THIS ROUTE MAY REQUIRE LINKS
						// FIND THE ROUTES OF THE ORIGIN
						const is_route_origin = !!route.locals.find((l) => locals_ids[0] === l.id);
						// FIND THE ROUTES OF DESTINATION
						const is_route_destination = !!route.locals.find((l) => locals_ids[1] === l.id);
						// SEARCH FOR LINKS BETWEEN ROUTES

						if (is_route_origin) {
							let other_routes_by_link = [];
							for (let link of route.links) {
								if (link.route1_id === route.id) {
									link.is_route1 = true;
									link.other_routes = routes.filter((r) => r.id === link.route2_id);
								}
								if (link.route2_id === route.id) {
									link.is_route1 = false;
									link.other_routes = routes.filter((r) => r.id === link.route1_id);
								}

								other_routes_by_link.push(link);
							}

							let route_link = {
								instruction: "Troca de Transporte",
								vehicle: "CAMINHADA",
							};

							for (let other_route_link of other_routes_by_link) {
								for (let other_route of other_route_link.other_routes) {
									let routeInfo = {
										route: [],
										stops: 0,
										departure_hour: null,
										departure_local_id: null,
										departure_place: null,
										arrival_hour: null,
										arrival_local_id: null,
										arrival_place: null,
										date: null,
										duration: 0,
										represents_connection: route.represents_connection,
										tickets_prices_total: 0,
									};

									const route_trace = {
										route_id: route.id,
										route_line: route.name,
										external: route.external,
										active: route.active,
										steps: [],
										departure_hour: null,
										arrival_hour: null,
										contacts: [],
										prices: [],
									};

									let route_to_evaluate = null;
									let other_route_to_evaluate = null;

									if (is_route_origin) {
										route_to_evaluate = route;
										other_route_to_evaluate = other_route;
									}
									if (is_route_destination) {
										route_to_evaluate = other_route;
										other_route_to_evaluate = route;
									}

									if (other_route_link.is_route1) {
										// SEARCH ON ROUTE THE ORIGIN AND LOCAL_ROUTE1_ID

										route_trace.steps = this.getSteps(
											route_to_evaluate,
											locals_ids[0],
											other_route_link.local_route1_id,
										);
										if (route_trace.steps.length > 0) {
											const last_time_step =
												route_trace.steps[route_trace.steps.length - 1].departure_time;
											route_trace.steps.push(route_link);
											route_trace.steps.push(
												...this.getSteps(
													other_route_to_evaluate,
													other_route_link.local_route2_id,
													locals_ids[1],
												),
												last_time_step,
											);
										}
									} else {
										// SEARCH ON ROUTE THE ORIGIN AND LOCAL_ROUTE2_ID
										route_trace.steps = this.getSteps(
											route_to_evaluate,
											locals_ids[0],
											other_route_link.local_route2_id,
										);
										if (route_trace.steps.length > 0) {
											const last_time_step =
												route_trace.steps[route_trace.steps.length - 1].departure_time;
											route_trace.steps.push(route_link);
											route_trace.steps.push(
												...this.getSteps(
													other_route_to_evaluate,
													other_route_link.local_route1_id,
													locals_ids[1],
													last_time_step,
												),
											);
										}
									}

									if (route_trace.steps.length > 0) {
										route_trace.departure_hour = route_trace.steps[0].departure_time;
										route_trace.arrival_hour =
											route_trace.steps[route_trace.steps.length - 1].departure_time;
										route_trace.prices = await this.calcPrices(
											ctx,
											route_trace.steps,
											route.zones,
											route.price_tables,
											ctx.meta.user,
										);

										// ROUTE INFO
										routeInfo.departure_hour = route_trace.steps[0].departure_time;
										routeInfo.arrival_hour =
											route_trace.steps[route_trace.steps.length - 1].departure_time;
										routeInfo.departure_local_id = route_trace.steps[0].local_id;
										routeInfo.departure_place = route_trace.steps[0].local;
										routeInfo.arrival_local_id =
											route_trace.steps[route_trace.steps.length - 1].local_id;
										routeInfo.arrival_place = route_trace.steps[route_trace.steps.length - 1].local;
										routeInfo.stops = route_trace.steps.length - 2;
										routeInfo.duration = this.calcDuration(
											routeInfo.departure_hour,
											routeInfo.arrival_hour,
										);

										routeInfo.route.push(route_trace);
										traces.push(routeInfo);
									}
								}
							}
						}
					}
				}

				//FIND REPEATED TRACES
				const routesIds = [];

				this.logger.info("###traces.length###");
				this.logger.info(traces.length);

				const tracesResult = traces.filter((r) => {
					this.logger.info("###r.represents_connection###");
					this.logger.info(r.represents_connection);

					const includeRoute = routesIds.find(
						(ri) =>
							ri.departure_hour === r.departure_hour &&
							ri.arrival_hour === r.arrival_hour &&
							ri.stops === r.stops,
					);

					if (r.represents_connection) {
						// EXIST NON SECUNDARY ROUTE?
						const onlySecundary = traces.find(
							(ri) =>
								ri.departure_hour === r.departure_hour &&
								ri.arrival_hour === r.arrival_hour &&
								ri.stops === r.stops &&
								r.represents_connection === false,
						);

						if (!includeRoute && onlySecundary) {
							routesIds.push(
								Object.assign(
									{},
									{
										departure_hour: r.departure_hour,
										arrival_hour: r.arrival_hour,
										stops: r.stops,
									},
								),
							);
							return true;
						} else {
							return false;
						}
					} else {
						if (!includeRoute) {
							routesIds.push(
								Object.assign(
									{},
									{
										departure_hour: r.departure_hour,
										arrival_hour: r.arrival_hour,
										stops: r.stops,
									},
								),
							);
							return true;
						} else {
							return false;
						}
					}
				});

				return { total: tracesResult.length, rows: tracesResult };

				/*



				this.logger.info("mode");
				this.logger.info(mode);
				this.logger.info("types_days");
				this.logger.info(types_days);
				this.logger.info("seasons");
				this.logger.info(seasons);
				this.logger.info("date");
				this.logger.info(date);

				this.logger.info("pre routes");
				this.logger.info(routes);

				routes = await this.filterRoutes(ctx, routes, ctx.params.origin, ctx.params.destination);

				this.logger.info("pos routes");
				this.logger.info(routes);

				let traces = this.trackingRoutes(
					routes,
					ctx.params.origin,
					ctx.params.destination,
					ctx.meta.user,
					date,
				);


				this.logger.info("final traces...");
				this.logger.info(traces);

				if (locals.length === 2) {
					if (locals[0].local && locals[1].local) {
						ctx.call("bus.search_logs.create", {
							origin: locals[0].local,
							destination: locals[1].local,
							date: date,
							result_count: traces.length,
							error: false,
						});

					}
				}

				for(let route of traces) {
					this.logger.info("for route");
					for(let step of route.route) {
						this.logger.info("for step");
						for(let st of step.steps) {
							this.logger.info("for st");
							if(st.local_id === ctx.params.origin) {
								step.departure_hour = st.departure_time;
							}
							if(st.local_id === ctx.params.destination) {
								step.arrival_hour = st.departure_time;
							}
						}
					}
				}

				this.logger.info("Final Response:");
				this.logger.info({ total: traces.length, rows: traces });

				return { total: traces.length, rows: traces };
				*/
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		getSteps(route, origin_id, destination_id, after_hour) {
			this.logger.info("getSteps ID:" + route.id);
			this.logger.info("getSteps route.timetables.lenght:" + route.timetables.length);

			const steps = [];
			// Tabelas da rota
			route.timetables.forEach((timetable) => {
				this.logger.info("getSteps.timetable.forEach");

				let origin_finded = false;
				let destination_finded = false;
				let hours_path = [];
				// horas da tabela
				timetable.hours.forEach((line) => {
					this.logger.info("getSteps.hours.forEach");

					let filtered_lines = [];
					if (after_hour) {
						filtered_lines = line.filter((l) => {
							return moment("2021-09-20 " + l.hour, "yyyy-MM-dd hh:mm:ss").isSameOrAfter(
								moment("2021-09-20 " + after_hour, "yyyy-MM-dd hh:mm:ss"),
							);
						});
					} else {
						filtered_lines = line;
					}

					//this.logger.info("getSteps.filtered_lines");
					//this.logger.info(filtered_lines);

					filtered_lines.forEach((hour, index) => {
						this.logger.info("getSteps.filtered_lines.forEach ->", hours_path.length);
						//this.logger.info(hour);

						if (hour.local.id === origin_id && !destination_finded) {
							origin_finded = true;
							//this.logger.info("getSteps.filtered_lines.originFinded");
							if (!after_hour) {
								this.logger.info("getSteps.filtered_lines.afterHour");
								hours_path = [];
							}
						}
						if (origin_finded && !destination_finded) {
							//this.logger.info("getSteps.filtered_lines.pushHour");
							hours_path.push(hour);
						}
						if (hour.local.id === destination_id && origin_finded && !destination_finded) {
							this.logger.info("getSteps.filtered_lines.destinationFinded");
							destination_finded = true;
						}
					});

					if (!destination_finded) {
						this.logger.info("getSteps DESSTINO NAO ENCONTRADO");
						hours_path = [];
					}
					this.logger.info("getSteps DESSTINO ENCONTRADO");
				});

				this.logger.info("getSteps.hours_path");
				this.logger.info(hours_path);

				for (let hour_path of hours_path) {
					const indexOf = hours_path.indexOf(hour_path);
					steps.push({
						local_id: hour_path.local.id,
						local: hour_path.local.local,
						region: hour_path.local.region,
						lat: hour_path.local.latitude,
						lng: hour_path.local.longitude,
						departure_time: hour_path.hour,
						duration: this.calcDuration(
							hour_path.hour,
							indexOf + 1 < hours_path.length ? hours_path[indexOf + 1].hour : null,
						),
						distance: route.locals.find((local) => local.id === hour_path.local_id).distance,
						instruction: route.locals.find((local) => local.id === hour_path.local_id).instruction,
						vehicle: "AUTOCARRO",
					});
				}
			});
			return steps;
		},

		/**
		 * Get the search date
		 * @param {object} ctx
		 */
		getDate(ctx) {
			let date;

			date =
				ctx.params.date === null || ctx.params.date === undefined
					? moment().format("YYYY-MM-DD")
					: ctx.params.date;
			return date;
		},
		/**
		 * Get the day type ids with the day of the week of the date
		 * @param {object} ctx
		 */
		async getTypesDays(ctx) {
			let day;
			let date;
			let types_days;

			date =
				ctx.params.date === null || ctx.params.date === undefined
					? moment().format("YYYY-MM-DD")
					: ctx.params.date;

			day = moment(date).day();

			types_days = await ctx.call("bus.days_group.find", {
				query: { day_id: day },
			});

			return types_days.map((d) => d["type_day_id"]);
		},

		/**
		 * Get search mode
		 * mode 0 -> Origem em texto e destino em númerico
		 * mode 1 -> Origem em númerico e destino em texto
		 * mode 2 -> Origem e destino em númerico
		 * mode 3 -> Origem e destino do tipo de texto
		 * @param {object} ctx
		 */
		async getModeForSearch(ctx) {
			if (isNaN(ctx.params.origin) && !isNaN(ctx.params.destination)) {
				await ctx.call("bus.locals.get", {
					id: ctx.params.destination,
				});
				return 0;
			} else if (!isNaN(ctx.params.origin) && isNaN(ctx.params.destination)) {
				await ctx.call("bus.locals.get", {
					id: ctx.params.origin,
				});
				return 1;
			} else if (!isNaN(ctx.params.origin) && !isNaN(ctx.params.destination)) {
				if (ctx.params.origin !== ctx.params.destination) {
					await ctx.call("bus.locals.get", {
						id: ctx.params.origin,
					});
					await ctx.call("bus.locals.get", {
						id: ctx.params.destination,
					});
					return 2;
				} else {
					throw new MoleculerError("Origin and destination are same.", 400);
				}
			} else {
				//mode 3
				throw new MoleculerError(
					"Invalid search the place of origin and destination cannot be both of the text type.",
					400,
				);
			}
		},
		/**
		 * Get locals by IDS
		 * @param {Array} ids
		 * @param {object} ctx
		 */
		async getLocals(ids, ctx) {
			return Promise.all(
				ids.map((id) => {
					return ctx
						.call("bus.locals.find", {
							query: { id: id },
						})
						.then((res) => {
							return res[0];
						});
				}),
			);
		},
		/**
		 * Get locals by IDS
		 * @param {Array} ids
		 * @param {object} ctx
		 */
		async getRoutesIDS(ids, ctx) {
			return Promise.all(
				ids.map((id) => {
					return ctx
						.call("bus.locals.find", {
							id: id,
						})
						.then((res) => {
							return res;
						});
				}),
			);
		},

		/**
		 * Get currents Seasons
		 * @param {object} ctx
		 */
		async getCurrentSeasonsIDS(ctx) {
			let date;

			if (ctx.params.date === null || ctx.params.date === undefined) {
				date = moment().format("YYYY-MM-DD");
			} else {
				date = moment(ctx.params.date).format("YYYY-MM-DD");
			}

			return ctx.call("bus.seasons.findCurrentsSeasonsIDS", { date: date });
		},

		/**
		 * Filters the routes containing the place of origin and destination
		 * @param {Array} routes
		 * @param {integer} origin
		 * @param {integer} destination
		 */
		filterRoutes(ctx, routes, origin, destination) {
			let originInclude = null;
			let destinationInclude = null;

			let shortRoutes = []; // Routes is contain origin and destination
			let destinationRoutes = []; // Routes is contain only the destination
			let originRoutes = []; // Routes is contain only the destination
			let routesLinked = [];
			let routesFound = [];

			// eslint-disable-next-line no-async-promise-executor
			return new Promise(async (resolve) => {
				routes.forEach((route) => {
					this.logger.info("[filterRoutes] foreach");
					if (route.locals && Array.isArray(route.locals)) {
						this.logger.info("[filterRoutes] if1");
						originInclude = route.locals.find((local) => local.id === origin);
						destinationInclude = route.locals.find((local) => local.id === destination);
						if (originInclude && destinationInclude) {
							this.logger.info("[filterRoutes] if2");
							shortRoutes.push([route]);
						} else if (destinationInclude) {
							this.logger.info("[filterRoutes] if3");
							destinationRoutes.push(route);
						} else if (originInclude) {
							this.logger.info("[filterRoutes] if4");
							originRoutes.push(route);
						}
					}
				});

				if (destinationRoutes.length !== 0) {
					this.logger.info("[filterRoutes] if5");
					this.logger.info("[filterRoutes] destinationRoutes");
					this.logger.info(destinationRoutes);

					routesLinked = await this.traceRoutesLinks(ctx, origin, destinationRoutes);
					this.logger.info("[filterRoutes] routesLinked");
					this.logger.info(routesLinked);
				}
				if (shortRoutes.length !== 0) {
					this.logger.info("[filterRoutes] if6");
					routesFound.push(...shortRoutes);
				}

				if (routesLinked.length !== 0) {
					this.logger.info("[filterRoutes] if7");
					routesFound = [...routesFound, ...routesLinked];
				}
				this.logger.info("[filterRoutes] routesFound");
				this.logger.info(routesFound);
				resolve(routesFound);
			});
		},

		/**
		 * Search for interconnected routes
		 * @param {object} ctx
		 * @param {integer} originLocal
		 * @param {array} destinationRoutes
		 */
		async traceRoutesLinks(ctx, originLocal, destinationRoutes) {
			let trees = [];
			let tree;
			return new Promise((resolve) => {
				destinationRoutes.forEach(async (route, index, array) => {
					this.logger.info("[traceRoutesLinks] foreach");
					tree = await this.createTree(ctx, originLocal, route);

					this.logger.info("[traceRoutesLinks] tree");
					this.logger.info(tree);

					trees.push(tree);
					if (array.length === index + 1) {
						resolve(this.getRoutesInTrees(trees));
					}
				});
			});
		},

		/**
		 * "Returns the route from the origin to the destination composed of a set of routes"
		 * @param {array<object>} tree
		 * @param {array<integer>} indexes
		 */
		findRouteInTreeBranches(tree, indexes) {
			this.logger.info("[findRouteInTreeBranches] tree");
			this.logger.info(tree);
			this.logger.info("[findRouteInTreeBranches] indexes");
			this.logger.info(indexes);

			let path = [];
			if (indexes && indexes.length !== 0) {
				let currentBranch = tree[0].branches;
				path.push(tree[0].route);
				indexes.shift();
				indexes.forEach((index) => {
					path.push(currentBranch[index].route);
					currentBranch = currentBranch[index].branches;
				});
			}
			return path.reverse();
		},

		/**
		 * "Returns a branch of the tree by indexes"
		 * @param {array<object>} tree
		 * @param {array<integer>} indexes
		 */
		findBranchInTree(tree, indexes) {
			let currentBranch;
			indexes.forEach((index) => {
				currentBranch = tree[index].branches;
			});
			return currentBranch;
		},

		/**
		 * "Save a branch on the tree"
		 * @param {array<object>} tree
		 * @param {array<integer>} indexes
		 * @param {integer} newIndex
		 * @param {object} value
		 */
		setBranchInTree(tree, indexes, newIndex, value) {
			tree.forEach((t) => {
				if (indexes.toString() === t.position.toString()) {
					tree[newIndex].branches = value;
				} else {
					this.setBranchInTree(t.branches, indexes, newIndex, value);
				}
			});
		},

		/**
		 * Get the routes interconnected with the destination
		 * @param {array<object>} trees
		 */
		async getRoutesInTrees(trees) {
			let routes = [];
			trees.forEach((tree) => {
				this.logger.info("[getRoutesInTrees] foreach");
				this.logger.info("[getRoutesInTrees] findRouteInTreeBranches");
				this.logger.info(
					this.findRouteInTreeBranches(
						tree.tree,
						tree.treePatches.length !== 0 ? _.first(tree.treePatches) : [],
					),
				);
				routes = [
					...routes,
					this.findRouteInTreeBranches(
						tree.tree,
						tree.treePatches.length !== 0 ? _.first(tree.treePatches) : [],
					),
				];
			});
			return routes;
		},

		/**
		 * "Creates a tree with interconnected routes"
		 * @param {object} ctx
		 * @param {integer} originLocal
		 * @param {object} route
		 *
		 */
		async createTree(ctx, originLocal, route) {
			let tree = [
				{
					type: "HEAD",
					position: [0],
					route: Object.assign(route, { origin: originLocal }),
					branches: [],
				},
			];

			this.logger.info("[createTree] route");
			this.logger.info(route);
			this.logger.info("[createTree] originLocal");
			this.logger.info(originLocal);
			let finalRouteLine = [];
			[tree[0].branches, finalRouteLine] = await this.findOriginRoute(
				ctx,
				originLocal,
				route,
				[0],
				finalRouteLine,
			);

			await this.createBranches(ctx, originLocal, tree, finalRouteLine).catch((error) => {
				throw new Errors.ValidationError(error, "CREATE_BRANCHES_FAILED", {});
			});
			return { tree: tree, treePatches: finalRouteLine };
		},

		/**
		 * "Creates the branches of the tree"
		 * @param {object} ctx
		 * @param {integer} originLocal
		 * @param {array<object>} tree
		 * @param {array<array<integer>>} finalRouteLine
		 */
		async createBranches(ctx, originLocal, tree, finalRouteLine) {
			let branches = this.findBranchInTree(tree, [0]);
			let branch;
			return new Promise((resolve, reject) => {
				try {
					branches.forEach(async (b, index, array) => {
						if (b.route !== null) {
							[branch, finalRouteLine] = await this.findOriginRoute(
								ctx,
								originLocal,
								b.route,
								b.position,
								finalRouteLine,
							);
							this.setBranchInTree(tree, b.position, index, branch);
						}
						if (array.length === index + 1) {
							resolve(true);
						}
					});
				} catch (e) {
					reject(e);
				}
			});
		},

		/**
		 * "Checks on each link whether or not the place of origin exists and keeps it in the branch"
		 * @param {object} ctx
		 * @param {integer} originLocal
		 * @param {object} route
		 * @param {array<integer>} position
		 * @param {array<array<integer>>} finalRouteLine
		 */
		async findOriginRoute(ctx, originLocal, route, position, finalRouteLine) {
			let routeLink;
			let branches = new Array();
			return new Promise((resolve, reject) => {
				this.logger.info("[findOriginRoute] start...");
				if (route.links) {
					this.logger.info("[findOriginRoute] links ?...");
					if (route.links.length !== 0) {
						this.logger.info("[findOriginRoute] links ??...");
						route.links.forEach(async (link, route_index, array) => {
							this.logger.info("[findOriginRoute] foreach");

							routeLink = await this.linkIsContainOrigin(ctx, link, originLocal);
							branches.push({
								type: routeLink.contain ? "FINAL" : "INTERMEDIATE",
								position: [...position, route_index],
								route: Object.assign(routeLink.route, { origin: link.local_route1_id }),
								branches: [],
							});
							if (routeLink.contain) {
								finalRouteLine.push([...position, route_index]);
							}
							if (array.length === route_index + 1) {
								resolve([branches, finalRouteLine]);
							}
						});
					} else {
						branches.push({ type: "END_OF_THE_LINE", route: null, position: 0, branches: [] });
						resolve(branches);
					}
				} else {
					reject(null);
				}
			});
		},

		/**
		 * "Checks whether the place of origin exists on that link"
		 * @param {object} ctx
		 * @param {object} link
		 * @param {integer} originLocal
		 */
		async linkIsContainOrigin(ctx, link, originLocal) {
			let contain = await ctx.call("bus.route_order.isLocalContainInRoute", {
				local_id: originLocal,
				route_id: link.route1_id,
			});
			let nextRoute = await ctx.call("bus.routes.find", {
				query: { id: link.route1_id },
				withRelated: "locals,timetables,linksIn,contact,zones,price_tables",
			});
			return { contain: contain, route: nextRoute[0] };
		},

		/**
		 * Calculates the duration between two locations
		 * @param {string} hour1
		 * @param {string} hour2
		 */
		calcDuration(hour1, hour2) {
			if (hour2 !== undefined) {
				let startTime = moment(hour1, "HH:mm:ss");
				let endTime = moment(hour2, "HH:mm:ss");
				let duration = moment.duration(endTime.diff(startTime));
				return parseInt(duration.asMinutes());
			} else {
				return 0;
			}
		},

		/**
		 * Calculates the price of the route
		 * @param {Array} trace
		 * @param {Array} locals_zones
		 * @param {Array} prices
		 * @param {object} user
		 */
		async calcPrices(ctx, trace, locals_zones, prices, user) {
			this.logger.info("calcPrices");

			this.logger.info("calcPrices.trace");
			this.logger.info(trace);

			let current_zone = 0;
			let zone_id;
			let nr_change_zones = 0;
			let ticketValues;
			let passValues;
			let pricesValues = [];

			let zone_id_local;
			let array_zones = [];

			this.logger.info("calcPrices.locals_zones");
			locals_zones.forEach((zone) => {
				zone_id_local = zone.id;
				this.logger.info("calcPrices.locals_zones.locals");
				zone.locals.forEach((local) => {
					local.zone_id = zone_id_local;
					array_zones.push(local);
				});
			});

			this.logger.info("calcPrices.locals_zones");
			this.logger.info(locals_zones);

			if (trace.length !== 0 && locals_zones.length !== 0) {
				this.logger.info("calcPrices.trace.forEach");

				trace.forEach((t) => {
					const array_find = array_zones.find((local_zone) => local_zone.id === t.local_id);
					this.logger.info("calcPrices.trace.array_find");
					this.logger.info(array_find);

					if (!current_zone) {
						current_zone = array_find.zone_id;
						nr_change_zones++;
					} else {
						this.logger.info(
							"##### current_zone !== array_find.zone_idcurrent_zone !== array_find.zone_id ####",
							current_zone !== array_find.zone_id,
						);
						if (current_zone !== array_find.zone_id) {
							current_zone = array_find.zone_id;
							nr_change_zones++;
						}
					}

					this.logger.info("##### CURRENT ZONE ####");
					this.logger.info("##### CURRENT ZONE ####");
					this.logger.info(current_zone);

					/*zone_id = null;

					this.logger.info("calcPrices.trace.forEach");
					if (array_find != undefined) {
						array_zones.find((local_zone) => local_zone.id === t.local_id);
					}
					if (current_zone === 0) {
						current_zone = zone_id;
					} else {
						if (current_zone !== zone_id) {
							nr_change_zones++;
							current_zone = zone_id;
						}
					}*/
				});

				this.logger.info("calcPrices.nr_change_zones");
				this.logger.info(nr_change_zones);

				ticketValues = prices.find(
					(price) => price.profile_id === user.profile_id && price.ticket_type === "TICKET",
				);
				passValues = prices.find(
					(price) => price.profile_id === user.profile_id && price.ticket_type === "PASS",
				);

				this.logger.info("calcPrices.prices");
				this.logger.info(prices);
				this.logger.info("calcPrices.user_meta");
				this.logger.info(user.profile_id);

				this.logger.info("calcPrices.ticketValues");
				this.logger.info(ticketValues);

				this.logger.info("calcPrices.passValues");
				this.logger.info(passValues);

				let ticketValue = ticketValues
					? ticketValues.prices.find((p) => p.number_stop === nr_change_zones - 1)
					: undefined;

				this.logger.info("######################");
				this.logger.info("######################");
				this.logger.info("######################");
				this.logger.info("calcPrices.ticketValue");
				this.logger.info(ticketValue);

				if (ticketValue) {
					const ticketHash = await this.getPriceHash(
						ctx,
						ticketValue.id,
						trace.length > 0 ? trace[0].local_id : null,
						trace.length > 0 ? trace[trace.length - 1].local_id : null,
					);
					pricesValues.push({
						ticket_type: "TICKET",
						hash: ticketHash,
						value: ticketValue !== undefined ? ticketValue.value : null,
					});
				}

				let passValue = passValues
					? passValues.prices.find((p) => p.number_stop === nr_change_zones - 1)
					: undefined;
				if (passValue) {
					const passHash = await this.getPriceHash(
						ctx,
						passValue.id,
						trace.length > 0 ? trace[0].local_id : null,
						trace.length > 0 ? trace[trace.length - 1].local_id : null,
					);
					pricesValues.push({
						ticket_type: "PASS",
						hash: passHash,
						value: passValue !== undefined ? passValue.value : null,
					});
				}

				this.logger.info("calcPrices.pricesValues");
				this.logger.info(pricesValues);

				return pricesValues;
			} else {
				return [];
			}
		},

		getPriceHash(ctx, price_id, departure_local_id, arrival_local_id) {
			return ctx
				.call("bus.tickets_hashs.getHash", {
					price_id,
					departure_local_id,
					arrival_local_id,
				})
				.then((r) => {
					return r.hash;
				});
		},

		/**
		 * Draws a route from the place of departure to the destination
		 * @param {Array} route
		 * @param {integer} origin
		 * @param {integer} destination
		 */

		// TODO : WORKING WHERE
		async traceRoute(ctx, route, origin, destination, user, last_hour) {
			let traces = [];
			let activeTrace = false;
			let last_destination;
			let ticket_price_value = 0;

			let route_trace = {
				route_id: route.id,
				route_line: route.name,
				external: route.external,
				active: route.active,
				steps: [],
				prices: [],
				contacts: route.contact,
			};

			// Tabelas da rota
			for (let timetable of route.timetables) {
				// horas da tabela
				for (let hour of timetable.hours) {
					try {
						// Linhas da tabela de horas

						let finalResult = false;
						hour.forEach((line, index) => {
							if (line.local.id === origin) {
								activeTrace = true;
								route_trace.steps = [];
								this.logger.info("activeTrace = true");
							}

							this.logger.info("activeTrace");
							if (activeTrace && !finalResult) {
								route_trace.steps.push({
									local_id: line.local.id,
									local: line.local.local,
									region: line.local.region,
									lat: line.local.latitude,
									lng: line.local.longitude,
									departure_time: line.hour,
									duration: this.calcDuration(
										line.hour,
										hour[index + 1] ? hour[index + 1].hour : null,
									),
									distance: route.locals.find((local) => local.id === line.local_id).distance,
									instruction: route.locals.find((local) => local.id === line.local_id).instruction,
									vehicle: "AUTOCARRO",
								});
							}

							if (line.local_id === destination && activeTrace) {
								last_hour = line.hour;
								last_destination = destination;
								activeTrace = false;
								finalResult = true;
								this.logger.info("activeTrace = false");
							}
						});

						route_trace.prices = await this.calcPrices(
							ctx,
							route_trace.steps,
							route.zones,
							route.price_tables,
							user,
						);

						ticket_price_value =
							route_trace.prices.length !== 0
								? route_trace.prices.find((price) => price.ticket_type === "TICKET").value
								: 0;

						traces.push(route_trace);

						route_trace = {
							route_id: route.id,
							route_line: route.name,
							external: route.external,
							active: route.active,
							steps: [],
						};
					} catch (error) {
						throw new Errors.ValidationError(error, "TRACE_ROUTE_ERROR", {});
					}
				}
			}
			return {
				traces: traces,
				last_hour: last_hour,
				last_destination: last_destination,
				ticket_price_value: ticket_price_value,
			};
		},
		/**
		 * Checks if the array contains empty arrays
		 * @param {array<object>} routeTrace
		 */
		filterIncompleteRoutes(routeTrace) {
			let complete = true;
			routeTrace.forEach((route) => {
				if (route.length === 0) {
					complete = false;
				}
			});
			return complete;
		},
		/**
		 * Traces several routes from the place of departure to the destination
		 * @param {Array} routes
		 * @param {integer} origin
		 * @param {integer} destination
		 */
		async traceRoutes(ctx, route, origin, destination, user, date) {
			let tracesFound = [];
			let originApplied;
			let destinationApplied;
			let last_destination;
			let last_hour = null;
			let tickets_prices_total = 0;

			let routeInfo = {
				stops: 0,
				departure_hour: null,
				departure_local_id: null,
				departure_place: null,
				arrival_hour: null,
				arrival_local_id: null,
				arrival_place: null,
				date: null,
				duration: 0,
				tickets_prices_total: 0,
			};

			let route_link = {
				instruction: "Troca de Transporte",
				vehicle: "CAMINHADA",
			};

			route.forEach(async (r, index, array) => {
				this.logger.info("[traceRoutes] forEach...");
				~this.logger.info("[traceRoutes] r...");
				this.logger.info(r);
				this.logger.info("[traceRoutes] index...");
				this.logger.info(index);
				this.logger.info("[traceRoutes] array...");
				this.logger.info(array);
				try {
					let routeTraced;
					if (index === 0 && array.length > 1) {
						this.logger.info("[traceRoutes] index === 0 && array.length > 1");
						originApplied = origin;
						destinationApplied = r.origin;
					} else if (index + 1 === array.length && array.length > 1) {
						this.logger.info(
							"[traceRoutes] lse if (index + 1 === array.length && array.length > 1",
						);
						routeInfo.stops++;
						tracesFound.push([{ steps: route_link }]);

						originApplied = last_destination;
						destinationApplied = destination;
					} else if (array.length > 1) {
						this.logger.info("[traceRoutes]  else if (array.length > 1)");
						originApplied = route[index - 1].origin;
						destinationApplied = r.origin;

						routeInfo.stops++;
						tracesFound.push([{ steps: route_link }]);
					} else {
						this.logger.info("[traceRoutes] else");
						originApplied = origin;
						destinationApplied = destination;
					}

					routeTraced = await this.traceRoute(
						ctx,
						r,
						originApplied,
						destinationApplied,
						user,
						last_hour,
					);
					this.logger.info("[traceRoutes] routeTraced...");
					this.logger.info(routeTraced);

					tracesFound.push(routeTraced.traces);
					last_hour = routeTraced.last_hour;
					last_destination = routeTraced.last_destination;
					tickets_prices_total += parseFloat(routeTraced.ticket_price_value);

					if (index === 0) {
						if (routeTraced.traces.length !== 0) {
							if (routeTraced.traces[0].steps.length !== 0) {
								if (
									routeTraced.traces[0].steps[0].departure_time &&
									routeTraced.traces[0].steps[0].local
								) {
									this.logger.info("IF CHATO 1");

									routeInfo.departure_hour = routeTraced.traces[0].steps[0].departure_time;
									routeInfo.arrival_hour =
										routeTraced.traces[0].steps[
											routeTraced.traces[0].steps.length - 1
										].departure_time;

									routeInfo.departure_local_id = routeTraced.traces[0].steps[0].local_id;
									routeInfo.departure_place = routeTraced.traces[0].steps[0].local;

									this.logger.info("ARRIVAL LOCAL ID");
									this.logger.info(routeTraced.traces[0].steps);
									routeInfo.arrival_local_id =
										routeTraced.traces[0].steps[routeTraced.traces[0].steps.length - 1].local_id;
									routeInfo.arrival_place =
										routeTraced.traces[0].steps[routeTraced.traces[0].steps.length - 1].local;
								}
							}
							routeInfo.stops = routeTraced.traces[0].steps.length;
						}
					} else {
						if (routeTraced.traces.length !== 0) {
							if (routeTraced.traces[0].steps.length !== 0) {
								if (
									routeTraced.traces[0].steps[0].departure_time &&
									routeTraced.traces[0].steps[0].local
								) {
									routeInfo.arrival_hour =
										routeTraced.traces[0].steps[
											routeTraced.traces[0].steps.length - 1
										].departure_time;

									routeInfo.arrival_local_id =
										routeTraced.traces[0].steps[routeTraced.traces[0].steps.length - 1].local_id;
									routeInfo.arrival_place =
										routeTraced.traces[0].steps[routeTraced.traces[0].steps.length - 1].local;
								}
								routeInfo.stops = routeTraced.traces[0].steps.length;
							}
						}
					}
				} catch (error) {
					throw new Errors.ValidationError(error, "TRACES_ROUTES_ERROR", {});
				}
			});

			routeInfo.date = date;
			routeInfo.tickets_prices_total = tickets_prices_total;
			routeInfo.duration = this.calcDuration(routeInfo.departure_hour, routeInfo.arrival_hour);

			this.logger.info("RESPONSE");
			this.logger.info({ tracesFound: tracesFound, routeInfo: routeInfo });

			return { tracesFound: tracesFound, routeInfo: routeInfo };
		},

		/**
		 * Generation of routes
		 * @param {array<array<object>>} routes
		 * @param {integer} origin
		 * @param {integer} destination
		 * @param {object} user
		 */
		async trackingRoutes(ctx, routes, origin, destination, user, date) {
			let routesTracking = [];
			let routeTraced;
			for (let route of routes) {
				this.logger.info("Tracking route...");
				routeTraced = await this.traceRoutes(ctx, route, origin, destination, user, date);
				this.logger.info("Route traced");
				this.logger.info(routeTraced);

				if (this.filterIncompleteRoutes(routeTraced.tracesFound)) {
					if (
						routeTraced.routeInfo.departure_local_id === origin &&
						routeTraced.routeInfo.arrival_local_id == destination
					) {
						routesTracking.push(
							Object.assign({ route: _.flatten(routeTraced.tracesFound) }, routeTraced.routeInfo),
						);
					}
				}
			}
			return routesTracking;
		},
		/**
		 * Clear cached entities
		 *
		 * @methods
		 * @returns {Promise}
		 */
		clearCache() {
			this.broker.broadcast("cache.clean.bus.route_search");
			if (this.broker.cacher) return this.broker.cacher.clean("bus.route_search.*");
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.clearCache();
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
