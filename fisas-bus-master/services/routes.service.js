"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { checkIdsIsNotExist } = require("../helpers/checkers");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.routes",
	table: "routes",
	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "routes")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"contact_id",
			"external",
			"represents_connection",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			locals(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.route_order.findLocalOfRoute", { route_id: doc.id })
							.then((res) => {
								doc.locals = res;
							});
					}),
				);
			},
			timetables(ids, docs, rule, ctx) {
				if (ctx.meta.withRelateds) {
					if (ctx.meta.withRelateds.timetables) {
						if (
							ctx.meta.withRelateds.timetables.type_day_id &&
							ctx.meta.withRelateds.timetables.season_id
						) {
							return Promise.all(
								docs.map((doc) => {
									return ctx
										.call("bus.timetables.find", {
											query: {
												route_id: doc.id,
												type_day_id: ctx.meta.withRelateds.timetables.type_day_id,
												season_id: ctx.meta.withRelateds.timetables.season_id,
											},
										})
										.then((res) => {
											doc.timetables = res;
										});
								}),
							);
						}
					}
				}

				return Promise.all(
					docs.map((doc) => {
						return ctx.call("bus.timetables.find", { query: { route_id: doc.id } }).then((res) => {
							doc.timetables = res;
						});
					}),
				);
			},
			price_tables(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.price_tables", "price_tables", "id", "route_id");
			},
			zones(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.zones", "zones", "id", "route_id");
			},
			tickets_bought(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.tickets_bought", "tickets_bought", "id", "route_id");
			},
			links(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx.call("bus.links.findLinksOfRoute", { route_id: doc.id }).then((res) => {
							doc.links = res;
						});
					}),
				);
			},
			linksOut(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.links", "links", "id", "route1_id");
			},
			linksIn(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.links", "links", "id", "route2_id");
			},
			contact(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.contacts", "contact", "contact_id");
			},
			ticket_config(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx.call("bus.ticket_config.find", { route_id: doc.id }).then((res) => {
							doc.ticket_config = res[0];
						});
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			contact_id: { type: "number", positive: true },
			represents_connection: { type: "boolean" },
			external: { type: "boolean" },
			active: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			create: [
				async function validateDaysWeekIDS(ctx) {
					let ids = await checkIdsIsNotExist(
						ctx,
						ctx.params.locals.map((local) => local["id"]),
						"bus",
						"locals",
					);
					if (ids.length !== 0) {
						throw new Errors.ValidationError(
							`There are no locals with these ids: ${ids.toString()}`,
							"LOCALS_IDS_NOT_FOUND",
							{
								locals_ids: ids,
							},
						);
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				"deleteRelatedRouteOrder",
				"deleteRelatedRouteDistances",
				"deleteRelatedTickerConfig",
				"deleteRelatedRouteLinks",
				"deleteRelatedTimetables",
				"deleteRelatedPriceTables",
				"deleteRelatedZones",
			],
		},
		after: {
			create: ["saveLocals", "saveDistances", "createTicketConfigForRoute"],
			update: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#language_id",
					"#withRelateds",
				],
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"bus.timetables.*"() {
			this.clearCache();
		},
		"bus.hours.*"() {
			this.clearCache();
		},
		"bus.zones.*"() {
			this.clearCache();
		},
		"bus.prices.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async saveLocals(ctx, res) {
			if (ctx.params.locals && Array.isArray(ctx.params.locals)) {
				await ctx.call("bus.route_order.save_locals", {
					route_id: res[0].id,
					locals: ctx.params.locals,
				});
				res[0].locals = await ctx.call("bus.route_order.find", {
					query: {
						route_id: res[0].id,
					},
				});
			}
			return res;
		},
		async saveDistances(ctx, res) {
			if (ctx.params.locals && Array.isArray(ctx.params.locals)) {
				let distances = [];

				ctx.params.locals.forEach((local, index) => {
					if (index !== ctx.params.locals.length - 1) {
						distances.push({
							local1_id: local.id,
							local2_id: ctx.params.locals[index + 1].id,
							route_id: res[0].id,
							distance: local.distance ? local.distance : 0,
							created_at: new Date(),
							updated_at: new Date(),
						});
					}
				});

				await ctx.call("bus.distances.save_distances", {
					distances: distances,
				});
			}
			return res;
		},
		async createTicketConfigForRoute(ctx, res) {
			await ctx.call("bus.ticket_config.create", {
				route_id: res[0].id,
				max_to_buy: 0,
				active: false,
			});
			return (res[0].ticket_config = await ctx.call("bus.ticket_config.find", {
				query: {
					route_id: res[0].id,
				},
			}));
		},
		async deleteRelatedRouteOrder(ctx, res) {
			typeof res;
			return ctx
				.call("bus.route_order.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (routeOderRel) =>
							await ctx.call("bus.route_order.remove", { id: routeOderRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Route Order relation!", err));
		},
		async deleteRelatedRouteDistances(ctx, res) {
			typeof res;
			return ctx
				.call("bus.distances.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (distanceRel) => await ctx.call("bus.distances.remove", { id: distanceRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Distance relation!", err));
		},
		async deleteRelatedRouteLinks(ctx, res) {
			typeof res;
			return ctx
				.call("bus.links.findLinksOfRoute", { route_id: ctx.params.id })
				.then((res) => {
					res.map(async (linksRel) => await ctx.call("bus.links.remove", { id: linksRel.id }));
				})
				.catch((err) => this.logger.error("Unable to delete Links relation!", err));
		},
		async deleteRelatedTickerConfig(ctx, res) {
			typeof res;
			return ctx
				.call("bus.ticket_config.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (ticketConfigRel) =>
							await ctx.call("bus.ticket_config.remove", { id: ticketConfigRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Ticket Config items!", err));
		},
		async deleteRelatedTimetables(ctx, res) {
			typeof res;
			return ctx
				.call("bus.timetables.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (timetableRel) =>
							await ctx.call("bus.timetables.remove", { id: timetableRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Timetable items!", err));
		},
		async deleteRelatedPriceTables(ctx, res) {
			typeof res;
			return ctx
				.call("bus.price_tables.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (priceTableRel) =>
							await ctx.call("bus.price_tables.remove", { id: priceTableRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Price Tables items!", err));
		},
		async deleteRelatedZones(ctx, res) {
			typeof res;
			return ctx
				.call("bus.zones.find", {
					query: {
						route_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(async (zoneRel) => await ctx.call("bus.zones.remove", { id: zoneRel.id }));
				})
				.catch((err) => this.logger.error("Unable to delete Zone items!", err));
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
