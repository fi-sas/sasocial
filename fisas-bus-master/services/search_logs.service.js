"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.search_logs",
	table: "search_logs",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "search_logs")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"origin",
			"destination",
			"date",
			"result_count",
			"error",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			origin: { type: "string" },
			destination: { type: "string" },
			date: { type: "string" },
			result_count: { type: "number" },
			error: { type: "boolean" },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
