"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.seasons",
	table: "seasons",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "seasons")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"date_begin",
			"date_end",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			date_begin: { type: "string" },
			date_end: { type: "string" },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findCurrentsSeasonsIDS: {
			params: {
				date: { type: "string" }
			},
			async handler(ctx) {
				const seasons = await this.findCurrentsSeasons(ctx);
				return seasons.map(d => d["id"]);
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		findCurrentsSeasons(ctx) {
			return this.adapter.find({
				query: (qb) => {
					qb.where("date_begin", "<=", ctx.params.date);
					qb.andWhere("date_end", ">=", ctx.params.date);
					return qb;
				}
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
