"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { sub23DeclarationsStatus } = require("../values/enums");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const DeclarationStateMachine = require("../state-machine/sub23_declaration.machine");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");

/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.sub23_declarations",
	table: "sub23_declarations",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "sub23_declarations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"academic_year",
			"name",
			"address",
			"locality",
			"postal_code",
			"country",
			"document_type_id",
			"identification",
			"birth_date",
			"organic_unit_id",
			"course_id",
			"course_year",
			"has_social_scholarship",
			"applied_social_scholarship",
			"status",
			"user_id",
			"declaration_id",
			"observations",
			"reject_reason",
			"cc_emitted_in",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"course",
			"organic_unit",
			"document_type",
			"declaration",
			"history"
		],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			organic_unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organic_unit", "organic_unit_id");
			},
			document_type(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.document-types", "document_type", "document_type_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.sub23_declarations_history", "history", "id", "sub23_declarations_id");
			},
			declaration(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "declaration", "declaration_id");
			},
		},
		entityValidator: {
			academic_year: { type: "string" },
			name: { type: "string" },
			address: { type: "string" },
			locality: { type: "string" },
			postal_code: { type: "string" },
			country: { type: "string" },
			identification: { type: "string" },
			document_type_id: { type: "number", convert: true },
			birth_date: { type: "date", convert: true },
			organic_unit_id: { type: "number", positive: true },
			course_id: { type: "number", positive: true },
			course_year: { type: "number", positive: true },
			has_social_scholarship: { type: "boolean" },
			applied_social_scholarship: { type: "boolean" },
			status: { type: "enum", values: sub23DeclarationsStatus },
			user_id: { type: "number", positive: true },
			declaration_id: { type: "number", positive: true, optional: true },
			observations: { type: "string", optional: true },
			reject_reason: { type: "string", optional: true, nullable: true },
			cc_emitted_in: { type: "string", optional: true, nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				async function validateUser(ctx) {
					ctx.params.user_id = ctx.meta.user.id;
					if (ctx.params.academic_year && ctx.params.user_id) {
						const open_apps = await ctx.call("bus.sub23_declarations.find", { query: { user_id: ctx.params.user_id, academic_year: ctx.params.academic_year, status: ["submitted", "approved"] } });
						if (open_apps.length > 0) {
							throw new ValidationError(
								"Already have one opened sub23 declaration",
								"BUS_SUB23_DECLARATION_ALREDY_OPEN",
								{},
							);
						}
					}
				},
				async function validateOrganicUnit(ctx) {
					if (ctx.params.organic_unit_id) {
						await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
					}
				},
				async function validateCourse(ctx) {
					if (ctx.params.course_id) {
						await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
					}
				},
				async function validateDocumentType(ctx) {
					if (ctx.params.document_type_id) {
						await ctx.call("authorization.document-types.get", { id: ctx.params.document_type_id });
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "validated";
					if (ctx.params.has_social_scholarship || ctx.params.applied_social_scholarship) {
						ctx.params.status = "in_validation";
					}
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function checkRequestOrigin(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.user_id = ctx.meta.user.id;
					}
				},
			]
		},
		after: {
			create: [
				function addHistoryChange(ctx, res) {
					ctx.call("bus.sub23_declarations_history.create", {
						user_id: ctx.meta.user.id,
						sub23_declarations_id: res[0].id,
						status: res[0].status,
					});
					return res;
				},
				function sendNotification(ctx, res) {
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "MOBILITY_SUB23_DECLARATION_SUBMITTED",
						user_id: res[0].user_id,
						user_data: {},
						data: {
							declaration: res[0],
						},
						variables: {},
						external_uuid: null,
					});
					return res;
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "bus:sub23_declarations:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: { type: "enum", values: ["emit", "reject", "close", "cancel", "validate"] },
			},
			async handler(ctx) {
				const declaration = await ctx.call("bus.sub23_declarations.get", ctx.params);
				if (ctx.params.event == "emit") {
					if (!ctx.params.declaration_id) {
						throw new ValidationError(
							"'declaration_id' is required!",
							"DELARATION_ID_FROM_REQUIRED",
							{},
						);
					}
					ctx.params.declaration = ctx.params.declaration ? ctx.params.declaration : {};
					ctx.params.declaration.declaration_id = ctx.params.declaration_id;
				}
				if(ctx.params.event == "reject"){
					ctx.params.declaration = ctx.params.declaration ? ctx.params.declaration : {};
					ctx.params.declaration.reject_reason = ctx.params.reject_reason;
				}
				const stateMachine = await DeclarationStateMachine.createStateMachine(
					declaration[0].status,
					ctx,
				);
				if (stateMachine.can(ctx.params.event.toUpperCase())) {
					const result = await stateMachine[ctx.params.event.toLowerCase()](declaration[0].user_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_DECLARATION_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"BUS_DECLARATION_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		printDeclaration: {
			rest: "POST /:id/declaration",
			visibility: "published",
			scope: "bus:sub23_declarations:status",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const declaration = await ctx.call("bus.sub23_declarations.get", ctx.params);
				declaration[0].file_emitted_date = moment().format("YYYY-MM-DD");
				return ctx.call("reports.templates.print", {
					key: "MOBILITY_SUB_23_DECLARATION",
					options: {
						convertTo: "pdf"
					},

					data: {
						...declaration[0]
					},
				});

			}
		},
		print_list_declarations: {
			rest: "GET /report-list",
			params: {
				academic_year : { type: "string" }
			},
			visibility: "published",
			scope: "bus:sub23_declarations:read",
			async handler(ctx) {
				const declarations = await this._find(ctx, { query: {
					academic_year : ctx.params.academic_year
				} });
				return ctx.call("reports.templates.print", {
					key: "MOBILITY_SUB_23_DECLARATION_LIST",
					data: { declaration : declarations },
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
