"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { sub23DeclarationsStatus } = require("../values/enums");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.sub23_declarations_history",
	table: "sub23_declarations_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "sub23_declarations_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"sub23_declarations_id",
			"user_id",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true },
			sub23_declarations_id: { type: "number", positive: true },
			status: { type: "enum", values: sub23DeclarationsStatus },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
