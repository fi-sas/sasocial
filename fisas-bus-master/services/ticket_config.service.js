"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.ticket_config",
	table: "ticket_config",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "ticket_config")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"route_id",
			"max_to_buy",
			"active",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			route_id: { type: "number", positive: true },
			max_to_buy: { type: "number" },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
