"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.tickets_bought",
	table: "tickets_bought",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "tickets_bought")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"price_id",
			"route_id",
			"departure_local_id",
			"arrival_local_id",
			"user_id",
			"movement_id",
			"has_canceled",
			"validate_date",
			"used",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["departure_local", "arrival_local", "user", "price", "route"],
		withRelateds: {
			price(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx.call("bus.prices.find", { query: { id: doc.price_id } }).then((res) => {
							doc.price = res[0];
						});
					}),
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id",
					"id",
					{},
					null,
					"profile",
				);
			},
			route(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.routes", "route", "route_id", "id", {}, null, "bus");
			},
			departure_local(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "departure_local", "departure_local_id");
			},
			arrival_local(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.locals", "arrival_local", "arrival_local_id");
			},
		},
		entityValidator: {
			price_id: { type: "number", positive: true },
			route_id: { type: "number", positive: true },
			departure_local_id: { type: "number", positive: true },
			arrival_local_id: { type: "number", positive: true },
			validate_date: { type: "date", optional: true },
			movement_id: { type: "string", nullable: true },
			user_id: { type: "number", positive: true },
			used: { type: "boolean" },
			has_canceled: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			list: [
				function checkQuery(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.user_id = ctx.meta.user.id;
					}
				},
			],
			create: [
				"validateTicketPurchase",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			list: [
				function addHash(ctx, res) {
					res.rows.map((r) => (r.hash = this.getHash(r)));
					return res;
				},
			],
			get: [
				function checkIfIsUser(ctx, res) {
					if (!ctx.meta.isBackoffice && res[0].user_id != ctx.meta.user.id) {
						throw new Errors.ForbiddenError(
							"This ticket doesnt belong to the user logged",
							"BUS_TICKET_FORBIDDEN",
						);
					}
					return res;
				},
				function addHash(ctx, res) {
					res.map((r) => (r.hash = this.getHash(r)));
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			cache: {
				keys: ["#user.id"],
			},
			async handler(ctx) {
				const list = await this._find(ctx, {
					query: (qb) => qb.whereRaw("	user_id = " + ctx.meta.user.id + " and used=false"),
				});
				if (list.length > 0) {
					return list;
				}
				throw new Errors.ValidationError(
					"User dont have any tickets",
					"BUS_USER_DONT_HAVE_TICKETS",
					{},
				);
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		create_order: {
			visibility: "public",
			async handler(ctx) {
				this.logger.info("tickets_bought.create_order");
				this.logger.info(ctx.params);
				const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "BUS_TICKET");

				if (filtered_items.length > 0) {
					const tickets = filtered_items.map((item) => ({
						price_id: item.extra_info.price_id,
						route_id: item.extra_info.route_id,
						departure_local_id: item.extra_info.departure_local_id,
						arrival_local_id: item.extra_info.arrival_local_id,
						validate_date: null,
						movement_id: item.movement_id,
						user_id: ctx.params.user_id,
						used: false,
						has_canceled: false,
						updated_at: new Date(),
						created_at: new Date(),
					}));

					let params = this.sanitizeParams(ctx, { entities: tickets });
					this._insert(ctx, params)
						.then(() => true)
						.catch((err) => {
							this.logger.error("TICKETS BOUGHT CONFIRM ERROR: ");
							this.logger.error(err);
							return false;
						});
				}

				return false;
			},
		},

		cancel_order: {
			visibility: "public",
			async handler(ctx) {
				/*const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "BAR");
				const updated_orders = [];
				for (const item of filtered_items) {
					const orders = await this._find(ctx, {
						query: {
							movement_id: item.movement_id,
							status: "NOT_SERVED",
						},
					});
					if (orders) {
						if (orders.length === 0) {
							return false;
						}
						if (orders[0].status === "SERVED") {
							throw new Errors.ValidationError("The order has served", "FOOD_ORDER_HAS_SERVED", {});
						} else {
							orders[0].status = "CANCELED";
							for (const order_line of orders[0].order_lines) {
								await ctx.call("alimentation.orders-lines.in_stock", {
									order_id: order_line.order_id,
									order_line_id: order_line.id,
								});
							}
							await this._update(ctx, orders[0])
								.then((order_update) => {
									updated_orders.push(order_update);
								})
								.catch(() => {
									return false;
								});
						}
					}
				}
				if (updated_orders.length === filtered_items.length) {
					return true;
				} else {
					return false;
				}*/
			},
		},
		validate_ticket: {
			visibility: "published",
			rest: "POST /validate",
			params: {
				hash: "string",
			},
			async handler(ctx) {
				const result = await this.decypherHash(ctx.params.hash);

				if (result.id) {
					return this._get(ctx, {
						id: result.id,
					})
						.then((ticket) => {
							if (ticket[0].used === false) {
								return this._update(
									ctx,
									{
										id: result.id,
										used: true,
										validate_date: new Date(),
									},
									true,
								)
									.then((used_ticket) => {
										return {
											valid: true,
											exception_code: null,
											id: used_ticket[0].id,
											name: ticket[0].user.name,
											route: ticket[0].route.name,
											created_at: ticket[0].created_at,
											departure_local: ticket[0].departure_local.local,
											arrival_local: ticket[0].arrival_local.local,
											validate_date: ticket[0].validate_date,
										};
									})
									.catch(() => {
										return {
											valid: false,
											exception_code: "UNABLE_TO_USE",
											id: result.id,
											name: ticket[0].user.name,
											route: ticket[0].route.name,
											created_at: ticket[0].created_at,
											departure_local: ticket[0].departure_local.local,
											arrival_local: ticket[0].arrival_local.local,
											validate_date: ticket[0].validate_date,
										};
									});
							} else {
								return {
									valid: false,
									exception_code: "ALREADY_USED",
									id: result.id,
									name: ticket[0].user.name,
									route: ticket[0].route.name,
									created_at: ticket[0].created_at,
									departure_local: ticket[0].departure_local.local,
									arrival_local: ticket[0].arrival_local.local,
									validate_date: ticket[0].validate_date,
								};
							}
						})
						.catch(() => {
							return {
								valid: false,
								exception_code: "TICKET_NOT_FOUND",
								id: result.id,
							};
						});
				} else {
					return {
						valid: false,
						exception_code: "INVALID_TICKET",
						id: null,
					};
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		getHash(ticket) {
			const objToHash = _.pick(ticket, ["id", "user_id", "route_id"]);
			let hash = Buffer.from(JSON.stringify(objToHash)).toString("base64");
			return hash.split("").reverse().join("");
		},
		decypherHash(hash) {
			return new Promise((resolve, reject) => {
				try {
					hash = hash.split("").reverse().join("");
					let jsonFromHash = Buffer.from(hash, "base64").toString("ascii");
					const obj = JSON.parse(jsonFromHash);
					resolve(obj);
				} catch (ex) {
					reject(ex);
				}
			});
		},
		async validateTicketPurchase(ctx) {
			let ticket_config = await ctx.call("bus.ticket_config.find", {
				route_id: ctx.params.route_id,
			});
			if (ticket_config) {
				if (ticket_config[0].active) {
					let totalTickets = await ctx.call("bus.tickets_bought.count", {
						route_id: ctx.params.route_id,
					});
					if (totalTickets == ticket_config[0].max_to_buy) {
						throw new Errors.ValidationError(
							"Reached the maximum number possible to purchase tickets",
							"REACHED_LIMIT",
							{},
						);
					}
				} else {
					throw new Errors.ValidationError(
						"Ticket purchase for this route is not active",
						"PURCHASE_NOT_ACTIVE",
						{},
					);
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
