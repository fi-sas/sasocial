"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const crypto = require("crypto");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.tickets_hashs",
	table: "ticket_hash",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "ticket_hash")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "price_id", "departure_local_id", "arrival_local_id", "hash"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			price_id: { type: "number", positive: true },
			departure_local_id: { type: "number", positive: true },
			arrival_local_id: { type: "number", positive: true },
			hash: { type: "string", nullable: true },
		},
	},
	hooks: {
		before: {},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getHash: {
			cache: {
				keys: ["price_id", "departure_local_id", "arrival_local_id"],
			},
			params: {
				price_id: { type: "number", positive: true },
				departure_local_id: { type: "number", positive: true },
				arrival_local_id: { type: "number", positive: true },
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						...ctx.params,
					},
				}).then((res) => {
					if (res.length > 0) {
						return res[0];
					}

					return this._create(ctx, {
						...ctx.params,
						hash: crypto
							.createHash("sha256")
							.update(
								ctx.params.price_id +
									"" +
									ctx.params.departure_local_id +
									"" +
									ctx.params.arrival_local_id,
								"binary",
							)
							.digest("base64"),
					}).then((res) => {
						return res[0];
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
