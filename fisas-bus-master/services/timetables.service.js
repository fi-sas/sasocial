"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

module.exports = {
	name: "bus.timetables",
	table: "timetables",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "timetables")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"type_day_id",
			"route_id",
			"season_id",
			"line_number",
			"active"
		],
		defaultWithRelateds: ["hours"],
		withRelateds: {
			type_day(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.types_days", "type_day", "type_day_id");
			},
			route(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.routes", "route", "route_id");
			},
			season(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.seasons", "season", "season_id");
			},
			hours(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.hours.findHoursForTimetable", { timetable_id: doc.id })
							.then((res) => {doc.hours = res;});
					}),
				);
			},
		},
		entityValidator: {
			type_day_id: { type: "number", positive: true },
			route_id: { type: "number", positive: true },
			season_id: { type: "number", positive: true },
			active: { type: "boolean" },
		}
	},
	hooks: {
		before: {
			create: [
				"validateHoursOrder",
				async function validateRoute(ctx) {
					if (ctx.params.route_id) {
						await ctx.call("bus.routes.get", { id: ctx.params.route_id });
					} else {
						throw new Errors.ValidationError(
							"'route_id' is required!",
							"ROUTE_ID_REQUIRED",
							{},
						);
					}
				},
				async function validateSeason(ctx) {
					if (ctx.params.season_id) {
						await ctx.call("bus.seasons.get", { id: ctx.params.season_id });
					} else {
						throw new Errors.ValidationError(
							"'season_id' is required!",
							"SEASON_ID_REQUIRED",
							{},
						);
					}
				},
				async function validateTypeDay(ctx) {
					if (ctx.params.type_day_id) {
						await ctx.call("bus.types_days.get", { id: ctx.params.type_day_id });
					} else {
						throw new Errors.ValidationError(
							"'type_day_id' is required!",
							"TYPE_DAY_ID_REQUIRED",
							{},
						);
					}
				},
			],
			update: [],
			remove: [
				"removeHours"
			]
		},
		after: {
			create: [
				"saveHours"
			],
			update: [
				"saveHours"
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		create: {
			rest: "POST /",
			scope: "bus:timetables:create",
			visibility: "published",
			async handler(ctx) {
				let params = ctx.params;

				if (ctx.params.hours) {

					ctx.meta.locals = await this.getRouteLocals(ctx);

					this.validateNumberOfColumns(ctx);
					return this._create(ctx, params);
				} else {
					throw new Errors.ValidationError(
						"'hours' is required!",
						"HOURS_REQUIRED",
						{},
					);
				}
			}
		},
		updateHour: {
			rest: "PUT /update/hour",
			scope: "bus:timetables:update",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, positive: true },
				hour: { type: "string" },
			},
			async handler(ctx) {
				await ctx.call("bus.hours.get", { id: ctx.params.id });
				const hours =  ctx.call("bus.hours.patch", {
					id: ctx.params.id,
					hour: ctx.params.hour,
				});
				this.clearCache();
				return hours;
			}
		},
		insertLine: {
			rest: "POST /insert/line",
			scope: "bus:timetables:create",
			visibility: "published",
			params: {
				id: { type: "number", positive: true },
				hours: {
					type: "array",
					items: {
						type: "string"
					}
				},
			},
			async handler(ctx) {

				let timetable = await ctx.call("bus.timetables.get", { id: ctx.params.id });

				ctx.params.route_id =  _.first(timetable).route_id;
				ctx.meta.locals = await this.getRouteLocals(ctx).catch(error => {
					throw new Errors.ValidationError(
						error,
						"ROUTE_IS_NOT_HAVE_LOCALS",
						{},
					);
				});

				ctx.params.hours = [ctx.params.hours];
				ctx.params.line_number = _.first(timetable).hours.length + 1;

				this.validateNumberOfColumns(ctx);

				await this.saveHourLine(ctx);

				return ctx.call("bus.timetables.get", {
					id: ctx.params.id
				});
			}
		},
		deleteLine: {
			rest: "DELETE /delete/line/:timetable_id/:line_number",
			scope: "bus:timetables:delete",
			visibility: "published",
			params: {
				timetable_id: { type: "number", positive: true, convert: true },
				line_number: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {

				this.clearCache();
				return await ctx.call("bus.hours.deleteLineOfTimetable", {
					timetable_id: ctx.params.timetable_id,
					line_number: ctx.params.line_number,
				});
			}
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
	/* 	async getRouteLocals(ctx) {
			let locals = await ctx.call("bus.route_order.findLocalOfRoute", {
				route_id: ctx.params.route_id
			});
			console.log("LOCAIS::::::::", locals);
			return new Promise((resolve, reject) => {

				if (locals.length !== 0) {
					resolve(locals.map(local => local["id"]));
				} else {
					reject("The route has no locals");
				}
			});
		}, */

		async getRouteLocals(ctx) {
			let locals = await ctx.call("bus.route_order.findLocalIdsOfRoute", {
				route_id: ctx.params.route_id
			});
			if (locals.length !== 0) {
				return locals;
			} else {
				throw new Errors.ValidationError(
					"The route has no locals",
					"ROUTE_IS_NOT_HAVE_LOCALS",
					{},
				);
			}

		},

		async saveHours(ctx, res) {
			if (ctx.params.hours && Array.isArray(ctx.params.hours)) {
				await ctx.call("bus.hours.save_hours", {
					timetable_id: _.first(res).id,
					route_id: ctx.params.route_id,
					locals: ctx.meta.locals,
					hours: ctx.params.hours
				});

				res[0].hours = await ctx.call("bus.hours.find", {
					query: {
						timetable_id: _.first(res).id
					}
				});
			}
			return res;
		},
		async saveHourLine(ctx) {
			if (ctx.params.hours && Array.isArray(ctx.params.hours)) {
				await ctx.call("bus.hours.save_line_hours", {
					timetable_id: ctx.params.id,
					route_id: ctx.params.route_id,
					line_number: ctx.params.line_number,
					locals: ctx.meta.locals,
					hours: ctx.params.hours
				});
			} else {
				throw new Errors.ValidationError(
					"'hours' is not array!",
					"HOURS_NOT_ARRAY",
					{},
				);
			}
		},
		async removeHours(ctx, res) {
			typeof(res);
			return ctx.call("bus.hours.find", {
				query: {
					timetable_id: ctx.params.id,
				}
			})
				.then((res) => {
					res.map(async () => {await ctx.call("bus.hours.delete_hours", { id: ctx.params.id }); this.clearCache; });
				})
				.catch(err => this.logger.error("Unable to delete Hour relation!", err));
		},
		validateNumberOfColumns(ctx) {
			let totalLocals = ctx.meta.locals.length;
			let hourLinesError = [];

			ctx.params.hours.forEach((hourLine, index) => {
				if(totalLocals !== hourLine.length) {
					hourLinesError.push(index + 1);
				}
			});
			if (hourLinesError.length !== 0) {
				throw new Errors.ValidationError(
					`The route only supports a maximum of ${ctx.meta.locals.length} columns for hours. Your hour line ${hourLinesError.toString()} does not match the same number of locals`,
					"COLUMNS_NUMBER_SUPPORT",
					{},
				);
			}
		},
		validateHoursOrder(ctx) {
			let lines = [];
			let sorted = true;
			if (ctx.params.hours) {
				ctx.params.hours.forEach((hourLine, index) => {
					sorted = this.isSorted(hourLine);
					!sorted ? lines.push(index + 1) : null;
				});
				if (lines.length !== 0) {
					throw new Errors.ValidationError(
						`Lines ${lines.toString()} of the table do not have the hours in order`,
						"LINES_NOT_ORDER",
						{},
					);
				}
			}
		},
		isSorted(hours) {
			let sorted = true;
			hours.forEach((hour, index) => {
				const firts_date = new Date("2018-12-12T" + hour);
				const second_date = new Date("2018-12-12T" + hours[index + 1]);
				if(firts_date >= second_date){
					sorted = false;
				}
			});
			return sorted;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
