"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { checkIdsIsNotExist } = require("../helpers/checkers");
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.types_days",
	table: "types_days",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "types_days")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["days_weeks"],
		withRelateds: {
			days_weeks(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.days_group.findDaysOfTypeDay", {
								type_day_id: doc.id,
							})
							.then((res) => (doc.days_weeks = res));
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				async function validateDaysWeekIDS(ctx) {
					let ids = await checkIdsIsNotExist(ctx, ctx.params.days_week_ids, "bus", "days_weeks");
					if (ids.length !== 0) {
						throw new Errors.ValidationError(
							`There are no days weeks with these ids: ${ids.toString()}`,
							"DAYS_WEEKS_IDS_NOT_FOUND",
							{},
						);
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			remove: [
				"ValidateRemoveDay",
				"deleteRelatedDays"
			]
		},
		after: {
			create: [
				"saveDays"
			],
			update: [
				"saveDays"
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		addDay: {
			rest: "POST /addDay",
			visibility: "published",
			params: {
				type_day_id: { type: "number", positive: true },
				day_id: { type: "number" },
			},
			async handler(ctx) {
				let type_day = await ctx.call("bus.types_days.get", {
					id: ctx.params.type_day_id,
				});

				if (type_day[0].days_weeks.find(day => day.id === ctx.params.day_id)) {
					throw new Errors.ValidationError(
						`The day ID ${ctx.params.day_id} is already exist in this type day`,
						"DAY_ID_ATREADY_EXIST",
						{},
					);
				}

				await ctx.call("bus.days_weeks.get", {
					id: ctx.params.day_id,
				});
				return ctx.call("bus.days_group.create", {
					type_day_id: ctx.params.type_day_id,
					day_id: ctx.params.day_id,
				});
			}
		},
		removeDay: {
			rest: "DELETE /removeDay/:day_group_id",
			visibility: "published",
			params: {
				day_group_id: { type: "number", positive: true, convert: true }
			},
			async handler(ctx) {
				await ctx.call("bus.days_group.get", {
					id: ctx.params.day_group_id,
				});
				return ctx.call("bus.days_group.remove_day", {
					id: ctx.params.day_group_id,
				});
			}
		},
	},

	/**
	 * Events
	 */
	events: {
		"bus.days_group.*"(){
			this.clearCache();
		}

	},

	/**
	 * Methods
	 */
	methods: {
		async saveDays(ctx, res) {
			if (ctx.params.days_week_ids && Array.isArray(ctx.params.days_week_ids)) {
				await ctx.call("bus.days_group.save_days", {
					type_day_id: res[0].id,
					days_week_ids: ctx.params.days_week_ids
				});
				res[0].days_week_ids = await ctx.call("bus.days_group.findDaysOfTypeDay", {
					type_day_id: res[0].id
				});
			}
			return res;
		},
		async deleteRelatedDays(ctx, res) {
			typeof(res);
			return ctx.call("bus.days_group.find", {
				query: {
					type_day_id: ctx.params.id,
				}
			})
				.then((res) => {
					res.map(async (dayRel) => await ctx.call("bus.days_group.remove", { id: dayRel.id }) );
				})
				.catch(err => this.logger.error("Unable to delete Day relation!", err));
		},

		async ValidateRemoveDay(ctx) {
			const timetables = await ctx.call("bus.timetables.find", { query: {
				type_day_id: ctx.params.id
			} });
			if(timetables.length !== 0 ) {
				throw new Errors.ValidationError(
					"This type day is using in timetable",
					"BUS_TYPE_DAY_IN_USE_TIMETABLES",
					{},
				);
			}
			const groups_day = await ctx.call("bus.days_group.find", { query: {
				type_day_id: ctx.params.id
			} });
			if(groups_day.length !== 0 ) {
				throw new Errors.ValidationError(
					"This type day is using in groups_day",
					"BUS_TYPE_DAY_IN_USE_GROUPS_DAYS",
					{},
				);
			}

		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
