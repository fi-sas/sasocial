"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { withdrawalsStatus } = require("../values/enums");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const WithdrawalsStateMachine = require("../state-machine/withdrawal.machine");

/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.withdrawals",
	table: "withdrawals",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "withdrawals")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"application_id",
			"justification",
			"date_withdrawal",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["application", "withdrawal_history"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.applications", "application", "application_id");
			},
			withdrawal_history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "bus.withdrawals_history", "withdrawal_history", "id", "withdrawal_id");
			},
		},
		entityValidator: {
			justification: { type: "string", max: 255 },
			status: { type: "enum", values: withdrawalsStatus },
			application_id: { type: "number", positive: true, convert: true },
			date_withdrawal: { type: "date", convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.date_withdrawal = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "pending";
				},
				async function validateApplication(ctx) {
					if (ctx.params.application_id) {
						const application = await ctx.call("bus.applications.get", { id: ctx.params.application_id });
						if (application[0].status != "accepted") {
							throw new ValidationError(
								"Only can withdrawal on application accepted status",
								"BUS_WITHDRAWAL_APPLICATION_NOT_VALID_STATUS",
								{},
							);
						}
					}
				},
				async function validateOpenWithdrawals(ctx) {
					const user_applications = await ctx.call("bus.applications.find", { query: { user_id: ctx.meta.user.id, status: "accepted" } });
					const withdrawals = await this._find(ctx, { query: { application_id: user_applications.map((r) => r.id), status: "pending" } });
					if (withdrawals.length > 0) {
						throw new ValidationError(
							"The user already have an open withdrawal request",
							"BUS_WITHDRAWAL_ALREADY_OPEN",
							{},
						);
					}
				},

			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: [
				async function changeApplciationStatus(ctx, res) {
					await ctx.call("bus.applications.status", { id: res[0].application_id, event: "quit" });
					return res;
				},

			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		approve: {
			// REST: PATCH /:id
			rest: "POST /:id/approve",
			scope: "bus:withdrawals:status",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, min: 0 },
			},
			async handler(ctx) {
				const withdrawal = await ctx.call("bus.withdrawals.get", { id: ctx.params.id });

				const stateMachine = await WithdrawalsStateMachine.createStateMachine(
					withdrawal[0].status,
					ctx,
				);

				if (stateMachine.can("ACCEPT")) {
					const result = await stateMachine["accept"](withdrawal[0].application_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_WITHDRAWAL_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				}
			},
		},
		reject: {
			// REST: PATCH /:id
			rest: "POST /:id/reject",
			scope: "bus:withdrawals:status",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, min: 0 },
			},
			async handler(ctx) {
				const withdrawal = await ctx.call("bus.withdrawals.get", { id: ctx.params.id });

				const stateMachine = await WithdrawalsStateMachine.createStateMachine(
					withdrawal[0].status,
					ctx,
				);

				if (stateMachine.can("REJECT")) {
					const result = await stateMachine["reject"](withdrawal[0].application_id);
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"BUS_WITHDRAWAL_UNAUTHORIZED_REQUEST",
							{},
						);
					}
					return result;
				}
			},
		},

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
