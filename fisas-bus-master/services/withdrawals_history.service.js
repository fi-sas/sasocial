"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { withdrawalsStatus } = require("../values/enums");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.withdrawals_history",
	table: "withdrawals_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "withdrawals_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"withdrawal_id",
			"user_id",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			user_id: { type: "number", positive: true },
			withdrawal_id: { type: "number", positive: true },
			status: { type: "enum", values: withdrawalsStatus },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
