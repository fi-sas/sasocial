"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { checkIdsIsNotExist } = require("../helpers/checkers");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('../helpers/node_modules/moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "bus.zones",
	table: "zones",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("bus", "zones")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"route_id",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["locals"],
		withRelateds: {
			locals(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("bus.local_zone.findLocalsOfZone", {
								zone_id: doc.id,
							})
							.then((res) => (doc.locals = res));
					}),
				);
			},
			route(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "bus.routes", "route", "route_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			route_id: { type: "number", positive: true },
			active: { type: "boolean" }
		}
	},
	hooks: {
		before: {
			create: [
				async function validateLocalsIDS(ctx) {
					if (ctx.params.locals_ids) {
						let ids = await checkIdsIsNotExist(ctx, ctx.params.locals_ids, "bus", "locals");
						if (ids.length !== 0) {
							throw new Errors.ValidationError(
								`There are no locals with these ids: ${ids.toString()}`,
								"LOCALS_IDS_NOT_FOUND",
								{},
							);
						}
					} else {
						throw new Errors.ValidationError(
							"'locals' is required!",
							"LOCALS_REQUIRED",
							{},
						);
					}
				},
				async function validateRoute(ctx) {
					if (ctx.params.route_id) {
						await ctx.call("bus.routes.get", { id: ctx.params.route_id });
					} else {
						throw new Errors.ValidationError(
							"'rout_id' is required!",
							"ROUTE_ID_REQUIRED",
							{},
						);
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			remove: [
				"removeLocalsOfZone"
			]
		},
		after: {
			create: [
				"saveLocals",
			],
			update: [
				"saveLocals",
			],
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
		async saveLocals(ctx, res) {
			if (ctx.params.locals_ids && Array.isArray(ctx.params.locals_ids)) {
				await ctx.call("bus.local_zone.save_locals", {
					zone_id: res[0].id,
					locals_ids: ctx.params.locals_ids
				});
				res[0].locals = await ctx.call("bus.local_zone.findLocalsOfZone", {
					zone_id: res[0].id
				});
			}
			return res;
		},
		async removeLocalsOfZone(ctx) {
			return await ctx.call("bus.local_zone.removeLocalZones", { zone_id: ctx.params.id });
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
