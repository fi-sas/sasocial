let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "APPROVE", from: "submitted", to: "approved" },
			{ name: "REJECT", from: "submitted", to: "rejected" },
		],
		methods: {
			saveAbsence: async function (lifecycle) {
				return ctx.call("bus.absences.patch", {
					id: ctx.params.id,
					status: lifecycle.to
				});
			},
			async sendNotification(key, abcense) {
				const applicaiton = await ctx.call("bus.applications.get", { id: abcense[0].applicaiton_id });
				ctx.call("notifications.alerts.create_alert", {
					alert_type_key: `MOBILITY_ABSENCE_${key}`,
					user_id: applicaiton[0].user_id,
					user_data: {},
					data: {
						application: applicaiton[0],
					},
					variables: {},
					external_uuid: null,
				});
			},
			onAfterApprove: async function (lifecycle) {
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const abcense = await this.saveAbsence(lifecycle);
				this.sendNotification("APPROVED", abcense);
				return abcense;
			},
			onAfterReject: async function (lifecycle) {
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const abcense = await this.saveAbsence(lifecycle);
				this.sendNotification("REJECTED", abcense);
				return abcense;
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
