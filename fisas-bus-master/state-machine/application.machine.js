let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [

			{ name: "ACCEPT", from: "approved", to: "accepted" },
			{ name: "REJECT", from: "approved", to: "rejected" },
			{ name: "CLOSE", from: "accepted", to: "closed" },
			{ name: "QUIT", from: "accepted", to: "quiting" },
			{ name: "WITHDRAWAL", from: "quiting", to: "withdrawal" },
			{ name: "WITHDRAWALREJECT", from: "quiting", to: "accepted" },
			{ name: "CANCEL", from: "submitted", to: "canceled" },
			{ name: "SUSPEND", from: "accepted", to: "suspended" },
			{ name: "RESUME", from: "suspended", to: "accepted" },
			{ name: "APPROVE", from: "submitted", to: "approved" },
			{ name: "NOTAPPROVE", from: "submitted", to: "not_approved"},
		],
		methods: {
			// Save new application state and operation in applications history
			saveApplication: async function (lifecycle) {
				const app = await ctx.call("bus.applications.patch", {
					id: ctx.params.id,
					status: lifecycle.to,
					updated_at: new Date(),
					...ctx.params.application,
				});
				await ctx.call("bus.application_history.create", {
					application_id: ctx.params.id,
					user_id: ctx.meta.user ? ctx.meta.user.id : null,
					status: lifecycle.to,
					notes: ctx.params.notes,
				});
				return app;
			},
			sendNotification(key, application) {
				ctx.call("notifications.alerts.create_alert", {
					alert_type_key: `MOBILITY_APPLICATION_${key}`,
					user_id: application[0].user_id,
					user_data: {},
					data: {
						application: application[0],
					},
					variables: {},
					external_uuid: null,
				});
			},
			onAfterAccept: async function (lifecycle) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" ) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("ACCEPTED", app);
				return app;
			},
			onAfterReject: async function (lifecycle) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none") {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("REJECTED", app);
				return app;
			},
			onAfterWithdrawal: async function (lifecycle) {
				// Validate if user who are trying change state, its the applicaiton owner
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("WITHDRAWAL", app);
				return app;
			},
			onAfterClose: async function (lifecycle) {
				// Validate if user who are trying change state, its the applicaiton owner
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("CLOSED", app);
				return app;
			},
			onAfterCancel: async function (lifecycle, applicationOwner) {
				// Validate if user who are trying change state, its the applicaiton owner
				if (lifecycle.from === "none" || ctx.meta.user.id != applicationOwner) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("CANCELLED", app);
				return app;
			},
			onAfterQuit: async function (lifecycle, applicationOwner) {
				// Validate if user who are trying change state, its the applicaiton owner
				if (lifecycle.from === "none" || ctx.meta.user.id != applicationOwner) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("QUITING", app);
				return app;
			},
			onAfterWithdrawalreject: async function (lifecycle) {
				// Validate if user who are trying change state, its the applicaiton owner
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("WITHDRAWALREJECT", app);
				return app;
			},
			onAfterSuspend: async function (lifecycle) {
				if (lifecycle.from === "none") {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("SUSPENDED", app);
				return app;
			},
			onAfterResume: async function (lifecycle) {
				if (lifecycle.from === "none") {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("RESUME", app);
				return app;
			},
			onAfterApprove:  async function (lifecycle) {
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("APPROVE", app);
				return app;
			},
			onAfterNotapprove: async function (lifecycle) {
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveApplication(lifecycle);
				this.sendNotification("NOTAPPROVE", app);
				return app;
		}}

	});
}

exports.createStateMachine = createStateMachine;


