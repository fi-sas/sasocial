let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "VALIDATE", from: "in_validation", to: "validated" },
			{ name: "EMIT", from: "validated", to: "emitted" },
			{ name: "REJECT", from: "in_validation", to: "rejected" },
			{ name: "REJECT", from: "validated", to: "rejected" }
		],
		methods: {
			// Save new application state and operation in applications history
			saveDeclaration: async function (lifecycle) {
				const app = await ctx.call("bus.sub23_declarations.patch", {
					id: ctx.params.id,
					status: lifecycle.to,
					...ctx.params.declaration,
				});
				await ctx.call("bus.sub23_declarations_history.create", {
					sub23_declarations_id: ctx.params.id,
					user_id: ctx.meta.user.id,
					status: lifecycle.to,
					notes: ctx.params.notes,
				});
				return app;
			},
			sendNotification(key, application) {
				ctx.call("notifications.alerts.create_alert", {
					alert_type_key: `MOBILITY_SUB23_DECLARATION_${key}`,
					user_id: application[0].user_id,
					user_data: {},
					data: {
						declaration: application[0],
					},
					variables: {},
					external_uuid: null,
				});
			},
		
			onAfterEmit: async function (lifecycle) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveDeclaration(lifecycle);
				this.sendNotification("EMITTED", app);
				return app;
			},

			onAfterValidate: async function (lifecycle) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveDeclaration(lifecycle);
				this.sendNotification("VALIDATED", app);
				return app;
			},

			onAfterReject: async function (lifecycle) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const app = await this.saveDeclaration(lifecycle);
				this.sendNotification("REJECTED", app);
				return app;
			}
		},

	});
}

exports.createStateMachine = createStateMachine;


