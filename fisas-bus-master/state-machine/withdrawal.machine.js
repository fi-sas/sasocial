let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "ACCEPT", from: "pending", to: "accepted" },
			{ name: "REJECT", from: "pending", to: "rejected" }
		],
		methods: {
			// Save new application state and operation in applications history
			saveWithdrawal: async function (lifecycle) {
				const withdrawal = await ctx.call("bus.withdrawals.patch", {
					id: ctx.params.id,
					status: lifecycle.to,
				});
				await ctx.call("bus.withdrawals_history.create", {
					withdrawal_id: ctx.params.id,
					user_id: ctx.meta.user.id,
					status: lifecycle.to
				});
				return withdrawal;
			},
			onAfterAccept: async function (lifecycle, application_id) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const savedWithdrawal = await this.saveWithdrawal(lifecycle);

				// Update application status
				await ctx.call("bus.applications.status", { id: application_id, event: "withdrawal" });
				return savedWithdrawal;
			},
			onAfterReject: async function (lifecycle, application_id) {
				// Validate if user have acess to backoffice
				if (lifecycle.from === "none" || !ctx.meta.user.can_access_BO) {
					return null;
				}
				const savedWithdrawal = await this.saveWithdrawal(lifecycle);

				// Update application status
				await ctx.call("bus.applications.status", { id: application_id, event: "withdrawalreject" });
				return savedWithdrawal;
			}
		},

	});
}

exports.createStateMachine = createStateMachine;
