var fs = require("fs");
var mysql_connection = require('../models/connection');

var connection = mysql_connection.conncection();


connection.query('USE '+ process.env.MYSQL_DATABASE, function (err) {

    if (!err) {

        fs.readFile('tools/update_database.sql', 'utf8', function (err, data) {

            if (!err) {

                    data = data.replace(/\r?\n|\r/g, " ");

                    connection.query(data, function (err) {

                        if (err) {
                            connection.end();
                            console.log(err);

                        } else {
                            connection.end();
                            console.log("Success!");
                        }
                    });
          } else {
             connection.end();
             console.log(err);
          }
        });
    } else {
        connection.end();
        console.log(err);
    }
 });
