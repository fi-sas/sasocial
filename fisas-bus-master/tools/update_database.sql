/* DROP procedure IF EXISTS `get_configuration`;

CREATE DEFINER=`root`@`%` PROCEDURE `get_configuration`()
BEGIN
SET @conf = (select count(*) from configuration);
IF (@conf = 0) THEN
INSERT INTO configuration (id,service_id) VALUES (1,0);
select * from configuration;
ELSE
select * from configuration;
END IF;
END;
 */

CREATE TABLE IF NOT EXISTS fisas_bus.applications (
	name varchar(100) NULL,
	birth_date varchar(100) NULL,
	student_number varchar(100) NULL,
	tin varchar(100) NULL,
	identification varchar(100) NULL,
	nationality varchar(100) NULL,
	`date` varchar(100) NULL,
	email varchar(100) NULL,
	phone_1 varchar(100) NULL,
	phone_2 varchar(100) NULL,
	course_year INT NULL,
	course_id INT NULL,
	renovation TINYINT NULL,
	school_id INT NULL,
	observations varchar(255) NULL,
	accept_procedure TINYINT NULL,
	morning_local_id_from INT NULL,
	morning_local_id_to INT NULL,
	afternoon_local_id_from INT NULL,
	afternoon_local_id_to INT NULL,
	user_id INT NULL,
	updated_at varchar(100) NULL,
	created_at varchar(100) NULL,
	status varchar(100) NULL,
	id INT auto_increment NOT NULL,
	CONSTRAINT applications_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS fisas_bus.payment_months (
	id INT auto_increment NOT NULL,
	application_id INT NULL,
	`month` varchar(100) NULL,
	`year` INT NULL,
	value FLOAT NULL,
	paid TINYINT NULL,
	created_at varchar(100) NULL,
	updated_at varchar(100) NULL,
	CONSTRAINT payment_months_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS fisas_bus.withdrawals (
	id INT auto_increment NOT NULL,
	application_id INT NULL,
	justification varchar(100) NULL,
	date_withdrawal varchar(100) NULL,
	status varchar(100) NULL,
	created_at varchar(100) NULL,
	updated_at varchar(100) NULL,
	CONSTRAINT withdrawals_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS fisas_bus.configurations (
	configuration_key varchar(100) NOT NULL,
	config varchar(500) NULL,
	CONSTRAINT configurations_PK PRIMARY KEY (configuration_key)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

INSERT INTO fisas_bus.configurations (configuration_key,config)
	VALUES ('APPLICATION_REGULATION','null');



