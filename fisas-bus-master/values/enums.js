const applicationsStatus = ["submitted", "approved", "not_approved", "accepted", "rejected", "withdrawal", "closed", "canceled", "quiting", "suspended"];

const withdrawalsStatus = ["pending", "accepted", "rejected"];
const sub23DeclarationsStatus = ["submitted", "in_validation", "validated", "emitted", "rejected"];



const ticketTypes = ["TICKET", "PASS"];

const configurationsKeys = ["GOOGLE_API_KEY", "APPLICATION_REGULATION", "SERVICE_ID", "ASSIGNED_STATUS_AUTO_CHANGE", "EVENT_FOR_AUTO_CHANGE", "NUNBER_DAYS_FOR_CHANGE"];

module.exports = {
	applicationsStatus,
	ticketTypes,
	withdrawalsStatus,
	configurationsKeys,
	sub23DeclarationsStatus
};


