## [1.27.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.6...v1.27.7) (2022-06-02)


### Performance Improvements

* add database index ([f9d37df](https://gitlab.com/fi-sas/fisas-current-account/commit/f9d37df7371f32b0a906cad0e8624de848560665))

## [1.27.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.5...v1.27.6) (2022-06-02)


### Bug Fixes

* **movements:** create movement checking created movement ([ac1af3d](https://gitlab.com/fi-sas/fisas-current-account/commit/ac1af3d830010b4e06f80c0b5db3e5622dfff475))

## [1.27.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.4...v1.27.5) (2022-05-26)


### Bug Fixes

* **carts:** clearCart cleaning user of checkout not logged user ([6211b4a](https://gitlab.com/fi-sas/fisas-current-account/commit/6211b4a4d6182aebba192e10673650102bc822a9))

## [1.27.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.3...v1.27.4) (2022-04-13)


### Bug Fixes

* **movements:** calculate liquidDiscountValue ([2be7740](https://gitlab.com/fi-sas/fisas-current-account/commit/2be7740270b8e30e8514551163d2965732227bd5))

## [1.27.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.2...v1.27.3) (2022-02-23)


### Bug Fixes

* **movements:** changes sales reports query to include invoices ([c2f4764](https://gitlab.com/fi-sas/fisas-current-account/commit/c2f476402740772437f0628dd495ebd9d3be62a1))

## [1.27.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.1...v1.27.2) (2022-02-22)


### Bug Fixes

* **movements:** limit duplicate invoice payment ([2bff8a6](https://gitlab.com/fi-sas/fisas-current-account/commit/2bff8a61abcb8ac76844f4ec7e625c7ce08e4eac))

## [1.27.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.27.0...v1.27.1) (2022-02-14)


### Bug Fixes

* **movement_item:** round vat value item ([ffab88b](https://gitlab.com/fi-sas/fisas-current-account/commit/ffab88b415e59b5efaaaa5393653ba04d80f9277))

# [1.27.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.26.0...v1.27.0) (2022-02-10)


### Bug Fixes

* **movements:** add new filters to movements report ([4522ebd](https://gitlab.com/fi-sas/fisas-current-account/commit/4522ebdeaa32a95148feace78cfc0fffe08697d5))


### Features

* add new reports ([e849fb9](https://gitlab.com/fi-sas/fisas-current-account/commit/e849fb90a02e5830db4f81a3eb17c30f204bc304))
* **movements:** add detailed charge report ([322e29c](https://gitlab.com/fi-sas/fisas-current-account/commit/322e29c90af7368d0b19ae3f57810b076c4b11a0))
* **reports:** add new columns to detail sales reports ([ff6783c](https://gitlab.com/fi-sas/fisas-current-account/commit/ff6783cf82f0922520da5d6351d1bb8957c804ae))

# [1.26.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.25.0...v1.26.0) (2022-02-09)


### Features

* **tpa_period:** initial version of tpa_period service ([4d04f5d](https://gitlab.com/fi-sas/fisas-current-account/commit/4d04f5db022f444b5d5c146aa84e31a8b23d66a5))

# [1.25.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.24.0...v1.25.0) (2022-02-09)


### Features

* add new fields client and merchant receipt ([4e183a4](https://gitlab.com/fi-sas/fisas-current-account/commit/4e183a4e536587a397802843efb54d01b987d8a0))

# [1.24.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.23.2...v1.24.0) (2022-01-27)


### Features

* **movements:** add payment_method_name to charges report ([0458bfe](https://gitlab.com/fi-sas/fisas-current-account/commit/0458bfec2c03eb7afc025ffceef25a2daf194e8e))

## [1.23.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.23.1...v1.23.2) (2022-01-26)


### Bug Fixes

* **payment:** remove uncessary timeout ([f97ecc5](https://gitlab.com/fi-sas/fisas-current-account/commit/f97ecc5ac7e520df63f12de3255192c5be83a019))

## [1.23.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.23.0...v1.23.1) (2022-01-25)


### Bug Fixes

* **movment:** set bypass validation ([47f2d34](https://gitlab.com/fi-sas/fisas-current-account/commit/47f2d34e87dbffb4dcc0af19cfb5ea85b7eb1d4b))

# [1.23.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.22.0...v1.23.0) (2022-01-24)


### Features

* **movement:** add sales reports ([d9753ca](https://gitlab.com/fi-sas/fisas-current-account/commit/d9753ca8b8535bc7bced1c51e74c1599f6236d6b))

# [1.22.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.7...v1.22.0) (2022-01-18)


### Features

* **movements:** bypass balance with invoice and some fixes ([cd6bffc](https://gitlab.com/fi-sas/fisas-current-account/commit/cd6bffc5f0d20bcb1a68a73a2394b739c5edf29e))

## [1.21.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.6...v1.21.7) (2022-01-05)


### Bug Fixes

* **guest:** prevent guest charge and payment ([cec0374](https://gitlab.com/fi-sas/fisas-current-account/commit/cec037478ddddbd5599aa9870212de1b95a0e5e4))

## [1.21.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.5...v1.21.6) (2021-11-30)


### Bug Fixes

* **seeds:** remove wrong field send_erp ([53a1e68](https://gitlab.com/fi-sas/fisas-current-account/commit/53a1e6895271c14532b7bcc5a01948830aabe392))

## [1.21.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.4...v1.21.5) (2021-11-19)


### Bug Fixes

* **movements:** limit checkMovementsToSendERP ([fc352a9](https://gitlab.com/fi-sas/fisas-current-account/commit/fc352a93315745082d24535337a32b4499e782b5))

## [1.21.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.3...v1.21.4) (2021-11-18)


### Bug Fixes

* **payment:** paymentConfirmed timeout ([a97908c](https://gitlab.com/fi-sas/fisas-current-account/commit/a97908c16aac1e31c3217a3e5c65c03b327259ad))

## [1.21.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.2...v1.21.3) (2021-11-18)


### Bug Fixes

* **erp:** change to true for remove on fail queue ([112c092](https://gitlab.com/fi-sas/fisas-current-account/commit/112c0927b1b3dfab237d2378c6bea50b9443d42c))

## [1.21.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.1...v1.21.2) (2021-11-16)


### Bug Fixes

* **payment:** error when confirmation throws ([a2e51b5](https://gitlab.com/fi-sas/fisas-current-account/commit/a2e51b5939f3e94976dd0adc9c92fd85f0817874))

## [1.21.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.21.0...v1.21.1) (2021-11-10)


### Bug Fixes

* **movement:** remove unnecessary added column sale_location ([15adf66](https://gitlab.com/fi-sas/fisas-current-account/commit/15adf66bff7a0f084adbb13ea1d90d8409646a44))

# [1.21.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.12...v1.21.0) (2021-11-10)


### Features

* **movement:** add field sale_location ([973df28](https://gitlab.com/fi-sas/fisas-current-account/commit/973df28979f26efacd2cae5f67a06adf851e7538))

## [1.20.12](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.11...v1.20.12) (2021-11-09)


### Bug Fixes

* **reports:** carges and movements ([ef4332d](https://gitlab.com/fi-sas/fisas-current-account/commit/ef4332d4f6c92e1ade91d20e2afe23a2e10875a2))

## [1.20.11](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.10...v1.20.11) (2021-11-05)


### Bug Fixes

* **report:** add erp_doc field ([58864c6](https://gitlab.com/fi-sas/fisas-current-account/commit/58864c6a0d54f0cdb3365f5959e68048addbfb42))

## [1.20.10](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.9...v1.20.10) (2021-11-05)


### Bug Fixes

* **reports:** add missing reports actions ([be4186b](https://gitlab.com/fi-sas/fisas-current-account/commit/be4186bc0232990c5d1f72bf80b516a98e334847))

## [1.20.9](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.8...v1.20.9) (2021-10-19)


### Bug Fixes

* **carts:** change order of remove item ([a53ab3a](https://gitlab.com/fi-sas/fisas-current-account/commit/a53ab3ab399ae10fc1a9b6731e12d8660dd4a6eb))

## [1.20.8](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.7...v1.20.8) (2021-10-13)


### Bug Fixes

* partialCancel find paid by id instead of productCode ([d9bdafc](https://gitlab.com/fi-sas/fisas-current-account/commit/d9bdafc4e6c73afc21f5aa93237d1581a73dc07f))

## [1.20.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.6...v1.20.7) (2021-10-13)


### Bug Fixes

* prevent multiple checkout on same transaction ([642ad63](https://gitlab.com/fi-sas/fisas-current-account/commit/642ad63596a3d8656adf68e883a0a0fcca1a6153))

## [1.20.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.5...v1.20.6) (2021-10-12)


### Bug Fixes

* **movement:** prevent tin length when come from users ([6c25d10](https://gitlab.com/fi-sas/fisas-current-account/commit/6c25d1099006c27d2a98f462ff5c69eb98f12f7e))
* update ms_core ([1f35b40](https://gitlab.com/fi-sas/fisas-current-account/commit/1f35b402d105fd9b3506b8fde2f955c9861a8eb7))

## [1.20.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.4...v1.20.5) (2021-10-11)


### Bug Fixes

* **movements:** add total cancelled control on action cancel ([8ff6dfb](https://gitlab.com/fi-sas/fisas-current-account/commit/8ff6dfb3b346fbe688075daba1601634559a5e86))
* **movements:** minor fixs ([aee59a9](https://gitlab.com/fi-sas/fisas-current-account/commit/aee59a975f61c32d5013ba3029e23b8a38f78ae8))

## [1.20.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.3...v1.20.4) (2021-09-17)


### Bug Fixes

* **payment:** credit_note dont need check funds ([e26c07d](https://gitlab.com/fi-sas/fisas-current-account/commit/e26c07d3496beaede2b5250e135f807fbb1a333b))

## [1.20.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.2...v1.20.3) (2021-09-15)


### Bug Fixes

* **document:** validator number wrong maxNumber set ([2abe98d](https://gitlab.com/fi-sas/fisas-current-account/commit/2abe98ddd991009323f3d16528aad7711ab30602))

## [1.20.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.1...v1.20.2) (2021-09-08)


### Bug Fixes

* **payment:** set movement cancel status ([3733b79](https://gitlab.com/fi-sas/fisas-current-account/commit/3733b79dac883c3d2a6414fa29fa568ef3c24c76))

## [1.20.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.20.0...v1.20.1) (2021-09-08)


### Bug Fixes

* **payments:** update movement status when cancelled ([72f60e4](https://gitlab.com/fi-sas/fisas-current-account/commit/72f60e4f5808e87827a8f8ae3bd22dc077b393e5))

# [1.20.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.10...v1.20.0) (2021-09-08)


### Features

* **erp:** add cron to resend movements with RETRY status ([2ce7916](https://gitlab.com/fi-sas/fisas-current-account/commit/2ce7916a9b5cadf8942e9d51d9c60cb17ee27284))

## [1.19.10](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.9...v1.19.10) (2021-09-06)


### Bug Fixes

* **checkout-item:** remove item before throw error ([65718bb](https://gitlab.com/fi-sas/fisas-current-account/commit/65718bb991303a6c7caed714bdf9b560d6122738))

## [1.19.9](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.8...v1.19.9) (2021-08-30)


### Bug Fixes

* **movements:** add extra_info on partial cancelled intems ([65b557e](https://gitlab.com/fi-sas/fisas-current-account/commit/65b557e8809bd48f8168140982000f2d97d99283))

## [1.19.8](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.7...v1.19.8) (2021-08-30)


### Bug Fixes

* **mevements:** add extra_info do cancels ([d695fca](https://gitlab.com/fi-sas/fisas-current-account/commit/d695fca78ffc79437f3bf82720f3e31c2245cfd8))

## [1.19.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.6...v1.19.7) (2021-08-23)


### Bug Fixes

* **payments:** notify services when not credit_note ([e8cf0dc](https://gitlab.com/fi-sas/fisas-current-account/commit/e8cf0dc7b9c6b5f35e66c39b01c5d88ab75d7c50))

## [1.19.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.5...v1.19.6) (2021-08-23)


### Bug Fixes

* **movement:** fix wrong is_immediate check ([92aeb14](https://gitlab.com/fi-sas/fisas-current-account/commit/92aeb14f5df88b5529bc48f0aa9652e294ea0bd8))

## [1.19.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.4...v1.19.5) (2021-08-20)


### Bug Fixes

* **refmb:** check payment_method when charge ([9579604](https://gitlab.com/fi-sas/fisas-current-account/commit/957960406402371b97afd42306f4e1b5e9405ae2))

## [1.19.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.3...v1.19.4) (2021-08-19)


### Bug Fixes

* **movement:** wrong payment_method_id source ([dc1ec27](https://gitlab.com/fi-sas/fisas-current-account/commit/dc1ec27bafc23af00bbf16dace5d0d20393e7a04))

## [1.19.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.2...v1.19.3) (2021-08-19)


### Bug Fixes

* **payments:** logs for trace payments confirm ([ee308dd](https://gitlab.com/fi-sas/fisas-current-account/commit/ee308ddfd86d2923e98cdb7ecaf7020d0818c377))

## [1.19.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.1...v1.19.2) (2021-08-19)


### Bug Fixes

* **payments:** logs for payment confirm debug ([481ea1d](https://gitlab.com/fi-sas/fisas-current-account/commit/481ea1d8c88600e0e0b0adbc0411a78bb6090361))

## [1.19.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.19.0...v1.19.1) (2021-08-19)


### Bug Fixes

* **ama:** check for user when AMA callback ([5129ec6](https://gitlab.com/fi-sas/fisas-current-account/commit/5129ec6c8507b770411cf404453e84e53da9d885))

# [1.19.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.18.3...v1.19.0) (2021-08-18)


### Features

* **erp:** add action to save related account docs to send erp ([098b3b4](https://gitlab.com/fi-sas/fisas-current-account/commit/098b3b4cccc0f4209299001fb9bf98574c0ba13b))

## [1.18.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.18.2...v1.18.3) (2021-08-17)


### Bug Fixes

* **cart:** remove item from cart when is from "pay now" item ([7825c64](https://gitlab.com/fi-sas/fisas-current-account/commit/7825c64460a0402e9bba9fa7fb20b3c6d30a0491))
* **queue:** middleware errors ([f3a9064](https://gitlab.com/fi-sas/fisas-current-account/commit/f3a9064ceab9d0fccc2c8eec96b82c95bb55d828))

## [1.18.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.18.1...v1.18.2) (2021-08-13)


### Bug Fixes

* **ama_paygate:** amount currency symbol ([c5a9ab5](https://gitlab.com/fi-sas/fisas-current-account/commit/c5a9ab5e705ff98396a6e6027827cb8c615fdcb8))

## [1.18.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.18.0...v1.18.1) (2021-08-11)


### Bug Fixes

* **middleware_erp:** fix action uri ([b0196c1](https://gitlab.com/fi-sas/fisas-current-account/commit/b0196c14e44d99edf47fc4fc1d29a8f13f87c7d8))

# [1.18.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.6...v1.18.0) (2021-08-11)


### Bug Fixes

* **env:** dev/local env cron value remove double-quotes ([8ae066c](https://gitlab.com/fi-sas/fisas-current-account/commit/8ae066cd7e90abe5ba187603ef187cda17ef875a))
* **env:** prod env cron value remove double-quotes ([b5e4660](https://gitlab.com/fi-sas/fisas-current-account/commit/b5e46603b53d9edc7cab717f86de4ad1997aea1c))
* **refmb:** format amount currency ([ab0bfc7](https://gitlab.com/fi-sas/fisas-current-account/commit/ab0bfc75537cf5102d22319fda952ac49284c7fd))


### Features

* **account:** create field for helpdesk_user ([fada8ad](https://gitlab.com/fi-sas/fisas-current-account/commit/fada8ada85845b81326c2456353a538d6351967f))
* **middleware:**  create alert helpdesk/cc when error ([d4091aa](https://gitlab.com/fi-sas/fisas-current-account/commit/d4091aa0c069a2090931833b1029f03775ad1c1e))

## [1.17.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.5...v1.17.6) (2021-08-10)


### Bug Fixes

* **payment:** device check at confirm from paygate ([943dff2](https://gitlab.com/fi-sas/fisas-current-account/commit/943dff26601141d4af4e9a7fb33911316b8bd4ca))

## [1.17.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.4...v1.17.5) (2021-08-06)


### Bug Fixes

* **cashAccount:** missing cashAccount id on charge confirm ([fc5a8c3](https://gitlab.com/fi-sas/fisas-current-account/commit/fc5a8c357caed3da9a1703dc6c671e581048cdbb))

## [1.17.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.3...v1.17.4) (2021-08-06)


### Bug Fixes

* **middleware_erp:** change to SKKIPED status docs not configured to send ([30b264f](https://gitlab.com/fi-sas/fisas-current-account/commit/30b264fa1c713eb69d207736b1423eabf1bade38))
* **payment_method:** add payment_method name to charge and credit_note ([4a11c21](https://gitlab.com/fi-sas/fisas-current-account/commit/4a11c211aefba2f76c4cdec93f3cb0706a9ce9b4))

## [1.17.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.2...v1.17.3) (2021-08-05)


### Bug Fixes

* **cashAccount:** fix wrong summaryTotal ([ecd9646](https://gitlab.com/fi-sas/fisas-current-account/commit/ecd96463b4cd4b797033bca9251d3f07f4232394))

## [1.17.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.1...v1.17.2) (2021-08-05)


### Bug Fixes

* **middleware_erp:** fix remove await ([233cf00](https://gitlab.com/fi-sas/fisas-current-account/commit/233cf0076ba6e2a3915aca3519fd37dcbdedc6a9))

## [1.17.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.17.0...v1.17.1) (2021-08-05)


### Bug Fixes

* **cc:** account wrong middleware_erp_company_id ([8642597](https://gitlab.com/fi-sas/fisas-current-account/commit/864259745520b79c934e09311166f569a9931811))

# [1.17.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.16.2...v1.17.0) (2021-08-04)


### Features

* **erp:** integration with erp middleware ([131b900](https://gitlab.com/fi-sas/fisas-current-account/commit/131b90074b39d4a35fec7a4329aef0e19a7e1ab7))

## [1.16.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.16.1...v1.16.2) (2021-08-04)


### Bug Fixes

* **movements:** add ddevice id from checkouts ([d4a2e43](https://gitlab.com/fi-sas/fisas-current-account/commit/d4a2e435633aa2cef73fa4337697838d0f315f5f))

## [1.16.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.16.0...v1.16.1) (2021-08-03)


### Bug Fixes

* **movements:** immediate charge ([cd9b5ed](https://gitlab.com/fi-sas/fisas-current-account/commit/cd9b5ed9e57fa07d6fa27ebe62441930ace230d8))

# [1.16.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.15.3...v1.16.0) (2021-07-30)


### Features

* add cancel to TPA callback ([5d83978](https://gitlab.com/fi-sas/fisas-current-account/commit/5d8397801a2f344f106bff859fd2b87a448d29a2))

## [1.15.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.15.2...v1.15.3) (2021-07-30)

### Bug Fixes

- **movements:** remove min: 1 from actions ([8110e2d](https://gitlab.com/fi-sas/fisas-current-account/commit/8110e2d3dd91a11d8529ebdd77aabc5d5b62e56f))

## [1.15.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.15.1...v1.15.2) (2021-07-30)

### Bug Fixes

- **movements:** add validation on charge account ([1322742](https://gitlab.com/fi-sas/fisas-current-account/commit/1322742707ed0c20f9f720de221324a19ed803ca))

## [1.15.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.15.0...v1.15.1) (2021-07-30)

### Bug Fixes

- **movements:** accepts charge in cents ([225b2fe](https://gitlab.com/fi-sas/fisas-current-account/commit/225b2fe0a4dfe9e32c7d743f5d8f04b3348855c0))

# [1.15.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.14.0...v1.15.0) (2021-07-30)

### Features

- add TPA callback, and fix scopes of payments ([807568d](https://gitlab.com/fi-sas/fisas-current-account/commit/807568d3a9d04b1e5eaa658f0af151198402f090))

# [1.14.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.13.0...v1.14.0) (2021-07-28)

### Bug Fixes

- **charge:** charge account is imediated from payment method ([6c2a07b](https://gitlab.com/fi-sas/fisas-current-account/commit/6c2a07bc1c491f3ba289594199924079ffff3a40))
- **payments:** add Kiosk CPUID to confirm payments ([37593db](https://gitlab.com/fi-sas/fisas-current-account/commit/37593dbecd1a020d1a67037175ba0eea0f4a9487))

### Features

- **tpa:** inital confirm version ([d561e5a](https://gitlab.com/fi-sas/fisas-current-account/commit/d561e5a95cbd9a3190fa7adc087980945ce7eb59))
- initial kiosk tpa confirm logic ([d6c4cd5](https://gitlab.com/fi-sas/fisas-current-account/commit/d6c4cd52b640c68c596d825496c5032575a3b767))

# [1.13.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.12.1...v1.13.0) (2021-07-22)

### Features

- **ama:** add cron checkTransactions PayGate ([5569370](https://gitlab.com/fi-sas/fisas-current-account/commit/556937018bf4b53a53005def423aa06171ee8ac8))
- **cron:** check paygate transactions ([f44e2b0](https://gitlab.com/fi-sas/fisas-current-account/commit/f44e2b0eaf00c52b26758de73ed1115ad3dc372f))

## [1.12.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.12.0...v1.12.1) (2021-07-22)

### Bug Fixes

- **cancel:** multiple or all item cancel ([d4e6326](https://gitlab.com/fi-sas/fisas-current-account/commit/d4e6326eb934ec6eda73dd35b3ea63b9deb8e812))

# [1.12.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.11.0...v1.12.0) (2021-07-19)

### Features

- **cash_account:** add totalSummary to cashAccountClose action ([6844971](https://gitlab.com/fi-sas/fisas-current-account/commit/6844971b31c33a8aa92cbe98dc351ecdd083ef80))

# [1.11.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.6...v1.11.0) (2021-07-15)

### Features

- **payment-method:** add payment-method TPA ([06dceda](https://gitlab.com/fi-sas/fisas-current-account/commit/06dcedadb20c5acfbaae3b71e77128a7fd4c954a))

## [1.10.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.5...v1.10.6) (2021-07-12)

### Bug Fixes

- **apyment:** check not partial payment at charge ([2f2e23c](https://gitlab.com/fi-sas/fisas-current-account/commit/2f2e23c791db4155e0f1c4f0f2d252528378492c))

## [1.10.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.4...v1.10.5) (2021-07-12)

### Bug Fixes

- **payment:** confirm check partial ([c84ad69](https://gitlab.com/fi-sas/fisas-current-account/commit/c84ad692cc5854dedfb0695fc036ecddf0540820))

## [1.10.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.3...v1.10.4) (2021-07-12)

### Bug Fixes

- **confirm:** add param payment_id ([6bb7186](https://gitlab.com/fi-sas/fisas-current-account/commit/6bb71861c14a7a95e66c3107d842153a5e3ff094))

## [1.10.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.2...v1.10.3) (2021-07-12)

### Bug Fixes

- **payment:** canPay wrong param movment_id ([ff20bce](https://gitlab.com/fi-sas/fisas-current-account/commit/ff20bcec21c37ece874d466bc1c9c5b33143c18a))

## [1.10.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.1...v1.10.2) (2021-07-07)

### Bug Fixes

- **charge:** add validation by min/max allowed amount ([4f5137e](https://gitlab.com/fi-sas/fisas-current-account/commit/4f5137e636435343405381fbd58bd997c01ff545))

## [1.10.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.10.0...v1.10.1) (2021-07-06)

### Bug Fixes

- add withRelated to payment_method ([f0c7005](https://gitlab.com/fi-sas/fisas-current-account/commit/f0c700509c1ea9e9968e152879194ad1faf2c8bc))
- **payment_method:** move to self table payment_method_settings ([477c224](https://gitlab.com/fi-sas/fisas-current-account/commit/477c22493c2d6b5e8c58d0604f6bb8a77ce99c6e))

# [1.10.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.9.3...v1.10.0) (2021-07-05)

### Features

- add min/max charge amount by payment_method ([4e28c78](https://gitlab.com/fi-sas/fisas-current-account/commit/4e28c78a0ccd664d1185d58280fc54e328439b23))

## [1.9.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.9.2...v1.9.3) (2021-06-22)

### Bug Fixes

- partial payment ([ccf8038](https://gitlab.com/fi-sas/fisas-current-account/commit/ccf8038ed98ec3043522f4c85e3add49af2dd511))

## [1.9.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.9.1...v1.9.2) (2021-06-01)

### Bug Fixes

- **cach_account:** set report fields to fixed params ([f7587a4](https://gitlab.com/fi-sas/fisas-current-account/commit/f7587a45922307f8df5d912d38355381b73d9f4e))

## [1.9.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.9.0...v1.9.1) (2021-06-01)

### Bug Fixes

- **cach_account:** fix template name ([0336f87](https://gitlab.com/fi-sas/fisas-current-account/commit/0336f877f59361aa7c8a483eda8f8a4faf9f66f3))

# [1.9.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.8.1...v1.9.0) (2021-06-01)

### Features

- **cash_account:** report endpoint ([06c98a3](https://gitlab.com/fi-sas/fisas-current-account/commit/06c98a355f37205d2672861b55aa328ce371ae3c))

## [1.8.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.8.0...v1.8.1) (2021-06-01)

### Bug Fixes

- **cash_account_close:** change find by list ([56100f7](https://gitlab.com/fi-sas/fisas-current-account/commit/56100f7c255b1bffa61af1454ff8edb74e61bbbd))

# [1.8.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.7.4...v1.8.0) (2021-05-31)

### Bug Fixes

- add hasOne fields criteria ([9c414f0](https://gitlab.com/fi-sas/fisas-current-account/commit/9c414f084223bc5702e84c26ca84b6e6a7cf202d))

### Features

- **cachAccount:** cashAccount close by logon user ([3d27d74](https://gitlab.com/fi-sas/fisas-current-account/commit/3d27d7489b18ed3e947b6a51cbf53e7d91e3f002))

## [1.7.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.7.3...v1.7.4) (2021-05-31)

### Bug Fixes

- add mandatory description to new paygate AMA ([53046eb](https://gitlab.com/fi-sas/fisas-current-account/commit/53046eb0bc8bfb776f98e4424023ab832374b2d0))
- **seeds:** change to the new AMA paygate ([47d398a](https://gitlab.com/fi-sas/fisas-current-account/commit/47d398a18fe5be6f404e266346d029bab7b08fd4))

## [1.7.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.7.2...v1.7.3) (2021-05-31)

### Bug Fixes

- add cash_account_id to unchargeAccount ([d5b9775](https://gitlab.com/fi-sas/fisas-current-account/commit/d5b9775be6d17a056327e4b94a123653c810c2ac))
- disabled unused megaepayment checkTransactions cron ([586ac86](https://gitlab.com/fi-sas/fisas-current-account/commit/586ac866391bc8dbc08247c2de75062058dedea7))

## [1.7.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.7.1...v1.7.2) (2021-05-25)

### Bug Fixes

- **cart-items:** add decimal precision to cart items ([62afa43](https://gitlab.com/fi-sas/fisas-current-account/commit/62afa4341ee854a4544d7af6ed1350c048d8c981))

## [1.7.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.7.0...v1.7.1) (2021-05-25)

### Bug Fixes

- **movements:** cancel movment action ([9fc0ab4](https://gitlab.com/fi-sas/fisas-current-account/commit/9fc0ab413fbd2fa76ac0b11896bcfdafa02bb82c))

# [1.7.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.6.2...v1.7.0) (2021-05-24)

### Bug Fixes

- **movements:** items type ([86480a1](https://gitlab.com/fi-sas/fisas-current-account/commit/86480a1db8953de1913815b72f78aa8957be77ee))
- **payment_methods:** "numerario" to "numerário" ([27e7196](https://gitlab.com/fi-sas/fisas-current-account/commit/27e7196e89e6b1b35a07c9efd7fbe90c32bcff27))

### Features

- **movements:** imlement inital logic for partial cancellement ([3e14250](https://gitlab.com/fi-sas/fisas-current-account/commit/3e1425060df8fcaaa57aea81ad1c425241fb9fb5))

## [1.6.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.6.1...v1.6.2) (2021-05-19)

### Bug Fixes

- **ama_gateway:** name of gateway ([4290860](https://gitlab.com/fi-sas/fisas-current-account/commit/4290860a95ca4a11d5ee1913885286e2cfe83174))

## [1.6.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.6.0...v1.6.1) (2021-05-19)

### Bug Fixes

- **movements:** change charge scope ([c720b7c](https://gitlab.com/fi-sas/fisas-current-account/commit/c720b7c17486704444e345135b608780e1b789db))

# [1.6.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.5...v1.6.0) (2021-05-19)

### Features

- **ama:** add paygate ([7324bc6](https://gitlab.com/fi-sas/fisas-current-account/commit/7324bc64098147426b603ecabdac4a1cc734c3fa))

## [1.5.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.4...v1.5.5) (2021-05-18)

### Bug Fixes

- **cart:** support add cart items to user from POS and BO ([46784d5](https://gitlab.com/fi-sas/fisas-current-account/commit/46784d58051d31cf7773ff1aa5d74f48220b4d3f))

## [1.5.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.3...v1.5.4) (2021-05-18)

### Bug Fixes

- **cart:** change error type on quantity exceeded ([6bc3882](https://gitlab.com/fi-sas/fisas-current-account/commit/6bc3882719033eee4fe0b7ba508a2d9f00e0e638))

## [1.5.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.2...v1.5.3) (2021-05-17)

### Bug Fixes

- **movements:** remove email from created_by ([7b6776e](https://gitlab.com/fi-sas/fisas-current-account/commit/7b6776e39cc49f2be7251a27aeaf02e946ee0f83))

## [1.5.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.1...v1.5.2) (2021-05-15)

### Bug Fixes

- **movement:** related created_by as object ([a88e0c9](https://gitlab.com/fi-sas/fisas-current-account/commit/a88e0c9e5ec9c6cafeb41ca04e85905f1050f250))
- **movement:** remove debug logs ([590c702](https://gitlab.com/fi-sas/fisas-current-account/commit/590c7022a8c19e0d95c9cfba26f0bd65588f67b3))

## [1.5.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.5.0...v1.5.1) (2021-05-14)

### Bug Fixes

- **movement:** add createdBy withRelated ([c932146](https://gitlab.com/fi-sas/fisas-current-account/commit/c93214628ed9584da02348d6868d4a3023e9c345))

# [1.5.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.10...v1.5.0) (2021-05-14)

### Bug Fixes

- **cash_account:** add cratedBy and iban ([c079f26](https://gitlab.com/fi-sas/fisas-current-account/commit/c079f26e71441eb32a9f73870742b26f0ab8d61e))
- **cash_account:** empty defaultWithRelateds users ([ddebf4a](https://gitlab.com/fi-sas/fisas-current-account/commit/ddebf4aa9a71adfe1b5280f3c4c7aef9ee01cae9))
- **gateway_ama:** set cron to hourly check refs ([ce004a3](https://gitlab.com/fi-sas/fisas-current-account/commit/ce004a348768c5b24d3e70c8a0c3020810635cc9))
- **movements:** partial cancel ([399c59c](https://gitlab.com/fi-sas/fisas-current-account/commit/399c59c25f7070e00dc22935d16438197f0dee0c))
- **payment:** debug info ([a44a098](https://gitlab.com/fi-sas/fisas-current-account/commit/a44a09820ae7712627cd9ed3628cbb132c62f8fa))
- **sample_seeds:** prevent delete movements ([23a63c0](https://gitlab.com/fi-sas/fisas-current-account/commit/23a63c089fd5a198842c559c254ffb83e7e00e6f))

### Features

- **cart:** allow checkout just one item ([8146f9c](https://gitlab.com/fi-sas/fisas-current-account/commit/8146f9cc5d1ab4832e574d2a949ef25e57748a02))
- **cart:** checkout just on item ([54ca56e](https://gitlab.com/fi-sas/fisas-current-account/commit/54ca56ebdc14dc707eae68c631a070b023a341ea))

## [1.4.10](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.9...v1.4.10) (2021-05-06)

### Bug Fixes

- **cart:** additem create cart for user_id ([52d16f9](https://gitlab.com/fi-sas/fisas-current-account/commit/52d16f957c2a545e50263f91d7ee9efdb87273f2))

## [1.4.9](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.8...v1.4.9) (2021-05-06)

### Bug Fixes

- **cart:** add cart POS and BO user_id ([2bdad55](https://gitlab.com/fi-sas/fisas-current-account/commit/2bdad55f2a56d535315bc1b8f4f6666bbd21f1c2))

## [1.4.8](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.7...v1.4.8) (2021-05-06)

### Bug Fixes

- **checkout:** support checkout by user_id ([ece202a](https://gitlab.com/fi-sas/fisas-current-account/commit/ece202a7d615dbcc3ddfcd29929cfe87f90b5b43))

## [1.4.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.6...v1.4.7) (2021-05-05)

### Bug Fixes

- **cart:** userId on Add cart item ([f6ea1f1](https://gitlab.com/fi-sas/fisas-current-account/commit/f6ea1f19b96a7798d0510e19438583fc2dda2ade))
- **movements:** add balances POS validation when user_id set ([81e2784](https://gitlab.com/fi-sas/fisas-current-account/commit/81e27841614874fa0dcce67b2fb655c97c7341a7))

## [1.4.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.5...v1.4.6) (2021-05-05)

### Bug Fixes

- **cart:** add item for custom user ([e94cf1f](https://gitlab.com/fi-sas/fisas-current-account/commit/e94cf1f4c91a7974ef87ddb1a0d79fce4466efa3))

## [1.4.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.4...v1.4.5) (2021-04-27)

### Bug Fixes

- **accounts:** clear cache on updatePaymentMethods ([8aeb3cb](https://gitlab.com/fi-sas/fisas-current-account/commit/8aeb3cba2ed06df64d80e0845b4d2ee665a65888))

## [1.4.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.3...v1.4.4) (2021-04-14)

### Bug Fixes

- **cart:** clear cart before return response checkout ([16490a5](https://gitlab.com/fi-sas/fisas-current-account/commit/16490a56c0bbc8dbea103dbdb9c53e3f52d667db))

## [1.4.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.2...v1.4.3) (2021-04-14)

### Bug Fixes

- **cart:** add default max_quantity to 999 ([4b29286](https://gitlab.com/fi-sas/fisas-current-account/commit/4b29286a5cda049ef96bd3aaaec1ca5dbae804a9))

## [1.4.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.1...v1.4.2) (2021-04-14)

### Bug Fixes

- **cart:** remove cart if empty on remove item ([3ea8184](https://gitlab.com/fi-sas/fisas-current-account/commit/3ea8184279e365090d863ff28dd47965a8bd8d95))

## [1.4.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.4.0...v1.4.1) (2021-04-13)

### Bug Fixes

- **cart:** add max_quantity field to cart item ([a5e0863](https://gitlab.com/fi-sas/fisas-current-account/commit/a5e0863e49e56369608f6ac973ff2da3f8555b8d))

# [1.4.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.3.0...v1.4.0) (2021-04-07)

### Bug Fixes

- **payment:** remove loggers ([a3bb592](https://gitlab.com/fi-sas/fisas-current-account/commit/a3bb592d280015dc161ea6e05ca0a4bcb3aec0be))

### Features

- **cash_account:** availble cash_account from user ([f6d2f71](https://gitlab.com/fi-sas/fisas-current-account/commit/f6d2f71f4753b2bc3b449d0db1447934b9800f19))

# [1.3.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.2.3...v1.3.0) (2021-04-07)

### Bug Fixes

- **account:** fix action put path ([05fa8f4](https://gitlab.com/fi-sas/fisas-current-account/commit/05fa8f429735ed8eb352970dbb104817b15e7864))

### Features

- **cash_account:** add cash_account ([d6099e8](https://gitlab.com/fi-sas/fisas-current-account/commit/d6099e8abf779a430ae97f1584c974ba185f7e0a))

## [1.2.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.2.2...v1.2.3) (2021-04-06)

### Bug Fixes

- **example:** fix example debugger_port ([1ba7933](https://gitlab.com/fi-sas/fisas-current-account/commit/1ba7933fc1f78bcf6cb03e5be0130f346c5b59d0))
- **movements:** add partially_paid enum ([cef67c2](https://gitlab.com/fi-sas/fisas-current-account/commit/cef67c2d84be4fa46384141a54eff8fc1d70b19a))

## [1.2.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.2.1...v1.2.2) (2021-03-24)

### Bug Fixes

- **movements:** update paid_value ([aaaf1ad](https://gitlab.com/fi-sas/fisas-current-account/commit/aaaf1ad544cebefd9ce4893f8e2ea3c3354ad284))

## [1.2.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.2.0...v1.2.1) (2021-03-24)

### Bug Fixes

- **document_type:** add patch ([196aa7a](https://gitlab.com/fi-sas/fisas-current-account/commit/196aa7a3d78d5eec4743820509af87c72c2be2b1))

# [1.2.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.1.2...v1.2.0) (2021-03-24)

### Features

- **movement:** partial payments ([a53bfe0](https://gitlab.com/fi-sas/fisas-current-account/commit/a53bfe0e23856d59398c6f37bd5b54aaf860d65c))

## [1.1.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.1.1...v1.1.2) (2021-03-24)

### Bug Fixes

- **movements:** document type acronym ([bee0851](https://gitlab.com/fi-sas/fisas-current-account/commit/bee0851a2a69eadc464fd27db0048768010d10d6))

## [1.1.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.1.0...v1.1.1) (2021-03-23)

### Bug Fixes

- **confirm:** gateway payments error ([f5febd0](https://gitlab.com/fi-sas/fisas-current-account/commit/f5febd0a954bfb08ac60919bb720f3b52a941765))
- **payment:** remove loggers ([abc4b5f](https://gitlab.com/fi-sas/fisas-current-account/commit/abc4b5f6d8b29280a736c47f85b840a86cea4266))

# [1.1.0](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.1...v1.1.0) (2021-03-23)

### Features

- **movement:** cancel not immediate charges ([046a19b](https://gitlab.com/fi-sas/fisas-current-account/commit/046a19b1509d99a777e76aa35b07d69c3169dd9c))
- **movements:** add document types ([3a72f5e](https://gitlab.com/fi-sas/fisas-current-account/commit/3a72f5edf6272c6e08ee1c76780080f25f41ead2))

## [1.0.1](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0...v1.0.1) (2021-03-09)

### Bug Fixes

- **movement:** cancel movement ([380e14a](https://gitlab.com/fi-sas/fisas-current-account/commit/380e14a6284cc2d4c5292d8b0b03ea0bbd2c9911))

# 1.0.0 (2021-03-09)

### Bug Fixes

- **account:** missing key 'id' wrong balances ([33a7545](https://gitlab.com/fi-sas/fisas-current-account/commit/33a7545a7909e4389dc130b158b2389c6df6c606))
- **balance:** cast float missing on funds calc ([3d2dd3b](https://gitlab.com/fi-sas/fisas-current-account/commit/3d2dd3bd3444de806802fdee02026d4f4942abfd))
- **balance:** check account funds and plafond ([12b387c](https://gitlab.com/fi-sas/fisas-current-account/commit/12b387c60b430f9c7c35c6ec3cb6299377bdb9ff))
- **cart:** add DEFAULT_PAYMENT_METHOD_ID ([841645a](https://gitlab.com/fi-sas/fisas-current-account/commit/841645af2615ecb4ffb54b763425178a56cf1353))
- **debug:** remove debug flags for MS AMA ([0a693dd](https://gitlab.com/fi-sas/fisas-current-account/commit/0a693dd13e0816ec8e51365a933d8adfd9b3932b))
- **defaults:** add optionals to times ([7289c88](https://gitlab.com/fi-sas/fisas-current-account/commit/7289c880ee33f0e9dec093fdb8b466cf3023094f))
- **env:** add env.local DEFAULT_PAYMENT_METHOD_ID ([dbf6133](https://gitlab.com/fi-sas/fisas-current-account/commit/dbf6133b603a3df292405528bc1ead37d01712f0))
- **migration:** document.number to string ([a72f975](https://gitlab.com/fi-sas/fisas-current-account/commit/a72f9758734c3fa9b12a917a4c0297d2439f99b2))
- **migration:** merge payment and movements ([6cabf01](https://gitlab.com/fi-sas/fisas-current-account/commit/6cabf01f2de3e9b847f1931fc4229a75e6777789))
- **migration:** timestamps fields and defaults ([6d0ee4f](https://gitlab.com/fi-sas/fisas-current-account/commit/6d0ee4fe15691d1c345282417acb77426d84b90e))
- **migrations:** rename files to conform knex ([7401240](https://gitlab.com/fi-sas/fisas-current-account/commit/7401240750806a1c34bb184b3bf986be2ac82e8b))
- **movement:** add operation CREDIT_NOTE enum ([bba46a3](https://gitlab.com/fi-sas/fisas-current-account/commit/bba46a344c7be45736c511d9a32a96677b67dbc5))
- **movement:** add payment_id ([317cb5a](https://gitlab.com/fi-sas/fisas-current-account/commit/317cb5a1f829a219e489485204605c0434b95fdb))
- **movement:** balance filters ([3f6b815](https://gitlab.com/fi-sas/fisas-current-account/commit/3f6b8154cce5ce0563dc9b986ab19ff27124a3e2))
- **movement:** status CONFIRMED when CHARGE ([bd38f11](https://gitlab.com/fi-sas/fisas-current-account/commit/bd38f119ade209846bd398303c7f78977f3800c2))
- **movement:** transaction_id not affected ([f2e2449](https://gitlab.com/fi-sas/fisas-current-account/commit/f2e2449ba2321f48c47cc718263917b037b484fa))
- **movement_items:** add serrvice withRelated ([0b4f49b](https://gitlab.com/fi-sas/fisas-current-account/commit/0b4f49b11362b7a4f905542d83dc41817616d4fe))
- **movements:** add default withRelateds ([d45aff0](https://gitlab.com/fi-sas/fisas-current-account/commit/d45aff03623c251393a443801d27541e1a9ee643))
- **movements:** add operation cancel_lend ([6fddf94](https://gitlab.com/fi-sas/fisas-current-account/commit/6fddf945421f65d680b92a035bff113c139f2308))
- **movements:** add PAID status ([d2ae699](https://gitlab.com/fi-sas/fisas-current-account/commit/d2ae6997383abca023ca1d7b478960b00f1c3bc7))
- **movements:** bypass bo valdation when from cart ([efcaa89](https://gitlab.com/fi-sas/fisas-current-account/commit/efcaa896e1b0dcf9b3c66e19e523acbae626870a))
- **movements:** cancel, credit_note ([ba102fd](https://gitlab.com/fi-sas/fisas-current-account/commit/ba102fd10209122e6910ce775b4886b5953b4a77))
- **movements:** change withRelated payment path ([c5d549c](https://gitlab.com/fi-sas/fisas-current-account/commit/c5d549c73f3e053a8d7023090c9c9420fdfa224e))
- **movements:** field name transaction_receipt ([97205fd](https://gitlab.com/fi-sas/fisas-current-account/commit/97205fdb930bcd07dc6f69736beeccd2e9ebb26b))
- **movements:** fix charge description ([ca7724c](https://gitlab.com/fi-sas/fisas-current-account/commit/ca7724cb31a093b19d2f3341b01b6fb9b2d8eb78))
- **movements:** fix notifyServices ([26fc751](https://gitlab.com/fi-sas/fisas-current-account/commit/26fc7512644b4fb840c135628444b960c011bc97))
- **movements:** fix payment method CC afects balance ([6b41e4f](https://gitlab.com/fi-sas/fisas-current-account/commit/6b41e4fe36535a5324b3c06c40c4dd17390fa98c))
- **movements:** liquid_unit_value and unit_value ([81bbf8a](https://gitlab.com/fi-sas/fisas-current-account/commit/81bbf8a379ded193fb98778e04e00c39a040b608))
- **movements:** operation filter criteria ([6c72a24](https://gitlab.com/fi-sas/fisas-current-account/commit/6c72a246cefd314bb28a60f63799e13876ce059b))
- **movements:** preview type POS ([ba099c4](https://gitlab.com/fi-sas/fisas-current-account/commit/ba099c4de834037cdeee3e0b5cd361cc8404f5c9))
- **movements:** set cancel item optional ([297d5a8](https://gitlab.com/fi-sas/fisas-current-account/commit/297d5a89eab4d01ac91e392a42d277fc35c418a4))
- **movemvents:** charges update status ([57c3cb2](https://gitlab.com/fi-sas/fisas-current-account/commit/57c3cb2bd83e74b0559d9075f6a5ecf3ce5fd7ad))
- **payment:** add device POS check ([b2aac16](https://gitlab.com/fi-sas/fisas-current-account/commit/b2aac161f7522476a72ad61ff14b0938c9c3a44d))
- **payment:** canPay creates wrong movemvent ([8d3753a](https://gitlab.com/fi-sas/fisas-current-account/commit/8d3753a1db5de6d6bed9a2285aea28bf43fd48d6))
- **payment:** check for AMA service availability ([bf5a25a](https://gitlab.com/fi-sas/fisas-current-account/commit/bf5a25ad5c6d79c670893aa3c50f3190f8bd808b))
- **payment:** define DEFAULT_PAYMENT_METHOD_ID ([c814a67](https://gitlab.com/fi-sas/fisas-current-account/commit/c814a677dc0dc308db94ab5dbdab344dfcdb59fd))
- **payments:** change find visibility ([269e002](https://gitlab.com/fi-sas/fisas-current-account/commit/269e002e9689eecf9d3e0bb8cf0d070e4feaa193))
- **payments:** missing get action ([16e947b](https://gitlab.com/fi-sas/fisas-current-account/commit/16e947b8cbe3a192822166ef4e65b144f38aae34))
- **refmb:** add refmb data into mov-item ([ff9611b](https://gitlab.com/fi-sas/fisas-current-account/commit/ff9611b6f178a7d61f3479cf52939ec4f5c92821))
- **seed:** check if payment_method already exists ([e702f1d](https://gitlab.com/fi-sas/fisas-current-account/commit/e702f1d134884909d3222f2650080093b3ed1c02))
- **seed:** minor fix ([491f4d2](https://gitlab.com/fi-sas/fisas-current-account/commit/491f4d2c3f20653dfe0985b2e3617262ab272101))
- **seed:** payment_method missing insert ([535287d](https://gitlab.com/fi-sas/fisas-current-account/commit/535287deb8f69b01a409d4933a2b4fee41e16175))
- **seeds:** accounts and movements ([fbe2865](https://gitlab.com/fi-sas/fisas-current-account/commit/fbe2865a36650b8ff93d6c2ae5fe7843b7f07c86))
- **seeds:** document url field replace file_id ([0306a44](https://gitlab.com/fi-sas/fisas-current-account/commit/0306a44119fa7a5df88f4c436fd277d1fc8c4a3a))
- **seeds:** minor fixes movements and items ([626f546](https://gitlab.com/fi-sas/fisas-current-account/commit/626f546b72a825e1d5b05c079115458a929a4f1a))
- **seeds:** movements change user to ALUNO ([8caf6a7](https://gitlab.com/fi-sas/fisas-current-account/commit/8caf6a777b0a4636a65b2e82a23de40a604c5e33))
- **seeds:** set device_id ([1bc6377](https://gitlab.com/fi-sas/fisas-current-account/commit/1bc6377c5f91de3a96ad865578637be49ca9647b))
- add improvements to movement creation ([8f91751](https://gitlab.com/fi-sas/fisas-current-account/commit/8f91751e2779c7c2e0d8f2c731b893d7b101acbf))
- minor fix's ([df2cf44](https://gitlab.com/fi-sas/fisas-current-account/commit/df2cf447372950d9fb461d90619f86238e8794c3))
- movements minor fixes ([c9dbec9](https://gitlab.com/fi-sas/fisas-current-account/commit/c9dbec9d98ecddb0c93adeb81568a36b35a908ca))
- remove erp data ([0dfcd67](https://gitlab.com/fi-sas/fisas-current-account/commit/0dfcd671e7e6ba7e16228586395b7765b222336c))
- remove unused fields and change names ([d65e4aa](https://gitlab.com/fi-sas/fisas-current-account/commit/d65e4aa4be868fa0b99fd06b3fc46267f38205e7))
- scope names of payments ([e57a1a8](https://gitlab.com/fi-sas/fisas-current-account/commit/e57a1a83eeb1459ef709feb6a276cb45b61f8738))
- **account:** fix enum document_types ([eac80fb](https://gitlab.com/fi-sas/fisas-current-account/commit/eac80fba234d94455afbd72b3c8f83077dba6185))
- **docker-compose:** create docker-compose to prod and dev ([b8fe9b7](https://gitlab.com/fi-sas/fisas-current-account/commit/b8fe9b769e46b54669ef28b1672cc0b23aa88954))
- **eslint:** add missing rules ([5ae421c](https://gitlab.com/fi-sas/fisas-current-account/commit/5ae421cc3ac3f014935880afbe71ffba44ed3f6d))
- **git:** update gitignore ([54ba372](https://gitlab.com/fi-sas/fisas-current-account/commit/54ba372e454ab31d95e7b7d2c953d133dcfc4eb8))
- **gitignore:** add idea and vscode ([7c349b5](https://gitlab.com/fi-sas/fisas-current-account/commit/7c349b5059cfecd037884e330490c20ea993d8f2))
- **gitignore:** remove .env file ([f472c7d](https://gitlab.com/fi-sas/fisas-current-account/commit/f472c7d8a1cc19a3edca33ed1d98d05d08966033))
- **items:** add service data ([6485d16](https://gitlab.com/fi-sas/fisas-current-account/commit/6485d16df51f972879dade30f648f6712f42d0a8))
- **items:** fix codes import ([57cfd55](https://gitlab.com/fi-sas/fisas-current-account/commit/57cfd557882b4206f1e35efa01f0faa5b3f0405d))
- **migrations:** movement precision numbers ([14b916c](https://gitlab.com/fi-sas/fisas-current-account/commit/14b916cb047e347fc3400a26ab644d4df7ff3f8d))
- **movement:** add cache keys to list action ([53b0492](https://gitlab.com/fi-sas/fisas-current-account/commit/53b0492e31b01fc22c5ccb366c71d0c0f9ed4dd3))
- **movement:** add fields liquid_unit_value ([17cb379](https://gitlab.com/fi-sas/fisas-current-account/commit/17cb379f95a294d7fefcfaf5a39a960ff0ba53f1))
- **movement:** add uuiv4 to movement and items ([f2b4bf7](https://gitlab.com/fi-sas/fisas-current-account/commit/f2b4bf701befaf39b50c8bb88a6dcb691acbf420))
- **movement:** change enum MOVEMENT_OPERATION ([b4dd3d8](https://gitlab.com/fi-sas/fisas-current-account/commit/b4dd3d88ce75707ae50caf2f7cf2969e3a4491e2))
- **movement:** change seeds uuid fields ([3589ca4](https://gitlab.com/fi-sas/fisas-current-account/commit/3589ca463577066214bc1c03f86c4567bc27c374))
- **movements:** add fields ([dae3a9c](https://gitlab.com/fi-sas/fisas-current-account/commit/dae3a9c9dae78e388901fa40aa4635b6b89929e4))
- **movements:** add file in documents ([cf7b8af](https://gitlab.com/fi-sas/fisas-current-account/commit/cf7b8afd6b64ebe7635130ae810067d64ada1b00))
- **movements:** add location to Adiamento ([1998299](https://gitlab.com/fi-sas/fisas-current-account/commit/19982998f40241f2c60b2be758650223d9ec1aae))
- **movements:** add location to adiantamento ([1208485](https://gitlab.com/fi-sas/fisas-current-account/commit/1208485c1155608122f126e54e5767f7881f65da))
- **movements:** add withRelated ([7f134df](https://gitlab.com/fi-sas/fisas-current-account/commit/7f134df2f9bcb4dbbd31424b7aa3d731199a192c))
- **movements:** annulament of advance location ([78d59b3](https://gitlab.com/fi-sas/fisas-current-account/commit/78d59b3f8b359ea4612a293b4c020d543c2ec3ff))
- **movements:** business rules ([8a0bdbc](https://gitlab.com/fi-sas/fisas-current-account/commit/8a0bdbcb0b66fcf524dc9f8afb57b4a28c7bfe2d))
- **movements:** business rules create ([67c0574](https://gitlab.com/fi-sas/fisas-current-account/commit/67c0574ff555e095de6fb27fb135cbb30ff416b1))
- **movements:** fix debuct advances error on multiple advances ([c5ef3ea](https://gitlab.com/fi-sas/fisas-current-account/commit/c5ef3eadb0d5583f5b67f93cbf189204b9ef34a4))
- **movements:** fix movements by id ([70535fc](https://gitlab.com/fi-sas/fisas-current-account/commit/70535fce2b1ba2f59c2ccebd05de551afe98b7e4))
- **movements:** fix typo on debuctDebt ([e98b40e](https://gitlab.com/fi-sas/fisas-current-account/commit/e98b40e4633c260eec684603f057c96b1f798199))
- **movements:** items relation ([9167cd8](https://gitlab.com/fi-sas/fisas-current-account/commit/9167cd89c9ca04e5ba8985d26b027a45009b4372))
- **movements:** minor fix ([56416b6](https://gitlab.com/fi-sas/fisas-current-account/commit/56416b653ec84615546709b883cf6fea2e985c22))
- **movements:** public_document_types enum ([00553db](https://gitlab.com/fi-sas/fisas-current-account/commit/00553db83fc6ac8fbad557c260bc8e8101f63077))
- **movements:** update business rules ([df9c7e7](https://gitlab.com/fi-sas/fisas-current-account/commit/df9c7e76aae7c723a4bef7833753170e2b4c9556))
- **payment:** change filds names ([44674fb](https://gitlab.com/fi-sas/fisas-current-account/commit/44674fb9b06dd4aa9dd12a0008ab81ba51f02f66))
- **payment:** enable action create ([c01c21a](https://gitlab.com/fi-sas/fisas-current-account/commit/c01c21a7f3eb7077bfefa9bff8fefa83ad1eb056))
- **payments:** new database fields ([38921b9](https://gitlab.com/fi-sas/fisas-current-account/commit/38921b9f0085eef6f0227a2c758ad649a6359415))
- **payments:** rearrange the canPay to return the MBRef Data ([b8a15e1](https://gitlab.com/fi-sas/fisas-current-account/commit/b8a15e12e53814c2aa3bb2899eeb2ba96d3cf0f1))
- allowing empty description @ moviment schema ([a728dea](https://gitlab.com/fi-sas/fisas-current-account/commit/a728deaab74edeef608c4b2a0f2d99109e7e40ef))
- allowing empty/null description @ movement item schema ([7341a92](https://gitlab.com/fi-sas/fisas-current-account/commit/7341a92bf320a96eab9b8de39392c8e4ec6cf5cf))
- changed service URI to be compatible with backoffice ([346bee5](https://gitlab.com/fi-sas/fisas-current-account/commit/346bee509b3ebe1da4497e4215f4e203c547704b))
- minor code fix ([06d6d94](https://gitlab.com/fi-sas/fisas-current-account/commit/06d6d948dd2fff92b254863bb1309e91979f5548))
- movement items guid ([cad48f7](https://gitlab.com/fi-sas/fisas-current-account/commit/cad48f7fbe0b8df7eae9b676551b41eb365d3287))
- plafond value equals zero ([2f4cdd4](https://gitlab.com/fi-sas/fisas-current-account/commit/2f4cdd4cdb669ce3a6ed6eb517f4bc3067d1a289))
- removing uuid from services (reverting to auto increment) ([276ac2f](https://gitlab.com/fi-sas/fisas-current-account/commit/276ac2f983101107f4e7c725720bb277a1a46c4e))
- required false to uinit_values ([facb753](https://gitlab.com/fi-sas/fisas-current-account/commit/facb7537b231d68abb2f99daea3df407e6b8b8f6))
- start.sh helper script ([7b86c0f](https://gitlab.com/fi-sas/fisas-current-account/commit/7b86c0feedd103486df09329af79ddb53eb7ea97))
- **withrelated:** add user in withrelated ([6faddb5](https://gitlab.com/fi-sas/fisas-current-account/commit/6faddb54bd8ed90d3f52c0a2e55c3faab637b8fb))

### Features

- **accounts:** add accounts payment-methods ([34223e2](https://gitlab.com/fi-sas/fisas-current-account/commit/34223e293b940ded172f7310f74973b0bcd09f75))
- **cart:** add change Qtd item cart ([0681cfc](https://gitlab.com/fi-sas/fisas-current-account/commit/0681cfc8bf2e85f4ee293f61f778f7e279ea7b8b))
- **cart:** add new fetas to the cart ([3a1323a](https://gitlab.com/fi-sas/fisas-current-account/commit/3a1323ae4cc32f882ebe2591d9247fbbdac1e05f))
- **cart:** chkout now generate movement ([89de0b0](https://gitlab.com/fi-sas/fisas-current-account/commit/89de0b070098ae32bfc769cf06c811b84827ae47))
- **carts:** add cron to remove old carts ([8f78849](https://gitlab.com/fi-sas/fisas-current-account/commit/8f78849cb433465dc79fb5ffc4be74620f99ebad))
- **charges:** add charge history, confirmed ([22b3ab3](https://gitlab.com/fi-sas/fisas-current-account/commit/22b3ab30908d198d0df691ff4b7598f7c4a7b6f6))
- **erp:** add erp_payment_config ([b3412de](https://gitlab.com/fi-sas/fisas-current-account/commit/b3412de8a524cadbc4c5fbe4f2e8f39606c4e542))
- **erp:** add service erp_mapping_vat ([fdb4484](https://gitlab.com/fi-sas/fisas-current-account/commit/fdb4484ee36884cb82df70e4274f70773d3045d7))
- **migration:** alter table migration template ([dd47345](https://gitlab.com/fi-sas/fisas-current-account/commit/dd4734562e0802a70c3a2724867595584c4adf1f))
- **movement:** service_confirm_path and fixes ([44e0aa1](https://gitlab.com/fi-sas/fisas-current-account/commit/44e0aa15a6f4a1b77cf0cc9efe5f6ebb2b50f8bc))
- **movements:** add cancel item ([79356d4](https://gitlab.com/fi-sas/fisas-current-account/commit/79356d4fbfff3e680dc84c0f3e3a8de7bd084e3d))
- **movements:** add user reports endpoint ([e1f2439](https://gitlab.com/fi-sas/fisas-current-account/commit/e1f24396311cc17ea292aa520fd3d9014be3ccfb))
- **movements:** cancel movement ([f1f0f24](https://gitlab.com/fi-sas/fisas-current-account/commit/f1f0f2433d790ca276f74ed755b87dc66ae65fb6))
- **movements:** cancel, payments, charges ([54c24cd](https://gitlab.com/fi-sas/fisas-current-account/commit/54c24cd7d9dce51c97cf7843d012a5df985a5263))
- **payment:** get MBRef from MS PPAP ([fa1af22](https://gitlab.com/fi-sas/fisas-current-account/commit/fa1af22077a95ae051975e27d5eeb6c5c4d4c173))
- add account validation to cart actions ([8eb92ef](https://gitlab.com/fi-sas/fisas-current-account/commit/8eb92efe7dc9fece99f066be11a00d91f0ea7dc8))
- add inital confirm and pay method ([f97b7a2](https://gitlab.com/fi-sas/fisas-current-account/commit/f97b7a2f710387260148373227a4a8a4cbce0df0))
- inital approach to confirm movements ([f7413a4](https://gitlab.com/fi-sas/fisas-current-account/commit/f7413a4d5a33850f8d952c3053ded21c1ba6262a))
- update core package ([41e5580](https://gitlab.com/fi-sas/fisas-current-account/commit/41e5580759de37d7fd239764d93cabb2475a4e92))
- **movements:** changes ([09f2856](https://gitlab.com/fi-sas/fisas-current-account/commit/09f2856bc2274f069ea2c0715b564b72a19d038f))
- **payments:** add initial cancel system ([1d5cfd2](https://gitlab.com/fi-sas/fisas-current-account/commit/1d5cfd220d5f1ce05c8306d20ea0b4934ea6e286))
- **payments:** inital system of onfirmation ([ecddcad](https://gitlab.com/fi-sas/fisas-current-account/commit/ecddcada320abc52717c5f2e24b7d2cd422d580b))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-03-04)

### Bug Fixes

- **movements:** operation filter criteria ([6c72a24](https://gitlab.com/fi-sas/fisas-current-account/commit/6c72a246cefd314bb28a60f63799e13876ce059b))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-03-04)

### Bug Fixes

- **account:** missing key 'id' wrong balances ([33a7545](https://gitlab.com/fi-sas/fisas-current-account/commit/33a7545a7909e4389dc130b158b2389c6df6c606))
- **payment:** canPay creates wrong movemvent ([8d3753a](https://gitlab.com/fi-sas/fisas-current-account/commit/8d3753a1db5de6d6bed9a2285aea28bf43fd48d6))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-03-02)

### Bug Fixes

- **movements:** add operation cancel_lend ([6fddf94](https://gitlab.com/fi-sas/fisas-current-account/commit/6fddf945421f65d680b92a035bff113c139f2308))

### Features

- **charges:** add charge history, confirmed ([22b3ab3](https://gitlab.com/fi-sas/fisas-current-account/commit/22b3ab30908d198d0df691ff4b7598f7c4a7b6f6))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-02-24)

### Bug Fixes

- **movements:** fix notifyServices ([26fc751](https://gitlab.com/fi-sas/fisas-current-account/commit/26fc7512644b4fb840c135628444b960c011bc97))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-02-24)

### Bug Fixes

- **movements:** set cancel item optional ([297d5a8](https://gitlab.com/fi-sas/fisas-current-account/commit/297d5a89eab4d01ac91e392a42d277fc35c418a4))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-02-23)

### Bug Fixes

- **env:** add env.local DEFAULT_PAYMENT_METHOD_ID ([dbf6133](https://gitlab.com/fi-sas/fisas-current-account/commit/dbf6133b603a3df292405528bc1ead37d01712f0))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-02-23)

### Bug Fixes

- **debug:** remove debug flags for MS AMA ([0a693dd](https://gitlab.com/fi-sas/fisas-current-account/commit/0a693dd13e0816ec8e51365a933d8adfd9b3932b))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-02-23)

### Bug Fixes

- **balance:** cast float missing on funds calc ([3d2dd3b](https://gitlab.com/fi-sas/fisas-current-account/commit/3d2dd3bd3444de806802fdee02026d4f4942abfd))
- **cart:** add DEFAULT_PAYMENT_METHOD_ID ([841645a](https://gitlab.com/fi-sas/fisas-current-account/commit/841645af2615ecb4ffb54b763425178a56cf1353))
- **movement:** add operation CREDIT_NOTE enum ([bba46a3](https://gitlab.com/fi-sas/fisas-current-account/commit/bba46a344c7be45736c511d9a32a96677b67dbc5))
- **movements:** cancel, credit_note ([ba102fd](https://gitlab.com/fi-sas/fisas-current-account/commit/ba102fd10209122e6910ce775b4886b5953b4a77))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-02-19)

### Bug Fixes

- **balance:** check account funds and plafond ([12b387c](https://gitlab.com/fi-sas/fisas-current-account/commit/12b387c60b430f9c7c35c6ec3cb6299377bdb9ff))
- **movements:** preview type POS ([ba099c4](https://gitlab.com/fi-sas/fisas-current-account/commit/ba099c4de834037cdeee3e0b5cd361cc8404f5c9))
- **payment:** add device POS check ([b2aac16](https://gitlab.com/fi-sas/fisas-current-account/commit/b2aac161f7522476a72ad61ff14b0938c9c3a44d))
- **payment:** check for AMA service availability ([bf5a25a](https://gitlab.com/fi-sas/fisas-current-account/commit/bf5a25ad5c6d79c670893aa3c50f3190f8bd808b))
- **payment:** define DEFAULT_PAYMENT_METHOD_ID ([c814a67](https://gitlab.com/fi-sas/fisas-current-account/commit/c814a677dc0dc308db94ab5dbdab344dfcdb59fd))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-02-12)

### Bug Fixes

- **refmb:** add refmb data into mov-item ([ff9611b](https://gitlab.com/fi-sas/fisas-current-account/commit/ff9611b6f178a7d61f3479cf52939ec4f5c92821))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-02-12)

### Bug Fixes

- **migrations:** rename files to conform knex ([7401240](https://gitlab.com/fi-sas/fisas-current-account/commit/7401240750806a1c34bb184b3bf986be2ac82e8b))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-02-12)

### Bug Fixes

- **payments:** missing get action ([16e947b](https://gitlab.com/fi-sas/fisas-current-account/commit/16e947b8cbe3a192822166ef4e65b144f38aae34))

### Features

- **payment:** get MBRef from MS PPAP ([fa1af22](https://gitlab.com/fi-sas/fisas-current-account/commit/fa1af22077a95ae051975e27d5eeb6c5c4d4c173))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-02-11)

### Bug Fixes

- **movements:** change withRelated payment path ([c5d549c](https://gitlab.com/fi-sas/fisas-current-account/commit/c5d549c73f3e053a8d7023090c9c9420fdfa224e))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-02-11)

### Bug Fixes

- **payments:** change find visibility ([269e002](https://gitlab.com/fi-sas/fisas-current-account/commit/269e002e9689eecf9d3e0bb8cf0d070e4feaa193))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-11)

### Features

- **accounts:** add accounts payment-methods ([34223e2](https://gitlab.com/fi-sas/fisas-current-account/commit/34223e293b940ded172f7310f74973b0bcd09f75))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-10)

### Bug Fixes

- **movements:** fix charge description ([ca7724c](https://gitlab.com/fi-sas/fisas-current-account/commit/ca7724cb31a093b19d2f3341b01b6fb9b2d8eb78))

### Features

- **movements:** cancel movement ([f1f0f24](https://gitlab.com/fi-sas/fisas-current-account/commit/f1f0f2433d790ca276f74ed755b87dc66ae65fb6))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-02-10)

### Features

- **movements:** cancel, payments, charges ([54c24cd](https://gitlab.com/fi-sas/fisas-current-account/commit/54c24cd7d9dce51c97cf7843d012a5df985a5263))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-02-04)

### Bug Fixes

- **migration:** timestamps fields and defaults ([6d0ee4f](https://gitlab.com/fi-sas/fisas-current-account/commit/6d0ee4fe15691d1c345282417acb77426d84b90e))
- **movements:** add PAID status ([d2ae699](https://gitlab.com/fi-sas/fisas-current-account/commit/d2ae6997383abca023ca1d7b478960b00f1c3bc7))
- **seeds:** set device_id ([1bc6377](https://gitlab.com/fi-sas/fisas-current-account/commit/1bc6377c5f91de3a96ad865578637be49ca9647b))

### Features

- **migration:** alter table migration template ([dd47345](https://gitlab.com/fi-sas/fisas-current-account/commit/dd4734562e0802a70c3a2724867595584c4adf1f))
- **movement:** service_confirm_path and fixes ([44e0aa1](https://gitlab.com/fi-sas/fisas-current-account/commit/44e0aa15a6f4a1b77cf0cc9efe5f6ebb2b50f8bc))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-29)

### Bug Fixes

- **seed:** check if payment_method already exists ([e702f1d](https://gitlab.com/fi-sas/fisas-current-account/commit/e702f1d134884909d3222f2650080093b3ed1c02))
- **seed:** minor fix ([491f4d2](https://gitlab.com/fi-sas/fisas-current-account/commit/491f4d2c3f20653dfe0985b2e3617262ab272101))
- **seed:** payment_method missing insert ([535287d](https://gitlab.com/fi-sas/fisas-current-account/commit/535287deb8f69b01a409d4933a2b4fee41e16175))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-29)

### Features

- **movements:** add user reports endpoint ([e1f2439](https://gitlab.com/fi-sas/fisas-current-account/commit/e1f24396311cc17ea292aa520fd3d9014be3ccfb))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-29)

### Bug Fixes

- **movements:** bypass bo valdation when from cart ([efcaa89](https://gitlab.com/fi-sas/fisas-current-account/commit/efcaa896e1b0dcf9b3c66e19e523acbae626870a))
- **seeds:** document url field replace file_id ([0306a44](https://gitlab.com/fi-sas/fisas-current-account/commit/0306a44119fa7a5df88f4c436fd277d1fc8c4a3a))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-29)

### Bug Fixes

- **migration:** document.number to string ([a72f975](https://gitlab.com/fi-sas/fisas-current-account/commit/a72f9758734c3fa9b12a917a4c0297d2439f99b2))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-28)

### Bug Fixes

- **movements:** add default withRelateds ([d45aff0](https://gitlab.com/fi-sas/fisas-current-account/commit/d45aff03623c251393a443801d27541e1a9ee643))
- **movements:** fix payment method CC afects balance ([6b41e4f](https://gitlab.com/fi-sas/fisas-current-account/commit/6b41e4fe36535a5324b3c06c40c4dd17390fa98c))
- add improvements to movement creation ([8f91751](https://gitlab.com/fi-sas/fisas-current-account/commit/8f91751e2779c7c2e0d8f2c731b893d7b101acbf))
- **movement:** status CONFIRMED when CHARGE ([bd38f11](https://gitlab.com/fi-sas/fisas-current-account/commit/bd38f119ade209846bd398303c7f78977f3800c2))
- **movements:** field name transaction_receipt ([97205fd](https://gitlab.com/fi-sas/fisas-current-account/commit/97205fdb930bcd07dc6f69736beeccd2e9ebb26b))
- **movemvents:** charges update status ([57c3cb2](https://gitlab.com/fi-sas/fisas-current-account/commit/57c3cb2bd83e74b0559d9075f6a5ecf3ce5fd7ad))

### Features

- add account validation to cart actions ([8eb92ef](https://gitlab.com/fi-sas/fisas-current-account/commit/8eb92efe7dc9fece99f066be11a00d91f0ea7dc8))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-01-27)

### Bug Fixes

- **migration:** merge payment and movements ([6cabf01](https://gitlab.com/fi-sas/fisas-current-account/commit/6cabf01f2de3e9b847f1931fc4229a75e6777789))
- **movement:** add payment_id ([317cb5a](https://gitlab.com/fi-sas/fisas-current-account/commit/317cb5a1f829a219e489485204605c0434b95fdb))
- minor fix's ([df2cf44](https://gitlab.com/fi-sas/fisas-current-account/commit/df2cf447372950d9fb461d90619f86238e8794c3))
- **movement:** balance filters ([3f6b815](https://gitlab.com/fi-sas/fisas-current-account/commit/3f6b8154cce5ce0563dc9b986ab19ff27124a3e2))
- **movement:** transaction_id not affected ([f2e2449](https://gitlab.com/fi-sas/fisas-current-account/commit/f2e2449ba2321f48c47cc718263917b037b484fa))
- **movements:** liquid_unit_value and unit_value ([81bbf8a](https://gitlab.com/fi-sas/fisas-current-account/commit/81bbf8a379ded193fb98778e04e00c39a040b608))

### Features

- add inital confirm and pay method ([f97b7a2](https://gitlab.com/fi-sas/fisas-current-account/commit/f97b7a2f710387260148373227a4a8a4cbce0df0))
- inital approach to confirm movements ([f7413a4](https://gitlab.com/fi-sas/fisas-current-account/commit/f7413a4d5a33850f8d952c3053ded21c1ba6262a))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-01-26)

### Bug Fixes

- movements minor fixes ([c9dbec9](https://gitlab.com/fi-sas/fisas-current-account/commit/c9dbec9d98ecddb0c93adeb81568a36b35a908ca))
- required false to uinit_values ([facb753](https://gitlab.com/fi-sas/fisas-current-account/commit/facb7537b231d68abb2f99daea3df407e6b8b8f6))
- **seeds:** accounts and movements ([fbe2865](https://gitlab.com/fi-sas/fisas-current-account/commit/fbe2865a36650b8ff93d6c2ae5fe7843b7f07c86))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-current-account/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-26)

### Bug Fixes

- **account:** fix enum document_types ([eac80fb](https://gitlab.com/fi-sas/fisas-current-account/commit/eac80fba234d94455afbd72b3c8f83077dba6185))
- **defaults:** add optionals to times ([7289c88](https://gitlab.com/fi-sas/fisas-current-account/commit/7289c880ee33f0e9dec093fdb8b466cf3023094f))
- **git:** update gitignore ([54ba372](https://gitlab.com/fi-sas/fisas-current-account/commit/54ba372e454ab31d95e7b7d2c953d133dcfc4eb8))
- **movement:** add uuiv4 to movement and items ([f2b4bf7](https://gitlab.com/fi-sas/fisas-current-account/commit/f2b4bf701befaf39b50c8bb88a6dcb691acbf420))
- **movement_items:** add serrvice withRelated ([0b4f49b](https://gitlab.com/fi-sas/fisas-current-account/commit/0b4f49b11362b7a4f905542d83dc41817616d4fe))
- **movements:** business rules ([8a0bdbc](https://gitlab.com/fi-sas/fisas-current-account/commit/8a0bdbcb0b66fcf524dc9f8afb57b4a28c7bfe2d))
- **movements:** business rules create ([67c0574](https://gitlab.com/fi-sas/fisas-current-account/commit/67c0574ff555e095de6fb27fb135cbb30ff416b1))
- **movements:** public_document_types enum ([00553db](https://gitlab.com/fi-sas/fisas-current-account/commit/00553db83fc6ac8fbad557c260bc8e8101f63077))
- **movements:** update business rules ([df9c7e7](https://gitlab.com/fi-sas/fisas-current-account/commit/df9c7e76aae7c723a4bef7833753170e2b4c9556))
- **seeds:** minor fixes movements and items ([626f546](https://gitlab.com/fi-sas/fisas-current-account/commit/626f546b72a825e1d5b05c079115458a929a4f1a))
- **seeds:** movements change user to ALUNO ([8caf6a7](https://gitlab.com/fi-sas/fisas-current-account/commit/8caf6a777b0a4636a65b2e82a23de40a604c5e33))
- remove erp data ([0dfcd67](https://gitlab.com/fi-sas/fisas-current-account/commit/0dfcd671e7e6ba7e16228586395b7765b222336c))
- remove unused fields and change names ([d65e4aa](https://gitlab.com/fi-sas/fisas-current-account/commit/d65e4aa4be868fa0b99fd06b3fc46267f38205e7))
- scope names of payments ([e57a1a8](https://gitlab.com/fi-sas/fisas-current-account/commit/e57a1a83eeb1459ef709feb6a276cb45b61f8738))
- **payment:** enable action create ([c01c21a](https://gitlab.com/fi-sas/fisas-current-account/commit/c01c21a7f3eb7077bfefa9bff8fefa83ad1eb056))

### Features

- update core package ([41e5580](https://gitlab.com/fi-sas/fisas-current-account/commit/41e5580759de37d7fd239764d93cabb2475a4e92))
- **cart:** chkout now generate movement ([89de0b0](https://gitlab.com/fi-sas/fisas-current-account/commit/89de0b070098ae32bfc769cf06c811b84827ae47))

# 1.0.0-rc.1 (2021-01-25)

### Bug Fixes

- **docker-compose:** create docker-compose to prod and dev ([b8fe9b7](https://gitlab.com/fi-sas/fisas-current-account/commit/b8fe9b769e46b54669ef28b1672cc0b23aa88954))
- **eslint:** add missing rules ([5ae421c](https://gitlab.com/fi-sas/fisas-current-account/commit/5ae421cc3ac3f014935880afbe71ffba44ed3f6d))
- **gitignore:** add idea and vscode ([7c349b5](https://gitlab.com/fi-sas/fisas-current-account/commit/7c349b5059cfecd037884e330490c20ea993d8f2))
- **gitignore:** remove .env file ([f472c7d](https://gitlab.com/fi-sas/fisas-current-account/commit/f472c7d8a1cc19a3edca33ed1d98d05d08966033))
- **items:** add service data ([6485d16](https://gitlab.com/fi-sas/fisas-current-account/commit/6485d16df51f972879dade30f648f6712f42d0a8))
- **items:** fix codes import ([57cfd55](https://gitlab.com/fi-sas/fisas-current-account/commit/57cfd557882b4206f1e35efa01f0faa5b3f0405d))
- **migrations:** movement precision numbers ([14b916c](https://gitlab.com/fi-sas/fisas-current-account/commit/14b916cb047e347fc3400a26ab644d4df7ff3f8d))
- **movement:** add cache keys to list action ([53b0492](https://gitlab.com/fi-sas/fisas-current-account/commit/53b0492e31b01fc22c5ccb366c71d0c0f9ed4dd3))
- **movement:** add fields liquid_unit_value ([17cb379](https://gitlab.com/fi-sas/fisas-current-account/commit/17cb379f95a294d7fefcfaf5a39a960ff0ba53f1))
- **movement:** change enum MOVEMENT_OPERATION ([b4dd3d8](https://gitlab.com/fi-sas/fisas-current-account/commit/b4dd3d88ce75707ae50caf2f7cf2969e3a4491e2))
- **movement:** change seeds uuid fields ([3589ca4](https://gitlab.com/fi-sas/fisas-current-account/commit/3589ca463577066214bc1c03f86c4567bc27c374))
- **movements:** add fields ([dae3a9c](https://gitlab.com/fi-sas/fisas-current-account/commit/dae3a9c9dae78e388901fa40aa4635b6b89929e4))
- **movements:** add file in documents ([cf7b8af](https://gitlab.com/fi-sas/fisas-current-account/commit/cf7b8afd6b64ebe7635130ae810067d64ada1b00))
- **movements:** add location to Adiamento ([1998299](https://gitlab.com/fi-sas/fisas-current-account/commit/19982998f40241f2c60b2be758650223d9ec1aae))
- **movements:** add location to adiantamento ([1208485](https://gitlab.com/fi-sas/fisas-current-account/commit/1208485c1155608122f126e54e5767f7881f65da))
- **movements:** add withRelated ([7f134df](https://gitlab.com/fi-sas/fisas-current-account/commit/7f134df2f9bcb4dbbd31424b7aa3d731199a192c))
- **movements:** annulament of advance location ([78d59b3](https://gitlab.com/fi-sas/fisas-current-account/commit/78d59b3f8b359ea4612a293b4c020d543c2ec3ff))
- **movements:** fix debuct advances error on multiple advances ([c5ef3ea](https://gitlab.com/fi-sas/fisas-current-account/commit/c5ef3eadb0d5583f5b67f93cbf189204b9ef34a4))
- **movements:** fix movements by id ([70535fc](https://gitlab.com/fi-sas/fisas-current-account/commit/70535fce2b1ba2f59c2ccebd05de551afe98b7e4))
- **movements:** items relation ([9167cd8](https://gitlab.com/fi-sas/fisas-current-account/commit/9167cd89c9ca04e5ba8985d26b027a45009b4372))
- **movements:** minor fix ([56416b6](https://gitlab.com/fi-sas/fisas-current-account/commit/56416b653ec84615546709b883cf6fea2e985c22))
- **payment:** change filds names ([44674fb](https://gitlab.com/fi-sas/fisas-current-account/commit/44674fb9b06dd4aa9dd12a0008ab81ba51f02f66))
- **payments:** new database fields ([38921b9](https://gitlab.com/fi-sas/fisas-current-account/commit/38921b9f0085eef6f0227a2c758ad649a6359415))
- **payments:** rearrange the canPay to return the MBRef Data ([b8a15e1](https://gitlab.com/fi-sas/fisas-current-account/commit/b8a15e12e53814c2aa3bb2899eeb2ba96d3cf0f1))
- allowing empty description @ moviment schema ([a728dea](https://gitlab.com/fi-sas/fisas-current-account/commit/a728deaab74edeef608c4b2a0f2d99109e7e40ef))
- allowing empty/null description @ movement item schema ([7341a92](https://gitlab.com/fi-sas/fisas-current-account/commit/7341a92bf320a96eab9b8de39392c8e4ec6cf5cf))
- changed service URI to be compatible with backoffice ([346bee5](https://gitlab.com/fi-sas/fisas-current-account/commit/346bee509b3ebe1da4497e4215f4e203c547704b))
- minor code fix ([06d6d94](https://gitlab.com/fi-sas/fisas-current-account/commit/06d6d948dd2fff92b254863bb1309e91979f5548))
- movement items guid ([cad48f7](https://gitlab.com/fi-sas/fisas-current-account/commit/cad48f7fbe0b8df7eae9b676551b41eb365d3287))
- plafond value equals zero ([2f4cdd4](https://gitlab.com/fi-sas/fisas-current-account/commit/2f4cdd4cdb669ce3a6ed6eb517f4bc3067d1a289))
- removing uuid from services (reverting to auto increment) ([276ac2f](https://gitlab.com/fi-sas/fisas-current-account/commit/276ac2f983101107f4e7c725720bb277a1a46c4e))
- start.sh helper script ([7b86c0f](https://gitlab.com/fi-sas/fisas-current-account/commit/7b86c0feedd103486df09329af79ddb53eb7ea97))
- **movements:** fix typo on debuctDebt ([e98b40e](https://gitlab.com/fi-sas/fisas-current-account/commit/e98b40e4633c260eec684603f057c96b1f798199))
- **withrelated:** add user in withrelated ([6faddb5](https://gitlab.com/fi-sas/fisas-current-account/commit/6faddb54bd8ed90d3f52c0a2e55c3faab637b8fb))

### Features

- **cart:** add change Qtd item cart ([0681cfc](https://gitlab.com/fi-sas/fisas-current-account/commit/0681cfc8bf2e85f4ee293f61f778f7e279ea7b8b))
- **cart:** add new fetas to the cart ([3a1323a](https://gitlab.com/fi-sas/fisas-current-account/commit/3a1323ae4cc32f882ebe2591d9247fbbdac1e05f))
- **carts:** add cron to remove old carts ([8f78849](https://gitlab.com/fi-sas/fisas-current-account/commit/8f78849cb433465dc79fb5ffc4be74620f99ebad))
- **erp:** add erp_payment_config ([b3412de](https://gitlab.com/fi-sas/fisas-current-account/commit/b3412de8a524cadbc4c5fbe4f2e8f39606c4e542))
- **erp:** add service erp_mapping_vat ([fdb4484](https://gitlab.com/fi-sas/fisas-current-account/commit/fdb4484ee36884cb82df70e4274f70773d3045d7))
- **movements:** add cancel item ([79356d4](https://gitlab.com/fi-sas/fisas-current-account/commit/79356d4fbfff3e680dc84c0f3e3a8de7bd084e3d))
- **movements:** changes ([09f2856](https://gitlab.com/fi-sas/fisas-current-account/commit/09f2856bc2274f069ea2c0715b564b72a19d038f))
- **payments:** add initial cancel system ([1d5cfd2](https://gitlab.com/fi-sas/fisas-current-account/commit/1d5cfd220d5f1ce05c8306d20ea0b4934ea6e286))
- **payments:** inital system of onfirmation ([ecddcad](https://gitlab.com/fi-sas/fisas-current-account/commit/ecddcada320abc52717c5f2e24b7d2cd422d580b))
