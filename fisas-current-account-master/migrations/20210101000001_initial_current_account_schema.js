module.exports.up = (db) => {
	return db.schema
		.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
		.createTable("document", (table) => {
			table.increments();
			table
				.enu(
					"type",
					[
						"INVOICE",
						"INVOICE_RECEIPT",
						"RECEIPT",
						"CREDIT_NOTE",
						"DEPOSIT",
						"CANCEL_DEPOSIT",
						"REFUND",
						"LEND",
						"CANCEL_LEND",
					],
					{
						useNative: true,
						//existingType: true,
						enumName: "document_type",
					},
				)
				.notNullable();
			table.string("series", 120).notNullable();
			table.string("number", 120).notNullable();
			table.datetime("issued_at").notNullable();
			table.string("url", 180).nullable();
			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("payment_method", (table) => {
			table.uuid("id").notNullable().primary();
			table.string("name", 45).notNullable();
			table.string("tag", 45).notNullable();
			table.string("description", 250).nullable();
			table.boolean("is_immediate").defaultTo(true).notNullable();
			table.boolean("active").defaultTo(false).notNullable();
			table.boolean("charge").defaultTo(false).notNullable();
			table.string("path", 120).notNullable();
			table.json("gateway_data").nullable();
			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
			table.unique("tag");
		})
		.createTable("default_item_configuration", (table) => {
			table.increments();
			table.integer("parameter_account_id").unsigned().notNullable();
			table.string("parameter_operation", 255);
			table.integer("parameter_service_id").unsigned();
			table.string("product_code", 120).notNullable();
			table.string("name", 255).notNullable();
			table.string("description", 250);
			table.integer("vat_id").notNullable();
			table.string("erp_budget", 250);
			table.string("erp_sncap", 250);
			table.string("erp_income_cost_center", 250);
			table.string("erp_funding_source", 250);
			table.string("erp_program", 250);
			table.string("erp_measure", 250);
			table.string("erp_project", 250);
			table.string("erp_activity", 250);
			table.string("erp_action", 250);
			table.string("erp_functional_classifier", 250);
			table.string("erp_organic", 250);
			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
			table.unique(
				["parameter_operation", "parameter_account_id", "parameter_service_id"],
				"parameters_unique",
			);
		})

		.createTable("account", (table) => {
			table.increments();
			table.string("name", 120).nullable();
			table.string("tin", 9).notNullable();
			table.string("iban", 34).nullable();
			table.boolean("allow_partial_payments").nullable().defaultTo(false);
			table
				.enu("plafond_type", ["PLAFOND", "ADVANCE"], {
					useNative: true,
					enumName: "account_plafond_type",
				})
				.nullable();
			table.decimal("plafond_value", 8, 2).nullable();
			table.json("public_document_types").nullable();

			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("payment", (table) => {
			table.uuid("id").notNullable().primary();
			table.integer("account_id").references("id").inTable("account").nullable();
			table.integer("user_id").unsigned();
			table.uuid("payment_method_id").references("id").inTable("payment_method").nullable();
			table.json("payment_method_data").nullable();
			table.uuid("movement_id").notNullable();
			table.integer("confirmation_user_id").unsigned().nullable();
			table.integer("confirmation_device_id").unsigned().nullable();
			table.text("transaction_receipt").nullable();
			table.decimal("value", 8, 2).notNullable();
			table
				.enu("status", ["CONFIRMED", "PENDING", "CANCELLED"], {
					useNative: true,
					/*existingType: true,*/
					enumName: "payment_status",
				})
				.notNullable()
				.defaultTo("PENDING");

			table.datetime("expire_at").nullable();
			table.datetime("paid_at").nullable();
			table.datetime("confirmed_at").nullable();
			table.datetime("cancelled_at").nullable();
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("movement", (table) => {
			table.uuid("id").notNullable().primary();
			table
				.integer("account_id")
				.unsigned()
				.references("id")
				.inTable("account")
				.notNullable()
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.integer("document_id").unsigned().references("id").inTable("document").nullable();
			table
				.enu(
					"operation",
					[
						"CHARGE",
						"CHARGE_NOT_IMMEDIATE",
						"CANCEL",
						"RECEIPT",
						"INVOICE",
						"LEND",
						"REFUND",
						"INVOICE_RECEIPT",
					],
					{
						useNative: true,
						enumName: "movement_operation",
					},
				)
				.notNullable();

			table.integer("device_id").unsigned().nullable();
			table.integer("user_id").unsigned().notNullable();
			table.string("entity", 120).nullable();
			table.string("tin", 9).nullable();
			table.string("email", 120).nullable();
			table.string("address", 250).nullable();
			table.string("postal_code", 10).nullable();
			table.string("city", 250).nullable();
			table.string("country", 250).nullable();
			table.string("description", 250).nullable();
			table.boolean("is_immediate").defaultTo(1).notNullable();

			table.uuid("transaction_id").nullable();
			table.decimal("transaction_value", 8, 2).notNullable();

			table.decimal("current_balance", 8, 2).defaultTo(0).notNullable();
			table.decimal("owing_value", 8, 2).defaultTo(0).notNullable(); //in_debt_value
			table.decimal("paid_value", 8, 2).defaultTo(0).notNullable();
			table.decimal("advanced_value", 8, 2).defaultTo(0).notNullable();

			table.uuid("original_movement_id").nullable();

			table
				.enu("status", ["PAID", "PENDING", "CONFIRMED", "CANCELLED", "CANCELLED_LACK_PAYMENT"], {
					useNative: true,
					enumName: "movement_status",
				})
				.notNullable()
				.defaultTo("PENDING");
			table.boolean("affects_balance").defaultsTo(false).notNullable();
			table.uuid("payment_method_id").references("id").inTable("payment_method").nullable();
			table.string("payment_method_name", 120).nullable();
			table.datetime("expiration_at").nullable();
			table
				.uuid("payment_id")
				.references("id")
				.inTable("payment")
				.onUpdate("CASCADE")
				.onDelete("CASCADE")
				.nullable();

			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("movement_item", (table) => {
			table.uuid("id").notNullable().primary();
			table
				.uuid("movement_id")
				.references("id")
				.inTable("movement")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.string("product_code", 120).nullable();
			table.string("name", 255).nullable();
			table.string("description", 250).nullable();
			table.json("extra_info").nullable();

			table.decimal("total_value", 12, 5).notNullable();
			table.integer("quantity").unsigned().notNullable();

			table.decimal("liquid_unit_value", 12, 5).notNullable();
			table.decimal("unit_value", 12, 5).notNullable();
			table.decimal("liquid_value", 12, 5).notNullable();
			table.decimal("discount_value", 12, 5).defaultTo(0).nullable();
			table.decimal("paid_value", 12, 5).defaultTo(0).nullable();

			table.integer("vat_id").notNullable();
			table.decimal("vat", 12, 5).notNullable();
			table.decimal("vat_value", 12, 5).notNullable();

			table.string("location", 255).notNullable();
			table.string("article_type", 120).nullable();

			table.integer("service_id").unsigned().nullable();
			table.string("service_confirm_path", 120).notNullable();
			table.string("service_cancel_path", 120).notNullable();

			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("movement_relation", (table) => {
			table.increments();
			table
				.uuid("movement_id")
				.references("id")
				.inTable("movement")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table
				.uuid("related_movement_id")
				.references("id")
				.inTable("movement")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})

		.createTable("cart", (table) => {
			table.uuid("id").notNullable().primary();
			table.uuid("payment_method_id").references("id").inTable("payment_method").nullable();
			table.json("payment_method_data").nullable();
			table.integer("account_id").references("id").inTable("account").nullable();
			table.integer("device_id").unsigned();
			table.integer("user_id").unsigned();
			table.datetime("checkout_at").nullable();
			table.unique(["account_id", "user_id", "device_id"]);
			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		})
		.createTable("cart_item", (table) => {
			table.uuid("id").notNullable().primary();
			table
				.uuid("cart_id")
				.references("id")
				.inTable("cart")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.integer("service_id").unsigned();
			table.string("product_code", 120).notNullable();
			table.string("article_type", 120).nullable();
			table.integer("quantity").unsigned();
			table.decimal("unit_value", 8, 2).notNullable();
			table.decimal("liquid_unit_value", 8, 2).notNullable();
			table.decimal("liquid_value", 8, 2).notNullable();
			table.integer("vat_id").unsigned().notNullable();
			table.float("vat").notNullable();
			table.decimal("vat_value", 8, 2).notNullable();
			table.decimal("total_value", 8, 2).notNullable();
			table.string("name", 250).notNullable();
			table.string("description", 250);
			table.string("location", 120).notNullable();
			table.json("extra_info");
			table.datetime("expires_in").nullable();

			table.string("service_confirm_path", 120).notNullable();
			table.string("service_cancel_path", 120).notNullable();

			//timestamps — table.timestamps([useTimestamps], [defaultToNow])
			table.timestamps(true, true);
			//table.datetime("created_at").notNullable();
			//table.datetime("updated_at").notNullable();
		});
}

module.exports.down = (db) => {
	return db.schema
		.dropTable("movement")
		.dropTable("movement_item")
		.dropTable("movement_relation")
		.dropTable("document")
		.dropTable("movement_document")
		.dropTable("account")
		.dropTable("payment_method")
		.dropTable("payment")
		.dropTable("cart")
		.dropTable("cart_item")
		.dropTable("document")
		.dropTable("default_item_configuration");
}

module.exports.configuration = { transaction: true };
