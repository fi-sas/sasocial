exports.up = function (knex) {
	return knex.schema.createTable("account_payment_method", (table) => {
		table.increments();
		table.integer("account_id").references("id").inTable("account").notNullable();
		table.integer("device_id").unsigned().notNullable();
		table.uuid("payment_method_id").references("id").inTable("payment_method").notNullable();
		table.unique(["account_id", "device_id", "payment_method_id"]);
		table.timestamps(true, true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("payment_method_device");
};
