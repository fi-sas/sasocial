exports.up = function (knex, Promise) {
	return knex.schema.raw("ALTER TYPE movement_operation ADD VALUE 'CANCEL_LEND';");
};

exports.down = function (knex, Promise) {
	return knex.schema.raw(`
            ALTER TYPE movement_operation RENAME TO movement_operation_old;
            CREATE TYPE movement_operation AS ENUM ('CHARGE','CHARGE_NOT_IMMEDIATE','CANCEL','RECEIPT','INVOICE','LEND','REFUND','INVOICE_RECEIPT','CREDIT_NOTE');
            ALTER TABLE movement ALTER COLUMN operation TYPE movement_operation USING operation::text::movement_operation;
            DROP TYPE movement_operation_old;
        `);
};
