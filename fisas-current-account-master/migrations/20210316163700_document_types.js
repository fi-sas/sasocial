exports.up = function (knex) {
	return knex.schema.createTable("document_types", (table) => {
		table.increments();
		table.string("name", 45).notNullable();
		table.string("description", 250);
		table.string("doc_type_acronym", 3).notNullable();
		table
			.enu(
				"operation",
				[
					"CHARGE",
					"CHARGE_NOT_IMMEDIATE",
					"CANCEL",
					"RECEIPT",
					"INVOICE",
					"LEND",
					"REFUND",
					"INVOICE_RECEIPT",
					"CREDIT_NOTE",
					"CANCEL_LEND",
				],
				{
					useNative: true,
					existingType: true,
					enumName: "movement_operation",
				},
			)
			.notNullable();
		table.boolean("active").defaultTo(true).notNullable();
		table.timestamps(true, true);
		table.unique(["operation"], "operation_unique");
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("document_types");
};
