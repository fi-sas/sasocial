module.exports.up = (db) => {
	return db.schema.alterTable("movement", (table) => {
		table.increments("seq_doc_num", { primaryKey: false });
		table.string("doc_type_acronym", 3).nullable();
	});
}

module.exports.down = (db) => {
	return db.schema.alterTable("movement", (table) => {
		table.dropColumn("seq_doc_num");
		table.dropColumn("doc_type_acronym");
	});
}

module.exports.configuration = { transaction: true };
