exports.up = function (knex, Promise) {
    return knex.schema.raw("ALTER TYPE movement_status ADD VALUE 'PARTIALLY_PAID';");
};

exports.down = function (knex, Promise) {
    return knex.schema.raw(`
            ALTER TYPE movement_status RENAME TO movement_status_old;
            CREATE TYPE movement_status AS ENUM ('PAID', 'PENDING', 'CONFIRMED', 'CANCELLED', 'CANCELLED_LACK_PAYMENT');
            ALTER TABLE movement ALTER COLUMN status TYPE movement_status USING status::text::movement_status;
            DROP TYPE movement_status_old;
        `);
};
