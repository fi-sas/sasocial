exports.up = function (knex) {
	return knex.schema
		.createTable("cash_account", (table) => {
			table.increments();

			table.string("code", 6).notNullable();
			table.string("description", 250).nullable();

			table.unique(["code"]);
			table.timestamps(true, true);
		})
		.createTable("cash_account_users", (table) => {
			table.increments();

			table.integer("cash_account_id").unsigned().references("id").inTable("cash_account");
			table.integer("user_id").unsigned().notNullable();

			table.unique(["cash_account_id", "user_id"]);
			table.timestamps(true, true);
		})
		.alterTable("movement", (table) => {
			table.integer("cash_account_id").unsigned().references("id").inTable("cash_account").nullable();
		})
		.alterTable("payment", (table) => {
			table.integer("cash_account_id").unsigned().references("id").inTable("cash_account").nullable();
		});
};

exports.down = function (knex) {
	return knex.schema
		.dropTable("cash_account")
		.dropTable("cash_account_users")
		.alterTable("movement", (table) => {
			table.dropColumn("cash_account_id");
		})
		.alterTable("payment", (table) => {
			table.dropColumn("cash_account_id");
		});
};