exports.up = function (knex) {
	return knex.schema.alterTable("cart_item", (table) => {
		table.integer("max_quantity").nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("cart_item", (table) => {
		table.dropColumn("max_quantity").nullable();
	});
};
