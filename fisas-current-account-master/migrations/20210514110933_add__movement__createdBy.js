exports.up = function (knex) {
	return knex.schema.alterTable("movement", (table) => {
		table.integer("created_by").unsigned().nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("movement", (table) => {
		table.dropColumn("created_by");
	});
};
