exports.up = function (knex) {
	return knex.schema.alterTable("cash_account", (table) => {
		table.string("iban", 34).nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("cash_account", (table) => {
		table.dropColumn("iban");
	});
};
