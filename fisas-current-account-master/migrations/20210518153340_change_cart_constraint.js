
exports.up = function (knex) {
	return knex.schema.alterTable("cart", async (table) => {
		await knex.raw("ALTER TABLE cart DROP CONSTRAINT IF EXISTS user_id;");
		table.unique(["account_id", "user_id", "device_id"]);
	});
};

exports.down = function (knex) {

};
