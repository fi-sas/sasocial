
exports.up = (knex) => {
	return knex.schema.alterTable("cart_item", (table) => {
		table.decimal("unit_value", 8, 3).notNullable().alter();
		table.decimal("liquid_unit_value", 8, 3).notNullable().alter();
		table.decimal("liquid_value", 8, 3).notNullable().alter();
		table.decimal("vat_value", 8, 3).notNullable().alter();
		table.decimal("total_value", 8, 3).notNullable().alter();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("cart_item", (table) => {
		table.decimal("unit_value", 8, 2).notNullable().alter();
		table.decimal("liquid_unit_value", 8, 2).notNullable().alter();
		table.decimal("liquid_value", 8, 2).notNullable().alter();
		table.decimal("vat_value", 8, 2).notNullable().alter();
		table.decimal("total_value", 8, 2).notNullable().alter();
	});
};
