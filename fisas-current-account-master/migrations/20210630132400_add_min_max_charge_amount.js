exports.up = function (knex) {
	return knex.schema.alterTable("account_payment_method", (table) => {
		table.decimal("min_charge_amount", 8, 3).defaultTo(5);
		table.decimal("max_charge_amount", 8, 3).defaultTo(200);
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("account_payment_method", (table) => {
		table.dropColumn("min_charge_amount");
		table.dropColumn("max_charge_amount");
	});
};
