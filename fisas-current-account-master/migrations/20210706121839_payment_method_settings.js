exports.up = function (knex) {
	return knex.schema.createTable("payment_method_settings", (table) => {
		table.increments();
		table.integer("account_id").references("id").inTable("account").notNullable();
		table.uuid("payment_method_id").references("id").inTable("payment_method").notNullable();
		table.decimal("min_charge_amount", 8, 3).defaultTo(5);
		table.decimal("max_charge_amount", 8, 3).defaultTo(200);
		table.unique(["account_id", "payment_method_id"]);
		table.timestamps(true, true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("payment_method_settings");
};
