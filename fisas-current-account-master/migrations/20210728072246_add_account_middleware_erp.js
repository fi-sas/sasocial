exports.up = function (knex) {
	return knex.schema.alterTable("account", (table) => {
		table.string("middleware_erp_path", 120).nullable();
		table.string("middleware_erp_exemption_reason", 3).nullable();

	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("account", (table) => {
		table.dropColumn("middleware_erp_path");
		table.dropColumn("middleware_erp_exemption_reason");
	});
};
