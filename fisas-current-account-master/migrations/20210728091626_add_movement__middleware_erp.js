exports.up = function (knex) {
    return knex.schema.alterTable("movement", (table) => {
        table.integer("document_erp_id").unsigned().nullable()
        table.string("document_erp_number").unsigned().nullable()
        table.string("document_erp_url", 256).nullable();
        table
            .enu("middleware_erp_status", [
                "SKIPPED",
                "PENDING",
                "IN_PROGRESS",
                "SUCCESS",
                "FAILED"], {
                useNative: true,
                enumName: "middleware_erp_status",
            })
            .notNullable()
            .defaultTo("PENDING");
        table.json("middleware_erp_result").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.alterTable("movement", (table) => {
        table.dropColumn("document_erp_url");
        table.dropColumn("middleware_erp_status");
        table.dropColumn("middleware_erp_result");
    });
};
