exports.up = function (knex) {
    return knex.schema.createTable("document_types_settings", (table) => {
        table.increments();
        table.integer("account_id").references("id").inTable("account").unsigned().notNullable();
        table.integer("document_type_id").references("id").inTable("document_types").unsigned().notNullable();
        table.boolean("send_to_erp").defaultsTo(false).notNullable();
        table.unique(["account_id", "document_type_id"]);
        table.timestamps(true, true);
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable("document_types_settings");
};
