
exports.up = function (knex) {
    return knex.schema.alterTable("document", (table) => {
        table.integer("document_erp_id").unsigned().nullable()
    });
};

exports.down = function (knex) {
    return knex.schema.alterTable("document", (table) => {
        table.dropColumn("document_erp_id");
    });
};
