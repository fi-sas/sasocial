
exports.up = function (knex) {
    return knex.schema.alterTable("account", (table) => {
        table.integer("helpdesk_user_id").unsigned().nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.alterTable("account", (table) => {
        table.dropColumn("helpdesk_user_id");
    });
};
