exports.up = function (knex, Promise) {
	return knex.schema.raw("ALTER TYPE middleware_erp_status ADD VALUE 'RETRY';");
};

exports.down = function (knex, Promise) {
	return knex.schema.raw(`
            ALTER TYPE middleware_erp_status RENAME TO middleware_erp_status_old;
            CREATE TYPE middleware_erp_status AS ENUM ('SKIPPED','PENDING','IN_PROGRESS','SUCCESS','FAILED');
            ALTER TABLE movement ALTER COLUMN middleware_erp_status TYPE middleware_erp_status USING operation::text::middleware_erp_status;
            DROP TYPE middleware_erp_status_old;
        `);
};
