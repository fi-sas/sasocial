
exports.up = function (knex) {
    return knex.schema.alterTable("cart", (table) => {
        table.boolean("is_blocked").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.alterTable("cart", (table) => {
        table.dropColumn("is_blocked");
    });
};