exports.up = function (knex) {
    return knex.schema.alterTable("movement", (table) => {
        table.dropColumn("sale_location");
    });
};

exports.down = function (knex) {
    return knex.schema.alterTable("movement", (table) => {
        table.string("sale_location", 255).nullable();
    });
};