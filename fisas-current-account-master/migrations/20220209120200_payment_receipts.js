exports.up = function (knex) {
	return knex.schema.alterTable("payment", (table) => {
		table.text("client_receipt").nullable();
		table.text("merchant_receipt").nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("payment", (table) => {
		table.dropColumn("client_receipt");
		table.dropColumn("merchant_receipt");
	});
};
