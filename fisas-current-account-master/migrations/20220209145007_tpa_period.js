exports.up = function (knex) {
	return knex.schema.createTable("tpa_period", (table) => {
		table.increments();
		table.integer("device_id").unsigned().notNullable();
		table.text("receipt");
		table.timestamps(true, true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("tpa_period");
};
