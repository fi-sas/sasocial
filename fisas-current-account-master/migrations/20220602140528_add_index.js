exports.up = function (knex) {
	return knex.schema.alterTable("movement", (table) => {
		table.index(["user_id", "account_id"], "idx_user_account");
		table.index(["transaction_id"], "idx_transaction");
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("movement", (table) => {
		table.dropIndex(["user_id", "account_id"], "idx_user_account");
		table.index(["transaction_id"], "idx_transaction");
	});
};
