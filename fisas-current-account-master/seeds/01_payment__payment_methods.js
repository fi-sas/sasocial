module.exports.seed = async (knex) => {
	const data = [
		{
			id: "0bbd1f3f-8720-4075-880c-bf3d789efceb",
			name: "Conta Corrente",
			tag: "CC",
			description: "Pagamento por saldo da conta corrente",
			path: "payments.payment-method-balance",
			is_immediate: true,
			active: true,
			charge: true,
			gateway_data: {},
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: "6273cc97-943b-4524-8ddf-a7287afd331b",
			name: "Numerário",
			tag: "NUM",
			description: "Pagamento por numerário.",
			path: "payments.payment-method-cash",
			is_immediate: true,
			active: true,
			charge: false,
			gateway_data: null,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: "ae10940a-a64b-438c-bc1e-290528b7fb96",
			name: "Multibanco",
			tag: "MB",
			description: "Pagamento por multibanco.",
			path: "payments.payment-method-tpa",
			is_immediate: true,
			active: true,
			charge: true,
			gateway_data: {},
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: "e9d4ad01-c1c8-4ac7-82f3-a373b08d030c",
			name: "Multibanco Quiosque",
			tag: "MB_KIOSK",
			description: "Pagamento por multibanco nos kiosks.",
			path: "payments.payment-method-tpa-kiosk",
			is_immediate: false,
			active: true,
			charge: true,
			gateway_data: {},
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: "ebaffe5f-db1d-48b6-944d-fb33f4596bb1",
			name: "Referência Multibanco",
			tag: "REFMB_AMA",
			description: "Pagamento por multibanco Plataforma AMA.",
			path: "payments.payment-method-multibank_ama_paygate",
			is_immediate: false,
			active: true,
			charge: true,
			gateway_data: {},
			created_at: new Date(),
			updated_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("payment_method").select().where({
				id: d.id,
				tag: d.tag,
			});

			if (rows.length === 0) {
				// no matching records found
				await knex("payment_method").insert(d);
			} else {
				await knex("payment_method").update(d).where({
					id: d.id,
				});
			}
		}),
	);
};
