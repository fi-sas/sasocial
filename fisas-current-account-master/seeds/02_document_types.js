module.exports.seed = async (knex) => {
	const data = [
		{
			name: "Carregamento",
			description: "Carregamento imediato: numerário/tpa",
			doc_type_acronym: "CNT",
			operation: "CHARGE",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Carregamento RefMB",
			description: "Carregamento não imediato: RefMB",
			doc_type_acronym: "CMB",
			operation: "CHARGE_NOT_IMMEDIATE",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Cancelamento",
			description: "Cancelamento de movimento",
			doc_type_acronym: "CNL",
			operation: "CANCEL",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Recibo",
			description: "Recebimento",
			doc_type_acronym: "RCB",
			operation: "RECEIPT",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Fatura",
			description: "Fatura",
			doc_type_acronym: "FTR",
			operation: "INVOICE",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Fatura/Recibo",
			description: "Fatura/Recibo",
			doc_type_acronym: "FRB",
			operation: "INVOICE_RECEIPT",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Nota de Crédito",
			description: "Nota de Crédito",
			doc_type_acronym: "NCR",
			operation: "CREDIT_NOTE",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Adiantamento",
			description: "Adiantamento",
			doc_type_acronym: "ADT",
			operation: "LEND",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Reembolso",
			description: "Reembolso",
			doc_type_acronym: "RMB",
			operation: "REFUND",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("document_types").select().where({
				operation: d.operation,
			});

			if (rows.length === 0) {
				// no matching records found
				await knex("document_types").insert(d);
			}
		}),
	);
};
