module.exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			name: "Main Account",
			tin: "123456799",
			iban: "PT50002700000001234567899",
			plafond_type: "PLAFOND",
			plafond_value: 10.0,
			allow_partial_payments: false,
			public_document_types: '["Recibo", "Fatura", "Fatura/Recibo"]',
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			id: 2,
			name: "Polo 1 / Bar 1",
			tin: "123456788",
			iban: "PT50002700000001234567888",
			plafond_type: "ADVANCE",
			plafond_value: 10.0,
			allow_partial_payments: false,
			public_document_types: '["Recibo", "Fatura", "Fatura/Recibo", "Adiantamento"]',
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			id: 3,
			name: "Polo 1 / Cantina 1",
			tin: "123456777",
			iban: "PT50002700000001234567886",
			plafond_type: "PLAFOND",
			plafond_value: 20.0,
			allow_partial_payments: false,
			public_document_types: '["Fatura/Recibo"]',
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			id: 4,
			name: "Polo 2 / Bar 1",
			tin: "123456777",
			iban: "PT50002700000001234567887",
			plafond_type: "PLAFOND",
			plafond_value: 10.0,
			allow_partial_payments: false,
			public_document_types: '["Recibo", "Fatura", "Fatura/Recibo"]',
			updated_at: new Date(),
			created_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("account").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("account").insert(d);
			}
			return true;
		}),
	);
};
