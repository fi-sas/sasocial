module.exports.seed = async (knex) => {
	const data = [
		{
			id: 1,
			type: "INVOICE_RECEIPT",
			series: "FT/RC001",
			number: 001,
			issued_at: new Date(),
			url:
				"https://d3pbdh1dmixop.cloudfront.net/pdfexpert/img/howto/templates/pdf/pt/service-invoice-template-pt.pdf",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			id: 2,
			type: "INVOICE",
			series: "FT002",
			number: 002,
			issued_at: new Date(),
			url:
				"https://d3pbdh1dmixop.cloudfront.net/pdfexpert/img/howto/templates/pdf/pt/service-invoice-template-pt.pdf",
			updated_at: new Date(),
			created_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("document").select().where("id", d.id);
			if (rows.length === 0) {
				await knex("document").insert(d);
			}
			return true;
		}),
	);
};
