const moment = require("moment");
exports.seed = (knex) =>
	knex("movement")
		.count({ count: "id" })
		.first()
		.then((res) => {
			if (res.count == 0) {
				return knex("movement")
					.insert([
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba1",
							account_id: 1, // Main Account
							document_id: null,
							operation: "CHARGE",

							seq_doc_num: 1,
							doc_type_acronym: "CNT",

							device_id: 2,
							user_id: 3, // Seed's: ALUNO <aluno@sasocial.pt>
							entity: "Aluno XYZ",
							tin: "123444444",
							email: "user@mail.com",
							address: "Rua XPTO, nºx",
							postal_code: "4700-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Carregamento",
							is_immediate: true,
							transaction_id: "bb029d3a-95a9-4c39-bbc6-63502c4402fb",

							transaction_value: 50.0,
							paid_value: 0.0,
							advanced_value: 0.0,
							current_balance: 50.0,
							owing_value: 0, //em dívida

							status: "CONFIRMED",
							affects_balance: true,
							payment_method_id: "6273cc97-943b-4524-8ddf-a7287afd331b",
							payment_method_name: "Numerário",
							expiration_at: null,
							updated_at: moment().add(1, "s").toDate(),
							created_at: moment().add(1, "s").toDate(),
						},
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba2",
							account_id: 1, // Main Account
							document_id: null,
							operation: "CHARGE",

							seq_doc_num: 2,
							doc_type_acronym: "CNT",

							device_id: 2,
							user_id: 2, // Seed's: ADMIN <admin@sasocial.pt>
							entity: "Admin user",
							tin: "123444444",
							email: "admin@sasocial.pt",
							address: "Rua XPTO, nºx",
							postal_code: "4700-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Carregamento",
							is_immediate: true,
							transaction_id: "bb029d3a-95a9-4c39-bbc6-63502c4402fb",

							transaction_value: 200.0,
							paid_value: 0.0,
							advanced_value: 0.0,
							current_balance: 200.0,
							owing_value: 0, //em dívida

							status: "CONFIRMED",
							affects_balance: true,
							payment_method_id: "6273cc97-943b-4524-8ddf-a7287afd331b",
							payment_method_name: "Numerário",
							expiration_at: null,
							updated_at: moment().add(2, "s").toDate(),
							created_at: moment().add(2, "s").toDate(),
						},
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba3",
							account_id: 1, // Main
							document_id: null,
							operation: "INVOICE_RECEIPT",

							seq_doc_num: 3,
							doc_type_acronym: "FRB",

							device_id: 4,
							user_id: 2, // Seed's: ADMIN <admin@sasocial.pt>
							entity: "Nome Utilizador",
							tin: "123444444",
							email: "user@mail.com",
							address: "Rua XPTO, nºx",
							postal_code: "4900-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Compra carrinho",
							is_immediate: true,
							transaction_id: "bb029d3a-95a9-4c39-bbc6-63502c4402fb",

							transaction_value: -6.396,
							paid_value: 6.396,
							advanced_value: 0.0,
							current_balance: 200.0,
							owing_value: 0.0, //em dívida

							status: "PAID",
							affects_balance: false,
							payment_method_id: "6273cc97-943b-4524-8ddf-a7287afd331b",
							payment_method_name: "Numerário",
							expiration_at: null,
							updated_at: moment().add(3, "s").toDate(),
							created_at: moment().add(3, "s").toDate(),
						},
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba4",
							account_id: 1, // Main Account
							document_id: null,
							operation: "INVOICE",

							seq_doc_num: 4,
							doc_type_acronym: "FTR",

							device_id: 9,
							user_id: 3, // Seed's: ALUNO <aluno@sasocial.pt>
							entity: "Aluno XYZ",
							tin: "123444444",
							email: "aluno@sasocial.pt",
							address: "Rua XPTO, nºx",
							postal_code: "4700-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Alojamento",
							is_immediate: false,
							transaction_id: "7fe5d561-c205-40c1-9d77-09b8618ee590",

							transaction_value: -92.25,
							paid_value: 0.0,
							advanced_value: 0.0,
							current_balance: 50.0,
							owing_value: 92.25, //em dívida

							status: "PENDING",
							affects_balance: false,
							expiration_at: moment().add(1, "s").toDate(),
							updated_at: moment().add(4, "s").toDate(),
							created_at: moment().add(4, "s").toDate(),
						},
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba5",
							account_id: 2, // Polo 1: Bar 1
							document_id: null,
							operation: "CHARGE",

							seq_doc_num: 5,
							doc_type_acronym: "CNT",

							device_id: 2,
							user_id: 3, // Seed's: ALUNO <aluno@sasocial.pt>
							entity: "Aluno XYZ",
							tin: "123444444",
							email: "user@mail.com",
							address: "Rua XPTO, nºx",
							postal_code: "4700-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Carregamento",
							is_immediate: true,
							transaction_id: "bb029d3a-95a9-4c39-bbc6-63502c4402fb",

							transaction_value: 20.0,
							paid_value: 20.0,
							advanced_value: 0.0,
							current_balance: 20.0,
							owing_value: 0, //em dívida

							status: "CONFIRMED",
							affects_balance: true,
							payment_method_id: "6273cc97-943b-4524-8ddf-a7287afd331b",
							payment_method_name: "Numerário",
							expiration_at: null,
							updated_at: moment().add(5, "s").toDate(),
							created_at: moment().add(5, "s").toDate(),
						},
						{
							id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba6",
							account_id: 2, // Polo 1: Bar 1
							document_id: null,
							operation: "INVOICE_RECEIPT",

							seq_doc_num: 6,
							doc_type_acronym: "FRB",

							device_id: 4,
							user_id: 3, // Seed's: ALUNO <aluno@sasocial.pt>
							entity: "Aluno XYZ",
							tin: "123444444",
							email: "user@mail.com",
							address: "Rua XPTO, nºx",
							postal_code: "4900-000",
							city: "Viana do Castelo",
							country: "Portugal",
							description: "Compra carrinho",
							is_immediate: true,
							transaction_id: "bb029d3a-95a9-4c39-bbc6-63502c4402fb",

							transaction_value: -1.2,
							paid_value: 1.2,
							advanced_value: 0.0,
							current_balance: 48.8,
							owing_value: 0.0, //em dívida

							status: "PAID",
							affects_balance: true,
							payment_method_id: "0bbd1f3f-8720-4075-880c-bf3d789efceb",
							payment_method_name: "Conta Corrente",
							expiration_at: null,
							updated_at: moment().add(6, "s").toDate(),
							created_at: moment().add(6, "s").toDate(),
						},
					])
					.then(() =>
						knex("movement_item").insert([
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba1",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba1",
								service_id: 4, // Serviço de Pagamentos/Carregamentos
								product_code: "Carregamento de saldo",
								name: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								description: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								extra_info: {},
								total_value: 50.0,
								quantity: 1,
								liquid_unit_value: 50.0, //valor unitário com iva
								unit_value: 50.0,
								liquid_value: 50.0,
								discount_value: 0.0,
								paid_value: 50.0,
								vat_id: 2,
								vat: 0.0,
								vat_value: 0.0,
								location: "",
								article_type: "CHARGE",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba2",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba2",
								service_id: 4, // Serviço de Pagamentos/Carregamentos
								product_code: "Carregamento de saldo",
								name: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								description: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								extra_info: {},
								total_value: 200.0,
								quantity: 1,
								liquid_unit_value: 200.0, //valor unitário com iva
								unit_value: 200.0,
								liquid_value: 200.0,
								discount_value: 0.0,
								paid_value: 200.0,
								vat_id: 2,
								vat: 0.0,
								vat_value: 0.0,
								location: "",
								article_type: "CHARGE",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba3",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba3",
								service_id: 2, // Serviço de Alimentação / Bar
								product_code: "A123",
								name: "Queque",
								description: "Queque",
								extra_info: {},
								total_value: 2.706,
								quantity: 2,
								liquid_unit_value: 1.353, //valor unitário com iva
								unit_value: 1.1,
								liquid_value: 2.2,
								discount_value: 0.0,
								paid_value: 2.706,
								vat_id: 1,
								vat: 0.23,
								vat_value: 0.506,
								location: "",
								article_type: "BAR",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba4",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba3",
								service_id: 21, // Serviço de Alimentação / Cantina
								product_code: "A124",
								name: "Senha Almoço",
								description: "Senha Almoço",
								extra_info: {},
								total_value: 3.69,
								quantity: 1,
								liquid_unit_value: 3.69, //valor unitário com iva
								unit_value: 3.0,
								liquid_value: 3.69,
								discount_value: 0.0,
								paid_value: 3.69,
								vat_id: 1,
								vat: 0.23,
								vat_value: 0.69,
								location: "",
								article_type: "REFECTORY",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba5",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba4",
								service_id: 1, // Serviço de Alojamento
								product_code: "ALJM-Q2",
								name: "Mensalidade Quarto Duplo",
								description: "Mensalidade Quarto duplo - mês janeiro",
								extra_info: {},
								total_value: 92.25,
								quantity: 1,
								liquid_unit_value: 123.0, //valor unitário com iva
								unit_value: 100.0,
								liquid_value: 75.0,
								discount_value: 25.0,
								paid_value: 0.0,
								vat_id: 1,
								vat: 0.23,
								vat_value: 23.0,
								location: "",
								article_type: "ACCOMODATION",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba6",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba5",
								service_id: 4, // Serviço de Pagamentos/Carregamentos
								product_code: "Carregamento de saldo",
								name: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								description: "Carregamento de saldo por MS Pagamentos/Carregamentos",
								extra_info: {},
								total_value: 20.0,
								quantity: 1,
								liquid_unit_value: 20.0, //valor unitário com iva
								unit_value: 20.0,
								liquid_value: 20.0,
								discount_value: 0.0,
								paid_value: 20.0,
								vat_id: 2,
								vat: 0.0,
								vat_value: 0.0,
								location: "",
								article_type: "CHARGE",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
							{
								id: "bbaffe5f-db1d-48b6-944d-fb33f4596ba7",
								movement_id: "ebaffe5f-db1d-48b6-944d-fb33f4596ba6",
								service_id: 2, // Serviço de Alimentação / Bar
								product_code: "B123",
								name: "Café",
								description: "Café",
								extra_info: {},
								total_value: 1.2054,
								quantity: 2,
								liquid_unit_value: 0.49, //valor unitário com iva
								unit_value: 0.6,
								liquid_value: 0.98,
								discount_value: 0.0,
								paid_value: 1.2,
								vat_id: 1,
								vat: 0.23,
								vat_value: 0.225,
								location: "Polo 1 / Bar 1",
								article_type: "BAR",
								service_confirm_path: "current_account.movements.fake_service_confirm",
								service_cancel_path: "current_account.movements.fake_service_cancel",
								created_at: moment().add(1, "s").toDate(),
								updated_at: moment().add(1, "s").toDate(),
							},
						]),
					);
			}
		});
