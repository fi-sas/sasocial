module.exports.seed = async (knex) => {
	const data = [
		{
			code: "CXA001",
			description: "Caixa Balcão Único",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			code: "CXA002",
			description: "Caixa Balcão Atendimento SAS",
			updated_at: new Date(),
			created_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("cash_account").select().where("code", d.code);
			if (rows.length === 0) {
				let ca = await knex("cash_account").insert(d).returning("id");

				return knex("cash_account_users").insert({
					cash_account_id: ca.pop(),
					user_id: 2, // <-- Admin
					created_at: new Date(),
					updated_at: new Date(),
				});
			}

			return true;
		}),
	);
};
