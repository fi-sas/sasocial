"use strict";
const Validator = require("fastest-validator");
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { ACCOUNT_PLAFOND_TYPE } = require("../utils/current_account_constants");
const IBAN = require("iban");
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.accounts",
	table: "account",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "accounts")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"tin",
			"iban",
			"allow_partial_payments",
			"plafond_type",
			"plafond_value",
			"public_document_types",
			"middleware_erp_path",
			"middleware_erp_exemption_reason",
			"helpdesk_user_id",
			"updated_at",
			"created_at",
		],

		defaultWithRelateds: [],

		withRelateds: {
			available_methods(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("current_account.accounts_payments_methods.available_methods", {
								account_id: doc.id,
							})
							.then((res) => (doc.available_methods = res));
					}),
				);
			},
			middleware_erp_document_types(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("current_account.document_types_settings.available_document_types", {
								account_id: doc.id,
							})
							.then((res) => (doc.middleware_erp_document_types = res));
					}),
				);
			},
			helpdesk_user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "helpdesk_user", "helpdesk_user_id").then(
					() => {
						docs.map((doc) => {
							if (doc.helpdesk_user)
								doc.helpdesk_user = _.pick(doc.helpdesk_user, ["id", "name", "email"]);
						});
					},
				);
			},
		},

		entityValidator: {
			name: { type: "string", max: 120, optional: true },
			tin: { type: "string", max: 9, optional: false },
			iban: { type: "string", max: 34, optional: true },
			allow_partial_payments: { type: "boolean", convert: true, optional: true, default: true },
			public_document_types: { type: "string" },
			plafond_type: { type: "enum", values: ACCOUNT_PLAFOND_TYPE, optional: true },
			plafond_value: { type: "number", min: 0, convert: true, optional: true },
			updated_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.plafond_type = ctx.params.plafond_type.toUpperCase();

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateDocumentTypes",
				"validateIBAN",
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					delete ctx.params.created_at;
				},
				"validateDocumentTypes",
				"validateIBAN",
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveMiddlewareAvailableDocumentTypes"],
			update: ["saveMiddlewareAvailableDocumentTypes"],
			patch: ["saveMiddlewareAvailableDocumentTypes"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#device.id",
				],
			},
		},
		list: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#device.id",
				],
			},
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			cache: {
				keys: [
					"id",
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#device.id",
				],
			},
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		paymentMethodsTree: {
			rest: "GET /:account_id/payment-methods",
			params: {
				account_id: { type: "number", positive: true, convert: true },
			},
			handler(ctx) {
				return ctx.call(
					"current_account.accounts_payments_methods.treePaymentMethodDevice",
					ctx.params,
				);
			},
		},
		updatePaymentMethods: {
			rest: "PUT /:account_id/payment-methods",
			params: {
				account_id: { type: "number", positive: true, convert: true },
				device_id: { type: "number", positive: true, convert: true },
				payment_method_id: { type: "uuid", version: 4, optional: false },
				active: { type: "boolean" },
			},
			handler(ctx) {
				return ctx
					.call("current_account.accounts_payments_methods.updatePaymentMethodDevice", ctx.params)
					.then((results) => {
						this.clearCache();
						return results;
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		validateIBAN(ctx, res) {
			if (_.has(ctx, "params.iban")) {
				if (_.isString(ctx.params.iban) && IBAN.isValid(ctx.params.iban)) {
					ctx.params.iban = IBAN.electronicFormat(ctx.params.iban);
				} else {
					throw new Errors.ValidationError("IBAN is invalid", "IBAN_INVALID", {
						iban: ctx.params.iban,
					});
				}
			}
		},
		validateDocumentTypes(ctx) {
			const v = new Validator();

			const schema = {
				public_document_types: {
					type: "array",
					items: {
						type: "enum",
						values: [
							"Adiantamento",
							"Anulação de Adiantamento",
							"Caução",
							"Anulação de Caução",
							"Fatura/Recibo",
							"Recibo",
							"Fatura",
							"Nota de Crédito",
							"Devolução",
						],
					},
				},
			};

			const check = v.compile(schema);

			if (check(ctx.params)) {
				ctx.params.public_document_types = JSON.stringify(ctx.params.public_document_types);
			}
		},
		async saveMiddlewareAvailableDocumentTypes(ctx, res) {
			if (
				ctx.params.middleware_erp_document_types &&
				Array.isArray(ctx.params.middleware_erp_document_types)
			) {
				const doc_types_ids = ctx.params.middleware_erp_document_types.map((d) => d.id);
				await ctx.call(
					"current_account.document_types_settings.saveMiddlewareAvailableDocumentTypes",
					{
						account_id: res[0].id,
						doc_types_ids,
					},
				);
			}
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
