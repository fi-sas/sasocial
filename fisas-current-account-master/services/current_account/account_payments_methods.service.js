"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.accounts_payments_methods",
	table: "account_payment_method",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "accounts_payments_methods")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"device_id",
			"account_id",
			"payment_method_id",
			"updated_at",
			"created_at",
			"min_charge_amount",
			"max_charge_amount",
		],

		defaultWithRelateds: [],

		withRelateds: {
			payment_method(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"payments.payment-methods",
					"payment_method",
					"payment_method_id",
					"id",
					{},
					"id,name,tag",
					false,
				);
			},
		},

		entityValidator: {
			device_id: { type: "number", integer: true, positive: true, min: 1 },
			payment_method_id: { type: "uuid", version: 4, optional: false },
			account_id: { type: "number", integer: true, positive: true, min: 1 },
			updated_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function validateIfUniq(ctx) {
					const total = await ctx.call("current_account.accounts_payments_methods.count", {
						query: {
							device_id: ctx.params.device_id,
							payment_method_id: ctx.params.payment_method_id,
							account_id: ctx.params.account_id,
						},
					});

					if (total > 0) {
						throw new Errors.ValidationError(
							"Payment method already associated to account",
							"PAYMENT_METHOD_ALREADY_ASSOCIATED_ACCOUNT",
							{},
						);
					}
				},
			],
			update: [],
			patch: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		/*
			RETURN THEW AVAILABLE PAYMENT METHODS FOR THE CURRENT DEVICE AND ACCOUNT
		*/
		available_methods: {
			visibility: "public",
			params: {
				account_id: { type: "number", integer: true, positive: true, min: 1 },
			},
			handler(ctx) {
				return ctx
					.call("current_account.accounts_payments_methods.find", {
						fields: ["payment_method_id"],
						query: {
							device_id: ctx.meta.device.id,
							account_id: ctx.params.account_id,
						},
					})
					.then((payment_methods_account) => {
						const ids = payment_methods_account.map((pma) => pma.payment_method_id);

						return ctx.call("payments.payment-methods.find", {
							withRelated: false,
							fields: ["id", "name", "tag"],
							query: {
								id: ids,
							},
						});
					});
			},
		},
		treePaymentMethodDevice: {
			visibility: "public",
			params: {
				account_id: { type: "number", integer: true, positive: true, min: 1, convert: true },
			},
			async handler(ctx) {
				const devices = await ctx.call("configuration.devices.find", {
					fields: ["id", "uuid", "name", "type"],
					withRelated: false,
				});
				const payment_methods = await ctx.call("payments.payment-methods.find", {
					withRelated: false,
					fields: ["id", "name", "tag"],
				});

				const payment_methods_account = await ctx.call(
					"current_account.accounts_payments_methods.find",
					{
						fields: ["payment_method_id", "device_id"],
						query: {
							account_id: ctx.params.account_id,
						},
					},
				);

				devices.forEach((device) => {
					device.payment_methods = _.cloneDeep(payment_methods);

					device.payment_methods.forEach((payment_method) => {
						const finded = payment_methods_account.find(
							(pma) => pma.device_id === device.id && pma.payment_method_id === payment_method.id,
						);
						payment_method.active = !!finded;
					});
				});
				return devices;
			},
		},

		updatePaymentMethodDevice: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true, convert: true },
				device_id: { type: "number", positive: true, convert: true },
				payment_method_id: { type: "uuid", version: 4, optional: false },
				active: { type: "boolean" },
			},
			async handler(ctx) {
				if (ctx.params.active) {
					return ctx
						.call("current_account.accounts_payments_methods.create", {
							account_id: ctx.params.account_id,
							device_id: ctx.params.device_id,
							payment_method_id: ctx.params.payment_method_id,
						})
						.then((result) => {
							return ctx.call("current_account.accounts_payments_methods.treePaymentMethodDevice", {
								account_id: ctx.params.account_id,
							});
						});
				} else {
					return ctx
						.call("current_account.accounts_payments_methods.find", {
							fields: ["id"],
							limit: 1,
							query: {
								account_id: ctx.params.account_id,
								device_id: ctx.params.device_id,
								payment_method_id: ctx.params.payment_method_id,
							},
						})
						.then((result) => {
							if (result.length > 0) {
								return ctx.call("current_account.accounts_payments_methods.remove", {
									id: result[0].id,
								});
							}

							return true;
						})
						.then((result) => {
							return ctx.call("current_account.accounts_payments_methods.treePaymentMethodDevice", {
								account_id: ctx.params.account_id,
							});
						});
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
