"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.cash_account",
	table: "cash_account",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "cash_account")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "code", "description", "iban", "created_at", "updated_at"],

		defaultWithRelateds: [],

		withRelateds: {
			users(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("current_account.cash_account_users.users_of_cash_account", {
								cash_account_id: doc.id,
							})
							.then((res) => (doc.users = res || []));
					}),
				);
			},
		},

		entityValidator: {
			code: "string|max:6",
			description: "string|max:250|optional",
			iban: { type: "string", max: 34, optional: true },
			created_at: "date|convert",
			updated_at: "date|convert",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to create",
							"ONLY_BACKOFFICE_CAN_CREATE",
						);
					}

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to update",
							"ONLY_BACKOFFICE_CAN_UPDATE",
						);
					}
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to update",
							"ONLY_BACKOFFICE_CAN_UPDATE",
						);
					}
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to remove",
							"ONLY_BACKOFFICE_CAN_REMOVE",
						);
					}
				},
				"deleteRelatedUsers",
			],
		},
		after: {
			create: ["saveUsers"],
			update: ["saveUsers"],
			patch: ["saveUsers"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		available_me: {
			visibility: "published",
			rest: "GET /available-me",
			scope: "current_account:cash_account:read",
			params: {},
			handler(ctx) {
				return ctx
					.call("current_account.cash_account_users.find", {
						query: {
							user_id: ctx.meta.user.id,
						},
					})
					.then((cash_accounts) => {
						const ids = cash_accounts.map((ca) => ca.cash_account_id);

						return ctx.call("current_account.cash_account.find", {
							withRelated: false,
							fields: ["id", "code", "description"],
							query: {
								id: ids,
							},
						});
					});
			},
		},
		cashAccountClose: {
			visibility: "published",
			rest: "GET /cashAccountClose",
			scope: "current_account:cash_account:read",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}
				ctx.params.query = ctx.params.query ? ctx.params.query : {};

				let params = ctx.params;
				Object.assign(params.query, {
					query: {
						//created_by: ctx.meta.user.id,
						operation: { in: ["CHARGE", "REFUND"] },
					},
				});

				if (!ctx.meta.makeItFind)
					return ctx.call("current_account.movements.list", params).then(async (res) => {
						let _res = res;
						const totalSummary = await this.getSummaryTotal(ctx);

						_res.rows.map((item) => Object.assign(item, { totalSummary }));

						return _res;
					});
				else return ctx.call("current_account.movements.find", params);
			},
		},
		cashAccountCloseReport: {
			visibility: "published",
			rest: "GET /cashAccountCloseReport",
			scope: "current_account:cash_account:read",
			params: {},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const userCashAccount = await ctx.call("authorization.users.get", {
					id: ctx.meta.user.id,
					fields: ["id", "name", "email"],
				});

				const data = {
					user: userCashAccount[0],
				};

				if (ctx.params.query && ctx.params.query.cash_account_id) {
					data.cash_account = (
						await ctx.call("current_account.cash_account.get", {
							id: ctx.params.query.cash_account_id,
						})
					)[0];
				}

				ctx.params.fields = [
					"created_at",
					"entity",
					"tin",
					"operation",
					"doc_type_acronym",
					"seq_doc_num",
					"transaction_value",
				];
				ctx.params.withRelated = ["payment_method", "cash_account", "created_by"];

				ctx.meta.makeItFind = true;
				data.movements = await ctx.call(
					"current_account.cash_account.cashAccountClose",
					ctx.params,
				);

				data.summary = 0;

				if (data.movements.length) {
					data.summary = data.movements.reduce(
						(acc, currVal) => acc + currVal.transaction_value,
						0,
					);
				}

				return ctx.call("reports.templates.print", {
					key: "CC_CASH_ACCOUNT_CLOSE_REPORT",
					options: {
						//convertTo: "pdf"
					},
					data,
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveUsers(ctx, res) {
			this.logger.info("##### saveUsers #### ");

			if (ctx.params.users && Array.isArray(ctx.params.users)) {
				const users_ids = [...new Set(ctx.params.users)];

				this.logger.info("users_ids: ");
				this.logger.info(users_ids);

				await ctx.call("current_account.cash_account_users.save_users", {
					cash_account_id: res[0].id,
					users_ids,
				});
				res[0].users = await ctx.call("current_account.cash_account_users.users_of_cash_account", {
					cash_account_id: res[0].id,
				});
			}
			return res;
		},
		async deleteRelatedUsers(ctx, res) {
			typeof res;
			return ctx
				.call("current_account.cash_account_users.find", {
					query: {
						cash_account_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (rel) =>
							await ctx.call("current_account.cash_account_users.remove", { id: rel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete CashAccount related users!", err));
		},
		async getSummaryTotal(ctx) {
			let params = ctx.params;
			Object.assign(params.query, {
				query: {
					created_by: ctx.meta.user.id,
					operation: { in: ["CHARGE", "REFUND"] },
					status: { in: ["CONFIRMED"] },
				},
			});

			const resultQueryItems = await ctx.call("current_account.movements.find", params);

			let totalSummary = 0;

			if (resultQueryItems.length) {
				totalSummary = resultQueryItems.reduce(
					(acc, currVal) => acc + currVal.transaction_value,
					0,
				);
			}

			return totalSummary;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
