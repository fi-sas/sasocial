"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.cash_account_users",
	table: "cash_account_users",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "cash_account_users")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"cash_account_id",
			"user_id",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: [],

		withRelateds: {
		},

		entityValidator: {
			cash_account_id: { type: "number", positive: true, integer: true, convert: true },
			user_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to update",
							"ONLY_BACKOFFICE_CAN_UPDATE",
						);
					}

					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		users_of_cash_account: {
			rest: {
				method: "GET",
				path: "/:cash_account_id/users"
			},
			params: {
				cash_account_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {

				return this._find(ctx, {
					query: {
						cash_account_id: ctx.params.cash_account_id,
					},
				}).then((res) => {
					return ctx.call("authorization.users.find", {
						fields: ["id", "name", "email", "can_access_BO", "profile_id"],
						query: {
							id: res.map((ca) => ca.user_id),
						},
					});
				});
			}
		},
		save_users: {
			params: {
				cash_account_id: { type: "number", positive: true, integer: true, convert: true },
				users_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					cash_account_id: ctx.params.cash_account_id
				});
				this.clearCache();

				const entities = ctx.params.users_ids.map(user_id => ({
					cash_account_id: ctx.params.cash_account_id,
					user_id,
				}));

				return this._insert(ctx, { entities });
			}
		},
		remove_relation_cash_account_user: {
			cache: {
				keys: ["cash_account_id", "user_id"],
				ttl: 60
			},
			rest: {
				method: "DELETE",
				path: "/:cash_account_id/:user_id"
			},
			params: {
				cash_account_id: {
					type: "number", integer: true, positive: true, convert: true
				},
				user_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {
				return this.removeRelationsBetweenCashAccountAndUser(ctx);
			}
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		removeRelationsBetweenCashAccountAndUser(ctx) {
			return this.adapter.find({
				query: (q) => {
					q.where({
						cash_account_id: ctx.params.cash_account_id,
						user_id: ctx.params.user_id
					})
						.del();
					return q;
				}
			})
				.then(docs => this.transformDocuments(ctx, ctx.params, docs));
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
