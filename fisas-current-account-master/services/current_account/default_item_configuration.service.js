"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.default_item_configuration",
	table: "default_item_configuration",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "default_item_configuration")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"parameter_account_id",
			"parameter_operation",
			"parameter_service_id",
			"product_code",
			"name",
			"description",
			"vat_id",
			"erp_vat",
			"erp_budget",
			"erp_sncap",
			"erp_income_cost_center",
			"erp_funding_source",
			"erp_program",
			"erp_measure",
			"erp_project",
			"erp_activity",
			"erp_action",
			"erp_functional_classifier",
			"erp_organic",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: [],

		withRelateds: {},

		entityValidator: {
			parameter_account_id: "number|integer|convert",
			parameter_operation: "string|max:255|optional",
			parameter_service_id: "number|integer|convert|optional",
			product_code: "string|max:120",
			name: "string|max:255|optional",
			description: "string|max:250|optional",
			vat_id: "number|integer|convert",
			erp_budget: "string|max:250|optional",
			erp_sncap: "string|max:250|optional",
			erp_income_cost_center: "string|max:250|optional",
			erp_funding_source: "string|max:250|optional",
			erp_program: "string|max:250|optional",
			erp_measure: "string|max:250|optional",
			erp_project: "string|max:250|optional",
			erp_activity: "string|max:250|optional",
			erp_action: "string|max:250|optional",
			erp_functional_classifier: "string|max:250|optional",
			erp_organic: "string|max:250|optional",
			created_at: "date|optional|convert",
			updated_at: "date|optional|convert",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		update: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
