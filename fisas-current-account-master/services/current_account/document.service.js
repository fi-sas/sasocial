"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { DOCUMENT_TYPE } = require("../utils/current_account_constants");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.documents",
	table: "document",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "documents")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"type",
			"series",
			"number",
			"issued_at",
			"url",
			"document_erp_id",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: [],

		withRelateds: {},

		entityValidator: {
			type: { type: "enum", values: DOCUMENT_TYPE, optional: false },
			series: { type: "string", max: 120, optional: false },
			number: { type: "number", optional: false },
			issued_at: { type: "date", optional: false },
			url: { type: "string", optional: true },
			document_erp_id: { type: "number", positive: true, convert: true, optional: false },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
