"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { MOVEMENT_OPERATION } = require("../utils/current_account_constants");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.document_types",
	table: "document_types",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "document_types")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"description",
			"doc_type_acronym",
			"operation",
			"active",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: [],

		withRelateds: {},

		entityValidator: {
			name: "string|max:45",
			description: "string|max:250|optional",
			doc_type_acronym: "string|max:3",
			operation: { type: "enum", values: MOVEMENT_OPERATION, optional: false },
			active: "boolean",
			created_at: "date|convert",
			updated_at: "date|convert",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						throw new Errors.ForbiddenError(
							"Only backoffice are allowed to update",
							"ONLY_BACKOFFICE_CAN_UPDATE",
						);
					}

					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
