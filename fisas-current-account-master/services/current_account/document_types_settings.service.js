"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.document_types_settings",
	table: "document_types_settings",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "document_types_settings")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "account_id", "document_type_id", "send_to_erp", "updated_at", "created_at"],

		defaultWithRelateds: ["document_types"],

		withRelateds: {
			document_types(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"current_account.document_types",
					"document_types",
					"document_type_id",
					"id",
					{},
					"id,name,description,doc_type_acronym,operation,active",
					false,
				);
			},
		},

		entityValidator: {
			account_id: { type: "number", integer: true, positive: true },
			document_type_id: { type: "number", integer: true, positive: true },
			send_to_erp: { type: "boolean", convert: true, optional: true, default: false },
			updated_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function validateIfUniq(ctx) {
					const total = await ctx.call("current_account.document_types_settings.count", {
						query: {
							document_type_id: ctx.params.document_type_id,
							account_id: ctx.params.account_id,
						},
					});

					if (total > 0) {
						throw new Errors.ValidationError(
							"DocumentType already associated to account",
							"CC_DOCUMENT_TYPE_ALREADY_ASSOCIATED_ACCOUNT",
							{},
						);
					}
				},
			],
			update: [],
			patch: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		available_document_types: {
			visibility: "public",
			params: {
				account_id: { type: "number", integer: true, positive: true, min: 1 },
			},
			handler(ctx) {
				return ctx
					.call("current_account.document_types_settings.find", {
						fields: ["document_type_id"],
						query: {
							account_id: ctx.params.account_id,
						},
					})
					.then((document_types_settings) => {
						const ids = document_types_settings.map((dts) => dts.document_type_id);

						return ctx.call("current_account.document_types.find", {
							withRelated: false,
							fields: ["id", "name", "description", "doc_type_acronym", "operation"],
							query: {
								id: ids,
							},
						});
					});
			},
		},
		saveMiddlewareAvailableDocumentTypes: {
			params: {
				account_id: { type: "number", positive: true, integer: true, convert: true },
				doc_types_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					account_id: ctx.params.account_id,
				});
				this.clearCache();

				const entities = ctx.params.doc_types_ids.map((document_type_id) => ({
					account_id: ctx.params.account_id,
					send_to_erp: true,
					document_type_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
