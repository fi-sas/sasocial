"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

const { Errors } = require("@fisas/ms_core").Helpers;
const { MoleculerClientError } = require("moleculer").Errors;
const {
	MOVEMENT_OPERATION,
	MOVEMENT_STATUS,
	MIDDLEWARE_ERP_STATUS,
} = require("../utils/current_account_constants");
const _ = require("lodash");
const { roundMoney } = require("../utils/operations");
const { v4: uuidv4 } = require("uuid");
require("dotenv").config();
const Stream = require("stream");
const Cron = require("moleculer-cron");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.movements",
	table: "movement",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "movements"), Cron],

	/*
	 * Remove old carts
	 */
	crons: [
		{
			name: "checkMovementsToSendERP",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("current_account.movements")
					.actions.checkMovementsToSendERP()
					.then(() => { });
			},
			runOnInit: false, //function () {},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		MS_DEBUG: process.env.MS_DEBUG == "true",
		fields: [
			"id",
			"account_id",
			"document_id",
			"operation",
			"doc_type_acronym",
			"seq_doc_num",
			"device_id",
			"user_id",
			"entity",
			"tin",
			"email",
			"address",
			"postal_code",
			"city",
			"country",
			"description",
			"is_immediate",
			"transaction_id",
			"transaction_value",
			"current_balance",
			"owing_value",
			"paid_value",
			"original_movement_id",
			"status",
			"affects_balance",
			"payment_id",
			"payment_method_id",
			"payment_method_name",
			"expiration_at",
			"cash_account_id",
			"created_by",
			"device",
			"created_at",
			"updated_at",

			"associated_documents",

			"document_erp_id",
			"document_erp_number",
			"document_erp_url",
			"middleware_erp_status",
			"middleware_erp_result",
		],

		defaultWithRelateds: [
			"items",
			"document",
			"payment_method",
			"payment",
			"cash_account",
			"created_by",
			"device",
			"associated_documents",
		],

		withRelateds: {
			items(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "current_account.items", "items", "id", "movement_id");
			},
			document(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "current_account.documents", "document", "document_id");
			},
			payment_method(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"payments.payment-methods",
					"payment_method",
					"payment_method_id",
					"id",
					{},
					"id,name,tag",
					false,
				);
			},
			account(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "current_account.accounts", "account", "account_id");
			},
			payment(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "payments.payments", "payment", "payment_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			cash_account(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"current_account.cash_account",
					"cash_account",
					"cash_account_id",
					"id",
					{},
					"id,code,description",
					false,
				);
			},
			created_by(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "created_by", "created_by").then(() => {
					docs.map((doc) => {
						if (doc.created_by)
							doc.created_by = _.pick(doc.created_by, ["id", "name" /*, "email"*/]);
					});
				});
			},
			device(ids, docs, rule, ctx) {
				/*return hasOne(docs, ctx, "configuration.devices", "device", "device_id").then(() => {
					docs.map((doc) => {
						if (doc.device)
							doc.device = _.pick(doc.device, ["id", "uuid", "name", "type"]);
					});
				});*/ /* OLD WAY */
				return hasOne(
					docs,
					ctx,
					"configuration.devices",
					"device",
					"device_id",
					"id",
					{},
					["id", "uuid", "name"],
					false,
				);
			},
			associated_documents(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"current_account.movements",
					"associated_documents",
					"original_movement_id",
					"id",
					{},
					"id,paid_value,transaction_value,document_erp_id,document",
					false,
				);
			},
		},

		entityValidator: {
			//service_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			account_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			document_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			operation: { type: "enum", values: MOVEMENT_OPERATION, optional: false },

			doc_type_acronym: { type: "string", max: 3, optional: true },
			seq_doc_num: { type: "number", integer: true, positive: true, min: 1, optional: true },

			device_id: { type: "number", integer: true, min: 1, optional: true },

			user_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			entity: { type: "string", max: 120, optional: true },
			tin: { type: "string", max: 9, optional: true },
			email: { type: "string", max: 120, optional: true },
			address: { type: "string", max: 250, optional: true },
			postal_code: { type: "string", max: 10, optional: true },
			city: { type: "string", max: 250, optional: true },
			country: { type: "string", max: 250, optional: true },

			description: { type: "string", max: 250, optional: true },

			transaction_id: { type: "uuid", version: 4, optional: true },
			transaction_value: { type: "number", min: 0, optional: false },

			cash_account_id: { type: "number", integer: true, positive: true, min: 1, optional: true },

			current_balance: { type: "number", optional: true },

			paid_value: { type: "number", optional: true },
			advanced_value: { type: "number", optional: true },
			owing_value: { type: "number", min: 0, optional: true },

			original_movement_id: { type: "uuid", version: 4, optional: true },

			status: { type: "enum", values: MOVEMENT_STATUS, optional: false },
			affects_balance: { type: "boolean", optional: false },
			payment_id: { type: "uuid", version: 4, optional: true },
			payment_method_id: { type: "uuid", version: 4, optional: true },
			payment_method_name: { type: "string", max: 120, optional: true },
			expiration_at: { type: "date", optional: true },

			created_by: { type: "number", integer: true, positive: true, min: 1, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },

			document_erp_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			document_erp_number: { type: "string", optional: true },
			document_erp_url: { type: "string", max: 256, optional: true },
			middleware_erp_status: { type: "enum", values: MIDDLEWARE_ERP_STATUS, optional: true },
			middleware_erp_result: { type: "string", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			list: [
				"sanitizeQuery",
				function defaultSort(ctx) {
					if (!ctx.params.sort) {
						ctx.params.sort = "-seq_doc_num";
					}
				},
				function sanitizeChargesQuery(ctx) {
					let operation = { nin: ["CHARGE_NOT_IMMEDIATE"] };

					if (ctx.params.query.operation) {
						Object.assign(operation, { in: _.flattenDeep([ctx.params.query.operation]) });
					}
					ctx.params.query.operation = operation;
				},
			],
			create: [
				function sanitizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.transaction_id = ctx.params.transaction_id
						? ctx.params.transaction_id
						: uuidv4();

					if (ctx.meta.user) {
						ctx.params.created_by = ctx.meta.user.id;
					}

					//SET BY DATABASE (DEFAULT CURRENT_TIMESTAMP)
					//ctx.params.created_at = new Date();
					//ctx.params.updated_at = new Date();
				},
				async function checkBypassBalanceWithInvoice(ctx) {
					if (
						ctx.params.bypass_balance_with_invoice &&
						ctx.params.operation === "INVOICE_RECEIPT"
					) {
						ctx.meta.bypass_bo_validation = true;
						this.logger.info("###### checkBypassBalanceWithInvoice ####");
						this.logger.info(ctx.params);

						const account = await ctx.call("current_account.movements.balance", {
							user_id: ctx.params.user_id,
							account_id: ctx.params.account_id,
						});

						let _items = await Promise.all(
							ctx.params.items.map((mvItem) => this.calculateMovementItemValues(ctx, mvItem)),
						);

						let transactionValue = 0;
						_items.map((item) => {
							transactionValue += item.total_value;
						});

						const haveFunds =
							parseFloat(account.current_balance) +
							parseFloat(account.account_plafond_value || 0) >=
							transactionValue;

						this.logger.warn(
							`  - bypass_balance_with_invoice: ${ctx.params.bypass_balance_with_invoice}`,
						);
						this.logger.info("  - funds:", account.current_balance);
						this.logger.info("  - transactionValue:", transactionValue);
						this.logger.info("  - haveFunds:", haveFunds);

						if (!haveFunds) {
							ctx.params.operation = "INVOICE";
						}
					}
					delete ctx.params.bypass_balance_with_invoice;
				},
				"validateParams",
				function initDefaultAmounts(ctx) {
					// init transaction_value, will be updated later
					ctx.params.transaction_value = 0;
					// init current_balance, will be updated later
					ctx.params.current_balance = 0;
					// init owing_value, will be updated later
					ctx.params.owing_value = 0;
					// init paid_value, will be updated later
					ctx.params.paid_value = 0;
					// init paid_value, will be updated later
					ctx.params.advanced_value = 0;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.isUpdate = true;
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.isUpdate = true;
				},
			],
			createPayment: [
				function sanatizeParams(ctx) {
					if (
						!(ctx.meta.isBackoffice || (ctx.meta.device && ctx.meta.device.type === "POS")) &&
						!ctx.params.payment_method_id
					) {
						ctx.params.payment_method_id = process.env.DEFAULT_PAYMENT_METHOD_ID;
					}
				},
			],
			createPartialPayment: [
				function sanatizeParams(ctx) {
					if (
						!(ctx.meta.isBackoffice || (ctx.meta.device && ctx.meta.device.type === "POS")) &&
						!ctx.params.payment_method_id
					) {
						ctx.params.payment_method_id = process.env.DEFAULT_PAYMENT_METHOD_ID;
					}
				},
			],
			cancel: [
				function sanatizeParams(ctx) {
					if (this.settings.MS_DEBUG) {
						this.logger.info("   1. ctx.params:", ctx.params);
					}
					if (ctx.params.movement_item_id) {
						ctx.params.items = _.flattenDeep([ctx.params.movement_item_id]);

						if (this.settings.MS_DEBUG) {
							this.logger.info("   -> existe movement_item_id");
							this.logger.info("   -> movement_item_id é um uuid:");
						}
					}

					if (this.settings.MS_DEBUG) {
						this.logger.info("   2. ctx.params:", ctx.params);
					}
				},
			],
		},
		after: {
			create: [
				async function sendToErp(ctx, res) {
					if (this.settings.MS_DEBUG) {
						this.logger.info("#### ######### ####");
						this.logger.info("#### sendToErp ####");
						this.logger.info("  - ctx.params:");
						this.logger.info(ctx.params);
					}
					return this.addToSendErpQueue(ctx, res);
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		fake_service_confirm: {
			visibility: "public",
			rest: "POST /fake_service_confirm",
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info();
					this.logger.info("##### --- #################### --- ####");
					this.logger.info("##### --- fake_service_confirm --- ####");
					this.logger.info("##### --- #################### --- ####");
					this.logger.info();
				}
				return true;
			},
		},
		fake_service_cancel: {
			visibility: "public",
			rest: "POST /fake_service_confirm",
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info();
					this.logger.info("##### --- #################### --- ####");
					this.logger.info("##### --- fake_service_cancel  --- ####");
					this.logger.info("##### --- ### params: ");
					this.logger.info(ctx.params);
					this.logger.info("##### --- #################### --- ####");
					this.logger.info();
				}
				return true;
			},
		},
		cancel: {
			visibility: "published",
			rest: "POST /cancel",
			scope: "current_account:movements:cancel",
			params: {
				movement_id: { type: "uuid", version: 4, optional: false },
				items: {
					type: "array",
					item: { type: "uuid", version: 4, optional: true },
					optional: true,
				},
			},
			async handler(ctx) {
				let user_id = ctx.meta.user.id;

				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					throw new Errors.ForbiddenError(
						"Only backoffice can cancel by user_id",
						"CC_ONLY_BACKOFFICE_CAN_CANCEL_BY_USER_ID",
					);
				}

				if ((ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					user_id = ctx.params.user_id;
				}

				const movementToCancel = await ctx.call("current_account.movements.get", {
					id: ctx.params.movement_id,
					withRelated: ["items"],
				});

				let description = `Cancelamento doc.: ${movementToCancel[0].doc_type_acronym}${movementToCancel[0].seq_doc_num}`;
				if (ctx.params.description) {
					description = ctx.params.description;
				}
				if (
					movementToCancel[0].operation === "INVOICE_RECEIPT" ||
					movementToCancel[0].operation === "INVOICE"
				) {
					if (movementToCancel[0].operation === "INVOICE_RECEIPT") {
						// Get transaction history
						const transactionHistory = await ctx.call("current_account.movements.find", {
							query: {
								transaction_id: movementToCancel[0].transaction_id,
							},
							sort: "-created_at",
						});

						// NOTAS DE CRÉDITO EXISTENTES RELACIONADAS AO MOVIMENTO A CANCELAR
						const creditNoteMovements = _.filter(transactionHistory, ["operation", "CREDIT_NOTE"]);
						let totalHistoryCreditNotes = _.reduce(
							creditNoteMovements,
							function (sum, n) {
								return sum + parseFloat(n.transaction_value);
							},
							0,
						);

						if (totalHistoryCreditNotes >= Math.abs(movementToCancel[0].transaction_value)) {
							throw new Errors.ValidationError(
								"The total cancelled value is greater than the original movement value",
								"CC_CANCEL_VALUE_GREATER",
								{
									movement_id: ctx.params.movement_id,
								},
							);
						}
					}

					// Set/filter items to cancel: if no items provided, ALL items from "movementToCancel" will be cancelled
					let itemsToCancel = movementToCancel[0].items;
					if (ctx.params.items) {
						const itemIdsToCancel = ctx.params.items;
						itemsToCancel = _.filter(itemsToCancel, function (o) {
							return _.includes(itemIdsToCancel, o.id);
						});
					}

					// Prepares the CANCEL movement/movement-item
					let dummyMovement = Object.assign(
						{},
						{
							id: uuidv4(),
							account_id: movementToCancel[0].account_id,
							operation: "CREDIT_NOTE",
							user_id: movementToCancel[0].user_id,
							description: description,
							status: "PAID",
							transaction_id: movementToCancel[0].transaction_id,
							original_movement_id: movementToCancel[0].id,

							payment_method_id: movementToCancel[0].payment_method_id,
							payment_method_name: movementToCancel[0].payment_method_name,

							items: itemsToCancel.map((itc) =>
								_.pick(itc, [
									"service_id",
									"product_code",
									"name",
									"description",
									"extra_info",
									"quantity",
									"liquid_unit_value",
									"unit_value",
									"discount_value",
									"vat_id",
									"location",
									"article_type",
									"service_confirm_path",
									"service_cancel_path",
								]),
							),
						},
					);

					// Creates the CANCEL movement prior the movement request
					return ctx
						.call("current_account.movements.create", dummyMovement)
						.then(async (cancelMovement) => {
							// CREDIT_NOTE

							let isFullyPaid = false;
							let cancelledMovement = await ctx.call("current_account.movements.get", {
								id: ctx.params.movement_id,
								fields: ["transaction_value", "paid_value"],
								withRelated: false,
							});
							const newStatus =
								Math.abs(cancelledMovement[0].transaction_value) -
									Math.abs(cancelledMovement[0].paid_value) ==
									0
									? "PAID"
									: "PENDING";

							return ctx
								.call("current_account.movements.patch", {
									id: ctx.params.movement_id,
									status: newStatus,
									withRelated: ["items"],
								})
								.then((resultCanceledMovement) => {
									let canceledToReturn = resultCanceledMovement;

									Object.assign(canceledToReturn[0], { movement_cancel_id: cancelMovement[0].id });

									return canceledToReturn;
								});
						});
				}

				throw new Errors.ValidationError(
					"You cant cancel this type of movement",
					"CC_CANT_CANCEL_TYPE_MOVEMENT",
					{
						id: ctx.params.movement_id,
					},
				);
			},
		},
		partialCancel: {
			visibility: "published",
			rest: "POST /partialCancel",
			scope: "current_account:movements:cancel",
			params: {
				movement_id: { type: "uuid", version: 4, optional: false },
				items: {
					type: "array",
					items: {
						type: "object",
						strict: "remove",
						props: {
							id: {
								type: "uuid",
								version: 4,
								optional: false,
							},
							quantity: {
								type: "number",
								integer: true,
								positive: true,
								convert: true,
								optional: false,
							},
							unit_value: { type: "number", min: 0, convert: true, optional: true },
						},
					},
				},
			},
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info();
					this.logger.info("##### ---- ############################### ---- ####");
					this.logger.info("##### ---   partial cancellation movement   --- ####");
					this.logger.info("##### ---- ############################### ---- ####");
					this.logger.info();
				}

				// MOVIMENTO A CANCELAR
				const movementToCancel = await ctx.call("current_account.movements.get", {
					id: ctx.params.movement_id,
					withRelated: ["items"],
				});

				/***
				 * INITALIZA THE ITEMS CTX PARAMS
				 */
				ctx.params.items.forEach((item) => {
					const temProductCode = movementToCancel[0].items.find((i) => i.id === item.id);
					if (!temProductCode) {
						throw new Errors.ValidationError(
							"No product with the id has found on the original movement",
							"CC_CANCEL_PRODUCT_ID_UNMATCH",
							{
								item_id: item.id,
							},
						);
					}

					if (item.unit_value) {
						item.total_value = item.quantity * item.unit_value;
					} else {
						item.unit_value = temProductCode.unit_value;
						item.total_value = item.quantity * item.unit_value;
					}
					item.product_code = temProductCode.product_code;
				});

				// TODO
				// CHECK IF THE ITEM TO CANCEL IF NOT GREATER THAN THE VALUE CREATED

				// Get transaction history
				const transactionHistory = await ctx.call("current_account.movements.find", {
					query: {
						transaction_id: movementToCancel[0].transaction_id,
					},
					sort: "-created_at",
				});

				// NOTAS DE CRÉDITO EXISTENTES RELACIONADAS AO MOVIMENTO A CANCELAR
				const creditNoteMovements = _.filter(transactionHistory, ["operation", "CREDIT_NOTE"]);
				let totalHistoryCreditNotes = _.reduce(
					creditNoteMovements,
					function (sum, n) {
						return sum + parseFloat(n.transaction_value);
					},
					0,
				);

				// CREDIT NOTES
				// 1 - Get all cancelled items
				let itemAlreadyCancelled = [];
				creditNoteMovements.forEach((creditNote) => itemAlreadyCancelled.push(...creditNote.items));

				// 2 - check if exist a item from ctx.params already cancelled
				const ctxItems = movementToCancel[0].items.filter((item) =>
					ctx.params.items.find((ctxItem) => ctxItem.id === item.id),
				);
				itemAlreadyCancelled = itemAlreadyCancelled.filter((itemCancelled) =>
					ctxItems.find((ctxItem) => ctxItem.product_code === itemCancelled.id),
				);

				let itemAlreadyCancelledTotalValues = [];
				itemAlreadyCancelled.forEach((item) => {
					if (!itemAlreadyCancelledTotalValues[item.id]) {
						itemAlreadyCancelledTotalValues[item.id] = 0.0;
					}

					itemAlreadyCancelledTotalValues[item.id] += item.total_value;
				});

				// 3 - Confirm the value of the cancelled + to cancel is not greater than the original movement item
				ctx.params.items.forEach((item) => {
					const originalItem = movementToCancel[0].items.find((i) => i.id === item.id);

					if (!originalItem) {
						// SOMETHING WEIRD HAPPENED
					}

					if (!itemAlreadyCancelledTotalValues[item.id]) {
						itemAlreadyCancelledTotalValues[item.id] = 0;
					}

					if (
						item.total_value + itemAlreadyCancelledTotalValues[item.id] >
						originalItem.total_value
					) {
						throw new Errors.ValidationError(
							"The total cancelled item value is greater than the original movement item value",
							"CC_CANCEL_PRODUCT_ITEM_VALUE_GREATER",
							{
								item_id: item.id,
							},
						);
					}
				});

				let description = `Cancelamento doc.: ${movementToCancel[0].doc_type_acronym}${movementToCancel[0].seq_doc_num}`;
				if (ctx.params.description) {
					description = ctx.params.description;
				}

				if (
					movementToCancel[0].operation === "INVOICE_RECEIPT" ||
					movementToCancel[0].operation === "INVOICE"
				) {
					//const movementToCancelStatus = movementToCancel[0].status;
					const itemsToCancel = movementToCancel[0].items.filter((item) =>
						ctx.params.items.find((ctxItem) => ctxItem.id === item.id),
					);

					// Prepares the CANCEL movement/movement-item
					let dummyMovement = Object.assign(
						{},
						{
							id: uuidv4(),
							account_id: movementToCancel[0].account_id,
							operation: "CREDIT_NOTE",
							user_id: movementToCancel[0].user_id,
							description: description,

							status: "PAID",
							transaction_id: movementToCancel[0].transaction_id,
							original_movement_id: movementToCancel[0].id,

							payment_method_id: movementToCancel[0].payment_method_id,
							payment_method_name: movementToCancel[0].payment_method_name,

							items: [
								// TODO WHERE I SEND THE DUPLICATE ITEM BUT WITH THE VALUE TO CANCEL
								...itemsToCancel.map((itemToCancel) => {
									const ctxItemToCancel = ctx.params.items.find(
										(ctxItem) => ctxItem.id === itemToCancel.id,
									);
									return {
										service_id: itemToCancel.service_id,
										product_code: itemToCancel.product_code,
										name: itemToCancel.name,
										description: description,
										vat_id: itemToCancel.vat_id,
										extra_info: itemToCancel.extra_info,
										quantity: ctxItemToCancel.quantity,
										unit_value: ctxItemToCancel.unit_value,
										location: ctxItemToCancel.location ? ctxItemToCancel.location : " ",
										article_type: ctxItemToCancel.article_type,
										service_confirm_path: "current_account.movements.fake_service_confirm",
										service_cancel_path: "current_account.movements.fake_service_cancel",
									};
								}),
							],
						},
					);

					// Creates the CANCEL (CreditNote) movement prior the movement request
					return ctx
						.call("current_account.movements.create", dummyMovement)
						.then(async (cancelMovement) => {
							// created CREDIT_NOTE
							return ctx
								.call("current_account.movements.patch", {
									id: ctx.params.movement_id,
									status: "PAID",
									withRelated: ["items"],
								})
								.then((resultCanceledMovement) => {
									let canceledToReturn = resultCanceledMovement;

									Object.assign(canceledToReturn[0], { movement_cancel_id: cancelMovement[0].id });

									this.logger.info(" -  cancel.canceledToReturn.4:");
									this.logger.info(canceledToReturn);

									return canceledToReturn;
								});
						});
				}
			},
		},
		user_report: {
			visibility: "published",
			rest: "GET /user-report",
			scope: "current_account:movements:read",
			params: {},
			async handler(ctx) {
				const data = {
					user: ctx.meta.user,
				};

				if (ctx.params.query && ctx.params.query.account_id) {
					data.account = (
						await ctx.call("current_account.accounts.get", { id: ctx.params.query.account_id })
					)[0];
				}

				data.movements = (await ctx.call("current_account.movements.list", ctx.params)).rows;

				return ctx.call("reports.templates.print", {
					key: "CC_USER_REPORT",
					options: {
						//convertTo: "pdf"
					},
					data,
				});
			},
		},
		user_report_history_charges: {
			visibility: "published",
			rest: "GET /user-report-history-charges",
			scope: "current_account:movements:read",
			params: {},
			async handler(ctx) {
				const data = {
					user: ctx.meta.user,
				};

				if (ctx.params.query && ctx.params.query.account_id) {
					data.account = (
						await ctx.call("current_account.accounts.get", { id: ctx.params.query.account_id })
					)[0];
				}

				data.movements = (
					await ctx.call("current_account.movements.charges_history", ctx.params)
				).rows;

				return ctx.call("reports.templates.print", {
					key: "CC_USER_REPORT_HISTORY_CHARGES",
					options: {
						//convertTo: "pdf"
					},
					data,
				});
			},
		},

		charges_period_report: {
			visibility: "published",
			rest: "GET /charges-period-report",
			scope: "current_account:movements:read",
			params: {
				start_date: { type: "string", optional: false },
				end_date: { type: "string", optional: false },
				account_id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const configurationDevices = await ctx.call("configuration.devices.find", {});

				let allDevicesList = configurationDevices.map((d) => `(${d.id},'${d.name}', '')`);

				const rawData = await this.adapter.raw(`
				-- ######################################## --
				-- CARREGAMENTOS: Group By SCHOOL and KIOSK --
				with chargesSummary (
				charges_date
				,account_id
				,school
				,payment_method_name
				,device
				,Sum_Of_Amount
				,Nb_Of_Charges) as
					 (
				select
					m.created_at::date "charges_date"
					,m.account_id "account_id"
					,split_part(t.name, '_', 1) "school"
					,m.payment_method_name "payment_method_name"
					,'['||m.device_id||'] '||t.name::varchar ||' ['||t.tpa||'] ' "device"
					,sum(m.transaction_value) "sum_of_amount"
					,count(*) "nb_of_charges"
				from
					movement m
				left join (
				values ${allDevicesList.join(",")}) as t (id,
					name, tpa) on
					t.id = m.device_id
				where
					(1 = 1)
					and m.device_id is not null
					and m.operation = 'CHARGE'
					and m.status = 'CONFIRMED'
					and m.created_at::date >= '${ctx.params.start_date}'::date
					and m.created_at::date <= '${ctx.params.end_date}'::date
					and m.account_id = ${ctx.params.account_id}
				group by
					m.created_at::date
					,m.account_id
					,school
					,m.payment_method_name
					,'['||m.device_id||'] '||t.name::varchar ||' ['||t.tpa||'] '
				order by created_at::date, device)
				select
					charges_date::varchar
					,account_id
					,school
					,payment_method_name
					,device
					,sum_of_amount
					,nb_of_charges
				from
					chargesSummary
				`);

				// GET ACCOUNT NAME
				const account = await ctx.call("current_account.accounts.get", {
					id: ctx.params.account_id,
					withRelated: false,
					fields: "id,name",
				});

				const data = {
					user: ctx.meta.user,
					filter: {
						start_date: ctx.params.start_date,
						end_date: ctx.params.end_date,
					},
					account: {
						name: account[0].name,
					},
				};
				data.movements = rawData.rows;

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "CC_CHARGES_PERIOD_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data,
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		charges_detailed_period_report: {
			visibility: "published",
			rest: "GET /charges-detailed-period-report",
			scope: "current_account:movements:read",
			params: {
				start_date: { type: "string", optional: false },
				end_date: { type: "string", optional: false },
				account_id: { type: "number", positive: true, convert: true },
				device_ids: {
					type: "array",
					item: { type: "number", positive: true, convert: true, optional: true },
					optional: true,
				},
				user_id: { type: "number", positive: true, convert: true, optional: true },
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const bindings = [ctx.params.start_date, ctx.params.end_date, ctx.params.account_id];

				if (ctx.params.device_ids) {
					bindings.push(...ctx.params.device_ids);
				}

				if (ctx.params.payment_method_id) {
					bindings.push(ctx.params.payment_method_id);
				}

				if (ctx.params.user_id) {
					bindings.push(ctx.params.user_id);
				}

				const configurationDevices = await ctx.call("configuration.devices.find", {});

				let allDevicesList = configurationDevices.map((d) => `(${d.id},'${d.name}', '')`);

				const rawData = await this.adapter.raw(
					`select
				m.created_at,
				m.operation as type,
				m.entity as user_name,
				m.status,
				pm."name" as payment_method_name ,
				t.name as device,
				m.transaction_value as value
			from
				movement m
			inner join payment_method pm on
				pm.id = m.payment_method_id
			left join (
							values ${allDevicesList.join(",")}) as t (id,
								name, tpa) on
								t.id = m.device_id
			where
				m.operation = 'CHARGE'
				and m.status = 'CONFIRMED'
				and m.created_at::date >= ?::date
				and m.created_at::date <= ?::date
				and m.account_id =?
				${ctx.params.device_ids
						? "and device_id in (" + ctx.params.device_ids.map(() => "?").join(",") + ")"
						: ""
					}
				${ctx.params.payment_method_id ? "and payment_method_id = ?" : ""}
				${ctx.params.user_id ? "and user_id = ?" : ""}
				order by m.created_at desc
				`,
					bindings,
				);

				//BEGIN GET USERS NAME
				const user_ids = [];
				rawData.rows.forEach((m) => {
					if (!user_ids.includes(m.user_id)) user_ids.push(m.user_id);
				});

				//END GET USERS NAME

				// GET ACCOUNT NAME
				const account = await ctx.call("current_account.accounts.get", {
					id: ctx.params.account_id,
					withRelated: false,
					fields: "id,name",
				});

				const data = {
					user: ctx.meta.user,
					filter: {
						start_date: ctx.params.start_date,
						end_date: ctx.params.end_date,
					},
					account: {
						name: account[0].name,
					},
				};
				data.movements = rawData.rows;

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "CC_CHARGES_DETAILED_PERIOD_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data,
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		movements_period_report: {
			visibility: "published",
			rest: "GET /movements-period-report",
			scope: "current_account:movements:read",
			params: {
				start_date: { type: "string", optional: false },
				end_date: { type: "string", optional: false },
				operations: {
					type: "array",
					item: { type: "enum", values: ["INVOICE_RECEIPT", "CREDIT_NOTE", "RECEIPT"] },
					optional: true,
				},
				user_id: { type: "number", positive: true, convert: true, optional: true },
				account_id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const configurationDevices = await ctx.call("configuration.devices.find", {});

				let allDevicesList = configurationDevices.map((d) => `(${d.id},'${d.name}', '')`);

				const bindings = [ctx.params.start_date, ctx.params.end_date, ctx.params.account_id];

				if (ctx.params.operations) {
					bindings.push(...ctx.params.operations);
				}

				if (ctx.params.user_id) {
					bindings.push(ctx.params.user_id);
				}

				const rawData = await this.adapter.raw(
					`
				-- ############################ --
				-- MOVIMENTOS: Device and ITEMS --
				 select
					x.created_at
					,'['||x.device_id||'] '||d.name as device
					,x.entity
					,x.operation
					,x.transaction_value
					,x.payment_method_name
					,x.status
					,x.middleware_erp_status
					,x.tin
					,x.email
					,x.entity
					,x.id
					,x.account_id
					,x.current_balance
					,doc.series||'/'||doc.number as erp_doc
					,x.user_id
					,x.middleware_erp_result
					,(
						select string_agg(' - '||mi.quantity||'x '||mi.name, E'\n') from movement_item mi
						where mi.movement_id = x.id

					) "ITEMS"
					,(
						select string_agg(' - '||mi.location||'/ '||mi.article_type, E'\n') from movement_item mi
						where mi.movement_id = x.id

					) "location"
					,x.address
					,x.postal_code
					,x.city
				from
					movement x
					left join (
				values ${allDevicesList.join(",")}) as d (id,
					name, tpa) on
					d.id = x.device_id
					left join document doc on doc.id = x.document_id
				where
					(1 = 1)
					and x.created_at::date >= ?::date
					and x.created_at::date <= ?::date
					and x.account_id = ?
					${ctx.params.operations
						? " and operation in (" + ctx.params.operations.map(() => "?").join(",") + ")"
						: " "
					}
					${ctx.params.user_id ? "and x.user_id = ?" : ""}
					and (x.operation != 'CHARGE_NOT_IMMEDIATE' or x.status in ('PENDING','CANCELLED'))
				order by
					x.created_at desc;
				`,
					bindings,
				);

				// GET ACCOUNT NAME
				const account = await ctx.call("current_account.accounts.get", {
					id: ctx.params.account_id,
					withRelated: false,
					fields: "id,name",
				});

				const data = {
					user: ctx.meta.user,
					filter: {
						start_date: ctx.params.start_date,
						end_date: ctx.params.end_date,
					},
					account: {
						name: account[0].name,
					},
				};
				data.movements = rawData.rows;

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "CC_MOVEMENTS_PERIOD_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data,
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},

		sales_resume_period_report: {
			visibility: "published",
			rest: "GET /sales-resume-period-report",
			scope: "current_account:movements:read",
			params: {
				start_date: { type: "string", optional: false },
				end_date: { type: "string", optional: false },
				account_id: { type: "number", positive: true, convert: true },
				location: { type: "string", optional: true },
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const bindings = [ctx.params.start_date, ctx.params.end_date, ctx.params.account_id];

				if (ctx.params.operations) {
					bindings.push(...ctx.params.operations);
				}

				let criteriaLocation = "";
				if (ctx.params.location) {
					criteriaLocation = `and mi.location='${ctx.params.location}' `;
				}

				const configurationServices = await ctx.call("configuration.services.find", {});
				let allServicesList = configurationServices.map(
					(s) => `(${s.id},'${s.translations.find((o) => o.language_id === 3).name}', '')`,
				);
				const rawData = await this.adapter.raw(
					`
				-- REPORT DE VENDAS # AGRUPADO POR DIA | SERVIÇO | LOCAL | OPERATION # --
				SELECT
					m.created_at::date,
					s.name "service_name",
					mi.location,
					m.operation,
					m.status,
					sum(CASE WHEN m.operation = 'CREDIT_NOTE'
								  THEN -mi.total_value
								  ELSE  mi.total_value
							 END)
				FROM movement m
				INNER JOIN movement_item mi ON mi.movement_id = m.id
				left join ( values
					${allServicesList.join(",")}) as s (id,name,description) on s.id = mi.service_id
				WHERE (1=1)
				and m.created_at::date >= ?::date
				and m.created_at::date <= ?::date
				and m.account_id = ?
				and operation in ('INVOICE', 'INVOICE_RECEIPT', 'CREDIT_NOTE', 'RECEIPT')
				--and position('FTR' in m.description)=0
				${criteriaLocation}
				GROUP BY m.created_at::date, s.name, mi.location, m.operation, m.status
				ORDER BY m.created_at::date
				`,
					bindings,
				);

				// GET ACCOUNT NAME
				const account = await ctx.call("current_account.accounts.get", {
					id: ctx.params.account_id,
					withRelated: false,
					fields: "id,name",
				});

				const data = {
					filter: {
						start_date: ctx.params.start_date,
						end_date: ctx.params.end_date,
					},
					account: {
						name: account[0].name,
					},
				};
				data.movements = rawData.rows;

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "CC_MOVEMENTS_SALES_RESUME_PERIOD_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data,
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},

		sales_detailed_period_report: {
			visibility: "published",
			rest: "GET /sales-detailed-period-report",
			scope: "current_account:movements:read",
			params: {
				start_date: { type: "string", optional: false },
				end_date: { type: "string", optional: false },
				operations: {
					type: "array",
					item: { type: "enum", values: ["INVOICE", "INVOICE_RECEIPT", "CREDIT_NOTE", "RECEIPT"] },
					optional: true,
				},
				device_ids: {
					type: "array",
					item: { type: "number", positive: true, convert: true, optional: true },
					optional: true,
				},
				service_id: { type: "number", positive: true, convert: true, optional: true },
				location: { type: "string", optional: true },
				user_id: { type: "number", positive: true, convert: true, optional: true },
			},
			async handler(ctx) {
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					throw new Errors.ForbiddenError(
						"Only backoffice/pos is allowed to this operation",
						"ONLY_BACKOFFICE_ALLOWED",
					);
				}

				const bindings = [ctx.params.start_date, ctx.params.end_date, ctx.params.account_id];

				if (ctx.params.operations) {
					bindings.push(...ctx.params.operations);
				}

				if (ctx.params.device_ids) {
					bindings.push(...ctx.params.device_ids);
				}

				if (ctx.params.service_id) {
					bindings.push(ctx.params.service_id);
				}

				if (ctx.params.user_id) {
					bindings.push(ctx.params.user_id);
				}

				let criteriaLocation = "";
				if (ctx.params.location) {
					criteriaLocation = "and mi.location= ? ";
					bindings.push(ctx.params.location);
				}

				const configurationServices = await ctx.call("configuration.services.find", {
					withRelated: "translations",
				});
				let allServicesList = configurationServices.map(
					(s) => `(${s.id},'${s.translations.find((o) => o.language_id === 3).name}', '')`,
				);

				const configurationDevices = await ctx.call("configuration.devices.find", {});
				let allDevicesList = configurationDevices.map((d) => `(${d.id},'${d.name}', '')`);

				const rawData = await this.adapter.raw(
					`
				-- NOT GROUPED -- REPORT DE VENDAS # AGRUPADO POR DIA | SERVIÇO | LOCAL | OPERATION # --
				SELECT m.created_at, s.name "service_name", m.user_id, m.status, d.name as device, mi.location, m.operation, CASE WHEN m.operation = 'CREDIT_NOTE'
								  THEN -mi.total_value
								  ELSE  mi.total_value
							 END
				FROM movement m
				INNER JOIN movement_item mi ON mi.movement_id = m.id
				left join ( values ${allServicesList.join(",")}) as s (id,name,description) on s.id = mi.service_id
				left join ( values ${allDevicesList.join(",")}) as d (id,name,description) on d.id = m.device_id
				WHERE (1=1)
				and m.created_at::date >= ?::date
				and m.created_at::date <= ?::date
				and m.account_id = ?
				and operation in ${ctx.params.operations
						? "(" + ctx.params.operations.map(() => "?").join(",") + ")"
						: " ('INVOICE', 'INVOICE_RECEIPT', 'CREDIT_NOTE', 'RECEIPT')"
					}
				${ctx.params.device_ids
						? "and device_id in (" + ctx.params.device_ids.map(() => "?").join(",") + ")"
						: ""
					}
				${ctx.params.service_id ? "and service_id = ?" : ""}
				${ctx.params.user_id ? "and user_id = ?" : ""}
				--and position('FTR' in m.description)=0
				${criteriaLocation}
				ORDER BY m.created_at::date, mi.location
				`,
					bindings,
				);

				//BEGIN GET USERS NAME
				const user_ids = [];
				rawData.rows.forEach((m) => {
					if (!user_ids.includes(m.user_id)) user_ids.push(m.user_id);
				});

				const users = await ctx.call("authorization.users.find", {
					query: {
						id: user_ids,
					},
					withRelated: false,
					fields: "id,name",
				});

				rawData.rows.forEach((m) => {
					const user = users.find((u) => u.id === m.user_id);
					if (user) m.user_name = user.name;
				});
				//END GET USERS NAME

				// GET ACCOUNT NAME
				const account = await ctx.call("current_account.accounts.get", {
					id: ctx.params.account_id,
					withRelated: false,
					fields: "id,name",
				});

				const data = {
					filter: {
						start_date: ctx.params.start_date,
						end_date: ctx.params.end_date,
					},
					account: {
						name: account[0].name,
					},
				};
				data.movements = rawData.rows;

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "CC_MOVEMENTS_SALES_DETAILED_PERIOD_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data,
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},

		charges_history: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
			visibility: "published",
			rest: "GET /chargesHistory",
			scope: "current_account:movements:charges-history",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			async handler(ctx) {
				ctx.params.query = ctx.params.query ? ctx.params.query : {};
				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
					ctx.params.query.user_id = ctx.meta.user.id;
				}

				//ctx.params.query.operation = ["CHARGE_NOT_IMMEDIATE", "CHARGE"];

				let operation = { in: ["CHARGE_NOT_IMMEDIATE" /*, "CHARGE"*/] };

				if (ctx.params.query.operation) {
					Object.assign(operation, { in: _.flattenDeep([ctx.params.query.operation]) });
				}
				ctx.params.query.operation = operation;

				this.sanitizeQuery(ctx, ctx.params);
				let params = this.sanitizeParams(ctx, ctx.params);
				return this._list(ctx, params);
			},
		},
		charge_account: {
			visibility: "published",
			rest: "POST /chargeAccount",
			scope: "current_account:accounts:charge",
			params: {
				account_id: { type: "number", integer: true, positive: true, optional: false },
				user_id: { type: "number", integer: true, positive: true, optional: true },
				payment_method_id: { type: "uuid", version: 4, optional: false },
				transaction_value: {
					type: "number",
					positive: true,
					convert: true,
					optional: false,
				},
				description: { type: "string", max: 250, optional: true },
				cash_account_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					this.logger.warn("");
					this.logger.warn(" chargeAccount -> isGuest: ", ctx.meta.isGuest);

					throw new MoleculerClientError(
						"Guest operation not allowed",
						403,
						"CC_GUEST_OPERATION_NOT_ALLOWED",
						{},
					);
				}

				let user_id = ctx.meta.user.id;

				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					throw new Errors.ForbiddenError(
						"Only backoffice can charge by user_id",
						"CHARGE_ONLY_BACKOFFICE_CAN_CHARGE_USER_ID",
					);
				}

				const paymentMethodProvided = await ctx.call("payments.payment-methods.get", {
					id: ctx.params.payment_method_id,
				});

				const isPaymentAvailable = await ctx.call(
					"current_account.accounts_payments_methods.find",
					{
						query: {
							account_id: ctx.params.account_id,
							device_id: ctx.meta.device.id,
							payment_method_id: ctx.params.payment_method_id,
						},
					},
				);

				// IF NOT AVAILABLE FOR THIS ACCOUNT
				if (!isPaymentAvailable.length) {
					throw new Errors.ForbiddenError(
						"Payment method not allowed",
						"CHARGE_PAYMENT_METHOD_NOT_ALLOWED",
					);
				}

				// CHECK IF AVAILABLE METHOD (SERVICE)
				await ctx.call("payments.payments.check_method_available", {
					payment_method_id: ctx.params.payment_method_id,
				});

				const is_immediate = paymentMethodProvided[0].is_immediate;

				if (
					!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS") &&
					(paymentMethodProvided[0].tag === "NUM" || paymentMethodProvided[0].tag === "MB")
				) {
					throw new Errors.ForbiddenError(
						"Payment method not allowed",
						"CHARGE_PAYMENT_METHOD_NOT_ALLOWED",
					);
				}

				if ((ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					user_id = ctx.params.user_id;
				}

				// Validate account provided
				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });

				const paymentMethodSettings = await ctx.call(
					"current_account.payment_method_settings.find",
					{
						query: {
							account_id: ctx.params.account_id,
							payment_method_id: ctx.params.payment_method_id,
						},
					},
				);
				if (paymentMethodSettings.length) {
					// Check LOWER limit amount
					if (ctx.params.transaction_value < paymentMethodSettings[0].min_charge_amount) {
						throw new Errors.ValidationError(
							"Charge amount is lower than allowed for this payment method",
							"CHARGE_AMOUNT_LOWER_THAN_ALLOWED",
							{
								min_charge_amount: paymentMethodSettings[0].min_charge_amount,
								max_charge_amount: paymentMethodSettings[0].max_charge_amount,
							},
						);
					}
					// Check GREATER limit amount
					if (ctx.params.transaction_value > paymentMethodSettings[0].max_charge_amount) {
						throw new Errors.ValidationError(
							"Charge amount is greater than allowed for this payment method",
							"CHARGE_AMOUNT_GREATER_THAN_ALLOWED",
							{
								min_charge_amount: paymentMethodSettings[0].min_charge_amount,
								max_charge_amount: paymentMethodSettings[0].max_charge_amount,
							},
						);
					}
				} else {
					this.logger.info(
						"No min/max limit amounts defined for this account and payment_method: ",
					);
					this.logger.info(" - account_id:", ctx.params.account_id);
					this.logger.info(
						" - payment_method_id:",
						paymentMethodProvided[0].id,
						"(" + paymentMethodProvided[0].tag + ")",
					);
				}

				let description = "Carregamento de Saldo";
				if (ctx.params.description) {
					description = ctx.params.description;
				}

				// Prepares the CHARGE movement/movement-item
				let dummyChargeMovement = Object.assign(
					{},
					{
						id: uuidv4(),
						account_id: ctx.params.account_id,
						operation: is_immediate ? "CHARGE" : "CHARGE_NOT_IMMEDIATE",
						user_id: user_id,
						device_id: ctx.meta.device.id,
						description: "Carregamento de Saldo",
						payment_method_id: ctx.params.payment_method_id,
						cash_account_id: ctx.params.cash_account_id ? ctx.params.cash_account_id : null,
						items: [
							Object.assign(
								{},
								{
									service_id: parseInt(process.env.SERVICE_ID_MS_CC_PAYMENTS),
									product_code: "--",
									name: "Pagamentos/ Carregamentos",
									description: description,
									vat_id: 2, //<--REVIEW //MUST BE TAX-ISENT
									quantity: 1,
									unit_value: ctx.params.transaction_value,
									location: "",
									article_type: "",
									service_confirm_path: "current_account.movements.fake_service_confirm",
									service_cancel_path: "current_account.movements.fake_service_cancel",
								},
							),
						],
					},
				);

				// Creates the CHARGE movement
				return ctx.call("current_account.movements.create", dummyChargeMovement);
			},
		},
		uncharge_account: {
			visibility: "published",
			rest: "POST /unchargeAccount",
			scope: "current_account:accounts:uncharge",
			params: {
				account_id: { type: "number", integer: true, positive: true, optional: false },
				user_id: { type: "number", integer: true, positive: true, optional: true },
				payment_method_id: { type: "uuid", version: 4, optional: true },
				transaction_value: {
					type: "number",
					positive: true,
					/*min: 1,*/
					convert: true,
					optional: false,
				},
				description: { type: "string", max: 250, optional: true },
				cash_account_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			},
			async handler(ctx) {
				let user_id = ctx.meta.user.id;

				if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					throw new Errors.ForbiddenError(
						"Only backoffice can refund by user_id",
						"REFUND_ONLY_BACKOFFICE_CAN_REFUND_USER_ID",
					);
				}

				if ((ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
					user_id = ctx.params.user_id;
				}

				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });

				const accountBalance = await ctx
					.call("current_account.movements.balance", {
						user_id: ctx.params.user_id,
						account_id: ctx.params.account_id,
					})
					.then((account) => {
						if (ctx.params.transaction_value > account.current_balance) {
							// Checks the available funds to refund
							throw new Errors.ValidationError(
								"Not enough funds to make this refund",
								"INSUFFICIENT_FUNDS_FOR_REFUND",
								{},
							);
						}
						return account;
					});

				const paymentMethodProvided = await ctx.call("payments.payment-methods.get", {
					id: ctx.params.payment_method_id,
				});

				const serviceMSCCPayments = await ctx.call("payments.payment-methods.get", {
					id: ctx.params.payment_method_id,
				});

				let description = "Serviço de Pagamentos/ Carregamentos";
				if (ctx.params.description) {
					description = ctx.params.description;
				}

				// Prepares the UNCHARGE movement/movement-item
				let dummyMovement = Object.assign(
					{},
					{
						id: uuidv4(),
						account_id: ctx.params.account_id,
						operation: "REFUND",
						user_id: user_id,
						description: "Devolução de Saldo",
						payment_method_id: ctx.params.payment_method_id,

						cash_account_id: ctx.params.cash_account_id ? ctx.params.cash_account_id : null,

						items: [
							Object.assign(
								{},
								{
									service_id: parseInt(process.env.SERVICE_ID_MS_CC_PAYMENTS),
									product_code: "--",
									name: "Pagamentos/ Carregamentos",
									description: description,
									vat_id: 2, //<--REVIEW //MUST BE TAX-ISENT
									quantity: 1,
									unit_value: ctx.params.transaction_value,
									location: "",
									article_type: "",
									service_confirm_path: "current_account.movements.fake_service_confirm",
									service_cancel_path: "current_account.movements.fake_service_cancel",
								},
							),
						],
					},
				);
				// Creates the UNCHARGE movement prior the movement request
				return ctx.call("current_account.movements.create", dummyMovement);
			},
		},
		get: {
			// REST: GET /:id
			cache: false,
			visibility: "published",
		},
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#isBackoffice",
				],
			},
			visibility: "published",
			scope: "current_account:movements:list",
		},
		create: {
			//visibility: "public",
			visibility: "published", //<-- REMOVE JUST FOR DEBUG
			rest: "POST /", //<-- REMOVE JUST FOR DEBUG

			scope: "current_account:movements:create",
			params: {
				account_id: {
					type: "number",
					convert: true,
					integer: true,
					positive: true,
					min: 1,
					optional: false,
				},
				operation: { type: "enum", values: MOVEMENT_OPERATION, optional: false },
				device_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
				user_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
				entity: { type: "string", max: 120, optional: true },
				tin: { type: "string", max: 9, optional: true },
				email: { type: "string", max: 120, optional: true },
				address: { type: "string", max: 250, optional: true },
				postal_code: { type: "string", max: 10, optional: true },
				city: { type: "string", max: 250, optional: true },
				country: { type: "string", max: 250, optional: true },
				description: { type: "string", max: 250, optional: true },
				payment_method_id: { type: "uuid", version: 4, optional: true },
				expiration_at: { type: "date", optional: true },
				items: {
					type: "array",
					items: {
						type: "object",
						strict: true,
						props: {
							service_id: {
								type: "number",
								integer: true,
								positive: true,
								min: 1,
								optional: false,
							},
							product_code: { type: "string", max: 120, optional: false },
							name: { type: "string", max: 255, optional: false },
							description: { type: "string", max: 250, optional: true },
							extra_info: { type: "object", optional: true },
							quantity: {
								type: "number",
								integer: true,
								positive: true,
								convert: true,
								optional: false,
							},
							liquid_unit_value: { type: "number", positive: true, convert: true, optional: true },
							unit_value: { type: "number", positive: true, convert: true, optional: true },
							discount_value: {
								type: "number",
								min: 0,
								optional: true,
								default: 0,
							},
							vat_id: {
								type: "number",
								convert: true,
								integer: true,
								positive: true,
								min: 1,
								optional: false,
							},
							location: { type: "string", max: 255, optional: false },
							article_type: { type: "string", max: 120, optional: true },
							service_confirm_path: {
								type: "string",
								max: 120,
								optional: true,
								default: "current_account.movements.fake_service_confirm",
							},
							service_cancel_path: {
								type: "string",
								max: 120,
								optional: true,
								default: "current_account.movements.fake_service_cancel",
							},
						},
					},
				},
				bypass_balance_with_invoice: { type: "boolean", convert: true, optional: true },
			},
			async handler(ctx) {
				// If is an INVOICE_RECEIPT and PaymentMethod is CASH or TPA
				//  - PRIOR CREATE THE CHARGE MOVEMENT

				// GET THE DEVICE
				if (!ctx.params.device_id && ctx.meta.device) ctx.params.device_id = ctx.meta.device.id;

				if (ctx.params.operation === "INVOICE_RECEIPT") {
					// Get the provided PaymentMethod details
					let paymentMethodProvided = null;
					if (ctx.params.payment_method_id) {
						paymentMethodProvided = (
							await ctx.call("payments.payment-methods.get", {
								id: ctx.params.payment_method_id,
							})
						)[0];

						// Checking if the paymentMethod is NUM or MB for MovementOperation "INVOICE_RECEIPT",
						// so is paying at Counter/Desk, and this operation create a CHARGE (loads Balance) prior to pay
						if (paymentMethodProvided.tag === "NUM" || paymentMethodProvided.tag === "MB") {
							ctx.params.affects_balance = true;
							ctx.params.payment_method_name = paymentMethodProvided.name;

							// Calculate TOTAL transaction-value from provided items
							let providedItems = ctx.params.items;
							let itemsProvided = await Promise.all(
								providedItems.map((mvItem) => this.calculateMovementItemValues(ctx, mvItem)),
							);
							let transactionValue = _.reduce(
								itemsProvided,
								(sum, n) => {
									return sum + parseFloat(n.total_value);
								},
								0,
							);

							// Prepares the CHARGE movement/movement-item
							let dummyChargeMovement = Object.assign(
								{},
								{
									id: uuidv4(),
									account_id: ctx.params.account_id,
									operation: "CHARGE",
									user_id: ctx.params.user_id,
									description: "Carregamento",
									payment_method_id: paymentMethodProvided.id, //"6273cc97-943b-4524-8ddf-a7287afd331b",

									cash_account_id: ctx.params.cash_account_id ? ctx.params.cash_account_id : null,

									items: [
										Object.assign(
											{},
											{
												service_id: 4, //<--REVIEW
												product_code: "CHARGE",
												name: `${paymentMethodProvided.tag}`,
												description: `ADD BALANCE BY ${paymentMethodProvided.tag}`,
												vat_id: 2, //<--REVIEW //MUST BE TAX-ISENT
												quantity: 1,
												liquid_unit_value: transactionValue,
												location: "",
												article_type: "",
												service_confirm_path: "current_account.movements.fake_service_confirm", // <--REVIEW
												service_cancel_path: "current_account.movements.fake_service_cancel", // <--REVIEW
											},
										),
									],
								},
							);
							// Creates the CHARGE movement prior the movement request
							await ctx.call("current_account.movements.create", dummyChargeMovement);
						}
					} else {
						ctx.params.payment_method_id = process.env.DEFAULT_PAYMENT_METHOD_ID;
					}
				}

				return this.createMovement(ctx, ctx.params).then((json) =>
					this.entityChanged("created", json, ctx).then(() => json),
				);
			},
		},
		balance: {
			visibility: "published",
			rest: "GET /balances",
			cache: false,
			params: {
				withRelated: { type: "string", optional: true },
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				requestTotalCharges: { type: "boolean", optional: true, convert: true, default: false },
			},
			async handler(ctx) {
				let user_id = ctx.meta.user ? ctx.meta.user.id : null;

				if (_.has(ctx, "params.query.account_id")) {
					ctx.params.account_id = ctx.params.query.account_id;
				}

				if (_.has(ctx, "params.query.user_id")) {
					ctx.params.user_id = ctx.params.query.user_id;
				}

				if (ctx.meta.bypass_bo_validation) {
					if (ctx.params.user_id) {
						user_id = ctx.params.user_id;
					}
				} else {
					if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
						throw new Errors.ForbiddenError(
							"Only backoffice can search for user_id",
							"BALANCES_ONLY_BACKOFFICE_CAN_SEARCH_USER_ID",
						);
					}

					if ((ctx.meta.isBackoffice || ctx.meta.device.type === "POS") && ctx.params.user_id) {
						user_id = ctx.params.user_id;
					}
				}

				if (_.has(ctx, "params.account_id") && ctx.params.account_id) {
					const account = await ctx.call("current_account.accounts.get", {
						withRelated: ctx.params.withRelated,
						id: ctx.params.account_id,
					});
					return this.checkBalance(ctx, account[0], user_id, ctx.params.requestTotalCharges);
				} else {
					const accounts = await ctx.call("current_account.accounts.find", {
						withRelated: ctx.params.withRelated,
					});
					return Promise.all(accounts.map((acc) => this.checkBalance(ctx, acc, user_id, ctx.params.requestTotalCharges)));
				}
			},
		},
		createPayment: {
			rest: "POST /pay",
			visibility: "published",
			scope: "current_account:movements:pay",
			params: {
				id: { type: "uuid", version: 4, optional: false }, //movement_id
				payment_method_id: { type: "uuid", version: 4, optional: true },
				description: { type: "string", max: 250, optional: true },
				transaction_value: { type: "number", positive: true, optional: true },
			},
			handler(ctx) {
				return ctx.call("current_account.movements.get", ctx.params).then(async (mov) => {
					if (mov[0].operation !== "CREDIT_NOTE" && mov[0].status !== "PENDING") {
						throw new Errors.ValidationError(
							"Movement is on a invalid status for payment",
							"PAYMENT_INVALID_MOVEMENT",
							{},
						);
					}

					//Get movementValue
					//let amountToPay = Math.abs(mov[0].transaction_value) - mov[0].paid_value;
					let amountToPay = Math.abs(mov[0].transaction_value);
					if (ctx.params.transaction_value) {
						//PartialPayment
						if (ctx.params.transaction_value > amountToPay) {
							throw new Errors.ValidationError(
								"Payment amount is greater than movement amount",
								"PAYMENT_AMOUNT_GREATER_THAN_MOVEMENT_AMOUNT",
								{},
							);
						}
						amountToPay = ctx.params.transaction_value;
					}

					if (mov[0].operation !== "CREDIT_NOTE") {
						const all_movements = await ctx.call("current_account.movements.find", {
							query: {
								transaction_id: mov[0].transaction_id,
								operation: ["RECEIPT", "CREDIT_NOTE"],
							},
							withRelated: false,
						});
						const total_paid = all_movements
							.filter((m) => m.id !== ctx.params.id)
							.reduce((a, b) => {
								return a + b.transaction_value;
							}, 0);

						if (ctx.params.transaction_value) {
							if (ctx.params.transaction_value > amountToPay - total_paid)
								throw new Errors.ValidationError(
									"The payment amount is greater than the amount missing in the movement",
									"PAYMENT_AMOUNT_GREATER_THAN_MISSING_MOVEMENT_AMOUNT",
									{},
								);
						} else {
							if (amountToPay > amountToPay - total_paid)
								throw new Errors.ValidationError(
									"The payment amount is greater than the amount missing in the movement",
									"PAYMENT_AMOUNT_GREATER_THAN_MISSING_MOVEMENT_AMOUNT",
									{},
								);
						}
					}

					// Create the related payment
					return ctx
						.call("payments.payments.create", {
							account_id: mov[0].account_id,
							user_id: mov[0].user_id,
							payment_method_id: ctx.params.payment_method_id,
							movement_id: mov[0].id,
							value: amountToPay,
							status: "PENDING",
							description: ctx.params.description,
							cash_account_id: mov[0].cash_account_id,
						})
						.then(async (resultCreatedPayment) => {
							if (!resultCreatedPayment) {
								this.logger.info(" - payment not created.1!");
								return false;
							}

							let movementToPatch = {
								id: ctx.params.id, //payment[0].movement_id,
								payment_method_id: ctx.params.payment_method_id, //payment[0].payment_method_id,
								payment_id: resultCreatedPayment[0].id,
								withRelated: ["items"],
							};
							if (ctx.params.description) {
								Object.assign(movementToPatch, { description: ctx.params.description });
							}
							return ctx
								.call("current_account.movements.patch", movementToPatch)
								.then(async (movementUpdated) => {
									const _paymentMethod = await ctx.call("payments.payment-methods.get", {
										id: ctx.params.payment_method_id,
									});
									// Update RefMB into item description
									if (_paymentMethod[0].tag === "REFMB_AMA") {
										// <= REVIEW
										if (resultCreatedPayment[0].payment_method_data) {
											const _refMB = resultCreatedPayment[0].payment_method_data;
											const _description = `RefMB: [ Entidade: ${_.get(
												_refMB,
												"EntityNumber",
												"--",
											)}, Referência: ${_.get(_refMB, "Reference", "--")}, Valor: ${_.get(
												_refMB,
												"Amount",
												"--",
											)} ]`;
											await ctx.call("current_account.items.patch", {
												id: movementUpdated[0].items[0].id,
												description: _description,
											});
										}
									}

									// Try Confirm Payment/movement if is_immediate
									if (
										_paymentMethod[0].is_immediate &&
										movementUpdated[0].operation != "CREDIT_NOTE"
									) {
										const _tryConfirmResult = await ctx
											.call("current_account.movements.tryConfirm", {
												id: ctx.params.id, //: payment[0].movement_id,
												payment_method_id: ctx.params.payment_method_id,
											})
											.catch((error) => {
												ctx.call("current_account.movements.patch", {
													id: ctx.params.id,
													payment_method_id: null,
													payment_id: null,
												});
												throw error;
											});

										// Notify services for paymentConfirmed
										await this.notifyServicesConfirmedPayment(ctx, movementUpdated[0])
											.then((confirmationResult) => {
												this.logger.info("### notifyServicesConfirmedPayment.confirmationResult:");
												this.logger.info(confirmationResult);

												if (_.includes(confirmationResult, false)) {
													// Some service notification returned false (fail)

													// 1. Add to incidents, with the fail info
													//TODO: create table to register the incidents

													// 2. Remove the movement and payment
													/*this.logger.info(`   - the relatedMovement will be deleted: ${movementUpdated[0].id}`);
													let removedMovement = await this._remove(ctx, {
														id: movementUpdated[0].id,
													});
													this.logger.info(`   - relatedMovement deleted:`);
													this.logger.info(removedMovement);*/

													// 3. Update Balance
													//TODO: based on operation affects balance

													// 4. Throw back the error
													throw new MoleculerClientError(
														"Cannot notify origin Service for payment confirmation",
														503,
														"CONFIRMATION_SERVICE_ERROR",
														{},
													);
												}
											})
											.catch((err) => {
												this.logger.error("########### itemsToNotify.err:");
												this.logger.error(err);
											});

										return _tryConfirmResult;
									}
								})
								.then(() => {
									return ctx.call("current_account.movements.get", {
										id: ctx.params.id, //payment[0].movement_id,
										withRelated: ["items"],
									});
								});
						});
				});
			},
		},
		createPartialPayment: {
			visibility: "public",
			//visibility: "published",
			//rest: "POST /payPartial",
			params: {
				id: { type: "uuid", version: 4, optional: false }, //movement_id
				payment_method_id: { type: "uuid", version: 4, optional: true },
				description: { type: "string", max: 250, optional: true },
				transaction_value: { type: "number", positive: true, optional: true },
			},
			handler(ctx) {
				return ctx.call("current_account.movements.get", ctx.params).then((mov) => {
					const movToPay = mov[0];
					if (this.settings.MS_DEBUG) {
						this.logger.info("### => createPartialPayment.1 movToPay:", movToPay);
					}

					//Get movementValue
					let currentPaidValue = movToPay.paid_value;
					let payableAmount = Math.abs(movToPay.transaction_value) - currentPaidValue;
					let amountToPay = Math.abs(movToPay.transaction_value);

					if (this.settings.MS_DEBUG) {
						this.logger.info("### => createPartialPayment.1 currentPaidValue:", currentPaidValue);
						this.logger.info("### => createPartialPayment.1 payableAmount:", payableAmount);
						this.logger.info("### => createPartialPayment.1 amountToPay:", amountToPay);
					}

					if (ctx.params.transaction_value) {
						//PartialPayment
						if (ctx.params.transaction_value > payableAmount) {
							throw new Errors.ValidationError(
								"Payment amount is greater than movement amount",
								"PAYMENT_AMOUNT_GREATER_THAN_MOVEMENT_AMOUNT",
								{},
							);
						}
						amountToPay = ctx.params.transaction_value;
					}

					if (this.settings.MS_DEBUG) {
						this.logger.info("### => createPartialPayment.1.1 amountToPay:", amountToPay);
					}

					// Create the related payment
					return ctx
						.call("payments.payments.create", {
							account_id: movToPay.account_id,
							user_id: movToPay.user_id,
							payment_method_id: ctx.params.payment_method_id,
							movement_id: movToPay.id,
							value: amountToPay,
							status: "PENDING",
							description: ctx.params.description,
							cash_account_id: movToPay.cash_account_id,
						})
						.then(async (resultCreatedPayment) => {
							if (!resultCreatedPayment) {
								if (this.settings.MS_DEBUG) {
									this.logger.info(" - payment not created.1!");
								}
								return false;
							}

							const updatedPaidValue = currentPaidValue + amountToPay;
							if (this.settings.MS_DEBUG) {
								this.logger.info(
									"### => createPartialPayment.2 updatedPaidValue:",
									updatedPaidValue,
								);
							}

							let movementToPatch = {
								id: ctx.params.id,
								payment_method_id: ctx.params.payment_method_id,
								payment_id: resultCreatedPayment[0].id,

								paid_value: updatedPaidValue,

								withRelated: ["items"],
							};

							if (this.settings.MS_DEBUG) {
								this.logger.info("### => createPartialPayment.2 movementToPatch:", movementToPatch);
							}

							if (ctx.params.description) {
								Object.assign(movementToPatch, { description: ctx.params.description });
							}
							return ctx
								.call("current_account.movements.patch", movementToPatch)
								.then(async (movementUpdated) => {
									const _paymentMethod = await ctx.call("payments.payment-methods.get", {
										id: ctx.params.payment_method_id,
									});

									// Try Confirm Payment/movement if is_immediate
									if (_paymentMethod[0].is_immediate) {
										const _tryConfirmResult = await ctx.call(
											"current_account.movements.tryConfirm",
											{
												id: ctx.params.id,
												payment_method_id: ctx.params.payment_method_id,
											},
											{ meta: { is_partial_pay: true } },
										);

										// Notify services for paymentConfirmed
										await this.notifyServicesConfirmedPayment(ctx, movementUpdated[0])
											.then((confirmationResult) => {
												if (this.settings.MS_DEBUG) {
													this.logger.info(
														"### notifyServicesConfirmedPayment.confirmationResult:",
													);
													this.logger.info(confirmationResult);
												}

												if (_.includes(confirmationResult, false)) {
													// Some service notification returned false (fail)

													// 1. Add to incidents, with the fail info
													//TODO: create table to register the incidents

													// 2. Remove the movement and payment
													/*this.logger.info(`   - the relatedMovement will be deleted: ${movementUpdated[0].id}`);
													let removedMovement = await this._remove(ctx, {
														id: movementUpdated[0].id,
													});
													this.logger.info(`   - relatedMovement deleted:`);
													this.logger.info(removedMovement);*/

													// 3. Update Balance
													//TODO: based on operation affects balance

													// 4. Throw back the error
													throw new MoleculerClientError(
														"Cannot notify origin Service for payment confirmation",
														503,
														"CONFIRMATION_SERVICE_ERROR",
														{},
													);
												}
											})
											.catch((err) => {
												this.logger.error("########### itemsToNotify.err:");
												this.logger.error(err);
											});

										return _tryConfirmResult;
									}
								})
								.then(() => {
									return ctx.call("current_account.movements.get", {
										id: ctx.params.id, //movement,
										withRelated: ["items"],
									});
								});
						});
				});
			},
		},
		tryConfirm: {
			visibility: "public",
			//visibility: "published",
			//rest: "POST /confirm",
			params: {
				id: { type: "uuid", version: 4, optional: false }, // movement_id
				payment_method_id: { type: "uuid", version: 4, optional: false },
			},
			handler(ctx) {
				return ctx.call("current_account.movements.get", ctx.params).then((mov) => {
					if (!mov[0].payment_id) {
						throw new Errors.ValidationError(
							"Movement without payment defined",
							"MOVEMENT_WITHOUT_PAYMENT",
							{},
						);
					}

					if (this.settings.MS_DEBUG) {
						this.logger.info("### => tryConfirm ctx.meta.is_partial_pay:", ctx.meta.is_partial_pay);
					}

					return ctx.call(
						"payments.payments.confirm",
						{ id: mov[0].payment_id },
						{ meta: { is_partial_pay: ctx.meta.is_partial_pay ? ctx.meta.is_partial_pay : false } },
					);
				});
			},
		},
		paymentConfirmed: {
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("");
					this.logger.info("### PAYMENT CONFIRM (MOVEMENT) ###");
					this.logger.info(" 1. current_account.movements.paymentConfirmed");
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}

				if (ctx.params.payment) {
					// Get movement data
					const movementToConfirm = await ctx.call("current_account.movements.get", {
						id: ctx.params.payment.movement_id,
						withRelated: ["items"],
					});

					if (this.settings.MS_DEBUG) {
						this.logger.info("");
						this.logger.info("  - operation:", movementToConfirm[0].operation);
					}

					if (
						movementToConfirm[0].operation === "CHARGE_NOT_IMMEDIATE" ||
						movementToConfirm[0].operation === "INVOICE"
					) {
						// CREATE ANOTHER MOVEMENT
						const newMovement = _.pick(movementToConfirm[0], [
							"account_id",
							"operation",
							"user_id",
							"entity",
							"tin",
							"email",
							"address",
							"postal_code",
							"city",
							"country",
							"description",
							"transaction_id",
							"payment_method_id",
							"expiration_at",
						]);

						newMovement.operation =
							movementToConfirm[0].operation === "CHARGE_NOT_IMMEDIATE" ? "CHARGE" : "RECEIPT";

						//by-pass REFMB and CHARGE_NOT_IMMEDIATE validation
						newMovement.payment_method_id = null;
						if (movementToConfirm[0].payment_method_id) {
							newMovement.payment_method_id = movementToConfirm[0].payment_method_id;
							newMovement.payment_method_name = movementToConfirm[0].payment_method_name;
						}
						//Set CashAccount from original movement
						if (movementToConfirm[0].cash_account_id) {
							newMovement.cash_account_id = movementToConfirm[0].cash_account_id;
						}

						// Set original movement
						newMovement.original_movement_id = movementToConfirm[0].id;

						// Set device
						newMovement.device_id = ctx.meta.device
							? ctx.meta.device.id
							: movementToConfirm[0].device_id;

						// #######
						// CHECK IF IS PARTIAL PAYMENT
						// ------

						let movementTotalPending =
							Math.abs(movementToConfirm[0].transaction_value) -
							movementToConfirm[0].paid_value -
							ctx.params.payment.value;

						let isMovementTotalPaid = movementTotalPending == 0;

						let isPartialPayment =
							Math.abs(movementToConfirm[0].transaction_value) !=
							Math.abs(ctx.params.payment.value);

						if (!isPartialPayment) {
							// Set related items
							newMovement.items = movementToConfirm[0].items.map((item) => {
								let _item = _.pick(item, [
									"service_id",
									"product_code",
									"name",
									"description",
									"extra_info",
									"quantity",
									"unit_value",
									"discount_value",
									"vat_id",
									"location",
									"article_type",
									"service_confirm_path",
									"service_cancel_path",
								]);

								_item.discount_value = parseFloat(_item.discount_value);

								return _item;
							});
						} else {
							newMovement.items = [
								Object.assign(
									{},
									{
										service_id: parseInt(process.env.SERVICE_ID_MS_CC_PAYMENTS),
										product_code: "--",
										name: "Pagamentos/ Carregamentos",
										description:
											"Pagamento parcial: Doc. " +
											movementToConfirm[0].doc_type_acronym +
											movementToConfirm[0].seq_doc_num,
										vat_id: 2, //<--REVIEW //MUST BE TAX-ISENT
										quantity: 1,
										unit_value: ctx.params.payment.value,
										location: "",
										article_type: "",
										service_confirm_path: "current_account.movements.fake_service_confirm",
										service_cancel_path: "current_account.movements.fake_service_cancel",
									},
								),
							];
						}

						return ctx
							.call("current_account.movements.create", newMovement)
							.then((resultNewMovementCreate) => {
								if (this.settings.MS_DEBUG) {
									this.logger.info("");
									this.logger.info(" 2. current_account.movements.create");
									this.logger.info(resultNewMovementCreate);
									this.logger.info("");
									this.logger.info("### /PAYMENT CONFIRM (MOVEMENT) ###");
								}
								return resultNewMovementCreate;
							})
							.then((resultCreatedMovement) => {
								let movStatus =
									ctx.params.payment.status === "CONFIRMED" &&
										resultCreatedMovement[0].operation != "CHARGE"
										? "PAID"
										: ctx.params.payment.status;

								let movOriginalStatus =
									ctx.params.payment.status === "CONFIRMED" &&
										resultCreatedMovement[0].operation != "CHARGE"
										? isMovementTotalPaid
											? "PAID"
											: "PENDING"
										: ctx.params.payment.status;
								// Update movement status
								return ctx
									.call("current_account.movements.patch", {
										id: resultCreatedMovement[0].id,
										status: movStatus,
									})
									.then((resultPatchedMovement) => {
										if (this.settings.MS_DEBUG) {
											this.logger.info("");
											this.logger.info(" 3. Updated movement status");
											this.logger.info(resultPatchedMovement);
											this.logger.info("");
											this.logger.info("### /PAYMENT CONFIRM (MOVEMENT) ###");
										}
										// Update related movement (original) status
										return ctx
											.call("current_account.movements.patch", {
												id: resultPatchedMovement[0].original_movement_id,
												//paid_value: Math.abs(movementTotalPaid), //resultCreatedMovement[0].paid_value, //<--REVIEW
												status: movOriginalStatus,
											})
											.then((resultPatchedOriginalMovement) => {
												if (this.settings.MS_DEBUG) {
													this.logger.info("");
													this.logger.info(" 4. Update related movement (original) status");
													this.logger.info(resultPatchedOriginalMovement);
													this.logger.info("");
													this.logger.info("### /PAYMENT CONFIRM (MOVEMENT) ###");
												}
												return resultPatchedOriginalMovement;
											});
									});
							});
					}

					let movStatus =
						ctx.params.payment.status === "CONFIRMED" && movementToConfirm[0].operation != "CHARGE"
							? "PAID"
							: ctx.params.payment.status;

					return ctx.call("current_account.movements.patch", {
						id: ctx.params.payment.movement_id,
						paid_value: ctx.params.payment.value, //<--REVIEW
						status: movStatus,
					});
				}

				return false;
			},
		},
		checkMovementsToSendERP: {
			handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("TICK: Trying to check movements to send MiddlewareERP!");
				}
				return this._find(ctx, {
					//withRelated: ["items"],
					query: {
						middleware_erp_status: "RETRY",
					},
					limit: 30,
					sort: "created_at",
				}).then(async (movs) => {
					if (this.settings.MS_DEBUG) {
						this.logger.info(`TICK: ${movs.length} movements to send ERP found!`);
					}

					for (const mov of movs) {
						if (this.settings.MS_DEBUG) {
							this.logger.info("found mov:");
							this.logger.info(
								_.pick(mov, ["id", "created_at", "operation", "middleware_erp_status"]),
							);
						}

						await this.addToSendErpQueue(ctx, [mov]);
					}
					return Promise.resolve();
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		sanitizeQuery(ctx) {
			ctx.params.query = ctx.params.query ? ctx.params.query : {};

			if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
				ctx.params.query.user_id = ctx.meta.user.id;
			}
		},
		async notifyServicesConfirmedPayment(ctx, movement) {
			if (this.settings.MS_DEBUG) {
				this.logger.info("### [NOTIFY SOURCE SERVICES / CONFIRMATION] ###");
			}

			// NOTIFY SOURCE-SERVICE WITH PAYMENT-CONFIRMED GROUPED BY: [ARTICLE_TYPE]
			const itemsToNotify = movement.items;

			/***
			*
			* Group items by ArticleType
			*
			*
			let groupedItemsToNotify = _.chain(items).groupBy("article_type").map(function (v, i) {
			 return {
				 article_type: _.get(_.find(v, 'article_type'), 'article_type', 'OTHER'),
				 service_confirm_path: _.get(_.find(v, 'service_confirm_path'), 'service_confirm_path'),
				 itemsToConfirm: _.map(v, items[i])
			 }
			}).value();
			*
			*  RESULT:
			*
			*	[
			*		{
			*			"article_type": "REFECTORY",
			*			"service_confirm_path": "alimentation.reservations.confirm_reservation",
			*			"itemsToConfirm": [
			*				"95cc2659-3eda-4cc4-bf7e-1275d190aeac" <- item id
			*			]
			*		},
			*		{
			*			"article_type": "BAR",
			*			"service_confirm_path": "alimentation.reservations.confirm_reservation",
			*			"itemsToConfirm": [
			*				"9bb3ac76-5495-42a4-a3ac-82f9d2d9fdd3" <- item id
			*			]
			*		}
			*	]
			*
			*
			**/

			let groupedItemsToNotify = _.chain(itemsToNotify)
				.groupBy("article_type")
				.map(function (v, i) {
					return {
						article_type: _.get(_.find(v, "article_type"), "article_type", "OTHER"),
						service_confirm_path: _.get(_.find(v, "service_confirm_path"), "service_confirm_path"),
						itemsToConfirm: _.map(v, itemsToNotify[i]),
					};
				})
				.value();

			const promises = [];
			groupedItemsToNotify.forEach((itmMov) => {
				if (this.settings.MS_DEBUG) {
					this.logger.info("  # Item to be notified:");
					this.logger.info(`  --> article_type: ${itmMov.article_type}`);
					this.logger.info(`  --> service_confirm_path: ${itmMov.service_confirm_path}`);
					this.logger.info(`  --> movement_id: ${movement.id}`);
					this.logger.info(
						`  --> movement_item_ids: ${JSON.stringify(
							_.map(itmMov.itemsToConfirm, "id"),
							null,
							" ",
						)}`,
					);
				}

				promises.push(
					ctx
						.call(itmMov.service_confirm_path, {
							user_id: movement.user_id, // comment to force error at origin service MS
							items: itmMov.itemsToConfirm,
						})
						.then((itemNotifiedResult) => {
							if (!itemNotifiedResult) {
								if (this.settings.MS_DEBUG) {
									this.logger.error(
										"  --> ## [FAIL] ## Payment Confirmation sent to Source Service returned: CONFIRMATION = false.",
									);
								}

								return false;
							}
						})
						.catch((err) => {
							this.logger.error("  --> ## [ERR] ## Payment Confirmation sent to Source Service:");
							this.logger.error(err);
						}),
				);
			});
			return Promise.all(promises);
		},
		async notifyServicesCancelledMovement(ctx, movement) {
			if (this.settings.MS_DEBUG) {
				this.logger.info("### [NOTIFY SOURCE SERVICES / CANCELATION] ###");
			}

			// NOTIFY SOURCE-SERVICE WITH PAYMENT-CANCELATION GROUPED BY: [ARTICLE_TYPE]
			const itemsToNotify = movement.items;

			let groupedItemsToNotify = _.chain(itemsToNotify)
				.groupBy("article_type")
				.map(function (v, i) {
					return {
						article_type: _.get(_.find(v, "article_type"), "article_type", "OTHER"),
						service_cancel_path: _.get(_.find(v, "service_cancel_path"), "service_cancel_path"),
						itemsToCancel: _.map(v, itemsToNotify[i]),
					};
				})
				.value();

			const promises = [];
			groupedItemsToNotify.forEach((itmMov) => {
				if (this.settings.MS_DEBUG) {
					this.logger.info("  # Item to be notified:");
					this.logger.info(`  --> article_type: ${itmMov.article_type}`);
					this.logger.info(`  --> service_cancel_path: ${itmMov.service_cancel_path}`);
					this.logger.info(`  --> movement_id: ${movement.id}`);
					this.logger.info(
						`  --> movement_item_ids: ${JSON.stringify(
							_.map(itmMov.itemsToCancel, "id"),
							null,
							" ",
						)}`,
					);
				}

				promises.push(
					ctx
						.call(itmMov.service_confirm_path, {
							user_id: movement.user_id, // comment to force error at origin service MS
							items: itmMov.itemsToCancel,
						})
						.then((itemNotifiedResult) => {
							if (!itemNotifiedResult) {
								if (this.settings.MS_DEBUG) {
									this.logger.error(
										"  --> ## [FAIL] ## Payment Cancellation sent to Source Service returned: CANCELATION = false.",
									);
								}

								return false;
							}
						})
						.catch((err) => {
							this.logger.error("  --> ## [ERR] ## Payment Cancellation sent to Source Service:");
							this.logger.error(err);
						}),
				);
			});
			return Promise.all(promises);
		},
		async createMovement(ctx, paramsMovement) {
			let movementToCreate = paramsMovement;
			let items = movementToCreate.items;

			if (this.settings.MS_DEBUG) {
				this.logger.info("");
				this.logger.info(" ### ############### ### ");
				this.logger.info(" ### CREATE MOVEMENT ### ");
				this.logger.info("  --- paramsMovement(movementToCreate):", movementToCreate);
			}

			if (!items.length) {
				throw new Errors.ValidationError(
					"Can't create a movement without items",
					"MOVEMENT_WITHOUT_ITEMS",
					{},
				);
			}

			// Get Account
			const account = await ctx.call("current_account.accounts.get", {
				id: movementToCreate.account_id,
			});

			// If Payment-method was provided, get them
			let paymentMethodProvided = null;
			if (movementToCreate.payment_method_id) {
				paymentMethodProvided = (
					await ctx.call("payments.payment-methods.get", {
						id: movementToCreate.payment_method_id, //ctx.params.payment_method_id,
					})
				)[0];

				//TODO: TODO: Verificar esta condição, não poderá ser através de is_immediate
				// porque quando o movimento é copiado do original, no caso dos pagamentos
				// das RefMB, o meio de pagamento será sempre não imediato, embora a operação
				// seja alterada para CHARGE mas o meio de pagamento é !is_immediate.
				if (movementToCreate.operation == "CHARGE") {
					movementToCreate.affects_balance = true;
				} else {
					if (!paymentMethodProvided.is_immediate) {
						movementToCreate.operation = "CHARGE_NOT_IMMEDIATE";
						movementToCreate.affects_balance = false;
					} else {
						movementToCreate.affects_balance = true;
					}
				}

				movementToCreate.payment_method_name = paymentMethodProvided.name;
			}

			// Get DocumentTypes
			const documentTypeAcronym = await ctx.call("current_account.document_types.find", {
				fields: ["doc_type_acronym"],
				query: {
					operation: movementToCreate.operation,
				},
			});
			// Set document acronym
			movementToCreate.doc_type_acronym = documentTypeAcronym[0].doc_type_acronym;

			// If entity is not provided get from `ctx.params.user_id`
			if (!movementToCreate.entity) {
				const entity = (
					await ctx.call("authorization.users.get", {
						id: movementToCreate.user_id,
						withRelated: false,
					})
				)[0];

				movementToCreate.entity = entity.name;
				movementToCreate.tin = (entity.tin || "").substring(0, 9); //<-- TODO: validate NIF !???
				movementToCreate.email = entity.email;
				movementToCreate.address = entity.address;
				movementToCreate.postal_code = /\d{4}-\d{3}/.test(entity.postal_code)
					? entity.postal_code
					: "0000-000";
				movementToCreate.city = entity.city;
				movementToCreate.country = entity.country;
			}

			// Get current_balance from the last movement and Account
			const lastMovementBalances = await this.checkBalance(
				ctx,
				account[0],
				movementToCreate.user_id,
				false
			);

			// Get transaction history
			const transactionHistory = await ctx.call("current_account.movements.find", {
				query: {
					transaction_id: ctx.params.transaction_id,
				},
				sort: "-created_at",
			});

			// Calculate movement-items values
			movementToCreate.items = await Promise.all(
				items.map((mvItem) => this.calculateMovementItemValues(ctx, mvItem)),
			);

			movementToCreate.current_balance = lastMovementBalances.current_balance;
			movementToCreate.owing_value = lastMovementBalances.in_debt;
			//movementToCreate.advanced_value = lastMovementBalances.in_debt;

			if (this.settings.MS_DEBUG) {
				this.logger.info("");
				this.logger.info(" ### --- ### ");
				this.logger.info(` ### CREATING MOVEMENT for operation: ${movementToCreate.operation}`);
				this.logger.info(" ###  - movementToCreate: ");
				this.logger.info(movementToCreate);
			}

			// Flag for control update PaidValue of OriginalMovement
			let originalMovementMustUpdatePaidValue = false;

			// Update movement amounts (transaction_value, current_balance, owing_value, advanced_value)
			items.map((item) => {
				switch (movementToCreate.operation) {
					case "CHARGE_NOT_IMMEDIATE":
						// C, No affects, (-)
						movementToCreate.is_immediate = false;
						movementToCreate.affects_balance = false;
						movementToCreate.transaction_value = item.total_value;
						//movementToCreate.owing_value = item.owing_value;
						break;
					case "CHARGE":
						// I, affects, (+)
						movementToCreate.is_immediate = true;
						movementToCreate.affects_balance = true;
						movementToCreate.transaction_value += item.total_value;
						movementToCreate.current_balance += item.total_value;
						if (
							transactionHistory.length > 0 &&
							transactionHistory[0].operation === "CHARGE_NOT_IMMEDIATE"
						) {
							//movementToCreate.owing_value -= item.total_value;
						}
						movementToCreate.paid_value = Math.abs(movementToCreate.transaction_value);
						break;
					case "CANCEL":
						if (this.settings.MS_DEBUG) {
							this.logger.info("CANCEL.1");
						}
						// I, no affects, (+)
						movementToCreate.is_immediate = true;
						movementToCreate.transaction_value += item.total_value;
						if (movementToCreate.affects_balance) {
							movementToCreate.current_balance += item.total_value;
						} else {
							movementToCreate.owing_value -= item.total_value;
						}
						break;
					case "RECEIPT":
						// I, affects?, (+)
						movementToCreate.is_immediate = false;
						movementToCreate.affects_balance = true;
						movementToCreate.transaction_value += item.total_value;
						movementToCreate.current_balance -= item.total_value;
						movementToCreate.owing_value += item.total_value;
						movementToCreate.paid_value = movementToCreate.transaction_value;

						// TODO UPDATE THE ORIGINAL MOVIMENT ITEM PAID VALUE

						break;
					case "CREDIT_NOTE": {
						movementToCreate.is_immediate = true;

						let original_movement = transactionHistory.find(
							(th) => th.id == movementToCreate.original_movement_id,
						);

						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info("####  - CREDIT_NOTE ####");
							this.logger.info("        - original_movement:");
							this.logger.info(original_movement);
						}
						if (!original_movement) {
							// TODO ...
						}
						const original_movement_item = original_movement.items.find(
							(omi) => omi.product_code === item.product_code,
						);
						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info("        - original_movement_item:");
							this.logger.info(original_movement_item);
						}
						if (!original_movement_item) {
							// TODO ...
						}
						const isTheItemPaid =
							original_movement_item.paid_value === original_movement_item.total_value;

						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info(
								"        - original_movement_item.paid_value:",
								original_movement_item.paid_value,
							);
							this.logger.info(
								"        - original_movement_item.total_value:",
								original_movement_item.total_value,
							);
							this.logger.info("        - isTheItemPaid:", isTheItemPaid);
						}
						// RETURN TO THE BALANCE

						if (isTheItemPaid) {
							movementToCreate.affects_balance = true;
							movementToCreate.current_balance += item.total_value;
							// SUBTRACT FROM PENDING VALUE
						} else {
							movementToCreate.affects_balance = false;
							movementToCreate.owing_value += item.total_value;
							originalMovementMustUpdatePaidValue +=
								//original_movement_item.paid_value + movementToCreate.paid_value;
								original_movement.paid_value + item.total_value;
						}
						movementToCreate.transaction_value += item.total_value;
						movementToCreate.paid_value = movementToCreate.transaction_value;
						break;
					}
					case "INVOICE":
						// C, no affects, (-)
						movementToCreate.is_immediate = false;
						movementToCreate.transaction_value -= item.total_value;
						movementToCreate.owing_value -= item.total_value;
						movementToCreate.paid_value = 0;
						break;
					case "REFUND":
						// I, affects, (-)
						movementToCreate.is_immediate = true;
						movementToCreate.affects_balance = true;
						movementToCreate.transaction_value = -item.total_value;
						movementToCreate.current_balance -= item.total_value;
						movementToCreate.paid_value = Math.abs(movementToCreate.transaction_value);
						break;
					case "INVOICE_RECEIPT":
						// I, affects?, (-)
						movementToCreate.is_immediate = true;
						movementToCreate.transaction_value -= item.total_value;
						if (movementToCreate.affects_balance) {
							movementToCreate.current_balance -= item.total_value;
						}
						movementToCreate.paid_value = Math.abs(movementToCreate.transaction_value);

						// IF IS INVOICE RECEIPT IS PAID
						item.paid_value = item.total_value;
						break;
					default:
						throw new Errors.ValidationError(
							"Movement operation is invalid",
							"MOVEMENT_OPERATION_INVALID",
							{
								operation: movementToCreate.operation,
							},
						);
				}
			});

			let movementToInsert = _.omit(movementToCreate, ["items"]);

			return this.adapter.db
				.transaction(async (trx) => {
					//Create the movement
					return this.adapter
						.db("movement")
						.transacting(trx)
						.insert(movementToInsert, "id")
						.then((x) => {
							const promises = [];

							//Create the movement items
							movementToCreate.items.forEach((itmMov) => {
								itmMov.id = uuidv4();
								itmMov.movement_id = movementToInsert.id;
								itmMov.created_at = new Date();
								itmMov.updated_at = new Date();
								promises.push(this.adapter.db("movement_item").insert(itmMov).transacting(trx));
							});

							return Promise.all(promises);
						})
						.then(() => {
							if (originalMovementMustUpdatePaidValue === false) {
								// Case: CreditNote -> update original movement PaidValue
								return Promise.resolve();
							} else {
								return this.adapter
									.db("movement")
									.where("id", "=", movementToInsert.original_movement_id)
									.update({ paid_value: originalMovementMustUpdatePaidValue })
									.transacting(trx);
							}
						})
						.then(trx.commit)
						.catch(trx.rollback);
					// Transaction Rollbacks if some went wrong...
				})
				.then((trxResult) => {
					if (this.settings.MS_DEBUG) {
						this.logger.info(" #### movementToCreate transaction ####");
						this.logger.info("  - trxResult: ");
						this.logger.info(trxResult);
					}

					if (movementToCreate.payment_method_id) {
						// IGNORE THE CREATE MOVEMENT ON CHECK BALANCE
						ctx.meta.exceptMovementId = movementToInsert.id;

						return ctx
							.call("current_account.movements.createPayment", {
								id: movementToInsert.id, // inserted movement id
								payment_method_id: movementToCreate.payment_method_id,
								cash_account_id: movementToCreate.cash_account_id,
								description: movementToCreate.description,
							})
							.then(async (resultCreatedPayment) => {
								// If created Payment IsConfirmed:
								// - update movment status
								if (resultCreatedPayment.status === "CONFIRMED") {
									// Update movement status
									let movStatus =
										resultCreatedPayment[0].status === "CONFIRMED" &&
											movementToCreate[0].operation != "CHARGE"
											? "PAID"
											: resultCreatedPayment[0].status;

									return ctx.call("current_account.movements.patch", {
										id: resultCreatedPayment[0].movement_id,
										payment_method_id: resultCreatedPayment[0].payment_method_id,
										payment_id: resultCreatedPayment[0].id,
										status: movStatus,
									});
								}
							})
							.catch(async (errCreatePayment) => {
								this.logger.info("  - trxResult.2.errCreatePayment: ");
								this.logger.info(errCreatePayment);

								const deletedMovement = await this._remove(ctx, { id: movementToInsert.id });

								this.logger.info(`  - movement deleted: ${movementToInsert.id}`);
								this.logger.info(deletedMovement);

								throw errCreatePayment;
							});
					}
				})
				.then(() => {
					return ctx.call("current_account.movements.get", {
						id: movementToInsert.id,
					});
				});
		},
		async checkBalance(ctx, account, user_id, requestTotalCharges) {

			const movements = await this.adapter.raw(`
			 SELECT * 
			 FROM movement as m 
			 WHERE 
			 m.user_id = ? 
			 AND m.account_id = ?
			 ${ctx.meta.exceptMovementId ? "AND m.id != '" + ctx.meta.exceptMovementId + "'" : ""}
			 ORDER BY m.created_at DESC
			 LIMIT 1`, [user_id, account.id]).then((d) => d.rows);

			let chargesConfirmed = null;
			if (requestTotalCharges) {
				// Get the confirmed charges SUM()
				chargesConfirmed = await this.adapter.find({
					query: (qb) => {
						qb.sum({ total_charges: "transaction_value" });
						qb.where("user_id", "=", user_id);
						qb.where("account_id", "=", account.id);
						qb.where("operation", "in", ["CHARGE"]);
						qb.where("status", "in", ["CONFIRMED"]);
						return qb;
					},
					withRelated: false,
				});
			}

			const response = {
				current_balance:
					movements[0] && movements[0].current_balance
						? roundMoney(movements[0].current_balance)
						: 0,
				in_debt:
					movements[0] && movements[0].owing_value ? roundMoney(movements[0].owing_value) : 0,
				paid_value:
					movements[0] && movements[0].paid_value ? roundMoney(movements[0].paid_value) : 0,
				advanced_value:
					movements[0] && movements[0].advanced_value ? roundMoney(movements[0].advanced_value) : 0,
				account_id: account.id,
				account_name: account.name,
				account_plafond_type: account.plafond_type,
				account_plafond_value: account.plafond_value,
				total_charges: requestTotalCharges ?
					(chargesConfirmed[0] && chargesConfirmed[0].total_charges
						? roundMoney(chargesConfirmed[0].total_charges)
						: 0) : undefined,
			};

			if (account.available_methods) {
				response.available_methods = account.available_methods;
			}

			return response;
		},
		async validateParams(ctx, res) {
			const errorObject = {
				operation: ctx.params.operation,
				transaction_id: ctx.params.transaction_id,
				transaction_value: ctx.params.transaction_value,
				items: JSON.stringify(ctx.params.items),
			};

			// Balance value can't be changed manualy
			if (_.has(ctx, "params.current_balance")) {
				throw new Errors.ValidationError(
					"Balance value will be calculate using the provide operation.",
					"INVALID_OVERRIDE_BALANCE_VALUE",
					errorObject,
				);
			}

			// Owing value can't be changed manualy
			if (_.has(ctx, "params.owing_value")) {
				throw new Errors.ValidationError(
					"In debt value will be calculate using the provide operation.",
					"INVALID_OVERRIDE_IN_DEBIT_VALUE",
					errorObject,
				);
			}
		},
		async calculateMovementItemValues(ctx, movItem) {
			if (!movItem.unit_value && !movItem.liquid_unit_value) {
				throw new Errors.ValidationError(
					"You should send unit_value or liquid_unit_value",
					"INVALID_CART_ITEM_UNIT_VALUE",
					{},
				);
			}

			const vat = await ctx.call("configuration.taxes.get", { id: movItem.vat_id });

			if (!vat[0].active) {
				throw new Errors.ValidationError("Must provide a valid VAT", "VAT_INACTIVE", {
					vat_id: movItem.vat_id,
				});
			}
			const vatMultiplier = vat[0].tax_value;
			movItem.vat = vatMultiplier;

			if (movItem.liquid_unit_value && !movItem.unit_value) {
				movItem.unit_value = movItem.liquid_unit_value * (1 + vatMultiplier);
			}

			if (movItem.unit_value && !movItem.liquid_unit_value) {
				movItem.liquid_unit_value = movItem.unit_value / (1 + vatMultiplier);
			}

			const unitValue = movItem.liquid_unit_value;
			const quantity = movItem.quantity;
			movItem.liquid_value = unitValue * quantity;

			const liquidValue = movItem.liquid_value;
			const discountValue = movItem.discount_value || 0;
			const liquidDiscountValue = discountValue / (1 + vatMultiplier);
			movItem.vat_value = (liquidValue - liquidDiscountValue) * vatMultiplier;

			if (liquidDiscountValue >= liquidValue) {
				throw new Errors.ValidationError("Invalid discount", "INVALID_DISCOUNT_VALUE", {
					liquidValue,
					liquidDiscountValue,
				});
			}

			const vatValue = movItem.vat_value;
			movItem.total_value = liquidValue - liquidDiscountValue + vatValue;

			return movItem;
		},
		async addToSendErpQueue(ctx, res) {
			if (this.settings.MS_DEBUG) {
				this.logger.info("#### ######### ####");
				this.logger.info("#### addToSendErpQueue ####");
				this.logger.info("  - ctx.params:");
				this.logger.info(ctx.params);
				this.logger.info("  - res:");
				this.logger.info(res);
			}

			const account = await ctx.call("current_account.accounts.get", {
				id: res[0].account_id, // ctx.params.account_id,
			});

			if (!account[0].middleware_erp_path) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("account[0].middleware_path:", account[0].middleware_erp_path);
				}

				await ctx.call("current_account.movements.patch", {
					id: res[0].id,
					middleware_erp_status: "SKIPPED",
				});

				return res;
			}

			const docType = await ctx.call("current_account.document_types.find", {
				query: {
					operation: res[0].operation, //ctx.params.operation,
				},
			});

			const docTypeSettings = await ctx.call("current_account.document_types_settings.find", {
				query: {
					account_id: account[0].id,
					document_type_id: docType[0].id,
				},
			});

			if (this.settings.MS_DEBUG) {
				this.logger.info("  - docTypeSettings:");
				this.logger.info(docTypeSettings);
			}

			if (docTypeSettings.length && docTypeSettings[0].send_to_erp) {
				/*
				job.data.configs, job.data.movement_id, job.data.financialMovement
				*/

				ctx
					.call("middlewares.erp.process", {
						configs: {
							middleware_path: account[0].middleware_erp_path,
							middleware_exemption_reason: account[0].middleware_erp_exemption_reason,
						},
						movement_id: res[0].id,
						financialDocument: res[0],
					})
					.catch((err) => {
						this.logger.error(err);
					});
			} else {
				await ctx.call("current_account.movements.patch", {
					id: res[0].id,
					middleware_erp_status: "SKIPPED",
				});
			}

			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
