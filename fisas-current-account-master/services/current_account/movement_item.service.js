"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
//const { ALLOWED_CURRENCIES } = require("../utils/current_account_constants");
const { roundMoney } = require("../utils/operations");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.items",
	table: "movement_item",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "items")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"service_id",
			"movement_id",
			"product_code",
			"name",
			"description",
			"extra_info",
			"total_value",
			"quantity",
			"liquid_unit_value",
			"unit_value",
			"liquid_value",
			"discount_value",
			"paid_value",
			"vat_id",
			"vat",
			"vat_value",
			"location",
			"article_type",
			"service_confirm_path",
			"service_cancel_path",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: ["service"],

		withRelateds: {
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.services", "service", "service_id");
			},
		},

		entityValidator: {
			service_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			movement_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			product_code: { type: "string", max: 120, optional: false },
			name: { type: "string", max: 255, optional: false },
			description: { type: "string", max: 250, optional: true },
			extra_info: { type: "object", optional: true },
			total_value: { type: "number", optional: false },
			quantity: { type: "number", integer: true, positive: true, convert: true, optional: false },
			liquid_unit_value: { type: "number", positive: true, convert: true, optional: true },
			unit_value: { type: "number", positive: true, convert: true, optional: true },
			liquid_value: { type: "number", positive: true, optional: false },
			discount_value: { type: "number", min: 0, optional: false, default: 0 },
			vat_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			vat: { type: "number", min: 0, optional: false },
			vat_value: { type: "number", min: 0, optional: false },
			location: { type: "string", max: 255, optional: false },
			article_type: { type: "string", max: 120, optional: true },
			service_confirm_path: { type: "string", max: 120, optional: true },
			service_cancel_path: { type: "string", max: 120, optional: true },
			//paid_value: { type: "number", min: 0, optional: false },
			//service_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			original_movement_id: {
				type: "number",
				integer: true,
				positive: true,
				min: 1,
				optional: true,
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"calculateValues",
				function sanatizeParams() {
					//SET BY DATABASE (DEFAULT CURRENT_TIMESTAMP)
					//ctx.params.created_at = new Date();
					//ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		create: {
			visibility: "public",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				// Generate GUID
				const params = _.omit(ctx.params, ["id"]);
				params.id = uuidv4();

				return this._create(ctx, params);
			},
		},
		patch: {
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async calculateValues(ctx, res) {
			if (_.has(ctx, "params.unit_value") && _.has(ctx, "params.quantity")) {
				const unitValue = ctx.params.unit_value;
				const quantity = ctx.params.quantity;
				ctx.params.liquid_value = unitValue * quantity;
			}

			const liquidValue = ctx.params.liquid_value;
			const discountValue = ctx.params.discount_value || 0;

			if (discountValue >= liquidValue) {
				throw new Errors.ValidationError("Invalid discount", "INVALID_DISCOUNT_VALUE", {
					liquidValue,
					discountValue,
				});
			}

			// VAT
			if (_.has(ctx, "params.vat_id")) {
				const vat = await ctx.call("configuration.taxes.get", { id: ctx.params.vat_id });
				if (_.isEmpty(vat)) {
					throw new Errors.ValidationError("Must provide a valid VAT", "VAT_NOT_FOUND", {
						vat_id: ctx.params.vat_id,
					});
				}
				if (!vat[0].active) {
					throw new Errors.ValidationError("Must provide a valid VAT", "VAT_INACTIVE", {
						vat_id: ctx.params.vat_id,
					});
				}

				const vatMultiplier = vat[0].tax_value;
				ctx.params.vat = vatMultiplier;

				// vat_value = (liquid_value + (-discount_value) ) * vat_index
				ctx.params.vat_value = roundMoney((liquidValue - discountValue) * vatMultiplier);
			} else {
				throw new Errors.ValidationError("Must provide a valid VAT", "VAT_NOT_FOUND", {
					vat_id: ctx.params.vat_id,
				});
			}

			// total_value = liquid_value + (-discount_value) + vat_value
			const vatValue = ctx.params.vat_value;

			ctx.params.total_value = roundMoney(liquidValue - discountValue + vatValue);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
