"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "current_account.payment_method_settings",
	table: "payment_method_settings",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "accounts")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"account_id",
			"payment_method_id",
			"min_charge_amount",
			"max_charge_amount",
			"updated_at",
			"created_at",
		],

		defaultWithRelateds: ["payment_method"],

		withRelateds: {
			payment_method(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"payments.payment-methods",
					"payment_method",
					"payment_method_id",
					"id",
					{},
					"id,name,tag",
					false,
				);
			},
		},

		entityValidator: {
			payment_method_id: { type: "uuid", version: 4, optional: false },
			account_id: { type: "number", integer: true, positive: true, min: 1 },
			min_charge_amount: { type: "number", positive: true, min: 1 },
			max_charge_amount: { type: "number", positive: true, min: 1 },
			updated_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function validateIfUniq(ctx) {
					const total = await ctx.call("current_account.payment_method_settings.count", {
						query: {
							payment_method_id: ctx.params.payment_method_id,
							account_id: ctx.params.account_id,
						},
					});

					if (total > 0) {
						throw new Errors.ValidationError(
							"Payment method already associated to account",
							"PAYMENT_METHOD_ALREADY_ASSOCIATED_ACCOUNT",
							{},
						);
					}
				},
			],
			update: [],
			patch: [],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
