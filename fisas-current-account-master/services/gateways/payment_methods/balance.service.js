"use strict";

const { Errors } = require("moleculer");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-method-balance",

	/**
	 * Settings
	 */
	settings: {},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) { }],
			update: [function sanatizeParams(ctx) { }],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		isAvailable: {
			handler(ctx) {
				return true;
			},
		},
		canPay: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
				value: { type: "number", positive: true },
				payment_id: { type: "uuid", version: 4 },
				description: { type: "string" },
			},
			async handler(ctx) {
				ctx.meta.bypass_bo_validation = true;
				return ctx
					.call("current_account.movements.balance", {
						user_id: ctx.params.user_id,
						account_id: ctx.params.account_id,
					})
					.then((account) => {
						this.logger.info();
						this.logger.info(" ### [payments.payment-method-balance].canPay:");
						this.logger.info(`   - account: ${account.current_balance}`);
						this.logger.info(account);
						this.logger.info(`   - account.current_balance: ${account.current_balance}`);
						this.logger.info(`   - account.plafond_type: ${account.account_plafond_type}`);
						this.logger.info(`   - account.plafond_value: ${account.account_plafond_value}`);
						this.logger.info(`   - ctx.params.value: ${ctx.params.value}`);

						const funds =
							parseFloat(account.current_balance) + parseFloat(account.account_plafond_value || 0);

						this.logger.info(`   - funds: ${funds}`);

						if (funds >= ctx.params.value) {
							return { canPay: true, payment_method_data: {} };
						} else {
							return {
								canPay: false,
								payment_method_data: {},
								error: new Errors.ValidationError(
									"Not enough funds to make this payment",
									"INSUFFICIENT_FUNDS_FOR_PAYMENT",
									{},
								),
							};
						}
					});
			},
		},
		confirm: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				payment_id: { type: "uuid", version: 4, optional: true },
			},
			async handler(ctx) {
				return {
					status: "CONFIRMED",
					isConfirmed: true,
					isCancelled: false,
				};
				//return true;
			},
		},
		cancel: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return false;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
