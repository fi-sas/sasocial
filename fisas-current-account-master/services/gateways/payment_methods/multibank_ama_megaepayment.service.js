"use strict";

const _ = require("lodash");
const Cron = require("moleculer-cron");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-method-multibank_ama_megaepayment",
	mixins: [Cron],
	/*
	 * Check pending transactions
	 */
	/*crons: [
		{
			name: "checkPendingTransactions",
			cronTime: "0 * * * *", // Cron job every hour
			onTick: function () {
				this.getLocalService("payments.payment-method-multibank_ama_megaepayment")
					.actions.checkPendingTransactions()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],*/

	/**
	 * Settings
	 */
	settings: {
		servicePath: "ppap.megaepayment",
	},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) { }],
			update: [function sanatizeParams(ctx) { }],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		isAvailable: {
			visibility: "published",
			rest: "GET /isAvailable",
			params: {},
			async handler(ctx) {
				// CHECK IF MS PPAP for generating MBRef's by AMA is Available
				return ctx
					.call(`${this.settings.servicePath}.ping`)
					.then((pingResult) => {
						return pingResult === "pong";
					})
					.catch((err) => {
						this.logger.info("### multibank_ama.isAvailable(ping) Error###");
						this.logger.info(err);
						return false;
					});
			},
		},
		canPay: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
				value: { type: "number", positive: true },
				payment_id: { type: "uuid", version: 4 },
				description: { type: "string" },
			},
			async handler(ctx) {
				return (
					ctx
						//.call(`${this.settings.servicePath}.debug`, {// <== ##### DEBUG FOR LOCAL MS PPAP/AMA - Fake RefMB #####
						.call(`${this.settings.servicePath}.${this.DEBUG_MODE}createMBRef`, {
							payment_id: ctx.params.payment_id,
							description: ctx.params.description,
							user_id: ctx.params.user_id,
							amount: ctx.params.value,
						})
						.then((resultRefMB) => {
							this.logger.info();
							this.logger.info(`   - ${this.settings.servicePath}.createMBRef => resultRefMB:`);
							this.logger.info(resultRefMB);
							this.logger.info();

							let canPayResult = { canPay: true, payment_method_data: resultRefMB };
							return canPayResult;
						})
						.then((canPayResult) => {
							// SEND NOTIFICATION
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "CC_PAYMENTS_REFMB_AMA_TRANSACTION_DATA",
								user_id: ctx.params.user_id,
								user_data: {},
								data: {
									name: ctx.meta.user.name,
									description: ctx.params.description,
									payment_method_data: canPayResult.payment_method_data,
								},
								variables: {},
								external_uuid: ctx.params.payment_id,
							});

							return canPayResult;
						})
				);
			},
		},
		confirm: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				payment_id: { type: "uuid", version: 4, optional: true },
			},
			async handler(ctx) {
				if (ctx.meta.payment_method_data) {
					const checkTransactionResult = await ctx
						//.call(`${this.settings.servicePath}.debug_checkTransaction`, {// <== ##### DEBUG FOR LOCAL MS PPAP/AMA - Fake RefMB #####
						.call(`${this.settings.servicePath}.${this.DEBUG_MODE}checkTransaction`, {
							transaction_id: ctx.meta.payment_method_data.TransactionId,
						});

					this.logger.info("checkTransactionResult: ");
					this.logger.info(checkTransactionResult);

					this.logger.info("return: ");
					this.logger.info({
						status: checkTransactionResult.StateName,
						isConfirmed: checkTransactionResult.StateName === "CAPTURADO",
						isCancelled: _.includes(["CANCELADO", "EXPIRADO"], checkTransactionResult.StateName),
					});

					return {
						status: checkTransactionResult.StateName,
						isConfirmed: checkTransactionResult.StateName === "CAPTURADO",
						isCancelled: _.includes(["CANCELADO", "EXPIRADO"], checkTransactionResult.StateName),
					};
				}
				return false;
			},
		},
		cancel: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return true;
			},
		},
		checkPendingTransactions: {
			async handler(ctx) {
				//this.logger.info("Cron/TICK: check pending transactions if confirmed at AMA!");

				//if (this.DEBUG_MODE.length)
				//	return false;// <== ##### DEBUG UNCOMMENT FOR LOCAL MS PPAP/AMA - Fake RefMB #####

				const refMBPaymentMethod = await ctx.call("payments.payment-methods.find", {
					withRelated: false,
					fields: ["id", "tag"],
					query: {
						tag: "REFMB_AMA",
					},
				});

				this.logger.info(" - refMBPaymentMethod: ");
				this.logger.info(refMBPaymentMethod);

				ctx
					.call("payments.payments.find", {
						fields: ["id", "payment_method_data"],
						offset: 0,
						limit: 10,
						sort: "-created_at",
						query: {
							status: "PENDING",
							payment_method_id: refMBPaymentMethod[0].id,
						},
					})
					.then((waitingPayments) => {
						this.logger.info(
							`Cron/TICK: [ ${waitingPayments.length} ] refMB/payment waiting for confirmation!`,
						);

						/*ctx.mcall(
								waitingPayments.map((p) => ({
									action: "payments.payments.confirm",
									params: { id: p.id },
								})),
							)
							.catch((err) => {
								this.logger.info("err.2");
								this.logger.info(err);
							});*/
						waitingPayments.map((p) => {
							ctx.call("payments.payments.confirm", { id: p.id }).catch((err) => {
								this.logger.info("err.2");
								//this.logger.info(err);

								let that = this;

								Promise.reject(`RefMb Confirmation fail! ${err.code} - ${err.type}`).then(
									function(errMsg) {
										// não executado
									},
									function(errMsg) {
										that.logger.info(errMsg);
									},
								);
							});
						});
					})
					.catch((err) => {
						this.logger.info("err.1");
						this.logger.info(err);
						throw err;
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.DEBUG_MODE = process.env.NODE_ENV == "development" ? "debug_" : "";
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
