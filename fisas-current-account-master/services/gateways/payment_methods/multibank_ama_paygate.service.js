"use strict";

const Cron = require("moleculer-cron");
require("dotenv").config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-method-multibank_ama_paygate",
	//mixins: [Cron],
	/*
	 * Check pending transactions
	 */
	/*crons: [
		{
			name: "checkPendingTransactions",
			cronTime: process.env.CRON_TIME_CHECK_PENDING_AMA_TRANSACTIONS || "0 * * * *", //"0 * * * *", // Cron job every hour
			onTick: function() {
				this.getLocalService("payments.payment-method-multibank_ama_paygate")
					.actions.checkPendingTransactions()
					.then(() => {});
			},
			runOnInit: function() {},
		},
	],*/

	/**
	 * Settings
	 */
	settings: {
		MS_DEBUG: process.env.MS_DEBUG == "true",
		ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS:
			process.env.ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS,
		servicePath: "ppap.paygate",
	},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) {}],
			update: [function sanatizeParams(ctx) {}],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		isAvailable: {
			visibility: "published",
			rest: "GET /isAvailable",
			params: {},
			async handler(ctx) {
				// CHECK IF MS PPAP for generating MBRef's by AMA is Available
				return ctx
					.call(`${this.settings.servicePath}.ping`)
					.then((pingResult) => {
						return pingResult === "pong";
					})
					.catch((err) => {
						this.logger.info("### multibank_ama.isAvailable(ping) Error###");
						this.logger.info(err);
						return false;
					});
			},
		},
		canPay: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
				value: { type: "number", positive: true },
				payment_id: { type: "uuid", version: 4 },
				description: { type: "string" },
			},
			async handler(ctx) {
				return ctx
					.call(`${this.settings.servicePath}.createMBRef`, {
						payment_ref: ctx.params.payment_id,
						description: ctx.params.description,
						user_id: ctx.params.user_id,
						amount: ctx.params.value,
					})
					.then((resultRefMB) => {
						if (this.settings.MS_DEBUG) {
							this.logger.info();
							this.logger.info(`   - ${this.settings.servicePath}.createMBRef => resultRefMB:`);
							this.logger.info(resultRefMB);
							this.logger.info();
						}

						let canPayResult = { canPay: true, payment_method_data: resultRefMB };
						return canPayResult;
					})
					.then((canPayResult) => {
						let createdRefMB = canPayResult.payment_method_data;
						Object.assign(createdRefMB, {
							Amount_with_currency_symbol: `€ ${createdRefMB.Amount.toFixed(2)}`,
						});

						if (ctx.meta.user) {
							// 1. If ctx.meta.user then isn't from AMA Middleware callback

							// SEND NOTIFICATION
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "CC_PAYMENTS_REFMB_AMA_TRANSACTION_DATA",
								user_id: ctx.params.user_id,
								user_data: {},
								data: {
									name: ctx.meta.user.name,
									description: ctx.params.description,
									payment_method_data: createdRefMB,
								},
								variables: {},
								external_uuid: ctx.params.payment_id,
							});
						}

						return canPayResult;
					});
			},
		},
		confirm: {
			visibility: "public",
			params: {
				//user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				//account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				payment_id: { type: "uuid", version: 4, optional: true },
			},
			async handler(ctx) {
				if (ctx.params.payment_id) {
					const checkTransactionResult = await ctx.call(
						`${this.settings.servicePath}.checkTransaction`,
						{
							payment_ref: ctx.params.payment_id,
						},
					);

					if (this.settings.MS_DEBUG) {
						this.logger.info("[Paygate/REFMB_AMA] checkTransactionResult: ");
						this.logger.info(checkTransactionResult);
					}

					/* Expected:
						{
							"PaymentTypeCode": "REFMB",
							"OperationProgressStatus": 0,
							"Success": true,
							"InProgress": false,
							"ReturnCode": "00000",
							"ShortReturnMessage": "Transação Aprovada",
							"LongReturnMessage": "Transação Aprovada",
							"SessionToken": "C492AE3E35DE47FE8201D6BAE88726B9",
							"TransactionID": "77006000001058",
							"PaymentID": 20261,
							"WalletID": null
						}
					*/

					const operationProgressStatus = { 0: "CAPTURADO", 2: "PENDENTE", "-1": "ERRO" };

					if (this.settings.MS_DEBUG) {
						this.logger.info("  - returned operationProgressStatus: ");
						this.logger.info({
							status:
								operationProgressStatus[checkTransactionResult.OperationProgressStatus.toString()],
							isConfirmed:
								checkTransactionResult.OperationProgressStatus == 0 /*  0 -> Operation performed*/,
							isInProgress:
								checkTransactionResult.OperationProgressStatus ==
								2 /*  2 -> Operation in progress*/,
							isCancelled:
								checkTransactionResult.OperationProgressStatus == -1 /* -1 -> Error ocurred*/,
						});
					}

					return {
						status:
							operationProgressStatus[
								checkTransactionResult.OperationProgressStatus.toString()
							] /*0 -> Operation performed*/,
						isConfirmed:
							checkTransactionResult.OperationProgressStatus == 0 /*  0 -> Operation performed*/,
						isInProgress:
							checkTransactionResult.OperationProgressStatus == 2 /*  2 -> Operation in progress*/,
						isCancelled:
							checkTransactionResult.OperationProgressStatus == -1 /* -1 -> Error ocurred*/,
						transactionReceiptData: JSON.stringify(checkTransactionResult),
					};
				}
				return false;
			},
		},
		cancel: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return true;
			},
		},
		checkPendingTransactions: {
			async handler(ctx) {
				this.logger.info("[Paygate/REFMB_AMA] checkPendingTransactions ...");

				// Check if Cron for Check Pending transactions is enabled!
				if (!this.settings.ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS) {
					return `ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS(${this.settings.ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS})`;
				}

				const refMBPaymentMethod = await ctx.call("payments.payment-methods.find", {
					withRelated: false,
					fields: ["id", "tag"],
					query: {
						tag: "REFMB_AMA",
					},
				});

				ctx
					.call("payments.payments.find", {
						fields: ["id", "payment_method_data"],
						offset: 0,
						limit: 10,
						sort: "-created_at",
						query: {
							status: "PENDING",
							payment_method_id: refMBPaymentMethod[0].id,
						},
					})
					.then(async (waitingPayments) => {
						this.logger.info(
							`Cron/TICK: [ ${waitingPayments.length} ] refMB/payment waiting for confirmation!`,
						);

						for (const payment of waitingPayments) {
							await ctx.call("payments.payments.confirm", { id: payment.id }).catch((err) => {
								this.logger.error(
									`RefMb Confirmation fail! ID: ${payment.id} ${err.code} - ${err.type}`,
								);
							});
						}
						return Promise.resolve();
					})
					.catch((err) => {
						this.logger.error("[Paygate/REFMB_AMA] checkPendingTransactions err.1");
						this.logger.error(err);
						throw err;
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.DEBUG_MODE = process.env.NODE_ENV == "development" ? "debug_" : "";

		this.logger.info(
			"[Paygate/REFMB_AMA] ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS:",
			process.env.ENABLE_CRON_CHECK_PENDING_AMA_TRANSACTIONS == "true",
		);
		this.logger.info(
			"[Paygate/REFMB_AMA] CRON_TIME_CHECK_PENDING_AMA_TRANSANCTIONS:",
			process.env.CRON_TIME_CHECK_PENDING_AMA_TRANSACTIONS || "0 * * * *",
		);
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
