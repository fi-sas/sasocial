"use strict";

const { UnauthorizedError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-method-tpa-kiosk",

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		isAvailable: {
			handler(ctx) {
				this.logger.info("The Meta : ");
				this.logger.info(ctx.meta);
				return !!ctx.meta.device.CPUID;
			},
		},
		canPay: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
				value: { type: "number", positive: true },
				payment_id: { type: "uuid", version: 4 },
				description: { type: "string" },
			},
			async handler(ctx) {
				return {
					canPay: true,
					payment_method_data: {},
				};
			},
		},
		confirm: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				CPUID: { type: "string", max: 64 },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				payment_id: { type: "uuid", version: 4, optional: true },
			},
			async handler(ctx) {
				// CAN I TRUST THIS CONFIRM ?
				// IS THE SAME DEVICE ?

				return ctx
					.call("payments.payments.get", {
						id: ctx.params.payment_id,
						withRelateds: false,
					})
					.then((payment) => {
						return ctx.call("current_account.movements.get", {
							id: payment[0].movement_id,
							withRelateds: false,
						});
					})
					.then((movement) => {
						//IS THE SAME DEVICE
						if (
							movement[0].device_id !== ctx.meta.device.id ||
							ctx.params.CPUID !== ctx.meta.device.CPUID
						) {
							return {
								status: "CANCELLED",
								isConfirmed: false,
								isCancelled: true,
							};
						}

						return {
							status: "CONFIRMED",
							isConfirmed: true,
							isCancelled: false,
						};
					});
			},
		},
		cancel: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return true;
			},
		},
		callback: {
			visibility: "published",
			rest: "POST /callback",
			params: {
				id: { type: "uuid" }, //payment_id
				client_receipt: { type: "string", optional: true },
				merchant_receipt: { type: "string", optional: true },
				cancel: { type: "boolean", optional: true },
				CPUID: { type: "string", max: 64 },
			},
			handler(ctx) {
				if (ctx.params.CPUID !== ctx.meta.device.CPUID) {
					throw new UnauthorizedError(
						"The payment callback is not valid",
						"PAYMENT_TPA_CONFIRM_INVALID",
						{},
					);
				}

				if (ctx.params.cancel === true) {
					return ctx.call("payments.payments.cancel", ctx.params);
				}
				delete ctx.params.cancel;

				return ctx.call("payments.payments.confirm", ctx.params);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
