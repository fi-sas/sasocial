"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-method-tpa",

	/**
	 * Settings
	 */
	settings: {},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) { }],
			update: [function sanatizeParams(ctx) { }],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		isAvailable: {
			handler(ctx) {
				return true;
			},
		},
		canPay: {
			visibility: "public",
			params: {
				account_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
				value: { type: "number", positive: true },
				payment_id: { type: "uuid", version: 4 },
				description: { type: "string" },
			},
			async handler(ctx) {
				return { canPay: true, payment_method_data: {} };
			},
		},
		confirm: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return {
					status: "CONFIRMED",
					isConfirmed: true,
					isCancelled: false,
				};
				//return true;
			},
		},
		cancel: {
			visibility: "public",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
				account_id: { type: "number", integer: true, min: 1, optional: true, convert: true },
			},
			async handler(ctx) {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
