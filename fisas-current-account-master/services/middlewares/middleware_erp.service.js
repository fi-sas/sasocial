"use strict";
const QueueService = require("moleculer-bull");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "middlewares.erp",
	mixins: [QueueService(process.env.REDIS_QUEUE)],

	queues: {
		"financialDocument.process"(job) {
			this.logger.info("[middlewares.erp] NEW JOB RECEIVED!", job.data);
			job.progress(10);

			return this.process(job.data.configs, job.data.movement_id, job.data.financialDocument)
				.then(() => {
					this.logger.warn("[middlewares.erp] QUEUE before resolve");

					return this.Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.error(" ");
					this.logger.error("[middlewares.erp] QUEUE before reject");
					this.logger.error("[middlewares.erp] err");
					this.logger.error(JSON.stringify(err));

					return Promise.reject(err);
				});
		},
	},

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Process/create financial document: configs and movement data.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * ...
		 */
		process: {
			params: {
				movement_id: { type: "uuid", version: 4 },
				configs: {
					type: "object",
					strict: "remove",
					props: {
						middleware_path: { type: "string" },
						middleware_exemption_reason: { type: "string" },
					},
				},
				financialDocument: {
					type: "object",
					strict: false,
					props: {
						id: { type: "uuid", version: 4 },
						account_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
						operation: { type: "string", optional: false },
						device_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
						user_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
						entity: { type: "string", max: 120, optional: true },
						tin: { type: "string", max: 9, optional: true },
						email: { type: "string", max: 120, optional: true },
						address: { type: "string", max: 250, optional: true },
						postal_code: { type: "string", max: 10, optional: true },
						city: { type: "string", max: 250, optional: true },
						country: { type: "string", max: 250, optional: true },
						description: { type: "string", max: 250, optional: true },
						payment_method_id: { type: "uuid", version: 4, optional: true },
						expiration_at: { type: "date", optional: true },
						items: {
							type: "array",
							items: {
								type: "object",
								strict: false,
								props: {
									service_id: {
										type: "number",
										integer: true,
										positive: true,
										min: 1,
										optional: false,
									},
									product_code: { type: "string", max: 120, optional: false },
									name: { type: "string", max: 255, optional: false },
									description: { type: "string", max: 250, optional: true },
									extra_info: { type: "object", optional: true },
									quantity: {
										type: "number",
										integer: true,
										positive: true,
										convert: true,
										optional: false,
									},
									liquid_unit_value: {
										type: "number",
										positive: true,
										convert: true,
										optional: true,
									},
									unit_value: { type: "number", positive: true, convert: true, optional: true },
									discount_value: {
										type: "number",
										min: 0,
										optional: true,
										default: 0,
									},
									vat_id: {
										type: "number",
										integer: true,
										positive: true,
										min: 1,
										optional: false,
									},
									location: { type: "string", max: 255, optional: false },
									article_type: { type: "string", max: 120, optional: true },
									service_confirm_path: { type: "string", max: 120, optional: true },
									service_cancel_path: { type: "string", max: 120, optional: true },
								},
							},
						},
					},
				},
			},
			async handler(ctx) {
				this.logger.info("middlewares.erp.process / ctx.params:");
				this.logger.info(ctx.params);

				this.addJob(ctx);
				return this.Promise.resolve(true);
			},
		},
		/**
		 * Validate configurations for sending to middleware erp.
		 * Returns true if successful. Otherwise, throws an Error.
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				middleware_path: { type: "string" },
				middleware_exemption_reason: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		addJob(ctx) {
			this.logger.info("CREATE JOB RECEIVED!", ctx.params);
			this.createJob(
				"financialDocument.process",
				{ ...ctx.params, id: ctx.params.movement_id, pid: process.pid },
				{
					attempts: 1,
					removeOnComplete: true,
					removeOnFail: true,
				},
			);

			return true;
		},
		/**
		 * Send the document
		 * @param {*} configs
		 * @param {*} movement_id
		 * @param {*} financialDocument
		 */
		async process(configs, movement_id, financialDocument) {
			this.logger.warn("#### --------- process (SEND_TO_ERP) ----------- ########");
			this.logger.warn(`${configs.middleware_path}.createFinancialDocument`);

			return this.broker
				.call(`${configs.middleware_path}.createFinancialDocument`, financialDocument)
				.then((result) => {
					this.logger.warn("");
					this.logger.warn("MIDDLEWARE ERP devolveu.1: ");
					this.logger.warn(`${configs.middleware_path}.createFinancialDocument result:`);
					this.logger.warn(result);

					return result;
				})
				.then((resMiddleware) => {
					this.logger.warn("");
					this.logger.warn("MIDDLEWARE ERP devolveu.2: ");
					this.logger.warn("resMiddleware: ", resMiddleware);

					if (!resMiddleware.valid) {
						this.logger.error("resMiddleware: ", resMiddleware);

						return this.broker.call("current_account.movements.patch", {
							id: movement_id,
							middleware_erp_result: JSON.stringify(resMiddleware),
							middleware_erp_status: "FAILED",
						});
					}

					return this.broker
						.call("current_account.documents.create", {
							type: financialDocument.operation,
							series: resMiddleware.series,
							number: resMiddleware.number,
							issued_at: moment(resMiddleware.issued_at).toDate(),
							url: resMiddleware.pdf_link.urlDirectDownload
								? resMiddleware.pdf_link.urlDirectDownload
								: null,
							document_erp_id: resMiddleware.document_id,
						})
						.then((resultCreatedDocument) => {
							this.logger.warn("current_account.documents.create / resultCreatedDocument: ");
							this.logger.warn(resultCreatedDocument);
							this.logger.warn("  - movement_id: ");
							this.logger.warn(movement_id);

							return this.broker.call("current_account.movements.patch", {
								id: movement_id,
								document_erp_id: resMiddleware.document_id,
								document_erp_number: resMiddleware.series + "/" + resMiddleware.number,
								document_id: resultCreatedDocument[0].id,
								document_erp_url: resMiddleware.pdf_link.urlDirectDownload
									? resMiddleware.pdf_link.urlDirectDownload
									: null,
								middleware_erp_result: JSON.stringify(resMiddleware),
								middleware_erp_status: "SUCCESS",
							});
						})
						.catch((err) => {
							this.logger.warn("");
							this.logger.warn("ERRO AO FAZER UPDATE AO MOVIMENTO COM RESULTADO DO MIDDLEWARE!");

							this.logger.error(`${configs.middleware_path}.createFinancialDocument/process err:`);
							this.logger.error(err);

							return this.broker.call("current_account.movements.patch", {
								id: movement_id,
								middleware_erp_result: JSON.stringify(err),
								middleware_erp_status: "FAILED",
							});
						});
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.getQueue("financialDocument.process").on("progress", (jobID, progress) => {
			this.logger.warn(`Job #${jobID} progress is ${progress}%`);
		});

		this.getQueue("financialDocument.process").on("completed", (job, res) => {
			this.logger.warn("financialDocument.process / on_completed fired!");
		});

		this.getQueue("financialDocument.process").on("failed", (job, err) => {
			this.logger.warn("");
			this.logger.warn("CATCH PORQUE O MIDDLEWARE DEVOLVEU ERRO!");

			this.logger.error(`Job #${job.id} on_failed fired!. movement patched:`);
			this.logger.error("on_failed / err:");
			this.logger.error(err);

			this.broker
				.call("current_account.movements.patch", {
					id: job.data.movement_id,
					middleware_erp_result: JSON.stringify(err),
					middleware_erp_status: "FAILED",
				})
				.then(async (resMovementErpStatusUpdated) => {
					//NOTIFICAR RESPONSÁVEL CC

					const account = await this.broker.call("current_account.accounts.get", {
						id: job.data.financialDocument.account_id,
						withRelated: "helpdesk_user",
					});

					if (account[0].helpdesk_user) {
						this.logger.error(" ");
						this.logger.error(
							`[middlewares.erp] Email sent to Helpdesk/CC (${account[0].helpdesk_user.email})!`,
						);

						const financial_document_stringify = JSON.stringify(
							resMovementErpStatusUpdated[0],
							null,
							" ",
						);
						const middleware_erp_message_stringify = JSON.stringify(err, null, " ");

						this.logger.error("[middlewares.erp] resMovementErpStatusUpdated[0]:");
						this.logger.error(resMovementErpStatusUpdated[0]);

						this.broker.call("notifications.alerts.create_alert", {
							alert_type_key: "CC_MIDDLEWARE_ERP_ERROR",
							user_id: account[0].helpdesk_user.id, //<-- configured HELPDESK/CC
							user_data: account[0].helpdesk_user,
							data: {
								account_name: account[0].name,
								financial_document: resMovementErpStatusUpdated[0],
								financial_document_stringify,
								middleware_erp_message: err,
								middleware_erp_message_stringify,
							},
							variables: {},
							external_uuid: job.data.movement_id,
						});
					}

					return Promise.resolve(true);
				});
		});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
