"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { MoleculerClientError } = require("moleculer").Errors;
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");
const Cron = require("moleculer-cron");
const moment = require("moment");
require("dotenv").config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.cart",
	table: "cart",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("payments", "cart"), Cron],

	/*
	 * Remove old carts
	 */
	crons: [
		{
			name: "removeOldCarts",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("payments.cart")
					.actions.removeOldCarts()
					.then(() => { });
			},
			runOnInit: function () { },
		},
	],

	/**
	 * Settings
	 */
	settings: {
		MS_DEBUG: process.env.MS_DEBUG == "true",
		clear_time_interval: 15, // !% MINUTEs MAXIMMUM OLD TIME
		fields: [
			"id",
			"user_id",
			"account_id",
			"device_id",
			"payment_method_id",
			"payment_method_data",
			"is_blocked",
			"checkout_at",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["items"],
		withRelateds: {
			items(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "payments.cart-item", "items", "id", "cart_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", integer: true, min: 1 },
			account_id: { type: "number", integer: true, min: 1 },
			device_id: { type: "number", integer: true, min: 1 },
			payment_method_id: { type: "uuid", version: 4, optional: true },
			payment_method_data: { type: "object", optional: true },
			checkout_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			find: [
				function addSort(ctx) {
					if (!ctx.params.sort) {
						ctx.params.sort = "-account_id";
					}
				},
				function addFilters(ctx) {
					ctx.params.query = ctx.params.query || {};

					// FOR BO AND POS ADD ITEM
					let userID = ctx.meta.user ? ctx.meta.user.id : null;
					if (
						ctx.meta.device &&
						(ctx.meta.device.type === "BO" || ctx.meta.device.type === "POS") &&
						ctx.params.user_id
					) {
						userID = ctx.params.user_id;
					}

					// GET THE CARTS OF THE USER && CREATE IN THE CURRENT DEVICE
					ctx.params.query.user_id = userID;
					ctx.params.query.device_id = ctx.meta.device.id;
				},
			],
			list: [
				function addSort(ctx) {
					if (!ctx.params.sort) {
						ctx.params.sort = "-account_id";
					}
				},
				function addFilters(ctx) {
					ctx.params.query = ctx.params.query || {};

					// FOR BO AND POS ADD ITEM
					let userID = ctx.meta.user ? ctx.meta.user.id : null;
					if (
						ctx.meta.device &&
						(ctx.meta.device.type === "BO" || ctx.meta.device.type === "POS") &&
						ctx.params.user_id
					) {
						userID = ctx.params.user_id;
					}

					// GET THE CARTS OF THE USER && CREATE IN THE CURRENT DEVICE
					ctx.params.query.user_id = userID;
					ctx.params.query.device_id = ctx.meta.device.id;
				},
			],
			create: [
				function sanatizeParams(ctx) {
					if (ctx.meta.isGuest) {
						this.logger.warn("");
						this.logger.warn(" -> isGuest: ", ctx.meta.isGuest);

						throw new MoleculerClientError(
							"Guest operation not allowed",
							403,
							"CC_GUEST_OPERATION_NOT_ALLOWED",
							{},
						);
					}

					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();

					// FOR BO AND POS ADD ITEM
					let userID = ctx.meta.user ? ctx.meta.user.id : null;
					if (
						ctx.meta.device &&
						(ctx.meta.device.type === "BO" || ctx.meta.device.type === "POS") &&
						ctx.params.user_id
					) {
						userID = ctx.params.user_id;
					}
					ctx.params.user_id = userID;

					ctx.params.device_id = ctx.meta.device ? ctx.meta.device.id : null;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				async function validateUniqueCart(ctx, res) {
					if (_.has(ctx, "params.account_id")) {
						const account_id = ctx.params.account_id;

						const device_id = ctx.params.device_id;
						const user_id = ctx.params.user_id;
						const cart = await this._find(ctx, { query: { user_id, account_id, device_id } });
						if (!_.isEmpty(cart)) {
							throw new Errors.ValidationError(
								"Already exist a card in this context",
								"CART_ALREADY_EXISTS",
								{
									account_id,
								},
							);
						}
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			checkout: [
				function blockCartPreventMuiltiCheckout(ctx) {
					//ctx.params.updated_at = new Date();
					this.logger.info("");
					this.logger.info("before::blockCartPreventMultiCheckout");
				},
			],
		},
		after: {
			checkout: [
				function unblockCartPreventMuiltiCheckout(ctx) {
					//ctx.params.updated_at = new Date();
					this.logger.info("");
					this.logger.info("after::unblockCartPreventMultiCheckout");
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			rest: "GET /",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#device.id",
				],
			},
		},
		clearCart: {
			visibility: "published",
			rest: "POST /clearcart",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true },
				account_id: { type: "number", integer: true, min: 1, optional: true },
			},
			async handler(ctx) {
				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });

				const query = {
					user_id: ctx.params.user_id || ctx.meta.user.id,
					device_id: ctx.meta.device.id,
				};

				if (ctx.params.account_id) {
					// DELETE ONLY THIS ACCOUNT ID
					query.account_id = ctx.params.account_id;
				}

				let carts = await this._find(ctx, {
					withRelated: false,
					query,
				});

				if (carts.length < 1) {
					return null;
				}

				return ctx.mcall(
					carts.map((cart) => ({ action: "payments.cart.remove", params: { id: cart.id } })),
				);
			},
		},
		addCartItem: {
			visibility: "published", //"public",
			rest: "POST /additem",
			scope: "payments:cart:create",
			params: {
				account_id: { type: "number", integer: true, min: 1 },
				service_id: { type: "number", integer: true, min: 1 },
				product_code: { type: "string", max: 120 },
				article_type: { type: "string", max: 120, optional: true },
				quantity: { type: "number", integer: true, min: 1 },
				max_quantity: { type: "number", min: 0, convert: true, optional: true },
				unit_value: { type: "number", min: 0, convert: true, optional: true },
				liquid_unit_value: { type: "number", min: 0, convert: true, optional: true },
				vat_id: { type: "number", integer: true, min: 1 },
				name: { type: "string", max: 250 },
				description: { type: "string", max: 250 },
				location: { type: "string", max: 120, optional: true },
				extra_info: { type: "object", optional: true },
				expires_in: { type: "date", optional: true, convert: true },
				payment_method_id: { type: "uuid", version: 4, optional: true },
				payment_method_data: { type: "object", optional: true },
				user_id: { type: "number", integer: true, min: 1, optional: true },
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					this.logger.info("");
					this.logger.info(" addCartItem -> isGuest: ", ctx.meta.isGuest);

					throw new MoleculerClientError(
						"Guest operation not allowed",
						403,
						"CC_GUEST_OPERATION_NOT_ALLOWED",
						{},
					);
				}

				let userID = ctx.meta.user ? ctx.meta.user.id : null;
				if (
					ctx.meta.device &&
					(ctx.meta.device.type === "BO" || ctx.meta.device.type === "POS") &&
					ctx.params.user_id
				) {
					userID = ctx.params.user_id;
				}
				delete ctx.params.user_id;

				if (!userID) {
					throw new Errors.ValidationError(
						"An error occurred while finding the user",
						"CART_ITEM_USER_ERROR",
						{},
					);
				}

				// 0 - CHECK IF NOT ALREADY EXPIRED
				if (ctx.params.expires_in && moment(ctx.params.expires_in).isBefore()) {
					throw new Errors.ValidationError(
						"The item is already expired",
						"CART_ITEM_ALREADY_EXPIRED",
						{
							expires_in: ctx.params.expires_in,
						},
					);
				}

				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });

				// 1- CHECK IF USER HAS A CART ON THIS DEVICE AND ACCOUNT
				let carts = await this._find(ctx, {
					limit: 1,
					withRelated: false,
					query: {
						user_id: userID,
						device_id: ctx.meta.device.id,
						account_id: ctx.params.account_id,
					},
				});

				if (
					ctx.params.payment_method_id &&
					(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")
				) {
					// 2 - VALIDATE PAYMENT METHOD
					await ctx.call("payments.payment-methods.get", { id: ctx.params.payment_method_id });
				} else {
					ctx.params.payment_method_id = process.env.DEFAULT_PAYMENT_METHOD_ID;
				}

				if (carts.length < 1) {
					// 3 - IF A CART IS NOT FOUND CREATE A CART FOR THIS USER_ID DEVICE_ID ACCOUNT_ID
					carts = await ctx.call("payments.cart.create", {
						user_id: userID,
						account_id: ctx.params.account_id,
						payment_method_id: ctx.params.payment_method_id,
					});
				}

				if (!ctx.params.max_quantity) {
					ctx.params.max_quantity = 999;
				}

				// 4 - ADD THE ITEM TO THE CART
				ctx.params.cart_id = carts[0].id;
				return ctx
					.call("payments.cart-item.create", ctx.params)
					.then(() => {
						return this.updateCartUpdateAt(ctx, ctx.params.cart_id);
					})
					.then(() => {
						return ctx.call("payments.cart.find", {
							withRelated: "items",
							limit: 1,
							query: {
								user_id: userID,
								device_id: ctx.meta.device.id,
								account_id: ctx.params.account_id,
							},
						});
					});
			},
		},
		removeCartItem: {
			visibility: "published",
			rest: "DELETE /removeItem",
			scope: "payments:cart:create",
			params: {
				item_id: { type: "uuid", version: 4 },
			},
			async handler(ctx) {
				const cartItem = await ctx.call("payments.cart-item.get", { id: ctx.params.item_id });
				const cart = await ctx.call("payments.cart.get", {
					id: cartItem[0].cart_id,
					withRelated: false,
				});

				if (cart[0].user_id != ctx.meta.user.id && cart[0].device_id != ctx.meta.device.id) {
					throw new Errors.ForbiddenError(
						"This item dont belong to yours cart",
						"CART_ITEM_OPERATION_FORBIDDEN",
						{},
					);
				}

				return ctx
					.call("payments.cart-item.remove", { id: ctx.params.item_id })
					.then(() => {
						return this.updateCartUpdateAt(ctx, cart[0].id);
					})
					.then(() => {
						return ctx
							.call("payments.cart-item.count", {
								query: {
									cart_id: cartItem[0].cart_id,
								},
							})
							.then((totalItemsOfCart) => {
								if (totalItemsOfCart === 0) {
									return ctx.call("payments.cart.remove", { id: cartItem[0].cart_id });
								}

								return Promise.resolve();
							});
					})
					.then(() => {
						return ctx.call("payments.cart.find", {
							withRelated: "items",
							limit: 1,
							query: {
								user_id: ctx.meta.user.id,
								device_id: ctx.meta.device.id,
								account_id: cart[0].account_id,
							},
						});
					});
			},
		},
		changeQtdCartItem: {
			visibility: "published",
			rest: "PUT /changeQtdItem",
			scope: "payments:cart:create",
			params: {
				item_id: { type: "uuid", version: 4 },
				quantity: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const cartItem = await ctx.call("payments.cart-item.get", { id: ctx.params.item_id });
				const cart = await ctx.call("payments.cart.get", {
					id: cartItem[0].cart_id,
					withRelated: false,
				});

				if (cartItem[0].max_quantity && ctx.params.quantity > cartItem[0].max_quantity) {
					throw new Errors.ValidationError(
						"The quantity requested exceed the max quantity allowed",
						"CART_ITEM_QUANTITY_EXCEEDED",
						{},
					);
				}

				if (cart[0].user_id != ctx.meta.user.id && cart[0].device_id != ctx.meta.device.id) {
					throw new Errors.ForbiddenError(
						"This item dont belong to yours cart",
						"CART_ITEM_OPERATION_FORBIDDEN",
						{},
					);
				}

				return ctx
					.call("payments.cart-item.patch", {
						id: ctx.params.item_id,
						quantity: ctx.params.quantity,
					})
					.then(() => {
						return this.updateCartUpdateAt(ctx, cart[0].id);
					})
					.then(() => {
						return ctx.call("payments.cart.find", {
							withRelated: "items",
							limit: 1,
							query: {
								user_id: ctx.meta.user.id,
								device_id: ctx.meta.device.id,
								account_id: cart[0].account_id,
							},
						});
					});
			},
		},
		addPaymentMethod: {
			visibility: "published",
			rest: "POST /payment_method",
			scope: "payments:cart:create",
			params: {
				account_id: { type: "number", integer: true, min: 1, convert: true, optional: true },
				payment_method_id: { type: "uuid", version: 4 },
			},
			async handler(ctx) {
				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });
				// 1- VALIDATE PAYMENT METHOD
				await ctx.call("payments.payment-methods.get", { id: ctx.params.payment_method_id });

				const query = {
					user_id: ctx.meta.user.id,
					device_id: ctx.meta.device.id,
				};

				// 2 - FIND CART OF ACCOUNT_ID IF SENDED
				if (ctx.params.account_id) {
					query.account_id = ctx.params.account_id;
				}

				let carts = await this._find(ctx, {
					limit: 1,
					withRelated: false,
					query,
				});

				if (carts.length === 0) {
					throw new MoleculerClientError("No cart found", 404, "CART_NOT_FOUND_ERROR", {});
				}

				return ctx.mcall(
					carts.map((cart) => ({
						action: "payments.cart.patch",
						params: {
							id: cart.id,
							payment_method_id: ctx.params.payment_method_id,
						},
					})),
				);
			},
		},
		checkout: {
			visibility: "published",
			rest: "POST /checkout",
			scope: "payments:cart:create",
			params: {
				user_id: { type: "number", integer: true, min: 1, optional: true },
				account_id: { type: "number", integer: true, min: 1 },
				payment_method_id: { type: "uuid", version: 4, optional: true },
				item_id: { type: "uuid", version: 4, optional: true },

				entity: { type: "string", max: 120, optional: true },
				tin: { type: "string", max: 9, optional: true },
				email: { type: "string", max: 120, optional: true },
				address: { type: "string", max: 250, optional: true },
				postal_code: { type: "string", max: 10, optional: true },
				city: { type: "string", max: 250, optional: true },
				country: { type: "string", max: 250, optional: true },
			},
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("");
					this.logger.info(" ### CHECKOUT ### ");
				}

				if (ctx.meta.isGuest) {
					this.logger.warn("");
					this.logger.warn(" checkout -> isGuest: ", ctx.meta.isGuest);

					throw new MoleculerClientError(
						"Guest operation not allowed",
						403,
						"CC_GUEST_OPERATION_NOT_ALLOWED",
						{},
					);
				}

				let userID = ctx.meta.user ? ctx.meta.user.id : null;
				if (
					ctx.meta.device &&
					(ctx.meta.device.type === "BO" || ctx.meta.device.type === "POS") &&
					ctx.params.user_id
				) {
					userID = ctx.params.user_id;
				}
				delete ctx.params.user_id;

				await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });
				// VALIDATE PAYMENT METHOD
				if (ctx.params.payment_method_id) {
					await ctx.call("payments.payment-methods.get", { id: ctx.params.payment_method_id });
				}

				const query = {
					user_id: userID,
					device_id: ctx.meta.device.id,
				};

				// 1 - FIND CART OF ACCOUNT_ID IF SENDED
				if (ctx.params.account_id) {
					query.account_id = ctx.params.account_id;
				}

				let carts = await this._find(ctx, {
					limit: 1,
					withRelated: "items",
					query,
				});

				// VALIDATIONS
				if (carts.length === 0) {
					throw new MoleculerClientError("No cart found", 404, "CART_NOT_FOUND_ERROR", {});
				}

				for (const cart of carts) {
					if (this.settings.MS_DEBUG) {
						this.logger.info("");
						this.logger.info(" - cart.is_blocked: ", cart.is_blocked);
					}

					if (cart.is_blocked) {
						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.error("### TENTATIVA DE CHECKOUT SEM TERMINAR PAGAMENTO PENDENTE ###");
						}
						throw new Errors.ValidationError("Cart is blocked", "CART_IS_BLOCKED", {
							account_id: cart.account_id,
						});
					} else {
						await this._update(ctx, { id: cart.id, is_blocked: true }, true);
					}

					// IF USER SEND PAYMENT METHOD OVERWRITE
					if (ctx.params.payment_method_id) cart.payment_method_id = ctx.params.payment_method_id;

					// CHECK IF WAS PAYMENT METHOD
					if (!cart.payment_method_id) {
						if (ctx.params.item_id) {
							//Is Checkout-item
							await ctx.call("payments.cart.removeCartItem", { item_id: ctx.params.item_id });
						}

						throw new Errors.ValidationError(
							"No Payment Method defined",
							"CART_NO_PAYMENT_METHOD_DEFINED",
							{
								account_id: cart.account_id,
							},
						);
					}

					// CHECK IF NOT EMPTY
					if (cart.items.length === 0)
						throw new Errors.ValidationError("Empty cart", "EMPTY_CART", {});

					// REMOVE EXPIRED ITEMS
					cart.items = cart.items.filter((ci) => ci.expired === false);
				}

				const paymentMethodProvided = await ctx.call("payments.payment-methods.get", {
					id: carts[0].payment_method_id,
				});

				let itemsForCheckout = carts[0].items.map((item) =>
					_.pick(item, [
						"id",
						"service_id",
						"product_code",
						"name",
						"description",
						"extra_info",
						"quantity",
						"liquid_unit_value",
						"unit_value",
						"discount_value",
						"vat_id",
						"location",
						"article_type",
						"service_confirm_path",
						"service_cancel_path",
					]),
				);

				if (ctx.params.item_id) {
					// ?? Is checkout-item?
					itemsForCheckout = _.filter(itemsForCheckout, ["id", ctx.params.item_id]);
				}

				// SHOULD RETURN THE GENERATED MOVEMENT
				return ctx
					.call("current_account.movements.create", {
						account_id: carts[0].account_id,
						operation: paymentMethodProvided[0].is_immediate ? "INVOICE_RECEIPT" : "INVOICE",
						user_id: carts[0].user_id,
						entity: ctx.params.entity,
						tin: ctx.params.tin,
						email: ctx.params.email,
						address: ctx.params.address,
						postal_code: ctx.params.postal_code,
						city: ctx.params.city,
						country: ctx.params.country,
						description: "Checkout Carrinho",
						payment_method_id: carts[0].payment_method_id,
						payment_method_name: paymentMethodProvided[0].name,
						items: itemsForCheckout.map((item) => _.omit(item, "id")),
					})
					.then(async (movement) => {
						/* //Debug
						return new Promise(resolve => {
							setTimeout(async () => {
								if (ctx.params.item_id) {
									//Is Checkout-item
									await ctx.call("payments.cart.removeCartItem", { item_id: ctx.params.item_id });
									await this._update(ctx, { id: carts[0].id, is_blocked: false }, true);
								} else {
									await ctx.call("payments.cart.clearCart", { account_id: ctx.params.account_id });
								}
								resolve(movement);
							}, 3000)
						})*/

						if (ctx.params.item_id) {
							//Is Checkout-item
							await this._update(ctx, { id: carts[0].id, is_blocked: false }, true);
							await ctx.call("payments.cart.removeCartItem", { item_id: ctx.params.item_id });
						} else {
							await ctx.call("payments.cart.clearCart", { user_id: carts[0].user_id, account_id: ctx.params.account_id });
						}
						return movement;
					})
					.catch(async (err) => {
						// Remove item from cart (processo: "PAGAR JÁ")
						if (ctx.params.item_id) {
							//Is Checkout-item
							await this._update(ctx, { id: carts[0].id, is_blocked: false }, true);
							await ctx.call("payments.cart.removeCartItem", { item_id: ctx.params.item_id });
						}

						for (const cart of carts) {
							await this._update(ctx, { id: cart.id, is_blocked: false }, true);
						}

						throw err;
					});
			},
		},
		removeOldCarts: {
			handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("TICK: Trying to clear old carts!");
				}
				this._find(ctx, {
					withRelated: false,
					query: (qb) => {
						qb.whereRaw(
							`updated_at < NOW() - INTERVAL '${this.settings.clear_time_interval} minutes'`,
						);
					},
				}).then((carts) => {
					if (this.settings.MS_DEBUG) {
						this.logger.info(`TICK: ${carts.length} old carts found!`);
					}
					ctx.mcall(
						carts.map((cart) => ({ action: "payments.cart.remove", params: { id: cart.id } })),
					);
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async updateCartUpdateAt(ctx, cart_id) {
			return ctx.call("payments.cart.patch", { id: cart_id, updated_at: new Date() });
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
