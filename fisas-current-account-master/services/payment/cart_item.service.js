"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");
const moment = require("moment");
const { roundMoney } = require("../utils/operations");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.cart-item",
	table: "cart_item",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("payments", "cart-item")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"cart_id",
			"service_id",
			"product_code",
			"article_type",
			"quantity",
			"max_quantity",
			"unit_value",
			"liquid_unit_value",
			"liquid_value",
			"vat_id",
			"vat",
			"vat_value",
			"total_value",
			"name",
			"description",
			"location",
			"extra_info",
			"expires_in",
			"service_confirm_path",
			"service_cancel_path",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			cart_id: { type: "uuid", version: 4, optional: false },
			service_id: { type: "number", integer: true, min: 1, convert: true },
			product_code: { type: "string", max: 120 },
			article_type: { type: "string", max: 120, optional: true },
			quantity: { type: "number", integer: true, min: 1, convert: true },
			max_quantity: { type: "number", integer: true, min: 1, convert: true, optional: true },
			unit_value: { type: "number", min: 0, convert: true, optional: true },
			liquid_unit_value: { type: "number", min: 0, convert: true, optional: true },
			liquid_value: { type: "number", min: 0, convert: true },
			vat_id: { type: "number", integer: true, min: 1, convert: true },
			vat: { type: "number", convert: true },
			vat_value: { type: "number", min: 0, convert: true },
			total_value: { type: "number", min: 0, convert: true },
			name: { type: "string", max: 250 },
			description: { type: "string", max: 250 },
			location: { type: "string", max: 120 },
			extra_info: { type: "object", optional: true },
			expires_in: { type: "date", optional: true, convert: true },
			service_confirm_path: { type: "string", max: 120, optional: true },
			service_cancel_path: { type: "string", max: 120, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			find: [
				function addSort(ctx) {
					if (!ctx.params.sort) {
						ctx.params.sort = "-name";
					}
				}
			],
			list: [
				function addSort(ctx) {
					if (!ctx.params.sort) {
						ctx.params.sort = "-name";
					}
				},
			],
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"caculateCartItem",
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.isUpdate = true;
				},
				"caculateCartItem",
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.isUpdate = true;
				},
				"caculateCartItem",
			],
		},
		after: {
			get: ["validateExpired"],
			list: ["validateExpired"],
			find: ["validateExpired"],
			create: ["validateExpired"],
			update: ["validateExpired"],
			remove: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		validateExpired(ctx, response) {
			// find
			if (_.isArray(response)) {
				return response.map((cartItem) => this.addExpiredField(cartItem));
			}

			// list
			if (response.rows) {
				response.rows = response.rows.map((cartItem) => this.addExpiredField(cartItem));
				return response;
			}

			if (_.isObject(response)) {
				response = this.addExpiredField(response);
			}

			return response;
		},
		addExpiredField(cartItem) {
			if (cartItem.expires_in) {
				cartItem.expired = moment(cartItem.expires_in).isBefore();
			} else {
				cartItem.expired = false;
			}

			return cartItem;
		},
		async caculateCartItem(ctx, isUpdate = false) {
			if (ctx.params.id && ctx.params.isUpdate) {
				delete ctx.params.isUpdate;
				const savedCartItem = await ctx.call("payments.cart-item.get", { id: ctx.params.id });
				ctx.params.unit_value = savedCartItem[0].unit_value;
				ctx.params.liquid_unit_value = savedCartItem[0].liquid_unit_value;
				ctx.params.vat_id = savedCartItem[0].vat_id;
				ctx.params.liquid_value = savedCartItem[0].liquid_value;
				ctx.params.discount_value = savedCartItem[0].discount_value;
			}

			if (!ctx.params.unit_value && !ctx.params.liquid_unit_value) {
				throw new Errors.ValidationError(
					"You should send unit_value or liquid_unit_value",
					"INVALID_CART_ITEM_UNIT_VALUE",
					{},
				);
			}

			const vat = await ctx.call("configuration.taxes.get", { id: ctx.params.vat_id });

			if (!vat[0].active) {
				throw new Errors.ValidationError("Must provide a valid VAT", "VAT_INACTIVE", {
					vat_id: ctx.params.vat_id,
				});
			}
			const vatMultiplier = vat[0].tax_value;
			ctx.params.vat = vatMultiplier;

			if (ctx.params.liquid_unit_value && !ctx.params.unit_value) {
				ctx.params.unit_value = ctx.params.liquid_unit_value * (1 + vatMultiplier);
			}

			if (ctx.params.unit_value && !ctx.params.liquid_unit_value) {
				ctx.params.liquid_unit_value = ctx.params.unit_value / (1 + vatMultiplier);
			}

			const unitValue = ctx.params.liquid_unit_value;
			const quantity = ctx.params.quantity;
			ctx.params.liquid_value = unitValue * quantity;

			const liquidValue = ctx.params.liquid_value;
			const discountValue = ctx.params.discount_value || 0;
			const liquidDiscountValue = discountValue / (1 + vatMultiplier);
			ctx.params.vat_value = (liquidValue - liquidDiscountValue) * vatMultiplier;

			if (liquidDiscountValue >= liquidValue) {
				throw new Errors.ValidationError("Invalid discount", "INVALID_DISCOUNT_VALUE", {
					liquidValue,
					liquidDiscountValue,
				});
			}

			const vatValue = ctx.params.vat_value;
			ctx.params.total_value = roundMoney(liquidValue - liquidDiscountValue + vatValue);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
