"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { PAYMENT_STATUS } = require("../utils/payment_constants");
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payments",
	table: "payment",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("payments", "payments")],

	/**
	 * Settings
	 */
	settings: {
		MS_DEBUG: process.env.MS_DEBUG == "true",
		fields: [
			"id",
			"account_id",
			"user_id",
			"payment_method_id",
			"payment_method_data",
			"movement_id",
			"value",
			"status",
			"expire_at",
			"paid_at",
			"confirmation_user_id",
			"confirmation_device_id",
			"transaction_receipt",
			"client_receipt",
			"merchant_receipt",
			"confirmed_at",
			"cancelled_at",
			"cash_account_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			payment_method(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "payments.payment-methods", "payment_method", "payment_method_id");
			},
		},
		entityValidator: {
			account_id: { type: "number", integer: true, min: 1 },
			user_id: { type: "number", integer: true, min: 1 },
			payment_method_id: { type: "uuid", version: 4 },
			payment_method_data: { type: "object", optional: true },
			movement_id: { type: "uuid", version: 4 },
			value: { type: "number" },
			status: { type: "enum", values: PAYMENT_STATUS },
			confirmation_user_id: { type: "number", integer: true, min: 1, optional: true },
			confirmation_device_id: { type: "number", integer: true, min: 1, optional: true },
			transaction_receipt: { type: "string", optional: true },
			client_receipt: { type: "string", optional: true },
			merchant_receipt: { type: "string", optional: true },
			expire_at: { type: "date", optional: true },
			paid_at: { type: "date", optional: true },
			confirmed_at: { type: "date", optional: true },
			cancelled_at: { type: "date", optional: true },
			cash_account_id: { type: "number", integer: true, min: 1, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			list: [
				function sanatizeQuery(ctx) {
					if (!(ctx.meta.isBackoffice || ctx.meta.device.type === "POS")) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						ctx.params.query.user_id = ctx.meta.user.id;
					}
				},
			],
			get: [
				function sanatizeQuery(ctx) {
					if (!(ctx.meta.isBackoffice || (ctx.meta.device && ctx.meta.device.type === "POS"))) {
						ctx.params.query = ctx.params.query ? ctx.params.query : {};
						if (ctx.meta.user) ctx.params.query.user_id = ctx.meta.user.id;
					}
				},
			],
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.status = _.has(ctx, "params.status") ? ctx.params.status : "PENDING";

					//SET BY DATABASE (DEFAULT CURRENT_TIMESTAMP)
					//ctx.params.created_at = new Date();
					//ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
					"#device.id",
					"#isBackoffice",
				],
			},
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			cache: false,
			visibility: "published",
		},
		check_method_available: {
			params: {
				payment_method_id: { type: "uuid", version: 4 },
			},
			async handler(ctx) {
				const payment_method = await ctx.call("payments.payment-methods.get", {
					id: ctx.params.payment_method_id,
				});

				return ctx.call(`${payment_method[0].path}.isAvailable`).then((result) => {
					if (!result) {
						throw new Errors.ValidationError(
							"This Payment Method is Not Available",
							"UNAVAILABLE_PAYMENT_METHOD",
							{},
						);
					}
				});
			},
		},
		create: {
			visibility: "public",
			//visibility: "published",
			//rest: "POST /",
			params: {
				payment_method_id: { type: "uuid", version: 4 },
				account_id: { type: "number", integer: true, min: 1 },
				user_id: { type: "number", integer: true, min: 1 },
				value: { type: "number" },
				movement_id: { type: "uuid", version: 4 },
				status: { type: "enum", values: PAYMENT_STATUS },

				payment_method_data: { type: "object", optional: true },
				confirmation_user_id: { type: "number", integer: true, min: 1, optional: true },
				confirmation_device_id: { type: "number", integer: true, min: 1, optional: true },
				transaction_receipt: { type: "string", optional: true },
				expire_at: { type: "date", optional: true },
				paid_at: { type: "date", optional: true },
				confirmed_at: { type: "date", optional: true },
				cancelled_at: { type: "date", optional: true },
				created_at: { type: "date", optional: true },
				updated_at: { type: "date", optional: true },

				description: { type: "string", optional: true },

				cash_account_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
			},
			async handler(ctx) {
				const payment_method = await ctx.call("payments.payment-methods.get", {
					id: ctx.params.payment_method_id,
				});

				if (!payment_method[0].active) {
					throw new Errors.ValidationError(
						"Payment Method no Activo",
						"INATIVE_PAYMENT_METHOD",
						{},
					);
				}

				let relatedMovement = await ctx.call("current_account.movements.get", {
					id: ctx.params.movement_id,
					withRelated: false,
				});

				//CHECK IF THE METHOD IS AVAILABLE
				return ctx
					.call(`${payment_method[0].path}.isAvailable`)
					.then((result) => {
						if (!result) {
							throw new Errors.ValidationError(
								"This Payment Method is Not Available",
								"UNAVAILABLE_PAYMENT_METHOD",
								{},
							);
						}
					})
					.then(() => {
						// CHECK IF THE USER CAN PAY // IN CASE OF CC CHECK THE BALANCE

						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info("### ignoreCheckBalance? ###");
							this.logger.info("  - relatedMovement.operation: ", relatedMovement[0].operation);
						}

						let ignoreCheckBalance =
							_.includes(["CREDIT_NOTE"], relatedMovement[0].operation) ||
							ctx.meta.bypass_balance_with_invoice;

						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info("  - ignoreCheckBalance: ", ignoreCheckBalance);
						}

						if (ignoreCheckBalance) {
							let params = ctx.params;
							return this._create(ctx, params);
						} else {
							return ctx
								.call(`${payment_method[0].path}.canPay`, {
									user_id: ctx.params.user_id,
									account_id: ctx.params.account_id,
									value: ctx.params.value,
									payment_id: ctx.params.id,
									description: ctx.params.description ? ctx.params.description : "",
								})
								.then((resultCanPay) => {
									if (!resultCanPay.canPay) {
										if (resultCanPay.error) {
											throw resultCanPay.error;
										}

										throw new Errors.ValidationError(
											"At the moment is not possible to make the payment",
											"UNABLE_OF_PAY_PAYMENT_METHOD",
											{},
										);
									}

									let params = ctx.params;
									if (resultCanPay.payment_method_data) {
										params.payment_method_data = resultCanPay.payment_method_data;

										// Set Payment expiration date if returned by payment_method
										if (resultCanPay.payment_method_data["ExpirationDate"]) {
											params.expiration_at = resultCanPay.payment_method_data["ExpirationDate"]; //.toISOString();
										}
									}
									return this._create(ctx, params);
								});
						}
					});
			},
		},
		confirm: {
			visibility: "public",
			//visibility: "published",
			//rest: "POST /confirm",
			params: {
				id: { type: "uuid" }, //payment_id
				transaction_receipt: { type: "string", optional: true },
				client_receipt: { type: "string", optional: true },
				merchant_receipt: { type: "string", optional: true },
				CPUID: { type: "string", optional: true, max: 64 },
			},
			async handler(ctx) {
				if (this.settings.MS_DEBUG) {
					this.logger.info("");
					this.logger.info("### PAYMENT CONFIRM ###");
					this.logger.info(" 1. payments.payment.confirm");
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}

				const payment = await ctx.call("payments.payments.get", {
					id: ctx.params.id,
					withRelated: "payment_method",
				});
				if (payment[0].status !== "PENDING") {
					throw new Errors.ValidationError(
						"Payment must be on a pending status",
						"ERROR_PAYING_PAYMENT_NOT_PENDING",
						{},
					);
				}
				return ctx
					.call(`${payment[0].payment_method.path}.confirm`, {
						payment_id: ctx.params.id,
						CPUID: ctx.params.CPUID, // NEEDED FOR TPA'S
					})
					.then((resultConfirm) => {
						if (this.settings.MS_DEBUG) {
							this.logger.info("");
							this.logger.info(` 2. ${payment[0].payment_method.path}.confirm`);
							this.logger.info("  - resultConfirm:");
							this.logger.info(resultConfirm);
						}

						if (resultConfirm.isConfirmed) {
							const transactionReceiptData = resultConfirm.transactionReceiptData
								? resultConfirm.transactionReceiptData
								: ctx.params.transaction_receipt || null;

							return ctx
								.call("payments.payments.patch", {
									id: ctx.params.id,
									status: "CONFIRMED",
									confirmed_at: new Date(),
									transaction_receipt: transactionReceiptData || null,
									client_receipt: ctx.params.client_receipt,
									merchant_receipt: ctx.params.merchant_receipt,
									confirmation_user_id: ctx.meta.user ? ctx.meta.user.id : null,
									confirmation_device_id: ctx.meta.device ? ctx.meta.device.id : null,
								})
								.then((resultPaymentPatch) => {
									if (this.settings.MS_DEBUG) {
										this.logger.info("");
										this.logger.info(" 3. payments.payments.patch");
										this.logger.info("  - params:");
										this.logger.info("     id: ", ctx.params.id);
										this.logger.info("     status: CONFIRMED");
										this.logger.info("     confirmed_at: ", new Date());
										this.logger.info("     transaction_receipt: ", transactionReceiptData);
										this.logger.info("     client_receipt: ", ctx.params.client_receipt);
										this.logger.info("     merchant_receipt: ", ctx.params.merchant_receipt);
										this.logger.info(
											"     confirmation_user_id: ",
											ctx.meta.user ? ctx.meta.user.id : null,
										);
										this.logger.info(
											"     confirmation_device_id: ",
											ctx.meta.device ? ctx.meta.device.id : null,
										);
										this.logger.info("");
										this.logger.info("  - resultPaymentPatch:");
										this.logger.info(resultPaymentPatch);
									}

									// ANNOUNCE THE CONFIRM PAYMENT TO THE CURRENT_ACCOUNT

									return ctx
										.call(
											"current_account.movements.paymentConfirmed",
											{
												payment: resultPaymentPatch[0],
											},
											{
												meta: {
													is_partial_pay: ctx.meta.is_partial_pay ? ctx.meta.is_partial_pay : false,
												},
											},
										)
										.then((resultPaymentConfirmed) => {
											if (this.settings.MS_DEBUG) {
												this.logger.info("");
												this.logger.info(" 4. current_account.movements.paymentConfirmed");
												this.logger.info("  - params:");
												this.logger.info("     payment: ", resultPaymentPatch[0]);
												this.logger.info("");
												this.logger.info("  - resultPaymentConfirmed:");
												this.logger.info(resultPaymentConfirmed);
												this.logger.info("");
												this.logger.info("### /PAYMENT CONFIRM ###");
											}

											return resultPaymentConfirmed;
										})
										.catch((err) => {
											this.logger.error("");
											this.logger.error(" Não foi possível confirmar o payment:");
											this.logger.error("  err:");
											this.logger.error(err);

											if (this.settings.MS_DEBUG) {
												this.logger.error("");
												this.logger.error(
													" 5. (após falha na confirmação) payments.payments.patch",
												);
												this.logger.error("  - params:");
												this.logger.error("     id: ", ctx.params.id);
												this.logger.error("     status: PENDING");
											}
											// TODO WARNING validate the process in case of confirmation failed
											return ctx
												.call("payments.payments.patch", {
													id: ctx.params.id,
													status: "PENDING",
												})
												.then((resultPaymentPatchBeforeError) => {
													if (this.settings.MS_DEBUG) {
														this.logger.error("");
														this.logger.error(" 5.1 resultPaymentPatchBeforeError:");
														this.logger.error(resultPaymentPatchBeforeError);
														this.logger.info("");
														this.logger.info("### /PAYMENT CONFIRM ###");
													}

													throw new Errors.ValidationError(
														"An error occurred on confirmation movement.",
														"CC_ERROR_ON_CONFIRMATION_MOVEMENT",
														{},
													);
												});
										});
								});
						} else {
							if (resultConfirm.isCancelled) {
								if (this.settings.MS_DEBUG) {
									this.logger.info("");
									this.logger.info(" 2.1 resultConfirm.isCancelled");
									this.logger.info("  payment will be cancelled");
									this.logger.info("  - params:");
									this.logger.info("     id: ", ctx.params.id);
								}

								return ctx
									.call("payments.payments.cancel", {
										id: ctx.params.id,
									})
									.then((resultPaymentCancel) => {
										if (this.settings.MS_DEBUG) {
											this.logger.error("");
											this.logger.error(" 2.2 resultPaymentCancel:");
											this.logger.error(resultPaymentCancel);
											this.logger.info("");
											this.logger.info("### /PAYMENT CONFIRM ###");
										}

										return resultPaymentCancel;
									});
							}
						}

						throw new Errors.ValidationError(
							"At the moment is not possible to confirm this payment.",
							"UNABLE_TO_CONFIRM_PAYMENT",
							{},
						);
					});
			},
		},
		cancel: {
			visibility: "public",
			//visibility: "published",
			//rest: "POST /cancel",
			params: {
				id: { type: "uuid" },
			},
			async handler(ctx) {
				const payment = await ctx.call("payments.payments.get", {
					id: ctx.params.id,
					withRelated: "payment_method",
				});
				if (payment[0].status !== "PENDING") {
					throw new Errors.ValidationError(
						"Payment must be on a pending status",
						"ERROR_PAYING_PAYMENT_NOT_PENDING",
						{},
					);
				}

				return ctx.call(`${payment[0].payment_method.path}.cancel`).then((cancelled) => {
					if (cancelled) {
						return ctx
							.call("payments.payments.patch", {
								id: ctx.params.id,
								status: "CANCELLED",
								cancelled_at: new Date(),
							})
							.then(async (payment) => {
								//if (payment[0].payment_method_id == "ebaffe5f-db1d-48b6-944d-fb33f4596bb1") {
								//RefMB_AMA
								await ctx.call("current_account.movements.patch", {
									id: payment[0].movement_id,
									status: payment[0].status,
								});
								//}
								// CHECK IF IS NOT BETTER TO CALL A ACTION FROM MOVEMENT
								//ctx.emit("payments.payments.cancelled", payment, ["current_account.movements"]);
								return payment;
							});
					}

					throw new Errors.ValidationError(
						"At the moment is not possible to cancel this payment.3",
						"UNABLE_TO_CANCEL_PAYMENT",
						{},
					);
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
