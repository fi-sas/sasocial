"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.payment-methods",
	table: "payment_method",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("payments", "payment-methods")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"tag",
			"description",
			"path",
			"is_immediate",
			"active",
			"charge",
			"gateway_data",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string", max: 45, optional: false },
			tag: { type: "string", max: 45, optional: false },
			description: { type: "string", max: 250, optional: true },
			path: { type: "string", max: 120, optional: false },
			is_immediate: { type: "boolean", optional: false },
			active: { type: "boolean", optional: false },
			charge: { type: "boolean", optional: false },
			gateway_data: { type: "string", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
