"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payments.tpa_period",
	table: "tpa_period",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("current_account", "tpa_periods")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "device_id", "receipt", "created_at", "updated_at"],

		defaultWithRelateds: [],

		withRelateds: {},

		entityValidator: {
			device_id: "number|integer|convert",
			receipt: "string",
			created_at: "date|optional|convert",
			updated_at: "date|optional|convert",
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.device_id = ctx.meta.device.id;

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST
			authorization: false,
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		update: {
			// REST: PATCH /:id
			visibility: "public",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
