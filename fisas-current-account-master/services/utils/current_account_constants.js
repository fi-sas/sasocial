/* eslint-disable no-unused-vars */
module.exports = {
	ACCOUNT_PLAFOND_TYPE: ["PLAFOND", "ADVANCE"],

	DOCUMENT_TYPE: [
		"INVOICE", // Factura (C, affects, -)
		"INVOICE_RECEIPT", // Factura/Recibo (I, affects?, -)
		"RECEIPT", // Recibo (C, affects?, +)
		"CREDIT_NOTE", // Nota de Crédito (I,affects, +)
		"DEPOSIT", // Caução (I, affetcs, -)
		"CANCEL_DEPOSIT", // Anulação da caução (I, affects, +)
		"REFUND", // Devolução (I, affects, -)
		"LEND", // Adiantamento (I, affects, +)
		"CANCEL_LEND", // Anulação de Adiantamento? (I, affects, -)
	],

	/*
	MOVEMENT_OPERATION: [
		"CHARGE", // charge operation (-)
		"TOP_UP", // add credit operation (+)
		"REFUND", // undo charge operation (+)
		"LEND", // lend money operation (+)
		"REPAY", // undo lend operation (-)
	],*/
	MOVEMENT_OPERATION: [
		"CHARGE", // charge operation (+)
		"CANCEL", // undo charge operation (+)
		"RECEIPT", // add credit operation (+)
		"INVOICE", // add debit operation (-)
		"CREDIT_NOTE", // add credit operation (+)
		"LEND", // add credit operation (+)
		"REFUND", // undo lend/charge operation (-) Reembolso/devolução
		"INVOICE_RECEIPT", // add debit operation (-)
		"CHARGE_NOT_IMMEDIATE", // add credit operation (+) when confirmed
	],

	MOVEMENT_STATUS: [
		"PAID", // movement paid
		"PARTIALLY_PAID", // movement partially paid
		"PENDING", // moviment pending confirmation
		"CONFIRMED", // moviment confirmed
		"CANCELLED", // moviment cancelled/declined
		"CANCELLED_LACK_PAYMENT", // moviment cancelled by system due lack payment
	],

	MIDDLEWARE_ERP_STATUS: [
		"SKIPPED", // will not sent
		"PENDING", // pending processing
		"IN_PROGRESS", // sent to middleware and waiting process response from middleware
		"SUCCESS", // created/processed
		"FAILED",  // processing fail
	],

	ALLOWED_CURRENCIES: ["EUR"],
};
