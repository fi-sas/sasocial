/* eslint-disable no-unused-vars */
const roundTo = require("round-to");

module.exports = {
	roundMoney: (value) => {
		value = parseFloat(value);
		return roundTo(value, 2);
	}
};
