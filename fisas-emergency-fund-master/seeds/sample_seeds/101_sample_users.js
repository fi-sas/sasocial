const crypto = require("crypto");
const UUIDV4 = require("uuid").v4;

exports.seed = (knex) =>
	knex("user")
		.select()
		.where("user_name", "aluno")
		.then(async (rows) => {
			if (rows.length === 0) {
				//Profile "ALUNOS" /Students
				const prof = await knex("profile").select().where("name", "Alunos");
				//Create "Aluno" /student user
				const salt = UUIDV4();
				await knex("user").insert({
					name: "ALUNO",
					email: "aluno@ipvc.pt",
					phone: "",
					user_name: "aluno",
					password: crypto.createHash("sha256").update(salt.concat("admin")).digest("hex"),
					gender: "F",
					salt,
					pin: crypto.createHash("sha256").update(salt.concat("1234")).digest("hex"),
					external: 0,
					active: true,
					profile_id: prof[0].id, //"Alunos" profile
					can_access_BO: 0,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
			return true;
		})
		.catch((err) => {
			console.log(err);
		});
