# [1.7.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.6.0...v1.7.0) (2022-02-03)


### Features

* **api:** add timeout of 100 Sec to api call's ([135c010](https://gitlab.com/fi-sas/fisas-gateway/commit/135c010e43501166b81117efa101fb34a85939c9))

# [1.6.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.5.1...v1.6.0) (2022-01-04)


### Features

* disable file public security ([54db51e](https://gitlab.com/fi-sas/fisas-gateway/commit/54db51e09727c9dcdc4931eaeba16fbfaadc60f0))

## [1.5.1](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.5.0...v1.5.1) (2022-01-04)


### Bug Fixes

* **uploads:** change uploads error validation ([cb292d8](https://gitlab.com/fi-sas/fisas-gateway/commit/cb292d8a63c709750023d0c874ed218afa5640cf))

# [1.5.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.4.1...v1.5.0) (2022-01-04)


### Features

* **uploads:** save filename on upload ([01038aa](https://gitlab.com/fi-sas/fisas-gateway/commit/01038aa8703c1e280896d446605031f258ed3c07))

## [1.4.1](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.4.0...v1.4.1) (2021-12-16)


### Bug Fixes

* **socketio:** return error of validateToken ([06b3ac0](https://gitlab.com/fi-sas/fisas-gateway/commit/06b3ac0d94ed5852aadf23d19a90de7a489d868e))

# [1.4.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.3.0...v1.4.0) (2021-10-29)


### Features

* **api:** add supports of external headers from config ([da9318d](https://gitlab.com/fi-sas/fisas-gateway/commit/da9318d69251b9a6b8d061785abefcea3787ed75))

# [1.3.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.8...v1.3.0) (2021-10-08)


### Features

* **reponse:** add possibility to return data wihtou assigned ([067be88](https://gitlab.com/fi-sas/fisas-gateway/commit/067be881c13ec821578d892d1708d86599595fba))

## [1.2.8](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.7...v1.2.8) (2021-07-28)


### Bug Fixes

* **authorize:** add meta showCPUID to validate token ([9fc87b3](https://gitlab.com/fi-sas/fisas-gateway/commit/9fc87b3b8525f8432238730c3f837586ece544f8))

## [1.2.7](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.6...v1.2.7) (2021-07-13)


### Bug Fixes

* **uploads:** add disk spcace control to upload endpoint ([ffecc30](https://gitlab.com/fi-sas/fisas-gateway/commit/ffecc306183cf416ce03dfbdac859e7a58722dca))

## [1.2.6](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.5...v1.2.6) (2021-07-08)


### Bug Fixes

* add 408 error code ([b84d189](https://gitlab.com/fi-sas/fisas-gateway/commit/b84d1897e642e1e4d6bd814c8828f8ae410a34a5))

## [1.2.5](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.4...v1.2.5) (2021-06-25)


### Bug Fixes

* **api:** add 413 status code ([3611c1d](https://gitlab.com/fi-sas/fisas-gateway/commit/3611c1d7617facd291b6a3a0624a1eb7673c3b5c))

## [1.2.4](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.3...v1.2.4) (2021-06-21)


### Bug Fixes

* **response:** sql error lenght verify ([4f961f1](https://gitlab.com/fi-sas/fisas-gateway/commit/4f961f1d83899863de2e66ec53df216a25c5136f))

## [1.2.3](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.2...v1.2.3) (2021-06-14)


### Bug Fixes

* **mixins:** upload creation remove unused fields ([2956fed](https://gitlab.com/fi-sas/fisas-gateway/commit/2956fed1b53a9dfd1c85924739322ec04aa0338c))

## [1.2.2](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.1...v1.2.2) (2021-06-14)


### Bug Fixes

* **types:** add option to upload csv file ([04818e7](https://gitlab.com/fi-sas/fisas-gateway/commit/04818e7d76a43f04baa3e9f07d9893c3e9f9823d))

## [1.2.1](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.2.0...v1.2.1) (2021-06-09)


### Bug Fixes

* **api:** remove console.logs and change SQL errors type ([b0b14d8](https://gitlab.com/fi-sas/fisas-gateway/commit/b0b14d834b70e50668a8111bcab924320e3bb0e7))

# [1.2.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.1.0...v1.2.0) (2021-04-14)


### Features

* **api:** add rate limitir config ([ca98d72](https://gitlab.com/fi-sas/fisas-gateway/commit/ca98d7208293d99af1c29e9bf7b12fb486843c4f))

# [1.1.0](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.1...v1.1.0) (2021-03-15)


### Bug Fixes

* remove socket io authorization ([5766158](https://gitlab.com/fi-sas/fisas-gateway/commit/5766158f87cff1c1fc618f2fcd80b00315ba59c4))


### Features

* **socket.io:** add socketio mixin ([e6fd99a](https://gitlab.com/fi-sas/fisas-gateway/commit/e6fd99ac788452ff21112c2266d41d84d6d3b259))
* **sokcetio:** initial version of socketio mixin ([733a757](https://gitlab.com/fi-sas/fisas-gateway/commit/733a757feb1f1d4f97064f4acc0689a4b01854c7))

## [1.0.1](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0...v1.0.1) (2021-03-03)


### Bug Fixes

* **api:** dont format responses different of json ([e373300](https://gitlab.com/fi-sas/fisas-gateway/commit/e3733000e346be535d9b58f27b5add342abd8c69))

# 1.0.0 (2021-02-10)


### Bug Fixes

* **semantic-release:** changed strategy ([48b37ad](https://gitlab.com/fi-sas/fisas-gateway/commit/48b37ad2711e44d13986844c1ea538264f9089b2))
* add isGuest metadata ([30af9a3](https://gitlab.com/fi-sas/fisas-gateway/commit/30af9a306e1fcd1c5a74cf23eeed266d0b90875f))
* lanauge acronym toLowerCase always ([d9ccf33](https://gitlab.com/fi-sas/fisas-gateway/commit/d9ccf339d0eee8608a91cf9b866f0172dcb8c03e))
* **api:** add list of possible data to be returned on links ([c24bb11](https://gitlab.com/fi-sas/fisas-gateway/commit/c24bb112c1ed3c7a9dbf9436cb7fa4b6df6c4c23))
* **api:** add meta isBackoffice ([22d5bd8](https://gitlab.com/fi-sas/fisas-gateway/commit/22d5bd8659c8a3a27ed5197f5e487bec108304a5))
* **api:** change error messages for 401 and 403 ([0312c39](https://gitlab.com/fi-sas/fisas-gateway/commit/0312c393ed3a87826191b30dc9827c9fff7c1b28))
* **api:** filter sort field not found error ([8eca7bc](https://gitlab.com/fi-sas/fisas-gateway/commit/8eca7bca1f996f105f2066d9a96139537f9ecf57))
* **api:** fix error 500 on null response from services ([b7aa50f](https://gitlab.com/fi-sas/fisas-gateway/commit/b7aa50f7202517bd668d9698426a607f68d3b855))
* **api:** fix on after call on upload ([ea8b28a](https://gitlab.com/fi-sas/fisas-gateway/commit/ea8b28aa887629c84f90223ed07b37268a9e47d6))
* **api:** fix onError header already sent ([cbd44b5](https://gitlab.com/fi-sas/fisas-gateway/commit/cbd44b598d669ebbf1422eba8fdf78d8bdfdaab3))
* **api:** fix the response of .list actions ([4f5a87b](https://gitlab.com/fi-sas/fisas-gateway/commit/4f5a87b1248e1869b571f83383fc21fcd011eb7a))
* **api:** return 204 code when response is null ([da0de4d](https://gitlab.com/fi-sas/fisas-gateway/commit/da0de4d960f411fbf00c22344d0d34e44d4f1270))
* **api:** return errors always in array ([190a328](https://gitlab.com/fi-sas/fisas-gateway/commit/190a328982435a2a67a7ac82ae133072a5413ffe))
* **api:** validate the onAfterCall method ([e7c727e](https://gitlab.com/fi-sas/fisas-gateway/commit/e7c727eb401857203d49968dae80f27cc8a058f3))
* **ci:** change the treafik and moleculer paths ([96670b7](https://gitlab.com/fi-sas/fisas-gateway/commit/96670b7230ecf7bb755ebcb82aa067d66806e91e))
* **gateway:** Add more essential features ([397d90f](https://gitlab.com/fi-sas/fisas-gateway/commit/397d90fd91febbb620bfe796c6634b29c9fe9cc9))
* **indent:** fixed indentation ([21d6017](https://gitlab.com/fi-sas/fisas-gateway/commit/21d6017b7624d65f95ffcd3c87cf77051a5ed80c))
* **upload:** add sizeOf image ([82f52e0](https://gitlab.com/fi-sas/fisas-gateway/commit/82f52e0ef59b54b25cbb3fddfe0c44a22733d317))
* **uplodas:** add 1y of  cache to medias files ([e3484fa](https://gitlab.com/fi-sas/fisas-gateway/commit/e3484fa8f9f333726ddc418ed4a942c2cf6b0347))
* disable env gateway authentication and authorization behaviour ([09ff3eb](https://gitlab.com/fi-sas/fisas-gateway/commit/09ff3eb253da49ffe8017d4999bc7dd51e4e7a93))
* fixing node version (node:12-buster-slim) ([84ccb8a](https://gitlab.com/fi-sas/fisas-gateway/commit/84ccb8a954430bfcf429cf2922a6a6079c02e855))
* missing lodash ([6e67454](https://gitlab.com/fi-sas/fisas-gateway/commit/6e67454f286a009cebcc9b990569f3b3b29a2eef))
* wrong authorized validation token action name ([5a421e8](https://gitlab.com/fi-sas/fisas-gateway/commit/5a421e8523ffc1a8188ba9f437b66576e44fc23f))


### Features

* add suport for cookies ([64c6cc9](https://gitlab.com/fi-sas/fisas-gateway/commit/64c6cc95bddd8292fe91be36a968e965d846d593))
* validate and fill language to metadata ([26ccc3c](https://gitlab.com/fi-sas/fisas-gateway/commit/26ccc3cf1d8660ac883bc8652f399ae0e1b11800))
* **api:** add media uploads ([a0ff670](https://gitlab.com/fi-sas/fisas-gateway/commit/a0ff670329a750b6a6eb31a61a4a8e3abe4f554e))
* **api:** add uploads virus verification ([4cb4936](https://gitlab.com/fi-sas/fisas-gateway/commit/4cb4936edf30fd7559a5ae305354e3c34cd3f4d7))
* **errors:** inital code for error system ([c8fd440](https://gitlab.com/fi-sas/fisas-gateway/commit/c8fd4404fbdcbf35bde10aed5a5fc277d9fe84c4))
* **gateway:** add webpage and backoffice server ([2db0e33](https://gitlab.com/fi-sas/fisas-gateway/commit/2db0e3321f1f71bc2479d6893d2e7e4a689aeaf3))
* **logs:** add stack for logs ([27ec7d1](https://gitlab.com/fi-sas/fisas-gateway/commit/27ec7d125904482f05c5380591e9303f73c4cee8))
* **media:** add media volumes ([9abe9cd](https://gitlab.com/fi-sas/fisas-gateway/commit/9abe9cd1895c6ebd27a27649d8366a376db431ea))
* **media:** add static media serve and auth ([2de2c27](https://gitlab.com/fi-sas/fisas-gateway/commit/2de2c27e58014b1719e0c3ab6b5d514a9deebef5))
* **medias:** add media uploads alias ([896c473](https://gitlab.com/fi-sas/fisas-gateway/commit/896c473f567aab759d89e734298dd0cb7fa902d2))
* **upload:** add the upload action and limit ([e2f8d09](https://gitlab.com/fi-sas/fisas-gateway/commit/e2f8d0992cbeb664e04b898531e448938b1d93b7))
* added scope validation ([f3e7cde](https://gitlab.com/fi-sas/fisas-gateway/commit/f3e7cde3a16207ce860a6c9cafff5456a0e0d304))
* adding swagger into gateway ([c3eb0b2](https://gitlab.com/fi-sas/fisas-gateway/commit/c3eb0b21ff6fcca93be9e02a73c41603b75e7961))
* api.service implemented ([1e4a9e8](https://gitlab.com/fi-sas/fisas-gateway/commit/1e4a9e80bb2649acb78192eaa01d7607418d460f))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-02-05)


### Features

* add suport for cookies ([64c6cc9](https://gitlab.com/fi-sas/fisas-gateway/commit/64c6cc95bddd8292fe91be36a968e965d846d593))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-01-06)


### Bug Fixes

* lanauge acronym toLowerCase always ([d9ccf33](https://gitlab.com/fi-sas/fisas-gateway/commit/d9ccf339d0eee8608a91cf9b866f0172dcb8c03e))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-01-06)


### Features

* validate and fill language to metadata ([26ccc3c](https://gitlab.com/fi-sas/fisas-gateway/commit/26ccc3cf1d8660ac883bc8652f399ae0e1b11800))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-01-05)


### Bug Fixes

* add isGuest metadata ([30af9a3](https://gitlab.com/fi-sas/fisas-gateway/commit/30af9a306e1fcd1c5a74cf23eeed266d0b90875f))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2020-12-28)


### Bug Fixes

* **api:** validate the onAfterCall method ([e7c727e](https://gitlab.com/fi-sas/fisas-gateway/commit/e7c727eb401857203d49968dae80f27cc8a058f3))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2020-12-21)


### Bug Fixes

* **api:** fix on after call on upload ([ea8b28a](https://gitlab.com/fi-sas/fisas-gateway/commit/ea8b28aa887629c84f90223ed07b37268a9e47d6))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2020-12-21)


### Bug Fixes

* **upload:** add sizeOf image ([82f52e0](https://gitlab.com/fi-sas/fisas-gateway/commit/82f52e0ef59b54b25cbb3fddfe0c44a22733d317))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2020-12-21)


### Features

* **api:** add uploads virus verification ([4cb4936](https://gitlab.com/fi-sas/fisas-gateway/commit/4cb4936edf30fd7559a5ae305354e3c34cd3f4d7))
* **upload:** add the upload action and limit ([e2f8d09](https://gitlab.com/fi-sas/fisas-gateway/commit/e2f8d0992cbeb664e04b898531e448938b1d93b7))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2020-12-14)


### Bug Fixes

* **api:** filter sort field not found error ([8eca7bc](https://gitlab.com/fi-sas/fisas-gateway/commit/8eca7bca1f996f105f2066d9a96139537f9ecf57))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2020-12-14)


### Bug Fixes

* **api:** return errors always in array ([190a328](https://gitlab.com/fi-sas/fisas-gateway/commit/190a328982435a2a67a7ac82ae133072a5413ffe))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2020-12-11)


### Bug Fixes

* **ci:** change the treafik and moleculer paths ([96670b7](https://gitlab.com/fi-sas/fisas-gateway/commit/96670b7230ecf7bb755ebcb82aa067d66806e91e))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2020-12-03)

### Bug Fixes

- **api:** change error messages for 401 and 403 ([0312c39](https://gitlab.com/fi-sas/fisas-gateway/commit/0312c393ed3a87826191b30dc9827c9fff7c1b28))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-11-25)

### Bug Fixes

- **api:** return 204 code when response is null ([da0de4d](https://gitlab.com/fi-sas/fisas-gateway/commit/da0de4d960f411fbf00c22344d0d34e44d4f1270))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-11-20)

### Bug Fixes

- **api:** add meta isBackoffice ([22d5bd8](https://gitlab.com/fi-sas/fisas-gateway/commit/22d5bd8659c8a3a27ed5197f5e487bec108304a5))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-11-19)

### Bug Fixes

- **api:** fix error 500 on null response from services ([b7aa50f](https://gitlab.com/fi-sas/fisas-gateway/commit/b7aa50f7202517bd668d9698426a607f68d3b855))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-11-18)

### Bug Fixes

- **api:** fix the response of .list actions ([4f5a87b](https://gitlab.com/fi-sas/fisas-gateway/commit/4f5a87b1248e1869b571f83383fc21fcd011eb7a))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-gateway/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-11-12)

### Bug Fixes

- fixing node version (node:12-buster-slim) ([84ccb8a](https://gitlab.com/fi-sas/fisas-gateway/commit/84ccb8a954430bfcf429cf2922a6a6079c02e855))

# 1.0.0-rc.1 (2020-11-12)

### Bug Fixes

- **api:** add list of possible data to be returned on links ([c24bb11](https://gitlab.com/fi-sas/fisas-gateway/commit/c24bb112c1ed3c7a9dbf9436cb7fa4b6df6c4c23))
- **indent:** fixed indentation ([21d6017](https://gitlab.com/fi-sas/fisas-gateway/commit/21d6017b7624d65f95ffcd3c87cf77051a5ed80c))
- **uplodas:** add 1y of cache to medias files ([e3484fa](https://gitlab.com/fi-sas/fisas-gateway/commit/e3484fa8f9f333726ddc418ed4a942c2cf6b0347))
- disable env gateway authentication and authorization behaviour ([09ff3eb](https://gitlab.com/fi-sas/fisas-gateway/commit/09ff3eb253da49ffe8017d4999bc7dd51e4e7a93))
- missing lodash ([6e67454](https://gitlab.com/fi-sas/fisas-gateway/commit/6e67454f286a009cebcc9b990569f3b3b29a2eef))
- wrong authorized validation token action name ([5a421e8](https://gitlab.com/fi-sas/fisas-gateway/commit/5a421e8523ffc1a8188ba9f437b66576e44fc23f))
- **api:** fix onError header already sent ([cbd44b5](https://gitlab.com/fi-sas/fisas-gateway/commit/cbd44b598d669ebbf1422eba8fdf78d8bdfdaab3))
- **gateway:** Add more essential features ([397d90f](https://gitlab.com/fi-sas/fisas-gateway/commit/397d90fd91febbb620bfe796c6634b29c9fe9cc9))

### Features

- **api:** add media uploads ([a0ff670](https://gitlab.com/fi-sas/fisas-gateway/commit/a0ff670329a750b6a6eb31a61a4a8e3abe4f554e))
- **errors:** inital code for error system ([c8fd440](https://gitlab.com/fi-sas/fisas-gateway/commit/c8fd4404fbdcbf35bde10aed5a5fc277d9fe84c4))
- **gateway:** add webpage and backoffice server ([2db0e33](https://gitlab.com/fi-sas/fisas-gateway/commit/2db0e3321f1f71bc2479d6893d2e7e4a689aeaf3))
- **logs:** add stack for logs ([27ec7d1](https://gitlab.com/fi-sas/fisas-gateway/commit/27ec7d125904482f05c5380591e9303f73c4cee8))
- **media:** add media volumes ([9abe9cd](https://gitlab.com/fi-sas/fisas-gateway/commit/9abe9cd1895c6ebd27a27649d8366a376db431ea))
- **medias:** add media uploads alias ([896c473](https://gitlab.com/fi-sas/fisas-gateway/commit/896c473f567aab759d89e734298dd0cb7fa902d2))
- added scope validation ([f3e7cde](https://gitlab.com/fi-sas/fisas-gateway/commit/f3e7cde3a16207ce860a6c9cafff5456a0e0d304))
- **media:** add static media serve and auth ([2de2c27](https://gitlab.com/fi-sas/fisas-gateway/commit/2de2c27e58014b1719e0c3ab6b5d514a9deebef5))
- adding swagger into gateway ([c3eb0b2](https://gitlab.com/fi-sas/fisas-gateway/commit/c3eb0b21ff6fcca93be9e02a73c41603b75e7961))
- api.service implemented ([1e4a9e8](https://gitlab.com/fi-sas/fisas-gateway/commit/1e4a9e80bb2649acb78192eaa01d7607418d460f))
