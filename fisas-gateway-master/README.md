# FISAS Gateway
This service is responsible for the sasocial gateway as well as the log system and nats server.

## Usage

| Name | Command |  Description |
|---|---|---|
| Start Gateway | npm run dc:up | Compiles and starts in background the gateway on the docker  |
| Show the logs | npm run dc:logs | Opens the docker-compose logs in persistent mode |
| Stop Gateway | npm run dc:down | Stops all containers of gateway service |


### Service Dependecies
These services are mandatory for the gateway to work correctly
 - authorization
 - configuration

## Endpoints
| Name | Endpoint |  Description |
|---|---|---|
| Api Gateway | /api  | To access the REST API of the SASocial  |
| Traefik dashboard | :81/  | Dashboard of reverse proxy and load balancer Traefik |
| Zipkin | /zipkin or :83/ | Zipkin is a distributed tracing system.  |
| Grafana | :84/  | Grafana is the open source analytics & monitoring solution for every database. |
| Prometheus | :85/  | Dashboard of prometheus. |


## Scopes
Here we will provide a brief description of how the scopes system works

## Errors
Here we will provide a brief description of how the error system works

