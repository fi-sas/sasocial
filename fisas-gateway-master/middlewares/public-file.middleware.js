/**
 * 1. Only execute the middleware if the request made is for a static file from
 *    the `uploads` folder.
 * 2. If it is a request for a static file, search for the file in the database
 *       - The file wasn't found in the database => proceed ✅
 *       - The file is public (1) => proceed ✅
 *       - The file isn't public (0)
 *            * The user is authenticated in the token => proceed ✅
 *            * The user isn't authenticated => forbidden ❌
 *
 *
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
module.exports = async function (req, res, next) {
	const ctx = req.$ctx;
	const path = req.originalUrl;
	const upload = new RegExp("^/media/uploads", "i");
	if (upload.test(path)) {
		const files = await ctx.call("media.files.find", {
			query: {
				path: path.substring(path.lastIndexOf("/") + 1),
			},
		});

		if (files === null || files.length === 0) {
			req.$public = true;
			return next();
		}

		if (files[0] && files[0].filename && files[0].mime_type) {
			res.setHeader("Content-Type", files[0].mime_type);
			res.setHeader("Content-Disposition", `attachment; filename="${files[0].filename}"`);
		}

		if (files[0].public) {
			req.$public = true;
			return next();
		}

		req.$public = false;
		return next();
	}

	req.$public = true;
	return next();
};
