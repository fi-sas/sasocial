"use strict";

const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");

module.exports = {
	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Authenticate the request. It check the `Authorization` token value in the request header.
		 * Check the token value & resolve the user by the token.
		 * The resolved user will be available in `ctx.meta.user`
		 *
		 * @param {Context} ctx
		 * @param {Object} route
		 * @param {IncomingRequest} req
		 * @returns {Promise}
		 */
		async authenticate(ctx, route, req) {
			let noAuthentication = false;
			if (req.$action) {
				noAuthentication = req.$action.authentication === false;
			}
			if (noAuthentication) {
				return null;
			}

			// Read the token from header
			const auth = req.headers["authorization"];

			if (auth) {
				const token = auth.startsWith("Bearer") ? auth.slice(7) : auth;

				ctx.meta.showCPUID = true;
				const result = await ctx.call("authorization.authorize.validateToken", {
					token,
				});
				ctx.meta.showCPUID = false;

				if (result) {
					ctx.meta.isGuest = result.isGuest;
					ctx.meta.device = result.device;
					ctx.meta.isBackoffice = result.device.type === "BO";
					ctx.meta.scopes = result.scopes;
					return result.user;
				} else {
					// Invalid token
					throw new Errors.UnauthorizedError("Token invalid", "ERR_INVALID_TOKEN");
				}
			} else {
				// No token. Throw an error or do nothing if anonymous access is allowed.
				throw new Errors.UnauthorizedError("No token provided", "ERR_NO_TOKEN");
			}
		},

		/**
		 * Authorize the request. Check that the authenticated user has right to access the resource.
		 *
		 * @param {Context} ctx
		 * @param {Object} route
		 * @param {IncomingRequest} req
		 * @returns {Promise}
		 */
		async authorize(ctx, route, req) {
			const noAuthorization = req.$action.authorization === false;
			if (noAuthorization) {
				return null;
			}

			// Get the authenticated user.
			const scopes = ctx.meta.scopes; // User from validate-token
			const neededScope = req.$action.scope ? req.$action.scope : false;

			// Check authorization scope
			if (neededScope) {
				if (!_.includes(scopes, neededScope)) {
					// console.debug(`Forbidden => (scope: ${neededScope} meta: ${ctx.meta})`);
					throw new Errors.ForbiddenError("No permission to access this endppoint", "NO_RIGHTS", {
						missingScope: neededScope,
					});
				}
			}

			delete ctx.meta.scopes;

			// TODO ONLY OWNER
			// TODO ONLY BACKOFFICE
		},
	},
};
