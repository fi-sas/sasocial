"use strict";

const E = require("moleculer-web").Errors;
const _ = require("lodash");

module.exports = {
	/**
	 * Settings
	 */
	settings: {
		/**
		 *  On error sanitation
		 * @param {*} req
		 * @param {*} res
		 * @param {*} err
		 */
		onError(req, res, err) {
			const ctx = req.$ctx;

			if (ctx.meta.uploadErrorOccurred) {
				return;
			}

			return this.generateErrorResponse(ctx, err, res);
		},
	},

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Gets de erro translations from the configuration service
		 * @param {*} ctx Moleculer context
		 * @param {*} err Exception obj
		 * @param {*} res Response obj
		 * @returns Promise
		 */
		generateErrorResponse(ctx, err, res) {
			if (err && Array.isArray(err) && err.length > 0) {
				err = err[0];
			}

			ctx.meta.errorOccured = true;

			let errors = [];

			if (err.type && err.type === "VALIDATION_ERROR" && Array.isArray(err.data)) {
				// ON FATEST VALIDATOR ERROR MAKE THE TRANSLATIONS
				const errors_promises = err.data.map((e) =>
					ctx
						.call("configuration.errors.getValidatorError", {
							type: e.type,
							message: e.message,
							field: e.field,
							real: e.real,
							expected: e.expected,
						})
						.then((error) => errors.push(error)),
				);

				return Promise.all(errors_promises)
					.then(() => {
						res.setHeader("Content-Type", "application/json");
						res.writeHead(400);
						res.end(
							JSON.stringify({
								status: "error",
								link: {},
								data: [],
								errors,
							}),
						);
					})
					.catch((exception) => {
						this.logger.error(exception);
						res.setHeader("Content-Type", "application/json");
						res.writeHead(400);
						res.end(
							JSON.stringify({
								status: "error",
								link: {},
								data: [],
								errors: [err.data],
							}),
						);
					});
			} else {
				// SQL ERROR
				if (err.code && err.code.length == 5) {
					err.type = "ERR_SQL_ERROR";
				}

				if (err.code === 404 && err.data && err.data.name) {
					err.type = err.data.name.toUpperCase().concat("_").concat(err.type);
				}

				if (err instanceof E.ServiceUnavailableError) err.type = "SERVICE_UNAVAILABLE";

				if (!err.type) err.type = "INTERNAL_SYSTEM_ERROR";

				// GET THE TRANSLATION ON THE ERRORS SERVICE
				return ctx
					.call("configuration.errors.getError", {
						type: err.type,
						message: err.message,
						data: err.data || {},
					})
					.then((error) => {
						if (error) {
							//error.message =  err.message;
							errors = Array.isArray(error) ? error : [error];
						} else {
							errors = [
								{
									code: err.code || 500,
									message: err.message,
								},
							];
						}

						res.setHeader("Content-Type", "application/json");
						res.writeHead(
							[400, 401, 403, 404, 408, 413, 422, 503].find((c) => c == err.code) ? err.code : 500,
						);
						const data = JSON.stringify({
							status: "error",
							link: {},
							data: [],
							errors: errors,
						});
						res.end(data);
						return data;
					})
					.catch((exception) => {
						this.logger.error(exception);
						res.setHeader("Content-Type", "application/json");
						res.writeHead(err.code && Number.isInteger(err.code) ? err.code : 500);
						const data = JSON.stringify({
							status: "error",
							link: {},
							data: [],
							errors: [
								{
									code: err.code ? err.code : 500,
									messsage: err.message || "Unexpected error occured",
								},
							],
						});
						res.end(data);
						return data;
					});
			}
		},
		/**
		 *  On after call contruct the output response
		 * @param {*} ctx
		 * @param {*} route
		 * @param {*} req
		 * @param {*} res
		 * @param {*} data
		 */
		onAfterCall(ctx, route, req, res, data) {
			if (!ctx.meta.$responseType) ctx.meta.$responseType = "application/json";

			// IF HAS A THROW ENTERS ON onError DONT SEND HEADERS
			if (ctx.meta.ignoreResponseSignature) {
				return data;
			}

			// IF ERROR WHILE UPLOADING
			if (ctx.meta.uploadErrorOccurred) {
				return this.generateErrorResponse(ctx, data, res);
			}

			if (!ctx.meta.errorOccured && ctx.meta.$responseType === "application/json") {
				let link = {};
				if (!Array.isArray(data)) {
					if (data && data.rows) {
						link = _.pick(data, ["total", "page", "pageSize", "totalPages"]);
						data = data.rows;
					} else {
						data = [data];
					}
				} else {
					data = data.flat();
				}

				return {
					status: "success",
					link,
					data,
					errors: [],
				};
			} else {
				return data;
			}
		},
	},
};
