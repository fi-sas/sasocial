const IO = require("socket.io");

module.exports = {
	name: "",
	settings: {
		io: null,
	},
	created() {},
	started() {
		this.settings.io = IO(this.server, {
			cors: {
				origin: "*",
			},
		});
		//this.logger.info("<--THIS->");
		//	console.log(this);
		//this.logger.info("<--SERVER->");
		//console.log(this.server);
		//this.logger.info("<--IO->");
		//console.log(this.settings.io);
		//this.logger.info("SocketIOMixin Created!");

		this.settings.io.use((socket, next) => {
			const token = socket.handshake.auth.token;

			if (token) {
				this.broker
					.call("authorization.authorize.validateToken", {
						token,
					})
					.then((result) => {
						socket.userID = result.user.id;
						socket.deviceID = result.device.id;
						next();
					})
					.catch((err) => {
						next(err);
					});
			} else {
				const err = new Error("NO_TOKEN_ACCESS");
				err.data = { content: "You must use the access token" };
				next(err);
			}
		});

		this.settings.io.on("connection", (socket) => {
			this.logger.info("Connection estabelished! ", socket.id);
			if (socket.userID) {
				this.logger.info("Socket join room user ->", socket.userID);
				socket.join("userID" + socket.userID);
			}

			if (socket.deviceID) {
				this.logger.info("Socket join room device ->", socket.deviceID);
				socket.join("deviceID" + socket.deviceID);
			}

			socket.on("disconnect", (reason) => {
				this.logger.info("Disconnection.... reason: ", reason);
			});
		});
		this.logger.info("SocketIOMixin Started!");
	},
	stopped() {},
	actions: {
		emitMessage: {
			params: {
				user_id: { type: "number", positive: true, convert: true, optional: true },
				device_id: { type: "number", positive: true, convert: true, optional: true },
				event: { type: "string" },
				args: { type: "array", items: "any", default: [] },
			},
			handler(ctx) {
				if (ctx.params.user_id)
					this.settings.io
						.to("userID".concat(ctx.params.user_id))
						.emit(ctx.params.event, ctx.params.args);

				if (ctx.params.device_id)
					this.settings.io
						.to("deviceID".concat(ctx.params.device_id))
						.emit(ctx.params.event, ctx.params.args);
			},
		},
		broadcastMessage: {
			params: {
				event: { type: "string" },
				args: { type: "array", items: "any", default: [] },
			},
			handler(ctx) {
				this.settings.io.emit(ctx.params.event, ...ctx.params.args);
			},
		},
	},
	methods: {},
};
