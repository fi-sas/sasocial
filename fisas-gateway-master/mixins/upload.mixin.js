"use strict";

const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const Types = require("../mixins/values/types");
const crypto = require("crypto");
const path = require("path");
const fs = require("fs");
const sizeOf = require("image-size");
const checkDiskSpace = require("check-disk-space").default;

module.exports = {
	/**
	 * Settings
	 */
	settings: {
		publicPath: path.join("/uploads"),
	},

	/**
	 * Actions
	 */
	actions: {
		upload: {
			timeout: 60 * 1000,
			//visibility: "public",
			handler(ctx) {
				ctx.meta.uploadErrorOccurred = false;
				return this.hasDiskSpaceLeft().then(() => {
					return new Promise((resolve, reject) => {
						let mimeType = null;
						let fileMeta = null;
						let fileName = null;
						try {
							mimeType = this.getMimeType(ctx.meta.mimetype);
							fileMeta = this.createFilePath(mimeType);

							if (!ctx.meta.filename) {
								ctx.meta.filename = fileMeta.name;
							}
							fileName = this.sanatizeFileName(ctx.meta.filename, mimeType);
						} catch (ex) {
							ctx.meta.uploadErrorOccurred = true;
							reject(ex);
							return;
						}

						const f = fs.createWriteStream(fileMeta.path);
						f.on("close", () => {
							// File written successfully
							this.logger.info(`Uploaded file stored in '${fileMeta.path}'`);

							/**
							 * CHECK FILE WITH CLAMAV
							 */
							ctx
								.call("media.antivirus.scan", { path: fileMeta.path })
								.then((result) => {
									ctx.params = this.createFileData(
										ctx.meta.$multipart.public == "true",
										mimeType,
										fileMeta,
										fileName,
										ctx.meta.$multipart.weight,
									);
									resolve({ filePath: fileMeta.path, meta: ctx.meta });
								})
								.catch((err) => {
									// Remove the errored file.
									if (fs.existsSync(fileMeta.path)) {
										//file exists
										fs.unlinkSync(fileMeta.path);
									}
									ctx.meta.uploadErrorOccurred = true;
									reject(err);
								});
						});

						ctx.params.on("error", (err) => {
							this.logger.info("File error received", err.message);
							ctx.meta.uploadErrorOccurred = true;
							reject(err);
							// Destroy the local file
							f.destroy(err);
						});

						f.on("error", (err) => {
							this.logger.error("Filestream Error");
							this.logger.error(err);
							// Remove the errored file.
							if (fs.existsSync(fileMeta.path)) {
								//file exists
								fs.unlinkSync(fileMeta.path);
							}
							ctx.meta.uploadErrorOccurred = true;
							reject(err);
						});

						ctx.params.pipe(f);
					}).then(() => {
						return ctx.call("media.files.create", ctx.params);
					});
				});
			},
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Return the mimeType object
		 * @param {*} inputMimeType
		 */
		getMimeType(inputMimeType) {
			let mimeType = this.findMimeType(inputMimeType);
			if (!mimeType.length) {
				throw new ValidationError("Invalid Mime Type", "ERR_INVALID_MIME_TYPE", {
					mimeType: inputMimeType,
				});
			}

			mimeType = mimeType.pop();
			return mimeType;
		},
		/**
		 * Return a string with the extension
		 * @param {*} mimeType
		 */
		getExtension(mimeType) {
			let [extension] = mimeType.extension;
			if (extension.length !== 1) {
				extension = mimeType.mimetype.split("/");
				extension = extension.pop();
				if (!this.isValidExtension(extension)) {
					[extension] = mimeType.extension;
				}
			}

			return extension;
		},
		/**
		 *
		 * @param {*} fileName
		 * @return string filename
		 */
		sanatizeFileName(fileName, mimeType) {
			if (!fileName.includes(".")) fileName = `${fileName}.${this.getExtension(mimeType)}`;

			return fileName;
		},
		/**
		 * Return the fileMeta object
		 * @param {*} mimeType
		 */
		createFilePath(mimeType) {
			const randomName = crypto.randomBytes(16).toString("hex");
			const fileName = `${randomName}-${Date.now()}.${this.getExtension(mimeType)}`;
			const filePath = path.join(this.settings.publicPath, fileName);
			return {
				path: filePath,
				name: fileName,
				mimeType,
			};
		},
		/**
		 * REeturn the data for create the entry on database
		 * @param boolean public boolean
		 * @param {*} mimeType mimeType object
		 * @param {*} fileMeta fileMeta object
		 * @param number weight
		 */
		createFileData(isPublic, mimeType, fileMeta, fileName, weight) {
			const data = {
				public: isPublic,
				type: mimeType.type,
				mime_type: mimeType.mimetype,
				path: fileMeta.name,
				filename: fileName,
				weight: parseFloat(weight) || 0,
				updated_at: new Date(),
				created_at: new Date(),
			};

			if (mimeType.type === "IMAGE") {
				const dimensions = sizeOf(fileMeta.path);
				data.width = dimensions.width || 0;
				data.height = dimensions.height || 0;
			}

			return data;
		},
		isValidExtension(extension) {
			let ext = extension;
			if (extension.charAt(0) === ".") {
				ext = extension.substr(1);
			}

			const list = Types.filter((type) => type.extension.indexOf(ext) !== -1);

			return list.length === 1;
		},
		isValidMimeType(mimeType) {
			const list = Types.filter((type) => type.mimetype === mimeType);
			return list.length === 1;
		},
		findMimeType(mimeType) {
			const list = Types.filter((type) => type.mimetype === mimeType);

			if (list.length === 1) {
				return [list[0]];
			}

			return [];
		},
		hasDiskSpaceLeft() {
			return checkDiskSpace("/uploads").then((diskSpace) => {
				const max_size = 100 * 1024 * 1024; // TODO 100MB of limit, later put this on the .env?
				if (diskSpace.free <= max_size) {
					throw new ValidationError(
						"There is not enough space to upload securely",
						"ERR_LOW_DISK_SPACE",
						{
							free: this.bytesToMegaBytes(diskSpace.free),
							size: this.bytesToMegaBytes(diskSpace.size),
						},
					);
				}

				return true;
			});
		},
		bytesToMegaBytes(bytes) {
			return bytes / (1024 * 1024);
		},
	},
};
