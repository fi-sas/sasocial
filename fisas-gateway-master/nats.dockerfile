FROM nats:2

COPY nats-server.conf nats-server.conf

CMD ["-c", "/nats-server.conf"]
