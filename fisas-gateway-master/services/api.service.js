"use strict";
/**
 * Mixins Load
 */

const cookieParser = require("cookie-parser");
const ApiGateway = require("moleculer-web");
const UploadMixin = require("../mixins/upload.mixin");
const ResponseMixin = require("../mixins/response.mixin");
const AuthorizationMixin = require("../mixins/authorization.mixin");
/**
 * Middlewares Load
 */
const publicFile = require("../middlewares/public-file.middleware");

/**
 * Dependencies
 */
const _ = require("lodash");

const SocketIOMixin = require("../mixins/socketio.mixin");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 * @typedef {import('http').IncomingMessage} IncomingRequest Incoming HTTP Request
 * @typedef {import('http').ServerResponse} ServerResponse HTTP Server Response
 */

module.exports = {
	name: "api",
	mixins: [ApiGateway, AuthorizationMixin, ResponseMixin, UploadMixin, SocketIOMixin],

	// More info about settings: https://moleculer.services/docs/0.14/moleculer-web.html
	settings: {
		// Exposed port
		port: process.env.PORT || 3000,

		// Exposed IP
		ip: "0.0.0.0",

		// Global Express middlewares. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Middlewares
		use: [],

		// Rate limiter
		rateLimit: {
			// How long to keep record of requests in memory (in milliseconds).
			// Defaults to 60000 (1 min)
			window: 60 * 1000,
			// Max number of requests during window. Defaults to 30
			limit: 10000,
			// Set rate limit headers to response. Defaults to false
			headers: true,
		},

		// Global CORS settings for all routes
		cors: {
			// Configures the Access-Control-Allow-Origin CORS header.
			origin: "*",
			// Configures the Access-Control-Allow-Methods CORS header.
			methods: ["GET", "OPTIONS", "POST", "PUT", "PATCH", "DELETE"],
			// Configures the Access-Control-Allow-Headers CORS header.
			allowedHeaders: "*",
			// Configures the Access-Control-Expose-Headers CORS header.
			exposedHeaders: "*",
			// Configures the Access-Control-Allow-Credentials CORS header.
			credentials: false,
			// Configures the Access-Control-Max-Age CORS header.
			maxAge: 3600,
		},

		actionsWithCookies: ["authorization.authorize.refreshToken", "authorization.authorize.logout"],

		// DEBUG ONLY
		// Folder to server assets (static files)
		assets: {
			// Root folder of assets
			folder: "./public",
			// Options to `server-static` module
			options: {},
		},

		routes: [
			{
				path: "/media/uploads",
				use: [
					(req, res, next) => {
						// Allow access request from any computers
						res.setHeader("Access-Control-Allow-Origin", "*");
						res.setHeader("Access-Control-Allow-Headers", "Authorization");
						res.setHeader("Access-Control-Allow-Methods", "OPTIONS,GET");
						res.setHeader("Access-Control-Allow-Credentials", true);
						if ("OPTIONS" == req.method) {
							res.writeHead(200);
							res.end();
						} else {
							next();
						}
					},
					publicFile,
					async (req, res, next) => {
						const ctx = req.$ctx;
						const autenticate = req.$service.authenticate;

						return next();
						/*
						REVIEW THIS : With this active we need to send the token to download
						if (!req.$public) {
							return autenticate(ctx, null, req)
								.then(() => {
									return next();
								})
								.catch((err) => {
									//return next(err);
									res.setHeader("Content-Type", "application/json");
									res.writeHead(401);
									const data = JSON.stringify({
										status: "error",
										link: {},
										data: [],
										errors: [
											{
												error: err.code,
												message: err.message,
											},
										],
									});
									res.end(data);
								});
						} else {
							return next();
						}*/
					},
					ApiGateway.serveStatic("/uploads", {
						maxAge: "1y",
					}),
				],
			},
			{
				path: "/api",

				whitelist: ["**"],

				// Route-level Express middlewares. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Middlewares
				use: [cookieParser()],

				// Enable/disable parameter merging method. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Disable-merging
				mergeParams: true,

				// Enable authentication. Implement the logic into `authenticate` method. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Authentication
				authentication:
					!_.isEmpty(process.env.GW_AUTHENTICATION) && process.env.GW_AUTHENTICATION === "false"
						? false
						: true,

				// Enable authorization. Implement the logic into `authorize` method. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Authorization
				authorization:
					!_.isEmpty(process.env.GW_AUTHORIZATION) && process.env.GW_AUTHORIZATION === "false"
						? false
						: true,

				// The auto-alias feature allows you to declare your route alias directly in your services.
				// The gateway will dynamically build the full routes from service schema.
				autoAliases: true,

				aliases: {
					// File upload from HTML multipart form
					"POST /media/files": "multipart:api.upload",
					// File upload from AJAX or cURL
					//"POST /media/files/stream": "stream:media.files.stream",
				},

				// Calling options
				callOptions: {
					timeout: 100 * 1000,
				},

				/**
				 * Busboy Configs - Upload Configs
				 */
				busboyConfig: {
					limits: {
						fileSize: 100 * 1024 * 1024, // TODO 100MB of limit, later put this on the .env?
						files: 1,
					},
				},

				/**
				 * Before call hook. You can check the request.
				 * @param {Context} ctx
				 * @param {Object} route
				 * @param {IncomingRequest} req
				 * @param {ServerResponse} res
				 * @param {Object} data
				 **/
				onBeforeCall(ctx, route, req) {
					// SAVE COOKIES IN META IN SOME ACTIONS
					if (this.settings.actionsWithCookies.find((act) => act === req.$action.name))
						ctx.meta.$cookies = req.cookies;

					// Set request headers to context meta
					//ctx.meta.userAgent = req.headers["user-agent"];

					// CHECK EXTERNAL HEADERS
					if (process.env.EXTERNAL_HEADERS) {
						const ext_headers = process.env.EXTERNAL_HEADERS.split(",");
						ext_headers.forEach((h) => {
							ctx.meta[h] = req.headers[h];
						});
					}

					ctx.meta.alimentation_entity_id = req.headers["x-alimentation-entity-id"];
					ctx.meta.language_acronym = req.headers["x-language-acronym"];
					ctx.meta.language_id = req.headers["x-language-id"];

					if (ctx.meta.language_acronym || ctx.meta.language_id) {
						if (ctx.meta.language_id) {
							return ctx
								.call("configuration.languages.get", {
									id: ctx.meta.language_id,
								})
								.then((languages) => {
									ctx.meta.language = languages[0];
								});
						}

						if (ctx.meta.language_acronym) {
							ctx.meta.language_acronym = ctx.meta.language_acronym.toLowerCase();
							return ctx
								.call("configuration.languages.getByAcronym", {
									acronym: ctx.meta.language_acronym,
								})
								.then((languages) => {
									ctx.meta.language = languages[0];
									ctx.meta.language_id = languages[0].id;
								});
						}
					}
				},

				/**
				 * After call hook. You can modify the data.
				 * @param {Context} ctx
				 * @param {Object} route
				 * @param {IncomingRequest} req
				 * @param {ServerResponse} res
				 * @param {Object} data
				 */
				onAfterCall(ctx, route, req, res, data) {
					if (this.onAfterCall) {
						return this.onAfterCall(ctx, route, req, res, data);
					}

					if (this.service && this.service.onAfterCall) {
						return this.service.onAfterCall(ctx, route, req, res, data);
					}
				},
				// Calling options. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Calling-options
				callingOptions: {},

				bodyParsers: {
					json: {
						strict: false,
						limit: "1MB",
					},
					urlencoded: {
						extended: true,
						limit: "1MB",
					},
				},

				// Mapping policy setting. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Mapping-policy
				mappingPolicy: "all", // Available values: "all", "restrict"

				// Enable/disable logging
				logging: true,
			},
		],

		// Do not log client side errors (does not log an error response when the error.code is 400<=X<500)
		log4XXResponses: false,
		// Logging the request parameters. Set to any log level to enable it. E.g. "info"
		logRequestParams: null,
		// Logging the response data. Set to any log level to enable it. E.g. "info"
		logResponseData: null,
	},

	methods: {},
};
