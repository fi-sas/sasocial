# [1.18.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.17.0...v1.18.0) (2022-01-25)


### Bug Fixes

* **health:** check response length in dashboard ([a5db456](https://gitlab.com/fi-sas/fisas-health/commit/a5db45652ee9834b7112265a4d2a1639b2a70755))
* **health:** solving lint issues and improvements ([20f7d28](https://gitlab.com/fi-sas/fisas-health/commit/20f7d2880577d84913e3540ce60cb8631bdb85a1))


### Features

* **health:** appointments sorted in dashboard ([56f2839](https://gitlab.com/fi-sas/fisas-health/commit/56f28399f40bd90031de385f1ab5225d5acc5e67))
* **health:** bypass balance with current account ([e3f07da](https://gitlab.com/fi-sas/fisas-health/commit/e3f07daccfc85951938584294aa017bb551061f7))
* **health:** configurations service ([c14d3d8](https://gitlab.com/fi-sas/fisas-health/commit/c14d3d85c2e8130e09b7abb407edb7c59829e048))

# [1.17.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.16.0...v1.17.0) (2022-01-14)


### Bug Fixes

* **port:** service port change ([2025432](https://gitlab.com/fi-sas/fisas-health/commit/2025432e77abc85c5aaedecc9500f2d2e360f2d5))


### Features

* **calendar:** calendar export + ics library ([f81574d](https://gitlab.com/fi-sas/fisas-health/commit/f81574dad0fdae2c81d274f635236c8184027cf4))

# [1.16.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.15.0...v1.16.0) (2022-01-06)


### Bug Fixes

* **various:** bugs and code improvement ([1770bf0](https://gitlab.com/fi-sas/fisas-health/commit/1770bf07fcbeaf607e6fe090ddd85debe670a95b))
* **various:** several fixs and improvements ([c861698](https://gitlab.com/fi-sas/fisas-health/commit/c861698299fb3933682db65500fd5e4cffe32d7a))


### Features

* **holidays:** check holidays table ([22a90b6](https://gitlab.com/fi-sas/fisas-health/commit/22a90b66772150e92748ddd4021db7c0c5c38310))
* **schedule:** agenda dos profissionais ([321373d](https://gitlab.com/fi-sas/fisas-health/commit/321373d4e9cde34050447db409e0ab6d0e1c5822))
* **various:** improvements and bug fixs ([7b46939](https://gitlab.com/fi-sas/fisas-health/commit/7b46939b4b74a6ea58d7567629a82b392b4df6d1))
* **various:** new tables and features ([2f28df9](https://gitlab.com/fi-sas/fisas-health/commit/2f28df92fa8e0b0ad2a3a8b511fd14aefac3ccee))

# [1.15.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.14.0...v1.15.0) (2021-10-15)


### Features

* **migration:** novos campos tabela especialidade ([b1ef6cc](https://gitlab.com/fi-sas/fisas-health/commit/b1ef6cc9541c376bea9feea532d2509182e05a15))

# [1.14.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.13.0...v1.14.0) (2021-09-14)


### Bug Fixes

* **await:** remover await do notifications ([fc94cf8](https://gitlab.com/fi-sas/fisas-health/commit/fc94cf839eb5b0757635db11acb29bc9f2e405a5))
* **return:** missing returns ([f89f9ef](https://gitlab.com/fi-sas/fisas-health/commit/f89f9ef27a2815af5a06b62bd4fab637b035a711))


### Features

* **notifications:** Implementação Notificações ([83093dc](https://gitlab.com/fi-sas/fisas-health/commit/83093dcbeba0f4dcc0154cc165fce357d05af617))

# [1.13.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.12.0...v1.13.0) (2021-09-13)


### Bug Fixes

* **various:** Fixing found bugs ([bfc584e](https://gitlab.com/fi-sas/fisas-health/commit/bfc584ea907fea1c846f8e50aacf56ee4f664edf))


### Features

* **appointment:** Marcação direta profissional ([9b62fdf](https://gitlab.com/fi-sas/fisas-health/commit/9b62fdfed16fd259d75a3d7fc1d66bbaa30b5d63))

# [1.12.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.11.0...v1.12.0) (2021-09-10)


### Bug Fixes

* **doctor:** response when adding to specialty ([4ebec1a](https://gitlab.com/fi-sas/fisas-health/commit/4ebec1a740b4ce00e73ec89d3f47bbee3396ff72))
* **error:** Error Message changed ([425d64b](https://gitlab.com/fi-sas/fisas-health/commit/425d64b022777e483ee8ef35b1de64790133f15d))
* **keys:** add "health" key to errors ([11753e5](https://gitlab.com/fi-sas/fisas-health/commit/11753e55b694d2937a1e132ebd86602f7da5edb6))
* **various:** fix some bugs ([5593204](https://gitlab.com/fi-sas/fisas-health/commit/55932047ca02a1fb01d1b969b24c750b2008f9ea))
* **various:** missing features ([90fb69a](https://gitlab.com/fi-sas/fisas-health/commit/90fb69acfdd1e3f1cbe4f04bae4c51d29fe73d88))


### Features

* **price:** price calculation based ([e94e52f](https://gitlab.com/fi-sas/fisas-health/commit/e94e52f5cacb1903e4229f81a8d36547aded4b17))

# [1.11.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.10.1...v1.11.0) (2021-09-08)


### Bug Fixes

* **various:** database and external_profile ([9d4f41e](https://gitlab.com/fi-sas/fisas-health/commit/9d4f41e2815d8530554d7311a7b6b21672a20d70))


### Features

* **appointment:** Approve external request ([ec7f1d2](https://gitlab.com/fi-sas/fisas-health/commit/ec7f1d243980291cea9be9a8e57f70f1f9af1daa))

## [1.10.1](https://gitlab.com/fi-sas/fisas-health/compare/v1.10.0...v1.10.1) (2021-09-06)


### Bug Fixes

* **cache:** Add this.clearCache() ([adee0fe](https://gitlab.com/fi-sas/fisas-health/commit/adee0fea17658bf2a3aaeb275885571d48c6826a))
* **place:** missing email field ([3194b9d](https://gitlab.com/fi-sas/fisas-health/commit/3194b9da45fea94902f05e1aa55646ceceda7a39))
* **various:** fixing lint warnings ([628f5eb](https://gitlab.com/fi-sas/fisas-health/commit/628f5eb4f376541110bb4825c5d2de3a5d5d9da8))
* **various:** Solving some bugs ([a74a026](https://gitlab.com/fi-sas/fisas-health/commit/a74a0268853ccc3708db7412a8663583a5a315c3))
* **various:** Solving some bugs ([2c95122](https://gitlab.com/fi-sas/fisas-health/commit/2c9512232c4d327724e4b13ac2de5f262f3dfe9b))

# [1.10.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.9.0...v1.10.0) (2021-09-06)


### Features

* **attendance:** iniciar registar dados consulta ([9dd8327](https://gitlab.com/fi-sas/fisas-health/commit/9dd8327dd141b1f2af8a037b47dba35fe3425976))
* **checklist:** checklist response in specialty ([84f7b93](https://gitlab.com/fi-sas/fisas-health/commit/84f7b93a5c03996b97c980b983f845f4c91984f3))

# [1.9.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.8.0...v1.9.0) (2021-08-31)


### Features

* **attendance:** serviços para consultas ([9a854b1](https://gitlab.com/fi-sas/fisas-health/commit/9a854b137b50de1e7ac89f34634f661d19ec5381))

# [1.8.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.7.0...v1.8.0) (2021-08-30)


### Bug Fixes

* **bug:** showing doctor busy appointments ([d2e4995](https://gitlab.com/fi-sas/fisas-health/commit/d2e49954b0277d69af20529ec58ce77f853423de))
* **doctor:** Missing commit-change variable ([2c57775](https://gitlab.com/fi-sas/fisas-health/commit/2c5777539cbe81efed662927325e086c90a60c11))
* **various:** last changes BD ([3efc656](https://gitlab.com/fi-sas/fisas-health/commit/3efc656f60b6afd0a22ae6e6f51d09f7aa27b91e))


### Features

* **doctor:** Using moment functions ([f2cf7a3](https://gitlab.com/fi-sas/fisas-health/commit/f2cf7a34918a3359eb3a97b33c4c27d1b82fded5))
* **various:** database schema e some services ([1aca622](https://gitlab.com/fi-sas/fisas-health/commit/1aca6225218773cf9978ab2c5f3586ccf749f1c3))

# [1.7.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.6.1...v1.7.0) (2021-08-25)


### Bug Fixes

* **historic:** Historic new approach ([560d7e0](https://gitlab.com/fi-sas/fisas-health/commit/560d7e0db5ff46d12d1a51001f12c38361431852))


### Features

* **historic:** histórico de marcações/consultas ([c767b3f](https://gitlab.com/fi-sas/fisas-health/commit/c767b3fdf7a2811b046c52e6f88eca13cb4b9e62))

## [1.6.1](https://gitlab.com/fi-sas/fisas-health/compare/v1.6.0...v1.6.1) (2021-08-24)


### Bug Fixes

* **migrations:** Change type of hour/date field ([465dfd8](https://gitlab.com/fi-sas/fisas-health/commit/465dfd8dc6b4ba50698d03e6bc52a0cee5bc077f))

# [1.6.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.5.0...v1.6.0) (2021-08-23)


### Bug Fixes

* **reports:** remove report service ([7d42a23](https://gitlab.com/fi-sas/fisas-health/commit/7d42a230a1b7202a816b4b9208eec757c18d583f))
* **various:** removing attendance table ([d6e2ca1](https://gitlab.com/fi-sas/fisas-health/commit/d6e2ca1309e5da5a66c4dbdb8ea51d9d29188384))


### Features

* **report:** Emitir Relatorios Presença e Gestão ([673b49a](https://gitlab.com/fi-sas/fisas-health/commit/673b49ac9a26e58ac19b8993117d41298afb55b6))

# [1.5.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.4.0...v1.5.0) (2021-08-19)


### Features

* **complain:** create complain service ([594e439](https://gitlab.com/fi-sas/fisas-health/commit/594e4394b2d3addad09b558c466eb354641bcc57))

# [1.4.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.3.0...v1.4.0) (2021-08-18)


### Bug Fixes

* **code:** code improvement ([96540ac](https://gitlab.com/fi-sas/fisas-health/commit/96540ac3cf0be9516df5dca9f2c270ba757ca785))
* **variable:** delete an unused variable ([09df47c](https://gitlab.com/fi-sas/fisas-health/commit/09df47c16a92a7d3cbfdc6826378fd9b7838e8a8))


### Features

* **doctor:** create an external doctor ([a8e506b](https://gitlab.com/fi-sas/fisas-health/commit/a8e506ba01d51f7a4451c35abbe5de82ef89a930))

# [1.3.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.2.0...v1.3.0) (2021-08-17)


### Features

* **appointment:** Change Appointment ([5163b17](https://gitlab.com/fi-sas/fisas-health/commit/5163b17490fc43580eae046f3c788c345d4684de))

# [1.2.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.1.0...v1.2.0) (2021-08-16)


### Bug Fixes

* **await:** remove doble async functions ([d338694](https://gitlab.com/fi-sas/fisas-health/commit/d33869472363d5fc7a29271a8fb47adf64386979))
* **code:** removing unnecessaries async ([33606bd](https://gitlab.com/fi-sas/fisas-health/commit/33606bd1d9f823def726b5f51d9cdbd06fcc8abb))
* **date:** fix date format (YYYY-MM-DD) ([f47af56](https://gitlab.com/fi-sas/fisas-health/commit/f47af5624b1eec742141f905c944e6ba2fe27ee1))


### Features

* **schedule:** View and Manage Doctor Schedules ([b11bdc7](https://gitlab.com/fi-sas/fisas-health/commit/b11bdc71ca5a3ae181de8906a5bf5097eb0c8094))

# [1.1.0](https://gitlab.com/fi-sas/fisas-health/compare/v1.0.0...v1.1.0) (2021-08-16)


### Bug Fixes

* **change:folder:** Folder structure ([60a51a2](https://gitlab.com/fi-sas/fisas-health/commit/60a51a2eb040aec6ba09d31f037f132b89fc9785))
* **database:** change database schema ([4cf0e40](https://gitlab.com/fi-sas/fisas-health/commit/4cf0e401ecf451d31633ec7fae61ccbf8ac7758c))
* **specialty:** Price Variation Show ([579c088](https://gitlab.com/fi-sas/fisas-health/commit/579c08867598ee9bb962711aadb9547cbeff7755))
* **various:** Fix Merge files ([3cb9b39](https://gitlab.com/fi-sas/fisas-health/commit/3cb9b39ef6cafe8478ce81ad7608cf468f6e1d08))
* **various:** missing commit for database services ([e4786b1](https://gitlab.com/fi-sas/fisas-health/commit/e4786b1fbd6e3a5ecd42520eb926e3179ff2b623))
* **various:** Update Database Schema and Services ([e10c60b](https://gitlab.com/fi-sas/fisas-health/commit/e10c60b924942ff87196ef1b093bde246667a822))


### Features

* **specialties:** view available specialties ([a3dd909](https://gitlab.com/fi-sas/fisas-health/commit/a3dd9099db42b61757aec6b0ea26c3017c692d05))
* **various:** Specialty Creation with Costs ([fe94c92](https://gitlab.com/fi-sas/fisas-health/commit/fe94c92483eb55a5300f62766ef995402df44b02))

# 1.0.0 (2021-07-27)


### Features

* **specialties:** view available specialties ([7c8c9da](https://gitlab.com/fi-sas/fisas-health/commit/7c8c9daa9cb866806b0aa6eeb845703cac467452))
* **various:** Medical Specialties and Doctors ([98ba022](https://gitlab.com/fi-sas/fisas-health/commit/98ba022954f05176f32ecab3046cbc7a9c95caa3))
