module.exports.up = async (db) =>
	db.schema
		.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
		// Types of Consult table
		.createTable("appointment_type", (table) => {
			table.increments();
			table.timestamps(true, true);
		})
		// Types of Consult Translation table
		.createTable("appointment_type_translation", (table) => {
			table.increments();
			table.integer("appointment_type_id").unsigned().references("id").inTable("appointment_type");
			table.integer("language_id").unsigned();
			table.string("name");
		})

		// Place table
		.createTable("place", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("email").notNullable();
			table.string("address").notNullable();
			table.string("postal_code").notNullable();
			table.string("city").notNullable();
			table.integer("phone");
			table.integer("fax");
			table.boolean("is_active").defaultTo(true).notNullable();
			table.timestamps(true, true);
		})

		// Medical Specialty table
		.createTable("specialty", (table) => {
			table.increments();
			table.boolean("is_external").defaultTo(false).notNullable();
			table.text("description");
			table.text("regulation");
			table.integer("avarage_time").defaultTo(30).notNullable();
			table.string("product_code", 255).notNullable();
			table
				.enu("payment_type", ["Ato Consulta", "Pré Pagamento"], {
					useNative: true,
					enumName: "payment_type",
				})
				.notNullable();
			table.integer("place_id").unsigned().references("id").inTable("place");
			table.text("payment_information");
			table.integer("organic_unit_id").unsigned();
			table.boolean("is_active").defaultTo(true).notNullable();
			table.timestamps(true, true);
		})

		// Medical Specialty Translation table
		.createTable("specialty_translation", (table) => {
			table.increments();
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.integer("language_id").unsigned();
			table.string("name");
		})

		// Tariff table
		.createTable("tariff", (table) => {
			table.increments();
			table.string("description").notNullable();
			table.boolean("is_scolarship").notNullable().defaultTo(false);
		})

		// Price Variation for Medical Specialty table
		.createTable("price_variation", (table) => {
			table.increments();
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.integer("tariff_id").unsigned().references("id").inTable("tariff");
			table.integer("vat_id").unsigned();
			table.decimal("price", 12, 5);
			table.timestamps(true, true);
		})

		// Profile User table
		.createTable("profile_user", (table) => {
			table.increments();
			table.integer("tariff_id").unsigned().references("id").inTable("tariff");
			table.integer("profile_id").unsigned();
		})

		// Types of Consult By Medical Specialty table
		.createTable("specialty_appointment_type", (table) => {
			table.increments();
			table.integer("appointment_type_id").unsigned().references("id").inTable("appointment_type");
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.timestamps(true, true);
		})

		// Checklist By Medical Specialty table
		.createTable("specialty_checklist", (table) => {
			table.increments();
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.string("question").notNullable();
			table
				.enu("answer_type", ["boolean", "text", "number", "date"], {
					useNative: true,
					enumName: "answer_type",
				})
				.notNullable();
			table.timestamps(true, true);
		})

		.createTable("doctor_external", (table) => {
			table.increments();
			table.string("name").nullable();
			table.string("email").nullable();
			table.enum("gender", ["M", "F", "U"]);
			table.string("address").nullable();
			table.string("postal_code").nullable();
			table.string("city").nullable();
			table.string("country").nullable();
			table.string("phone").nullable();
			table.string("nif").nullable();
			table.boolean("is_validated").defaultTo(false);
			table.boolean("is_active").defaultTo(true);
			table.boolean("allow_historic");
			table.timestamps(true, true);
		})

		.createTable("doctor_external_attachment", (table) => {
			table.increments();
			table.integer("doctor_external_id").unsigned().references("id").inTable("doctor_external");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Doctor Informations table
		.createTable("doctor", (table) => {
			table.increments();
			table.integer("user_id").unsigned();
			table.boolean("allow_historic");
			table.boolean("is_external");
			table.timestamps(true, true);
		})

		// Doctor Attachements table
		.createTable("doctor_attachment", (table) => {
			table.increments();
			table.integer("doctor_id").unsigned().references("id").inTable("doctor");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Doctor Specialty Association table
		.createTable("doctor_specialty", (table) => {
			table.increments();
			table.integer("doctor_id").unsigned().references("id").inTable("doctor");
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.timestamps(true, true);
		})

		// periods table
		.createTable("period", (table) => {
			table.increments();
			table.integer("doctor_specialty_id").unsigned().references("id").inTable("doctor_specialty");
			table.date("start_date").notNullable();
			table.date("end_date").notNullable();
			table.timestamps(true, true);
		})

		// schedule table
		.createTable("schedule", (table) => {
			table.increments();
			table.integer("period_id").unsigned().references("id").inTable("period");
			table
				.enu(
					"day",
					["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
					{
						useNative: true,
						enumName: "day",
					},
				)
				.notNullable();
			table.specificType("start_hour", "time").notNullable();
			table.specificType("end_hour", "time").notNullable();
			table.timestamps(true, true);
		})

		// Appointment table
		.createTable("appointment", (table) => {
			table.increments();
			table.integer("user_id").unsigned();
			table.integer("corporate_id").unsigned();
			table.string("student_number");
			table.string("name", 255);
			table.enu("gender", ["M", "F", "U"]);
			table.string("email");
			table.string("mobile_phone_number");
			table.string("telephone_number");
			table.integer("document_type_id").unsigned();
			table.string("identification_number");
			table.string("nif");
			table.string("niss");
			table.string("address");
			table.string("postal_code");
			table.string("city");
			table.string("nationality");
			table.integer("course_id").unsigned();
			table.integer("course_year");
			table.integer("department_id").unsigned();
			table.integer("organic_unit_id").unsigned();
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.date("date");
			table.specificType("start_hour", "time");
			table.specificType("end_hour", "time");
			table.boolean("has_scholarship");
			table.text("notes");
			table.boolean("allow_historic").defaultTo(false);
			table.boolean("overlapAppointment").defaultTo(false);
			table.integer("appointment_type_id").unsigned();
			table.enu("payment_type", ["Ato Consulta", "Pré Pagamento"], {
				useNative: true,
				enumName: "payment_type_appointment",
			});
			table.decimal("price", 8, 2).defaultTo(0);
			table.integer("tariff_id").unsigned().references("id").inTable("tariff");
			table.integer("doctor_id").unsigned().references("id").inTable("doctor");
			table.integer("file_certificate_id").unsigned();
			table.string("status").notNullable();
			table.timestamps(true, true);
		})

		// attendance table
		.createTable("attendance", (table) => {
			table.increments();
			table.integer("appointment_id").unsigned().references("id").inTable("appointment");
			table.text("observations");
			table.date("date");
			table.specificType("start_hour", "timetz");
			table.specificType("end_hour", "timetz");
			table.string("status").notNullable();
			table.timestamps(true, true);
		})

		// Checklist attendance table
		.createTable("attendance_checklist", (table) => {
			table.increments();
			table.integer("attendance_id").unsigned().references("id").inTable("attendance");
			table.text("notes");
			table.timestamps(true, true);
		})

		// Checklist Attachment attendance table
		.createTable("checklist_attachment", (table) => {
			table.increments();
			table
				.integer("attendance_checklist_id")
				.unsigned()
				.references("id")
				.inTable("attendance_checklist");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Checklist Answers table
		.createTable("checklist_answers", (table) => {
			table.increments();
			table
				.integer("attendance_checklist_id")
				.unsigned()
				.references("id")
				.inTable("attendance_checklist");
			table.string("question");
			table.string("answer");
			table.timestamps(true, true);
		})

		// Attendance Attachment table
		.createTable("attendance_attachment", (table) => {
			table.increments();
			table.integer("attendance_id").unsigned().references("id").inTable("attendance");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Historic Appointment table
		.createTable("appointment_historic", (table) => {
			table.increments();
			table.integer("appointment_id").unsigned().references("id").inTable("appointment");
			table.date("date");
			table.specificType("start_hour", "time");
			table.specificType("end_hour", "time");
			table.integer("doctor_id").unsigned().references("id").inTable("doctor");
			table.integer("specialty_id").unsigned().references("id").inTable("specialty");
			table.string("status").notNullable();
			table.text("observations");
			table.timestamps(true, true);
		})

		// Complaint table
		.createTable("complaint", (table) => {
			table.increments();
			table.integer("user_id").unsigned();
			table.integer("appointment_id").unsigned().references("id").inTable("appointment");
			table.text("observation");
			table.text("compaint_answer");
			table.string("answered_by");
			table.string("status").notNullable();
			table.timestamps(true, true);
		})

		// Attachement table
		.createTable("appointment_attachment", (table) => {
			table.increments();
			table.integer("appointment_id").unsigned().references("id").inTable("appointment");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Attachement Complaint table
		.createTable("complaint_attachment", (table) => {
			table.increments();
			table.integer("complaint_id").unsigned().references("id").inTable("complaint");
			table.integer("file_id").unsigned();
			table.timestamps(true, true);
		})

		// Configuration table
		.createTable("configuration", (table) => {
			table.increments();
			table.string("key", 120).notNullable();
			table.string("value", 250).notNullable();
			table.timestamps(true, true);
			table.unique("key");
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("appointment_type")
		.dropTable("appointment_type_translation")
		.dropTable("place")
		.dropTable("specialty")
		.dropTable("specialty_translation")
		.dropTable("tariff")
		.dropTable("price_variation")
		.dropTable("profile_user")
		.dropTable("specialty_appointment_type")
		.dropTable("specialty_checklist")
		.dropTable("doctor")
		.dropTable("doctor_external")
		.dropTable("doctor_external_attachment")
		.dropTable("doctor_attachment")
		.dropTable("doctor_specialty")
		.dropTable("schedule")
		.dropTable("period")
		.dropTable("appointment")
		.dropTable("attendance")
		.dropTable("attendance_checklist")
		.dropTable("checklist_attachment")
		.dropTable("checklist_answers")
		.dropTable("attendance_attachment")
		.dropTable("appointment_historic")
		.dropTable("complaint")
		.dropTable("appointment_attachment")
		.dropTable("complaint_attachment")
		.dropTable("appointment_attendance")
		.dropTable("configuration");

module.exports.configuration = {
	transaction: true,
};
