module.exports.seed = (knex) => {
	return Promise.all([
		knex("appointment_type")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					await knex("appointment_type").insert({
						id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});

					await knex("appointment_type_translation").insert({
						appointment_type_id: 1,
						language_id: 3,
						name: "Presencial",
					});
					await knex("appointment_type_translation").insert({
						appointment_type_id: 1,
						language_id: 4,
						name: "Presential",
					});

					await knex("appointment_type").insert({
						id: 2,
						created_at: new Date(),
						updated_at: new Date(),
					});

					await knex("appointment_type_translation").insert({
						appointment_type_id: 2,
						language_id: 3,
						name: "WEB",
					});
					await knex("appointment_type_translation").insert({
						appointment_type_id: 2,
						language_id: 4,
						name: "WEB",
					});

					return true;
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};