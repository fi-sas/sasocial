module.exports.seed = (knex) => {
	return Promise.all([
		knex("place")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					await knex("place").insert({
						id: 1,
						name: "TESTE PLACE",
						email: "PLACE@EMAIL.COM",
						address: "PLACE ADDRESS",
						postal_code: "1111-111",
						city: "PLACE CITY",
						phone: 123456789,
						fax: 123456789,
						is_active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});

					return true;
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
