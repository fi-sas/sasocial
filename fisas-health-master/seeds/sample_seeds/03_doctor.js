module.exports.seed = (knex) => {

	const data = [
		{
			id: 1,
			user_id: 1,
			allow_historic: true,
			is_external: false,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			id: 2,
			user_id: 2,
			allow_historic: true,
			is_external: false,
			created_at: new Date(),
			updated_at: new Date(),
		}
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("doctor").select().where({
				id: d.id,
			});

			if (rows.length === 0) {
				// no matching records found
				await knex("doctor").insert(d);
			}
			else {
				await knex("doctor").update(d).where({
					id: d.id
				});
			}
		}),
	);
};