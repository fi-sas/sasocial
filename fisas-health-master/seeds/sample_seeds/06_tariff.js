module.exports.seed = (knex) => {

	const data = [
		{
			id: 1,
			description: "Aluno Bolseiro",
			is_scolarship: true,
		},
		{
			id: 2,
			description: "Aluno Não Bolseiro",
			is_scolarship: false,
		},
		{
			id: 3,
			description: "Funcionário",
			is_scolarship: false,
		},
		{
			id: 4,
			description: "Func. Escalão 2000 e 4000",
			is_scolarship: false,
		}
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("tariff").select().where({
				id: d.id,
			});

			if (rows.length === 0) {
				// no matching records found
				await knex("tariff").insert(d);
			}
			else {
				await knex("tariff").update(d).where({
					id: d.id
				});
			}
		}),
	);
};