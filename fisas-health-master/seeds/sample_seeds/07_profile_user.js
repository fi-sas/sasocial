module.exports.seed = (knex) => {

	const data = [
		{
			id: 1,
			tariff_id: 1,
			profile_id: 1,
		},
		{
			id: 2,
			tariff_id: 2,
			profile_id: 1,
		},
		{
			id: 3,
			tariff_id: 3,
			profile_id: 3,
		},
		{
			id: 4,
			tariff_id: 3,
			profile_id: 6,
		},
		{
			id: 5,
			tariff_id: 4,
			profile_id: 3,
		},
		{
			id: 6,
			tariff_id: 4,
			profile_id: 6,
		}
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("profile_user").select().where({
				id: d.id,
			});

			if (rows.length === 0) {
				// no matching records found
				await knex("profile_user").insert(d);
			}
			else {
				await knex("profile_user").update(d).where({
					id: d.id
				});
			}
		}),
	);
};