exports.seed = (knex) => {
	const data = [
		{
			key: "EXTERNAL_PROFILE",
			value: "Externo Saude",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "CURRENT_ACCOUNT_ID",
			value: 1,
			created_at: new Date(),
			updated_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("configuration").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("configuration").insert(d);
			}
			return true;
		}),
	);
};
