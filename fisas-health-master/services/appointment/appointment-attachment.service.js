/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.appointment-attachment",
	table: "appointment_attachment",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "appointment-attachment")], // name of the service.

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "appointment_id", "file_id", "created_at", "updated_at"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			appointment_id: { type: "number", positive: true, integer: true },
			file_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		get_files: {
			params: {
				appointment_id: {
					type: "number",
					convert: true,
				},
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						appointment_id: ctx.params.appointment_id,
					},
				}).then((response) => {
					return ctx.call("health.appointment-attachment.get", {
						withRelated: null,
						id: response.map((r) => r.id),
					});
				});
			},
		},
		save_files: {
			params: {
				appointment_id: { type: "number", positive: true, integer: true, convert: true },
				attachments_ids: {
					type: "array",
					item: {
						file_id: { type: "number", positive: true, integer: true, convert: true },
					},
				},
			},
			handler(ctx) {
				this.clearCache();
				const entities = ctx.params.attachments_ids.map((file_id) => ({
					appointment_id: ctx.params.appointment_id,
					file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
