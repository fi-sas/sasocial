/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.appointment-historic",
	table: "appointment_historic",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "appointment-historic")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"appointment_id",
			"date",
			"start_hour",
			"end_hour",
			"doctor_id",
			"specialty_id",
			"status",
			"observations",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			appointment_id: { type: "number", positive: true, integer: true },
			date: { type: "date", optional: true },
			start_hour: { type: "string", optional: true },
			end_hour: { type: "string", optional: true },
			doctor_id: { type: "number", positive: true, integer: true, optional: true },
			specialty_id: { type: "number", positive: true, integer: true, optional: true },
			status: { type: "string" },
			observations: { type: "string", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
