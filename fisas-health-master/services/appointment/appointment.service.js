"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { GENDER, APPOINTMENT_STATUS, CONFIGURATION_KEYS } = require("../utils/constants");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
const _ = require("lodash");
const ics = require("ics");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.appointment",
	table: "appointment",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "appointment")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_id",
			"corporate_id",
			"student_number",
			"name",
			"gender",
			"email",
			"mobile_phone_number",
			"telephone_number",
			"document_type_id",
			"identification_number",
			"nif",
			"niss",
			"address",
			"postal_code",
			"city",
			"nationality",
			"course_id",
			"course_year",
			"department_id",
			"organic_unit_id",
			"specialty_id",
			"date",
			"start_hour",
			"end_hour",
			"has_scholarship",
			"notes",
			"allow_historic",
			"appointment_type_id",
			"payment_type",
			"price",
			"tariff_id",
			"doctor_id",
			"overlapAppointment",
			"file_certificate_id",
			"status",
		],
		defaultWithRelateds: [
			"attachments_ids",
			"specialty",
			"doctor",
			"appointment_type",
			"course",
			"organicUnit",
			"documentType",
		],
		withRelateds: {
			attachments_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.appointment-attachment.get_files", {
								appointment_id: doc.id,
							})
							.then((res) => {
								doc.attachments_ids = res;
							});
					}),
				);
			},
			specialty(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "health.specialty", "specialty", "specialty_id");
			},
			doctor(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "health.doctor", "doctor", "doctor_id");
			},
			appointment_type(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.appointment-type.get", {
								id: doc.appointment_type_id,
							})
							.then((res) => {
								doc.appointment_type = res[0];
							});
					}),
				);
			},
			course(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						if (doc.course_id) {
							return ctx
								.call("configuration.courses.get", {
									id: doc.course_id,
								})
								.then((res) => {
									doc.course = res[0];
								});
						}
					}),
				);
			},
			organicUnit(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						if (doc.organic_unit_id) {
							return ctx
								.call("infrastructure.organic-units.get", {
									id: doc.organic_unit_id,
								})
								.then((res) => {
									doc.organicUnit = res[0];
								});
						}
					}),
				);
			},
			documentType(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						if (doc.document_type_id) {
							return ctx
								.call("authorization.document-types.get", {
									id: doc.document_type_id,
								})
								.then((res) => {
									doc.documentType = res[0];
								});
						}
					}),
				);
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			corporate_id: { type: "number", positive: true, integer: true, optional: true },
			student_number: { type: "string", optional: true },
			name: { type: "string", optional: true },
			gender: { type: "enum", values: Object.keys(GENDER), optional: true },
			email: { type: "string", optional: true },
			mobile_phone_number: { type: "string", optional: true },
			telephone_number: { type: "string", optional: true },
			document_type_id: { type: "number", positive: true, integer: true, optional: true },
			identification_number: { type: "string", optional: true },
			nif: { type: "string", optional: true },
			niss: { type: "string", optional: true },
			address: { type: "string", optional: true },
			postal_code: { type: "string", optional: true },
			city: { type: "string", optional: true },
			nationality: { type: "string", optional: true },
			course_id: { type: "number", positive: true, integer: true, optional: true },
			course_year: { type: "number", positive: true, integer: true, optional: true },
			department_id: { type: "number", positive: true, integer: true, optional: true },
			organic_unit_id: { type: "number", positive: true, integer: true, optional: true },
			specialty_id: { type: "number", positive: true, integer: true, optional: true },
			date: { type: "date", convert: true, optional: true },
			start_hour: { type: "string", optional: true },
			end_hour: { type: "string", optional: true },
			has_scholarship: { type: "boolean", optional: true },
			notes: { type: "string", optional: true },
			allow_historic: { type: "boolean", optional: true },
			overlapAppointment: { type: "boolean", optional: true },
			appointment_type_id: { type: "number", positive: true, integer: true, optional: true },
			tariff_id: { type: "number", positive: true, integer: true, optional: true },
			doctor_id: { type: "number", positive: true, integer: true, optional: true },
			status: { type: "enum", values: Object.keys(APPOINTMENT_STATUS), optional: true },
			attachments_ids: {
				type: "array",
				optional: true,
				item: {
					file_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
		},
	},
	hooks: {
		before: {
			create: [
				"validateUser",
				"checkIfAppointmentIsAvailable",
				"validateAppointmentType",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					if (ctx.params.price !== 0) {
						ctx.params.status = "PENDING";
					} else {
						const specialty = await ctx.call("health.specialty.get", {
							id: ctx.params.specialty_id,
						});
						if (specialty && specialty.length && !specialty[0].is_external) {
							ctx.params.status = await this.getStatusAppointment(ctx, ctx.params.specialty_id);
						}
					}
				},
			],
			update: [
				"validateUser",
				"validateAppointmentType",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.status = await this.getStatusAppointment(ctx, ctx.params.specialty_id);
				},
			],
			patch: [
				"validateUser",
				"validateAppointmentType",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: [
				"saveAttachments",
				async function saveHistoric(ctx, response) {
					if (response) {
						return this.saveHistoric(ctx, response[0], "CREATED");
					}
				},
				async function validadeCurrentAccount(ctx, response) {
					if (response && response.length) {
						if (response[0].price !== 0) {
							switch (response[0].payment_type) {
								case "Pré Pagamento":
									this.createAppointmentPayment(
										ctx,
										response[0],
										"health.appointment.confirm_appointment",
									);
									break;
								case "Ato Consulta":
									ctx.call("health.appointment.patch", {
										id: response[0].id,
										status: await this.getStatusAppointment(ctx, response[0].specialty_id),
									});
									break;
							}
						} else {
							return ctx.call("health.appointment.patch", {
								id: response[0].id,
								status: await this.getStatusAppointment(ctx, response[0].specialty_id),
							});
						}

						/*let notificationKey;
						const specialtyInfo = await ctx.call("health.specialty.get", {
							id: response[0].specialty_id,
						});
						switch (response[0].status) {
							case "PENDING":
								notificationKey = "HEALTH_APPOINTMENT_STATUS_PENDING";
								break;
							case "APPROVED":
								notificationKey = "HEALTH_APPOINTMENT_STATUS_APPROVED";
								break;
						}
						let appointment = {};
						appointment.name = response[0].name;
						appointment.specialty_name = specialtyInfo[0].translations.find(
							(x) => x.language_id == 3,
						)
							? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
							: "";
						appointment.date = moment(response[0].date).format("YYYY-MM-DD");
						appointment.start_hour = moment(response[0].start_hour, "HH:mm");
						ctx.call("notifications.alerts.create_alert", {
							alert_type_key: notificationKey,
							user_id: response[0].user_id,
							user_data: {},
							data: {
								appointment: appointment,
							},
							variables: {},
							external_uuid: null,
						});
						return response;*/
					}
				},
			],
			list: [
				function filterAppointments(ctx, response) {
					return response.rows.filter((r) => r.status !== "CHANGED");
				},
			],
			update: [
				"saveAttachments",
				async function saveHistoric(ctx, response) {
					if (response) {
						return this.saveHistoric(ctx, response[0], response[0].status);
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		change_appointment: {
			rest: "POST /:id/change",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				date: { type: "date", convert: true },
				start_hour: { type: "string" },
				end_hour: { type: "string" },
				observations: { type: "string", optional: true },
				overlapAppointment: { type: "boolean", default: false },
			},
			handler(ctx) {
				let appointmentObj = null;
				let specialtyObj = null;
				return this._get(ctx, { id: ctx.params.id })
					.then(async (appointment) => {
						if (appointment.length > 0 && appointment[0].status === "CLOSED") {
							throw new Errors.ValidationError("Appointment closed!", "HEALTH_APPOINTMENT_CLOSED");
						}
						if (
							ctx.params.date &&
							moment(ctx.params.date).isBefore(moment(new Date()).format("YYYY-MM-DD"))
						) {
							throw new Errors.ValidationError(
								"Appointment Date less than today!",
								"HEALTH_APPOINTMENT_DATE_LESS_TODAY",
							);
						}

						specialtyObj = await ctx
							.call("health.specialty.get", { id: appointment[0].specialty_id })
							.then((specialty) => specialty[0]);
						if (
							await this.checkIfDayIsAnHoliday(ctx, ctx.params.date, specialtyObj.organic_unit_id)
						) {
							throw new Errors.ValidationError(
								"Appointment Date match as an holiday!",
								"HEALTH_APPOINTMENT_DATE_HOLIDAY",
							);
						}
						appointmentObj = appointment[0];
						return ctx.call("health.doctor-specialty.find", {
							query: {
								doctor_id: appointment[0].doctor_id,
								specialty_id: appointment[0].specialty_id,
							},
						});
					})
					.then((doctor_specialty) => {
						if (doctor_specialty.length > 0) {
							return ctx.call("health.period.find", {
								query: {
									doctor_specialty_id: doctor_specialty[0].id,
								},
							});
						}
					})
					.then(async (periods) => {
						if (!ctx.params.overlapAppointment) {
							if (
								await !this.checkIfPeriodExists(
									ctx,
									periods,
									ctx.params.date,
									ctx.params.start_hour,
									ctx.params.end_hour,
								)
							) {
								throw new Errors.ValidationError(
									"Schedule not Available",
									"HEALTH_SCHEDULE_NOT_AVAILABLE",
								);
							}
						}
						if (
							await !this.checkIfUserHaveAnotherAppointments(
								ctx,
								ctx.params.date,
								ctx.params.start_hour,
								ctx.params.end_hour,
								appointmentObj.user_id,
							)
						) {
							throw new Errors.ValidationError(
								"The User have anoter appointment in the same schedule",
								"HEALTH_USER_HAVE_ANOTHER_APPOITMENT",
							);
						}

						return ctx.call("health.appointment-historic.create", {
							appointment_id: appointmentObj.id,
							date: appointmentObj.date,
							start_hour: appointmentObj.start_hour,
							end_hour: appointmentObj.end_hour,
							doctor_id: ctx.params.doctor_id ? ctx.params.doctor_id : appointmentObj.doctor_id,
							specialty_id: ctx.params.specialty_id
								? ctx.params.specialty_id
								: appointmentObj.specialty_id,
							status: "CHANGED",
							observations: ctx.params.observations,
						});
					})
					.then((historic) => {
						return ctx
							.call("health.appointment.patch", {
								id: ctx.params.id,
								date: ctx.params.date,
								start_hour: ctx.params.start_hour,
								end_hour: ctx.params.end_hour,
								doctor_id: ctx.params.doctor_id ? ctx.params.doctor_id : appointmentObj.doctor_id,
								specialty_id: ctx.params.specialty_id
									? ctx.params.specialty_id
									: appointmentObj.specialty_id,
							})
							.then(() => {
								const appointmentNotification = {};
								appointmentNotification.name = appointmentObj.name;
								appointmentNotification.specialty_name = specialtyObj.translations.find(
									(x) => x.language_id == 3,
								)
									? specialtyObj.translations.find((x) => x.language_id == 3).name
									: "";
								appointmentNotification.date = moment(historic[0].date).format("YYYY-MM-DD");
								appointmentNotification.start_hour = moment(historic[0].start_hour, "HH:mm");
								appointmentNotification.new_date = moment(appointmentObj.date).format("YYYY-MM-DD");
								appointmentNotification.new_start_hour = moment(appointmentObj.start_hour, "HH:mm");
								ctx.call("notifications.alerts.create_alert", {
									alert_type_key: "HEALTH_APPOINTMENT_STATUS_CHANGED",
									user_id: appointmentObj.user_id,
									user_data: {},
									data: {
										appointment: appointmentNotification,
									},
									variables: {},
									external_uuid: null,
								});
							});
					});
			},
		},
		create_complaint: {
			rest: "POST /:id/complaint",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				observation: { type: "string", optional: false },
				files_ids: {
					type: "array",
					item: {
						file_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
						},
					},
				},
			},
			handler(ctx) {
				return ctx
					.call("health.appointment.get", { id: ctx.params.id })
					.then((response) => {
						if (response.length > 0) {
							switch (response[0].status) {
								case "CANCELED":
									throw new Errors.ValidationError(
										"Appointment cancelled!",
										"HEALTH_APPOINTMENT_CANCELED",
									);
								case "STARTED":
								case "PENDING":
								case "APPROVED":
									throw new Errors.ValidationError(
										"Appointment not closed!",
										"HEALTH_APPOINTMENT_NOT_CLOSED",
									);
							}
						}
						let newComplaint = {};
						newComplaint.user_id = response[0].user_id;
						newComplaint.appointment_id = ctx.params.id;
						newComplaint.observation = ctx.params.observation;
						newComplaint.attachments_ids = ctx.params.files_ids;
						return ctx.call("health.complaint.create_complaint", newComplaint);
					})
					.then(async (complaint) => {
						if (complaint.length > 0) {
							const appointmentInfo = await this._get(ctx, { id: ctx.params.id });
							const specialtyInfo = await ctx.call("health.specialty.get", {
								id: appointmentInfo[0].specialty_id,
							});
							let appointment = {};
							appointment.name = appointmentInfo[0].name;
							appointment.specialty_name = specialtyInfo[0].translations.find(
								(x) => x.language_id == 3,
							)
								? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
								: "";
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "HEALTH_STATUS_COMPLAIN_SUBMITED",
								user_id: appointmentInfo[0].user_id,
								user_data: {},
								data: {
									appointment: appointment,
								},
								variables: {},
								external_uuid: null,
							});
						}
						return complaint;
					});
			},
		},
		read_complaint: {
			rest: "GET /:id/complaint",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return ctx.call("health.complaint.find", {
					query: {
						appointment_id: ctx.params.id,
					},
				});
			},
		},
		answer_complaint: {
			rest: "POST /:id/answer-complaint",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				complaint_answer: { type: "string", optional: false },
			},
			handler(ctx) {
				let answer = {};
				answer.appointment_id = ctx.params.id;
				answer.complaint_answer = ctx.params.complaint_answer;
				return ctx.call("health.complaint.answer_complaint", answer).then(async (complaint) => {
					const appointmentInfo = await this._get(ctx, { id: ctx.params.id });
					const specialtyInfo = await ctx.call("health.specialty.get", {
						id: appointmentInfo[0].specialty_id,
					});
					let appointment = {};
					appointment.name = appointmentInfo[0].name;
					appointment.specialty_name = specialtyInfo[0].translations.find((x) => x.language_id == 3)
						? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
						: "";
					ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "HEALTH_STATUS_COMPLAIN_ANSWERED",
						user_id: appointmentInfo[0].user_id,
						user_data: {},
						data: {
							appointment: appointment,
						},
						variables: {},
						external_uuid: null,
					});
					return complaint;
				});
			},
		},
		attendance_certificate: {
			rest: "GET /:id/certificate",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then(async (appointment) => {
					if (appointment[0] && appointment[0].status !== "CLOSED") {
						throw new Errors.ValidationError(
							"Appointment not closed!",
							"HEALTH_APPOINTMENT_NOT_CLOSED",
						);
					}
					if (appointment[0].file_certificate_id) {
						return ctx.call("media.files.get", { id: appointment[0].file_certificate_id });
					}
					const specialtyInfo = await ctx.call("health.specialty.get", {
						id: appointment[0].specialty_id,
					});
					let attendaceCert = {};
					attendaceCert.name = appointment[0].name;
					attendaceCert.niss = appointment[0].niss;
					attendaceCert.date = moment(appointment[0].date).format("YYYY-MM-DD");
					attendaceCert.start_hour = appointment[0].start_hour;
					attendaceCert.end_hour = appointment[0].end_hour;
					attendaceCert.specialty_name = specialtyInfo[0].translations.find(
						(x) => x.language_id == 3,
					)
						? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
						: "";
					attendaceCert.specialty_place = specialtyInfo[0].place.name;
					attendaceCert.certificate_date = moment(new Date()).format("YYYY-MM-DD");

					return ctx
						.call("reports.templates.print", {
							key: "HEALTH_ATTENDANCE_DECLARATION",
							options: {
								convertTo: "pdf",
							},
							data: {
								...attendaceCert,
							},
						})
						.then((response) => {
							return ctx.call("health.appointment.patch", {
								id: ctx.params.id,
								file_certificate_id: response[0].id,
							});
						})
						.then((certificate) => {
							return ctx.call("media.files.get", { id: certificate[0].file_certificate_id });
						});
				});
			},
		},
		close_attendance: {
			rest: "POST /:id/close-attendance",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				observations: { type: "string", optional: true },
				date: { type: "date", convert: true },
				start_hour: { type: "string" },
				end_hour: { type: "string" },
				attachments_ids: { type: "array", optional: true },
				checklist_data: { type: "object", optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id })
					.then((response) => {
						if (response.length > 0) {
							switch (response[0].status) {
								case "CLOSED":
									throw new Errors.ValidationError(
										"Appointment closed!",
										"HEALTH_APPOINTMENT_CLOSED",
									);
								case "PENDING":
									throw new Errors.ValidationError(
										"Appointment pending for approval!",
										"HEALTH_APPOINTMENT_PENDING",
									);
								case "CHANGED":
									throw new Errors.ValidationError(
										"Appointment not available!",
										"HEALTH_APPOINTMENT_CHANGED",
									);
								case "STARTED":
									return ctx.call("health.attendance.close_attendance", { attendance: ctx.params });
							}
						}
					})
					.then(async () => {
						return this._get(ctx, { id: ctx.params.id }).then(async (appointment) => {
							if (appointment.length) {
								if (
									appointment[0].payment_type === "Ato Consulta" &&
									appointment[0].price.price !== 0
								) {
									return this.createAppointmentPayment(
										ctx,
										appointment[0],
										"health.appointment.close_appointment",
									);
								} else {
									return ctx.call("health.appointment.patch", {
										id: appointment[0].id,
										status: "CLOSED",
									});
								}
							}
						});
					})
					.then(async () => {
						this._get(ctx, { id: ctx.params.id }).then(async (appointment_res) => {
							const specialtyInfo = await ctx.call("health.specialty.get", {
								id: appointment_res[0].specialty_id,
							});
							let appointment = {};
							appointment.name = appointment_res[0].name;
							appointment.specialty_name = specialtyInfo[0].translations.find(
								(x) => x.language_id == 3,
							)
								? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
								: "";
							appointment.date = moment(appointment_res[0].date).format("YYYY-MM-DD");
							appointment.start_hour = moment(appointment_res[0].start_hour, "HH:mm");
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "HEALTH_APPOINTMENT_STATUS_CLOSED",
								user_id: appointment_res[0].user_id,
								user_data: {},
								data: {
									appointment: appointment,
								},
								variables: {},
								external_uuid: null,
							});
							return this.saveHistoric(ctx, appointment_res[0], "CLOSED");
						});
					});
			},
		},
		start_attendance: {
			rest: "GET /:id/start-attendance",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then(async (appointment) => {
					if (appointment.length > 0) {
						switch (appointment[0].status) {
							case "CLOSED":
								throw new Errors.ValidationError(
									"Appointment closed!",
									"HEALTH_APPOINTMENT_CLOSED",
								);
							case "PENDING":
								throw new Errors.ValidationError(
									"Appointment pending for approval!",
									"HEALTH_APPOINTMENT_PENDING",
								);
							case "CHANGED":
								throw new Errors.ValidationError(
									"Appointment not available!",
									"HEALTH_APPOINTMENT_CHANGED",
								);
							case "APPROVED":
								return ctx
									.call("health.appointment.patch", {
										id: appointment[0].id,
										status: "STARTED",
									})
									.then((response) => {
										return ctx.call("health.attendance.start_attendance", {
											appointment: response[0],
										});
									});
						}
					}
				});
			},
		},
		get_attendance_information: {
			rest: "GET /:id/attendance-information",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then((appointment) => {
					return ctx.call("health.attendance.getAttendanceInformation", {
						appointment: appointment[0],
					});
				});
			},
		},
		cancel_appointment: {
			rest: "POST /:id/cancel",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id })
					.then((response) => {
						if (response.length > 0 && response[0].status === "CLOSED") {
							throw new Errors.ValidationError("Appointment closed!", "HEALTH_APPOINTMENT_CLOSED");
						}
						return ctx.call("health.appointment.patch", { id: ctx.params.id, status: "CANCELED" });
					})
					.then(async (appointment_res) => {
						if (appointment_res.length > 0) {
							const specialtyInfo = await ctx.call("health.specialty.get", {
								id: appointment_res[0].specialty_id,
							});
							let appointment = {};
							appointment.name = appointment_res[0].name;
							appointment.specialty_name = specialtyInfo[0].translations.find(
								(x) => x.language_id == 3,
							)
								? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
								: "";
							appointment.date = moment(appointment_res[0].date).format("YYYY-MM-DD");
							appointment.start_hour = moment(appointment_res[0].start_hour, "HH:mm");
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "HEALTH_APPOINTMENT_STATUS_CANCELLED",
								user_id: appointment_res[0].user_id,
								user_data: {},
								data: {
									appointment: appointment,
								},
								variables: {},
								external_uuid: null,
							});
							return this.saveHistoric(ctx, appointment_res[0], "CANCELED");
						}
					});
			},
		},
		get_historic_by_appointment: {
			rest: "GET /:id/historic",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then(async (response) => {
					const obj = {};
					obj.appointment = response[0];
					obj.historic = await this.getHistoric(ctx, response[0].id);
					return obj;
				});
			},
		},
		approve_request: {
			rest: "POST /:id/approve-request",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id })
					.then((response) => {
						if (response.length > 0) {
							if (response[0].status === "CLOSED") {
								throw new Errors.ValidationError(
									"Appointment closed!",
									"HEALTH_APPOINTMENT_CLOSED",
								);
							}
							return ctx
								.call("health.specialty.get", { id: response[0].specialty_id })
								.then((specialty) => {
									if (
										specialty.length > 0 &&
										specialty[0].is_external &&
										response[0].status === "PENDING"
									) {
										return ctx.call("health.appointment.patch", {
											id: ctx.params.id,
											status: "APPROVED",
										});
									}
								});
						}
					})
					.then(async (appointment_res) => {
						if (appointment_res && appointment_res.length > 0) {
							const specialtyInfo = await ctx.call("health.specialty.get", {
								id: appointment_res[0].specialty_id,
							});
							let appointment = {};
							appointment.name = appointment_res[0].name;
							appointment.specialty_name = specialtyInfo[0].translations.find(
								(x) => x.language_id == 3,
							)
								? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
								: "";
							appointment.date = moment(appointment_res[0].date).format("YYYY-MM-DD");
							appointment.start_hour = moment(appointment_res[0].start_hour, "HH:mm");
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "HEALTH_APPOINTMENT_STATUS_APPROVED",
								user_id: appointment_res[0].user_id,
								user_data: {},
								data: {
									appointment: appointment,
								},
								variables: {},
								external_uuid: null,
							});
							return this.saveHistoric(ctx, appointment_res[0], "APPROVED");
						}
					});
			},
		},
		absence_appointment: {
			rest: "POST /:id/absence",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id })
					.then((response) => {
						if (response.length > 0 && response[0].status === "CLOSED") {
							throw new Errors.ValidationError("Appointment closed!", "HEALTH_APPOINTMENT_CLOSED");
						}
						if (
							moment(response[0].date, "YYYY-MM-DD").isAfter(
								moment(new Date()).format("YYYY-MM-DD"),
							)
						) {
							throw new Errors.ValidationError(
								"Appointment is not in the date!",
								"HEALTH_APPOINTMENT_DIFFERENT_DATE",
							);
						}
						return ctx.call("health.appointment.patch", { id: ctx.params.id, status: "MISSED" });
					})
					.then(async (appointment_res) => {
						if (appointment_res.length > 0) {
							const specialtyInfo = await ctx.call("health.specialty.get", {
								id: appointment_res[0].specialty_id,
							});
							let appointment = {};
							appointment.name = appointment_res[0].name;
							appointment.specialty_name = specialtyInfo[0].translations.find(
								(x) => x.language_id == 3,
							)
								? specialtyInfo[0].translations.find((x) => x.language_id == 3).name
								: "";
							appointment.date = moment(appointment_res[0].date).format("YYYY-MM-DD");
							appointment.start_hour = moment(appointment_res[0].start_hour, "HH:mm");
							ctx.call("notifications.alerts.create_alert", {
								alert_type_key: "HEALTH_APPOINTMENT_STATUS_ABSENCE",
								user_id: appointment_res[0].user_id,
								user_data: {},
								data: {
									appointment: appointment,
								},
								variables: {},
								external_uuid: null,
							});
							return this.saveHistoric(ctx, appointment_res[0], "MISSED");
						}
					});
			},
		},
		get_historic_by_user: {
			rest: "GET /historic",
			visibility: "published",
			handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.meta.user.id,
					},
				}).then((response) => {
					const closedAppointments = response.filter(
						(r) => r.status !== "PENDING" && r.status !== "APPROVED" && r.status !== "STARTED",
					);
					const openAppointments = response
						.filter(
							(r) => r.status === "PENDING" || r.status === "APPROVED" || r.status === "STARTED",
						)
						.sort(
							(a, b) =>
								moment(moment(a.date).format("YYYY-MM-DD") + "T" + a.start_hour).valueOf() -
								moment(moment(b.date).format("YYYY-MM-DD") + "T" + b.start_hour).valueOf(),
						);
					return {
						closed: closedAppointments,
						open: openAppointments,
					};
				});
			},
		},
		get_appointments_by_doctor: {
			rest: "GET /doctor_appointments",
			visibility: "published",
			async handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				params = params.query || {};
				params.doctor_id = ctx.meta.user.id;
				if (_.has(ctx, "params.query.status")) {
					params.status = JSON.parse(ctx.params.query.status);
				}
				if (_.has(ctx, "params.query.date") && _.has(ctx, "params.query.date")) {
					params.date = ctx.params.query.date;
				}
				if (_.has(ctx, "params.query.specialties")) {
					params.specialty_id = JSON.parse(ctx.params.query.specialties);
					delete params.specialties;
				}
				return this._find(ctx, ctx.params).then(async (response) => {
					const olderNotClosedAppointments = response
						.filter(
							(r) =>
								(r.status === "PENDING" || r.status === "APPROVED" || r.status === "STARTED") &&
								moment(r.date, "YYYY-MM-DD").isBefore(moment(new Date()).format("YYYY-MM-DD")),
						)
						.sort(
							(a, b) =>
								moment(moment(a.date).format("YYYY-MM-DD") + "T" + a.start_hour).valueOf() -
								moment(moment(b.date).format("YYYY-MM-DD") + "T" + b.start_hour).valueOf(),
						);
					const closedAppointments = response
						.filter(
							(r) =>
								r.status !== "PENDING" &&
								r.status !== "APPROVED" &&
								r.status !== "STARTED" &&
								moment(r.date, "YYYY-MM-DD").isSameOrAfter(moment(new Date()).format("YYYY-MM-DD")),
						)
						.sort(
							(a, b) =>
								moment(moment(a.date).format("YYYY-MM-DD") + "T" + a.start_hour).valueOf() -
								moment(moment(b.date).format("YYYY-MM-DD") + "T" + b.start_hour).valueOf(),
						);
					const openAppointments = response
						.filter(
							(r) => r.status === "PENDING" || r.status === "APPROVED" || r.status === "STARTED",
						)
						.sort(
							(a, b) =>
								moment(moment(a.date).format("YYYY-MM-DD") + "T" + a.start_hour).valueOf() -
								moment(moment(b.date).format("YYYY-MM-DD") + "T" + b.start_hour).valueOf(),
						);

					for (const appointment of closedAppointments) {
						const attendanceInfo = await ctx
							.call("health.attendance.find", { query: { appointment_id: appointment.id } })
							.then((response) => {
								if (response.length > 0) {
									return {
										start_hour: response[0].start_hour,
										end_hour: response[0].end_hour,
									};
								}
							});
						appointment.attendance = attendanceInfo;
					}
					return {
						older_open: olderNotClosedAppointments,
						closed: closedAppointments,
						open: openAppointments,
					};
				});
			},
		},
		wp_dashboard: {
			rest: "GET /wp_dashboard",
			visibility: "published",
			handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.meta.user.id,
						status: ["PENDING", "APPROVED", "STARTED"],
					},
				}).then((response) => {
					return response.length
						? response.sort(
								(a, b) =>
									moment(moment(a.date).format("YYYY-MM-DD") + "T" + a.start_hour).valueOf() -
									moment(moment(b.date).format("YYYY-MM-DD") + "T" + b.start_hour).valueOf(),
						  )
						: response;
				});
			},
		},
		confirm_appointment: {
			visibility: "public",
			async handler(ctx) {
				if (ctx.params.items.length && ctx.params.items[0].extra_info) {
					const appointment_id = ctx.params.items[0].extra_info.appointment_id;
					this._get(ctx, { id: appointment_id }).then(async (appointment) => {
						if (appointment.length) {
							await ctx
								.call("health.specialty.get", { id: appointment[0].specialty_id })
								.then(async (specialty) => {
									if (specialty.length > 0) {
										if (specialty[0].is_external) {
											const allowedProfiles = await ctx
												.call("authorization.profiles.find", {
													query: {
														name: ["Funcionários", "Externo Saude"],
													},
												})
												.then((response) => {
													return response.map((res) => res.id);
												});
											if (allowedProfiles.includes(ctx.params.extra_info.profile_id)) {
												return "APPROVED";
											} else {
												return "PENDING";
											}
										} else if (!specialty[0].is_external) {
											return "APPROVED";
										}
									}
								})
								.then((status) => {
									if (status) {
										ctx.call("health.appointment.patch", {
											id: appointment_id,
											status: status,
										});
									}
								});
						}
					});
				}
			},
		},
		cancel_appointment2: {
			visibility: "published",
			handler(ctx) {
				if (ctx.params.items.length && ctx.params.items[0].extra_info) {
					const appointment_id = ctx.params.items[0].extra_info.appointment_id;
					return ctx.call("health.appointment.patch", {
						id: appointment_id,
						status: "CANCELED",
					});
				}
			},
		},
		close_appointment: {
			visibility: "public",
			async handler(ctx) {
				if (ctx.params.items.length && ctx.params.items[0].extra_info) {
					const appointment_id = ctx.params.items[0].extra_info.appointment_id;
					return ctx.call("health.appointment.patch", {
						id: appointment_id,
						status: "CLOSED",
					});
				}
			},
		},
		export_appointment_calendar: {
			rest: "GET /:id/export_calendar",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then((appointment) => {
					if (appointment.length) {
						const start =
							moment(appointment[0].date).format("YYYY-MM-DD") + "T" + appointment[0].start_hour;
						const start_formated = moment(start).format("YYYY-M-D-H-m").split("-");
						const end =
							moment(appointment[0].date).format("YYYY-MM-DD") + "T" + appointment[0].end_hour;
						const end_formated = moment(end).format("YYYY-M-D-H-m").split("-");
						const event = {
							start: start_formated.map((s) => parseInt(s)),
							end: end_formated.map((e) => parseInt(e)),
							title: "Appointment - " + appointment[0].specialty.translations[0].name,
							description: appointment[0].specialty.translations[0].name,
							location: appointment[0].specialty.place.city,
							categories: ["Appointment"],
							status: "CONFIRMED",
							organizer: { name: appointment[0].name, email: appointment[0].email },
							/*attendees: [
								{ name: 'Adam Gibbons', email: 'adam@example.com', rsvp: true, partstat: 'ACCEPTED', role: 'REQ-PARTICIPANT' },
								{ name: 'Brittany Seaton', email: 'brittany@example2.org', dir: 'https://linkedin.com/in/brittanyseaton', role: 'OPT-PARTICIPANT' }
							]*/
						};
						return ics.createEvent(event, (error, value) => {
							if (error) {
								return error;
							}

							/*let filename = `${appointment[0].specialty.translations[0].name}_${appointment[0].name}.ics`;
							ctx.meta.$responseType = "text/plain; charset=utf-8";
							ctx.meta.$responseHeaders = {
								"Content-Disposition": `attachment; filename="${filename}"`,
							};
							return ctx.meta;*/
							return value;
						});
					}
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"health.doctor.*"() {
			this.clearCache();
		},
		"health.specialty.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async saveAttachments(ctx, response) {
			if (ctx.params.attachments_ids && Array.isArray(ctx.params.attachments_ids)) {
				const attachments_ids = [...new Set(ctx.params.attachments_ids)];
				await ctx.call("health.appointment-attachment.save_files", {
					appointment_id: response[0].id,
					attachments_ids,
				});
				response[0].attachments_ids = await ctx.call("health.appointment-attachment.find", {
					query: {
						appointment_id: response[0].id,
					},
				});
			}
			return response;
		},

		validateUser(ctx) {
			if (ctx.params.user_id) {
				return ctx.call("authorization.users.get", {
					id: ctx.params.user_id,
				});
			}
		},

		async checkIfAppointmentIsAvailable(ctx) {
			if (
				ctx.params.date &&
				moment(ctx.params.date).isBefore(moment(new Date()).format("YYYY-MM-DD"))
			) {
				throw new Errors.ValidationError(
					"Appointment Date less than today!",
					"HEALTH_APPOINTMENT_DATE_LESS_TODAY",
				);
			}

			return ctx
				.call("health.specialty.get", { id: ctx.params.specialty_id })
				.then(async (specialty) => {
					if (
						specialty.length > 0 &&
						(await this.checkIfDayIsAnHoliday(ctx, ctx.params.date, specialty[0].organic_unit_id))
					) {
						throw new Errors.ValidationError(
							"Appointment Date match as an holiday!",
							"HEALTH_APPOINTMENT_DATE_HOLIDAY",
						);
					}
					return ctx.call("health.doctor-specialty.find", {
						query: {
							doctor_id: ctx.params.doctor_id,
							specialty_id: ctx.params.specialty_id,
						},
					});
				})
				.then((doctor_specialty) => {
					if (doctor_specialty.length > 0) {
						return ctx.call("health.period.find", {
							query: {
								doctor_specialty_id: doctor_specialty[0].id,
							},
						});
					}
				})
				.then(async (periods) => {
					if (!ctx.params.overlapAppointment) {
						const checkPeriods = await this.checkIfPeriodExists(
							ctx,
							periods,
							ctx.params.date,
							ctx.params.start_hour,
							ctx.params.end_hour,
						);
						if (!checkPeriods) {
							throw new Errors.ValidationError(
								"Schedule not Available",
								"HEALTH_SCHEDULE_NOT_AVAILABLE",
							);
						}
					}
					const checkAnotherAppointments = await this.checkIfUserHaveAnotherAppointments(
						ctx,
						ctx.params.date,
						ctx.params.start_hour,
						ctx.params.end_hour,
						ctx.params.user_id,
					);
					if (!checkAnotherAppointments) {
						throw new Errors.ValidationError(
							"The User have anoter appointment in the same schedule",
							"HEALTH_USER_HAVE_ANOTHER_APPOITMENT",
						);
					}
				});
		},

		async checkIfDayIsAnHoliday(ctx, date, organic_unit_id) {
			const holidays = await ctx
				.call("configuration.holidays.list", {})
				.then((holidays_response) => {
					return holidays_response.rows.filter((h) =>
						h.organic_units.length > 0 ? h.organic_units.find((o) => o.id === organic_unit_id) : h,
					);
				});
			const checkIfDayIsHoliday = holidays.filter((h) =>
				moment(h.date, "YYYY-MM-DD").isSame(moment(date)),
			);
			if (checkIfDayIsHoliday.length > 0) {
				return true;
			}
			return false;
		},

		async checkIfPeriodExists(ctx, periods, date, start_hour, end_hour) {
			let returnresponse = true;
			if (periods.length > 0) {
				const checkIfPeriodExists = periods.filter((p) =>
					moment(date, "YYYY-MM-DD").isBetween(
						moment(p.start_date, "YYYY-MM-DD"),
						moment(p.end_date, "YYYY-MM-DD"),
						null,
						"[]",
					),
				);
				if (checkIfPeriodExists.length > 0) {
					const checkIfDayExists = checkIfPeriodExists.filter((d) =>
						d.days.filter((h) =>
							h.days.find(
								(hour) =>
									moment(start_hour, "HH:mm:ss").isBetween(
										moment(hour.start_hour, "HH:mm:ss"),
										moment(hour.end_hour, "HH:mm:ss"),
										null,
										"[]",
									) &&
									moment(end_hour, "HH:mm:ss").isBetween(
										moment(hour.start_hour, "HH:mm:ss"),
										moment(hour.end_hour, "HH:mm:ss"),
										null,
										"[]",
									) &&
									moment().day(hour.day).day() === moment(date).day(),
							),
						),
					);
					if (checkIfDayExists.length === 0) {
						returnresponse = false;
					}
				}
			}
			return returnresponse;
		},

		async checkIfUserHaveAnotherAppointments(ctx, date, start_hour, end_hour, user_id) {
			return ctx
				.call("health.appointment.find", {
					query: {
						user_id: user_id,
						date: moment(date).format("YYYY-MM-DD"),
						status: ["APPROVED", "STARTED"],
					},
				})
				.then((appointments) => {
					let returnresponse = true;
					if (appointments.length > 0) {
						const checkIfExistsAppointments = appointments.filter(
							(appointment) =>
								moment(start_hour, "HH:mm:ss").isBetween(
									moment(appointment.start_hour, "HH:mm:ss"),
									moment(appointment.end_hour, "HH:mm:ss"),
									null,
									"[]",
								) &&
								moment(end_hour, "HH:mm:ss").isBetween(
									moment(appointment.start_hour, "HH:mm:ss"),
									moment(appointment.end_hour, "HH:mm:ss"),
									null,
									"[]",
								),
						);
						if (checkIfExistsAppointments.length > 0) {
							returnresponse = false;
						}
					}
					return returnresponse;
				});
		},

		validateAppointmentType(ctx) {
			if (ctx.params.appointment_type_id && ctx.params.specialty_id) {
				return ctx
					.call("health.specialty.get", { id: ctx.params.specialty_id, withRelateds: null })
					.then((response) => {
						if (response[0] && response[0].appointment_types) {
							const appointment_type = response[0].appointment_types.filter(
								(appType) => appType.id === ctx.params.appointment_type_id,
							);
							if (appointment_type.length === 0) {
								throw new Errors.ValidationError(
									"Appointment Type not Allowed",
									"HEALTH_APPOINTMENT_TYPE_NOT_ALLOWED",
								);
							}
						}
					});
			}
		},

		saveHistoric(ctx, appointment, status) {
			return ctx
				.call("health.appointment-historic.create", {
					appointment_id: appointment.id,
					date: appointment.date,
					start_hour: appointment.start_hour,
					end_hour: appointment.end_hour,
					doctor_id: appointment.doctor_id,
					specialty_id: appointment.specialty_id,
					status: status,
				})
				.then((response) => {
					return this._get(ctx, { id: response[0].appointment_id });
				});
		},

		async getHistoric(ctx, appointment_id) {
			return ctx.call("health.appointment-historic.find", {
				query: {
					appointment_id: appointment_id,
				},
			});
		},

		async getAttendancePrice(ctx) {
			return this._get(ctx, { id: ctx.params.id })
				.then((appointment) => {
					return ctx.call("health.price-variations.find", {
						query: {
							specialty_id: appointment[0].specialty_id,
							tariff_id: appointment[0].tariff_id,
						},
					});
				})
				.then((priceVariation) => {
					return (
						priceVariation[0].price + priceVariation[0].price * priceVariation[0].vat.tax_value
					);
				});
		},

		async getStatusAppointment(ctx, specialty_id) {
			let status = null;
			const specialty = await ctx.call("health.specialty.get", {
				id: specialty_id,
			});
			if (specialty.is_external) {
				const allowedProfiles = await ctx
					.call("authorization.profiles.find", {
						query: {
							name: ["Funcionários", "Externo Saude"],
						},
					})
					.then((response) => {
						return response.map((res) => res.id);
					});
				if (allowedProfiles.includes(ctx.meta.user.profile_id)) {
					status = "APPROVED";
				} else {
					status = "PENDING";
				}
				status = "PENDING";
			} else if (!specialty.is_external) {
				status = "APPROVED";
			}
			return status;
		},

		async createAppointmentPayment(ctx, appointment, confirm_path) {
			const price_variation = await ctx.call("health.price-variations.find", {
				query: {
					tariff_id: appointment.tariff_id,
				},
			});
			const specialty = await ctx.call("health.specialty.get", {
				id: appointment.specialty_id,
			});

			const current_account_id = await ctx
				.call("health.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.CURRENT_ACCOUNT_ID,
					},
				})
				.then((response) => response[0].value);

			const entity = [
				{
					service_id: 7,
					product_code: specialty[0].product_code,
					name: specialty[0].translations[0].name,
					description: specialty[0].translations[0].name,
					quantity: 1,
					vat_id: price_variation[0].vat_id,
					unit_value: price_variation[0].price,
					// price: bi.price,
					discount_value: 0,
					location: specialty[0].place.name,
					article_type: "health",
					extra_info: { appointment_id: appointment.id, profile_id: ctx.meta.user.profile_id },
					service_confirm_path: confirm_path,
					service_cancel_path: "health.appointment.cancel_appointment2",
				},
			];

			return ctx.call("current_account.movements.create", {
				account_id: current_account_id,
				operation: "INVOICE_RECEIPT",
				user_id: appointment.user_id,
				description: "Saúde",
				items: entity,
				bypass_balance_with_invoice: true,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
