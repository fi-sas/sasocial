/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.complaint",
	table: "complaint",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "complaint")], // name of the service.

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_id",
			"appointment_id",
			"observation",
			"compaint_answer",
			"answered_by",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["files", "appointment"],
		withRelateds: {
			files(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "health.complaint-attachment", "files", "id", "complaint_id");
			},
			appointment(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "health.appointment", "appointment", "appointment_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			appointment_id: { type: "number", positive: true, integer: true },
			observation: { type: "string", optional: true },
			complaint_answer: { type: "string", optional: true },
			answered_by: { type: "string", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "CREATED";
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create_complaint: {
			params: {
				user_id: { type: "number", positive: true, integer: true, convert: true },
				appointment_id: { type: "number", positive: true, integer: true, convert: true },
				observation: { type: "string" },
				attachments_ids: {
					type: "array",
					optional: true,
					item: {
						file_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
						},
					},
				},
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						appointment_id: ctx.params.appointment_id,
						status: "CREATED",
					},
				})
					.then((complaint) => {
						if (complaint.length > 0) {
							throw new Errors.ValidationError(
								"There are an open complaint",
								"HEALTH_COMPLAINT_NOT_CLOSED",
							);
						}
						const newComplaint = {};
						newComplaint.user_id = ctx.params.user_id;
						newComplaint.appointment_id = ctx.params.appointment_id;
						newComplaint.observation = ctx.params.observation;
						newComplaint.status = "CREATED";
						return this._insert(ctx, { entity: newComplaint });
					})
					.then((response) => {
						const attachments_ids = [...new Set(ctx.params.attachments_ids)];
						return ctx.call("health.complaint-attachment.save_files", {
							complaint_id: response[0].id,
							attachments_ids,
						});
					});
			},
		},
		answer_complaint: {
			params: {
				appointment_id: { type: "number", positive: true, integer: true, convert: true },
				complaint_answer: { type: "string" },
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						appointment_id: ctx.params.appointment_id,
						status: ["CREATED", "PENDING"],
					},
				}).then(async (complaint) => {
					if (complaint[0] && complaint[0].status !== "SOLVED") {
						return ctx.call("health.complaint.patch", {
							id: complaint[0].id,
							compaint_answer: ctx.params.complaint_answer,
							answered_by: await ctx
								.call("authorization.users.get", { id: ctx.meta.user.id })
								.then((user) => user[0].name),
							status: "SOLVED",
						});
					} else {
						throw new Errors.ValidationError("Complaint Closed!", "HEALTH_COMPLAINT_CLOSED");
					}
				});
			},
		},

		get_complaints_by_user: {
			rest: "GET /user-complaints",
			visibility: "published",
			handler(ctx) {
				return ctx
					.call("health.appointment.find", {
						query: {
							user_id: ctx.meta.user.id,
						},
					})
					.then((response) => {
						return this._find(ctx, {
							query: {
								appointment_id: response.map((r) => r.id),
							},
						});
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
