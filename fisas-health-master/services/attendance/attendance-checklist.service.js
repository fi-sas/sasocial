/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.attendance-checklist",
	table: "attendance_checklist",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "attendance-checklist")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "attendance_id", "notes", "created_at", "updated_at"],
		defaultWithRelateds: ["attachments_ids", "answers"],
		withRelateds: {
			attachments_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.checklist-attachment.get_files", {
								attendance_checklist_id: doc.id,
							})
							.then((res) => {
								doc.attachments_ids = res;
							});
					}),
				);
			},
			answers(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.checklist-answers.get_answers", {
								attendance_checklist_id: doc.id,
							})
							.then((res) => {
								doc.answers = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			attendance_id: { type: "number", positive: true, integer: true },
			notes: { type: "string", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_checklist: {
			params: {
				attendance_id: { type: "number", positive: true, integer: true, convert: true },
				checklist_data: {
					type: "object",
					optional: true,
					item: {
						notes: { type: "string" },
						attachments_ids: {
							type: "array",
							optional: true,
							item: {
								file_id: {
									type: "number",
									positive: true,
									integer: true,
									convert: true,
								},
							},
						},
						answers: {
							type: "array",
							optional: true,
							item: {
								question: { type: "string" },
								answer: { type: "string" },
							},
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					attendance_id: ctx.params.attendance_id,
				});
				const attendanceChecklist = {};
				attendanceChecklist.attendance_id = ctx.params.attendance_id;
				attendanceChecklist.notes = ctx.params.checklist_data.notes;
				return this._insert(ctx, { entity: attendanceChecklist }).then(async (response) => {
					const attachments_ids = [...new Set(ctx.params.checklist_data.attachments_ids)];
					const answers = [...new Set(ctx.params.checklist_data.answers)];
					await ctx.call("health.checklist-attachment.save_files", {
						attendance_checklist_id: response[0].id,
						attachments_ids,
					});
					await ctx.call("health.checklist-answers.save_answers", {
						attendance_checklist_id: response[0].id,
						answers,
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
