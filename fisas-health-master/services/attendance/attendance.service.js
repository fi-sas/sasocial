/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ATTENDANCE_STATUS } = require("../utils/constants");
const Errors = require("@fisas/ms_core").Helpers.Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.attendance",
	table: "attendance",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "attendance")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"appointment_id",
			"observations",
			"date",
			"start_hour",
			"end_hour",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["attachments_ids", "checklist_data"],
		withRelateds: {
			checklist_data(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.attendance-checklist.find", {
								query: {
									attendance_id: doc.id,
								},
							})
							.then((res) => {
								doc.checklist_data = res;
							});
					}),
				);
			},
			attachments_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.appointment-attachment.get_files", {
								appointment_id: doc.id,
							})
							.then((res) => {
								doc.attachments_ids = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			appointment_id: { type: "number", positive: true, integer: true },
			observations: { type: "string", optional: true },
			date: { type: "date", convert: true, optional: true },
			start_hour: { type: "string", optional: true },
			end_hour: { type: "string", optional: true },
			status: { type: "enum", values: Object.keys(ATTENDANCE_STATUS) },
			attachments_ids: {
				type: "array",
				optional: true,
				item: {
					file_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			checklist_data: { type: "object", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"checkAttendanceStatus",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"checkAttendanceStatus",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveAttachments", "saveChecklistData"],
			update: ["saveAttachments", "saveChecklistData"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		start_attendance: {
			params: {
				appointment: { type: "object" },
			},
			handler(ctx) {
				if (ctx.params.appointment) {
					return this._find(ctx, {
						query: {
							appointment_id: ctx.params.appointment.id,
						},
					}).then((response) => {
						if (response.length > 0) {
							throw new Errors.ValidationError(
								"Attendance Already Started",
								"HEALTH_APPOINTMENT_ALREADY_EXISTS",
							);
						}
						let attendanceObj = {};
						attendanceObj.appointment_id = ctx.params.appointment.id;
						attendanceObj.status = "STARTED";
						return this._insert(ctx, { entity: attendanceObj }).then(() => {
							return this.attendanceInformation(ctx, ctx.params.appointment);
						});
					});
				}
			},
		},

		close_attendance: {
			params: {
				attendance: { type: "object" },
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						appointment_id: ctx.params.attendance.id,
					},
				}).then((attendanceResponse) => {
					if (attendanceResponse.length > 0 && ctx.params.attendance) {
						if (attendanceResponse[0].status === "CLOSED") {
							throw new Errors.ValidationError(
								"Appointment Already Exists",
								"HEALTH_APPOINTMENT_ALREADY_EXISTS",
							);
						}
						return ctx
							.call("health.attendance.patch", {
								id: attendanceResponse[0].id,
								observations: ctx.params.attendance.observations,
								date: ctx.params.attendance.date,
								start_hour: ctx.params.attendance.start_hour,
								end_hour: ctx.params.attendance.end_hour,
								status: "CLOSED",
							})
							.then(async (attendance) => {
								const attachments_ids = [...new Set(ctx.params.attendance.attachments_ids)];
								await ctx.call("health.attendance-attachment.save_files", {
									attendance_id: attendance[0].id,
									attachments_ids,
								});
								await ctx.call("health.attendance-checklist.save_checklist", {
									attendance_id: attendance[0].id,
									checklist_data: ctx.params.attendance.checklist_data,
								});
							});
					}
				});
			},
		},

		getAttendanceInformation: {
			params: {
				appointment: { type: "object" },
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						appointment_id: ctx.params.appointment.id,
					},
				}).then((response) => {
					if (response.length > 0) {
						return this.attendanceInformation(ctx, ctx.params.appointment);
					}
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"health.appointment.*"() {
			this.clearCache();
		},
		"health.doctor.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		checkAttendanceStatus(ctx) {
			return ctx
				.call("health.attendance.find", {
					query: {
						appointment_id: ctx.params.appointment_id,
					},
				})
				.then((response) => {
					if (response[0] && response[0].status === "CLOSED") {
						throw new Errors.ValidationError("Attendance closed", "HEALTH_ATTENDANCE_CLOSED");
					}
				});
		},

		async saveAttachments(ctx, response) {
			if (ctx.params.attachments_ids && Array.isArray(ctx.params.attachments_ids)) {
				const attachments_ids = [...new Set(ctx.params.attachments_ids)];
				await ctx.call("health.attendance-attachment.save_files", {
					attendance_id: response[0].id,
					attachments_ids,
				});
				response[0].attachments_ids = await ctx.call("health.attendance-attachment.find", {
					query: {
						attendance_id: response[0].id,
					},
				});
				return response;
			}
		},

		async saveChecklistData(ctx, response) {
			if (ctx.params.checklist_data) {
				await ctx.call("health.attendance-checklist.save_checklist", {
					attendance_id: response[0].id,
					checklist_data: ctx.params.checklist_data,
				});
				response[0].checklist_data = await ctx.call("health.attendance-checklist.find", {
					query: {
						attendance_id: response[0].id,
					},
				});
				return response;
			}
		},

		async getUserAttendaceHistoric(ctx, appointment) {
			if (appointment.allow_historic) {
				return ctx
					.call("health.appointment.find", {
						query: {
							user_id: appointment.user_id,
							status: "CLOSED",
						},
					})
					.then(async (appointments) => {
						const responseHistory = [];
						for (const app of appointments) {
							await ctx
								.call("health.doctor.get", {
									id: app.doctor_id,
								})
								.then(async (doctor_historic) => {
									if (doctor_historic.length > 0 && doctor_historic[0].allow_historic) {
										const attendance = await this.getAttendanceById(ctx, app.id);
										if (attendance.length > 0) {
											responseHistory.push(...attendance);
										}
									}
								});
						}
						return responseHistory;
					});
			}
		},

		getAttendanceById(ctx, appointmentId) {
			return this._find(ctx, {
				query: {
					appointment_id: appointmentId,
				},
			});
		},

		async attendanceInformation(ctx, appointment) {
			let response = {};
			response.user = appointment;
			response.checklist = await ctx.call("health.specialty-checklist.checklist_by_specialty", {
				specialty_id: appointment.specialty_id,
			});
			response.historic = await this.getUserAttendaceHistoric(ctx, appointment);
			return response;
		},

		/*getPrice(ctx){
			let user_id;
			return ctx.call("health.appointment.get", { id: ctx.params.attendance.id })
				.then(appointment => {
					if(appointment.length > 0 ){
						user_id = appointment[0].user_id;
						return ctx.call("health.price-variations.find", {
							query:{
								specialty_id: appointment[0].specialty_id,
								tariff_id: ctx.params.attendance.tariff_id
							}
						});
					}
				}).then(async priceVariation => {
					if(priceVariation.length > 0){
						const usersProfile = await ctx.call("health.profile-user.find", { query : { tariff_id : priceVariation[0].tariff_id } })
							.then(response => {
								return response.map(x => x.profile_id);
							});
						const user = await ctx.call("authorization.users.get", { id: user_id });
						if(usersProfile.length > 0 && usersProfile.includes(user[0].profile_id)){
							return priceVariation[0].price + (priceVariation[0].price * priceVariation[0].vat.tax_value);
						}else{
							throw new Errors.ValidationError("Profile doesn't match!", "HEALTH_PROFILE_NOT_MATCH");
						}

					}
				});
		}*/
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
