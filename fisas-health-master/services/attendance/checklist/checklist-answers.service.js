/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.checklist-answers",
	table: "checklist_answers",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "checklist-answers")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "attendance_checklist_id", "question", "answer", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			attendance_checklist_id: { type: "number", positive: true, integer: true },
			question: { type: "string" },
			answer: { type: "string" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		get_answers: {
			params: {
				attendance_checklist_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						attendance_checklist_id: ctx.params.attendance_checklist_id,
					},
				}).then((response) => {
					return ctx.call("health.checklist-answers.get", {
						withRelated: null,
						id: response.map((r) => r.id),
					});
				});
			},
		},
		save_answers: {
			params: {
				attendance_checklist_id: { type: "number", positive: true, integer: true, convert: true },
				answers: {
					type: "array",
					item: {
						question: { type: "string" },
						answer: { type: "string" },
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					attendance_checklist_id: ctx.params.attendance_checklist_id,
				});
				this.clearCache();

				const entities = ctx.params.answers.map((answer) => ({
					attendance_checklist_id: ctx.params.attendance_checklist_id,
					answer: answer.answer,
					question: answer.question,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
