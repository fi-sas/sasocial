"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { CONFIGURATION_KEYS } = require("./utils/constants");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("health", "configurations")],
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});
				return result;
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			params: {
				CURRENT_ACCOUNT_ID: { type: "number", convert: true, optional: true },
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key: key } }).then((config) => {
						if (config.length > 0 && ctx.params[config[0].key] != null) {
							return this._update(
								ctx,
								{ id: config[0].id, value: JSON.stringify(ctx.params[config[0].key]) },
								true,
							);
						}
					});
				});
				return Promise.all(promisses).then(() => ctx.call("health.configurations.list", {}));
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getExternalProfileId: {
			visibility: "published",
			rest: "GET /external_profile_id",
			params: {},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						key: CONFIGURATION_KEYS.EXTERNAL_PROFILE,
					},
				})
					.then((response) => {
						if (response.length > 0) {
							return ctx.call("authorization.profiles.find", {
								query: {
									name: response[0].value,
								},
							});
						}
					})
					.then((profile) => {
						return profile[0] ? profile[0].id : null;
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
