"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.doctor-specialty",
	table: "doctor_specialty",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "doctor-specialty")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "doctor_id", "specialty_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			doctor_id: { type: "number", positive: true },
			specialty_id: { type: "number", positive: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		doctors_by_specialty: {
			params: {
				specialty_id: {
					type: "number",
					convert: true,
				},
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						specialty_id: ctx.params.specialty_id,
					},
				}).then((response) => {
					return ctx.call("health.doctor.get", {
						withRelated: null,
						id: response.map((r) => r.doctor_id),
					});
				});
			},
		},

		save_doctors_specialties: {
			params: {
				specialty_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				doctors_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				this.clearCache();
				let dataResponse = [];
				const doctorsSpecialty = await this._find(ctx, {
					query: { specialty_id: ctx.params.specialty_id },
				}).then((response) => response.map((r) => r.doctor_id));
				const doctorsDelete = doctorsSpecialty.filter((x) => !ctx.params.doctors_ids.includes(x));
				for (const doctor_id of ctx.params.doctors_ids) {
					if (!doctorsDelete.includes(doctor_id)) {
						this._find(ctx, {
							query: {
								specialty_id: ctx.params.specialty_id,
								doctor_id: doctor_id,
							},
						}).then(async (response) => {
							if (response.length > 0) {
								dataResponse.push({
									message: "doctor_id: " + doctor_id + " already assigned to specialty!",
								});
							} else {
								let doctor = {};
								doctor.specialty_id = ctx.params.specialty_id;
								doctor.doctor_id = doctor_id;
								this._insert(ctx, { entity: doctor }).then((doctor_created) => {
									dataResponse.push({
										message:
											"doctor_id: " + doctor_created[0].doctor_id + " assigned to specialty!",
									});
								});
							}
						});
					}
				}
				if (doctorsDelete.length > 0) {
					await ctx.call("health.doctor-specialty.delete_doctors_specialties", {
						specialty_id: ctx.params.specialty_id,
						doctors_ids: doctorsDelete,
					});
				}
				return dataResponse;
			},
		},
		delete_doctors_specialties: {
			params: {
				specialty_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				doctors_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				this.clearCache();
				let dataResponse = [];
				for (const doctor_id of ctx.params.doctors_ids) {
					this._find(ctx, {
						query: {
							specialty_id: ctx.params.specialty_id,
							doctor_id: doctor_id,
						},
					}).then(async (response) => {
						if (response.length > 0) {
							ctx
								.call("health.appointment.find", {
									query: {
										doctor_id: response[0].doctor_id,
										specialty_id: response[0].specialty_id,
										status: ["PENDING", "APPROVED"],
									},
								})
								.then(async (appointments) => {
									if (appointments.length > 0) {
										dataResponse.push({
											message: "doctor_id: " + doctor_id + " with open appointments!",
										});
									} else {
										ctx
											.call("health.period.find", {
												query: {
													doctor_specialty_id: response[0].id,
												},
											})
											.then(async (periods) => {
												for (const p of periods) {
													await ctx.call("health.period.remove", { id: p.id });
												}
												return this.adapter.db.transaction(async (trx) => {
													return this.adapter
														.db("doctor_specialty")
														.transacting(trx)
														.where("id", response[0].id)
														.del();
												});
											});
									}
								});
						} else {
							dataResponse.push({
								message: "doctor_id: " + doctor_id + " not assigned to specialty!",
							});
						}
					});
				}
				return dataResponse;
			},
		},

		specialty_ids_by_doctor: {
			params: {
				doctor_id: {
					type: "number",
					convert: true,
				},
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						doctor_id: ctx.params.doctor_id,
					},
				}).then((response) => {
					return response.map((res) => {
						return res.specialty_id;
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			if (ctx.params.translations && Array.isArray(ctx.params.translations)) {
				const ids = ctx.params.translations.map((translation) => translation.language_id);
				await Promise.all(
					ids.map((id) => {
						ctx.call("configuration.languages.get", {
							id,
						});
					}),
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
