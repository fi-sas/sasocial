"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const moment = require("moment");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.doctor",
	table: "doctor",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "doctor")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "allow_historic", "is_external", "created_at", "updated_at"],
		defaultWithRelateds: ["user", "attachments_ids", "specialties_ids"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			attachments_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.doctor-attachment.find", {
								doctor_id: doc.id,
							})
							.then((res) => {
								doc.attachments_ids = res.map((r) => r.file_id);
							});
					}),
				);
			},
			specialties_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.doctor-specialty.specialty_ids_by_doctor", {
								doctor_id: doc.id,
							})
							.then((res) => {
								doc.specialties_ids = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, convert: true },
			allow_historic: { type: "boolean" },
			is_external: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["isPossibleToRemove"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		get_specialties: {
			rest: "GET /:id/specialties",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			async handler(ctx) {
				return ctx
					.call("health.doctor-specialty.find", { query: { doctor_id: ctx.params.id } })
					.then((response) => {
						return ctx.call("health.specialty.get", {
							id: response.map((res) => res.specialty_id),
						});
					})
					.then(async (responseSpec) => {
						const response = [];
						for (const r of responseSpec) {
							const obj = {};
							obj.checked = false;
							obj.translations = r.translations;
							obj.value = r.id;
							obj.id = r.id;
							obj.avarage_time = r.avarage_time;
							obj.doctor_specialty_id = await this.getDoctorSpecialty(ctx, r.id);
							response.push(obj);
						}
						return response;
					});
			},
		},
		create_schedule: {
			rest: "POST /schedule/create",
			visibility: "published",
			params: {
				doctor_id: { type: "number", min: 0, convert: true, optional: true },
				specialty_id: { type: "number", min: 0, convert: true, optional: true },
				doctor_specialty_id: { type: "number", min: 0, convert: true, optional: true },
				schedules: {
					type: "array",
					item: {
						start_date: { type: "date", convert: true },
						end_date: { type: "date", convert: true },
						days: { type: "array" },
					},
				},
			},
			handler(ctx) {
				return this.getSpecialtyDoctor(ctx).then(async (response) => {
					if (response.specialty[0].is_active) {
						if (!response.specialty[0].is_external) {
							const checkResponse = await this.checkIfScheduleIsAvailable(ctx);
							if (checkResponse) {
								await ctx.call("health.period.save_period", {
									doctor_specialty_id: response.doctor_specialty_id,
									schedules: checkResponse.schedules,
								});
							}
						} else {
							throw new Errors.ValidationError(
								"Specialty is External!",
								"HEALTH_SPECIALTY_EXTERNAL",
							);
						}
					} else {
						throw new Errors.ValidationError("Specialty is Inactive!", "HEALTH_SPECIALTY_INACTIVE");
					}
				});
			},
		},
		update_schedule: {
			rest: "POST /schedule/update/:id",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
				schedules: {
					type: "array",
					item: {
						start_date: { type: "date", convert: true },
						end_date: { type: "date", convert: true },
						days: { type: "array" },
					},
				},
			},
			handler(ctx) {
				return this.getSpecialtyDoctor(ctx).then(async (response) => {
					if (response.specialty[0].is_active) {
						if (!response.specialty[0].is_external) {
							await ctx
								.call("health.appointment.find", {
									query: {
										doctor_id: ctx.params.doctor_id,
										specialty_id: response.specialty[0].id,
										status: ["PENDING", "APPROVED"],
									},
								})
								.then(async (appointments) => {
									const checkResponse = await this.checkIfScheduleIsAvailable(ctx);
									if (checkResponse) {
										ctx.call("health.doctor.delete_schedule", { id: ctx.params.id }).then(() => {
											ctx.call("health.period.save_period", {
												doctor_specialty_id: response.doctor_specialty_id,
												schedules: checkResponse.schedules,
											});
										});
									}
								});
						} else {
							throw new Errors.ValidationError(
								"Specialty is External!",
								"HEALTH_SPECIALTY_EXTERNAL",
							);
						}
					} else {
						throw new Errors.ValidationError("Specialty is Inactive!", "HEALTH_SPECIALTY_INACTIVE");
					}
				});
			},
		},
		delete_schedule: {
			rest: "DELETE /schedule/remove/:id",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return ctx
					.call("health.period.remove", { id: ctx.params.id })
					.then(() => this.clearCache());
			},
		},
		get_period: {
			rest: "GET /schedule/get/:id",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return ctx.call("health.period.get", { id: ctx.params.id }).then(async (response) => {
					const doctor_specialty = await ctx.call("health.doctor-specialty.get", {
						id: response[0].doctor_specialty_id,
					});
					const obj = {};
					obj.doctor_id = doctor_specialty[0].doctor_id;
					obj.specialty_id = doctor_specialty[0].specialty_id;
					obj.schedules = response;
					return obj;
				});
			},
		},
		create_internal: {
			rest: "POST /create-internal",
			visibility: "published",
			params: {
				user_id: { type: "number", positive: true, convert: true },
				allow_historic: { type: "boolean" },
				is_external: { type: "boolean" },
				attachments_ids: {
					type: "array",
					item: {
						file_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
						},
					},
				},
			},
			handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
					},
				})
					.then((doctor) => {
						if (doctor.length > 0) {
							throw new Errors.ValidationError("Doctor already exists", "HEALTH_DOCTOR_EXISTS", {});
						}
						return ctx.call("authorization.users.get", { id: ctx.params.user_id });
					})
					.then(() => {
						const new_doctor = {};
						new_doctor.user_id = ctx.params.user_id;
						new_doctor.allow_historic = ctx.params.allow_historic;
						new_doctor.is_external = ctx.params.is_external;
						return this._insert(ctx, { entity: new_doctor });
					})
					.then((response) => {
						if (response.length > 0) {
							return this.saveDoctorAttachments(ctx, ctx.params.attachments_ids, response[0].id);
						}
					})
					.catch((err) => {
						throw new Errors.ValidationError(
							"Doctor creation failed",
							"HEALTH_ERROR_CREATE_DOCTOR",
							{},
						);
					});
			},
		},
		get_historic: {
			rest: "GET /:id/historic",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			handler(ctx) {},
		},
		getScheduleBySpecialty: {
			rest: "GET /calendar",
			visibility: "published",
			params: {
				doctor_specialty_id: { type: "number", positive: true, convert: true },
			},
			handler(ctx) {
				return ctx
					.call("health.doctor-specialty.get", { id: ctx.params.doctor_specialty_id })
					.then(async (response) => {
						const schedule = [];
						const specialty = await ctx.call("health.specialty.get", {
							id: response[0].specialty_id,
						});
						const holidays = await ctx
							.call("configuration.holidays.list", {})
							.then((holidays_response) => {
								return holidays_response.rows.filter((h) =>
									h.organic_units.length > 0
										? h.organic_units.find((o) => o.id === specialty[0].organic_unit_id)
										: h,
								);
							});
						const periods = await ctx.call("health.period.find", {
							query: {
								doctor_specialty_id: ctx.params.doctor_specialty_id,
							},
						});
						for (const period of periods) {
							let periodStartDate = moment(period.start_date, "YYYY-MM-DD");
							while (periodStartDate.isSameOrBefore(moment(period.end_date, "YYYY-MM-DD"))) {
								let checkIfDayIsHoliday = holidays.filter((h) =>
									moment(h.date, "YYYY-MM-DD").isSame(periodStartDate),
								);
								if (checkIfDayIsHoliday.length > 0) {
									const convertStartDate = periodStartDate.format("YYYY-MM-DD") + "T00:00:00";
									const obj = {};
									obj.id = convertStartDate;
									obj.title = checkIfDayIsHoliday.map(
										(h) => h.translations.find((t) => t.language_id === 3).name,
									);
									obj.start_date = convertStartDate;
									obj.is_available = false;
									obj.is_holiday = true;
									schedule.push(obj);
								} else {
									const days = period.days.filter(
										(d) => periodStartDate.day() === moment().day(d.day).day(),
									);
									if (days.length > 0) {
										for (const day of days) {
											for (const h of day.days) {
												let start_hour = moment(h.start_hour, "HH:mm:ss");
												while (start_hour.isBefore(moment(h.end_hour, "HH:mm:ss"))) {
													const hourAvailable = await this.checkIfExistsAppointment(
														ctx,
														start_hour.format("HH:mm:ss"),
														periodStartDate,
														specialty[0].avarage_time,
														response[0].specialty_id,
														response[0].doctor_id,
													);

													const minutesDiff = moment(h.end_hour, "HH:mm:ss").diff(
														moment(start_hour),
														"minutes",
													);

													if (minutesDiff >= specialty[0].avarage_time) {
														let end_hour = start_hour.valueOf();
														end_hour = moment(end_hour).add(specialty[0].avarage_time, "minutes");

														const convertStartDate =
															periodStartDate.format("YYYY-MM-DD") +
															"T" +
															start_hour.format("HH:mm:ss");
														const convertEndDate =
															periodStartDate.format("YYYY-MM-DD") +
															"T" +
															end_hour.format("HH:mm:ss");

														const obj = {};
														obj.id = convertStartDate;
														obj.start_date = moment(convertStartDate);
														obj.end_date = moment(convertEndDate);
														obj.title =
															hourAvailable.length > 0 ? hourAvailable[0].name : "Disponivel";
														obj.is_available = hourAvailable;
														schedule.push(obj);
													}
													start_hour.add(specialty[0].avarage_time, "minutes");
												}
											}
										}
									}
								}
								periodStartDate.add(1, "day");
							}
						}
						return schedule;
					});
			},
		},
		checkUserLogged: {
			rest: "GET /isProfessional",
			visibility: "published",
			handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.meta.user.id,
					},
				}).then((response) => {
					return response.length > 0 ? true : false;
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		isPossibleToRemove(ctx) {
			if (ctx.params.id) {
				return ctx
					.call("health.doctor-specialty.count", {
						query: {
							doctor_id: ctx.params.id,
						},
					})
					.then((response) => {
						if (response.length > 0) {
							throw new Errors.ValidationError(
								"You have medical specialties associated to this doctor",
								"HEALTH_SPECIALTY_WITH_DOCTOR",
							);
						}
						return ctx.call("health.appointment.find", {
							query: {
								doctor_id: ctx.params.id,
								status: ["PENDING", "APPROVED"],
							},
						});
					})
					.then((appointments) => {
						if (appointments.length > 0) {
							throw new Errors.ValidationError(
								"You have open appointments associated to this doctor",
								"HEALTH_OPEN_APPOINTMENTS_ASSOCIATED_TO_DOCTOR",
							);
						}
					});
			}
		},

		getSpecialtyDoctor(ctx) {
			let doctor_specialty_id;
			return ctx
				.call("health.doctor-specialty.find", {
					query: {
						doctor_id: ctx.params.doctor_id,
						specialty_id: ctx.params.specialty_id,
					},
				})
				.then((result) => {
					if (!result[0]) {
						throw new Errors.ValidationError(
							"Doctor not associated to this specialty",
							"HEALTH_DOCTOR_NOT_ASSOCIATED_TO_SPECIALTY",
						);
					}
					doctor_specialty_id = result[0].id;
					return ctx.call("health.specialty.get", {
						id: result[0].specialty_id,
					});
				})
				.then((specialty) => {
					return {
						doctor_specialty_id: doctor_specialty_id,
						specialty: specialty,
					};
				});
		},

		saveDoctorAttachments(ctx, attachments_ids, doctor_id) {
			if (attachments_ids && Array.isArray(attachments_ids)) {
				const attachments_ids = [...new Set(ctx.params.attachments_ids)];
				return ctx.call("health.doctor-attachment.save_files", {
					doctor_id: doctor_id,
					attachments_ids,
				});
			}
		},

		async checkIfScheduleIsAvailable(ctx, isUpdate) {
			return ctx
				.call("health.doctor-specialty.find", {
					query: {
						doctor_id: ctx.params.doctor_id,
					},
				})
				.then(async (doctorsSpecialties) => {
					return await this.schedulesCheck(ctx, doctorsSpecialties);
				});
		},

		async schedulesCheck(ctx, doctorsSpecialties) {
			await ctx
				.call("health.period.find", {
					query: {
						doctor_specialty_id: doctorsSpecialties.map((ds) => ds.id),
					},
				})
				.then(async (periods) => {
					for (const schedule of ctx.params.schedules) {
						let periodBetweenFiltered = [];
						if (ctx.params.id) {
							periodBetweenFiltered = periods.filter(
								(p) =>
									p.id !== ctx.params.id &&
									(moment(schedule.start_date, "YYYY-MM-DD").isBetween(
										moment(p.start_date, "YYYY-MM-DD"),
										moment(p.end_date, "YYYY-MM-DD"),
										null,
										"[]",
									) ||
										moment(schedule.end_date, "YYYY-MM-DD").isBetween(
											moment(p.start_date, "YYYY-MM-DD"),
											moment(p.end_date, "YYYY-MM-DD"),
											null,
											"[]",
										)),
							);
						} else {
							periodBetweenFiltered = periods.filter(
								(p) =>
									moment(schedule.start_date, "YYYY-MM-DD").isBetween(
										moment(p.start_date, "YYYY-MM-DD"),
										moment(p.end_date, "YYYY-MM-DD"),
										null,
										"[]",
									) ||
									moment(schedule.end_date, "YYYY-MM-DD").isBetween(
										moment(p.start_date, "YYYY-MM-DD"),
										moment(p.end_date, "YYYY-MM-DD"),
										null,
										"[]",
									),
							);
						}
						if (periodBetweenFiltered.length > 0) {
							for (const daysSchedule of schedule.days) {
								const checkDay = periodBetweenFiltered.map((p) =>
									p.days.filter((d) => d.day === daysSchedule.day),
								);
								if (checkDay && checkDay.length) {
									for (const hour of daysSchedule.hours) {
										const checkHour = checkDay.map((d) =>
											d.map((day) =>
												day.days.find(
													(h) =>
														moment(hour.start_hour, "HH:mm:ss").isBetween(
															moment(h.start_hour, "HH:mm:ss"),
															moment(h.end_hour, "HH:mm:ss"),
															null,
															"[)",
														) ||
														moment(hour.end_hour, "HH:mm:ss").isBetween(
															moment(h.start_hour, "HH:mm:ss"),
															moment(h.end_hour, "HH:mm:ss"),
															null,
															"(]",
														),
												),
											),
										);
										for (const err of checkHour) {
											if (err[0]) {
												throw new Errors.ValidationError(
													"Já existe um Horário nesse periodo",
													"HEALTH_DOCTOR_SCHEDULE_ALREADY_EXISTS",
												);
											}
										}
									}
								}
							}
						}
					}
				});
			return ctx.params;
		},

		getDoctorSpecialty(ctx, specialty_id) {
			return ctx
				.call("health.doctor-specialty.find", {
					query: {
						doctor_id: ctx.params.id,
						specialty_id: specialty_id,
					},
				})
				.then((response) => response[0].id);
		},

		checkIfExistsAppointment(ctx, start_hour, date, avarage_time, specialty_id, doctor_id) {
			const convertedHour = moment(start_hour, "HH:mm:ss");
			const end_hour = convertedHour.add(avarage_time, "minutes").format("HH:mm:ss");
			return ctx.call("health.appointment.find", {
				query: {
					doctor_id: doctor_id,
					specialty_id: specialty_id,
					date: moment(date).format("YYYY-MM-DD"),
					start_hour: start_hour,
					end_hour: end_hour,
					status: ["APPROVED", "PENDING", "CLOSED", "MISSED"],
				},
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
