/* eslint-disable no-console */
"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.doctor-external-attachment",
	table: "doctor_external_attachment",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "doctor-external-attachment")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "doctor_external_id", "file_id", "created_at", "updated_at"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			doctor_external_id: { type: "number", positive: true, integer: true },
			file_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_files: {
			params: {
				doctor_external_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				attachments_ids: {
					type: "array",
					item: {
						file_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					doctor_external_id: ctx.params.doctor_external_id,
				});
				this.clearCache();
				const entities = ctx.params.attachments_ids.map((file_id) => ({
					doctor_external_id: ctx.params.doctor_external_id,
					file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
