"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { GENDER } = require("../../utils/constants");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.doctor-external",
	table: "doctor_external",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "doctor-external")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"email",
			"gender",
			"address",
			"postal_code",
			"city",
			"country",
			"phone",
			"nif",
			"is_validated",
			"is_active",
			"allow_historic",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["attachments_ids"],
		withRelateds: {
			attachments_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.doctor-external-attachment.find", {
								doctor_external_id: doc.id,
							})
							.then((res) => {
								doc.attachments_ids = res.map((r) => r.file_id);
							});
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			email: { type: "string" },
			gender: { type: "enum", values: Object.keys(GENDER) },
			address: { type: "string" },
			postal_code: { type: "string" },
			city: { type: "string" },
			country: { type: "string" },
			phone: { type: "string" },
			nif: { type: "string" },
			is_validated: { type: "boolean", default: false },
			is_active: { type: "boolean", default: false },
			allow_historic: { type: "boolean", default: false },
			attachments_ids: {
				type: "array",
				item: {
					file_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["saveAttachments"],
			update: ["saveAttachments"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		validate_external: {
			rest: "POST /:id/validate",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then(async (response) => {
					if (response.length > 0) {
						let user = {};
						user.user_name = response[0].email;
						user.name = response[0].name;
						user.email = response[0].email;
						user.gender = response[0].gender;
						user.address = response[0].address;
						user.postal_code = response[0].postal_code;
						user.birth_date = response[0].birth_date ? response[0].birth_date : new Date();
						user.city = response[0].city;
						user.country = response[0].country;
						user.phone = response[0].phone;
						user.tin = response[0].nif;
						user.can_access_BO = false;
						user.external = true;
						user.profile_id = await ctx.call("health.configurations.getExternalProfileId", {});
						user.active = true;
						return ctx.call("authorization.users.create", user).then((userResponse) => {
							const new_doctor = {};
							new_doctor.user_id = userResponse[0].id;
							new_doctor.allow_historic = response[0].allow_historic;
							new_doctor.is_external = true;
							new_doctor.attachments_ids = response[0].attachments_ids;
							return ctx.call("health.doctor.create", new_doctor).then(() => {
								return ctx.call("health.doctor-external.patch", {
									id: ctx.params.id,
									is_validated: true,
									is_active: userResponse[0].active,
								});
							});
						});
					}
				});
			},
		},
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveAttachments(ctx, response) {
			if (ctx.params.attachments_ids && Array.isArray(ctx.params.attachments_ids)) {
				const attachments_ids = [...new Set(ctx.params.attachments_ids)];
				await ctx.call("health.doctor-external-attachment.save_files", {
					doctor_external_id: response[0].id,
					attachments_ids,
				});
				response[0].attachments_ids = await ctx.call("health.doctor-external-attachment.find", {
					query: {
						doctor_external_id: response[0].id,
					},
				});
				return response;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
