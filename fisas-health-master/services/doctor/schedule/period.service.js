"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const _ = require("lodash");
const moment = require("moment");
const Errors = require("@fisas/ms_core").Helpers.Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.period",
	table: "period",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "period")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "doctor_specialty_id", "start_date", "end_date", "created_at", "updated_at"],
		defaultWithRelateds: ["days"],
		withRelateds: {
			days(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.schedule.find", {
								query: {
									period_id: doc.id,
								},
							})
							.then((days) => {
								const res = days.reduce(function (r, a) {
									r[a.day] = r[a.day] || [];
									r[a.day].push(a);
									return r;
								}, Object.create(null));
								const keys = [];
								for (const day in res) {
									let obj = {};
									obj.days = res[day];
									obj.day = day;
									keys.push(obj);
								}
								doc.days = keys;
							});
					}),
				);
			},
		},
		entityValidator: {
			doctor_specialty_id: { type: "number", positive: true, integer: true },
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			cache: false,
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" },
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);
				return this._get(ctx, { id: id })
					.then((response) => {
						return ctx.call("health.doctor-specialty.get", { id: response[0].doctor_specialty_id });
					})
					.then((doctor_specialty) => {
						return ctx.call("health.appointment.find", {
							query: {
								doctor_id: doctor_specialty[0].doctor_id,
								specialty_id: doctor_specialty[0].specialty_id,
								status: ["PENDING", "APPROVED"],
							},
						});
					})
					.then((appointment) => {
						/*if(appointment.length > 0){
							throw new Errors.ValidationError(
								"Appointment not closed!",
								"HEALTH_APPOINTMENT_NOT_CLOSED",
							);
						}*/
						return this.adapter.db.transaction(async (trx) => {
							return this.adapter
								.db("schedule")
								.transacting(trx)
								.where("period_id", id)
								.del()
								.then(() => {
									return this.adapter.db("period").transacting(trx).where("id", id).del();
								})
								.then((doc) => {
									if (!doc)
										return Promise.reject(new Errors.EntityNotFoundError("period", params.id));
									this.clearCache();
									return this.transformDocuments(ctx, {}, doc).then((json) =>
										this.entityChanged("REMOVED!", json, ctx).then(() => json),
									);
								});
						});
					});
			},
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_period: {
			params: {
				doctor_specialty_id: { type: "number", convert: true },
				schedules: {
					type: "array",
					item: {
						start_date: { type: "date", convert: true },
						end_date: { type: "date", convert: true },
						days: { type: "array" },
					},
				},
			},
			async handler(ctx) {
				if (ctx.params.schedules && Array.isArray(ctx.params.schedules)) {
					for (const schedule of ctx.params.schedules) {
						const insertPeriod = {};
						insertPeriod.doctor_specialty_id = ctx.params.doctor_specialty_id;
						insertPeriod.start_date = moment(schedule.start_date).format("YYYY-MM-DD");
						insertPeriod.end_date = schedule.end_date;
						await this._insert(ctx, { entity: insertPeriod }).then(async (inserted) => {
							await ctx.call("health.schedule.save_days", {
								period_id: inserted[0].id,
								days: schedule.days,
							});
						});
						this.clearCache();
					}
				}
			},
		},
		get_periods: {
			rest: "GET /periods",
			visibility: "published",
			cache: false,
			params: {
				doctor_specialty_id: {
					type: "number",
					integer: true,
					min: 1,
					optional: true,
					convert: true,
				},
			},
			handler(ctx) {
				if (_.has(ctx, "params.query.doctor_specialty_id")) {
					ctx.params.doctor_specialty_id = ctx.params.query.doctor_specialty_id;
				}
				return this._find(ctx, {
					query: {
						doctor_specialty_id: ctx.params.doctor_specialty_id,
					},
				});
			},
		},
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
