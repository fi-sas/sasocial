"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { DAYS } = require("../../utils/constants");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.schedule",
	table: "schedule",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "schedule")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "period_id", "day", "start_hour", "end_hour", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			period_id: { type: "number", positive: true, integer: true },
			day: { type: "enum", values: Object.keys(DAYS) },
			start_hour: { type: "string" },
			end_hour: { type: "string" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_days: {
			params: {
				period_id: { type: "number" },
				days: {
					type: "array",
					item: {
						day: { type: "enum", values: Object.keys(DAYS) },
						hours: {
							type: "array",
							item: {
								start_hour: { type: "string" },
								end_hour: { type: "string" },
							},
						},
					},
				},
			},
			async handler(ctx) {
				if (ctx.params.days && Array.isArray(ctx.params.days)) {
					if (ctx.params.days && ctx.params.days.length) {
						const daysSorted = ctx.params.days.sort(
							(a, b) => moment().day(a.day).day() - moment().day(b.day).day(),
						);
						for (const day of daysSorted) {
							const list = [];
							for (const hour of day.hours) {
								const y = day.hours.filter(
									(h2) =>
										moment(hour.start_hour, "HH:mm:ss").isBetween(
											moment(h2.start_hour, "HH:mm:ss"),
											moment(h2.end_hour, "HH:mm:ss"),
										) ||
										moment(hour.end_hour, "HH:mm:ss").isBetween(
											moment(h2.start_hour, "HH:mm:ss"),
											moment(h2.end_hour, "HH:mm:ss"),
										),
								);
								if (y.length > 0) {
									const y2 = y.reduce((a, b) =>
										this.getMinutesDiff(a.start_hour, a.end_hour) >=
										this.getMinutesDiff(b.start_hour, b.end_hour)
											? a
											: b,
									);
									if (y2 && !list.find((l) => l === y2)) {
										list.push(y2);
									}
								} else {
									list.push(hour);
								}
							}
							const listSortHours = list.sort(
								(a, b) =>
									moment(a.start_hour, "HH:mm").valueOf() - moment(b.start_hour, "HH:mm").valueOf(),
							);
							for (const insert of listSortHours) {
								this._insert(ctx, {
									entity: {
										period_id: ctx.params.period_id,
										day: day.day,
										start_hour: insert.start_hour,
										end_hour: insert.end_hour,
									},
								});
							}
						}
					}
					this.clearCache();
				}
			},
		},
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async checkIfHourExists(ctx, period_id, day, start_hour, end_hour) {
			const hoursSchedule = await this._find(ctx, {
				query: {
					period_id: period_id,
					day: day,
				},
			});
			return hoursSchedule.filter(
				(r) =>
					moment(r.start_hour, "HH:mm:ss").isBetween(
						moment(start_hour, "HH:mm:ss"),
						moment(end_hour, "HH:mm:ss"),
						null,
						"[]",
					) ||
					moment(r.end_hour, "HH:mm").isBetween(
						moment(start_hour, "HH:mm:ss"),
						moment(end_hour, "HH:mm:ss"),
						null,
						"[]",
					),
			);
		},

		getMinutesDiff(start_hour, end_hour) {
			return moment(end_hour, "HH:mm:ss").diff(moment(start_hour, "HH:mm:ss"), "minutes");
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
