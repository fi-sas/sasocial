"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.appointment-type-translations",
	table: "appointment_type_translation",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "appointment-type-translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"appointment_type_id",
			"language_id",
			"name",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			language_id: { type: "number", positive: true, integer: true },
		}
	},
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				appointment_type_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					appointment_type_id: ctx.params.appointment_type_id
				});
				this.clearCache();

				const entities = ctx.params.translations.map(translation => ({
					appointment_type_id: ctx.params.appointment_type_id,
					language_id: translation.language_id,
					name: translation.name,
				}));
				return this._insert(ctx, { entities });
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
