"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.appointment-type",
	table: "appointment_type",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "appointment-type")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"health.appointment-type-translations",
					"translations",
					"id",
					"appointment_type_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
		}
	},
	hooks: {
		before: {
			create: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			list: [
				function addFilterByTranslations(ctx) {
					return addSearchRelation(ctx, ["name"], "health.appointment-type-translations", "appointment_type_id",);
				},
			],
			remove: ["isPossibleToRemove"],
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);
				this.clearCache();
				return this.adapter.db
					.transaction(async (trx) => {
						return this.adapter.db("appointment_type_translation").transacting(trx).where("appointment_type_id", id).del()
							.then(() => {
								return this.adapter.db("appointment_type").transacting(trx).where("id", id).del();
							})
							.then(doc => {
								if (!doc)
									return Promise.reject(new Errors.EntityNotFoundError("appointment_type", params.id));
								this.clearCache();
								return this.transformDocuments(ctx, {}, doc)
									.then(json => this.entityChanged("REMOVED!", json, ctx).then(() => json));
							});
					});
			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);

			await Promise.all(
				ids.map((id) => {
					ctx.call("configuration.languages.get", {
						id,
					});
				}),
			);
		},
		async saveTranslations(ctx, response) {
			await ctx.call("health.appointment-type-translations.save_translations", {
				appointment_type_id: response[0].id,
				translations: ctx.params.translations,
			});
			response[0].translations = await ctx.call("health.appointment-type-translations.find", {
				query: {
					appointment_type_id: response[0].id,
				},
			});
			return response;
		},

		async isPossibleToRemove(ctx){
			const countTypes = await Promise.all([
				ctx.call("health.specialty-appointment-type.count", {
					query: {
						appointment_type_id: ctx.params.id,
					},
				}),
			]);
			if (countTypes > 0) {
				throw new Errors.ValidationError(
					"You have medical specialties associated",
					"HEALTH_SPECIALTY_WITH_TYPE",
				);
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
