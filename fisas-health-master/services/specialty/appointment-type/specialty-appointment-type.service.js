"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.specialty-appointment-type",
	table: "specialty_appointment_type",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "specialty-appointment-type")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "appointment_type_id", "specialty_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			appointment_type_id: { type: "number", positive: true },
			specialty_id: { type: "number", positive: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		specialty_appointment_types: {
			cache: {
				keys: ["specialty_id"],
			},
			rest: {
				path: "GET /specialty/:specialty_id/specialty-appointment-types",
			},
			params: {
				specialty_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						specialty_id: ctx.params.specialty_id,
					},
				}).then((response) => {
					return ctx.call("health.appointment-type.get", {
						withRelated: null,
						id: response.map((r) => r.appointment_type_id),
					});
				});
			},
		},
		save_specialty_appointment_types: {
			params: {
				specialty_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				appointment_types_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			async handler(ctx) {
				if (ctx.params.appointment_types_ids && Array.isArray(ctx.params.appointment_types_ids)) {
					await this.adapter.removeMany({
						specialty_id: ctx.params.specialty_id,
					});
					this.clearCache();
					const entities = ctx.params.appointment_types_ids.map((appointment_type_id) => ({
						specialty_id: ctx.params.specialty_id,
						appointment_type_id,
					}));
					return this._insert(ctx, { entities });
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
