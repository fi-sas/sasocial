"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ANSWER_TYPES } = require("../../utils/constants");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.specialty-checklist",
	table: "specialty_checklist",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "specialty-checklist")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"specialty_id",
			"question",
			"answer_type",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			specialty_id: { type: "number", positive: true, integer: true },
			question: { type: "string" },
			answer_type: { type: "enum", values: Object.keys(ANSWER_TYPES) },
		}
	},
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_questions_specialty: {
			params: {
				specialty_id: { type: "number", positive: true, integer: true, convert: true },
				checklist: {
					type: "array",
					item: {
						question: { type: "string" },
						answer_type: { type: "enum", values: Object.keys(ANSWER_TYPES) }
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					specialty_id: ctx.params.specialty_id
				});
				this.clearCache();

				const entities = ctx.params.checklist.map(qes => ({
					specialty_id: ctx.params.specialty_id,
					question: qes.question,
					answer_type: qes.answer_type,
				}));
				return this._insert(ctx, { entities });
			}
		},

		checklist_by_specialty: {
			cache: {
				keys: ["specialty_id"],
			},
			rest: {
				path: "GET /specialty/:specialty_id/checklist"
			},
			params: {
				specialty_id: {
					type: "number",
					convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						specialty_id: ctx.params.specialty_id
					}
				}).then(response => {
					return ctx.call("health.specialty-checklist.get", {
						withRelated: null,
						id: response.map(r => r.id)
					});
				});
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
