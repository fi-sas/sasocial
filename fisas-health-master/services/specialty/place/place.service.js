"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.place",
	table: "place",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "place")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"email",
			"address",
			"postal_code",
			"city",
			"phone",
			"fax",
			"created_at",
			"updated_at",
			"is_active",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			address: { type: "string" },
			email: { type: "string" },
			postal_code: { type: "string" },
			city: { type: "string" },
			phone: { type: "number", positive: true, integer: true, convert: true },
			fax: { type: "number", positive: true, integer: true, convert: true },
			is_active: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["isPossibleToRemove"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		status: {
			rest: "POST /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				is_active: { type: "boolean" },
			},
			handler(ctx) {
				if (ctx.params.is_active) {
					//ativar
					return this._get(ctx, { id: ctx.params.id }).then((response) => {
						if (response.length > 0) {
							if (response[0].is_active) {
								throw new Errors.ValidationError("Place already active", "HEALTH_PLACE_ACTIVE");
							}
							return ctx.call("health.place.patch", {
								id: ctx.params.id,
								is_active: ctx.params.is_active,
							});
						}
					});
				} else {
					//desativar
					return ctx
						.call("health.specialty.find", {
							query: {
								place_id: ctx.params.id,
							},
						})
						.then((specialties) => {
							if (specialties.length > 0) {
								throw new Errors.ValidationError(
									"Specialties associated to this place",
									"HEALTH_PLACE_WITH_SPECIALTIES",
								);
							}
							return this._get(ctx, { id: ctx.params.id });
						})
						.then((place) => {
							if (place.length > 0 && !place[0].is_active) {
								throw new Errors.ValidationError(
									"Place already deactivated",
									"HEALTH_PLACE_DEACTIVATED",
								);
							}
							return ctx.call("health.place.patch", {
								id: ctx.params.id,
								is_active: ctx.params.is_active,
							});
						});
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleToRemove(ctx) {
			const countPlace = await Promise.all([
				ctx.call("health.specialty.count", {
					query: {
						place_id: ctx.params.id,
					},
				}),
			]);
			if (countPlace > 0) {
				throw new Errors.ValidationError(
					"Specialties associated to this place",
					"HEALTH_PLACE_WITH_SPECIALTIES",
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
