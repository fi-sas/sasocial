"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.price-variations",
	table: "price_variation",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "tariff")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "specialty_id", "tariff_id", "vat_id", "price", "created_at", "updated_at"],
		defaultWithRelateds: ["vat", "tariffs"],
		withRelateds: {
			vat(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "vat", "vat_id");
			},
			tariffs(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.tariff.get", {
								id: doc.tariff_id,
							})
							.then((res) => {
								doc.tariffs = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			specialty_id: { type: "number", positive: true, convert: true },
			tariff_id: { type: "number", positive: true, convert: true },
			vat_id: { type: "number", positive: true, convert: true },
			price: { type: "number", convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["isPossibleToRemove"],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_price_variations: {
			params: {
				specialty_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				price_variations: {
					type: "array",
					item: {
						tariff_id: { type: "number", positive: true, integer: true, convert: true },
						vat_id: { type: "number", positive: true, integer: true, convert: true },
						price: { type: "number", positive: true, integer: true, convert: true },
					},
				},
			},
			async handler(ctx) {
				if (ctx.params.price_variations && Array.isArray(ctx.params.price_variations)) {
					await this.adapter.removeMany({
						specialty_id: ctx.params.specialty_id,
					});
					this.clearCache();
					const entities = ctx.params.price_variations.map((price_variation) => ({
						specialty_id: ctx.params.specialty_id,
						tariff_id: price_variation.tariff_id,
						vat_id: price_variation.vat_id,
						price: price_variation.price,
					}));
					return this._insert(ctx, { entities });
				}
			},
		},
		price_variations_specialty: {
			cache: {
				keys: ["specialty_id"],
			},
			rest: {
				path: "GET /specialty/:specialty_id/price-variations",
			},
			params: {
				specialty_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						specialty_id: ctx.params.specialty_id,
					},
				}).then((response) => {
					return ctx.call("health.price-variations.get", {
						withRelated: null,
						id: response.map((r) => r.id),
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleToRemove(ctx) {
			const countSpecialties = await Promise.all([
				ctx.call("health.specialty.count", {
					query: {
						specialty_id: ctx.params.id,
					},
				}),
			]);
			if (countSpecialties > 0) {
				throw new Errors.ValidationError(
					"You have specialties associated",
					"HEALTH_PRICE_VARIATIONS_ASSOCIATED",
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
