"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.profile-user",
	table: "profile_user",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "profile-user")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"tariff_id",
			"profile_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			tariff_id: { type: "number", positive: true, convert: true },
			profile_id: { type: "number", positive: true, convert: true },
		}
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_profiles: {
			params: {
				tariff_id: { type: "number", positive: true, integer: true, convert: true },
				profile_ids: {
					type: "array",
					item: {
						profile_id: { type: "number", positive: true, integer: true, convert: true },
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					tariff_id: ctx.params.tariff_id
				});
				this.clearCache();
				const entities = ctx.params.profile_ids.map(profile_id => ({
					tariff_id: ctx.params.tariff_id,
					profile_id: profile_id,
				}));
				return this._insert(ctx, { entities });
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};