"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.tariff",
	table: "tariff",

	adapter: new KnexAdpater(require("../../../knexfile")),
	mixins: [DbMixin("health", "tariff")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"description",
			"is_scolarship",
		],
		defaultWithRelateds: ["profile_ids"],
		withRelateds: {
			profile_ids(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.profile-user.find", {
								query:{
									tariff_id: doc.id,
								}
							})
							.then((res) => {
								doc.profile_ids = res.map( res => res.profile_id);
							});
					}),
				);
			},
		},
		entityValidator: {
			description: { type: "string" },
			is_scolarship: { type: "boolean" },
			profile_ids: {
				type: "array",
				item: {
					profile_id: { type: "number", positive: true, integer: true, convert: true },
				}
			}
		}
	},
	hooks: {
		after: {
			create: ["saveProfiles"],
			update: ["saveProfiles"],
		},
		before: {
			remove: ["isPossibleToRemove"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" }
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);
				this.clearCache();
				return this.adapter.db
					.transaction(async (trx) => {
						return this.adapter.db("profile_user").transacting(trx).where("tariff_id", id).del()
							.then(() => {
								return this.adapter.db("tariff").transacting(trx).where("id", id).del();
							})
							.then(doc => {
								if (!doc)
									return Promise.reject(new Errors.EntityNotFoundError("tariff", params.id));
								this.clearCache();
								return this.transformDocuments(ctx, {}, doc)
									.then(json => this.entityChanged("REMOVED!", json, ctx).then(() => json));
							});
					});
			}
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async isPossibleToRemove(ctx){
			const countTariffs = await Promise.all([
				ctx.call("health.price-variations.count", {
					query: {
						tariff_id: ctx.params.id,
					},
				}),
			]);
			if (countTariffs > 0) {
				throw new Errors.ValidationError(
					"You have Price Variations associated",
					"HEALTH_TARIFF_ASSOCIATED",
				);
			}
		},
		async saveProfiles(ctx, response){
			await ctx.call("health.profile-user.save_profiles", {
				tariff_id: response[0].id,
				profile_ids: ctx.params.profile_ids,
			});
			response[0].profile_ids = await ctx.call("health.profile-user.find", {
				query: {
					tariff_id: response[0].id,
				},
			}).then(profiles => { return profiles.map(profile => profile.profile_id);});
			return response;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};