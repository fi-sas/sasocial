"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.specialty-translations",
	table: "specialty_translation",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "specialty-translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"specialty_id",
			"language_id",
			"name",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			language_id: { type: "number", positive: true, integer: true },
		}
	},
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_translations: {
			params: {
				specialty_id: { type: "number", positive: true, integer: true, convert: true },
				translations: {
					type: "array",
					item: {
						language_id: { type: "number", positive: true, integer: true, convert: true },
						name: { type: "string" },
					}
				}
			},
			handler(ctx) {
				this.clearCache();
				for(const translation of ctx.params.translations){
					this._find(ctx, {
						query: {
							specialty_id: ctx.params.specialty_id,
							language_id: translation.language_id
						}
					}).then(response => {
						if(response.length > 0){
							ctx.call("health.specialty-translations.patch", {
								id: response[0].id,
								name: translation.name
							});
						}else{
							let translationInsert = {};
							translationInsert.specialty_id = ctx.params.specialty_id;
							translationInsert.language_id = translation.language_id;
							translationInsert.name = translation.name;
							this._insert(ctx, { entity: translationInsert });
						}
					});
				}
			}
		},
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
