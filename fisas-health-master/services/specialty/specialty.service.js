"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const { ANSWER_TYPES, PAYMENT_TYPES } = require("../utils/constants");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "health.specialty",
	table: "specialty",

	adapter: new KnexAdpater(require("../../knexfile")),
	mixins: [DbMixin("health", "specialty")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"is_external",
			"description",
			"regulation",
			"avarage_time",
			"product_code",
			"place_id",
			"payment_type",
			"payment_information",
			"is_active",
			"organic_unit_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"translations",
			"appointment_types",
			"price_variations",
			"place",
			"checklist",
			"doctors",
		],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"health.specialty-translations",
					"translations",
					"id",
					"specialty_id",
				);
			},
			appointment_types(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.specialty-appointment-type.specialty_appointment_types", {
								specialty_id: doc.id,
							})
							.then((res) => {
								doc.appointment_types = res;
							});
					}),
				);
			},
			price_variations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"health.price-variations",
					"price_variations",
					"id",
					"specialty_id",
				);
			},
			doctors(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.doctor-specialty.doctors_by_specialty", {
								specialty_id: doc.id,
							})
							.then((res) => {
								doc.doctors = res;
							});
					}),
				);
			},
			place(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "health.place", "place", "place_id");
			},
			checklist(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("health.specialty-checklist.checklist_by_specialty", {
								specialty_id: doc.id,
							})
							.then((res) => {
								doc.checklist = res;
							});
					}),
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
					name: { type: "string" },
				},
				min: 1,
			},
			doctors_ids: {
				type: "array",
				item: {
					doctor_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			appointment_types_ids: {
				type: "array",
				item: {
					appointment_type_id: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			price_variations: {
				type: "array",
				item: {
					tariff_id: { type: "number", positive: true, convert: true },
					vat_id: { type: "number", positive: true, convert: true },
					price: { type: "number", positive: true, convert: true },
				},
			},
			checklist: {
				type: "array",
				item: {
					question: { type: "string" },
					answer_type: { type: "enum", values: Object.keys(ANSWER_TYPES) },
				},
			},
			place: {
				type: "object",
				optional: true,
				item: {
					name: { type: "string" },
					address: { type: "string" },
					email: { type: "string" },
					postal_code: { type: "string" },
					city: { type: "string" },
					phone: { type: "number", positive: true, integer: true, convert: true },
					fax: { type: "number", positive: true, integer: true, convert: true },
				},
			},
			is_external: { type: "boolean" },
			description: { type: "string" },
			regulation: { type: "string" },
			avarage_time: { type: "number", positive: true, convert: true },
			payment_type: { type: "enum", values: Object.keys(PAYMENT_TYPES) },
			product_code: { type: "string" },
			place_id: { type: "number", positive: true, convert: true, optional: true },
			is_active: { type: "boolean" },
			payment_information: { type: "string" },
			organic_unit_id: { type: "number", positive: true, convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateLanguageIds",
				"validateAppointmentTypes",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					//ctx.params.place_id = await this.checkPlace(ctx);
				},
			],
			update: [
				"validateLanguageIds",
				"validateAppointmentTypes",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					//ctx.params.place_id = await this.checkPlace(ctx);
				},
			],
			remove: ["isPossibleToRemove"],
			list: [
				function addFilterByTranslations(ctx) {
					return addSearchRelation(ctx, ["name"], "health.specialty-translations", "specialty_id");
				},
			],
		},
		after: {
			create: [
				"saveTranslations",
				"saveAppointmentTypes",
				"saveDoctorsSpecialty",
				"savePriceVariations",
				"saveChecklist",
				"savePlace",
			],
			update: [
				"saveTranslations",
				"saveAppointmentTypes",
				"saveDoctorsSpecialty",
				"savePriceVariations",
				"saveChecklist",
				"savePlace",
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			params: {
				id: { type: "any" },
			},
			handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const id = this.decodeID(params.id);
				return this.adapter.db.transaction(async (trx) => {
					return this.adapter
						.db("specialty_translation")
						.transacting(trx)
						.where("specialty_id", id)
						.del()
						.then(() =>
							this.adapter
								.db("specialty_appointment_type")
								.transacting(trx)
								.where("specialty_id", id)
								.del(),
						)
						.then(() =>
							this.adapter
								.db("specialty_checklist")
								.transacting(trx)
								.where("specialty_id", id)
								.del(),
						)
						.then(() =>
							this.adapter.db("doctor_specialty").transacting(trx).where("specialty_id", id).del(),
						)
						.then(() => {
							return this.adapter
								.db("price_variation")
								.transacting(trx)
								.where("specialty_id", id)
								.del();
						})
						.then(() => {
							return this.adapter.db("specialty").transacting(trx).where("id", id).del();
						})
						.then((doc) => {
							if (!doc)
								return Promise.reject(new Errors.EntityNotFoundError("specialty", params.id));
							this.clearCache();
							return this.transformDocuments(ctx, {}, doc).then((json) =>
								this.entityChanged("REMOVED!", json, ctx).then(() => json),
							);
						});
				});
			},
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		get_doctors: {
			rest: "GET /:id/doctors",
			visibility: "published",

			handler(ctx) {
				return ctx.call("health.doctor-specialty.doctors_by_specialty", {
					specialty_id: ctx.params.id,
				});
			},
		},
		add_doctors: {
			rest: "POST /:id/doctors",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				doctors_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			handler(ctx) {
				if (ctx.params.doctors_ids && Array.isArray(ctx.params.doctors_ids)) {
					const doctors_ids = [...new Set(ctx.params.doctors_ids)];
					return ctx.call("health.doctor-specialty.save_doctors_specialties", {
						specialty_id: ctx.params.id,
						doctors_ids,
					});
				}
			},
		},
		delete_doctors: {
			rest: "DELETE /:id/doctors",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				doctors_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
			},
			handler(ctx) {
				if (ctx.params.doctors_ids && Array.isArray(ctx.params.doctors_ids)) {
					let doctors_ids = [...new Set(ctx.params.doctors_ids)];
					return ctx.call("health.doctor-specialty.delete_doctors_specialties", {
						specialty_id: ctx.params.id,
						doctors_ids,
					});
				}
			},
		},
		status: {
			rest: "POST /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				is_active: { type: "boolean" },
			},
			handler(ctx) {
				if (ctx.params.is_active) {
					//ativar
					return this._get(ctx, { id: ctx.params.id }).then((response) => {
						if (response.length > 0) {
							if (response[0].is_active) {
								throw new Errors.ValidationError(
									"Specialty already active",
									"HEALTH_SPECIALTY_ACTIVE",
								);
							}
							return ctx.call("health.specialty.patch", {
								id: ctx.params.id,
								is_active: ctx.params.is_active,
							});
						}
					});
				} else {
					//desativar
					return ctx
						.call("health.appointment.find", {
							query: {
								specialty_id: ctx.params.id,
								status: ["APPROVED", "PENDING"],
							},
						})
						.then((appointments) => {
							if (appointments.length > 0) {
								throw new Errors.ValidationError(
									"You have open appointments associated to specialty",
									"HEALTH_SPECIALTY_WITH_APPOINTMENTS",
								);
							}
							return this._get(ctx, { id: ctx.params.id });
						})
						.then((specialty) => {
							if (specialty.length > 0) {
								if (!specialty[0].is_active) {
									throw new Errors.ValidationError(
										"Specialty already deactivated",
										"HEALTH_SPECIALTY_DEACTIVATED",
									);
								}
								return ctx.call("health.specialty.patch", {
									id: ctx.params.id,
									is_active: ctx.params.is_active,
								});
							}
						});
				}
			},
		},
		getScheduleByDoctorSpecialty: {
			rest: "GET /:id/schedule",
			visibility: "published",
			params: {
				id: { type: "number", min: 0, convert: true },
				doctor_id: { type: "number", min: 0, convert: true },
				date: { type: "date", convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id }).then(async (specialty) => {
					const schedule = [];
					const holidays = await ctx
						.call("configuration.holidays.list", {})
						.then((holidays_response) => {
							return holidays_response.rows.filter((h) =>
								h.organic_units.length > 0
									? h.organic_units.find((o) => o.id === specialty[0].organic_unit_id)
									: h,
							);
						});
					const periods = await ctx
						.call("health.doctor-specialty.find", {
							query: {
								doctor_id: ctx.params.doctor_id,
								specialty_id: ctx.params.id,
							},
						})
						.then((doctors_specialties) => {
							return ctx.call("health.period.find", {
								query: {
									doctor_specialty_id: doctors_specialties.map((d) => d.id),
								},
							});
						});

					for (const period of periods) {
						const periodStartDate = moment(period.start_date, "YYYY-MM-DD");
						while (
							periodStartDate.isSameOrBefore(moment(period.end_date, "YYYY-MM-DD")) &&
							moment(periodStartDate).diff(moment(new Date()), "days") <= 15
						) {
							if (periodStartDate.isSameOrAfter(moment(new Date()).format("YYYY-MM-DD"))) {
								const checkIfDayIsHoliday = holidays.filter((h) =>
									moment(h.date, "YYYY-MM-DD").isSame(periodStartDate),
								);
								if (checkIfDayIsHoliday.length > 0) {
									const convertStartDate = periodStartDate.format("YYYY-MM-DD") + "T00:00:00";
									const obj = {};
									obj.id = convertStartDate;
									obj.title = checkIfDayIsHoliday.map(
										(h) => h.translations.find((t) => t.language_id === 3).name,
									);
									obj.start_date = convertStartDate;
									obj.is_available = false;
									obj.is_holiday = true;
									schedule.push(obj);
								} else {
									const days = period.days.filter(
										(d) => periodStartDate.day() === moment().day(d.day).day(),
									);
									if (days.length > 0) {
										for (const day of days) {
											for (const h of day.days) {
												let start_hour = moment(h.start_hour, "HH:mm:ss");
												while (start_hour.isBefore(moment(h.end_hour, "HH:mm:ss"))) {
													if (
														periodStartDate.isSame(moment(new Date()).format("YYYY-MM-DD")) &&
														start_hour.isSameOrAfter(moment(new Date(), "HH:mm:ss"))
													) {
														const appointmentCheck = await this.fillAppointmentEventObj(
															ctx,
															start_hour,
															h.end_hour,
															periodStartDate,
															specialty[0].avarage_time,
														);
														if (appointmentCheck) {
															schedule.push(appointmentCheck);
														}
													} else if (
														periodStartDate.isAfter(moment(new Date()).format("YYYY-MM-DD"))
													) {
														const appointmentCheck = await this.fillAppointmentEventObj(
															ctx,
															start_hour,
															h.end_hour,
															periodStartDate,
															specialty[0].avarage_time,
														);
														if (appointmentCheck) {
															schedule.push(appointmentCheck);
														}
													}
													start_hour.add(specialty[0].avarage_time, "minutes");
												}
											}
										}
									}
								}
							}
							periodStartDate.add(1, "day");
						}
					}
					return schedule;
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"health.appointment.*"() {
			this.clearCache();
		},
		"health.doctor.*"() {
			this.clearCache();
		},
		"health.doctor-specialty.*"() {
			this.clearCache();
		},
		"health.price-variation.*"() {
			this.clearCache();
		},
		"health.place.*"() {
			this.clearCache();
		},
		"health.checklist.*"() {
			this.clearCache();
		},
		"health.appointment-type.*"() {
			this.clearCache();
		},
		"health.specialty.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguageIds(ctx) {
			if (ctx.params.translations && Array.isArray(ctx.params.translations)) {
				const ids = ctx.params.translations.map((translation) => translation.language_id);
				await Promise.all(
					ids.map((id) => {
						ctx.call("configuration.languages.get", {
							id,
						});
					}),
				);
			}
		},

		async saveTranslations(ctx, response) {
			if (ctx.params.translations && Array.isArray(ctx.params.translations)) {
				await ctx.call("health.specialty-translations.save_translations", {
					specialty_id: response[0].id,
					translations: ctx.params.translations,
				});
				response[0].translations = await ctx.call("health.specialty-translations.find", {
					query: {
						specialty_id: response[0].id,
					},
				});
				return response;
			}
		},

		isPossibleToRemove(ctx) {
			if (ctx.params.id) {
				return ctx
					.call("health.appointment.count", {
						query: {
							specialty_id: ctx.params.id,
						},
					})
					.then((response) => {
						if (response > 0) {
							throw new Errors.ValidationError(
								"You have medical specialties associated to appointments",
								"HEALTH_SPECIALTY_WITH_APPOINTMENTS",
							);
						}
					});
			}
		},

		async validateAppointmentTypes(ctx) {
			if (ctx.params.appointment_types_ids && Array.isArray(ctx.params.appointment_types_ids)) {
				await Promise.all(
					ctx.params.appointment_types_ids.map((id) => {
						ctx.call("health.appointment-type.get", {
							id,
						});
					}),
				);
			}
		},

		async saveAppointmentTypes(ctx, response) {
			if (ctx.params.appointment_types_ids && Array.isArray(ctx.params.appointment_types_ids)) {
				const appointment_types_ids = [...new Set(ctx.params.appointment_types_ids)];
				await ctx.call("health.specialty-appointment-type.save_specialty_appointment_types", {
					specialty_id: response[0].id,
					appointment_types_ids,
				});
				response[0].appointment_types = await ctx.call(
					"health.specialty-appointment-type.specialty_appointment_types",
					{
						specialty_id: response[0].id,
					},
				);
			}
			return response;
		},

		async saveDoctorsSpecialty(ctx, response) {
			if (ctx.params.doctors_ids && Array.isArray(ctx.params.doctors_ids)) {
				const doctors_ids = [...new Set(ctx.params.doctors_ids)];
				await ctx.call("health.doctor-specialty.save_doctors_specialties", {
					specialty_id: response[0].id,
					doctors_ids,
				});
				response[0].doctors = await ctx.call("health.doctor-specialty.doctors_by_specialty", {
					specialty_id: response[0].id,
				});
				return response;
			}
		},

		async savePriceVariations(ctx, response) {
			if (ctx.params.price_variations && Array.isArray(ctx.params.price_variations)) {
				const price_variations = [...new Set(ctx.params.price_variations)];
				await ctx.call("health.price-variations.save_price_variations", {
					specialty_id: response[0].id,
					price_variations,
				});
				response[0].price_variations = await ctx.call(
					"health.price-variations.price_variations_specialty",
					{
						specialty_id: response[0].id,
					},
				);
				return response;
			}
		},

		async saveChecklist(ctx, response) {
			if (ctx.params.checklist && Array.isArray(ctx.params.checklist)) {
				const checklist = [...new Set(ctx.params.checklist)];
				await ctx.call("health.specialty-checklist.save_questions_specialty", {
					specialty_id: response[0].id,
					checklist,
				});
				response[0].checklist = await ctx.call(
					"health.specialty-checklist.checklist_by_specialty",
					{
						specialty_id: response[0].id,
					},
				);
				return response;
			}
		},

		checkIfExistsAppointment(ctx, start_hour, date, avarage_time) {
			const convertedHour = moment(start_hour, "HH:mm:ss");
			const end_hour = convertedHour.add(avarage_time, "minutes").format("HH:mm:ss");
			return ctx
				.call("health.appointment.find", {
					query: {
						doctor_id: ctx.params.doctor_id,
						specialty_id: ctx.params.id,
						date: moment(date).format("YYYY-MM-DD"),
						start_hour: start_hour,
						end_hour: end_hour,
						status: ["APPROVED", "PENDING", "STARTED"],
					},
				})
				.then((appointment) => {
					if (appointment.length > 0) {
						return false;
					}
					return true;
				});
		},

		async fillAppointmentEventObj(ctx, start_hour, end_hour, periodStartDate, avarage_time) {
			const hourAvailable = await this.checkIfExistsAppointment(
				ctx,
				start_hour.format("HH:mm:ss"),
				periodStartDate,
				avarage_time,
			);
			const minutesDiff = moment(end_hour, "HH:mm:ss").diff(moment(start_hour), "minutes");
			if (minutesDiff >= avarage_time) {
				let end_hour_converted = start_hour.valueOf();
				end_hour_converted = moment(end_hour_converted).add(avarage_time, "minutes");
				const convertStartDate =
					periodStartDate.format("YYYY-MM-DD") + "T" + start_hour.format("HH:mm:ss");
				const convertEndDate =
					periodStartDate.format("YYYY-MM-DD") + "T" + end_hour_converted.format("HH:mm:ss");
				const obj = {};
				obj.id = convertStartDate;
				obj.start_date = moment(convertStartDate);
				obj.end_date = moment(convertEndDate);
				obj.title = hourAvailable ? "Disponivel" : "Indisponivel";
				obj.is_available = hourAvailable;
				return obj;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
