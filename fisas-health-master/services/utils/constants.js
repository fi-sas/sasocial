module.exports = {
	GENDER: {
		M: "M",
		F: "F",
		O: "O",
	},
	DAYS: {
		Sunday: "Sunday",
		Monday: "Monday",
		Tuesday: "Tuesday",
		Wednesday: "Wednesday",
		Thursday: "Thursday",
		Friday: "Friday",
		Saturday: "Saturday",
	},
	ANSWER_TYPES: {
		boolean: "boolean",
		text: "text",
		number: "number",
		date: "date",
	},
	PAYMENT_TYPES: {
		"Ato Consulta": "Ato Consulta",
		"Pré Pagamento": "Pré Pagamento",
	},
	APPOINTMENT_STATUS: {
		CLOSED: "CLOSED",
		PENDING: "PENDING",
		APPROVED: "APPROVED",
		CHANGED: "CHANGED",
		MISSED: "MISSED",
		CANCELED: "CANCELED",
		STARTED: "STARTED",
	},
	COMPLAINT_STATUS: {
		SOLVED: "SOLVED",
		PENDING: "PENDING",
		CREATED: "CREATED",
	},
	ATTENDANCE_STATUS: {
		STARTED: "STARTED",
		CLOSED: "CLOSED",
	},

	CONFIGURATION_KEYS: {
		EXTERNAL_PROFILE: "EXTERNAL_PROFILE",
		CURRENT_ACCOUNT_ID: "CURRENT_ACCOUNT_ID",
	},
};
