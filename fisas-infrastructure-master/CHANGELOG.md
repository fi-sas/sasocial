## [1.4.1](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.4.0...v1.4.1) (2022-04-28)


### Bug Fixes

* **infrastructure:** set dgs code as optional ([4a4b38d](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4a4b38d217a6c72ce370100eb314e8738a8e3818))

# [1.4.0](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.3.0...v1.4.0) (2021-08-19)


### Features

* **organic_units:** add dgs_code field ([74947fa](https://gitlab.com/fi-sas/fisas-infrastructure/commit/74947fa9c579305327908ff8361bd74727b0461d))

# [1.3.0](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.2.2...v1.3.0) (2021-07-16)


### Features

* **organic_units:** add address fields to organic units ([d118899](https://gitlab.com/fi-sas/fisas-infrastructure/commit/d118899aa39230d68366fec28c488dd8a0ee7ad0))

## [1.2.2](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.2.1...v1.2.2) (2021-07-08)


### Bug Fixes

* **various:** add dependents validation on remove ([8a31e34](https://gitlab.com/fi-sas/fisas-infrastructure/commit/8a31e341341a67f0e258b6d8315837b4d540f7a0))

## [1.2.1](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.2.0...v1.2.1) (2021-05-27)


### Bug Fixes

* **infrastructure:** fix deep search helper ([f18c072](https://gitlab.com/fi-sas/fisas-infrastructure/commit/f18c0726555a363e328d5b2f42cb7f1d959ce839))

# [1.2.0](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.1.1...v1.2.0) (2021-05-13)


### Features

* **organic-units:** add field external_ref ([b3a3644](https://gitlab.com/fi-sas/fisas-infrastructure/commit/b3a3644572cb9d7b7430f6a188d74f650382f459))

## [1.1.1](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.1.0...v1.1.1) (2021-03-29)


### Bug Fixes

* **services:** fix search by floor_id ([680b043](https://gitlab.com/fi-sas/fisas-infrastructure/commit/680b0435c258e3125bf54892de022d055be0710a))

# [1.1.0](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0...v1.1.0) (2021-03-19)


### Bug Fixes

* **helper:** fix lint errors ([a0fb24c](https://gitlab.com/fi-sas/fisas-infrastructure/commit/a0fb24cbe643e9a1b7f4fca822f44a2ac7348354))
* **seeds:** add missing sequence updater ([4160fc7](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4160fc75cb44793b27e4b603d638fcc484f85207))
* **seeds:** remove delete from seeds ([67328a4](https://gitlab.com/fi-sas/fisas-infrastructure/commit/67328a4bf49c5e1d0332bedb6e23ba35eab7aec9))


### Features

* add search through multiple services ([29013d8](https://gitlab.com/fi-sas/fisas-infrastructure/commit/29013d87b6828fb931de745990c8e0dcd1927585))
* **search:** inital search by multiple services ([b495a70](https://gitlab.com/fi-sas/fisas-infrastructure/commit/b495a70464a76dcb8bd2c313287ed48139f51dbc))

# 1.0.0 (2021-03-09)


### Bug Fixes

* actions visibility changed to published ([7c7bbf0](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7c7bbf08340c8f316ed0d18dc6b5e4d0457b5db8))
* added organic unit filter by ID ([e21770e](https://gitlab.com/fi-sas/fisas-infrastructure/commit/e21770e4654b6f919c9a002c4d0c0d34cf162aa9))
* bugs in asset-documents, assest ([e835584](https://gitlab.com/fi-sas/fisas-infrastructure/commit/e835584bd46a43db676f1abab8a962a3006f5aca))
* error in call services ([7735c47](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7735c470cd0a93429efc0a97e5b8a2c7667136f5))
* file migrations ([ce6024e](https://gitlab.com/fi-sas/fisas-infrastructure/commit/ce6024eca64efb5aaa4ee57664af85a2d7d09dde))
* fixing node version (node:12-buster-slim) ([c04ebfb](https://gitlab.com/fi-sas/fisas-infrastructure/commit/c04ebfbbcc2c9ab913e43129f5d9a6273b7a2075))
* migration and asset ([4d8e4fa](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4d8e4fac428ca016cabc48a82ee1fb0a7219f964))
* migration file and sample_seeds ([431f0a6](https://gitlab.com/fi-sas/fisas-infrastructure/commit/431f0a69f3410f606d2c624e73e41b761cb27d84))
* migration file and seeds ([55eff0a](https://gitlab.com/fi-sas/fisas-infrastructure/commit/55eff0a4e03f9534859d761ee88c475a4bca0331))
* migration files, seeds and simple services ([ddc33a4](https://gitlab.com/fi-sas/fisas-infrastructure/commit/ddc33a4de550c68e79c944ce50f9cae5360787cf))
* package files ([deda6c5](https://gitlab.com/fi-sas/fisas-infrastructure/commit/deda6c53805578ac3ece1a6e88b1bf82b07e6aa9))
* seed organic_unit ([7282a8a](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7282a8a764ceb23166c2f6cda2971330d4b7171e))
* small fix ([f33543c](https://gitlab.com/fi-sas/fisas-infrastructure/commit/f33543c943b73c8788a73cbf00251ae4643a3288))
* small fix in services ([09bc74f](https://gitlab.com/fi-sas/fisas-infrastructure/commit/09bc74fd152e06430004529301fc0c3048af16fb))
* verifications on path ([9af82b0](https://gitlab.com/fi-sas/fisas-infrastructure/commit/9af82b084363e620e686c34c2c1ea1d21b9986dc))
* **docker-compose:** create docker-compose to prod and dev ([bc76958](https://gitlab.com/fi-sas/fisas-infrastructure/commit/bc7695808dda467ea75cb4e650b4e4dc6ca84155))
* **ms_core:** update ms_core package ([9b6c054](https://gitlab.com/fi-sas/fisas-infrastructure/commit/9b6c054d41e19fe2eaa504f4da6d43c83ae90266))
* **rooms:** add code validation ([7f92658](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7f92658b228a3e4341abe104c857a3b2c93d69d1))
* **seeds:** move seed to sample seeds ([92b4c47](https://gitlab.com/fi-sas/fisas-infrastructure/commit/92b4c4738cf765bf9822a2afa55b767547d24b06))
* **services:** add code to multiples services ([4d055ad](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4d055adf60eb5c5c92e8a92e43f55a3ac92a2288))
* **services:** minor fixes ([489318c](https://gitlab.com/fi-sas/fisas-infrastructure/commit/489318c49e2617c0adde06e984949ad95b6ac5d6))


### Features

* add component-documents and location-documents ([3fb9888](https://gitlab.com/fi-sas/fisas-infrastructure/commit/3fb9888a57ebc0ea89115b07510d54e37da0fc8d))
* add create documents to assets ([7405a03](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7405a03c36c925a7a6f9582012a9f79c1d3ed48c))
* migrate from MySQL into Postgres ([47f6953](https://gitlab.com/fi-sas/fisas-infrastructure/commit/47f695377bc82472d1f018202c8cf4944c8a194b))
* **ms:** add the new boilerplate stracture ([27e0dbc](https://gitlab.com/fi-sas/fisas-infrastructure/commit/27e0dbc19ad84be29729e82d61274b8043a5a567))
* **services:** add base services of infrastructure ([7fab396](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7fab396f66c8412959e431b447056e1f888dd6b7))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-02-08)


### Bug Fixes

* seed organic_unit ([7282a8a](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7282a8a764ceb23166c2f6cda2971330d4b7171e))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-01-18)


### Bug Fixes

* package files ([deda6c5](https://gitlab.com/fi-sas/fisas-infrastructure/commit/deda6c53805578ac3ece1a6e88b1bf82b07e6aa9))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-15)


### Bug Fixes

* error in call services ([7735c47](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7735c470cd0a93429efc0a97e5b8a2c7667136f5))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-15)


### Bug Fixes

* migration file and sample_seeds ([431f0a6](https://gitlab.com/fi-sas/fisas-infrastructure/commit/431f0a69f3410f606d2c624e73e41b761cb27d84))
* migration file and seeds ([55eff0a](https://gitlab.com/fi-sas/fisas-infrastructure/commit/55eff0a4e03f9534859d761ee88c475a4bca0331))
* small fix ([f33543c](https://gitlab.com/fi-sas/fisas-infrastructure/commit/f33543c943b73c8788a73cbf00251ae4643a3288))
* small fix in services ([09bc74f](https://gitlab.com/fi-sas/fisas-infrastructure/commit/09bc74fd152e06430004529301fc0c3048af16fb))
* verifications on path ([9af82b0](https://gitlab.com/fi-sas/fisas-infrastructure/commit/9af82b084363e620e686c34c2c1ea1d21b9986dc))


### Features

* add component-documents and location-documents ([3fb9888](https://gitlab.com/fi-sas/fisas-infrastructure/commit/3fb9888a57ebc0ea89115b07510d54e37da0fc8d))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-13)


### Bug Fixes

* bugs in asset-documents, assest ([e835584](https://gitlab.com/fi-sas/fisas-infrastructure/commit/e835584bd46a43db676f1abab8a962a3006f5aca))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-13)


### Bug Fixes

* file migrations ([ce6024e](https://gitlab.com/fi-sas/fisas-infrastructure/commit/ce6024eca64efb5aaa4ee57664af85a2d7d09dde))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-12)


### Bug Fixes

* migration and asset ([4d8e4fa](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4d8e4fac428ca016cabc48a82ee1fb0a7219f964))
* migration files, seeds and simple services ([ddc33a4](https://gitlab.com/fi-sas/fisas-infrastructure/commit/ddc33a4de550c68e79c944ce50f9cae5360787cf))


### Features

* add create documents to assets ([7405a03](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7405a03c36c925a7a6f9582012a9f79c1d3ed48c))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-28)


### Bug Fixes

* **seeds:** move seed to sample seeds ([92b4c47](https://gitlab.com/fi-sas/fisas-infrastructure/commit/92b4c4738cf765bf9822a2afa55b767547d24b06))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-21)


### Bug Fixes

* **services:** minor fixes ([489318c](https://gitlab.com/fi-sas/fisas-infrastructure/commit/489318c49e2617c0adde06e984949ad95b6ac5d6))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-infrastructure/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-21)


### Bug Fixes

* **services:** add code to multiples services ([4d055ad](https://gitlab.com/fi-sas/fisas-infrastructure/commit/4d055adf60eb5c5c92e8a92e43f55a3ac92a2288))

# 1.0.0-rc.1 (2020-12-18)


### Bug Fixes

* **ms_core:** update ms_core package ([9b6c054](https://gitlab.com/fi-sas/fisas-infrastructure/commit/9b6c054d41e19fe2eaa504f4da6d43c83ae90266))
* fixing node version (node:12-buster-slim) ([c04ebfb](https://gitlab.com/fi-sas/fisas-infrastructure/commit/c04ebfbbcc2c9ab913e43129f5d9a6273b7a2075))
* **rooms:** add code validation ([7f92658](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7f92658b228a3e4341abe104c857a3b2c93d69d1))
* actions visibility changed to published ([7c7bbf0](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7c7bbf08340c8f316ed0d18dc6b5e4d0457b5db8))
* added organic unit filter by ID ([e21770e](https://gitlab.com/fi-sas/fisas-infrastructure/commit/e21770e4654b6f919c9a002c4d0c0d34cf162aa9))
* **docker-compose:** create docker-compose to prod and dev ([bc76958](https://gitlab.com/fi-sas/fisas-infrastructure/commit/bc7695808dda467ea75cb4e650b4e4dc6ca84155))


### Features

* migrate from MySQL into Postgres ([47f6953](https://gitlab.com/fi-sas/fisas-infrastructure/commit/47f695377bc82472d1f018202c8cf4944c8a194b))
* **ms:** add the new boilerplate stracture ([27e0dbc](https://gitlab.com/fi-sas/fisas-infrastructure/commit/27e0dbc19ad84be29729e82d61274b8043a5a567))
* **services:** add base services of infrastructure ([7fab396](https://gitlab.com/fi-sas/fisas-infrastructure/commit/7fab396f66c8412959e431b447056e1f888dd6b7))
