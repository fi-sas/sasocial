module.exports.up = async (db) =>
	db.schema
		.createTable("organic_unit", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("building", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.string("address", 255).notNullable();
			table.string("district", 255).notNullable();
			table.string("city", 255).notNullable();
			table.string("locality", 255).notNullable();
			table.string("postal_code", 8).notNullable();
			table.double("area").notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("organic_unit_id").unsigned().references("id").inTable("organic_unit");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("wing", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("building_id").unsigned().references("id").inTable("building");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("floor", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("wing_id").unsigned().references("id").inTable("wing");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("room", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("floor_id").unsigned().references("id").inTable("floor");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("service", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("room_id").unsigned().references("id").inTable("room");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("department", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.string("code", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("location", (table) => {
			table.increments();
			table.string("code", 255).notNullable();
			table.string("description", 255).notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("organic_unit_id").unsigned().references("id").inTable("organic_unit");
			table.integer("building_id").nullable().unsigned().references("id").inTable("building");
			table.integer("wing_id").nullable().unsigned().references("id").inTable("wing");
			table.integer("floor_id").nullable().unsigned().references("id").inTable("floor");
			table.integer("room_id").nullable().unsigned().references("id").inTable("room");
			table.integer("service_id").nullable().unsigned().references("id").inTable("service");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("location_gallery", (table) => {
			table.increments();
			table.integer("location_id").unsigned().references("id").inTable("location");
			table.integer("file_id").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("document_type", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.text("description");
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("area", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.text("description");
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("area_maintenance_tasks", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.string("code", 255).notNullable();
			table.text("description");
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("area_id").unsigned().references("id").inTable("area").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("category", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.text("description");
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("category_failure", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.text("description");
			table.boolean("active").notNullable().defaultTo(true);
			table.integer("category_id").unsigned().references("id").inTable("category").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("asset", (table) => {
			table.increments();
			table.string("code", 255).notNullable();
			table.string("designation", 255).notNullable();
			table.integer("area_id").unsigned().references("id").inTable("area");
			table.integer("category_id").unsigned().references("id").inTable("category");
			table.boolean("maintenance").notNullable().defaultTo(false);
			table.enum("maintenance_type", ["INTERNAL", "EXTERNAL"]).nullable();
			table.integer("location_id").unsigned().references("id").inTable("location");
			table.string("observations").nullable();
			table.datetime("acquisition_date").notNullable();
			table.double("acquisition_value").notNullable();
			table.double("electric_power").nullable();
			table.string("classifier", 255).notNullable();
			table.string("manufacturer", 255).notNullable();
			table.string("model", 255).notNullable();
			table.string("color", 255).notNullable();
			table.string("serial_number", 255).notNullable();
			table.double("length").notNullable();
			table.double("width").notNullable();
			table.double("height").notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.string("rfid").notNullable();
			table.string("barcode").notNullable();
			table.string("erp").notNullable();
			table.integer("n_sequencial").notNullable();
			table.integer("n_good").notNullable();
			table.string("initials_n_good").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("component", (table) => {
			table.increments();
			table.string("code", 255).notNullable();
			table.string("designation", 255).notNullable();
			table.integer("asset_id").unsigned().notNullable().references("id").inTable("asset");
			table.integer("area_id").unsigned().references("id").inTable("area");
			table.integer("category_id").unsigned().references("id").inTable("category");
			table.boolean("maintenance").notNullable().defaultTo(false);
			table.enum("maintenance_type", ["INTERNAL", "EXTERNAL"]).nullable();
			table.integer("location_id").unsigned().references("id").inTable("location");
			table.string("observations").nullable();
			table.datetime("acquisition_date").notNullable();
			table.double("acquisition_value").notNullable();
			table.double("electric_power").nullable();
			table.string("classifier", 255).notNullable();
			table.string("manufacturer", 255).notNullable();
			table.string("model", 255).notNullable();
			table.string("color", 255).notNullable();
			table.string("serial_number", 255).notNullable();
			table.double("length").notNullable();
			table.double("width").notNullable();
			table.double("height").notNullable();
			table.boolean("active").notNullable().defaultTo(true);
			table.string("rfid").notNullable();
			table.string("barcode").notNullable();
			table.string("erp").notNullable();
			table.integer("n_sequencial").notNullable();
			table.integer("n_good").notNullable();
			table.string("initials_n_good").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		.createTable("component_document", (table) => {
			table.increments();
			table.integer("component_id").unsigned().references("id").inTable("component").notNullable();
			table.integer("file_id").unsigned();
			table
				.integer("document_type_id")
				.unsigned()
				.references("id")
				.inTable("document_type")
				.notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("asset_document", (table) => {
			table.increments();
			table.integer("asset_id").unsigned().references("id").inTable("asset").notNullable();
			table.integer("file_id").unsigned();
			table
				.integer("document_type_id")
				.unsigned()
				.references("id")
				.inTable("document_type")
				.notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("asset")
		.dropTable("location_gallery")
		.dropTable("location")
		.dropTable("service")
		.dropTable("room")
		.dropTable("floor")
		.dropTable("wing")
		.dropTable("building")
		.dropTable("organic_unit")
		.dropTable("document_type")
		.dropTable("area")
		.dropTable("area_maintenance_tasks")
		.dropTable("category")
		.dropTable("category_failure")
		.dropTable("component")
		.dropTable("component_document")
		.dropTable("asset_document");

module.exports.configuration = { transaction: true };
