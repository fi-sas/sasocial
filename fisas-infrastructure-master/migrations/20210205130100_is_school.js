module.exports.up = async (db) =>
	db.schema.alterTable("organic_unit", (table) => {
		table.boolean("is_school").notNullable().defaultTo(false);
	});
module.exports.down = async (db) =>
	db.schema.alterTable("organic_unit", (table) => {
		table.dropColumn("is_school");
	});

module.exports.configuration = { transaction: true };
