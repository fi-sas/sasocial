module.exports.up = async (db) =>
	db.schema.alterTable("organic_unit", (table) => {
		table.string("external_ref").nullable();
	});
module.exports.down = async (db) =>
	db.schema.alterTable("organic_unit", (table) => {
		table.dropColumn("external_ref");
	});

module.exports.configuration = { transaction: true };
