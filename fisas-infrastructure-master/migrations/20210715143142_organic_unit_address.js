module.exports.up = (db) =>
    db.schema
        .alterTable("organic_unit", (table) => {
            table.string("address");
            table.string("locality");
            table.string("city");
            table.string("district");
            table.string("postal_code", 8);
            table.string("phone");
            table.string("email");
        });
module.exports.down = (db) =>
    db.schema
        .alterTable("organic_unit", (table) => {
            table.dropColumn("address");
            table.dropColumn("locality");
            table.dropColumn("city");
            table.dropColumn("district");
            table.dropColumn("postal_code");
            table.dropColumn("phone");
            table.dropColumn("email");
        });

module.exports.configuration = { transaction: true };
