module.exports.up = (db) =>
    db.schema
        .alterTable("organic_unit", (table) => {
            table.string("dgs_code");
        });
module.exports.down = (db) =>
    db.schema
        .alterTable("organic_unit", (table) => {
            table.dropColumn("dgs_code");
        });

module.exports.configuration = { transaction: true };
