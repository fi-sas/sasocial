module.exports.seed = (knex) => {


	return Promise.all([
		knex("organic_unit")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 1,
						name: "Escola Superior Tecnologia e Gestão",
						code: "ESTG",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

		knex("organic_unit")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 2,
						name: "Escola Superior de Saúde",
						code: "ESS",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("organic_unit")
			.select()
			.where("id", 3)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 3,
						name: "Escola Superior de Educação",
						code: "ESE",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("organic_unit")
			.select()
			.where("id", 4)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 4,
						name: "Escola Superior de Desporto e Lazer",
						code: "ESDL",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("organic_unit")
			.select()
			.where("id", 5)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 5,
						name: "Escola Superior de Ciências Empresariais",
						code: "ESCE",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("organic_unit")
			.select()
			.where("id", 6)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 6,
						name: "Escola Superior Agrária",
						code: "ESA",
						active: true,
						is_school: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

		knex("organic_unit")
			.select()
			.where("id", 6)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("organic_unit").insert({
						id: 7,
						name: "Serviço Acção Social",
						code: "SAS",
						active: true,
						is_school: false,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);

};
