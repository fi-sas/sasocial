module.exports.seed = (knex) => {

	return Promise.all([
		knex("building")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("building").insert({
						id: 1,
						name: "Edifício SAS",
						code: "SAS-1",
						address: "Rua dos edificions",
						district: "VISEU",
						city: "Viseu",
						locality: "VISEU",
						postal_code: "2599-000",
						area: 123,
						active: true,
						organic_unit_id: 6,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
