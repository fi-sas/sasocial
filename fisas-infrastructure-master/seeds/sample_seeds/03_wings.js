module.exports.seed = (knex) => {
	return Promise.all([
		knex("wing")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("wing").insert({
						id: 1,
						name: "Ala Norte",
						code: "SAS-1-N",
						active: true,
						building_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
