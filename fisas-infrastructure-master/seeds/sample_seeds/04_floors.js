
module.exports.seed = (knex) => {
	return Promise.all([
		knex("floor")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("floor").insert({
						id: 1,
						name: "R/C",
						code: "SAS-1-N-RC",
						active: true,
						wing_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
