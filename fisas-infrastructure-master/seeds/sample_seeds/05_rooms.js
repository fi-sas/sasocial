module.exports.seed = (knex) => {
	return Promise.all([
		knex("room")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("room").insert({
						id: 1,
						name: "Quarto 101",
						code: "SAS-1-N-RC-101",
						active: true,
						floor_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
