
module.exports.seed = (knex) => {
	return Promise.all([
		knex("service")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("service").insert({
						id: 1,
						name: "Cama 1",
						code: "SAS-1-N-RC-101-1",
						active: true,
						room_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
