
module.exports.seed = (knex) => {
	return Promise.all([
		knex("category")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("category").insert({
						id: 1,
						name: "Computadores",
						description: "Categoria de Computadores",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("category")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("category").insert({
						id: 2,
						name: "Ar-Condicionado",
						description: "Categoria de Ares-Condicionados",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
