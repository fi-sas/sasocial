module.exports.seed = (knex) => {
	return Promise.all([
		knex("location")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("location").insert({
						id: 1,
						code: "SAS-1-N-RC-101-1-1",
						description: "DESCRIÇAO",
						active: true,
						organic_unit_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
