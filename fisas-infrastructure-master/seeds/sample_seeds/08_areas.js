
module.exports.seed = (knex) => {
	return Promise.all([
		knex("area")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("area").insert({
						id: 1,
						name: "Rede Elétrica",
						description: "Área relacionada com a Rede Elétrica",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("area")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("area").insert({
						id: 2,
						name: "AVAC",
						description: "Área de Aquecimento, Ventilação e Ar-Condicionado",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
