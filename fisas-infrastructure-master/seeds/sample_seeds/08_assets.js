module.exports.seed = (knex) => {
	return Promise.all([
		knex("asset")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("asset").insert({
						id: 1,
						code: "SAS-1-N-RC-101-1-1-1",
						designation: "Cama",
						classifier: "Bens de descanso",
						area_id: 1,
						category_id: 1,
						manufacturer: "IKEA",
						model: "Malmö",
						color: "Preta",
						serial_number: "123456789",
						acquisition_date: new Date(),
						acquisition_value: 1,
						length: 200,
						width: 160,
						height: 50,
						active: true,
						location_id: 1,
						rfid: "sd",
						barcode: "asd",
						erp: "sd",
						n_sequencial: 1,
						n_good: 1,
						initials_n_good: "ss",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
