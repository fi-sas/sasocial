module.exports.seed = (knex) => {
	return Promise.all([
		knex("location_gallery")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("location_gallery").insert({
						id: 1,
						location_id: 1,
						file_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
