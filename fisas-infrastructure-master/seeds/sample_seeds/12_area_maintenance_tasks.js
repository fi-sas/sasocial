module.exports.seed = (knex) => {
	return Promise.all([
		knex("area_maintenance_tasks")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("area_maintenance_tasks").insert({
						id: 1,
						name: "Limpeza",
						description: "Limpeza do equipamento",
						code: "123-SAS-456",
						area_id: 2,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("area_maintenance_tasks")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("area_maintenance_tasks").insert({
						id: 2,
						name: "Trocar Fusíveis",
						description: "Trocar Fusíveis",
						code: "987-SAS-s46",
						area_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("area_maintenance_tasks")
			.select()
			.where("id", 3)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("area_maintenance_tasks").insert({
						id: 3,
						name: "Atualização de Componente",
						description: "Atualização de Componente",
						code: "a72-SAS-i45",
						area_id: 1,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
