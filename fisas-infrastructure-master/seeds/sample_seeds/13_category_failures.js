module.exports.seed = (knex) => {
	return Promise.all([
		knex("category_failure")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("category_failure").insert({
						id: 1,
						name: "Falha de Disco",
						description: "Disco rígido em falha",
						category_id: 1,
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("category_failure")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("category_failure").insert({
						id: 2,
						name: "Sistema Operativo",
						description: "Erro no Sistema Operativo",
						category_id: 1,
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("category_failure")
			.select()
			.where("id", 3)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("category_failure").insert({
						id: 3,
						name: "Termóstato",
						description: "Termóstato não funciona",
						category_id: 2,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
