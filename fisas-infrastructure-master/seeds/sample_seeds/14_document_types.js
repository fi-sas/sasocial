module.exports.seed = (knex) => {
	return Promise.all([
		knex("document_type")
			.select()
			.where("id", 1)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("document_type").insert({
						id: 1,
						name: "Certificado",
						description: "Certificado",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("document_type")
			.select()
			.where("id", 2)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("document_type").insert({
						id: 2,
						name: "Plano de Manutenção",
						description: "Plano de Manutenção",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
		knex("document_type")
			.select()
			.where("id", 3)
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("document_type").insert({
						id: 3,
						name: "Ficha Técnica",
						description: "Ficha Técnica",
						active: true,
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),
	]);
};
