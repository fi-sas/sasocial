"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.area-maintenance-tasks",
	table: "area_maintenance_tasks",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "area-maintenance-tasks")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "code", "description", "active", "area_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			area(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.areas", "area", "area_id", "id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 100 },
			code: { type: "string", max: 255 },
			description: { type: "string" },
			active: { type: "boolean", default: true },
			area_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.area_id) {
				await ctx.call("infrastructure.areas.get", { id: ctx.params.area_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
