"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.assets",
	table: "asset",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "assets")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"code",
			"designation",
			"area_id",
			"category_id",
			"maintenance",
			"maintenance_type",
			"location_id",
			"observations",
			"acquisition_date",
			"acquisition_value",
			"electric_power",
			"classifier",
			"manufacturer",
			"model",
			"color",
			"serial_number",
			"length",
			"width",
			"height",
			"rfid",
			"barcode",
			"erp",
			"n_sequencial",
			"n_good",
			"initials_n_good",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["documents", "location"],
		withRelateds: {
			documents(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.asset-documents", "documents", "id", "asset_id");
			},
			location(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.locations", "location", "location_id", "id");
			},
			area(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.areas", "area", "area_id", "id");
			},
			category(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.categories", "category", "category_id", "id");
			},
		},
		entityValidator: {
			code: { type: "string", max: 255 },
			designation: { type: "string", max: 255 },
			area_id: { type: "number", integer: true, positive: true },
			category_id: { type: "number", integer: true, positive: true },
			maintenance: { type: "boolean", default: false },
			maintenance_type: { type: "enum", values: ["INTERNAL", "EXTERNAL"], optional: true },
			location_id: { type: "number", positive: true, integer: true },
			observations: { type: "string", optional: true, nullable: true },
			acquisition_date: { type: "date", convert: true },
			acquisition_value: { type: "number", integer: false },
			electric_power: { type: "number", optional: true, nullable: true, integer: false },
			classifier: { type: "string", max: 255 },
			manufacturer: { type: "string", max: 255 },
			model: { type: "string", max: 255 },
			color: { type: "string", max: 255 },
			serial_number: { type: "string", max: 255 },
			length: { type: "number" },
			width: { type: "number" },
			height: { type: "number" },
			rfid: { type: "string" },
			barcode: { type: "string" },
			erp: { type: "string" },
			n_sequencial: { type: "number" },
			n_good: { type: "number" },
			initials_n_good: { type: "string" },
			active: { type: "boolean", default: true },
			documents: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						file_id: { type: "number", integer: true, positive: true },
						document_type_id: { type: "number", integer: true, positive: true },
					},
				},
			},
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications_patch",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["save_asset_documents"],
			update: ["save_asset_documents"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			await ctx.call("infrastructure.areas.get", { id: ctx.params.area_id });
			await ctx.call("infrastructure.categories.get", { id: ctx.params.category_id });
			await ctx.call("infrastructure.locations.get", { id: ctx.params.location_id });
			if (ctx.params.documents) {
				for (const document of ctx.params.documents) {
					await ctx.call("infrastructure.document-types.get", { id: document.document_type_id });
					await ctx.call("media.files.get", { id: document.file_id });
				}
			}
		},

		/**
		 * Method for verifications on patch
		 */
		async verifications_patch(ctx) {
			if (ctx.params.area_id) {
				await ctx.call("infrastructure.areas.get", { id: ctx.params.area_id });
			}
			if (ctx.params.category_id) {
				await ctx.call("infrastructure.categories.get", { id: ctx.params.category_id });
			}
			if (ctx.params.location_id) {
				await ctx.call("infrastructure.locations.get", { id: ctx.params.location_id });
			}
		},

		/**
		 * Metoh for save documents of asset
		 */
		async save_asset_documents(ctx, res) {
			if (ctx.params.documents) {
				if (ctx.params.documents.length !== 0) {
					const docs = await ctx.call("infrastructure.asset-documents.save_asset_documentations", {
						asset_id: res[0].id,
						documents: ctx.params.documents,
					});
					res[0].documents = docs;
					return res;
				} else {
					await ctx.call("infrastructure.asset-documents.remove_asset_documentations", {
						asset_id: res[0].id,
					});
					res[0].documents = [];
					return res;
				}
			}
			await ctx.call("infrastructure.asset-documents.remove_asset_documentations", {
				asset_id: res[0].id,
			});
			res[0].documents = [];
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
