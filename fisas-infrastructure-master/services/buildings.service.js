"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.buildings",
	table: "building",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "buildings")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"code",
			"address",
			"district",
			"city",
			"locality",
			"postal_code",
			"organic_unit_id",
			"area",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			organicUnit(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"infrastructure.organic-units",
					"organicUnit",
					"organic_unit_id",
					"id",
				);
			},
			wings(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.wings", "wings", "id", "building_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			code: { type: "string", max: 255 },
			address: { type: "string", max: 255 },
			district: { type: "string", max: 255 },
			city: { type: "string", max: 255 },
			locality: { type: "string", max: 255 },
			postal_code: { type: "string", max: 8 },
			area: { type: "number", integer: false },
			active: { type: "boolean", default: true },
			organic_unit_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				async function validateDependencies(ctx) {
					const wings = await ctx.call("infrastructure.wings.count", { query: { building_id: ctx.params.id } });
					if (wings > 0) {
						throw new Errors.ValidationError(
							"You need to remove dependent wings first",
							"INFRASTRUCTURE_BUILDING_REMOVE_DEPENDENT_WINGS",
							{},
						);
					}
				}
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.organic_unit_id) {
				await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
