"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.component-documents",
	table: "component_document",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "component_document")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "component_id", "file_id", "document_type_id", "created_at", "updated_at"],
		defaultWithRelateds: ["document_type", "file"],
		withRelateds: {
			document_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"infrastructure.document-types",
					"document_type",
					"document_type_id",
					"id",
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id", "id");
			},
		},
		entityValidator: {
			component_id: { type: "number", integer: true, positive: true, convert: true },
			file_id: { type: "number", integer: true, positive: true, convert: true },
			document_type_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for remove documentations of component
		 */
		remove_component_documentations: {
			params: {
				component_id: { type: "number", integer: true, positive: true, convert: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					component_id: ctx.params.component_id,
				});
				this.clearCache();
			},
		},

		/**
		 * Action for save documentations of components
		 */
		save_component_documentations: {
			params: {
				component_id: { type: "number", integer: true, positive: true, convert: true },
				documents: {
					type: "array",
					items: {
						type: "object",
						props: {
							file_id: { type: "number", integer: true, positive: true, convert: true },
							document_type_id: { type: "number", integer: true, positive: true, convert: true },
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					component_id: ctx.params.component_id,
				});
				this.clearCache();
				const resp = [];
				for (const document of ctx.params.documents) {
					document.component_id = ctx.params.component_id;
					document.created_at = new Date();
					document.updated_at = new Date();
					const document_created = await this._create(ctx, document);
					resp.push(document_created[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for clear cache
		 */
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
