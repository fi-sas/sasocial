"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.components",
	table: "component",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "components")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"code",
			"designation",
			"asset_id",
			"area_id",
			"category_id",
			"maintenance",
			"manufacturer",
			"maintenance_type",
			"location_id",
			"observations",
			"acquisition_date",
			"acquisition_value",
			"electric_power",
			"classifier",
			"model",
			"color",
			"serial_number",
			"length",
			"width",
			"height",
			"active",
			"rfid",
			"barcode",
			"erp",
			"n_sequencial",
			"n_good",
			"initials_n_good",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["location", "area"],
		withRelateds: {
			documents(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"infrastructure.component-documents",
					"documents",
					"id",
					"component_id",
				);
			},
			location(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.locations", "location", "location_id", "id");
			},
			area(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.areas", "area", "area_id", "id");
			},
			category(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.categories", "category", "category_id", "id");
			},
			galery(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"infrastructure.component-documents",
					"galery",
					"id",
					"component_id",
				);
			},
		},
		entityValidator: {
			code: { type: "string", max: 255 },
			designation: { type: "string", max: 255 },
			asset_id: { type: "number", integer: true, positive: true, convert: true },
			area_id: { type: "number", integer: true, positive: true, convert: true },
			category_id: { type: "number", integer: true, positive: true, convert: true },
			maintenance: { type: "boolean" },
			maintenance_type: {
				type: "enum",
				values: ["INTERNAL", "EXTERNAL"],
				optional: true,
				nullable: true,
			},
			location_id: { type: "number", integer: true, positive: true, convert: true },
			observations: { type: "string", optional: true, nullable: true },
			acquisition_date: { type: "date", convert: true },
			acquisition_value: { type: "number" },
			electric_power: { type: "number", optional: true, nullable: true },
			classifier: { type: "string" },
			manufacturer: { type: "string" },
			model: { type: "string", max: 255 },
			color: { type: "string", max: 255 },
			serial_number: { type: "string", max: 255 },
			length: { type: "number" },
			width: { type: "number" },
			height: { type: "number" },
			active: { type: "boolean", default: true },
			rfid: { type: "string" },
			barcode: { type: "string" },
			erp: { type: "string" },
			n_sequencial: { type: "number" },
			n_good: { type: "number" },
			documents: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						file_id: { type: "number", integer: true, positive: true },
						document_type_id: { type: "number", integer: true, positive: true },
					},
				},
			},
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications_patch",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["save_component_documents"],
			update: ["save_component_documents"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id;
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validations
		 */
		async verifications(ctx) {
			await ctx.call("infrastructure.assets.get", { id: ctx.params.asset_id });
			await ctx.call("infrastructure.areas.get", { id: ctx.params.area_id });
			await ctx.call("infrastructure.locations.get", { id: ctx.params.location_id });
			await ctx.call("infrastructure.categories.get", { id: ctx.params.category_id });
			if (ctx.params.documents) {
				for (const document of ctx.params.documents) {
					await ctx.call("infrastructure.document-types.get", { id: document.document_type_id });
					await ctx.call("media.files.get", { id: document.file_id });
				}
			}
		},

		/**
		 * Method for verifications on patch
		 */
		async verifications_patch(ctx) {
			if (ctx.params.asset_id) {
				await ctx.call("infrastructure.assets.get", { id: ctx.params.asset_id });
			}
			if (ctx.params.area_id) {
				await ctx.call("infrastructure.areas.get", { id: ctx.params.area_id });
			}
			if (ctx.params.location_id) {
				await ctx.call("infrastructure.locations.get", { id: ctx.params.location_id });
			}
			if (ctx.params.category_id) {
				await ctx.call("infrastructure.categories.get", { id: ctx.params.category_id });
			}
		},

		/**
		 * Method for save documents of component
		 */
		async save_component_documents(ctx, res) {
			if (ctx.params.documents) {
				if (ctx.params.documents.length !== 0) {
					const docs = await ctx.call(
						"infrastructure.component-documents.save_component_documentations",
						{
							component_id: res[0].id,
							documents: ctx.params.documents,
						},
					);
					res[0].documents = docs;
					return res;
				} else {
					await ctx.call("infrastructure.component-documents.remove_component_documentations", {
						component_id: res[0].id,
					});
					res[0].documents = [];
					return res;
				}
			}
			await ctx.call("infrastructure.component-documents.remove_component_documentations", {
				component_id: res[0].id,
			});
			res[0].documents = [];
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
