"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { searchByMultiService } = require("./utils/search.helper");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.floors",
	table: "floor",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "floors")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "code", "wing_id", "active", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			wing(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.wings", "wing", "wing_id", "id");
			},
			rooms(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.rooms", "rooms", "id", "floor_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			code: { type: "string", max: 255 },
			wing_id: { type: "number", integer: true, positive: true, convert: true },
			active: { type: "boolean", default: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function validateParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.organic_unit_id) {
						await searchByMultiService(ctx, "organic-units", ctx.params.query.organic_unit_id, "floors");
					}
					if (ctx.params.query.building_id) {
						await searchByMultiService(ctx, "buildings", ctx.params.query.building_id, "floors");
					}
				},
				function deleteExtraFields(ctx) {
					delete ctx.params.query.organic_unit_id;
					delete ctx.params.query.building_id;
				},
			],
			remove: [
				async function validateDependencies(ctx) {
					const rooms = await ctx.call("infrastructure.rooms.count", { query: { floor_id: ctx.params.id } });
					if (rooms > 0) {
						throw new Errors.ValidationError(
							"You need to remove dependent rooms first",
							"INFRASTRUCTURE_FLOOR_REMOVE_DEPENDENT_ROOMS",
							{},
						);
					}
				}
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Metoh for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.wing_id) {
				await ctx.call("infrastructure.wings.get", { id: ctx.params.wing_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
