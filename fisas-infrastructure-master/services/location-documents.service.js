"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.location-documents",
	table: "location_gallery",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "location-documents")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "location_id", "file_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			location_id: { type: "number", integer: true, positive: true, convert: true },
			file_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for remove documentations of location
		 */
		remove_location_documentations: {
			params: {
				location_id: { type: "number", integer: true, positive: true, convert: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					location_id: ctx.params.location_id,
				});
				this.clearCache();
			},
		},

		/**
		 * Action for save documentations of location
		 */
		save_location_documentations: {
			params: {
				location_id: { type: "number", integer: true, positive: true, convert: true },
				galery: {
					type: "array",
					items: {
						type: "number",
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					location_id: ctx.params.location_id,
				});
				this.clearCache();
				const resp = [];
				for (const document of ctx.params.galery) {
					const entity = {
						location_id: ctx.params.location_id,
						file_id: document,
						created_at: new Date(),
						updated_at: new Date(),
					};
					const docs = await this._create(ctx, entity);
					resp.push(docs[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for clear cache
		 */
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
