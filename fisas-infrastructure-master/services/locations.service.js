"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.locations",
	table: "location",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "locations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"code",
			"description",
			"organic_unit_id",
			"building_id",
			"wing_id",
			"floor_id",
			"room_id",
			"service_id",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			assets(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.assets", "assets", "id", "location_id");
			},
			services(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.services", "services", "service_id", "id");
			},
			rooms(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.rooms", "rooms", "room_id", "id");
			},
			floors(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.floors", "floors", "floor_id", "id");
			},
			wings(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.wings", "wings", "wing_id", "id");
			},
			buildings(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.buildings", "buildings", "building_id", "id");
			},
			organic_units(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"infrastructure.organic-units",
					"organic_units",
					"organic_unit_id",
					"id",
				);
			},
		},
		entityValidator: {
			code: { type: "string", max: 255, optional: true },
			description: { type: "string", max: 255 },
			organic_unit_id: { type: "number", integer: true, positive: true, convert: true },
			building_id: {
				type: "number",
				integer: true,
				positive: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			wing_id: {
				type: "number",
				integer: true,
				positive: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			floor_id: {
				type: "number",
				integer: true,
				positive: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			service_id: {
				type: "number",
				integer: true,
				positive: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			galery: {
				type: "array",
				optional: true,
				items: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			active: { type: "boolean", default: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				async function setCode(ctx) {
					ctx.params.code = await this.constructCode(ctx);
				},
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				async function setCode(ctx) {
					ctx.params.code = await this.constructCode(ctx);
				},
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: ["save_locations_documents"],
			update: ["save_locations_documents"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
			if (ctx.params.building_id) {
				const building = await ctx.call("infrastructure.buildings.get", {
					id: ctx.params.building_id,
				});
				if (building[0].organic_unit_id !== ctx.params.organic_unit_id) {
					throw new Errors.ValidationError(
						"The building not correspond the organic unit",
						"BUILDING_NOT_CORRESPOND_THE_ORGANIC_UNIT",
						{},
					);
				}
			}
			if (ctx.params.wing_id) {
				const wing = await ctx.call("infrastructure.wings.get", { id: ctx.params.wing_id });
				if (wing[0].building_id !== ctx.params.building_id) {
					throw new Errors.ValidationError(
						"The wing not correspond the building",
						"WING_NOT_CORRESPOND_THE_BUILDING",
						{},
					);
				}
			}
			if (ctx.params.floor_id) {
				const floor = await ctx.call("infrastructure.floors.get", { id: ctx.params.floor_id });
				if (floor[0].wing_id !== ctx.params.wing_id) {
					throw new Errors.ValidationError(
						"The floor not correspond the wing",
						"FLOOR_NOT_CORRESPOND_THE_WING",
						{},
					);
				}
			}
			if (ctx.params.room_id) {
				const rooms = await ctx.call("infrastructure.rooms.get", { id: ctx.params.room_id });
				if (rooms[0].floor_id !== ctx.params.floor_id) {
					throw new Errors.ValidationError(
						"The Room not correspond the floor",
						"ROOM_NOT_CORRESPOND_THE_FLOOR",
						{},
					);
				}
			}
			if (ctx.params.service_id) {
				const service = await ctx.call("infrastructure.services.get", {
					id: ctx.params.service_id,
				});
				if (service[0].room_id !== ctx.params.room_id) {
					throw new Errors.ValidationError(
						"The service not correspond the room",
						"SERVICE_NOT_CORRESPOND_THE_ROOM",
						{},
					);
				}
			}
			if (ctx.params.galery) {
				for (const document of ctx.params.galery) {
					await ctx.call("media.files.get", { id: document });
				}
			}
		},

		/**
		 * Method for construct code
		 */
		async constructCode(ctx) {
			let code = "";
			if (ctx.params.code) {
				return ctx.params.code;
			}
			if (ctx.params.organic_unit_id) {
				const organic_unit = await ctx.call("infrastructure.organic-units.get", {
					id: ctx.params.organic_unit_id,
				});
				code = code + organic_unit[0].code;
			}
			if (ctx.params.building_id) {
				const building = await ctx.call("infrastructure.buildings.get", {
					id: ctx.params.building_id,
				});
				code = code + building[0].code;
			}
			if (ctx.params.wing_id) {
				const wing = await ctx.call("infrastructure.wings.get", { id: ctx.params.wing_id });
				code = code + wing[0].code;
			}
			if (ctx.params.floor_id) {
				const floor = await ctx.call("infrastructure.floors.get", { id: ctx.params.floor_id });
				code = code + floor[0].code;
			}
			if (ctx.params.service_id) {
				const service = await ctx.call("infrastructure.services.get", {
					id: ctx.params.service_id,
				});
				code = code + service[0].code;
			}
			return code;
		},

		/**
		 * Method for save documentation of location
		 */
		async save_locations_documents(ctx, res) {
			if (ctx.params.galery) {
				if (ctx.params.galery.length !== 0) {
					const docs = await ctx.call(
						"infrastructure.location-documents.save_location_documentations",
						{
							location_id: res[0].id,
							galery: ctx.params.galery,
						},
					);
					res[0].galery = docs;
					return res;
				} else {
					await ctx.call("infrastructure.location-documents.remove_location_documentations", {
						location_id: res[0].id,
					});
					return res;
				}
			}
			await ctx.call("infrastructure.location-documents.remove_location_documentations", {
				location_id: res[0].id,
			});
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
