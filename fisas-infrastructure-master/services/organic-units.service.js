"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.organic-units",
	table: "organic_unit",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "organic-units")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"code",
			"address",
			"locality",
			"city",
			"district",
			"postal_code",
			"phone",
			"email",
			"is_school",
			"active",
			"external_ref",
			"dgs_code",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			buildings(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.buildings", "buildings", "id", "organic_unit_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			code: { type: "string", max: 255 },
			address: { type: "string" },
			locality: { type: "string" },
			city: { type: "string" },
			district: { type: "string" },
			postal_code: { type: "string" },
			phone: { type: "string" },
			email: { type: "string" },
			active: { type: "boolean", default: true },
			is_school: { type: "boolean", default: false },
			external_ref: { type: "string", max: 255, optional: true },
			dgs_code: { type: "string", optional: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
