"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { searchByMultiService } = require("./utils/search.helper");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.rooms",
	table: "room",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "rooms")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "active", "code", "floor_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			services(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.services", "services", "id", "room_id");
			},
			floor(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.floors", "floor", "floor_id", "id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			active: { type: "boolean", default: true },
			code: { type: "string", max: 255 },
			floor_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {

			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function validateParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.organic_unit_id) {
						await searchByMultiService(ctx, "organic-units", ctx.params.query.organic_unit_id, "rooms");
					}
					if (ctx.params.query.building_id) {
						await searchByMultiService(ctx, "buildings", ctx.params.query.building_id, "rooms");
					}
					if (ctx.params.query.wing_id) {
						await searchByMultiService(ctx, "wings", ctx.params.query.wing_id, "rooms");
					}
				},
				function deleteExtraFields(ctx) {
					delete ctx.params.query.organic_unit_id;
					delete ctx.params.query.building_id;
					delete ctx.params.query.wing_id;
				},
			],
			remove: [
				async function validateDependencies(ctx) {
					const rooms = await ctx.call("accommodation.rooms.count", { query: { room_id: ctx.params.id } });
					if (rooms > 0) {
						throw new Errors.ValidationError(
							"You need to remove dependent accommodation rooms first",
							"INFRASTRUCTURE_ROOM_DEPENDENT_ACCOMMDOATION_ROOMS",
							{},
						);

					}
					const services = await ctx.call("infrastructure.services.count", { query: { room_id: ctx.params.id } });
					if (services > 0) {
						throw new Errors.ValidationError(
							"You need to remove dependent services first",
							"INFRASTRUCTURE_ROOM_REMOVE_DEPENDENT_SERVICES",
							{},
						);
					}
				}
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.floor_id) {
				await ctx.call("infrastructure.floors.get", { id: ctx.params.floor_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
