"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { searchByMultiService } = require("./utils/search.helper");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.services",
	table: "service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "services")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "code", "active", "room_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			locations(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.locations", "locations", "id", "room_id");
			},
			room(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.rooms", "room", "room_id", "id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			code: { type: "string", max: 255 },
			active: { type: "boolean", default: true },
			room_id: { type: "number", integer: true, positive: true, convert: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function validateParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.organic_unit_id) {
						await searchByMultiService(ctx, "organic-units", ctx.params.query.organic_unit_id, "services");
					}
					if (ctx.params.query.building_id) {
						await searchByMultiService(ctx, "buildings", ctx.params.query.building_id, "services");
					}
					if (ctx.params.query.wing_id) {
						await searchByMultiService(ctx, "wings", ctx.params.query.wing_id, "services");
					}
					if (ctx.params.query.floor_id) {
						await searchByMultiService(ctx, "floors", ctx.params.query.floor_id, "services");
					}

				},
				function deleteExtraFields(ctx) {
					delete ctx.params.query.organic_unit_id;
					delete ctx.params.query.building_id;
					delete ctx.params.query.wing_id;
					delete ctx.params.query.floor_id;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.room_id) {
				await ctx.call("infrastructure.rooms.get", { id: ctx.params.room_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
