const deepSearch = ["organic-units", "buildings", "wings", "floors", "rooms", "services",];

const searchValues = { "organic-units": "id", buildings: "organic_unit_id", wings: "building_id", floors: "wing_id", rooms: "floor_id", services: "room_id", };

async function searchByMultiService(ctx, entity_origin, id, entity_target) {
	const params = ctx.params.query || {};
	const start = deepSearch.indexOf(entity_origin) + 1;
	const end = deepSearch.indexOf(entity_target);
	const nodes = deepSearch.slice(start, end);
	let lastSearchTerm = id;
	let result = [];
	for (let i = 0; i < nodes.length; i++) {
		let query = {};
		query[searchValues[nodes[i]]] = lastSearchTerm;
		result = await ctx.call("infrastructure.".concat(nodes[i]).concat(".find"), {
			query,
		});
		lastSearchTerm = result.map((r) => r.id);
	}
	if (params[searchValues[entity_target]]) {
		params[searchValues[entity_target]] = Array.isArray(params[searchValues[entity_target]]) ? params[searchValues[entity_target]] : [+params[searchValues[entity_target]]];
		params[searchValues[entity_target]] = matchingValuesInTwoArrays(params[searchValues[entity_target]], result.map((svc) => svc.id));
	} else {
		params[searchValues[entity_target]] = result.map((svc) => svc.id);
	}
	ctx.params.query = params;
}
function matchingValuesInTwoArrays(arr1, arr2) {
	let ret = [];
	for (let i of arr1) {
		if (arr2.indexOf(i) > -1) {
			ret.push(i);
		}
	}
	return ret;
}
module.exports = { searchByMultiService };
