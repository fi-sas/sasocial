"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { searchByMultiService } = require("./utils/search.helper");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "infrastructure.wings",
	table: "wing",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("infrastructure", "wings")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "code", "building_id", "active", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			building(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.buildings", "building", "building_id", "id");
			},
			floors(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "infrastructure.floors", "floors", "id", "wing_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 255 },
			code: { type: "string", max: 255 },
			building_id: { type: "number", integer: true, positive: true, convert: true },
			active: { type: "boolean", default: true },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"verifications",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function validateParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.organic_unit_id) {
						return searchByMultiService(ctx, "organic-units", ctx.params.query.organic_unit_id, "wings");
					}
				},
				function deleteExtraFields(ctx) {
					delete ctx.params.query.organic_unit_id;
				},
			],
			remove: [
				async function validateDependencies(ctx) {
					const floors = await ctx.call("infrastructure.floors.count", { query: { wing_id: ctx.params.id } });
					if (floors > 0) {
						throw new Errors.ValidationError(
							"You need to remove dependent floors first",
							"INFRASTRUCTURE_WING_REMOVE_DEPENDENT_FLOORS",
							{},
						);
					}
				}
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for verifications
		 */
		async verifications(ctx) {
			if (ctx.params.building_id) {
				await ctx.call("infrastructure.buildings.get", { id: ctx.params.building_id });
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
