## [1.0.1](https://gitlab.com/fi-sas/fisas-measurements-monitorization/compare/v1.0.0...v1.0.1) (2021-07-28)


### Bug Fixes

* **measurement:** get password from file in production ([0a6d72b](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/0a6d72b9fb7f818e8ab9112b2ebc25220942f6db))

# 1.0.0 (2021-07-27)


### Bug Fixes

* **measurement:** fix unit validation ([ac2a920](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/ac2a92006843eee11b0f0734609969235aafa3fd))
* **measurement_configuration:** add source from equipment ([5fcaab8](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/5fcaab8a445f94d0b24c564c5f92231c697f81b4))
* **measurement_configuration_user:** update fields ([ffdb0bd](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/ffdb0bd09ba1d558d458dbcbd72ee1d085553d49))
* **measurement_configurations:** update fields and withrelateds and validations ([65be3e9](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/65be3e9629d0c6999531a3121be7ab7c1c973b45))
* **measurement_equipment:** update withrelated ([19a77a0](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/19a77a0706bee1169ff2fec3c0e9b3c981d8f85e))
* **measurement_equipments:** fields, add users, and withrelateds ([d1771d1](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/d1771d19efa526ccbc9f4838765022a63379750c))
* **measurement_source:** update fields ([544066d](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/544066d8b8b43ee1bac021416ba0b6b4f3a62b20))
* **measurements:** update location description ([d0830d6](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/d0830d6f7722cab3cb0b25839c177bb5697cee9f))
* **measuremnet_configurations:** update fields ([8ac9d85](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/8ac9d85f164cbad96cff3c6972b1a9365dc5418c))
* **mesaurement_equipment:** update field and relations ([1812a0c](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/1812a0c6f68593dcb362556ec6728df31f8fdb7d))
* **migrations:** remove delete ([5fcefc5](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/5fcefc5ca87f531a6eeb80bcfaca5abc83815943))
* **migrations:** update measurement_configuration and create measurement_group ([c815957](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/c8159575c6005ddb46d1feb70f652794e79b314c))
* **migrations:** update table measurement_unit ([d9bb031](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/d9bb031f6601d88b18bd01cc1f4262b7197f0edf))
* **modbus_cronjob:** add filter by active ([f6b3e6d](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/f6b3e6d79a04fb84cfb42b9ff5fd4f836219a371))
* **modbus_cronjob:** add get relation and update measurement fields ([d841037](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/d84103753cc11e79294909e2665c323d7114a4b4))
* **modbus_cronjob:** fix bugs ([2e3f43a](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/2e3f43ac2c8a9c78bde05093fd65795e01c361fe))
* **notification_measurement:** add validations, and update send_notifications ([8b05c5d](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/8b05c5d9abc3adebe32304f0399959203cf71c12))
* **package:** updates librarys ([fd91ae8](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/fd91ae8b166aeaad6cd44b7ed285f401cc15fe83))
* README update ([f56c286](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/f56c2862f19cc105ab0807cee62fe50c19981ab2))


### Features

* **measurement_configuration:** add endpoint get by user ([8c8e7d3](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/8c8e7d30cff8dbe75b1d1df681a958146aff7c22))
* **measurement_configuration_user:** add service ([6cd33d7](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/6cd33d737ed6cc41da485acfa0383ee351e2b639))
* **measurement_configurations:** add group measurement ([01c62cc](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/01c62ccae870365da59df476a706b4dedca14289))
* **measurement_configurations:** add new fields ([09d8238](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/09d8238aa2c74028a5baf53b454104b6432dfd26))
* **measurement_configurations:** add validations, relations, location_id ([9ed56c3](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/9ed56c3b080cc55bb7ca0311adcec98a4b5911b0))
* **measurement_equipment:** add service mesasurement equipment ([160f1d3](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/160f1d3623dbd105aa1b5c56872553ceaa3f7f0c))
* **measurement_equipments:** add active/desactive endpoint ([8191dbb](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/8191dbbe114e2156703bc3feed5f182b0712e375))
* **measurement_group:** add new fields ([f14d674](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/f14d6749e30cde26525c1cf8a8fa550d9d58cb6e))
* **measurement_group:** create service ([0254a72](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/0254a72f279cd9ab2f2ef2e4407496302ea0983d))
* **measurement_units:** add service ([ff21fed](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/ff21fedb4edbf699a3032ebfeb2b03debeac22f7))
* **measurements:** add send notifications max and min value ([304f712](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/304f7123fd16e50138c109bb21633af56b5d049c))
* **migration:** add measurement_configuration users responsable ([3bfed3e](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/3bfed3e05e737e63c1afa3b051e8f9971d1ca305))
* **migration:** add migration create table units ([d83dd81](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/d83dd81f3af78e9918430b7f6e947621b2a80fd1))
* **migration:** add table, notification_measurement_configuration ([74480a3](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/74480a365e61b65b514e73115534a2d7c92c210d))
* **migration:** create table measurement equipment ([a527fac](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/a527fac6237c04ee3d06ae1632c3d4eae591b542))
* **migrations:** add fields actives ([2c7d263](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/2c7d263f2b52fe9df276e1025fd85f74b12d3f60))
* **migrations:** add location_id in measurement configuration ([dd06855](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/dd068557b0bd6db754ef3bbb1847f344d407b555))
* **migrations:** add new fields measurement_configuration ([8052976](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/80529762e835d3fe9c16181790780cb5fd53c270))
* **migrations:** update table, measurement_configuration, measurement_equipment, measurement_configuration_user ([ef73a69](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/ef73a693ba9ccc7ad9bbdf07e9694ce6c5c42fdd))
* **notification_measurement:** update action send notification ([8698643](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/86986430e8fe9f73a5cec41441a995b58579b4d2))
* **notification_measurements:** add withrelated ([b81204f](https://gitlab.com/fi-sas/fisas-measurements-monitorization/commit/b81204ff0c403f1b5b7b9762f8f5f5d6f345a68a))
