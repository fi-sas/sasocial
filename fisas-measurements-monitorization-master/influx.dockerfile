FROM influxdb:1.8.4

COPY influx_pre_entry.sh /pre-entry.sh
RUN chmod +x ./pre-entry.sh
ENTRYPOINT ["/pre-entry.sh"]
CMD ["influxd"]