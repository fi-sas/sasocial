module.exports.up = async (db) =>
	db.schema
		.createTable("measurement_source", (table) => {
			table.increments();
			table.string("name", 250).notNullable();
			table.enu("type", ["modbus/tcp"]).notNullable();
			table.json("connection_data").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
			table.unique("type");
		})
		.createTable("measurement_configuration", (table) => {
			table.increments();
			table
				.integer("measurement_source_id")
				.unsigned()
				.references("id")
				.inTable("measurement_source");
			table.string("name", 250).notNullable();
			table.string("key", 250).notNullable();
			table.string("location", 250).notNullable();
			table.enu("type", ["measure", "alert"]).notNullable();
			table.float("min_value").nullable();
			table.float("max_value").nullable();
			table.enu("unit", ["kWh", "V", "A", "Hz", "PF", "kW", "%"]).nullable();
			table.enu("relevance", ["high", "medium", "low"]).nullable();
			table.json("modbus_data").nullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
			table.unique("key");
		});

module.exports.down = async (db) => db.schema.dropTable("measurement");

module.exports.configuration = { transaction: true };
