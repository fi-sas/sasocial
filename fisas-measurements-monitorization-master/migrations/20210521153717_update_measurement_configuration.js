module.exports.up = async (db) =>
	db.schema.alterTable("measurement_configuration", (table) => {
		table.dropColumn("location");
		table.integer("location_id");
	});

module.exports.down = async (db) =>
	db.schema.alterTable("measurement_configuration", (table) => {
		table.string("location", 250).notNullable();
		table.dropColumn("location_id");
	});

module.exports.configuration = { transaction: true };
