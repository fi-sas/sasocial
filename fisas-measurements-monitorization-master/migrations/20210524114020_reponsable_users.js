module.exports.up = async (db) =>
	db.schema.createTable("measurement_configuration_user", (table) => {
        table.increments();
        table.integer("measurement_configuration_id").notNullable().unsigned()
        .references("id")
        .inTable("measurement_configuration");
        table.integer("user_id").notNullable().unsigned();
        table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
    });

module.exports.down = async (db) =>
	db.schema.dropTable("measurement_configuration_user")

module.exports.configuration = { transaction: true };
