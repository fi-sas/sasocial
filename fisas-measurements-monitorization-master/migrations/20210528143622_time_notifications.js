module.exports.up = async (db) =>
	db.schema.alterTable("measurement_configuration", (table) => {
        table.integer("minutes_notifications").unsigned().nullable();
	})
    .createTable("notification_measurement_configuration", (table) => {
        table.increments();
        table.integer("measurement_configuration_id").notNullable().unsigned()
        .references("id")
        .inTable("measurement_configuration");
        table.enum("alert_type", ["MIN", "MAX"])
        table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
    });

module.exports.down = async (db) =>
	db.schema.alterTable("measurement_configuration", (table) => {
		table.dropColumn("minutes_notifications");
	})
    .dropTable("notification_measurement_configuration")

module.exports.configuration = { transaction: true };
