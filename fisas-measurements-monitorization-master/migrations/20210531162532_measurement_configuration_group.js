
module.exports.up = async (db) =>
db.schema
.createTable("measurement_group", (table) => {
    table.increments();
    table.string("description").notNullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
})

.alterTable("measurement_configuration", (table) => {
    table.integer("measurement_group_id").nullable().unsigned()
    .references("id")
    .inTable("measurement_group");
});

module.exports.down = async (db) =>
db.schema.alterTable("measurement_configuration", (table) => {
    table.dropColumn("measurement_group_id");
})
.dropTable("measurement_group")

module.exports.configuration = { transaction: true };

