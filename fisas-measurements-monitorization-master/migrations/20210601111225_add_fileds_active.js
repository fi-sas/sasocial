
module.exports.up = async (db) =>
db.schema
.alterTable("measurement_configuration", (table) => {
    table.boolean("active").defaultTo(true);
})
.alterTable("measurement_group", (table) => {
    table.boolean("active").defaultTo(true)
})

module.exports.down = async (db) =>
db.schema.alterTable("measurement_configuration", (table) => {
    table.dropColumn("active");
})
.alterTable("measurement_group", (table) => {
    table.dropColumn("active");
})

module.exports.configuration = { transaction: true };

