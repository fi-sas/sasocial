
module.exports.up = async (db) =>
db.schema
.alterTable("measurement_configuration", (table) => {
    table.string("mark").nullable();
    table.string("model").nullable();
    table.string("serial_number").nullable();
    table.integer("scouting_certificate_file_id").nullable();
    table.datetime("validity").nullable();
});

module.exports.down = async (db) =>
db.schema.alterTable("measurement_configuration", (table) => {
    table.dropColumn("mark");
    table.dropColumn("model");
    table.dropColumn("serial_number");
    table.dropColumn("scouting_certificate_file_id");
    table.dropColumn("validity");
});

module.exports.configuration = { transaction: true };

