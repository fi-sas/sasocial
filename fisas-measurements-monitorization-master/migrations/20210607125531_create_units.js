
module.exports.up = async (db) =>
db.schema
.createTable("measurement_unit", (table) => {
    table.increments();
    table.string("name").notNullable();
    table.string("acronym").notNullable();
    table.string("active").notNullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
});

module.exports.down = async (db) =>
db.schema.dropTable("measurement_unit");

module.exports.configuration = { transaction: true };
