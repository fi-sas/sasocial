
module.exports.up = async (db) =>
db.schema
.createTable("measurement_equipment", (table) => {
    table.increments();
    table.string("name").notNullable();
    table.string("mark").notNullable();
    table.string("model").notNullable();
    table.string("serial_number").notNullable();
    table.integer("scouting_certificate_file_id").nullable();
    table.datetime("validity").nullable();
    table.integer("location_id").notNullable();
    table.integer("measurement_group_id").notNullable().unsigned()
    .references("id")
    .inTable("measurement_group");
    table.boolean("active").notNullable().defaultTo(true);
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
});

module.exports.down = async (db) =>
db.schema.dropTable("measurement_equipment");

module.exports.configuration = { transaction: true };
