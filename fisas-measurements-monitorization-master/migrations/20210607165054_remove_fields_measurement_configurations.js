
module.exports.up = async (db) =>
db.schema
.alterTable("measurement_configuration", (table) => {
    table.dropColumn("mark");
    table.dropColumn("model");
    table.dropColumn("serial_number");
    table.dropColumn("scouting_certificate_file_id");
    table.dropColumn("validity");
    table.dropColumn("location_id");
    table.dropColumn("measurement_group_id");
    table.dropColumn("unit");
    table.integer("unit_id").unsigned().references("id").inTable("measurement_unit");
    table.integer("measurement_equipment_id").unsigned().references("id").inTable("measurement_equipment");
})
.alterTable("measurement_equipment", (table) => {
    table.integer("measurement_source_id").unsigned().references("id").inTable("measurement_source");
})
.alterTable("measurement_configuration_user", (table) => {
    table.dropColumn("measurement_configuration_id");
    table.integer("measurement_equipment_id").references("id").inTable("measurement_equipment");
})
.renameTable('measurement_configuration_user', 'measurement_equipment_user')

module.exports.down = async (db) =>
db.schema.alterTable("measurement_configuration", (table) => {
    table.string("mark").nullable();
    table.string("model").nullable();
    table.string("serial_number").nullable();
    table.integer("scouting_certificate_file_id").nullable();
    table.enu("unit", ["kWh", "V", "A", "Hz", "PF", "kW", "%"]).nullable();
    table.datetime("validity").nullable();
    table.integer("location_id");
    table.integer("measurement_group_id").nullable().unsigned()
    .references("id")
    .inTable("measurement_group");
    table.dropColumn("unit_id");
    table.dropColumn("measurement_equipment_id");
})
.alterTable("measurement_equipment", (table) => {
    table.dropColumn("measurement_source_id");
})
.alterTable("measurement_configuration_user", (table) => {
    table.dropColumn("measurement_equipment_id");
    table.integer("measurement_configuration_id").references("id").inTable("measurement_configuration");
})
.renameTable('measurement_equipment_user', 'measurement_configuration_user')

module.exports.configuration = { transaction: true };
