

module.exports.up = async (db) =>
db.schema
.alterTable("measurement_unit", (table) => {
    table.boolean("active").notNullable().alter();
});

module.exports.down = async (db) =>
db.schema.alterTable("measurement_unit", (table) => {
    table.string("active").notNullable().alter()
});

module.exports.configuration = { transaction: true };

