"use strict";
const { KnexAdpater } = require("@fisas/ms_core").Mixins;
const Influx = require("influx");
const _ = require("lodash");
const Validator = require("fastest-validator");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurements",

	adapter: new KnexAdpater(require("../knexfile")),

	mixins: [],

	// https://node-influx.github.io/
	client: null,

	/**
	 * Settings
	 */
	settings: {},

	metadata: {},

	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Create a new entity (save data to influxDB).
		 *
		 * @actions
		 *
		 * @param {Object} params - Entity to save.
		 *
		 * @returns {Object} Saved entity.
		 */
		create: {
			visibility: "published",
			rest: "POST /",
			scope: null,
			async handler(ctx) {
				const params = ctx.params;
				const location_name = ctx.params.tags.location.description;
				delete params.tags.location;
				params.tags.location = location_name;
				const influxMeasure = _.assign({}, params, { timestamp: new Date() });
				return await this.writeDataToInflux(ctx, influxMeasure);
			},
		},

		/**
		 * Get all measures (InfluxDB).
		 *
		 * @actions
		 *
		 * @returns {Array<Object>} List of found measures.
		 */
		list: {
			visibility: "published",
			rest: "GET /",
			scope: null,
			async handler(ctx) {
				return await this.client.query("SHOW MEASUREMENTS");
			},
		},

		/**
		 * Get all values for a measure (InfluxDB).
		 *
		 * @actions
		 *
		 * @param {string} influxdb_measure - Measure key/identifier for a InfluxDB Entity.
		 *
		 * @returns {Array<Object>}  List of found values for a measure.
		 */
		get: {
			visibility: "published",
			rest: "GET /:influxdb_measure",
			scope: null,
			async handler(ctx) {
				// TODO implement filtering behaviour ('where', 'limit' and etc)
				const params = ctx.params;
				return await this.client.query(`SELECT * FROM ${params.influxdb_measure}`);
			},
		},

		get_equipment: {
			visibility: "published",
			rest: "GET /equipment/:id",
			scope: null,
			params: {
				id: { type: "number", integer: true, convert: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				const sensors = await ctx.call("measurements_monitorization.measurement_configuration.find", {
					query: {
						measurement_equipment_id: ctx.params.id
					}, withRelated: "unit"
				});
				const list_key = sensors.map((elemt) => elemt.key);
				let list_resp = [];
				for (const sensor of sensors) {
					if (sensor.key !== "54_bits") {
						const measurement = await this.client.query("SELECT LAST(measure_value) FROM " + sensor.key);
						const data = {
							unit_name: sensor.unit.name,
							unit_acronym: sensor.unit.acronym,
							name: sensor.name,
							id: sensor.id,
							max: sensor.max_value,
							min: sensor.min_value,
							measurement: measurement
						}
						list_resp.push(data);
					}
				}
				return list_resp;
			},

		},

		//action for get min and max 
		"get_max_min": {
			visibility: "published",
			rest: "GET /sensor-max-min/:id",
			scope: null,
			params: {
				id: { type: "number", integer: true, convert: true },
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
				filter: { type: "enum", values: ["NO_FILTER", "BY_DAY", "BY_MONTH", "BY_HOURS"] }
			},
			async handler(ctx) {
				const start_date = new Date(ctx.params.start_date).toISOString();
				const end_date = new Date(ctx.params.end_date).toISOString();
				const sensor = await ctx.call("measurements_monitorization.measurement_configuration.get", { id: ctx.params.id });
				if (ctx.params.filter === "NO_FILTER") {
					return await this.client.query("SELECT max(measure_value), min(measure_value), time FROM " + sensor[0].key);
				}
				if (ctx.params.filter === "BY_DAY") {
					return await this.client.query("SELECT max(measure_value), min(measure_value) FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' group by time(1d)");
				}
				if (ctx.params.filter === "BY_MONTH") {
					return await this.client.query("SELECT max(measure_value), min(measure_value) FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' group by time(4w)");
				}
				if (ctx.params.filter === "BY_HOURS") {
					return await this.client.query("SELECT max(measure_value), min(measure_value) FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' group by time(1h)");
				}
			}
		},

		"get-values-by-sensor": {
			visibility: "published",
			rest: "GET /values/:id",
			scope: null,
			params: {
				id: { type: "number", convert: true, integer: true },
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
				filter: { type: "enum", values: ["NO_FILTER", "BY_DAY", "AVG_BY_MONTH", "AVG_BY_HOURS"] }
			},
			async handler(ctx) {
				const start_date = new Date(ctx.params.start_date).toISOString();
				const end_date = new Date(ctx.params.end_date).toISOString();
				const sensor = await ctx.call("measurements_monitorization.measurement_configuration.get", { id: ctx.params.id });
				if (ctx.params.filter === "NO_FILTER") {
					return await this.client.query("SELECT * FROM " + sensor[0].key + " ORDER BY time DESC LIMIT 100");
				}
				if (ctx.params.filter === "BY_DAY") {
					return await this.client.query("SELECT * FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' ");
				}
				if (ctx.params.filter === "AVG_BY_MONTH") {
					return await this.client.query("SELECT mean(measure_value) FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' group by time(4w)");
				}
				if (ctx.params.filter === "AVG_BY_HOURS") {
					return await this.client.query("SELECT mean(measure_value) FROM " + sensor[0].key + " where time >= '" + start_date + "' and time <= '" + end_date + "' group by time(1h)");
				}
			}
		}

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async writeDataToInflux(ctx, data) {

			const v = new Validator();
			const schema = {
				measurement: { type: "string", min: 2, max: 512, optional: false },
				measurement_id: { type: "number", integer: true, convert: true },
				tags: {
					type: "object",
					strict: true,
					props: {
						unit: { type: "string", optional: false },
						location: { type: "string" },
					},
					optional: false,
				},
				fields: {
					type: "object",
					strict: true,
					props: { measure_value: { type: "number" } },
					optional: false,
				},
			};

			const check = v.compile(schema);

			const isValid = check({ ...data });

			if (_.isBoolean(isValid) && isValid) {
				const point = {
					measurement: data.measurement,
					tags: { ...data.tags },
					fields: { ...data.fields },
					timestamp: data.timestamp,
				};

				const measurement_configuration = await ctx.call("measurements_monitorization.measurement_configuration.get", { id: data.measurement_id });
				if (point.measurement > measurement_configuration[0].max_value) {
					await ctx.call("measurements_monitorization.notification_measurement_configuration.verify_and_send_nofication", { measurement_configuration_id: data.measurement_id, alert_type: "MAX" });
				}
				if (point.measurement < measurement_configuration[0].min_value) {
					await ctx.call("measurements_monitorization.notification_measurement_configuration.verify_and_send_nofication", { measurement_configuration_id: data.measurement_id, alert_type: "MIN" });
				}

				await this.client
					.writePoints([point], {
						database: process.env.INFLUXDB_DATABASE,
						precision: "s",
					})
					.catch((error) => {
						this.logger.error(`Error saving data to InfluxDB! ${error.stack}`);
					});

				return point;
			}

			this.logger.error(`Error parsing data to save to InfluxDB! ${JSON.stringify(isValid)}`);

			throw new Errors.ValidationError(
				"Measure data is invalid",
				"INVALID_MEASURE_WRITE_INFLUXDUB",
				{ validation: isValid },
			);
		},
		createInfluxClient() {
			return new Promise((resolve, reject) => {
				if (process.env.INFLUXDB_ADMIN_PASSWORD_FILE) {
					process.env.INFLUXDB_ADMIN_PASSWORD = fs.readFileSync(
						process.env.INFLUXDB_ADMIN_PASSWORD_FILE,
						"utf8"
					);
				}

				this.client = new Influx.InfluxDB({
					database: process.env.INFLUXDB_DB,
					host: process.env.INFLUXDB_HOST,
					port: 8086,
					username: process.env.INFLUXDB_ADMIN_USER,
					password: process.env.INFLUXDB_ADMIN_PASSWORD,
				});

				// Check InfluxDB connection
				const timeout = 5000; // 5 seconds
				this.client
					.ping(timeout)
					.then((hosts) => {
						hosts.forEach((host) => {
							if (host.online) {
								this.logger.info(
									`${host.url.host} responded in ${host.rtt}ms running ${host.version})`,
								);
							} else {
								this.logger.error(`${host.url.host} is offline :(`);
							}
						});
					})
					.then(() => {
						return this.client
							.getDatabaseNames()
							.then((names) => {
								this.logger.info(`InfluxDB databases: ${names}`);
								resolve();
							})
							.catch((error) => {
								this.logger.error("ERROR GETTING DATABASES");
								this.logger.error({ error });
								reject(error);
							});
					})
					.catch((error) => {
						this.logger.error("ERROR GETTING DATABASES");
						this.logger.error({ error });
						reject(error);
					});
			});
		},
		retryPromise(fn, retriesLeft = 3, interval = 200) {
			this.logger.info("Start trying connect INFLUX");
			return new Promise((resolve, reject) => {
				return fn()
					.then(resolve)
					.catch((error) => {
						this.logger.info("Retrying Connection wiht INFLUX");
						if (retriesLeft === 1) {
							// reject('maximum retries exceeded');
							this.logger.info("Maxximmun of retrys on connection wiht INFLUX");
							reject(error);
							return;
						}

						setTimeout(() => {
							this.logger.info("retriesLeft: ", retriesLeft);
							// Passing on "reject" is the important part
							this.retryPromise(fn, retriesLeft - 1, interval).then(resolve, reject);
						}, interval);
					});
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.retryPromise(this.createInfluxClient, 10, 5000);
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {


	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
