"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurement_equipment_user",
	table: "measurement_equipment_user",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "measurement_equipment_user")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "measurement_equipment_id", "user_id", "created_at", "updated_at"],
		defaultWithRelateds: ["user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			}
		},
		entityValidator: {
            measurement_equipment_id: { type: "number", integer: true, convert: true },
            user_id: { type: "number", integer: true, convert: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
        list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove_users: {
			params:{
				measurement_equipment_id : { type: "number", integer: true, convert: true}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					measurement_equipment_id: ctx.params.measurement_equipment_id
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
