"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurement_configuration",
	table: "measurement_configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "measurement_configuration")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"measurement_source_id",
			"name",
			"key",
			"type",
			"min_value",
			"max_value",
			"relevance",
			"modbus_data",
			"minutes_notifications",
			"active",
			"unit_id",
			"measurement_equipment_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["unit"],
		withRelateds: {
			measurement_equipment(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "measurements_monitorization.measurement_equipment", "measurement_equipment", "measurement_equipment_id");
			},
			measurement_source(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "measurements_monitorization.measurement_source", "measurement_source", "measurement_source_id");
			},
			unit(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "measurements_monitorization.measurement_unit", "unit", "unit_id");
			}
		},
		entityValidator: {
			measurement_source_id: { type: "number", optional: true },
			name: { type: "string" },
			key: { type: "string", min: 2, max: 250 },
			type: { type: "enum", values: ["measure", "alert"] },
			min_value: { type: "number", optional: true },
			max_value: { type: "number", optional: true },
			relevance: { type: "enum", values: ["high", "medium", "low"], optional: true },
			active: { type: "boolean", default: true },
			modbus_data: {
				type: "object",
				strict: true,
				props: {
					registerOffset: { type: "number", optional: true },
					registerNumber: { type: "number", optional: false },
					format: {
						type: "enum",
						values: ["Integer16", "UInteger16", "Integer32", "UInteger32", "Float", "Double"],
						optional: false,
					},
				},
			},
			minutes_notifications: { type: "number", integer:true, convert: true },
			unit_id : { type: "number", integer: true, convert: true },
			measurement_equipment_id : { type: "number", integer: true, convert: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.key = "R_" + ctx.params.modbus_data.registerNumber;
					const equipment = await ctx.call("measurements_monitorization.measurement_equipment.get", { id : ctx.params.measurement_equipment_id });
					
					await ctx.call("measurements_monitorization.measurement_unit.get", { id : ctx.params.unit_id });
					ctx.params.measurement_source_id = equipment[0].measurement_source_id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validate_key"
			],
			update: [
				async function sanatizeParams(ctx) {
					ctx.params.key = "R_" + ctx.params.modbus_data.registerNumber;
					const equipment = await ctx.call("measurements_monitorization.measurement_equipment.get", { id : ctx.params.measurement_equipment_id });
					await ctx.call("measurements_monitorization.measurement_unit.get", { id : ctx.params.unit_id });
					ctx.params.measurement_source_id = equipment[0].measurement_source_id;
					ctx.params.updated_at = new Date();
				},
			],
		}
},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		get_by_user: {
			rest: "GET /user-responsable",
			visibility: "published",
			async handler(ctx) {
				const config_user = await ctx.call("measurements_monitorization.measurement_equipment_user.find", { query: { 
					user_id: ctx.meta.user.id
				}, withRelated: false});
				if(config_user.length > 0){
					for(const equipment of config_user) {
						if(equipment.measurement_equipment_id !== null){
							const equipments = await ctx.call("measurements_monitorization.measurement_equipment.get", { id: equipment.measurement_equipment_id })
							return equipments[0];
						}
					}
				}
				else {
					const equipments = await ctx.call("measurements_monitorization.measurement_equipment.list", { limit: 5 });
					return equipments.rows[0];
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		
		/**
		 * Validate key already exist
		 * @param {*} ctx 
		 */
		async validate_key(ctx) {
			const configuration = await this._find(ctx, { query: {
				key: ctx.params.key
			}});
			if(configuration.length > 0 ){
				throw new Errors.ValidationError(
					"This key already exist",
					"MEASUREMENT_KEY_ALREADY_EXIST",
					{},
				);
			}
		},
		
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
