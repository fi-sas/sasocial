"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurement_equipment",
	table: "measurement_equipment",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "measurement_equipment")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
            "mark",
            "model",
            "serial_number",
            "scouting_certificate_file_id",
            "validity",
            "location_id",
            "measurement_group_id",
            "active",
			"measurement_source_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["users", "measurement_source", "location", "measurement_group"],
		withRelateds: {
            measurement_group(ids, docs, rule, ctx){
                return hasOne(docs, ctx, "measurements_monitorization.measurement_group", "measurement_group", "measurement_group_id");
            },
			measurement_source(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "measurements_monitorization.measurement_source", "measurement_source", "measurement_source_id");
			},
			users(ids, docs, rule, ctx){
				return hasMany(docs, ctx, "measurements_monitorization.measurement_equipment_user", "users", "id", "measurement_equipment_id");
			},
			location(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "infrastructure.locations", "location", "location_id");
			},
			measurement_config(ids, docs, rule, ctx){
				return hasMany(docs, ctx, "measurements_monitorization.measurement_configuration", "measurement_config", "id", "measurement_equipment_id");
			},
		},
		entityValidator: {
            name: { type: "string" },
            mark: { type: "string" },
            model: { type: "string" },
            serial_number: { type: "string" },
            scouting_certificate_file_id : { type: "number", integer: true, convert: true, optional: true, nullable: true },
            validity: { type: "date", convert: true, optional: true, nullable: true },
            measurement_group_id: { type: "number", convert: true, integer: true },
			measurement_source_id: { type: "number", convert: true, integer: true },
			location_id: { type: "number", convert: true, integer: true },
            active: { type: "boolean", default: true },
			users: { type: "array", optional: true, items: "number"},
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
    },
    hooks: {
        before: {
            create: [
                "validateMeasurementGroup",
                async function sanatizeParams(ctx) {
					await ctx.call("measurements_monitorization.measurement_source.get", { id : ctx.params.measurement_source_id });
                    ctx.params.created_at = new Date()
                    ctx.params.updated_at = new Date()
                }
            ],
            update:[
                "validateMeasurementGroup",
                async function sanatizeParams(ctx){
					await ctx.call("measurements_monitorization.measurement_source.get", { id : ctx.params.measurement_source_id });
                    ctx.params.updated_at = new Date();
                }
            ],
        },
		after: {
			create: [ "save_users" ],
			update: [ "save_users_updated" ],
		}
    },
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	
		active: {
			rest: "PUT /active/:id",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			visibility: "published",
			async handler(ctx){
				return await ctx.call("measurements_monitorization.measurement_equipment.patch", { id : ctx.params.id, active: true });
			}
		},
	
		desactive: {
			rest: "PUT /desactive/:id",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			visibility: "published",
			async handler(ctx){
				const equipment = await ctx.call("measurements_monitorization.measurement_equipment.patch", { id : ctx.params.id, active: false })
				const list_equipment_config = await ctx.call("measurements_monitorization.measurement_configuration.find", { 
					query: {
						measurement_equipment_id: equipment[0].id
					}
				});
				for(const config of list_equipment_config) {
					await ctx.call("measurements_monitorization.measurement_configuration.patch", { id: config.id, active: false });
				}
				return equipment;
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * validate group
		 * @param {*} ctx 
		 */
        async validateMeasurementGroup(ctx){
            await ctx.call("measurements_monitorization.measurement_group.get", { id: ctx.params.measurement_group_id });
        },
			/**
		 * Create association user and measurement configuration
		 * @param {*} ctx 
		 * @param {*} res 
		 * @returns 
		 */
		async save_users(ctx, res) {
			let list_user = [];
			if (ctx.params.users) {
				for (const user of ctx.params.users) {
					const association = await ctx.call("measurements_monitorization.measurement_equipment_user.create", {
						user_id: user,
						measurement_equipment_id: res[0].id
					})
					list_user.push(association);
				}
				res[0].users = list_user;
			}
			return res;
		},

		async save_users_updated(ctx, res){
			this.clearCache();
			await ctx.call("measurements_monitorization.measurement_equipment_user.remove_users", { measurement_equipment_id : res[0].id });
			let list_user = [];
			if(ctx.params.users) {
				for (const user of ctx.params.users){
					const association = await ctx.call("measurements_monitorization.measurement_equipment_user.create", {
						user_id : user,
						measurement_equipment_id: res[0].id
					})
					list_user.push(association);
				}
				res[0].users = list_user;
			}
			return res;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
