"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurement_source",
	table: "measurement_source",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "measurement_source")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "type", "connection_data", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			// This validation assumes that source are using { type: modbus/tcp }
			name: { type: "string" },
			type: { type: "enum", values: ["modbus/tcp"] },
			connection_data: {
				type: "object",
				strict: true,
				props: {
					host: { type: "string" },
					port: { type: "number", positive: true, default: () => 502 },
					// default_scantime: { type: "number", default: () => 3000 }, // in milliseconds
					default_timeout: { type: "number", default: () => 7000 }, // in milliseconds
				},
			},
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		find: {
			cache: false
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
