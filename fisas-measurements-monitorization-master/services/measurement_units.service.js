"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.measurement_unit",
	table: "measurement_unit",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "measurement_unit")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
            "acronym",
            "active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
            name: { type: "string" },
            acronym: { type: "string" },
            active: { type: "boolean", default: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
    },
    hooks: {
        before: {
            create: [
                "validateUnit",
                function sanatizeParams(ctx) {
                    ctx.params.created_at = new Date()
                    ctx.params.updated_at = new Date()
                }
            ],
            update:[
                function sanatizeParams(ctx){
                    ctx.params.updated_at = new Date();
                }
            ]
        }
    },
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
        async validateUnit(ctx){
            const units = await this._find(ctx, { 
                query: {
                    acronym : ctx.params.acronym
                }
            });
            if(units.length > 0 ){
                throw new Errors.ValidationError(
                    "Already exist unit with acronym",
                    "MEASUREMENT_UNITS_ACRONYM_ALREADY_EXIST",
                    {}
                );
            }
        }
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
