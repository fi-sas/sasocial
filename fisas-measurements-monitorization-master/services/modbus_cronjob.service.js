/* eslint-disable no-console */
"use strict";
const { KnexAdpater } = require("@fisas/ms_core").Mixins;
const CronJob = require("moleculer-cronjob");
const ModbusRTU = require("modbus-serial");
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.modbus_cronjob",
	adapter: new KnexAdpater(require("../knexfile")),

	mixins: [CronJob],

	// https://github.com/yaacov/node-modbus-serial#readme
	client: null,

	/**
	 * Settings
	 */
	settings: {
		cronTime: "*/45 * * * * *", // every 45 seconds
		runOnInit: true,
	},

	metadata: {
		ticksCount: 0,

		state: {
			MBS_STATE_INIT: "State init",
			MBS_STATE_IDLE: "State idle",
			MBS_STATE_NEXT: "State next",
			MBS_STATE_GOOD_READ: "State good (read)",
			MBS_STATE_FAIL_READ: "State fail (read)",
			MBS_STATE_GOOD_CONNECT: "State good (port)",
			MBS_STATE_FAIL_CONNECT: "State fail (port)",
		},

		mbsState: null,

		mbsStatus: "Initializing...",
	},

	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async onTick() {
			this.logger.info(`Tick #${++this.metadata.ticksCount}`);
			await this.runModbus();
			// if (this.metadata.ticksCount === 5) {
			// 	this.$cronjob.stop();
			// }
		},

		onComplete() {
			this.logger.info("Complete");
		},

		async runModbus() {
			// TODO nextAction flow should have in consideration control registers:
			// Bit packed information containing different status information
			// bit 1: Inactivity timeout
			// bit 2:  Store and Forward command sent
			// bit 3:  Store and Forward response sent
			// bit 4: Valid message received
			// bit 5: Parity error
			// bit 6: Frame error
			// bit 7: Overrun error
			// bit 8: Crc/Chksum error
			// bit 9: Exception Message sent
			// bit 10: Exception Message exceeds send buffer size
			// bit 11: Attempt to send Exception Message when transmit busy)
			let nextAction;

			switch (this.metadata.mbsState) {
				case this.metadata.state.MBS_STATE_INIT:
					nextAction = this.connectClient;
					break;

				case this.metadata.state.MBS_STATE_NEXT:
					nextAction = this.readModbusData;
					break;

				case this.metadata.state.MBS_STATE_GOOD_CONNECT:
					nextAction = this.readModbusData;
					break;

				case this.metadata.state.MBS_STATE_FAIL_CONNECT:
					nextAction = this.connectClient;
					break;

				case this.metadata.state.MBS_STATE_GOOD_READ:
					nextAction = this.readModbusData;
					break;

				case this.metadata.state.MBS_STATE_FAIL_READ:
					if (this.client.isOpen) {
						this.metadata.mbsState = this.metadata.state.MBS_STATE_NEXT;
					} else {
						nextAction = this.connectClient;
					}
					break;

				default:
				// nothing to do, keep scanning until actionable case
			}

			// console.log(nextAction);
			this.logger.info(`${this.metadata.mbsState}`);

			// execute "next action" function if defined
			if (nextAction !== undefined && typeof nextAction === "function") {
				await nextAction();
				// this.metadata.mbsState = this.metadata.state.MBS_STATE_IDLE;
			}
		},

		async readModbusData() {
			const measureConfigs = await this.broker.call(
				"measurements_monitorization.measurement_configuration.find",
				{
					query: { type: "measure", active: true },
					limit: false,
					withRelated: ["measurement_equipment", "unit"]
				},
			);
			for(const x of measureConfigs){
				const mbsData = x.modbus_data;
				const registerAddress = mbsData.registerNumber + (mbsData.registerOffset || 0);
				let value = null;
				switch (x.modbus_data.format) {
					case "Integer16":
						value = await this.readInteger16(registerAddress);
						break;
					case "UInteger16":
						value = await this.readUInteger16(registerAddress);
						break;
					case "Integer32":
						value = await this.readInteger32(registerAddress);
						break;
					case "UInteger32":
						value = await this.readUInteger32(registerAddress);
						break;
					case "Float":
						value = await this.readFloat(registerAddress);
						break;
					case "Double":
						value = await this.readDouble(registerAddress);
						break;
					default:
						this.logger.error(
							`Error reading register {address: ${registerAddress}, value: ${value}, timestamp: ${new Date()}}`,
						);
						value = null;
						break;
				}
				if (_.isNumber(value) && !_.isNaN(value)) {
					await this.broker.call("measurements_monitorization.measurements.create", {
						measurement: x.key,
						measurement_id: x.id,
						tags: {
							unit: x.unit.acronym,
							location: x.measurement_equipment.location,
						},
						fields: {
							measure_value: value,
						},
					});
				} else {
					this.logger.error(
						`Error creating measure {address: ${registerAddress}, value: ${value}, timestamp: ${new Date()}}`,
					);
				}
			}
		},

		async connectClient() {
			const modbus_source = await this.broker.call(
				"measurements_monitorization.measurement_source.find",
				{ query: { type: "modbus/tcp" } },
			);

			if (modbus_source && _.isArray(modbus_source) && !_.isEmpty(modbus_source)) {
				const source = modbus_source[0];
				const connection_data = {
					mbsID: source.id,
					mbsTimeout: source.connection_data.default_timeout,
					mbsHost: source.connection_data.host,
					mbsPort: source.connection_data.port,
				};

				// close port (NOTE: important in order not to create multiple connections)
				// this.client.close();

				// set requests parameters
				// this.client.setID(connection_data.mbsId);
				this.client.setTimeout(connection_data.mbsTimeout);

				// try to connect
				this.client
					.connectTCP(connection_data.mbsHost, {
						port: connection_data.mbsPort,
					})
					.then(() => {
						this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_CONNECT;
						this.metadata.mbsStatus = "Connected, wait for reading...";
						this.logger.info(`${this.metadata.mbsStatus}`);
					})
					.catch((e) => {
						this.metadata.mbsState = this.metadata.state.BS_STATE_FAIL_CONNECT;
						this.metadata.mbsStatus = e.message;
						this.logger.error(e);
					});
			}
		},

		async readInteger16(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 1)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					// swap is necessary
					buffer.swap16();
					return buffer.readInt16LE(0);
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},

		async readUInteger16(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 1)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					// swap is necessary
					buffer.swap16();
					return buffer.readUInt16LE(0);
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},

		async readInteger32(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 2)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					return buffer.readFloatBE();
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},

		async readUInteger32(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 4)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					// swap is necessary
					buffer.swap32();
					return buffer.readDoubleLE();
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},

		async readFloat(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 2)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					// swap is necessary
					buffer.swap16();
					return buffer.readFloatLE(0);
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},

		async readDouble(registerAddress) {
			// try to read data
			return this.client
				.readHoldingRegisters(registerAddress, 4)
				.then((data) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_GOOD_READ;
					this.metadata.mbsStatus = "success";
					const buffer = Buffer.from(data.buffer, "hex");
					// swap is necessary
					buffer.swap16();
					return buffer.readDoubleLE(0);
				})
				.catch((e) => {
					this.metadata.mbsState = this.metadata.state.MBS_STATE_FAIL_READ;
					this.metadata.mbsStatus = e.message;
					this.logger.error(e);
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.metadata.mbsState = this.metadata.state.MBS_STATE_INIT;
		this.client = new ModbusRTU();
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {
		if (this.client) {
			this.client.close();
		}
	},
};
