"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "measurements_monitorization.notification_measurement_configuration",
	table: "notification_measurement_configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("measurements_monitorization", "notification_measurement_configuration")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"measurement_configuration_id",
            "alert_type",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			measurement_configuration(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "measurements_monitorization.measurement_configuration", "measurement_configuration", "measurement_configuration_id");
			}
		},
		entityValidator: {
			measurement_configuration_id: { type: "number", convert: true, integer: true },
            alert_type: { type: "enum", values: ["MIN", "MAX"]},
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
    },
    hooks: {
    },
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

        "verify_and_send_nofication": {
            params: {
                measurement_configuration_id: { type: "number", integer: true, convert: true },
                alert_type : { type: "enum", values: [ "MIN", "MAX" ]},
            },
            async handler(ctx) {
                const measurement_configuration = await ctx.call("measurements_monitorization.measurement_configuration.get", { id: ctx.params.measurement_configuration_id, WithRelated:["measurement_equipment"] } );
                const last_notification = await this._find(ctx, { query: {
                    measurement_configuration_id: ctx.params.measurement_configuration_id,
                    alert_type: ctx.params.alert_type
                }, limit: 1, sort: "created_at", sortValue : "descend" })
                if(last_notification.length !== 0) {
                    const calculated_time = this.calculate_minutes(last_notification[0].updated_at);
                    if(measurement_configuration[0].minutes_notifications !== null && measurement_configuration[0].minutes_notifications !== 0){
                        if(calculated_time >= measurement_configuration[0].minutes_notifications){
							last_notification[0].updated_at = new Date();
							await this._update(ctx, last_notification[0]);
                            await this.send_notification(ctx, measurement_configuration[0].measurement_equipment.users, ctx.params.alert_type, measurement_configuration[0].name )
                        }
                    }
                    else {
                        if(calculated_time >= 60) {
							last_notification[0].updated_at = new Date();
							await this._update(ctx, last_notification[0]);
                            await this.send_notification(ctx, measurement_configuration[0].measurement_equipment.users, ctx.params.alert_type, measurement_configuration[0].name )
                        }
                    }
                }
                else {
                    const entitie =  {
                        measurement_configuration_id: measurement_configuration[0].id,
                        alert_type : ctx.params.alert_type,
                        created_at: new Date(),
                        updated_at : new Date(),
                    }
                    await this._create(ctx, entitie);
                    await this.send_notification(ctx, measurement_configuration[0].measurement_equipment.users, ctx.params.alert_type, measurement_configuration[0].name);
                }
            }
        }
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
        async send_notification(ctx, users, alert_type, equipment) {
            for(const user of users) {
                    await ctx.call("notifications.alerts.create_alert", {
                        alert_type_key: "MEASUREMENT_VALUE_ALERT_" + alert_type,
                        user_id: user.user.id,
                        user_data: {},
                        data: { name: user.user.name, equipment: equipment },
                        variables: {},
                        external_uuid: null,
                        medias: [],
                    });
            }
        },

        calculate_minutes(last_notification){
            const duration = moment.duration(moment().diff(last_notification));
            return duration.asMinutes();
        }
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
