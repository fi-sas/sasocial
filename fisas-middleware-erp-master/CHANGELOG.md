## [1.3.21](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.20...v1.3.21) (2022-06-27)


### Bug Fixes

* **erp:** error on creditnote validation duplicate ([c87e8e5](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/c87e8e5bc7daa71e9ee3496aad30eafa2f5e7b7e))

## [1.3.20](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.19...v1.3.20) (2022-06-24)


### Bug Fixes

* **erp:** check duplicated lines double cancellantion ([720eefd](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/720eefdea69b961b3248e9749b9ea26f7a4f3138))

## [1.3.19](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.18...v1.3.19) (2022-03-31)


### Bug Fixes

* **erp:** give 3 seconds interval before get document ([007bd4b](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/007bd4bc465fcab3e80ad624bf81bc8d23113e72))

## [1.3.18](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.17...v1.3.18) (2022-03-31)


### Bug Fixes

* **erp:** credit_notes multiple lines cancel ([0bc4305](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/0bc430576dce6bc9299f9c35dd25da53fb87d303))

## [1.3.17](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.16...v1.3.17) (2022-03-29)


### Bug Fixes

* logging for mitigate getOneError ([23bd644](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/23bd644f53cf88fe4cf5c02dc8b2e70eba71e526))

## [1.3.16](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.15...v1.3.16) (2022-03-17)


### Bug Fixes

* **erp:** remove delay ([2867a41](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/2867a4124178b2e1aacfa071fc261a498f55327f))

## [1.3.15](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.14...v1.3.15) (2022-03-17)


### Bug Fixes

* **erp:** fix import delay ([c7386a3](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/c7386a38ffa9466e50d4f10b871c570d69ed86df))

## [1.3.14](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.13...v1.3.14) (2022-03-11)


### Bug Fixes

* **moloni:** sanatize catchs ([cc7bc43](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/cc7bc43f3eec30a0e304420f745c8f083aa9be5e))

## [1.3.13](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.12...v1.3.13) (2022-02-23)


### Bug Fixes

* **erp:** credit note same products lines credit note ([fb3e7c6](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/fb3e7c69b0afb65996aad20df06d870594caf5e6))

## [1.3.12](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.11...v1.3.12) (2022-02-23)


### Bug Fixes

* **moloni:** fix typeerror on catch ([e7aac15](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/e7aac15c92f9531ba0a14e8a202a26d6f91362db))

## [1.3.11](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.10...v1.3.11) (2022-02-23)


### Bug Fixes

* **erp:** try create credit note with other products ([a2cd9f3](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/a2cd9f3eb160a8c421032cf515f0e0c1bbdb59f5))

## [1.3.10](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.9...v1.3.10) (2022-02-15)


### Bug Fixes

* **erp:** credit note error on multiple products ([a7f3cc7](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/a7f3cc75f563cb88b64e0d7965c4426f5969a0f2))

## [1.3.9](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.8...v1.3.9) (2021-12-10)


### Bug Fixes

* debug info ([1d0f038](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/1d0f0387e7444a5ebb8fc3b505d2e44dcdb3c313))

## [1.3.8](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.7...v1.3.8) (2021-12-10)


### Bug Fixes

* replace find of product_id ([3022381](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/302238101fbc5af48ce1f3de0180aa0613577616))

## [1.3.7](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.6...v1.3.7) (2021-12-10)


### Bug Fixes

* **credit_note:** already cancelled items ([6074409](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/6074409cb72d8af66e3cba4a8be201629fb90d5d))

## [1.3.6](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.5...v1.3.6) (2021-11-23)

### Bug Fixes

- **erp:** credit-note fails when more than one already paid ([19d9a31](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/19d9a31b082403535efd9764ad0881d1d0e9b532))

## [1.3.5](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.4...v1.3.5) (2021-11-19)

### Bug Fixes

- set expiration_date from financialDocument.expiration_at ([cccbf30](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/cccbf307ab3837b3ca197aa5886d4724133dce2d))

## [1.3.4](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.3...v1.3.4) (2021-11-18)

### Bug Fixes

- **erp:** check timeout error ([b96aa6c](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/b96aa6c6488db6d8b0879823e9b5ab9b16a0616f))

## [1.3.3](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.2...v1.3.3) (2021-11-18)

### Bug Fixes

- **erp:** fix timeout ([1b3bd6b](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/1b3bd6bf03842988acf8ba93bf03b758aa1a8229))

## [1.3.2](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.1...v1.3.2) (2021-11-18)

### Bug Fixes

- **erp:** valiate associated document credit note ([8fe6580](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/8fe658012189b4d65c3d789122e47640f9f64651))

## [1.3.1](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.3.0...v1.3.1) (2021-11-18)

### Bug Fixes

- **erp:** get first element of already_created_doc ([23e2a55](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/23e2a550f40c14ca3066c77964c5a9eea1dc5df7))

# [1.3.0](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.6...v1.3.0) (2021-11-18)

### Features

- **erp:** check document existence ([aa72b2d](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/aa72b2dc116fbfe039002f513d24f0f0c23e55b0))

## [1.2.6](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.5...v1.2.6) (2021-11-17)

### Bug Fixes

- **moloni:** add 0 sec timeout ([d4d8cfe](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/d4d8cfef32ec07b6dba42362b7efe637d50c31b4))

## [1.2.5](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.4...v1.2.5) (2021-11-17)

### Bug Fixes

- **moloni:** executeRequest set timeout 0 sec ([8105347](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/81053475e65e595e0cee04428d03dabe236532eb))

## [1.2.4](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.3...v1.2.4) (2021-11-17)

### Bug Fixes

- **erp:** remove erp timeout ([ef2584b](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/ef2584b5666cb45b30adff44305c20efc9b0051d))

## [1.2.3](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.2...v1.2.3) (2021-11-17)

### Bug Fixes

- **erp:** timeout ([2d97cbf](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/2d97cbf52070b8f634abc91da2a74baee69190da))

## [1.2.2](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.1...v1.2.2) (2021-11-16)

### Bug Fixes

- **erp:** wrong debug validation ([34b3743](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/34b3743e271d6de2717632fd776e07742c7b9ecb))

## [1.2.1](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.2.0...v1.2.1) (2021-09-10)

### Bug Fixes

- **credit_note:** associated value ([a17f543](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/a17f5430cc5c49a8c5daa4a82f32017b67ecd39b))

# [1.2.0](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.5...v1.2.0) (2021-09-07)

### Features

- **configs:** endpoint to return initial configs ([f7794c0](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/f7794c01c1a21e75de5290df9fef2aab6367283b))

## [1.1.5](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.4...v1.1.5) (2021-08-11)

### Bug Fixes

- **receipt:** set net_value ([38ec425](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/38ec425d738e4cbcaf9211ee0d12cc2103c8c5e2))

## [1.1.4](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.3...v1.1.4) (2021-08-11)

### Bug Fixes

- **customer:** catch create customer human_error ([0ccf5d1](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/0ccf5d111b6aee377c79f20806569e11cf7a9dd0))

## [1.1.3](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.2...v1.1.3) (2021-08-09)

### Bug Fixes

- **receipt:** add associated_documents ([a030833](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/a030833f18abc71a7c8796bab60a763c4e9bbc97))

## [1.1.2](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.1...v1.1.2) (2021-08-05)

### Bug Fixes

- missing protocol https ([22817f0](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/22817f09a10fabf06b052be59ce0eac82bb83981))

## [1.1.1](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.1.0...v1.1.1) (2021-08-05)

### Bug Fixes

- **debug:** add logger ([efbe487](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/efbe48777fb9737ec4b064b003b1006fe21341a7))

# [1.1.0](https://gitlab.com/fi-sas/fisas-middleware-erp/compare/v1.0.0...v1.1.0) (2021-08-04)

### Features

- **moloni:** initial development ([707e4c7](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/707e4c737f59a6a02d4291bc3baf1a5176d2a4cb))

# 1.0.0 (2021-06-28)

### Bug Fixes

- **moloni:** set base_host and base_url ([d222bef](https://gitlab.com/fi-sas/fisas-middleware-erp/commit/d222befd16e970f28755fff8e66fd3d675e17fa9))
