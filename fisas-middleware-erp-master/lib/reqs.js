/**
 * Request data from the API
 */

const axios = require("axios");

  
const { resolve, reject } = require("bluebird");
const { resolveConfig } = require("prettier");
var querystring = require("qs");

var Request = function Request(options) {
	this._configure(options);
};

Request.prototype._configure = function (options) {
	this.client_id = options.client_id;
	this.client_secret = options.client_secret;
	this.username = options.username;
	this.password = options.password;
	this.credentials = null;
	this.baseUrl = options.rest_base_url;
	this.host = options.rest_base_host;
	this.headers = options.headers;
};

Request.prototype.executeRequest = function (route, call, parameters) {

	return new Promise((resolve, reject) => {
	
	console.log("");
	console.log("# --------------------------- #");
	console.log("# --> [MOLONI] executeRequest #");
	console.log("# --------------------------- #");

	if (!this.credentials) {
		console.log("  !!! NÃO TEM CREDENCIAIS !!! ");
		console.log("  - !this.credentials: ", !this.credentials);
		console.log("  -- need call that.authenticate --  ");
	} else {
		console.log("  # TEM CREDENCIAIS # ");
		console.log("  - this.credentials: ", this.credentials);
		console.log("  - this.credentials.expires: ", this.credentials.expires);
		console.log("  - new Date(): ", new Date());
		console.log("  - new Date().now(): ", Date.now());
		console.log("  - new Date().getTime(): ", new Date().getTime());
		console.log(
			"  - (this.credentials.expires < new Date()): ",
			this.credentials.expires < Date.now()
		);
		console.log(
			"  - (this.credentials.expires < new Date().now()): ",
			this.credentials.expires < Date.now(),
		);
		console.log("  - will expires at: ", new Date(this.credentials.expires));
	}
	console.log("");

	if (
		!this.credentials ||
		!this.credentials.access_token ||
		//|| this.credentials.expires < new Date()) {
		this.credentials.expires < Date.now()
	) {
		console.log("result");
		return this.authenticate().then((result) => {

			console.log(result);

			this.credentials = result;
			
			//that.credentials.expires = new Date() + (3590*1000); //ORIGINAL
			//that.credentials.expires = new Date().getTime() + (3590*1000);
			this.credentials.expires = Date.now() + 2 * 1000; // 30 segundos
				
			return this.executeRequest(route, call, parameters);
			
		});
	} else {
		var method = "POST";
		var headers = this.headers;

		if (
			(parameters && call != "getMe") ||
			(parameters && call == "getAll" && route != "companies")
		) {
			var data = querystring.stringify(parameters);
			headers["Content-Type"] = "application/x-www-form-urlencoded";
			headers["Content-Length"] = Buffer.byteLength(data);
		} else {
			method = "GET";
			parameters = false;
		}

		console.log("ANTE DO REQUEST");
		return axios.request({
			method,
			url: this.host + "/" + route + "/" + call + "/?access_token=" + this.credentials.access_token,
			headers: this.headers,
		}).then(response => {
			console.log(" - execute request response RESULT");
			return response.data;
		});
		/*var req = https.request(
			{
				hostname: this.host,
				path: this.baseUrl + route + "/" + call + "/?access_token=" + this.credentials.access_token,
				method: method,
				headers: headers,
			},
			function (res) {
				var chunks = "";
				res.on("data", function (resultData) {
					chunks += resultData;
				});
				res.on("end", function () {
					if (chunks.length > 0) {
						let parsedChunks;
						try {
							parsedChunks = JSON.parse(chunks);
						} catch (e) {
							return callback(chunks);
						}

						if (parsedChunks.error) return callback(parsedChunks);
						callback(null, parsedChunks);
					} else {
						return callback(500);
					}
				});
			},
		);
		if (parameters) {
			req.write(data);
		}
		req.end();

		req.on("error", function (error) {
			console.log("  - [call: "+call+"] on.error / error: ", error);
			callback(error);
		});*/
	}
		
});
	
};

Request.prototype.authenticate = function () {
	console.log("- AUTENTICAR (authenticate) -");
	var parameters = {};
	parameters.grant_type = "password";
	parameters.client_id = this.client_id;
	parameters.client_secret = this.client_secret;
	parameters.username = this.username;
	parameters.password = this.password;
	return axios.request({
		method: "GET",
		url: this.host + this.baseUrl + "grant/?" + querystring.stringify(parameters),
		headers: this.headers,
	})
	.then((response) => {
		console.log(" - autenticação response RESULT");
	  return response.data;
	})
	.catch((error) => {
		console.log("- autenticação response ERROR");
		console.log(error);
		return error;
		// TODO VALIDATE
	})
}

/*
Request.prototype.authenticate = function (callback) {
	var parameters = {};
	parameters.grant_type = "password";
	parameters.client_id = this.client_id;
	parameters.client_secret = this.client_secret;
	parameters.username = this.username;
	parameters.password = this.password;

	console.log(" # ---------------------- # ");
	console.log(" # NECESSITA AUTENTICAÇÃO # ");
	console.log(" # ---------------------- # ");
	console.log("  - autenticate -");
	console.log("  - parameters: ");
	console.log(parameters);
	console.log("");
try {
	https
		.get(
			{
				hostname: this.host,
				path: this.baseUrl + "grant/?" + querystring.stringify(parameters),
				headers: this.headers,
			},
			function (res) {
				//console.log("    -> res: ", res);
				if (res.statusCode && res.statusCode === 200) {
					var chunks = "";
					res.on("data", function (resultData) {
						console.log("RECEBEU PARTE DE UM FODASSE");
						chunks += resultData;
					});
					res.on("error", function (err) {
						console.log("FODASSE", err);
					});
					res.on("end", function () {
						console.log("");
						console.log(" # ---------------------- # ");
						console.log(" #  CREDENCIAIS  OBTIDAS  # ");
						console.log(" # ---------------------- # ");

						console.log("  - chunks: ");
						console.log(chunks);
						console.log("");

						let parsedChunks;
						try {
							parsedChunks = JSON.parse(chunks);
						} catch (e) {
							console.log("  - on.end / error: ", error);
							return callback(chunks);
						}

						callback(null, parsedChunks);
					});
				} else {
					callback({ code: res.StatusCode });
				}
			},
		)
		.on("error", function (error) {
			console.log("  - on.error / error: ", error);
			callback(error);
		});
	} catch(ex) {
		console.log("FODASSE");
		console.log(ex);
	}
};
*/

exports.Request = Request;