"use strict";

require("dotenv").config();

const { Errors } = require("@fisas/ms_core").Helpers;

const _ = require("lodash");
const Moloni = require("../utils/moloni");

let axios = require("axios");
const qs = require("qs");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "middleware.erp",

	requestTimeout: 20 * 1000, // REVIEW

	/**
	 * Settings
	 */
	settings: {
		DEBUG: process.env.MS_DEBUG == "true",

		rest_base_host: process.env.ERP_API_BASE_HOST,
		rest_base_url: process.env.ERP_API_BASE_URL,

		ERP_API_CLIENT_ID: process.env.ERP_API_CLIENT_ID,
		ERP_API_CLIENT_SECRET: process.env.ERP_API_CLIENT_SECRET,
		ERP_API_USERNAME: process.env.ERP_API_USERNAME,
		ERP_API_PASSWORD: process.env.ERP_API_PASSWORD,

		ERP_API_ACCESS_TOKEN: null,
		ERP_API_REFRESH_TOKEN: null,

		ERP_API_COMPANY_ID: parseInt(process.env.ERP_API_COMPANY_ID, 0),
		ERP_API_DEFAULT_DOCUMENT_SET_ID: parseInt(process.env.ERP_API_DEFAULT_DOCUMENT_SET_ID, 0),

		ERP_API_DEFAULT_PAYMENT_METHOD_ID: parseInt(process.env.ERP_API_DEFAULT_PAYMENT_METHOD_ID, 0),
		ERP_API_DEFAULT_UNIT_ID: parseInt(process.env.ERP_API_DEFAULT_UNIT_ID, 0),
		ERP_API_DEFAULT_PRODUCT_TYPE: parseInt(process.env.ERP_API_DEFAULT_PRODUCT_TYPE, 0),

		document_status: 1,
	},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) {}],
			update: [function sanatizeParams(ctx) {}],

			getMe: [function sanatizeParams(ctx) {}],
		},
		after: {
			createFinancialDocument: [
				async function appendResultParams(ctx, res) {
					if (res && res.valid) {
						const params = {
							company_id: this.settings.ERP_API_COMPANY_ID,
							document_id: res.document_id,
						};

						if (this.settings.DEBUG) {
							this.logger.warn("after / createFinancialDocument / appendResultParams / params:");
							this.logger.warn(params);
						}

						await new Promise((res) => {
							setTimeout(() => {
								res();
							}, 3000);
						});

						let resDoc = null;
						await ctx
							.call("middleware.erp.getOne", params)
							.then((resDocument) => {
								resDoc = resDocument;
								res.series =
									resDocument.document_type.saft_code + " " + resDocument.document_set_name;
								res.number = resDocument.number;
								res.issued_at = resDocument.date;
							})
							.catch((ex) => {
								throw new Errors.ValidationError("middleware.erp.getOne", "ERROR_ON_GET_ONE", {
									ex,
									resDoc,
								});
							});

						await ctx
							.call("middleware.erp.getPDFLink", params)
							.then((resPdfUrl) => {
								res.pdf_link = resPdfUrl;
							})
							.catch((ex) => {
								throw new Errors.ValidationError(
									"middleware.erp.getPDFLink",
									"ERROR_ON_GET_PDFLink",
									ex,
								);
							});
					}

					if (this.settings.DEBUG) {
						this.logger.warn("after / createFinancialDocument / appendResultParams / res:");
						this.logger.warn(res);
					}

					return res;
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		configs: {
			authorization: false,
			authentication: false,

			visibility: "published",
			rest: "GET /configs",
			params: {},
			async handler(ctx) {
				const moloniApiUri = this.settings.rest_base_host + this.settings.rest_base_url;

				/*
				const moloniApiGrantUri = moloniApiUri + "grant/" +
					"?grant_type=password" +
					"&client_id=" + ctx.params.client_id +
					"&client_secret=" + ctx.params.client_secret +
					"&username=" + ctx.params.username +
					"&password=" + ctx.params.password;
					*/

				const moloniApiGrantUri =
					moloniApiUri +
					"grant/" +
					"?grant_type=password" +
					"&client_id=" +
					this.settings.ERP_API_CLIENT_ID +
					"&client_secret=" +
					this.settings.ERP_API_CLIENT_SECRET +
					"&username=" +
					this.settings.ERP_API_USERNAME +
					"&password=" +
					this.settings.ERP_API_PASSWORD;

				return axios
					.request({
						method: "GET",
						url: moloniApiGrantUri,
					})
					.then((response) => {
						this.logger.info("responseGrant.data:");
						this.logger.info(response.data);

						let resConfigs = {};
						let access_token;

						if (response.data && response.data.access_token) {
							access_token = response.data.access_token;

							return axios
								.request({
									method: "GET",
									url:
										moloniApiUri + "companies/getAll/" + "?json=true&access_token=" + access_token,
								})
								.then(async (resCompaniesGetAll) => {
									const companies = resCompaniesGetAll.data.filter((c) => c.vat == ctx.params.vat);

									Object.assign(resConfigs, { ERP_API_COMPANY_ID: companies[0].company_id });

									if (this.settings.DEBUG) {
										this.logger.info("");
										this.logger.info("resConfigs.1:");
										this.logger.info(resConfigs);
									}

									// documentSets
									await axios
										.request({
											method: "POST",
											url:
												moloniApiUri +
												"documentSets/getAll/" +
												"?json=true&access_token=" +
												access_token,
											data: { company_id: resConfigs.ERP_API_COMPANY_ID },
										})
										.then((resDocumentsSetGetAll) => {
											this.logger.info("");
											this.logger.info("resDocumentsSetGetAll:");
											this.logger.info(resDocumentsSetGetAll.data);

											const defaultDocumentsSet = resDocumentsSetGetAll.data.filter(
												(ds) => ds.active_by_default == 1,
											);

											Object.assign(resConfigs, {
												ERP_API_DEFAULT_DOCUMENT_SET_ID: defaultDocumentsSet[0].document_set_id,
											});
										})
										.catch((err) => {
											this.logger.info("");
											this.logger.error("err.2:");
											this.logger.error(err.response.data.error_description);
										});

									if (this.settings.DEBUG) {
										this.logger.info("");
										this.logger.info("resConfigs.2:");
										this.logger.info(resConfigs);
									}

									// paymentMethods
									await axios
										.request({
											method: "POST",
											url:
												moloniApiUri +
												"paymentMethods/getAll/" +
												"?json=true&access_token=" +
												access_token,
											data: { company_id: resConfigs.ERP_API_COMPANY_ID },
										})
										.then((resPaymentMethodsGetAll) => {
											if (this.settings.DEBUG) {
												this.logger.info("");
												this.logger.info("resPaymentMethodsGetAll:");
												this.logger.info(resPaymentMethodsGetAll.data);
											}

											const paymentMethodsSaldoCC = resPaymentMethodsGetAll.data.filter(
												(pm) => pm.name.toLowerCase() == "Saldo CC".toLocaleLowerCase(),
											);

											Object.assign(resConfigs, {
												ERP_API_DEFAULT_PAYMENT_METHOD_ID:
													paymentMethodsSaldoCC[0].payment_method_id,
											});
										})
										.catch((err) => {
											this.logger.info("");
											this.logger.error("err.3:");
											this.logger.error(err.response.data.error_description);
										});

									if (this.settings.DEBUG) {
										this.logger.info("");
										this.logger.info("resConfigs.3:");
										this.logger.info(resConfigs);
									}

									// measurementUnits
									await axios
										.request({
											method: "POST",
											url:
												moloniApiUri +
												"measurementUnits/getAll/" +
												"?json=true&access_token=" +
												access_token,
											data: { company_id: resConfigs.ERP_API_COMPANY_ID },
										})
										.then((resMeasurementUnitsGetAll) => {
											if (this.settings.DEBUG) {
												this.logger.info("");
												this.logger.info("resMeasurementUnitsGetAll:");
												this.logger.info(resMeasurementUnitsGetAll.data);
											}

											const uniMeasurementUnits = resMeasurementUnitsGetAll.data.filter(
												(mu) => mu.short_name == "Uni.",
											);

											Object.assign(resConfigs, {
												ERP_API_DEFAULT_UNIT_ID: uniMeasurementUnits[0].unit_id,
											});
										})
										.catch((err) => {
											this.logger.info("err.4:");
											this.logger.info(err.response.data.error_description);
										});

									if (this.settings.DEBUG) {
										this.logger.info("");
										this.logger.info("resConfigs.4:");
										this.logger.info(resConfigs);
									}

									return resConfigs;
								})
								.catch((err) => {
									this.logger.info("");
									this.logger.error("err.1:");
									this.logger.error(err.response.data.error_description);
								});
						}

						return false;
					});
			},
		},

		callback_api: {
			authorization: false,
			authentication: false,

			visibility: "published",
			rest: "post /callback_api",
			params: {},
			async handler(ctx) {
				if (this.settings.DEBUG) {
					this.logger.info(ctx.params);
				}
				return true;
			},
		},
		isAvailable: {
			handler(ctx) {
				return true;
			},
		},
		getMe: {
			authorization: false,
			authentication: false,

			visibility: "public",
			params: {},
			async handler(ctx) {
				return new Promise((resolve, reject) => {
					this.moloni.users(ctx, "getMe").then((result) => {
						resolve(result);
					});
				});
			},
		},
		getAllCompanies: {
			authorization: false,
			authentication: false,

			visibility: "published",
			params: {},
			async handler(ctx) {
				return this.getAllCompanies(ctx);
			},
		},

		getProductByReference: {
			//TODO: remove this action or set visibility to 'public', just for debug
			authorization: false,
			authentication: false,

			visibility: "public",
			params: {
				company_id: { type: "number" },
				reference: { type: "string" },
				exact: { type: "number", positive: true, optional: true },

				qty: { type: "number", optional: true }, // - O parâmetro qty tem como default 50 e o offset 0, sendo que o máximo qty é 50;
				offset: { type: "number", optional: true },
			},
			async handler(ctx) {
				const params = ctx.params;
				return this.getProductByReference(ctx, params);
			},
		},
		getProductById: {
			//TODO: remove this action or set visibility to 'public', just for debug
			authorization: false,
			authentication: false,

			visibility: "public",
			params: {
				company_id: { type: "number" },
				product_id: { type: "number" },
			},
			async handler(ctx) {
				return this.getProductById(ctx);
			},
		},
		getCustomerByVat: {
			//TODO: remove this action or set visibility to 'public', just for debug

			authorization: false,
			authentication: false,

			visibility: "published", //
			rest: "POST /getCustomerByVat",
			params: {
				company_id: { type: "number" },
				vat: { type: "string" },

				qty: { type: "number", optional: true }, // - O parâmetro qty tem como default 50 e o offset 0, sendo que o máximo qty é 50;
				offset: { type: "number", optional: true },
			},
			async handler(ctx) {
				return this.getCustomerByVat(ctx);
			},
		},

		getPDFLink: {
			authorization: false,
			authentication: false,

			visibility: "published",
			params: {
				company_id: { type: "number" },
				document_id: { type: "number" },
			},
			async handler(ctx) {
				return this.getPDFLink(ctx);
			},
		},
		getPDF: {
			authorization: false,
			authentication: false,

			visibility: "published",
			params: {
				h: { type: "string" }, //hash
				d: { type: "number", convert: true }, //document_id
			},
			async handler(ctx) {
				ctx.meta.$responseType = "application/pdf";
				ctx.meta.$responseHeaders = {
					"Content-Disposition": `attachment; filename="doc-${ctx.params.d}.pdf"`,
				};

				return axios
					.request({
						method: "GET",
						url:
							"https://www.moloni.pt/downloads/index.php?" +
							"action=getDownload" +
							"&h=" +
							ctx.params.h +
							"&d=" +
							ctx.params.d,
						responseType: "stream",
					})
					.then(async (response) => {
						if (response.headers["content-description"] == "File Transfer") {
							// Download available
							ctx.meta.$responseType = response.headers["content-type"]; //"application/pdf";
							ctx.meta.$responseHeaders = {
								"Content-Disposition": response.headers["content-disposition"],
							};
						} else {
							// pdfLink is expired, download is not available

							ctx.params.company_id = this.settings.ERP_API_COMPANY_ID;
							ctx.params.document_id = ctx.params.d; //"425312689"; //ctx.params.d; //TODO: remove hardCoded ctx.params.d;

							const renewedPdfLink = await this.getPDFLink(ctx);

							if (!renewedPdfLink.valid) {
								// returned: { valid: 0 }

								return Promise.reject(
									new Errors.ValidationError(
										"Document download is not available!",
										"INVALID_DOCUMENT_URI",
										{},
									),
								);
							}

							if (this.settings.MS_DEBUG) {
								this.logger.warn(" - (pdfLink is expired) renewedPdfLink:");
								this.logger.warn(renewedPdfLink);
							}

							let objQueryString = qs.parse(renewedPdfLink.url.split("?")[1]);

							return axios
								.request({
									method: "GET",
									url:
										"https://www.moloni.pt/downloads/index.php?" +
										"action=getDownload" +
										"&h=" +
										objQueryString.h +
										"&d=" +
										ctx.params.d, //"425312689", //ctx.params.d,
									responseType: "stream",
								})
								.then(async (resRenewedUri) => {
									if (resRenewedUri.headers["content-description"] == "File Transfer") {
										// Download is now available with new hash

										ctx.meta.$responseType = resRenewedUri.headers["content-type"]; //"application/pdf";
										ctx.meta.$responseHeaders = {
											"Content-Disposition": resRenewedUri.headers["content-disposition"],
										};

										return resRenewedUri.data;
									} else {
										return Promise.reject(false);
									}
								});
						}
						return response.data;
					});
			},
		},
		getOne: {
			authorization: false,
			authentication: false,

			visibility: "public",
			params: {
				company_id: { type: "number" },
				document_id: { type: "number" },
			},
			async handler(ctx) {
				const searchParams = {
					company_id: ctx.params.company_id,
					document_id: ctx.params.document_id,
				};
				return this.getOne(ctx, searchParams);
			},
		},

		createFinancialDocument: {
			authorization: false,
			authentication: false,
			timeout: 60000,
			visibility: "public",
			rest: "POST /createFinancialDocument",

			params: {
				account_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
				operation: { type: "string", optional: false },
				device_id: { type: "number", integer: true, positive: true, min: 1, optional: true },
				user_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
				entity: { type: "string", max: 120, optional: true },
				tin: { type: "string", max: 9, optional: true },
				email: { type: "string", max: 120, optional: true },
				address: { type: "string", max: 250, optional: true },
				postal_code: { type: "string", max: 10, optional: true },
				city: { type: "string", max: 250, optional: true },
				country: { type: "string", max: 250, optional: true },
				description: { type: "string", max: 250, optional: true },
				payment_method_id: { type: "uuid", version: 4, optional: true },
				expiration_at: { type: "date", optional: true },
				items: {
					type: "array",
					items: {
						type: "object",
						strict: false,
						props: {
							service_id: {
								type: "number",
								integer: true,
								positive: true,
								min: 1,
								optional: false,
							},
							product_code: { type: "string", max: 120, optional: false },
							name: { type: "string", max: 255, optional: false },
							description: { type: "string", max: 250, optional: true },
							extra_info: { type: "object", optional: true },
							quantity: {
								type: "number",
								integer: true,
								positive: true,
								convert: true,
								optional: false,
							},
							liquid_unit_value: { type: "number", positive: true, convert: true, optional: true },
							unit_value: { type: "number", positive: true, convert: true, optional: true },
							discount_value: {
								type: "number",
								min: 0,
								optional: true,
								default: 0,
							},
							vat_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
							location: { type: "string", max: 255, optional: false },
							article_type: { type: "string", max: 120, optional: true },
							service_confirm_path: { type: "string", max: 120, optional: true },
							service_cancel_path: { type: "string", max: 120, optional: true },
						},
					},
				},
			},
			async handler(ctx) {
				if (this.settings.DEBUG) {
					this.logger.warn("");

					this.logger.warn("# ------------------------------ #");
					this.logger.warn("# -  CREATE FINANCIAL DOCUMENT - #");
					this.logger.warn("# ------------------------------ #");
				}

				let _params = ctx.params;
				const docType = ctx.params.operation;

				// CHECK IF DOCUMENT EXIST
				if (_params.middleware_erp_status === "RETRY" && docType !== "RECEIPT") {
					const our_reference = "SASocial: " + _params.doc_type_acronym + _params.seq_doc_num;
					if (this.settings.DEBUG) {
						this.logger.info("    --> checkIfDocumentAlreadyExist by OUR_REFERENCE:");
						this.logger.info(our_reference);
					}

					const already_created_doc = await this.getOne(ctx, {
						company_id: this.settings.ERP_API_COMPANY_ID,
						our_reference,
					}).then((res) => {
						if (this.settings.DEBUG) {
							this.logger.info("    <-- checkIfDocumentAlreadyExist by OUR_REFERENCE:");
							this.logger.info(res);
						}

						if (!Array.isArray(res)) res = [res];

						return res;
					});

					if (already_created_doc && already_created_doc.length > 0)
						return { valid: 1, document_id: already_created_doc[0].document_id };
				}

				switch (docType) {
					case "INVOICE_RECEIPT":
						return ctx.call("middleware.erp.createInvoiceReceipt", {
							financialDocument: _params,
						});

					case "INVOICE":
						return ctx.call("middleware.erp.createInvoice", {
							financialDocument: _params,
						});

					case "RECEIPT":
						return ctx.call("middleware.erp.createReceipt", {
							financialDocument: _params,
						});

					case "CREDIT_NOTE":
						return ctx.call("middleware.erp.createCreditNote", {
							financialDocument: _params,
						});

					/*case "CREDIT_NOTE_RAPPEL":
							return ctx.call("middleware.erp.createCreditNoteRappel", {
								financialDocument: _params,
							});*/

					default:
						throw new Errors.ValidationError(
							"Provided document type is invalid!",
							"INVALID_DOCUMENT_TYPE",
							{ operation: docType },
						);
				}
			},
		},
		createDocInvoiceReceipt: {
			//TODO: [REMOVE] JUST FOR DEBUG PURPOSES
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "published",
			rest: "POST /createDocInvoiceReceipt",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				/*
				{
					"financialDocument": {
						"operation": "INVOICE_RECEIPT",
						"date": "2021-07-13T00:00:00+0100",
						"expiration_date": "2021-07-13T00:00:00+0100",
						"document_set_id": 413297,
						"customer_id": 50536716,
						"our_reference": "TESTE_SASOCIAL",
						"products": [
							{
								"product_id": 90654735,
								"name": "Café Curto",
								"qty": 3,
								"price": 0.45,
								"discount": 0,
								"order": 1,
								"taxes": [
									{
										"tax_id": 2224155,
										"value": 6,
										"order": 1,
										"cumulative": 0
									}
								]
							}
						],
						"payments": [
							{
								"payment_method_id": 1346687,
								"date": "2021-07-13T00:00:00+0100",
								"value": 1.43,
								"notes": "SALDO_SASOCIAL"
							}
						],
						"notes": "NOTAS_SASOCIAL",
						"status": 0
					}
				}
				*/

				return new Promise((resolve, reject) => {
					this.moloni
						.invoiceReceipts(ctx, "insert", ctx.params.financialDocument)
						.then((result) => {
							resolve(result);
						});
				});
			},
		},
		createInvoiceReceipt: {
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "public",
			rest: "POST /createInvoiceReceipt",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				if (this.settings.DEBUG) {
					this.logger.info("    --> createInvoiceReceipt XYZ:");
					this.logger.info(ctx.params);
				}

				let paramsDoc = ctx.params.financialDocument;

				let dummyDoc = {
					operation: "INVOICE_RECEIPT",
					company_id: this.settings.ERP_API_COMPANY_ID,
					date: new Date(),
					expiration_date: paramsDoc.expiration_at ? paramsDoc.expiration_at : new Date(),
					document_set_id: this.settings.ERP_API_DEFAULT_DOCUMENT_SET_ID,
					customer_id: null,
					our_reference: "SASocial: " + paramsDoc.doc_type_acronym + paramsDoc.seq_doc_num,
					products: [],
					payments: [],
					notes: paramsDoc.location || "",
					status: paramsDoc.erp_status ? paramsDoc.erp_status : this.settings.document_status, // 0: Rascunho - 1: Definitivo/Fechado
				};

				if (paramsDoc.payment) {
					const _payment = paramsDoc.payment;
					Object.assign(dummyDoc, {
						payments: [
							{
								payment_method_id: this.settings.ERP_API_DEFAULT_PAYMENT_METHOD_ID, //TODO: get default payment_method (SALDO)
								date: _payment.confirmed_at,
								value: _payment.value,
								notes: "payment_id: " + _payment.id,
							},
						],
					});
				}

				// Customer validate
				this.logger.info("Check Customer ...");
				await this.checkCustomerByVat(ctx, dummyDoc);

				// Products validate
				this.logger.info("Check Products ...");
				await this.checkProducts(ctx, dummyDoc);

				this.logger.info("createInvoiceReceipt (insert) dummyDoc: ");
				this.logger.info(dummyDoc);

				//return dummyDoc;

				return new Promise((resolve, reject) => {
					this.moloni.invoiceReceipts(ctx, "insert", dummyDoc).then((result) => {
						resolve(result);
					});
				});
			},
		},

		createDocInvoice: {
			//TODO:[REMOVE]JUST FOR DEBUG PURPOSES
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "published",
			//rest: "POST /createDocInvoiceReceipt",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				/*
				{
					"financialDocument": {
						"operation": "INVOICE_RECEIPT",
						"company_id": 186695,
						"date": "2021-07-13T00:00:00+0100",
						"expiration_date": "2021-07-13T00:00:00+0100",
						"document_set_id": 413297,
						"customer_id": 50536716,
						"our_reference": "TESTE_SASOCIAL",
						"products": [
							{
								"product_id": 90654735,
								"name": "Café Curto",
								"qty": 3,
								"price": 0.45,
								"discount": 0,
								"order": 1,
								"taxes": [
									{
										"tax_id": 2224155,
										"value": 6,
										"order": 1,
										"cumulative": 0
									}
								]
							}
						],
						"payments": [
							{
								"payment_method_id": 1346687,
								"date": "2021-07-13T00:00:00+0100",
								"value": 1.43,
								"notes": "SALDO_SASOCIAL"
							}
						],
						"notes": "NOTAS_SASOCIAL",
						"status": 0
					}
				}
				*/

				return new Promise((resolve, reject) => {
					this.moloni.invoices(ctx, "insert", ctx.params.financialDocument).then((result) => {
						resolve(result);
					});
				});
			},
		},
		createInvoice: {
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "public",
			//rest: "POST /createInvoice",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				this.logger.info("    --> createInvoice XYZ:");
				this.logger.info(ctx.params);

				let paramsDoc = ctx.params.financialDocument;

				let dummyDoc = {
					operation: "INVOICE",
					company_id: this.settings.ERP_API_COMPANY_ID,
					date: new Date(),
					expiration_date: paramsDoc.expiration_at ? paramsDoc.expiration_at : new Date(),
					document_set_id: this.settings.ERP_API_DEFAULT_DOCUMENT_SET_ID,
					customer_id: null,
					//alternate_address_id: null,
					our_reference: "SASocial: " + paramsDoc.doc_type_acronym + paramsDoc.seq_doc_num,
					//your_reference: "SASocial: " + paramsDoc.doc_type_acronym + paramsDoc.seq_doc_num,
					//financial_discount: 0,
					//salesman_id: null,
					//salesman_commission: null,
					//deduction_id: null, //TODO: check if this is the CreditNote Rappel
					//special_discount: 0,
					associated_documents: [],
					//related_documents_notes: ""
					products: [],
					payments: [],
					notes: "",
					status: paramsDoc.erp_status ? paramsDoc.erp_status : this.settings.document_status, // 0: Rascunho - 1: Definitivo/Fechado
				};

				if (paramsDoc.associated_documents) {
					const _payment = paramsDoc.payment;
					Object.assign(dummyDoc, {
						payments: [
							{
								payment_method_id: this.settings.ERP_API_DEFAULT_PAYMENT_METHOD_ID, //TODO: get default payment_method (SALDO)
								date: _payment.confirmed_at,
								value: _payment.value,
								notes: "payment_id: " + _payment.id,
							},
						],
					});
				}

				// Customer validate
				this.logger.info("Check Customer ...");
				await this.checkCustomerByVat(ctx, dummyDoc);

				// Products validate
				this.logger.info("Check Products ...");
				await this.checkProducts(ctx, dummyDoc);

				this.logger.info("createInvoice (insert) dummyDoc: ");
				this.logger.info(dummyDoc);

				//return dummyDoc;

				return new Promise((resolve, reject) => {
					this.moloni.invoices(ctx, "insert", dummyDoc).then((result) => {
						resolve(result);
					});
				});
			},
		},
		createReceipt: {
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "public",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				this.logger.info("    --> createReceipt XYZ:");
				this.logger.info(ctx.params);

				let paramsDoc = ctx.params.financialDocument;

				let dummyDoc = {
					operation: "RECEIPT",
					company_id: this.settings.ERP_API_COMPANY_ID,
					date: new Date(),
					document_set_id: this.settings.ERP_API_DEFAULT_DOCUMENT_SET_ID,
					customer_id: null,
					//alternate_address_id: null,
					net_value: 0,
					associated_documents: [],
					//related_documents_notes: ""
					products: [],
					payments: [],
					//exchange_currency_id: null, //optional. Default:
					//exchange_rate: 0, //optional
					notes: "",
					status: paramsDoc.erp_status ? paramsDoc.erp_status : this.settings.document_status, // 0: Rascunho - 1: Definitivo/Fechado
				};

				if (paramsDoc.associated_documents) {
					const _associated_doc = paramsDoc.associated_documents;
					Object.assign(dummyDoc, {
						associated_documents: [
							{
								associated_id: _associated_doc.document_erp_id,
								value: paramsDoc.paid_value,
							},
						],
						net_value: paramsDoc.paid_value,
					});
				}

				if (paramsDoc.payment) {
					const _payment = paramsDoc.payment;
					Object.assign(dummyDoc, {
						payments: [
							{
								payment_method_id: this.settings.ERP_API_DEFAULT_PAYMENT_METHOD_ID, //TODO: get default payment_method (SALDO)
								date: _payment.confirmed_at,
								value: _payment.value,
								notes: "payment_id: " + _payment.id,
							},
						],
					});
				}

				// Customer validate
				this.logger.info("Check Customer ...");
				await this.checkCustomerByVat(ctx, dummyDoc);

				this.logger.info("createReceipt (insert) dummyDoc: ");
				this.logger.info(dummyDoc);

				return new Promise((resolve, reject) => {
					this.moloni.receipts(ctx, "insert", dummyDoc).then((result) => {
						resolve(result);
					});
				});
			},
		},
		createCreditNote: {
			timeout: 0,
			authorization: false,
			authentication: false,

			visibility: "public",

			params: {
				financialDocument: { type: "object" },
			},
			async handler(ctx) {
				if (this.settings.DEBUG) {
					this.logger.warn("");
					this.logger.warn("  --> Create CREDIT_NOTE:");
					this.logger.warn("");
					this.logger.warn(JSON.stringify(ctx.params));
				}
				let paramsDoc = ctx.params.financialDocument;

				let dummyDoc = {
					operation: "CREDIT_NOTE",
					company_id: this.settings.ERP_API_COMPANY_ID,
					date: new Date(),
					expiration_date: paramsDoc.expiration_at ? paramsDoc.expiration_at : new Date(),
					document_set_id: this.settings.ERP_API_DEFAULT_DOCUMENT_SET_ID,
					customer_id: null,
					net_value: 0,
					our_reference: "SASocial: " + paramsDoc.doc_type_acronym + paramsDoc.seq_doc_num,
					//your_reference: "SASocial: " + paramsDoc.doc_type_acronym + paramsDoc.seq_doc_num,
					//salesman_id: null,
					//salesman_commission: null,
					//deduction_id: null, //TODO: check if this is the CreditNote Rappel
					//special_discount: 0,
					associated_documents: [],
					//related_documents_notes: ""
					products: [],
					payments: [],
					notes: "",
					status: paramsDoc.erp_status ? paramsDoc.erp_status : this.settings.document_status, // 0: Rascunho - 1: Definitivo/Fechado
				};

				// Customer validate
				this.logger.warn("");
				this.logger.warn("  -->  Check Customer ...");
				await this.checkCustomerByVat(ctx, dummyDoc);

				// Products validate
				this.logger.warn("");
				this.logger.warn("  -->  Check Products ...");
				await this.checkProducts(ctx, dummyDoc);

				if (paramsDoc.associated_documents && paramsDoc.associated_documents.document_erp_id) {
					const _associated_doc = paramsDoc.associated_documents; //[0];
					Object.assign(dummyDoc, {
						associated_documents: [
							{
								associated_id: _associated_doc.document_erp_id,
								value: paramsDoc.paid_value, //_associated_doc.paid_value,
							},
						],
					});

					//Set related_id to Products by reference
					//original_movement can be INVOICE or INVOICE_RECEIPT
					const relatedDoc = await this.getOne(ctx, {
						company_id: dummyDoc.company_id,
						document_id: _associated_doc.document_erp_id,
					});

					let alreadyCancelledProducts = [];

					//Check for "reverse_associated_documents" existence (NC's)
					if (
						relatedDoc.reverse_associated_documents &&
						relatedDoc.reverse_associated_documents.length
					) {
						//Get all related NC's from OriginalDocument (INVOICE or INVOICE_RECEIPT)
						//let foundRelatedOriginalDocCreditNotes = [];

						for (const rad of relatedDoc.reverse_associated_documents) {
							//foreach alreadyCreated_CREDIT_NOTE

							const associatedDoc = await this.getOne(ctx, {
								company_id: dummyDoc.company_id,
								document_id: rad.document_id,
							});

							if (associatedDoc.document_type_id == 3) {
								//CREDIT_NOTE

								//foundRelatedOriginalDocCreditNotes.push(associatedDoc);

								/*alreadyCancelledProducts = [
									...alreadyCancelledProducts,
									...associatedDoc.products.map((p) => (p.document_product_id)),
								];*/

								alreadyCancelledProducts = [
									...alreadyCancelledProducts,
									...associatedDoc.products.map((p) => {
										const originalProduct = relatedDoc.products.find(
											(rp) => rp.document_product_id === p.related_id,
										);

										return {
											document_product_id: p.document_product_id,
											reference: p.reference,
											price: p.price,
											name: p.name,
											related_id: p.related_id, //<--"document_product_id" do documento original

											original_price: originalProduct
												? (originalProduct.price * originalProduct.qty).toFixed(2)
												: 0,
											cancelled_price: (p.price * p.qty).toFixed(2),
										};
									}),
								];
							}
						}
					}

					// Remove already canceled products
					const alreadyCancelledProductsIds = alreadyCancelledProducts
						.filter((f) => {
							const total_cancelled = alreadyCancelledProducts
								.filter((acpf) => acpf.related_id === f.related_id)
								.reduce((a, b) => a + b.cancelled_price, 0);
							return f.original_price < total_cancelled;
						})
						.map((acp) => acp.related_id);
					const availableProductsToCancel = relatedDoc.products.filter(
						(p) => !alreadyCancelledProductsIds.includes(p.document_product_id),
					);

					this.logger.warn("");
					this.logger.warn(" ### ------ ###");
					this.logger.warn("");
					this.logger.warn("  - alreadyCancelledProducts: ");
					this.logger.warn(alreadyCancelledProducts);
					this.logger.warn("");
					this.logger.warn("  - alreadyCancelledProductsIds: ");
					this.logger.warn(alreadyCancelledProductsIds);
					this.logger.warn("");
					this.logger.warn("  - availableProductsToCancel: ");
					this.logger.warn(availableProductsToCancel);
					this.logger.warn("");
					this.logger.warn(" ### ------ ###");
					this.logger.warn("");

					for (const p of dummyDoc.products) {
						let product_to_cancel = availableProductsToCancel.find(
							(apc) =>
								apc.product_id === p.product_id &&
								apc.name === p.name &&
								apc.summary === p.summary &&
								apc.price === p.price && !dummyDoc.products.find(dp => dp.related_id === apc.document_product_id),
						);

						if (!product_to_cancel) {
							product_to_cancel = availableProductsToCancel.find(
								(apc) => apc.product_id === p.product_id && apc.name === p.name,
							);
						}

						if (product_to_cancel) {
							Object.assign(p, {
								related_id: product_to_cancel.document_product_id,
								price: product_to_cancel.price,
							});
						} else {
							const product_to_cancel_by_code = availableProductsToCancel.find(
								(apc) => apc.product_id === p.product_id && apc.price === p.price,
							);

							if (product_to_cancel_by_code) {
								Object.assign(p, {
									related_id: product_to_cancel_by_code.document_product_id,
									price: product_to_cancel_by_code.price,
								});
							} else {
								throw new Errors.ValidationError(
									"No valid product found to create a credit note!",
									"INVALID_AVAILABLE_PRODUCTS_TO_CANCEL",
									{},
								);
							}
						}
					}
				} else {
					this.logger.warn("");
					this.logger.warn("No associated document id to create credit note!");
					// THROW ERROR
					throw new Errors.ValidationError(
						"The Credit Note has no associated document!",
						"INVALID_ASSOCIATED_DOCUMENT",
						{},
					);
				}

				if (this.settings.DEBUG) {
					this.logger.warn("");
					this.logger.warn("   -->  CreateCreditNote (insert) dummyDoc: ");
					this.logger.warn(dummyDoc);
				}

				return new Promise((resolve, reject) => {
					this.moloni.creditNotes(ctx, "insert", dummyDoc).then((result) => {
						resolve(result);
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		//COMPANIES
		async getAllCompanies(ctx) {
			return this.moloni.companies(ctx, "getAll");
		},

		//CUSTOMERS
		getCustomerByVat(ctx) {
			const searchData = ctx.params;

			return ctx.call("middleware_erp.moloni.executeRequest", {
				route: "customers",
				call: "getByVat",
				parameters: searchData,
			});
		},
		checkCustomerByVat(ctx, dummyDoc) {
			let _params = ctx.params.financialDocument;
			let _dummyDoc = dummyDoc;

			const searchData = {
				company_id: this.settings.ERP_API_COMPANY_ID,
				vat: _params.tin,
			};

			return this.moloni.customers(ctx, "getByVat", searchData).then(async (result) => {
				if (result.length) {
					// Customer found
					if (this.settings.DEBUG) {
						this.logger.warn("");
						this.logger.warn(
							"       --->  Customer found:",
							_.pick(result[0], ["customer_id", "name", "vat"]),
						);
					}
					Object.assign(_dummyDoc, { customer_id: result[0].customer_id });
				} else {
					// Customer not found, so create them

					this.logger.warn("");
					this.logger.warn("   <--  Customer vat[", _params.tin, "] NOT found, must be created!");

					const customerNextNumber = await this.getCustomerNextNumber(
						ctx,
						this.settings.ERP_API_COMPANY_ID,
					);
					let newCustomerData = {
						company_id: this.settings.ERP_API_COMPANY_ID,
						vat: _params.tin,
						number: customerNextNumber,
						name: _params.entity,
						language_id: 1, //PT
						address: _params.address,
						zip_code: _params.postal_code,
						city: _params.city,
						country_id: 1, //PORTUGAL
						email: _params.email,
						maturity_date_id: 30, //30, 60, 90 dias ou 0 - Pronto Pagamento
						payment_method_id: this.settings.ERP_API_DEFAULT_PAYMENT_METHOD_ID,
						field_notes: "SASocial user_id:" + _params.user_id,
						salesman_id: 0,
						payment_day: 0,
						discount: 0,
						credit_limit: 0,
						delivery_method_id: 0,
					};
					if (this.settings.DEBUG) {
						this.logger.warn("");
						this.logger.warn("  -->  Customer data to CREATE:", newCustomerData);
					}

					const createdCustomerResult = await this.createCustomer(ctx, newCustomerData);

					if (this.settings.DEBUG) {
						this.logger.warn("");
						this.logger.warn("  <--  Customer creation result:", createdCustomerResult);
					}

					if (!createdCustomerResult.valid) {
						//reject(createdCustomerResult);
						return Promise.reject(
							new Errors.ValidationError(
								"Customer validation failed",
								"CC_MIDDLEWARE_ERP_CUSTOMER_VALIDATION_FAILED",
								createdCustomerResult,
							),
						);
					}

					Object.assign(_dummyDoc, { customer_id: createdCustomerResult.customer_id });

					if (this.settings.DEBUG) {
						this.logger.warn("");
						this.logger.warn("-->  CREATED:", newCustomerData);
					}
				}
			});
		},
		getCustomerNextNumber(ctx, company_id) {
			return new Promise((resolve, reject) => {
				this.moloni
					.customers(ctx, "getNextNumber", { company_id })
					.then((result) => {
						this.logger.warn("");
						this.logger.warn("  -->  GetCustomerNextNumber:", result.number);
						resolve(result.number);
					})
					.catch((error) => {
						this.logger.error("");
						this.logger.error("  --> Error: ", error);
						reject(error);
					});
			});
		},
		createCustomer(ctx, newCustomerData) {
			return new Promise((resolve, reject) => {
				this.moloni
					.customers(ctx, "insert", newCustomerData)
					.then((result) => {
						if (this.settings.DEBUG) {
							this.logger.warn("");
							this.logger.warn("  <-- createCustomer result.2: ", result);
						}

						resolve(result);
					})
					.catch((error) => {
						this.logger.error("");
						this.logger.error("  <-- createCustomer error.2: ", error);
						reject(error);
					});
			});
		},

		// PRODUCT_CATEGORIES
		createCategoryProduct(ctx, newCategoryData) {
			return new Promise((resolve, reject) => {
				this.moloni
					.productCategories(ctx, "insert", newCategoryData)
					.then((result) => {
						if (this.settings.DEBUG) {
							this.logger.warn("");
							this.logger.warn("  <-- createCategoryProduct result: ", result);
						}
						if (!result.valid) {
							reject(result);
						} else {
							resolve(result);
						}
					})
					.catch((error) => {
						this.logger.error("");
						this.logger.error("  <-- createCategoryProduct error: ", error);
						reject(error);
					});
			});
		},
		async checkProductCategoryByItem(ctx, itemProduct) {
			let servicePT = _.find(itemProduct.service.translations, { language_id: 3 });

			const searchData = {
				company_id: this.settings.ERP_API_COMPANY_ID,
				parent_id: 0, //Caso envie um valor de "0" serão retornadas todas as categorias de topo
			};

			return new Promise((resolve, reject) => {
				this.moloni.productCategories(ctx, "getAll", searchData).then(async (resultGetAll) => {
					const foundCategory = _.find(resultGetAll, (o) => {
						return o.name.toLowerCase() == servicePT.name.toLowerCase();
					});

					if (foundCategory) {
						// ProductCategory found

						this.logger.info("checkProductCategoryByItem / ProductCategory found:", foundCategory);
						resolve(foundCategory.category_id);
						//return foundCategory.category_id;
					} else {
						// Create ProductCategory

						let newCategoryData = {
							company_id: this.settings.ERP_API_COMPANY_ID,
							parent_id: 0,
							name: servicePT.name,
							description: servicePT.description,
						};

						this.logger.info("checkProductCategoryByItem / ProductCategory NOT found!");

						const createdCategoryProduct = await this.createCategoryProduct(
							ctx,
							newCategoryData,
						).catch((error) => {
							this.logger.error(" <- createProductCategory error: ", error);
							reject(error);
						});

						resolve(createdCategoryProduct.category_id);
					}
				});
			});
		},

		// PRODUCTS
		getProductByReference(ctx, referenceParams) {
			/*return new Promise((resolve, reject) => {
				this.moloni.products(ctx, "getByReference", referenceData).then(result => {

					resolve(result);
				});
			});*/
			return this.moloni.products(ctx, "getByReference", referenceParams);
		},
		getProductById(ctx) {
			return new Promise((resolve, reject) => {
				this.moloni.products(ctx, "getOne", ctx.params).then((result) => {
					resolve(result);
				});
			});
		},
		createProduct(ctx) {
			//return ctx.params;
			this.logger.info(ctx.params);

			return new Promise((resolve, reject) => {
				this.moloni.products(ctx, "insert", ctx.params).then((result) => {
					resolve(result);
				});
			});
		},
		async checkProducts(ctx, dummyDoc) {
			let _params = ctx.params.financialDocument;
			let _dummyDoc = dummyDoc;

			let itemsDoc = _params.items;

			let documentProducts = [];

			for (const item of itemsDoc) {
				const ndxItem = itemsDoc.indexOf(item) + 1;
				const searchProductData = {
					company_id: this.settings.ERP_API_COMPANY_ID,
					reference: item.product_code,
				};

				// Check if TAX exist
				const vatTypeValAsTaxValue = item.vat * 100; // received from movement at format: 0.23
				const searchTaxParams = {
					company_id: this.settings.ERP_API_COMPANY_ID,
					value: vatTypeValAsTaxValue,
				};
				const taxFound = await this.getTaxByTaxValue(ctx, searchTaxParams).then(
					(resultTaxFound) => {
						if (this.settings.DEBUG) {
							this.logger.warn("  getTaxByTaxValue - resultTaxFound:", resultTaxFound);
						}

						if (resultTaxFound) {
							// Tax FOUND
							this.logger.warn("  Tax found");
							return resultTaxFound;
						} else {
							// Tax NOT FOUND, so CREATE them
							this.logger.warn("  Tax NOT found");

							const vat_type_values = [
								{ code: "RED", val: 6, description: "IVA reduzido" },
								{ code: "INT", val: 13, description: "IVA intermédio" },
								{ code: "NOR", val: 23, description: "IVA normal" },
								{ code: "ISE", val: 0, description: "Isento de IVA" },
								{ code: "OUT", val: -1, description: "Outro tipo de IVA" },
							];

							const vatTypeFoundVal = _.get(
								_.find(vat_type_values, ["val", vatTypeValAsTaxValue]),
								"code",
								"OUT",
							);

							let newTaxTemplate = {
								company_id: this.settings.ERP_API_COMPANY_ID,
								name: item.name,
								value: item.vat_value * 100, // 0,23 *100 = 23 (percentagem)

								/* type
							1: A taxa é uma percentagem (Ex.: IVA);
							2: A taxa é um valor monetário fixo, independentemente do artigo ou serviço a que é aplicado (Ex: Selo fiscal);
							3: A taxa é um valor monetário e depende do artigo ou serviço a que é aplicado (Ex.: Eco taxa). */
								type: 1,

								/* saft_type
							1: A taxa é um valor adicionado (IVA);
							2: A taxa é um imposto direto (Imposto de Selo);
							3: A taxa não é nenhum dos dois casos anteriores. */
								saft_type: 1,

								/* vat_type
							RED: IVA reduzido;
							INT: IVA intermédio;
							NOR: IVA normal;
							ISE: Isento de IVA;
							OUT: Outro tipo de IVA. */
								vat_type: vatTypeFoundVal,

								/* stamp_tax
							O campo stamp_type é obrigatório, caso saft_type=2 (Imposto de Selo) */
								stamp_tax: "",

								/* exemption_reason
							Se o país for Portugal (Códigos fiscais da zona de "PT", "PT-AC" ou "PT-MA")
							apenas os valores de "M01" para "M16" são aceitáveis. */
								exemption_reason: item.vat == 0 ? "M07" : "",

								/* fiscal_zone
							Se o país for Portugal (Códigos fiscais da zona de "PT", "PT-AC" ou "PT-MA"). */
								fiscal_zone: "PT",

								active_by_default: 0,
							};

							return this.moloni.taxes(ctx, "insert", newTaxTemplate).then((resultCreatedTax) => {
								//TODO: check if was return an error on Tax creation
								/* { valid: 1, tax_id: int } */

								if (this.settings.DEBUG) {
									this.logger.warn(
										"       ---> getTaxByTaxValue / getTaxByTaxValueFound / (insert) resultCreatedTax:",
										resultCreatedTax,
									);
								}

								return resultCreatedTax;
							});
						}
					},
				);

				// Check if PRODUCT exist
				const productFound = await this.getProductByReference(ctx, searchProductData).then(
					(resultProductFound) => {
						if (this.settings.DEBUG) {
							this.logger.warn("  getProductByReference - resultProductFound:", resultProductFound);
						}

						if (resultProductFound.length) {
							return resultProductFound[0];
						} else {
							// Product NOT FOUND, so CREATE them
							this.logger.warn("  Product NOT found");

							return this.checkProductCategoryByItem(ctx, item).then(
								(resultProductCategoryIdCreatedIfNotExists) => {
									if (this.settings.DEBUG) {
										this.logger.warn(
											"    --> getProductByReference / checkProductCategoryByItem / resultProductCategoryFound:",
											resultProductCategoryIdCreatedIfNotExists,
										);
									}

									let newProductTemplate = {
										company_id: this.settings.ERP_API_COMPANY_ID,
										category_id: resultProductCategoryIdCreatedIfNotExists,
										type: this.settings.ERP_API_DEFAULT_PRODUCT_TYPE, // 1: Produto; 2: Serviço; 3: Outros;
										name: item.name,
										summary: item.description,
										reference: item.product_code,
										price: item.liquid_unit_value, //item.unit_value,
										unit_id: this.settings.ERP_API_DEFAULT_UNIT_ID,
										has_stock: 0,
										stock: 0,
										//exemption_reason: "", //if vat_value is '0' then 'exemption_reason' must be sent
										taxes: [
											{
												tax_id: taxFound.tax_id,
												value: vatTypeValAsTaxValue,
												order: 1,
												cumulative: 0,
											},
										],
									};

									if (vatTypeValAsTaxValue == 0) {
										Object.assign(newProductTemplate, { exemption_reason: "M07" });
									}

									//TODO: check if was return an error on Tax creation
									/* { valid: 1, product_id: int } */

									return this.moloni
										.products(ctx, "insert", newProductTemplate)
										.then((resultCreatedProduct) => {
											if (this.settings.DEBUG) {
												this.logger.warn(
													"       ---> getProductByReference / checkProductCategoryByItem / (insert) resultProductCategoryFound:",
													resultCreatedProduct,
												);
											}

											return resultCreatedProduct;
										});
								},
							);
						}
					},
				);

				let docItemProductTemplate = {
					product_id: null,
					name: null,
					qty: null,
					price: null,
					discount: null,
					order: null,
					taxes: [
						{
							tax_id: taxFound.tax_id, // Tax by "value" 6, 12, 23
							value: vatTypeValAsTaxValue,
							order: ndxItem,
							cumulative: 0,
						},
					],
				};

				documentProducts.push(
					Object.assign({}, docItemProductTemplate, {
						product_id: productFound.product_id,
						name: item.name,
						summary: item.description,
						qty: item.quantity,
						price: item.liquid_unit_value, //item.unit_value,
						discount: item.discount_value,
						order: ndxItem,
					}),
				);
			}

			if (this.settings.DEBUG) {
				this.logger.warn("#### -------------------- ####");
				this.logger.warn(" ----- documentProducts ----- ");
				this.logger.warn(documentProducts);
				this.logger.warn("#### -------------------- ####");
			}

			_dummyDoc.products = documentProducts;
		},

		//TAXES
		getTaxByTaxValue(ctx, searchTaxParams) {
			// {company_id: int, value: int}
			const searchData = searchTaxParams;

			return new Promise((resolve, reject) => {
				this.moloni.taxes(ctx, "getAll", searchData).then(async (resultGetAll) => {
					const foundItem = _.find(resultGetAll, (o) => {
						return o.value == searchData.value;
					});

					if (foundItem) {
						resolve(foundItem);
					} else {
						reject("Tax not created!");
					}
				});
			});
		},

		//DOCUMENTS
		getPDFLink(ctx) {
			//Example: https://www.moloni.pt/downloads/index.php?action=getDownload&h=0048d02e33c04ec2a10ded687b1f18f8&d=419292188&e=&i=1

			return new Promise((resolve, reject) => {
				this.moloni.documents(ctx, "getPDFLink", ctx.params).then((result) => {
					let res = result;

					const pdfDownloadUri = "https://www.moloni.pt/downloads/index.php?";
					let pdfLinkUriParams = {
						action: "getDownload",
					};

					if (res.url) {
						let objQueryString = qs.parse(res.url.split("?")[1]);
						Object.assign(pdfLinkUriParams, { h: objQueryString.h, d: objQueryString.d });
						Object.assign(res, {
							urlDirectDownload: pdfDownloadUri + qs.stringify(pdfLinkUriParams),
						});
					}

					resolve(res);
				});
			});
		},
		getOne(ctx, searchParams) {
			return this.moloni.documents(ctx, "getOne", searchParams);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.moloni = new Moloni();
		if (this.settings.DEBUG) {
			this.logger.warn("MS_DEBUG:", this.settings.DEBUG);
		}
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
