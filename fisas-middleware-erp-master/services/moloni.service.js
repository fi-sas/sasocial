"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

let axios = require("axios");
let querystring = require("qs");

module.exports = {
	name: "middleware_erp.moloni",

	/**
	 * Settings
	 */
	settings: {
		DEBUG: process.env.MS_DEBUG == "true",
		client_id: process.env.ERP_API_CLIENT_ID,
		client_secret: process.env.ERP_API_CLIENT_SECRET,
		username: process.env.ERP_API_USERNAME,
		password: process.env.ERP_API_PASSWORD,
		rest_base_host: process.env.ERP_API_BASE_HOST,
		rest_base_url: process.env.ERP_API_BASE_URL,
		sandbox: process.env.ERP_API_SANDBOX == "true" || false,
		headers: {
			Accept: "*/*",
			Connection: "keep-alive", //'close',
			"User-Agent": "node-moloni/0.0.1",
		},
	},
	hooks: {
		before: {},
		after: {},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		authenticate: {
			timeout: 0,
			handler(ctx) {
				if (this.settings.DEBUG) {
					this.logger.debug("- Requer AUTENTICAR (authenticate) -");
				}

				let parameters = {};
				parameters.grant_type = "password";
				parameters.client_id = this.settings.client_id;
				parameters.client_secret = this.settings.client_secret;
				parameters.username = this.settings.username;
				parameters.password = this.settings.password;
				return axios
					.request({
						method: "GET",
						url:
							this.settings.rest_base_host +
							this.settings.rest_base_url +
							"grant/?" +
							querystring.stringify(parameters),
					})
					.then((response) => {
						this.settings.credentials = response.data;
						return response.data;
					})
					.catch((error) => {
						this.logger.error("- autenticação response ERROR");
						this.logger.error(error);
						throw new Error(error);
					});
			},
		},
		executeRequest: {
			timeout: 0,
			params: {
				parameters: { type: "object", optional: true },
				call: { type: "string" },
				route: { type: "string" },
			},
			handler(ctx) {
				let parameters = ctx.params.parameters;
				const route = ctx.params.route;
				const call = ctx.params.call;

				if (this.settings.DEBUG) {
					this.logger.debug("");
					this.logger.debug("# --------------------------- #");
					this.logger.debug("# --> [MOLONI] executeRequest #");
					this.logger.debug("# --------------------------- #");
					this.logger.debug("");
				}

				if (
					!this.settings.credentials ||
					!this.settings.credentials.access_token ||
					this.settings.credentials.expires < Date.now()
				) {
					return ctx
						.call("middleware_erp.moloni.authenticate")
						.then((result) => {
							if (this.settings.DEBUG) {
								this.logger.error("middleware_erp.moloni.authenticate result:");
								this.logger.error(result);
							}

							// must convert received 'expires' (3600 in seconds) minus 10 seconds (3590 seconds) to miliSeconds
							let expires = Date.now() + (this.settings.credentials.expires_in - 10) * 1000;
							this.settings.credentials.expires = expires; // 3590 segundos

							return ctx.call("middleware_erp.moloni.executeRequest", {
								route,
								call,
								parameters,
							});
						})
						.catch((err) => {
							this.logger.error("middleware_erp.moloni.authenticate err:");
							this.logger.error(err);
							return err;
						});
				} else {
					let method = "POST";

					if (
						(parameters && call != "getMe") ||
						(parameters && call == "getAll" && route != "companies")
					) {
						//method = "POST";
					} else {
						method = "GET";
						parameters = false;
					}

					if (this.settings.DEBUG) {
						this.logger.warn("ANTES DO REQUEST");
						this.logger.warn("#--------------#");
					}

					if (this.settings.DEBUG) {
						let reqUrl =
							this.settings.rest_base_host +
							this.settings.rest_base_url +
							route +
							"/" +
							call +
							"/?human_errors=true&json=true&access_token=" +
							this.settings.credentials.access_token;

						let reqData = parameters;

						if (this.settings.DEBUG) {
							this.logger.warn(" reqUrl:", method + " " + reqUrl);
							this.logger.warn(" reqData:", reqData);
						}
					}

					return axios
						.request({
							method,
							url:
								this.settings.rest_base_host +
								this.settings.rest_base_url +
								route +
								"/" +
								call +
								"/?human_errors=true&json=true&access_token=" +
								this.settings.credentials.access_token,
							//headers: this.headers,
							data: parameters,
						})
						.then((response) => {
							if (this.settings.DEBUG) {
								this.logger.warn(" - execute request response.data");
								this.logger.warn(response.data);
							}
							return response.data;
						})
						.catch((ex) => {
							if (ex.response) {
								this.logger.error("Error Moloni");
								this.logger.error(ex.response.data ? ex.response : ex.response.data);
								return ex.response.data ? ex.response : ex.response.data;
							}
							this.logger.error(ex);
							return ex;
						});
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
