var VERSION = "0.0.1";

function Moloni(options) {
	if (!(this instanceof Moloni)) return new Moloni();
	this._moloni_service = "middleware_erp.moloni.executeRequest";
}
Moloni.VERSION = VERSION;
module.exports = Moloni;

Moloni.prototype.users = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "users", call, parameters: params });
};

Moloni.prototype.companies = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "companies", call, parameters: params });
};

Moloni.prototype.countries = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "countries", call, parameters: params });
};
Moloni.prototype.fiscalZones = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "fiscalZones", call, parameters: params });
};
Moloni.prototype.languages = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "languages", call, parameters: params });
};

Moloni.prototype.subscription = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "subscription", call, parameters: params });
};

Moloni.prototype.customers = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "customers", call, parameters: params });
};

Moloni.prototype.productCategories = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "productCategories", call, parameters: params });
};

Moloni.prototype.products = function (ctx, call, params) {
	/*if (typeof params == 'function') {
		callback = params;
		params = false;
	}
	this._request.executeRequest('products', call, params, callback);
	return this;*/
	return ctx.call(this._moloni_service, {
		route: "products",
		call,
		parameters: params,
	});
};

Moloni.prototype.bankAccounts = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "bankAccounts", call, parameters: params });
};

Moloni.prototype.paymentMethods = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "paymentMethods", call, parameters: params });
};

Moloni.prototype.maturityDates = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "maturityDates", call, parameters: params });
};

Moloni.prototype.deliveryMethods = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "deliveryMethods", call, parameters: params });
};

Moloni.prototype.vehicles = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "vehicles", call, parameters: params });
};

Moloni.prototype.deductions = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "deductions", call, parameters: params });
};

Moloni.prototype.taxExemptions = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "taxExemptions", call, parameters: params });
};

Moloni.prototype.taxes = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "taxes", call, parameters: params });
};

Moloni.prototype.measurementUnits = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "measurementUnits", call, parameters: params });
};

Moloni.prototype.identificationTemplates = function (ctx, call, params) {
	return ctx.call(this._moloni_service, {
		route: "identificationTemplates",
		call,
		parameters: params,
	});
};

Moloni.prototype.documentSets = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "documentSets", call, parameters: params });
};

Moloni.prototype.customerAlternateAddresses = function (ctx, call, params) {
	return ctx.call(this._moloni_service, {
		route: "customerAlternateAddresses",
		call,
		parameters: params,
	});
};

Moloni.prototype.suppliers = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "suppliers", call, parameters: params });
};

Moloni.prototype.salesmen = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "salesmen", call, parameters: params });
};

Moloni.prototype.documents = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "documents", call, parameters: params });
};

Moloni.prototype.invoices = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "invoices", call, parameters: params });
};

Moloni.prototype.receipts = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "receipts", call, parameters: params });
};

Moloni.prototype.creditNotes = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "creditNotes", call, parameters: params });
};

Moloni.prototype.debitNotes = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "debitNotes", call, parameters: params });
};

Moloni.prototype.simplifiedInvoices = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "simplifiedInvoices", call, parameters: params });
};

Moloni.prototype.deliveryNotes = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "deliveryNotes", call, parameters: params });
};

Moloni.prototype.billsOfLading = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "billsOfLading", call, parameters: params });
};

Moloni.prototype.ownAssetsMovementGuides = function (ctx, call, params) {
	return ctx.call(this._moloni_service, {
		route: "ownAssetsMovementGuides",
		call,
		parameters: params,
	});
};

Moloni.prototype.waybills = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "waybills", call, parameters: params });
};

Moloni.prototype.customerReturnNotes = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "customerReturnNotes", call, parameters: params });
};

Moloni.prototype.estimates = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "estimates", call, parameters: params });
};

Moloni.prototype.invoiceReceipts = function (ctx, call, params) {
	return ctx.call(this._moloni_service, { route: "invoiceReceipts", call, parameters: params });
};
