[
    {
        "iso_3166_1": "pt",
        "country_id": 1,
        "order": 1,
        "name": "Portugal",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 1,
                "language_id": 1,
                "name": "Portugal"
            },
            {
                "country_id": 1,
                "language_id": 2,
                "name": "Portugal"
            },
            {
                "country_id": 1,
                "language_id": 3,
                "name": "Portugal"
            }
        ],
        "fiscal_zones": [
            {
                "fiscal_zone_id": 1,
                "country_id": 1,
                "name": "Madeira",
                "code": "PT-MA"
            },
            {
                "fiscal_zone_id": 2,
                "country_id": 1,
                "name": "Açores",
                "code": "PT-AC"
            }
        ]
    },
    {
        "iso_3166_1": "br",
        "country_id": 33,
        "order": 2,
        "name": "Brasil",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 33,
                "language_id": 1,
                "name": "Brasil"
            },
            {
                "country_id": 33,
                "language_id": 2,
                "name": "Brazil"
            },
            {
                "country_id": 33,
                "language_id": 3,
                "name": "Brasil"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ao",
        "country_id": 8,
        "order": 3,
        "name": "Angola",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 8,
                "language_id": 1,
                "name": "Angola"
            },
            {
                "country_id": 8,
                "language_id": 2,
                "name": "Angola"
            },
            {
                "country_id": 8,
                "language_id": 3,
                "name": "Angola"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cv",
        "country_id": 39,
        "order": 4,
        "name": "Cabo Verde",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 39,
                "language_id": 1,
                "name": "Cabo Verde"
            },
            {
                "country_id": 39,
                "language_id": 2,
                "name": "Cape Verde"
            },
            {
                "country_id": 39,
                "language_id": 3,
                "name": "Cabo Verde"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mz",
        "country_id": 142,
        "order": 5,
        "name": "Moçambique",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 142,
                "language_id": 1,
                "name": "Moçambique"
            },
            {
                "country_id": 142,
                "language_id": 2,
                "name": "Mozambique"
            },
            {
                "country_id": 142,
                "language_id": 3,
                "name": "Mozambique"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "st",
        "country_id": 185,
        "order": 6,
        "name": "S. Tomé e Príncipe",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 185,
                "language_id": 1,
                "name": "S. Tomé e Príncipe"
            },
            {
                "country_id": 185,
                "language_id": 2,
                "name": "S. Tome and Principe"
            },
            {
                "country_id": 185,
                "language_id": 3,
                "name": "S. Tomé y Príncipe"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "es",
        "country_id": 70,
        "order": 10,
        "name": "Espanha",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 70,
                "language_id": 1,
                "name": "Espanha"
            },
            {
                "country_id": 70,
                "language_id": 2,
                "name": "Spain"
            },
            {
                "country_id": 70,
                "language_id": 3,
                "name": "España"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gb",
        "country_id": 174,
        "order": 11,
        "name": "Reino Unido",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 174,
                "language_id": 1,
                "name": "Reino Unido"
            },
            {
                "country_id": 174,
                "language_id": 2,
                "name": "United Kingdom"
            },
            {
                "country_id": 174,
                "language_id": 3,
                "name": "Reino Unido"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fr",
        "country_id": 79,
        "order": 12,
        "name": "França",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 79,
                "language_id": 1,
                "name": "França"
            },
            {
                "country_id": 79,
                "language_id": 2,
                "name": "France"
            },
            {
                "country_id": 79,
                "language_id": 3,
                "name": "Francia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "de",
        "country_id": 6,
        "order": 13,
        "name": "Alemanha",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 6,
                "language_id": 1,
                "name": "Alemanha"
            },
            {
                "country_id": 6,
                "language_id": 2,
                "name": "Germany"
            },
            {
                "country_id": 6,
                "language_id": 3,
                "name": "Alemania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fi",
        "country_id": 77,
        "order": 14,
        "name": "Finlândia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 77,
                "language_id": 1,
                "name": "Finlândia"
            },
            {
                "country_id": 77,
                "language_id": 2,
                "name": "Finland"
            },
            {
                "country_id": 77,
                "language_id": 3,
                "name": "Finlandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "us",
        "country_id": 71,
        "order": 15,
        "name": "Estados Unidos da América",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 71,
                "language_id": 1,
                "name": "Estados Unidos da América"
            },
            {
                "country_id": 71,
                "language_id": 2,
                "name": "USA"
            },
            {
                "country_id": 71,
                "language_id": 3,
                "name": "EE.UU."
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ie",
        "country_id": 107,
        "order": 16,
        "name": "República da Irlanda",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 107,
                "language_id": 1,
                "name": "República da Irlanda"
            },
            {
                "country_id": 107,
                "language_id": 2,
                "name": "Republic of Ireland"
            },
            {
                "country_id": 107,
                "language_id": 3,
                "name": "República de Irlanda"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "af",
        "country_id": 2,
        "order": 100,
        "name": "Afeganistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 2,
                "language_id": 1,
                "name": "Afeganistão"
            },
            {
                "country_id": 2,
                "language_id": 2,
                "name": "Afghanistan"
            },
            {
                "country_id": 2,
                "language_id": 3,
                "name": "Afganistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "za",
        "country_id": 3,
        "order": 100,
        "name": "Africa do Sul",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 3,
                "language_id": 1,
                "name": "Africa do Sul"
            },
            {
                "country_id": 3,
                "language_id": 2,
                "name": "South Africa"
            },
            {
                "country_id": 3,
                "language_id": 3,
                "name": "Sudáfrica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "us",
        "country_id": 4,
        "order": 100,
        "name": "Alaska",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 4,
                "language_id": 1,
                "name": "Alaska"
            },
            {
                "country_id": 4,
                "language_id": 2,
                "name": "Alaska"
            },
            {
                "country_id": 4,
                "language_id": 3,
                "name": "Alaska"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "al",
        "country_id": 5,
        "order": 100,
        "name": "Albânia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 5,
                "language_id": 1,
                "name": "Albânia"
            },
            {
                "country_id": 5,
                "language_id": 2,
                "name": "Albania"
            },
            {
                "country_id": 5,
                "language_id": 3,
                "name": "Albania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ad",
        "country_id": 7,
        "order": 100,
        "name": "Andorra",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 7,
                "language_id": 1,
                "name": "Andorra"
            },
            {
                "country_id": 7,
                "language_id": 2,
                "name": "Andorra"
            },
            {
                "country_id": 7,
                "language_id": 3,
                "name": "Andorra"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ai",
        "country_id": 9,
        "order": 100,
        "name": "Anguilla",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 9,
                "language_id": 1,
                "name": "Anguilla"
            },
            {
                "country_id": 9,
                "language_id": 2,
                "name": "Anguilla"
            },
            {
                "country_id": 9,
                "language_id": 3,
                "name": "Anguilla"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ag",
        "country_id": 10,
        "order": 100,
        "name": "Antígua e Barbuda",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 10,
                "language_id": 1,
                "name": "Antígua e Barbuda"
            },
            {
                "country_id": 10,
                "language_id": 2,
                "name": "Antigua and Barbuda"
            },
            {
                "country_id": 10,
                "language_id": 3,
                "name": "Antigua y Barbuda"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "an",
        "country_id": 11,
        "order": 100,
        "name": "Antilhas Holandesas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 11,
                "language_id": 1,
                "name": "Antilhas Holandesas"
            },
            {
                "country_id": 11,
                "language_id": 2,
                "name": "Antilles"
            },
            {
                "country_id": 11,
                "language_id": 3,
                "name": "Antillas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sa",
        "country_id": 12,
        "order": 100,
        "name": "Arábia Saudita",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 12,
                "language_id": 1,
                "name": "Arábia Saudita"
            },
            {
                "country_id": 12,
                "language_id": 2,
                "name": "Saudi Arabia"
            },
            {
                "country_id": 12,
                "language_id": 3,
                "name": "Arabia Saudita"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "dz",
        "country_id": 13,
        "order": 100,
        "name": "Argélia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 13,
                "language_id": 1,
                "name": "Argélia"
            },
            {
                "country_id": 13,
                "language_id": 2,
                "name": "Algeria"
            },
            {
                "country_id": 13,
                "language_id": 3,
                "name": "Argelia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ar",
        "country_id": 14,
        "order": 100,
        "name": "Argentina",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 14,
                "language_id": 1,
                "name": "Argentina"
            },
            {
                "country_id": 14,
                "language_id": 2,
                "name": "Argentina"
            },
            {
                "country_id": 14,
                "language_id": 3,
                "name": "Argentina"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "am",
        "country_id": 15,
        "order": 100,
        "name": "Arménia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 15,
                "language_id": 1,
                "name": "Arménia"
            },
            {
                "country_id": 15,
                "language_id": 2,
                "name": "Armenia"
            },
            {
                "country_id": 15,
                "language_id": 3,
                "name": "Armenia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "aw",
        "country_id": 16,
        "order": 100,
        "name": "Aruba",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 16,
                "language_id": 1,
                "name": "Aruba"
            },
            {
                "country_id": 16,
                "language_id": 2,
                "name": "Aruba"
            },
            {
                "country_id": 16,
                "language_id": 3,
                "name": "Aruba"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ac",
        "country_id": 17,
        "order": 100,
        "name": "Ascensão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 17,
                "language_id": 1,
                "name": "Ascensão"
            },
            {
                "country_id": 17,
                "language_id": 2,
                "name": "rise"
            },
            {
                "country_id": 17,
                "language_id": 3,
                "name": "Ascensão"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "au",
        "country_id": 18,
        "order": 100,
        "name": "Austrália",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 18,
                "language_id": 1,
                "name": "Austrália"
            },
            {
                "country_id": 18,
                "language_id": 2,
                "name": "Australia"
            },
            {
                "country_id": 18,
                "language_id": 3,
                "name": "Australia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "at",
        "country_id": 19,
        "order": 100,
        "name": "Áustria",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 19,
                "language_id": 1,
                "name": "Áustria"
            },
            {
                "country_id": 19,
                "language_id": 2,
                "name": "Austria"
            },
            {
                "country_id": 19,
                "language_id": 3,
                "name": "Austria"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "az",
        "country_id": 20,
        "order": 100,
        "name": "Azerbaijão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 20,
                "language_id": 1,
                "name": "Azerbaijão"
            },
            {
                "country_id": 20,
                "language_id": 2,
                "name": "Azerbaijan"
            },
            {
                "country_id": 20,
                "language_id": 3,
                "name": "Azerbaiyán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bs",
        "country_id": 21,
        "order": 100,
        "name": "Bahamas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 21,
                "language_id": 1,
                "name": "Bahamas"
            },
            {
                "country_id": 21,
                "language_id": 2,
                "name": "Bahamas"
            },
            {
                "country_id": 21,
                "language_id": 3,
                "name": "Bahamas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bh",
        "country_id": 22,
        "order": 100,
        "name": "Bahrein",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 22,
                "language_id": 1,
                "name": "Bahrein"
            },
            {
                "country_id": 22,
                "language_id": 2,
                "name": "Bahrain"
            },
            {
                "country_id": 22,
                "language_id": 3,
                "name": "Bahrein"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bd",
        "country_id": 23,
        "order": 100,
        "name": "Bangladesh",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 23,
                "language_id": 1,
                "name": "Bangladesh"
            },
            {
                "country_id": 23,
                "language_id": 2,
                "name": "Bangladesh"
            },
            {
                "country_id": 23,
                "language_id": 3,
                "name": "Bangladesh"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bb",
        "country_id": 24,
        "order": 100,
        "name": "Barbados",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 24,
                "language_id": 1,
                "name": "Barbados"
            },
            {
                "country_id": 24,
                "language_id": 2,
                "name": "Barbados"
            },
            {
                "country_id": 24,
                "language_id": 3,
                "name": "Barbados"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "be",
        "country_id": 25,
        "order": 100,
        "name": "Bélgica",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 25,
                "language_id": 1,
                "name": "Bélgica"
            },
            {
                "country_id": 25,
                "language_id": 2,
                "name": "Belgium"
            },
            {
                "country_id": 25,
                "language_id": 3,
                "name": "Bélgica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bz",
        "country_id": 26,
        "order": 100,
        "name": "Belize",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 26,
                "language_id": 1,
                "name": "Belize"
            },
            {
                "country_id": 26,
                "language_id": 2,
                "name": "Belize"
            },
            {
                "country_id": 26,
                "language_id": 3,
                "name": "Belice"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bj",
        "country_id": 27,
        "order": 100,
        "name": "Benin",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 27,
                "language_id": 1,
                "name": "Benin"
            },
            {
                "country_id": 27,
                "language_id": 2,
                "name": "Benin"
            },
            {
                "country_id": 27,
                "language_id": 3,
                "name": "Benin"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bm",
        "country_id": 28,
        "order": 100,
        "name": "Bermudas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 28,
                "language_id": 1,
                "name": "Bermudas"
            },
            {
                "country_id": 28,
                "language_id": 2,
                "name": "Bermuda"
            },
            {
                "country_id": 28,
                "language_id": 3,
                "name": "Bermuda"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "by",
        "country_id": 29,
        "order": 100,
        "name": "Bielorrussia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 29,
                "language_id": 1,
                "name": "Bielorrussia"
            },
            {
                "country_id": 29,
                "language_id": 2,
                "name": "Belarus"
            },
            {
                "country_id": 29,
                "language_id": 3,
                "name": "Bielorrusia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bo",
        "country_id": 30,
        "order": 100,
        "name": "Bolívia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 30,
                "language_id": 1,
                "name": "Bolívia"
            },
            {
                "country_id": 30,
                "language_id": 2,
                "name": "Bolivia"
            },
            {
                "country_id": 30,
                "language_id": 3,
                "name": "Bolivia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ba",
        "country_id": 31,
        "order": 100,
        "name": "Bósnia-Herzegovina",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 31,
                "language_id": 1,
                "name": "Bósnia-Herzegovina"
            },
            {
                "country_id": 31,
                "language_id": 2,
                "name": "Bosnia - Herzegovina"
            },
            {
                "country_id": 31,
                "language_id": 3,
                "name": "Bosnia - Herzegovina"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bw",
        "country_id": 32,
        "order": 100,
        "name": "Botswana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 32,
                "language_id": 1,
                "name": "Botswana"
            },
            {
                "country_id": 32,
                "language_id": 2,
                "name": "Botswana"
            },
            {
                "country_id": 32,
                "language_id": 3,
                "name": "Botswana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bn",
        "country_id": 34,
        "order": 100,
        "name": "Brunei",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 34,
                "language_id": 1,
                "name": "Brunei"
            },
            {
                "country_id": 34,
                "language_id": 2,
                "name": "Brunei"
            },
            {
                "country_id": 34,
                "language_id": 3,
                "name": "Brunéi"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bg",
        "country_id": 35,
        "order": 100,
        "name": "Bulgária",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 35,
                "language_id": 1,
                "name": "Bulgária"
            },
            {
                "country_id": 35,
                "language_id": 2,
                "name": "Bulgaria"
            },
            {
                "country_id": 35,
                "language_id": 3,
                "name": "Bulgaria"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bf",
        "country_id": 36,
        "order": 100,
        "name": "Burkina Faso",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 36,
                "language_id": 1,
                "name": "Burkina Faso"
            },
            {
                "country_id": 36,
                "language_id": 2,
                "name": "Burkina Faso"
            },
            {
                "country_id": 36,
                "language_id": 3,
                "name": "Burkina Faso"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bi",
        "country_id": 37,
        "order": 100,
        "name": "Burundi",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 37,
                "language_id": 1,
                "name": "Burundi"
            },
            {
                "country_id": 37,
                "language_id": 2,
                "name": "Burundi"
            },
            {
                "country_id": 37,
                "language_id": 3,
                "name": "Burundi"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bt",
        "country_id": 38,
        "order": 100,
        "name": "Butão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 38,
                "language_id": 1,
                "name": "Butão"
            },
            {
                "country_id": 38,
                "language_id": 2,
                "name": "Bhutan"
            },
            {
                "country_id": 38,
                "language_id": 3,
                "name": "Bhutan"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cm",
        "country_id": 40,
        "order": 100,
        "name": "Camarões",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 40,
                "language_id": 1,
                "name": "Camarões"
            },
            {
                "country_id": 40,
                "language_id": 2,
                "name": "Cameroon"
            },
            {
                "country_id": 40,
                "language_id": 3,
                "name": "Camerún"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kh",
        "country_id": 41,
        "order": 100,
        "name": "Camboja",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 41,
                "language_id": 1,
                "name": "Camboja"
            },
            {
                "country_id": 41,
                "language_id": 2,
                "name": "Cambodia"
            },
            {
                "country_id": 41,
                "language_id": 3,
                "name": "Camboya"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ca",
        "country_id": 42,
        "order": 100,
        "name": "Canadá",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 42,
                "language_id": 1,
                "name": "Canadá"
            },
            {
                "country_id": 42,
                "language_id": 2,
                "name": "Canada"
            },
            {
                "country_id": 42,
                "language_id": 3,
                "name": "Canadá"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gd",
        "country_id": 43,
        "order": 100,
        "name": "Carriacou",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 43,
                "language_id": 1,
                "name": "Carriacou"
            },
            {
                "country_id": 43,
                "language_id": 2,
                "name": "Carriacou"
            },
            {
                "country_id": 43,
                "language_id": 3,
                "name": "Carriacou"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ky",
        "country_id": 44,
        "order": 100,
        "name": "Ilhas Caimão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 44,
                "language_id": 1,
                "name": "Ilhas Caimão"
            },
            {
                "country_id": 44,
                "language_id": 2,
                "name": "Cayman Islands"
            },
            {
                "country_id": 44,
                "language_id": 3,
                "name": "Islas Caimán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kz",
        "country_id": 45,
        "order": 100,
        "name": "Cazaquistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 45,
                "language_id": 1,
                "name": "Cazaquistão"
            },
            {
                "country_id": 45,
                "language_id": 2,
                "name": "Kazakhstan"
            },
            {
                "country_id": 45,
                "language_id": 3,
                "name": "Kazajstán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "td",
        "country_id": 46,
        "order": 100,
        "name": "Chade",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 46,
                "language_id": 1,
                "name": "Chade"
            },
            {
                "country_id": 46,
                "language_id": 2,
                "name": "Chad"
            },
            {
                "country_id": 46,
                "language_id": 3,
                "name": "Chad"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cl",
        "country_id": 47,
        "order": 100,
        "name": "Chile",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 47,
                "language_id": 1,
                "name": "Chile"
            },
            {
                "country_id": 47,
                "language_id": 2,
                "name": "Chile"
            },
            {
                "country_id": 47,
                "language_id": 3,
                "name": "Chile"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cn",
        "country_id": 48,
        "order": 100,
        "name": "China",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 48,
                "language_id": 1,
                "name": "China"
            },
            {
                "country_id": 48,
                "language_id": 2,
                "name": "China"
            },
            {
                "country_id": 48,
                "language_id": 3,
                "name": "China"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cy",
        "country_id": 49,
        "order": 100,
        "name": "Chipre",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 49,
                "language_id": 1,
                "name": "Chipre"
            },
            {
                "country_id": 49,
                "language_id": 2,
                "name": "Cyprus"
            },
            {
                "country_id": 49,
                "language_id": 3,
                "name": "Chipre"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "co",
        "country_id": 50,
        "order": 100,
        "name": "Colombia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 50,
                "language_id": 1,
                "name": "Colombia"
            },
            {
                "country_id": 50,
                "language_id": 2,
                "name": "Colombia"
            },
            {
                "country_id": 50,
                "language_id": 3,
                "name": "Colombia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "km",
        "country_id": 51,
        "order": 100,
        "name": "Comores",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 51,
                "language_id": 1,
                "name": "Comores"
            },
            {
                "country_id": 51,
                "language_id": 2,
                "name": "Comoros"
            },
            {
                "country_id": 51,
                "language_id": 3,
                "name": "Comoras"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cg",
        "country_id": 52,
        "order": 100,
        "name": "Congo",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 52,
                "language_id": 1,
                "name": "Congo"
            },
            {
                "country_id": 52,
                "language_id": 2,
                "name": "Congo"
            },
            {
                "country_id": 52,
                "language_id": 3,
                "name": "Congo"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ck",
        "country_id": 53,
        "order": 100,
        "name": "Ilhas Cook",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 53,
                "language_id": 1,
                "name": "Ilhas Cook"
            },
            {
                "country_id": 53,
                "language_id": 2,
                "name": "Cook Islands"
            },
            {
                "country_id": 53,
                "language_id": 3,
                "name": "Islas Cook"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kp",
        "country_id": 54,
        "order": 100,
        "name": "Coreia do Norte",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 54,
                "language_id": 1,
                "name": "Coreia do Norte"
            },
            {
                "country_id": 54,
                "language_id": 2,
                "name": "North Korea"
            },
            {
                "country_id": 54,
                "language_id": 3,
                "name": "Corea del Norte"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kr",
        "country_id": 55,
        "order": 100,
        "name": "Coreia do Sul",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 55,
                "language_id": 1,
                "name": "Coreia do Sul"
            },
            {
                "country_id": 55,
                "language_id": 2,
                "name": "South Korea"
            },
            {
                "country_id": 55,
                "language_id": 3,
                "name": "Corea del Sur"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ci",
        "country_id": 56,
        "order": 100,
        "name": "Costa do Marfim",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 56,
                "language_id": 1,
                "name": "Costa do Marfim"
            },
            {
                "country_id": 56,
                "language_id": 2,
                "name": "Ivory Coast"
            },
            {
                "country_id": 56,
                "language_id": 3,
                "name": "Costa de Marfil"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cr",
        "country_id": 57,
        "order": 100,
        "name": "Costa Rica",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 57,
                "language_id": 1,
                "name": "Costa Rica"
            },
            {
                "country_id": 57,
                "language_id": 2,
                "name": "Costa Rica"
            },
            {
                "country_id": 57,
                "language_id": 3,
                "name": "Costa Rica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "hr",
        "country_id": 58,
        "order": 100,
        "name": "Croácia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 58,
                "language_id": 1,
                "name": "Croácia"
            },
            {
                "country_id": 58,
                "language_id": 2,
                "name": "Croatia"
            },
            {
                "country_id": 58,
                "language_id": 3,
                "name": "Croacia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cu",
        "country_id": 59,
        "order": 100,
        "name": "Cuba",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 59,
                "language_id": 1,
                "name": "Cuba"
            },
            {
                "country_id": 59,
                "language_id": 2,
                "name": "Cuba"
            },
            {
                "country_id": 59,
                "language_id": 3,
                "name": "Cuba"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "dj",
        "country_id": 60,
        "order": 100,
        "name": "Djibouti",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 60,
                "language_id": 1,
                "name": "Djibouti"
            },
            {
                "country_id": 60,
                "language_id": 2,
                "name": "Djibouti"
            },
            {
                "country_id": 60,
                "language_id": 3,
                "name": "Djibouti"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "dk",
        "country_id": 61,
        "order": 100,
        "name": "Dinamarca",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 61,
                "language_id": 1,
                "name": "Dinamarca"
            },
            {
                "country_id": 61,
                "language_id": 2,
                "name": "Denmark"
            },
            {
                "country_id": 61,
                "language_id": 3,
                "name": "Dinamarca"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "dm",
        "country_id": 62,
        "order": 100,
        "name": "Dominica",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 62,
                "language_id": 1,
                "name": "Dominica"
            },
            {
                "country_id": 62,
                "language_id": 2,
                "name": "Dominica"
            },
            {
                "country_id": 62,
                "language_id": 3,
                "name": "Dominica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sv",
        "country_id": 63,
        "order": 100,
        "name": "El Salvador",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 63,
                "language_id": 1,
                "name": "El Salvador"
            },
            {
                "country_id": 63,
                "language_id": 2,
                "name": "El Salvador"
            },
            {
                "country_id": 63,
                "language_id": 3,
                "name": "El Salvador"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "eg",
        "country_id": 64,
        "order": 100,
        "name": "Egipto",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 64,
                "language_id": 1,
                "name": "Egipto"
            },
            {
                "country_id": 64,
                "language_id": 2,
                "name": "Egypt"
            },
            {
                "country_id": 64,
                "language_id": 3,
                "name": "Egipto"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ae",
        "country_id": 65,
        "order": 100,
        "name": "Emirados Árabes Unidos",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 65,
                "language_id": 1,
                "name": "Emirados Árabes Unidos"
            },
            {
                "country_id": 65,
                "language_id": 2,
                "name": "United Arab Emirates"
            },
            {
                "country_id": 65,
                "language_id": 3,
                "name": "Emiratos Árabes Unidos"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ec",
        "country_id": 66,
        "order": 100,
        "name": "Equador",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 66,
                "language_id": 1,
                "name": "Equador"
            },
            {
                "country_id": 66,
                "language_id": 2,
                "name": "Ecuador"
            },
            {
                "country_id": 66,
                "language_id": 3,
                "name": "Ecuador"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "er",
        "country_id": 67,
        "order": 100,
        "name": "Eritreia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 67,
                "language_id": 1,
                "name": "Eritreia"
            },
            {
                "country_id": 67,
                "language_id": 2,
                "name": "Eritrea"
            },
            {
                "country_id": 67,
                "language_id": 3,
                "name": "Eritrea"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sk",
        "country_id": 68,
        "order": 100,
        "name": "Eslováquia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 68,
                "language_id": 1,
                "name": "Eslováquia"
            },
            {
                "country_id": 68,
                "language_id": 2,
                "name": "Slovakia"
            },
            {
                "country_id": 68,
                "language_id": 3,
                "name": "Eslovaquia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "si",
        "country_id": 69,
        "order": 100,
        "name": "Eslovénia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 69,
                "language_id": 1,
                "name": "Eslovénia"
            },
            {
                "country_id": 69,
                "language_id": 2,
                "name": "Slovenia"
            },
            {
                "country_id": 69,
                "language_id": 3,
                "name": "Eslovenia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ee",
        "country_id": 72,
        "order": 100,
        "name": "Estónia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 72,
                "language_id": 1,
                "name": "Estónia"
            },
            {
                "country_id": 72,
                "language_id": 2,
                "name": "Estonia"
            },
            {
                "country_id": 72,
                "language_id": 3,
                "name": "Estonia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "et",
        "country_id": 73,
        "order": 100,
        "name": "Etiópia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 73,
                "language_id": 1,
                "name": "Etiópia"
            },
            {
                "country_id": 73,
                "language_id": 2,
                "name": "Ethiopia"
            },
            {
                "country_id": 73,
                "language_id": 3,
                "name": "Etiopía"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fo",
        "country_id": 74,
        "order": 100,
        "name": "Ilhas Feroe",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 74,
                "language_id": 1,
                "name": "Ilhas Feroe"
            },
            {
                "country_id": 74,
                "language_id": 2,
                "name": "Faroe Islands"
            },
            {
                "country_id": 74,
                "language_id": 3,
                "name": "Islas Feroe"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fj",
        "country_id": 75,
        "order": 100,
        "name": "Fiji",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 75,
                "language_id": 1,
                "name": "Fiji"
            },
            {
                "country_id": 75,
                "language_id": 2,
                "name": "Fiji"
            },
            {
                "country_id": 75,
                "language_id": 3,
                "name": "Fiji"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ph",
        "country_id": 76,
        "order": 100,
        "name": "Filipinas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 76,
                "language_id": 1,
                "name": "Filipinas"
            },
            {
                "country_id": 76,
                "language_id": 2,
                "name": "Philippines"
            },
            {
                "country_id": 76,
                "language_id": 3,
                "name": "Filipinas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tw",
        "country_id": 78,
        "order": 100,
        "name": "Formosa",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 78,
                "language_id": 1,
                "name": "Formosa"
            },
            {
                "country_id": 78,
                "language_id": 2,
                "name": "Taiwan"
            },
            {
                "country_id": 78,
                "language_id": 3,
                "name": "Formosa"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ga",
        "country_id": 80,
        "order": 100,
        "name": "Gabão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 80,
                "language_id": 1,
                "name": "Gabão"
            },
            {
                "country_id": 80,
                "language_id": 2,
                "name": "Gabon"
            },
            {
                "country_id": 80,
                "language_id": 3,
                "name": "Gabón"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gm",
        "country_id": 81,
        "order": 100,
        "name": "Gâmbia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 81,
                "language_id": 1,
                "name": "Gâmbia"
            },
            {
                "country_id": 81,
                "language_id": 2,
                "name": "Gambia"
            },
            {
                "country_id": 81,
                "language_id": 3,
                "name": "Gambia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gh",
        "country_id": 82,
        "order": 100,
        "name": "Gana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 82,
                "language_id": 1,
                "name": "Gana"
            },
            {
                "country_id": 82,
                "language_id": 2,
                "name": "Ghana"
            },
            {
                "country_id": 82,
                "language_id": 3,
                "name": "Ghana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ge",
        "country_id": 83,
        "order": 100,
        "name": "Geórgia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 83,
                "language_id": 1,
                "name": "Geórgia"
            },
            {
                "country_id": 83,
                "language_id": 2,
                "name": "Georgia"
            },
            {
                "country_id": 83,
                "language_id": 3,
                "name": "Georgia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gi",
        "country_id": 84,
        "order": 100,
        "name": "Gibraltar",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 84,
                "language_id": 1,
                "name": "Gibraltar"
            },
            {
                "country_id": 84,
                "language_id": 2,
                "name": "Gibraltar"
            },
            {
                "country_id": 84,
                "language_id": 3,
                "name": "Gibraltar"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gd",
        "country_id": 85,
        "order": 100,
        "name": "Granada",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 85,
                "language_id": 1,
                "name": "Granada"
            },
            {
                "country_id": 85,
                "language_id": 2,
                "name": "grenade"
            },
            {
                "country_id": 85,
                "language_id": 3,
                "name": "Granada"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gr",
        "country_id": 86,
        "order": 100,
        "name": "Grécia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 86,
                "language_id": 1,
                "name": "Grécia"
            },
            {
                "country_id": 86,
                "language_id": 2,
                "name": "Greece"
            },
            {
                "country_id": 86,
                "language_id": 3,
                "name": "Grecia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gl",
        "country_id": 87,
        "order": 100,
        "name": "Gronelândia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 87,
                "language_id": 1,
                "name": "Gronelândia"
            },
            {
                "country_id": 87,
                "language_id": 2,
                "name": "Greenland"
            },
            {
                "country_id": 87,
                "language_id": 3,
                "name": "Groenlandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gp",
        "country_id": 88,
        "order": 100,
        "name": "Guadalupe",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 88,
                "language_id": 1,
                "name": "Guadalupe"
            },
            {
                "country_id": 88,
                "language_id": 2,
                "name": "Guadalupe"
            },
            {
                "country_id": 88,
                "language_id": 3,
                "name": "Guadalupe"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gu",
        "country_id": 89,
        "order": 100,
        "name": "Guam",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 89,
                "language_id": 1,
                "name": "Guam"
            },
            {
                "country_id": 89,
                "language_id": 2,
                "name": "Guam"
            },
            {
                "country_id": 89,
                "language_id": 3,
                "name": "Guam"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gt",
        "country_id": 90,
        "order": 100,
        "name": "Guatemala",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 90,
                "language_id": 1,
                "name": "Guatemala"
            },
            {
                "country_id": 90,
                "language_id": 2,
                "name": "Guatemala"
            },
            {
                "country_id": 90,
                "language_id": 3,
                "name": "Guatemala"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gy",
        "country_id": 91,
        "order": 100,
        "name": "Guiana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 91,
                "language_id": 1,
                "name": "Guiana"
            },
            {
                "country_id": 91,
                "language_id": 2,
                "name": "Guyana"
            },
            {
                "country_id": 91,
                "language_id": 3,
                "name": "Guayana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gf",
        "country_id": 92,
        "order": 100,
        "name": "Guiana Francesa",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 92,
                "language_id": 1,
                "name": "Guiana Francesa"
            },
            {
                "country_id": 92,
                "language_id": 2,
                "name": "French Guiana"
            },
            {
                "country_id": 92,
                "language_id": 3,
                "name": "Guiana francés"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gw",
        "country_id": 93,
        "order": 100,
        "name": "Guiné - Bissau",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 93,
                "language_id": 1,
                "name": "Guiné - Bissau"
            },
            {
                "country_id": 93,
                "language_id": 2,
                "name": "Guinea - Bissau"
            },
            {
                "country_id": 93,
                "language_id": 3,
                "name": "Guinea - Bissau"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gn",
        "country_id": 94,
        "order": 100,
        "name": "Guiné-Conacri",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 94,
                "language_id": 1,
                "name": "Guiné-Conacri"
            },
            {
                "country_id": 94,
                "language_id": 2,
                "name": "Guinea"
            },
            {
                "country_id": 94,
                "language_id": 3,
                "name": "Guinea"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gq",
        "country_id": 95,
        "order": 100,
        "name": "Guiné Equatorial",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 95,
                "language_id": 1,
                "name": "Guiné Equatorial"
            },
            {
                "country_id": 95,
                "language_id": 2,
                "name": "Equatorial Guinea"
            },
            {
                "country_id": 95,
                "language_id": 3,
                "name": "Guinea Ecuatorial"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ht",
        "country_id": 96,
        "order": 100,
        "name": "Haiti",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 96,
                "language_id": 1,
                "name": "Haiti"
            },
            {
                "country_id": 96,
                "language_id": 2,
                "name": "Haiti"
            },
            {
                "country_id": 96,
                "language_id": 3,
                "name": "Haití"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "us",
        "country_id": 97,
        "order": 100,
        "name": "Hawai",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 97,
                "language_id": 1,
                "name": "Hawai"
            },
            {
                "country_id": 97,
                "language_id": 2,
                "name": "Hawai"
            },
            {
                "country_id": 97,
                "language_id": 3,
                "name": "Hawai"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nl",
        "country_id": 98,
        "order": 100,
        "name": "Holanda",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 98,
                "language_id": 1,
                "name": "Holanda"
            },
            {
                "country_id": 98,
                "language_id": 2,
                "name": "Netherlands"
            },
            {
                "country_id": 98,
                "language_id": 3,
                "name": "Países Bajos"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "hn",
        "country_id": 99,
        "order": 100,
        "name": "Honduras",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 99,
                "language_id": 1,
                "name": "Honduras"
            },
            {
                "country_id": 99,
                "language_id": 2,
                "name": "Honduras"
            },
            {
                "country_id": 99,
                "language_id": 3,
                "name": "Honduras"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "hk",
        "country_id": 100,
        "order": 100,
        "name": "Hong Kong",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 100,
                "language_id": 1,
                "name": "Hong Kong"
            },
            {
                "country_id": 100,
                "language_id": 2,
                "name": "Hong Kong"
            },
            {
                "country_id": 100,
                "language_id": 3,
                "name": "Hong Kong"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "hu",
        "country_id": 101,
        "order": 100,
        "name": "Hungria",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 101,
                "language_id": 1,
                "name": "Hungria"
            },
            {
                "country_id": 101,
                "language_id": 2,
                "name": "Hungary"
            },
            {
                "country_id": 101,
                "language_id": 3,
                "name": "Hungría"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ye",
        "country_id": 102,
        "order": 100,
        "name": "Iémen",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 102,
                "language_id": 1,
                "name": "Iémen"
            },
            {
                "country_id": 102,
                "language_id": 2,
                "name": "Yemen"
            },
            {
                "country_id": 102,
                "language_id": 3,
                "name": "Yemen"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "in",
        "country_id": 103,
        "order": 100,
        "name": "India",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 103,
                "language_id": 1,
                "name": "India"
            },
            {
                "country_id": 103,
                "language_id": 2,
                "name": "India"
            },
            {
                "country_id": 103,
                "language_id": 3,
                "name": "India"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "id",
        "country_id": 104,
        "order": 100,
        "name": "Indonésia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 104,
                "language_id": 1,
                "name": "Indonésia"
            },
            {
                "country_id": 104,
                "language_id": 2,
                "name": "Indonesia"
            },
            {
                "country_id": 104,
                "language_id": 3,
                "name": "Indonesia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ir",
        "country_id": 105,
        "order": 100,
        "name": "Irão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 105,
                "language_id": 1,
                "name": "Irão"
            },
            {
                "country_id": 105,
                "language_id": 2,
                "name": "Iran"
            },
            {
                "country_id": 105,
                "language_id": 3,
                "name": "Irán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "iq",
        "country_id": 106,
        "order": 100,
        "name": "Iraque",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 106,
                "language_id": 1,
                "name": "Iraque"
            },
            {
                "country_id": 106,
                "language_id": 2,
                "name": "Iraq"
            },
            {
                "country_id": 106,
                "language_id": 3,
                "name": "Irak"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gb",
        "country_id": 108,
        "order": 100,
        "name": "Irlanda do Norte",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 108,
                "language_id": 1,
                "name": "Irlanda do Norte"
            },
            {
                "country_id": 108,
                "language_id": 2,
                "name": "Northern Ireland"
            },
            {
                "country_id": 108,
                "language_id": 3,
                "name": "Irlanda del Norte"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "is",
        "country_id": 109,
        "order": 100,
        "name": "Islândia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 109,
                "language_id": 1,
                "name": "Islândia"
            },
            {
                "country_id": 109,
                "language_id": 2,
                "name": "Iceland"
            },
            {
                "country_id": 109,
                "language_id": 3,
                "name": "Islandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "il",
        "country_id": 110,
        "order": 100,
        "name": "Israel",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 110,
                "language_id": 1,
                "name": "Israel"
            },
            {
                "country_id": 110,
                "language_id": 2,
                "name": "Israel"
            },
            {
                "country_id": 110,
                "language_id": 3,
                "name": "Israel"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "it",
        "country_id": 111,
        "order": 100,
        "name": "Itália",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 111,
                "language_id": 1,
                "name": "Itália"
            },
            {
                "country_id": 111,
                "language_id": 2,
                "name": "Italy"
            },
            {
                "country_id": 111,
                "language_id": 3,
                "name": "Italia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "jm",
        "country_id": 112,
        "order": 100,
        "name": "Jamaica",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 112,
                "language_id": 1,
                "name": "Jamaica"
            },
            {
                "country_id": 112,
                "language_id": 2,
                "name": "Jamaica"
            },
            {
                "country_id": 112,
                "language_id": 3,
                "name": "Jamaica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "jp",
        "country_id": 113,
        "order": 100,
        "name": "Japão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 113,
                "language_id": 1,
                "name": "Japão"
            },
            {
                "country_id": 113,
                "language_id": 2,
                "name": "Japan"
            },
            {
                "country_id": 113,
                "language_id": 3,
                "name": "Japón"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "jo",
        "country_id": 114,
        "order": 100,
        "name": "Jordânia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 114,
                "language_id": 1,
                "name": "Jordânia"
            },
            {
                "country_id": 114,
                "language_id": 2,
                "name": "Jordan"
            },
            {
                "country_id": 114,
                "language_id": 3,
                "name": "Jordania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kw",
        "country_id": 115,
        "order": 100,
        "name": "Koweit",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 115,
                "language_id": 1,
                "name": "Koweit"
            },
            {
                "country_id": 115,
                "language_id": 2,
                "name": "Kuwait"
            },
            {
                "country_id": 115,
                "language_id": 3,
                "name": "Kuwait"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "la",
        "country_id": 116,
        "order": 100,
        "name": "Laos",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 116,
                "language_id": 1,
                "name": "Laos"
            },
            {
                "country_id": 116,
                "language_id": 2,
                "name": "Laos"
            },
            {
                "country_id": 116,
                "language_id": 3,
                "name": "Laos"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ls",
        "country_id": 117,
        "order": 100,
        "name": "Lesoto",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 117,
                "language_id": 1,
                "name": "Lesoto"
            },
            {
                "country_id": 117,
                "language_id": 2,
                "name": "Lesotho"
            },
            {
                "country_id": 117,
                "language_id": 3,
                "name": "Lesoto"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lv",
        "country_id": 118,
        "order": 100,
        "name": "Letónia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 118,
                "language_id": 1,
                "name": "Letónia"
            },
            {
                "country_id": 118,
                "language_id": 2,
                "name": "Latvia"
            },
            {
                "country_id": 118,
                "language_id": 3,
                "name": "Letonia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lb",
        "country_id": 119,
        "order": 100,
        "name": "Líbano",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 119,
                "language_id": 1,
                "name": "Líbano"
            },
            {
                "country_id": 119,
                "language_id": 2,
                "name": "Lebanon"
            },
            {
                "country_id": 119,
                "language_id": 3,
                "name": "Líbano"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lr",
        "country_id": 120,
        "order": 100,
        "name": "Libéria",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 120,
                "language_id": 1,
                "name": "Libéria"
            },
            {
                "country_id": 120,
                "language_id": 2,
                "name": "Liberia"
            },
            {
                "country_id": 120,
                "language_id": 3,
                "name": "Liberia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ly",
        "country_id": 121,
        "order": 100,
        "name": "Líbia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 121,
                "language_id": 1,
                "name": "Líbia"
            },
            {
                "country_id": 121,
                "language_id": 2,
                "name": "Libya"
            },
            {
                "country_id": 121,
                "language_id": 3,
                "name": "Libia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "li",
        "country_id": 122,
        "order": 100,
        "name": "Liechtenstein",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 122,
                "language_id": 1,
                "name": "Liechtenstein"
            },
            {
                "country_id": 122,
                "language_id": 2,
                "name": "Liechtenstein"
            },
            {
                "country_id": 122,
                "language_id": 3,
                "name": "Liechtenstein"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lt",
        "country_id": 123,
        "order": 100,
        "name": "Lituânia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 123,
                "language_id": 1,
                "name": "Lituânia"
            },
            {
                "country_id": 123,
                "language_id": 2,
                "name": "Lithuania"
            },
            {
                "country_id": 123,
                "language_id": 3,
                "name": "Lituania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lu",
        "country_id": 124,
        "order": 100,
        "name": "Luxemburgo",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 124,
                "language_id": 1,
                "name": "Luxemburgo"
            },
            {
                "country_id": 124,
                "language_id": 2,
                "name": "Luxembourg"
            },
            {
                "country_id": 124,
                "language_id": 3,
                "name": "Luxemburgo"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mo",
        "country_id": 125,
        "order": 100,
        "name": "Macau",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 125,
                "language_id": 1,
                "name": "Macau"
            },
            {
                "country_id": 125,
                "language_id": 2,
                "name": "Macao"
            },
            {
                "country_id": 125,
                "language_id": 3,
                "name": "Macao"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mk",
        "country_id": 126,
        "order": 100,
        "name": "Macedónia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 126,
                "language_id": 1,
                "name": "Macedónia"
            },
            {
                "country_id": 126,
                "language_id": 2,
                "name": "Macedonia"
            },
            {
                "country_id": 126,
                "language_id": 3,
                "name": "Macedonia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mg",
        "country_id": 127,
        "order": 100,
        "name": "Madagáscar",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 127,
                "language_id": 1,
                "name": "Madagáscar"
            },
            {
                "country_id": 127,
                "language_id": 2,
                "name": "Madagascar"
            },
            {
                "country_id": 127,
                "language_id": 3,
                "name": "Madagascar"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "my",
        "country_id": 128,
        "order": 100,
        "name": "Malásia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 128,
                "language_id": 1,
                "name": "Malásia"
            },
            {
                "country_id": 128,
                "language_id": 2,
                "name": "Malaysia"
            },
            {
                "country_id": 128,
                "language_id": 3,
                "name": "Malasia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mw",
        "country_id": 129,
        "order": 100,
        "name": "Malawi",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 129,
                "language_id": 1,
                "name": "Malawi"
            },
            {
                "country_id": 129,
                "language_id": 2,
                "name": "Malawi"
            },
            {
                "country_id": 129,
                "language_id": 3,
                "name": "Malawi"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mv",
        "country_id": 130,
        "order": 100,
        "name": "Maldivas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 130,
                "language_id": 1,
                "name": "Maldivas"
            },
            {
                "country_id": 130,
                "language_id": 2,
                "name": "Maldives"
            },
            {
                "country_id": 130,
                "language_id": 3,
                "name": "Maldivas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ml",
        "country_id": 131,
        "order": 100,
        "name": "Mali",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 131,
                "language_id": 1,
                "name": "Mali"
            },
            {
                "country_id": 131,
                "language_id": 2,
                "name": "Mali"
            },
            {
                "country_id": 131,
                "language_id": 3,
                "name": "Malí"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mt",
        "country_id": 132,
        "order": 100,
        "name": "Malta",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 132,
                "language_id": 1,
                "name": "Malta"
            },
            {
                "country_id": 132,
                "language_id": 2,
                "name": "Malta"
            },
            {
                "country_id": 132,
                "language_id": 3,
                "name": "Malta"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fk",
        "country_id": 133,
        "order": 100,
        "name": "Malvinas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 133,
                "language_id": 1,
                "name": "Malvinas"
            },
            {
                "country_id": 133,
                "language_id": 2,
                "name": "Falklands"
            },
            {
                "country_id": 133,
                "language_id": 3,
                "name": "Malvinas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ma",
        "country_id": 134,
        "order": 100,
        "name": "Marrocos",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 134,
                "language_id": 1,
                "name": "Marrocos"
            },
            {
                "country_id": 134,
                "language_id": 2,
                "name": "Morocco"
            },
            {
                "country_id": 134,
                "language_id": 3,
                "name": "Marruecos"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mh",
        "country_id": 135,
        "order": 100,
        "name": "Ilhas Marshall",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 135,
                "language_id": 1,
                "name": "Ilhas Marshall"
            },
            {
                "country_id": 135,
                "language_id": 2,
                "name": "Marshall Islands"
            },
            {
                "country_id": 135,
                "language_id": 3,
                "name": "Islas Marshall"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mq",
        "country_id": 136,
        "order": 100,
        "name": "Martinica",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 136,
                "language_id": 1,
                "name": "Martinica"
            },
            {
                "country_id": 136,
                "language_id": 2,
                "name": "Martinique"
            },
            {
                "country_id": 136,
                "language_id": 3,
                "name": "Martinica"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mr",
        "country_id": 137,
        "order": 100,
        "name": "Mauritânia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 137,
                "language_id": 1,
                "name": "Mauritânia"
            },
            {
                "country_id": 137,
                "language_id": 2,
                "name": "Mauritania"
            },
            {
                "country_id": 137,
                "language_id": 3,
                "name": "Mauritania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mu",
        "country_id": 138,
        "order": 100,
        "name": "Maurícias",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 138,
                "language_id": 1,
                "name": "Maurícias"
            },
            {
                "country_id": 138,
                "language_id": 2,
                "name": "Mauritius"
            },
            {
                "country_id": 138,
                "language_id": 3,
                "name": "Mauricio"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "yt",
        "country_id": 139,
        "order": 100,
        "name": "Mayotte",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 139,
                "language_id": 1,
                "name": "Mayotte"
            },
            {
                "country_id": 139,
                "language_id": 2,
                "name": "Mayotte"
            },
            {
                "country_id": 139,
                "language_id": 3,
                "name": "Mayotte"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mx",
        "country_id": 140,
        "order": 100,
        "name": "México",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 140,
                "language_id": 1,
                "name": "México"
            },
            {
                "country_id": 140,
                "language_id": 2,
                "name": "Mexico"
            },
            {
                "country_id": 140,
                "language_id": 3,
                "name": "México"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "fm",
        "country_id": 141,
        "order": 100,
        "name": "Micronésia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 141,
                "language_id": 1,
                "name": "Micronésia"
            },
            {
                "country_id": 141,
                "language_id": 2,
                "name": "Micronesia"
            },
            {
                "country_id": 141,
                "language_id": 3,
                "name": "Micronesia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "md",
        "country_id": 143,
        "order": 100,
        "name": "Moldávia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 143,
                "language_id": 1,
                "name": "Moldávia"
            },
            {
                "country_id": 143,
                "language_id": 2,
                "name": "Moldavia"
            },
            {
                "country_id": 143,
                "language_id": 3,
                "name": "Moldavia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mc",
        "country_id": 144,
        "order": 100,
        "name": "Mónaco",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 144,
                "language_id": 1,
                "name": "Mónaco"
            },
            {
                "country_id": 144,
                "language_id": 2,
                "name": "Monaco"
            },
            {
                "country_id": 144,
                "language_id": 3,
                "name": "Mónaco"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mn",
        "country_id": 145,
        "order": 100,
        "name": "Mongólia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 145,
                "language_id": 1,
                "name": "Mongólia"
            },
            {
                "country_id": 145,
                "language_id": 2,
                "name": "Mongolia"
            },
            {
                "country_id": 145,
                "language_id": 3,
                "name": "Mongolia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ms",
        "country_id": 146,
        "order": 100,
        "name": "Montserrat",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 146,
                "language_id": 1,
                "name": "Montserrat"
            },
            {
                "country_id": 146,
                "language_id": 2,
                "name": "Montserrat"
            },
            {
                "country_id": 146,
                "language_id": 3,
                "name": "Montserrat"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "mm",
        "country_id": 147,
        "order": 100,
        "name": "Myanmar",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 147,
                "language_id": 1,
                "name": "Myanmar"
            },
            {
                "country_id": 147,
                "language_id": 2,
                "name": "Myanmar"
            },
            {
                "country_id": 147,
                "language_id": 3,
                "name": "Myanmar"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "na",
        "country_id": 148,
        "order": 100,
        "name": "Namíbia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 148,
                "language_id": 1,
                "name": "Namíbia"
            },
            {
                "country_id": 148,
                "language_id": 2,
                "name": "Namibia"
            },
            {
                "country_id": 148,
                "language_id": 3,
                "name": "Namibia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nr",
        "country_id": 149,
        "order": 100,
        "name": "Nauru",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 149,
                "language_id": 1,
                "name": "Nauru"
            },
            {
                "country_id": 149,
                "language_id": 2,
                "name": "Nauru"
            },
            {
                "country_id": 149,
                "language_id": 3,
                "name": "Nauru"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "np",
        "country_id": 150,
        "order": 100,
        "name": "Nepal",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 150,
                "language_id": 1,
                "name": "Nepal"
            },
            {
                "country_id": 150,
                "language_id": 2,
                "name": "Nepal"
            },
            {
                "country_id": 150,
                "language_id": 3,
                "name": "Nepal"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ni",
        "country_id": 151,
        "order": 100,
        "name": "Nicarágua",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 151,
                "language_id": 1,
                "name": "Nicarágua"
            },
            {
                "country_id": 151,
                "language_id": 2,
                "name": "Nicaragua"
            },
            {
                "country_id": 151,
                "language_id": 3,
                "name": "Nicaragua"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ne",
        "country_id": 152,
        "order": 100,
        "name": "Niger",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 152,
                "language_id": 1,
                "name": "Niger"
            },
            {
                "country_id": 152,
                "language_id": 2,
                "name": "Niger"
            },
            {
                "country_id": 152,
                "language_id": 3,
                "name": "Níger"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ng",
        "country_id": 153,
        "order": 100,
        "name": "Nigéria",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 153,
                "language_id": 1,
                "name": "Nigéria"
            },
            {
                "country_id": 153,
                "language_id": 2,
                "name": "Nigeria"
            },
            {
                "country_id": 153,
                "language_id": 3,
                "name": "Nigeria"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nu",
        "country_id": 154,
        "order": 100,
        "name": "Niue",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 154,
                "language_id": 1,
                "name": "Niue"
            },
            {
                "country_id": 154,
                "language_id": 2,
                "name": "Niue"
            },
            {
                "country_id": 154,
                "language_id": 3,
                "name": "Niue"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nf",
        "country_id": 155,
        "order": 100,
        "name": "Norfolk",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 155,
                "language_id": 1,
                "name": "Norfolk"
            },
            {
                "country_id": 155,
                "language_id": 2,
                "name": "Norfolk"
            },
            {
                "country_id": 155,
                "language_id": 3,
                "name": "Norfolk"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "no",
        "country_id": 156,
        "order": 100,
        "name": "Noruega",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 156,
                "language_id": 1,
                "name": "Noruega"
            },
            {
                "country_id": 156,
                "language_id": 2,
                "name": "Norway"
            },
            {
                "country_id": 156,
                "language_id": 3,
                "name": "Noruega"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nc",
        "country_id": 157,
        "order": 100,
        "name": "Nova Caledónia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 157,
                "language_id": 1,
                "name": "Nova Caledónia"
            },
            {
                "country_id": 157,
                "language_id": 2,
                "name": "New Caledonia"
            },
            {
                "country_id": 157,
                "language_id": 3,
                "name": "Nueva Caledonia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "nz",
        "country_id": 158,
        "order": 100,
        "name": "Nova Zelândia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 158,
                "language_id": 1,
                "name": "Nova Zelândia"
            },
            {
                "country_id": 158,
                "language_id": 2,
                "name": "New Zealand"
            },
            {
                "country_id": 158,
                "language_id": 3,
                "name": "Nueva Zelandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "om",
        "country_id": 159,
        "order": 100,
        "name": "Omã",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 159,
                "language_id": 1,
                "name": "Omã"
            },
            {
                "country_id": 159,
                "language_id": 2,
                "name": "Oman"
            },
            {
                "country_id": 159,
                "language_id": 3,
                "name": "Omán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pw",
        "country_id": 160,
        "order": 100,
        "name": "Palau",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 160,
                "language_id": 1,
                "name": "Palau"
            },
            {
                "country_id": 160,
                "language_id": 2,
                "name": "Palau"
            },
            {
                "country_id": 160,
                "language_id": 3,
                "name": "Palau"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pa",
        "country_id": 161,
        "order": 100,
        "name": "Panamá",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 161,
                "language_id": 1,
                "name": "Panamá"
            },
            {
                "country_id": 161,
                "language_id": 2,
                "name": "Panama"
            },
            {
                "country_id": 161,
                "language_id": 3,
                "name": "Panamá"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pg",
        "country_id": 162,
        "order": 100,
        "name": "Papua e Nova Guiné",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 162,
                "language_id": 1,
                "name": "Papua e Nova Guiné"
            },
            {
                "country_id": 162,
                "language_id": 2,
                "name": "Papua and New Guinea"
            },
            {
                "country_id": 162,
                "language_id": 3,
                "name": "Papúa y Nueva Guinea"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pk",
        "country_id": 163,
        "order": 100,
        "name": "Paquistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 163,
                "language_id": 1,
                "name": "Paquistão"
            },
            {
                "country_id": 163,
                "language_id": 2,
                "name": "Pakistan"
            },
            {
                "country_id": 163,
                "language_id": 3,
                "name": "Pakistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "py",
        "country_id": 164,
        "order": 100,
        "name": "Paraguai",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 164,
                "language_id": 1,
                "name": "Paraguai"
            },
            {
                "country_id": 164,
                "language_id": 2,
                "name": "Paraguay"
            },
            {
                "country_id": 164,
                "language_id": 3,
                "name": "Paraguay"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pe",
        "country_id": 165,
        "order": 100,
        "name": "Peru",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 165,
                "language_id": 1,
                "name": "Peru"
            },
            {
                "country_id": 165,
                "language_id": 2,
                "name": "Peru"
            },
            {
                "country_id": 165,
                "language_id": 3,
                "name": "Perú"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pn",
        "country_id": 166,
        "order": 100,
        "name": "Pitcairn",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 166,
                "language_id": 1,
                "name": "Pitcairn"
            },
            {
                "country_id": 166,
                "language_id": 2,
                "name": "Pitcairn"
            },
            {
                "country_id": 166,
                "language_id": 3,
                "name": "Pitcairn"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pf",
        "country_id": 167,
        "order": 100,
        "name": "Polinésia Francesa",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 167,
                "language_id": 1,
                "name": "Polinésia Francesa"
            },
            {
                "country_id": 167,
                "language_id": 2,
                "name": "French Polynesia"
            },
            {
                "country_id": 167,
                "language_id": 3,
                "name": "Polinesia francés"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pl",
        "country_id": 168,
        "order": 100,
        "name": "Polónia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 168,
                "language_id": 1,
                "name": "Polónia"
            },
            {
                "country_id": 168,
                "language_id": 2,
                "name": "Poland"
            },
            {
                "country_id": 168,
                "language_id": 3,
                "name": "Polonia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "pr",
        "country_id": 169,
        "order": 100,
        "name": "Porto Rico",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 169,
                "language_id": 1,
                "name": "Porto Rico"
            },
            {
                "country_id": 169,
                "language_id": 2,
                "name": "Puerto Rico"
            },
            {
                "country_id": 169,
                "language_id": 3,
                "name": "Puerto Rico"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "qa",
        "country_id": 170,
        "order": 100,
        "name": "Qatar",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 170,
                "language_id": 1,
                "name": "Qatar"
            },
            {
                "country_id": 170,
                "language_id": 2,
                "name": "Qatar"
            },
            {
                "country_id": 170,
                "language_id": 3,
                "name": "Katar"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ke",
        "country_id": 171,
        "order": 100,
        "name": "Quenia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 171,
                "language_id": 1,
                "name": "Quenia"
            },
            {
                "country_id": 171,
                "language_id": 2,
                "name": "Kenya"
            },
            {
                "country_id": 171,
                "language_id": 3,
                "name": "Kenia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kg",
        "country_id": 172,
        "order": 100,
        "name": "Quirguistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 172,
                "language_id": 1,
                "name": "Quirguistão"
            },
            {
                "country_id": 172,
                "language_id": 2,
                "name": "Kyrgyzstan"
            },
            {
                "country_id": 172,
                "language_id": 3,
                "name": "Kirguistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ki",
        "country_id": 173,
        "order": 100,
        "name": "Quiribati",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 173,
                "language_id": 1,
                "name": "Quiribati"
            },
            {
                "country_id": 173,
                "language_id": 2,
                "name": "Kiribati"
            },
            {
                "country_id": 173,
                "language_id": 3,
                "name": "Kiribati"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cf",
        "country_id": 175,
        "order": 100,
        "name": "Rep. Centro Africana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 175,
                "language_id": 1,
                "name": "República Centro-Africana"
            },
            {
                "country_id": 175,
                "language_id": 2,
                "name": "Central African Republic"
            },
            {
                "country_id": 175,
                "language_id": 3,
                "name": "República Centroafricana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cz",
        "country_id": 176,
        "order": 100,
        "name": "República Checa",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 176,
                "language_id": 1,
                "name": "República Checa"
            },
            {
                "country_id": 176,
                "language_id": 2,
                "name": "Czech Republic"
            },
            {
                "country_id": 176,
                "language_id": 3,
                "name": "República Checa"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cd",
        "country_id": 177,
        "order": 100,
        "name": "Rep. Dem. Congo",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 177,
                "language_id": 1,
                "name": "República Dem. do Congo"
            },
            {
                "country_id": 177,
                "language_id": 2,
                "name": "Dem. Republic of the Congo"
            },
            {
                "country_id": 177,
                "language_id": 3,
                "name": "República Dem. del Congo"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "do",
        "country_id": 178,
        "order": 100,
        "name": "Rep.Dominicana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 178,
                "language_id": 1,
                "name": "República Dominicana"
            },
            {
                "country_id": 178,
                "language_id": 2,
                "name": "Dominican Republic"
            },
            {
                "country_id": 178,
                "language_id": 3,
                "name": "República Dominicana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "re",
        "country_id": 179,
        "order": 100,
        "name": "Reunião",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 179,
                "language_id": 1,
                "name": "Reunião"
            },
            {
                "country_id": 179,
                "language_id": 2,
                "name": "Réunion"
            },
            {
                "country_id": 179,
                "language_id": 3,
                "name": "Islas Reunión"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ro",
        "country_id": 180,
        "order": 100,
        "name": "Roménia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 180,
                "language_id": 1,
                "name": "Roménia"
            },
            {
                "country_id": 180,
                "language_id": 2,
                "name": "Romania"
            },
            {
                "country_id": 180,
                "language_id": 3,
                "name": "Rumania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "rw",
        "country_id": 181,
        "order": 100,
        "name": "Ruanda",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 181,
                "language_id": 1,
                "name": "Ruanda"
            },
            {
                "country_id": 181,
                "language_id": 2,
                "name": "Rwanda"
            },
            {
                "country_id": 181,
                "language_id": 3,
                "name": "Ruanda"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ru",
        "country_id": 182,
        "order": 100,
        "name": "Rússia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 182,
                "language_id": 1,
                "name": "Rússia"
            },
            {
                "country_id": 182,
                "language_id": 2,
                "name": "Russia"
            },
            {
                "country_id": 182,
                "language_id": 3,
                "name": "Rusia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sh",
        "country_id": 183,
        "order": 100,
        "name": "S. Helena",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 183,
                "language_id": 1,
                "name": "S. Helena"
            },
            {
                "country_id": 183,
                "language_id": 2,
                "name": "S. Helena"
            },
            {
                "country_id": 183,
                "language_id": 3,
                "name": "S. Helena"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sm",
        "country_id": 184,
        "order": 100,
        "name": "S. Marino",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 184,
                "language_id": 1,
                "name": "S. Marino"
            },
            {
                "country_id": 184,
                "language_id": 2,
                "name": "S. Marino"
            },
            {
                "country_id": 184,
                "language_id": 3,
                "name": "S. Marino"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "vc",
        "country_id": 186,
        "order": 100,
        "name": "S. vicente e Grenadines",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 186,
                "language_id": 1,
                "name": "S. Vicente e Grenadines"
            },
            {
                "country_id": 186,
                "language_id": 2,
                "name": "S. Vincent and the Grenadines"
            },
            {
                "country_id": 186,
                "language_id": 3,
                "name": "S. Vicente y las Granadinas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "as",
        "country_id": 189,
        "order": 100,
        "name": "Samoa Americana",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 189,
                "language_id": 1,
                "name": "Samoa Americana"
            },
            {
                "country_id": 189,
                "language_id": 2,
                "name": "American Samoa"
            },
            {
                "country_id": 189,
                "language_id": 3,
                "name": "Samoa Americana"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ws",
        "country_id": 190,
        "order": 100,
        "name": "Samoa Ocidental",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 190,
                "language_id": 1,
                "name": "Samoa Ocidental"
            },
            {
                "country_id": 190,
                "language_id": 2,
                "name": "Western Samoa"
            },
            {
                "country_id": 190,
                "language_id": 3,
                "name": "Samoa Occidental"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sc",
        "country_id": 191,
        "order": 100,
        "name": "Seychelles",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 191,
                "language_id": 1,
                "name": "Seychelles"
            },
            {
                "country_id": 191,
                "language_id": 2,
                "name": "Seychelles"
            },
            {
                "country_id": 191,
                "language_id": 3,
                "name": "Seychelles"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sn",
        "country_id": 192,
        "order": 100,
        "name": "Senegal",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 192,
                "language_id": 1,
                "name": "Senegal"
            },
            {
                "country_id": 192,
                "language_id": 2,
                "name": "Senegal"
            },
            {
                "country_id": 192,
                "language_id": 3,
                "name": "Senegal"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sl",
        "country_id": 193,
        "order": 100,
        "name": "Serra Leoa",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 193,
                "language_id": 1,
                "name": "Serra Leoa"
            },
            {
                "country_id": 193,
                "language_id": 2,
                "name": "Sierra Leone"
            },
            {
                "country_id": 193,
                "language_id": 3,
                "name": "Sierra Leona"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "me",
        "country_id": 194,
        "order": 100,
        "name": "Montenegro",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 194,
                "language_id": 1,
                "name": "Montenegro"
            },
            {
                "country_id": 194,
                "language_id": 2,
                "name": "Montenegro"
            },
            {
                "country_id": 194,
                "language_id": 3,
                "name": "Montenegro"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "rs",
        "country_id": 195,
        "order": 100,
        "name": "Servia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 195,
                "language_id": 1,
                "name": "Servia"
            },
            {
                "country_id": 195,
                "language_id": 2,
                "name": "Servia"
            },
            {
                "country_id": 195,
                "language_id": 3,
                "name": "Servia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sg",
        "country_id": 196,
        "order": 100,
        "name": "Singapura",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 196,
                "language_id": 1,
                "name": "Singapura"
            },
            {
                "country_id": 196,
                "language_id": 2,
                "name": "Singapore"
            },
            {
                "country_id": 196,
                "language_id": 3,
                "name": "Singapur"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sy",
        "country_id": 197,
        "order": 100,
        "name": "Síria",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 197,
                "language_id": 1,
                "name": "Síria"
            },
            {
                "country_id": 197,
                "language_id": 2,
                "name": "Syria"
            },
            {
                "country_id": 197,
                "language_id": 3,
                "name": "Siria"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "so",
        "country_id": 198,
        "order": 100,
        "name": "Somália",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 198,
                "language_id": 1,
                "name": "Somália"
            },
            {
                "country_id": 198,
                "language_id": 2,
                "name": "Somalia"
            },
            {
                "country_id": 198,
                "language_id": 3,
                "name": "Somalia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lk",
        "country_id": 199,
        "order": 100,
        "name": "Sri Lanka",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 199,
                "language_id": 1,
                "name": "Sri Lanka"
            },
            {
                "country_id": 199,
                "language_id": 2,
                "name": "Sri Lanka"
            },
            {
                "country_id": 199,
                "language_id": 3,
                "name": "Sri Lanka"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sz",
        "country_id": 200,
        "order": 100,
        "name": "Suazilândia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 200,
                "language_id": 1,
                "name": "Suazilândia"
            },
            {
                "country_id": 200,
                "language_id": 2,
                "name": "Swaziland"
            },
            {
                "country_id": 200,
                "language_id": 3,
                "name": "Swazilandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sd",
        "country_id": 201,
        "order": 100,
        "name": "Sudão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 201,
                "language_id": 1,
                "name": "Sudão"
            },
            {
                "country_id": 201,
                "language_id": 2,
                "name": "Sudan"
            },
            {
                "country_id": 201,
                "language_id": 3,
                "name": "Sudán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "se",
        "country_id": 202,
        "order": 100,
        "name": "Suécia",
        "image": "",
        "vies_vat_check_available": 1,
        "languages": [
            {
                "country_id": 202,
                "language_id": 1,
                "name": "Suécia"
            },
            {
                "country_id": 202,
                "language_id": 2,
                "name": "Sweden"
            },
            {
                "country_id": 202,
                "language_id": 3,
                "name": "Suecia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ch",
        "country_id": 203,
        "order": 100,
        "name": "Suíça",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 203,
                "language_id": 1,
                "name": "Suíça"
            },
            {
                "country_id": 203,
                "language_id": 2,
                "name": "Switzerland"
            },
            {
                "country_id": 203,
                "language_id": 3,
                "name": "Suiza"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sr",
        "country_id": 204,
        "order": 100,
        "name": "Suriname",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 204,
                "language_id": 1,
                "name": "Suriname"
            },
            {
                "country_id": 204,
                "language_id": 2,
                "name": "Suriname"
            },
            {
                "country_id": 204,
                "language_id": 3,
                "name": "Suriname"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "th",
        "country_id": 205,
        "order": 100,
        "name": "Tailândia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 205,
                "language_id": 1,
                "name": "Tailândia"
            },
            {
                "country_id": 205,
                "language_id": 2,
                "name": "Thailand"
            },
            {
                "country_id": 205,
                "language_id": 3,
                "name": "Tailandia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tj",
        "country_id": 206,
        "order": 100,
        "name": "Tajiquistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 206,
                "language_id": 1,
                "name": "Tajiquistão"
            },
            {
                "country_id": 206,
                "language_id": 2,
                "name": "Tajikistan"
            },
            {
                "country_id": 206,
                "language_id": 3,
                "name": "Tayikistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tz",
        "country_id": 207,
        "order": 100,
        "name": "Tanzânia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 207,
                "language_id": 1,
                "name": "Tanzânia"
            },
            {
                "country_id": 207,
                "language_id": 2,
                "name": "Tanzania"
            },
            {
                "country_id": 207,
                "language_id": 3,
                "name": "Tanzania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tl",
        "country_id": 208,
        "order": 100,
        "name": "Timor Lorosae",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 208,
                "language_id": 1,
                "name": "Timor Lorosae"
            },
            {
                "country_id": 208,
                "language_id": 2,
                "name": "East Timor"
            },
            {
                "country_id": 208,
                "language_id": 3,
                "name": "Timor del Este"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tt",
        "country_id": 209,
        "order": 100,
        "name": "Trinidade e Tobago",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 209,
                "language_id": 1,
                "name": "Trinidade e Tobago"
            },
            {
                "country_id": 209,
                "language_id": 2,
                "name": "Trinidad and Tobago"
            },
            {
                "country_id": 209,
                "language_id": 3,
                "name": "Trinidad y Tobago"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tg",
        "country_id": 210,
        "order": 100,
        "name": "Togo",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 210,
                "language_id": 1,
                "name": "Togo"
            },
            {
                "country_id": 210,
                "language_id": 2,
                "name": "Togo"
            },
            {
                "country_id": 210,
                "language_id": 3,
                "name": "Togo"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tk",
        "country_id": 211,
        "order": 100,
        "name": "Tokelau",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 211,
                "language_id": 1,
                "name": "Tokelau"
            },
            {
                "country_id": 211,
                "language_id": 2,
                "name": "Tokelau"
            },
            {
                "country_id": 211,
                "language_id": 3,
                "name": "Tokelau"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "to",
        "country_id": 212,
        "order": 100,
        "name": "Tonga",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 212,
                "language_id": 1,
                "name": "Tonga"
            },
            {
                "country_id": 212,
                "language_id": 2,
                "name": "Tonga"
            },
            {
                "country_id": 212,
                "language_id": 3,
                "name": "Tonga"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "sh",
        "country_id": 213,
        "order": 100,
        "name": "Tristão da Cunha",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 213,
                "language_id": 1,
                "name": "Tristão da Cunha"
            },
            {
                "country_id": 213,
                "language_id": 2,
                "name": "Tristan da Cunha"
            },
            {
                "country_id": 213,
                "language_id": 3,
                "name": "Tristan da Cunha"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tn",
        "country_id": 214,
        "order": 100,
        "name": "Tunísia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 214,
                "language_id": 1,
                "name": "Tunísia"
            },
            {
                "country_id": 214,
                "language_id": 2,
                "name": "Tunisia"
            },
            {
                "country_id": 214,
                "language_id": 3,
                "name": "Túnez"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tc",
        "country_id": 215,
        "order": 100,
        "name": "Turks e Caicos",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 215,
                "language_id": 1,
                "name": "Ilhas Turcas e Caicos"
            },
            {
                "country_id": 215,
                "language_id": 2,
                "name": "Turks and Caicos"
            },
            {
                "country_id": 215,
                "language_id": 3,
                "name": "Turcas y Caicos"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tm",
        "country_id": 216,
        "order": 100,
        "name": "Turquemenistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 216,
                "language_id": 1,
                "name": "Turquemenistão"
            },
            {
                "country_id": 216,
                "language_id": 2,
                "name": "Turkmenistan"
            },
            {
                "country_id": 216,
                "language_id": 3,
                "name": "Turkmenistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tr",
        "country_id": 217,
        "order": 100,
        "name": "Turquia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 217,
                "language_id": 1,
                "name": "Turquia"
            },
            {
                "country_id": 217,
                "language_id": 2,
                "name": "Turkey"
            },
            {
                "country_id": 217,
                "language_id": 3,
                "name": "Turquía"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "tv",
        "country_id": 218,
        "order": 100,
        "name": "Tuvalu",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 218,
                "language_id": 1,
                "name": "Tuvalu"
            },
            {
                "country_id": 218,
                "language_id": 2,
                "name": "Tuvalu"
            },
            {
                "country_id": 218,
                "language_id": 3,
                "name": "Tuvalu"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ua",
        "country_id": 219,
        "order": 100,
        "name": "Ucrânia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 219,
                "language_id": 1,
                "name": "Ucrânia"
            },
            {
                "country_id": 219,
                "language_id": 2,
                "name": "Ukraine"
            },
            {
                "country_id": 219,
                "language_id": 3,
                "name": "Ucrania"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ug",
        "country_id": 220,
        "order": 100,
        "name": "Uganda",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 220,
                "language_id": 1,
                "name": "Uganda"
            },
            {
                "country_id": 220,
                "language_id": 2,
                "name": "Uganda"
            },
            {
                "country_id": 220,
                "language_id": 3,
                "name": "Uganda"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "uy",
        "country_id": 221,
        "order": 100,
        "name": "Uruguai",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 221,
                "language_id": 1,
                "name": "Uruguai"
            },
            {
                "country_id": 221,
                "language_id": 2,
                "name": "Uruguay"
            },
            {
                "country_id": 221,
                "language_id": 3,
                "name": "Uruguay"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "uz",
        "country_id": 222,
        "order": 100,
        "name": "Uzbequistão",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 222,
                "language_id": 1,
                "name": "Uzbequistão"
            },
            {
                "country_id": 222,
                "language_id": 2,
                "name": "Uzbekistan"
            },
            {
                "country_id": 222,
                "language_id": 3,
                "name": "Uzbekistán"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "vu",
        "country_id": 223,
        "order": 100,
        "name": "Vanuatu",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 223,
                "language_id": 1,
                "name": "Vanuatu"
            },
            {
                "country_id": 223,
                "language_id": 2,
                "name": "Vanuatu"
            },
            {
                "country_id": 223,
                "language_id": 3,
                "name": "Vanuatu"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "va",
        "country_id": 224,
        "order": 100,
        "name": "Vaticano",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 224,
                "language_id": 1,
                "name": "Vaticano"
            },
            {
                "country_id": 224,
                "language_id": 2,
                "name": "Vatican"
            },
            {
                "country_id": 224,
                "language_id": 3,
                "name": "Vaticano"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ve",
        "country_id": 225,
        "order": 100,
        "name": "Venezuela",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 225,
                "language_id": 1,
                "name": "Venezuela"
            },
            {
                "country_id": 225,
                "language_id": 2,
                "name": "Venezuela"
            },
            {
                "country_id": 225,
                "language_id": 3,
                "name": "Venezuela"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "vn",
        "country_id": 226,
        "order": 100,
        "name": "Vietname",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 226,
                "language_id": 1,
                "name": "Vietname"
            },
            {
                "country_id": 226,
                "language_id": 2,
                "name": "Vietnam"
            },
            {
                "country_id": 226,
                "language_id": 3,
                "name": "Vietnam"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "vi",
        "country_id": 227,
        "order": 100,
        "name": "Ilhas Virgens Americanas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 227,
                "language_id": 1,
                "name": "Ilhas Virgens Americanas"
            },
            {
                "country_id": 227,
                "language_id": 2,
                "name": "U.S. Virgin Islands"
            },
            {
                "country_id": 227,
                "language_id": 3,
                "name": "Islas Vírgenes de los EE.UU."
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "wf",
        "country_id": 229,
        "order": 100,
        "name": "Wallis e Futuna",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 229,
                "language_id": 1,
                "name": "Wallis e Futuna"
            },
            {
                "country_id": 229,
                "language_id": 2,
                "name": "Wallis and Futuna"
            },
            {
                "country_id": 229,
                "language_id": 3,
                "name": "Wallis y Futuna"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "zm",
        "country_id": 230,
        "order": 100,
        "name": "Zâmbia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 230,
                "language_id": 1,
                "name": "Zâmbia"
            },
            {
                "country_id": 230,
                "language_id": 2,
                "name": "Zambia"
            },
            {
                "country_id": 230,
                "language_id": 3,
                "name": "Zambia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "zw",
        "country_id": 231,
        "order": 100,
        "name": "Zimbabwe",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 231,
                "language_id": 1,
                "name": "Zimbabwe"
            },
            {
                "country_id": 231,
                "language_id": 2,
                "name": "Zimbabwe"
            },
            {
                "country_id": 231,
                "language_id": 3,
                "name": "Zimbabue"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "vg",
        "country_id": 232,
        "order": 100,
        "name": "Ilhas Virgens Britânicas",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 232,
                "language_id": 1,
                "name": "Ilhas Virgens Britânicas"
            },
            {
                "country_id": 232,
                "language_id": 2,
                "name": "British Virgin Islands"
            },
            {
                "country_id": 232,
                "language_id": 3,
                "name": "Islas Vírgenes Británicas"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "xk",
        "country_id": 233,
        "order": 100,
        "name": "Kosovo",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 233,
                "language_id": 1,
                "name": "Kosovo"
            },
            {
                "country_id": 233,
                "language_id": 2,
                "name": "Kosovo"
            },
            {
                "country_id": 233,
                "language_id": 3,
                "name": "Kosovo"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "ps",
        "country_id": 234,
        "order": 100,
        "name": "Palestina",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 234,
                "language_id": 1,
                "name": "Palestina"
            },
            {
                "country_id": 234,
                "language_id": 2,
                "name": "Palestine"
            },
            {
                "country_id": 234,
                "language_id": 3,
                "name": "Palestina"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "je",
        "country_id": 236,
        "order": 100,
        "name": "Jersey",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 236,
                "language_id": 1,
                "name": "Jersey"
            },
            {
                "country_id": 236,
                "language_id": 2,
                "name": "Jersey"
            },
            {
                "country_id": 236,
                "language_id": 3,
                "name": "Jersey"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "bl",
        "country_id": 237,
        "order": 100,
        "name": "São Bartolomeu",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 237,
                "language_id": 1,
                "name": "São Bartolomeu"
            },
            {
                "country_id": 237,
                "language_id": 2,
                "name": "Saint Barthelemy"
            },
            {
                "country_id": 237,
                "language_id": 3,
                "name": "Saint Barthelemy"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "im",
        "country_id": 238,
        "order": 100,
        "name": "Ilha de Man",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 238,
                "language_id": 1,
                "name": "Ilha de Man"
            },
            {
                "country_id": 238,
                "language_id": 2,
                "name": "Isle of Man"
            },
            {
                "country_id": 238,
                "language_id": 3,
                "name": "Isla de Man"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "cw",
        "country_id": 239,
        "order": 100,
        "name": "Curaçao",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 239,
                "language_id": 1,
                "name": "Curaçao"
            },
            {
                "country_id": 239,
                "language_id": 2,
                "name": "Curaçao"
            },
            {
                "country_id": 239,
                "language_id": 3,
                "name": "Curaçao"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "gg",
        "country_id": 240,
        "order": 100,
        "name": "Guernsey",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 240,
                "language_id": 1,
                "name": "Guernsey"
            },
            {
                "country_id": 240,
                "language_id": 2,
                "name": "Guernsey"
            },
            {
                "country_id": 240,
                "language_id": 3,
                "name": "Guernsey"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kn",
        "country_id": 241,
        "order": 100,
        "name": "Federação de São Cristóvão e Nevis",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 241,
                "language_id": 1,
                "name": "Federação de São Cristóvão e Nevis"
            },
            {
                "country_id": 241,
                "language_id": 2,
                "name": "Federation of Saint Kitts and Nevis"
            },
            {
                "country_id": 241,
                "language_id": 3,
                "name": "Federación de San Cristóbal y Nieves"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "lc",
        "country_id": 251,
        "order": 100,
        "name": "Santa Lucia",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 251,
                "language_id": 1,
                "name": "Santa Lucia"
            },
            {
                "country_id": 251,
                "language_id": 2,
                "name": "Saint Lucia"
            },
            {
                "country_id": 251,
                "language_id": 3,
                "name": "Santa Lucia"
            }
        ],
        "fiscal_zones": []
    },
    {
        "iso_3166_1": "kn",
        "country_id": 252,
        "order": 100,
        "name": "São Cristóvão e Neves",
        "image": "",
        "vies_vat_check_available": 0,
        "languages": [
            {
                "country_id": 252,
                "language_id": 1,
                "name": "São Cristóvão e Neves"
            },
            {
                "country_id": 252,
                "language_id": 2,
                "name": "Saint Kitts and Nevis"
            },
            {
                "country_id": 252,
                "language_id": 3,
                "name": "San Cristóbal y Nieves"
            }
        ],
        "fiscal_zones": []
    }
]