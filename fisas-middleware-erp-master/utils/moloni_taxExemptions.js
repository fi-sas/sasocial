[
    {
        "code": "M01",
        "name": "Artigo 16.º n.º 6 do CIVA (Ou similar)",
        "description": "Artigo 16.º n.º 6 alínea a) a d) do CIVA"
    },
    {
        "code": "M02",
        "name": "Artigo 6.º do Decreto-Lei n.º 198/90, de 19 de Junho",
        "description": "Artigo 6.º do Decreto-Lei n.º 198/90, de 19 de junho"
    },
    {
        "code": "M03",
        "name": "Exigibilidade de caixa",
        "description": "Decreto-Lei n.º 204/97, de 9 de agosto\nDecreto-Lei n.º 418/99, de 21 de outubro\nLei n.º 15/2009, de 1 de abril"
    },
    {
        "code": "M04",
        "name": "Isento Artigo 13.º do CIVA (Ou similar)",
        "description": "Artigo 13.º do CIVA"
    },
    {
        "code": "M05",
        "name": "Isento Artigo 14.º do CIVA (Ou similar)",
        "description": "Artigo 14.º do CIVA"
    },
    {
        "code": "M06",
        "name": "Isento Artigo 15.º do CIVA (Ou similar)",
        "description": "Artigo 15.º do CIVA"
    },
    {
        "code": "M07",
        "name": "Isento Artigo 9.º do CIVA (Ou similar)",
        "description": "Artigo 9.º do CIVA"
    },
    {
        "code": "M08",
        "name": "IVA - Autoliquidação",
        "description": "Artigo 2.º n.º 1 alínea i), j) ou l) do CIVA\nArtigo 6.º do CIVA\nDecreto-Lei n.º 21/2007, de 29 de janeiro\nDecreto-Lei n.º 362/99, de 16 de setembro"
    },
    {
        "code": "M09",
        "name": "IVA - não confere direito a dedução",
        "description": "Artigo 60.º CIVA\nArtigo 72.º n.º 4 do CIVA"
    },
    {
        "code": "M10",
        "name": "IVA - Regime de isenção",
        "description": "Artigo 53.ºdo CIVA"
    },
    {
        "code": "M11",
        "name": "Regime particular do tabaco",
        "description": "Decreto-Lei n.º 346/85, de 23 de agosto"
    },
    {
        "code": "M12",
        "name": "Regime da margem de lucro - Agências de viagens",
        "description": "Decreto-Lei n.º 221/85, de 3 de julho"
    },
    {
        "code": "M13",
        "name": "Regime da margem de lucro - Bens em segunda mão",
        "description": "Decreto-Lei n.º 199/96, de 18 de outubro"
    },
    {
        "code": "M14",
        "name": "Regime da margem de lucro - Objetos de arte",
        "description": "Decreto-Lei n.º 199/96, de 18 de outubro"
    },
    {
        "code": "M15",
        "name": "Regime da margem de lucro - Objetos de coleção e antiguidades",
        "description": "Decreto-Lei n.º 199/96, de 18 de outubro"
    },
    {
        "code": "M16",
        "name": "Isento Artigo 14.º do RITI (Ou similar)",
        "description": "Artigo 14.º do RITI"
    },
    {
        "code": "M20",
        "name": "IVA - Regime forfetário",
        "description": "Artigo 59.º-B do CIVA"
    },
    {
        "code": "M99",
        "name": "Não sujeito; não tributado (Ou similar)",
        "description": "Outras situações de não liquidação do imposto\r\n(Exemplos: artigo 2.º, n.º 2; artigo 3.º, n.ºs 4, 6 e 7; artigo 4.º, n.º 5, todos do CIVA)"
    },
    {
        "code": "M99-1",
        "name": "Lei n.º 13/2020 de 7 de Maio 2020",
        "description": "Artigo 2.º, n.º 2 da Lei n.º 13/2020 de 7 de Maio 2020"
    }
]