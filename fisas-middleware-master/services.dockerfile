FROM timbru31/java-node:11-jdk-erbium

ARG NODE_ENV

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm i

COPY . .

RUN chmod +x ./docker-entrypoint.sh

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]

CMD npm run start:$NODE_ENV
