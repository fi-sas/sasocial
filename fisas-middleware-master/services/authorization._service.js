"use strict";
const Errors = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "middleware.authorization",

	mixins: [],

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Hooks
	 */
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * validate
		 *
		 * After create the user on the SASocial plat. must return `true`
		 *  if the user dont exist or if is not possoble to create you must return `false`
		 */
		validate: {
			visibility: "public",
			params: {
				email: { type: "email" },
				password: { type: "string" },
			},
			handler(ctx) {
				// EXAMPLE!!
				return ctx.params.password === "myPassword";
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		findActiveUser(ctx, prop, value) {
			const params = {
				withRelated: false,
				query: {
					active: true,
				},
			};
			params["query"][prop] = value;

			// CEHCK RFID_VALIDITY
			if (prop === "rfid") {
				//params["query"]["rfid_validity"] = value; TODO
			}

			return ctx.call("authorization.users.find", params).then((res) => {
				if (Array.isArray(res) & (res.length > 0)) {
					return res[0];
				} else {
					throw new Errors.UnauthorizedError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
				}
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
