"use strict";
const Errors = require("@fisas/ms_core").Helpers.Errors;
const { MoleculerError } = require("moleculer").Errors;
const samlify = require("samlify");
const validator = require("@authenio/samlify-xsd-schema-validator");
const fs = require("fs");
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "middleware.sso",

	mixins: [],

	/**
	 * Settings
	 */
	settings: {
		$secureSettings: ["spPrivateKey"],

		domain: process.env.DOMAIN,
		spSigningCert: process.env.SIGNING_CERT,
		spPrivateKey: process.env.SIGNING_CERT_KEY,
	},

	/**
	 * Hooks
	 */
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		metadata: {
			visibility: "published",
			handler(ctx) {
				if (this.sp) {
					ctx.meta.$responseType = "application/xml";
					return this.sp.getMetadata();
				} else {
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");
				}
			},
		},
		url: {
			visibility: "published",
			params: {
				device: { type: "enum", values: ["WEB", "BO"], default: "WEB", optional: true },
			},
			handler(ctx) {
				const sp = this.createSP(ctx.params.device);
				if (sp) {
					return sp.createLoginRequest(this.idp, samlify.Constants.wording.binding.redirect);
				} else {
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");
				}
			},
		},
		acknowledge: {
			visibility: "published",
			handler(ctx) {
				const sp = this.createSP();
				if (sp) {
					return sp
						.parseLoginResponse(this.idp, samlify.Constants.wording.binding.post, {
							query: {},
							body: ctx.params,
							octetString: null,
						})
						.then(async (parseResult) => {
							const user = await this.findOrCreateUser(ctx, parseResult);

							return {
								device_type: ctx.params.RelayState,
								user,
							};
						})
						.catch((er) => {
							throw er;
						});
				} else {
					throw new Errors.ValidationError("The SSO Login is not active", "SSO_INACTIVE");
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		createIDP() {
			try {
				if (fs.existsSync("metadatas/idp_metadata.xml")) {
					return samlify.IdentityProvider({
						metadata: fs.readFileSync("metadatas/idp_metadata.xml"),
						isAssertionEncrypted: true,
						messageSigningOrder: "encrypt-then-sign",
					});
				}
				throw new MoleculerError("IDP Metadata not found", 500, "SSO_IDP_METADATA_NOT_FOUND");
			} catch (err) {
				this.logger.error("Error loading IDP Metadata");
				this.logger.error(err);
				throw err;
			}
		},
		createSP(device = "WEB") {
			try {
				if (this.settings.domain && this.settings.spSigningCert && this.settings.spPrivateKey) {
					return samlify.ServiceProvider({
						entityID: `https://${this.settings.domain}`,
						wantAssertionsSigned: true,
						signingCert: this.settings.spSigningCert,
						privateKey: this.settings.spPrivateKey,
						encryptCert: this.settings.spSigningCert,
						encPrivateKey: this.settings.spPrivateKey,
						nameIDFormat: [samlify.Constants.namespace.format.persistent],
						assertionConsumerService: [
							{
								isDefault: true,
								Binding: samlify.Constants.BindingNamespace.Post,
								Location: `https://${this.settings.domain}/api/authorization/authorize/sso/acs`,
							},
						],
						elementsOrder: samlify.Constants.elementsOrder.shibboleth,
						clockDrifts: [-60000, 60000],
						relayState: device,
					});
				}
				throw new MoleculerError("SP Metadata not found", 500, "SSO_SP_METADATA_NOT_FOUND");
			} catch (err) {
				this.logger.error("Error loading SP Metadata");
				this.logger.error(err);
				throw err;
			}
		},

		randomPin() {
			return Math.floor(1000 + Math.random() * 9000).toString();
		},
		randomPassword() {
			return Math.random().toString(36).slice(-8);
		},
		getObjectFields(object, fields) {
			let newObject = {};
			fields.forEach((key) => (newObject[key] = object[key]));
			return newObject;
		},

		async getSASocialProfileId(ctx, ssoProfile) {
			let profile = await ctx.call("authorization.profiles.find", {
				query: { external_ref: ssoProfile },
			});
			return profile.length !== 0 ? _.first(profile).id : 1;
		},
		async createUserInSASocial(ctx, user) {
			const newUser = await ctx.call("authorization.users.create", user);
			return _.first(newUser);
		},
		async updateUserInSASocial(ctx, user) {
			const updatedUser = await ctx.call("authorization.users.patch", user);
			return _.first(updatedUser);
		},
		findActiveUserInSASocialByEmail(ctx, email) {
			const params = {
				withRelated: false,
				query: {
					active: true,
					email,
				},
			};
			return ctx.call("authorization.users.find", params).then((res) => {
				if (Array.isArray(res) & (res.length > 0)) {
					return res[0];
				} else {
					return null;
				}
			});
		},

		async findOrCreateUser(ctx, parseResult) {
			/**
			 * WHERE YOU CREATE THE USER AND MAKE THE VALIDATIONS :)
			 * THIS IS JUST AN EXAMPLE YOU MIGHT HAVE TO CUSTOMIZE IT TO YOUR CONTEXT
			 */

			const attributes = {
				commonName: _.get(parseResult.extract.attributes, "urn:oid:2.5.4.3"),
				displayName: _.get(parseResult.extract.attributes, "urn:oid:2.16.840.1.113730.3.1.241"),
				email: _.get(parseResult.extract.attributes, "urn:oid:0.9.2342.19200300.100.1.3"),
				eduPersonPrimaryAffiliation: _.get(
					parseResult.extract.attributes,
					"urn:oid:1.3.6.1.4.1.5923.1.1.1.5",
					"external",
				),
			};

			const username = attributes.email.substring(0, attributes.email.indexOf("@"));
			const profileId = await this.getSASocialProfileId(
				ctx,
				attributes.eduPersonPrimaryAffiliation,
			);

			let userSASocial = await this.findActiveUserInSASocialByEmail(ctx, attributes.email);

			if (!userSASocial) {
				// Does not exist, create one
				const newUser = {
					name: attributes.commonName,
					email: attributes.email,
					user_name: username,
					external: false,
					pin: this.randomPin(),
					password: this.randomPassword(),
					can_access_BO: profileId === 6, // Worker
					active: true,
					birth_date: "1970-01-01 00:00:00",
					gender: "U",
					profile_id: profileId,
				};

				// If Student
				if (profileId === 1) {
					newUser.student_number = username;
				}

				// TODO Define some permissions here?
				//newUser.scope_ids = [];
				//newUser.user_group_ids = [];

				userSASocial = await this.createUserInSASocial(ctx, newUser);
			} else {
				// Exists, update fields
				const fieldsToUpdate = ["name"];

				const userSSO = {
					name: attributes.commonName,
				};

				const userSASocialFields = this.getObjectFields(userSASocial, fieldsToUpdate);
				const userSSOFields = this.getObjectFields(userSSO, fieldsToUpdate);

				if (!_.isEqual(userSASocialFields, userSSOFields)) {
					userSSOFields.id = userSASocial.id;
					userSASocial = await this.updateUserInSASocial(ctx, userSSOFields);
				}
			}

			return userSASocial;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		samlify.setSchemaValidator(validator);

		this.sp = null;
		this.idp = null;

		// Configure IDP
		try {
			this.idp = this.createIDP();
		} catch (err) {
			this.logger.error("Error Testing Initial Settings for IDP");
			this.logger.error(err);
		}

		// Configure SP
		try {
			this.sp = this.createSP();
		} catch (err) {
			this.logger.error("Error Testing Initial Settings for SP");
			this.logger.error(err);
		}
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
