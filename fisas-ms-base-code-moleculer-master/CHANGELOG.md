# [0.17.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.16.0...v0.17.0) (2021-10-08)


### Features

* **knex:** add shared pool connection ([0929a24](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/0929a24a9ee144cd60ee80c646516b68319b85de))

# [0.16.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.15.4...v0.16.0) (2021-10-06)


### Features

* **knex:** add extraQuery option ([8661914](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8661914f0382fc2ed22e9e149c575c913f3d5537))


### Reverts

* Revert "feat(knex): add query by innerjoin" ([8dd9698](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8dd96988f8f672c97b114ddfef808a9c03479104))
* Revert "style: fix lint pipeline" ([dac302c](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/dac302c0b3def1895d1c65d32ef3e1579163fddf))
* Revert "chore(release): version [skip ci]0.15.0" ([cdfbeec](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/cdfbeec8ec877f11c7cc4647bb8f2edd28a5432c))
* Revert "fix(knex): shared connection lost pool on hot reload" ([8236163](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8236163fb56e2cbbed9d708c81596d8eefb9c947))
* Revert "chore(release): version [skip ci]0.15.1" ([1569538](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1569538e5dd6c2c315774ae4e3ba45f548de0d03))
* Revert "fix: fieldName error knex create cursor" ([979a1eb](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/979a1eb03d72577f7988a7d702b44b063fc200f3))
* Revert "chore(release): version [skip ci]0.15.2" ([3937adc](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/3937adc556c185a7460ee105646025c23ef59763))
* Revert "fix(knex): on date rearrange query" ([af288a7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/af288a7a1e1b98ed130bc1458649846fa108753f))
* Revert "chore(release): version [skip ci]0.15.3" ([ed9d103](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/ed9d103c3ed1d5699d88ec66f801f3883460c28c))
* Revert "chore(release): version [skip ci]0.15.4" ([d676891](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/d67689100e1af6f1004a4e686459c57c694211f8))

## [0.14.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.14.1...v0.14.2) (2021-06-22)


### Bug Fixes

* **db.mixin:**  add query by date time ([d4c278b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/d4c278b44756a54710746eb400e7c0dcecf3d8e7))

## [0.14.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.14.0...v0.14.1) (2021-05-28)


### Bug Fixes

* **helpers:** isseus with fields param on relateds ([52c34aa](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/52c34aacaec3939f3213ece73f6602b7897447de))

# [0.14.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.13.1...v0.14.0) (2021-05-28)


### Features

* **helpers:** add new options to hasOne hasMany tool ([c33f431](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/c33f431e6a439635631c306297df0d4c8ed81263))

## [0.13.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.13.0...v0.13.1) (2021-04-21)


### Bug Fixes

* **knex:** add bindings to raw method ([2eefc23](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/2eefc237bacd6b68335bc250c706ed87c8623501))

# [0.13.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.12.4...v0.13.0) (2021-03-31)


### Features

* **knex:** add gt, gte, lt ,lte operations ([849ea95](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/849ea95dddf9a27835af9eafaffb40ce82dec8a3))

## [0.12.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.12.3...v0.12.4) (2021-03-29)


### Bug Fixes

* **gitlab-ci:** fixed prerelease version ([62b98cd](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/62b98cda35454c020668d084fedf3e1419f42e3d))

## [0.12.3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.12.2...v0.12.3) (2021-03-29)


### Bug Fixes

* **gitlab-ci:** removed manual jon on master ([ed48e61](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/ed48e6100b76970d8d8e8e841f0b65f4d516f248))

## [0.12.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.12.1...v0.12.2) (2021-03-29)


### Bug Fixes

* **ci:** changed versioning strategy for pre-releases ([292d555](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/292d555756c9897729699090f6221bb19b1cd29c))

## [0.12.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.12.0...v0.12.1) (2021-03-23)


### Bug Fixes

* **searchRelated:** add searchRelated to Helpers index ([0c34c74](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/0c34c7495faa149f5ea071af06d50eae96f12472))

# [0.12.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.11.0...v0.12.0) (2021-03-23)


### Bug Fixes

* **withRelated:** add performance fix ([5016473](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/5016473801809ce4ebd0692b47bc1545c42d33e1))


### Features

* **db.mixin:** clear cache on MS start ([2c6cdf9](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/2c6cdf9d551159e871b8ad8893ed3d8a5db5beb5))

# [0.11.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.10.0...v0.11.0) (2021-03-23)


### Features

* **searchRelated:** add new helper for search with related ([8750e78](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8750e78fd5c1db4e1783bf6fe3228e155b7b2dae))

# [0.10.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.9.0...v0.10.0) (2021-03-04)


### Features

* **knex:** add In to object param ([be861d1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/be861d17c1b487be425e3dd4a9fc74ea4bd26627))

# [0.9.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.8.2...v0.9.0) (2021-03-02)


### Features

* **knex:** add notEqual and NotIn ([4180732](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/418073258ae97d3adf77d157d9ecef382c068fa8))

## [0.8.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.8.1...v0.8.2) (2021-02-23)


### Bug Fixes

* **helpers:** fix withRelateds additional query's hasMany ([e1d6ae3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/e1d6ae309c535eec80c13d6a384dabe4aead5fcf))

## [0.8.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.8.0...v0.8.1) (2021-02-22)


### Bug Fixes

* **knex:**  search query ([82ab7fd](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/82ab7fdbc611e5691011cb859caf7fb4da83ebf3))

# [0.8.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.7.1...v0.8.0) (2021-02-22)


### Features

* **withRelateds:** add additional query to hasMany ([97377d5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/97377d5d66ce8a13e658df3aefef5a26dcf4ae71))

## [0.7.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.7.0...v0.7.1) (2021-02-08)


### Bug Fixes

* **semantic-release:** Changed configuration ([53650bc](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/53650bc0aa0bdcc6fc4b1e906da1df96dd0b4e4e))

# [0.7.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.6.0...v0.7.0) (2021-01-29)


### Features

* **knex:** add new query operators ([a185511](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/a185511bda6903f87a794bf6d8a52b924f463f85))

# [0.7.0-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.6.0...v0.7.0-rc.1) (2021-01-29)


### Features

* **knex:** add new query operators ([a185511](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/a185511bda6903f87a794bf6d8a52b924f463f85))

# [0.6.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.6...v0.6.0) (2021-01-26)


### Features

* add parser to PG ([db39d03](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/db39d03943a3b565e41c99853748bfe9bc6fd4b8))
* release new version ([3079a01](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/3079a016f3e68dece96cd849b180a0792b306413))

# [0.6.0-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.6...v0.6.0-rc.1) (2021-01-26)


### Features

* add parser to PG ([db39d03](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/db39d03943a3b565e41c99853748bfe9bc6fd4b8))

## [0.5.6](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.5...v0.5.6) (2021-01-21)


### Bug Fixes

* **dbMixin:** parse shorthand validator on patch ([c3eedc2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/c3eedc2e8b8f686306d8e815b7d200e41fa2a0b8))

## [0.5.6-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.5...v0.5.6-rc.1) (2021-01-21)


### Bug Fixes

* **dbMixin:** parse shorthand validator on patch ([c3eedc2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/c3eedc2e8b8f686306d8e815b7d200e41fa2a0b8))

## [0.5.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.4...v0.5.5) (2021-01-19)


### Bug Fixes

* **knex:** is date valid format ([d9336d4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/d9336d4ef01a254620e29ea01bac6c08a33e8a3f))

## [0.5.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3...v0.5.4) (2021-01-18)


### Bug Fixes

* **db.mixin:** count take searchFileds in query ([1e3e38e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1e3e38ea3fc0189c00731e0d6c0222e27acdd853))
* **knex:** add ::date conversion on search for data ([1d1dc3f](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1d1dc3ffc272c166cddcc96e439adf20a9e62be7))
* **knex:** fix date field convertion ([f35b2d7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f35b2d7adc7d763a85ac348ccf368074aeb8ff52))
* **knex:** if is date convert on postgres ([8aa28e7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8aa28e7d6c0712647017e9c55f78559bee4ffd48))
* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))

## [0.5.3-rc.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.4...v0.5.3-rc.5) (2021-01-18)


### Bug Fixes

* **knex:** fix date field convertion ([f35b2d7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f35b2d7adc7d763a85ac348ccf368074aeb8ff52))

## [0.5.3-rc.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.3...v0.5.3-rc.4) (2021-01-18)


### Bug Fixes

* **knex:** if is date convert on postgres ([8aa28e7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8aa28e7d6c0712647017e9c55f78559bee4ffd48))

## [0.5.3-rc.3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.2...v0.5.3-rc.3) (2021-01-13)


### Bug Fixes

* **knex:** add ::date conversion on search for data ([1d1dc3f](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1d1dc3ffc272c166cddcc96e439adf20a9e62be7))

## [0.5.3-rc.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.1...v0.5.3-rc.2) (2021-01-13)


### Bug Fixes

* **db.mixin:** count take searchFileds in query ([1e3e38e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1e3e38ea3fc0189c00731e0d6c0222e27acdd853))

## [0.5.3-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.2...v0.5.3-rc.1) (2021-01-13)


### Bug Fixes

* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))
* **knex:** search is now INSENSATIVE CASE ([2085783](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/20857836b22dedd57512ee6c9679e99cacedec3b))

## [0.5.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.1...v0.5.2) (2021-01-06)
* **helpers:** remove this in getSubWithRelated ([4bd918b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/4bd918b37ef2dcfd92a0bfbb2ffd8253a6ff4d5c))
## [0.5.1-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.0...v0.5.1-rc.1) (2021-01-13)

### Bug Fixes
* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))
* **knex:** search is now INSENSATIVE CASE ([2085783](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/20857836b22dedd57512ee6c9679e99cacedec3b))
## [0.5.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.0...v0.5.1) (2021-01-06)

### Bug Fixes

* **helpers:** add searchField to hasOne ([7f3fb10](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/7f3fb10e02d1482f7d54a3ff5adb790159f5512e))


# [0.5.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.6...v0.5.0) (2020-12-02)


### Features

* **erros:** add forbidden and unauthorized errors ([181722b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/181722b80751103b0c1404619d0bca7f68e828b9))

## [0.4.6](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.5...v0.4.6) (2020-11-27)


### Bug Fixes

* **db-mixin:** if withRelated = false skip step ([bba1a21](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/bba1a2113b3367d0f4eb18afdfb8c3afbf4208a0))

## [0.4.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.4...v0.4.5) (2020-11-27)


### Bug Fixes

* **db.mixin:** add withRelated boolean validation ([f3f7c3a](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f3f7c3a22b28cf6930c28af0cab3f9b42a35349a))

## [0.4.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.3...v0.4.4) (2020-11-18)


### Bug Fixes

* **db.mixin:** metadatos in the response to save in cache ([0fec57b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/0fec57b89d44a87a808867c6a376e6df8885316d))

## [0.4.3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.2...v0.4.3) (2020-11-02)


### Bug Fixes

* **db.mixin:** fix _insert on enteties empty array ([116c469](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/116c469d7daf37dd841d609bc510a214202559b9))

## [0.4.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.1...v0.4.2) (2020-11-02)


### Bug Fixes

* **db.mixin:** fix insert return value ([30da099](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/30da099e2d7ff1210b9efafd00ecdf02d23e729e))

## [0.4.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.0...v0.4.1) (2020-10-30)


### Bug Fixes

* **db.mixin:** fix limit sanitize in case of -1 ([47af10e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/47af10efd55757480c3b03b5638ab51ea55cd451))

# [0.4.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.3.0...v0.4.0) (2020-10-30)


### Features

* **validator:** add validateExternalIds ([05f6c07](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/05f6c07ed18ce804f474e838c097e009916c182f))

# [0.3.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.2.6...v0.3.0) (2020-10-30)


### Features

* **db.mixin:** add function for raw queries ([ad31c4e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/ad31c4ebce8e81ae999fe757a328e7f01c077c18))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.6](///compare/v0.2.5...v0.2.6) (2020-10-27)

### [0.2.5](///compare/v0.2.4...v0.2.5) (2020-10-26)


### Bug Fixes

* **db.mixin:** fix _insert function no return ([1150285](///commit/1150285d4737a4ec386caf188c82d0d930b08292))

### [0.2.4](///compare/v0.2.3...v0.2.4) (2020-10-22)


### Bug Fixes

* **knex:** increment knex version to allow set seeds directory path as array ([3c3ba3d](///commit/3c3ba3d1c90f150cfde6d7ae4f33bb8da9b539e6))

### [0.2.3](///compare/v0.2.2...v0.2.3) (2020-10-06)


### Features

* saga pattern middleware ([4bd4081](///commit/4bd4081263424e29da70170f72fbc74756075532))

### [0.2.2](///compare/v0.2.0...v0.2.2) (2020-09-28)


### Bug Fixes

* **db.adapter:** remove params from count ([387eebc](///commit/387eebc379fad1181ec24f0c05fe3cfdf2d63bda))
* **helpers:** fix withRelated hasOne helper ([e4c8894](///commit/e4c889462e360e6c782463d20720f79b71589315))
* **knex-adapter:** fix the insert returning for postgres ([d21321c](///commit/d21321cd8a3ac43ff387737b7eb332324a74ab69))

### [0.2.1](///compare/v0.2.0...v0.2.1) (2020-09-28)


### Bug Fixes

* **helpers:** fix withRelated hasOne helper ([e4c8894](///commit/e4c889462e360e6c782463d20720f79b71589315))
* **knex-adapter:** fix the insert returning for postgres ([d21321c](///commit/d21321cd8a3ac43ff387737b7eb332324a74ab69))

## [0.2.0](///compare/v0.1.17...v0.2.0) (2020-07-01)


### Features

* **db:** broadcast a event automatically ([778d00f](///commit/778d00f003abd740fa138b277080efa420562d90))

### [0.1.17](///compare/v0.1.16...v0.1.17) (2020-07-01)


### Bug Fixes

* **db:** add withRelateds to authorized fields ([0ae8e88](///commit/0ae8e88ca31774e661d76afaefa0ce02a3bcfa51))

### [0.1.16](///compare/v0.1.15...v0.1.16) (2020-06-30)


### Bug Fixes

* **db:** add withRelateds to authorized fields ([dc781c9](///commit/dc781c9613030b5e1004fdb93d0978bf994b461b))
* **db:** fix update return value ([384a4df](///commit/384a4df8cda1b6be16c1ca079aabaa92453b7277))

### [0.1.15](///compare/v0.1.14...v0.1.15) (2020-06-30)


### Bug Fixes

* **db:** fix path and foriegn fields removal ([2bf1895](///commit/2bf18958949bfb46e3eac31183e024ab2b67a80f))

### [0.1.14](///compare/v0.1.13...v0.1.14) (2020-06-30)


### Bug Fixes

* **db:** add remove fireign fields to update / patch ([2e824cd](///commit/2e824cdd2051d5f12d00fed5012da23cca5aadc5))

### [0.1.13](///compare/v0.1.12...v0.1.13) (2020-06-30)


### Bug Fixes

* **db:** fix patch entity validator compilation ([89a917f](///commit/89a917fcb2f3477fb2822bf287c33930a249ea21))

### [0.1.12](///compare/v0.1.11...v0.1.12) (2020-06-30)


### Bug Fixes

* **db:** fix patch entity validator ([8b20d87](///commit/8b20d87458f3437a90d7b9c9b5c4dd7a0e0525fb))

### [0.1.11](///compare/v0.1.10...v0.1.11) (2020-06-30)


### Features

* **db:** add defaultWithRelateds ([aba0505](///commit/aba0505de30947b58bada5e13dec69bb27586788))


### Bug Fixes

* **db:** add entity validator to update and patch method ([d34d158](///commit/d34d158017ca92f2a7880bd11345b88e941b9f08))
* **db:** lint fixs ([852b5ba](///commit/852b5ba537cb0bee3984e4d754ad72a2762ea090))

### [0.1.10](///compare/v0.1.9...v0.1.10) (2020-06-26)

### [0.1.9](///compare/v0.1.8...v0.1.9) (2020-06-25)


### Bug Fixes

* **db.mixin:** fix lint warnings ([b61eac7](///commit/b61eac76ddd920346f46598c4932fd860a88b40e))
* **errors:** change validation error ([0abe831](///commit/0abe831db71112e45a8388a6621708523d9380d4))

### [0.1.8](///compare/v0.1.6...v0.1.8) (2020-06-24)


### Features

* implemented openapi.mixin ([ffc44e3](///commit/ffc44e37f8ec078a35973283bbe6909b4b4379f8))


### Bug Fixes

* **errors:** fix not  found error ([02abd1c](///commit/02abd1c6e30f725fee4931c516649e2f45263413))

### [0.1.7](///compare/v0.1.6...v0.1.7) (2020-06-17)


### Features

* implemented openapi.mixin ([ffc44e3](///commit/ffc44e37f8ec078a35973283bbe6909b4b4379f8))

### [0.1.6](///compare/v0.1.5...v0.1.6) (2020-05-12)


### Bug Fixes

* **knex.adapter:** fix getById return object ([582b75f](///commit/582b75f398c3acc121c400233fca6e9a647f8123))

### [0.1.5](///compare/v0.1.4...v0.1.5) (2020-05-12)


### Features

* **errors:** add validation error ([028be5f](///commit/028be5f7f2c93f74e9b7c6742ec0b016b1c62734))


### Bug Fixes

* **db.mixin:** fix parseBooleans method ([498cee0](///commit/498cee033e0cb008e09f066502614b4cddb2eb2c))

### [0.1.4](///compare/v0.1.3...v0.1.4) (2020-05-12)


### Bug Fixes

* **knex:** add destroy on disconnect ([d385d79](///commit/d385d796420db1b3e12262ba5f6206860ec6e9f1))

### [0.1.3](///compare/v0.1.2...v0.1.3) (2020-05-12)


### Bug Fixes

* **db.mixin:** remove memory adapter opts ([8ddba40](///commit/8ddba40ede103e235c33c41510ff202f40dcdcd5))

### [0.1.2](///compare/v0.1.1...v0.1.2) (2020-05-11)

### 0.1.1 (2020-05-11)


### Features

* **core:** add initial version ([dfd679b](///commit/dfd679b042f0a85b78661264c41f15ed4c7a27bc))
