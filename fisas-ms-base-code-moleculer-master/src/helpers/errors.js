/*
 * moleculer-db
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */

"use strict";

const { MoleculerClientError } = require("moleculer").Errors;

//const ERR_ENTITY_NOT_FOUND = "ERR_ENTITY_NOT_FOUND";

/**
 * Entity not found
 *
 * @class EntityNotFoundError
 * @extends {MoleculerClientError}
 */
class EntityNotFoundError extends MoleculerClientError {

	/**
	 * Creates an instance of EntityNotFoundError.
	 *
	 * @param {any} Name of entity
	 * @param {any} ID of entity
	 *
	 * @memberOf EntityNotFoundError
	 */
	constructor(name, id) {
		super(`Entity ${name} with id: ${id} not found`, 404, "NOT_FOUND_ERROR", {
			code: 404,
			name,
			id
		});
	}
}

/**
 * Validation Error
 *
 * @class ValidationError
 * @extends {MoleculerClientError}
 */
class ValidationError extends MoleculerClientError {

	/**
	 * Creates an instance of EntityNotFoundError.
	 *
	 * @param {any} message a tecnical message of the error
	 * @param {any} type a string that identifies uniquely the error
	 * @param {any} data array of validation erros
	 * @param {any} data.field the name of the field
	 * @param {any} data.type the typ of error FastestValidatorType
	 * @param {any} data.message message like FastestValidator
	 *
	 * @memberOf ValidationError
	 */
	constructor(message, type, data) {
		super(message, 400, type, data);
	}
}


/**
 * Unauthorized Error
 *
 * @class UnauthorizedError
 * @extends {MoleculerClientError}
 */
class UnauthorizedError extends MoleculerClientError {

	/**
	 * Creates an instance of EntityNotFoundError.
	 *
	 * @param {any} message a tecnical message of the error
	 * @param {any} type a string that identifies uniquely the error
	 * @param {any} data array of validation erros
	 * @param {any} data.field the name of the field
	 * @param {any} data.type the typ of error FastestValidatorType
	 * @param {any} data.message message like FastestValidator
	 *
	 * @memberOf ValidationError
	 */
	constructor(message, type, data) {
		super(message, 401, type, data);
	}
}


/**
 * Forbidden Error
 *
 * @class ForbiddenError
 * @extends {MoleculerClientError}
 */
class ForbiddenError extends MoleculerClientError {

	/**
	 * Creates an instance of EntityNotFoundError.
	 *
	 * @param {any} message a tecnical message of the error
	 * @param {any} type a string that identifies uniquely the error
	 * @param {any} data array of validation erros
	 * @param {any} data.field the name of the field
	 * @param {any} data.type the typ of error FastestValidatorType
	 * @param {any} data.message message like FastestValidator
	 *
	 * @memberOf ValidationError
	 */
	constructor(message, type, data) {
		super(message, 403, type, data);
	}
}


module.exports = {
	EntityNotFoundError,
	ValidationError,
	UnauthorizedError,
	ForbiddenError
};