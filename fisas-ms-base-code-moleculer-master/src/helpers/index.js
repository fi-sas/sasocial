const Errors = require("./errors");
const Responses = require("./responses");
const WithRelateds = require("./withRelated");
const SearchRelateds = require("./searchRelated");
module.exports = {
	Errors,
	Responses,
	WithRelateds,
	SearchRelateds,
};
