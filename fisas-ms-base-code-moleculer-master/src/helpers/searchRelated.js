
/**
 *
 * @param {*} ctx The Moleculer Context object
 * @param {*} fields Array of fields to search
 * @param {*} actionPath The path of the action without de `.find`
 * @param {*} idField The field that identifies the data on external MS for example `user_id`
 * @returns Promise with the updated ctx.params
 */
function addSearchRelation(ctx, fields, actionPath, idField) {
	if (ctx.params.searchFields && ctx.params.search) {

		const searchFields = ctx.params.searchFields.split(",");
		fields = fields.filter(field => searchFields.includes(field));
		fields.forEach(field => {
			if (searchFields.includes(field))
				searchFields.splice(searchFields.indexOf(field), 1);
		});
		ctx.params.searchFields = searchFields.join(",");

		return ctx.call(actionPath.concat(".find"), {
			searchFields: fields,
			search: ctx.params.search
		}).then(res => {
			if (ctx.params.searchIds && Array.isArray(ctx.params.searchIds)) {
				ctx.params.searchIds.push(...res.map(translation => translation[idField]));
			} else {
				ctx.params.searchIds = res.map(translation => translation[idField]);
			}
		});
	}
}

module.exports = {
	addSearchRelation,
};
