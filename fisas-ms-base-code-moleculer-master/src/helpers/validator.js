const _ = require("lodash");

/***
 * Validate external ids
 * {
						course_id: { action: "configuration.courses.get" }, }
 * @methods
 * @param {Object} ctx The context object
 * @param {Object} opts The object for testing, the key of objecto is the name of the file
 * @param {string} opts.action the string with the path of the action
 * @returns {Array} return a array with all enteties
 */
async function validateExternalIds(ctx, opts) {
	const opts_fields = _.keys(opts);

	let promises = [];
	_.forEach(opts_fields, (field) => {
		this.logger.info(opts[field].action, {
			id: ctx.params[field]
		});
		if (ctx.params[field]) {
			promises.push(ctx.call(opts[field].action, {
				id: ctx.params[field]
			}));
		}
	});

	return await Promise.all(promises).then(res => {
		return res;
	}).catch(err => {
		throw err;
	});
}

module.exports = {
	validateExternalIds
};