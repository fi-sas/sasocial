const _ = require("lodash");

/**
 * DEPRECATED getSubWithRelated
 * @param {*} ctx
 * @param {*} field
 * @returns
 */
function getSubWithRelated(ctx, field) {
	const withRelateds = ctx.params.withRelated;
	if (Array.isArray(withRelateds)) {
		const withRelatedField = withRelateds
			.filter((k) => k.startsWith(field.concat(".")))
			.map((k) => k.split(".").pop());
		return withRelatedField;
	} else {
		return [];
	}
}

/**
 * Use this in the withRelateds when you want to  fin one relation of this services
 *
 * @param {*} docs The moleculer docs
 * @param {*} ctx The muleculer context
 * @param {*} serviceName The name of service where you want the data
 * @param {*} newField The new field name
 * @param {*} identificatorField The field the doc you want to use to compare
 * @param {*} searchField The field you want to use in the query
 * @param {*} additionalQuery  Optional field for extra query data
 * @param {*} fields  Optional field for fields to return
 * @param {*} withRelated  Optional field for the withRealteds of the request
 */
function hasOne(
	docs,
	ctx,
	serviceName,
	newField,
	identificatorField,
	searchField = "id",
	additionalQuery = {},
	fields = null,
	withRelated = null,
) {
	const ids = _.uniq(docs.map((d) => d[identificatorField]));
	const query = { ...additionalQuery };
	if (searchField) {
		query[searchField] = ids;
	} else {
		query["id"] = ids;
	}

	const params = {
		query
	};

	if(fields) {
		if(Array.isArray(fields))
			fields = fields.join(",");

		params.fields = searchField + "," + fields;
	}

	if(withRelated !== null) {
		params.withRelated = withRelated;
	}

	return ctx
		.call(serviceName + ".find", params)
		.then((result) => {
			return Promise.resolve(
				docs.map(
					(d) =>
						(d[newField] =
							result.find((c) =>
								c ? c.id === d[identificatorField] : false
							) || null)
				)
			);
		});
}

/**
 * Use this in the withRelateds when you want to find a relation of multiple items for this service
 *
 * @param {*} docs The moleculer docs
 * @param {*} ctx The muleculer context
 * @param {*} serviceName The name of service where you want the data
 * @param {*} newField The new field name
 * @param {*} identificatorField The field the doc you want to use to compare
 * @param {*} searchField The field you want to use in the query
 * @param {*} additionalQuery  Optional field for extra query data
 * @param {*} fields  Optional field for fields to return
 * @param {*} withRelated  Optional field for the withRealteds of the request
 */
function hasMany(
	docs,
	ctx,
	serviceName,
	newField,
	identificatorField = "id",
	searchField,
	additionalQuery = {},
	fields = null,
	withRelated = null,
) {
	const ids = docs.map((d) => d[identificatorField]);
	const query = { ...additionalQuery };
	query[searchField] = ids;
	const params = {
		query
	};

	if(fields) {
		if(Array.isArray(fields))
			fields = fields.join(",");

		params.fields = searchField + "," + fields;
	}

	if(withRelated !== null) {
		params.withRelated = withRelated;
	}

	return ctx.call(serviceName + ".find", params).then(data => {
		return docs.map(d => (d[newField] = data.filter(dt => dt[searchField] === d[identificatorField])));
	});
}

module.exports = {
	hasOne,
	hasMany,
};
