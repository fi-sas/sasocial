/*
 * moleculer-db-adapter-knex
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */

"use strict";

const _ = require("lodash");
const Promise = require("bluebird");
const Knex = require("knex");
const moment = require("moment");

// Knex shared pool between services
let sharedKnex = null;


class KnexDbAdapter {

	/**
	 * Creates an instance of KnexDbAdapter.
	 * @param {any} opts
	 * @param {any} opts2
	 *
	 * @memberof KnexDbAdapter
	 */
	constructor(opts, opts2) {
		this.opts = opts;
		this.opts2 = opts2;
	}

	/**
	 * Initialize adapter
	 *
	 * @param {ServiceBroker} broker
	 * @param {Service} service
	 *
	 * @memberof KnexDbAdapter
	 */
	init(broker, service) {
		this.broker = broker;
		this.service = service;

		if (!this.service.schema.table) {
			/* istanbul ignore next */
			throw new Error("Missing `table` definition in schema of service!");
		}
		this.idField = this.service.settings.idField || "id";
	}

	/**
	 * Connect to database
	 *
	 * @returns {Promise}
	 *
	 * @memberof KnexDbAdapter
	 */
	connect() {

		if(this.db && this.db.client.pool) {
			return Promise.resolve(true);
		}

		if (!sharedKnex) {
			sharedKnex = Knex(this.opts || {}, this.opts2);
		} else {
			if (sharedKnex && !sharedKnex.client.pool) {
				sharedKnex = Knex(this.opts || {}, this.opts2);
			}
		}

		this.db = sharedKnex;



		const tableDef = this.service.schema.table;
		if (_.isString(tableDef)) {
			this.table = tableDef;
		} else if (_.isObject(tableDef)) {
			this.table = tableDef.name;
			if (_.isFunction(tableDef.builder)) {
				return this.db.schema.createTableIfNotExists(this.table, tableDef.builder);
			}
		}

		return Promise.resolve(true);
	}

	/**
	 * Disconnect from database
	 *
	 * @returns {Promise}
	 *
	 * @memberof KnexDbAdapter
	 */
	disconnect() {
		if (this.db) {
			this.db.destroy();
		}
		return Promise.resolve(true);
	}

	/**
	 * Run a query a return the result
	 *
	 * @param {string} query
	 * @returns {Promise<Array>}
	 *
	 * @memberof KnexDbAdapter
	 */
	raw(query, bindings) {
		this.connect();
		return this.db.raw(query, bindings);
	}

	/**
	 * Find all entities by filters.
	 *
	 * Available filter props:
	 * 	- limit
	 *  - offset
	 *  - sort
	 *  - search
	 *  - searchFields
	 *  - query
	 *
	 * @param {Object} filters
	 * @returns {Promise<Array>}
	 *
	 * @memberof KnexDbAdapter
	 */
	find(filters) {
		this.connect();
		return this.createCursor(filters, false);
	}

	/**
	 * Find an entities by ID.
	 *
	 * @param {String} id
	 * @returns {Promise<Object>} Return with the found document.
	 *
	 * @memberof KnexDbAdapter
	 */
	findById(id) {
		this.connect();
		return this.db(this.table).where(this.idField, id).limit(1).then(res => res.length > 0 ? res : null);
	}

	/**
	 * Find any entities by IDs.
	 *
	 * @param {Array} idList
	 * @returns {Promise<Array>} Return with the found documents in an Array.
	 *
	 * @memberof KnexDbAdapter
	 */
	findByIds(idList) {
		this.connect();
		return this.db(this.table).whereIn(this.idField, idList);
	}

	/**
	 * Get count of filtered entites.
	 *
	 * Available query props:
	 *  - search
	 *  - searchFields
	 *  - query
	 *
	 * @param {Object} [filters={}]
	 * @returns {Promise<Number>} Return with the count of documents.
	 *
	 * @memberof KnexDbAdapter
	 */
	count(filters = {}) {
		this.connect();
		return new Promise((resolve, reject) => {
			this.createCursor(filters, true).then((result) => {
				resolve(result[0] ? result[0].count : 0);
			}).catch((err) => reject(err));
		});
	}

	/**
	 * Insert an entity.
	 *
	 * @param {Object} entity
	 * @returns {Promise<Object>} Return with the inserted document.
	 *
	 * @memberof KnexDbAdapter
	 */
	insert(entity) {
		this.connect();
		return this.db(this.table).returning(this.idField).insert(entity);
	}

	/**
	 * Insert many entities
	 *
	 * @param {Array} entities
	 * @returns {Promise<Array<Object>>} Return with the inserted documents in an Array.
	 *
	 * @memberof KnexDbAdapter
	 */
	insertMany(entities) {
		this.connect();
		return this.db(this.table).returning(this.idField).insert(entities, "*");
	}

	/**
	 * Update many entities by `query` and `update`
	 *
	 * @param {Object} query
	 * @param {Object} update
	 * @returns {Promise<Number>} Return with the count of modified documents.
	 *
	 * @memberof KnexDbAdapter
	 */
	updateMany(query, update) {
		this.connect();
		return this.db(this.table).where(query).update(update);
	}

	/**
	 * Update an entity by ID and `update`
	 *
	 * @param {String} id - ObjectID as hexadecimal string.
	 * @param {Object} update
	 * @returns {Promise<Object>} Return with the updated document.
	 *
	 * @memberof KnexDbAdapter
	 */
	updateById(id, update) {
		this.connect();
		return this.db(this.table).where(this.idField, id).update(update);
	}

	/**
	 * Remove entities which are matched by `query`
	 *
	 * @param {Object} query
	 * @returns {Promise<Number>} Return with the count of deleted documents.
	 *
	 * @memberof KnexDbAdapter
	 */
	removeMany(query) {
		this.connect();
		return this.db(this.table).where(query).del();
	}

	/**
	 * Remove an entity by ID
	 *
	 * @param {String} id - ObjectID as hexadecimal string.
	 * @returns {Promise<Object>} Return with the removed document.
	 *
	 * @memberof KnexDbAdapter
	 */
	removeById(id) {
		this.connect();
		return this.db(this.table).where(this.idField, id).del();
	}

	/**
	 * Clear all entities from table
	 *
	 * @returns {Promise}
	 *
	 * @memberof KnexDbAdapter
	 */
	clear() {
		this.connect();
		return this.db(this.table).del();
	}

	/**
	 * Convert DB entity to JSON object. It converts the `id` to hexadecimal `String`.
	 *
	 * @param {Object} entity
	 * @returns {Object}
	 * @memberof KnexDbAdapter
	 */
	entityToObject(entity) {
		let json = Object.assign({}, entity);
		return json;
	}

	/**
	 * Create a filtered cursor.
	 *
	 *
	 * Available filters in `params`:
	 *  - search
	 * 	- sort
	 * 	- limit
	 * 	- offset
	 *  - query
	 *
	 * @param {Object} params
	 * @param {Boolean} isCounting
	 * @returns {MongoCursor}
	 */
	createCursor(params, isCounting) {

		if (!params) {
			if (isCounting) {

				return this.db(this.table).count(this.idField, {
					as: "count"
				});
			}

			return this.db(this.table).where({});
		}


		let q = this.db(this.table);

		if(params.extraQuery) {
			if (_.isFunction(params.extraQuery)) {
				params.extraQuery(q);
			}
		}

		if (params.query) {

			if (_.isObject(params.query)) {
				Object.keys(params.query).map(k => {

					if (Array.isArray(params.query[k])) {
						q = q.whereIn(k, params.query[k]);
					} else if (_.isObject(params.query[k])) {
						Object.keys(params.query[k]).map(sk => {

							// IF IS DATE
							if(moment(params.query[k][sk], "YYYY-MM-DD", true).isValid()) {
								// NOT NULL
								if (sk == "gte" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"::date`), ">=",params.query[k][sk]);

								if (sk == "gt" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"::date`), ">",params.query[k][sk]);

								if (sk == "lte" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"::date`), "<=",params.query[k][sk]);

								if (sk == "lt" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"::date`), "<",params.query[k][sk]);

							// IF IS DATETIME
							} else if(moment(params.query[k][sk], moment.ISO_8601, true).isValid()) {
								// NOT NULL
								if (sk == "gte" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"`), ">=",params.query[k][sk]);

								if (sk == "gt" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"`), ">",params.query[k][sk]);

								if (sk == "lte" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"`), "<=",params.query[k][sk]);

								if (sk == "lt" && params.query[k][sk])
									q = q.where(Knex.raw(`"${k}"`), "<",params.query[k][sk]);

							} else {

								// NOT NULL
								if (sk == "notNull" && params.query[k][sk])
									q = q.whereNotNull(k);

								// NOT EQUAL
								if (sk == "ne" && params.query[k][sk])
									q = q.whereNotEqual(k, params.query[k][sk]);

								// NOT IN
								if (sk == "nin" && params.query[k][sk])
									q = q.whereNotIn(k, params.query[k][sk]);

								// IN
								if (sk == "in" && params.query[k][sk])
									q = q.whereIn(k, params.query[k][sk]);

								// GTE
								if (sk == "gte" && params.query[k][sk])
									q = q.where(k, ">=",params.query[k][sk]);

								// GT
								if (sk == "gt" && params.query[k][sk])
									q = q.where(k, ">",params.query[k][sk]);

								// LTE
								if (sk == "lte" && params.query[k][sk])
									q = q.where(k, "<=",params.query[k][sk]);

								// LT
								if (sk == "lt" && params.query[k][sk])
									q = q.where(k, "<",params.query[k][sk]);

							}
						});
					} else {

						// IF IS DATE TELL POSTGRES
						if(moment(params.query[k], "YYYY-MM-DD", true).isValid()) {
							params.query[k] = moment(params.query[k]).format("YYYY-MM-DD");
							q = q.where(Knex.raw(`"${k}"::date`), params.query[k]);
						} else {
							q = q.where(k, params.query[k]);
						}
					}


				});
			}

			if (_.isFunction(params.query)) {
				params.query(q);
			}
		}

		// Full-text search
		if (_.isString(params.search) && params.search !== "") {

			let fields = [];
			if (params.searchFields) {
				fields = _.isString(params.searchFields) ? params.searchFields.split(" ") : params.searchFields;
			} else if (this.service.settings.searchFields) {
				fields = this.service.settings.searchFields;
			}

			q.andWhere((qft) => {
				fields.forEach(f => {
					if (f) {
						qft.orWhere(f, "ILIKE", `%${params.search}%`);
					}
				});

				if (params.searchIds) {
					qft.orWhere(this.idField, "in", params.searchIds);
				}
			});



		}

		// Sort
		if (params.sort) {
			q = this.transformSort(q, params.sort);
		}

		// Offset
		if (_.has(params, "offset") && _.isNumber(params.offset) && params.offset > 0)
			q.offset(params.offset);

		// Limit
		if (_.has(params, "limit") && _.isNumber(params.limit) && params.limit > 0)
			q.limit(params.limit);

		//return q;


		// If not params
		if (isCounting) {
			return q.count(this.idField, {
				as: "count"
			});
		} else {
			return q;
		}
	}

	/**
	 * Transforms 'idField' into NeDB's 'id'
	 * @param {Object} entity
	 * @param {String} idField
	 * @memberof MemoryDbAdapter
	 * @returns {Object} Modified entity
	 */
	beforeSaveTransformID(entity, idField) {
		let newEntity = _.cloneDeep(entity);

		if (idField !== "id" && entity[idField] !== undefined) {
			newEntity.id = newEntity[idField];
			delete newEntity[idField];
		}

		return newEntity;
	}

	/**
	 * Transforms NeDB's 'id' into user defined 'idField'
	 * @param {Object} entity
	 * @param {String} idField
	 * @memberof MemoryDbAdapter
	 * @returns {Object} Modified entity
	 */
	afterRetrieveTransformID(entity, idField) {
		if (idField !== "id") {
			entity[idField] = entity["id"];
			delete entity.id;
		}
		return entity;
	}

	/**
	 * Convert the `sort` param to a `sort` object to Mongo queries.
	 *
	 * @param {Cursor} q
	 * @param {String|Array<String>|Object} paramSort
	 * @returns {Object} Return with a sort object like `{ "votes": 1, "title": -1 }`
	 * @memberof KnexDbAdapter
	 */
	transformSort(q, paramSort) {
		let sort = paramSort;
		if (_.isString(sort))
			sort = sort.replace(/,/, " ").split(" ");

		if (Array.isArray(sort)) {
			let sortArray = [];
			sort.forEach(s => {
				if (s.startsWith("-"))
					sortArray.push({
						column: s.replace("-", ""),
						order: "desc"
					});
				else
					sortArray.push({
						column: s.replace("-", ""),
						order: "asc"
					});
			});

			return q.orderBy(sortArray);
		}

		if (_.isObject(sort)) {
			Object.keys(sort).forEach(key => {
				q = q.sort(key, sort[key] > 0 ? "asc" : "desc");
			});
		}

		return q;
	}

}

module.exports = KnexDbAdapter;
