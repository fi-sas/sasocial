"use strict";

const { ServiceBroker } = require("moleculer");
const Adapter = require("../../src/mixins/knex-adapter");


function protectReject(err) {
	if (err && err.stack) {
		console.error(err);
		console.error(err.stack);
	}
	expect(err).toBe(true);
}

describe("Test Adapter constructor", () => {
	it("should be created", () => {
		const adapter = new Adapter({
			client: "sqlite3",
			connection: ":memory:",
		});
		expect(adapter).toBeDefined();
	});

	it("should be created with opts", () => {
		const opts = {
			client: "sqlite3",
			connection: ":memory:",
		};
		const adapter = new Adapter(opts);
		expect(adapter).toBeDefined();
		expect(adapter.opts).toBe(opts);
	});
});

describe("Test Adapter methods", () => {
	const broker = new ServiceBroker({ logger: false });
	const service = broker.createService({
		name: "test",
		table: "test",
		settings: {
			searchFields: ["name", "email"]
		}
	});

	const opts = {
		client: "sqlite3",
		connection: {
			filename: ":memory:"
		},
		useNullAsDefault: true,
		debug: true
	};

	const adapter = new Adapter(opts);
	adapter.init(broker, service);

	it("should connect", () => {
		return expect(adapter.connect()).resolves.toBeUndefined();
	});

	it("should create table", () => {
		return expect(adapter.db.schema
			.createTable('test', function (table) {
				table.increments();
				table.string('name');
				table.string('email');
				table.integer('age');
			})).resolves.toEqual([]);
	});
	const doc = {
		name: "Walter White",
		age: 48,
		email: "heisenberg@gmail.com"
	};

	let savedDocId;

	it("should insert a document", () => {
		return adapter.insert(doc)
			.then(res => {
				expect(res).toEqual([1]);
				savedDocId = res[0];
			})
			.catch(protectReject);
	});

	it("should insert multiple document", () => {
		return adapter.insertMany([
			{ name: "John Doe", age: 41 },
			{ name: "Jane Doe", age: 35 },
			{ name: "Adam Smith", email: "adam.smith@gmail.com", age: 35 }
		])
			.then(res => {
				expect(res).toStrictEqual([4]);
			})
			.catch(protectReject);
	});

	it("should find by ID", () => {
		doc.id = savedDocId;
		return expect(adapter.findById(savedDocId)).resolves.toEqual([doc]);
	});

	it("should find one", () => {
		return expect(adapter.find({ age: 48, limit: 1 })).resolves.toEqual([doc]);
	});

	const savedDocs = [
		{ id: 2, name: "John Doe", email: null, age: 41 },
		{ id: 3, name: "Jane Doe", email: null, age: 35 },
		{ id: 4, name: "Adam Smith", email: "adam.smith@gmail.com", age: 35 }
	];

	it("should find by multiple ID", () => {
		return expect(adapter.findByIds([2,3,])).resolves.toEqual([savedDocs[0], savedDocs[1]]);
	});

	it("should find all without filter", () => {
		return adapter.find().then(res => {
			expect(res.length).toBe(4);
		}).catch(protectReject);
	});

	it("should find all 'name' with raw query", () => {
		return expect(adapter.find({ query: { name: "John Doe" } })).resolves.toEqual([savedDocs[0]]);
	});

	it("should find all 'age: 35'", () => {
		return adapter.find({ query: { age: 35 } }).then(res => {
			expect(res.length).toBe(2);
			expect(res[0].age).toEqual(35);
			expect(res[1].age).toEqual(35);

		}).catch(protectReject);
	});

	it("should find all 'Doe'", () => {
		return adapter.find({ search: "Doe" }).then(res => {
			expect(res.length).toBe(2);
			expect(res[0].name).toMatch("Doe");
			expect(res[1].name).toMatch("Doe");

		}).catch(protectReject);
	});

	it("should find all 'Doe' in filtered fields", () => {
		return adapter.find({ search: "Doe", searchFields: ["email"] }).then(res => {
			expect(res.length).toBe(0);
		}).catch(protectReject);
	});

	it("should find all 'walter' in filtered fields", () => {
		return adapter.find({ search: "walter", searchFields: "email name" }).then(res => {
			expect(res.length).toBe(1);
			expect(res[0]).toEqual(doc);

		}).catch(protectReject);
	});

	it("should count all 'walter' in filtered fields", () => {
		return expect(adapter.count({ search: "walter", searchFields: "email name" })).resolves.toBe(1);
	});

	it("should sort the result", () => {
		return expect(adapter.find({ sort: ["name"] })).resolves.toEqual([
			savedDocs[2],
			savedDocs[1],
			savedDocs[0],
			doc,
		]);
	});

	it("should sort by two fields in array", () => {
		return expect(adapter.find({ sort: ["-age", "-name"] })).resolves.toEqual([
			doc,
			savedDocs[0],
			savedDocs[1],
			savedDocs[2],
		]);
	});

	it("should limit & skip the result", () => {
		return expect(adapter.find({ sort: ["-age", "-name"], limit: 2, offset: 1 })).resolves.toEqual([
			savedDocs[0],
			savedDocs[1],
		]);
	});

	it("should count all entities", () => {
		return expect(adapter.count()).resolves.toBe(4);
	});

	it("should count filtered entities", () => {
		return expect(adapter.count({ query: { email: { notNull: true } } })).resolves.toBe(2);
	});

	it("should update a document", () => {
		return expect(adapter.updateById(doc.id, { age: 35 })).resolves.toBe(1);
	});

	it("should update many documents", () => {
		return expect(adapter.updateMany({ age: 35 }, { age: 36 } )).resolves.toBe(3);
	});

	it("should remove by ID", () => {
		return expect(adapter.removeById(doc.id)).resolves.toBe(1);
	});

	it("should remove many documents", () => {
		return expect(adapter.removeMany({ age: 36 })).resolves.toBe(2);
	});

	it("should count all entities", () => {
		return expect(adapter.count()).resolves.toBe(1);
	});

	it("should clear all documents", () => {
		return expect(adapter.clear()).resolves.toBe(1);
	});

	it("should disconnect", () => {
		return expect(adapter.disconnect()).resolves.toBeUndefined();
	});
});
