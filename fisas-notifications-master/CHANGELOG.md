## [1.30.2](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.30.1...v1.30.2) (2022-05-06)


### Bug Fixes

* **alerts:** bulk alerts metadata ([e07e30d](https://gitlab.com/fi-sas/fisas-notifications/commit/e07e30dd081257dac168267dd51169bed662183f))

## [1.30.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.30.0...v1.30.1) (2022-04-22)


### Bug Fixes

* **alerts:** bulk alerts timeout because nested call ([17623c9](https://gitlab.com/fi-sas/fisas-notifications/commit/17623c9f798521ef8f675ed370deb3f3a17011eb))

# [1.30.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.29.1...v1.30.0) (2022-03-28)


### Bug Fixes

* **accommodation:** Change name from IPVC to SASocial ([fdfab2f](https://gitlab.com/fi-sas/fisas-notifications/commit/fdfab2f8f2af89d69eb2923f175fc69ab1f64415))
* **accommodation:** changing email and removing href for ipvc ([372b83f](https://gitlab.com/fi-sas/fisas-notifications/commit/372b83fcfcdd954817f6eb63ce2c009fa8f5de75))


### Features

* **accommodation:** new alerts for period changes ([dad0fbf](https://gitlab.com/fi-sas/fisas-notifications/commit/dad0fbf2a8b79c95f23371805ab9830144f05f9b))

## [1.29.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.29.0...v1.29.1) (2022-03-08)


### Bug Fixes

* **sms:** add timeout to sms send request ([ae69c45](https://gitlab.com/fi-sas/fisas-notifications/commit/ae69c4585ffd770b9f3277bb37b837010c794ef8))

# [1.29.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.28.0...v1.29.0) (2022-02-14)


### Features

* **alerts:** send attachements on emails ([5c067ef](https://gitlab.com/fi-sas/fisas-notifications/commit/5c067ef5bad7ff122a4955a6799b43627f07c18c))
* open create alert endpoint ([6eb8e0c](https://gitlab.com/fi-sas/fisas-notifications/commit/6eb8e0c293446c30802af631531b056aeae618f0))

# [1.28.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.27.0...v1.28.0) (2022-01-11)


### Features

* **alerts:** allow notifications to external entities ([845c68f](https://gitlab.com/fi-sas/fisas-notifications/commit/845c68f60c3bd932d4809c13c15ec0d9f06888a9))

# [1.27.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.26.1...v1.27.0) (2022-01-05)


### Features

* attendance approach and faster, close desk ([148e9d7](https://gitlab.com/fi-sas/fisas-notifications/commit/148e9d736b387e79be49583cf6f0d319bf681697))
* new experience, user experience, application ([b6b4d9f](https://gitlab.com/fi-sas/fisas-notifications/commit/b6b4d9f2f7aba16ae40a944f8272d3c4d41ce64f))

## [1.26.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.26.0...v1.26.1) (2021-12-13)


### Bug Fixes

* **alerts:** add pending notifications to retry ([e71f60a](https://gitlab.com/fi-sas/fisas-notifications/commit/e71f60aa76276b519fa0a407d18c85fb025b9d68))

# [1.26.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.25.0...v1.26.0) (2021-12-13)


### Features

* **alert:** retry alert ([5a104ed](https://gitlab.com/fi-sas/fisas-notifications/commit/5a104ed0fa26037ceffa9f29e53a0c890bef3c66))
* **notifications:** add priorirty to notifications ([83fb90c](https://gitlab.com/fi-sas/fisas-notifications/commit/83fb90c7ac2cb033f925fd604960995ffbc5c7d7))

# [1.25.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.24.1...v1.25.0) (2021-11-17)


### Features

* estado aceite ([78396a0](https://gitlab.com/fi-sas/fisas-notifications/commit/78396a0c23a01bf5a0e0cac38cd70d1eaae1f3ec))

## [1.24.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.24.0...v1.24.1) (2021-11-10)


### Bug Fixes

* **alert-types:** clear cache on template model change ([5071c55](https://gitlab.com/fi-sas/fisas-notifications/commit/5071c559bd59a31d8c9bd36ab495b1a5df790e91))

# [1.24.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.23.0...v1.24.0) (2021-10-14)


### Features

* **seeds:** add notification volunteering dispatch ([e9bd85b](https://gitlab.com/fi-sas/fisas-notifications/commit/e9bd85b7e3a2dc9eef70853fd58985a2802403e1))
* **seeds:** add seeds dispatch scholarship ([60f779a](https://gitlab.com/fi-sas/fisas-notifications/commit/60f779a6f1b1c61f74d4435ae7e3983deda044c9))

# [1.23.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.22.0...v1.23.0) (2021-10-06)


### Features

* notifications for emergency fund v2 ([d473912](https://gitlab.com/fi-sas/fisas-notifications/commit/d4739122e40726a465bbee2b7cae59738be1cdf2))

# [1.22.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.21.0...v1.22.0) (2021-09-17)


### Features

* **alert-types:** add iban change request alert type ([6934c67](https://gitlab.com/fi-sas/fisas-notifications/commit/6934c67a3696fd5f88ea964a73b36c38d0c13e73))

# [1.21.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.20.0...v1.21.0) (2021-09-15)


### Features

* **seeds:** apdate notifications bus ([69bf8ea](https://gitlab.com/fi-sas/fisas-notifications/commit/69bf8eab44281507ae3ce1cc4e2d55f3c4af95da))

# [1.20.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.19.0...v1.20.0) (2021-09-14)


### Features

* **seed:** absence notification ([02b0210](https://gitlab.com/fi-sas/fisas-notifications/commit/02b0210f8db81af8d01f121bc1792ef16622679b))
* **seed:** notificações para o MS Saúde ([4a42d39](https://gitlab.com/fi-sas/fisas-notifications/commit/4a42d392ec41b697d789fa03a3c9a58c7466a7a2))

# [1.19.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.18.1...v1.19.0) (2021-09-14)


### Bug Fixes

* **alert_types:** fix errors on alert types ([4478cc5](https://gitlab.com/fi-sas/fisas-notifications/commit/4478cc5737fb5611b0bc8574f83cddad155d017d))


### Features

* **alert-types:** add room change alert types ([c47ddad](https://gitlab.com/fi-sas/fisas-notifications/commit/c47ddada9a0e14316cde8161726ad72fb20de02a))

## [1.18.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.18.0...v1.18.1) (2021-09-08)


### Bug Fixes

* **seeds:** update seeds notifications social support ([b42c02f](https://gitlab.com/fi-sas/fisas-notifications/commit/b42c02f1f8ec9ab16868c797275b3bb865b81021))

# [1.18.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.17.0...v1.18.0) (2021-09-07)


### Features

* **alert_types:** add accommodations new alert types ([deb3c5a](https://gitlab.com/fi-sas/fisas-notifications/commit/deb3c5a8bf7d32b63b327861b6ed1c850027e696))

# [1.17.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.16.1...v1.17.0) (2021-09-02)


### Features

* **seeds:** add calendar seeds alert users ([f28c0b8](https://gitlab.com/fi-sas/fisas-notifications/commit/f28c0b85eeff64bfa698b0bbefda285685e1fb20))

## [1.16.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.16.0...v1.16.1) (2021-08-24)


### Bug Fixes

* **seed:** fix auth pin variable name ([6de87d6](https://gitlab.com/fi-sas/fisas-notifications/commit/6de87d66ad92b6cf753d774c821c3732b552df65))

# [1.16.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.15.1...v1.16.0) (2021-08-19)


### Features

* **seed:** add accommodation application change requests alert types ([a92b6d5](https://gitlab.com/fi-sas/fisas-notifications/commit/a92b6d57c163d252593836157fdbab4b0a158e1e))

## [1.15.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.15.0...v1.15.1) (2021-08-17)


### Bug Fixes

* **cc:** changes template erp middleware ([72cc6ae](https://gitlab.com/fi-sas/fisas-notifications/commit/72cc6aea5f83ba3fa2822f467f56c0795061e10c))

# [1.15.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.14.0...v1.15.0) (2021-07-30)


### Features

* initial notifications for emergency fund ([d717d06](https://gitlab.com/fi-sas/fisas-notifications/commit/d717d062764918f4fcacbe4926f8aa37ef61559f))

# [1.14.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.13.1...v1.14.0) (2021-07-27)


### Features

* **seed:** add new accommodaiton alert types ([6bcf4dd](https://gitlab.com/fi-sas/fisas-notifications/commit/6bcf4dd957693dfea7aca619e947f40b17574b50))

## [1.13.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.13.0...v1.13.1) (2021-07-22)


### Bug Fixes

* **alerts:** add scope to retry endpoint ([ab78f83](https://gitlab.com/fi-sas/fisas-notifications/commit/ab78f83690d7d202db84539f884f609a82edcb68))

# [1.13.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.12.0...v1.13.0) (2021-07-07)


### Features

* **alert_types:** add mobility alert types ([01b1d19](https://gitlab.com/fi-sas/fisas-notifications/commit/01b1d196e116425e5469c925724425f0a0093fb5))

# [1.12.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.11.0...v1.12.0) (2021-07-01)


### Bug Fixes

* **seed:** fix bus sub23 delarations alert types ([97919a4](https://gitlab.com/fi-sas/fisas-notifications/commit/97919a47c8482287b32febfe454999153af42b2a))


### Features

* **seed:** add sub23 applications alert types ([f2171c7](https://gitlab.com/fi-sas/fisas-notifications/commit/f2171c79f430c12236c70e934de759bc9b376c2a))

# [1.11.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.10.0...v1.11.0) (2021-06-29)


### Features

* **notifications:** add option do delte internal ([4c9f3c0](https://gitlab.com/fi-sas/fisas-notifications/commit/4c9f3c0b242811fdcb51c751769d69c4c58df6fb))
* **social_support_seed:** new status change ([9148e16](https://gitlab.com/fi-sas/fisas-notifications/commit/9148e16d488c0f43a62f898a8fee89d823a425b4))
* **volunteering_seed:** new status change ([6c5a96e](https://gitlab.com/fi-sas/fisas-notifications/commit/6c5a96e7a48538235d61da9a1a611ae42b94a9ae))

# [1.10.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.9.1...v1.10.0) (2021-06-24)


### Features

* **seed:** add mobility alert types seed ([890876f](https://gitlab.com/fi-sas/fisas-notifications/commit/890876f59b3397501e0462a84e177a9c96fc9f5f))

## [1.9.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.9.0...v1.9.1) (2021-06-16)


### Bug Fixes

* **notifications:** bad historic info generated ([cb584a6](https://gitlab.com/fi-sas/fisas-notifications/commit/cb584a62ec906cdb8ce833e2fdde2c77d4b2ae38))

# [1.9.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.8.0...v1.9.0) (2021-06-09)


### Bug Fixes

* **seeds:** add return_reason ([7480435](https://gitlab.com/fi-sas/fisas-notifications/commit/74804356af9daf6fc38d43096715c0d388f646f2))


### Features

* **seed:** social suport ([88485bc](https://gitlab.com/fi-sas/fisas-notifications/commit/88485bccdb517fdacb931017e4d0db2a3e6e3b2f))

# [1.8.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.7.0...v1.8.0) (2021-05-28)


### Features

* **seeds:** adicionar seeds alertas ms monitorização ([3864d9a](https://gitlab.com/fi-sas/fisas-notifications/commit/3864d9ab55da5895d8b1f5b2c218eda804f51722))

# [1.7.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.6.2...v1.7.0) (2021-05-27)


### Features

* **alert-types:** add simulation action ([6a26c82](https://gitlab.com/fi-sas/fisas-notifications/commit/6a26c82d05ad00fb8c6df264dfde24ad095bfd70))

## [1.6.2](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.6.1...v1.6.2) (2021-05-19)


### Bug Fixes

* **notification:** count filter unread message ([b6bee1c](https://gitlab.com/fi-sas/fisas-notifications/commit/b6bee1cc28e4f4f17c9bb0c8d3439092707f47c0))

## [1.6.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.6.0...v1.6.1) (2021-05-18)


### Bug Fixes

* **notifications:** remove authorization from list notifications ([fd42979](https://gitlab.com/fi-sas/fisas-notifications/commit/fd4297972b07f3bc132c4f180da0b9199b55ee70))

# [1.6.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.5.0...v1.6.0) (2021-05-14)


### Features

* **notitifcations:** add services and total unread to notifications ([ad1e43c](https://gitlab.com/fi-sas/fisas-notifications/commit/ad1e43c54c75deb6e6cb738a40292eba09c432ae))

# [1.5.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.4.0...v1.5.0) (2021-05-14)


### Features

* **notifications:** add unread field ([6b5f52c](https://gitlab.com/fi-sas/fisas-notifications/commit/6b5f52cc7f53706e7520eec1723d7af388898523))

# [1.4.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.3.0...v1.4.0) (2021-05-04)


### Bug Fixes

* **alerts:** remove unused alert_type_id ([787101b](https://gitlab.com/fi-sas/fisas-notifications/commit/787101b00f82e5830b95f4be4014aca1e421d489))
* **alerts:** remove unused code ([23b6026](https://gitlab.com/fi-sas/fisas-notifications/commit/23b6026dc36f3c3759a56bd7126f5b0b55a6450b))


### Features

* inital version of bulk alerts ([20111c1](https://gitlab.com/fi-sas/fisas-notifications/commit/20111c18123c3d3587bf55d8cbc6100ac43f919b))
* **alert:** inital create_bulk_alert skeleton ([c8b0079](https://gitlab.com/fi-sas/fisas-notifications/commit/c8b0079d41763b9f5f8e2fb286a2a7a133e790c8))
* **alert:** inital structure of create_bulk_alert ([4575769](https://gitlab.com/fi-sas/fisas-notifications/commit/4575769311f89eac84823141c52a6bbecbd45231))

# [1.3.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.2.1...v1.3.0) (2021-04-27)


### Features

* **seed:** add seed bolsa ([aac0a64](https://gitlab.com/fi-sas/fisas-notifications/commit/aac0a64695fe506efc8bbfda62029563f94397c8))
* **seeds:** add seed voluntariado ([2a85d44](https://gitlab.com/fi-sas/fisas-notifications/commit/2a85d44099a7d51c5f837881cc398f3b86c9fd57))

## [1.2.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.2.0...v1.2.1) (2021-04-26)


### Bug Fixes

* **gateways:** ifx recursive event creation ([dc2811f](https://gitlab.com/fi-sas/fisas-notifications/commit/dc2811f38a2884006651cefa397e6f928ada01aa))

# [1.2.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.1.2...v1.2.0) (2021-04-14)


### Bug Fixes

* **alert:** status update after notification changed ([3ddcab8](https://gitlab.com/fi-sas/fisas-notifications/commit/3ddcab870f7f3c6539727ba68024fe653943df83))


### Features

* **volunteering_seed:** add templates seeds ([be46a74](https://gitlab.com/fi-sas/fisas-notifications/commit/be46a74cb3d736344cbb91d7e16d1037df9b5300))

## [1.1.2](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.1.1...v1.1.2) (2021-03-25)


### Bug Fixes

* **notifications:** save user_id on create ([6f546ae](https://gitlab.com/fi-sas/fisas-notifications/commit/6f546ae2a2807ab83c7fc8d9899507390557b15b))

## [1.1.1](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.1.0...v1.1.1) (2021-03-25)


### Bug Fixes

* **notifications:** fix internal notification list ([f9ad689](https://gitlab.com/fi-sas/fisas-notifications/commit/f9ad6893b59bfa26f5a271edb4b343d60c2c92ec))

# [1.1.0](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0...v1.1.0) (2021-03-18)


### Features

* **seeds:** add seeds for notifications ms_scholarship ([916d6e7](https://gitlab.com/fi-sas/fisas-notifications/commit/916d6e7d39b3d18923141d1e187a3c55c68fe909))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **notifications:** remove scope from internal ([7a8136b](https://gitlab.com/fi-sas/fisas-notifications/commit/7a8136bd0285d4842cfcb616aa290be9f54c4b30))
* **seed:** fix some grammar errors ([f352422](https://gitlab.com/fi-sas/fisas-notifications/commit/f3524227847472dfe2ff6bf359d9ade32853794c))
* change volunteering service_id ([9c84c04](https://gitlab.com/fi-sas/fisas-notifications/commit/9c84c045f4486ba2cff58a6a97c233d2cb0cca97))
* fix internal value migration ([b0a00f5](https://gitlab.com/fi-sas/fisas-notifications/commit/b0a00f59bf71573e52870dfbfb6ed7a60b7ce248))
* initial retry method ([2827e83](https://gitlab.com/fi-sas/fisas-notifications/commit/2827e83aa0c2eee3bf0d1b89fd1348d6641f084e))
* **alert:** send notifications to various methods ([9cebd72](https://gitlab.com/fi-sas/fisas-notifications/commit/9cebd7297b00d8f43526e7781cff9606bc0d484f))
* **alert-type:** remove always_push_notification ([79f9a58](https://gitlab.com/fi-sas/fisas-notifications/commit/79f9a581dcf6a0231e5074826c1505c49dc9e47b))
* **alerts:** add user validation ([c562baf](https://gitlab.com/fi-sas/fisas-notifications/commit/c562bafa7168a0dbc84b2be6f709d8b9fdc3e2c4))
* **alerts:** medias is now optional ([697a089](https://gitlab.com/fi-sas/fisas-notifications/commit/697a08912add7b1acbe72b6bc13116e6ecda8f5b))
* **docker-compose:** create docker-compose to prod and dev ([41f3978](https://gitlab.com/fi-sas/fisas-notifications/commit/41f3978361fe407245cc85fc7abc190023ecdadf))
* **eamil_sender:** add from ([749e332](https://gitlab.com/fi-sas/fisas-notifications/commit/749e332d2b68f3dc5639f06adb325b2429e470a3))
* **gateways:** add queue redis url to env variables ([9edead0](https://gitlab.com/fi-sas/fisas-notifications/commit/9edead0eca4547947816391efd07a61289dd59db))
* **methods:** add internal value validation ([1c4f299](https://gitlab.com/fi-sas/fisas-notifications/commit/1c4f29940929a6186e9a999cda04b739cb45ae9b))
* **ms_core:** update ms_core package ([a9bc056](https://gitlab.com/fi-sas/fisas-notifications/commit/a9bc0565c9be1e9039d1d69c3740674d000c9e89))
* **notifications:** fix data and notification method ([9b0c24e](https://gitlab.com/fi-sas/fisas-notifications/commit/9b0c24e5dc9c27949376c9c96b808fe58f39a5b5))
* **seeds:** update the seeds ([84b4ff0](https://gitlab.com/fi-sas/fisas-notifications/commit/84b4ff0733abdce65c8ebe70a1daed2de474b9ff))
* fixing node version (node:12-buster-slim) ([4ca722c](https://gitlab.com/fi-sas/fisas-notifications/commit/4ca722cf18b2ab474e03dd29533359a6080c98bf))
* missing import @ alert_notification_method ([20201f8](https://gitlab.com/fi-sas/fisas-notifications/commit/20201f867cfa3c5df0fa896808b4e2a5f943a61c))
* **gitignore:** discard idea and vscode files ([938d076](https://gitlab.com/fi-sas/fisas-notifications/commit/938d07620a1b2703ac92cb030f7ec323a055b7a4))
* **migrations:** remove migration  not used ([a8d8c71](https://gitlab.com/fi-sas/fisas-notifications/commit/a8d8c71491996a70fc8feb40f8c150e675ac252e))
* **push_notification:** enable notification sound ([778f8fb](https://gitlab.com/fi-sas/fisas-notifications/commit/778f8fba38ea6cbe0d919a6ca6520c0b44d50791))
* **push-notification:** remove database url from config ([8faa5da](https://gitlab.com/fi-sas/fisas-notifications/commit/8faa5da979572a40d8f91939db83dc12803adc2c))
* removed sender option (returns error from gateway) ([6682e53](https://gitlab.com/fi-sas/fisas-notifications/commit/6682e53825826f62cbd8c09867dbc0c5bd7b4ed7))


### Features

* **alert:** add cc_payments_refmb alert ([3dd4ec3](https://gitlab.com/fi-sas/fisas-notifications/commit/3dd4ec3cb98236b3c9ebf88ad1c4fa0ee76cea02))
* **alert type:** add always push notification ([2253d44](https://gitlab.com/fi-sas/fisas-notifications/commit/2253d4447d090c262d6037ff92b82337c0230f35))
* **notifications:** add INTERNAL notificaition method ([bb63026](https://gitlab.com/fi-sas/fisas-notifications/commit/bb63026e554291be96542f508f519225ad8e794e))
* **push:** add initial code for push notifications ([90208e4](https://gitlab.com/fi-sas/fisas-notifications/commit/90208e4a0e22a07f590060717d8ab8f79db17bd4))
* **push-instance:** create table to push isntances ([d9826de](https://gitlab.com/fi-sas/fisas-notifications/commit/d9826def35cac7a6f0ddb128a528d619dda5d939))
* **seed:** add social_support alert-types ([d005108](https://gitlab.com/fi-sas/fisas-notifications/commit/d005108c40d1ebfc89f504f1e17d0582c4fb70ab))
* add external user creation notification type ([002398f](https://gitlab.com/fi-sas/fisas-notifications/commit/002398f0c8e6d5419e6b7ad6adb9dfc2b65740da))
* **push-instance-model:** create the model ([ba7fa60](https://gitlab.com/fi-sas/fisas-notifications/commit/ba7fa60d866d48bb35f0c96101d846609f7e0fa3))
* **push-notifications:** Add push notifications service ([d9ac93d](https://gitlab.com/fi-sas/fisas-notifications/commit/d9ac93d1d1beeb35f2b2a5e7224fde8bdd00a3b4))
* **seed:** add notification for listing approved ([90d7ecb](https://gitlab.com/fi-sas/fisas-notifications/commit/90d7ecb0e1613e7da2767c52debb30bc0fedd9ac))
* add volunteering alert-types to seed ([850f8a2](https://gitlab.com/fi-sas/fisas-notifications/commit/850f8a24a5f6a7a39cc61ec355141b15c5933719))
* **private_accomodation:** add template for owner application in pending ([850aaff](https://gitlab.com/fi-sas/fisas-notifications/commit/850aaffcdb037eb69696b1f629943d9e6d18f653))
* adding default relations fetching @ alert_notification_method ([633aec6](https://gitlab.com/fi-sas/fisas-notifications/commit/633aec6f6e3ad14a3f469da60ca60759b92a00af))
* migration into boilerplate v2 ([6c70e29](https://gitlab.com/fi-sas/fisas-notifications/commit/6c70e2977cbc56901d82084e2df0d9d0afd8ed11))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-03-03)


### Bug Fixes

* **notifications:** remove scope from internal ([7a8136b](https://gitlab.com/fi-sas/fisas-notifications/commit/7a8136bd0285d4842cfcb616aa290be9f54c4b30))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-26)


### Features

* **alert:** add cc_payments_refmb alert ([3dd4ec3](https://gitlab.com/fi-sas/fisas-notifications/commit/3dd4ec3cb98236b3c9ebf88ad1c4fa0ee76cea02))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-02)


### Bug Fixes

* **seed:** fix some grammar errors ([f352422](https://gitlab.com/fi-sas/fisas-notifications/commit/f3524227847472dfe2ff6bf359d9ade32853794c))


### Features

* **seed:** add social_support alert-types ([d005108](https://gitlab.com/fi-sas/fisas-notifications/commit/d005108c40d1ebfc89f504f1e17d0582c4fb70ab))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-01-29)


### Features

* add external user creation notification type ([002398f](https://gitlab.com/fi-sas/fisas-notifications/commit/002398f0c8e6d5419e6b7ad6adb9dfc2b65740da))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-01-29)


### Bug Fixes

* change volunteering service_id ([9c84c04](https://gitlab.com/fi-sas/fisas-notifications/commit/9c84c045f4486ba2cff58a6a97c233d2cb0cca97))


### Features

* add volunteering alert-types to seed ([850f8a2](https://gitlab.com/fi-sas/fisas-notifications/commit/850f8a24a5f6a7a39cc61ec355141b15c5933719))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-28)


### Bug Fixes

* initial retry method ([2827e83](https://gitlab.com/fi-sas/fisas-notifications/commit/2827e83aa0c2eee3bf0d1b89fd1348d6641f084e))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-25)


### Features

* **seed:** add notification for listing approved ([90d7ecb](https://gitlab.com/fi-sas/fisas-notifications/commit/90d7ecb0e1613e7da2767c52debb30bc0fedd9ac))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-19)


### Features

* **private_accomodation:** add template for owner application in pending ([850aaff](https://gitlab.com/fi-sas/fisas-notifications/commit/850aaffcdb037eb69696b1f629943d9e6d18f653))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-12)


### Bug Fixes

* **notifications:** fix data and notification method ([9b0c24e](https://gitlab.com/fi-sas/fisas-notifications/commit/9b0c24e5dc9c27949376c9c96b808fe58f39a5b5))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-12-31)


### Bug Fixes

* **alerts:** add user validation ([c562baf](https://gitlab.com/fi-sas/fisas-notifications/commit/c562bafa7168a0dbc84b2be6f709d8b9fdc3e2c4))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-31)


### Bug Fixes

* **methods:** add internal value validation ([1c4f299](https://gitlab.com/fi-sas/fisas-notifications/commit/1c4f29940929a6186e9a999cda04b739cb45ae9b))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-31)


### Bug Fixes

* fix internal value migration ([b0a00f5](https://gitlab.com/fi-sas/fisas-notifications/commit/b0a00f59bf71573e52870dfbfb6ed7a60b7ce248))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-notifications/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-30)


### Features

* **notifications:** add INTERNAL notificaition method ([bb63026](https://gitlab.com/fi-sas/fisas-notifications/commit/bb63026e554291be96542f508f519225ad8e794e))

# 1.0.0-rc.1 (2020-12-18)


### Bug Fixes

* **alert:** send notifications to various methods ([9cebd72](https://gitlab.com/fi-sas/fisas-notifications/commit/9cebd7297b00d8f43526e7781cff9606bc0d484f))
* **alert-type:** remove always_push_notification ([79f9a58](https://gitlab.com/fi-sas/fisas-notifications/commit/79f9a581dcf6a0231e5074826c1505c49dc9e47b))
* **alerts:** medias is now optional ([697a089](https://gitlab.com/fi-sas/fisas-notifications/commit/697a08912add7b1acbe72b6bc13116e6ecda8f5b))
* **docker-compose:** create docker-compose to prod and dev ([41f3978](https://gitlab.com/fi-sas/fisas-notifications/commit/41f3978361fe407245cc85fc7abc190023ecdadf))
* **eamil_sender:** add from ([749e332](https://gitlab.com/fi-sas/fisas-notifications/commit/749e332d2b68f3dc5639f06adb325b2429e470a3))
* **gateways:** add queue redis url to env variables ([9edead0](https://gitlab.com/fi-sas/fisas-notifications/commit/9edead0eca4547947816391efd07a61289dd59db))
* **gitignore:** discard idea and vscode files ([938d076](https://gitlab.com/fi-sas/fisas-notifications/commit/938d07620a1b2703ac92cb030f7ec323a055b7a4))
* **migrations:** remove migration  not used ([a8d8c71](https://gitlab.com/fi-sas/fisas-notifications/commit/a8d8c71491996a70fc8feb40f8c150e675ac252e))
* **ms_core:** update ms_core package ([a9bc056](https://gitlab.com/fi-sas/fisas-notifications/commit/a9bc0565c9be1e9039d1d69c3740674d000c9e89))
* **push_notification:** enable notification sound ([778f8fb](https://gitlab.com/fi-sas/fisas-notifications/commit/778f8fba38ea6cbe0d919a6ca6520c0b44d50791))
* **seeds:** update the seeds ([84b4ff0](https://gitlab.com/fi-sas/fisas-notifications/commit/84b4ff0733abdce65c8ebe70a1daed2de474b9ff))
* fixing node version (node:12-buster-slim) ([4ca722c](https://gitlab.com/fi-sas/fisas-notifications/commit/4ca722cf18b2ab474e03dd29533359a6080c98bf))
* missing import @ alert_notification_method ([20201f8](https://gitlab.com/fi-sas/fisas-notifications/commit/20201f867cfa3c5df0fa896808b4e2a5f943a61c))
* **push-notification:** remove database url from config ([8faa5da](https://gitlab.com/fi-sas/fisas-notifications/commit/8faa5da979572a40d8f91939db83dc12803adc2c))
* removed sender option (returns error from gateway) ([6682e53](https://gitlab.com/fi-sas/fisas-notifications/commit/6682e53825826f62cbd8c09867dbc0c5bd7b4ed7))


### Features

* adding default relations fetching @ alert_notification_method ([633aec6](https://gitlab.com/fi-sas/fisas-notifications/commit/633aec6f6e3ad14a3f469da60ca60759b92a00af))
* migration into boilerplate v2 ([6c70e29](https://gitlab.com/fi-sas/fisas-notifications/commit/6c70e2977cbc56901d82084e2df0d9d0afd8ed11))
* **alert type:** add always push notification ([2253d44](https://gitlab.com/fi-sas/fisas-notifications/commit/2253d4447d090c262d6037ff92b82337c0230f35))
* **push:** add initial code for push notifications ([90208e4](https://gitlab.com/fi-sas/fisas-notifications/commit/90208e4a0e22a07f590060717d8ab8f79db17bd4))
* **push-instance:** create table to push isntances ([d9826de](https://gitlab.com/fi-sas/fisas-notifications/commit/d9826def35cac7a6f0ddb128a528d619dda5d939))
* **push-instance-model:** create the model ([ba7fa60](https://gitlab.com/fi-sas/fisas-notifications/commit/ba7fa60d866d48bb35f0c96101d846609f7e0fa3))
* **push-notifications:** Add push notifications service ([d9ac93d](https://gitlab.com/fi-sas/fisas-notifications/commit/d9ac93d1d1beeb35f2b2a5e7224fde8bdd00a3b4))
