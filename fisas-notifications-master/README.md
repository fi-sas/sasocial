# FI@SAS MICROSERVICE Notification

This is the boilerplate from which FI@SAS microservices should be forked.
## Install Instructions

Dependency to install:

 - ncp - Asynchronous recursive file & directory copying:  `$ npm i ncp -g`


First time select the environment in which you want to setup a docker-compose.yml and .env will be generated.

Commands:

 - Development: `$ npm run docker-dev`

 - Production: `$ npm run docker-prod`

Other times just do: `$ docker-compose up` or `$ npm run docker-up`

Grab a cup of coffee, and once you see something like:

```
Node.js API server is listening on http://api:3000/
```

Your server is ready! 👍

**Note: http://api:3000 is the internal docker server, which is bridged to the 8081 port of your host machine - so, in order to test the API, you must hit http://localhost:8081/ in the browser!** You should be able to see a "Hello, World!" message 🎉

*All of this can be configured via the `.env` file in the root of the project.*

## Available commands

**NOTE: Any of these commands MUST be ran inside the docker container of your application!** To do so easily, you can use the following command on your host machine - `docker-compose exec api XXXXXX` - where `XXXXXX` is the command to be executed (e.g. `docker-compose exec api npm bd-migrate`).

`$ npm build` - Transpiles the source code, producing a "ready-to-serve" distribution version of the microservice.

`$ npm build-watch` - Builds the source code and keeps watching for changes, so that you can develop like a breeze.

`$ npm db-version` - Prints the version of the database software being used.

`$ npm db-migrate` - Executes the migrations on the database until the latest version.

`$ npm db-rollback` - Rollsback the latest batch of migrations ran.

`$ npm db-seed` - Seeds the database with example data.

`$ npm run test` - Runs the tests for the repo.

`$ npm run docker-up` - Up the docker-compose.

`$ npm run docker-bash` - Access the container terminal.

`$ npm run docker-logs` - Container logs.

`$ npm run docker-restart` - Restart the docker-compose.

`$ npm run docker-build` - Build a new images and up the containers.

## SWAGGER

OpenAPI specification is available through Swagger at `/api-docs` endpoint. When using the development environment, use [http://localhost:8081/api-docs](http://localhost:8081/api-docs).

## Features

* Fully dockerized 🐳 - the only required dependency in your local machine is [Docker](https://www.docker.com/).
* Uses [Express.js](https://expressjs.com/) to provide a standard API webserver.
* Currently supports relational database structures - [PostgreSQL](https://www.postgresql.org/) using [Bookshelf.js](http://bookshelfjs.org/) as an ORM and [Knex.js](https://knexjs.org/) for the database migrations and seeds.
* Supportes entity validations using [Fastest-Validator](https://github.com/icebob/fastest-validator) to perform blazing fast validations and sanitizations.
* "ES6-friendly" by using [Babel](https://babeljs.io/) to build the source code.

## WIP Features

* Code linting using ESLint
* Logging / auditing tools
* Monitoring tools
* Support for unit / integration testing
