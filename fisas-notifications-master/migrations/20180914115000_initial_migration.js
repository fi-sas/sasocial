module.exports.up = async (db) =>
	db.schema
		.createTable("alert_template", (table) => {
			table.increments();
			table.string("name", 120).notNullable();
			table.text("template").notNullable();
			table.text("data");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("alert_template_media", (table) => {
			table.increments();
			table.integer("alert_template_id").unsigned().references("id").inTable("alert_template");
			table.enum("type", ["IMAGE", "ATTACHMENT"], { useNative: true, enumName: 'alert_template_media_type' }).notNullable();
			table.string("key", 250).notNullable();
			table.integer("file_id");
		})
		.createTable("alert_type", (table) => {
			table.increments();
			table.integer("service_id").notNullable();
			table.integer("alert_template_id").unsigned().references("id").inTable("alert_template");
			table.string("key", 120).notNullable();
			table.string("name", 120).notNullable();
			table.string("description", 500);
			table.string("subject", 250).notNullable();
			table.text("message").notNullable();
			table.text("simplified_message");
			table.text("data");
			table.string("endpoint_url", 250);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("alert_type_media", (table) => {
			table.increments();
			table.integer("alert_type_id").unsigned().references("id").inTable("alert_type");
			table.enum("type", ["IMAGE", "ATTACHMENT"], { useNative: true, enumName: 'alert_type_media_type' }).notNullable();
			table.string("key", 250).notNullable();
			table.integer("file_id");
		})
		.createTable("notification_method", (table) => {
			table.increments();
			table.string("name", 120).notNullable();
			table.enum("service", [
				"INTERNAL",
				"EMAIL",
				"SMS",
				"PUSH_NOTIFICATIONS",
				"FACEBOOK_MESSENGER",
				"WHATSAPP",
			], { useNative: true, enumName: 'notification_method_service' });
			table.string("description", 250);
			table.text("configs").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("alert_notification_method", (table) => {
			table.increments();
			table.integer("alert_type_id").unsigned().references("id").inTable("alert_type");
			table
				.integer("notification_method_id")
				.unsigned()
				.references("id")
				.inTable("notification_method");
			table.integer("priority").unsigned();
		})
		.createTable("alert", (table) => {
			table.increments();
			table.integer("alert_type_id").unsigned().references("id").inTable("alert_type");
			table.integer("user_id").nullable();
			table.text("data").notNullable();
			table.enum("status", ["NEW", "PENDING", "NOTIFIED", "READ", "ERROR"]).notNullable();
			table.text("variables");
			table.text("user_data");
			table.string("external_uuid", 36);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("alert_media", (table) => {
			table.increments();
			table.integer("alert_id").unsigned().references("id").inTable("alert");
			table.enum("type", ["IMAGE", "ATTACHMENT"], { useNative: true, enumName: 'alert_media_type' }).notNullable();
			table.string("key", 250).notNullable();
			table.integer("file_id");
		})
		.createTable("notification", (table) => {
			table.increments();
			table.integer("alert_id").unsigned().references("id").inTable("alert");
			table
				.integer("notification_method_id")
				.unsigned()
				.references("id")
				.inTable("notification_method");
			table.string("subject", 250);
			table.text("message").notNullable();
			table.enum("status", ["SUCCESS", "PENDING", "ERROR"], { useNative: true, enumName: 'notification_status' }).notNullable();
			table.string("error_message", 250);
			table.integer("trial").defaultTo(0);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("alert_history", (table) => {
			table.increments();
			table.integer("alert_id").unsigned().references("id").inTable("alert");
			table.string("status").notNullable();
			table.string("notes");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})
		.createTable("alert_reply", (table) => {
			table.increments();
			table.integer("alert_id").unsigned().references("id").inTable("alert").notNullable();
			table.text("reply_text").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("alert_template_media")
		.dropTable("alert_template")
		.dropTable("notification")
		.dropTable("alert_media")
		.dropTable("alert")
		.dropTable("alert_notification_method")
		.dropTable("notification_method")
		.dropTable("alert_type_media")
		.dropTable("alert_type")
		.dropTable("alert_history")
		.dropTable("alert_reply");

module.exports.configuration = {
	transaction: true,
};
