exports.up = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.integer("user_id").nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.dropColumn("user_id");
	});
};
