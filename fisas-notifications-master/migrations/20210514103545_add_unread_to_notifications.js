exports.up = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.bool("unread").notNullable().defaultTo(true);
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.dropColumn("unread");
	});
};
