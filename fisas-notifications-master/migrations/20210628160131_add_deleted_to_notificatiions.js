exports.up = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.bool("deleted").notNullable().defaultTo(false);
		table.datetime("deleted_at").nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("notification", (table) => {
		table.dropColumn("deleted");
		table.dropColumn("deleted_at");
	});
};
