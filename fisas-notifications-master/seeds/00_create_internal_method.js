module.exports.seed = async (knex) =>
	knex("notification_method")
		.select()
		.where("service", "INTERNAL")
		.then((result) => {
			if (result.length === 0) {
				return knex("notification_method").insert([
					{
						name: "Internal Notification Method",
						service: "INTERNAL",
						description:
							"This notification method is used for webpage and backoffice UI",
						configs:
							"{}",
						created_at: new Date(),
						updated_at: new Date(),
					},
				]);
			}
			return result;
		});
