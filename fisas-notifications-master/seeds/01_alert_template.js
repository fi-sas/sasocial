exports.seed = (knex, Promise) => {
	return knex("alert_template")
		.select()
		.where("id", 1)
		.then(async (rows) => {
			if (rows.length === 0) {
				// no matching records found
				// Insert alert type and example related alert template
				const template = await knex("alert_template").insert(
					{
						id: 1,
						name: "Generico",
						template: `<!DOCTYPE html
						PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html>
					
					<head>
						<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
						<meta name="viewport" content="width=device-width" />
						<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
						<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
						<title>SASocial Email</title>
						<style gf-font-style="'Montserrat script=all rev=1':300">
							@font-face {
								font-family: 'Montserrat script=all rev=1';
								font-style: normal;
								font-weight: 300;
								src: url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gTD_u50.woff2) format('woff2');
								unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
							}
					
							/* cyrillic */
							@font-face {
								font-family: 'Montserrat script=all rev=1';
								font-style: normal;
								font-weight: 300;
								src: url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3g3D_u50.woff2) format('woff2');
								unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
							}
					
							/* vietnamese */
							@font-face {
								font-family: 'Montserrat script=all rev=1';
								font-style: normal;
								font-weight: 300;
								src: url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gbD_u50.woff2) format('woff2');
								unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
							}
					
							/* latin-ext */
							@font-face {
								font-family: 'Montserrat script=all rev=1';
								font-style: normal;
								font-weight: 300;
								src: url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gfD_u50.woff2) format('woff2');
								unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
							}
					
							/* latin */
							@font-face {
								font-family: 'Montserrat script=all rev=1';
								font-style: normal;
								font-weight: 300;
								src: url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gnD_g.woff2) format('woff2');
								unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
							}
					
							/* cyrillic-ext */
						</style>
						<style>
							/* -------------------------------------
													  GLOBAL RESETS
												  ------------------------------------- */
					
							/*All the styling goes here*/
					
							img {
								border: none;
								-ms-interpolation-mode: bicubic;
								max-width: 100%;
							}
					
							body {
								background-color: #f3f3f3;
								font-family: "Montserrat", sans-serif;
								-webkit-font-smoothing: antialiased;
								font-size: 14px;
								line-height: 1.4;
								margin: 0;
								padding: 0;
								padding-top: 60px;
								padding-bottom: 60px;
								-ms-text-size-adjust: 100%;
								-webkit-text-size-adjust: 100%;
					
							}
					
							table {
								border-collapse: separate;
								mso-table-lspace: 0pt;
								mso-table-rspace: 0pt;
								width: 100%;
					
							}
					
							table td {
								font-family: sans-serif;
								font-size: 14px;
								vertical-align: top;
							}
					
							/* -------------------------------------
													  BODY & CONTAINER
												  ------------------------------------- */
							.body {
								background-color: #f3f3f3;
								width: 100%;
							}
					
							/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
							.container {
								display: block;
								/*margin: 0 auto !important; */
								margin: auto;
								/* makes it centered */
								max-width: 800px;
								padding: 10px;
								width: 100%;
							}
					
							/* This should also be a block element, so that it will fill 100% of the .container */
							.content {
								box-sizing: border-box;
								display: block;
								margin: 0 auto;
								max-width: 800px;
								padding: 10px;
							}
					
							/* -------------------------------------
													  HEADER, FOOTER, MAIN
												  ------------------------------------- */
							.main {
								background: #ffffff;
								border-radius: 3px;
								width: 100%;
								padding: 40px;
							}
					
							.wrapper {
								box-sizing: border-box;
								padding: 20px;
							}
					
							.content-block {
								padding-bottom: 10px;
								padding-top: 10px;
							}
					
							.footer {
								clear: both;
								margin-top: 10px;
								text-align: center;
								width: 100%;
							}
					
							.footer td,
							.footer p,
							.footer span,
							.footer a {
								color: #999999;
								font-size: 12px;
								text-align: center;
							}
					
							/* -------------------------------------
													  TYPOGRAPHY
												  ------------------------------------- */
							h1,
							h2,
							h3,
							h4 {
								color: #000000;
								font-family: sans-serif;
								font-weight: 400;
								line-height: 1.4;
								margin: 0;
								margin-bottom: 30px;
							}
					
							h1 {
								font-size: 35px;
								font-weight: 300;
								text-align: center;
								text-transform: capitalize;
							}
					
							p,
							ul,
							ol {
								font-family: sans-serif;
								font-size: 14px;
								font-weight: normal;
								margin: 0;
								margin-bottom: 15px;
							}
					
							p li,
							ul li,
							ol li {
								list-style-position: inside;
								margin-left: 5px;
							}
					
							a {
								color: #3498db;
								text-decoration: underline;
							}
					
							/* -------------------------------------
													  BUTTONS
												  ------------------------------------- */
							.btn {
								box-sizing: border-box;
								width: 100%;
								padding-top: 20px;
							}
					
							.btn>tbody>tr>td {
								padding-bottom: 15px;
							}
					
							.btn table {
								width: auto;
							}
					
							.btn table td {
								background-color: #107E90;
								border-radius: 25px;
								text-align: center;
							}
					
							.btn a {
								background-color: #107E90;
								border: solid 1px #107E90;
								border-radius: 25px;
								box-sizing: border-box;
								color: #3498db;
								cursor: pointer;
								display: inline-block;
								font-size: 20px;
								font-weight: bold;
								margin: 0;
								padding: 12px 25px;
								text-decoration: none;
								text-transform: capitalize;
							}
					
							.btn-primary table td {
								background-color: #3498db;
							}
					
							.btn-primary a {
								background-color: #107E90;
								border-color: #107E90;
								color: #ffffff;
							}
					
							/* -------------------------------------
													  OTHER STYLES THAT MIGHT BE USEFUL
												  ------------------------------------- */
							.last {
								margin-bottom: 0;
							}
					
							.first {
								margin-top: 0;
							}
					
							.align-center {
								text-align: center;
							}
					
							.align-right {
								text-align: right;
							}
					
							.align-left {
								text-align: left;
							}
					
							.clear {
								clear: both;
							}
					
							.mt0 {
								margin-top: 0;
							}
					
							.mb0 {
								margin-bottom: 0;
							}
					
							.preheader {
								color: transparent;
								display: none;
								height: 0;
								max-height: 0;
								max-width: 0;
								opacity: 0;
								overflow: hidden;
								mso-hide: all;
								visibility: hidden;
								width: 0;
							}
					
							.powered-by a {
								text-decoration: none;
							}
					
							hr {
								border: 0;
								border-bottom: 1px solid #f6f6f6;
								margin: 20px 0;
							}
					
							/* -------------------------------------
													  RESPONSIVE AND MOBILE FRIENDLY STYLES
												  ------------------------------------- */
							@media only screen and (max-width: 620px) {
								table[class=body] h1 {
									font-size: 28px !important;
									margin-bottom: 10px !important;
								}
					
								table[class=body] p,
								table[class=body] ul,
								table[class=body] ol,
								table[class=body] td,
								table[class=body] span,
								table[class=body] a {
									font-size: 20px !important;
								}
					
								table[class=body] .wrapper,
								table[class=body] .article {
									padding: 40px !important;
								}
					
								table[class=body] .content {
									padding: 0 !important;
								}
					
								table[class=body] .container {
									padding: 0 !important;
									width: 100% !important;
								}
					
								table[class=body] .main {
									border-left-width: 0 !important;
									border-radius: 0 !important;
									border-right-width: 0 !important;
								}
					
								table[class=body] .btn table {
									/*width: 100% !important; */
								}
					
								table[class=body] .btn a {
									/*width: 100% !important; */
								}
					
								table[class=body] .img-responsive {
									height: auto !important;
									max-width: 100% !important;
									width: auto !important;
								}
							}
					
							/* -------------------------------------
													  PRESERVE THESE STYLES IN THE HEAD
												  ------------------------------------- */
							@media all {
								.ExternalClass {
									width: 100%;
								}
					
								.ExternalClass,
								.ExternalClass p,
								.ExternalClass span,
								.ExternalClass font,
								.ExternalClass td,
								.ExternalClass div {
									line-height: 100%;
								}
					
								.apple-link a {
									color: inherit !important;
									font-family: inherit !important;
									font-size: inherit !important;
									font-weight: inherit !important;
									line-height: inherit !important;
									text-decoration: none !important;
								}
					
								#MessageViewBody a {
									color: inherit;
									text-decoration: none;
									font-size: inherit;
									font-family: inherit;
									font-weight: inherit;
									line-height: inherit;
								}
					
								.btn-primary table td:hover {
									background-color: #054e61 !important;
								}
					
								.btn-primary a:hover {
									background-color: #054e61 !important;
									border-color: #054e61 !important;
								}
					
								.header {
									padding-bottom: 60px;
								}
					
								.message {
									font-weight: bold;
									font-size: 21px;
									line-height: 24px;
								}
					
								.submessage {
									font-family: "Montserrat script=all rev=1", "Adobe Blank";
									font-weight: 300;
									font-style: normal;
									font-size: 14px;
									line-height: 24px;
								}
					
								.support_message {
									font-size: 13px;
									line-height: 18px;
								}
					
								.email_link {
									color: black;
									font-weight: bold;
								}
							}
						</style>
					</head>
					
					<body>
						<span class="preheader">{{simplified_message}}</span>
						<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
							<tr>
								<td>&nbsp;</td>
								<td class="container">
									<div class="content">
										​
										<!-- START CENTERED WHITE CONTAINER -->
										<table role="presentation" class="main">
											​
											<!-- START MAIN CONTENT AREA -->
											<tr>
												<td class="wrapper">
													<table role="presentation" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td class="header">
																<img class="img-responsive" width="155" src="{{medias.image.logo.url}}" />
															</td>
														</tr>
														<tr>
															<td>
																{{{message}}}
															</td>
														</tr>
													</table>
												</td>
											</tr>
											​
											<!-- END MAIN CONTENT AREA -->
										</table>
										<!-- END CENTERED WHITE CONTAINER -->
										​
									</div>
								</td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</body>
					
					</html>`,
						data: `{ "sasocial_link": "http://localhost"}`,
						updated_at: new Date(),
						created_at: new Date(),
					},
				);

				return knex("alert_template_media").insert(
					{
						alert_template_id : 1,
						type : "IMAGE",
						key : "logo",
						file_id : 200
					},
				);
			}
			return true;
		})
		.catch(() => {
			// you can find errors here.
		});

};
