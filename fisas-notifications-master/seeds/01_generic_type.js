const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {

	return Promise.all([
		createIfNotExistAlert(
			knex,
			"GENERIC",
			{
				service_id: 19,
				alert_template_id: 1,
				name: "Generico",
				description:
					"Tipo de alerta Generico para uso do envio de newsletter e afins",
				subject: "{{{subject}}}",
				message: "{{{message}}}",
				simplified_message: "{{{simplified_message}}}",
				data: "{}",
			},
		),
	]);
};
