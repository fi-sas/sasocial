const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura submetida",
				description: "Envia alerta que a candidatura foi submetida com sucesso.",
				subject: "Candidatura submetida com sucesso",
				message: `<br><p class="message"><br>Caro(a) {{application.full_name}}, a sua candidatura a alojamento para a residência do(a) {{application.residence.name}} foi submetida com sucesso.</p>
				<p class="submessage">
					Todas as alterações ao estado do processo ser-lhe-ão comunicadas através do seu e-mail institucional e ficam registadas para consulta na plataforma SASocial.
				</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						  <tbody>
							<tr>
							  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
							</tr>
							<tr>
							<td>

							</td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </tbody>
				</table>`,
				simplified_message: "{{application.full_name}}, a sua candidatura a alojamento {{application.residence.name}} foi submetida com sucesso.",
				data: "{}",
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura cancelada",
				description: "Envia alerta que a candidatura foi cancelada com sucesso.",
				subject: "Candidatura cancelada com sucesso",
				message: `<p class="message"><br>Caro(a) {{application.full_name}}, conforme solicitado a sua candidatura a alojamento para a residência do(a) {{application.residence.name}} foi cancelada.</p>
				<p class="submessage">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p> <p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
							<td>

							</td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.residence.name}} foi cancelada.",
				data: "{}",
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_ANALYSED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura em análise",
				description: "Envia alerta que a candidatura encontra se em análise.",
				subject: "Candidatura em análise",
				message: `<p class="message"><br>Caro(a) {{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.residence.name}} encontra-se em análise.</p>
				<p class="submessage">
					O resultado da análise ser-lhe-à comunicado através do seu e-mail institucional.
				</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p> <p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.residence.name}} encontra-se em análise. ",
				data: "{}",
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_PENDING",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura em despacho",
				description: "Envia alerta que a candidatura encontra-se em despacho.",
				subject: "Candidatura pendente de decisão",
				message: `<p class="message"><br>Caro(a) {{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.residence.name}} encontra-se em fase de despacho pela Administração dos SAS-SASocial.</p>
				<p class="submessage">
					O resultado do despacho ser-lhe-à comunicado através do seu e-mail institucional.
				</p>
			   <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.residence.name}} encontra-se em fase de despacho pela Administração dos SAS-SASocial.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_UNASSIGNED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura não atribuida",
				description: "Envia alerta que a candidatura não foi colocada.",
				subject: "Candidatura submetida não colocada",
				message: `<p class="message"><br>Caro(a) {{application.full_name}},  informamos que a sua candidatura a alojamento na residência do(a) {{application.residence.name}} foi indeferida.<br>Mais se informa que dispõe de 10 dias para apresentar oposição à decisão.</p>

				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, informamos que a sua candidatura a alojamento na residência do(a) {{application.residence.name}} foi indeferida. Para mais informações contacte a Área de Alojamento dos SAS-SASocial.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_QUEUED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura em lista de espera",
				description: "Envia alerta que a candidatura foi para lista de espera.",
				subject: "Candidatura em lista de espera",
				message: `<p class="message"><br>
				Caro(a) {{application.full_name}}, informamos que a sua colocação na residência do(a) {{application.assignedResidence.name}} se encontra em lista de espera.</p>
				<p class="submessage">Caso surja alguma vaga que permita a sua colocação será contactado pela Área de Alojamento dos SAS-SASocial.
				</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, a sua colocação na residência do(a) {{application.assignedResidence.name}}  encontra-se em lista de espera. Para mais informações contacte a Área de Alojamento dos SAS-SASocial.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_ASSIGNED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura atribuida",
				description: "Envia alerta que a candidatura foi colocada.",
				subject: "Candidatura colocada",
				message: `<p class="message"><br>
				Caro(a) {{application.full_name}}, na sequência da sua candidatura a alojamento, informamos que foi colocado(a) na residência do(a) {{application.assignedResidence.name}}.</p>
				<p class="submessage">
					 Importante: De acordo com o Regulamento Interno das Residências, caso não pretenda usufruir deste benefício, dispõe  de 5 dias para anular a sua candidatura, findo este prazo, caso não se pronuncie, será automaticamente considerado colocado ficando sujeito ao pagamento de 10 mensalidades de alojamento.
				</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, foi colocado na residência do(a) {{application.assignedResidence.name}}. De acordo com o Regulamento dispõe de 5 dias para desistir, caso não se prenuncie é considerado colocado ficando sujeito ao pagamento de 10 mensalidades.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_CONFIRMED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura confirmada",
				description: "Envia alerta que a candidatura foi confirmada com sucesso.",
				subject: "Candidatura confirmada com sucesso",
				message: ` <p class="message"><br>Caro(a) {{application.full_name}}, obrigado por confirmar a sua colocação na residência do(a) {{application.assignedResidence.name}}.</p>
				<p class="submessage">
					Para dar inicio ao processo de admissão deverá dirigir-se presencialmente à residência onde foi colocado(a) dentro do seguinte horário: dias úteis das 09.30h às 12:00h e das 14:00h às 16:00h.</p>
			   <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}}, obrigado por confirmar a sua colocação na residência do(a) {{application.assignedResidence.name}}. Deverá dirigir-se à residência para dar inicio ao processo de admissão.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura não aceite",
				description: "Envia alerta que a não aceitação da colocação foi registada com sucesso.",
				subject: "Candidatura rejeitada com sucesso",
				message: `<p class="message"><br>Caro(a){{application.full_name}}, a sua informação de não aceitação da colocação na residência do(a) {{application.assignedResidence.name}} foi registada com sucesso.</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
										Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
										<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
										  <tbody>
											<tr>
											  <td align="left">
												<table role="presentation" border="0" cellpadding="0" cellspacing="0">
												<tbody>
												  <tr>
													<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
												  </tr>
												  <tr>
												  <td>

												  </td>
												  </tr>
												</tbody>
									  </table>`,
				simplified_message: "{{application.full_name}}, a sua informação de não aceitação da colocação na residência do(a) {{application.assignedResidence.name}} foi registada com sucesso.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_CONTRACTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura em contrato",
				description: "Envia alerta que a candidatura foi contratada.",
				subject: "Candidatura contratada",
				message: ` <p class="message"><br>Caro(a), {{application.full_name}} a sua colocação na residência do(a) {{application.assignedResidence.name}} já se encontra com um contrato associado.</p>
				<p class="submessage">
					Consulte os documentos na plataforma SASocial.
				</p>
				<p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a></p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						  <tr>
							<td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						  </tr>
						  <tr>
						  <td>

						  </td>
						  </tr>
						</tbody>
			  </table>`,
				simplified_message: "{{application.full_name}} a sua colocação na residência do(a) {{application.assignedResidence.name}} já se encontra com um contrato associado. Consulta os documentos na plataforma SASocial.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_REOPENED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura reaberta",
				description: "Envia alerta que a candidatura foi reaberta.",
				subject: "Candidatura reaberta",
				message: ` <p class="message"><br>Caro(a){{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.assignedResidence.name}} foi reaberta.</p>
				<p class="submessage">
					O resultado da análise ser-lhe-à comunicado através do seu e-mail institucional.
				</p>
				<p class="support_message">Para mais informações contacta a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
				Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				  <tbody>
					<tr>
					  <td align="left">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
						  <tbody>
							<tr>
							  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </tbody>
				</table>`,
				simplified_message: "{{application.full_name}}, a sua candidatura a alojamento na residência do(a) {{application.assignedResidence.name}} foi reaberta.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_CLOSED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Candidatura fechada",
				description: "Candidatura fechada",
				subject: "Candidatura fechada",
				message: ` <p class="message"><br>
				Caro(a) {{application.full_name}},  o seu contrato de alojamento na residência do(a) {{application.assignedResidence.name}} terminou.</p>
			  <p class="submessage">Deve verificar junto da Área de Alojamento dos SAS-SASocial se concluiu todos os procedimentos inerentes ao término do contrato.<br>
			  Agradecemos  a escolha de uma residência dos SAS-SASocial para a sua estadia, esperamos voltar a vê-lo em breve.</p>
			   <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>. </p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
						<tr>
						<td>

						</td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},  o seu contrato de alojamento na residência do(a) {{application.assignedResidence.name}} terminou. Deve verificar junto da Área de Alojamento dos SAS-SASocial se concluiu todos os procedimentos inerentes ao término do contrato.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_STATUS_OPPOSITION",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: cCandidatura em oposição",
				description: "Envia alerta que a candidatura entrou em estado de oposição",
				subject: "Candidatura em oposição",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a oposição ao resultado da sua candidatura a alojamento para a residência do(a) {{application.residence.name}} foi submetida com sucesso.</p>
			 <p class="submessage">
				 O resultado da oposição ser-lhe-à comunicado através do seu e-mail institucional e fica registado para consulta na plataforma SASocial.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a oposição ao resultado da sua candidatura a alojamento para a residência do(a) {{application.assignedResidence.name}} foi submetida com sucesso. O resultado da oposição ser-lhe-à comunicado através do seu e-mail institucional e fica registado para consulta na plataforma SASocial.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime submetida",
				description: "Envia alerta que a alteração de regime foi submetido",
				subject: "Alteração de regime submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime em análise",
				description: "Envia alerta que a alteração de regime está em análise",
				subject: "Alteração de regime em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime em despacho",
				description: "Envia alerta que a alteração de regime está  em despacho",
				subject: "Alteração de regime em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime aprovada",
				description: "Envia alerta que a alteração de regime foi aprovado",
				subject: "Alteração de regime aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime foi aprovado.</p>
			 <p class="submessage">
				O seu regime será alterado e faturado a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime rejeitada",
				description: "Envia alerta que a alteração de regime foi rejeitada",
				subject: "Alteração de regime rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime foi rejeitada.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi rejeitada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_REGIME_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime cancelada",
				description: "Envia alerta que a alteração de regime foi cancelada",
				subject: "Alteração de regime cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de regime foi cancelada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi cancelada.",
				data: '{}',
			},
		),



		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de extras submetida",
				description: "Envia alerta que a alteração de extras foi submetida",
				subject: "Alteração de extras submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de extras foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de extras em análise",
				description: "Envia alerta que a alteração de extras está em análise",
				subject: "Alteração de extras em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de extras está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de extras em despacho",
				description: "Envia alerta que a alteração de extras está  em despacho",
				subject: "Alteração de extras em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de extras está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime aprovada",
				description: "Envia alerta que a alteração de regime foi aprovado",
				subject: "Alteração de regime aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras foi aprovado.</p>
			 <p class="submessage">
				Os seus extras serão alterados e faturados a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de extras foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de extras rejeitada",
				description: "Envia alerta que a alteração de extras foi rejeitada",
				subject: "Alteração de extras rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de extras foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXTRAS_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de extras cancelada",
				description: "Envia alerta que a alteração de extras foi cancelada",
				subject: "Alteração de extras cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de extras foi cancelado.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi cancelado.",
				data: '{}',
			},
		),





		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência submetida",
				description: "Envia alerta que a comunicação de ausência foi submetida",
				subject: "Comunicação de ausência submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência foi submetida.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência foi submetida.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência em análise",
				description: "Envia alerta que a comunicação de ausência está em análise",
				subject: "Comunicação de ausência  está em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência está em análise".</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência está em análise.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência em despacho",
				description: "Envia alerta que a comunicação de ausência está em despacho",
				subject: "Comunicação de ausência  está em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência está em despacho".</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência está em despacho.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_APPROVED_WITH_SUSPENSION",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência foi aprovada com suspensão de pagamento",
				description: "Envia alerta que a comunicação de ausência foi aprovada com suspensão de pagamento",
				subject: "Comunicação de ausência foi aprovada com suspensão de pagamento",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência foi aprovada com suspensão de pagamento".</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência foi aprovada com suspensão de pagamento.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_APPROVED_WITHOUT_SUSPENSION",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência foi aprovada sem suspensão de pagamento",
				description: "Envia alerta que a comunicação de ausência foi aprovada sem suspensão de pagamento",
				subject: "Comunicação de ausência foi aprovada sem suspensão de pagamento",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência foi aprovada sem suspensão de pagamento".</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência foi aprovada sem suspensão de pagamento.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_ABSENCES_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de ausência foi cancelada",
				description: "Envia alerta que a comunicação de ausência foi cancelada",
				subject: "Comunicação de ausência foi cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, a sua comunicação de ausência foi cancelada".</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}}, a sua comunicação de ausência foi cancelada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia submetida",
				description: "Envia alerta que a alteração de tipologia foi submetido",
				subject: "Alteração de tipologia submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia em análise",
				description: "Envia alerta que a alteração de tipologia está em análise",
				subject: "Alteração de tipologia em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia em despacho",
				description: "Envia alerta que a alteração de tipologia está  em despacho",
				subject: "Alteração de tipologia em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia aprovada",
				description: "Envia alerta que a alteração de tipologia foi aprovado",
				subject: "Alteração de tipologia aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia foi aprovado.</p>
			 <p class="submessage">
				O seu tipologia será alterado e faturado a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia foi aprovado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia rejeitada",
				description: "Envia alerta que a alteração de tipologia foi rejeitada",
				subject: "Alteração de tipologia rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia foi rejeitada.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia foi rejeitada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TYPOLOGY_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de tipologia cancelada",
				description: "Envia alerta que a alteração de tipologia foi cancelada",
				subject: "Alteração de tipologia cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de tipologia foi cancelada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de tipologia foi cancelada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência submetida",
				description: "Envia alerta que a alteração de residência foi submetido",
				subject: "Alteração de residência submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência em análise",
				description: "Envia alerta que a alteração de residência está em análise",
				subject: "Alteração de residência em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência em despacho",
				description: "Envia alerta que a alteração de residência está  em despacho",
				subject: "Alteração de residência em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência aprovada",
				description: "Envia alerta que a alteração de residência foi aprovado",
				subject: "Alteração de residência aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência foi aprovado.</p>
			 <p class="submessage">
				O seu residência será alterado e faturado a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência foi aprovado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência rejeitada",
				description: "Envia alerta que a alteração de residência foi rejeitada",
				subject: "Alteração de residência rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência foi rejeitada.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência foi rejeitada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_RESIDENCE_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de residência cancelada",
				description: "Envia alerta que a alteração de residência foi cancelada",
				subject: "Alteração de residência cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de residência foi cancelada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de residência foi cancelada.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto submetida",
				description: "Envia alerta que a permuta de quarto foi submetido",
				subject: "Permuta de quarto submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto em análise",
				description: "Envia alerta que a permuta de quarto está em análise",
				subject: "Permuta de quarto em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto em despacho",
				description: "Envia alerta que a permuta de quarto está  em despacho",
				subject: "Permuta de quarto em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto aprovada",
				description: "Envia alerta que a permuta de quarto foi aprovado",
				subject: "Permuta de quarto aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto foi aprovado.</p>
			 <p class="submessage">
				O seu residência será alterado e faturado a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto foi aprovado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto rejeitada",
				description: "Envia alerta que a permuta de quarto foi rejeitada",
				subject: "Permuta de quarto rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto foi rejeitada.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto foi rejeitada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERMUTE_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Permuta de quarto cancelada",
				description: "Envia alerta que a permuta de quarto foi cancelada",
				subject: "Permuta de quarto cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de permuta de quarto foi cancelada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de permuta de quarto foi cancelada.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_MAINTENANCE_REQUEST_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de manutenção submetido",
				description: "Envia alerta que a pedido de manutenção foi submetido",
				subject: "Pedido de manutenção submetido",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de manutenção foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de manutenção foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_MAINTENANCE_REQUEST_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de manutenção cancelado",
				description: "Envia alerta que a pedido de manutenção foi cancelado",
				subject: "Pedido de manutenção cancelado",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de manutenção foi cancelado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de manutenção foi cancelado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_MAINTENANCE_REQUEST_RESOLVING",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de manutenção em resolução",
				description: "Envia alerta que a pedido de manutenção está em resolução",
				subject: "Pedido de manutenção em resolução",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de manutenção está em resolução.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de manutenção está em resolução.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_MAINTENANCE_REQUEST_RESOLVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de manutenção rejeitado",
				description: "Envia alerta que a pedido de manutenção foi rejeitado",
				subject: "Pedido de manutenção rejeitado",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de manutenção foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de manutenção foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_MAINTENANCE_REQUEST_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de manutenção rejeitado",
				description: "Envia alerta que a pedido de manutenção foi rejeitado",
				subject: "Pedido de manutenção rejeitado",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de manutenção foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de manutenção foi rejeitado.",
				data: '{}',
			},
		),



		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Extensão de contrato submetida",
				description: "Envia alerta que a Extensão de contrato foi submetida",
				subject: "Extensão de contrato submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de Extensão de contrato foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Extensão de contrato em análise",
				description: "Envia alerta que a Extensão de contrato está em análise",
				subject: "Extensão de contrato em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de Extensão de contrato está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Extensão de contrato em despacho",
				description: "Envia alerta que a Extensão de contrato está  em despacho",
				subject: "Extensão de contrato em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de Extensão de contrato está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime aprovada",
				description: "Envia alerta que a alteração de regime foi aprovado",
				subject: "Alteração de regime aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato foi aprovado.</p>
			 <p class="submessage">
				Os seus extras serão alterados e faturados a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de Extensão de contrato foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Extensão de contrato rejeitada",
				description: "Envia alerta que a Extensão de contrato foi rejeitada",
				subject: "Extensão de contrato rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de Extensão de contrato foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_EXTENSION_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Extensão de contrato cancelada",
				description: "Envia alerta que a Extensão de contrato foi cancelada",
				subject: "Extensão de contrato cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de Extensão de contrato foi cancelado.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi cancelado.",
				data: '{}',
			},
		),




		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Desistência submetida",
				description: "Envia alerta que a desistência foi submetida",
				subject: "Desistência submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de desistência foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Desistência em análise",
				description: "Envia alerta que a desistência está em análise",
				subject: "Desistência em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de desistência está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Desistência em despacho",
				description: "Envia alerta que a desistência está  em despacho",
				subject: "Desistência em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de desistência está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime aprovada",
				description: "Envia alerta que a alteração de regime foi aprovado",
				subject: "Alteração de regime aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência foi aprovado.</p>
			 <p class="submessage">
				Os seus extras serão alterados e faturados a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de desistência foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Desistência rejeitada",
				description: "Envia alerta que a desistência foi rejeitada",
				subject: "Desistência rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de desistência foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_WITHDRAWAL_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Desistência cancelada",
				description: "Envia alerta que a desistência foi cancelada",
				subject: "Desistência cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de desistência foi cancelado.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi cancelado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de tarifário submetida",
				description: "Envia alerta que a pedido de alteração de tarifário foi submetida",
				subject: "Pedido de alteração de tarifário submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de alteração de tarifário foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de tarifário em análise",
				description: "Envia alerta que a pedido de alteração de tarifário está em análise",
				subject: "Pedido de alteração de tarifário em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de alteração de tarifário está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de tarifário em despacho",
				description: "Envia alerta que a pedido de alteração de tarifário está  em despacho",
				subject: "Pedido de alteração de tarifário em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de alteração de tarifário está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de regime aprovada",
				description: "Envia alerta que a alteração de regime foi aprovado",
				subject: "Alteração de regime aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário foi aprovado.</p>
			 <p class="submessage">
				Os seus extras serão alterados e faturados a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de alteração de tarifário foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de tarifário rejeitada",
				description: "Envia alerta que a pedido de alteração de tarifário foi rejeitada",
				subject: "Pedido de alteração de tarifário rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de alteração de tarifário foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_TARIFF_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de tarifário cancelada",
				description: "Envia alerta que a pedido de alteração de tarifário foi cancelada",
				subject: "Pedido de alteração de tarifário cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de alteração de tarifário foi cancelado.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de regime foi cancelado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXIT_COMMUNICATION_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de saída submetida",
				description: "Envia alerta que a comunicação de saída foi submetida",
				subject: "Pedido de comunicação de saída foi submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de saída foi submetida.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},a sua comunicação de saída foi submetida.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXIT_COMMUNICATION_REPLY",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Resposta á sua comunicação de saída",
				description: "Envia alerta que há uma resposta á sua comunicação de saída",
				subject: "Resposta á sua comunicação de saída",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de saída foi respondido.</p>
				<p><b>Resposta:</b></p>
				<p>{{response}}</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},tem uma resposta á sua comunicação de saída.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_EXIT_COMMUNICATION_CLOSED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de saída fechada",
				description: "Envia alerta que a comunicação de saída foi fechada",
				subject: "Pedido de comunicação de saída foi fechada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de saída foi fechada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},a sua comunicação de saída foi fechada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ENTRY_COMMUNICATION_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de entrada submetida",
				description: "Envia alerta que a comunicação de entrada foi submetida",
				subject: "Pedido de comunicação de entrada foi submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de entrada foi submetida.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},a sua comunicação de entrada foi submetida.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ENTRY_COMMUNICATION_REPLY",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Resposta á sua comunicação de entrada",
				description: "Envia alerta que há uma resposta á sua comunicação de entrada",
				subject: "Resposta á sua comunicação de entrada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de entrada foi respondido.</p>
				<p><b>Resposta:</b></p>
				<p>{{response}}</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de pedido de comunicação de entrada foi respondido.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ENTRY_COMMUNICATION_CLOSED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Comunicação de entrada fechada",
				description: "Envia alerta que a comunicação de entrada foi fechada",
				subject: "Pedido de comunicação de entrada foi fechada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de pedido de comunicação de entrada foi fechada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},a sua comunicação de entrada foi fechada.",
				data: '{}',
			},
		),




		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto submetida",
				description: "Envia alerta que a alteração de quarto foi submetido",
				subject: "Pedido de alteração de quarto submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto em análise",
				description: "Envia alerta que a alteração de quarto está em análise",
				subject: "Pedido de alteração de quarto em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto em despacho",
				description: "Envia alerta que a alteração de quarto está  em despacho",
				subject: "Pedido de alteração de quarto em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto aprovada",
				description: "Envia alerta que a alteração de quarto foi aprovado",
				subject: "Pedido de alteração de quarto aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto foi aprovado.</p>
			 <p class="submessage">
				O seu residência será alterado e faturado a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto foi aprovado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto rejeitada",
				description: "Envia alerta que a alteração de quarto foi rejeitada",
				subject: "Pedido de alteração de quarto rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto foi rejeitada.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto foi rejeitada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_ROOM_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de quarto cancelada",
				description: "Envia alerta que a alteração de quarto foi cancelada",
				subject: "Pedido de alteração de quarto cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de quarto foi cancelada.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de quarto foi cancelada.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_IBAN_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Pedido de alteração de iban submetido",
				description: "Envia alerta que a alteração de iban submetido",
				subject: "Pedido de alteração de iban submetido",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de iban submetido.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de iban submetido.",
				data: '{}',
			},
		),
				createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_SUBMITTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento submetida",
				description: "Envia alerta que a alteração de periodo de alojamento foi submetida",
				subject: "Alteração de periodo de alojamento submetida",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento foi submetido.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento foi submetido.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_ANALYSIS",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento em análise",
				description: "Envia alerta que a alteração de periodo de alojamento está em análise",
				subject: "Alteração de periodo de alojamento em análise",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento está em análise.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento está em análise.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_DISPATCH",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento em despacho",
				description: "Envia alerta que a alteração de periodo de alojamento está  em despacho",
				subject: "Alteração de periodo de alojamento em despacho",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento está em despacho.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento está em despacho.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_APPROVED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento aprovada",
				description: "Envia alerta que a alteração de periodo de alojamento foi aprovado",
				subject: "Alteração de periodo de alojamento aprovada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento foi aprovado.</p>
			 <p class="submessage">
				Os seus extras serão alterados e faturados a partir do mês indicado.
			 </p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento foi aprovado.",
				data: '{}',
			},
		),


		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_REJECTED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento rejeitada",
				description: "Envia alerta que a alteração de periodo de alojamento foi rejeitada",
				subject: "Alteração de periodo de alojamento rejeitada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento foi rejeitado.</p>

			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"ACCOMMODATION_APPLICATION_PERIOD_CHANGE_CANCELLED",
			{
				service_id: 1,
				alert_template_id: 1,
				name: "Alojamento: Alteração de periodo de alojamento cancelada",
				description: "Envia alerta que a alteração de periodo de alojamento foi cancelada",
				subject: "Alteração de periodo de alojamento cancelada",
				message: `<td>
				<p class="message"><br>Caro(a) {{application.full_name}}, o seu pedido de alteração de periodo de alojamento foi cancelado.</p>
			 <p class="support_message">Para mais informações contacte a equipa de suporte através do email <a class="email_link" href="mailto:alojamento@sasocial.pt">alojamento@sasocial.pt</a>.</p><p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>
			  Para contactar os SAS-SASocial deverá utilizar os canais alternativos presentes em <a href="" target="_blank">Contactos SAS-SASocial</a></p>
			  <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
				<tbody>
				  <tr>
					<td align="left">
					  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
					  <tbody>
						<tr>
						  <td> <a href="{{sasocial_link}}" target="_blank">Ir para SASocial</a> </td>
						</tr>
					  </tbody>
			</table>`,
				simplified_message: "{{application.full_name}},o seu pedido de alteração de periodo de alojamento foi cancelado.",
				data: '{}',
			},
		)
	]);
};
