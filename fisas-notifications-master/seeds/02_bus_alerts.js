const createIfNotExistAlert = require("./utils/createIfNotExist");
const bus_service_id = 3;
exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_DECLARATION_READY",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Declaração temporária disponível",
				description: "Envia alerta quando a declaração temporária fica disponivel para o aluno",
				subject: "Mobilidade: Declaração temporária disponível",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua declaração temporária esta disponivel 
				na sua area pessoal ou através do seguinte link <a href="{{application.declaration.url}}">Declaração</a>.</p>`,
				simplified_message: "{{application.name}}, a sua declaração temporária encontra-se disponível na sua area pessoal.",
				data: `{
					"application": {
						"name": "--",
						"declaration": {
							"url": "https://sasocial.pt"
						}
					}
				}`,
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_SUBMITTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura submetida",
				description: "Envia alerta que a candidatura foi submetida com sucesso",
				subject: " Candidatura submetida com sucesso",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi submetida com sucesso.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi submetida com sucesso.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_APPROVE",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura aprovada",
				description: "Envia alerta que a candidatura foi aprovada",
				subject: "Mobilidade: Candidatura aprovada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi aprovada.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi aprovada.",
				data: `{
					"application": {
						"name": "--"
					 }
				   }`,
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_NOTAPPROVE",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura não aprovada",
				description: "Envia alerta que a candidatura não foi aprovada",
				subject: "Mobilidade: Candidatura não  aprovada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura não foi aprovada.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura não foi aprovada.",
				data: `{
					"application": {
						"name": "--"
					 }
				   }`,
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_ACCEPTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura aceite",
				description: "Envia alerta que a candidatura foi aceite",
				subject: "Mobilidade: Candidatura aceite",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi aceite.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi aceite.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_REJECTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura rejeitada",
				description: "Envia alerta que a candidatura foi rejeitada",
				subject: " Candidatura rejeitada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi rejeitada.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi rejeitada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_CLOSED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura fechada",
				description: "Envia alerta que a candidatura foi fechada",
				subject: " Candidatura fechada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi fechada.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi fechada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_QUITING",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura em processo de desistência",
				description: "Envia alerta que a candidatura está em processo de desistência",
				subject: " Candidatura em processo de desistência",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura está em processo de desistência. Deve aguardar a decisão dos serviços responsaveis.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura está em processo de desistência. Deve aguardar a decisão dos serviços responsaveis.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_WITHDRAWAL",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Desistência aprovada",
				description: "Envia alerta que a desistência foi aprovada",
				subject: "Desistência da candidatura aprovada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua desistência foi aprovada. </p>`,
				simplified_message: "{{application.name}}, a sua desistência foi aprovada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_WITHDRAWALREJECT",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Desistência rejeitada",
				description: "Envia alerta que a desistência foi rejeitada",
				subject: "Desistência da candidatura rejeitada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua desistência foi rejeitada. </p>`,
				simplified_message: "{{application.name}}, a sua desistência foi rejeitada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_CANCELLED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura cancelada",
				description: "Envia alerta que a candidatura foi cancelada",
				subject: " Candidatura cancelada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi cancelada.</p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi cancelada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_SUSPENDED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura suspensa",
				description: "Envia alerta que a candidatura foi suspensa",
				subject: " Candidatura suspensa",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi suspensa. </p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi suspensa.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_APPLICATION_RESUME",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Candidatura retomada",
				description: "Envia alerta que a candidatura foi retomada",
				subject: " Candidatura retomada",
				message: `<p class="message"><br>Caro(a) {{application.name}}, a sua candidatura foi retomada. </p>`,
				simplified_message: "{{application.name}}, a sua candidatura foi retomada.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_ABSENCE_SUBMITTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de suspensão submetido",
				description: "Envia alerta que o seu pedido de suspensão foi submetido",
				subject: " Pedido de suspensão submetido",
				message: `<p class="message"><br>Caro(a) {{application.name}}, o seu pedido de suspensão foi submetido. </p>`,
				simplified_message: "{{application.name}}, o seu pedido de suspensão foi submetido.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_ABSENCE_APPROVED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de suspensão aprovado",
				description: "Envia alerta que o seu pedido de suspensão foi aprovado",
				subject: " Pedido de suspensão aprovado",
				message: `<p class="message"><br>Caro(a) {{application.name}}, o seu pedido de suspensão foi aprovado. </p>`,
				simplified_message: "{{application.name}}, o seu pedido de suspensão foi aprovado.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_ABSENCE_REJECTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de suspensão rejeitado",
				description: "Envia alerta que o seu pedido de suspensão foi rejeitado",
				subject: " Pedido de suspensão rejeitado",
				message: `<p class="message"><br>Caro(a) {{application.name}}, o seu pedido de suspensão foi rejeitado. </p>`,
				simplified_message: "{{application.name}}, o seu pedido de suspensão foi rejeitado.",
				data: '{}',
			},
		),

		createIfNotExistAlert(
			knex,
			"MOBILITY_SUB23_DECLARATION_SUBMITTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de declaração para passe sub-23 submetido",
				description: "Envia alerta que o pedido de declaração para passe sub-23 foi submetido com sucesso",
				subject: " Pedido de declaração para passe sub-23 submetido com sucesso",
				message: `<p class="message"><br>Caro(a) {{declaration.name}}, o pedido de declaração para passe sub-23 foi submetido com sucesso.</p>`,
				simplified_message: "{{declaration.name}}, o pedido de declaração para passe sub-23 foi submetido com sucesso.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_SUB23_DECLARATION_EMITTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de declaração para passe sub-23 emitido",
				description: "Envia alerta que o pedido de declaração para passe sub-23 foi emitido com sucesso",
				subject: " Pedido de declaração para passe sub-23 emitido com sucesso",
				message: `<p class="message"><br>Caro(a) {{declaration.name}}, o pedido de declaração para passe sub-23 foi emitido com sucesso.</p>`,
				simplified_message: "{{declaration.name}}, o pedido de declaração para passe sub-23 foi emitido com sucesso.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_SUB23_DECLARATION_VALIDATED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de declaração para passe sub-23 validada",
				description: "Envia alerta que o pedido de declaração para passe sub-23 foi validada com sucesso",
				subject: " Pedido de declaração para passe sub-23 validada com sucesso",
				message: `<p class="message"><br>Caro(a) {{declaration.name}}, o pedido de declaração para passe sub-23 foi validada com sucesso.</p>`,
				simplified_message: "{{declaration.name}}, o pedido de declaração para passe sub-23 foi validada com sucesso.",
				data: '{}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MOBILITY_SUB23_DECLARATION_REJECTED",
			{
				service_id: bus_service_id,
				alert_template_id: 1,
				name: "Mobilidade: Pedido de declaração para passe sub-23 rejeitado",
				description: "Envia alerta que o pedido de declaração para passe sub-23 foi rejeitado",
				subject: " Pedido de declaração para passe sub-23 rejeitado",
				message: `<p class="message"><br>Caro(a) {{declaration.name}}, o pedido de declaração para passe sub-23 foi rejeitado, pelo seguinte motivo {{declaration.reject_reason}}.</p>`,
				simplified_message: "{{declaration.name}}, o pedido de declaração para passe sub-23 foi rejeitado, pelo seguinte motivo {{declaration.reject_reason}}.",
				data: '{}',
			},
		)
	]);
};
