const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"CALENDAR_EVENT_APPROVAL_NEEDED",
			{
				service_id: 18,
				alert_template_id: 1,
				name: "Calendário: Evento pendente de aprovação",
				description:
					"Alerta de inserção ou edição de evento que necessita de aprovação. " +
					'Permite acesso aos dados "event" com o objecto completo de Event.',
				subject: "Evento pendente de aprovação",
				message: "O seguinte Evento necessita de aprovação: {{event.id}}",
				simplified_message: "O seguinte Evento necessita de aprovação: {{event.id}}",
				data: '{"event": {}}',
			},
		),
		createIfNotExistAlert(
			knex,
			"CALENDAR_EVENT_NOTIFICATION",
			{
				service_id: 18,
				alert_template_id: 1,
				name: "Calendário: Alerta de evento",
				description:
					"Alerta para notificação de evento para um determinado Target. " +
					'Permite acesso aos dados "event" com o objecto completo de Event.',
				subject: "Alerta de evento: {{event.translations[0].name}}",
				message:
					"O evento {{event.translations[0].name}} aproxima-se! Saiba mais em {{event.translations[0].url}}",
				simplified_message: "O evento {{event.translations[0].name} aproxima-se!",
				data: '{"event": {}}',
			},
		),
		createIfNotExistAlert(knex, "CALENDAR_EVENT_CANCELLED", {
			service_id: 18,
			alert_template_id: 1,
			name: "Calendário de eventos: evento cancelado",
			description: "Envia alerta que o evento na qual gostaria de participar foi cancelado.",
			subject: "Evento cancelado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o evento {{event_name}}, onde gostaria de participar foi cancelado.</p> ',
			simplified_message: "{{name}}, o evento {{event_name}} onde gostaria de participar foi cancelado.",
			data: '{"name":"", "event_name":""}',
		}),
		createIfNotExistAlert(knex, "CALENDAR_EVENT_EDITED", {
			service_id: 18,
			alert_template_id: 1,
			name: "Calendário de eventos: evento alterado",
			description: "Envia alerta que o evento na qual gostaria de participar foi alterado.",
			subject: "Evento alterado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o evento {{event_name}}, onde gostaria de participar foi alterado.</p> ',
			simplified_message: "{{name}}, o evento {{event_name}} onde gostaria de participar foi alterado.",
			data: '{"name":"", "event_name": ""}',
		}),
		createIfNotExistAlert(knex, "CALENDAR_EVENT_APPROVED", {
			service_id: 19,
			alert_template_id: 1,
			name: "Calendário de eventos: evento aprovado",
			description: "Envia alerta que o evento na qual gostaria de participar foi aprovado",
			subject: "Evento aprovado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o evento {{event_name}}, onde gostaria de participar foi aprovado.</p> ',
			simplified_message: "{{name}}, o evento {{event_name}} onde gostaria de participar foi aprovado.",
			data: '{"name":"", "event_name": ""}',
		})
	]);
};
