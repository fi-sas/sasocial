const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"MAINTENANCE_PLAN_NOTIFICATION",
			{
				service_id: 20,
				alert_template_id: 1,
				name: "Manutenção Preventiva",
				description: "Notificação de proximidade de Manutenção Preventiva",
				subject: "Manutenção Preventiva",
				message: "O equipamento {{record.name}} tem agendada uma manutenção preventiva",
				simplified_message: "O equipamento {{record.name}} tem agendada uma manutenção preventiva",
				data: '{"record": {}}',
			},
		),
		createIfNotExistAlert(
			knex,
			"MAINTENANCE_REQUEST_NOTIFICATION",
			{
				service_id: 20,
				alert_template_id: 1,
				name: "Manutenção Corretiva",
				description: "Notificação de Manutenção Corretiva",
				subject: "Manutenção Corretiva",
				message: "O equipamento {{task.name}} necessita de uma intervenção",
				simplified_message: "O equipamento {{task.name}} necessita de uma intervenção",
				data: '{"task": {}}',
			},
		),
	]);
};
