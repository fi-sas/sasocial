const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_LISTING_APPROVAL_NEEDED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de Oferta pendente de aprovação",
				description:
					"Alerta de inserção ou edição de oferta que necessita de aprovação. " +
					'Permite acesso aos dados "listing" com o objecto completo de Listing.',
				subject: "Oferta pendente de aprovação",
				message: "A seguinte Oferta necessita de aprovação: {{listing.id}}",
				simplified_message: "A seguinte Oferta necessita de aprovação: {{listing.id}}",
				data: '{"listing": {}}',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_OWNER_APPLICATION_REJECTED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de rejeição de aplicação a proprietário",
				description:
					"Alerta de rejeição de aplicação a proprietário enviado ao candidato, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "application" com o objecto completo de OwnerApplication e "notes" (comentário à rejeição).',
				subject: "Registo de proprietário rejeitado",
				message:
					"Caro/a {{application.full_name}}, \nLamentamos informar mas o seu registo como proprietário foi recusado.\n {{notes}}",
				simplified_message:
					"Lamentamos informar mas o seu registo como proprietário foi recusado. {{notes}}",
				data: '{"application": {}, "notes": "" }',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_OWNER_APPLICATION_APPROVED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de aprovação de aplicação a proprietário",
				description:
					"Alerta de aprovação de aplicação a proprietário enviado ao candidato, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "application" com o objecto completo de OwnerApplication e "user" com o object completo do novo User associado.',
				subject: "Registo de proprietário aprovado",
				message:
					"Caro/a {{application.full_name}}, \nÉ com muito gosto que informamos ques o seu registo como proprietário foi aprovado. {{notes}}",
				simplified_message:
					"É com muito gosto que informamos ques o seu registo como proprietário foi aprovado. {{notes}}",
				data: '{"application": {}, "user": {}, "notes": "" }',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_OWNER_APPLICATION_PENDING",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Regristo criado com sucesso",
				description:
					"Alerta de criação da aplicação a proprietário enviado ao candidato, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "application" com o objecto completo de OwnerApplication e "user" com o object completo do novo User associado.',
				subject: "Registo de proprietário aprovado",
				message:
					"Caro/a {{application.full_name}}, \nÉ com muito gosto que informamos ques o seu registo como proprietário foi registrado, aguarde a sua aprovação pelos serviços.",
				simplified_message:
					"É com muito gosto que informamos ques o seu registo como proprietário foi criado.",
				data: '{"application": {}, "user": {}, "notes": "" }',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_WANTED_AD_REJECTED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de rejeição de procura",
				description:
					"Alerta de rejeição de procura de alojamento enviado ao autor da mesma, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "wanted_ad" com o objecto completo de WantedAd e "notes" (comentário à rejeição).',
				subject: "Registo de procura rejeitado",
				message:
					"Caro/a {{wanted_ad.author_name}}, \nLamentamos informar mas o seu registo de procura foi recusado.\n {{notes}}",
				simplified_message:
					"Lamentamos informar mas o seu registo de procura de alojamento foi recusado. {{notes}}",
				data: '{"wanted_ad": {}, "notes": "" }',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_LISTING_APPROVED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de aprovação da oferta",
				description:
					"Alerta de aprovação de oferta de alojamento enviado ao utilizador proprietário da mesma, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "listing" com o objecto completo de Listing, "user" com o objecto User proprietário da oferta e "notes" (comentário à rejeição).',
				subject: "Registo de oferta aprovado",
				message:
					"Caro/a {{user.name}}, \nÉ um prazer informar mas o seu registo de oferta foi aprovado.\n {{notes}}",
				simplified_message:
					"É um prazer informar mas o seu registo de oferta de alojamento foi aprovado. {{notes}}",
				data: '{"listing": {}, "user": {}, "notes": "" }',
			},
		),

		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_LISTING_REJECTED",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de rejeição de oferta",
				description:
					"Alerta de rejeição de oferta de alojamento enviado ao utilizador proprietário da mesma, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "listing" com o objecto completo de Listing, "user" com o objecto User proprietário da oferta e "notes" (comentário à rejeição).',
				subject: "Registo de oferta rejeitado",
				message:
					"Caro/a {{user.name}}, \nLamentamos informar mas o seu registo de oferta foi recusado.\n {{notes}}",
				simplified_message:
					"Lamentamos informar mas o seu registo de oferta de alojamento foi recusado. {{notes}}",
				data: '{"listing": {}, "user": {}, "notes": "" }',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_NEW_COMPLAINT",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de submissão de nova reclamação",
				description:
					"Alerta de submissão de uma reclamação relativa a uma oferta de alojamento, enviado ao utilizador autor da mesma, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "complaint" e "user" com o objecto User autor da reclamação.',
				subject: "Reclamação submetida",
				message:
					"Caro/a {{user.name}}, \nA sua reclamação foi submetida com sucesso.\n {{complaint.description}}",
				simplified_message: "A sua reclamação foi submetida com sucesso.",
				data: '{"complaint": {}, "user": {}}',
			},
		),
		createIfNotExistAlert(
			knex,
			"PRIVATE_ACCOMMODATION_COMPLAINT_RESPONSE",
			{
				service_id: 15,
				alert_template_id: 1,
				name: "Alojamento Privado: Alerta de resposta a reclamação",
				description:
					"Alerta de resposta a uma reclamação relativa a uma oferta de alojamento, enviado ao utilizador autor da mesma, a partir do MS de Alojamento Privado. " +
					'Permite acesso aos dados "complaint" (objecto Complaint original, com objecto "response" associado) e "user" com o objecto User autor da reclamação.',
				subject: "Resposta a reclamação",
				message:
					"Caro/a {{user.name}}, \nA sua reclamação foi tratada com sucesso.\n {{complaint.response.description}}\n. Reclamação original: {{complaint.description}}",
				simplified_message:
					"A sua reclamação obteve a seguinte resposta: {{complaint.response.description}}",
				data: '{"complaint": {}, "user": {}}',
			},
		),
	]);
};
