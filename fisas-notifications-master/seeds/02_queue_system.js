const createIfNotExistAlert = require("./utils/createIfNotExist");

exports.seed = (knex) => {

	return Promise.all([
		createIfNotExistAlert(
			knex,
			"QUEUE_TICKET_CALLED",
			{
				service_id: 21,
				alert_template_id: 1,
				name: "Fila de espera: Senha chamada",
				description: "Alerta de chamada de uma senha",
				subject: "Senha chamada",
				message: "A sua senha {{ticket_number}} foi chamada, será atendida no balcão {{desk}}.",
				simplified_message: "A sua senha {{ticket_number}} foi chamada, será atendida no balcão {{desk}}.",
				data: '{"ticket_number":"","desk":""}',
			},
		),

		createIfNotExistAlert(
			knex,
			"QUEUE_APPOINTMENT_INFORMATION",
			{
				service_id: 21,
				alert_template_id: 1,
				name: "Fila de espera: Marcação efetuada",
				description: "Alerta de marcação efetuada",
				subject: "Marcação efetuada",
				message: "A sua marcação {{ticket_code}} para o serviço {{service_id}}, fila {{subject_id}}, para a data {{appointment_schedule}} foi efetuada às {{issue_date}}.",
				simplified_message: "A sua marcação foi efetuada.",
				data: '{"ticket_code":"","service_id":"","subject_id":"","appointment_schedule":"","issue_date":""}',
			},
		),

		createIfNotExistAlert(
			knex,
			"QUEUE_ATTENDANCE_APPROACH",
			{
				service_id: 21,
				alert_template_id: 1,
				name: "Fila de espera: Atendimento para breve",
				description: "Alerta de atendimento",
				subject: "Atendimento para breve",
				message: "Estimamos que a sua senha {{ticket_code}} será atendida às {{attendance_time}}, faltam {{ticket_first_than_yours}} senhas para ser chamado.",
				simplified_message: "Alerta de atendimento.",
				data: '{"ticket_code":"","attendance_time":"","ticket_first_than_yours":""}',
			},
		),		

		createIfNotExistAlert(
			knex,
			"QUEUE_CLOSE_DESK",
			{
				service_id: 21,
				alert_template_id: 1,
				name: "Fila de espera: Encerramento de balcão por força maior",
				description: "Alerta de encerramento de balcão",
				subject: "Alerta de encerramento de balcão",
				message: "Por motivo de força maior o balcão {{desk_name}} será encerrado, solicitamos que realize nova marcação.",
				simplified_message: "Alerta de encerramento de balcão.",
				data: '{"desk_name":""}',
			},
		),	
		
		createIfNotExistAlert(
			knex,
			"QUEUE_FASTER_ATTENDANCE",
			{
				service_id: 21,
				alert_template_id: 1,
				name: "Fila de espera: Atendimento mais célere",
				description: "Alerta de atendimento mais célere",
				subject: "Alerta de atendimento mais célere",
				message: "O serviço encontra-se com um atendimento mais rápido, sugerimos que se dirija ao balcão {{desk_name}} mais cedo do que a hora inicialmente prevista.",
				simplified_message: "Alerta de atendimento mais célere.",
				data: '{"desk_name":""}',
			},
		),		

	]);
};
