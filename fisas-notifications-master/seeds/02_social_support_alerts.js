const createIfNotExistAlert = require("./utils/createIfNotExist");
const SOCIAL_SCHOLARSHIP_SERVICE_ID = 17;
exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(
			knex,
			"SOCIAL_SUPPORT_PAYMENT_GRID_GENERATED",
			{
				service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
				alert_template_id: 1,
				name: "Bolsa de Colaboradores: Grelha de pagamentos gerada",
				description:
					"Alerta de geração automática de grelha de pagamentos, que necessita de aprovação. " +
					'Permite acesso aos dados "month" (1-12), "year" e "count" (valor numérico com total de linhas criadas).',
				subject: "Grelha de pagamentos da Bolsa de Colaboradores gerada",
				message:
					"Foi gerada a grelha de pagamentos relativo à Bolsa de Colaboradores para o período {{month}}/{{year}}, com um total de {{count}} linhas.",
				simplified_message:
					"Gerada a grelha de pagamentos relativo à Bolsa de Colaboradores para o período {{month}}/{{year}} ({{count}} linhas).",
				data: '{"month": 2, "year": 2019, "count": 50}',
			},
		),
		createIfNotExistAlert(
			knex,
			"SOCIAL_SUPPORT_EXTERNAL_ENTITY_CREATED",
			{
				service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
				alert_template_id: 1,
				name: "Bolsa de Colaboradores: Registo criado com sucesso",
				description:
					"Alerta de criação do registo de um utilizador do tipo Entidade Externa, a partir do MS de bolsa de colaboradores. ",
				subject: "Bolsa de Colaboradores: Registo de utilizador criado",
				message:
					"Caro/a {{name}}, \nÉ com muito gosto que informamos ques o seu registo como entidade externa para a bolsa de colaboradores foi criado. Os dados para acesso são: \n Email:  {{email}}\nPassword: {{password}} ",
				simplified_message:
					"É com muito gosto que informamos ques o seu registo como entidade externa para a bolsa de colaboradores  foi criado.",
				data: '{"name": "","email": "", "password":""  }',
			},
		),
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura submetida",
			description: "Envia alerta que a candidatura foi submetida com sucesso.",
			subject: "Candidatura submetida com sucesso",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores foi submetida.</p> ',
			simplified_message: "{{name}}, a sua candidatura à bolsa de colaboradores foi submetida.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_ANALYSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em análise",
			description: "Envia alerta que a candidatura encontra-se em análise.",
			subject: "Candidatura em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em análise.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se em análise.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_CANCELLED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura cancelada",
			description: "Envia alerta que a candidatura foi cancelada com sucesso.",
			subject: "Candidatura cancelada com sucesso",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores foi cancelada.</p> ',
			simplified_message: "{{name}}, a sua candidatura à bolsa de colaboradores foi cancelada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_INTERVIEWED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em entrevista",
			description: "Envia alerta que a candidatura encontra-se em entrevista com sucesso.",
			subject: "Candidatura entrevistada com sucesso",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em entrevista.</p> ',
			simplified_message: "{{name}}, a sua candidatura à bolsa de colaboradores encontra-se em entrevista.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_ACCEPTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura aceite",
			description: "Envia alerta que a candidatura foi aceite.",
			subject: "Candidatura aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores foi aceite.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores foi aceite.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_DECLINED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura não aceite",
			description: "Envia alerta que a candidatura não foi aceite.",
			subject: "Candidatura não aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores não foi aceite.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores não foi aceite.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_WAITING", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em espera",
			description: "Envia alerta que a candidatura encontra-se em espera.",
			subject: "Candidatura em espera",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em espera.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se em espera.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_ACKNOWLEDGED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em reconhecimento",
			description: "Envia alerta que a candidatura encontra-se em reconhecimento.",
			subject: "Candidatura em reconhecimento",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em reconhecimento.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se em reconhecimento.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_CLOSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura fechada",
			description: "Envia alerta que a candidatura encontra-se fechada.",
			subject: "Candidatura fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se fechada.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se fechada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_WITHDRAWAL", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em estado de desistencia",
			description: "Envia alerta que a candidatura encontra-se em estado de desistencia.",
			subject: "Candidatura em estado de desistencia",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em estado de desistencia.</p>',
			simplified_message:
				"{{name}},a sua candidatura à bolsa de colaboradores encontra-se em estado de desistencia.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_PUBLISHED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura publicada",
			description: "Envia alerta que a candidatura encontra-se publicada.",
			subject: "Candidatura publicada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se publicada.</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se publicada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_EXPIRED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura expirada",
			description: "Envia alerta que a candidatura encontra-se expirada.",
			subject: "Candidatura expirada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se expirada</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se expirada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_STATUS_DISPATCH", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura em despacho",
			description: "Envia alerta que a candidatura encontra-se em despacho.",
			subject: "Candidatura em despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura à bolsa de colaboradores encontra-se em despacho</p>',
			simplified_message: "{{name}},a sua candidatura à bolsa de colaboradores encontra-se em despacho.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_APPLICATION_SCHEDULE_INTERVIEW", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Candidatura, agendada entrevista",
			description: "Envia alerta que foi agendada uma entrevista para a candidatura.",
			subject: "Candidatura agendada entrevista",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua entrevista à bolsa de colaboradores foi agendada para a data {{date}} no local {{local}}. </p>',
			simplified_message: "{{name}},a sua entrevista a candidatura foi agendada.",
			data: '{"name":"", "local":"", "date":""}',
		}),

		// SEEDS EXPERIENCE
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta Submetida",
			description: "Envia alerta que a sua oferta encontra-se submetida.",
			subject: "Oferta de experiência submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se submetida</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se submetida.",
			data: '{"name":"", "experience_name": ""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_ANALYSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em análise",
			description: "Envia alerta que a sua oferta encontra-se em análise.",
			subject: "Oferta de experiência em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em análise</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em análise.",
			data: '{"name":"", "experience_name": ""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_APPROVED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta aprovada",
			description: "Envia alerta que a sua oferta encontra-se aprovada.",
			subject: "Oferta de experiência aprovada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se aprovada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se aprovada.",
			data: '{"name":"", "experience_name": ""}',
		}),
		

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_PUBLISHED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta publicada",
			description: "Envia alerta que a oferta encontra-se publicada.",
			subject: "Oferta de experiência publicada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se publicada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se publicada.",
			data: '{"name":"", "experience_name": ""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_REJECTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta rejeitada",
			description: "Envia alerta que a oferta encontra-se rejeitada.",
			subject: "Oferta de experiência rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se rejeitada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se rejeitada.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_CANCELLED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta Cancelada",
			description: "Envia alerta que a oferta encontra-se cancelada.",
			subject: "Oferta de experiência cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se cancelada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se cancelada.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_SEND_SEEM", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em Emitir Parecer",
			description: "Envia alerta que a oferta encontra-se em emitir parecer.",
			subject: "Oferta de experiência em emitir parecer",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em emitir parecer</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em emitir parecer.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_DISPATCH", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em despacho",
			description: "Envia alerta que a oferta encontra-se em despacho.",
			subject: "Oferta de experiência em  despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em despacho</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em despacho.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_EXTERNAL_SYSTEM", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em análise pelo sistema externo",
			description: "Envia alerta que a oferta encontra-se em análise pelo sistema externo.",
			subject: "Oferta de experiência em análise pelo sistema externo",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em análise pelo sistema externo</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em análise pelo sistema externo.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_SELECTION", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em selecção",
			description: "Envia alerta que a oferta encontra-se em selecção.",
			subject: "Oferta de experiência em selecção",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em selecção</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em selecção.",
			data: '{"name":"", "experience_name": ""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_IN_COLABORATION", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta em colaboração",
			description: "Envia alerta que a oferta encontra-se em colaboração.",
			subject: "Oferta de experiência em colaboração",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se em colaboração</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se em colaboração.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_CLOSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta fechada",
			description: "Envia alerta que a oferta encontra-se fechada.",
			subject: "Oferta de experiência fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} colaboradores encontra-se fechada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se fechada.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_CANCELED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta cancelada",
			description: "Envia alerta que a oferta encontra-se cancelada.",
			subject: "Oferta de experiência cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência  {{experience_name}} encontra-se cancelada</p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se cancelada.",
			data: '{"name":"", "experience_name": ""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_SEND_TARGET_USERS", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta de experiência",
			description: "Envia alerta que existe uma oferta de experiência que lhe pode interessar.",
			subject: "Oferta de experiência",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a oferta de experiência {{title}}, pode ser-lhe de interesse.</p>',
			simplified_message: "{{name}},a oferta de experiência {{title}}, pode ser-lhe de interesse.",
			data: '{"name":"", "title": ""}',
		}),

		// NOTIFICATIONS MANIFEST INTEREST USER
		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se submetida.",
			subject: "Manifestação de interesse submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se submetida.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se submetida.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_ANALYSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em análise.",
			subject: "Manifestação de interesse em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em análise.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em análise.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_INTERVIEWED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em entrevista.",
			subject: "Manifestação de interesse em entrevista",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em entrevista.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em entrevista.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_APPROVED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se aprovada.",
			subject: "Manifestação de interesse aprovada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se aprovada.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se aprovada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_WAITING", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em fila de espera.",
			subject: "Manifestação de interesse encontra-se em fila de espera",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em fila de espera.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em fila de espera.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_NOT_SELECTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se como não selecionado.",
			subject: "Manifestação de interesse encontra-se como não selecionado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se como não selecionado.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se como não selecionado.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_ACCEPTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se aceite.",
			subject: "Manifestação de interesse encontra-se aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se aceite.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se aceite.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_COLABORATION", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em colaboração.",
			subject: "Manifestação de interesse encontra-se em colaboração",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em colaboração.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em colaboração.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_WITHDRAWAL", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em desistência.",
			subject: "Manifestação de interesse encontra-se em em desistência",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em desistência.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em desistência.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_DECLINED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse rejeitada.",
			subject: "Manifestação de interesse rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se rejeitada.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se rejeitada.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_DECLINED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse rejeitada.",
			subject: "Manifestação de interesse rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se rejeitada.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se rejeitada.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_CANCELLED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse cancelada.",
			subject: "Manifestação de interesse cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se cancelada.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se cancelada.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_CLOSED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse fechada.",
			subject: "Manifestação de interesse fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se fechada.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se fechada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_SCHEDULE_INTERVIEW", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse, agendada entrevista",
			description: "Envia alerta que foi agendada uma entrevista para a manifestação de interesse.",
			subject: "Manifestação de interesse com entrevista agendada.",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua entrevista à manifestação de interesse foi agendada para a data {{date}} no local {{local}}. </p>',
			simplified_message: "{{name}},a sua entrevista a manifestação de interesse foi agendada, na data {{date}}, no local {{local}}.",
			data: '{"name":"", "local":"", "date":""}',
		}),


		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_WITHDRAWAL_ACCEPTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse, aceite desistência",
			description: "Envia alerta que foi aprovado o pedido de desitencia da colaboração",
			subject: "Manifestação de interesse com entrevista agendada.",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se com a desistência aprovada. </p>',
			simplified_message: "{{name}},a sua manifestação de interesse encontra-se com a desistência aprovada. ",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_DISPATCH", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Manifestação de interesse",
			description: "Envia alerta que a sua manifestação de interesse encontra-se em despacho.",
			subject: "Manifestação de interesse em despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua manifestação de interesse encontra-se em depacho.</p>',
			simplified_message: "{{name}}, a sua manifestação de interesse encontra-se em despacho.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_PAYMENT_GRID_APPROVED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Mapa de pagamento",
			description: "Envia alerta que foi aprovado um mapa de pagamento.",
			subject: "Mapa de pagamento aprovado.",
			message:
				'<p class="message"><br>Foi aprovado um mapa de pagamento referente ao mês {{month}}, do ano {{year}} da oferta {{title}} </p>',
			simplified_message: "Foi aprovado um mapa de pagamento.",
			data: '{"month":"", "year":"", "title":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_COMPLAIN", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: reclamação",
			description: "Envia alerta que foi respondida a sua reclamação",
			subject: "Reclamação respondida.",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua reclamação foi respondida, com o seguinte resposta : {{reponse}}. </p>',
			simplified_message: "{{name}},a sua reclamação já foi respondida.",
			data: '{"name":"", "response":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_EXPERIENCE_STATUS_RETURNED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Oferta Devolvida",
			description: "Envia alerta que a sua oferta encontra-se devolvida.",
			subject: "Oferta de experiência devolvida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de experiência {{experience_name}} encontra-se devolvida, pelo seguinte motivo {{reason_return}}. </p>',
			simplified_message: "{{name}},a sua oferta de experiência encontra-se devolvida, pelo seguinte motivo {{reason_return}}.",
			data: '{"name":"", "experience_name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_NEW_APPLICATION_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Nova candidatura submetida",
			description: "Envia alerta que uma nova candidatura foi submetida com sucesso.",
			subject: "Nova candidatura submetida com sucesso",
			message:
				'<p class="message"><br>Caro(a) utilizador(a), uma nova candidatura à bolsa de colaboradores foi submetida pelo aluno {{name}}.</p> ',
			simplified_message: "Uma nova candidatura à bolsa de colaboradores foi submetida pelo aluno {{name}}.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_NEW_MANIFEST_INTEREST_USERS_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Nova manifestação de interesse submetida",
			description: "Envia alerta que uma nova manifestação de interesse foi submetida com sucesso.",
			subject: "Nova manifestação de interesse submetida com sucesso",
			message:
				'<p class="message"><br>Caro(a) utilizador(a), uma nova manifestação de interesse à bolsa de colaboradores foi submetida pelo aluno {{name}}.</p> ',
			simplified_message: "Uma nova manifestação de interesse à bolsa de colaboradores foi submetida pelo aluno {{name}}.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "SOCIAL_SUPPORT_NEW_EXPERIENCE_SUBMITTED", {
			service_id: SOCIAL_SCHOLARSHIP_SERVICE_ID,
			alert_template_id: 1,
			name: "Bolsa de Colaboradores: Nova oferta submetida",
			description: "Envia alerta que uma nova oferta foi submetida com sucesso.",
			subject: "Nova oferta submetida com sucesso",
			message:
				'<p class="message"><br>Caro(a) utilizador(a), uma nova oferta à bolsa de colaboradores foi submetida pelo aluno {{name}}.</p> ',
			simplified_message: "Uma nova oferta à bolsa de colaboradores foi submetida pelo aluno {{name}}.",
			data: '{"name":""}',
		}),

	]);
};
