const createIfNotExistAlert = require("./utils/createIfNotExist");
const UBikeServiceId = 13;
exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_SUBMITTED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura submetida",
			description: "Envia alerta que a candidatura foi submetida com sucesso.",
			subject: "Candidatura submetida com sucesso",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike foi submetida.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao UBike foi submetida.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_ANALYSIS", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em análise",
			description: "Envia alerta que a candidatura encontra-se em análise.",
			subject: "Candidatura em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em análise.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se em análise.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_DISPATCH", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em despacho",
			description: "Envia alerta que a candidatura encontra-se em despacho.",
			subject: "Candidatura em despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em despacho.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se em despacho.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_CANCELLED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura cancelada",
			description: "Envia alerta que a candidatura foi cancelada com sucesso.",
			subject: "Candidatura cancelada com sucesso",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike foi cancelada.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao UBike foi cancelada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_ASSIGNED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura atribuida",
			description: "Envia alerta que a candidatura encontra-se atribuida.",
			subject: "Candidatura atribuida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se atribuida.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se atribuida.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_CONFIRMED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura confirmada",
			description: "Envia alerta que a candidatura encontra-se confirmada.",
			subject: "Candidatura confirmada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se confirmada.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se confirmada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_REJECTED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura não aceite",
			description: "Envia alerta que a candidatura não foi aceite.",
			subject: "Candidatura não aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike  não foi aceite.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike  não foi aceite.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_UNASSIGNED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura não atribuida",
			description: "Candidatura submetida não colocada.",
			subject: "Candidatura não atribuida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike não foi colocada.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike não foi colocada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_ENQUEUED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em espera",
			description: "Envia alerta que a candidatura encontra-se em espera.",
			subject: "Candidatura em espera",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em espera.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se em espera.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_COMPLAIN", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em estado de reclamação",
			description: "Envia alerta que a candidatura encontra-se em estado de reclamação.",
			subject: "Candidatura em estado de reclamação",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em estado de reclamação.</p>',
			simplified_message:
				"{{name}},a sua candidatura ao UBike encontra-se em estado de reclamação.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_QUITING", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em estado de termino",
			description: "Envia alerta que a candidatura encontra-se em estado de termino.",
			subject: "Candidatura em estado de termino",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em estado de termino.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se em estado de termino.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_WITHDRAWAL", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura em estado de desistencia",
			description: "Envia alerta que a candidatura encontra-se em estado de desistencia.",
			subject: "Candidatura em estado de desistencia",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se em estado de desistencia.</p>',
			simplified_message:
				"{{name}},a sua candidatura ao UBike encontra-se em estado de desistencia.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_CLOSED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura fechada",
			description: "Envia alerta que a candidatura encontra-se fechada.",
			subject: "Candidatura fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se fechada.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se fechada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_CONTRACTED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura contratada",
			description: "Envia alerta que a candidatura encontra-se contratada.",
			subject: "Candidatura contratada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se contratada.</p>',
			simplified_message: "{{name}},a sua candidatura ao UBike encontra-se contratada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_STATUS_ACCEPTED", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Candidatura aceite",
			description: "Envia alerta que a candidatura encontra-se aceite.",
			subject: "Candidatura aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se aceite.</p>',
			simplified_message: "{{name}}, a sua candidatura ao UBike encontra-se aceite.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_MISSING_CONFIRMATION", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Aprovação pendente",
			description: "Envia alerta que o utilizador tem uma aprovação pendente.",
			subject: "Aprovação pendente",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao UBike encontra-se pendente de uma aprovação.</p>',
			simplified_message:
				"{{name}},a sua candidatura ao UBike encontra-se pendente de uma aprovação.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "UBIKE_APPLICATION_ENDING_CONTRACT", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Proximidade do final de contrato",
			description: "Envia alerta que o utilizador sobre a proximidade do fim do seu contrato.",
			subject: "Proximidade do final de contrato",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu contrato relativo á candidatura UBike encontra-se perto do seu final.</p>',
			simplified_message:
				"{{name}}, o seu contrato relativo á candidatura UBike encontra-se perto do seu final.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "UBIKE_APPLICATION_MISSING_TRIPS", {
			service_id: UBikeServiceId,
			alert_template_id: 1,
			name: "Ubike: Falta de reporte de actividade",
			description: "Envia alerta que o utilizador sobre a falta de reporte da actividade.",
			subject: "Falta de reporte de actividade",
			message:
				'<p class="message"><br>Caro(a) {{name}}, deve actualizar os seus reportes de actividade.</p>',
			simplified_message: "{{name}}, deve actualizar os seus reportes de actividade.",
			data: '{"name":""}',
		}),
	]);
};
