const createIfNotExistAlert = require("./utils/createIfNotExist");
const VOLUNTEERING_SERVICE_ID = 28;
exports.seed = (knex) => {
    return Promise.all([
        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_SUBMITTED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura submetida",
            description: "Envia alerta que a candidatura foi submetida com sucesso.",
            subject: "Candidatura submetida com sucesso",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado foi submetida.</p> ',
            simplified_message: "{{name}}, a sua candidatura ao voluntariado foi submetida.",
            data: '{"name":""}',
        }),
        
        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_ANALYSED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura em análise",
            description: "Envia alerta que a candidatura encontra-se em análise.",
            subject: "Candidatura em análise",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado encontra-se em análise.</p>',
            simplified_message: "{{name}},a sua candidatura ao voluntariado encontra-se em análise.",
            data: '{"name":""}',
        }),
        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_CANCELLED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura cancelada",
            description: "Envia alerta que a candidatura foi cancelada com sucesso.",
            subject: "Candidatura cancelada com sucesso",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado foi cancelada.</p> ',
            simplified_message: "{{name}}, a sua candidatura ao voluntariado foi cancelada.",
            data: '{"name":""}',
        }),
        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_EXPIRED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura entrevistada",
            description: "Envia alerta que a candidatura foi expirada com sucesso.",
            subject: "Candidatura expirada com sucesso",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado foi expirada.</p> ',
            simplified_message: "{{name}}, a sua candidatura ao voluntariado foi expirada.",
            data: '{"name":""}',
        }),
        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_ACCEPTED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura aceite",
            description: "Envia alerta que a candidatura foi aceite.",
            subject: "Candidatura aceite",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado foi aceite.</p>',
            simplified_message: "{{name}},a sua candidatura ao voluntariado foi aceite.",
            data: '{"name":""}',
        }),

        createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_DECLINED", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura não aceite",
            description: "Envia alerta que a candidatura não foi aceite.",
            subject: "Candidatura não aceite",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado não foi aceite.</p>',
            simplified_message: "{{name}},a sua candidatura ao voluntariado não foi aceite.",
            data: '{"name":""}',
        }),

		createIfNotExistAlert(knex, "VOLUNTEERING_APPLICATION_STATUS_DISPATCH", {
            service_id: VOLUNTEERING_SERVICE_ID,
            alert_template_id: 1,
            name: "Voluntariado: Candidatura em despacho",
            description: "Envia alerta que a candidatura em despacho.",
            subject: "Candidatura em despacho",
            message:
                '<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao voluntariado em despacho.</p>',
            simplified_message: "{{name}},a sua candidatura ao voluntariado em despacho.",
            data: '{"name":""}',
        }),


        createIfNotExistAlert(
            knex,
            "VOLUNTEERING_EXTERNAL_ENTITY_CREATED",
            {
                service_id: VOLUNTEERING_SERVICE_ID,
                alert_template_id: 1,
                name: "Voluntariado: Registo criado com sucesso",
                description:
                    "Alerta de criação do registo de um utilizador do tipo Entidade Externa, a partir do MS de bolsa de colaboradores. ",
                subject: "Voluntariado: Registo de utilizador criado",
                message:
                    "Caro/a {{name}}, \nÉ com muito gosto que informamos ques o seu registo como entidade externa para o Voluntariado foi criado. Os dados para acesso são: \n Email:  {{email}}\nPassword: {{password}} ",
                simplified_message:
                    "É com muito gosto que informamos ques o seu registo como entidade externa para o Voluntariado foi criado.",
                data: '{"name": "","email": "", "password":""}',
            },
        ),

        //Notification target user
        createIfNotExistAlert(
            knex,
            "VOLUNTEERING_EXPERIENCE_SEND_TARGET_USERS",
            {
                service_id: VOLUNTEERING_SERVICE_ID,
                alert_template_id: 1,
                name: "Voluntariado: Oferta de voluntariado",
                description: "Envia alerta que existe uma oferta de voluntariado que lhe pode interessar.",
                subject: "Oferta de voluntariado",
                message:
                    '<p class="message"><br>Caro(a) {{name}}, a oferta de voluntariado {{title}}, pode ser-lhe de interesse.</p>',
                simplified_message: "{{name}},a oferta de voluntariado {{title}}, pode ser-lhe de interesse.",
                data: '{"name":"", "title": ""}',
            },
        ),

        //Notification experience status
        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_SUBMITTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta Submetida",
			description: "Envia alerta que a sua oferta encontra-se submetida.",
			subject: "Oferta de voluntariado submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se submetida</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se submetida.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_RETURNED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta Devolvida",
			description: "Envia alerta que a sua oferta encontra-se devolvida.",
			subject: "Oferta de voluntariado devolvida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se devolvida</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se devolvida.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_ANALYSED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em análise",
			description: "Envia alerta que a sua oferta encontra-se em análise.",
			subject: "Oferta de voluntariado em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em análise</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em análise.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_APPROVED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta aprovada",
			description: "Envia alerta que a sua oferta encontra-se aprovada.",
			subject: "Oferta de voluntariado aprovada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se aprovada</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se aprovada.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_PUBLISHED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta publicada",
			description: "Envia alerta que a sua oferta encontra-se publicada.",
			subject: "Oferta de voluntariado publicada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se publicada</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se publicada.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_REJECTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta rejeitada",
			description: "Envia alerta que a sua oferta encontra-se rejeitada.",
			subject: "Oferta de voluntariado rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se rejeitada</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se rejeitada.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_SEND_SEEM", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em emitir parecer",
			description: "Envia alerta que a sua oferta encontra-se em emitir parecer.",
			subject: "Oferta de voluntariado em emitir parecer",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em emitir parecer</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em emitir parecer.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_EXTERNAL_SYSTEM", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em sistema externo ",
			description: "Envia alerta que a sua oferta encontra-se em sistema externo.",
			subject: "Oferta de voluntariado em sistema externo",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em sistema externo</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em sistema externo.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_SELECTION", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em seleção",
			description: "Envia alerta que a sua oferta encontra-se em seleção.",
			subject: "Oferta de voluntariado em seleção",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em seleção</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em seleção.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_IN_COLABORATION", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em colaboração",
			description: "Envia alerta que a sua oferta encontra-se em colaboração.",
			subject: "Oferta de voluntariado em colaboração",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em colaboração</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em colaboração.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_CLOSED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta fechada",
			description: "Envia alerta que a sua oferta encontra-se fechada.",
			subject: "Oferta de voluntariado fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se fechada</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se fechada.",
			data: '{"name":""}',
		}),

        createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_CANCELED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta cancelada",
			description: "Envia alerta que a sua oferta encontra-se cancelada.",
			subject: "Oferta de voluntariado cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se cancelada</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se cancelada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_EXPERIENCE_STATUS_DISPATCH", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Oferta de voluntariado: Oferta em despacho",
			description: "Envia alerta que a sua oferta encontra-se em despacho.",
			subject: "Oferta de voluntariado em despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua oferta de voluntariado encontra-se em despacho</p>',
			simplified_message: "{{name}},a sua oferta de voluntariado encontra-se em despacho.",
			data: '{"name":""}',
		}),

		// Notification of manifest_interest
		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_SUBMITTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição submetida",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se submetida.",
			subject: "Inscrição voluntariado submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se submetida</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se submetida.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_ANALYSED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição em análise",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se em análise.",
			subject: "Inscrição voluntariado em análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se em análise</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se em análise.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_APPROVED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição aprovada",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se aprovada.",
			subject: "Inscrição voluntariado aprovada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se aprovada</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se aprovada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_WAITING", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição em fila de espera",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se em fila de espera.",
			subject: "Inscrição voluntariado em fila de espera",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se em fila de espera</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se em fila de espera.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_NOT_SELECTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição não selecionada",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se como não selecionada",
			subject: "Inscrição voluntariado não selecionada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se como não selecionada</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se como não selecionada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_ACCEPTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição aceite",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se aceite",
			subject: "Inscrição voluntariado aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se aceite</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se aceite.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_COLABORATION", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição em colaboração",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se em colaboração",
			subject: "Inscrição voluntariado em colaboração",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se em colaboração</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se em colaboração.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_WITHDRAWAL", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição em desistência",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se em deistência",
			subject: "Inscrição voluntariado em desistência",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se em desistência</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se em desistência.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_DECLINED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição rejeitada",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se rejeitada",
			subject: "Inscrição voluntariado rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se rejeitada</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se rejeitada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_CANCELLED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição cancelada",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se cancelada",
			subject: "Inscrição voluntariado cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se cancelada</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se cancelada.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_CLOSED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: desistência aceite",
			description: "Envia alerta que a inscrição a oferta de voluntariado com a desistência aceite",
			subject: "Inscrição voluntariado com a desistência aceite",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se com a desistência aceite.</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se com a desistência aceite.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_WITHDRAWAL_ACCEPTED", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição fechada",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se fechada",
			subject: "Inscrição voluntariado fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se fechada</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se fechada.",
			data: '{"name":""}',
		}),


		createIfNotExistAlert(knex, "VOLUNTEERING_MANIFEST_INTEREST_USERS_DISPATCH", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Inscrição voluntariado: inscrição em despacho",
			description: "Envia alerta que a inscrição a oferta de voluntariado encontra-se em despacho",
			subject: "Inscrição voluntariado em despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua inscrição a oferta de voluntariado encontra-se em despacho</p>',
			simplified_message: "{{name}},a sua inscrição a oferta de voluntariado encontra-se em despacho.",
			data: '{"name":""}',
		}),

		createIfNotExistAlert(knex, "VOLUNTEERING_COMPLAIN", {
			service_id: VOLUNTEERING_SERVICE_ID,
			alert_template_id: 1,
			name: "Voluntariado: reclamação",
			description: "Envia alerta que foi respondida a sua reclamação",
			subject: "Reclamação respondida.",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua reclamação foi respondida, com o seguinte resposta : {{reponse}}. </p>',
			simplified_message: "{{name}},a sua reclamação já foi respondida.",
			data: '{"name":"", "response":""}',
		}),

    ]);
};
