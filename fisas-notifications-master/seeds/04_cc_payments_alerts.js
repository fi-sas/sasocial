const createIfNotExistAlert = require("./utils/createIfNotExist");
const CC_PAYMENTS_SERVICE_ID = 4;
exports.seed = (knex) => {
    return Promise.all([
        createIfNotExistAlert(knex, "CC_PAYMENTS_REFMB_AMA_TRANSACTION_DATA", {
            service_id: CC_PAYMENTS_SERVICE_ID,
            alert_template_id: 1,
            name: "Pagamentos/ Carregamentos: Dados para pagamento RefMB",
            description: "Envia alerta com dados para pagamento no Multibanco (REFMB_AMA).",
            subject: "Pagamentos/ Carregamentos: Dados para pagamento RefMB",
            message:
                `<p><br/>Caríssimo(a) {{user_data.name}},
                <br/><br/>Foi gerada a referência multibanco com os seguintes dados para pagamento:
                <br/><br/></p>
            {{#description}}<p>[Descrição: "<span style="font-style: italic">{{description}}</span>"]<br/><br/></p>{{/description}}
                <p>
                    <table style="border:1px solid #818181;padding:5px; border-collapse:collapse;">
                        <tbody>
                            <tr>
                                <td rowspan="4" align="center" style="vertical-align: middle;width: 110px">
                                    <img style="max-height:60px;" src="{{medias.image.CC_LOGO_MB.url}}" alt="MB"></td>
                                <td align="left" style="vertical-align: middle;width: 90px">
                                    <span>&nbsp;Entidade: </span></td>
                                <td align="center" style="vertical-align: middle;">
                                    <span style="font-weight:bold">{{payment_method_data.EntityNumber}}</span></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align: middle;width: 90px">
                                    <span>&nbsp;Ref. MB: </span></td>
                                <td align="center" style="vertical-align: middle;">
                                    <span style="font-weight:bold">{{payment_method_data.Reference}}</span></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align: middle;width: 90px">
                                    <span>&nbsp;Valor: </span></td>
                                <td align="center" style="vertical-align: middle;">
                                    <span style="font-weight:bold">{{payment_method_data.Amount_with_currency_symbol}}</span></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align: middle;width: 90px">
                                    <span>&nbsp;Data limite: </span></td>
                                <td align="center" style="vertical-align: middle;"><span style="font-weight:bold">
                      {{#payment_method_data.ActiveExpiration}}{{payment_method_data.ExpirationDate | date:"yyyy-MM-dd"}}{{/payment_method_data.ActiveExpiration}}
                      {{^payment_method_data.ActiveExpiration}}&nbsp;--{{/payment_method_data.ActiveExpiration}}
                      </span></td>
                            </tr>
                        </tbody>
                    </table>
                </p>`,
            simplified_message: "{{user_data.name}}, Foi gerada a referência multibanco com os seguintes dados para pagamento. Entidade: {{payment_method_data.EntityNumber}} | Referência: {{payment_method_data.Reference}} | Valor: {{payment_method_data.Amount}}",
            data: '{"name":"", "description":"", "payment_method_data":{}}',
        }),
        createIfNotExistAlert(knex, "CC_MIDDLEWARE_ERP_ERROR", {
            service_id: CC_PAYMENTS_SERVICE_ID,
            alert_template_id: 1,
            name: "Middleware ERP: ocorrências integração",
            description: "Envia alerta com ocorrências na geração de documento financeiro.",
            subject: "SASocial / Middleware ERP: ocorrências integração ({{financial_document.id}})",
            message:
                `<p><br/>Atenção {{user_data.name}},
                <br/><br/>Foi devolvida uma ocorrência no envio de documento financeiro para o ERP:
                </p>
                <p>                    
                
                <br/><b>Conta-Corrente</b>: [{{financial_document.account_id}}] {{account_name}}
                <br>
                <br/><b>ID Movimento</b>: {{financial_document.id}}
                <br/><b>Data/Hora</b>: {{financial_document.created_at}}
                <br/><b>Device</b>: {{financial_document.device.name}}
                <br>
                <br/><b>Operação</b>: {{financial_document.operation}}
                <br/><br/><b>Entidade</b>:
                <ul>
                    <li>Nome: {{financial_document.entity}}</li>
                    <li>NIF: {{financial_document.tin}}</li>
                    <li>Email: {{financial_document.email}}</li>
                </ul>
                <br/><br/>Status: <b>{{financial_document.middleware_erp_status}}</b>
                <br/><br/>
                
                <b>Documento financeiro enviado</b>:
                <br/>
                <textarea rows="5" cols="60">{{&financial_document_stringify}}</textarea>
                
                <br/><br/>
                <b>Resposta</b>:
                <br/>
                <ul>
                {{#middleware_erp_message}}
{{#data}}
{{#description}}<li>{{&description}}</li>{{/description}}
{{/data}}
{{/middleware_erp_message}}
                </ul>

                <textarea rows="5" cols="60">{{middleware_erp_message_stringify}}</textarea>
                </p>`,
            simplified_message: "SASocial/MiddlewareERP: Foi gerada uma ocorrência ao gerar documento financeiro (device: {{financial_document.device.name}} / id: {{financial_document.id}} / middleware_erp_status: {{financial_document.middleware_erp_status}}).",
            data: '{"account_name": "", "financial_document":{}, "financial_document_stringify":"", "middleware_erp_message":"", "middleware_erp_message_stringify":""}',
        }),

    ]);
};
