const createIfNotExistAlert = require("./utils/createIfNotExist");
const AUTH_SERVICE_ID = 8;
exports.seed = (knex) => {
    return Promise.all([
        createIfNotExistAlert(knex, "AUTH_VERIFY_ACCOUNT", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "Verificação de conta de utilizador ",
            description: "Verificação de conta de utilizador ",
            subject: "SASocial confirmação de email",
            message:
                `<p>Olá</p>
				<p> para terminar o registo da conta SASocial ({{user_data.email}}) confirm o seu email.

					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}/account-verification/{{verification_token}}" target="_blank">Confirmar email</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá, para terminar o registo da conta SASocial ({{user_data.email}}) confirme o seu email entrando  seguinte link {{sasocial_link}}/account-verification/{{verification_token}}",
            data: '{"verification_token":"------"}',
        }),
		createIfNotExistAlert(knex, "AUTH_RESET_PASSWORD", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "Notificação para recuperação de password",
            description: "Recuperação de password",
            subject: "SASocial - Recuperação de password",
            message:
                `<p>Olá {{user_data.name}},</p>
				<p> para prosseguir com a recuperação da password da conta ({{user_data.email}}) do SASocial cliqua no botão abaixo.

					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}/auth/new-password/{{recovery_token}}" target="_blank">Recuperar password</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá {{user_data.name}}, segue o link para prosseguir com a recuperação da password da conta {{user_data.email}} do SASocial.  {{sasocial_link}}/auth/new-password/{{recovery_token}}",
            data: '{}',
        }),
		createIfNotExistAlert(knex, "AUTH_PASSWORD_CLOSE_EXPIRATION_PERIOD", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "Prazo próximo para atualização de password",
            description: "Prazo de password expirar próximo da data",
            subject: "SASocial - Prazo de expiração de password próximo",
            message:
                `<p>Olá {{user_data.name}},</p>
				<p>  faltam {{number_of_days}} dias para a password da conta SASocial ({{user_data.email}}) expirar por favor altere a password na pagina <a href="{{sasocial_link}}">SASocial</a>. <br>  Após o prazo ser expirado só poderá entrar na conta após fazer recuperar password.

					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}" target="_blank">SASocial</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá {{user_data.name}}, faltam {{number_of_days}} dias para a password da conta SASocial ({{user_data.email}}) expirar por favor altere a password na pagina {{sasocial_link}}. Após o prazo ser expirado só poderá entrar na conta após fazer recuperar password.",
            data: '{"number_of_days":"-"}',
        }),
		createIfNotExistAlert(knex, "AUTH_PASSWORD_EXPIRATED_PERIOD", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "Prazo de password expirado",
            description: "Data limite para alterar a password foi atingido",
            subject: "SASocial - Prazo de expiração de password foi atingido",
            message:
                `<p>Olá {{user_data.name}},</p>
				<p>a password da conta SASocial ({{user_data.email}}) expirou devido a ter atingido a data limite para alteração de password.<br>Para voltar aceder deve fazer recuprar a password.

					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}" target="_blank">SASocial</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá {{user_data.name}}, a password da conta SASocial ({{user_data.email}}) expirou devido a ter atingido a data limite para alteração de password. Para voltar aceder deve fazer recuperar password.",
            data: '{}',
        }),
		createIfNotExistAlert(knex, "AUTH_PASSWORD_DISABLED_ACCOUNT", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "Conta desativada por 180 dias de password expirada",
            description: "Conta desativada por 180 dias de password expirada",
            subject: "SASocial - Conta desativada",
            message:
                `<p>Olá {{user_data.name}},</p>
				<p>a conta SASocial ({{user_data.email}}) foi desativa devido a password ter expirado a 180 dias.<br> Para voltar aceder deve contactar os serviços.

					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}" target="_blank">SASocial</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá {{user_data.name}}, a conta SASocial ({{user_data.email}}) foi desativa devido a password ter expirado a 180 dias. Para voltar aceder deve contactar os serviços.",
            data: '{}',
        }),
		createIfNotExistAlert(knex, "AUTH_SEND_RANDOM_PIN", {
            service_id: AUTH_SERVICE_ID,
            alert_template_id: 1,
            name: "SASocial - Pin de acesso",
            description: "Envio de pin de acesso aleatório",
            subject: "SASocial - Pin de Acesso",
            message:
                `<p>Olá {{user_data.name}},</p>
				<p> foi pedido um PIN acesso a tua conta SASocial ({{user_data.email}}). <br>
				 o seu PIN foi alterado para  <b>{{pin}}</b>
					<p class="support_message">
						Para mais informações contacte a equipa de suporte através do email <a class="email_link"
							href="mailto:help@sasocial.pt">help@sasocial.pt</a>.</p>
					<p>Este e-mail não é supervisionado pelo que as respostas a esta mensagem não serão lidas.<br>

								Para contactar os SAS deverá utilizar os canais alternativos presentes em <a href="#" target="_blank">Contactos</a></p>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
						<tbody>
							<tr>
								<td align="left">
									<table role="presentation" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td> <a href="{{sasocial_link}}" target="_blank">SASocial</a> </td>
											</tr>
											<tr>
												<td>

												</td>
											</tr>
										</tbody>
									</table>`,
            simplified_message: "Olá {{user_data.name}}, foi pedido um PIN de acesso a sua conta SASocial ({{user_data.email}}), o seu PIN foi alterado para {{pin}} .",
            data: '{ "pin": "****" }',
        }),
    ]);
};
