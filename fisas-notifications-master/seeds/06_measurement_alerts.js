const createIfNotExistAlert = require("./utils/createIfNotExist");
const MEASUREMENT_SERVICE_ID = 26;




exports.seed = (knex) => {
    return Promise.all([
        createIfNotExistAlert(knex, "MEASUREMENT_VALUE_ALERT_MIN", {
            service_id: MEASUREMENT_SERVICE_ID,
            alert_template_id: 1,
            name: "Gestão Sensores: Valor fora do padrão",
            description: "Envia alerta que foram detetados valores a baixo do padrão mínimo.",
            subject: "Detetado valor a baixo do valor mínimo ",
            message:
                '<p class="message"><br>Caro(a) {{name}}, foi detetada uma leitura no sensor do equipamento {{equipment}} a baixo do limite mínimo de referência. </p> ',
            simplified_message: "{{name}}, o sensor do equipamento {{equipment}} apresentou uma leitura a baixo do valor mínimo.",
            data: '{"name":"", "equipment":""}',
        }),

        createIfNotExistAlert(knex, "MEASUREMENT_VALUE_ALERT_MAX", {
            service_id: MEASUREMENT_SERVICE_ID,
            alert_template_id: 1,
            name: "Gestão Sensores: Valor fora do padrão",
            description: "Envia alerta que foram detetados valores a baixo do padrão máximo.",
            subject: "Detetado valor a baixo do valor máximo ",
            message:
                '<p class="message"><br>Caro(a) {{name}}, foi detetada uma leitura no sensor do equipamento {{equipment}} a acima do limite máximo de referência. </p> ',
            simplified_message: "{{name}}, o sensor do equipamento {{equipment}} apresentou uma leitura a cima do valor máximo.",
            data: '{"name":"", "equipment":""}',
        }),
        
    ]);
};


