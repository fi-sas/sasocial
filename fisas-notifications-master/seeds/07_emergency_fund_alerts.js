const createIfNotExistAlert = require("./utils/createIfNotExist");
const EMERGENCY_FUND_SERVICE_ID = 29;
exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_SUBMITTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura submetida",
			description: "Envia alerta - candidatura submetida.",
			subject: "Candidatura submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência foi submetida.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência foi submetida.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_ANALYZED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura em fase de análise",
			description: "Envia alerta - candidatura em fase de análise.",
			subject: "Candidatura em fase de análise",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de análise.</p>',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de análise.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_CANCELLED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura cancelada",
			description: "Envia alerta - candidatura cancelada.",
			subject: "Candidatura cancelada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência foi cancelada.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência foi cancelada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_INTERVIEWED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura em fase de entrevista",
			description: "Envia alerta - candidatura em fase de entrevista.",
			subject: "Candidatura em fase de entrevista",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de entrevista.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de entrevista.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_APPROVED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura aprovada",
			description: "Envia alerta - candidatura aprovada.",
			subject: "Candidatura aprovada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência foi aprovada.</p>',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência foi aprovada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_REJECTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura rejeitada",
			description: "Envia alerta - candidatura rejeitada.",
			subject: "Candidatura rejeitada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência foi rejeitada.</p>',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência foi rejeitada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_CLOSED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura fechada",
			description: "Envia alerta - candidatura fechada.",
			subject: "Candidatura fechada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência foi fechada.</p>',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência foi fechada.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_STATUS_ORDERED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura em fase de despacho",
			description: "Envia alerta - candidatura em fase de despacho.",
			subject: "Candidatura em fase de despacho",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de despacho.</p> ',
			simplified_message: "{{name}}, a sua candidatura ao Fundo de Emergência encontra-se em fase de despacho.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_SCHEDULE_INTERVIEW", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura - agendada entrevista",
			description: "Envia alerta - agendada entrevista para a candidatura.",
			subject: "Candidatura - agendada entrevista",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua entrevista ao Fundo de Emergência foi agendada para a data {{date}} no local {{local}}.</p>',
			simplified_message: "{{name}}, a sua entrevista foi agendada.",
			data: '{"name":"", "local":"", "date":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_ADD_COMPLAIN", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura - registada reclamação",
			description: "Envia alerta - registada reclamação para a candidatura.",
			subject: "Candidatura - registada reclamação",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua reclamação ao Fundo de Emergência foi registada: {{complain_reason}}.</p>',
			simplified_message: "{{name}}, a sua reclamação foi registada.",
			data: '{"name":"", "complain_reason":""}',
		}),		
		createIfNotExistAlert(knex, "EMERGENCY_FUND_COMPLAIN_RESPONSE", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Reclamação respondida",
			description: "Envia alerta - reclamação respondida.",
			subject: "Reclamação respondida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua reclamação foi respondida, com a seguinte resposta : {{response}}.</p>',
			simplified_message: "{{name}}, a sua reclamação foi respondida.",
			data: '{"name":"", "response":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_EXPENSE_STATUS_SUBMITTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Despesa submetida",
			description: "Envia alerta - despesa submetida.",
			subject: "Despesa submetida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa foi submetida.</p> ',
			simplified_message: "{{name}}, a sua despesa foi submetida.",
			data: '{"name":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_EXPENSE_STATUS_VALIDATED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Despesa validada",
			description: "Envia alerta - despesa validada.",
			subject: "Despesa validada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa foi validada.</p> ',
			simplified_message: "{{name}}, a sua despesa foi validada.",
			data: '{"name":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_EXPENSE_STATUS_PROCESSED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Despesa processada",
			description: "Envia alerta - despesa processada.",
			subject: "Despesa processada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa foi processada.</p> ',
			simplified_message: "{{name}}, a sua despesa foi processada.",
			data: '{"name":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_EXPENSE_STATUS_CORRECTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Despesa corrigida",
			description: "Envia alerta - despesa corrigida.",
			subject: "Despesa corrigida",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa foi corrigida.</p> ',
			simplified_message: "{{name}}, a sua despesa foi corrigida.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_EXPENSE_STATUS_PAID", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Despesa paga",
			description: "Envia alerta - despesa paga.",
			subject: "Despesa paga",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa foi paga.</p> ',
			simplified_message: "{{name}}, a sua despesa foi paga.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_ADD_EXPENSE", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura - registada despesa",
			description: "Envia alerta - registada despesa para a candidatura.",
			subject: "Candidatura - registada despesa",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa relativa ao Fundo de Emergência foi registada: {{document_number}}. </p>',
			simplified_message: "{{name}}, a sua despesa foi registada.",
			data: '{"name":"", "document_number":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_STATUS_SUBMITTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de candidatura submetido",
			description: "Envia alerta - pedido de alteração de candidatura submetido.",
			subject: "Pedido de alteração de candidatura submetido",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi submetido.</p> ',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi submetido.",
			data: '{"name":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_STATUS_CANCELLED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de candidatura cancelado",
			description: "Envia alerta - pedido de alteração de candidatura cancelado.",
			subject: "Pedido de alteração de candidatura cancelado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi cancelado.</p> ',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi cancelado.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_STATUS_REJECTED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de candidatura rejeitado",
			description: "Envia alerta - pedido de alteração de candidatura rejeitado.",
			subject: "Pedido de alteração de candidatura rejeitado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi rejeitado.</p> ',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi rejeitado.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_STATUS_APPROVED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de candidatura aprovado",
			description: "Envia alerta - pedido de alteração de candidatura aprovado.",
			subject: "Pedido de alteração de candidatura aprovado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi aprovado.</p> ',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi aprovado.",
			data: '{"name":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_ADD_APPLICATION_CHANGE", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido alteração de candidatura registado",
			description: "Envia alerta - pedido de alteração de candidatura registado.",
			subject: "Pedido de alteração de candidatura registado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi registado: {{application_change_exposition}}.</p>',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi registado.",
			data: '{"name":"", "application_change_exposition":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_INTENDED_SUPPORT", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de apoio registado",
			description: "Envia alerta - pedido de alteração de apoio registado.",
			subject: "Pedido de alteração de apoio registado",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de apoio foi registado: {{support_description}}.</p>',
			simplified_message: "{{name}}, o seu pedido de alteração de apoio foi registado.",
			data: '{"name":"", "support_description":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_RESPONSE", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Pedido de alteração de candidatura respondido",
			description: "Envia alerta - pedido de alteração de candidatura respondido.",
			subject: "Pedido de alteração de candidatura respondido",
			message:
				'<p class="message"><br>Caro(a) {{name}}, o seu pedido de alteração de candidatura foi respondido, com a seguinte resposta : {{response}}.</p>',
			simplified_message: "{{name}}, o seu pedido de alteração de candidatura foi respondido.",
			data: '{"name":"", "response":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_CHANGE_EXPENSE", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Candidatura - despesa alterada",
			description: "Envia alerta - despesa alterada.",
			subject: "Candidatura - despesa alterada",
			message:
				'<p class="message"><br>Caro(a) {{name}}, a sua despesa relativa ao Fundo de Emergência foi alterada: {{document_number}}.</p>',
			simplified_message: "{{name}}, a sua despesa foi alterada.",
			data: '{"name":"", "document_number":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_ADD_PAYMENT_MAP", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Mapa de pagamento registado",
			description: "Envia alerta - mapa de pagamento registado.",
			subject: "Mapa de pagamento registado",
			message:
				'<p class="message"><br>Foi registado um mapa de pagamento referente ao mês {{month}}, do ano {{year}} </p>',
			simplified_message: "Foi registado um mapa de pagamento.",
			data: '{"month":"", "year":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_APPLICATION_REMOVE_PAYMENT_MAP", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Mapa de pagamento removido",
			description: "Envia alerta - mapa de pagamento removido.",
			subject: "Mapa de pagamento removido",
			message:
				'<p class="message"><br>Foi removido um mapa de pagamento referente ao mês {{month}}, do ano {{year}} </p>',
			simplified_message: "Foi removido um mapa de pagamento.",
			data: '{"month":"", "year":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_PAYMENT_MAP_STATUS_GENERATED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Mapa de pagamento gerado",
			description: "Envia alerta - mapa de pagamento gerado.",
			subject: "Mapa de pagamento gerado",
			message:
				'<p class="message"><br>Foi gerado um mapa de pagamento referente ao mês {{month}}, do ano {{year}} </p>',
			simplified_message: "Foi gerado um mapa de pagamento.",
			data: '{"month":"", "year":""}',
		}),	
		createIfNotExistAlert(knex, "EMERGENCY_FUND_PAYMENT_MAP_STATUS_VALIDATED", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Mapa de pagamento validado",
			description: "Envia alerta - mapa de pagamento validado.",
			subject: "Mapa de pagamento validado",
			message:
				'<p class="message"><br>Foi validado um mapa de pagamento referente ao mês {{month}}, do ano {{year}} </p>',
			simplified_message: "Foi validado um mapa de pagamento.",
			data: '{"month":"", "year":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_PAYMENT_MAP_STATUS_PAID", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Mapa de pagamento pago",
			description: "Envia alerta - mapa de pagamento pago.",
			subject: "Mapa de pagamento pago",
			message:
				'<p class="message"><br>Foi pago um mapa de pagamento referente ao mês {{month}}, do ano {{year}} </p>',
			simplified_message: "Foi pago um mapa de pagamento.",
			data: '{"month":"", "year":""}',
		}),
		createIfNotExistAlert(knex, "EMERGENCY_FUND_REMOVE_REPORT", {
			service_id: EMERGENCY_FUND_SERVICE_ID,
			alert_template_id: 1,
			name: "Fundo de Emergência: Relatório removido",
			description: "Envia alerta - relatório removido.",
			subject: "Relatório removido",
			message:
				'<p class="message"><br>Foi removido o relatório - {{report_type_description}} com data de início {{start_date}} e data de fim {{end_date}} </p>',
			simplified_message: "Foi removido um relatório.",
			data: '{"report_type_description":"", "start_date":"", "end_date":""}',
		}),

	]);
};
