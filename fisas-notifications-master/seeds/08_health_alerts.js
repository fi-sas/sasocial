const createIfNotExistAlert = require("./utils/createIfNotExist");
const HealthServiceId = 7;
exports.seed = (knex) => {
	return Promise.all([
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_APPROVED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Marcação Aprovada",
			description: "Envia alerta que a marcação de consulta foi aprovada",
			subject: "Marcação de Consulta Aprovada",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua marcação de {{appointment.specialty_name}} para o dia {{appointment.date}} às {{appointment.start_hour}} foi aprovada.</p>`,
			simplified_message: "{{appointment.name}}, a sua marcação de consulta foi aprovada.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_PENDING", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Pedido de Marcação em Análise",
			description: "Envia alerta que a marcação de consulta encontra-se em análise",
			subject: "Pedido de Marcação de Consulta em Análise",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, o seu pedido marcação de {{appointment.specialty_name}} para o dia {{appointment.date}} às {{appointment.start_hour}} encontra-se em análise.</p>`,
			simplified_message: "{{appointment.name}},o seu pedido de marcação de consulta encontra-se em análise.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_CANCELLED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Marcação Cancelada",
			description: "Envia alerta que a marcação de consulta foi cancelada",
			subject: "Marcação de Consulta Cancelada",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua marcação de {{appointment.specialty_name}} para o dia {{appointment.date}} às {{appointment.start_hour}} foi cancelada.</p>`,
			simplified_message: "{{appointment.name}},o sua marcação de consulta foi cancelada.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_ABSENCE", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Falta na Marcação",
			description: "Envia alerta que o utente faltou a marcação de consulta",
			subject: "Falta na Marcação",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua marcação de {{appointment.specialty_name}} para o dia {{appointment.date}} às {{appointment.start_hour}} ficou registada como falta.</p>`,
			simplified_message: "{{appointment.name}},o faltou a marcação da consulta.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_CHANGED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Marcação Alterada",
			description: "Envia alerta que a marcação de consulta foi alterada",
			subject: "Marcação de Consulta Alterada",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua marcação de {{appointment.specialty_name}} para o dia {{appointment.date}} às {{appointment.start_hour}} foi alterada para o dia {{appointment.new_date}} às {{appointment.new_start_hour}}.</p>`,
			simplified_message: "{{appointment.name}},o sua marcação de consulta foi alterada.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_APPOINTMENT_STATUS_CLOSED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Consulta Concluída",
			description: "Envia alerta que a consulta foi concluída",
			subject: "Consulta Concluída",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua consulta de {{appointment.specialty_name}} no dia {{appointment.date}} às {{appointment.start_hour}} foi concluída.</p>`,
			simplified_message: "{{appointment.name}},o sua consulta foi concluída.",
			data: '{}',
		}),

		createIfNotExistAlert(knex, "HEALTH_STATUS_COMPLAIN_SUBMITED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Reclamação Submetida",
			description: "Envia alerta que a reclamação foi submetida",
			subject: "Reclamação Submetida",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua reclamação relativa a consulta de {{appointment.specialty_name}} foi submetida.</p>`,
			simplified_message: "{{appointment.name}},o sua reclamação foi submetida.",
			data: '{}',
		}),
		createIfNotExistAlert(knex, "HEALTH_STATUS_COMPLAIN_ANSWERED", {
			service_id: HealthServiceId,
			alert_template_id: 1,
			name: "Saúde: Reclamação Respondida/Fechada",
			description: "Envia alerta que a reclamação foi respondida e fechada",
			subject: "Reclamação Respondida/Fechada",
			message: `<td>
			<p class="message"><br>Caro(a) {{appointment.name}}, a sua reclamação relativa a consulta de {{appointment.specialty_name}} foi respondida e fechada.</p>`,
			simplified_message: "{{appointment.name}},o sua reclamação foi respondida e fechada.",
			data: '{}',
		}),
	]);
};
