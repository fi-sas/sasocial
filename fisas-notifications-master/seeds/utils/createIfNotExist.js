
const createIfNotExistAlert = (knex, key, alertType, alertTemplate) =>
knex("alert_type")
    .select()
    .where("key", key)
    .then(async (rows) => {
        if (rows.length === 0) {
            // no matching records found
            // Insert alert type and example related alert template
            const a = await knex("alert_type").insert(
                Object.assign(alertType, {
                    key,
                    updated_at: new Date(),
                    created_at: new Date(),
                }),
            );

            return knex("alert_template").insert(
                Object.assign(alertTemplate, {
                    alert_type_id: a.pop(),
                    updated_at: new Date(),
                    created_at: new Date(),
                }),
            );
        }
        return true;
    })
    .catch(() => {
        // you can find errors here.
    });

module.exports = createIfNotExistAlert;