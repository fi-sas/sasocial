"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const promiseSeries = require("promise.series");
const gateway_paths = require("./utils/gateway_paths");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alerts",
	table: "alert",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alerts")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"alert_type_id",
			"user_id",
			"data",
			"status",
			"variables",
			"user_data",
			"external_uuid",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["medias", "notifications", "type", "history"],
		withRelateds: {
			notifications(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "notifications.notifications", "notifications", "id", "alert_id");
			},
			medias(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "notifications.alert-medias", "medias", "id", "alert_id");
			},
			type(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "notifications.alert-types", "type", "alert_type_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "notifications.alert-history", "history", "id", "alert_id");
			},
		},
		entityValidator: {
			alert_type_id: { type: "number", min: 1, convert: true, optional: true },
			user_id: { type: "number", min: 1, convert: true, optional: true },
			data: { type: "object", strict: false, optional: true },
			status: { type: "enum", values: ["NEW", "PENDING", "NOTIFIED", "READ", "ERROR"] },
			variables: { type: "object", strict: false, optional: true },
			user_data: { type: "object", strict: false, optional: true },
			external_uuid: { type: "uuid", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "NEW";
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			create_alert: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.created_at = new Date();
					ctx.params.status = "NEW";
				},
			],
		},
		after: {
			find: ["parseData"],
			list: ["parseData"],
			get: ["parseData"],
			create: ["parseData"],
			create_alert: ["parseData"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create_bulk_alert: {
			params: {
				notification_method_ids: {
					type: "array",
					min: 1,
					items: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
				// alert_template_id: { type: "number", min: 0, convert: true },
				uuid: { type: "uuid" },
				subject: { type: "string" },
				message: { type: "string" },
				simplified_message: { type: "string" },
				target_id: { type: "number", min: 0, convert: true },
			},
			async handler(ctx) {
				const notification_double_render = true;
				const override_notifcation_methods = await ctx
					.call("notifications.notification-methods.find", {
						query: {
							id: ctx.params.notification_method_ids,
						},
					})
					.then((methods) =>
						methods.map((nm) => ({
							notification_method: nm,
							notification_method_id: nm.id,
							priority: 3,
						})),
					);

				// GET USER IDS
				let user_ids = await ctx.call("authorization.users.get_users_by_target_id", {
					target_id: ctx.params.target_id,
				});

				user_ids = user_ids.map((uid) => uid.id);

				const alert_type_generic = await ctx.call("notifications.alert-types.count", {
					query: {
						key: "GENERIC",
					},
				});
				if (alert_type_generic === 0) {
					throw new Errors.ValidationError(
						"No generic alert type found to procced bulk notifications",
						"NOTIFICATION_NO_GENERIC_ALERT_TYPE",
						{},
					);
				}

				const alerts = user_ids.map((uid) => {
					return () => {
						return new Promise((resolve) => {
							this.broker
								.call(
									"notifications.alerts.create_alert",
									{
										alert_type_key: "GENERIC",
										user_id: uid,
										data: {
											subject: ctx.params.subject,
											simplified_message: ctx.params.simplified_message,
											message: ctx.params.message,
										},
										user_data: {},
										variables: {},
										external_uuid: ctx.params.uuid,
										medias: [],
									},
									{
										meta: {
											notification_double_render,
											override_notifcation_methods,
										},
									},
								)
								.then(() => {
									resolve(true);
								})
								.catch(() => {
									resolve(false);
								});
						});
					};
				});

				// each item returns a Promise
				promiseSeries(alerts);

				return true;
			},
		},
		retry: {
			// REMOVE THIS ON PRODUCTION
			rest: "POST /:alert_id/retry",
			scope: "notifications:alerts:create",
			visibility: "published",
			params: {
				alert_id: { type: "number", integer: true, convert: true, min: 0 },
			},
			async handler(ctx) {
				// GET NOTIFICATIONS THAT FAILED IN THE ALERT

				const alert = await this._get(ctx, {
					id: ctx.params.alert_id,
				});

				const user = await ctx.call("authorization.users.get", {
					id: alert[0].user_id,
				});

				return ctx
					.call("notifications.notifications.find", {
						query: {
							alert_id: ctx.params.alert_id,
							status: ["ERROR", "PENDING"],
						},
					})
					.then((notifications) => {
						notifications.map((n) => {
							return ctx
								.call(`${gateway_paths[n.notificationMethod.service]}.send`, {
									notification_id: n.id,
									configs: n.notificationMethod.configs,
									user_data: {
										email: user[0].email,
										phone: user[0].phone,
									},
									subject: n.subject,
									message: n.message,
									attachments: [],
								})
								.then(() => {
									return ctx.emit("notifications.alerts.trackHistory", {
										alert_id: alert[0].id,
										status: "PENDING",
										notes: `Notification process started. Trying ${n.notificationMethod.name} [1] notification method.`,
									});
								})
								.catch((err) => {
									return ctx.call("notifications.notifications.patch", {
										id: n.id,
										status: "ERROR",
										error_message: err.message,
									});
								});
						});
					});
			},
		},
		create_alert: {
			rest: "POST /:alert_type_key",
			visibility: "published",
			scope: "notifications:alerts:create",
			timeout: 0,
			params: {
				alert_type_key: {
					type: "string",
				},
				user_id: { type: "number", positive: true, convert: true, optional: true },
				user_data: { type: "object", strict: false, optional: true },
				data: { type: "object", strict: false, optional: true },
				variables: { type: "object", strict: false, optional: true },
				external_uuid: { type: "uuid", optional: true },
				medias: {
					type: "array",
					items: {
						type: "object",
						props: {
							type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
							key: { type: "string", empty: false },
							file_id: { type: "number", positive: true, convert: true },
						},
					},
					optional: true,
				},
			},
			async handler(ctx) {
				if (ctx.params.user_id) {
					await ctx.call("authorization.users.get", { id: ctx.params.user_id });
				} else {
					// NO USER ID? SO TAKE THE GUEST ID
					ctx.params.user_id = 1;

					//BUT PROVIDE  MUST USER_DATA
					// TODO CHECK USER_DATA ?
				}

				if (!ctx.params.data) {
					ctx.params.data = {};
				}

				const alert_type = await ctx.call("notifications.alert-types.find", {
					query: {
						key: ctx.params.alert_type_key,
					},
				});

				if (alert_type.length === 0) {
					throw new Errors.EntityNotFoundError(
						"notifications.create_alert.alert-types",
						ctx.params.alert_type_key,
					);
				}

				delete ctx.params.alert_type_key;
				ctx.params.alert_type_id = alert_type[0].id;
				return this._create(ctx, ctx.params).then((alert) => {
					ctx.emit("notifications.alerts.trackHistory", {
						alert_id: alert[0].id,
						status: "NEW",
						notes: "New Alert",
					});
					return this.saveMedias(ctx, alert);
				});
			},
		},
		update_status: {
			params: {
				alert_id: { type: "number", positive: true, integer: true, convert: true },
			},
			handler(ctx) {
				return ctx
					.call("notifications.notifications.find", {
						fields: ["id", "status"],
						query: {
							alert_id: ctx.params.alert_id,
						},
					})
					.then((notifications) => {
						const totalNotifications = notifications.length;

						if (notifications.filter((n) => n.status === "PENDING").length > 0) {
							return ctx.call("notifications.alerts.patch", {
								id: ctx.params.alert_id,
								status: "PENDING",
							});
						}

						if (notifications.filter((n) => n.status === "ERROR").length > 0) {
							return ctx.call("notifications.alerts.patch", {
								id: ctx.params.alert_id,
								status: "ERROR",
							});
						}

						if (notifications.filter((n) => n.status === "SUCCESS").length === totalNotifications) {
							return ctx.call("notifications.alerts.patch", {
								id: ctx.params.alert_id,
								data: {},
								status: "NOTIFIED",
							});
						}
					});
			},
		},
		getService: {
			visibility: "public",
			cache: {
				keys: ["id"],
			},
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const alert = await ctx.call("notifications.alerts.get", {
					id: ctx.params.id,
					withRelated: false,
				});
				return ctx
					.call("notifications.alert-types.get", {
						id: alert[0].alert_type_id,
						withRelated: "service",
					})
					.then((alert_type) => {
						return alert_type[0].service;
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async saveMedias(ctx, response) {
			if (ctx.params.medias && Array.isArray(ctx.params.medias)) {
				const medias = [...new Set(ctx.params.medias)];
				await ctx.call("notifications.alert-medias.save_medias_of_alert", {
					alert_id: response[0].id,
					medias,
				});
				response[0].mediaIds = await ctx.call("notifications.alert-medias.find", {
					query: {
						alert_id: response[0].id,
					},
				});
			}

			return response;
		},
		parseData(ctx, res) {
			if (Array.isArray(res)) {
				res.map((r) => {
					if (r.data) r.data = typeof r.data === "string" ? JSON.parse(r.data) : r.data;
					if (r.variables)
						r.variables = typeof r.variables === "string" ? JSON.parse(r.variables) : r.variables;
					if (r.user_data)
						r.user_data = typeof r.user_data === "string" ? JSON.parse(r.user_data) : r.user_data;
				});
			} else {
				if (res.data) {
					res.data = typeof res.data === "string" ? JSON.parse(res.data) : res.data;
				}
				if (res.variables) {
					res.variables =
						typeof res.variables === "string" ? JSON.parse(res.variables) : res.variables;
				}
				if (res.data) {
					res.user_data =
						typeof res.user_data === "string" ? JSON.parse(res.user_data) : res.user_data;
				}
			}

			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
