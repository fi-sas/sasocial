"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-history",
	table: "alert_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-history")],

	/**
   * Settings
   */
	settings: {
		fields: ["id", "alert_id", "status", "notes", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: [],
		entityValidator: {
			alert_id: { type: "number", min: 1, convert: true },
			status: { type: "enum", values: ["NEW", "PENDING", "NOTIFIED", "READ", "ERROR"] },
			notes: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {},

	/**
   * Events
   */
	events: {
		"notifications.alerts.trackHistory"(ctx) {
			this.logger.debug("CAPTURED EVENT: notifications.alerts.trackHistory");
			this._create(ctx, {
				...ctx.params,
				created_at: new Date(),
				updated_at: new Date()
			});
		}
	},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
