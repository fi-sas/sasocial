"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-medias",
	table: "alert_media",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-medias")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"alert_id",
			"file_id",
			"type",
			"key",
		],
		defaultWithRelateds: [
			"file"
		],
		withRelateds: {
			"file"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			alert_id: { type: "number", min: 1 },
			file_id: { type: "number", min: 1 },
			type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
			key: { type: "string", max: 250 },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		save_medias_of_alert: {
			visibility: "public",
			params: {
				alert_id: { type: "number", positive: true, integer: true, convert: true },
				medias: {
					type: "array",
					item: {
						type: "object", props: {
							type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
							key: { type: "string", empty: false },
							file_id: { type: "number", positive: true, convert: true }
						}
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					alert_id: ctx.params.alert_id,
				});
				this.clearCache();

				const entities = ctx.params.medias.map((media) => ({
					alert_id: ctx.params.alert_id,
					type: media.type,
					key: media.key,
					file_id: media.file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
