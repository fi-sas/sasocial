"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-notification-method",
	table: "alert_notification_method",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-notification-method")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "alert_type_id", "notification_method_id", "priority"],
		defaultWithRelateds: ["notification_method"],
		withRelateds: {
			notification_method(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"notifications.notification-methods",
					"notification_method",
					"notification_method_id",
				);
			},
		},
		entityValidator: {
			alert_type_id: { type: "number", min: 1 },
			notification_method_id: { type: "number", min: 1 },
			priority: { type: "number", min: 1 },
		},
	},
	hooks: {
		before: {
			create: [],
			update: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		fetch_notification_methods_by_type: {
			visibility: "public",
			params: {
				alert_type_id: {
					type: "number",
					convert: true,
				},
			},
			async handler(ctx) {
				return this._find(ctx, {
					withRelated: false,
					query: {
						alert_type_id: ctx.params.alert_type_id,
					},
				}).then((res) => {
					return ctx.call("notifications.notification-methods.get", {
						id: res.map((r) => r.notification_method_id),
					});
				});
			},
		},
		save_notifications_methods_of_type: {
			visibility: "public",
			params: {
				alert_type_id: { type: "number", positive: true, integer: true, convert: true },
				notification_methods: {
					type: "array",
					items: {
						type: "object",
						props: {
							notification_method_id: { type: "number", positive: true, convert: true },
							priority: { type: "number", positive: true, convert: true },
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					alert_type_id: ctx.params.alert_type_id,
				});
				this.clearCache();

				const entities = ctx.params.notification_methods.map((nm) => ({
					alert_type_id: ctx.params.alert_type_id,
					notification_method_id: nm.notification_method_id,
					priority: nm.priority,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
