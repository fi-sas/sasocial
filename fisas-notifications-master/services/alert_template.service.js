"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-templates",
	table: "alert_template",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-templates")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"name",
			"data",
			"template",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"medias"
		],
		withRelateds: {
			"medias"(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "notifications.alert-templates-medias", "medias", "id", "alert_template_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 120 },
			template: { type: "string" },
			data: { type: "object" },
			medias: { type: "array", items: {
				type: "object", props: {
					type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
					key: { type: "string", empty: false },
					file_id: { type: "number", positive: true, convert: true }
				}
			} },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			find: [
				"parseData"
			],
			list: [
				"parseData"
			],
			get: [
				"parseData"
			],
			create: [
				"saveMedias",
			],
			update: [
				"saveMedias",
			]
		}
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {
		async saveMedias(ctx, response) {
			if (ctx.params.medias && Array.isArray(ctx.params.medias)) {
				const medias = [...new Set(ctx.params.medias)];
				await ctx.call("notifications.alert-templates-medias.save_medias_of_template", {
					alert_template_id: response[0].id,
					medias,
				});
				response[0].mediaIds = await ctx.call("notifications.alert-templates-medias.find", {
					query: {
						alert_template_id: response[0].id,
					}
				});
			}
			return response;
		},
		parseData(ctx, res) {
			if (Array.isArray(res)) {
				res.map(r => {
					if (r.data)
						r.data = JSON.parse(r.data);
				});
			} else if (res.data) {
				res.data = JSON.parse(res.data);
			}

			return res;
		}
	},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
