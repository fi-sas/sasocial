"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-templates-medias",
	table: "alert_template_media",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-templates-medias")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"alert_template_id",
			"file_id",
			"type",
			"key",
		],
		defaultWithRelateds: [
			"file"
		],
		withRelateds: {
			"file"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			alert_template_id: { type: "number", positive: true, convert: true },
			type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
			key: { type: "string", empty: false },
			file_id: { type: "number", positive: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
			],
			update: [
			],
		},
	},
	/**
	* Dependencies
	*/
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		save_medias_of_template: {
			visibility: "public",
			params: {
				alert_template_id: { type: "number", positive: true, integer: true, convert: true },
				medias: {
					type: "array",
					item: {
						type: "object", props: {
							type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
							key: { type: "string", empty: false },
							file_id: { type: "number", positive: true, convert: true }
						}
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					alert_template_id: ctx.params.alert_template_id,
				});
				this.clearCache();

				const entities = ctx.params.medias.map((media) => ({
					alert_template_id: ctx.params.alert_template_id,
					type: media.type,
					key: media.key,
					file_id: media.file_id,
				}));
				return this._insert(ctx, { entities });
			},
		},
	},

	/**
	* Events
	*/
	events: {},

	/**
	* Methods
	*/
	methods: {},

	/**
	* Service created lifecycle event handler
	*/
	created() { },

	/**
	* Service started lifecycle event handler
	*/
	async started() { },

	/**
	* Service stopped lifecycle event handler
	*/
	async stopped() { },
};
