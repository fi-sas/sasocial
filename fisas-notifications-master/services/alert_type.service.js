"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.alert-types",
	table: "alert_type",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "alert-types")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"service_id",
			"alert_template_id",
			"key",
			"name",
			"description",
			"subject",
			"simplified_message",
			"message",
			"data",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["medias", "service", "notification_methods", "template"],
		withRelateds: {
			medias(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"notifications.alert-types-medias",
					"medias",
					"id",
					"alert_type_id",
				);
			},
			notification_methods(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"notifications.alert-notification-method",
					"notification_methods",
					"id",
					"alert_type_id",
				);
			},
			template(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "notifications.alert-templates", "template", "alert_template_id");
			},
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.services", "service", "service_id");
			},
		},
		entityValidator: {
			service_id: { type: "number" },
			alert_template_id: { type: "number" },
			key: { type: "string" },
			name: { type: "string" },
			description: { type: "string" },
			subject: { type: "string" },
			simplified_message: { type: "string" },
			message: { type: "string" },
			data: { type: "object" },
			endpoint_url: { type: "string", optional: true },
			medias: {
				type: "array",
				items: {
					type: "object",
					props: {
						type: { type: "enum", values: ["IMAGE", "ATTACHMENT"] },
						key: { type: "string", empty: false },
						file_id: { type: "number", positive: true, convert: true },
					},
				},
			},
			notification_methods: {
				type: "array",
				items: {
					type: "object",
					props: {
						notification_method_id: { type: "number", min: 0, convert: true },
						priority: { type: "number", positive: true, convert: true },
					},
				},
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateService",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateService",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			find: ["parseData"],
			list: ["parseData"],
			get: ["parseData"],
			create: ["saveNotificationsMethods", "saveMedias"],
			update: ["saveNotificationsMethods", "saveMedias"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		simulate: {
			rest: "POST /:id/simulate",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, positive: true },
				data: { type: "object", strict: false, optional: true },
			},
			async handler(ctx) {
				const alert_type = await this._get(ctx, {
					id: ctx.params.id,
				});

				return ctx.call("notifications.alerts.create_alert", {
					alert_type_key: alert_type[0].key,
					user_id: ctx.meta.user.id,
					user_data: {},
					data: ctx.params.data,
					variables: {},
					external_uuid: null,
					medias: [],
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"notifications.alert-templates.*"() {
			this.logger.info("notifications.alert-templates.* event received");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async saveNotificationsMethods(ctx, response) {
			if (ctx.params.notification_methods && Array.isArray(ctx.params.notification_methods)) {
				const notification_methods = [...new Set(ctx.params.notification_methods)];
				await ctx.call(
					"notifications.alert-notification-method.save_notifications_methods_of_type",
					{
						alert_type_id: response[0].id,
						notification_methods,
					},
				);
				response[0].mediaIds = await ctx.call("notifications.alert-notification-method.find", {
					query: {
						alert_type_id: response[0].id,
					},
				});
			}
			return response;
		},
		async saveMedias(ctx, response) {
			if (ctx.params.medias && Array.isArray(ctx.params.medias)) {
				const medias = [...new Set(ctx.params.medias)];
				await ctx.call("notifications.alert-types-medias.save_medias_of_type", {
					alert_type_id: response[0].id,
					medias,
				});
				response[0].mediaIds = await ctx.call("notifications.alert-types-medias.find", {
					query: {
						alert_type_id: response[0].id,
					},
				});
			}
			return response;
		},
		async validateService(ctx) {
			if (_.has(ctx, "params.service_id")) {
				const service = await ctx.call("configuration.services.find", {
					query: {
						id: ctx.params.service_id,
					},
				});
				if (_.isArray(service) && !_.isEmpty(service[0])) {
					ctx.params.service = service[0];
				} else {
					throw new Errors.EntityNotFoundError("service", ctx.params.service_id);
				}
			}
		},
		parseData(ctx, res) {
			if (Array.isArray(res)) {
				res.map((r) => {
					if (r.data) r.data = JSON.parse(r.data);
				});
			} else if (res.data) {
				res.data = JSON.parse(res.data);
			}

			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
