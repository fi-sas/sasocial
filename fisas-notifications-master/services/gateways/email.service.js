"use strict";
const nodemailer = require("nodemailer");
const QueueService = require("moleculer-bull");
const fs = require("fs");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.email",
	mixins: [QueueService(process.env.REDIS_QUEUE)],

	queues: {
		"email.send"(job) {
			this.logger.info("NEW JOB RECEIVED!");

			return this.send(
				job.data.configs,
				job.data.user_data,
				job.data.subject,
				job.data.message,
				job.data.attachments,
			)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.info("ANTES DE MANDAR O REJECT");
					return Promise.reject(err);
				});
		},
	},

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Sends a notification with received configs, subject and message.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * @param {String} subject
		 * @param {String} message
		 */
		send: {
			params: {
				notification_id: { type: "number", positive: true, convert: true },
				configs: {
					type: "object",
					strict: "remove",
					props: {
						host: { type: "string" },
						port: { type: "string" },
						username: { type: "string" },
						password: { type: "string" },
					},
				},
				user_data: {
					type: "object",
					strict: false,
					props: {
						email: { type: "string" },
					},
				},
				subject: { type: "string" },
				message: { type: "string" },
				attachments: { type: "array", items: "object", optional: true },
				priority: { type: "number", default: 1, optional: true },
			},
			async handler(ctx) {
				this.addJob(ctx);
				return this.Promise.resolve(true);
			},
		},
		/**
		 * Validate configurations for sending an email.
		 * Returns true if successful. Otherwise, throws an Error.
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				host: { type: "string" },
				port: { type: "string" },
				username: { type: "string" },
				password: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		addJob(ctx) {
			this.logger.info("CREATE JOB RECEIVED!", ctx.params);
			this.createJob(
				"email.send",
				{ ...ctx.params, id: ctx.params.notification_id, pid: process.pid },
				{
					attempts: 1,
					priority: ctx.params.priority || 1,
					removeOnComplete: true,
					removeOnFail: true,
				},
			);

			return true;
		},
		/**
		 * Sends de Email
		 * @param {*} configs
		 * @param {*} user_data
		 * @param {*} subject
		 * @param {*} message
		 * @param {*} attachments
		 */
		async send(configs, user_data, subject, message, attachments = []) {
			// eslint-disable-next-line security/detect-unsafe-regex
			const email_validator = new RegExp(
				/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
				"g",
			).exec(user_data.email);
			if (!email_validator) {
				throw new Error("Not a valid email!");
			}

			if (process.env.EMAIL_DEBUG === "true") {
				const account = await nodemailer.createTestAccount();
				if (!account) {
					throw new Error("Could not create email test account");
				}

				// override transporter configurations to debug
				configs.username = account.user;
				configs.password = account.pass;
				configs.host = account.smtp.host;
				configs.port = account.smtp.port;
			}

			return new Promise((resolve, reject) => {
				let transporter = null;
				try {
					transporter = this.setupTransport(configs);
				} catch (err) {
					reject(err);
				}

				let email = {
					from: "SASocial < " + configs.username + ">",
					to: user_data.name + " <" + user_data.email + ">",
					subject: subject,
					text: message,
					html: message,
				};

				if (Array.isArray(attachments) && attachments.length > 0) {
					email.attachments = attachments.map((media) => ({
						filename: media.file.filename,
						content: fs.createReadStream("/uploads/".concat(media.file.path)),
					}));
				}

				transporter
					.sendMail(email)
					.then((info) => {
						if (process.env.EMAIL_DEBUG === "true") {
							//this.logger.info("Info:", info);
							this.logger.info("Preview URL: ", nodemailer.getTestMessageUrl(info));
						}

						transporter.close();
						resolve(info);
					})
					.catch((err) => {
						transporter.close();
						reject(err);
					});
			});
		},

		/**
		 * Private Method
		 * Setup Nodemailer transport.
		 */
		setupTransport(configs) {
			return nodemailer.createTransport(
				{
					host: configs.host,
					port: configs.port,
					secure: parseInt(configs.port, 10) === 465,
					auth: {
						user: configs.username,
						pass: configs.password,
					},
					// requireTLS: true,
					logger: process.env.EMAIL_DEBUG === "true",
					debug: process.env.EMAIL_DEBUG === "true",
				},
				{
					headers: {
						"X-Notification-ID": configs.alert_id,
					},
				},
			);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.getQueue("email.send").on("progress", (jobID, progress) => {
			this.logger.info(`Job #${jobID} progress is ${progress}%`);
		});

		this.getQueue("email.send").on("completed", (job, res) => {
			this.logger.info(`Job #${job.data.id} completed!. Result:`, res);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "SUCCESS",
			});
		});

		this.getQueue("email.send").on("failed", (job, err) => {
			this.logger.info(`Job #${job.id} failed!. Result:`, err);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "ERROR",
				error_message: err.message,
			});
		});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
