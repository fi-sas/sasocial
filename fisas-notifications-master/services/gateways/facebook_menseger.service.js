"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.facebook_menseger",
	mixins: [],

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Sends a notification with received configs, subject and message.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * @param {String} subject
		 * @param {String} message
		 */
		send: {
			params: {
				configs: {
					type: "object",
					strict: "remove",
					props: {
						access_token: { type: "string" },
					},
				},
				user_data: { type: "object" },
				subject: { type: "string" },
				message: { type: "string" },
				attachments: { type: "array", items: "object", optional: true },
				priority: { type: "number", default: 1, optional: true },
			},
			handler() {
				/*const recipientId = user_data.fb_messenger_id ? user_data.fb_messenger_id : user_data.phone;

        if (!recipientId) {
          throw new Error(
            "Could not send FB Messenger message: user fb_messenger_id and phone number missing.",
          );
        }

        // TODO use attachments once url is available
        let success = false;
        const messages = [
          {
            text: message,
          },
          // ...Object.keys(attachments).map(k => ({
          //   attachment: {
          //     type: getAttachmentType(attachments[k]),
          //     payload: {
          //       url: attachments[k].url,
          //     },
          //   },
          // }))
        ];

        const batchMessages = JSON.stringify([
          ...messages.map((msg) => {
            const data = {
              messaging_type: "MESSAGE_TAG",
              tag: "NON_PROMOTIONAL_SUBSCRIPTION",
              recipient: JSON.stringify({ id: recipientId }),
              message: JSON.stringify(msg),
            };

            return {
              method: "POST",
              relative_url: "me/messages",
              body: Object.keys(data)
                .map((key) => `${key}=${encodeURIComponent(data[key])}`)
                .join("&"),
            };
          }),
        ]);

        const url = "https://graph.facebook.com/v2.6/";
        const form = new FormData();
        form.append("access_token", configs.access_token);
        form.append("batch", batchMessages);

        const errors = [];
        try {
          const response = await got(url, {
            headers: {},
            method: "POST",
            body: form,
          });

          if (response.body) {
            const body = JSON.parse(response.body);
            body.forEach((partialResponse) => {
              const d = JSON.parse(partialResponse.body);
              if (partialResponse.code !== 200 && d.error) {
                errors.push(d.error);
              }
            });
          }
        } catch (e) {
          if (e.response && e.response.body && e.response.body.error) {
            errors.push(e.response.body.error);
          } else {
            errors.push({ code: e.statusCode });
          }
        }

        if (errors.length) {
          const msg = errors
            .map((error) => `[code:${error.code}][subcode:${error.error_subcode}]${error.message}`)
            .join("|");
          throw new Error(`Failed to send FB Messenger message: ${msg}`);
        } else {
          success = true;
        }

        return success;
        */
			},
		},
		/**
		 * Validate configurations for sending an email.
		 * Returns true if successful. Otherwise, throws an Error.
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				access_token: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Maps Medias type into Facebook recognized attachment types.
		 * Current mapping includes:
		 * - IMAGE -> image
		 * - VIDEO -> video
		 * - DOCUMENT -> file
		 *
		 * @param {Object} attachment Media object
		 */
		getAttachmentType(attachment) {
			if (attachment.type === "IMAGE" || attachment.type === "VIDEO") {
				return attachment.type.toLowerCase();
			}
			return "file";
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
