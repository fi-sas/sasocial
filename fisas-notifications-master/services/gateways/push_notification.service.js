"use strict";
const admin = require("firebase-admin");
const QueueService = require("moleculer-bull");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.push_notifications",
	mixins: [QueueService(process.env.REDIS_QUEUE)],

	queues: {
		"push_notifications.send"(job) {
			this.logger.info("NEW JOB RECEIVED!");

			return this.send(job.data.configs, job.data.user_data, job.data.subject, job.data.message)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.info("ANTES DE MANDAR O REJECT", err);
					return Promise.reject(err);
				});
		},
	},

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Sends a notification with received configs, subject and message.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * @param {Object} user_data
		 * @param {String} subject
		 * @param {String} message
		 * @param {Array} attachments
		 */
		send: {
			params: {
				notification_id: { type: "number", positive: true, convert: true },
				configs: {
					type: "object",
					strict: "remove",
					props: {
						type: { type: "string" },
						project_id: { type: "string" },
						private_key_id: { type: "string" },
						private_key: { type: "string" },
						client_email: { type: "string" },
						client_id: { type: "string" },
						auth_uri: { type: "string" },
						token_uri: { type: "string" },
						auth_provider_x509_cert_url: { type: "string" },
						client_x509_cert_url: { type: "string" },
					},
				},
				user_data: { type: "object" },
				subject: { type: "string" },
				message: { type: "string" },
				attachments: { type: "array", items: "object", optional: true },
				priority: { type: "number", default: 1, optional: true },
			},
			handler(ctx) {
				this.addJob(ctx);
				return this.Promise.resolve(true);
			},
		},
		/**
		 * Validate configurations for sending an push notifications.
		 * Returns true if successful. Otherwise, throws an Error.
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				type: { type: "string" },
				project_id: { type: "string" },
				private_key_id: { type: "string" },
				private_key: { type: "string" },
				client_email: { type: "string" },
				client_id: { type: "string" },
				auth_uri: { type: "string" },
				token_uri: { type: "string" },
				auth_provider_x509_cert_url: { type: "string" },
				client_x509_cert_url: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		addJob(ctx) {
			this.logger.info("CREATE JOB RECEIVED!", ctx.params);
			this.createJob(
				"push_notifications.send",
				{ ...ctx.params, id: ctx.params.notification_id, pid: process.pid },
				{
					attempts: 1,
					priority: ctx.params.priority || 1,
					removeOnComplete: true,
					removeOnFail: true,
				},
			);
			return true;
		},
		/**
		 * Sends de Email
		 * @param {*} configs
		 * @param {*} user_data
		 * @param {*} subject
		 * @param {*} message
		 */
		send(configs, user_data, subject, message) {
			return new Promise((resolve, reject) => {
				let app;
				try {
					app = admin.initializeApp({
						credential: admin.credential.cert(configs),
						databaseURL: configs.databaseURL,
					});
					const messaging = app.messaging();
					const userEmail = user_data.email.replace("@", ".");
					const messageData = {
						notification: {
							title: subject,
							body: message,
						},
						android: {
							notification: {
								sound: "default",
							},
						},
						apns: {
							payload: {
								aps: {
									sound: "default",
								},
							},
						},
						data: {},
						topic: userEmail,
					};
					messaging
						.send(messageData, process.env.PUSH_DEBUG === "true")
						.then((info) => {
							resolve(info);
							app.delete();
						})
						.catch((err) => {
							app.delete();
							reject(err);
						});
				} catch (e) {
					app.delete();
					reject(e);
				}
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.getQueue("push_notifications.send").on("progress", (jobID, progress) => {
			this.logger.info(`Job #${jobID} progress is ${progress}%`);
		});

		this.getQueue("push_notifications.send").on("completed", (job, res) => {
			this.logger.info(`Job #${job.id} completed!. Result:`, res);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "SUCCESS",
			});
		});

		this.getQueue("push_notifications.send").on("failed", (job, err) => {
			this.logger.info(`Job #${job.id} failed!. Result:`, err);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "ERROR",
				error_message: err.message,
			});
		});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
