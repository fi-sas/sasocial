"use strict";
const QueueService = require("moleculer-bull");
const got = require("got");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.sms_ama",
	mixins: [QueueService(process.env.REDIS_QUEUE)],

	queues: {
		"sms.send"(job) {
			this.logger.info("NEW JOB RECEIVED!");

			return this.send(job.data.configs, job.data.user_data, job.data.subject, job.data.message)
				.then(() => {
					this.logger.info("ANTES DE MANDAR O RESOLVE");
					return Promise.resolve({
						done: true,
						id: job.data.id,
						worker: process.pid,
					});
				})
				.catch((err) => {
					this.logger.info("ANTES DE MANDAR O REJECT", err);
					return Promise.reject(err);
				});
		},
	},

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Sends an SMS notification with received configs and message.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * @param {Object} user_data
		 * @param {String} subject
		 * @param {String} message
		 * @param {Array} attachments
		 */
		send: {
			params: {
				notification_id: { type: "number", positive: true, convert: true },
				configs: {
					type: "object",
					strict: "remove",
					props: {
						application: { type: "string" },
						username: { type: "string" },
						password: { type: "string" },
						url: { type: "string" },
					},
				},
				user_data: { type: "object" },
				subject: { type: "string" },
				message: { type: "string" },
				attachments: { type: "array", items: "object", optional: true },
				priority: { type: "number", default: 1, optional: true },
			},
			handler(ctx) {
				this.logger.info("SMS SEND :)");
				this.addJob(ctx);
				return this.Promise.resolve(true);
			},
		},
		/**
		 * Validate configurations for sending an SMS.
		 * Returns true if successful. Otherwise, throws an Error.
		 *
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				application: { type: "string" },
				username: { type: "string" },
				password: { type: "string" },
				url: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		addJob(ctx) {
			this.logger.info("CREATE JOB RECEIVED!", ctx.params);
			this.createJob(
				"sms.send",
				{ ...ctx.params, id: ctx.params.notification_id, pid: process.pid },
				{
					attempts: 1,
					priority: ctx.params.priority || 1,
					removeOnComplete: true,
					removeOnFail: true,
				},
			);

			return true;
		},
		/**
		 * Sends de Email
		 * @param {*} configs
		 * @param {*} user_data
		 * @param {*} subject
		 * @param {*} message
		 * @param {*} attachments
		 */
		send(configs, user_data, subject, message) {
			const phone_validator = new RegExp(/(9[1236]\d) ?(\d{3}) ?(\d{3})/, "g").exec(
				user_data.phone,
			);
			if (!phone_validator) {
				throw new Error("Not a valid phone number!");
			}

			return new Promise((resolve, reject) => {
				const smsBody = `<?xml version="1.0" encoding="UTF-8" ?>
            <message>
            <auth>
               <application>${configs.application}</application>
               <password>${configs.password}</password>
               <username>${configs.username}</username>
            </auth>
            <addr>
               <destination_addr>${user_data.phone}</destination_addr>
            </addr>
            <short_message>
               <text>${message}</text>
            </short_message>
            <message_options>
               <registered_delivery>1</registered_delivery>
               <priority_flag>0</priority_flag>
            </message_options>
         </message>
         `;


				if(process.env.SMS_DEBUG === "true") {
					setTimeout(() => {
						resolve(true);
					}, 1000);
				} else {
					// let result = null;
					const options = {
						method: "POST",
						body: smsBody,
						timeout: {
							request: 60000,
						},
						headers: {
							"Content-Type": "text/xml",
						},
					};

					return got(configs.url, options)
						.then((response) => {
							/*if (process.env.SMS_DEBUG) {
							fs.appendFileSync("../ama_response.txt", response.body);
						}*/
							resolve(response.body);
						})
						.catch((error) => {
							reject(error);
						});
				}

			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.getQueue("sms.send").on("progress", (jobID, progress) => {
			this.logger.info(`Job #${jobID} progress is ${progress}%`);
		});

		this.getQueue("sms.send").on("completed", (job, res) => {
			this.logger.info(`Job #${job.id} completed!. Result:`, res);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "SUCCESS",
			});
		});

		this.getQueue("sms.send").on("failed", (job, err) => {
			this.logger.info(`Job #${job.id} failed!. Result:`, err);
			this.broker.call("notifications.notifications.patch", {
				id: job.data.notification_id,
				status: "ERROR",
				error_message: err.message,
			});
		});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
