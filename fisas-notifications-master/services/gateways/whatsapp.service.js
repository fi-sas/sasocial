"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.whatsapp",
	mixins: [],

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Sends a notification with received configs, subject and message.
		 * If an error occurs, throws Error.
		 * Otherwise, returns true.
		 *
		 * @param {Object} configs
		 * @param {String} subject
		 * @param {String} message
		 */
		send: {
			params: {
				configs: {
					type: "object",
					strict: "remove",
					props: {
						accountSid: { type: "string" },
						authToken: { type: "string" },
					},
				},
				user_data: { type: "object" },
				subject: { type: "string" },
				message: { type: "string" },
				attachments: { type: "array", items: "object", optional: true },
				priority: { type: "number", default: 1, optional: true },
			},
			handler() {
				/*const recipientId = user_data.phone_number;
				if (!recipientId) {
					throw new Error("Could not send WhatsApp message: user phone number missing.");
				}

				// TODO: missing attachments

				const { accountSid, authToken } = configs;
				// eslint-disable-next-line global-require
				const client = require("twilio")(accountSid, authToken);
				await client.messages.create({
					body: message,
					from: "whatsapp:+14155238886",
					to: `whatsapp:${recipientId}`,
				});

				return true;
				*/
				return true;
			},
		},
		/**
		 * Validate configurations for sending WhatsApp messages.
		 * Returns true if successful. Otherwise, throws an Error.
		 * @param {Object} configs
		 */
		validateConfigs: {
			params: {
				accountSid: { type: "string" },
				authToken: { type: "string" },
			},
			handler() {
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
