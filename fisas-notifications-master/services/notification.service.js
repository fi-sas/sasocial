"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Mustache = require("mustache");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const gateway_paths = require("./utils/gateway_paths");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.notifications",
	table: "notification",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "notifications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"alert_id",
			"user_id",
			"notification_method_id",
			"subject",
			"message",
			"status",
			"error_message",
			"trial",
			"unread",
			"deleted",
			"deleted_at",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["notificationMethod", "service"],
		withRelateds: {
			notificationMethod(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"notifications.notification-methods",
					"notificationMethod",
					"notification_method_id",
				);
			},
			service(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("notifications.alerts.getService", { id: doc.alert_id })
							.then((res) => (doc.service = res));
					}),
				);
			},
		},
		entityValidator: {
			alert_id: { type: "number", positive: true },
			user_id: { type: "number", positive: true },
			notification_method_id: { type: "number", positive: true },
			subject: { type: "string", max: 250, empty: true },
			message: { type: "string", empty: true },
			status: { type: "enum", values: ["SUCCESS", "PENDING", "ERROR"] },
			error_message: { type: "string", empty: true, optional: true },
			trial: { type: "number", min: 0, optional: true },
			unread: { type: "boolean", default: true },
			is_internal: { type: "boolean", optional: true },
			deleted: { type: "boolean", optional: true },
			deleted_at: { type: "date", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.unread = true;
					ctx.params.deleted = false;
					ctx.params.deleted_at = null;
					ctx.params.trial = 0;

					if (!ctx.params.is_internal) {
						ctx.params.status = "PENDING";
					}
					delete ctx.params.is_internal;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create: [
				function updateHistory(ctx, res) {
					this.updateAlertHistory(ctx, res);
					return res;
				},
			],
			update: [
				function updateHistory(ctx, res) {
					this.updateAlertHistory(ctx, res);
					return res;
				},
			],
			patch: [
				function updateHistory(ctx, res) {
					this.updateAlertHistory(ctx, res);
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		// OBTAIN THE INTERNAL NOTIFICATIONS TO THE USER
		internal: {
			visibility: "published",
			rest: "GET  /internal",
			params: {
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				$$strict: "remove",
			},
			handler(ctx) {
				if (!ctx.meta.user || ctx.meta.isGuest) {
					return [];
				}

				return ctx
					.call("notifications.notification-methods.find", {
						query: {
							service: "INTERNAL",
						},
					})
					.then(async (internal_nm) => {
						if (internal_nm.length === 0) {
							return [];
						}

						const total_unread = await ctx.call("notifications.notifications.count", {
							query: {
								user_id: ctx.meta.user.id,
								notification_method_id: internal_nm[0].id,
								status: "SUCCESS",
								unread: true,
								deleted: false,
							},
						});

						return ctx
							.call("notifications.notifications.list", {
								...ctx.params,
								sort: "-created_at",
								fields: ["id", "subject", "message", "unread", "service", "created_at"],
								withRelated: "service",
								query: {
									user_id: ctx.meta.user.id,
									notification_method_id: internal_nm[0].id,
									status: "SUCCESS",
									deleted: false,
								},
							})
							.then((result) => {
								result.rows.forEach((row) => {
									row.total_unread = total_unread;
								});
								return result;
							});
					});
			},
		},
		setReaded: {
			rest: "PUT /:id/read",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const notification = await ctx.call("notifications.notifications.find", {
					fields: ["id", "subject", "message", "unread", "created_at"],
					query: {
						user_id: ctx.meta.user.id || 1,
						id: ctx.params.id,
					},
				});

				if (notification.length > 0) {
					return ctx
						.call("notifications.notifications.patch", {
							id: ctx.params.id,
							unread: false,
						})
						.then(() => {
							return [];
						});
				}

				return [];
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			scope: null,
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const notification = await ctx.call("notifications.notifications.find", {
					fields: ["id", "subject", "message", "unread", "created_at", "notification_method_id"],
					withRelated: false,
					query: {
						user_id: ctx.meta.user.id || 1,
						id: ctx.params.id,
					},
				});

				const INTERNAL = await ctx.call("notifications.notification-methods.find", {
					query: {
						service: "INTERNAL",
					},
				});

				if (
					notification.length > 0 &&
					INTERNAL.length > 0 &&
					notification[0].notification_method_id === INTERNAL[0].id
				) {
					return ctx
						.call("notifications.notifications.patch", {
							id: ctx.params.id,
							deleted: true,
							deleted_at: new Date(),
						})
						.then(() => {
							return [];
						});
				}

				return [];
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		async "notifications.alerts.created"(ctx) {
			const alert = ctx.params[0];

			let subject = "";
			let template = "";
			let simplified_message = "";
			let message = "";
			let type = alert.type;
			let notificationsMethods = [];

			let alert_medias = await ctx.call("notifications.alert-medias.find", {
				query: {
					alert_id: alert.id,
				},
			});

			let medias = [
				...alert.type.medias,
				...alert.type.template.medias,
				...alert.medias,
				...alert_medias,
			];

			if (!type) {
				type = await ctx.call("notifications.alert-types.get", {
					id: alert.alert_type_id,
				});
			}

			if (type.template) {
				template = type.template.template;
			} else {
				const alert_template = await ctx.call("notifications.alert-templates.get", {
					id: type.alert_template_id,
				});

				template = alert_template[0].template;
			}

			// IS JSON
			if (typeof alert.user_data === "string") {
				try {
					alert.user_data = JSON.parse(alert.user_data);
				} catch (ex) {
					this.logger.error("User data is a BAD json !");
				}
			}

			// GET USER DATA IF POSSIBLE
			if (!alert.user_data || alert.user_data === {} || !alert.user_data.email) {
				const user = await ctx.call("authorization.users.get", { id: alert.user_id });
				alert.user_data = user[0];
			}

			let alert_data = {};

			// IS JSON STRING
			if (typeof alert.data === "string") {
				try {
					alert_data = JSON.parse(alert.data);
				} catch (ex) {
					this.logger.info(alert.data);
					this.logger.error("Alert data is a BAD json !");
				}
			} else {
				alert_data = alert.data;
			}

			subject = type.subject;
			message = type.message;
			simplified_message = type.simplified_message;
			notificationsMethods = type.notification_methods;

			if (ctx.meta.override_notifcation_methods) {
				notificationsMethods = ctx.meta.override_notifcation_methods;
			}
			const variables = {
				medias: medias.reduce(
					(previous, current) => {
						const reduced = previous;
						reduced[current.type.toLowerCase()][current.key] = current.file;
						return reduced;
					},
					{
						image: {},
						attachment: {},
					},
				),
				...type.template.data,
				...type.data,
				...alert_data,
				user_data: alert.user_data,
			};

			let subject_output = Mustache.render(subject, variables);

			if (ctx.meta.notification_double_render) {
				subject_output = Mustache.render(subject_output, variables);
			}

			let simplified_message_output = Mustache.render(simplified_message, variables);

			if (ctx.meta.notification_double_render) {
				simplified_message_output = Mustache.render(simplified_message_output, variables);
			}

			// Renderizar mensagens
			let message_output = Mustache.render(template, {
				medias: type.template.medias.reduce(
					(previous, current) => {
						const reduced = previous;
						reduced[current.type.toLowerCase()][current.key] = current.file;
						return reduced;
					},
					{
						image: {},
						attachment: {},
					},
				),
				...type.template.data,
				simplified_message: simplified_message_output,
				message: Mustache.render(message, variables),
			});

			if (ctx.meta.notification_double_render) {
				message_output = Mustache.render(message_output, variables);
			}

			if (notificationsMethods.length === 0) {
				ctx.emit("notifications.alerts.trackHistory", {
					alert_id: alert.id,
					status: "ERROR",
					notes: "No notification method configured",
				});
			} else {
				// CREATE A INTERNAL NOTIFICATION ALWAYS
				const internalMethod = await ctx.call("notifications.notification-methods.find", {
					query: {
						service: "INTERNAL",
					},
				});

				// REMOVE THE INTERNAL METHOD IF PRESENT
				notificationsMethods = notificationsMethods.filter(
					(nm) => nm.notification_method.id !== internalMethod[0].id,
				);
				ctx.call("notifications.notifications.create", {
					is_internal: true,
					alert_id: alert.id,
					user_id: alert.user_id,
					notification_method_id: internalMethod[0].id,
					status: "SUCCESS",
					subject: subject_output,
					message: simplified_message_output,
				});

				if (notificationsMethods.length === 0) {
					ctx.emit("notifications.alerts.trackHistory", {
						alert_id: alert.id,
						status: "ERROR",
						notes: "No notification methods configurated for this alert type.",
					});
					this.changeAlertStatus(ctx, alert.id, "ERROR");
				} else {
					this.changeAlertStatus(ctx, alert.id, "PENDING");
				}

				// CREATE NOTIFICATIONS
				notificationsMethods.map((nm) => {
					return ctx
						.call("notifications.notifications.create", {
							alert_id: alert.id,
							user_id: alert.user_id,
							notification_method_id: nm.notification_method_id,
							subject: subject_output,
							message: ["PUSH_NOTIFICATIONS", "SMS"].includes(nm.notification_method.service)
								? simplified_message_output
								: message_output,
						})
						.then((result) => {
							return ctx
								.call(`${gateway_paths[nm.notification_method.service]}.send`, {
									notification_id: result[0].id,
									configs: nm.notification_method.configs,
									user_data: alert.user_data,
									subject: result[0].subject,
									message: result[0].message,
									attachments: medias.filter((m) => m.type === "ATTACHMENT"),
									priority: nm.priority < 1 ? 1 : nm.priority,
								})
								.then(() => {
									return ctx.emit("notifications.alerts.trackHistory", {
										alert_id: alert.id,
										status: "PENDING",
										notes: `Notification process started. Trying ${nm.notification_method.name} [${nm.priority}] notification method.`,
									});
								})
								.catch((err) => {
									return ctx.call("notifications.notifications.patch", {
										id: result[0].id,
										status: "ERROR",
										error_message: err.message,
									});
								});
						})
						.catch((err) => {
							this.logger.error(err);
						});
				});
			}
		},
	},

	/**
	 * Methods
	 */
	methods: {
		updateAlertHistory(ctx, res) {
			if (Array.isArray(res)) {
				res = res[0];
			}

			if (res.status !== "PENDING") {
				if (res.status === "SUCCESS") {
					this.updateAlertStatus(ctx, res.alert_id);
					ctx.emit("notifications.alerts.trackHistory", {
						alert_id: res.alert_id,
						status: "NOTIFIED",
						notes: `Successful notification (${res.notificationMethod.name}).`,
					});
				}

				if (res.status === "ERROR") {
					this.updateAlertStatus(ctx, res.alert_id);
					ctx.emit("notifications.alerts.trackHistory", {
						alert_id: res.alert_id,
						status: "ERROR",
						notes: `Error on notification (${res.notificationMethod.name}).`,
					});
				}
			}
		},
		updateAlertStatus(ctx, alert_id) {
			ctx.call("notifications.alerts.update_status", {
				alert_id,
			});
		},
		changeAlertStatus(ctx, alert_id, status) {
			ctx.call("notifications.alerts.patch", {
				id: alert_id,
				status,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
