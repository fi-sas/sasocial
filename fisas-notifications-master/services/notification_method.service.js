"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const gateway_paths = require("./utils/gateway_paths");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "notifications.notification-methods",
	table: "notification_method",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("notifications", "notification-methods")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"name",
			"service",
			"description",
			"configs",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string", max: 120, empty: false },
			service: {
				type: "enum",
				values: [
					"INTERNAL",
					"EMAIL",
					"SMS",
					"PUSH_NOTIFICATIONS",
					"FACEBOOK_MESSENGER",
					"WHATSAPP",
				],
			},
			description: { type: "string", max: 250, empty: true },
			configs: { type: "object", strict: false },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateMethod"
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"validateMethod"
			],
		},
		after: {
			find: [
				"parseConfigs"
			],
			list: [
				"parseConfigs"
			],
			get: [
				"parseConfigs"
			]
		}
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {
		validateMethod(ctx) {
			if (gateway_paths[ctx.params.service]) {
				return ctx.call(`${gateway_paths[ctx.params.service]}.validateConfigs`, {
					...ctx.params.configs
				});
			}
		},
		parseConfigs(ctx, res) {
			if (Array.isArray(res)) {
				res.map(r => {
					if (r.configs)
						r.configs = JSON.parse(r.configs);
				});
			} else if (res.configs) {
				res.configs = JSON.parse(res.configs);
			}

			return res;
		}
	},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
