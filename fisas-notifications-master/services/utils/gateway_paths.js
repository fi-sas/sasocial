module.exports = {
	EMAIL: "notifications.email",
	SMS: "notifications.sms_ama",
	PUSH_NOTIFICATIONS: "notifications.push_notifications",
	FACEBOOK_MESSENGER: "notifications.facebook_menseger",
	WHATSAPP: "notifications.whatsapp",
};