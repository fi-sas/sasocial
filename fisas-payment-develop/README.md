[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# Fisas 'payment' Microservice
This is a [Moleculer](https://moleculer.services/)-based microservices project.


## Bootstraping

If you want to create a new project using this repo as template.

If you have never installed this awesome framework, use the first following command:

  `$ npm i moleculer-cli -g`

We strongly advise that you create a new molecule project using the follow commands:

  `$ moleculer init gitlab:fi-sas/node-api-boilerplate#master-v2 new-project-folder`
 
## Usage - Local
Start the project with `npm run dev` command. 

After starting, open the http://localhost:3000/ URL in your browser. 
On the welcome page you can test the generated services via API Gateway and check the nodes & services.

In the terminal, try the following commands:
- `nodes` - List all connected nodes.
- `actions` - List all registered service actions.
- `call service.method` - Call the `service.method` action.
- `call service.method --name value` - Call the `service.method` action with the `name` parameter.

## Usage - Docker
Create a new `.env` using `example.dev.env` as a example.

Start the project with `docker-compose up` command.

## Services
It's advise to name your services in the README of your new project, as such:
- **Dummy Service 1**
- **Dummy Service 2**
- ...

## Mixins & Middlewares
It's advise to name any mixin or middleware that you created on your project.

There is also a few mixins maintained by FISAS team, those mixins are loosely related to cross-cutting concerns.

- **DbMixin**: Database access mixin for FISAS services [@fisas/ms_core](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer)
- **SagaMiddleware**: (`WIP`) Middleware that enable actions to have saga compensations [@fisas/ms_core/middlewares](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer)


## Useful links

* Moleculer website: https://moleculer.services/
* Moleculer Documentation: https://moleculer.services/docs/0.14/

## NPM scripts

- `npm run dev`: Start development mode (load all services locally with hot-reload & REPL)
- `npm run start`: Start production mode (set `SERVICES` env variable to load certain services)
- `npm run cli`: Start a CLI and connect to production. Don't forget to set production namespace with `--ns` argument in script
- `npm run lint`: Run ESLint
- `npm run ci`: Run continuous test mode with watching
- `npm test`: Run tests & generate coverage report
- `npm run dc:up`: Start the stack with Docker Compose
- `npm run dc:down`: Stop the stack with Docker Compose

- `npm run knex:list`: List existing migration files
- `npm run knex:migration:create`: Create database migration file
- `npm run knex:migration:migrate`: Execute database migration
- `npm run knex:migration:rollback`: Rollback database migration
- `npm run knex:seed:create`: Create seed
- `npm run knex:seed:run`: Execute seed's
