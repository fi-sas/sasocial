module.exports.up = async (db) =>
	db.schema
		.createTable("payment_method", (table) => {
			table.uuid("id").notNullable().primary();
			table.string("name", 45).notNullable();
			table.string("tag", 45).notNullable();
			table.string("description", 250).nullable();
			table.boolean("is_immediate").defaultTo(true).notNullable();
			table.boolean("active").defaultTo(false).notNullable();
			table.boolean("charge").defaultTo(false).notNullable();
			table.json("gateway_data").nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("tag");
		})
		.createTable("payment", (table) => {
			table.uuid("id").notNullable().primary();
			table.integer("account_id").unsigned();
			table.integer("user_id").unsigned();
			table.uuid("payment_method_id").references("id").inTable("payment_method").nullable();
			table.json("payment_method_data").nullable();
			// table.json("movement_ids");
			table.uuid("movement_id").nullable();
			table.decimal("value", 8, 2).notNullable();
			table
				.enu("status", ["CONFIRMED", "PENDING", "CANCELLED"], {
					useNative: true,
					enumName: "payment_status",
				})
				.notNullable()
				.defaultTo("PENDING");

			table.datetime("expire_at").nullable();
			table.datetime("paid_at").nullable();
			table.datetime("confirmed_at").nullable();
			table.datetime("cancelled_at").nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("cart", (table) => {
			// table.increments();
			table.uuid("id").notNullable().primary();
			table.uuid("payment_method_id").references("id").inTable("payment_method").nullable();
			table.integer("account_id").unsigned();
			table.integer("user_id").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("account_id");
		})
		.createTable("cart_item", (table) => {
			table.uuid("id").notNullable().primary();
			table
				.uuid("cart_id")
				.references("id")
				.inTable("cart")
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.integer("service_id").unsigned();
			table.string("product_code", 120).notNullable();
			table.string("product_type", 120).nullable();
			table.integer("quantity").unsigned();
			table.decimal("unit_value", 8, 2).notNullable();
			table.decimal("liquid_value", 8, 2).notNullable();
			table.integer("vat_id").unsigned().notNullable();
			table.float("vat_multiplier").notNullable();
			table.decimal("vat_value", 8, 2).notNullable();
			table.decimal("total_value", 8, 2).notNullable();
			table.string("name", 250).notNullable();
			table.string("description", 250);
			table.string("location", 120).notNullable();
			table.json("extra_info");
			table.datetime("checkout_at").nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.createTable("charge", (table) => {
			table.uuid("id").notNullable().primary();
			table.decimal("total_value", 8, 2).notNullable();
			table.decimal("charge_value", 8, 2).notNullable();
			table.decimal("cost_value", 8, 3);
			table.decimal("fee", 8, 3);
			table.decimal("fee_value", 8, 3);
			table.boolean("user_supports_cost").defaultTo(false);
			table
				.enu("status", ["CONFIRMED", "PENDING", "CANCELLED"], {
					useNative: true,
					enumName: "charge_status",
				})
				.notNullable()
				.defaultTo("PENDING");
			table.integer("user_id").unsigned().notNullable();
			table.integer("account_id").unsigned().notNullable();
			table.integer("device_id").unsigned().notNullable();
			table.uuid("payment_id").references("id").inTable("payment").nullable();
			table.string("description", 250).nullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("payment")
		.dropTable("cart")
		.dropTable("cart_item")
		.dropTable("payment_method")
		.dropTable("charge");

module.exports.configuration = { transaction: true };
