FROM postgres:12.4

RUN apt-get update && apt-get install -y postgresql-contrib

ADD postgres.exension.sh /docker-entrypoint-initdb.d/
RUN chmod 755 /docker-entrypoint-initdb.d/postgres.exension.sh