exports.seed = (knex) =>
	// Deletes ALL existing seed entries
	knex("payment_method")
		.whereIn("id", [
			"0bbd1f3f-8720-4075-880c-bf3d789efceb",
			"6273cc97-943b-4524-8ddf-a7287afd331b",
			"cbdd30af-d9e3-4919-89f3-ad5a7f42b5d5",
			"ae10940a-a64b-438c-bc1e-290528b7fb96",
			"ebaffe5f-db1d-48b6-944d-fb33f4596bb1",
		])
		.del()
		// Inserts ALL seed entries
		.then(() =>
			knex("payment_method").insert([
				{
					id: "0bbd1f3f-8720-4075-880c-bf3d789efceb",
					name: "Conta Corrente",
					tag: "CC",
					description: "Pagamento por saldo da conta corrente",
					is_immediate: true,
					active: true,
					charge: true,
					gateway_data: {},
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: "6273cc97-943b-4524-8ddf-a7287afd331b",
					name: "Numerario",
					tag: "NUM",
					description: "Pagamento por numerário.",
					is_immediate: true,
					active: true,
					charge: false,
					gateway_data: null,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: "cbdd30af-d9e3-4919-89f3-ad5a7f42b5d5",
					name: "Débito direto",
					tag: "DD",
					description: "Pagamento por débito direto.",
					is_immediate: false,
					active: true,
					charge: false,
					gateway_data: null,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: "ae10940a-a64b-438c-bc1e-290528b7fb96",
					name: "Multibanco",
					tag: "MB",
					description: "Pagamento por multibanco.",
					is_immediate: false,
					active: true,
					charge: true,
					gateway_data: {},
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: "ebaffe5f-db1d-48b6-944d-fb33f4596bb1",
					name: "Referência Multibanco",
					tag: "REFMB_AMA",
					description: "Pagamento por multibanco Plataforma AMA.",
					is_immediate: false,
					active: true,
					charge: true,
					gateway_data: {},
					created_at: new Date(),
					updated_at: new Date(),
				},
			]),
		);
