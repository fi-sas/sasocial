"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payment.carts",
	table: "cart",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("payment", "carts")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "account_id", "payment_method_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			payment_method_id: { type: "uuid", version: 4, optional: true },
			account_id: { type: "number", integer: true, min: 1 },
			user_id: { type: "number", integer: true, min: 1 },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				async function validateUniqueCart(ctx, res) {
					if (_.has(ctx, "params.account_id")) {
						const account_id = ctx.params.account_id;
						const cart = await this._find(ctx, { query: { account_id } });
						if (!_.isEmpty(cart)) {
							throw new Errors.ValidationError(
								"Cart with same account_id already exists",
								"CART_ALREADY_EXISTS",
								{
									account_id,
								},
							);
						}
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
			rest: "POST /",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				let params = ctx.params;
				const cart = await this._create(ctx, params);
				this.broker.emit("payment.carts.create-cart.success", {
					cart_id: ctx.params.id,
					items: ctx.params.items,
				});
				return cart;
			},
		},
		addCarItem: {
			visibility: "published",
			rest: "POST /:id/add-item",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				const cartId = ctx.params.id;
				const params = _.omit(ctx.params, "id");
				return await ctx.call(
					"payment.cart-item.create",
					_.assign({}, params, { cart_id: cartId }),
				);
			},
		},
		checkout: {
			visibility: "published",
			rest: "POST /:id/checkout",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				const cartId = ctx.params.id;
				const params = _.omit(ctx.params, "id");

				const cart = await ctx.call("payment.carts.get", { id: cartId });

				const cartItems = await ctx.call("payment.cart-item.list", { cart_id: cartId });

				if (_.isUndefined(cartItems) || _.isEmpty(cartItems)) {
					throw new Errors.ValidationError("Empty cart", "EMPTY_CART", {
						cart_id: cartId,
					});
				}

				// Calculate transaction value based on items
				let transactionValue = 0;
				_.each(cartItems, (item) => {
					transactionValue += parseFloat(item.total_value);
				});

				// Create Payment
				const paymentData = {
					account_id: cart[0].account_id,
					user_id: cart[0].user_id,
					payment_method_id: cart[0].payment_method_id,
					payment_method_data: null,
					movement_id: null,
					value: transactionValue,
				};
				const payment = await ctx.call("payment.payments.create", _.assign({}, paymentData));

				// Emit create movement event
				this.broker.emit("payment.carts.checkout.create-payment.success", {
					cart_id: cartId,
					payment: payment[0],
				});

				// Delete cart
				this._remove(ctx, { id: cartId });

				return payment;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
