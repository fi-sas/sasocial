"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payment.cart-item",
	table: "cart_item",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("payment", "cart-item")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"cart_id",
			"service_id",
			"product_code",
			"product_type",
			"quantity",
			"unit_value",
			"liquid_value",
			"vat_id",
			"vat_multiplier",
			"vat_value",
			"total_value",
			"name",
			"description",
			"location",
			"extra_info",
			"checkout_at",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			cart_id: { type: "uuid", version: 4, optional: false },
			service_id: { type: "number", integer: true, min: 1 },
			product_code: { type: "string", max: 120 },
			product_type: { type: "string", max: 120, optional: true },
			quantity: { type: "number", integer: true, min: 1 },
			unit_value: { type: "number", min: 0 },
			liquid_value: { type: "number", min: 0 },
			vat_id: { type: "number", integer: true, min: 1 },
			vat_multiplier: { type: "number" },
			vat_value: { type: "number", min: 0 },
			total_value: { type: "number", min: 0 },
			name: { type: "string", max: 250 },
			description: { type: "string", max: 250 },
			location: { type: "string", max: 120 },
			extra_info: { type: "object", optional: true },
			checkout_at: { type: "date", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				"validateCartIdentifier",
				"caculateCartItem",
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		create: {
			// TODO remote this endpoint/visibility (TESTING)
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		async "payment.carts.create-cart.success"(ctx) {
			this.logger.info("CAPTURED EVENT => payment.carts.create-cart.success");
			const items_saved = [];
			try {
				if (
					_.has(ctx, "params.items") &&
					!_.isEmpty(ctx.params.items) &&
					_.has(ctx, "params.items")
				) {
					const cartId = ctx.params.cart_id;
					const cartItems = ctx.params.items;

					// Save movement items
					await cartItems.reduce(async (promise, item) => {
						await promise;
						const item_saved = await ctx.call(
							"payment.cart-item.create",
							_.assign({}, item, { cart_id: cartId }),
						);
						items_saved.push(item_saved[0]);
					}, Promise.resolve());
				} else {
					throw new Errors.ValidationError(
						"ERROR on payment.carts.create-cart.success",
						"ERROR_EVENT_CREATE_CART_SUCCESS",
						{ params: ctx.params },
					);
				}
			} catch (e) {
				// this._remove(ctx, { id: cart_id });
				throw e;
			}
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateCartIdentifier(ctx, res) {
			if (_.has(ctx, "params.cart_id")) {
				const cart = await ctx.call("payment.carts.get", { id: ctx.params.cart_id });
				if (!cart || _.isEmpty(cart) || !cart[0]) {
					throw new Errors.ValidationError("Invalid cart", "INVALID_CART", {
						cart_id: ctx.params.cart_id,
					});
				}
			}
		},
		async caculateCartItem(ctx) {
			// liquid_value = quantity * unit_value
			if (_.has(ctx, "params.unit_value") && _.has(ctx, "params.quantity")) {
				const unitValue = ctx.params.unit_value;
				const quantity = ctx.params.quantity;
				ctx.params.liquid_value = unitValue * quantity;
			}

			const liquidValue = ctx.params.liquid_value;
			const discountValue = ctx.params.discount_value || 0;

			if (discountValue >= liquidValue) {
				throw new Errors.ValidationError("Invalid discount", "INVALID_DISCOUNT_VALUE", {
					liquidValue,
					discountValue,
				});
			}

			// VAT
			if (_.has(ctx, "params.vat_id")) {
				const vat = await ctx.call("configuration.taxes.get", { id: ctx.params.vat_id });
				if (_.isEmpty(vat)) {
					throw new Errors.ValidationError("Must provide a valid VAT", "VAT_NOT_FOUND", {
						vat_id: ctx.params.vat_id,
					});
				}
				if (!vat[0].active) {
					throw new Errors.ValidationError("Must provide a valid VAT", "VAT_INACTIVE", {
						vat_id: ctx.params.vat_id,
					});
				}

				const vatMultiplier = vat[0].tax_value;
				ctx.params.vat_multiplier = vatMultiplier;

				// vat_value = (liquid_value + (-discount_value) ) * vat_index
				ctx.params.vat_value = (liquidValue - discountValue) * vatMultiplier;
			} else {
				throw new Errors.ValidationError("Must provide a valid VAT", "VAT_NOT_FOUND", {
					vat_id: ctx.params.vat_id,
				});
			}

			// total_value = liquid_value + (-discount_value) + vat_value
			const vatValue = ctx.params.vat_value;

			ctx.params.total_value = liquidValue - discountValue + vatValue;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
