"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { CHARGE_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payment.charges",
	table: "charge",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("payment", "charges")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"total_value",
			"charge_value",
			"cost_value",
			"fee",
			"fee_value",
			"user_supports_cost",
			"status",
			"user_id",
			"account_id",
			"device_id",
			"payment_id",
			"description",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			total_value: { type: "number" },
			charge_value: { type: "number" },
			cost_value: { type: "number" },
			fee: { type: "number" },
			fee_value: { type: "number" },
			user_supports_cost: { type: "boolean" },
			status: { type: "enum", values: CHARGE_STATUS },
			user_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			account_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			device_id: { type: "number", integer: true, positive: true, min: 1, optional: false },
			payment_id: { type: "uuid", version: 4, optional: true },
			description: { type: "string", max: 250, optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.status = _.has(ctx, "params.status") ? ctx.params.status : "PENDING";
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		chargeAccount: {
			visibility: "published",
			rest: "POST /charge-account",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				let params = ctx.params;

				// TODO find method based on method_id, then process the type of payment to check
				// whether there is some fee and/or cost to be applied.
				// now, to keep it simple, default to a free transaction
				params.total_value = params.charge_value;
				params.cost_value = 0;
				params.fee = 0;
				params.fee_value = 0;
				params.user_supports_cost = false;

				// Save the charge
				const charge = await ctx.call("payment.charges.create", { ...params });

				// Create a payment associated with the created charging
				const chargePaymentParams = {
					account_id: params.account_id,
					user_id: params.user_id,
					payment_method_id: null,
					payment_method_data: null,
					value: params.total_value,
				};
				const payment = await ctx.call("payment.payments.create", { ...chargePaymentParams });

				// Save the payment id associated with the charge
				const chargeResult = await ctx.call("payment.charges.patch", {
					id: charge[0].id,
					payment_id: payment[0].id,
				});

				this.broker.emit("payment.charges.charge-account.success", {
					charge: chargeResult[0],
					payment: payment[0],
				});

				return chargeResult;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		async "payment.charges.charge-account.success"(ctx) {
			this.logger.info("CAPTURED EVENT => payment.charges.charge-account.success");
			const charge = ctx.params.charge;
			const payment = ctx.params.payment;

			const movementData = {
				// TODO which service should be used here, if any ?
				// service_id: ???,
				account_id: charge.account_id,
				operation: "CREDIT",
				user_id: charge.user_id,
				is_immediate: true,
				transaction_id: payment.id,
				transaction_value: parseFloat(payment.value),
				status: "PENDING",
				affects_balance: true,
			};
			await ctx.call("current_account.movements.create", { ...movementData });
			await ctx.call("payment.charges.patch", { id: charge.id, status: "CONFIRMED" });
		},
	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
