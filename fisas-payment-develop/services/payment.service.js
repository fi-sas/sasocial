"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { PAYMENT_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { v4: uuidv4 } = require("uuid");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "payment.payments",
	table: "payment",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("payment", "payments")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"account_id",
			"user_id",
			"payment_method_id",
			"payment_method_data",
			"movement_id",
			"value",
			"status",
			"expire_at",
			"paid_at",
			"confirmed_at",
			"cancelled_at",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			account_id: { type: "number", integer: true, min: 1 },
			user_id: { type: "number", integer: true, min: 1 },
			payment_method_id: { type: "uuid", version: 4, optional: true },
			payment_method_data: { type: "object", optional: true },
			movement_id: { type: "uuid", version: 4, optional: true },
			value: { type: "number" },
			status: { type: "enum", values: PAYMENT_STATUS },
			expire_at: { type: "date", optional: true },
			paid_at: { type: "date", optional: true },
			confirmed_at: { type: "date", optional: true },
			cancelled_at: { type: "date", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.id = _.has(ctx, "params.id") ? ctx.params.id : uuidv4();
					ctx.params.status = _.has(ctx, "params.status") ? ctx.params.status : "PENDING";
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		pay: {
			visibility: "published",
			rest: "POST /:id/pay",
			scope: `${this.namespace}:${this.name}:create`,
			async handler(ctx) {
				const paymentId = ctx.params.id;
				const params = _.omit(ctx.params, "id");

				const payment = await ctx.call("payment.payments.get", { id: paymentId });
				if (payment[0].status !== "PENDING") {
					throw new Errors.ValidationError(
						"Payment must be on a pending status to be paid",
						"ERROR_PAYING_PAYMENT_NOT_PENDING",
						{ payment: payment[0] },
					);
				}

				const paymentMethodId = payment[0].payment_method_id || ctx.params.payment_method_id;
				if (!paymentMethodId) {
					throw new Errors.ValidationError(
						"Payment method not provided",
						"PAYMENT_METHOD_NOT_FOUND",
						{ payment: payment[0] },
					);
				}

				const paymentMethod = await ctx.call("payment.payment-methods.get", {
					id: paymentMethodId,
				});

				if (!_.isEmpty(paymentMethod) && _.has(paymentMethod[0], "is_immediate")) {
					const isImmediate = paymentMethod[0].is_immediate;
					if (_.isBoolean(isImmediate) && isImmediate) {
						// Confirm payment
						const paymentPaid = await ctx.call("payment.payments.patch", {
							id: paymentId,
							payment_method_id: paymentMethodId,
							payment_method_data: ctx.params.payment_method_data,
							status: "CONFIRMED",
						});

						await this.broker.emit("payment.payments.paid.success", {
							payment: paymentPaid[0],
							payment_method: paymentMethod[0],
						});
					} else {
						// TODO call payment gateways here!!!
						throw new Errors.ValidationError(
							"Invalid operation for this payment method! Not implemented yet!",
							"PAY_IS_NOT_IMMEDIATE_NOT_IMPLEMENTED",
							{ payment_method: paymentMethod },
						);
					}
				}

				return await ctx.call("payment.payments.get", { id: paymentId });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validatePaymentMethod(ctx) {
			const pm_id = ctx.params.payment_id;
			const pm = await ctx.call("payment.payment-methods.find", {
				query: {
					id: pm_id,
				},
			});

			if (_.isUndefined(pm) || (_.isEmpty(pm) && false)) {
				throw new EntityNotFoundError("PaymentMethod", pm_id);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
