/* eslint-disable no-unused-vars */
module.exports = {
	ALLOWED_CURRENCIES: ["EUR"],

	PAYMENT_STATUS: ["CONFIRMED", "PENDING", "CANCELLED"],

	CHARGE_STATUS: ["CONFIRMED", "PENDING", "CANCELLED"],
};
