# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2020-10-30)


### Features

* **apis-errors:** add handler for errors ([21b2747](https://gitlab.com/fi-sas/fisas-pos/commit/21b2747c09b91f20042688ceda29da883bb0ef60))
* **auth:** add authentication services ([3ef9baa](https://gitlab.com/fi-sas/fisas-pos/commit/3ef9baa0c2f51fe6cf766c8d122bbb49694f115f))
* **canteen:** buy meals to the user ([8111e3c](https://gitlab.com/fi-sas/fisas-pos/commit/8111e3c5d1ebd46785191cbdf03f9b92c6db851b))
* **canteen:** inital dashboard exemplo ([5a391cb](https://gitlab.com/fi-sas/fisas-pos/commit/5a391cb4bb5c25c490d30726676e4441ac81ca03))
* **canteen:** initial canteen ([f354889](https://gitlab.com/fi-sas/fisas-pos/commit/f3548894997c14465683c96de554681dbbeb628e))
* **canteen:** list charges per day ([f36c72f](https://gitlab.com/fi-sas/fisas-pos/commit/f36c72fccbd8ea16ff6064cebbbab0e23e1c9908))
* **chargeAccount:** chargeAccount module ([7521501](https://gitlab.com/fi-sas/fisas-pos/commit/75215018129c02e042def988ba70d2d8fb0c4239))
* **charges:** print charges ([c0c7ad3](https://gitlab.com/fi-sas/fisas-pos/commit/c0c7ad3ac2623e81da123fb6ec1ca6b79912c6bd))
* **dashboard:** add dashboard page ([9509739](https://gitlab.com/fi-sas/fisas-pos/commit/9509739f9dbb62aafaa8d12b70977d52b938e45e))
* **dashboard:** validate if the settings is correct ([0a5d015](https://gitlab.com/fi-sas/fisas-pos/commit/0a5d0151d79b582a47f5d9d700061ac377158c97))
* **last-reservetions:** load last reservations in dashboard ([737083c](https://gitlab.com/fi-sas/fisas-pos/commit/737083c7855608172eec6fd398ecd85c044dedbb))
* **list charges:** add total in charges ([b85b948](https://gitlab.com/fi-sas/fisas-pos/commit/b85b94826dad700836adb0b4791c7ee8f8c6956e))
* **movements:** add list for movements ([50fe399](https://gitlab.com/fi-sas/fisas-pos/commit/50fe3993dd647cb194f3e08d19d7a8c2418b9725))
* **pos:** add layout and antd ([7b8e194](https://gitlab.com/fi-sas/fisas-pos/commit/7b8e194f8d3962c54ffa779f4253543bba9551d5))
* **pos:** inicial structure ([2e363b2](https://gitlab.com/fi-sas/fisas-pos/commit/2e363b2bb4cdec5005209f4e9e4c7b31db9ca878))
* **reservations:** add reservations stats and cancel reservation option ([752b41e](https://gitlab.com/fi-sas/fisas-pos/commit/752b41eb1cbea1d246eceaf4e19f44e769e2807b))
* **reservations:** create module ([f65efee](https://gitlab.com/fi-sas/fisas-pos/commit/f65efee2f553dffa5d13ae9a349e1cab8d908a50))
* **reservations:** reservations history ([8fa0777](https://gitlab.com/fi-sas/fisas-pos/commit/8fa07778b592028dd39ddb7895f167fa329ea886))
* **rfid:** add rfid reader ([37e6a0b](https://gitlab.com/fi-sas/fisas-pos/commit/37e6a0b9b8e173439b51acf16f2745fa2560e8ef))
* **settings:** first settings of the POS ([a596103](https://gitlab.com/fi-sas/fisas-pos/commit/a5961036a1d59e4b440faf12f2c33ba17ae2a685))
* **uncharge-account:** create uncharge component ([2c089f9](https://gitlab.com/fi-sas/fisas-pos/commit/2c089f9378328b81748904e86adecb217e58563c))


### Bug Fixes

* **canteen:** fix cancel reservations ([f69817a](https://gitlab.com/fi-sas/fisas-pos/commit/f69817a10ce0bfb02687a421ef9dbb3305333b53))
* **canteen:** get entity from configs ([8ce9114](https://gitlab.com/fi-sas/fisas-pos/commit/8ce9114c125651aac257c01968240fa65c9c4f5d))
* **canteen:** show reservations of the active meal ([616243d](https://gitlab.com/fi-sas/fisas-pos/commit/616243d911120165eff35a330ecfafc4d66b5438))
* **charges:** fix decimal after value ([046e9f8](https://gitlab.com/fi-sas/fisas-pos/commit/046e9f84e726519afd62189e143beed478223251))
* **dashboard:** add logout button ([6c6eeda](https://gitlab.com/fi-sas/fisas-pos/commit/6c6eedae0fc2688a9b512157f494263c5792c6ae))
* **dashboard:** ajust layout ([e03e7b7](https://gitlab.com/fi-sas/fisas-pos/commit/e03e7b768a171652d029c870d03334190e446685))
* **geral:** fix minor issues ([956c54b](https://gitlab.com/fi-sas/fisas-pos/commit/956c54b6bc4958d2715524ee30d969941995f08b))
* **list-charge-day:** fix issues ([7eb3988](https://gitlab.com/fi-sas/fisas-pos/commit/7eb398845f50f6d790bc637e3a74e5092228f33b))
* **main:** remove augury_path ([f586a90](https://gitlab.com/fi-sas/fisas-pos/commit/f586a90f6c37fb7a8e775e30c53d422a063c28a4))
* **movements:** remove limit and add order ([0a65480](https://gitlab.com/fi-sas/fisas-pos/commit/0a65480ea31ee3b9dd60c9374fb90fe4631183e7))
* **reservations-history:** add meals ([5503fbb](https://gitlab.com/fi-sas/fisas-pos/commit/5503fbbe290363553a0a9efaa6572adfe4e745fc))
* **reservations-history:** fix date ([bc5415c](https://gitlab.com/fi-sas/fisas-pos/commit/bc5415cfc242d2f6b76dfd4cb7d29c0ff6282e7e))
* **reservations-history:** fix params ([cbc588d](https://gitlab.com/fi-sas/fisas-pos/commit/cbc588d59dd9ec46810681e453f564d7e6a695b4))
* **reservations-served:** autoload ([df33ad3](https://gitlab.com/fi-sas/fisas-pos/commit/df33ad34e04b1ddfa07b4a66af9a2ed5e13c3d31))
* **search:** add search to email and student number ([078ad7f](https://gitlab.com/fi-sas/fisas-pos/commit/078ad7f7b60e6c007f38e3199843e8f399230dd7))
* **shared:** system erro img path ([3482305](https://gitlab.com/fi-sas/fisas-pos/commit/3482305bfd02699f8aa1d5ed1a811e98bac0f052))
* **uncharge:** decimal after value ([113998b](https://gitlab.com/fi-sas/fisas-pos/commit/113998b19aecd2ebf7f51465ce0498031029b6b9))
* **uncharges:** fix uncharge top preview value ([aa03019](https://gitlab.com/fi-sas/fisas-pos/commit/aa030194b5678010dbf3c50db12b33620e5eadff))
* **user-info:** add nif ([bd68035](https://gitlab.com/fi-sas/fisas-pos/commit/bd68035e9e57ea9526a2a2cfc574d7c554832096))

## 0.0.0 (2019-06-21)


### Features

* **pos:** inicial structure ([2e363b2](https://gitlab.com/fi-sas/fisas-pos/commit/2e363b2))
