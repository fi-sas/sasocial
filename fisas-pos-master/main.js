"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
var electron_dl_1 = require("electron-dl");
var win = null;
var argv = require("minimist")(process.argv.slice(2));
var gotTheLock = electron_1.app.requestSingleInstanceLock();
var serve = argv.serve;
function createWindow() {
    var electronScreen = electron_1.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;
    // Create the browser window.
    win = new electron_1.BrowserWindow({
        x: 0,
        y: 0,
        width: size.width,
        height: size.height,
        webPreferences: {
            enableRemoteModule: true,
            webSecurity: false,
            nodeIntegration: true,
            allowRunningInsecureContent: serve ? true : false,
        },
    });
    electron_1.app.commandLine.appendSwitch("disable-features", "OutOfBlinkCors");
    if (serve) {
        // let auguryPath = '/home/leandromds/.config/google-chrome/Default/Extensions/elgalmkoelokbchhkhacckoklkejnhcd/1.24.1_0'
        // BrowserWindow.addDevToolsExtension(auguryPath);
        require("electron-reload")(__dirname, {
            electron: require(__dirname + "/node_modules/electron"),
        });
        win.loadURL("http://localhost:4206");
    }
    else {
        win.loadURL(url.format({
            pathname: path.join(__dirname, "dist/index.html"),
            protocol: "file:",
            slashes: true,
        }));
    }
    if (serve) {
        win.webContents.openDevTools();
    }
    // Emitted when the window is closed.
    win.on("closed", function () {
        // Dereference the window object, usually you would store window
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });
    electron_1.ipcMain.on("download-save-file", function (event, data) {
        console.log("DOWNLOAD:", data);
        electron_dl_1.download(win, data, {
            filename: data.split("/")[data.split("/").length - 1],
            saveAs: true,
        });
    });
    return win;
}
try {
    if (!gotTheLock) {
        electron_1.app.quit();
    }
    else {
        electron_1.app.on("second-instance", function (event, commandLine, workingDirectory) {
            // Someone tried to run a second instance, we should focus our window.
            if (win) {
                if (win.isMinimized())
                    win.restore();
                win.focus();
            }
        });
        // This method will be called when Electron has finished
        // initialization and is ready to create browser windows.
        // Some APIs can only be used after this event occurs.
        electron_1.app.on("ready", createWindow);
        // Quit when all windows are closed.
        electron_1.app.on("window-all-closed", function () {
            // On OS X it is common for applications and their menu bar
            // to stay active until the user quits explicitly with Cmd + Q
            if (process.platform !== "darwin") {
                electron_1.app.quit();
            }
        });
        electron_1.app.on("activate", function () {
            // On OS X it's common to re-create a window in the app when the
            // dock icon is clicked and there are no other windows open.
            if (win === null) {
                createWindow();
            }
        });
    }
}
catch (e) {
    // Catch Error
    // throw e;
}
//# sourceMappingURL=main.js.map