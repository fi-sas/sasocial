import { PrivateGuard } from './auth/guards/private.guard';
import { PublicGuard } from './auth/guards/public.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FullLayoutComponent } from './core/layouts/full-layout/full-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [PrivateGuard],
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'configs',
        loadChildren: './modules/configs/configs.module#ConfigsModule'
      },
      {
        path: 'canteen',
        loadChildren: './modules/canteen/canteen.module#CanteenModule'
      },
      {
        path: 'bar',
        loadChildren: './modules/bar/bar.module#BarModule'
      }
    ]

  },
  {
    path: 'login',
    canActivate: [PublicGuard],
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
