import { RfidService } from './services/rfid.service';
import { ConfigsService } from './services/configs.service';
import { PublicGuard } from './auth/guards/public.guard';
import { PrivateGuard } from './auth/guards/private.guard';
import { AuthService } from './auth/services/auth.service';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ElectronService } from './services/electron.service';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './auth/interceptors/token.interceptor';
import { ApisErrorsInterceptor } from './shared/interceptors/apis-errors.interceptor';

import { MiddlewareListenerService } from './services/middleware.service';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { ReportsService } from './modules/reports/services/reports.service';

import { NgxElectronModule } from 'ngx-electron';

import { SearchUserModule } from './modules/search-user/search-user.module';
import { MatKeyboardService } from './keyboard/public_api';
import { QrcodeService } from './services/qrcode.service';


export function initializeApp(authService: AuthService) {
  return (): Promise<any> => {
    return authService.refreshToken();
  }
}

const config: SocketIoConfig = { url: 'http://localhost:8080', options: {
  transports: ['websocket']
} };


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    NgxElectronModule,
    SharedModule,
    SocketIoModule.forRoot(config),
    CoreModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    SearchUserModule
  ],
  declarations: [
    AppComponent,
     DashboardComponent,
     LoginComponent,
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    MatKeyboardService,
    MiddlewareListenerService,
    AuthService,
    ConfigsService,
    ElectronService,
    RfidService,
    QrcodeService,
    PrivateGuard,
    PublicGuard,
    ReportsService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AuthService], multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ApisErrorsInterceptor, multi: true },
  ],
})
export class AppModule {
}
