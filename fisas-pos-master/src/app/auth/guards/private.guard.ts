import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class PrivateGuard implements CanActivate, CanActivateChild {
 
  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.tokenVerification(route, state);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.tokenVerification(route, state);
  }

  tokenVerification(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return Observable.create(observable => {
      this.authService.validateToken().subscribe(
        value => {
          if (value) {
            // IF TOKEN IS VALID
            if (route.data.hasOwnProperty('scope')) {
              // IF HAS SCOPE PROPRIETY ON ROUTING
              const hasPermission = this.authService.hasPermission(
                route.data.scope
              );
              if (hasPermission) {
                // IF USER HAS THE SCOPE OF THE ROUTING
                observable.next(true);
              } else {
                // IF USER DONT HAVE THE SCOPE OF ROUTING
                this.router.navigate(['unauthorized']);
                observable.next(false);
              }
            } else {
              // IF IS ONLY CHECK OF VALID TOKEN
              observable.next(true);
            }
          } else {
            // IF TOKEN IS NOT VALID
            this.router.navigate(['login'], {
              queryParams: { returnUrl: state.url }
            });
            observable.next(false);
            return;
          }
        },
        error1 => {
          // WHEN ERROR OCCUR
          this.router.navigate(['login'], {
            queryParams: { returnUrl: state.url }
          });
          observable.next(false);
        }
      );
    });
  }
}
