import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService, AuthObject} from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const authObj: AuthObject = this.authService.getAuth();
    if (!request.headers.has('Authorization') && authObj && authObj.token) {
      const cloned = request.clone({
        headers: request.headers.append('Authorization', `Bearer ${authObj.token}`)
      });
      return next.handle(cloned);
    }

    return next.handle(request);
  }
}
