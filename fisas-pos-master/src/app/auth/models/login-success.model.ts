export class LoginSuccessModel {
  token: string;
  expires_in: Date;
}
