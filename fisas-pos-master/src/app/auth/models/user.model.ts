export class UserModel {
  id: number;
  rfid: string;
  name: string;
  email: string;
  phone: string;
  user_name: string;
  institute: string;
  student_number: string;
  profile_id: number;
  profile: any;
  external: boolean;
  active: boolean;
  can_access_BO: boolean;
  address: string;
  city: string;
  country: string;
  postal_code: string;
  tin: string;
  scopes: any[];
  user_groups: any[];
  updated_at: Date;
  created_at: Date;
}
