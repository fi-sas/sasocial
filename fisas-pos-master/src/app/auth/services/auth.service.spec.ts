import { FiUrlService } from './../../core/services/url.service';
import { FiResourceService } from './../../core/services/resource.service';
import { CoreModule } from './../../core/core.module';
import { SharedModule } from './../../shared/shared.module';
import { TestBed, inject } from '@angular/core/testing';
import { AuthService } from './auth.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, CoreModule, HttpClientTestingModule],
      providers: [FiResourceService, FiUrlService]
    });
  });

  it(
    'should be created',
    inject([AuthService], (service: AuthService) => {
      expect(service).toBeTruthy();
    })
  );
});
