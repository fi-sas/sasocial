import { FiUrlService } from './../../core/services/url.service';
import { FiResourceService } from './../../core/services/resource.service';
import { LoginSuccessModel } from './../models/login-success.model';
import { UserModel } from './../models/user.model';

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { first } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { ConfigsService } from '../../services/configs.service';

export class AuthObject {
  user: UserModel;
  scopes: AuthScope[];
  token: string;
  auth_date: Date;
  expires_in: Date;
}

export class AuthScope {
  microservice: string;
  entity: string;
  permission: string;
  all: boolean;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  _authObj: AuthObject;
  _refreshTimeout = null;

  isLogged = false;
  isLoggedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  userSubject: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(
    new UserModel()
  );

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private configsService: ConfigsService,
  ) {
    this.changeIsLogged(false);
  }

  /***
   * Check's the credentials and add to the storage the login data
   * @param email
   * @param password
   */
  login(email: string, password: string): Observable<AuthObject> {
    let url = null;
    if(this.configsService.has('pos.device_id'))
      url = this.urlService.get('AUTH.LOGIN_ID', { id: this.configsService.get('pos.device_id')})
    else
      url = this.urlService.get('AUTH.LOGIN')
    
    return new Observable<AuthObject>(observer => {
      this.resourceService
        .create<LoginSuccessModel>(url, {
          email,
          password
        },{
          headers: new HttpHeaders().append("no-error", "true"),
          withCredentials: true,
        }).pipe(first()).subscribe(
          value => {

            this.createAuthObject(value.data[0]).subscribe(
              () => {
                observer.next(this.getAuth());
                observer.complete();
              },
              (err) => {
                observer.error(err);
                observer.complete();
              }
            );
          },
          error => {
            observer.error(error);
          }
        );
    });
  }

    /***
   * Check's the credentials and add to the storage the login data
   * @param email
   * @param password
   */
     loginPin(email: string, pin: string): Observable<AuthObject> {
      let url = null;
      if(this.configsService.has('pos.device_id'))
        url = this.urlService.get('AUTH.LOGIN_ID', { id: this.configsService.get('pos.device_id')})
      else
        url = this.urlService.get('AUTH.LOGIN')
      
      return new Observable<AuthObject>(observer => {
        this.resourceService
          .create<LoginSuccessModel>(url, {
            email,
            pin
          },{
            headers: new HttpHeaders().append("no-error", "true"),
            withCredentials: true,
          }).pipe(first()).subscribe(
            value => {
  
              this.createAuthObject(value.data[0]).subscribe(
                () => {
                  observer.next(this.getAuth());
                  observer.complete();
                },
                (err) => {
                  observer.error(err);
                  observer.complete();
                }
              );
            },
            error => {
              observer.error(error);
            }
          );
      });
    }

  refreshToken(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.REFRESH_TOKEN", { type: 'POS'}),
          {},
          {
            headers: new HttpHeaders().append("no-error", "true"),
            withCredentials: true,
          }
          )
          .pipe(first())
          .subscribe(
            (value) => {
              
            this.createAuthObject(value.data[0]).subscribe(
              (res) => {
                resolve(true);
                this.validateToken().pipe(first()).subscribe();
              },
              (err) => {
                resolve(true);
              }
            );
          },
          (err) => {
            resolve(true);
          }
        );
    });
  }

  createTimeoutToUpdateToken() {
    var now = new Date().getTime();
    var diff = this._authObj.expires_in.getTime() - now - (1000 * 60);
    this._refreshTimeout = setTimeout(() => {
      this.refreshToken();
      clearTimeout(this._refreshTimeout);
    }, diff);
  }

  /***
   * Creates the auth objects after authentication
   */
  createAuthObject(result: LoginSuccessModel): Observable<AuthObject> {
    return new Observable<AuthObject>(observer => {
      const authObj = new AuthObject();

      authObj.auth_date = new Date();
      authObj.token = result.token;
      authObj.expires_in = new Date(result.expires_in);
      this._authObj = authObj;

      this.resourceService
        .read<UserModel>(this.urlService.get('AUTH.LOGIN_USER'), {
        })
        .subscribe(
          value1 => {

            this.setUser(value1.data[0]);
            this.changeIsLogged(true);
            this.userSubject.next(authObj.user);


            // CREATES THE TIMEOUT TO UPDATE THE TOKEN
            this.createTimeoutToUpdateToken();

            observer.next(authObj);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  private changeIsLogged(isLogged: boolean) {
    this.isLogged = isLogged;
    this.isLoggedSubject.next(this.isLogged);
  }

  getIsLoggedObservable() {
    return this.isLoggedSubject.asObservable();
  }
  
  /***
   * Remove the data of the login
   */
  logout() {
    this._authObj = null;
    this.userSubject.next(null);

    clearTimeout(this._refreshTimeout);
    this._refreshTimeout = null;

    this.changeIsLogged(false);

    this.resourceService
    .read<any>(
      // TODO type: POS
      this.urlService.get("AUTH.LOGOUT", { type: 'POS'}),
      {
        headers: new HttpHeaders().append("no-error", "true"),
        withCredentials: true,
      }
      )
      .pipe(first())
      .subscribe(
        () => {
          }
        );
  }

  /***
   * Return the user data of the login data
   */
  getAuth(): AuthObject {
    return this._authObj;
  }

  /***
   * Return the user data of the login data
   */
  getUser(): UserModel {
    if (this.getAuth() !== null) {
      return this.getAuth().user;
    } else {
      return null;
    }
  }



  /***
   * Update the user object
   * @param user
   */
  setUser(user: UserModel): UserModel {
    if (this.getAuth() !== null) {
      this._authObj.user = user;
      return this.getAuth().user;
    } else {
      return null;
    }
  }

  /***
   * Return the scopes of the user
   */
  getScopes(): AuthScope[] {
    if (this.getAuth() !== null) {
      return this.getAuth().scopes;
    } else {
      return null;
    }
  }

  /***
   * Update the scopes of the user
   */
  setScopes(scopes: string[]): AuthScope[] {
    const scopesT: AuthScope[] = [];
    scopes.map(value => {
      const parts = value.split(':');

      const authScopeT: AuthScope = new AuthScope();
      authScopeT.microservice = parts[0];
      authScopeT.entity = parts[1];

      if (parts.length > 2) {
        authScopeT.permission = parts[2];
      } else {
        authScopeT.all = true;
      }
      scopesT.push(authScopeT);
    });

    if (this.getAuth() !== null) {
      this.getAuth().scopes = scopesT;
      return this.getAuth().scopes;
    } else {
      return null;
    }
  }

  /***
   * Return true if the user has one or more permisstion for the microservice
   * send in argument
   * @param microservice
   */
  accessToMicroservice(microservice: string): boolean {
    const resultScopes = this.getScopes().filter(
      value => value.microservice === microservice
    );

    return resultScopes.length > 0 ? true : false;
  }


  /***
   * Return true if the user have the scope send by param
   * if send only scope microservice:entity checks if have all
   * permissions if send microservice:entity:(crud operation) check if has
   * this permission
   * @param scope
   */
  hasPermission(scope: string): boolean {
    if(!scope) {
      return false;
    }

    const parts = scope.split(':');
    const scopes = this.getScopes();

    if(!scopes) {
      return false;
    }

    let resultScopes = scopes.filter(
      value => value.microservice === parts[0] && value.entity === parts[1]
    );

    if (resultScopes.length > 0) {
      // IF FIND SCOPES FOR THE MICROSERVICE AND ENTITY
      if (resultScopes.filter(value => value.all === true).length > 0) {
        // IF USER HAS ALL PERMISSIONS ON ENTITY
        return true;
      } else {
        // IF USER DONT HAVE ALL PERMISSIONS ON THE ENTITY
        if (parts.length > 2) {
          //CHECK IF THE SCOPE PARAM AS SPECIFIC PERMISSION
          resultScopes = resultScopes.filter(
            value => value.permission === parts[2]
          );
          if (resultScopes.length > 0) {
            // IF USER HAS THE PERMISSION
            return true;
          } else {
            // IF USER DONT HAV THE PERMISSION
            return false;
          }
        } else {
          // IF REACH WHERE IS BECAUSE THE SCOPE IS NOT FOR ALL PERMISSIONS
          return false;
        }
      }
    } else {
      // WHEN NOT THE MICROSERVICE AND ENTITY HAS NOT FOUND
      return false;
    }
  }

  /***
   * Return a Observable of the user object
   */
  getUserObservable(): Observable<UserModel> {
    return this.userSubject.asObservable();
  }

  /***
   * Returns the token of the user logged
   */
  getToken(): string {
     return this.getAuth() ? this.getAuth().token : null;
  }

  /***
   * Validate the token and updates the scopes of the user
   */
  validateToken(): Observable<boolean> {
    return Observable.create(observable => {

      if (!this.getToken()) {
        observable.next(false);
      } else {
      this.resourceService
        .create<{ user: UserModel; scopes: string[] }>(
          this.urlService.get('AUTH.VALIDATE_TOKEN', {}), { token: this.getToken() }
        )
        .pipe(first())
        .subscribe(
          value => {
            if (value.data.length === 0) {
              observable.next(false);
            } else {
              this.setUser(value.data[0].user);
              this.setScopes(value.data[0].scopes);
              observable.next(true);
            }
          },
          err => {
            observable.error(err);
          }
        );
      }
    });
  }
}
