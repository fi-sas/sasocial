import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';
import { CoreModule } from '../../core/core.module';

describe('UsersService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });
});
