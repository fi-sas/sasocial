import { HttpOptions } from './../../core/services/resource.service';
import { Injectable } from '@angular/core';
import { FiResourceService, Resource } from '../../core/services/resource.service';
import { FiUrlService } from '../../core/services/url.service';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private urlService: FiUrlService,
    private resourceService: FiResourceService
  ) { }

  public list(options: HttpOptions): Observable<Resource<UserModel>> {
    return this.resourceService.list(this.urlService.get('AUTH.USERS'), options);
  }
}
