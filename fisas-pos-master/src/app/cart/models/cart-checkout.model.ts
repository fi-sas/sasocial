export class CartCheckoutModel {
  tin?: string;
  email?: string;
  address?: string;
  postal_code?: string;
  city?: string;
  country?: string;
  entity?: string
  payment_method_id: string;
  account_id: number;
  user_id: number;
}


