export class PaymentMethodModel {
  id: number;
  name: string;
  tag: string;
  description: string;
  path: string;
  is_immediate: boolean;
  active: boolean;
  charge: boolean;
  gateway_data: string;
  created_at: string;
  updated_at: string;
}


