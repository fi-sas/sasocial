import { CartCheckoutModel } from './../models/cart-checkout.model';
import { CartModel } from './../models/cart.model';
import { FiUrlService } from './../../core/services/url.service';
import { FiResourceService, Resource } from './../../core/services/resource.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  createCart(cart: CartModel): Observable<Resource<CartModel>> {
    return this.resourceService.create<CartModel>(this.urlService.get('PAYMENTS.CART', {}), cart);
  }

  cartCheckout(checkout: CartCheckoutModel): Observable<Resource<CartCheckoutModel>> {
    return this.resourceService.create<CartCheckoutModel>(this.urlService.get('PAYMENTS.CART_CHECKOUT', {}), checkout);
  }
}
