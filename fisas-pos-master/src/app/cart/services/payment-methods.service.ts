import { PaymentMethodModel } from './../models/payment-method.model';
import { FiUrlService } from './../../core/services/url.service';
import { FiResourceService, Resource } from './../../core/services/resource.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  list(immediate?: boolean, active?: boolean, charge?: boolean): Observable<Resource<PaymentMethodModel>> {
    let params = new HttpParams();

    if (immediate) {
        params = params.set('query[is_immediate]', '' + immediate);
    }

    if (active) {
      params = params.set('query[active]', '' + active);
    }

    if (charge) {
      params = params.set('query[charge]', '' + charge);
    }

    return this.resourceService.list<PaymentMethodModel>(this.urlService.get('PAYMENTS.PAYMENTS_METHODS'), { params });
  }

  read(id: number): Observable<Resource<PaymentMethodModel>> {
    return this.resourceService.read<PaymentMethodModel>(this.urlService.get('PAYMENTS.PAYMENTS_METHODS_ID', { id }), {});
  }

}
