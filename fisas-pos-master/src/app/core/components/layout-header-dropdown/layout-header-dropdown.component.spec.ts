import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutHeaderDropdownComponent } from './layout-header-dropdown.component';
import { SharedModule } from '../../../shared/shared.module';
import { FiResourceService } from '../../services/resource.service';
import { FiUrlService } from '../../services/url.service';
import { RouterModule } from '@angular/router';

describe('LayoutHeaderDropdownComponent', () => {
  let component: LayoutHeaderDropdownComponent;
  let fixture: ComponentFixture<LayoutHeaderDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule, RouterTestingModule, RouterModule
      ],
      declarations: [ LayoutHeaderDropdownComponent ],
      providers: [
        FiResourceService,
        FiUrlService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
