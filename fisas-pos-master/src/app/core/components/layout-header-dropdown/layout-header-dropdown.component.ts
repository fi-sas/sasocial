import { Router } from '@angular/router';
import { AuthService } from './../../../auth/services/auth.service';
import { UserModel } from './../../../auth/models/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fisas-layout-header-dropdown',
  templateUrl: './layout-header-dropdown.component.html',
  styleUrls: ['./layout-header-dropdown.component.less']
})
export class LayoutHeaderDropdownComponent implements OnInit {
  userModel: UserModel;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authService.getUserObservable().subscribe(user => {
      this.userModel = user;
    });

    this.userModel = this.authService.getUser();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
