import { FiUrlService } from './../../services/url.service';
import { FiResourceService } from './../../services/resource.service';
import { LayoutHeaderDropdownComponent } from './../layout-header-dropdown/layout-header-dropdown.component';
import { SharedModule } from './../../../shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderComponent } from './layout-header.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

describe('LayoutHeaderComponent', () => {
  let component: LayoutHeaderComponent;
  let fixture: ComponentFixture<LayoutHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterModule,
        RouterTestingModule
      ],
      declarations: [ LayoutHeaderComponent, LayoutHeaderDropdownComponent ],
      providers: [
        FiResourceService,
        FiUrlService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
