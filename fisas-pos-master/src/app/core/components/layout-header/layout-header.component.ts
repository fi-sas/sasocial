import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'fisas-layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.less']
})
export class LayoutHeaderComponent implements OnInit {
  time = moment().format('H:mm:ss');

  constructor() {
    setInterval(() => {
      this.time = moment().format('H:mm:ss');
  }, 1000)
  }

  ngOnInit() {
  }

}
