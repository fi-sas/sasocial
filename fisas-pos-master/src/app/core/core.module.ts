import { FiStorageService } from './services/storage.service';
import { FiConfigurator } from './../libs/configurator/configurator.service';
import { HttpClient } from '@angular/common/http';
import { FiResourceService } from './services/resource.service';
import { FiUrlService } from './services/url.service';
import { SharedModule } from './../shared/shared.module';
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';
import { LayoutHeaderComponent } from './components/layout-header/layout-header.component';
import { LayoutFooterComponent } from './components/layout-footer/layout-footer.component';
import { FiConfiguratorModule } from '../libs/configurator/configurator.module';
import { LayoutHeaderDropdownComponent } from './components/layout-header-dropdown/layout-header-dropdown.component';

@NgModule({
  declarations: [
    FullLayoutComponent,
    LayoutHeaderComponent,
    LayoutFooterComponent,
    LayoutHeaderDropdownComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FiConfiguratorModule
  ],
  providers: [
    { provide: FiUrlService, useClass: FiUrlService, deps: [FiConfigurator] },
    {
      provide: FiResourceService,
      useClass: FiResourceService,
      deps: [HttpClient, FiConfigurator]
    },
    { provide: FiStorageService, useClass: FiStorageService }
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('Core Module is already loaded use only on Root Module');
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      // providers: [UiService]
    };
  }
}
