import { FiUrlService } from './../../services/url.service';
import { FiResourceService } from './../../services/resource.service';
import { CoreModule } from './../../core.module';
import { LayoutHeaderDropdownComponent } from './../../components/layout-header-dropdown/layout-header-dropdown.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from './../../../shared/shared.module';
import { LayoutFooterComponent } from './../../components/layout-footer/layout-footer.component';
import { LayoutHeaderComponent } from './../../components/layout-header/layout-header.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullLayoutComponent } from './full-layout.component';

describe('FullLayoutComponent', () => {
  let component: FullLayoutComponent;
  let fixture: ComponentFixture<FullLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      declarations: [
        FullLayoutComponent,
        LayoutHeaderComponent,
        LayoutHeaderDropdownComponent,
      LayoutFooterComponent
     ],
     providers: [
       FiResourceService,
       FiUrlService
     ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
