export class BalanceModel {
  account_id: number;
  account_name: string;
  advanced_value: number;
  current_balance: number;
  paid_value: number;
  in_debt: number;
  available_methods: any[];
}


