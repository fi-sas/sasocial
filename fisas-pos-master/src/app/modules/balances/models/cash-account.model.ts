
export class CashAccountModel {
    id: number;
    code: string;
    description: string;
    iban: string;
    users: CashAccountUserModel[];
    created_at: Date;
    updated_at: Date;
}

export class CashAccountUserModel {
    id: number;
    name: string;
}
