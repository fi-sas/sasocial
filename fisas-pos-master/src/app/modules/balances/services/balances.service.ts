import { BalanceModel } from './../models/balance.model';
import { FiUrlService } from './../../../core/services/url.service';
import { FiResourceService, Resource } from './../../../core/services/resource.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { CashAccountModel } from '../models/cash-account.model';


@Injectable({
  providedIn: 'root'
})
export class BalancesService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  balances(user_id: number): Observable<Resource<BalanceModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'available_methods');
    params = params.set('query[user_id]', user_id.toString());
    return this.resourceService.list<BalanceModel>(this.urlService.get('CURRENT_ACCOUNT.BALANCES', { }), {
      params: params
    });
  }

  getUserAvailableCashAccounts(): Observable<Resource<CashAccountModel>> {
    const url = this.urlService.get('CURRENT_ACCOUNT.CASH_ACCOUNTS_AVAILABLE_ME');
    return this.resourceService.list<CashAccountModel>(url);
  }

  
}
