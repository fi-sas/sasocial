import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServeBarComponent } from './pages/serve-bar/serve-bar.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'serve-bar',
    pathMatch: 'full'
  },
  {
    path: 'serve-bar',
    component: ServeBarComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarRoutingModule { }
