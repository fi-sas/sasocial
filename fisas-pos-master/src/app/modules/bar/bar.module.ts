import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarRoutingModule } from './bar-routing.module';
import { ServeBarComponent } from './pages/serve-bar/serve-bar.component';
import { BarHistoryComponent } from './components/bar-history/bar-history.component';
import { SharedModule } from '../../shared/shared.module';
import { UserInfoBarComponent } from './components/user-info-bar/user-info-bar.component';
import { StockComponent } from './components/stock/stock.component';
import { StockDrawerService } from './services/stock-drawer.service';
import { SearchUserModule } from '../search-user/search-user.module';
import { UnchargeAccountModule } from '../uncharge-account/uncharge-account.module';
import { ChargeAccountModule } from '../charge-account/charge-account.module';
import { MovementsModule } from '../movements/movements.module';
import { BarProductsComponent } from './components/bar-products/bar-products.component';
import { OrderHistoryComponent } from './components/orders-history/orders-history.component';
import { OrderHistoryDrawerService } from './services/order-history-drawer.service';
import { BoxHistoryModule } from '../box-history/box-history.module';

@NgModule({
  declarations: [
    ServeBarComponent,
    BarHistoryComponent,
    UserInfoBarComponent,
    StockComponent,
    BarProductsComponent,
    OrderHistoryComponent
  ],
  imports: [
    CommonModule,
    BarRoutingModule,
    SharedModule,
    SearchUserModule,
    UnchargeAccountModule,
    ChargeAccountModule,
    BoxHistoryModule,
    MovementsModule
  ],
  providers: [
    StockDrawerService,
    OrderHistoryDrawerService
  ],
  entryComponents: [
    StockComponent,
    OrderHistoryComponent
  ]

})
export class BarModule { }
