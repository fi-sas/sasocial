import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '../../../../shared/shared.module';
import { BarHistoryComponent } from './bar-history.component';

describe('BarHistoryComponent', () => {
    let component: BarHistoryComponent;
    let fixture: ComponentFixture<BarHistoryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                BarHistoryComponent
            ],
            imports: [
                SharedModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BarHistoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
