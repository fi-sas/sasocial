
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';
import { OrdersModel } from '../../models/orders.model';
import { BarService } from '../../services/bar.service';

@Component({
    selector: 'fisas-bar-history',
    templateUrl: './bar-history.component.html',
    styleUrls: ['./bar-history.component.less']
})
export class BarHistoryComponent implements OnInit, OnDestroy {
    isLoadingRefresh = false;
    interval = null;
    ordersWait: OrdersModel[] = [];
    _ordersWait: Subscription = null;
    orders: OrdersModel[] = [];
    _orders: Subscription = null;
    idOrderSelect: number = -1;
    orderLoadingTotal = false;
    _selectedOrderLoadingTotal: Subscription = null;
    _idOrder: Subscription = null;

    constructor(private barService: BarService) { }

    ngOnInit() {
        this.barService.refreshListOrders(false).subscribe();
        if(this.idOrderSelect == -1) {
            this.barService.selectNextOrderObservable().subscribe(data => {
                if(this.idOrderSelect == -1) {
                    this.idOrderSelect = data;
                    this.getInfoByIdOrder(this.idOrderSelect);
                }
               
            });
        }
        this._selectedOrderLoadingTotal = this.barService
            .orderLoadingTotalObservable()
            .subscribe(Loading => {
                this.orderLoadingTotal = Loading;
            });

        this._idOrder = this.barService.selectedUserBarObservable().subscribe(order => {
            if (!order) {
                this.idOrderSelect = -1;
            }

        });
        this.getWait();
        this.getList();

        this.interval = setInterval(() => {
            this.refreshList();
        }, 10000);
    }



    ngOnDestroy() {
        if (this.interval) {
            clearInterval(this.interval);
        }

    }

    getInfoByIdOrder(idOrder: number) {
        this.idOrderSelect = idOrder;
        this.barService.getInfoByIdOrder(idOrder);

    }

    getWait() {
        this._ordersWait = this.barService
            .selectedlistOrderWaitObservable()
            .subscribe(order => {
                this.ordersWait = order;
            });
    }

    getList() {
        this._orders = this.barService
            .selectedlistOrderNotServedObservable()
            .subscribe(order => {
                this.orders = order;
            });

    }

    refreshList() {
        this.orderLoadingTotal = true;
        this.barService.refreshListNotServed().pipe(first(),finalize(()=>this.orderLoadingTotal = false)).subscribe();
    }

    refreshListTotal() {
        this.isLoadingRefresh = true;
        this.barService.refreshListOrders(true).pipe(finalize(() => this.isLoadingRefresh = false)).subscribe(() => {
            if (this.idOrderSelect && this.idOrderSelect != -1) {
                this.barService.getInfoByIdOrder(this.idOrderSelect);
                this._idOrder = this.barService.selectedUserBarObservable().subscribe(order => {
                    if (order) {
                        this.idOrderSelect = order.id;
                    } else {
                        this.idOrderSelect = -1;
                    }

                });
            }
        });

    }
}
