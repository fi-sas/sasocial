import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '../../../../shared/shared.module';
import { BarProductsComponent } from './bar-products.component';

describe('BarProductsComponent', () => {
    let component: BarProductsComponent;
    let fixture: ComponentFixture<BarProductsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                BarProductsComponent
            ],
            imports: [
                SharedModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BarProductsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
