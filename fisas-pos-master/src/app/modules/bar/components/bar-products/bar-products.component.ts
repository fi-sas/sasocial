
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';
import { OrdersModel } from '../../models/orders.model';
import { ProductModel } from '../../models/product.model';
import { BarService } from '../../services/bar.service';

@Component({
    selector: 'fisas-bar-products',
    templateUrl: './bar-products.component.html',
    styleUrls: ['./bar-products.component.less']
})
export class BarProductsComponent implements OnInit, OnDestroy {
    isloadingWaitOrder = false;
    isloadingDeliverOrder = false;
    orderSelectedBar: OrdersModel = null;
    _orderSelectedBar: Subscription = null;
    orderLoading = false;
    _selectedOrderLoading: Subscription = null;
    modalDeleteItemVisible = false;
    isloadingDeleteOrder = false;
    selectProd: ProductModel = new ProductModel();
    loadingDeliverItem = false;
    constructor(
        private barService: BarService,) { }

    ngOnInit() {
        this._orderSelectedBar = this.barService.selectedUserBarObservable().subscribe(order => {
            this.orderSelectedBar = order;
        });

        this._selectedOrderLoading = this.barService
        .orderLoadingObservable()
        .subscribe(Loading => {
            this.orderLoading = Loading;
        });

    }

    ngOnDestroy() {
        this._orderSelectedBar.unsubscribe();
        this._selectedOrderLoading.unsubscribe();
    }

    wait() {
        this.isloadingWaitOrder = true;
        this.barService.waitOrder(this.orderSelectedBar.id).pipe(first(), finalize(() => this.isloadingWaitOrder = false)).subscribe((data) => {
            this.barService.refreshListOrders(false).subscribe();
            this.barService.selectNextOrder();
        })

    }

    deliver() {
        this.isloadingDeliverOrder = true;
        this.barService.serveOrder(this.orderSelectedBar.id).pipe(first(), finalize(() => this.isloadingDeliverOrder = false)).subscribe((data) => {
            this.barService.refreshListOrders(false).subscribe();
            this.barService.selectNextOrder();
        })
    }

    closeItem(prod) {
        this.selectProd = prod;
        this.modalDeleteItemVisible = true; 
    }



    deleteItem() {
        this.isloadingDeleteOrder = true;
        this.barService.deleteItemOrder(this.selectProd.id).pipe(first(), finalize(() => this.isloadingDeleteOrder = false)).subscribe((data) => {
            this.modalDeleteItemVisible = false;
            this.barService.getOrderById(this.orderSelectedBar.id).pipe(first()).subscribe((data)=> {
                this.barService.setUserBar(data.data[0]);
                if(data.data[0].status == 'SERVED' || data.data[0].status == 'CANCELED') {
                    this.barService.refreshListOrders(false).subscribe();
                    this.barService.selectNextOrder();
                }
            });
        })
    }

    deliverItem(prod) {
        this.loadingDeliverItem = true;
        this.selectProd = prod;
        this.barService.serveItemOrder(this.selectProd.id).pipe(first(), finalize(()=>this.loadingDeliverItem = false)).subscribe((data) => {
            this.barService.getOrderById(this.orderSelectedBar.id).pipe(first()).subscribe((data)=> {
                this.barService.setUserBar(data.data[0]);
                if(data.data[0].status == 'SERVED' || data.data[0].status == 'CANCELED') {
                    this.barService.refreshListOrders(false).subscribe();
                    this.barService.selectNextOrder();
                }
            });

        })
    }
}
