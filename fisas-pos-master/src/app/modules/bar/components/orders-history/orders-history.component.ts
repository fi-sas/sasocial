import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { OrdersModel } from '../../models/orders.model';
import { BarService } from '../../services/bar.service';

@Component({
  selector: 'fisas-orders-history',
  templateUrl: './orders-history.component.html',
  styleUrls: ['./orders-history.component.less']
})
export class OrderHistoryComponent implements OnInit {
  pageIndex = 1;
  pageSize = 10;
  totalData = 0;

  selectOptions = [
    {
      value: moment().toISOString(),
      label: 'Hoje'
    },
    {
      value: moment().subtract(1, 'day').toISOString(),
      label: 'Ontem'
    },
    {
      value: 'CUSTOM',
      label: 'Personalizado'
    }
  ];

  loading = false;
  orders: OrdersModel[] = [];

  // Value from select input
  selectedValue;

  dateFormat = 'yyyy/MM/dd';
  startDate = null;
  endDate = null;

  disableStartDate = (current: Date): boolean => {
    if(this.endDate !== null) {
      return !moment(this.endDate).isBetween(moment(current), moment());
    }
    return current > new Date();
  };

  disableEndDate = (current: Date): boolean => {
    if (this.startDate !== null) {
      return !moment(current).isBetween(moment(this.startDate), moment());
    }
    return current > new Date();
    
  };

  constructor(
    private barService: BarService,
    private modalService: NzModalService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.selectedValue = this.selectOptions[0].value;
    this.getOrdersServed(null, null, this.selectedValue);
  }

  /**
   *
   */
  onChangeSelectInput() {
    if (this.selectedValue !== 'CUSTOM') {
      this.startDate = null;
      this.endDate = null;
      this.getOrdersServed(null, null, this.selectedValue, true);
    }else{
      this.orders = [];
    }
  }

  /**
   *
   * @param startDate
   * @description
   */
  onChangeStartDate(startDate) {
    this.startDate = startDate.toISOString();
    if (this.startDate !== null && this.endDate !== null) {
      this.getOrdersServed(this.startDate, this.endDate, null, true);
    }
  }

  /**
   *
   * @param endDate
   */
  onChangeEndDate(endDate) {
    this.endDate = endDate.toISOString();
    if (this.startDate !== null && this.endDate !== null) {
      this.getOrdersServed(this.startDate, this.endDate, null,true);
    }
  }

  /**
   *
   * @param startDate
   * @param endDate
   * @param date
   * @description get all reservations served on a certain date
   */
  getOrdersServed(startDate: string, endDate: string, date: string, reset: boolean = false): void {
    this.loading = true;
    if (reset) {
      this.pageIndex = 1;
    }

    this.barService.getOrdersServed(startDate,endDate,date, this.pageIndex,
      this.pageSize
    ).pipe(first()).subscribe(data => {
      this.orders = data.data;
      this.totalData = data.link.total;
      this.loading = false;
    }, () => {
      this.loading = false;
    })
  }


  markUnserved(id: number) {
    this.modalService.confirm({
      nzTitle: 'Vai marcar este pedido como não servido. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markUnservedSubmit(id)
    });

  }

  markUnservedSubmit(id: number) {
    this.barService
      .unServed(id)
      .pipe(first())
      .subscribe(result => {
        this.message.success('Pedido marcado como não servido com sucesso');
        this.getOrdersServed(null, null, this.selectedValue);
      });

  }

  markServed(id: number) {
    this.modalService.confirm({
      nzTitle: 'Vai marcar este pedido como servido. Tem a certeza que dejesa continuar?',
      nzOnOk: () => this.markServedSubmit(id)
    });
  }

  markServedSubmit(id: number) {
    this.barService.serveOrder(id)
    .pipe(first())
      .subscribe(result => {
        this.message.success('Pedido marcado como servido com sucesso');
        this.getOrdersServed(null, null, this.selectedValue);
      });
  }


}
