
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ConfigsService } from '../../../../services/configs.service';
import { CategoriesModel } from '../../models/categories.model';
import { OperationStockModel } from '../../models/operation-stock.model';
import { ProductModel } from '../../models/product.model';
import { stockModel } from '../../models/stock.model';
import { StockService } from '../../services/stock.service';

@Component({
    selector: 'fisas-stock',
    templateUrl: './stock.component.html',
    styleUrls: ['./stock.component.less']
})
export class StockComponent implements OnInit {
    loadingFam = true;
    loading = false;
    service_id = null;
    productsLoading = false;
    categories: CategoriesModel[] = [];
    catSelect: CategoriesModel = null;
    catSelectId: number = -1;
    products: ProductModel[] = [];
    modalStockEdit: boolean = false;
    productSelect: ProductModel = null;
    openQTD = false;
    input: number = 0;
    lotes: stockModel[] = [];
    loteSelect: string;
    loteLoading = false;
    filterValue: string = '';
    removeQtd = false;
    step2 = false;
    reason: string;
    anotherReason: string;
    modalStockHistory = false;
    history = false;
    operationStock: OperationStockModel[] = [];
    loadingOperation = false;
    wharehouseSelect;
    quantityExist: number;
    commaActive: boolean = false;
    pageIndex = 1;
    pageSize = 10;
    totalData = 0;
    dateExpired: Date;
    inputType = 'select';
    constructor(private stockService: StockService, private configService: ConfigsService,
        private drawerRef: NzDrawerRef<boolean>, private cdr: ChangeDetectorRef) {
        this.service_id = this.configService.get('service.service_bar_id');
    }

    ngOnInit() {
        this.getCategories();
    }


    getCategories() {
        this.stockService.getCategories().pipe(first(), finalize(() => this.loadingFam = false)).subscribe((data) => {
            this.categories = data.data;
            if (this.categories.length > 0) {
                this.categories = this.categories.sort((a, b) => {
                    if(a.translations.find(tran => tran.language_id ==3) && b.translations.find(tran => tran.language_id ==3)) {
                        if (a.translations.find(tran => tran.language_id ==3).name.toUpperCase() > b.translations.find(tran => tran.language_id ==3).name.toUpperCase()) {
                            return 1;
                        }
                        if (a.translations.find(tran => tran.language_id ==3).name.toUpperCase() < b.translations.find(tran => tran.language_id ==3).name.toUpperCase()) {
                            return -1;
                        }
                        return 0;
                    }

                });
                this.catSelectId = this.categories[0].id;
                this.catSelect = this.categories[0];
                this.getProdutsByCategory();
            }
        })
    }

    getProdutsByCategory() {
        this.productsLoading = true;
        this.service_id = this.configService.get('service.service_bar_id');
        this.stockService.getProducts(this.catSelectId, this.service_id, this.filterValue).pipe(first(), finalize(() => this.productsLoading = false)).subscribe((data) => {
            this.products = data.data;
            if (this.products.length > 0) {
                this.products = this.products.sort((a, b) => {
                    if(a.translations.find(tran => tran.language_id ==3) && b.translations.find(tran => tran.language_id ==3)) {
                        if (a.translations.find(tran => tran.language_id ==3).name.toUpperCase() > b.translations.find(tran => tran.language_id ==3).name.toUpperCase()) {
                            return 1;
                        }
                        if (a.translations.find(tran => tran.language_id ==3).name.toUpperCase() < b.translations.find(tran => tran.language_id ==3).name.toUpperCase()) {
                            return -1;
                        }
                        return 0;
                    }
                });
            }
           
        })

    }

    closeStock(success = false) {
        this.drawerRef.close(success);
    }

    addToInput(number: string) {
        this.input === null ? this.input = 0 : null;
        if (this.input === 0) {
            if (this.commaActive) {
                this.input = parseFloat('0.' + number);
                this.commaActive = false;
            } else {
                this.input = parseFloat(number);
            }
        } else {
            if (this.commaActive) {
                this.input = parseFloat(this.input.toString().concat('.' + number));
                this.commaActive = false;
            } else {
                this.input = parseFloat(this.input.toString().concat(number));
            }
        }
    }

    activeComma() {
        this.commaActive = true;
    }

    removeFromInput() {
        this.input = parseFloat(this.input.toString().slice(0, -1));
        if (!this.input) {
            this.input = 0;
        }
    }

    getLotes(prod) {
        this.service_id = this.configService.get('service.service_bar_id');
        this.stockService.getWharehouse(this.service_id).pipe(first()).subscribe((data) => {
            if (data.data.length > 0) {
                this.wharehouseSelect = data.data[0];
                this.stockService.getLotesByProductWharehouse(prod.id, this.wharehouseSelect.wharehouse_id).pipe(first()).subscribe((data) => {
                    this.lotes = data.data;
                })
            }
        })

    }

    remove(prod) {
        this.removeQtd = true;
        this.history = false;
        this.modalStockEdit = true;
        this.productSelect = prod;
        this.getLotes(prod);
    }

    add(prod) {
        this.removeQtd = false;
        this.history = false;
        this.modalStockEdit = true;
        this.productSelect = prod;
        this.inputType = 'select';
        this.getLotes(prod);
    }

    clearVariable() {
        this.input = 0;
        this.reason = '';
        this.anotherReason = '';
        this.loteSelect = null;
        this.inputType = 'select';
    }

    closeModalEdit() {
        this.modalStockEdit = false; 
        this.openQTD = false; 
        this.removeQtd = false; 
        this.step2 = false; 
        this.dateExpired = null;
        this.clearVariable();
    }

    selecMovHistory(reset: boolean = false, prod) {
        if (reset) {
            this.pageIndex = 1;
        }
       this.clearVariable();
        this.productSelect = prod;
        this.loadingOperation = true;
        this.modalStockHistory = true;
        this.operationStock = [];
        this.service_id = this.configService.get('service.service_bar_id');
        this.stockService.getWharehouse(this.service_id).pipe(first()).subscribe((data) => {
            this.wharehouseSelect = data.data[0];
            if (data.data.length > 0) {
                this.stockService.movimentsHistory(this.pageIndex,
                    this.pageSize, this.productSelect.id, data.data[0].wharehouse_id).pipe(first(), finalize(() => this.loadingOperation = false)).subscribe((dataMov) => {
                        this.operationStock = dataMov.data;
                        this.totalData = dataMov.link.total;

                    })
            }
        })

    }

    changeLote(event) {
        this.lotes.forEach((data) => {
            if (data.lote == event) {
                this.quantityExist = data.quantity;
            }
        })
    }


    saveQtdPlus() {
        this.loading = true;
        let sendValue: stockModel = new stockModel();
        sendValue.lote = this.loteSelect.length==1 ? this.loteSelect[0] : this.loteSelect;
        sendValue.expired = this.dateExpired;
        sendValue.quantity = this.input;
        sendValue.product_id = this.productSelect.id;
        sendValue.operation = "in";
        sendValue.wharehouse_id = this.wharehouseSelect.wharehouse_id;
        sendValue.stock_reason = this.reason == 'Outro' ? this.anotherReason : this.reason;
        this.stockService.saveStock(sendValue).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
            this.modalStockEdit = false;
            this.getProdutsByCategory();
            this.clearVariable();
        })
    }

    saveQtdRemove() {
        this.loading = true;
        let sendValue: stockModel = new stockModel();
        sendValue.lote = this.loteSelect;
        sendValue.product_id = this.productSelect.id;
        sendValue.quantity = this.input;
        sendValue.operation = "out";
        sendValue.stock_reason = this.reason == 'Outro' ? this.anotherReason : this.reason;
        sendValue.wharehouse_id = this.wharehouseSelect.wharehouse_id;
        this.stockService.saveStock(sendValue).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
            this.modalStockEdit = false;
            this.getProdutsByCategory();
            this.clearVariable();
        })
    }



}
