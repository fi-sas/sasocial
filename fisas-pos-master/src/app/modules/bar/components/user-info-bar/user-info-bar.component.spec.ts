import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '../../../../shared/shared.module';
import { UserInfoBarComponent } from './user-info-bar.component';

describe('UserInfoBarComponent', () => {
    let component: UserInfoBarComponent;
    let fixture: ComponentFixture<UserInfoBarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                UserInfoBarComponent
            ],
            imports: [
                SharedModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserInfoBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
