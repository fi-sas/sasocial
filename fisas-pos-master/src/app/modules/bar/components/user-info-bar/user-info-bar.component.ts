
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';
import { UserModel } from '../../../../auth/models/user.model';
import { BalanceModel } from '../../../balances/models/balance.model';
import { ChargeDrawerService } from '../../../charge-account/services/charge-drawer.service';
import { MovementsDrawerService } from '../../../movements/services/movements-drawer.service';
import { UnchargeDrawerService } from '../../../uncharge-account/services/uncharge-drawer.service';
import { OrdersModel } from '../../models/orders.model';
import { BarService } from '../../services/bar.service';

@Component({
    selector: 'fisas-user-info-bar',
    templateUrl: './user-info-bar.component.html',
    styleUrls: ['./user-info-bar.component.less']
})
export class UserInfoBarComponent implements OnInit, OnDestroy {

    orderSelectedBar: OrdersModel = null;
    _orderSelectedBar: Subscription = null;
    orderLoading = false;
    _selectedOrderLoading: Subscription = null;


    balancesLoading = false;
    _selectedUserBarBalances: Subscription = null;
    _selectedUserBarBalancesLoading: Subscription = null;
    balance: BalanceModel = null;
    boxSelected;
    currentPositive = true;
    currentZero = false;
    isloadingDeleteOrder = false;
    modalDeleteOrderVisible = false;

    constructor(
        private barService: BarService,
        private chargeDrawerService: ChargeDrawerService,
        private movementsService: MovementsDrawerService,
        private unChargeServiceDrawer: UnchargeDrawerService) { }

    ngOnInit() {
        this.boxSelected = localStorage.getItem('boxSelected');
        this._orderSelectedBar = this.barService.selectedUserBarObservable().subscribe(order => {
            this.orderSelectedBar = order;
        });

        this._selectedUserBarBalances = this.barService
            .balancesObservable()
            .subscribe(balances => {
                this.balance = balances.length > 0 ? this.balance = balances[0] : null;

                if (this.balance) {
                    this.currentPositive = this.balance.current_balance >= 0;
                    this.currentZero = this.balance.current_balance === 0;
                }
            });

        this._selectedUserBarBalancesLoading = this.barService
            .balancesLoadingObservable()
            .subscribe(balancesLoading => {
                this.balancesLoading = balancesLoading;
            });
        this._selectedOrderLoading = this.barService
            .orderLoadingObservable()
            .subscribe(Loading => {
                this.orderLoading = Loading;
            });
    }

    ngOnDestroy() {
        this._orderSelectedBar.unsubscribe();
        this._selectedUserBarBalances.unsubscribe();
        this._selectedUserBarBalancesLoading.unsubscribe();
        this._selectedOrderLoading.unsubscribe();
    }


    chargeAccount() {
        const order = this.barService.getUserBar();
        if (order) {
            const drawer = this.chargeDrawerService.chargeAccountDrawer(order.user);
            drawer.afterClose.pipe(first()).subscribe(data => {
                if (data) {
                    this.barService.updateBalancesBar();
                }
            });
        }
    }

    /**
   * @description open the drawer to uncharge the account of user
   */
    unChargeAccount() {
        const order = this.barService.getUserBar();
        if (order) {
            const drawer = this.unChargeServiceDrawer.unchargeAccountDrawer(order.user);
            drawer.afterClose.pipe(first()).subscribe(data => {
                if (data) {
                    this.barService.updateBalancesBar();
                }
            });
        }
    }

    /**
   * Show de movements
   */
    movements() {
        this.movementsService.movementsDrawer('barOrder');
    }

    deleteOrder() {
        this.isloadingDeleteOrder = true;
        this.barService.cancelOrder(this.orderSelectedBar.id).pipe(first(), finalize(() => this.isloadingDeleteOrder = false)).subscribe((data) => {
            this.modalDeleteOrderVisible = false;
            this.barService.refreshListOrders(false).subscribe();
            this.barService.selectNextOrder();
        })

    }
}
