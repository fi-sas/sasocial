import { ProductModel } from "./product.model";

export class CategoriesModel {
    active: boolean;
    created_at: Date;
    fentity_id: number;
    id: number; 
    translations: TranslationsModel[] = [];
    products: ProductModel[] = [];
}

export class TranslationsModel {
    family_id: number;
    language_id: number;
    name: string;
}