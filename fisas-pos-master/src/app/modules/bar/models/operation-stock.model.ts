import { stockModel } from "./stock.model";

export class OperationStockModel {
    id: number;
    lote: string;
    operation: string;
    quantity: number;
    quantity_after: number;
    quantity_before: number;
    stock_id: number;
    stock_reason: string;
    created_at: Date;
    stock: stockModel;
    transfer_wharehouse_id: number;
}