import { UserModel } from "../../../auth/models/user.model";

export class OrdersModel {
    id: number;
    created_at: Date;
    served: boolean;
    order_lines: any[];
    served_at: Date;
    user: UserModel;
    user_served: UserModel;
    user_id: number;
    status: string;
    total_price: number;
    updated_at: Date;
}