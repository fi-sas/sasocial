import { stockModel } from "./stock.model";

export class ProductModel {
    account_id: number;
    allergens: any[];
    available: boolean;
    code: string;
    complements: any[];
    id: number;
    isStockAvailable: boolean;
    price: number;
    prices: any[]
    service_id: number;
    show_derivatives: boolean;
    stockQuantity: number;
    translations: TranslationModel[] = [];
    user_allergic: boolean;
    status: string;
    product: ProductModel;
    stocks: stockModel[];
    unit: any;
}

export class TranslationModel {
    description: string;
    name: string;
    language_id: number;
    product_id: number;
}
