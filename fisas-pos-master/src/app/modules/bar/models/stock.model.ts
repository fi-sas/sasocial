import { ProductModel } from "./product.model";

export class stockModel {
    id: number;
    lote: string;
    quantity: number;
    wharehouse_id: number;
    operation: string;
    stock_reason: string;
    product: ProductModel;
    wharehouse: any;
    product_id: number;
    expired: Date;
}