import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { SharedModule } from '../../../../shared/shared.module';

import { CoreModule } from '../../../../core/core.module';
import { ElectronService } from '../../../../services/electron.service';
import { ServeBarComponent } from './serve-bar.component';
import { BarHistoryComponent } from '../../components/bar-history/bar-history.component';
import { UserInfoBarComponent } from '../../components/user-info-bar/user-info-bar.component';
import { StockComponent } from '../../components/stock/stock.component';
import { StockDrawerService } from '../../services/stock-drawer.service';
import { BarProductsComponent } from '../../components/bar-products/bar-products.component';
import { OrderHistoryComponent } from '../../components/orders-history/orders-history.component';

describe('ServeBarComponent', () => {
    let component: ServeBarComponent;
    let fixture: ComponentFixture<ServeBarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ServeBarComponent,
                BarHistoryComponent,
                UserInfoBarComponent,
                StockComponent,
                BarProductsComponent,
                OrderHistoryComponent
            ],
            imports: [
                SharedModule,
                CoreModule
            ],
            providers: [
                ElectronService,
                StockDrawerService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ServeBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
