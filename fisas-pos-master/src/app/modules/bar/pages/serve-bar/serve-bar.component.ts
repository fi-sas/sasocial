import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';
import { UserModel } from '../../../../auth/models/user.model';
import { ConfigsService } from '../../../../services/configs.service';
import { BalanceModel } from '../../../balances/models/balance.model';
import { BoxDrawerService } from '../../../box-history/service/box-drawer.service';
import { ChargeDrawerService } from '../../../charge-account/services/charge-drawer.service';
import { MovementsDrawerService } from '../../../movements/services/movements-drawer.service';
import { SearchDrawerService } from '../../../search-user/services/search-drawer.service';
import { UnchargeDrawerService } from '../../../uncharge-account/services/uncharge-drawer.service';
import { BarService } from '../../services/bar.service';
import { OrderHistoryDrawerService } from '../../services/order-history-drawer.service';
import { StockDrawerService } from '../../services/stock-drawer.service';

@Component({
  selector: 'fisas-serve-bar',
  templateUrl: './serve-bar.component.html',
  styleUrls: ['./serve-bar.component.less']
})
export class ServeBarComponent implements OnInit, OnDestroy {
  modalUser = false;
  changeInterval = null;
  service_id: number;
  loadingStock = false;
  activeUser: UserModel = null;
  _selectedUser: Subscription = null;
  stockView = false;
  _selectedUserBalances: Subscription = null;
  _selectedUserBalancesLoading: Subscription = null;
  balance: BalanceModel = null;
  balancesLoading = false;

  currentPositive = true;
  currentZero = false;
  boxSelected;
  
  constructor(
    private stockDrawerService: StockDrawerService,
    private configService:ConfigsService,
    private orderHistoryDrawerService: OrderHistoryDrawerService,
    private boxDrawerService: BoxDrawerService,
    private searchDrawerService: SearchDrawerService,
    private barService: BarService,
    private ref: ChangeDetectorRef,
    private chargeDrawerService: ChargeDrawerService,
    private unChargeServiceDrawer: UnchargeDrawerService,
    private movementsService: MovementsDrawerService,
  ) { }

  ngOnInit() {
    this.change();
    this.boxSelected = localStorage.getItem('boxSelected');
    this._selectedUser = this.barService.selectedUserObservable().subscribe(user => {
      this.activeUser = user;
    });

    this._selectedUserBalances = this.barService
      .balancesObservable()
      .subscribe(balances => {
        this.balance = balances.length > 0 ? this.balance = balances[0] : null;

        if (this.balance) {
          this.currentPositive = this.balance.current_balance >= 0;
          this.currentZero = this.balance.current_balance === 0;
        }
      });

    this._selectedUserBalancesLoading = this.barService
      .balancesLoadingObservable()
      .subscribe(balancesLoading => {
        this.balancesLoading = balancesLoading;
      });
    this.accessWharehouses();
  }

  ngOnDestroy() {
    clearInterval(this.changeInterval);
    this._selectedUser.unsubscribe();
    this._selectedUserBalances.unsubscribe();
    this._selectedUserBalancesLoading.unsubscribe();
  }

  stock() {
    this.stockDrawerService.stockDrawer();
  }

  searchUser() {
    this.searchDrawerService.searchUserDrawer().pipe(first()).subscribe(user => {
      if (user) {
        this.barService.setUser(user);
      }
    });
  }

  change() {
    this.changeInterval = setInterval(() => {
      this.ref.detectChanges();
    }, 500);
  }

  chargeAccount() {
    if (this.barService.getUser()) {
      const drawer = this.chargeDrawerService.chargeAccountDrawer(this.barService.getUser());
      drawer.afterClose.pipe(first()).subscribe(data => {
        if (data) {
          this.barService.updateBalances();
        }
      });
    }
  }

  unChargeAccount() {
    if (this.barService.getUser()) {
      const drawer = this.unChargeServiceDrawer.unchargeAccountDrawer(this.barService.getUser());
      drawer.afterClose.pipe(first()).subscribe(data => {
        if (data) {
          this.barService.updateBalances();
        }
      });
    }
  }

  movements() {
    this.movementsService.movementsDrawer('bar');
  }

  history() {
    this.orderHistoryDrawerService.historyDrawer();
  }

  boxListHistory() {
    this.boxDrawerService.boxDrawer();
  }

  accessWharehouses(){
    this.loadingStock = true;
    this.service_id = this.configService.get('service.service_bar_id');
    this.barService.getAccessUser().pipe(first(), finalize(()=>this.loadingStock=false)).subscribe((data)=> {
     if(data.data.length>0) {
       data.data.forEach((whare)=> {
         whare.services.forEach(service => {
           if(service.id == this.service_id) {
             this.stockView = true;
           }
         });
       })
     }
    })
  }
}
