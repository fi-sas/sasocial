import { NzMessageService } from 'ng-zorro-antd';
import { UsersService } from './../../../auth/services/users.service';
import { BalancesService } from './../../balances/services/balances.service';
import { BalanceModel } from './../../balances/models/balance.model';
import { ConfigsService } from './../../../services/configs.service';
import { RfidService } from "./../../../services/rfid.service";
import { first, finalize, map, distinctUntilChanged, observeOn, mergeMap, flatMap, combineAll } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { UserModel } from "../../../auth/models/user.model";
import { Subject, Observable, Subscription, forkJoin, merge, combineLatest } from "rxjs";
import { HttpHeaders, HttpParams } from "@angular/common/http";
import { FiResourceService, HttpOptions, Resource } from '../../../core/services/resource.service';
import { FiUrlService } from '../../../core/services/url.service';
import { OrdersModel } from '../models/orders.model';
import * as moment from 'moment';

@Injectable({
  providedIn: "root"
})
export class BarService {

  private service_id = null;
  private account_id = null;

  //SELECTED ORDER BAR
  private orderSelectedBar: OrdersModel = null;
  private _orderSelectedBar: Subject<OrdersModel> = new Subject();
  private _orderLoading: Subject<boolean> = new Subject();
  private orderLoading: boolean;
  private _orderLoadingTotal: Subject<boolean> = new Subject();
  private orderLoadingTotal: boolean;
  private listOrderNotServed: OrdersModel[] = [];
  private _listOrderNotServed: Subject<OrdersModel[]> = new Subject();
  private listOrderWait: OrdersModel[] = [];
  private _listOrderWait: Subject<OrdersModel[]> = new Subject();

  private selectedOrderNext: Subject<number> = new Subject();
  // SELECTED USER
  private selectedUser: UserModel = null;
  private _selectedUser: Subject<UserModel> = new Subject();

  //Balances
  private balances: BalanceModel[] = [];
  private _userBalances: Subject<BalanceModel[]> = new Subject();
  private _balancesLoading: Subject<boolean> = new Subject();
  private balancesLoading: boolean;

  // RFID subscription
  private rfidSubs: Subscription = null;

  constructor(
    private configService: ConfigsService,
    rfidService: RfidService,
    private message: NzMessageService,
    private usersService: UsersService,
    private balancesService: BalancesService,
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
  ) {
    this.account_id = this.configService.get('service.account_id');
    this.service_id = this.configService.get('service.service_bar_id');

    this.rfidSubs = rfidService.getRfidObservable().pipe(distinctUntilChanged()).subscribe(rfid => {
      this.getUserByRFID(rfid);
    });
  }

  destroy() {
    this.rfidSubs.unsubscribe();
  }

  getUserByRFID(rfid: string) {
    const messageId = this.message.loading('A carregar utilizador', { nzDuration: 0 }).messageId;

    this.usersService.list({
      params: new HttpParams().append('limit', '1').append('query[rfid]', rfid)
    }).pipe(first(), finalize(() => this.message.remove(messageId))).subscribe(value => {
      if (value.data.length > 0) {

        this.setUser(value.data[0]);
      } else {
        this.message.error('Utilizador não encontrado');
      }
    });
  }

  /**OPERATOR */
  public getUser(): UserModel {
    return this.selectedUser;
  }

  setUser(user: UserModel) {
    this.selectedUser = user;
    this.updateBalances();
    this._selectedUser.next(user);
  }

  selectedUserObservable(): Observable<UserModel> {
    return this._selectedUser.asObservable();
  }

  /**USER SELECTED BAR */
  setUserBarNull() {
    this.orderSelectedBar = null;
    this._orderSelectedBar.next(null);
  }

  setUserBar(order: OrdersModel) {
    this.orderSelectedBar = order;
    this.updateBalancesBar();
    this._orderSelectedBar.next(order);
  }

  public getUserBar(): OrdersModel {
    return this.orderSelectedBar;
  }

  selectedUserBarObservable(): Observable<OrdersModel> {
    return this._orderSelectedBar.asObservable();
  }

  /**LIST NOT SERVED */
  setListNotServed(list: OrdersModel[]) {
    this.listOrderNotServed = list;
    this._listOrderNotServed.next(list);
  }

  public getListNotServed(): OrdersModel[] {
    return this.listOrderNotServed;
  }

  selectedlistOrderNotServedObservable(): Observable<OrdersModel[]> {
    return this._listOrderNotServed.asObservable();
  }

  /***List wait */
  setListWait(list: OrdersModel[]) {
    this.listOrderWait = list;
    this._listOrderWait.next(list);
  }

  public getListWait(): OrdersModel[] {
    return this.listOrderWait;
  }

  selectedlistOrderWaitObservable(): Observable<OrdersModel[]> {
    return this._listOrderWait.asObservable();
  }

  /**Loading orders selected */
  orderLoadingObservable(): Observable<boolean> {
    return this._orderLoading.asObservable();
  }

  private setOrderLoading(loading: boolean) {
    this.orderLoading = loading;
    this._orderLoading.next(loading);
  }

  /**Loading total list orders */
  private setOrderLoadingTotal(loading: boolean) {
    this.orderLoadingTotal = loading;
    this._orderLoadingTotal.next(loading);
  }

  orderLoadingTotalObservable(): Observable<boolean> {
    return this._orderLoadingTotal.asObservable();
  }

  /**balances */
  balancesLoadingObservable(): Observable<boolean> {
    return this._balancesLoading.asObservable();
  }

  private setBalancesLoading(loading: boolean) {
    this.balancesLoading = loading;
    this._balancesLoading.next(loading);
  }

  balancesObservable(): Observable<BalanceModel[]> {
    return this._userBalances.asObservable();
  }

  private setBalances(balances: BalanceModel[]) {
    this.balances = balances;
    this._userBalances.next(balances);
  }

  updateBalances() {
    this.setBalances([]);
    this.setBalancesLoading(true);
    this.balancesService.balances(this.selectedUser.id)
      .pipe(first(), map(v => v.data.filter(v1 => v1.account_id === this.account_id)))
      .subscribe(value => {
        this.setBalances(value);
        this.setBalancesLoading(false);
      });
  }

  updateBalancesBar() {
    this.setBalances([]);
    this.setBalancesLoading(true);
    this.balancesService.balances(this.orderSelectedBar.user.id)
      .pipe(first(), map(v => v.data.filter(v1 => v1.account_id === this.account_id)))
      .subscribe(value => {
        this.setBalances(value);
        this.setBalancesLoading(false);
      });
  }

  getOrdersNoServed(): Observable<Resource<OrdersModel>> {
    this.service_id = this.configService.get('service.service_bar_id');
    const today = new Date();
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params: HttpParams = new HttpParams();
    params = params.set('withRelated', 'user');
    params = params.append('query[status]', 'NOT_SERVED');
    params = params.set('sort', 'id');
    //params = params.set('query[created_at]', moment(today).format('YYYY-MM-DD'));
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[service_id]', this.service_id);
    return this.resourceService.list<OrdersModel>(this.urlService.get('BAR.ORDERS'), {
      headers: headers,
      params: params
    });
  }

  getOrdersWait(): Observable<Resource<OrdersModel>> {
    this.service_id = this.configService.get('service.service_bar_id');
    const today = new Date();
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params: HttpParams = new HttpParams();
    params = params.set('withRelated', 'user');
    params = params.append('query[status]', 'WAITING');
    params = params.set('sort', 'id');
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[created_at]', moment(today).format('YYYY-MM-DD'));
    params = params.set('query[service_id]', this.service_id);
    return this.resourceService.list<OrdersModel>(this.urlService.get('BAR.ORDERS'), {
      headers: headers,
      params: params
    });
  }

  getOrderById(id: number): Observable<Resource<OrdersModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params: HttpParams = new HttpParams();
    params = params.set('withRelated', 'user,order_lines,user_served');
    return this.resourceService.list<OrdersModel>(this.urlService.get('BAR.ORDER_ID', { id }), {
      headers: headers,
      params: params
    });
  }


  getAccessUser(): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params = new HttpParams();
    params = params.set('withRelated', 'services');
    return this.resourceService.list<OrdersModel>(this.urlService.get('BAR.WHAREHOUSES_ACCESS'), {
      headers: headers,
      params: params
    });
  }

  getOrdersServed(startDate,endDate,date, page, pageSize): Observable<Resource<OrdersModel>> {
    this.service_id = this.configService.get('service.service_bar_id');
    let params = new HttpParams();
    params = params.append('query[status]', 'SERVED');
    params = params.append('query[status]', 'CANCELED');
    params = params.set('withRelated', 'user,order_lines,user_served');
    params = params.set('sort', '-updated_at');
    if(startDate && endDate) {
      params = params.set('query[updated_at][gte]', moment(startDate).format('YYYY-MM-DD'));
      params = params.set('query[updated_at][lte]', moment(endDate).format('YYYY-MM-DD'));
    }else {
      params = params.set('query[updated_at]', moment(date).format('YYYY-MM-DD'));
    }
    params = params.set('query[service_id]', this.service_id);
    params = params.set('offset', ((page - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    return this.resourceService.list<OrdersModel>(this.urlService.get('BAR.ORDERS'), {
      headers: headers,
      params: params
    });
  }


  getInfoByIdOrder(idOrder) {
    this.setOrderLoading(true);
    this.getOrderById(idOrder).pipe(first(), finalize(() => this.setOrderLoading(false))).subscribe((data) => {
      this.setUserBar(data.data[0]);
    })
  }

  cancelOrder(order_id: number) {
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.ORDER_CANCEL', { order_id: order_id }), {});
  }

  waitOrder(order_id: number) {
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.ORDER_WAIT', { order_id: order_id }), {});
  }

  serveOrder(order_id: number) {
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.ORDER_SERVE', { order_id: order_id }), {});
  }

  unServed(order_id: number): Observable<Resource<OrdersModel>>{
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.ORDER_NOT_SERVED', { order_id }), {});
  }

  refreshListOrders(userNull): Observable<any> {
    return new Observable(obs => {
      this.setOrderLoadingTotal(true);
      if(!userNull) {
        this.setUserBarNull();
      }
      combineLatest(this.getOrdersNoServed().pipe(first()),this.getOrdersWait().pipe(first())).subscribe(([dataOrders, dataWait])=> {
        this.setListNotServed(dataOrders.data);
        this.setListWait(dataWait.data);
        this.setOrderLoadingTotal(false);
        obs.next(true);
        obs.complete();
      }, () => {
        this.setOrderLoadingTotal(false);
      })

    })
  }

  selectNextOrder() {
    this._listOrderNotServed.subscribe((data)=> {
      if(data.length>0) {
        this.selectedOrderNext.next(data[0].id);
      }
    })
  }
  
  selectNextOrderObservable(): Observable<number> {
    return this.selectedOrderNext.asObservable();
  }

  refreshListNotServed() {
    return new Observable(obs => {
      this.getOrdersNoServed().pipe(first()).subscribe((dataOrders)=> {
        this.setListNotServed(dataOrders.data);
        obs.next(true);
        obs.complete();
      }, () => {
      })

    })
}


  deleteItemOrder(order_line_id: number) {
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.DELETE_ITEM', { order_line_id: order_line_id }), {});
  }

  serveItemOrder(order_line_id: number) {
    return this.resourceService.create<OrdersModel>(this.urlService.get('BAR.SERVED_ITEM', { order_line_id: order_line_id }), {});
  }

}
