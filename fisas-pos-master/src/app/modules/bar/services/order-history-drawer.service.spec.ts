import { TestBed } from '@angular/core/testing';

import { CoreModule } from '../../../core/core.module';
import { OrderHistoryDrawerService } from './order-history-drawer.service';

describe('OrderHistoryDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: OrderHistoryDrawerService = TestBed.get(OrderHistoryDrawerService);
    expect(service).toBeTruthy();
  });
});
