
import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { OrderHistoryComponent } from '../components/orders-history/orders-history.component';

@Injectable({
  providedIn: 'root'
})
export class OrderHistoryDrawerService {


  constructor(private drawerService: NzDrawerService) { 

  }

    historyDrawer() {
        const drawerRef = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
            nzContent: OrderHistoryComponent,
            nzWidth: window.innerWidth,
            nzContentParams: {
            }
        });

        return drawerRef.afterClose;
    }



}
