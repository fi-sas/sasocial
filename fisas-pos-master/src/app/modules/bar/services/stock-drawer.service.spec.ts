import { TestBed } from '@angular/core/testing';

import { CoreModule } from '../../../core/core.module';
import { StockDrawerService } from './stock-drawer.service';

describe('StockDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: StockDrawerService = TestBed.get(StockDrawerService);
    expect(service).toBeTruthy();
  });
});
