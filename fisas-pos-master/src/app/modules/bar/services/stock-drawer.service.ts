
import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { StockComponent } from '../components/stock/stock.component';

@Injectable({
  providedIn: 'root'
})
export class StockDrawerService {


  constructor(private drawerService: NzDrawerService) { 

  }

    stockDrawer() {
        const drawerRef = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
            nzContent: StockComponent,
            nzWidth: window.innerWidth,
            nzContentParams: {
            }
        });

        return drawerRef.afterClose;
    }



}
