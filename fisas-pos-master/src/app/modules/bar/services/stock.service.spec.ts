import { TestBed } from '@angular/core/testing';

import { CoreModule } from '../../../core/core.module';
import { StockService } from './stock.service';

describe('StockService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: StockService = TestBed.get(StockService);
    expect(service).toBeTruthy();
  });
});
