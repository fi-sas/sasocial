import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { FiUrlService } from '../../../core/services/url.service';
import { CategoriesModel } from '../models/categories.model';
import { OperationStockModel } from '../models/operation-stock.model';
import { ProductModel } from '../models/product.model';
import { stockModel } from '../models/stock.model';
import { ConfigsService } from './../../../services/configs.service';

@Injectable({
  providedIn: 'root'
})
export class StockService {


  constructor(private configService: ConfigsService,
    private urlService: FiUrlService,
    private resourceService: FiResourceService) {

  }

  getCategories(): Observable<Resource<CategoriesModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params: HttpParams = new HttpParams();
    params = params.set('withRelated', 'translations');
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[active]', 'true');

    return this.resourceService.list<any>(
      this.urlService.get('BAR.FAMILIES'), {
      headers: headers,
      params: params
    }
    );
  }

  getProducts(id, service_id, filter): Observable<Resource<ProductModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params = new HttpParams();
    params = params.set('limit','-1');
    if (filter) {
      params = params.set('searchFields', 'name,code,description');
      params = params.set('search', filter.toString());
    
    }
    return this.resourceService.list<any>(
      this.urlService.get('BAR.PRODUCTS', { id, service_id }), {
      headers: headers,
      params: params
    }
    );
  }

  getLotes(idWhareHouse, idProd): Observable<Resource<stockModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params: HttpParams = new HttpParams();
    params = params.set('query[wharehouse_id]', idWhareHouse);
    params = params.set('query[product_id]', idProd);
    return this.resourceService.list<stockModel>(this.urlService.get('BAR.STOCKS'), {
      headers: headers,
      params: params
    });
  }

  saveStock(data): Observable<Resource<stockModel>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    return this.resourceService.create<stockModel>(this.urlService.get('BAR.STOCKS'), 
      data,
      {headers: headers}
    );
  }

  getWharehouse(id): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params = new HttpParams();
      params = params.set('withRelated', 'wharehouse');
    return this.resourceService.list<any>(this.urlService.get('BAR.SERVICES', { id }), {
      headers: headers,
      params: params
    });
  }

  changeQTD(id_stock, data: any): Observable<Resource<any>> {
    return this.resourceService.update<any>(this.urlService.get('BAR.QTDCHANGE', { id_stock }),
      data
    );
  }

  historyStock(id_stock, page, pageSize):
    Observable<Resource<OperationStockModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params = new HttpParams();
    params = params.set('offset', ((page - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort', '-created_at')
    return this.resourceService.list<OperationStockModel>(this.urlService.get('BAR.HISTORY_STOCK', { id_stock }),
      {
        headers: headers,
        params: params
      });
  }

  movimentsHistory(page, pageSize, product_id, wharehouse_id):
    Observable<Resource<OperationStockModel>> {
    const headers: HttpHeaders = new HttpHeaders()
      .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    let params = new HttpParams();
    params = params.set('offset', ((page - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort', '-created_at');
    params = params.set('query[product_id]', product_id.toString());
    params = params.set('query[wharehouse_id]', wharehouse_id.toString());
    params = params.set('withRelated', 'stock,user,device,transferWharehouse');
    return this.resourceService.list<OperationStockModel>(this.urlService.get('BAR.STOCK_OPERATION'),
      {
        headers: headers,
        params: params
      });

  }

  getLotesByProductWharehouse(id_prod, id_whare) {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configService.get('service.entity_id'));
    return this.resourceService.list<stockModel>(this.urlService.get('BAR.LOTES', { id_prod: id_prod, id_whare: id_whare }), {  headers: headers, });
  }

}
