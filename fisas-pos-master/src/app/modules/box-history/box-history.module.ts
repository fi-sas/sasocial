import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { BoxListHistoryComponent } from './box-history/box-list-history.component';
import { BoxDrawerService } from './service/box-drawer.service';
import { MovementsModule } from '../movements/movements.module';
import { ReportsModule } from '../reports/reports.module';

@NgModule({
  declarations: [BoxListHistoryComponent],
  imports: [
    CommonModule,
    MovementsModule,
    SharedModule,
    ReportsModule
  ],
  providers: [
    BoxDrawerService
  ],
  entryComponents: [
    BoxListHistoryComponent
  ]
})
export class BoxHistoryModule { }
