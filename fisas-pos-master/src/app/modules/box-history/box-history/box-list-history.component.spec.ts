import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '../../../../core/core.module';
import { SharedModule } from '../../../../shared/shared.module';
import { BoxListHistoryComponent } from './box-list-history.component';


describe('BoxListHistoryComponent', () => {
  let component: BoxListHistoryComponent;
  let fixture: ComponentFixture<BoxListHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BoxListHistoryComponent
      ],
      imports: [
          SharedModule,
          CoreModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxListHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
