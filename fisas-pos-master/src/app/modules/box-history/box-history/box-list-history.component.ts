import { Component, OnInit } from "@angular/core";
import * as moment from 'moment';
import { finalize, first } from "rxjs/operators";
import { ConfigsService } from "../../../services/configs.service";
import { CashAccountModel } from "../../balances/models/cash-account.model";
import { BalancesService } from "../../balances/services/balances.service";
import { ReportsService } from "../../reports/services/reports.service";
import { MovementModel } from "../../movements/models/movement.model";
import { MovementsService } from "../../movements/services/movements.service";
import { NzMessageService } from "ng-zorro-antd";

@Component({
    selector: 'fisas-box-list-history',
    templateUrl: './box-list-history.component.html',
    styleUrls: ['./box-list-history.component.less']
})
export class BoxListHistoryComponent implements OnInit {
    pageIndex = 1;
    pageSize = 10;
    totalData = 0;
    loadingReport = false;
    selectOptions = [
        {
            value: moment().toISOString(),
            label: 'Hoje'
        },
        {
            value: moment().subtract(1, 'day').toISOString(),
            label: 'Ontem'
        },
        {
            value: 'CUSTOM',
            label: 'Personalizado'
        }
    ];

    selectedValue;

    dateFormat = 'yyyy/MM/dd';
    startDate = null;
    endDate = null;
    loading = true;
    loadingBox = false;
    cashAccounts: CashAccountModel[] = [];
    box;
    boxSelected;
    movements: MovementModel[] = [];
    totalMov: number;

    disableStartDate = (current: Date): boolean => {
        if(this.endDate !== null) {
            return !moment(this.endDate).isBetween(moment(current), moment());
          }
        return current > new Date();
      };
    
    disableEndDate = (current: Date): boolean => {
        if (this.startDate !== null) {
            return !moment(current).isBetween(moment(this.startDate), moment());
        }
        return current > new Date();
    
    };


    constructor(private balancesService: BalancesService, private message: NzMessageService, private movementsService: MovementsService,
        private configsService: ConfigsService, private reportsService: ReportsService) {
    }

    ngOnInit() {
        this.boxSelected = localStorage.getItem('boxSelected');

        this.selectedValue = this.selectOptions[0].value;
        this.balancesService.getUserAvailableCashAccounts().pipe(first(),
            finalize(() => this.loadingBox = false)
        ).subscribe(response => {
            this.cashAccounts = response.data;
            if (this.cashAccounts.length > 0) {
                this.box = Number(this.boxSelected);
                this.getMovements(null, null, this.selectedValue);
            }
        })

    }

    onChangeSelectInput() {
        if (this.selectedValue !== 'CUSTOM') {
            this.startDate = null;
            this.endDate = null;
            this.getMovements(null, null, this.selectedValue, true)
        }else{
            this.movements = [];
        }
    }

    onChangeStartDate(startDate) {
        this.startDate = startDate.toISOString();
        if (this.startDate !== null && this.endDate !== null) {
            this.getMovements(this.startDate, this.endDate, null, true)
        }
    }

    onChangeEndDate(endDate) {
        this.endDate = endDate.toISOString();
        if (this.startDate !== null && this.endDate !== null) {
            this.getMovements(this.startDate, this.endDate, null, true)
        }
    }

    getMovements(startDate: string, endDate: string, date: string, reset: boolean = false) {
        if (reset) {
            this.pageIndex = 1;
        }
        this.loading = true;
        this.movementsService.movementsByBox(this.configsService.get('service.account_id'), this.box, startDate, endDate, date, this.pageIndex,
            this.pageSize).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
                this.movements = data.data;
                this.totalData = data.link.total;
                this.totalMov = data.data.length>0 ? data.data[0].totalSummary : 0;
            })
    }

    report(startDate: string, endDate: string, date: string) {
        this.loadingReport = true;
        this.movementsService.movementsByBoxReport(this.configsService.get('service.account_id'), this.box, startDate, endDate, date, this.pageIndex,
            this.pageSize).pipe(first(), finalize(() => this.loadingReport = false)).subscribe((data) => {
                this.reportsService.loadData(false);
                this.message.success("Relatório gerado com sucesso");
            })

    }
}
