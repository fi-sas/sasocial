import { TestBed } from '@angular/core/testing';

import { CoreModule } from '../../../core/core.module';
import { BoxDrawerService } from './box-drawer.service';

describe('BoxDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: BoxDrawerService = TestBed.get(BoxDrawerService);
    expect(service).toBeTruthy();
  });
});
