
import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { BoxListHistoryComponent } from '../box-history/box-list-history.component';

@Injectable({
  providedIn: 'root'
})
export class BoxDrawerService {


  constructor(private drawerService: NzDrawerService) { 

  }

    boxDrawer() {
        const drawerRef = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
            nzContent: BoxListHistoryComponent,
            nzWidth: window.innerWidth,
            nzContentParams: {
            }
        });

        return drawerRef.afterClose;
    }



}
