import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServeComponent } from './pages/serve/serve.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'serve',
    pathMatch: 'full'
  },
  {
    path: 'serve',
    component: ServeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanteenRoutingModule { }
