import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CanteenRoutingModule } from './canteen-routing.module';
import { ServeComponent } from './pages/serve/serve.component';
import { MealsHistoryComponent } from './components/meals-history/meals-history.component';
import { MealsHistoryCardComponent } from './components/meals-history-card/meals-history-card.component';
import { UserMealsComponent } from './components/user-meals/user-meals.component';
import { StatusMealsTypesComponent } from './components/status-meals-types/status-meals-types.component';
import { ServeUserInfoComponent } from './components/serve-user-info/serve-user-info.component';
import { MealRowComponent } from './components/meal-row/meal-row.component';
import { ChargeAccountModule } from '../charge-account/charge-account.module';
import { SearchUserModule } from '../search-user/search-user.module';
import { NgBooleanPipesModule, NgWherePipeModule } from 'angular-pipes';
import { MovementsModule } from '../movements/movements.module';
import { ReservationsHistoryModule } from '../reservations-history/reservations-history.module';
import { UnchargeAccountModule } from '../uncharge-account/uncharge-account.module';
import { CanteenService } from './services/canteen.service';
import { ReservationStatsModule } from '../reservations-stats/reservation-stats.module';
import { BoxHistoryModule } from '../box-history/box-history.module';
import { ReportsModule } from '../reports/reports.module';


@NgModule({
  declarations: [
    ServeComponent,
    MealsHistoryComponent,
    MealsHistoryCardComponent,
    UserMealsComponent,
    StatusMealsTypesComponent,
    ServeUserInfoComponent,
    MealRowComponent,
  ],
  imports: [
    CommonModule,
    NgBooleanPipesModule,
    NgWherePipeModule,
    CanteenRoutingModule,
    ChargeAccountModule,
    SearchUserModule,
    MovementsModule,
    ReservationsHistoryModule,
    UnchargeAccountModule,
    ReservationStatsModule,
    BoxHistoryModule,
    SharedModule,
    ReportsModule
  ],
  providers: [
    CanteenService
  ]
})
export class CanteenModule { }
