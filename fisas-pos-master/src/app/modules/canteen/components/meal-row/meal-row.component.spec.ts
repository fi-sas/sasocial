import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealRowComponent } from './meal-row.component';
import { SharedModule } from '../../../../shared/shared.module';

describe('MealRowComponent', () => {
  let component: MealRowComponent;
  let fixture: ComponentFixture<MealRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          MealRowComponent],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
