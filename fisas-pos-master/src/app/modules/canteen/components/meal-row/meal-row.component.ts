import { CartCheckoutModel } from './../../../../cart/models/cart-checkout.model';
import { ReservationsService } from './../../services/reservations.service';
import { NzMessageService } from 'ng-zorro-antd';
import { CartService } from './../../../../cart/services/cart.service';
import { CanteenService } from './../../services/canteen.service';
import { first, finalize, subscribeOn } from 'rxjs/operators';
import { Component, OnInit, Input } from '@angular/core';
import { PaymentMethodsService } from '../../../../cart/services/payment-methods.service';
import { PaymentMethodModel } from '../../../../cart/models/payment-method.model';

export enum MealRowType {
  RESERVATIONS = 'reservations',
  USED_RESERVATIONS = 'used_reservations',
  MENU = 'menu'
}

@Component({
  selector: 'fisas-meal-row',
  templateUrl: './meal-row.component.html',
  styleUrls: ['./meal-row.component.less']
})
export class MealRowComponent implements OnInit {
  MealRowType = MealRowType;
  
  serveLoading = false;
  unserveLoading = false;
  isloadingPurchase = false;
  cancelLoading = false;

  buyModalVisible = false;
  cancelModalVisible = false;

  paymentMethod: number = null;
  paymentMethodsLoading = false;
  date = new Date();
  paymentMethods: PaymentMethodModel[] = [];

  @Input() type: MealRowType = null;
  @Input() pack: boolean = false;
  // ReservationModel|MenuModel
  @Input() data: any = null;

  constructor(
    private canteenService: CanteenService,
    private message: NzMessageService,
    private paymentMethodsService: PaymentMethodsService,
    private reservationsService: ReservationsService,
    private cartService: CartService
  ) { }

  ngOnInit() {
  }

  getDishTypeName() {
    if (this.data !== null) {
      if (this.type === MealRowType.MENU) {
        return this.data.dish_type_translation.find((tran)=>tran.language_id == 3).name;
      } else {
        return this.data.menu_dish.type.translations.find((tran)=>tran.language_id == 3).name;
      }
    }
  }

  getDishName() {
    if (this.data !== null) {
      if (this.type === MealRowType.MENU) {
        return this.data.translations.find((tran)=>tran.language_id == 3).name;
      } else {
        return this.data.menu_dish.dish.translations.find((tran)=>tran.language_id == 3).name;
      }
    }
  }

  getDishPrice() {
    if (this.data !== null) {
      if (this.type === MealRowType.MENU) {
        return this.data.price + '€';
      } else {
        return '';
      }
    }

  }

  isAvailable() {
    if (this.type === MealRowType.MENU) {
      return this.data !== null ? this.data.available : false;
    } else {
      return true;
    }
  }

  isAvailableStock() {
    if (this.type === MealRowType.MENU) {
      return this.data !== null ? this.data.isStockAvailable : false;
    } else {
      return true;
    }
  }

  isCancelled() {
    if (this.data !== null) {
      if (this.data.has_canceled) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  unserveMeal() {
    this.unserveLoading = true;
    this.canteenService.unserveMeal(this.data.id)
      .pipe(
        first(),
        finalize(() => (this.unserveLoading = false))
      ).subscribe();
  }

  serveMeal() {
    this.serveLoading = true;
    this.canteenService
      .serveMeal(this.data.id)
      .pipe(
        first(),
        finalize(() => (this.serveLoading = false))
      ).subscribe();
  }

  cancelMeal(payment_method_id: number) {
    this.cancelLoading = true;
    this.canteenService.cancelMeal(this.data.id, payment_method_id)
      .pipe(
        first(),
        finalize(() => (this.cancelLoading = false))
      ).subscribe();
  }

  getPaymentMethods() {
    this.paymentMethodsService.list(true, true).pipe(
      first(),
      finalize(() => this.paymentMethodsLoading = false)
    ).subscribe(value => {
      this.paymentMethods = value.data;
      const cc = this.paymentMethods.find(pm => pm.tag === 'CC');
      if (cc) {
        this.paymentMethod = cc.id;
      }
    });
  }

  openCancelMealModal() {
    this.getPaymentMethods();
    this.cancelModalVisible = true;
  }

  buyMeal() {
    this.isloadingPurchase = true;
    let userId = this.canteenService.getUser().id;

    this.reservationsService.refectoryCartAdd(this.data.id, userId).pipe(first()).subscribe(() => {

      let cartCheckout = new CartCheckoutModel();

      cartCheckout.account_id = this.data.account_id;
      cartCheckout.user_id = userId;
      this.cartService.cartCheckout(cartCheckout).pipe(first()).subscribe(() => {
        this.isloadingPurchase = false;
        this.buyModalVisible = false;
        this.canteenService.updateBalances();
        this.canteenService.updateReservations();
        this.message.success('Compra efetuada com sucesso!');
      }, (errCartCheckout) => {
        this.isloadingPurchase = false;
        this.buyModalVisible = false;
        let error;
        error = errCartCheckout.error.errors[0].translations ? errCartCheckout.error.errors[0].translations.find(tran => tran.language_id == 3).message : 'Erro ao efetuar a operação';
        this.message.error(error);
      })

    }, (errCartAdd) => {
      this.isloadingPurchase = false;
      this.buyModalVisible = false;
      this.message.error(errCartAdd.message);
    })
  }

  validDate(dateCancel): boolean {
    if(new Date(dateCancel) > this.date) {
      return true;
    }
    return false;
  }

}
