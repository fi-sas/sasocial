import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsHistoryCardComponent } from './meals-history-card.component';
import { SharedModule } from '../../../../shared/shared.module';

describe('MealsHistoryCardComponent', () => {
  let component: MealsHistoryCardComponent;
  let fixture: ComponentFixture<MealsHistoryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          MealsHistoryCardComponent
      ],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsHistoryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
