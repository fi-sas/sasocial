import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserModel } from '../../../../auth/models/user.model';
import { CanteenService } from '../../services/canteen.service';

@Component({
  selector: 'fisas-meals-history-card',
  templateUrl: './meals-history-card.component.html',
  styleUrls: ['./meals-history-card.component.less']
})
export class MealsHistoryCardComponent implements OnInit {

  //@Input() reservation: ReservationModel = null;
  @Input() user: UserModel = null;
  @Output() userSelec = new EventEmitter();
  userSelectedId: number = 0;

  constructor(private canteenService: CanteenService) { 
    this.canteenService.selectedUserRFIDObservable().subscribe((data)=>{
      this.userSelectedId = data;
    })
  }

  ngOnInit() {
  }

  
  getReservationsUser(){
    this.userSelec.emit(this.user.id);
    this.canteenService.setUser(this.user);
  }
}
