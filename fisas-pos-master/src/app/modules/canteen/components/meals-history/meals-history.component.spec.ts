import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsHistoryComponent } from './meals-history.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MealsHistoryCardComponent } from '../meals-history-card/meals-history-card.component';

describe('MealsHistoryComponent', () => {
  let component: MealsHistoryComponent;
  let fixture: ComponentFixture<MealsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          MealsHistoryComponent,
          MealsHistoryCardComponent
      ],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
