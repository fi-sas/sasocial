import { ReservationModel } from './../../models/reservation.model';
import { ReservationsService } from './../../services/reservations.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ReservationDrawerService } from '../../../reservations-history/services/reservation-drawer.service';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { CanteenService } from '../../services/canteen.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { ConfigsService } from '../../../../services/configs.service';
import { ReservationStatsDrawerService } from '../../../reservations-stats/services/reservation-stats-drawer.service';
import { UserModel } from '../../../../auth/models/user.model';

@Component({
  selector: 'fisas-meals-history',
  templateUrl: './meals-history.component.html',
  styleUrls: ['./meals-history.component.less']
})
export class MealsHistoryComponent implements OnInit, OnDestroy {
  service_id = null;
  reservations: ReservationModel[] = [];
  isLoading = false;
  reservationSubscription: Subscription = null;
  userListRFID: UserModel[] = [];

  constructor(private reservationsDrawer: ReservationDrawerService,
    private ReservationsStatsDrawer: ReservationStatsDrawerService,
    private configsService: ConfigsService,
    private reservationsService: ReservationsService,

    protected canteenService: CanteenService) { }

  ngOnInit() {
    this.updateListUserRFID();
    //this.autoUpdate();
  }

  ngOnDestroy(): void {
    //this.reservationSubscription.unsubscribe();
  }

  /**
 * Show the reservation History
 */
  showReservationsHistory() {
    this.reservationsDrawer.reservationsDrawer();
  }

  /**
   * Show the reservation Stats
   */
  showReservationsStats() {
    this.ReservationsStatsDrawer.reservationsStatsDrawer();
  }

  userSelec(event) {
    if(event) {
      this.canteenService.setUserSelectRFID(event);
    }
  }


  /**
   * @description
  */
  /*updateReservations() {
    this.service_id = this.configsService.get('service.service_canteen_id');
    this.isLoading = true;
    this.reservationsService.userReservationDate({
      params: new HttpParams()
        .append('limit', '10')
        .append('sort', '-served_at')
        .append('query[date]', moment().toISOString())
        .append('query[service_id]', this.service_id)
        .append('query[is_served]', 'true')
        .append('withRelated', 'dishs,types,menu_dish,user')
    }).pipe(first()).subscribe(reservations => {«
      this.reservations = reservations.data;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }*/

  updateListUserRFID(){
    this.userListRFID = this.canteenService.getUserListRFID();
  }

  /**
   * @description Auto update the served reservations
   */
  /*autoUpdate() {
    this.reservationSubscription = this.canteenService.reservationsObservable().subscribe(() => {
      this.updateReservations();
    })
  }*/


}
