import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeUserInfoComponent } from './serve-user-info.component';
import { SharedModule } from '../../../../shared/shared.module';

describe('ServeUserInfoComponent', () => {
  let component: ServeUserInfoComponent;
  let fixture: ComponentFixture<ServeUserInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          ServeUserInfoComponent
      ],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServeUserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
