import { BalanceModel } from './../../../balances/models/balance.model';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ChargeDrawerService } from '../../../charge-account/services/charge-drawer.service';
import { SearchDrawerService } from '../../../search-user/services/search-drawer.service';
import { first } from 'rxjs/operators';
import { UserModel } from '../../../../auth/models/user.model';
import { Subscription } from 'rxjs';
import { CanteenService } from '../../services/canteen.service';
import { MovementsDrawerService } from '../../../movements/services/movements-drawer.service';
import { UnchargeDrawerService } from '../../../uncharge-account/services/uncharge-drawer.service';

@Component({
  selector: 'fisas-serve-user-info',
  templateUrl: './serve-user-info.component.html',
  styleUrls: ['./serve-user-info.component.less']
})
export class ServeUserInfoComponent implements OnInit, OnDestroy {

  changeInterval = null;

  activeUser: UserModel = null;
  _selectedUser: Subscription = null;

  _selectedUserBalances: Subscription = null;
  _selectedUserBalancesLoading: Subscription = null;
  balance: BalanceModel = null;
  balancesLoading= false;
  boxSelected;
  currentPositive = true;
  currentZero = false;

  constructor(
    private ref: ChangeDetectorRef,
    private canteenService: CanteenService,
    private chargeDrawerService: ChargeDrawerService,
    private movementsService: MovementsDrawerService,
    private unChargeServiceDrawer: UnchargeDrawerService,
    private searchDrawerService: SearchDrawerService,
    
  ) { }

  ngOnInit() {
    this.change();
    this.boxSelected = localStorage.getItem('boxSelected');
    this._selectedUser = this.canteenService.selectedUserObservable().subscribe( user => {
      this.activeUser = user;
      const listUserRFID = this.canteenService.getUserListRFID(); 
      if(listUserRFID.length>0 && this.activeUser){
        if(listUserRFID.find((user)=>user.id == this.activeUser.id)) {
          this.canteenService.setUserSelectRFID(this.activeUser.id);
        }else {
          this.canteenService.setUserSelectRFID(null);
        }
      }
    });

    this._selectedUserBalances = this.canteenService
      .balancesObservable()
      .subscribe(balances => {
        this.balance = balances.length > 0 ? this.balance = balances[0] : null;

        if(this.balance) {
          this.currentPositive = this.balance.current_balance >= 0;
          this.currentZero = this.balance.current_balance === 0;
        }
      });

    this._selectedUserBalancesLoading = this.canteenService
      .balancesLoadingObservable()
      .subscribe(balancesLoading => {
        this.balancesLoading = balancesLoading;
      });
  }

  ngOnDestroy() {
    clearInterval(this.changeInterval);
    this._selectedUser.unsubscribe();
    this._selectedUserBalances.unsubscribe();
    this._selectedUserBalancesLoading.unsubscribe();
  }

  chargeAccount() {
    if(this.canteenService.getUser()) {
      const drawer = this.chargeDrawerService.chargeAccountDrawer(this.canteenService.getUser());
      drawer.afterClose.pipe(first()).subscribe(data => {
        if(data) {
          this.canteenService.updateBalances();
        }
      });
    }
  }

  /**
   * @description open the drawer to uncharge the account of user
   */
  unChargeAccount() {
      if(this.canteenService.getUser()) {
          const drawer = this.unChargeServiceDrawer.unchargeAccountDrawer(this.canteenService.getUser());
          drawer.afterClose.pipe(first()).subscribe(data => {
              if(data) {
                  this.canteenService.updateBalances();
              }
          });
      }
  }

  /**
   * Show de movements
   */
  movements() {
      this.movementsService.movementsDrawer('canteen');
  }

  searchUser() {
    this.searchDrawerService.searchUserDrawer().pipe(first()).subscribe( user => {
      if(user) {
        this.canteenService.setUser(user);
      }
    });
  }

  change() {
    this.changeInterval = setInterval(() => {
      this.ref.detectChanges();
    }, 500);
  }
}
