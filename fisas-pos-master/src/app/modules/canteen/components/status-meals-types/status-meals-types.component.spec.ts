import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusMealsTypesComponent } from './status-meals-types.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MealRowComponent } from '../meal-row/meal-row.component';
import { CoreModule } from '../../../../core/core.module';
import { ElectronService } from '../../../../services/electron.service';

describe('StatusMealsTypesComponent', () => {
  let component: StatusMealsTypesComponent;
  let fixture: ComponentFixture<StatusMealsTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          StatusMealsTypesComponent,
          MealRowComponent
      ],
      imports: [
          SharedModule,
          CoreModule
      ],
      providers: [
          ElectronService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusMealsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
