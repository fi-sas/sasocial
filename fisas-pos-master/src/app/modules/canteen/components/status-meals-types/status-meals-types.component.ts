import { MenuModel, Meal } from './../../models/menu.model';
import { UserModel } from './../../../../auth/models/user.model';
import { CanteenService } from './../../services/canteen.service';
import { ReservationModel } from './../../models/reservation.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fisas-status-meals-types',
  templateUrl: './status-meals-types.component.html',
  styleUrls: ['./status-meals-types.component.less']
})
export class StatusMealsTypesComponent implements OnInit, OnDestroy {
  Meal = Meal;
  today = new Date();

  activeMeal: Meal = Meal.LUNCH;

  reservationsLoading = false;
  reservations: ReservationModel[] = [];
  _reservations: Subscription = null;
  _reservationsLoading: Subscription = null;

  menuLoading = false;
  menu: MenuModel[] = [];
  _menu: Subscription = null;
  _menuLoading: Subscription = null;

  selectedUser: UserModel = null;
  _selectedUser: Subscription = null;
  intervalSubs: Subscription = null;

  constructor(
    private canteenService: CanteenService,
  ) { }

  ngOnInit() {

    this._selectedUser = this.canteenService.selectedUserObservable().subscribe(user => {
      this.selectedUser = user;
      this.activeMeal = this.canteenService.activeMeal();
    });
    this._reservations = this.canteenService.reservationsObservable().subscribe(reservations => {
      this.reservations = reservations;
    });
    this._reservationsLoading = this.canteenService.reservationsLoadingObservable().subscribe(reservationsLoading => {
      this.reservationsLoading = reservationsLoading;
    });
    this._menuLoading = this.canteenService.menuLoadingObservable().subscribe(menuLoading => {
      this.menuLoading = menuLoading;
    });
    this._menu = this.canteenService.menuObservable().subscribe(menu => {
      this.menu = menu;
    });

  }

  ngOnDestroy() {
    this._selectedUser.unsubscribe();
    this._reservations.unsubscribe();
    this._reservationsLoading.unsubscribe();

    // this.intervalSubs.unsubscribe();
  }

  closeUser() {
    const listUserRFID = this.canteenService.getUserListRFID();
    //removes the terminated user from the list, if it exists in the list
    if (listUserRFID.length > 0 && this.selectedUser) {
      for (let i = 0; i < listUserRFID.length; i++) {
        if (listUserRFID[i].id == this.selectedUser.id) {
          listUserRFID.splice(i, 1);
          this.canteenService.setUserListRFID(listUserRFID);
        }
      }
    }
    //If there is still data in the list, go to the next user, if not clear
    if(listUserRFID.length > 0 && this.selectedUser) {
      this.selectedUser = listUserRFID[0];
      this.canteenService.setUser(this.selectedUser);
      this.canteenService.updateBalances();
    }else{
      this.selectedUser = null;
      this.canteenService.setUser(null);
      this.canteenService.updateBalances();
    }
  }

}
