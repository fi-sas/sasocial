
export interface DishTranslationModel {
    language_id: number,
    name: string,
    description: string,
};
export interface DishModel {
    id: number,
    active: boolean,
    created_at: Date,
    updated_at: Date,
    file_id: number,
    translations: DishTranslationModel[],
    recipes_ids: number[],
    types_ids: number[]
};
