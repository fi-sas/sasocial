import { DishModel } from './dish.model';
import { MenuMealModel } from './menu-meal.model';

export interface MenuDishModel {
    id: number,
    active: boolean,
    created_at: Date,
    updated_at: Date,
    menuId: number,
    dish_id: number,
    type_id: number,
    doses_available: number,
    available: boolean,
    dish: DishModel,
    type: any;
    menu?: MenuMealModel
}
