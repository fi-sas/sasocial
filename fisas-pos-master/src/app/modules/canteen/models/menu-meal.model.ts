export interface MenuMealModel {
    id: number,
    active: boolean,
    created_at: Date,
    updated_at: Date,
    service_id: number;
    date: Date,
    meal: string,
    validate: boolean,
}
