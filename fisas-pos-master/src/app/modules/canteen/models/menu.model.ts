
export enum Meal {
  LUNCH = 'lunch',
  DINNER = 'dinner'
}

export class TranslationModel {
  language_id: number;
  name: string;
  description?: string;
}

export class MenuModel {
  id: number;
  code: string;
  price: number;
  tax_id: number;
  prices: number[];
  available: boolean;
  show_derivatives: boolean;
  translations: TranslationModel[];
  user_allergic: boolean;
  nutrients: any[];
  complements: any[];
  isStockAvailable: boolean;
  stockQuantity: number;
  file: any;
  file_id: number;
  service_id: number;
  account_id: number;
  dish_type_translation: TranslationModel[];
  dish_type_id: number;
  allergens: any[];
  tax?: any;
  location: string;
}
