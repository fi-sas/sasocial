export interface ReservationCountModel {
    id: number;
    type: string;
    dish: string;
    served: number;
    nonServed: number;
    cancelled: number;
    total: number;
    available: number;
    doses_available: number;
}
