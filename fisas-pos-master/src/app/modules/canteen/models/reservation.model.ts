import { MenuDishModel } from './menu-dish.model';
import { UserModel } from '../../../auth/models/user.model';

export interface ReservationModel {
    id: number,
    active: boolean,
    created_at: Date,
    updated_at: Date,
    date: Date,
    user_id: number,
    order_id: number,
    location: string,
    is_from_pack: boolean,
    is_available: boolean,
    is_served: boolean,
    served_at: null,
    menu_dish_id: number,
    fentity_id: null,
    menu_dish: MenuDishModel,
    user?: UserModel,
    has_canceled: boolean;
}
