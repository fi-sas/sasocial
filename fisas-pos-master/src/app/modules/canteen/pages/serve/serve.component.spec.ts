import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeComponent } from './serve.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MealsHistoryComponent } from '../../components/meals-history/meals-history.component';
import { ServeUserInfoComponent } from '../../components/serve-user-info/serve-user-info.component';
import { StatusMealsTypesComponent } from '../../components/status-meals-types/status-meals-types.component';
import { MealsHistoryCardComponent } from '../../components/meals-history-card/meals-history-card.component';
import { MealRowComponent } from '../../components/meal-row/meal-row.component';
import { CoreModule } from '../../../../core/core.module';
import { ElectronService } from '../../../../services/electron.service';

describe('ServeComponent', () => {
  let component: ServeComponent;
  let fixture: ComponentFixture<ServeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          ServeComponent,
          MealsHistoryComponent,
          MealsHistoryCardComponent,
          ServeUserInfoComponent,
          StatusMealsTypesComponent,
          MealRowComponent
      ],
      imports: [
          SharedModule,
          CoreModule
      ],
      providers: [
          ElectronService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
