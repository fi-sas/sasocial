import { Component, OnInit, OnDestroy } from '@angular/core';
import { BoxDrawerService } from '../../../box-history/service/box-drawer.service';
import { CanteenService } from '../../services/canteen.service';

@Component({
  selector: 'fisas-serve',
  templateUrl: './serve.component.html',
  styleUrls: ['./serve.component.less']
})
export class ServeComponent implements OnInit, OnDestroy {
  boxSelected;
  constructor(
    private canteenService: CanteenService,
    private boxDrawerService: BoxDrawerService
  ) { }

  ngOnInit() {
    this.canteenService.init();
    this.boxSelected = localStorage.getItem('boxSelected');
  }

  ngOnDestroy() {
    this.canteenService.destroy();
  }

  boxListHistory() {
    this.boxDrawerService.boxDrawer();
  }
}
