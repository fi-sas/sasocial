import { TestBed } from '@angular/core/testing';

import { CanteenService } from './canteen.service';
import { CoreModule } from '../../../core/core.module';

describe('CanteenService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: CanteenService = TestBed.get(CanteenService);
    expect(service).toBeTruthy();
  });
});
