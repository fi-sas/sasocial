import { NzMessageService } from 'ng-zorro-antd';
import { UsersService } from './../../../auth/services/users.service';
import { BalancesService } from './../../balances/services/balances.service';
import { BalanceModel } from './../../balances/models/balance.model';
import { ConfigsService } from './../../../services/configs.service';
import { MenuModel } from "./../models/menu.model";
import { RfidService } from "./../../../services/rfid.service";
import { MenuService } from "./menu.service";
import { first, finalize, map, distinctUntilChanged } from "rxjs/operators";
import { ReservationsService } from "./reservations.service";
import { ReservationModel } from "./../models/reservation.model";
import { Injectable } from "@angular/core";
import { UserModel } from "../../../auth/models/user.model";
import { Subject, Observable, Subscription } from "rxjs";
import { Resource } from "../../../core/services/resource.service";
import { Meal } from "../models/menu.model";
import { HttpParams } from "@angular/common/http";
import * as moment from 'moment';
import { QrcodeService } from '../../../services/qrcode.service';


@Injectable({
  providedIn: "root"
})
export class CanteenService {

  private service_id = null;
  private account_id = null;

  private mealActive: Meal = null;

  // SELECTED USER
  private selectedUser: UserModel = null;
  private _selectedUser: Subject<UserModel> = new Subject();

  // RESERVATIONS OF USER
  private reservations: ReservationModel[] = [];
  private _userReservations: Subject<ReservationModel[]> = new Subject();
  private _reservationsLoading: Subject<boolean> = new Subject();
  private reservationsLoading: boolean;

  // Menu
  private menu: MenuModel[] = [];
  private _userMenu: Subject<MenuModel[]> = new Subject();
  private _menuLoading: Subject<boolean> = new Subject();
  private menuLoading: boolean;

  //Balances
  private balances: BalanceModel[] = [];
  private _userBalances: Subject<BalanceModel[]> = new Subject();
  private _balancesLoading: Subject<boolean> = new Subject();
  private balancesLoading: boolean;

  // RFID subscription
  private rfidSubs: Subscription = null;
  private listUserRFID: UserModel[] = [];
  private _selectedUserRFID: Subject<number> = new Subject();
  private selectUserRFID: number;

  private qrcodeSubs: Subscription = null;

  constructor(
    private configService: ConfigsService,
    private rfidService: RfidService,
    private qrcodeService: QrcodeService,
    private message: NzMessageService,
    private usersService: UsersService,
    private balancesService: BalancesService,
    private menuService: MenuService,
    private reservationsService: ReservationsService
  ) {
    this.account_id = configService.get('service.account_id');
    this.service_id = configService.get('service.service_canteen_id');
  }

  init() {
    this.rfidSubs = this.rfidService.getRfidObservable().pipe(distinctUntilChanged()).subscribe(rfid => {
      this.getUserByRFID(rfid);
    });

    this.qrcodeSubs = this.qrcodeService.getQrcodeObservable().pipe(distinctUntilChanged()).subscribe(qrcode => {
      this.getUserByQrcode(qrcode);
    });
  }

  destroy() {
    this.rfidSubs.unsubscribe();
    this.qrcodeSubs.unsubscribe();
  }

  getUserByRFID(rfid: string) {
    const messageId = this.message.loading('A carregar utilizador', { nzDuration: 0 }).messageId;

    this.usersService.list({
      params: new HttpParams().append('limit', '1').append('query[rfid]', rfid)
    }).pipe(first(), finalize(() => this.message.remove(messageId))).subscribe(value => {
      if (value.data.length > 0) {
        if (this.listUserRFID.length > 0) {
          if (this.listUserRFID.find((user) => user.id == value.data[0].id)) {
            return;
          } else {
            this.listUserRFID.push(value.data[0]);
          }
        } else {
          this.listUserRFID.push(value.data[0]);
        }

      } else {
        this.message.error('Utilizador não encontrado');
      }
    });
  }

  getUserByQrcode(qrcode: string) {
    const messageId = this.message.loading('A carregar utilizador', { nzDuration: 0 }).messageId;

    this.usersService.list({
      params: new HttpParams().append('limit', '1').append(`query[${qrcode.includes('@') ? 'email' : 'rfid'}]`, qrcode)
    }).pipe(first(), finalize(() => this.message.remove(messageId))).subscribe(value => {
      if (value.data.length > 0) {
        if (this.listUserRFID.length > 0) {
          if (this.listUserRFID.find((user) => user.id == value.data[0].id)) {
            return;
          } else {
            this.listUserRFID.push(value.data[0]);
          }
        } else {
          this.listUserRFID.push(value.data[0]);
        }

      } else {
        this.message.error('Utilizador não encontrado');
      }
    });
  }

  reservationsLoadingObservable(): Observable<boolean> {
    return this._reservationsLoading.asObservable();
  }

  private setReservationsLoading(loading: boolean) {
    this.reservationsLoading = loading;
    this._reservationsLoading.next(loading);
  }

  reservationsObservable(): Observable<ReservationModel[]> {
    return this._userReservations.asObservable();
  }

  private setReservations(reservations: ReservationModel[]) {
    this.reservations = reservations;
    this._userReservations.next(reservations);
  }

  updateReservations() {
    this.service_id = this.configService.get('service.service_canteen_id');
    this.setReservations([]);
    const today = new Date();
    this.setReservationsLoading(true);
    this.reservationsService
      .userReservationDate(
        {
          params: new HttpParams()
            .append('withRelated', 'dishs,types,menu_dish')
            .append('query[user_id]', this.selectedUser.id.toString())
            .append('query[meal]', this.activeMeal())
            .append('query[date]', today.toISOString())
            .append('query[service_id]', this.service_id)
            .append('limit', '-1')
        })
      .pipe(first())
      .subscribe(value => {
        this.setReservations(value.data);
        this.setReservationsLoading(false);
      });
  }

  setUser(user: UserModel) {
    this.selectedUser = user;
    if (this.selectedUser) {
      this.updateReservations();
      this.updateMenu();
      this.updateBalances();
    }
    this._selectedUser.next(user);
  }

  setUserSelectRFID(userId: number) {
    this.selectUserRFID = userId;
    this._selectedUserRFID.next(userId);
  }

  public getUserSelectedRFID(): number {
    return this.selectUserRFID;
  }

  selectedUserRFIDObservable(): Observable<number> {
    return this._selectedUserRFID.asObservable();
  }

  public getUser(): UserModel {
    return this.selectedUser;
  }

  public getUserListRFID(): UserModel[] {
    return this.listUserRFID;
  }

  setUserListRFID(user: UserModel[]) {
    this.listUserRFID = user;
  }

  selectedUserObservable(): Observable<UserModel> {
    return this._selectedUser.asObservable();
  }

  unserveMeal(id: number): Observable<Resource<ReservationModel>> {
    return this.reservationsService.unserveMeal(id).pipe(
      first(),
      finalize(() => this.updateReservations())
    );
  }

  serveMeal(id: number): Observable<Resource<ReservationModel>> {
    return this.reservationsService.serveMeal(id).pipe(
      first(),
      finalize(() => this.updateReservations())
    );
  }


  cancelMeal(id: number, payment_method_id: number): Observable<Resource<ReservationModel>> {
    return this.reservationsService.cancelMeal(id, payment_method_id).pipe(
      first(),
      finalize(() => this.updateReservations())
    );
  }

  menuLoadingObservable(): Observable<boolean> {
    return this._menuLoading.asObservable();
  }

  private setMenuLoading(loading: boolean) {
    this.menuLoading = loading;
    this._reservationsLoading.next(loading);
  }

  menuObservable(): Observable<MenuModel[]> {
    return this._userMenu.asObservable();
  }

  private setMenu(menu: MenuModel[]) {
    this.menu = menu;
    this._userMenu.next(menu);
  }

  updateMenu() {
    this.service_id = this.configService.get('service.service_canteen_id');
    this.setMenu([]);
    const today = new Date();
    this.setMenuLoading(true);
    this.menuService
      .menus(
        this.service_id,
        moment(today).format('YYYY-MM-D'),
        this.activeMeal(),
        {
          params: new HttpParams().append("query[user_id]", this.selectedUser.id.toString()).append('withRelated', 'taxes')
        }
      )
      .pipe(first())
      .subscribe(value => {
        this.setMenu(value.data);
        this.setMenuLoading(false);
      });
  }

  balancesLoadingObservable(): Observable<boolean> {
    return this._balancesLoading.asObservable();
  }

  private setBalancesLoading(loading: boolean) {
    this.balancesLoading = loading;
    this._balancesLoading.next(loading);
  }

  balancesObservable(): Observable<BalanceModel[]> {
    return this._userBalances.asObservable();
  }

  setBalances(balances: BalanceModel[]) {
    this.balances = balances;
    this._userBalances.next(balances);
  }

  updateBalances() {
    if (this.selectedUser) {
      this.setBalances([]);
      this.setBalancesLoading(true);
      this.balancesService.balances(this.selectedUser.id)
        .pipe(first(), map(v => v.data.filter(v1 => v1.account_id === this.account_id)))
        .subscribe(value => {
          this.setBalances(value);
          this.setBalancesLoading(false);
        });
    } else {
      this.setBalances([]);
    }

  }

  activeMeal(): Meal {
    if (moment().hours() >= 16) {
      return Meal.DINNER
    } else {
      return Meal.LUNCH
    }
  }
}
