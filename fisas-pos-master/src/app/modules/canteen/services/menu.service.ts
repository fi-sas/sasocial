import { Observable } from 'rxjs';
import { MenuModel, Meal } from './../models/menu.model';
import { FiResourceService, Resource, HttpOptions } from './../../../core/services/resource.service';
import { FiUrlService } from './../../../core/services/url.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  menus(service_id: number, date: string, meal: Meal, options?: HttpOptions):
    Observable<Resource<MenuModel>> {

    return this.resourceService.list<MenuModel>(this.urlService.get('FOOD.MENU_SERVICE_DATE_MEAL', { service_id, date, meal }), options);
  }
}
