import { TestBed } from '@angular/core/testing';

import { ReservationsService } from './reservations.service';
import { CoreModule } from '../../../core/core.module';
import { ElectronService } from '../../../services/electron.service';

describe('ReservationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ],
      providers: [
          ElectronService
      ]
  }));

  it('should be created', () => {
    const service: ReservationsService = TestBed.get(ReservationsService);
    expect(service).toBeTruthy();
  });
});
