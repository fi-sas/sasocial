import { CartModel } from './../../../cart/models/cart.model';
import { Injectable } from '@angular/core';
import { FiUrlService } from '../../../core/services/url.service';
import {
    FiResourceService, HttpOptions,
    Resource
} from '../../../core/services/resource.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { ConfigsService } from '../../../services/configs.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ReservationModel } from '../models/reservation.model';
import { ReservationCountModel } from '../models/reservation-count.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {
  entity_id = null;
  service_id = null;

  constructor(
    private urlService: FiUrlService,
    private resourceService: FiResourceService,
    private configsService: ConfigsService
  ) {
    this.entity_id = this.configsService.get('service.entity_id');
    this.service_id = this.configsService.get('service.service_canteen_id');
  }

  reservationsDate() {
    return moment().format('YYYY-MM-DD');
  }

  reservationMeal() {
    const current_hour = moment().hours();

    if (current_hour <= 16) {
      return 'lunch';
    } else {
      return 'dinner';
    }
  }

  unserveMeal(id: number) {
    this.entity_id = this.configsService.get('service.entity_id');
    return this.resourceService.delete<ReservationModel>(
      this.urlService.get('FOOD.RESERVATIONS_UNSERVE', {
        entity_id: this.entity_id,
        id
      }),
      {}
    );
  }

  cancelMeal(id: number, payment_method_id: number) {
    this.entity_id = this.configsService.get('service.entity_id');
        return this.resourceService.create<ReservationModel>(
            this.urlService.get('FOOD.RESERVATIONS_CANCEL', {
                entity_id: this.entity_id,
                id
            }),
            {
                payment_method_id: payment_method_id
            }
        );
  }

  serveMeal(id: number): Observable<Resource<ReservationModel>> {
    this.entity_id = this.configsService.get('service.entity_id');
    return this.resourceService.create<ReservationModel>(
      this.urlService.get('FOOD.RESERVATIONS_SERVE', {
        entity_id: this.entity_id,
        id
      }),
      {}
    );
  }

  userReservationDate(options: HttpOptions): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));

    return this.resourceService.list<any>(
      this.urlService.get('FOOD.RESERVATIONS'), {
        headers: headers,
        params: options.params
      }
    );
  }
  
  userReservationDateHistory(startDate,endDate,date, page, pageSize): Observable<Resource<any>> {
    this.service_id = this.configsService.get('service.service_canteen_id');
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
    let params = new HttpParams();
    params = params.set('query[is_served]',  'true');
    params = params.set('withRelated', 'dishs,types,menus,user,menu_dish');
    if(startDate && endDate) {
      params = params.set('query[date][gte]', moment(startDate).format('YYYY-MM-DD'));
      params = params.set('query[date][lte]', moment(endDate).format('YYYY-MM-DD'));
    }else {
      params = params.set('query[date]', moment(date).format('YYYY-MM-DD'));
    }
    params = params.set('query[service_id]', this.service_id);
    params = params.set('offset', ((page - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort', '-served_at');
    return this.resourceService.list<any>(
      this.urlService.get('FOOD.RESERVATIONS'), {
        headers: headers,
        params: params
      }
    );
  }
  

  reservationsCount(): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
    this.service_id = this.configsService.get('service.service_canteen_id');
    const params = new HttpParams()
      .append('service_id', this.service_id)
      .append('date', this.reservationsDate())
      .append('meal', this.reservationMeal());

      return this.resourceService.list<any>(
        this.urlService.get('FOOD.RESERVATIONS_COUNT'), {
          headers: headers,
          params: params
        }
      );
  }

  reservationsCountByDate(date: string, meal: string): Observable<Resource<ReservationCountModel>> {
    this.service_id = this.configsService.get('service.service_canteen_id');
    const headers: HttpHeaders = new HttpHeaders()
    .set('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
      const params = new HttpParams()
          .set('service_id', this.service_id)
          .set('date', date)
          .set('meal',meal)

          return this.resourceService.list<any>(
            this.urlService.get('FOOD.RESERVATIONS_COUNT'), {
              headers: headers,
              params: params
            }
          );
  }

  refectoryCartAdd(dish_id: number, user_id?: number): Observable<Resource<CartModel>> {
    return this.resourceService.create<CartModel>(this.urlService.get('FOOD.REFECTORY_CART', { dish_id}), {user_id: user_id});
  }

  menuDishChangeQtd(menu_dish_id: number, operation: 'in'|'out', quantity: number): Observable<Resource<any>> {
    return this.resourceService.update<any>(this.urlService.get('FOOD.MENU_DISH_ID_QTD_CHANGE', { id: menu_dish_id }), { operation, quantity });
  }

  getDishTypes(): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
    return this.resourceService.list<any>(this.urlService.get('FOOD.DISH_TYPES', {}), {
      headers,
      params: new HttpParams().append('query[active]', 'true').append('query[pos_operations]', 'true')
    });
  }
  
  getDishsByName(dish_type_id: number, search: string): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
    const params: HttpParams = new HttpParams()
    .append('dish_type_id', dish_type_id.toString())
    .append('searchFields', "name,description")
    .append('search', search);
    return this.resourceService.list<any>(this.urlService.get('FOOD.DISHS', {}), {
      headers,
      params
    });
  }

  addNewDish(dish:  {
      meal: string,
      date: string,
      type_id: number,
      dish_id: number,
      doses_available: number,
      service_id: number,
      available: boolean
  }) {

    dish.service_id = this.service_id;

    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', this.configsService.get('service.entity_id'));
    return this.resourceService.create<any>(this.urlService.get('FOOD.MENU_ADD_DISH', {}),  dish, {
      headers,
    });
  }

  
}
