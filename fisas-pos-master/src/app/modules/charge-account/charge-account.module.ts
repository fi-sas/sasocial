import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChargeComponent } from './charge/charge.component';
import { ChargeDrawerService } from './services/charge-drawer.service';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [ChargeComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    ChargeDrawerService
  ],
  entryComponents: [
    ChargeComponent
  ]
})
export class ChargeAccountModule { }
