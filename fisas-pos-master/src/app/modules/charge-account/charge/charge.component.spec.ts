import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeComponent } from './charge.component';
import { SharedModule } from '../../../shared/shared.module';

describe('ChargeComponent', () => {
  let component: ChargeComponent;
  let fixture: ComponentFixture<ChargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          ChargeComponent
      ],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
