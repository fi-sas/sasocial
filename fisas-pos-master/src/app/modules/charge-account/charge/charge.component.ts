import { BalancesService } from './../../balances/services/balances.service';
import { BalanceModel } from './../../balances/models/balance.model';
import { ConfigsService } from './../../../services/configs.service';
import { ChargesService } from './../services/charges.service';
import { NzDrawerRef, NzMessageService } from 'ng-zorro-antd';
import { first, finalize } from 'rxjs/operators';
import { PaymentMethodsService } from './../../../cart/services/payment-methods.service';
import { PaymentMethodModel } from './../../../cart/models/payment-method.model';
import { UserModel } from './../../../auth/models/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fisas-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.less']
})
export class ChargeComponent implements OnInit {

  user: UserModel = null;
  input: number = 0;
  paymentMethod: number = null;
  account_id: number = null;

  commaActive: boolean = false;

  paymentMethods: PaymentMethodModel[] = [];
  paymentMethodsLoading = false;

  balance: BalanceModel = null;
  balancesLoading = false;
  boxSelected;
  chargeModalVisible = false;
  chargeAccountLoading = false;

  constructor(
    private paymentMethodsService: PaymentMethodsService,
    private balancesService: BalancesService,
    private chargesService: ChargesService,
    private configsService: ConfigsService,
    private drawerRef: NzDrawerRef<boolean>,
    private message: NzMessageService
  ) {
    this.account_id = this.configsService.get('service.account_id');
  }

  ngOnInit() {
    this.boxSelected = localStorage.getItem('boxSelected');
    this.balancesService.balances(this.user.id)
      .pipe(first(), finalize(() => this.balancesLoading = false))
      .subscribe(value => {
        this.balance = value.data.find(b => b.account_id === this.account_id);
        this.paymentMethods = this.balance.available_methods;
        const num = this.paymentMethods.find( pm => pm.tag === 'NUM');
        if (num) {
            this.paymentMethod = num.id;
        }
      });
  }

  addToInput(number: string) {
      this.input === null? this.input = 0 : null;
      if (this.input === 0) {
        if (this.commaActive) {
          this.input = parseFloat('0.' + number);
          this.commaActive = false;
        } else {
          this.input = parseFloat(number);
        }
      } else {
        if (this.commaActive) {
          this.input = parseFloat(this.input.toString().concat('.' + number));
          this.commaActive = false;
        } else {
          this.input = parseFloat(this.input.toString().concat(number));
        }
      }
  }

  removeFromInput() {
    this.input = parseFloat(this.input.toString().slice(0, -1));
    if(!this.input) {
      this.input = 0;
    }
  }

  activeComma() {
    this.commaActive = true;
  }

  close(success = false) {
    this.drawerRef.close(success);
  }

  charge() {
    this.chargeAccountLoading = true;
    this.chargesService.chargeAccount({
      transaction_value: this.input,
      account_id: this.account_id,
      user_id: this.user.id,
      description: "CARREGAMENTO EFETUADO NO POS.",
      payment_method_id: this.paymentMethod,
      cash_account_id: Number(this.boxSelected)
    })
      .pipe(finalize(() => this.chargeAccountLoading = false))
      .subscribe(value => {
        this.close(true);
      },err=> {
        if(err.error.errors[0].type == "CHARGE_AMOUNT_GREATER_THAN_ALLOWED" || err.error.errors[0].type == "CHARGE_AMOUNT_LOWER_THAN_ALLOWED") {
          this.message.error("O valor inserido não é permitido (min: " 
          + err.error.errors[0].data.min_charge_amount + '€ e máx: ' + err.error.errors[0].data.max_charge_amount + '€)');
        }
      });
  }
}
