export interface ChargeModel {
    transaction_value: number,
    account_id: number,
    user_id: number,
    payment_method_id: number,
    description: string;
    cash_account_id: number;
}