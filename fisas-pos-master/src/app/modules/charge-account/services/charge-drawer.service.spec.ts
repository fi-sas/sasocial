import { TestBed } from '@angular/core/testing';

import { ChargeDrawerService } from './charge-drawer.service';
import { CoreModule } from '../../../core/core.module';

describe('ChargeDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: ChargeDrawerService = TestBed.get(ChargeDrawerService);
    expect(service).toBeTruthy();
  });
});
