import { UserModel } from './../../../auth/models/user.model';
import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { ChargeComponent } from '../charge/charge.component';

@Injectable({
  providedIn: 'root'
})
export class ChargeDrawerService {

  constructor(private drawerService: NzDrawerService) {}

  chargeAccountDrawer(user: UserModel) {
    return this.drawerService.create({
      nzMask: true,
      nzMaskStyle: {
        'opacity': '0',
        '-webkit-animation': 'none',
        'animation': 'none',
      },
      nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
      nzContent: ChargeComponent,
      nzWidth: (window.innerWidth / 24) * 12,
      nzContentParams: {
        user,
        // value: this.value
      }
    });
  }
}
