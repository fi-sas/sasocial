import { TestBed } from '@angular/core/testing';

import { ChargesService } from './charges.service';
import { CoreModule } from '../../../core/core.module';

describe('ChargesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: ChargesService = TestBed.get(ChargesService);
    expect(service).toBeTruthy();
  });
});
