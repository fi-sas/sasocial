import { first } from 'rxjs/operators';
import { ChargeModel } from './../model/charge.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { FiUrlService } from '../../../core/services/url.service';

@Injectable({
  providedIn: 'root'
})
export class ChargesService {

  constructor(private urlService: FiUrlService,
              private resourceService: FiResourceService) { }

    chargeAccount(charge: ChargeModel): Observable<Resource<ChargeModel>> {
      return this.resourceService.create<ChargeModel>(this.urlService.get('CURRENT_ACCOUNT.CHARGE_ACCOUNT'), charge).pipe(first());
    }
}
