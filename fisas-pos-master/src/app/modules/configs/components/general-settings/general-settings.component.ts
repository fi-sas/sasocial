import { ConfigsService } from './../../../../services/configs.service';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { DevicesService } from '../../services/devices.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fisas-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.less']
})
export class GeneralSettingsComponent implements OnInit {

  device_id = null;
  devices: any[] = [];

  constructor(
    private configsService: ConfigsService,
    private devicesService: DevicesService,
    private message: NzMessageService,
    private router: Router
  ) { }

  ngOnInit() {

    this.devicesService.getDevices().pipe(first()).subscribe(result => {
      this.devices = result.data;
    });

    if (this.configsService.has('pos.device_id')) {
      this.device_id = this.configsService.get('pos.device_id')
    }
  }

  save() {
    this.configsService.set('pos.device_id', this.device_id);
    this.message.success('Configurações gerais gravadas');
  }

  out() {
    this.router.navigate(['/dashboard']);
  }
}
