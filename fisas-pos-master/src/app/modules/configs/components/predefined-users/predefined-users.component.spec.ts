import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ElectronService } from './../../../../services/electron.service';
import { SharedModule } from './../../../../shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule } from '@angular/router';
import { WherePipe } from 'angular-pipes';
import { CoreModule } from '../../../../core/core.module';
import { PredefinedUsersComponent } from './predefined-users.component';
import { ServiceSettingsComponent } from '../service-settings/service-settings.component';

describe('PredefinedUsersComponent', () => {
  let component: PredefinedUsersComponent;
  let fixture: ComponentFixture<PredefinedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterModule,
        RouterTestingModule,
        NoopAnimationsModule,
        CoreModule
      ],
      declarations: [
          ServiceSettingsComponent,
          WherePipe
      ],
      providers: [
        ElectronService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredefinedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
