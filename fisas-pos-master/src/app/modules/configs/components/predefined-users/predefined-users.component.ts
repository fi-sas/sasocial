import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NzMessageService, NzModalService } from "ng-zorro-antd";
import { ConfigsService } from "../../../../services/configs.service";

@Component({
    selector: 'fisas-predefined-users',
    templateUrl: './predefined-users.component.html',
    styleUrls: ['./predefined-users.component.less']
})
export class PredefinedUsersComponent implements OnInit {

    listUsers: any[] = [];
    form: FormGroup;
    submited = false;
    constructor(private configsService: ConfigsService,
        private router: Router,
        private modalService: NzModalService,
        private message: NzMessageService) { }

    ngOnInit() {
        this.form = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            name: new FormControl('', Validators.required)
        });
        if (this.configsService.has('user.list')) {
            this.listUsers = this.configsService.get('user.list');
        }

    }

    save() {
        this.submited = true;
        let error = false;
        if (this.form.valid) {
            if (this.listUsers.length > 0) {
                this.listUsers.forEach((user) => {
                    if (user.emailUser == this.form.get('email').value) {
                        error = true;
                    }
                })
            }
            if (!error) {
                this.submited = false;
                let user: any = {};
                user.nameUser = this.form.get('name').value;
                user.emailUser = this.form.get('email').value;
                this.listUsers.push(user);
                this.configsService.set('user.list', this.listUsers);
                this.form.reset();
                this.message.success('Configurações de utilizadores predefinidos gravadas');
            } else {
                this.message.error('Já existe esse email gravado');
            }

        }

    }

    out() {
        this.router.navigate(['/dashboard']);
    }

    deleteUser(idx) {
        this.modalService.confirm({
            nzTitle: 'Tem a certeza que deseja eliminar este utilizador da lista de predefinidos?',
            nzOnOk: () => this.confirmDelete(idx)
        });
    }

    confirmDelete(idx) {
        this.listUsers.splice(idx, 1);
        this.configsService.set('user.list', this.listUsers);
        this.message.success('Utilizador removido com sucesso');
    }
}