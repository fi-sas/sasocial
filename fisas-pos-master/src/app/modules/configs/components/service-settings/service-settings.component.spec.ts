import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ElectronService } from './../../../../services/electron.service';
import { SharedModule } from './../../../../shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceSettingsComponent } from './service-settings.component';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule } from '@angular/router';
import { WherePipe } from 'angular-pipes';
import { CoreModule } from '../../../../core/core.module';

describe('ServiceSettingsComponent', () => {
  let component: ServiceSettingsComponent;
  let fixture: ComponentFixture<ServiceSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterModule,
        RouterTestingModule,
        NoopAnimationsModule,
        CoreModule
      ],
      declarations: [
          ServiceSettingsComponent,
          WherePipe
      ],
      providers: [
        ElectronService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
