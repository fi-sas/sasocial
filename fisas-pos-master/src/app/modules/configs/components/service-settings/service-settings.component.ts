import { Component, OnInit } from '@angular/core';
import { FiscalEntitiesService } from '../../services/fiscal-entities.service';
import { ServicesFoodService } from '../../services/services-food.service';
import { first } from 'rxjs/operators';
import { ConfigsService } from '../../../../services/configs.service';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'fisas-service-settings',
  templateUrl: './service-settings.component.html',
  styleUrls: ['./service-settings.component.less']
})
export class ServiceSettingsComponent implements OnInit {

  entity_id = null;
  account_id = null;
  service_canteen_id = null;
  service_bar_id = null;

  entities = [];
  services = [];

  constructor(
    private entitiesService: FiscalEntitiesService,
    private servicesServices: ServicesFoodService,
    private configsService: ConfigsService,
    private message: NzMessageService,
    private router: Router
  ) { }

  ngOnInit() {

    if (this.configsService.has('service.entity_id')) {
      this.entity_id = this.configsService.get('service.entity_id')
    }

    if (this.configsService.has('service.account_id')) {
      this.account_id = this.configsService.get('service.account_id')
    }

    if (this.configsService.has('service.service_canteen_id')) {
      this.service_canteen_id = this.configsService.get('service.service_canteen_id');
    }

    if (this.configsService.has('service.service_bar_id')) {
      this.service_bar_id = this.configsService.get('service.service_bar_id');
    }

    this.entitiesService.getEntities().pipe(first()).subscribe(result => {
      this.entities = result.data;
    });

    if (this.entity_id) {
      this.loadServices();
    }
  }

  save() {
    this.configsService.set('service.entity_id', this.entity_id);
    this.configsService.set('service.account_id', this.account_id);
    this.configsService.set('service.service_canteen_id', this.service_canteen_id);
    this.configsService.set('service.service_bar_id', this.service_bar_id);
    this.message.success('Configurações de serviço gravadas');
    this.router.navigate(['/dashboard']);
  }

  out() {
    this.router.navigate(['/dashboard']);
  }

  loadServices() {
    this.services = [];
    if (this.entity_id) {
      this.servicesServices.getServices(this.entity_id).pipe(first()).subscribe(result => {
        this.services = result.data;
      });
    }
  }

  entityChanged(entity_id) {
    this.account_id = this.entities.find(e => e.id === entity_id).account_id;
    this.service_canteen_id = null;
    this.service_bar_id = null;
    this.loadServices();
  }
}
