import { ConfigsComponent } from './pages/configs/configs.component';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigsRoutingModule } from './configs-routing.module';
import { GeneralSettingsComponent } from './components/general-settings/general-settings.component';
import { ServiceSettingsComponent } from './components/service-settings/service-settings.component';

import { NgArrayPipesModule, NgWherePipeModule } from 'angular-pipes';
import { PredefinedUsersComponent } from './components/predefined-users/predefined-users.component';
@NgModule({
  declarations: [
    ConfigsComponent,
    GeneralSettingsComponent,
    ServiceSettingsComponent,
    PredefinedUsersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ConfigsRoutingModule,
    NgWherePipeModule,
    NgArrayPipesModule,
  ]
})
export class ConfigsModule { }
