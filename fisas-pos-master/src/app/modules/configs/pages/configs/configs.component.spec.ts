import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ElectronService } from '../../../../services/electron.service';
import { SharedModule } from '../../../../shared/shared.module';
import { ServiceSettingsComponent } from '../../components/service-settings/service-settings.component';
import { RfidConfigComponent } from '../../components/rfid-config/rfid-config.component';
import { GeneralSettingsComponent } from '../../components/general-settings/general-settings.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigsComponent } from './configs.component';
import { RouterTestingModule } from '@angular/router/testing';
import { WherePipe } from 'angular-pipes';
import { CoreModule } from '../../../../core/core.module';

describe('ConfigsComponent', () => {
  let component: ConfigsComponent;
  let fixture: ComponentFixture<ConfigsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterModule,
        RouterTestingModule,
        NoopAnimationsModule,
        CoreModule
      ],
      declarations: [
        ConfigsComponent,
        WherePipe,
        GeneralSettingsComponent,
        RfidConfigComponent,
        ServiceSettingsComponent
       ],
       providers: [
         ElectronService
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
