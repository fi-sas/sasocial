import { TestBed } from '@angular/core/testing';

import { DevicesService } from './devices.service';
import { CoreModule } from '../../../core/core.module';

describe('DevicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: DevicesService = TestBed.get(DevicesService);
    expect(service).toBeTruthy();
  });
});
