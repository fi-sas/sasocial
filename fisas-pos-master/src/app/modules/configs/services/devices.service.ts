import { Injectable } from '@angular/core';
import { FiUrlService } from '../../../core/services/url.service';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  constructor(
    private urlService: FiUrlService,
    private resourceService: FiResourceService,
  ) { }

  getDevices(): Observable<Resource<any>> {
    const url = this.urlService.get('CONFIGS.DEVICES');
    return this.resourceService.list(url, {
      params: new HttpParams().append('limit', '-1').append('query[type]', 'POS'),
    });
  }
}
