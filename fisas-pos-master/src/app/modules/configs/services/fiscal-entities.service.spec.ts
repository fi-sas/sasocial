import { TestBed } from '@angular/core/testing';

import { FiscalEntitiesService } from './fiscal-entities.service';
import { CoreModule } from '../../../core/core.module';

describe('FiscalEntitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: FiscalEntitiesService = TestBed.get(FiscalEntitiesService);
    expect(service).toBeTruthy();
  });
});
