import { TestBed } from '@angular/core/testing';

import { ServicesFoodService } from './services-food.service';
import { CoreModule } from '../../../core/core.module';

describe('ServicesFoodService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]}));

  it('should be created', () => {
    const service: ServicesFoodService = TestBed.get(ServicesFoodService);
    expect(service).toBeTruthy();
  });
});
