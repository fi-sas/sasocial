import { Injectable } from '@angular/core';
import { FiUrlService } from '../../../core/services/url.service';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesFoodService {

  constructor(
    private urlService: FiUrlService,
    private resourceService: FiResourceService,
  ) { }

  getServices(entity_id: number): Observable<Resource<any>> {
    const headers: HttpHeaders = new HttpHeaders()
    .append('x-alimentation-entity-id', entity_id.toString());

    return this.resourceService.list<any>(this.urlService.get('FOOD.SERVICES'),  {
      params: new HttpParams().append('limit', '-1'),
      headers: headers
    });
  }
}
