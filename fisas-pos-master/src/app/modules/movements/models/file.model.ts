export class FileModel {
    id: number;
    file_category_id: number;
    public: boolean;
    type: string;
    mime_type: string;
    path: string;
    weight: number;
    width: number;
    height: number;
    updated_at: string;
    created_at: string;
    url: string;
}
