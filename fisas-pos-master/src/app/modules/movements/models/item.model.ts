export class ItemModel {
  id: number;
  movement_id: number;
  product_code: string;
  name: string;
  description: string;
  extra_info: object;
  quantity: number;
  unit_value: number;
  liquid_unit_value: number;
  vat_value: number;
  total_value: number;
  vat_id: number;
  vat: number;
  location: string;
  article_type: string;
  service_confirm_path?: string;
  service_cancel_path?: string;
  updated_at: string;
  created_at: string;
  service_id: number;
  original_movement_id: number;
  paid_value: number;
  discount_value: number;
}
