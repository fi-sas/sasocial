import { ItemModel } from './item.model';

export class MovementByItem {
    id: number;
    operation: Operation;
    status: MovementStatus;
    transaction_value: number;
    current_balance: number;
    item: ItemModel;
}

export class MovementModel {
  id: number;
  operation: Operation;
  expiration_date: string;
  account_id: number;
  user_id: number;
  entity: string;
  tin: string;
  email: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  transaction_id: number;
  description: string;
  transaction_type: TransactionType;
  status: MovementStatus;
  payment_method_id: string;
  payment_method_name: string;
  affects_current_balance: boolean;
  transaction_value: number;
  erp_payment_method: string;
  erp_payment_account_code: string;
  current_balance: number;
  in_debt_value: number;
  advanced_value: number;
  paid_value: number;
  value_to_pay: number;
  document_id? : number;
  items: ItemModel[];
  created_at: string;
  updated_at: string;
  totalSummary: number;
  payment_method: any;
  cash_account: any;
}

export enum Operation {
  CHARGE = 'CHARGE',
  LEND = 'LEND',
  CANCEL_LEND = 'CANCEL_LEND',
  INVOICE_RECEIPT = 'INVOICE_RECEIPT',
  RECEIPT = 'RECEIPT',
  INVOICE = 'INVOICE',
  REFUND = 'REFUND',
  CANCEL = 'CANCEL'
}

export enum TransactionType {
    IMMEDIATE = 'IMMEDIATE',
    CREDIT = 'CREDIT'
}

export enum MovementStatus {
    CONFIRMED = 'CONFIRMED',
    PENDING = 'PENDING',
    PAID = 'PAID',
    CANCELLED = 'CANCELLED',
    CANCELLED_DUE_TO_LACK_OF_PAYMENT = 'CANCELLED_DUE_TO_LACK_OF_PAYMENT'
}
