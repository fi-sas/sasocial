export class PrintFileModel {
    convertTo: string;
    options: object;
    params: object;
    urls: object;
    data: object;
}
