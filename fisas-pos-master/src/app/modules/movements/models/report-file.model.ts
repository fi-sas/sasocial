import { FileModel } from './file.model';

export class ReportFileModel {
    key: string;
    name: string;
    description: string;
    file_id: number;
    updated_at: string;
    created_at: string;
    options: object;
    params: object;
    urls: object;
    data: object;
    file: FileModel;
    filename: string;
}
