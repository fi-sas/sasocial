import { Component, OnInit } from '@angular/core';
import { MovementsService } from '../services/movements.service';
import { MovementByItem, MovementStatus, Operation } from '../models/movement.model';
import * as moment from 'moment';
import { first } from 'rxjs/operators';
import { CanteenService } from '../../canteen/services/canteen.service';
import { NzMessageService } from 'ng-zorro-antd';
import { ConfigsService } from '../../../services/configs.service';
import { BarService } from '../../bar/services/bar.service';

/**
 * Auxiliary class for movement
 */
class MovementInfo {
    id: number;
    operation: Operation;
    status: MovementStatus;
    transaction_value: number;
    current_balance: number;
}

@Component({
    selector: 'fisas-movement',
    templateUrl: './movement.component.html',
    styleUrls: ['./movement.component.less']
})

export class MovementComponent implements OnInit {

    MovementStatus = MovementStatus;
    movementsByItems: MovementByItem[];
    loading = false;
    bar: string = '';
    constructor(private movementService: MovementsService,
        private canteenService: CanteenService,
        private barService: BarService,
        private message: NzMessageService,
        private configsService: ConfigsService) { }

    ngOnInit() {
        const initDate = moment().startOf('month').toISOString();
        const endDate = moment().endOf('month').toISOString();
        this.getMovements(initDate, endDate);
    }

    /**
     *
     * @param month
     * @description Obtains user movements of a particular checking account id related to the service's entity per month
     */
    movementsSearchForMonth(month) {
        const initDate = moment(month).startOf('month').toISOString();
        const endDate = moment(month).endOf('month').toISOString();
        this.getMovements(initDate, endDate);
    }

    /**
     *
     * @param initDate
     * @param endDate
     * @description Obtains user movements of a particular checking account id related to the service's entity per month
     */
    getMovements(initDate, endDate) {
        let user;
        if(this.bar == 'bar') {
            user = this.barService.getUser();
        }else if(this.bar == 'canteen'){
            user = this.canteenService.getUser();
        }else if(this.bar == 'barOrder'){
            let order = this.barService.getUserBar();
            user = order.user;
        }
        if (user !== null) {
            this.loading = true;
            this.movementsByItems = [];
            let movementInfo = new MovementInfo();
            let prepareItem: MovementByItem = new MovementByItem();

            this.movementService.movements(this.configsService.get('service.account_id'), user.id, initDate, endDate).pipe(first()).subscribe(movements => {

                movements.data.forEach(movement => {

                    movementInfo.id = movement.id;
                    movementInfo.status = movement.status;
                    movementInfo.operation = movement.operation;
                    movementInfo.transaction_value = movement.transaction_value;
                    movementInfo.current_balance = movement.current_balance;
                    movement.items.forEach((item, index) => {
                        prepareItem.id = movementInfo.id;
                        if (movementInfo.transaction_value < 0 && index !== 0) {
                            prepareItem.current_balance = movementInfo.current_balance + item.total_value;
                        } else {
                            prepareItem.current_balance = movementInfo.current_balance;
                        }
                        movementInfo.current_balance = prepareItem.current_balance;
                        prepareItem.transaction_value = movementInfo.transaction_value;
                        prepareItem.operation = movementInfo.operation;
                        prepareItem.status = movementInfo.status;
                        prepareItem.item = item;
                        this.movementsByItems.push(prepareItem);
                        prepareItem = new MovementByItem();
                    });
                    movementInfo = new MovementInfo();
                });
                this.loading = false;
            }, () => {
                this.message.error('Ocorreu um erro ao carregar os movimentos');
                this.loading = false;
            });
        }
    }
}
