import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovementComponent } from './movement/movement.component';
import { MovementsDrawerService } from './services/movements-drawer.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    MovementComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    MovementsDrawerService
  ],
  entryComponents: [
    MovementComponent
  ]
})
export class MovementsModule { }
