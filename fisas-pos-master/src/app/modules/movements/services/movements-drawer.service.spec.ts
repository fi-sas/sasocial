import { TestBed } from '@angular/core/testing';

import { MovementsDrawerService } from './movements-drawer.service';
import { SharedModule } from '../../../shared/shared.module';

describe('MovementsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          SharedModule
      ]
  }));

  it('should be created', () => {
    const service: MovementsDrawerService = TestBed.get(MovementsDrawerService);
    expect(service).toBeTruthy();
  });
});
