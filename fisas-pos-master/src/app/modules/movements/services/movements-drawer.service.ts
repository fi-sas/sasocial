import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { MovementComponent } from '../movement/movement.component';

@Injectable({
  providedIn: 'root'
})
export class MovementsDrawerService {

  constructor(private drawerService: NzDrawerService) { }

    /**
     * Drawer of movements
     */
    movementsDrawer(bar: string) {
        return this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper',
            nzContent: MovementComponent,
            nzWidth: (window.innerWidth / 24) * 12,
            nzContentParams: {
                 bar
            }
        });
    }
}
