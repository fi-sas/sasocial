import { TestBed } from '@angular/core/testing';

import { MovementsService } from './movements.service';
import { CoreModule } from '../../../core/core.module';

describe('MovementsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: MovementsService = TestBed.get(MovementsService);
    expect(service).toBeTruthy();
  });
});
