import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { MovementModel } from '../models/movement.model';
import { HttpParams } from '@angular/common/http';
import { FiUrlService } from '../../../core/services/url.service';
import { ConfigsService } from '../../../services/configs.service';
import * as moment from 'moment';
import { PrintFileModel } from '../models/print-file.model';
import { ReportFileModel } from '../models/report-file.model';

@Injectable({
    providedIn: 'root'
})
export class MovementsService {

    constructor(private urlService: FiUrlService,
        private resourceService: FiResourceService,
        private configsService: ConfigsService) { }

    /**
     * 
    * @param movement 
    */
    buyMeal(movement: MovementModel): Observable<Resource<MovementModel>> {
        return this.resourceService.create<MovementModel>(this.urlService.get('CURRENT_ACCOUNT.MOVEMENTS', {}), movement);
    }

    /**
     *
     * @param accountId
     * @param min_date
     * @param user_id
     * @param max_date
     * TODO
     */
    movements(accountId: number, user_id: number, min_date: string, max_date: string): Observable<Resource<MovementModel>> {
        const url = this.urlService.get('CURRENT_ACCOUNT.MOVEMENTS', {});

        const params = new HttpParams()
            .append('account_id', accountId.toString())
            .append('sort', '-created_at')
            .append('limit', '-1')
            .append('query[created_at][gte]', moment(min_date).format("YYYY-MM-DD"))
            .append('query[created_at][lte]', moment(max_date).format("YYYY-MM-DD"))
            .append('query[user_id]', user_id.toString());
        return this.resourceService.list(url, {
            params
        });
    }

    movementsByBox(accountId: number, cash_account_id, startDate: string, endDate: string, date: string, page, pageSize): Observable<Resource<MovementModel>> {
        const url = this.urlService.get('CURRENT_ACCOUNT.CASH_ACCOUNT_CLOSE', {});
        let params = new HttpParams();
        params = params.set('query[account_id]', accountId.toString());
        if(cash_account_id) {
            params = params.set('query[cash_account_id]', cash_account_id);
        }
      
        params = params.set('sort', '-created_at');
        params = params = params.set('offset', ((page - 1) * pageSize).toString());
        params = params = params.set('limit', pageSize.toString());
        if (startDate && endDate) {
            params = params.set('query[created_at][gte]', moment(startDate).format('YYYY-MM-DD'));
            params = params.set('query[created_at][lte]', moment(endDate).format('YYYY-MM-DD'));
        } else if(date != 'CUSTOM'){
            params = params.set('query[created_at]', moment(date).format('YYYY-MM-DD'));
        }
        params = params.set('withRelated','payment_method,cash_account,created_by');
        return this.resourceService.list(url, {
            params
        });
    }

    movementsByBoxReport(accountId: number, cash_account_id, startDate: string, endDate: string, date: string, page, pageSize): Observable<Resource<MovementModel>> {
        const url = this.urlService.get('CURRENT_ACCOUNT.CASH_ACCOUNT_CLOSE_REPORT', {});
        let params = new HttpParams();
        params = params.set('query[account_id]', accountId.toString());
        params = params.set('query[cash_account_id]', cash_account_id);
        params = params.set('sort', '-created_at');
        params = params = params.set('offset', ((page - 1) * pageSize).toString());
        params = params = params.set('limit', pageSize.toString());
        if (startDate && endDate) {
            params = params.set('query[created_at][gte]', moment(startDate).format('YYYY-MM-DD'));
            params = params.set('query[created_at][lte]', moment(endDate).format('YYYY-MM-DD'));
        } else {
            params = params.set('query[created_at]', moment(date).format('YYYY-MM-DD'));
        }
        params = params.set('withRelated','payment_method,cash_account,created_by');
        return this.resourceService.list(url, {
            params
        });
    }

    /**
     *
     * @param date
     * TODO
     */
    listCharges(date: string): Observable<Resource<MovementModel>> {
        const url = this.urlService.get('CURRENT_ACCOUNT.MOVEMENTS', {});

        const params = new HttpParams()
            .append('query[account_id]', this.configsService.get('service.account_id'))
            .append('sort', '-created_at')
            .append('limit', '-1')
            .append('query[created_at]', moment(date).format("YYYY-MM-DD"))
            .append('query[operation]', 'CHARGE');
        return this.resourceService.list(url, {
            params
        });
    }

    createFileReport(date: string, sumValues: number, movements: MovementModel[]): Observable<Resource<ReportFileModel>> {
        const fileOptions = new PrintFileModel();


        fileOptions.convertTo = 'pdf';
        fileOptions.data = {
            date: date,
            movements: movements,
            total_value: sumValues
        };
        fileOptions.options = {
            //     "lang": "pt-pt",
            //     "currency": {
            //     "source": "EUR",
            //     "target": "EUR",
            //     "rates": {
            //     "EUR": 1,
            //     "USD": 2
            //     }
            // }
        };
        fileOptions.urls = {};
        fileOptions.params = {};
        return this.resourceService.create<ReportFileModel>(
            this.urlService.get('REPORTS.TEMPLATE_PRINT', { key: 'LIST_CHARGES_DAY' }), fileOptions);
    }
}
