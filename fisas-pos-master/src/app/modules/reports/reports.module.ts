import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ReportsComponent } from '../reports/reports/reports.component';

@NgModule({
  declarations: [
    ReportsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ReportsComponent
  ]
})
export class ReportsModule { }
