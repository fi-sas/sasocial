import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ReportModel } from '../models/reports.model';
import { ReportsService } from '../services/reports.service';
import { ElectronService } from 'ngx-electron';
@Component({
  selector: 'fisas-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.less']
})
export class ReportsComponent implements OnInit {

    visibleReports: boolean;
    $reports = null;
    $reportsLength = null;
    loading = true;
    total: number = 0;
    constructor(
        private reportsService: ReportsService,
        private _electronService: ElectronService,
        ) {
        this.getReports();
        this.getReportsLength();
        this.reportsService.total$.subscribe(
            (total) => {
                this.total = total;
            }
        );
    }

    ngOnInit() {
    }

    getReports() {
        this.$reports = null;
        this.$reports = this.reportsService
            .getReportsObservable();
    }

    getReportsLength() {
        this.$reportsLength = null;
        this.$reportsLength = this.reportsService
            .getReportsLengthObservable();
        this.loading = false;
    }


    refresh() {
        this.loading = true;
        setTimeout(() => {
            this.loading = false;
            this.reportsService.loadData();
          }, 200);
    }

    clickMe(): void {
        this.visibleReports = false;
    }

    change(value: boolean): void {
        console.log(value);
    }

    showMore() {
        this.reportsService.loadData(true);
    }

    download(report: ReportModel) {
        if(report.unread == true) {
            this.reportsService.updateReadReport(report.id).pipe(first()).subscribe(() => {
                report.unread = false;
                this.reportsService.decrementTotal();
            });
        }

        this._electronService.ipcRenderer.send('download-save-file',  report.file.url);
    }

    
    delete(id) {
        this.reportsService.delete(id).pipe(first()).subscribe((data)=> {
            this.refresh();
        })
    }

}
