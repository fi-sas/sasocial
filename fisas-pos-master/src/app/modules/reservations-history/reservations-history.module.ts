import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationsComponent } from './reservations/reservations.component';
import { ReservationDrawerService } from './services/reservation-drawer.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
      ReservationsComponent
  ],
  imports: [
      CommonModule,
      SharedModule
  ],
  providers: [
      ReservationDrawerService
  ],
  entryComponents: [
      ReservationsComponent
  ]
})
export class ReservationsHistoryModule { }
