import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ReservationsService } from '../../canteen/services/reservations.service';
import { HttpParams } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { ReservationModel } from '../../canteen/models/reservation.model';

@Component({
  selector: 'fisas-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.less']
})
export class ReservationsComponent implements OnInit {
  pageIndex = 1;
  pageSize = 10;
  totalData = 0;
  selectOptions = [
    {
      value: moment().toISOString(),
      label: 'Hoje'
    },
    {
      value: moment().subtract(1, 'day').toISOString(),
      label: 'Ontem'
    },
    {
      value: 'CUSTOM',
      label: 'Personalizado'
    }
  ];

  meals = [
    {
      value: 'lunch',
      translate: 'Almoço'
    },
    {
      value: 'dinner',
      translate: 'Jantar'
    }
  ];

  loading = false;
  reservations: ReservationModel[] = [];

  // Value from select input
  selectedValue;

  dateFormat = 'yyyy/MM/dd';
  startDate = null;
  endDate = null;

  disableStartDate = (current: Date): boolean => {
    if(this.endDate !== null) {
      return !moment(this.endDate).isBetween(moment(current), moment());
    }
    return current > new Date();
    
  };

  disableEndDate = (current: Date): boolean => {
    if (this.startDate !== null) {
      return !moment(current).isBetween(moment(this.startDate), moment());
    }
    return current > new Date();
  };

  constructor(private reservationService: ReservationsService) { }

  ngOnInit() {
    this.selectedValue = this.selectOptions[0].value;
    this.getReservations(null, null, this.selectedValue);
  }

  /**
   *
   */
  onChangeSelectInput() {
    if (this.selectedValue !== 'CUSTOM') {
      this.startDate = null;
      this.endDate = null;
      this.getReservations(null, null, this.selectedValue, true)
    }else{
      this.reservations = [];
  }
  }

  /**
   *
   * @param startDate
   * @description
   */
  onChangeStartDate(startDate) {
    this.startDate = startDate.toISOString();
    if (this.startDate !== null && this.endDate !== null) {
      this.getReservations(this.startDate, this.endDate, null, true)
    }
  }

  /**
   *
   * @param endDate
   */
  onChangeEndDate(endDate) {
    this.endDate = endDate.toISOString();
    if (this.startDate !== null && this.endDate !== null) {
      this.getReservations(this.startDate, this.endDate, null, true)
    }
  }

  /**
   *
   * @param startDate
   * @param endDate
   * @param date
   * @description get all reservations served on a certain date
   */
  getReservations(startDate: string, endDate: string, date: string, reset: boolean = false): void {
    this.loading = true;
    if (reset) {
      this.pageIndex = 1;
    }

    this.reservationService.userReservationDateHistory(
      startDate, endDate, date, this.pageIndex,
      this.pageSize
    ).pipe(first()).subscribe(reservations => {
      this.reservations = reservations.data;
      this.totalData = reservations.link.total;
      this.loading = false;
    }, () => {
      this.loading = false;
    })
  }

  /**
   *
   * @param mealIn
   * @description Translate meal to portuguese language
   */
  translateMeal(mealIn: string): string {
    return this.meals.find(meal => meal.value === mealIn).translate
  }

}
