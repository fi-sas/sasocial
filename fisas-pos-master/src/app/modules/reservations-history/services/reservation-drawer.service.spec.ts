import { TestBed } from '@angular/core/testing';

import { ReservationDrawerService } from './reservation-drawer.service';
import { SharedModule } from '../../../shared/shared.module';

describe('ReservationDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          SharedModule
      ]
  }));

  it('should be created', () => {
    const service: ReservationDrawerService = TestBed.get(ReservationDrawerService);
    expect(service).toBeTruthy();
  });
});
