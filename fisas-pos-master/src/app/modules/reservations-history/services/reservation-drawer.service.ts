import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { ReservationsComponent } from '../reservations/reservations.component';

@Injectable({
  providedIn: 'root'
})
export class ReservationDrawerService {

  constructor(private drawerService: NzDrawerService) { }

    /**
     * Drawer of reservations history
     */
    reservationsDrawer() {
        const drawerRef = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper',
            nzContent: ReservationsComponent,
            nzWidth: window.innerWidth,
            nzContentParams: {
                // value: this.value
            }
        });

        return drawerRef.afterClose;
    }
}
