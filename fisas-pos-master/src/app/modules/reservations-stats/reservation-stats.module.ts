import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsComponent } from './stats/stats.component';
import { SharedModule } from '../../shared/shared.module';
import { ReservationStatsDrawerService } from './services/reservation-stats-drawer.service';



@NgModule({
  declarations: [
    StatsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    ReservationStatsDrawerService
  ],
  entryComponents: [
    StatsComponent
  ]
})
export class ReservationStatsModule { }
