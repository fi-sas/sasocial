import { TestBed } from '@angular/core/testing';

import { ReservationStatsDrawerService } from './reservation-stats-drawer.service';

describe('ReservationStatsDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReservationStatsDrawerService = TestBed.get(ReservationStatsDrawerService);
    expect(service).toBeTruthy();
  });
});
