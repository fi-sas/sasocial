import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { StatsComponent } from '../stats/stats.component';

@Injectable({
  providedIn: 'root'
})
export class ReservationStatsDrawerService {

  constructor(private drawerService: NzDrawerService) { }

    /**
     * Drawer of reservations stats
     */
    reservationsStatsDrawer() {
        const drawerRef = this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper',
            nzContent: StatsComponent,
            nzWidth: window.innerWidth,
            nzContentParams: {
                // value: this.value
            }
        });

        return drawerRef.afterClose;
    }
}
