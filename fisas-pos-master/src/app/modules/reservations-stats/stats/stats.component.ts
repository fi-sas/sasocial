import { Component, OnInit } from "@angular/core";
import { ReservationsService } from "../../canteen/services/reservations.service";
import { HttpParams } from "@angular/common/http";
import * as moment from "moment";
import { finalize, first } from "rxjs/operators";
import { ReservationModel } from "../../canteen/models/reservation.model";
import { ConfigsService } from "../../../services/configs.service";
import { ReservationCountModel } from "../../canteen/models/reservation-count.model";

@Component({
  selector: "fisas-stats",
  templateUrl: "./stats.component.html",
  styleUrls: ["./stats.component.less"],
})
export class StatsComponent implements OnInit {
  meals = [
    {
      translate: "Almoço",
      type: "lunch",
    },
    {
      translate: "Jantar",
      type: "dinner",
    },
  ];

  date = moment().toISOString();
  dateFormat = "yyyy/MM/dd";
  reservations: ReservationModel[] = [];
  reservationsTotal = 0;
  isLoadingReservations = false;
  isLoadingStats = false;
  stats: ReservationCountModel[] = [];
  mealValue = this.reservationsService.reservationMeal();
  filterOption = "all";
  pageSize = 5;
  pageIndex = 1;
  offset = 0;

  /*
   * MODAL
   */
  qtdModalVisible = false;
  qtdToChange = 0;
  qtdToAfterChange = 0;
  qtdToAvailableDoses = 0;
  qtdModalMenuDishId = null;
  qtdOperationToChange: "in" | "out" = "in";

  constructor(
    private reservationsService: ReservationsService,
    private configsService: ConfigsService
  ) {}

  ngOnInit() {
    this.getReservations(this.date, this.mealValue);
    this.getReservationsStats(this.date, this.mealValue);
  }

  /**
   *
   * @param date
   * @description get reservation and stats by date
   */
  onChangeDate(date) {
    if (date !== null) {
      this.pageIndex = 1;
      this.pageSize = 5;
      this.offset = 0;
      this.date = date.toISOString();
      this.getReservations(this.date, this.mealValue);
      this.getReservationsStats(
        moment(this.date).format("YYYY-MM-DD"),
        this.mealValue
      );
    }
  }

  /**
   * @description get reservation and stats by meal
   */
  onChangeSelectInput() {
    this.pageIndex = 1;
    this.pageSize = 5;
    this.offset = 0;
    this.getReservations(this.date, this.mealValue);
    this.getReservationsStats(
      moment(this.date).format("YYYY-MM-DD"),
      this.mealValue
    );
  }

  /**
   * @description get reservation by filter
   */
  onChangeFilterInput() {
    this.pageIndex = 1;
    this.pageSize = 5;
    this.offset = 0;
    this.getReservations(this.date, this.mealValue);
  }

  /**
   * @description get reservations by day
   * @param date
   * @param meal
   */
  getReservations(date: string, meal: string) {
    this.isLoadingReservations = true;

    let params = new HttpParams()
      .append("offset", this.offset.toString())
      .append("limit", this.pageSize.toString())
      .append("query[date]", date)
      .append(
        "query[service_id]",
        this.configsService.get("service.service_canteen_id")
      )
      .append("withRelated", "dishs,types,menu_dish,user");

    if (meal) {
      params = params.set("query[meal]", meal);
    }

    if (this.filterOption === "served") {
      params = params.set("query[is_served]", "true");
      params = params.set("sort", "-served_at");
    } else if (this.filterOption === "nonServed") {
      params = params.set("query[is_served]", "false");
      params = params.set("query[has_canceled]", "false");
    } else if (this.filterOption === "cancelled") {
      params = params.set("query[has_canceled]", "true");
    }

    this.reservationsService
      .userReservationDate({
        params: params,
      })
      .pipe(first())
      .subscribe(
        (reservations) => {
          this.reservations = reservations.data;
          this.reservationsTotal = reservations.link.total;
          this.isLoadingReservations = false;
        },
        () => {
          this.isLoadingReservations = false;
        }
      );
  }

  /**
   *
   * @param date
   * @param meal
   * @description get reservations stats
   */
  getReservationsStats(date: string, meal: string) {
    this.isLoadingReservations = true;
    this.reservationsService
      .reservationsCountByDate(moment(date).format("YYYY-MM-DD"), meal)
      .subscribe(
        (stats) => {
          this.stats = [...stats.data];
          this.isLoadingReservations = false;
        },
        () => {
          this.isLoadingReservations = false;
        }
      );
  }

  /**
   * @param pageIndex
   */
  loadMoreReservations(pageIndex) {
    if (pageIndex === 1) {
      this.offset = 0;
    } else {
      this.pageIndex = pageIndex;
      this.offset = this.pageSize * pageIndex - this.pageSize;
    }
    this.getReservations(this.date, this.mealValue);
  }

  /**
   *
   * @param prop
   * @description total sum of each row
   */
  countTotalStats(prop: string): number {
    if (this.stats.length !== 0) {
      return this.stats
        .map(function (b) {
          return b[prop];
        })
        .reduce(function (p, c) {
          return p + c;
        });
    } else {
      return 0;
    }
  }

  /**
   *
   * @param mealIn
   * @description Translate meal to portuguese language
   */
  translateMeal(mealIn: string): string {
    return this.meals.find((meal) => meal.type === mealIn).translate;
  }

  openModalQtd(menu_dish_id: number, doses_available: number) {
    this.qtdModalVisible = true;
    this.qtdModalMenuDishId = menu_dish_id;
    this.qtdToChange = 0;
    this.qtdToAvailableDoses = doses_available;
    this.qtdToAfterChange = doses_available;
    this.qtdOperationToChange = "in";
  }

  closeModalQtd() {
    this.qtdModalVisible = false;
    this.qtdModalMenuDishId = null;
    this.qtdOperationToChange = "in";
    this.qtdToChange = 0;
    this.qtdToAfterChange = 0;
  }

  confirmQtd() {
    this.reservationsService
      .menuDishChangeQtd(
        this.qtdModalMenuDishId,
        this.qtdOperationToChange,
        this.qtdToChange
      )
      .pipe(first())
      .subscribe((resp) => {
        this.getReservationsStats(this.date, this.mealValue);
        this.closeModalQtd();
      });
  }

  addQtdToChange() {
    this.qtdToChange++;
    this.updateQtdToAfterChange();
  }

  removeQtdToChange() {
    if (this.qtdToChange > 1) {
      this.qtdToChange--;
    }
    this.updateQtdToAfterChange();
  }
  updateQtdToAfterChange(operation?: "in" | "out") {
    if (operation) {
      this.qtdOperationToChange = operation;
    }

    if (this.qtdOperationToChange === "in") {
      this.qtdToAfterChange = this.qtdToAvailableDoses + this.qtdToChange;
    } else {
      this.qtdToAfterChange = this.qtdToAvailableDoses - this.qtdToChange;
      if (this.qtdToAfterChange < 0) {
        this.qtdToChange--;
        this.updateQtdToAfterChange();
      }
    }
  }

  addMenuDishIndex = 0;
  addNewDishModal_visible = false;
  addNewDishModal_loading = false;
  loadingDishTypes = false;
  loadingDishs = false;
  dishTypes: any = [];
  selectedDishType_id = null;
  searchDish = "";
  menuDishQuantity = 0;
  dishs: any = [];
  selectedDish_id = null;

  onMenuDishIndexChange(event)  {

  }

  loadDishsTypes() {
    this.loadingDishTypes = true;
    this.reservationsService
      .getDishTypes()
      .pipe(
        first(),
        finalize(() => (this.loadingDishTypes = false))
      )
      .subscribe((result) => {
        this.dishTypes = result.data;
      });
  }

  selectDishType(dish_type_id: number) {
      this.selectedDishType_id = dish_type_id;
      this.addMenuDishIndex++;
      this.loadDishs();
  }

  selectDish(dish_id: number) {
      this.selectedDish_id = dish_id;
      this.addMenuDishIndex++;
  }

  backStep() {
      if(this.addMenuDishIndex > 0)
        this.addMenuDishIndex--;
      else 
        this.closeNewDishModal();
  }

  searchDishByName(name) {
    this.loadDishs();
  }

  loadDishs() {
        this.loadingDishs = true;
      this.reservationsService.getDishsByName(this.selectedDishType_id, this.searchDish)
      .pipe(
          first(),
          finalize(() => this.loadingDishs = false)
      ).subscribe(result => {
            this.dishs = result.data;
      });
  }

  addNewDishModal() {
    this.addNewDishModal_visible = true;
    this.loadDishsTypes();
  }

  confirmDish() {
    this.addNewDishModal_loading = true;
    this.reservationsService.addNewDish({
      meal: this.mealValue,
      date: this.date,
      service_id: null,
      available: true,
      doses_available: this.menuDishQuantity,
      dish_id: this.selectedDish_id,
      type_id: this.selectedDishType_id
    }).pipe(
      first(),
      finalize(() => this.addNewDishModal_loading = false)
    ).subscribe(() => {
      this.getReservationsStats(
        moment(this.date).format("YYYY-MM-DD"),
        this.mealValue
      );
      this.closeNewDishModal();
    })
  }

  closeNewDishModal() {
    this.addNewDishModal_visible = false;
    this.searchDish = '';
    this.selectedDishType_id = null;
    this.selectedDish_id = null;
    this.addMenuDishIndex = 0;
    this.menuDishQuantity = 0;
  }

  addToQuantityInput(number: string) {
    this.menuDishQuantity === null? this.menuDishQuantity = 0 : null;
    if (this.menuDishQuantity === 0) {
      
        this.menuDishQuantity = parseFloat(number);
    } else {
        this.menuDishQuantity = parseFloat(this.menuDishQuantity.toString().concat(number));
    }
  }
  removeQuantityFromInput() {
    this.menuDishQuantity = parseFloat(this.menuDishQuantity.toString().slice(0, -1));
    if(!this.menuDishQuantity) {
      this.menuDishQuantity = 0;
    }
  }
}
