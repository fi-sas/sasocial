import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchUserComponent } from './search-user/search-user.component';
import { SearchDrawerService } from './services/search-drawer.service';
import { SharedModule } from '../../shared/shared.module';
import { UsersService } from '../../auth/services/users.service';
import { MatKeyboardModule } from '../../keyboard/keyboard.module';
@NgModule({
  declarations: [
    SearchUserComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatKeyboardModule
  ],
  providers: [
    SearchDrawerService,
    UsersService,
  ],
  entryComponents: [
    SearchUserComponent
  ]
})
export class SearchUserModule { }
