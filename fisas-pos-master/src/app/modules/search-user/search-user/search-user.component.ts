import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { UsersService } from '../../../auth/services/users.service';
import { Subject } from 'rxjs';
import { debounceTime, first, finalize } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { UserModel } from '../../../auth/models/user.model';
import { NzDrawerRef } from 'ng-zorro-antd';
import { ComponentPortal } from '@angular/cdk/portal';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
@Component({
  selector: 'fisas-search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.less'],
})


export class SearchUserComponent implements OnInit, OnDestroy {

  userSearch = '';
  _userSearchSubject: Subject<string> = new Subject();

  loading = false;
  users: UserModel[] = [];

  constructor(
    private usersService: UsersService,
    private drawerRef: NzDrawerRef<UserModel>,
    private cdr: ChangeDetectorRef) { }

  close(user: UserModel): void {
    this.drawerRef.close(user);
  }

  ngOnInit() {
    this.searchUsers();
  }

  ngOnDestroy() {
    this._userSearchSubject.unsubscribe();
  }

  searchUsers() {
    this._userSearchSubject.pipe(
      debounceTime(500)
    ).subscribe((searchValue: string) => {
      this.loading = true;
      this.usersService.list({
        params: new HttpParams()
          .append('searchFields', 'name,student_number,email')
          .append('query[active]', 'true')
          .append('search', searchValue)
          .append('limit', '30'),
      }).pipe(first(), finalize(() => this.loading = false)).subscribe(values => {
        this.userSearch = searchValue;
        this.users = values.data;
      });
    });
  }

  userSearchChanged(search) {
    this._userSearchSubject.next(search);
  }

  selectUser(user) {
    this.close(user);
  }

  changeValue(event: string) {
    this.userSearch = event;
    this.userSearchChanged(this.userSearch);
  }
}
