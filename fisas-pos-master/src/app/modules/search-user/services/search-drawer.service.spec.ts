import { TestBed } from '@angular/core/testing';

import { SearchDrawerService } from './search-drawer.service';
import { SharedModule } from '../../../shared/shared.module';

describe('SearchDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          SharedModule
      ]
  }));

  it('should be created', () => {
    const service: SearchDrawerService = TestBed.get(SearchDrawerService);
    expect(service).toBeTruthy();
  });
});
