import { Injectable } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd';
import { SearchUserComponent } from '../search-user/search-user.component';
import { UserModel } from '../../../auth/models/user.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SearchDrawerService {

  constructor(private drawerService: NzDrawerService) {}

  searchUserDrawer(): Observable<UserModel> {
    const drawerRef = this.drawerService.create({
      nzMask: true,
      nzMaskStyle: {
        'opacity': '0',
        '-webkit-animation': 'none',
        'animation': 'none',
      },
      nzWrapClassName: 'drawerWrapper',
      nzContent: SearchUserComponent,
      nzWidth: window.innerWidth,
      nzContentParams: {
        // value: this.value
      }
    });

    return drawerRef.afterClose;
  }
}
