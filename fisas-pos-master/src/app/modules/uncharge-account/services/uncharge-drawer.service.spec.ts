import { TestBed } from '@angular/core/testing';

import { UnchargeDrawerService } from './uncharge-drawer.service';
import { CoreModule } from '../../../core/core.module';

describe('UnchargeDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: UnchargeDrawerService = TestBed.get(UnchargeDrawerService);
    expect(service).toBeTruthy();
  });
});
