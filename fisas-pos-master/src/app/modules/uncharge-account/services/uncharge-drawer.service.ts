import { Injectable } from '@angular/core';
import { UserModel } from '../../../auth/models/user.model';
import { NzDrawerService } from 'ng-zorro-antd';
import { UnchargeComponent } from '../uncharge/uncharge.component';

@Injectable({
  providedIn: 'root'
})
export class UnchargeDrawerService {

  constructor(private drawerService: NzDrawerService) { }

    unchargeAccountDrawer(user: UserModel) {
        return this.drawerService.create({
            nzMask: true,
            nzMaskStyle: {
                'opacity': '0',
                '-webkit-animation': 'none',
                'animation': 'none',
            },
            nzWrapClassName: 'drawerWrapper chargeDrawerWrapper',
            nzContent: UnchargeComponent,
            nzWidth: (window.innerWidth / 24) * 12,
            nzContentParams: {
                user,
                // value: this.value
            }
        });
    }
}
