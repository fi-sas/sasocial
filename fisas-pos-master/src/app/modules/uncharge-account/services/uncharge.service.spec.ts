import { TestBed } from '@angular/core/testing';

import { UnchargeService } from './uncharge.service';
import { CoreModule } from '../../../core/core.module';

describe('UnchargeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
          CoreModule
      ]
  }));

  it('should be created', () => {
    const service: UnchargeService = TestBed.get(UnchargeService);
    expect(service).toBeTruthy();
  });
});
