import { Injectable } from '@angular/core';
import { FiUrlService } from '../../../core/services/url.service';
import { FiResourceService, Resource } from '../../../core/services/resource.service';
import { UnchargeModel } from '../model/uncharge.model';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnchargeService {

  constructor(private urlService: FiUrlService,
              private resourceService: FiResourceService) { }

    unChargeAccount(uncharge: UnchargeModel): Observable<Resource<UnchargeModel>> {
        return this.resourceService.create<UnchargeModel>(this.urlService.get('CURRENT_ACCOUNT.UNCHARGE_ACCOUNT'), uncharge).pipe(first());
    }
}
