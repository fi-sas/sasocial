import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnchargeComponent } from './uncharge/uncharge.component';
import { SharedModule } from '../../shared/shared.module';
import { UnchargeService } from './services/uncharge.service';
import { UnchargeDrawerService } from './services/uncharge-drawer.service';


@NgModule({
  declarations: [
     UnchargeComponent
  ],
  imports: [
     CommonModule,
     SharedModule
  ],
  providers: [
     UnchargeService,
     UnchargeDrawerService
  ],
  entryComponents: [
     UnchargeComponent
  ]
})
export class UnchargeAccountModule { }
