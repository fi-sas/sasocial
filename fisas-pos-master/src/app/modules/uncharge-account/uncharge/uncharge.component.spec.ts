import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnchargeComponent } from './uncharge.component';
import { SharedModule } from '../../../shared/shared.module';

describe('UnchargeComponent', () => {
  let component: UnchargeComponent;
  let fixture: ComponentFixture<UnchargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          UnchargeComponent
      ],
      imports: [
          SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnchargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
