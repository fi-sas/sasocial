import { Component, OnInit } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { NzDrawerRef } from 'ng-zorro-antd';
import { ConfigsService } from '../../../services/configs.service';
import { BalancesService } from '../../balances/services/balances.service';
import { PaymentMethodsService } from '../../../cart/services/payment-methods.service';
import { UserModel } from '../../../auth/models/user.model';
import { BalanceModel } from '../../balances/models/balance.model';
import { PaymentMethodModel } from '../../../cart/models/payment-method.model';
import { UnchargeService } from '../services/uncharge.service';

@Component({
  selector: 'fisas-uncharge',
  templateUrl: './uncharge.component.html',
  styleUrls: ['./uncharge.component.less']
})
export class UnchargeComponent implements OnInit {

    user: UserModel = null;
    input: number = 0;
    paymentMethod: number = null;
    account_id: number = null;

    commaActive: boolean = false;

    paymentMethods: PaymentMethodModel[] = [];
    paymentMethodsLoading = false;

    balance: BalanceModel = null;
    balancesLoading = false;
    boxSelected;
    unchargeModalVisible = false;
    unchargeAccountLoading = false;

    constructor(
        private paymentMethodsService: PaymentMethodsService,
        private balancesService: BalancesService,
        private unChargeService: UnchargeService,
        private configsService: ConfigsService,
        private drawerRef: NzDrawerRef<boolean>,
    ) {
        this.account_id = this.configsService.get('service.account_id');
    }

    ngOnInit() {
        this.boxSelected = localStorage.getItem('boxSelected');
        this.balancesService.balances(this.user.id)
            .pipe(first(), finalize(() => this.balancesLoading = false))
            .subscribe(value => {
                this.balance = value.data.find(b => b.account_id === this.account_id);
                this.paymentMethods = this.balance.available_methods;
                const num = this.paymentMethods.find( pm => pm.tag === 'NUM');
                if (num) {
                    this.paymentMethod = num.id;
                }
            });
    }

    addToInput(number: string) {
        this.input === null? this.input = 0 : null;
        if (this.input === 0) {
            if (this.commaActive) {
                this.input = parseFloat('0.' + number);
                this.commaActive = false;
            } else {
                this.input = parseFloat(number);
            }
        } else {
            if (this.commaActive) {
                this.input = parseFloat(this.input.toString().concat('.' + number));
                this.commaActive = false;
            } else {
                this.input = parseFloat(this.input.toString().concat(number));
            }
        }
    }

    removeFromInput() {
        this.input = parseFloat(this.input.toString().slice(0, -1));
        if(!this.input) {
            this.input = 0;
        }
    }

    activeComma() {
        this.commaActive = true;
    }

    close(success = false) {
        this.drawerRef.close(success);
    }

    unCharge() {
        this.unchargeAccountLoading = true;
        this.unChargeService.unChargeAccount({
            transaction_value: this.input,
            account_id: this.account_id,
            user_id: this.user.id,
            description: "DEVOLUÇÃO EFETUADA NO POS.",
            payment_method_id: this.paymentMethod,
            cash_account_id: Number(this.boxSelected)
        })
        .pipe(finalize(() => this.unchargeAccountLoading = false))
        .subscribe(value => {
            this.close(true);
        });
    }

}
