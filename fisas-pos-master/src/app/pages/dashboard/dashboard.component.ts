import { ConfigsService } from './../../services/configs.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/services/auth.service';

export class DashboardModule {
  name: string;
  link?: string;
  icon?: string;
  active: boolean;
  callback: () => void;

  constructor(name: string, link?: string, icon?: string, active = false, callback = null) {
    this.name = name;
    this.link = link;
    this.icon = icon;
    this.active = active;
    this.callback = callback;
  }
}

@Component({
  selector: 'fisas-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  modules: DashboardModule[] = [];
  service_id_Canteen = null;
  service_id_Bar = null;
  constructor(
    private router: Router,
    private authService: AuthService,
    private configsService: ConfigsService) {
  }

  ngOnInit() {

    if(!this.configsService.isSettingsValid()) {
      this.router.navigate(['configs']);
    }
    this.service_id_Canteen = this.configsService.get('service.service_canteen_id');
    this.service_id_Bar = this.configsService.get('service.service_bar_id');
    this.modules.push(
      new DashboardModule('Bar', '/bar', 'icons:icons-bar', this.service_id_Bar ? true : false),
      new DashboardModule('Cantina', '/canteen', 'icons:icons-food', this.service_id_Canteen ? true : false),
      new DashboardModule('Configurações', '/configs', 'icons:icons-settings', this.authService.hasPermission('pos:config')),
      new DashboardModule('Sair', null, 'icons:icons-off', true, () => {this.logout(); }),
    );
    this.modules = this.modules.filter((mod)=>mod.active);
  }

  selectModule(module) {
    if (module.active === true && module.link) {
      this.router.navigate([module.link]);
    }

    if (module.active === true && module.callback) {
      module.callback();
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
