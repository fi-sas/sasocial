import { AuthService } from './../../auth/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BalancesService } from '../../modules/balances/services/balances.service';
import { finalize, first } from 'rxjs/operators';
import { CashAccountModel } from '../../modules/balances/models/cash-account.model';
import { ConfigsService } from '../../services/configs.service';

@Component({
  selector: 'fisas-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isLoading = false;
  showError = false;
  submitted = false;
  error: string;
  returnUrl: string;
  loginPredefined = false;
  modalOpen = false;
  loadingBox = false;
  cashAccounts: CashAccountModel[] = [];
  boxSeleted;
  listUsers: any[] = [];
  userSelected: any = null;
  input = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private configsService: ConfigsService,
    private balancesService: BalancesService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
    if (this.configsService.has('user.list') && this.configsService.get('user.list').length>0) {
      this.listUsers = this.configsService.get('user.list');
      this.loginPredefined = true;
    }else{
      this.loginPredefined = false;
    }
  }

  changeForLogin() {
    this.loginPredefined = false; 
    this.userSelected = null; 
    this.input = null; 
    this.submitted = false;
  }

  getEmailInputError() {
    return this.loginForm.controls.email.errors.required
      ? 'Introduza o email'
      : this.loginForm.controls.email.errors.email
        ? 'Introduza um email válido'
        : '';
  }

  getPasswordInputError() {
    return this.loginForm.controls.password.errors.required
      ? 'Introduza a password'
      : '';
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(formValue: any, valid: boolean) {
    if (!valid) {
      this.loginForm.markAsDirty();
      return;
    }
    localStorage.removeItem('boxSelected');
    this.showError = false;
    this.isLoading = true;

    this.authService.login(formValue.email, formValue.password).pipe(first()).subscribe(
      () => {
        this.isLoading = false;

        this.loadingBox = true;
        this.balancesService.getUserAvailableCashAccounts().pipe(first(),
          finalize(() => this.loadingBox = false)
        ).subscribe(response => {
          this.cashAccounts = response.data;
          if (this.cashAccounts.length > 0) {
            if (this.cashAccounts.length == 1) {
              this.boxSeleted = this.cashAccounts[0].id;
            }
            this.modalOpen = true;
          } else {
            localStorage.setItem('boxSelected', this.boxSeleted);
            this.router.navigate([this.returnUrl]);
          }
        }, error => {
          this.router.navigate([this.returnUrl]);
        });
      },
      error => {
        if (error.status === 400) {
          if (error.error.errors.length > 0) {
            if (error.error.errors[0].code === 902) {
              this.error = 'As credencias são inválidas';
            }
          }
        } else {
          this.error = 'Ocorreu um erro ao realizar o login.';
        }

        this.showError = true;
        this.isLoading = false;
        throw error;
      }
    );
  }

  save() {
    this.submitted = true;
    if (this.boxSeleted) {
      this.submitted = false;
      this.router.navigate([this.returnUrl]);
      localStorage.setItem('boxSelected', this.boxSeleted);
    }
  }

  addToInput(number: string) {
   if(this.input == null){
     this.input = number;
     return;
   }
    this.input = this.input.toString().concat(number);
  }

  removeFromInput() {
    if(this.input == null) {
      return;
    }
    this.input = this.input.toString().slice(0, -1);
    if (!this.input) {
      this.input = null;
    }
  }

  pin(user) {
    this.userSelected = user;
    this.input = null;
  }

  loginPredif() {
    this.submitted = true;
    if(this.input && this.input.length == 4 && this.userSelected) {
      this.submitted = false;
      localStorage.removeItem('boxSelected');
      this.showError = false;
      this.isLoading = true;
  
      this.authService.loginPin(this.userSelected.emailUser, this.input.toString()).pipe(first()).subscribe(
        () => {
          this.isLoading = false;
          this.loadingBox = true;
          this.balancesService.getUserAvailableCashAccounts().pipe(first(),
            finalize(() => this.loadingBox = false)
          ).subscribe(response => {
            this.cashAccounts = response.data;
            if (this.cashAccounts.length > 0) {
              if (this.cashAccounts.length == 1) {
                this.boxSeleted = this.cashAccounts[0].id;
              }
              this.modalOpen = true;
            } else {
              localStorage.setItem('boxSelected', this.boxSeleted);
              this.router.navigate([this.returnUrl]);
            }
          }, error => {
            this.router.navigate([this.returnUrl]);
          });
        },
        error => {
          if (error.status === 400) {
            if (error.error.errors.length > 0) {
              if (error.error.errors[0].code === 902) {
                this.error = 'As credencias são inválidas';
              }
            }
          } else {
            this.error = 'Ocorreu um erro ao realizar o login.';
          }
          this.input = null;
          this.showError = true;
          this.isLoading = false;
          throw error;
        }
      );
    }
  }
}
