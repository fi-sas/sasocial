import { TestBed } from '@angular/core/testing';

import { ConfigsService } from './configs.service';
import { ElectronService } from './electron.service';

describe('ConfigsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ElectronService
    ]
  }));

  it('should be created', () => {
    const service: ConfigsService = TestBed.get(ConfigsService);
    expect(service).toBeTruthy();
  });
});
