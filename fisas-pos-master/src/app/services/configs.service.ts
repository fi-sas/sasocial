import { set, get } from 'lodash';
import { ElectronService } from './electron.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigsService {

  constructor(
    private electronService: ElectronService
  ) { 
  }


  isSettingsValid(): boolean {

    if(!this.electronService.settings.has('pos.device_id')) {
      return false;
    }

    if(!this.electronService.settings.has('service.entity_id') && 
    !this.electronService.settings.has('service.account_id')) {
      return false;
    }
    
    if(!this.electronService.settings.has('service.service_canteen_id')) {
      return false;
    }

    if(!this.electronService.settings.has('service.service_bar_id')) {
      return false;
    }
    


    return true;
  }

  set(key: string, value: any) {
    if (this.electronService.settings) {
    return this.electronService.settings.set(key, value);
    } else {
      return null;
    }
  }

  get(key: string) {
    if (this.electronService.settings) {
    return this.electronService.settings.get(key);
    } else {
      return null;
    }
  }

  has(key: string) {
    if (this.electronService.settings) {
    return this.electronService.settings.has(key);
    } else {
      return null;
    }
  }

}
