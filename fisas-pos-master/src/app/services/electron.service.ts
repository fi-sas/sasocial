import { Injectable } from '@angular/core';
import * as ElectronSettings from 'electron-settings';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote } from 'electron';
import * as childProcess from 'child_process';

declare global {
  interface Window {
    require: any;
    process: any;
  }
}

@Injectable()
export class ElectronService {
  settings: typeof ElectronSettings;

  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;

  constructor() {
    // Conditional imports

    if (this.isElectron()) {
      this.settings = window.require('electron-settings');
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;

      this.childProcess = window.require('child_process');
    }
  }

  isElectron = () => {
    return window && window.process && window.process.type;
  };
}
