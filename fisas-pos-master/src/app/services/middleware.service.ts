import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MiddlewareListenerService {

  isSocketOn = false;

  constructor(
    private _router: Router,
    _socket: Socket) {

    _socket.on('connect', () => {
      this.isSocketOn = true;

    });
    _socket.on('disconnect', () => {
      this.isSocketOn = false;
    });

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean> {
    if (this.isSocketOn || state.url === '/install') {
      return of(true);
    } else {
      this.navigateToUnavailable();
      return of(false);
    }
  }

  gotoDashboard(): void {
    this._router.navigate(['/']);
  }

  navigateToUnavailable() {
    this._router.navigate(['unavailable']);
  }

}
