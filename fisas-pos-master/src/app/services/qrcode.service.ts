import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  _qrcode: Subject<string> = new Subject<string>();

  constructor(private socket: Socket) {
    this.socket.on("QRCODE_READ", (value) => {
      this._qrcode.next(value);
    });
   
  }

  getQrcodeObservable(): Observable<string> {
    return this._qrcode.asObservable().pipe(
      filter(prd => prd !== null)
    );
  }

}