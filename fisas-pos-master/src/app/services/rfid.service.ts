import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class RfidService {

  _rfid: Subject<string> = new Subject<string>();

  constructor(private socket: Socket) {
    this.socket.on("RFID_READ", (value) => {
      this._rfid.next(value);
    });
   
  }

  getRfidObservable(): Observable<string> {
    return this._rfid.asObservable().pipe(
      filter(prd => prd !== null)
    );
  }

}