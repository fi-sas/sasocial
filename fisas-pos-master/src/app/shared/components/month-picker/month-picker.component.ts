import { Component, EventEmitter, OnInit , Output } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'fisas-month-picker',
  templateUrl: './month-picker.component.html',
  styleUrls: ['./month-picker.component.less']
})
export class MonthPickerComponent implements OnInit {

  @Output() monthSelected = new EventEmitter();
  date = new Date();

  constructor() { }

  ngOnInit() {}

  forwardMonth() {
      this.date =  moment(this.date).add(1, 'month').toDate();
      this.monthSelected.emit(this.date);
  }

  backMonth() {
      this.date =  moment(this.date).subtract(1, 'month').toDate();
      this.monthSelected.emit(this.date);
  }

}
