import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'fisas-no-connection',
  templateUrl: './no-connection.component.html',
  styleUrls: ['./no-connection.component.less']
})
export class NoConnectionComponent implements OnInit {

  // Network connection
  connection = false;
  constructor(private modal: NzModalRef) { }

  ngOnInit() {}

  /**
   * @description check for network connection
   */
  verifyConnection() {
      this.connection = true;
      setTimeout(() => {
          if (navigator.onLine) {
              this.modal.destroy();
          }
          this.connection = false;
      }, 1000);
  }

}
