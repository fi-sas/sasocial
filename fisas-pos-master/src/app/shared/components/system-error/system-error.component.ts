import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'fisas-system-error',
  templateUrl: './system-error.component.html',
  styleUrls: ['./system-error.component.less']
})
export class SystemErrorComponent implements OnInit {

  constructor(private modal: NzModalRef) { }

  ngOnInit() {}

  handleCancel(): void {
      this.modal.destroy();
  }
}
