import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NoConnectionComponent } from '../components/no-connection/no-connection.component';
import { SystemErrorComponent } from '../components/system-error/system-error.component';

@Injectable()
export class ApisErrorsInterceptor implements  HttpInterceptor{

  errorsCodes = [
      {
          code: 1015175744,
          message: 'Não é possivel anular a senha a hora limite expirou'
      }
  ];


  constructor(private modalService: NzModalService,
              private messageService: NzMessageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let noError = false;

        if(request.headers.has('no-error')) {
            noError = true;
            request = request.clone({ headers: request.headers.delete('no-error') });
        }

        return next.handle(request).pipe(catchError(error => {

            if (error instanceof HttpErrorResponse) {
                if (!navigator.onLine) {
                    this.modalService.create({
                        nzWidth: 300,
                        nzMaskClosable: false,
                        nzClosable: false,
                        nzContent: NoConnectionComponent,
                        nzFooter: [{
                            label: 'Verificar',
                            onClick: (componentInstance) => {
                                componentInstance.verifyConnection();
                            }
                        }]
                    });
                } else {
                    const httpErrorCode = error.status;
                    switch (httpErrorCode) {
                        case 503:
                            this.modalService.create({
                                nzWidth: 416,
                                nzMaskClosable: false,
                                nzClosable: true,
                                nzContent: SystemErrorComponent,
                                nzFooter: null
                            });
                            break;
                        case 500:
                            this.modalService.create({
                                nzWidth: 416,
                                nzMaskClosable: false,
                                nzClosable: true,
                                nzContent: SystemErrorComponent,
                                nzFooter: null
                            });
                            break;
                        case 0:
                            this.modalService.create({
                                nzWidth: 416,
                                nzMaskClosable: false,
                                nzClosable: true,
                                nzContent: SystemErrorComponent,
                                nzFooter: null
                            });
                            break;
                        case 404:
                            break;
                        case 400:
                            if (error.error.errors && !noError) {
                                error.error.errors.map(er => {
                                    let erFind = this.errorsCodes.find( erCode => erCode.code === er.code);
                                    if (erFind !== undefined) {
                                        this.messageService.error(erFind.message);
                                    } else {
                                        this.messageService.error('Ocorreu um erro no processo');
                                    }
                                });
                            }
                            break;
                        default:
                    }
                }
                return throwError(error);
            }
        }));
    }
}


