import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'boldSubStr'})
export class BoldSubStrPipe implements PipeTransform {
  transform(value: string, sub?: string): string {
    var re = new RegExp(sub, 'gi');
    if (!re) { return value; }
    return value.replace(re, '<b>' + sub + '</b>');
  }
}
