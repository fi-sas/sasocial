
import { Pipe, PipeTransform } from '@angular/core';
import { FiConfigurator } from '../../libs/configurator/configurator.service';

@Pipe({
  name: 'translation'
})
export class TranslationPipe implements PipeTransform {

  default_lang = 3;
  constructor(
    private configs: FiConfigurator,
  ) {
    this.default_lang = this.configs.getOption('DEFAULT_LANG_ID');
  }

  transform(translations: any, language_id: number): any {
    if(translations) {
      return translations.find(translation => translation.language_id === (language_id ? language_id : this.default_lang));
    }
    return '';
  }
}
