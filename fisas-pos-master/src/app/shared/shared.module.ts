import { POS_CONFIGURATION } from './../app.config';
import { OPTIONS_TOKEN } from './../libs/configurator/configurator.service';
import { FiConfiguratorModule } from './../libs/configurator/configurator.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterModule, RouterOutlet } from '@angular/router';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule} from '@angular/common';
/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';

import { NZ_I18N, pt_PT  } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BoldSubStrPipe } from './pipes/bold-sub-str.pipe';
import { MonthPickerComponent } from './components/month-picker/month-picker.component';
import { NoConnectionComponent } from './components/no-connection/no-connection.component';
import { SystemErrorComponent } from './components/system-error/system-error.component';

//import { MatKeyboardModule } from '@ngx-material-keyboard/core';
import { MatKeyboardModule } from './../keyboard/keyboard.module';
import { MatButtonModule } from "@angular/material";
import { TranslationPipe } from './pipes/translation.pipe';

registerLocaleData(pt);

@NgModule({
  declarations: [
    BoldSubStrPipe,
    MonthPickerComponent,
    NoConnectionComponent,
    SystemErrorComponent,
    TranslationPipe
  ],
  imports: [
    FiConfiguratorModule,
    CommonModule,
    RouterModule,
    NgZorroAntdModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    MatButtonModule,
    MatKeyboardModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt' },
    { provide: NZ_I18N, useValue: pt_PT },
    { provide: OPTIONS_TOKEN, useValue: POS_CONFIGURATION },
  ],
  exports: [
    FiConfiguratorModule,
    RouterOutlet,
    NgZorroAntdModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BoldSubStrPipe,
    MonthPickerComponent,
    MatKeyboardModule,
    TranslationPipe
  ],
  entryComponents: [
    NoConnectionComponent,
    SystemErrorComponent
  ]
})
export class SharedModule { }
