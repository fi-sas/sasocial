export const gateway_url = 'https://sasocial.sas.ipvc.pt/api';
export const environment = {
  production: true,
  version : require('../../package.json').version,
  organization_theme: 'ipvc',
  auth_api_url: gateway_url + '/authorization',
  conf_api_url: gateway_url + '/configuration',
  media_api_url: gateway_url + '/media',
  media_url: gateway_url + '/media',
  communication_api_url: gateway_url + '/communication',
  alimentation_api_url: gateway_url + '/alimentation',
  //alimentation_api_url: 'http://0.0.0.0:7080/api',
  bus_api_url: gateway_url + '/bus',
  private_accommodation_api_url: gateway_url + '/private_accommodation',
  accommodation_api_url: gateway_url + '/accommodation',
  infrastructure_api_url: gateway_url + '/infrastructure',
  u_bike_api_url: gateway_url + '/u_bike',
  sport_api_url: gateway_url + '/sport',
  calendar_api_url: gateway_url + '/calendar',
  current_account_api_url: gateway_url + '/current_account',
  payments_api_url: gateway_url + '/payments',
  notifications_api_url: gateway_url + '/notifications',
  gamification_api_url: gateway_url + '/gamification',
  social_support_api_url: gateway_url + '/social_support',
  reports_api_url: gateway_url + '/reports'
};
