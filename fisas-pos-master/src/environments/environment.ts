// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const gateway_url = 'http://localhost:4206';
//export const gateway_url = 'http://localhost';
export const environment = {
  production: false,
  version : require('../../package.json').version,
  organization_theme: 'ipvc',
  auth_api_url: gateway_url + '/api/authorization',
  conf_api_url: gateway_url + '/api/configuration',
  media_api_url: gateway_url + '/api/media',
  media_url: gateway_url + '/api/media',
  communication_api_url: gateway_url + '/api/communication',
  alimentation_api_url: gateway_url + '/api/alimentation',
  // alimentation_api_url: 'http://0.0.0.0:7080/api/v1',
  bus_api_url: gateway_url + '/api/bus',
  private_accommodation_api_url: gateway_url + '/api/private_accommodation',
  accommodation_api_url: gateway_url + '/api/accommodation',
  infrastructure_api_url: gateway_url + '/api/infrastructure',
  u_bike_api_url: gateway_url + '/api/u_bike',
  sport_api_url: gateway_url + '/api/sport',
  calendar_api_url: gateway_url + '/api/calendar',
  current_account_api_url: gateway_url + '/api/current_account',
  payments_api_url: gateway_url + '/api/payments',
  notifications_api_url: gateway_url + '/api/notifications',
  gamification_api_url: gateway_url + '/api/gamification',
  social_support_api_url: gateway_url + '/api/social_support',
  reports_api_url: gateway_url + '/api/reports'
};

export const AppConfig = {
  production: false,
  environment: 'LOCAL',
};
