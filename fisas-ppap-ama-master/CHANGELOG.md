## [1.3.5](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.3.4...v1.3.5) (2021-08-19)


### Bug Fixes

* **paygate:** increase timeout to 9000ms request AMA api ([2e35952](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/2e35952cfa3140ed690bf0830b7bf2a37fc4ede7))

## [1.3.4](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.3.3...v1.3.4) (2021-08-19)


### Bug Fixes

* **callback:** logs for trace callback ([742bbc3](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/742bbc3ffd5e16a7959b7844ae807ff56d50c8ef))

## [1.3.3](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.3.2...v1.3.3) (2021-08-10)


### Bug Fixes

* **paygate:** undefined device object ([a96eb9c](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/a96eb9cbecdcb66ea40d3cce1b38b4c2e1ae4bcb))

## [1.3.2](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.3.1...v1.3.2) (2021-07-12)


### Bug Fixes

* callback to confirm ([7c54135](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/7c541356e278786229c10af0c85421d48d7e11ee))

## [1.3.1](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.3.0...v1.3.1) (2021-06-02)


### Bug Fixes

* **paygate:** change callbackMBRef verb to POST ([7974c78](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/7974c78a4cb3dec74197000dfd657bbfaedc82a7))

# [1.3.0](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.2.4...v1.3.0) (2021-06-02)


### Features

* **paygate:** add callback endpoint ([b8f1949](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/b8f194979bbce1b06ff90df3ce13b12210b0cc11))

## [1.2.4](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.2.3...v1.2.4) (2021-05-30)


### Bug Fixes

* catch wrong var name ([9fb6d44](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/9fb6d44bb913da1f75b8451bfea700637391b6be))
* debug service response ([cd11e64](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/cd11e64bd921793470a8e68655b1c132add5941b))

## [1.2.3](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.2.2...v1.2.3) (2021-05-19)


### Bug Fixes

* **envs:** add paygate examples ([4a78a5e](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/4a78a5efca2a8bb5e4507d77f46e676639645bdc))

## [1.2.2](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.2.1...v1.2.2) (2021-05-18)


### Bug Fixes

* remove this from axios instance ([f04dc80](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/f04dc806615f63ddbb0fe508898043382d87983a))

## [1.2.1](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.2.0...v1.2.1) (2021-05-18)


### Bug Fixes

* **paygate:** remove debug scope ([4b603ea](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/4b603ea0ca644e4c50374de2e5fb147db45615ab))

# [1.2.0](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.5...v1.2.0) (2021-05-18)


### Features

* **paygate:** add version 2 endpoints ([0e0bc50](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/0e0bc50275cc899e123ba69425726b270963a03d))

## [1.1.5](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.4...v1.1.5) (2021-03-24)


### Bug Fixes

* **checkTransaction:** validate ctx.meta.device ([d54d3e1](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/d54d3e1a5a2b326cff242c5b97b23a882d1804a4))

## [1.1.4](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.3...v1.1.4) (2021-03-12)


### Bug Fixes

* **setMultibankParameters:** malformed param ([c139372](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/c1393720d570a79f08d9f86494cc830e731842bd))

## [1.1.3](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.2...v1.1.3) (2021-03-12)


### Bug Fixes

* **megaepayment:** remove debug info ([cb32566](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/cb325661b1afe74a3568a266f322d7987eb77cc0))

## [1.1.2](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.1...v1.1.2) (2021-03-10)


### Bug Fixes

* **debug:** add debug for expiration date ([c1fae30](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/c1fae30abf25ac33afb291353e1a152eae0b5053))

## [1.1.1](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.1.0...v1.1.1) (2021-03-10)


### Bug Fixes

* **createRefMB:** fix config key name expiration ([58048cd](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/58048cd98ee63f1e3caac97415e2e3b5255740a4))

# [1.1.0](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0...v1.1.0) (2021-03-10)


### Features

* **createRefMB:** set expiration date config ([3583637](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/35836370ec5507986341d15c2000b851e3ba8c6c))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **createMBRef:** fix response ([b8b1c13](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/b8b1c13cdaad7d651f441fea4548d2fe44c2ffb3))
* **createMBRef:** params string convert ([ce7f5df](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/ce7f5dfa13ee1fb834287904ee395d37bba6d982))
* **debug:** remove debug fake actions ([0ecb87d](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/0ecb87d2dec04b05916042db86ba95132e5e6c67))
* minor fixes ([19b61bd](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/19b61bd68d494a61268b94395b310f61dbdc3896))
* **lint:** minor fixes ([0c12afa](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/0c12afa0eaf8d2ab70d4002e409ddb685a587a04))
* change actions visibility to public ([5bee173](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/5bee173ea04e43bdc43de0c46fad35317790aa79))
* minor fixes ([f5b6360](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/f5b636038690c5609216e6c997a7dc97896a81f0))
* minor fixes ([d808dc4](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/d808dc49e1a719e3f29ef433f301ff295d43a0dd))
* **lint:** minor fixes ([e6b1b82](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/e6b1b8274e933b57d0313d4d6045493bdced0123))
* **megaepayment:** rename params ([dfa327b](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/dfa327b45ac0f7e1130fcd19f2e7e2aab44febb4))
* minor fixes ([808e9a4](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/808e9a45e4f6eaa183d9e23a179ccbed3cd386a0))
* **stack:** add extra_hosts to stack ([5c6af37](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/5c6af37ca2c43da91fa659047d2801062a85824c))


### Features

* **megaepayment:** add generate MBRef action ([89141cb](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/89141cbdb28bcef9d925f59bbc091111dea1be0e))
* **refMB:** check webservice availability ([bd58e93](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/bd58e93ea28bbcd2769675c6a570acb16b8dad1f))
* **test:** add dummy unit test ([37689ac](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/37689ac2684cb064bbb753456d9ba1a9c0f95aae))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-23)


### Bug Fixes

* **debug:** remove debug fake actions ([0ecb87d](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/0ecb87d2dec04b05916042db86ba95132e5e6c67))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-19)


### Features

* **refMB:** check webservice availability ([bd58e93](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/bd58e93ea28bbcd2769675c6a570acb16b8dad1f))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-02-12)


### Bug Fixes

* **createMBRef:** params string convert ([ce7f5df](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/ce7f5dfa13ee1fb834287904ee395d37bba6d982))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-02-12)


### Bug Fixes

* minor fixes ([19b61bd](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/19b61bd68d494a61268b94395b310f61dbdc3896))
* **createMBRef:** fix response ([b8b1c13](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/b8b1c13cdaad7d651f441fea4548d2fe44c2ffb3))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-02-12)


### Bug Fixes

* **lint:** minor fixes ([0c12afa](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/0c12afa0eaf8d2ab70d4002e409ddb685a587a04))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-02-12)


### Bug Fixes

* change actions visibility to public ([5bee173](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/5bee173ea04e43bdc43de0c46fad35317790aa79))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-02-11)


### Bug Fixes

* minor fixes ([f5b6360](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/f5b636038690c5609216e6c997a7dc97896a81f0))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-02-11)


### Bug Fixes

* minor fixes ([d808dc4](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/d808dc49e1a719e3f29ef433f301ff295d43a0dd))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-02-11)


### Bug Fixes

* **megaepayment:** rename params ([dfa327b](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/dfa327b45ac0f7e1130fcd19f2e7e2aab44febb4))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-02-11)


### Bug Fixes

* minor fixes ([808e9a4](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/808e9a45e4f6eaa183d9e23a179ccbed3cd386a0))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-02-10)


### Bug Fixes

* **lint:** minor fixes ([e6b1b82](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/e6b1b8274e933b57d0313d4d6045493bdced0123))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-ppap-ama/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-02-10)


### Features

* **megaepayment:** add generate MBRef action ([89141cb](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/89141cbdb28bcef9d925f59bbc091111dea1be0e))
* **test:** add dummy unit test ([37689ac](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/37689ac2684cb064bbb753456d9ba1a9c0f95aae))

# 1.0.0-rc.1 (2021-02-10)


### Bug Fixes

* **stack:** add extra_hosts to stack ([5c6af37](https://gitlab.com/fi-sas/fisas-ppap-ama/commit/5c6af37ca2c43da91fa659047d2801062a85824c))
