FROM node:8.11.3

ARG NODE_ENV

RUN mkdir /app

WORKDIR /app


COPY certs/ppap.qualidade.cer /etc/ssl/certs/ppap.qualidade.cer

RUN echo "Europe/Lisbon" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata



COPY package.json package-lock.json ./

RUN npm i

COPY . .

RUN chmod +x ./docker-entrypoint.sh

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]

CMD npm run start:$NODE_ENV
