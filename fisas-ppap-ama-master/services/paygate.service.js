"use strict";

require("dotenv").config();
const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

const { MoleculerClientError } = require("moleculer").Errors;

const axios = require("axios");
//TODO REMOVE ME
//const HttpsProxyAgent = require("https-proxy-agent");

module.exports = {
	name: "ppap.paygate",

	/**
	 * Settings
	 */
	settings: {

		PPAP_AMA_DEBUG: process.env.PPAP_AMA_WCF_DEBUG == "true",

		MULTIBANK_EXPIRATION_DATE_AVAILABILITY:
			process.env.MULTIBANK_EXPIRATION_DATE_AVAILABILITY == "true",
		MULTIBANK_EXPIRATION_IN_DAYS: parseInt(process.env.MULTIBANK_EXPIRATION_IN_DAYS),

		PAYGATE_ENDPOINT_REST: process.env.PAYGATE_ENDPOINT_REST,
		PAYGATE_MERCHANT_CODE: process.env.PAYGATE_MERCHANT_CODE,
		PAYGATE_ACCESS_TOKEN: process.env.PAYGATE_ACCESS_TOKEN,
		PAYGATE_USERNAME: process.env.PAYGATE_USERNAME,
		PAYGATE_PASSWORD: process.env.PAYGATE_PASSWORD,
		PAYGATE_CALLBACK: process.env.PAYGATE_CALLBACK,

	},

	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		ping: {

			authorization: false,
			authentication: false,

			visibility: "published",
			rest: "GET /ping",
			params: {},
			async handler(ctx) {

				if (this.settings.PPAP_AMA_DEBUG) {
					this.logger.info("## DBG: /PAYGATE ping (heartbeat) ##");
					this.logger.info(`  - device: ${ctx.meta.device}`);
					//this.logger.info(`    # uuid: ${ctx.meta.device.uuid || "--"}`);
					//this.logger.info(`    # name/type: ${ctx.meta.device.name || "--"}/${ctx.meta.device.type}`);
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}
				this.logger.info(`  - this.IS_LOCAL: ${this.IS_LOCAL}`);

				if (this.IS_LOCAL)
					return "pong"; // <== ##### DEBUG FOR LOCAL MS PPAP/AMA - Fake RefMB #####

				return axios.get(`${this.settings.PAYGATE_ENDPOINT_REST}/Ping`)
					.then(() => {
						return "pong";
					})
					.catch((err) => {
						this.logger.info("### /PAYGATE Ping Error ###");
						this.logger.info(err);

						throw new MoleculerClientError(
							"Service not available, please try later",
							503,
							"SERVICE_UNAVAILABLE",
							{},
						);
					});
			},
		},/*
		debug_createMBRef: {
			visibility: "published",
			rest: "POST /debug_createMBRef",
			params: {
				payment_id: { type: "string" },
				description: { type: "string" },
				user_id: { type: "string", convert: true },
				amount: { type: "string", convert: true },
				expiration_at: { type: "date", convert: true, optional: true },
			},

			async handler(ctx) {
				//throw new MoleculerClientError("No transaction found", 404, "TRANSACTION_NOT_FOUND_ERROR", {});

				this.logger.info(" ########### [ppap.megaepayment].debug.0 #############:");
				this.logger.info(this.settings.wsConfig);

				let _expirationDate = new Date();
				_expirationDate.setDate(_expirationDate.getDate() + this.settings.MULTIBANK_EXPIRATION_IN_DAYS);
				if (this.settings.MULTIBANK_EXPIRATION_DATE_AVAILABILITY) {
					if (!ctx.params.expiration_at) {
						ctx.params.expiration_at = _expirationDate;
					}
				}


				this.logger.info();
				this.logger.info(" ########### [ppap.megaepayment].debug.1:");
				this.logger.info(ctx.params);
				this.logger.info();

				let _resultPaygate = {
					"URL": "string",
					"cardType": "string",
					"cardBIN": "string",
					"cardExpireYYMM": 0,
					"REFMB_Entity": "77006",
					"REFMB_SubEntity": "string",
					"REFMB_Reference": "000000009",
					"REFMB_Amount": ctx.params.amount,
					"DUC_Entity": "string",
					"DUC_Amount": 0,
					"DUC_Reference": "string",
					"AuthorizationCode": "string",
					"QRCodeImage": "string",
					"OperationProgressStatus": 0,
					"Success": true,
					"InProgress": true,
					"ReturnCode": "string",
					"ShortReturnMessage": "string",
					"LongReturnMessage": "string",
					"SessionToken": "string",
					"TransactionID": "string",
					"PaymentID": ctx.params.payment_id,
					"WalletID": "string"
				};

				let debugResult = Object.assign(
					{},
					{
						TransactionId: _.get(_resultPaygate, ["TransactionID"]),
						Amount: _.get(_resultPaygate, ["REFMB_Amount"]),
						RepeatableOperation: false,
						EntityNumber: _.get(_resultPaygate, ["REFMB_Entity"]),
						Reference: _.get(_resultPaygate, ["REFMB_Reference"]),
						ExpirationDateAvailability: this.settings.MULTIBANK_EXPIRATION_DATE_AVAILABILITY,
						ExpirationDate: ctx.params.expiration_at.toISOString(),
						ActiveExpiration: false,
					}
				);

				this.logger.info();
				this.logger.info("    - [ppap.paygate].debugResult.2:");
				this.logger.info(debugResult);
				this.logger.info();

				return debugResult;
			},
		},
		debug_checkTransaction: {
			visibility: "published",
			rest: "GET /debugCheckTransaction/:transaction_id",
			params: {
				payment_ref: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				let transactionResult = {
					TransactionID: "1177450",
					Date: "2021-02-11T17:41:37.117+00:00",
					StateID: "6",
					StateName: "CAPTURADO",
					MerchantName: "IPVC",
					MerchantID: "198",
					UserID: "3",
					OrderID: "be1f22af-5db1-43c4-984b-278b343a4a92",
					OrderDesc: "Carregamento não imediato",
					LastOperationAmount: "25",
					OperationName: "CAPTURA",
					GroupID: null,
					TransactionBalance: "25",
					ApplicationID: "169",
					ApplicationName: "IPVC",
				};

				transactionResult = Object.assign(transactionResult, { StateID: "8", StateName: "CANCELADO", OperationName: "CANCELAMENTO", TransactionBalance: "0" });


				return transactionResult;
			},
		},*/

		checkTransaction: {
			visibility: "public",
			//rest: "GET /checkTransaction/:transaction_id",
			//scope: "ppap-ama:megaepayment:check-transaction",
			params: {
				payment_ref: { type: "string" },
			},
			async handler(ctx) {

				//TODO: REMOVE ME [just for DEBUG]
				//const agent = new HttpsProxyAgent("http://127.0.0.1:8080");


				if (this.settings.PPAP_AMA_DEBUG) {
					this.logger.info("");
					this.logger.info("## DBG: /checkTransaction ##");
					if (ctx.meta.device) {
						this.logger.info(`  - device: ${ctx.meta.device}`);
						//this.logger.info(`    # uuid: ${ctx.meta.device.uuid || "--"}`);
						//this.logger.info(`    # name/type: ${ctx.meta.device.name || "--"}/${ctx.meta.device.type}`);
					}
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}

				const payload = {
					"ACCESS_TOKEN": this.settings.PAYGATE_ACCESS_TOKEN,
					"MERCHANT_CODE": this.settings.PAYGATE_MERCHANT_CODE,
					"PAYMENT_REF": ctx.params.payment_ref
				};

				let authPaygate = "Basic " + Buffer.from(this.settings.PAYGATE_USERNAME + ":" + this.settings.PAYGATE_PASSWORD).toString("base64");
				const instance = axios.create({
					baseURL: `${this.settings.PAYGATE_ENDPOINT_REST}/`,
					timeout: 9000,
					headers: { "Authorization": authPaygate },
					//httpsAgent: agent, //DEBUG
				});
				return instance.post("GetPaymentStatus", payload)
					.then(res => {
						if (this.settings.PPAP_AMA_DEBUG) {
							this.logger.info("  - checkTransaction - GetPaymentStatus (AMA/Paygate endpoint) - res.data");
							this.logger.info(res.data);
						}
						return res.data;
					}).catch((err) => {
						this.logger.error("  - checkTransaction - GetPaymentStatus (AMA/Paygate endpoint) - err");
						this.logger.error(err);
						return err;
					});

			},
		},

		createMBRef: {

			authorization: false,
			authentication: false,

			visibility: "public",
			//rest: "POST /createMBRef",
			//scope: "ppap-ama:megaepayment:create-mbref",
			params: {
				payment_ref: { type: "string" },
				description: { type: "string" },
				user_id: { type: "string", convert: true },
				amount: { type: "string", convert: true },
				expiration_at: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {

				//TODO REMOVE ME
				//const agent = new HttpsProxyAgent("http://127.0.0.1:8080");
				//ctx.meta = {
				//	device: {
				//		uuid: "123456789-123456789",
				//		name: "dummy-device-name",
				//		type: "BO"
				//	}
				//};
				// ---


				if (this.settings.PPAP_AMA_DEBUG) {
					this.logger.info("## DBG: /createMBRef ##");
					this.logger.info(`  - device: ${ctx.meta.device}`);
					//this.logger.info(`    # uuid: ${ctx.meta.device.uuid || "--"}`);
					//this.logger.info(`    # name/type: ${ctx.meta.device.name || "--"}/${ctx.meta.device.type}`);
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}

				let _expirationDate = new Date();
				_expirationDate.setDate(
					_expirationDate.getDate() + this.settings.MULTIBANK_EXPIRATION_IN_DAYS,
				);
				if (this.settings.MULTIBANK_EXPIRATION_DATE_AVAILABILITY) {
					if (!ctx.params.expiration_at) {
						ctx.params.expiration_at = _expirationDate;
					}
				}

				let payload = {
					"REFMB_START_DATE": new Date(),
					//"REFMB_END_DATE": ctx.params.expiration_at,
					"REFMB_MIN_AMOUNT": ctx.params.amount,
					"REFMB_MAX_AMOUNT": ctx.params.amount,
					"ACCESS_TOKEN": this.settings.PAYGATE_ACCESS_TOKEN,
					"MERCHANT_CODE": this.settings.PAYGATE_MERCHANT_CODE,
					/*"CLIENT_NAME": "string",
					"PHONE_NUMBER": "string",
					"ADDRESS_LINE_1": "string",
					"ADDRESS_LINE_2": "string",
					"CITY": "string",
					"POSTAL_CODE": "4900000",
					"STATE": "string",*/
					"COUNTRY_CODE": "PT",
					/*"EMAIL": "tony.wolfango@xpertgo.pt",*/
					"LANGUAGE": "PT",
					"PAYMENT_REF": ctx.params.payment_ref,
					"TRANSACTION_DESC": ctx.params.description,
					"CURRENCY": "EUR",
					"TOTAL_AMOUNT": ctx.params.amount,
					"CALLBACK_SERVER_URL": this.settings.PAYGATE_CALLBACK,
					"CALLBACK_SERVER_PARMS": [
						{
							"key": "payment_ref",
							"value": ctx.params.payment_ref
						}
					]
				};

				if (this.settings.MULTIBANK_EXPIRATION_DATE_AVAILABILITY) {

					Object.assign(payload, { REFMB_END_DATE: ctx.params.expiration_at });

				}

				let authPaygate = "Basic " + Buffer.from(this.settings.PAYGATE_USERNAME + ":" + this.settings.PAYGATE_PASSWORD).toString("base64");
				const instance = axios.create({
					baseURL: `${this.settings.PAYGATE_ENDPOINT_REST}/`,
					timeout: 5000,
					headers: { "Authorization": authPaygate },

					//httpsAgent: agent, //TODO REMOVE ME
				});
				return instance.post("REFMBPayment", payload)
					.then(res => {

						this.logger.info("createMBRef - REFMBPayment res.data:");
						this.logger.info(res.data);

						let result = Object.assign(
							{},
							{
								TransactionId: _.get(res.data, ["TransactionID"]),
								Amount: _.get(res.data, ["REFMB_Amount"]),
								RepeatableOperation: false,
								EntityNumber: _.get(res.data, ["REFMB_Entity"]),
								Reference: _.get(res.data, ["REFMB_Reference"]),
								ExpirationDateAvailability: this.settings.MULTIBANK_EXPIRATION_DATE_AVAILABILITY,
								ExpirationDate: ctx.params.expiration_at.toISOString(),
								ActiveExpiration: true,

								Success: _.get(res.data, ["Success"]),
								InProgress: _.get(res.data, ["InProgress"]),
								OperationProgressStatus: _.get(res.data, ["OperationProgressStatus"]),
								ReturnCode: _.get(res.data, ["ReturnCode"]),
								ShortReturnMessage: _.get(res.data, ["ShortReturnMessage"]),
								LongReturnMessage: _.get(res.data, ["LongReturnMessage"]),
							}
						);

						return result;
					})
					.catch((err) => {
						this.logger.error("createMBRef - REFMBPayment error:");
						this.logger.error(err);
						return err;
					});



			},
		},
		callback: {

			authorization: false,
			authentication: false,

			visibility: "published",
			rest: "POST /callbackMBRef",
			params: {},
			async handler(ctx) {

				if (this.settings.PPAP_AMA_DEBUG) {
					this.logger.info("");
					this.logger.info("## DBG: /PAYGATE callbackMBRef ##");
					this.logger.info("  - params:");
					this.logger.info(ctx.params);
				}
				return ctx.call("payments.payments.confirm", { id: ctx.params.payment_ref })
					.then(res => {

						if (this.settings.PPAP_AMA_DEBUG) {
							this.logger.info("## DBG: /PAYGATE callbackMBRef / payments.payments.confirm ##");
							this.logger.info("  - params:");
							this.logger.info(ctx.params);
							this.logger.info("  - res:");
							this.logger.info(res);
						}

						return Promise.resolve();
					})
					.catch((err) => {
						this.logger.error(" - callbackMBRef error:");
						this.logger.error(err);
						return err;
					});

			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

		this.IS_LOCAL = process.env.NODE_ENV == "development";

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
