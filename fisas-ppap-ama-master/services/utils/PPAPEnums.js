/* eslint-disable no-unused-vars */
module.exports = {

	OperationName: {
		AUTORIZACAO: "AUTORIZACAO",
		CONF_AUTORIZACAO: "CONF_AUTORIZACAO",
		CANCELAMENTO: "CANCELAMENTO",
		CAPTURA: "CAPTURA",
		DEVOLUCAO: "DEVOLUCAO",
		TRANSFERENCIA: "TRANSFERENCIA",
		CONF_TRANSFERENCIA: "CONF_TRANSFERENCIA",
		ROLLBACK: "CANCELAMENTO",
		ERRO: "ERRO"
	},
	CorrectionOperationType: {
		SemTipo: "SemTipo",
		CorreccaoMontante: "CorreccaoMontante",
		CorreccaoDataCobranca: "CorreccaoDataCobranca",
		CorreccaoNIF: "CorreccaoNIF",
		AnulacaoFalsaCobranca: "AnulacaoFalsaCobranca",
		AnulacaoCobranca: "AnulacaoCobranca",
		CorreccaoBalcaoCobrador: "CorreccaoBalcaoCobrador"
	},

	MonextLanguageCode: {
		pt: "pt",
		eng: "eng",
		spa: "spa",
		ita: "ita",
		fra: "fra",
		ger: "ger"
	},
	CvvAvailability: {
		Available: "Available",
		Unreadable: "Unreadable",
		NotAvailable: "NotAvailable",
		Undefined: "Undefined"
	}

};
