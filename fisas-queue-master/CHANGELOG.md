# [1.16.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.15.2...v1.16.0) (2022-01-05)


### Bug Fixes

* exclude called tickets ([330f94f](https://gitlab.com/fi-sas/fisas-queue/commit/330f94f1a6288dcba1378714ffca7354ea30c03f))


### Features

* notifications: approach, faster, close desk ([9ac7de6](https://gitlab.com/fi-sas/fisas-queue/commit/9ac7de6a9a833190f3c192707ac03d8375859fc4))

## [1.15.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.15.1...v1.15.2) (2021-12-23)


### Bug Fixes

* sort recall tickets ([b8184eb](https://gitlab.com/fi-sas/fisas-queue/commit/b8184eb126deea76c215cedd2f5414c4ee5ebbaa))

## [1.15.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.15.0...v1.15.1) (2021-12-23)


### Bug Fixes

* manage tickets ([4cd2853](https://gitlab.com/fi-sas/fisas-queue/commit/4cd285356c81648aba77e5b99c3d8feaa7458d50))
* manage tickets ([dee6988](https://gitlab.com/fi-sas/fisas-queue/commit/dee698802a65b91f909b737857161d0e2934a1fe))

# [1.15.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.14.1...v1.15.0) (2021-12-23)


### Features

* manage and recall tickets ([09cfd6e](https://gitlab.com/fi-sas/fisas-queue/commit/09cfd6ef317cc1193f227d4bdfb67cb7c1d4e647))

## [1.14.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.14.0...v1.14.1) (2021-12-20)


### Bug Fixes

* change get_open_subjects_by_service ([7e010d0](https://gitlab.com/fi-sas/fisas-queue/commit/7e010d0b31b90f863e3df703e37a7f1431d903a4))

# [1.14.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.13.0...v1.14.0) (2021-12-20)


### Bug Fixes

* add and remove subjects from desk ([15003a0](https://gitlab.com/fi-sas/fisas-queue/commit/15003a072c981f2f532a9e5ae158a1ec370fcee7))


### Features

* add and remove subjects from desk ([267ffc8](https://gitlab.com/fi-sas/fisas-queue/commit/267ffc88ca3c94321efecf6e3bd79cd601796422))
* add and remove subjects from desk ([87abd75](https://gitlab.com/fi-sas/fisas-queue/commit/87abd752d8386318689d16b238566b108f932b08))

# [1.13.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.12.0...v1.13.0) (2021-12-07)


### Features

* queue without desk and operator ([bb0d6fd](https://gitlab.com/fi-sas/fisas-queue/commit/bb0d6fde72c089967fa5e51a113fd71e5f628ae9))

# [1.12.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.11.3...v1.12.0) (2021-12-02)


### Features

* ordering and time zone ([9c69e66](https://gitlab.com/fi-sas/fisas-queue/commit/9c69e6613d2415cb8a6be15f224abcf4cc17fea3))
* ordering and time zone ([525c420](https://gitlab.com/fi-sas/fisas-queue/commit/525c420a04fcae1a51d7951dc9bcdbf3506ce004))

## [1.11.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.11.2...v1.11.3) (2021-11-25)


### Bug Fixes

* **tickets:** sanitizePagination ([0f939b6](https://gitlab.com/fi-sas/fisas-queue/commit/0f939b61f1ea7b17143a79bdb01143889cde01aa))

## [1.11.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.11.1...v1.11.2) (2021-11-24)


### Bug Fixes

* **tickets:** remove knex raw function ([54adb61](https://gitlab.com/fi-sas/fisas-queue/commit/54adb61e62761ed1f286d7b16daa661a8ac0896d))

## [1.11.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.11.0...v1.11.1) (2021-11-24)


### Bug Fixes

* replace db to getDB function ([67dead6](https://gitlab.com/fi-sas/fisas-queue/commit/67dead6b3ea2904396c558c10f21074c1d70a320))

# [1.11.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.10.0...v1.11.0) (2021-11-24)


### Features

* numeração marcações ([4ba2ce2](https://gitlab.com/fi-sas/fisas-queue/commit/4ba2ce2e1f7afe37c1fa779f654b0b94c339aaea))

# [1.10.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.9.0...v1.10.0) (2021-11-04)


### Features

* tickets from just one day ([9733a3d](https://gitlab.com/fi-sas/fisas-queue/commit/9733a3dfaed181706f7e8a2535cb365eebdff2a9))

# [1.9.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.8.0...v1.9.0) (2021-11-03)


### Features

* expire, just one day, message, ticket_code ([00fa22d](https://gitlab.com/fi-sas/fisas-queue/commit/00fa22d7f935b146dd64ea2fa2f0f550eba544a7))

# [1.8.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.7.0...v1.8.0) (2021-10-29)


### Features

* bugs correction for queue ([e7b9b8c](https://gitlab.com/fi-sas/fisas-queue/commit/e7b9b8c1ee037ccdf28cf9a03b7df921572fe5f7))

# [1.7.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.9...v1.7.0) (2021-10-21)


### Features

* bugs correction for queue ([64a14e4](https://gitlab.com/fi-sas/fisas-queue/commit/64a14e4c48243e942692488806c0a6c3fc150353))

## [1.6.9](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.8...v1.6.9) (2021-09-10)


### Bug Fixes

* **tickets:** remove scope ([f565635](https://gitlab.com/fi-sas/fisas-queue/commit/f565635849708385264f131b8be1db336056f96e))

## [1.6.8](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.7...v1.6.8) (2021-09-10)


### Bug Fixes

* **tickets:** remove cancel permission ([6e671b3](https://gitlab.com/fi-sas/fisas-queue/commit/6e671b31f9c4a704fc0424449dea9970e8e55c8e))

## [1.6.7](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.6...v1.6.7) (2021-09-10)


### Bug Fixes

* **tickets:** change scope of cancel ticket ([39c6bf5](https://gitlab.com/fi-sas/fisas-queue/commit/39c6bf5140f80964fa90efb799769b8e9890e4bc))

## [1.6.6](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.5...v1.6.6) (2021-09-03)


### Bug Fixes

* **tickets:** fix subject closed error ([8103d0e](https://gitlab.com/fi-sas/fisas-queue/commit/8103d0e9c7e61c7499c67f1b10c06ef1be018593))

## [1.6.5](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.4...v1.6.5) (2021-09-01)


### Bug Fixes

* **services:** clear service cache on change withrelated (subject) ([9d008ee](https://gitlab.com/fi-sas/fisas-queue/commit/9d008eeee394b745641641696c0d7c0d77718dcc))

## [1.6.4](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.3...v1.6.4) (2021-08-31)


### Bug Fixes

* **tickets:** fix cron to clear tickets and tv cache ([ac2f5c7](https://gitlab.com/fi-sas/fisas-queue/commit/ac2f5c7ff85ff6188ba692c1d542ea72df5c3c60))

## [1.6.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.2...v1.6.3) (2021-08-25)


### Bug Fixes

* **dashboard:** send to dashboard only tickets of the user ([5bcf47f](https://gitlab.com/fi-sas/fisas-queue/commit/5bcf47f13938b0d5650cd7785586c70c8340236d))

## [1.6.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.1...v1.6.2) (2021-06-18)


### Bug Fixes

* **tickets:** problem lint ([b99488c](https://gitlab.com/fi-sas/fisas-queue/commit/b99488c3f40d36c936b744373655e1986f5bbf31))

## [1.6.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.6.0...v1.6.1) (2021-06-18)


### Bug Fixes

* **tickets:** return the correct link fields on response ([27c959e](https://gitlab.com/fi-sas/fisas-queue/commit/27c959e126a5641fdeadfff4fa3e29302f22641d))

# [1.6.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.5.1...v1.6.0) (2021-06-17)


### Features

* **tickets:** add job for cancel tickets ([78ddc59](https://gitlab.com/fi-sas/fisas-queue/commit/78ddc595d0339841ffc7cabcb5bbf9c4dac86f2a))

## [1.5.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.5.0...v1.5.1) (2021-06-17)


### Bug Fixes

* **tickets_history:** reserve changes last commit ([3f7ec97](https://gitlab.com/fi-sas/fisas-queue/commit/3f7ec9755af6b510d0c839d6a606b2ca321b4dde))

# [1.5.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.4.3...v1.5.0) (2021-06-17)


### Bug Fixes

* **tickets:** on transfer unattended tickets dont update avg time ([e26f833](https://gitlab.com/fi-sas/fisas-queue/commit/e26f8335e2037782febad793dc61391d4cce7575))


### Features

* **desks:** close all desk form operator ([809fbbf](https://gitlab.com/fi-sas/fisas-queue/commit/809fbbf9ee6ee4886843c552d5d57837328729f3))
* **migration:** update ticket_history ([c8dcec5](https://gitlab.com/fi-sas/fisas-queue/commit/c8dcec5b19c9488283d1357f39199ab0797f23e3))
* **subject_desk_operator:** call close desks from operator ([6d901f0](https://gitlab.com/fi-sas/fisas-queue/commit/6d901f0904a99747772a872aba4ece4d7231650f))
* **ticket_history:** add ticket_code ([ea698c1](https://gitlab.com/fi-sas/fisas-queue/commit/ea698c10a1950fd34d020eea07aadcec86c5fd67))
* **tickets:** add validation attend ticket ([c983615](https://gitlab.com/fi-sas/fisas-queue/commit/c983615166ee47ed14a21af4b8c18109abda9d2f))

## [1.4.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.4.2...v1.4.3) (2021-06-16)


### Bug Fixes

* **tickets_history:** change call_time to call_times field ([0cb2fb0](https://gitlab.com/fi-sas/fisas-queue/commit/0cb2fb02403b7194a5d12872444413a2cfabc167))

## [1.4.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.4.1...v1.4.2) (2021-06-16)


### Bug Fixes

* **tickets:** validation, scope and update list ([468d235](https://gitlab.com/fi-sas/fisas-queue/commit/468d23505c0e18ce4a068e063e4410d742a4dc0d))

## [1.4.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.4.0...v1.4.1) (2021-06-15)


### Bug Fixes

* **desk:** update stop service ([0f76a7c](https://gitlab.com/fi-sas/fisas-queue/commit/0f76a7cc07c3a1615d6e1f7a7c948eab7540af91))
* **subject_desk_operator:** endpoint update remove validation ([0916e4d](https://gitlab.com/fi-sas/fisas-queue/commit/0916e4db2d57b8293c9f8f4de1b382d0abc1fd43))

# [1.4.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.7...v1.4.0) (2021-06-14)


### Bug Fixes

* **desk:** update fields from association ([723da80](https://gitlab.com/fi-sas/fisas-queue/commit/723da808da8c065a2dc014f230a4fbf2fe58befb))
* **subject_desk_operator:** update fields ([ea6f9e3](https://gitlab.com/fi-sas/fisas-queue/commit/ea6f9e3200a9d40f1db4790c92e206c1c98d6ea3))


### Features

* **migration:** remove fields from subject_desk_operator ([e8fa7df](https://gitlab.com/fi-sas/fisas-queue/commit/e8fa7df703768dc1fa7277176b39fbdb042bf4c9))

## [1.3.7](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.6...v1.3.7) (2021-06-08)


### Bug Fixes

* **tickets_history:** optimaze endpoint and update problem with dates ([c03503f](https://gitlab.com/fi-sas/fisas-queue/commit/c03503f0513b9d0be25bc870b4718ace76dd74ac))

## [1.3.6](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.5...v1.3.6) (2021-06-08)


### Bug Fixes

* **subject_desk_operator:** add clear cache and withrelated ([d4fccc9](https://gitlab.com/fi-sas/fisas-queue/commit/d4fccc9bd3efbd0b8c877f7d222179488041a0af))
* **subjects:** add clear cache ([3cf6577](https://gitlab.com/fi-sas/fisas-queue/commit/3cf657795925b76f53a2c1efb3e75b3f7eaad445))

## [1.3.5](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.4...v1.3.5) (2021-06-07)


### Bug Fixes

* **tickets:** queue return empty array on no subjects ([333e886](https://gitlab.com/fi-sas/fisas-queue/commit/333e8864db95b709ffbbfe858ed63a1f898cbd14))

## [1.3.4](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.3...v1.3.4) (2021-06-07)


### Bug Fixes

* **desk_operator:** add query by active ([da491e1](https://gitlab.com/fi-sas/fisas-queue/commit/da491e12405adeb13235e6e3a6532e161c8a1c92))

## [1.3.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.2...v1.3.3) (2021-05-17)


### Bug Fixes

* **services:** change name of scope ([2e9bd37](https://gitlab.com/fi-sas/fisas-queue/commit/2e9bd37fdbba66bfa3962f27fc0bac21a213e42c))

## [1.3.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.1...v1.3.2) (2021-05-05)


### Bug Fixes

* **services:** filter TV services to return only services with subjects ([dec92ba](https://gitlab.com/fi-sas/fisas-queue/commit/dec92ba1c66ce3336cc84eec09a43305a8a45f70))

## [1.3.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.3.0...v1.3.1) (2021-03-24)


### Bug Fixes

* **services:** add missing query active param ([578c83e](https://gitlab.com/fi-sas/fisas-queue/commit/578c83e6d325077cd73a3a65d13dacc97ea3005e))

# [1.3.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.2.2...v1.3.0) (2021-03-17)


### Bug Fixes

* **services:** create service_groups on create service ([984b501](https://gitlab.com/fi-sas/fisas-queue/commit/984b5011a39e842fd6227409286524e7caabcf4e))
* **subject_desk_operator:** replace find for count and fix subject params ([a56d993](https://gitlab.com/fi-sas/fisas-queue/commit/a56d9937bc2a37e034ba423fd8ce00b265caa309))
* **tickets:** add type to user tickets validations ([b515f5d](https://gitlab.com/fi-sas/fisas-queue/commit/b515f5de7caeda9b57a47709692938620bbc59b5))
* **tickets:** fix callTicket notification and event ([caa8020](https://gitlab.com/fi-sas/fisas-queue/commit/caa802019a567fd81912264b9f5e1a3b1741e56f))


### Features

* **services:** associate service to group of devices and custom response by device ([1a8f140](https://gitlab.com/fi-sas/fisas-queue/commit/1a8f1405171c8bce0d834f5b3325b907d448206e))
* **tickets:** fix callticket calls wrong ticket code ([48c3b00](https://gitlab.com/fi-sas/fisas-queue/commit/48c3b00f0a1c9f3287fb81e2acec50f4d0b05496))

## [1.2.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.2.1...v1.2.2) (2021-03-15)


### Bug Fixes

* **ticket:** add more data to socket event ([c549539](https://gitlab.com/fi-sas/fisas-queue/commit/c549539f4b3e6fc1d306972ec6eb6712bd569e91))

## [1.2.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.2.0...v1.2.1) (2021-03-15)


### Bug Fixes

* **tickets:** change update to patch ([aa0b288](https://gitlab.com/fi-sas/fisas-queue/commit/aa0b2887993dc9e652dfb4ef81e044a836a67991))

# [1.2.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.1.3...v1.2.0) (2021-03-15)


### Bug Fixes

* **migration:** fix migration ticket_code field name ([a5958b5](https://gitlab.com/fi-sas/fisas-queue/commit/a5958b584233cbe8b386282f27c72688a2482e93))
* **services:** remove cache ([a8a0050](https://gitlab.com/fi-sas/fisas-queue/commit/a8a00509cd3d423160b685130f5b50f1d195f70c))
* **subject:** add missing code to subject translations ([c22d410](https://gitlab.com/fi-sas/fisas-queue/commit/c22d41035a53c6d3cc1eb434f69529838fb9808d))
* **subjects:** fix subject translations and fix lint warns ([380f2f9](https://gitlab.com/fi-sas/fisas-queue/commit/380f2f9967868a10bc85f10abcbe72373f9b9ec2))
* **tickets:** add concatenated ticket code field ([f427b83](https://gitlab.com/fi-sas/fisas-queue/commit/f427b833d7b21cb1d82b84e14a70d0d6c04b4da3))


### Features

* **services:** add service translations ([1cd5363](https://gitlab.com/fi-sas/fisas-queue/commit/1cd5363a989a096c005a72b76fdc6420f3a1172c))
* **tickets:** add function to concact ticket code/number ([db8d63a](https://gitlab.com/fi-sas/fisas-queue/commit/db8d63a3630dd0637728b5c645db8df55a58315c))

## [1.1.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.1.2...v1.1.3) (2021-03-15)


### Bug Fixes

* **tickets:** change next ticket socket event ([a70a964](https://gitlab.com/fi-sas/fisas-queue/commit/a70a9647498b719163162fe0957d0be474faa1f5))

## [1.1.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.1.1...v1.1.2) (2021-03-12)


### Bug Fixes

* **services:** add flag to subject closed and if user already have a ticket ([fde6c34](https://gitlab.com/fi-sas/fisas-queue/commit/fde6c34f9a9e94fc9fde6ea57f34cc6bb15f7645))
* **services:** add missing cache keys on list ([d159c64](https://gitlab.com/fi-sas/fisas-queue/commit/d159c647695b576f31a84410581916411abc1f80))
* **subjects:** change virtual fields to withRelateds ([81cb411](https://gitlab.com/fi-sas/fisas-queue/commit/81cb41113be8b9bda1e7aace828e5d23c06fe391))
* **tickets:** add pagination to ticket queue list ([13c05be](https://gitlab.com/fi-sas/fisas-queue/commit/13c05be198dc537f5f8f60fd4f87ad607cfe0f03))

## [1.1.1](https://gitlab.com/fi-sas/fisas-queue/compare/v1.1.0...v1.1.1) (2021-03-11)


### Bug Fixes

* minor fixs ([c0e77bc](https://gitlab.com/fi-sas/fisas-queue/commit/c0e77bc46bae1f51d8b679a0b14e26ba028eed7c))

# [1.1.0](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0...v1.1.0) (2021-03-09)


### Bug Fixes

* **tickets:** add empty valdiation to endpoint for WP widget ([e886f42](https://gitlab.com/fi-sas/fisas-queue/commit/e886f42215fb7c4baa7b22f997d534c280766c66))


### Features

* **tickets:** add endpoint for WP widget information ([7badf86](https://gitlab.com/fi-sas/fisas-queue/commit/7badf8602a4c066d0551a1a0d3d8087e0f0b8160))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **subjects:** add tickets in queue field to subjects ([c8b5106](https://gitlab.com/fi-sas/fisas-queue/commit/c8b5106934d7c7c03d330e3dfa7714730aca73ac))
* **subjects:** change list to find on subjects hooks ([a720f1f](https://gitlab.com/fi-sas/fisas-queue/commit/a720f1f242063037bb8cd0a8520c3d6aa243c1d3))
* **tickets:** add origin device type generic ([7cdf3c1](https://gitlab.com/fi-sas/fisas-queue/commit/7cdf3c1322296a28cb1a71790dd40d5afeea9f58))
* **tickets:** add pagination to /user requests ([12db706](https://gitlab.com/fi-sas/fisas-queue/commit/12db70644d48854c774b0601fd0fd0a204fdb0bb))
* **tickets:** small fixs on get user active tickets ([4e23d9a](https://gitlab.com/fi-sas/fisas-queue/commit/4e23d9af0a2286bafc4a970c42c5660c63ed6e5e))
* **tickets:** subject withRelated ([39b89ed](https://gitlab.com/fi-sas/fisas-queue/commit/39b89edd6461d6d57a86428b9e3cff185a9e016a))
* **tickets_history:** fiz paginator on /user ([770b9b5](https://gitlab.com/fi-sas/fisas-queue/commit/770b9b5223f28f1b48c1126d1fcab461c0474bdc))
* **tickets_history:** remove unused code ([44e82f1](https://gitlab.com/fi-sas/fisas-queue/commit/44e82f1b5ff323023b57e7be21bfd9c9efb63c5a))
* bug in create desk_operator ([e49f9e7](https://gitlab.com/fi-sas/fisas-queue/commit/e49f9e7e355cddcee0a1add7b5959f79054bf5db))
* bug in create/update operator and sameuserdesk ([2285de5](https://gitlab.com/fi-sas/fisas-queue/commit/2285de5569d7e07903d326ee4eb1d0998f7cda5a))
* bug in get associations by user and desk ([c17defd](https://gitlab.com/fi-sas/fisas-queue/commit/c17defd2f0bbab037606ff25e05cea2211792786))
* bugs and validations ([d5c016a](https://gitlab.com/fi-sas/fisas-queue/commit/d5c016a31e098f77c8c769c4d3cefa440f831873))
* bugs in reports hours ([4802898](https://gitlab.com/fi-sas/fisas-queue/commit/48028981041e8711e24add5f64ac54ae945a8d8d))
* errors on returning user ticket history ([9b4007e](https://gitlab.com/fi-sas/fisas-queue/commit/9b4007e75df5221ba1da79ace23cf561f871d273))
* fix bug in create association and init desk ([0e64e5e](https://gitlab.com/fi-sas/fisas-queue/commit/0e64e5e4e84e03f59caf91ac7118e41e6ac1cdac))
* fix seed problem ([5726887](https://gitlab.com/fi-sas/fisas-queue/commit/5726887bdf07e395198398a6c92dfccd9f4ac8cd))
* get ticket list error ([08c3661](https://gitlab.com/fi-sas/fisas-queue/commit/08c3661246bc9c6734f1f53112966e699ee2b18a))
* getPercentagesAllServices returning NaM ([1a470f4](https://gitlab.com/fi-sas/fisas-queue/commit/1a470f4f01bce3da7f061ed8f832c0f1475f585b))
* hours and avgs ([0241777](https://gitlab.com/fi-sas/fisas-queue/commit/024177716f7aec3394514b2493c0a5d0c26ad585))
* minor fix ([107a8dc](https://gitlab.com/fi-sas/fisas-queue/commit/107a8dc974e8e84e3e5d0a88b8eb8825f6f84860))
* name of response ([d09637e](https://gitlab.com/fi-sas/fisas-queue/commit/d09637efc636d4f568b8f42c60a37c1deda674b6))
* resolve problem on begin/end attendence ([bb3f856](https://gitlab.com/fi-sas/fisas-queue/commit/bb3f856ab084a52a7053f84a66a13d5a13a89409))
* ticket call error ([73f2af8](https://gitlab.com/fi-sas/fisas-queue/commit/73f2af8a8d754b2ac431d88fc2bc2ea0326a169b))
* ticket history bugs ([f84ecda](https://gitlab.com/fi-sas/fisas-queue/commit/f84ecda3a31565050a13100a5b56b49fedadf832))
* **desk_operators:** return user ([64a0eef](https://gitlab.com/fi-sas/fisas-queue/commit/64a0eef9381eec53355abf0b45938a8f5ed1ec2f))
* **desks:** convert dates ([150b7bd](https://gitlab.com/fi-sas/fisas-queue/commit/150b7bd01107b8b6dd2a12a3124d147f51262fef))
* **knex:** fix knex unexpected close connection ([35a0b13](https://gitlab.com/fi-sas/fisas-queue/commit/35a0b13e3d16e95d6009654fa8295bff88f4cf57))
* **schedules:** add pattern validation on schedule hours ([1d46fc8](https://gitlab.com/fi-sas/fisas-queue/commit/1d46fc8e21322b1a615a0e76b5bc80b22dbdad4c))
* **services:** change actions to published ([2b0b097](https://gitlab.com/fi-sas/fisas-queue/commit/2b0b0979162432b6eb1af41a8e96c31cd5ef6d5d))
* **services:** service withrelated ([20f1062](https://gitlab.com/fi-sas/fisas-queue/commit/20f1062d6dd625e27c850fd705e14aaa003c31f0))
* **tickets:** set ticket origin from meta.device ([3b99164](https://gitlab.com/fi-sas/fisas-queue/commit/3b991642c6ef219f2071a97060ee7febac9b1d19))
* **tickets:** set ticket(sequential) origin  from meta.device ([c3d5ee2](https://gitlab.com/fi-sas/fisas-queue/commit/c3d5ee2dc7928ab6155cbaf57eeb584de8eddab3))
* name of services ([1f1db03](https://gitlab.com/fi-sas/fisas-queue/commit/1f1db03a496562163549dc44aef6dd2cf2ebe728))
* remove seed file from migrations ([84e78b4](https://gitlab.com/fi-sas/fisas-queue/commit/84e78b4c58936854e0bd80d47887236517228334))


### Features

*  average working time ([ca8465f](https://gitlab.com/fi-sas/fisas-queue/commit/ca8465f24555271d5bd8fbb1ca804411a0ec0b2d))
* add appointment reason ([e38c03a](https://gitlab.com/fi-sas/fisas-queue/commit/e38c03ab9af3f6a4875eaf3ef73fd93a70920aee))
* add boilerplate files ([d3dd823](https://gitlab.com/fi-sas/fisas-queue/commit/d3dd823657ee2199508e867e92116a23357eaf74))
* add broadcast event to ticket call operation ([efe859e](https://gitlab.com/fi-sas/fisas-queue/commit/efe859e4cdc77ad612cb3563213bf70f4085343c))
* add endpoint get associations in operation by operator id ([824f05a](https://gitlab.com/fi-sas/fisas-queue/commit/824f05ac3e9960925cd8b0ec504b539282699ffe))
* add get avg between call and attendance ([5f1ddd4](https://gitlab.com/fi-sas/fisas-queue/commit/5f1ddd44f1633c05bb3a2fa2b48dbd7f11a80b26))
* add method for get info tickets by subjects ([dfb6e09](https://gitlab.com/fi-sas/fisas-queue/commit/dfb6e0912075aac9deeb58e97b81f12532c219ac))
* add method to start service and method to stop service ([daaae15](https://gitlab.com/fi-sas/fisas-queue/commit/daaae1508b1ccb247f7a5ce005d3b9ce7b2d4832))
* add migration file and initialservice ([3bde469](https://gitlab.com/fi-sas/fisas-queue/commit/3bde4691d28a326bd0664fa381705aad09cc42ef))
* add new reports ([e1d0906](https://gitlab.com/fi-sas/fisas-queue/commit/e1d0906490545233dce1ad162c38e03910a99140))
* add news requests for report ([3c0051c](https://gitlab.com/fi-sas/fisas-queue/commit/3c0051c089efeaf20221be83e35880823e5fac8f))
* add service subject_desk ([d74eba9](https://gitlab.com/fi-sas/fisas-queue/commit/d74eba9d08f170f7068b9de4acae06c21a72d081))
* add services and CRUDS ([0d40f04](https://gitlab.com/fi-sas/fisas-queue/commit/0d40f043c46323f6cd541481a8859d791b4c3ff8))
* add services create and update methods ([4ce4e85](https://gitlab.com/fi-sas/fisas-queue/commit/4ce4e85cad4eab8ad0e039b52df9218697a0dee6))
* add the method to get the desk according to a subject ([db9af82](https://gitlab.com/fi-sas/fisas-queue/commit/db9af821eea81215e50261cd7f4f591c49ad627a))
* add ticket default withRelated ([124c874](https://gitlab.com/fi-sas/fisas-queue/commit/124c874f26baf6163baa149b26d2f9af42c01150))
* add ticket service ([b91c1ac](https://gitlab.com/fi-sas/fisas-queue/commit/b91c1acb55a368564b5fe7bd64336b47b88ab7b6))
* add total number of tickets forwarded ([64b6354](https://gitlab.com/fi-sas/fisas-queue/commit/64b6354ad99b7511a4e95a24e3a6f44680cdaa78))
* add validation desk exist ([56b436e](https://gitlab.com/fi-sas/fisas-queue/commit/56b436e122393ae487773c9361a31894dfc2e54c))
* add validation operating exist ([df2b3aa](https://gitlab.com/fi-sas/fisas-queue/commit/df2b3aa8eda491d1be415889e2ff35157d42aac3))
* add waiting time between call and attendence ([c4d00d3](https://gitlab.com/fi-sas/fisas-queue/commit/c4d00d33f66a5ca9e882e466f7f2df14e992899d))
* add/get (withRelated) association between desk,operator,subject ([6fb9dd4](https://gitlab.com/fi-sas/fisas-queue/commit/6fb9dd41cb6a879ebac8017a632d21658c448730))
* all tickets attended out of target ([c1d5488](https://gitlab.com/fi-sas/fisas-queue/commit/c1d5488dc73ff21eb563f2398b96a212c23589f0))
* average of pauses ([0adff6d](https://gitlab.com/fi-sas/fisas-queue/commit/0adff6d98f7811e2e243a89b7c7021b9ee9335cf))
* configurations ([5d124d4](https://gitlab.com/fi-sas/fisas-queue/commit/5d124d48aa26b3e1b06d616860ce92990d995a79))
* create and update user_schedule by user ([a1aa069](https://gitlab.com/fi-sas/fisas-queue/commit/a1aa069ce3a4caaf9d09872a2877041ea78e6455))
* get subjects by tv_id ([202f8b0](https://gitlab.com/fi-sas/fisas-queue/commit/202f8b0da1aa492a8dfa6a0b38bbab8a04227bbe))
* management associations ([83caeb4](https://gitlab.com/fi-sas/fisas-queue/commit/83caeb4fe7b9ba20aea9375f126a34a5df1e0927))
* number of tickets digital and papel, average time waiting attended ([ce36d24](https://gitlab.com/fi-sas/fisas-queue/commit/ce36d242f1051ce5319ac450e390939acccc794f))
* percentage of tickest attended for services and tickets recovered ([263d92b](https://gitlab.com/fi-sas/fisas-queue/commit/263d92bfaac969c2277a905800a4821155469f55))
* recalculate estimated_time on return list of user tickets ([59a1c1a](https://gitlab.com/fi-sas/fisas-queue/commit/59a1c1a143389e5ebe23ce0a35c5eb7f81be342e))
* recalculate ticket estimated time ([efaed0c](https://gitlab.com/fi-sas/fisas-queue/commit/efaed0ce05e69c62f6449d8c4a3681fb1cda7eb5))
* services crud and TV's services ([97e41f6](https://gitlab.com/fi-sas/fisas-queue/commit/97e41f6c6b0108e24aded21a3431215dff43bc87))
* subjects and schedules crud ([92ff31f](https://gitlab.com/fi-sas/fisas-queue/commit/92ff31fde5cfe6e94ca99516cd08ec4ec492a9ec))
* ticket attendence and ticket queue transfer ([c12c579](https://gitlab.com/fi-sas/fisas-queue/commit/c12c579b5f0d6e7d8f676751f7193c29b2028e95))
* validation ticket with subject schedule ([4bbc5aa](https://gitlab.com/fi-sas/fisas-queue/commit/4bbc5aaf3b9ca67226da43b5d71f33748c2f587f))
* **subjects:** create, update, get valid hours to appointments methods ([52a5080](https://gitlab.com/fi-sas/fisas-queue/commit/52a508056629bb8622349c307039748a2e940f2f))
* **tickets:** create and cancel  ticket/apointment ([d8c927a](https://gitlab.com/fi-sas/fisas-queue/commit/d8c927a1e0bcc4cf0981330cafef0e6224b301ee))


### Reverts

* add missing file on last commit ([3f2b456](https://gitlab.com/fi-sas/fisas-queue/commit/3f2b4565fe8b667bab7ee93a42ff2492506d84d5))
* revert changes on knexfile in last commit ([7f39504](https://gitlab.com/fi-sas/fisas-queue/commit/7f39504bc0f2b1063d072ceff260d03605416c2f))

# [1.0.0-rc.33](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.32...v1.0.0-rc.33) (2021-02-25)


### Bug Fixes

* **tickets:** add origin device type generic ([7cdf3c1](https://gitlab.com/fi-sas/fisas-queue/commit/7cdf3c1322296a28cb1a71790dd40d5afeea9f58))

# [1.0.0-rc.32](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-02-19)


### Bug Fixes

* **tickets:** subject withRelated ([39b89ed](https://gitlab.com/fi-sas/fisas-queue/commit/39b89edd6461d6d57a86428b9e3cff185a9e016a))

# [1.0.0-rc.31](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-02-19)


### Bug Fixes

* **subjects:** add tickets in queue field to subjects ([c8b5106](https://gitlab.com/fi-sas/fisas-queue/commit/c8b5106934d7c7c03d330e3dfa7714730aca73ac))
* **subjects:** change list to find on subjects hooks ([a720f1f](https://gitlab.com/fi-sas/fisas-queue/commit/a720f1f242063037bb8cd0a8520c3d6aa243c1d3))

# [1.0.0-rc.30](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-02-15)


### Bug Fixes

* **tickets_history:** fiz paginator on /user ([770b9b5](https://gitlab.com/fi-sas/fisas-queue/commit/770b9b5223f28f1b48c1126d1fcab461c0474bdc))
* **tickets_history:** remove unused code ([44e82f1](https://gitlab.com/fi-sas/fisas-queue/commit/44e82f1b5ff323023b57e7be21bfd9c9efb63c5a))

# [1.0.0-rc.29](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-02-15)


### Bug Fixes

* **tickets:** add pagination to /user requests ([12db706](https://gitlab.com/fi-sas/fisas-queue/commit/12db70644d48854c774b0601fd0fd0a204fdb0bb))

# [1.0.0-rc.28](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-02-12)


### Bug Fixes

* **tickets:** small fixs on get user active tickets ([4e23d9a](https://gitlab.com/fi-sas/fisas-queue/commit/4e23d9af0a2286bafc4a970c42c5660c63ed6e5e))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-01-19)


### Features

* add appointment reason ([e38c03a](https://gitlab.com/fi-sas/fisas-queue/commit/e38c03ab9af3f6a4875eaf3ef73fd93a70920aee))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-01-19)


### Bug Fixes

* resolve problem on begin/end attendence ([bb3f856](https://gitlab.com/fi-sas/fisas-queue/commit/bb3f856ab084a52a7053f84a66a13d5a13a89409))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-01-15)


### Features

* add ticket default withRelated ([124c874](https://gitlab.com/fi-sas/fisas-queue/commit/124c874f26baf6163baa149b26d2f9af42c01150))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-01-15)


### Features

* recalculate estimated_time on return list of user tickets ([59a1c1a](https://gitlab.com/fi-sas/fisas-queue/commit/59a1c1a143389e5ebe23ce0a35c5eb7f81be342e))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-01-15)


### Bug Fixes

* errors on returning user ticket history ([9b4007e](https://gitlab.com/fi-sas/fisas-queue/commit/9b4007e75df5221ba1da79ace23cf561f871d273))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-01-15)


### Features

* add waiting time between call and attendence ([c4d00d3](https://gitlab.com/fi-sas/fisas-queue/commit/c4d00d33f66a5ca9e882e466f7f2df14e992899d))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-01-15)


### Bug Fixes

* ticket history bugs ([f84ecda](https://gitlab.com/fi-sas/fisas-queue/commit/f84ecda3a31565050a13100a5b56b49fedadf832))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-01-15)


### Bug Fixes

* minor fix ([107a8dc](https://gitlab.com/fi-sas/fisas-queue/commit/107a8dc974e8e84e3e5d0a88b8eb8825f6f84860))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-01-13)


### Bug Fixes

* get ticket list error ([08c3661](https://gitlab.com/fi-sas/fisas-queue/commit/08c3661246bc9c6734f1f53112966e699ee2b18a))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-01-13)


### Bug Fixes

* ticket call error ([73f2af8](https://gitlab.com/fi-sas/fisas-queue/commit/73f2af8a8d754b2ac431d88fc2bc2ea0326a169b))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-01-07)


### Bug Fixes

* bugs and validations ([d5c016a](https://gitlab.com/fi-sas/fisas-queue/commit/d5c016a31e098f77c8c769c4d3cefa440f831873))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-01-05)


### Bug Fixes

* name of response ([d09637e](https://gitlab.com/fi-sas/fisas-queue/commit/d09637efc636d4f568b8f42c60a37c1deda674b6))


### Features

* add endpoint get associations in operation by operator id ([824f05a](https://gitlab.com/fi-sas/fisas-queue/commit/824f05ac3e9960925cd8b0ec504b539282699ffe))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-01-05)


### Bug Fixes

* hours and avgs ([0241777](https://gitlab.com/fi-sas/fisas-queue/commit/024177716f7aec3394514b2493c0a5d0c26ad585))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-01-05)


### Bug Fixes

* bugs in reports hours ([4802898](https://gitlab.com/fi-sas/fisas-queue/commit/48028981041e8711e24add5f64ac54ae945a8d8d))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-01-05)


### Bug Fixes

* **tickets:** set ticket(sequential) origin  from meta.device ([c3d5ee2](https://gitlab.com/fi-sas/fisas-queue/commit/c3d5ee2dc7928ab6155cbaf57eeb584de8eddab3))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-01-04)


### Bug Fixes

* **tickets:** set ticket origin from meta.device ([3b99164](https://gitlab.com/fi-sas/fisas-queue/commit/3b991642c6ef219f2071a97060ee7febac9b1d19))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-01-04)


### Bug Fixes

* getPercentagesAllServices returning NaM ([1a470f4](https://gitlab.com/fi-sas/fisas-queue/commit/1a470f4f01bce3da7f061ed8f832c0f1475f585b))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2020-12-22)


### Bug Fixes

* **schedules:** add pattern validation on schedule hours ([1d46fc8](https://gitlab.com/fi-sas/fisas-queue/commit/1d46fc8e21322b1a615a0e76b5bc80b22dbdad4c))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2020-12-21)


### Bug Fixes

* **knex:** fix knex unexpected close connection ([35a0b13](https://gitlab.com/fi-sas/fisas-queue/commit/35a0b13e3d16e95d6009654fa8295bff88f4cf57))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2020-12-21)


### Bug Fixes

* **desk_operators:** return user ([64a0eef](https://gitlab.com/fi-sas/fisas-queue/commit/64a0eef9381eec53355abf0b45938a8f5ed1ec2f))


### Features

* add method for get info tickets by subjects ([dfb6e09](https://gitlab.com/fi-sas/fisas-queue/commit/dfb6e0912075aac9deeb58e97b81f12532c219ac))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2020-12-21)


### Bug Fixes

* **services:** change actions to published ([2b0b097](https://gitlab.com/fi-sas/fisas-queue/commit/2b0b0979162432b6eb1af41a8e96c31cd5ef6d5d))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-12-18)

### Bug Fixes

- bug in create/update operator and sameuserdesk ([2285de5](https://gitlab.com/fi-sas/fisas-queue/commit/2285de5569d7e07903d326ee4eb1d0998f7cda5a))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-12-18)

### Bug Fixes

- bug in get associations by user and desk ([c17defd](https://gitlab.com/fi-sas/fisas-queue/commit/c17defd2f0bbab037606ff25e05cea2211792786))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-18)

### Bug Fixes

- bug in create desk_operator ([e49f9e7](https://gitlab.com/fi-sas/fisas-queue/commit/e49f9e7e355cddcee0a1add7b5959f79054bf5db))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-18)

### Bug Fixes

- **desks:** convert dates ([150b7bd](https://gitlab.com/fi-sas/fisas-queue/commit/150b7bd01107b8b6dd2a12a3124d147f51262fef))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-queue/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-17)

### Bug Fixes

- **services:** service withrelated ([20f1062](https://gitlab.com/fi-sas/fisas-queue/commit/20f1062d6dd625e27c850fd705e14aaa003c31f0))

# 1.0.0-rc.1 (2020-12-16)

### Bug Fixes

- fix bug in create association and init desk ([0e64e5e](https://gitlab.com/fi-sas/fisas-queue/commit/0e64e5e4e84e03f59caf91ac7118e41e6ac1cdac))
- fix seed problem ([5726887](https://gitlab.com/fi-sas/fisas-queue/commit/5726887bdf07e395198398a6c92dfccd9f4ac8cd))
- name of services ([1f1db03](https://gitlab.com/fi-sas/fisas-queue/commit/1f1db03a496562163549dc44aef6dd2cf2ebe728))
- remove seed file from migrations ([84e78b4](https://gitlab.com/fi-sas/fisas-queue/commit/84e78b4c58936854e0bd80d47887236517228334))

### Features

- average working time ([ca8465f](https://gitlab.com/fi-sas/fisas-queue/commit/ca8465f24555271d5bd8fbb1ca804411a0ec0b2d))
- add boilerplate files ([d3dd823](https://gitlab.com/fi-sas/fisas-queue/commit/d3dd823657ee2199508e867e92116a23357eaf74))
- add broadcast event to ticket call operation ([efe859e](https://gitlab.com/fi-sas/fisas-queue/commit/efe859e4cdc77ad612cb3563213bf70f4085343c))
- add get avg between call and attendance ([5f1ddd4](https://gitlab.com/fi-sas/fisas-queue/commit/5f1ddd44f1633c05bb3a2fa2b48dbd7f11a80b26))
- add method to start service and method to stop service ([daaae15](https://gitlab.com/fi-sas/fisas-queue/commit/daaae1508b1ccb247f7a5ce005d3b9ce7b2d4832))
- add migration file and initialservice ([3bde469](https://gitlab.com/fi-sas/fisas-queue/commit/3bde4691d28a326bd0664fa381705aad09cc42ef))
- add new reports ([e1d0906](https://gitlab.com/fi-sas/fisas-queue/commit/e1d0906490545233dce1ad162c38e03910a99140))
- add news requests for report ([3c0051c](https://gitlab.com/fi-sas/fisas-queue/commit/3c0051c089efeaf20221be83e35880823e5fac8f))
- add service subject_desk ([d74eba9](https://gitlab.com/fi-sas/fisas-queue/commit/d74eba9d08f170f7068b9de4acae06c21a72d081))
- add services and CRUDS ([0d40f04](https://gitlab.com/fi-sas/fisas-queue/commit/0d40f043c46323f6cd541481a8859d791b4c3ff8))
- add services create and update methods ([4ce4e85](https://gitlab.com/fi-sas/fisas-queue/commit/4ce4e85cad4eab8ad0e039b52df9218697a0dee6))
- add the method to get the desk according to a subject ([db9af82](https://gitlab.com/fi-sas/fisas-queue/commit/db9af821eea81215e50261cd7f4f591c49ad627a))
- add ticket service ([b91c1ac](https://gitlab.com/fi-sas/fisas-queue/commit/b91c1acb55a368564b5fe7bd64336b47b88ab7b6))
- add total number of tickets forwarded ([64b6354](https://gitlab.com/fi-sas/fisas-queue/commit/64b6354ad99b7511a4e95a24e3a6f44680cdaa78))
- add validation desk exist ([56b436e](https://gitlab.com/fi-sas/fisas-queue/commit/56b436e122393ae487773c9361a31894dfc2e54c))
- add validation operating exist ([df2b3aa](https://gitlab.com/fi-sas/fisas-queue/commit/df2b3aa8eda491d1be415889e2ff35157d42aac3))
- add/get (withRelated) association between desk,operator,subject ([6fb9dd4](https://gitlab.com/fi-sas/fisas-queue/commit/6fb9dd41cb6a879ebac8017a632d21658c448730))
- all tickets attended out of target ([c1d5488](https://gitlab.com/fi-sas/fisas-queue/commit/c1d5488dc73ff21eb563f2398b96a212c23589f0))
- average of pauses ([0adff6d](https://gitlab.com/fi-sas/fisas-queue/commit/0adff6d98f7811e2e243a89b7c7021b9ee9335cf))
- configurations ([5d124d4](https://gitlab.com/fi-sas/fisas-queue/commit/5d124d48aa26b3e1b06d616860ce92990d995a79))
- create and update user_schedule by user ([a1aa069](https://gitlab.com/fi-sas/fisas-queue/commit/a1aa069ce3a4caaf9d09872a2877041ea78e6455))
- get subjects by tv_id ([202f8b0](https://gitlab.com/fi-sas/fisas-queue/commit/202f8b0da1aa492a8dfa6a0b38bbab8a04227bbe))
- management associations ([83caeb4](https://gitlab.com/fi-sas/fisas-queue/commit/83caeb4fe7b9ba20aea9375f126a34a5df1e0927))
- number of tickets digital and papel, average time waiting attended ([ce36d24](https://gitlab.com/fi-sas/fisas-queue/commit/ce36d242f1051ce5319ac450e390939acccc794f))
- percentage of tickest attended for services and tickets recovered ([263d92b](https://gitlab.com/fi-sas/fisas-queue/commit/263d92bfaac969c2277a905800a4821155469f55))
- recalculate ticket estimated time ([efaed0c](https://gitlab.com/fi-sas/fisas-queue/commit/efaed0ce05e69c62f6449d8c4a3681fb1cda7eb5))
- services crud and TV's services ([97e41f6](https://gitlab.com/fi-sas/fisas-queue/commit/97e41f6c6b0108e24aded21a3431215dff43bc87))
- subjects and schedules crud ([92ff31f](https://gitlab.com/fi-sas/fisas-queue/commit/92ff31fde5cfe6e94ca99516cd08ec4ec492a9ec))
- ticket attendence and ticket queue transfer ([c12c579](https://gitlab.com/fi-sas/fisas-queue/commit/c12c579b5f0d6e7d8f676751f7193c29b2028e95))
- validation ticket with subject schedule ([4bbc5aa](https://gitlab.com/fi-sas/fisas-queue/commit/4bbc5aaf3b9ca67226da43b5d71f33748c2f587f))
- **subjects:** create, update, get valid hours to appointments methods ([52a5080](https://gitlab.com/fi-sas/fisas-queue/commit/52a508056629bb8622349c307039748a2e940f2f))
- **tickets:** create and cancel ticket/apointment ([d8c927a](https://gitlab.com/fi-sas/fisas-queue/commit/d8c927a1e0bcc4cf0981330cafef0e6224b301ee))

### Reverts

- add missing file on last commit ([3f2b456](https://gitlab.com/fi-sas/fisas-queue/commit/3f2b4565fe8b667bab7ee93a42ff2492506d84d5))
- revert changes on knexfile in last commit ([7f39504](https://gitlab.com/fi-sas/fisas-queue/commit/7f39504bc0f2b1063d072ceff260d03605416c2f))
