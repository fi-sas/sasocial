const { table } = require("../services/tickets_history.service");


module.exports.up = async (db) =>
	db.schema

		// Service Table
		.createTable("service", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.boolean("active");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})


		// Tv_service table
		.createTable("tv_service", (table) => {
			table.increments();
			table.string("tv_id").notNullable();
			table.integer("service_id").notNullable().unsigned().references("id").inTable("service");
		})

		// // ticket_status table
		// .createTable("ticket_status", (table) => {
		// 	table.increments();
		// 	table.string("description", 50).notNullable();
		// })

		// Desk Operator table
		.createTable("desk_operator", (table) => {
			table.increments();
			table.integer("user_id").notNullable();
			table.boolean("show_photo").notNullable();
			table.boolean("active").notNullable();
		})

		// ticket table
		.createTable("ticket", (table) => {
			table.increments();
			table.integer("sequencial_number");
			table.float("order_number");
			table.datetime("created_at").notNullable();
			table.datetime("estimated_time").notNullable();
			table.string("origin", 10).notNullable();
			table.integer("call_number").notNullable();
			table.datetime("call_time");
			table.datetime("initial_time");
			table.datetime("final_time");
			table.integer("appointment_number");
			table.datetime("appointment_schedule");
			table.boolean("priority").notNullable();
			table.string("notes", 250);
			// table.integer("status_id").notNullable().unsigned().references("id").inTable("ticket_status");
			table.enum("status", ["EM_ESPERA", "ATENDIDO", "CANCELADO", "NAO_COMPARECEU", "CHAMADO", "EM_ATENDIMENTO", "TRANSFERIDO"], { useNative: true, enumName: "ticket_status" });
			table.integer("subject_id").notNullable().unsigned();
			table.integer("attended_by_id").unsigned();
			table.string("email", 50);
			table.string("student_number", 50);
			table.enum("type", ["S", "P", "M"]);
			table.integer("user_id");
		})

		// Ticket history table
		.createTable("ticket_history", (table) => {
			table.increments();
			table.integer("sequencial_number");
			table.float("order_number");
			table.datetime("created_at").notNullable();
			table.datetime("estimated_time").notNullable();
			table.string("origin", 10).notNullable();
			table.integer("call_number").notNullable();
			table.datetime("call_time");
			table.datetime("initial_time");
			table.datetime("final_time");
			table.integer("appointment_number");
			table.datetime("appointment_schedule");
			table.boolean("priority").notNullable();
			table.string("notes", 250);
			// table.integer("status_id").notNullable().unsigned().references("id").inTable("ticket_status");
			table.enum("status", ["EM_ESPERA", "ATENDIDO", "CANCELADO", "NAO_COMPARECEU", "CHAMADO", "EM_ATENDIMENTO", "TRANSFERIDO"], { useNative: true, existingType: true, enumName: "ticket_status" });
			table.integer("subject_id").notNullable().unsigned();
			table.integer("attended_by_id").unsigned();
			table.string("email", 50);
			table.string("student_number", 50);
			table.enum("type", ["S", "P", "M"]);
			table.integer("user_id");
		})

		// Subject table
		.createTable("subject", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.string("code", 3).notNullable();
			table.string("schedule_description", 150).notNullable();
			table.float("avg_time").notNullable();
			table.float("n_tickets").notNullable().defaultTo(1);
			table.integer("max_tolerance").notNullable().unsigned();
			table.boolean("appointments").notNullable();
			table.integer("appointments_interval");
			table.integer("estimated_time");
			table.integer("service_id").notNullable().unsigned().references("id").inTable("service");
			table.boolean("active").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
			table.string("ongoing_ticket", 10);
			table.string("last_desk", 10);
		})

		// Desk table
		.createTable("desk", (table) => {
			table.increments();
			table.string("name", 50).notNullable();
			table.boolean("actual_state").notNullable().defaultTo(false);
			table.boolean("active").notNullable().defaultTo(true);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// subject_schedule_day table
		.createTable("subject_schedule_day", (table) => {
			table.increments();
			table.string("description", 50).notNullable();
			table.integer("week_day").notNullable();
			table.integer("subject_id").notNullable().unsigned().references("id").inTable("subject");

		})
		// subject_schedule table
		.createTable("subject_schedule", (table) => {
			table.increments();
			table.string("opening_hour", 6).notNullable();
			table.string("closing_hour", 6).notNullable();
			table
				.integer("subject_schedule_day_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("subject_schedule_day");
		})

		// subject_schedule_day table
		.createTable("operator_schedule_day", (table) => {
			table.increments();
			table.string("description", 50).notNullable();
			table.integer("week_day").notNullable();
			table.integer("operator_id").notNullable().unsigned().references("id").inTable("desk_operator");

		})

		// subject_schedule table
		.createTable("operator_schedule", (table) => {
			table.increments();
			table.string("opening_hour", 6).notNullable();
			table.string("closing_hour", 6).notNullable();
			table
				.integer("operator_schedule_day_id")
				.notNullable()
				.unsigned()
				.references("id")
				.inTable("operator_schedule_day");
		})

		// subject_schedule_day table
		.createTable("pause", (table) => {
			table.increments();
			table.datetime("initial_date").notNullable();
			table.datetime("final_date");
			table.integer("desk_id").notNullable().unsigned().references("id").inTable("desk");
		})

		.createTable("hours_attendance_desk", (table) => {
			table.increments();
			table.datetime("start_time").notNullable();
			table.datetime("final_time");
			table.integer("desk_id").notNullable().unsigned().references("id").inTable("desk");
		})

		.createTable("subject_desk_operator", (table) => {
			table.increments();
			table.integer("operator_id").notNullable().unsigned().references("id").inTable("desk_operator");
			table.integer("desk_id").notNullable().unsigned().references("id").inTable("desk");
			table.integer("subject_id").notNullable().unsigned().references("id").inTable("subject");
			table.datetime("inicial_date").notNullable();
			table.boolean("operating").notNullable();
			table.datetime("final_date");
		})

		.raw(
			"ALTER TABLE ticket_history ADD CONSTRAINT ticket_history_subject_fk FOREIGN KEY (subject_id) REFERENCES subject (id);",
		)
		.raw(
			"ALTER TABLE ticket ADD CONSTRAINT ticket_subject_fk FOREIGN KEY (subject_id) REFERENCES subject (id);",
		)

		.createTable("ticket_queue", (table) => {
			table.increments();
			table.integer("last");
			table.enu("type", ["S", "P", "M"]).notNullable();
			table.datetime("updated_at").notNullable();
			table.integer("subject_id").notNullable().unsigned().references("id").inTable("subject");
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("service")
		.dropTable("subject")
		.dropTable("subject_schedule_day")
		.dropTable("subject_schedule")
		.dropTable("desk")
		// .dropTable("ticket_status")
		.dropTable("ticket")
		.dropTable("ticket_history")
		.dropTable("subject_desk_operator")
		.dropTable("desk_operator")
		.dropTable("tv_service")
		.dropTable("operator_schedule_day")
		.dropTable("hours_attendance_desk")
		.dropTable("operator_schedule")
		.dropTable("pause")
		.dropTable("ticket_queue");
