
module.exports.up = async (db) =>
    db.schema
        .table("ticket", (table) => {
            table.specificType('call_times', 'timestamp[]');
            table.integer("attended_on").unsigned().references("id").inTable("desk");
            table.dropColumn('call_time');
        })
        .table("ticket_history", (table) => {
            table.specificType('call_times', 'timestamp[]');
            table.integer("attended_on").unsigned().references("id").inTable("desk");
            table.dropColumn('call_time');

        });
module.exports.down = async db => db.schema
    .table("ticket", (table) => {
        table.dropColumn('call_times');
        table.dropColumn('attended_on');
        table.datetime("call_time");
    })
    .table("ticket_history", (table) => {
        table.dropColumn('call_times');
        table.dropColumn('attended_on');
        table.datetime("call_time");
    })
    ;

module.exports.configuration = { transaction: true };
