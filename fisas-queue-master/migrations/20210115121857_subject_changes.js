
module.exports.up = async (db) =>
	db.schema
		.table("subject", (table) => {
			table.integer("waiting_time");
		});

module.exports.down = async db => db.schema
	.table("subject", (table) => {
		table.dropColumn("waiting_time");
	});

module.exports.configuration = { transaction: true };
