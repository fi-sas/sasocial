
module.exports.up = async (db) =>
	db.schema
		.table("ticket", (table) => {
			table.string("appointment_reason", 255);
		})
		.table("ticket_history", (table) => {
			table.string("appointment_reason", 255);
		})
	;

module.exports.down = async db => db.schema
	.table("ticket", (table) => {
		table.dropColumn("appointment_reason");
	})
	.table("ticket_history", (table) => {
		table.dropColumn("appointment_reason");
	});

module.exports.configuration = { transaction: true };
