
module.exports.up = async (db) =>
	db.schema
		.table("ticket", (table) => {
			table.string("ticket_code", 25);
		})
		// Service translations Table
		.createTable("service_translation", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.integer("service_id").unsigned().references("id").inTable("service");
			table.integer("language_id").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.table("service", (table) => {
			table.dropColumn("name");
		})
		// Subjects translations Table
		.createTable("subject_translation", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table.string("schedule_description", 150).notNullable();
			table.integer("subject_id").unsigned().references("id").inTable("subject");
			table.integer("language_id").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})
		.table("subject", (table) => {
			table.dropColumn("name");
			table.dropColumn("schedule_description");
		});

module.exports.down = async db => db.schema
	.table("ticket", (table) => {
		table.string("ticket_code", 25);
	})
	.dropTable("service_translation")
	.table("service", (table) => {
		table.string("name", 100).notNullable();
	})
	.dropTable("subject_translation")
	.table("subject", (table) => {
		table.string("name", 100).notNullable();
		table.string("schedule_description", 150).notNullable();
	});

module.exports.configuration = { transaction: true };
