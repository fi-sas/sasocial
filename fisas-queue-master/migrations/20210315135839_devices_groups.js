module.exports.up = async (db) =>
	db.schema
		.createTable("service_group", (table) => {
			table.increments();
			table.integer("service_id").unsigned().references("id").inTable("service");
			table.integer("group_id").unsigned();
			table.unique(["service_id", "group_id"]);
		});

module.exports.down = async db => db.schema
	.dropTable("service_group");

module.exports.configuration = { transaction: true };
