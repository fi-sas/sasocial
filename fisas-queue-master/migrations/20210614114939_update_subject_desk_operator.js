module.exports.up = async (db) =>
	db.schema
		.alterTable("subject_desk_operator", (table) => {
			table.dropColumn("inicial_date");
            table.dropColumn("final_date");
            table.boolean("active").defaultTo(true);
		});

module.exports.down = async db => db.schema
	.alterTable("subject_desk_operator", (table) => {
        table.datetime("inicial_date").notNullable();
		table.datetime("final_date");
        table.dropColumn("active");
    });

module.exports.configuration = { transaction: true };
