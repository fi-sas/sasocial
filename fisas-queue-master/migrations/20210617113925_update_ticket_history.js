
module.exports.up = async (db) =>
db.schema
    .alterTable("ticket_history", (table) => {
        table.string("ticket_code", 25);
    });

module.exports.down = async db => db.schema
.alterTable("ticket_history", (table) => {
    table.dropColumn("ticket_code");
});

module.exports.configuration = { transaction: true };
