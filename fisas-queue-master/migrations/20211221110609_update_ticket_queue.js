module.exports.up = async (db) =>
	db.schema.table("ticket_queue", (table) => {
		table.date("attendance_day").nullable();
	});

module.exports.down = async (db) =>
	db.schema.table("ticket_queue", (table) => {
		table.dropColumn("attendance_day");
	});

module.exports.configuration = { transaction: true };
