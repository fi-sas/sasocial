"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.desk_operators",
	table: "desk_operator",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "desk_operators")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "show_photo", "active"],
		defaultWithRelateds: ["user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			schedule_operator(ids, operators, rule, ctx) {
				return Promise.all(
					operators.map((operator) => {
						return ctx
							.call("queue.operator_schedule_days.getScheduleByOperator", {
								operator_id: operator.id,
							})
							.then((res) => (operator.schedule_operator = res));
					}),
				);
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			show_photo: { type: "boolean", nullable: true },
			active: { type: "boolean" },
		}
	},
	hooks: {
		before: {
			create: ["validateHourFormat"],
			update: [
				async function check_operator(ctx) {
					await this._get(ctx, { id: ctx.params.id });
				},
			],
			remove : [
				function preventDelete() {
					throw new Errors.ValidationError("Delete operation is forbiden",
						"SERVICE_DELETE_FORBIDEN", {});
				},
			]
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create: {
			visibility: "published",
			params:{
				operator: {
					type: "object", props: {
						user_id: { type: "number", positive: true, integer: true },
						show_photo: { type: "boolean", nullable: true },
						active: { type: "boolean" },
					}
				},
				schedule:{
					type: "array",
					items: {
						type: "object", props: {
							description: { type: "string" },
							week_day: { type: "number" },
							schedules: {
								type: "array",
								items: {
									type: "object", props:{
										opening_hour: { type: "string" },
										closing_hour: { type: "string" }
									}
								}
							}
						}
					}
				}
			},
			async handler(ctx) {
				if (ctx.params.schedule === undefined || !ctx.params.schedule) {
					throw new Errors.ValidationError(
						"Schedule is empty",
						"SCHEDULE_IS_EMPTY",
						{});
				}
				if (ctx.params.schedule.length === 0) {
					throw new Errors.ValidationError(
						"Schedule is empty",
						"SCHEDULE_IS_EMPTY",
						{});
				} else {
					await ctx.call("authorization.users.get", { id: ctx.params.operator.user_id });
					const desk_operator = await this._find(ctx, {
						query: {
							user_id: ctx.params.operator.user_id,
							active: true
						},
					});
					if (desk_operator.length !== 0) {
						throw new Errors.ValidationError(
							"Already exist desk operator with user Id",
							"ALREADY_EXIST_DESK_OPERATOR_WITH_USER_ID",
							{},
						);
					}
					return await this._create(ctx, ctx.params.operator).then(async (operator) => {
						ctx.params.operator_id = operator[0].id;
						const operator_schedule = await ctx.call(
							"queue.operator_schedule_days.create",
							ctx.params,
						);
						operator[0].schedule = operator_schedule;
						return operator;
					});
				}
			},
		},
		update: {
			visibility: "published",
			params:{
				operator: {
					type: "object", props: {
						id: { type: "number"},
						user_id: { type: "number", positive: true, integer: true },
						show_photo: { type: "boolean", nullable: true },
						active: { type: "boolean" },
					}
				},
				schedule:{
					type: "array",
					items: {
						type: "object", props: {
							description: { type: "string" },
							week_day: { type: "number" },
							schedules: {
								type: "array",
								items: {
									type: "object", props:{
										opening_hour: { type: "string" },
										closing_hour: { type: "string" }
									}
								}
							}
						}
					}
				}
			},
			async handler(ctx) {
				if (ctx.params.schedule === undefined || !ctx.params.schedule) {
					throw new Errors.ValidationError(
						"Schedule is empty",
						"SCHEDULE_IS_EMPTY",
						{});
				}
				if (ctx.params.schedule.length === 0) {
					throw new Errors.ValidationError(
						"Schedule is empty",
						"SCHEDULE_IS_EMPTY",
						{});
				}
				else {
					const desk_operator = await this._get(ctx, { id: ctx.params.id });
					ctx.params.operator.user_id = desk_operator[0].user_id;
					const operator = await this._update(ctx, ctx.params.operator);
					ctx.params.operator_id = operator[0].id;
					const operator_schedules = await ctx.call(
						"queue.operator_schedule_days.update",
						ctx.params,
					);
					operator[0].schedule = operator_schedules;
					return operator;
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"queue.desk_operators.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		validateHourFormat(ctx) {
			for (const scheduleDays of ctx.params.schedule) {
				for (const schedule of scheduleDays.schedules) {
					const regex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;
					if (!regex.test(schedule.opening_hour) || !regex.test(schedule.closing_hour)) {
						throw new Errors.ValidationError(
							"Desk operator schedule hour [" + schedule.opening_hour + " - " + schedule.closing_hour + " ] isn't valid",
							"DESK_OPERATOR_INVALID_HOURS",
						);
					}
				}
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
