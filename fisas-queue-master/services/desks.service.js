"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "queue.desks",
	table: "desk",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "desks")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "actual_state", "active", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string", max: 50 },
			actual_state: { type: "boolean" },
			active: { type: "boolean" },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateName",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateName",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				async function check_desk(ctx) {
					await this._get(ctx, { id: ctx.params.id });
				},
			],
			remove : [
				function preventDelete() {
					throw new Errors.ValidationError("Delete operation is forbiden",
						"SERVICE_DELETE_FORBIDEN", {});
				},
			]
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		start_pause_desk: {
			params: {
				desk_id: {
					type: "number",
					positive: true,
					integer: true,
				},
			},
			scope: "queue:desks:operate",
			async handler(ctx) {
				return await this._get(ctx, { id: ctx.params.desk_id }).then(async (desk) => {
					let desk_update = desk[0];
					desk_update.actual_state = false;
					return await this._update(ctx, desk_update);
				});
			},
		},
		stop_pause_desk: {
			params: {
				desk_id: {
					type: "number",
					convert: true,
					positive: true,
					integer: true,
				},
			},
			scope: "queue:desks:operate",
			async handler(ctx) {
				return await this._get(ctx, { id: ctx.params.desk_id }).then(async (desk) => {
					let desk_update = desk[0];
					desk_update.actual_state = true;
					return await this._update(ctx, desk_update);
				});
			},
		},
		start_service: {
			rest: "POST /starService/desk/:desk_id/operator/:operator_id",
			visibility: "published",
			scope: "queue:desks:operate",
			async handler(ctx) {
				await ctx.call("queue.desks.desk_close_association", { operator_id: ctx.params.operator_id});
				this.verify_validation();
				let resultList = [];
				let associations = await ctx.call("queue.subject_desk_operators.find", {
					query: {
						active: true,
						operator_id: ctx.params.operator_id,
						desk_id: ctx.params.desk_id
					},
				});
				await this._get(ctx, { id: ctx.params.desk_id }).then(async (desk) => {
					desk[0].actual_state = true;
					await this._update(ctx, desk[0]).then(async (desk_updated) => {
						let association_list = [];
						for (let association of associations) {
							if(association.desk_id === parseInt(ctx.params.desk_id)) {
								association.operating = true;
								const result = await ctx.call("queue.subject_desk_operators.update", association);
								association_list.push(result[0]);
							} else {
								association.operating = false;
								const result = await ctx.call("queue.subject_desk_operators.update", association);
								association_list.push(result[0]);
							}
						}
						await ctx.call("queue.hours_attendance_desks.create", {
							desk_id: parseInt(ctx.params.desk_id),
						});
						desk_updated[0].associations = association_list;
						resultList.push(desk_updated[0]);
					});
				});
				return resultList;
			},
		},

		stop_service: {
			rest: "POST /stopService/desk/:desk_id/operator/:operator_id",
			scope: "queue:desks:operate",
			visibility: "published",
			async handler(ctx) {
				this.verify_validation();
				let resultList = [];
				let associations = await ctx.call("queue.subject_desk_operators.find", {
					query: {
						active: true,
						operator_id: ctx.params.operator_id,
						desk_id: ctx.params.desk_id,
					},
				});
				let association_list = [];
				const desk = await this._get(ctx, { id: ctx.params.desk_id });
				const desk_updated = await ctx.call("queue.desks.patch", { id: desk[0].id, actual_state: false });
				for (let association of associations) {
					association.operating = false;
					const result = await ctx.call("queue.subject_desk_operators.update", association);
					association_list.push(result[0]);
				}
				const list_times = await ctx.call("queue.hours_attendance_desks.find", {
					query: {
						desk_id: ctx.params.desk_id,
						final_time: null,
					},
				});
				for (let times of list_times) {
					times.final_time = new Date();
					await ctx.call("queue.hours_attendance_desks.update", times);
				}
				desk_updated[0].associations = association_list;
				resultList.push(desk_updated[0]);
				return resultList;
			},
		},

		desk_close_association: {
			params: {
				operator_id: { type: "number", integer: true, convert: true }
			},
			async handler(ctx) {
				const associations = await ctx.call("queue.subject_desk_operators.find", { query: {
					operator_id: ctx.params.operator_id,
					active: true,
				}});
				await this.close_desks(ctx, associations);
				for(const association of associations) {
					await ctx.call("queue.subject_desk_operators.patch", { id: association.id, operating: false });
					const list_times = await ctx.call("queue.hours_attendance_desks.find", {
						query: {
							desk_id: association.desk_id,
							final_time: null,
						},
					});
					for (let times of list_times) {
						times.final_time = new Date();
						await ctx.call("queue.hours_attendance_desks.update", times);
					}
				}
			}
		}

	},

	/**
	 * Events
	 */
	events: {
		"queue.desks.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},

		async verify_validation(ctx) {
			let operator = await ctx.call("queue.desk_operators.get", { id: ctx.params.operator_id });

			if (operator.length !== 0) {
				if (operator[0].active === false) {
					throw new Errors.ValidationError("Desk operator not active", "DESK_OPERATOR_NOT_ACTIVE", {
						operator: "operator_id",
					});
				}
			}
			let desk = await this._get(ctx, { id: ctx.params.desk_id });
			if (desk.length !== 0) {
				if (desk[0].active === false) {
					throw new Errors.ValidationError("Desk not active", "DESK_NOT_ACTIVE", {
						desk: "desk_id",
					});
				}
			}
			let association = await ctx.call("queue.subject_desk_operator.find", {
				query: {
					operating: true,
					active: true,
					operator_id: ctx.params.operator_id,
				},
			});
			if (association.length !== 0) {
				throw new Errors.ValidationError(
					"This desk operator is working another desk ",
					"OPERATOR_IS_WORKING",
					{
						operator_id: ctx.params.operator_id,
					},
				);
			}
		},

		async close_desks(ctx, list) {
			const list_desks = [];
			for (const association of list) {
				const exist = list_desks.includes(association.desk_id);
				if (exist === false) {
					list_desks.push(association.desk_id);
				}
			}
			for (const desk of list_desks) {
				await ctx.call("queue.desks.patch", { id: desk, actual_state: false });
			}
		},

		async validateName(ctx) {
			const desk = await this._find(ctx, {
				query: { name: ctx.params.name },
			});

			if (desk.length !== 0) {
				throw new Errors.ValidationError(
					"Desk already exists",
					"QUEUE_DESK_ALREADY_EXISTS", {});
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
