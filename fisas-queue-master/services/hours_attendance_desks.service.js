"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.hours_attendance_desks",
	table: "hours_attendance_desk",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "hours_attendance_desks")],

	/**
	 * Settings
	 */

	settings: {
		fields: ["id", "start_time", "final_time", "desk_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			start_time: { type: "date", optional: true, nullable: true, convert: true },
			final_time: { type: "date", optional: true, nullable: true, convert: true },
			desk_id: { type: "number", positive: true, integer: true, optional: false, nullable: false },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.start_time = new Date();
					ctx.params.final_time = null;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		//Get media de tempo de funcionamento dos serviços
		getAvgTimeOperation: {
			scope: "queue:hours_attendance_desks:read",
			rest: "GET /avgtimeoperating",
			visibility: "published",
			cache: {
				enabled: false
			},
			async handler() {
				const list_times = await this.adapter
					.getDB("hours_attendance_desk")
					.whereNotNull("final_time");

				let list_calculations = [];
				for (const times of list_times) {
					const start_date = times.start_time.getTime();
					const end_date = times.final_time.getTime();
					const calculation = end_date - start_date;
					list_calculations.push(calculation);
				}

				let total_calculation = 0;
				for (const times of list_calculations) {
					total_calculation += times;
				}

				let ms_avg = list_calculations.length > 0 ? (total_calculation / list_calculations.length) : 0;
				let sec_avg = list_calculations.length > 0 ? (total_calculation / list_calculations.length / 1000) : 0;
				let compose = this.formatHours(ms_avg);
				const result = {
					avg_hours: compose,
					avg_sec: sec_avg.toFixed(2),
					avg_ms: ms_avg.toFixed(2),
				};
				return result;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"queue.hours_attendance_desks.*"() {
			this.logger.error("BROADCAST EVENT 2");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		// Converter ms para o formato 00:00:00
		formatHours(ctx) {
			let ms = ctx % 1000;
			ctx = (ctx - ms) / 1000;
			let secs = ctx % 60;
			ctx = (ctx - secs) / 60;
			let mins = ctx % 60;
			let hrs = (ctx - mins) / 60;

			if (mins < 10) {
				mins = "0" + mins;
			}
			if (secs < 10) {
				secs = "0" + secs;
			}
			if (hrs < 10) {
				hrs = "0" + hrs;
			}
			const compose = hrs + ":" + mins + ":" + secs;

			return compose;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
