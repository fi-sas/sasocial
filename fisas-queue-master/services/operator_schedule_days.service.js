"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "queue.operator_schedule_days",
	table: "operator_schedule_day",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "operator_schedule_days")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "operator_id", "week_day", "description"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			operator_id: { type: "number", integer: true },
			week_day: { type: "number", integer: true, optional: true },
			description: { type: "string", max: 25, optional: true },
		},
	},
	hooks: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create: {
			visibility: "published",
			params: {
				schedule:{
					type: "array",
					items: {
						type: "object", props: {
							description: { type: "string" },
							week_day: { type: "number" },
							schedules: {
								type: "array",
								items: {
									type: "object", props:{
										opening_hour: { type: "string" },
										closing_hour: { type: "string" }
									}
								}
							}
						}
					}
				}
			},
			async handler(ctx) {
				let schedules = ctx.params.schedule;
				let resultList = [];
				for (const days of schedules) {
					let entities = {
						operator_id: ctx.params.operator_id,
						week_day: days.week_day,
						description: days.description,
					};
					await this._create(ctx, entities).then(async (operator_days) => {
						let day_hours = [];
						for (const hours of days.schedules) {
							hours.operator_schedule_day_id = operator_days[0].id;
							const list_hours = await ctx.call("queue.operator_schedules.create", hours);
							day_hours.push(list_hours);
						}
						operator_days[0].schedules = day_hours;
						resultList.push(operator_days);
					});
				}
				return resultList;
			},
		},
		update: {
			visibility: "published",
			params: {
				schedule:{
					type: "array",
					items: {
						type: "object", props: {
							description: { type: "string" },
							week_day: { type: "number" },
							schedules: {
								type: "array",
								items: {
									type: "object", props:{
										opening_hour: { type: "string" },
										closing_hour: { type: "string" }
									}
								}
							}
						}
					}
				}
			},
			async handler(ctx) {
				let schedules = ctx.params.schedule;
				let resultList = [];
				await this.removeSchedulesByOperator(ctx).then(async () => {
					for (const days of schedules) {
						let entities = {
							operator_id: ctx.params.operator_id,
							week_day: days.week_day,
							description: days.description,
						};
						await this._create(ctx, entities).then(async (operator_days) => {
							let day_hours = [];
							for (const hours of days.schedules) {
								hours.operator_schedule_day_id = operator_days[0].id;
								const list_hours = await ctx.call("queue.operator_schedules.create", hours);
								day_hours.push(list_hours);
							}
							operator_days[0].schedules = day_hours;
							resultList.push(operator_days);
						});
					}
				});
				return resultList;
			},
		},
		getScheduleByOperator: {
			scope: "queue:operator_schedule_days:read",
			params: { operator_id: { type: "number" } },
			async handler(ctx) {
				let resultList = [];
				await this._find(ctx, {
					query: { operator_id: ctx.params.operator_id },
				}).then(async (schedules_operator) => {
					for (const days of schedules_operator) {
						await ctx
							.call("queue.operator_schedules.find", {
								query: { operator_schedule_day_id: days.id },
							})
							.then(async (hours) => {
								days.hours = hours;
							});
						resultList.push(days);
					}
				});
				return resultList;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async removeSchedulesByOperator(ctx) {
			await this._find(ctx, {
				query: { operator_id: ctx.params.operator_id },
			}).then(async (schedules_operator) => {
				for (const days of schedules_operator) {
					await ctx
						.call("queue.operator_schedules.find", {
							query: { operator_schedule_day_id: days.id },
						})
						.then(async (hours) => {
							for (const item of hours) {
								await ctx.call("queue.operator_schedules.remove", { id: item.id });
							}
						});
					await this._remove(ctx, { id: days.id });
				}
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
