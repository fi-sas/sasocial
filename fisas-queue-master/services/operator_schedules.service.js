"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.operator_schedules",
	table: "operator_schedule",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "operator_schedules")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "operator_schedule_day_id", "opening_hour", "closing_hour"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			operator_schedule_day_id: { type: "number", integer: true },
			opening_hour: { type: "string", max: 6 },
			closing_hour: { type: "string", max: 6 },
		},
		hooks: {},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
