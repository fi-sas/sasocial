"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.pauses",
	table: "pause",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "pauses")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "desk_id", "initial_date", "final_date"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			desk_id: { type: "number", positive: true, integer: true, optional: false, nullable: false },
			initial_date: { type: "date", optional: true, nullable: true, convert: true },
			final_date: { type: "date", optional: true, nullable: true, convert: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function add_inicial_time(ctx) {
					ctx.params.initial_time = new Date();
					ctx.params.final_time = null;
				},
			],
		},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		start_pause: {
			scope: "queue:pauses:create",
			rest: "POST /starpause/:desk_id",
			visibility: "published",
			async handler(ctx) {
				const entities = {
					desk_id: parseInt(ctx.params.desk_id),
					initial_date: new Date(),
					final_date: null,
				};
				return await this._insert(ctx, { entity: entities }).then(async (pause_entitie) => {
					return await ctx
						.call("queue.desks.start_pause_desk", { desk_id: parseInt(ctx.params.desk_id) })
						.then((desk) => {
							pause_entitie[0].desk = desk;
							return pause_entitie;
						});
				});
			},
		},

		stop_pause: {
			scope: "queue:pauses:create",
			rest: "POST /stoppause/:desk_id",
			visibility: "published",
			async handler(ctx) {
				return await this._find(ctx, {
					query: {
						desk_id: parseInt(ctx.params.desk_id),
						final_date: null,
					},
				}).then(async (pause) => {
					let pause_update = pause[0];
					pause_update.final_date = new Date();
					return await this._update(ctx, pause_update).then(async (pause_entitie) => {
						return await ctx
							.call("queue.desks.stop_pause_desk", { desk_id: parseInt(ctx.params.desk_id) })
							.then((desk) => {
								pause_entitie[0].desk = desk;
								return pause_entitie;
							});
					});
				});
			},
		},

		//Tempo medio de pausas
		avg_pauses: {
			scope: "queue:pauses:read",
			rest: "GET /avgpauses",
			visibility: "published",
			async handler() {
				const completPauses = await this.adapter.getDB("pause").whereNotNull("final_date");
				let listCalculated = [];
				for (let pause of completPauses) {
					const inicial_time = pause.initial_date.getTime();
					const final_time = pause.final_date.getTime();
					const calculation = final_time - inicial_time;
					listCalculated.push(calculation);
				}
				let numberTotalCausesComplets = 0;
				for (const calculations of listCalculated) {
					numberTotalCausesComplets += calculations;
				}
				const ms_avg = listCalculated.length > 0 ? (numberTotalCausesComplets / listCalculated.length) : 0;
				const sec_avg = listCalculated.length > 0 ? (numberTotalCausesComplets / listCalculated.length / 1000) : 0;
				const compose = this.formatHours(ms_avg);
				const result = {
					avg_hours: compose,
					avg_sec: sec_avg.toFixed(2),
					avg_ms: ms_avg.toFixed(2),
				};
				return result;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		// Converter ms para formato 00:00:00
		formatHours(ctx) {
			let ms = ctx % 1000;
			ctx = (ctx - ms) / 1000;
			let secs = ctx % 60;
			ctx = (ctx - secs) / 60;
			let mins = ctx % 60;
			let hrs = (ctx - mins) / 60;

			if (mins < 10) {
				mins = "0" + mins;
			}
			if (secs < 10) {
				secs = "0" + secs;
			}
			if (hrs < 10) {
				hrs = "0" + hrs;
			}
			const compose = hrs + ":" + mins + ":" + secs;

			return compose;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
