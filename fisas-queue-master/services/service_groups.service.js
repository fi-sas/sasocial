"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

/** TODO: find by active */

module.exports = {
	name: "queue.service_groups",
	table: "service_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "service_groups")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "service_id", "group_id"],
		defaultWithRelateds: ["group"],
		withRelateds: {
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.services", "service", "service_id");
			},
			group(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.groups", "group", "group_id");
			},
		},
		entityValidator: {
			service_id: { type: "number" },
			group_id: { type: "number", integer: true, convert: true }
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {



	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
