"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

/** TODO: find by active */

module.exports = {
	name: "queue.service_translations",
	table: "service_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "service_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "service_id", "language_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			language(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.languages", "language", "language_id");
			},
		},
		entityValidator: {
			name: { type: "string", max: 100 },
			service_id: { type: "number" },
			language_id: { type: "number" },
			created_at: { type: "date", optional: true, nullable: false, convert: true },
			updated_at: { type: "date", optional: true, nullable: false, convert: true },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "public",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {



	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
