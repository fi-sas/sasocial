"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

/** TODO: find by active */

module.exports = {
	name: "queue.services",
	table: "service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "services")],

	/**
	 * Settings
	 */

	settings: {
		fields: ["id", "name", "active", "created_at", "updated_at"],
		defaultWithRelateds: ["translations", "groups"],
		withRelateds: {
			subjects(ids, services, rule, ctx) {
				return Promise.all(
					services.map((service) => {
						return ctx
							.call("queue.subjects.find", { query: { service_id: service.id, active: true }, sort: "-id", })
							.then((subjects) => {
								service.subjects = subjects;
							});
					}),
				);
			},
			"translations"(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("queue.service_translations.find", { query: { service_id: doc.id, language_id: ctx.meta.language_id } })
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return hasMany(docs, ctx, "queue.service_translations", "translations", "id", "service_id");
				}
			},
			groups(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "queue.service_groups", "groups", "id", "service_id");
			}
		},
		entityValidator: {
			active: { type: "boolean", optional: true, default: true },
			translations: {
				type: "array", min: 1,
				items: {
					type: "object", props: {
						name: "string|max: 100",
						language_id: "number|integer|convert"
					}
				}
			},
			groups: {
				type: "array", optional: true,
				items: {
					type: "object", props: {
						group_id: { type: "number", integer: true, convert: true }
					}
				}
			},
			created_at: { type: "date", optional: true, nullable: false, convert: true },
			updated_at: { type: "date", optional: true, nullable: false, convert: true },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateLanguages",
				"validateGroups",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.active = true;
				}
			],
			update: [
				"validateLanguages",
				"validateGroups",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"validateLanguages",
				"validateGroups",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: [
				function preventDelete() {
					throw new Errors.ValidationError(
						"Delete operation is forbiden",
						"SERVICE_DELETE_FORBIDEN",
					);
				},
			],
		},
		after: {
			create: [
				async function sanatizeParams(ctx, response) {
					response[0].translations = await this.createWithRelated(ctx, "queue.service_translations.create", response[0].id, ctx.params.translations);

					if (ctx.params.groups)
						response[0].groups = await this.createWithRelated(ctx, "queue.service_groups.create", response[0].id, ctx.params.groups);

					return response;
				}
			],
			update: [
				async function sanatizeParams(ctx, response) {
					await this.deleteWithRelated(ctx, "queue.service_translations.remove", await ctx.call("queue.service_translations.find", { query: { service_id: response[0].id } }));
					response[0].translations = await this.createWithRelated(ctx, "queue.service_translations.create", response[0].id, ctx.params.translations);

					if (ctx.params.groups) {
						await this.deleteWithRelated(ctx, "queue.service_groups.remove", await ctx.call("queue.service_groups.find", { query: { service_id: response[0].id } }));
						response[0].groups = await this.createWithRelated(ctx, "queue.service_groups.create", response[0].id, ctx.params.groups);
					}

					return response;
				}
			],
			patch: [
				async function sanatizeParams(ctx, response) {
					if (ctx.params.translations) {
						await this.deleteWithRelated(ctx, "queue.service_translations.remove", await ctx.call("queue.service_translations.find", { query: { service_id: response[0].id } }));
						response[0].translations = await this.createWithRelated(ctx, "queue.service_translations.create", response[0].id, ctx.params.translations);
					}
					if (ctx.params.groups) {
						await this.deleteWithRelated(ctx, "queue.service_groups.remove", await ctx.call("queue.service_groups.find", { query: { service_id: response[0].id } }));
						response[0].groups = await this.createWithRelated(ctx, "queue.service_groups.create", response[0].id, ctx.params.groups);
					}
					return response;
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		services: {
			visibility: "published",
			cache: {
				enabled: true,
				ttl: 30,
				keys: ["#device.id"],
			},
			rest: {
				method: "GET",
				path: "/device",
			},
			async handler(ctx) {
				const groups = await ctx.call("configuration.groups_devices.groups_of_device", {
					device_id: ctx.meta.device.id,
				});
				const groupsIds = groups.map((g) => {
					return g.id;
				});

				const services = await ctx.call("queue.service_groups.find", { query: { group_id: groupsIds } });
				const servicesIds = services.map((s) => {
					return s.service_id;
				});

				return ctx.call("queue.services.find", { query: { id: servicesIds, active: true }, withRelated: "subjects,translations" }).then(services => {
					return services.filter(s => s.subjects && s.subjects.length > 0);
				});
			}
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: false
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			}
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		update: {
			rest: "PUT /:id",
			visibility: "published",
			async handler(ctx) {
				let params = ctx.params;
				// Deactivate all subjects of a service
				if (params.active == false && params.active !== params.serviceObj.active) {
					for (const subject of await ctx.call("queue.subjects.find", {
						query: { service_id: params.id },
					})) {
						subject.active = false;
						await ctx.call("queue.subjects.disable", { subject: subject });
					}
					ctx.call("queue.tvs.removeByServiceId", { service_id: params.id });
				}
				params.serviceObj = null;
				return await this._update(ctx, params);
			},
		},

		inactivate: {
			rest: "POST /:id/inactivate",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},			
			async handler(ctx) {
				const subjects = await ctx.call("queue.subjects.find", {
					query: { service_id: ctx.params.id },
				});

				// Verify if subject has open tickects
				for (const item_subject of subjects) {
					const tickets = await ctx.call("queue.tickets.find", {
						query: { 
							subject_id: item_subject.id 
						},
					}
					);

					if (tickets.length > 0 ) {
						throw new Errors.ValidationError(
							"Subject with id " + item_subject.id + " has open tickets",
							"SUBJECT_HAS_OPEN_TICKETS",
						);
					}
				}

				// Disable subject and related
				for (const item_subject of subjects) {
					await ctx.call("queue.subjects.inactivate", { id: item_subject.id });
				}

				// Remove TV service associated with this service_id
				ctx.call("queue.tvs.removeByServiceId", { service_id: ctx.params.id });

				// Update service (inactivate)
				return await ctx.call("queue.services.patch", {
					id: ctx.params.id,
					active: false,
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"queue.services.*"() {
			this.clearCache();
		},
		"queue.subjects.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		async validateLanguages(ctx) {
			if (Array.isArray(ctx.params.translations)) {
				for (const translation of ctx.params.translations) {
					await ctx.call("configuration.languages.get", { id: translation.language_id });
				}
			}
		},
		async validateGroups(ctx) {
			if (Array.isArray(ctx.params.groups)) {
				for (const group of ctx.params.groups) {
					await ctx.call("configuration.groups.get", { id: group.group_id });
				}
			}
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} service_id
		 * @param {*} list
		 */
		async createWithRelated(ctx, service, service_id, list) {
			if (!(list instanceof Array)) { return []; }
			const listToReturn = [];
			for (const item of list) {
				item.service_id = service_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) { return; }
			for (const item of list) {
				await ctx.call(service, item);
			}
		}



	},


	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
