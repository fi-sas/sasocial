"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: "queue.subject_desk_operators",
	table: "subject_desk_operator",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "subject_desk_operators")],

	/**
	 * Settings
	 */
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"operator_id",
			"desk_id",
			"subject_id",
			"operating",
			"active",
		],
		defaultWithRelateds: [],
		withRelateds: {
			desk(ids, desks, rule, ctx) {
				return Promise.all(
					desks.map((subject_desk_operators) => {
						return ctx
							.call("queue.desks.find", { query: { id: subject_desk_operators.desk_id } })
							.then((res) => (subject_desk_operators.desk = res));
					}),
				);
			},
			operator(ids, operators, rule, ctx) {
				return Promise.all(
					operators.map((subject_desk_operators) => {
						return ctx
							.call("queue.desk_operators.find", {
								query: { id: subject_desk_operators.operator_id },
							})
							.then((res) => (subject_desk_operators.operator = res));
					}),
				);
			},
			subject(ids, subject, rule, ctx) {
				return Promise.all(
					subject.map((subject_desk_operators) => {
						return ctx
							.call("queue.subjects.getSubjectAndService", {
								subject_id: subject_desk_operators.subject_id,
							})
							.then((res) => (subject_desk_operators.subject = res));
					}),
				);
			},
		},
		entityValidator: {
			operator_id: { type: "number", positive: true, integer: true },
			desk_id: { type: "number", positive: true, integer: true },
			subject_id: { type: "number", positive: true, integer: true },
			operating: { type: "boolean", optional: true },
			active: { type: "boolean", default: true },
		},
	},

	hooks: {
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create: {
			rest: "POST /",
			visibility: "published",
			async handler(ctx) {
				let listSubjects = [];
				listSubjects = ctx.params.subjects;
				let resultList = [];
				
				await this.verify_validations(ctx);

				for (const item of listSubjects) {
					const assocService = await this._find(ctx, {
						query: {
							desk_id: ctx.params.desk_id,
							active: true,
						},
					});

					if (assocService.length !== 0) {
						const subjectExiste = await ctx.call("queue.subjects.find", {
							query: {
								id: assocService[0].subject_id,
							},
						});

						const subject = await ctx.call("queue.subjects.find", {
							query: {
								id: item.subject_id,
							},
						});

						if (subjectExiste[0].service_id !== subject[0].service_id) {
							throw new Errors.ValidationError(
								"This desk is associated with another service actives ",
								"DESK_IS_ASSOCIATED_ANOTHER_SERVICE",
								{
									service_id: subjectExiste.service_id,
								},
							);
						}
					}

					// Verify if desk and operator is already operating
					const associationAux = await this._find(ctx, {
						query: {
							operator_id: ctx.params.operator_id,
							desk_id: ctx.params.desk_id,
							operating: true,
						},
					});

					let operating_desk = false;

					if (associationAux.length !== 0) {
						operating_desk = true;
					}

					const association = await this._find(ctx, {
						query: {
							operator_id: ctx.params.operator_id,
							subject_id: item.subject_id,
							desk_id: ctx.params.desk_id,
						},
					});

					if (association.length !== 0) {
						association[0].active = true;
						association[0].operating = operating_desk;
						
						const updated_association = await ctx.call("queue.subject_desk_operators.update", association[0]);
						
						resultList.push(updated_association[0]);
					} else {
						ctx.params.subject_id = item.subject_id;
						ctx.params.operating = operating_desk;
						ctx.params.active = true;
						
						const associactionCreated = await this._create(ctx, ctx.params);
						
						resultList.push(associactionCreated[0]);
					}
				}

				return resultList;
			},
		},

		createAndInitAssociation: {
			//TODO falta iniciar o balcão ou nao se der erro
			rest: "POST /createAndInitAssociation",
			scope: "queue:subject_desk_operators:update",
			visibility: "published",
			async handler(ctx) {
				let listSubjects = [];
				listSubjects = ctx.params.subjects;
				let resultList = [];
				let resultListUpdated = [];
				await ctx.call("queue.desks.desk_close_association", { operator_id: ctx.params.operator_id});
				this.verify_validations(ctx);
				// Verify if exist association between operator an desk active
				const associationExist = await this._find(ctx, {
					query: {
						operator_id: ctx.params.operator_id,
						desk_id: ctx.params.desk_id,
					},
				});
				// Desactive association
				for (const subjects of listSubjects) {
					// Verify if desk as a active association
					const associationDesk = await this._find(ctx, {
						query: {
							desk_id: ctx.params.desk_id,
							active: true,
						},
					});
					if (associationDesk.length !== 0) {
						const subjectExiste = await ctx.call("queue.subjects.find", {
							query: {
								id: associationDesk[0].subject_id,
							},
						});
						const subject = await ctx.call("queue.subjects.find", {
							query: {
								id: subjects.subject_id,
							},
						});
						// Verify if existent association is the same service
						if (subjectExiste[0].service_id !== subject[0].service_id) {
							for (let subject of resultListUpdated) {
								subject.operating = false;
								await this._update(ctx, subject);
							}
							throw new Errors.ValidationError(
								"This desk is associated with another service actives ",
								"DESK_IS_ASSOCIATED_ANOTHER_SERVICE",
								{
									service_id: subjectExiste.service_id,
								},
							);
						}
					}
					for (const item of associationExist) {
						const exist = listSubjects.find((ele) => ele.subject_id === item.subject_id);
						if (exist) {
							item.active = true;
							item.operating = false;
							await this._update(ctx, item);
							resultList.push(item);
						} else {
							item.active = false;
							item.operating = false;
							await this._update(ctx, item);
						}
					}
					// Create association and init operation
					const exist = resultList.find((ele) => ele.subject_id === subjects.subject_id);
					if (!exist) {
						ctx.params.subject_id = subjects.subject_id;
						ctx.params.operating = false;
						const association = await this._create(ctx, ctx.params);
						resultList.push(association[0]);
					}
				}
				let resultListUnique = resultList.filter(function (elem, index, self) {
					return index === self.indexOf(elem);
				});
				if (resultListUnique.length !== 0) {
					for (let resultUpdate of resultListUnique) {
						resultUpdate.operating = true;
						let result = await this._update(ctx, resultUpdate);
						resultListUpdated.push(result[0]);
					}
				}
				let desk = await ctx.call("queue.desks.get", { id: ctx.params.desk_id });
				desk[0].actual_state = true;
				await ctx.call("queue.desks.update", desk[0]);
				await ctx.call("queue.hours_attendance_desks.create", {
					desk_id: parseInt(ctx.params.desk_id),
				});
				return resultListUpdated;
			},
		},

		update: {
			rest: "PUT /:id",
			visibility: "published",
			scope: "queue:subject_desk_operators:update",
			async handler(ctx) {
				const association = await this._find(ctx, { query: { id: ctx.params.id } });
				this.verify_validations(ctx);
				const subjectExiste = await ctx.call("queue.subjects.find", {
					query: {
						id: association[0].subject_id,
					},
				});
				const subject = await ctx.call("queue.subjects.find", {
					query: {
						id: ctx.params.subject_id,
					},
				});
				if (subjectExiste[0].service_id !== subject[0].service_id) {
					throw new Errors.ValidationError(
						"This desk is associated with another service actives ",
						"DESK_IS_ASSOCIATED_ANOTHER_SERVICE",
						{
							service_id: subjectExiste.service_id,
						},
					);
				} else {
					const association = await this._update(ctx, ctx.params);
					return await this._find(ctx, { query: { id: association[0].id } });
				}
			},
		},
		desactive: {
			rest: "POST /:id/desactive",
			scope: "queue:subject_desk_operators:update",
			visibility: "published",
			async handler(ctx) {
				let association = await this._find(ctx, { query: { id: ctx.params.id } });
				if (association.length === 0) {
					throw new Errors.EntityNotFoundError("Association", ctx.params.type);
				} else {
					if (association[0].operating === true) {
						throw new Errors.ValidationError(
							"This association is operating you can not modify.",
							"ASSOCIATION_IS_OPERATING",
							{},
						);
					} else {
						association[0].active = false;
						association[0].operating = false;
						return await this._update(ctx, association[0]);
					}
				}
			},
		},

		active: {
			rest: "POST /:id/active",
			scope: "queue:subject_desk_operators:update",
			visibility: "published",
			async handler(ctx) {
				let association = await this._find(ctx, { query: { id: ctx.params.id } });
				if (association.length === 0) {
					throw new Errors.EntityNotFoundError("Association", ctx.params.type);
				} else {
					const associationDesk = await this._find(ctx, {
						query: {
							desk_id: association[0].desk_id,
							active: true,
						},
					});
					if (associationDesk.length !== 0) {
						const subjectExiste = await ctx.call("queue.subjects.find", {
							query: {
								id: associationDesk[0].subject_id,
							},
						});
						const subject = await ctx.call("queue.subjects.find", {
							query: {
								id: association[0].subject_id,
							},
						});
						// Verify if existent association is the same service
						if (subjectExiste[0].service_id !== subject[0].service_id) {
							throw new Errors.ValidationError(
								"This desk is associated with another service actives ",
								"DESK_IS_ASSOCIATED_ANOTHER_SERVICE",
								{
									service_id: subjectExiste.service_id,
								},
							);
						}
					}
					if (association[0].operating === true) {
						throw new Errors.ValidationError(
							"This association is operating you can not modify.",
							"ASSOCIATION_IS_OPERATING",
						);
					} else {
						association[0].active = true;
						association[0].operating = false;
						return await this._update(ctx, association[0]);
					}
				}
			},
		},

		getAssociationSameUserAndDesk: {
			rest: "GET /sameUserDesk",
			scope: "queue:subject_desk_operators:read",
			visibility: "published",
			async handler(ctx) {
				// Group By Operator and Desk
				let resp = [];
				let AssociationByGroup = await this.adapter.getDB()
					.select("desk_id", "operator_id")
					.from("subject_desk_operator")
					.groupByRaw("desk_id, operator_id");

				for (let association of AssociationByGroup) {
					let subject_info_list = [];
					// Get info of Operator
					const operator_info = await ctx.call("queue.desk_operators.find", {
						query: { id: association.operator_id },
					});
					association.operator = operator_info;
					// Get desk of Operator
					const desk_info = await ctx.call("queue.desks.find", {
						query: { id: association.desk_id },
					});
					association.desk = desk_info;
					// Get all subjects of groupBY Operator and Desk
					let subjectlist = await this._find(ctx, {
						query: {
							desk_id: association.desk_id,
							operator_id: association.operator_id,
							active: true,
						},
					});
					if (subjectlist.length !== 0) {
						let subject_info = await ctx.call("queue.subjects.find", {
							query: { id: subjectlist[0].subject_id },
						});
						let service_info = await ctx.call("queue.services.find", {
							query: { id: subject_info[0].service_id },
						});
						association.service = service_info[0];
						let operating = false;
						let tickets_waiting = 0;
						let media_time = 0;
						for (let subject of subjectlist) {
							if (subject.operating === true) {
								operating = true;
							}
							let subjects_teste = await ctx.call("queue.subjects.find", {
								query: { id: subject.subject_id },
							});
							subject_info_list.push(subjects_teste[0]);
							const tickets = await ctx.call("queue.tickets.count", {
								query: {
									subject_id: subject.subject_id,
									status: "EM_ESPERA",
								},
							});
							tickets_waiting += tickets.length;
							media_time += subjects_teste[0].avg_time;
						}
						association.media_time = media_time / subjectlist.length;
						association.tickets_waiting = tickets_waiting;
						association.operating = operating;
						association.service.subjects = subject_info_list;
						resp.push(association);
					}
				}
				return resp;
			},
		},

		getNumberOfDesksAttend: {
			params: { subject_id: { type: "number" } },
			async handler(ctx) {
				let desksList = [];
				let resultList = [];
				// Get list of associations actives as operating by subject
				const associationList = await this._find(ctx, {
					query: {
						subject_id: ctx.params.subject_id,
						active: true,
						operating: true,
					},
				});
				// Get all desks presents in association list
				for (const association of associationList) {
					const desk_info = await ctx.call("queue.desks.get", { id: association.desk_id });
					desksList.push(desk_info[0]);
				}
				// Verify if desks is actives
				for (const desk of desksList) {
					if (desk.actual_state === true) {
						resultList.push(desk);
					}
				}
				for (const result of resultList) {
					let subjectsList = [];
					// Get list of associations actives as operating by desk
					let associations = await this._find(ctx, {
						query: {
							desk_id: result.id,
							active: true,
							operating: true,
						},
					});
					// Get info about subject
					for (const item of associations) {
						const subject_info = await ctx.call("queue.subjects.get", { id: item.subject_id });
						subjectsList.push(subject_info[0]);
					}
					// Add subjects to desk
					result.subjects = subjectsList;
				}
				return resultList;
			},
		},
		getTicktesByAssociation: {
			rest: "GET /information/tickets",
			scope: "queue:subject_desk_operators:read",
			visibility: "published",
			async handler(ctx) {
				let resp = [];
				const listSubjects = await ctx.call("queue.subjects.find", {
					query: {
						active: true,
					},
				});
				for (const subject of listSubjects) {
					const assoc = await this._find(ctx, {
						query: {
							subject_id: subject.id,
							active: true,
						},
					});
					let desks = [];
					for (const a of assoc) {
						const desk_info = await ctx.call("queue.desks.get", { id: a.desk_id });
						const verify_duplicated = desks.find((elm) => elm === desk_info[0].name);
						if (!verify_duplicated) {
							desks.push(desk_info[0].name);
						}
					}
					const tickets_waiting = await ctx.call("queue.tickets.count", {
						query: {
							subject_id: subject.id,
							status: "EM_ESPERA",
						},
					});
					const tickets_priority = await ctx.call("queue.tickets.count", {
						query: {
							subject_id: subject.id,
							status: "EM_ESPERA",
							type: "P",
						},
					});
					const tickets_appointment = await ctx.call("queue.tickets.count", {
						query: {
							subject_id: subject.id,
							status: "EM_ESPERA",
							type: "M",
						},
					});
					const tickest_attended = await this.adapter
						.getDB("ticket_history")
						.count("id")
						.where("subject_id", subject.id)
						.andWhere("created_at", ">=", new Date(this.formatDate(new Date()) + " " + "00:00:00"))
						.andWhere("created_at", "<", new Date(this.formatDate(new Date()) + " " + "23:59:59"));

					let response = {
						subject: subject,
						desks: desks,
						total_tickets: tickest_attended[0].count + tickets_waiting,
						tickets_waiting: tickets_waiting,
						tickets_priority: tickets_priority,
						tickets_appointment: tickets_appointment,
					};
					resp.push(response);
				}
				return resp;
			},
		},

		getAssociationOperating: {
			rest: "GET /association/operating/:operator_id",
			scope: "queue:subject_desk_operators:read",
			visibility: "published",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						operator_id: ctx.params.operator_id,
						operating: true,
					}, withRelated: ["desk"]
				});
			},
		},

		getCountBySubject: {
			rest: "GET /association/operating/:subject_id",
			scope: "queue:subject_desk_operators:read",
			visibility: "published",
			async handler(ctx) {
				return await this._count(ctx, {
					query: {
						subject_id: ctx.params.subject_id,
						operating: true,
					},
				});
			},
		},		

		addRemoveSubject: {
			rest: "POST /subject",
			scope: "queue:subject_desk_operators:update",
			visibility: "published",
			params: {
				subjects:
					[
						{ type: "array", item: { type: "number", positive: true, convert: true, optional: false } },
					],
				operator_id: { type: "number", positive: true, integer: true, optional: false},
				desk_id: { type: "number", positive: true, integer: true, optional: false },
				operating: { type: "boolean", optional: false },
			},			
			async handler(ctx) {
				await this.verify_validations(ctx);

				let resultList = [];

				let listSubjects = ctx.params.subjects;

				for (const item of listSubjects) {
					// Verify if association already exists
					const association = await this._find(ctx, {
						query: {
							operator_id: ctx.params.operator_id,
							subject_id: item.subject_id,
							desk_id: ctx.params.desk_id,
						},
					});

					// Patch or Create these association
					if (association.length !== 0) {
						const patch_association = await ctx.call("queue.subject_desk_operators.patch", { 
							id:  association[0].id, 
							operating: ctx.params.operating 
						});

						resultList.push(patch_association[0]);
					} else {
						ctx.params.operating = true;
						ctx.params.active = true;
						ctx.params.subject_id = item.subject_id;						

						const create_association = await this._create(ctx, ctx.params);

						resultList.push(create_association[0]);
					}
				}

				return resultList;
			},
		},
			
	},

	/**
	 * Events
	 */
	events: {
		"queue.subject_desk_operators.*"() {
			this.clearCache();
		},
		"queue.pauses.*"(){
			this.clearCache();
		},
		"queue.desks.*"(){
			this.clearCache();
		}
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		async verify_validations(ctx) {
			//Verify operator
			const operator = await ctx.call("queue.desk_operators.get", {
				id: ctx.params.operator_id,
			});
			if (operator.length === 0) {
				throw new Errors.EntityNotFoundError("operator_id", ctx.params.operator_id);
			}
			if (operator.length !== 0) {
				if (operator[0].active === false) {
					throw new Errors.ValidationError("Desk Operator not active", "DESK_OPERATOR_NOT_ACTIVE", {
						desk_operator: "operator_id",
					});
				}
			}
			// Verify desk
			const desk = await ctx.call("queue.desks.get", {
				id: ctx.params.desk_id,
			});
			if (desk.length === 0) {
				throw new Errors.EntityNotFoundError("desk_id", ctx.params.desk_id);
			}
			if (desk.length !== 0) {
				if (desk[0].active === false) {
					throw new Errors.ValidationError("Desk not active", "DESK_NOT_ACTIVE", {
						desk: "desk_id",
					});
				}
			}
			// verify subjects
			let subjectList = ctx.params.subjects;
			for (const item of subjectList) {
				const subject = await ctx.call("queue.subjects.get", {
					id: item.subject_id,
				});
				if (subject.length === 0) {
					throw new Errors.EntityNotFoundError("subject_id", item.desk_id);
				}
				if (subject.length !== 0) {
					if (subject[0].active === false) {
						throw new Errors.ValidationError("Subject not active", "SUBJECT_NOT_ACTIVE", {
							subject: "subject_id",
						});
					}
				}
			}
		},
		formatDate(date) {
			let d = new Date(date),
				month = "" + (d.getMonth() + 1),
				day = "" + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = "0" + month;
			if (day.length < 2) day = "0" + day;

			return [year, month, day].join("-");
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
