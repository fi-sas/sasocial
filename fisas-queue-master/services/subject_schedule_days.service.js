"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.subject_schedule_days",
	table: "subject_schedule_day",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "subject_schedule_day")],

	/**
	 * Settings
	 */

	settings: {
		fields: ["id", "description", "week_day", "subject_id"],
		defaultWithRelateds: [],
		withRelateds: {
			"schedules"(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "queue.subject_schedules", "schedules", "id", "subject_schedule_day_id");
			},
		},
		entityValidator: {
			id: { type: "number", optional: true },
			description: { type: "string", max: 50 },
			week_day: { type: "number" },
			subject_id: { type: "number" },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		getSchedulesPerWeekDay: {
			params: { subject_id: { type: "number" } },
			async handler(ctx) {
				const list = await this.adapter
					.getDB("subject_schedule_day")
					.where("subject_id", ctx.params.subject_id);
				for (const item of list) {
					await this.adapter
						.getDB("subject_schedule")
						.where("subject_schedule_day_id", item.id)
						.then((resp) => (item.schedules = resp));
				}
				return list;
			},
		},

		removeSubjectSchedules: {
			params: { subject_id: { type: "number" } },
			async handler(ctx) {
				const subject_schedule_days = await this.adapter
					.getDB("subject_schedule_day")
					.where("subject_id", ctx.params.subject_id);

				for (const week_day of subject_schedule_days) {
					const schedules_list = await this.adapter
						.getDB("subject_schedule")
						.where("subject_schedule_day_id", week_day.id);

					for (const schedule of schedules_list) {
						await ctx.call("queue.subject_schedules.remove", { id: schedule.id });
					}
					this.logger.info(week_day);
					await ctx.call("queue.subject_schedule_days.remove", week_day);
				}
			},
		},

		itsInsideSchedule: {
			//rest: "POST /itsInsideSchedule",
			//visibility: "published",
			params: {
				subject_id: { type: "number" },
				week_day: { type: "number" },
				date: { type: "date", convert: true },
				waiting: { type: "number" },
			},
			async handler(ctx) {
				ctx.params.date = moment(ctx.params.date).toDate();
				const scheduleDay = await this._find(ctx, {
					query: { subject_id: ctx.params.subject_id, week_day: ctx.params.week_day },
				});
				if (scheduleDay.length == 0) {
					throw new Errors.ValidationError("Invalid hour", "TICKET_INVALID_HOUR");
				}
				const currentHour = moment().format("HH:mm");
				const schedules = await this.adapter
					.getDB("subject_schedule")
					.where("subject_schedule_day_id", scheduleDay[0].id)
					.where("opening_hour", "<=", currentHour)
					.where("closing_hour", ">", currentHour)
					.orderBy("closing_hour", "asc");
				if (schedules.length == 0) {
					throw new Errors.ValidationError("Subject closed ", "SUBJECT_CLOSED");
				}

				// Ticket tirado antes da abertura da fila
				const response = await this.beforeOpeningHour(ctx, scheduleDay, ctx.params.waiting);

				if (new Date(response).getTime() != ctx.params.date.getTime()) {
					return response;
				}

   			    // Hora estimada de atendimento do ticket, fora do horário de funcionamento da fila
				await this.afterClosingHour(ctx, scheduleDay);

				// O horário de funcionamento não possui pausas de funcionamento (por exemplo, "9:00 ás 13:00" )
				if (schedules.length < 2) {
					return ctx.params.date;
				}
				const estimatedTime = await this.onBreakingHours(ctx, schedules);
				this.logger.info(new Date(estimatedTime));
				return estimatedTime;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"queue.subject_schedule_days.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		/**
		 * Returns ticket estimated time
		 * Verify if ticket have estimated time on a breaking hour (for example, lauching). If that occurs, estimated time is recalculated to being attended after breaking hour.
		 * @param {*} ctx
		 * @param {Number} schedules Subject working schedules
		 * @param {Number} waitingInSeconds Seconds that users need to wait to be called
		 */
		async onBreakingHours(ctx, schedules) {
			for (let index = 0; index < schedules.length - 1; index++) {
				const initialBreakHour = this.transformTimeInDate(schedules[index].closing_hour);
				const finalBreakHour = this.transformTimeInDate(schedules[index + 1].opening_hour);
				if (
					ctx.params.date.getTime() > initialBreakHour.getTime() &&
					ctx.params.date.getTime() < finalBreakHour.getTime()
				) {
					return (
						finalBreakHour.getTime() + (ctx.params.date.getTime() - initialBreakHour.getTime())
					);
				}
			}
			return ctx.params.date;
		},
		/**
		 * Returns ticket estimated time
		 * Verify if ticket as generated before queue opening hour. If that occurs, the estimated time is calculated only after opening hour.
		 *
		 * @param {*} ctx
		 * @param {Number} scheduleDay Week day identificator {Sun=0,Mon=1,Tues=2,Wed=3,Thurs=4,Fri=5,Sat=6}
		 * @param {Number} waitingInSeconds Seconds that users need to wait to be called
		 */
		async beforeOpeningHour(ctx, scheduleDay, waitingInSeconds) {
			const ticketEstimatedAttendence = ctx.params.date.getTime();
			const subjectSchedule = await this.adapter
				.getDB("subject_schedule")
				.where("subject_schedule_day_id", scheduleDay[0].id)
				.orderBy("opening_hour", "asc")
				.limit(1);
			const openingHour = this.transformTimeInDate(subjectSchedule[0].opening_hour);
			if (openingHour.getTime() > ticketEstimatedAttendence) {
				openingHour.setSeconds(openingHour.getSeconds() + waitingInSeconds);
				return openingHour;
			}
			return ticketEstimatedAttendence;
		},
		/**
		 *  Validate if ticket estimated time is out of queue schedule closing hour
		 * @param {*} ctx
		 * @param {Number} scheduleDay Week day identificator {Sun=0,Mon=1,Tues=2,Wed=3,Thurs=4,Fri=5,Sat=6}
		 */
		async afterClosingHour(ctx, scheduleDay) {
			const subjectSchedule = await this.adapter
				.getDB("subject_schedule")
				.where("subject_schedule_day_id", scheduleDay[0].id)
				.orderBy("closing_hour", "desc")
				.limit(1);
			const closingHour = this.transformTimeInDate(subjectSchedule[0].closing_hour);
			if (closingHour.getTime() < ctx.params.date.getTime()) {
				throw new Errors.ValidationError(
					"The queue is closed. Plase come back tomorrow ",
					"TICKET_AFTER_SCHEDULE",
				);
			}
		},
		/**
		 *
		 * @param {String} hour hour and minutes, for ex. "14:30"
		 */
		transformTimeInDate(hour) {
			const date = new Date();
			date.setHours(hour.toString().split(":")[0], hour.toString().split(":")[1], 0, 0);
			return date;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
