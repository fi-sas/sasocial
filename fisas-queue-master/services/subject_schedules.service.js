"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.subject_schedules",
	table: "subject_schedule",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "subject_schedule")],

	/**
	 * Settings
	 */

	settings: {
		fields: ["id", "opening_hour", "closing_hour", "subject_schedule_day_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			opening_hour: { type: "string", max: 6 },
			closing_hour: { type: "string", max: 6 },
			subject_schedule_day_id: { type: "number" },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"queue.subject_schedules.*"() {
			// this.logger.error("BROADCAST EVENT 2");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
