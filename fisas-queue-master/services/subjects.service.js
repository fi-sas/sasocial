"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.subjects",
	table: "subject",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "subjects")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"code",
			"avg_time",
			"max_tolerance",
			"appointments",
			"appointments_interval",
			"estimated_time",
			"service_id",
			"active",
			"created_at",
			"updated_at",
			"ongoing_ticket",
			"n_tickets",
			"last_desk",
			"waiting_time"
		],
		defaultWithRelateds: ["service", "schedule", "tickets_in_queue", "is_open", "user_open_tickets", "translations"],
		withRelateds: {
			schedule(ids, subjects, rule, ctx) {
				return Promise.all(
					subjects.map((subject) => {
						return ctx
							.call("queue.subject_schedule_days.getSchedulesPerWeekDay", {
								subject_id: subject.id,
							})
							.then((schedules) => {
								subject.schedule = schedules;
							});
					}),
				);
			},
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.services", "service", "service_id", "id");
			},
			"translations"(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("queue.subject_translations.find", { query: { subject_id: doc.id, language_id: ctx.meta.language_id } })
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return hasMany(docs, ctx, "queue.subject_translations", "translations", "id", "subject_id");
				}
			},
			"tickets_in_queue"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("queue.tickets.getTicketsInQueue", {
								subject: doc.id,
							})
							.then((count) => {
								doc.tickets_in_queue = count;
							});
					}),
				);
			},
			"is_open"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return this.isOpenNow(ctx, doc.id).then(x => doc.is_open = x);
					}),
				);
			},
			"user_open_tickets"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						if (!ctx.meta.isBackoffice && ctx.meta.user && !ctx.meta.isGuest)
							return this.haveOpenTickets(ctx, ctx.meta.user.id, doc.id).then(x => doc.user_open_tickets = x);
						return doc;
					}),
				);
			},

		},
		entityValidator: {
			code: { type: "string", max: 3 },
			schedule: {
				type: "array",
				item: {
					opening_hour: { type: "string", max: 6 },
					closing_hour: { type: "string", max: 6 },
					subject_schedule_day_id: { type: "number" },
				},
			},
			avg_time: { type: "number" },
			max_tolerance: { type: "number" },
			appointments: { type: "boolean" },
			appointments_interval: { type: "number", optional: true, nullable: true },
			estimated_time: { type: "number" },
			service_id: { type: "number" },
			active: { type: "boolean" },
			n_tickets: { type: "number", default: 1 },
			ongoing_ticket: { type: "string", optional: true },
			last_desk: { type: "string", optional: true },
			waiting_time: { type: "number", default: 160 },
			translations: {
				type: "array", min: 1,
				items: {
					type: "object", props: {
						name: "string|max: 100",
						language_id: "number|integer|convert",
						schedule_description: "string|max: 150",
					}
				}
			},
		},
	},
	/**
	 * hooks
	 */
	hooks: {
		before: {
			create: [
				"validateLanguages",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.avg_time = ctx.params.estimated_time;

					if (ctx.params.code) {
						// Valida se o código é único
						const sub = await this._find(ctx, { query: { code: ctx.params.code, active: true } });

						if (sub.length > 0) {
							throw new Errors.ValidationError(
								"Subject with code " + ctx.params.code + " already exists",
								"SUBJECT_ALREADY_EXISTS",
							);
						}
					}

					if (ctx.params.service_id) {
						// Valida se o Serviço associado está ativo
						const service = await ctx.call("queue.services.get", { id: ctx.params.service_id });
						if (service.length > 0 && !service[0].active) {
							throw new Errors.ValidationError(
								"Service with id " + ctx.params.service_id + " isn't active",
								"SUBJECT_SERVICE_NOT_ACTIVE",
							);
						}
					}
					// Valida se as horas estão no formato válido (hh:mm)
					this.validateHourFormat(ctx);
				},
			],
			update: [
				"validateLanguages",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					// Valida se a subject a editar está ativa
					const sub = await this._get(ctx, { id: ctx.params.id });
					if (!sub[0].active) {
						throw new Errors.ValidationError(
							"Subject with id " + ctx.params.id + " isn't active",
							"SUBJECT_NOT_ACTIVE",
						);
					}

					// Valida se o código foi alterado, e se o novo código é único
					const alreadyWithCode = await this._find(ctx, {
						query: { code: ctx.params.code, active: true },
					});
					if (sub[0].code != ctx.params.code && alreadyWithCode.length >= 1) {
						throw new Errors.ValidationError(
							"Subject with code " + ctx.params.code + " already exists",
							"SUBJECT_CODE_ALREADY_EXISTS",
						);
					}
					// Valida se as horas estão no formato válido (hh:mm)
					this.validateHourFormat(ctx);
				},
			],
			remove: [
				function preventDelete() {
					throw new Errors.ValidationError(
						"Delete operation is forbiden",
						"SUBJECT_DELETE_FORBIDEN",
					);
				},
			],
		},
		after: {
			create: [
				async function sanatizeParams(ctx, response) {
					response[0].translations = await this.createWithRelated(ctx, "queue.subject_translations.create", response[0].id, ctx.params.translations);
					return response;
				}
			],
			update: [
				async function sanatizeParams(ctx, response) {
					await this.deleteWithRelated(ctx, "queue.subject_translations.remove", await ctx.call("queue.subject_translations.find", { query: { subject_id: response[0].id } }));
					response[0].translations = await this.createWithRelated(ctx, "queue.subject_translations.create", response[0].id, ctx.params.translations);
					return response;
				}
			],
			patch: [
				async function sanatizeParams(ctx, response) {
					if (ctx.params.translations) {
						await this.deleteWithRelated(ctx, "queue.subject_translations.remove", await ctx.call("queue.subject_translations.find", { query: { subject_id: response[0].id } }));
						response[0].translations = await this.createWithRelated(ctx, "queue.subject_translations.create", response[0].id, ctx.params.translations);
					}
					return response;
				}
			]
		}

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: ["withRelated", "fields", "limit", "offset", "sort", "search", "searchFields", "query", "#user.id", "#language_id"]
			}
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			}
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		find: {
			cache: false
		},
		create: {
			rest: "POST /",
			visibility: "published",
			scope: "queue:subjects:create",
			async handler(ctx) {
				let params = ctx.params;
				let schedule = ctx.params.schedule;
				const subject = await this._create(ctx, params);
				for (const day of schedule) {
					day.subject_id = subject[0].id;
					const scheduleDay = await ctx.call("queue.subject_schedule_days.create", day);
					for (const hours of day.schedules) {
						hours.subject_schedule_day_id = scheduleDay[0].id;
						await ctx.call("queue.subject_schedules.create", hours);
					}
				}
				return subject;
			},
		},
		update: {
			rest: "PUT /:id",
			visibility: "published",
			scope: "queue:subjects:update",
			async handler(ctx) {
				let params = ctx.params;
				let schedule = ctx.params.schedule;
				const editedSub = await this._update(ctx, params);
				if (schedule && params.active) {
					await ctx.call("queue.subject_schedule_days.removeSubjectSchedules", {
						subject_id: editedSub[0].id,
					});
					for (const day of schedule) {
						day.subject_id = editedSub[0].id;
						const scheduleDay = await ctx.call("queue.subject_schedule_days.create", day);
						for (const hours of day.schedules) {
							hours.subject_schedule_day_id = scheduleDay[0].id;
							await ctx.call("queue.subject_schedules.create", hours);
						}
					}
				}
				return this._get(ctx, { id: editedSub[0].id, withRelated: ["schedule"] });
			},
		},
		getSubjectAndService: {
			params: { subject_id: { type: "number" } },
			async handler(ctx) {
				return await this._get(ctx, { id: ctx.params.subject_id }).then(async (subject) => {
					const service = await ctx.call("queue.services.get", { id: subject[0].service_id });
					subject[0].service = service;
					return subject;
				});
			},
		},
		disable: {
			params: { subject: { type: "object" } },
			async handler(ctx) {
				let params = ctx.params;
				const editedSub = await this._update(ctx, params.subject);
				await ctx.call("queue.subject_schedule_days.removeSubjectSchedules", {
					subject_id: editedSub[0].id,
				});
				return editedSub;
			},
		},

		// Disable subject and related
		inactivate: {
			rest: "POST /:id/inactivate",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				// Verify if subject has open tickects
				const tickets = await ctx.call("queue.tickets.find", {
					query: {
						subject_id: ctx.params.id
					},
				}
				);

				if (tickets.length > 0 ) {
					throw new Errors.ValidationError(
						"Subject with id " + ctx.params.id + " has open tickets",
						"SUBJECT_HAS_OPEN_TICKETS",
					);
				}

				// Disable subject and remove subject schedules
				const editedSub = await ctx.call("queue.subjects.patch", {
					id: ctx.params.id,
					active: false,
				});

				await ctx.call("queue.subject_schedule_days.removeSubjectSchedules", {
					subject_id: ctx.params.id,
				});

				// Remove subject desktop operators
				const subject_desk_operators = await ctx.call("queue.subject_desk_operators.find", {
					query: {
						subject_id: ctx.params.id
					},
				}
				);

				for (const item_desk_operators of subject_desk_operators) {
					await ctx.call("queue.subject_desk_operators.remove", {
						id: item_desk_operators.id,
					});
				}

  			    // Remove subject ticket history
				const ticket_histories = await ctx.call("queue.tickets_history.find", {
					query: {
						subject_id: ctx.params.id
					},
				}
				);

				for (const item_ticket_history of ticket_histories) {
					await ctx.call("queue.tickets_history.remove", {
						id: item_ticket_history.id,
					});
				}

				// Remove subject ticket queue
				const ticket_queues = await ctx.call("queue.tickets_queue.find", {
					query: {
						subject_id: ctx.params.id
					},
				}
				);

				for (const item_ticket_queue of ticket_queues) {
					await ctx.call("queue.tickets_queue.remove", {
						id: item_ticket_queue.id,
					});
				}

				return editedSub;
			},
		},
		appointments: {
			rest: "GET /:id/appointments",
			visibility: "published",
			params: { id: { type: "string" }, day: { type: "string" } },
			async handler(ctx) {
				ctx.params.day = new Date(parseInt(ctx.params.day));

				const subject = await ctx.call("queue.subjects.get", {
					id: ctx.params.id,
					withRelated: "schedule",
				});
				// Valida se a subject está ativa
				if (!subject[0].active) {
					throw new Errors.ValidationError(
						"Subject with id " + ctx.params.id + " is not active ",
						"SUBJECT_NOT_ACTIVE",
					);
				}
				const schedulesByWeekday = await subject[0].schedule.find(
					(x) => x.week_day === new Date(ctx.params.day).getDay(),
				);


				// Se não estiver aberto (sem horários para o dia indicado) ou não ceitar marcações
				if (schedulesByWeekday == null || subject[0].appointments == false) {
					return { appointments: false, hours: [] };
				} else {
					let validHours = await this.getValidHoursByDate(
						new Date(ctx.params.day),
						schedulesByWeekday,
						subject[0].appointments_interval,
						ctx.params.id,
						ctx,
					);
					return { appointments: true, hours: validHours };
				}
			},
		},
		getSubjectsByService: {
			params: { service_id: { type: "number" } },
			async handler(ctx) {
				const subjectsList = await this._find(ctx, {
					query: { service_id: ctx.params.service_id, active: true },
					withRelated: ["service"],
				});
				for (const sub of subjectsList) {
					if (sub.ongoing_ticket == null || (await this.restartTicketIncrementation(ctx, sub))) {
						sub.ongoing_ticket = "-";
						sub.last_desk = "-";
					}
				}
				return subjectsList;
			},
		},

		getOpenSubjectsByService: {
			rest: "GET /get_open_subjects_by_service",
			scope: "queue:subjects:read",
			visibility: "published",
			params: { 
				service_id: { type: "number", positive: true, convert: true, optional: false } 
			},
			async handler(ctx) {
				// Select all subjects
				const subjectsList = await this._find(ctx, {
					query: { 
						service_id: ctx.params.service_id, 
						active: true
					}, 
					sort: "-id",
					withRelated: ["is_open", "translations"],
				});

				let open_subjects = [];

				for (let subject of subjectsList) {
					// Filter by open subjects
					if (subject.is_open === true) {
						open_subjects.push(subject);
					}
				}

				return open_subjects;
			},
		},		

		/*isOpenNowAux: {
			rest: "POST /:id/isOpenNowAux",
			visibility: "published",
			params: { },
			async handler(ctx) {
				return this.isOpenNow(ctx, ctx.params.id);
			},
		},*/		

	},

	/**
	 * Events
	 */
	events: {
		"queue.subjects.*"() {
			this.clearCache();
		},
		"queue.subject_schedules.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async getValidHoursByDate(date, schedulesByWeekday, interval, subject_id, ctx) {
			let appointmentsValidHours = [];
			let iteratorDate = new Date(date);
			let limit_date = new Date(date);

			for (const iterator of schedulesByWeekday.schedules) {
				limit_date.setHours(
					iterator.closing_hour.toString().split(":")[0],
					iterator.closing_hour.toString().split(":")[1],
					0,
					0,
				);
				iteratorDate.setHours(
					iterator.opening_hour.toString().split(":")[0],
					iterator.opening_hour.toString().split(":")[1],
					0,
					0,
				);
				appointmentsValidHours = appointmentsValidHours.concat(
					await this.getIntervalsBetweenTwoDates(
						iteratorDate,
						limit_date,
						interval,
						subject_id,
						ctx,
					),
				);
			}

			const appointmentsList = await ctx.call("queue.tickets.getAppointmentsByDay", {
				subject_id: parseInt(subject_id),
				day: date.getTime(),
			});

			for (const appointment of appointmentsList) {
				if (
					appointmentsValidHours.findIndex(
						(x) => x.getTime() === appointment.appointment_schedule.getTime(),
					) != -1
				)
					appointmentsValidHours.splice(
						appointmentsValidHours.findIndex(
							(x) => x.getTime() === appointment.appointment_schedule.getTime(),
						),
						1,
					);
			}
			return appointmentsValidHours;
		},
		async getIntervalsBetweenTwoDates(initialDate, finalDate, interval, subject_id, ctx) {
			let list = [];
			let lastTicketTime = new Date();
			if (new Date().toLocaleDateString() === initialDate.toLocaleDateString()) {
				const lastTicket = await ctx.call("queue.tickets.lastTicket", {
					subject_id: parseInt(subject_id),
				});
				if (lastTicket.length > 0) {
					lastTicketTime = lastTicket[0].estimated_time;
				}
			}

			while (initialDate < finalDate) {
				if (initialDate > new Date() && initialDate > lastTicketTime) {
					list.push(new Date(initialDate));
				}
				initialDate.setMinutes(initialDate.getMinutes() + interval);
			}
			return list;
		},
		clearCache() {
			this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		async restartTicketIncrementation(ctx, subject) {
			let queues = await ctx.call("queue.tickets_queue.find", {
				query: { subject_id: subject.id },
			});
			const currdate = new Date();
			currdate.setHours(0, 0, 0, 0);
			for (const queue of queues) {
				if (queue.updated_at < currdate) {
					return true;
				}
			}
		},
		validateHourFormat(ctx) {
			for (const scheduleDays of ctx.params.schedule) {
				for (const schedule of scheduleDays.schedules) {
					const regex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;
					if (!regex.test(schedule.opening_hour) || !regex.test(schedule.closing_hour)) {
						throw new Errors.ValidationError(
							"Subject hour [" + schedule.opening_hour + " - " + schedule.closing_hour + " ] isn't valid",
							"SUBJECT_INVALID_HOURS",
						);
					}
				}
			}
		},
		async isOpenNow(ctx, subject_id) {
			const scheduleDay = await ctx.call("queue.subject_schedule_days.find", {
				query: { subject_id: subject_id, week_day: new Date().getDay() }, withRelated: ["schedules"]
			});
			if (scheduleDay.length == 0 || scheduleDay[0].schedules.length == 0) {
				return false;
			}
			for (const schedule of scheduleDay[0].schedules) {
				const opening_hour = this.transformTimeInDate(schedule.opening_hour);
				const closing_hour = this.transformTimeInDate(schedule.closing_hour);
				const current_date = new Date();
				if (current_date.getTime() >= opening_hour.getTime() && current_date.getTime() < closing_hour.getTime()) {
					return true;
				}
			}
			return false;

		},
		/**
		*
		* @param {String} hour hour and minutes, for ex. "14:30"
		*/
		transformTimeInDate(hour) {
			const date = new Date();
			date.setHours(hour.toString().split(":")[0], hour.toString().split(":")[1], 0, 0);
			return date;
		},
		async haveOpenTickets(ctx, user_id, subject_id) {
			return await ctx.call("queue.tickets.count", { query: { user_id: user_id, subject_id: subject_id, type: ["S", "P"] } }) > 0;
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} subject_id
		 * @param {*} list
		 */
		async createWithRelated(ctx, service, subject_id, list) {
			if (!(list instanceof Array)) { return []; }
			const listToReturn = [];
			for (const item of list) {
				item.subject_id = subject_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) { return; }
			for (const item of list) {
				await ctx.call(service, item);
			}
		},
		async validateLanguages(ctx) {
			if (Array.isArray(ctx.params.translations)) {
				for (const translation of ctx.params.translations) {
					await ctx.call("configuration.languages.get", { id: translation.language_id });
				}
			}
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
