"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const Cron = require("moleculer-cron");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.tickets",
	table: "ticket",
	/*
	 * Crons
	 */
	crons: [
		{
			name: "automatiCancelTickets",
			cronTime: "0 0 * * *",
			onTick: function () {
				this.logger.info("cron cancel tickets!");
				this.getLocalService("queue.tickets")
					.actions.cancel_tickets()
					.then(() => { });
			},
			runOnInit: function () {
			},
		},
	],

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "tickets"), Cron],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"sequencial_number",
			"order_number",
			"created_at",
			"estimated_time",
			"origin",
			"call_number",
			"initial_time",
			"final_time",
			"appointment_number",
			"appointment_schedule",
			"appointment_reason",
			"priority",
			"notes",
			"status",
			"subject_id",
			"email",
			"student_number",
			"type",
			"user_id",
			"attended_by_id",
			"attended_on",
			"call_times",
			"ticket_code",
		],
		defaultWithRelateds: ["subject", "attended_by"],
		withRelateds: {
			subject(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.subjects", "subject", "subject_id", "id");
			},
			attended_on(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.desks", "attended_on", "attended_on", "id");
			},
			attended_by(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "attended_by", "attended_by_id", "id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			id: { type: "number", optional: true },
			sequencial_number: { type: "number", optional: true, nullable: true },
			order_number: { type: "number", optional: true },
			estimated_time: { type: "date", convert: true, optional: true },
			origin: { type: "string", max: 10 },
			call_number: { type: "number", default: 0, optional: true },
			initial_time: { type: "date", optional: true, nullable: true, convert: true },
			final_time: { type: "date", optional: true, nullable: true, convert: true },
			appointment_number: { type: "number", optional: true, nullable: true },
			appointment_schedule: { type: "date", convert: true, optional: true, nullable: true },
			appointment_reason: { type: "string", max: 255, optional: true },
			priority: { type: "boolean", optional: true, default: false },
			notes: { type: "string", max: 200, optional: true },
			status: { type: "string", optional: true, default: 1 },
			subject_id: { type: "number" },
			email: { type: "string", max: 50, optional: true },
			student_number: { type: "string", max: 50, optional: true },
			type: { type: "string", max: 1, optional: true },
			user_id: { type: "number", optional: true, nullable: true },
			attended_by_id: { type: "number", optional: true, nullable: true },
			attended_on: { type: "number", optional: true, nullable: true },
			ticket_code: { type: "string", max: 25 },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			createSequentialTicket: ["validateSequetialTicketCreation"],
		    createAppointmentTicket: ["validateAppointmentCreation"],
			remove: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
				},
			],
			beginAttendence: [
				async function sanatizeParams(ctx) {
					const tickets = await this._find(ctx, {
						query: { attended_by_id: ctx.meta.user.id, status: "EM_ATENDIMENTO" },
					});

					if (tickets.length > 0) {
						throw new Errors.ValidationError(
							"Already have one attendence open",
							"OPERATOR_ALREADY_BUSY",
						);
					}
				},
			],
			finishAttendence: [
				async function sanatizeParams(ctx) {
					const ticket = await this._get(ctx, { id: ctx.params.id });
					
					if (ticket[0].status != "EM_ATENDIMENTO") {
						throw new Errors.ValidationError(
							"Ticket need to start atendence to finish them",
							"INVALID_TICKET_STATUS",
						);
					}

					if (ticket[0].attended_by_id != ctx.meta.user.id) {
						throw new Errors.ValidationError(
							"This ticket is not attended by you",
							"UNAUTHORIZED_USER_CHANGE",
						);
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				let tickets = await this._find(ctx, {
					query: (qb) =>
						qb.whereRaw("user_id = " + ctx.meta.user.id + " and type in ('S','P') limit 2"),
				});

				if (tickets.length < 2) {
					const appointments = await this._find(ctx, {
						query: (qb) =>
							qb.whereRaw(
								"user_id = " + ctx.meta.user.id + " and type = 'M' limit " + (2 - +tickets.length),
							),
					});
					
					tickets = tickets.concat(appointments);
				}
				
				if (tickets.length > 0) {
					return tickets;
				} else {
					throw new Errors.ValidationError("User dont have any tickets", "QUEUE_USER_NO_TICKETS");
				}
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		// Action Cancel Tickets By Job
		cancel_tickets: {
			visibility: "public",
			async handler(ctx) {
				const tickets = await this._find(ctx, {
					query: (qb) => {
						qb.where((qb1) =>
							qb1
								.whereIn("type", ["S", "P"])
								.andWhere("created_at", "<=", new Date()),
						);
						qb.orWhere((qb2) =>
							qb2
								.where("type", "=", "M")
								.andWhere("appointment_schedule", "<=", new Date()),
						);
					},
				});

				for (const ticket of tickets) {
					await this._remove(ctx, { id: ticket.id });
					
					ticket.status = "CANCELADO";
					ticket.id = undefined;
					
					await ctx.call("queue.tickets_history.create", ticket);
				}
			}
		},

		// Marcações
		createAppointmentTicket: {
			rest: "POST /appointment",
			visibility: "published",
			params: {
				appointment_schedule: { type: "date", convert: true },
				subject_id: { type: "number" },
				appointment_reason: { type: "string", optional: true, max: 255 },
			},
			async handler(ctx) {
				let params = ctx.params;

				const attendance_day = moment(params.appointment_schedule).format("YYYY-MM-DD");

				// Obter o número de marcações existentes para aquele dia, de modo a saber o seu número sequencial
				params.appointment_number = await this.getTicketNumber(ctx, params.type, params.subject_id, attendance_day);

				params.estimated_time = params.appointment_schedule;

				// Adicionar dados de email/número/id/telefone de estudante presentes no token
				params.email = ctx.meta.user.email;
				params.student_number = ctx.meta.user.student_number;
				params.user_id = ctx.meta.user.id;
				params.phone = ctx.meta.user.phone;

				const subject = await ctx.call("queue.subjects.get", { id: params.subject_id });

				params.ticket_code = this.getTicketConcatedNumber(
					params.appointment_number,
					params.type,
					subject[0].code,
				);
				
				const appointment = await this._insert(ctx, { entity: params });

				this.updateTicketNumber(ctx, params.type, params.subject_id, params.appointment_number, attendance_day);

				// Send notification with service_name, subject code, appointment_schedule, issue_date and ticket_code
				if (subject[0].service && subject[0].service.translations) {
					const service_translation = subject[0].service.translations.find(
						(x) => x.language_id == 3,
					);

					subject[0].service.name = service_translation.name;
				}

				await ctx.call("notifications.alerts.create_alert", {
					alert_type_key: "QUEUE_APPOINTMENT_INFORMATION",
					user_id: params.user_id,
					user_data: { email: params.email, phone: params.phone },
					data: { 
						ticket_code: params.ticket_code, 
						service_id: subject[0].service.name, 
						subject_id: subject[0].code, 
						appointment_schedule: moment(params.estimated_time).format("YYYY-MM-DD HH:mm"), 
						issue_date: moment().format("YYYY-MM-DD HH:mm"),
					 },
					variables: {},
					external_uuid: null,
					medias: [],
				});			
				
				return appointment;
			},
		},
		// Sequenciais
		createSequentialTicket: {
			rest: "POST /sequential",
			visibility: "published",
			params: {
				subject_id: { type: "number", positive: true },
				priority: { type: "boolean" },
			},
			async handler(ctx) {
				let params = ctx.params;
				
				const desks = 1;
				
				// this.logger.info(await ctx.call("queue.subject_desk_operators.getNumberOfDesksAttend", { subject_id: params.subject_id }));
				//TODO: calculate how many desks are active to this subject
				let waitingInSeconds = 0;
				
				// Todos os tickets sequenciais já tirados
				let ticketsinQueue = await this._count(ctx, {
					query: {
						subject_id: params.subject_id,
						appointment_number: null,
						appointment_schedule: null,
						status: "EM_ESPERA",
					},
				});
				
				const subject = await ctx.call("queue.subjects.get", { id: params.subject_id });
				
				let appointmentsInQueue = [];
				
				const attendance_day = moment().format("YYYY-MM-DD");

				if (params.priority) {
					params.sequencial_number = await this.getTicketNumber(ctx, "P", params.subject_id, attendance_day);
					
					// Calcula o tempo estimado de um ticket prioritário. Se nãoi houver outros priotários, o tempo estimado é o tempo da criação. Se houver, adiciona-se o tempo médio do numerod e prioritários ja existentes
					const priorityList = await this._count(ctx, {
						query: { subject_id: params.subject_id, priority: true, status: "EM_ESPERA" },
					});
					
					params.estimated_time = new Date(
						new Date().getTime() + subject[0].avg_time * Number.parseInt(priorityList) * 1000,
					);
					
					ticketsinQueue = 1 + Number.parseInt(priorityList);
					waitingInSeconds = subject[0].avg_time * Number.parseInt(priorityList);
				} else {
					params.sequencial_number = await this.getTicketNumber(ctx, "S", params.subject_id, attendance_day);
					let estimatedWaiting = (parseInt(ticketsinQueue) * parseInt(subject[0].avg_time)) / desks;
					let t = new Date();
					t.setSeconds(t.getSeconds() + parseInt(estimatedWaiting));
					
					// Todas as marcações previstas antes da hora prevista de atendimento
					appointmentsInQueue = Array.from(
						await this.adapter
							.getDB("ticket")
							.whereRaw(
								"subject_id = ? and  status=?  and appointment_schedule is not null and appointment_schedule < ? ",
								[params.subject_id, "EM_ESPERA", t],
							),
					);
					
					if (appointmentsInQueue.length > 0) {
						t = new Date();
						estimatedWaiting =
							((parseInt(ticketsinQueue) + appointmentsInQueue.length) *
								parseInt(subject[0].avg_time)) /
							desks;
						t.setSeconds(t.getSeconds() + parseInt(estimatedWaiting));
					}

					waitingInSeconds = estimatedWaiting;
					params.estimated_time = moment(t).isBefore(moment()) ? new Date() : t;
				}
				
				// Valida se está dentro dos horários de funcionamento da fila
				params.estimated_time = await ctx.call("queue.subject_schedule_days.itsInsideSchedule", {
					subject_id: params.subject_id,
					week_day: new Date().getDay(),
					date: params.estimated_time,
					waiting: waitingInSeconds,
					avgTime: subject[0].avg_time,
				});

				if (params.origin === "web" || params.origin === "mobile") {
					params.email = ctx.meta.user.email;
					params.student_number = ctx.meta.user.student_number;
					params.user_id = ctx.meta.user.id;
				}

				params.ticket_code = this.getTicketConcatedNumber(
					params.sequencial_number,
					params.type,
					subject[0].code,
				);
				
				const ticket = await this._insert(ctx, { entity: params });
				
				this.updateTicketNumber(ctx, params.type, params.subject_id, params.sequencial_number, attendance_day);
				
				return {
					ticket: ticket[0],
					ntickets: parseInt(ticketsinQueue) + appointmentsInQueue.length,
				};
			},
		},
		// Cancelar um ticket (sequencial/marcação)
		remove: {
			rest: "DELETE /:id",
			visibility: "published",
			scope: null,
			params: {
				id: { type: "string" },
			},
			async handler(ctx) {
				const ticket = await this._get(ctx, { id: ctx.params.id });
				
				if (ticket[0].origin == "quiosque") {
					throw new Errors.ValidationError("Invalid ticket type", "INVALID_TICKET_TYPE");
				}
				
				if (ticket[0].user_id != ctx.meta.user.id) {
					throw new Errors.ValidationError("Unauthorized operation", "UNAUTHORIZED_TICKET_CANCEL");
				}
				
				if (ticket[0].status == "EM_ATENDIMENTO") {
					throw new Errors.ValidationError("Unauthorized operation", "UNAUTHORIZED_TICKET_CANCEL");
				}
				
				ticket[0].status = "CANCELADO";
				ticket[0].id = undefined;
				
				await ctx.call("queue.tickets_history.create", ticket[0]);
				
				await this._remove(ctx, { id: ctx.params.id });
				
				return true;
			},
		},
		
		// Listar os tickets em fila
		getQueues: {
			rest: "GET /queue",
			visibility: "published",
			scope: "queue:tickets:read",
			params: {
				subjects:
					[
						{ type: "array", item: { type: "number", positive: true, convert: true, optional: true } },
						{ type: "number", positive: true, convert: true, optional: true }
					],
				desk_id: { type: "number", positive: true, convert: true },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
			},
			async handler(ctx) {
				ctx.params.sort = "estimated_time";
				ctx.params.withRelated = ["subject"];
				ctx.params.query = ctx.params.query || {};
				ctx.params.query.subject_id = ctx.params.subjects;
				
				delete ctx.params.subjects;
				
				ctx.params.query.status = ["EM_ESPERA", "CHAMADO", "NAO_COMPARECEU", "EM_ATENDIMENTO"];

				ctx.params.extraQuery = (qb) => {
					qb
						.where(qb1 => {
							qb1.where(this.adapter.raw("\"appointment_schedule\"::date"), "<=", moment(new Date(), "YYYY-MM-DD").endOf("day"));
							qb1.orWhereNull("appointment_schedule");
						})
					    // Exclude tickets that are ongoing in other desks
						.whereNotIn(
							"id",
							qb => qb
								.select("id")
								.whereIn("status", ["CHAMADO", "EM_ATENDIMENTO"])
								.andWhere("attended_on", "<>", ctx.params.desk_id)
						  )
					;
				};

				let params = this.sanitizeParams(ctx, ctx.params, true);

				return this._list(ctx, params);
			},
		},

		// Listar os todos os tickets com filtros
		getTickectsAndSubjects: {
			rest: "GET /tickets_subjects",
			visibility: "published",
			scope: "queue:tickets:read",
			params: {
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
				service_id: { type: "number", positive: true, convert: true, optional: true },
				subject_id: { type: "number", positive: true, convert: true, optional: true },
				desk_id: { type: "number", positive: true, convert: true, optional: true },
				operator_id: { type: "number", positive: true, convert: true, optional: true },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
			},
			async handler(ctx) {
				ctx.params.sort = "-id";
				ctx.params.withRelated = ["subject", "attended_by", "user"];
				ctx.params.query = ctx.params.query || {};

				if (ctx.params.subject_id !== null && ctx.params.subject_id !== undefined) {
					ctx.params.query.subject_id = ctx.params.subject_id;
				}

				if (ctx.params.desk_id !== null && ctx.params.desk_id !== undefined) {
					ctx.params.query.attended_on = ctx.params.desk_id;
				}

				if (ctx.params.operator_id !== null && ctx.params.operator_id !== undefined) {
					ctx.params.query.attended_by_id = ctx.params.operator_id;
				}

				if ( (ctx.params.start_date !== null && ctx.params.start_date !== undefined) ||
					 (ctx.params.end_date !== null && ctx.params.end_date !== undefined) ||
					 (ctx.params.service_id !== null && ctx.params.service_id !== undefined) ) {
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.start_date !== null && ctx.params.start_date !== undefined) {
							qb
								.where(qb1 => {
									qb1.where(this.adapter.raw("\"estimated_time\"::date"), ">=", moment(ctx.params.start_date).toDate());
									qb1.orWhereNull("estimated_time");
								});
						}

						if (ctx.params.end_date !== null && ctx.params.end_date !== undefined) {	
							qb
								.where(qb1 => {
									qb1.where(this.adapter.raw("\"estimated_time\"::date"), "<=",  moment(ctx.params.end_date).toDate());
									qb1.orWhereNull("estimated_time");
								});							
						}

						if (ctx.params.service_id !== null && ctx.params.service_id !== undefined) {
							qb
								.whereIn(
									"subject_id",
									qb1 => qb1
										.from("subject")
										.select("id")
										.where("service_id", "=", ctx.params.service_id)
								);
						}
					};
				}

				let params = this.sanitizeParams(ctx, ctx.params, true);

				return this._list(ctx, params);
			},
		},		

		// Numero de tickets em uma fila
		getTicketsInQueue: {
			visibility: "published",
			params: { subject: { type: "number" } },
			async handler(ctx) {
				const ticketList = await this.adapter
					.getDB("ticket")
					.where("subject_id", ctx.params.subject)
					.whereIn("status", ["EM_ESPERA", "CHAMADO", "NAO_COMPARECEU"])
					.whereIn("type", ["S", "P"])
					.orderBy("estimated_time", "asc");
				
				return ticketList.length;
			},
		},

		// Chamar um tiket para atendimento
		callTicket: {
			rest: "PATCH /:id/call",
			visibility: "published",
			params: { id: { type: "number", convert: true }, desk: "number|convert" },
			async handler(ctx) {
				const deskOperator = await this.validateOperator(ctx);
				
				const desk = await ctx.call("queue.desks.get", { id: ctx.params.desk });
				
				let ticket = await this._get(ctx, { id: ctx.params.id });
				
				ticket[0].status = "CHAMADO";
				ticket[0].call_number = ticket[0].call_number + 1;
				ticket[0].attended_on = desk[0].id;
				ticket[0].attended_by_id = ctx.meta.user.id;
				
				if (ticket[0].call_times) {
					ticket[0].call_times = ticket[0].call_times.concat([new Date()]);
				} else {
					ticket[0].call_times = [new Date()];
				}
				
				ticket = await this._update(ctx, ticket[0], true);

				ctx.meta.language_id = 3;
				
				const subject = await ctx.call("queue.subjects.find", {
					query: { id: ticket[0].subject_id },
					withRelated: "translations,service",
				});

				await ctx.call("queue.subjects.patch", {
					id: subject[0].id,
					ongoing_ticket: ticket[0].ticket_code,
					last_desk: desk[0].name,
				});

				ctx.call("api.broadcastMessage", {
					event: "QUEUE_NEXT_TICKET",
					args: [
						{
							ticket: ticket[0].ticket_code,
							desk: desk[0].name,
							operator: ctx.meta.user.name,
							photo: deskOperator.show_photo ? ctx.meta.user.picture : null,
							subject_name: subject[0].translations[0].name,
							service_name: subject[0].service.translations[0].name,
						},
					],
				});

				if (ticket[0].user_id) {
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "QUEUE_TICKET_CALLED",
						user_id: ticket[0].user_id,
						user_data: { email: null, phone: null },
						data: { ticket_number: ticket[0].ticket_code, desk: desk[0].name },
						variables: {},
						external_uuid: null,
						medias: [],
					});
				}

				return ticket;
			},
		},
		// Iniciar um atendimento
		beginAttendence: {
			rest: "PATCH /:id/begin",
			visibility: "published",
			params: { id: { type: "string" } },
			async handler(ctx) {
				const deskOperator = await this.validateOperator(ctx);

				const ticket = await this._get(ctx, { id: ctx.params.id });
				
				if (ticket[0].status !== "CHAMADO") {
					throw new Errors.ValidationError(
						"This ticket never is called",
						"TICKET_HAS_NOT_CALLED",
					);
				}

				// Start - Send notification of attendance approach
				//
				// Get subjects by operator id
				const subjects = await ctx.call("queue.subject_desk_operators.getAssociationOperating", {
					operator_id: deskOperator.id,
				});

				let subjectIdsList = [];

				for (const item_subject of subjects) {
					subjectIdsList.push(item_subject.subject_id);
				}

				// Get last 3 tickets from all subjects and desk
				const all_tickets = await ctx.call("queue.tickets.getQueues", {
					subjects: subjectIdsList,
					desk_id: ticket[0].attended_on,
					limit: 3,
				});

				let ticket_first_than_yours = 0;
				let attendance_time = 0;
				let avg_time_sum = 0;

				for (const item_ticket of all_tickets.rows) {
					// Accumulate subject time
					avg_time_sum = avg_time_sum + item_ticket.subject.avg_time;
					
					// Exclude ongoing ticket (the first one)
					if (ticket_first_than_yours === 0) {
						ticket_first_than_yours++;
						continue;
					}

					// Calculates attendance time estimate
					attendance_time = new Date(new Date().getTime() + avg_time_sum * 1000, );

					// Send approach notification
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "QUEUE_ATTENDANCE_APPROACH",
						user_id: item_ticket.user_id,
						user_data: { email: item_ticket.email },
						data: { 
							ticket_code: item_ticket.ticket_code,
							attendance_time: moment(attendance_time).format("HH:mm") + "h", 
							ticket_first_than_yours: ticket_first_than_yours,
						 },
						variables: {},
						external_uuid: null,
						medias: [],
					});

					ticket_first_than_yours++;
				}
				// End - Send notification of attendance approach

				// Update ticket to ongoing status
				ticket[0].attended_by_id = ctx.meta.user.id;
				ticket[0].status = "EM_ATENDIMENTO";
				ticket[0].initial_time = new Date();
				
				return await this._update(ctx, ticket[0]);
			},
		},
		// Terminar um atendimento
		finishAttendence: {
			rest: "PATCH /:id/end",
			visibility: "published",
			params: {
				id: { type: "string" },
				notes: { type: "string", nullable: true, optional: true },
			},
			async handler(ctx) {
				this.validateOperator(ctx);
				
				const ticket = await this._get(ctx, { id: ctx.params.id });
				
				ticket[0].status = "ATENDIDO";
				ticket[0].final_time = new Date();
				ticket[0].attended_by_id = ctx.meta.user.id;
				
				if (ctx.params.notes != null) {
					ticket[0].notes = ctx.params.notes;
				}
				
				ticket[0].id = undefined;
				
				await ctx.call("queue.tickets_history.create", ticket[0]);
				
				await this._remove(ctx, { id: ctx.params.id });
				
				await this.updateAttendenceAvgTime(ctx, ticket[0]);
				
				return true;
			},
		},
		// Cancelar um ticket pq o utilizador não apareceu
		didntCome: {
			rest: "PATCH /:id/didntCome",
			visibility: "published",
			params: { id: { type: "string" } },
			async handler(ctx) {
				this.validateOperator(ctx);
				
				let ticket = await this._get(ctx, { id: parseInt(ctx.params.id) });
				
				ticket[0].status = "NAO_COMPARECEU";
				ticket[0].attended_on = null;
				ticket[0].attended_by_id = null;
				ticket[0].id = undefined;
				
				await ctx.call("queue.tickets_history.create", ticket[0]);
				
				await this._remove(ctx, { id: parseInt(ctx.params.id) });
				
				return true;
			},
		},
		lastTicket: {
			params: { subject_id: { type: "number" } },
			async handler(ctx) {
				return await this.adapter
					.getDB("ticket")
					.where({
						subject_id: ctx.params.subject_id,
						status: "EM_ESPERA",
						appointment_schedule: null,
					})
					.orderBy("estimated_time", "desc")
					.limit(1);
			},
		},
		//Lista de marcações para um determinado dia
		getAppointmentsByDay: {
			params: { subject_id: { type: "number" }, day: { type: "date", convert: true } },
			async handler(ctx) {
				let beginDay = new Date(ctx.params.day);
				
				if (new Date(ctx.params.day).toLocaleDateString === new Date().toLocaleDateString) {
					beginDay = new Date();
				}
				
				let endDay = new Date(ctx.params.day);
				endDay.setHours(23, 59, 59, 999);
				
				return await this.adapter
					.getDB("ticket")
					.where({ subject_id: ctx.params.subject_id, status: "EM_ESPERA" })
					.andWhereBetween("appointment_schedule", [beginDay, endDay]);
			},
		},
		//Lista de tickets de um utilizador (loggedIn)
		getTicketsByUser: {
			rest: "GET /user/:type",
			visibility: "published",
			params: {
				type: { type: "string" },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
			},
			async handler(ctx) {
				let ticketList;
				
				const pag = {
					page: ctx.params.page,
					pageSize: ctx.params.pageSize,
					limit: ctx.params.limit,
					offset: ctx.params.offset,
					sort: ctx.params.sort,
				};
				
				if (ctx.params.type == "S" || ctx.params.type == "P") {
					ticketList = await this._list(ctx, {
						...pag,
						query: { user_id: ctx.meta.user.id, type: ["S", "P"] },
					});
				} else {
					ticketList = await this._list(ctx, {
						...pag,
						query: { user_id: ctx.meta.user.id, type: ["M"] },
					});
				}
				
				if (ctx.params.type == "S") {
					for (const ticket of ticketList.rows) {
						const desks = 1;
						
						const list = await this.adapter
							.getDB("ticket")
							.where({
								subject_id: ticket.subject_id,
								appointment_number: null,
								appointment_schedule: null,
								status: "EM_ESPERA",
							})
							.andWhere("sequencial_number", "<", ticket.sequencial_number);
						
						let ticketsinQueue = list.length;
						
						const subject = await ctx.call("queue.subjects.get", { id: ticket.subject_id });
						
						let estimatedWaiting =
							(parseInt(ticketsinQueue) * parseInt(subject[0].avg_time)) / desks;
						
						let estimated_time = new Date();
						estimated_time.setSeconds(estimated_time.getSeconds() + parseInt(estimatedWaiting));
						
						// Todas as marcações previstas antes da hora prevista de atendimento
						const appointmentsInQueue = Array.from(
							await this.adapter
								.getDB("ticket")
								.whereRaw(
									"subject_id = ? and  status=?  and appointment_schedule is not null and appointment_schedule < ? ",
									[ticket.subject_id, "EM_ESPERA", estimated_time],
								),
						);

						if (appointmentsInQueue.length > 0) {
							estimated_time = new Date();
							estimatedWaiting =
								((parseInt(ticketsinQueue) + appointmentsInQueue.length) *
									parseInt(subject[0].avg_time)) /
								desks;
							estimated_time.setSeconds(estimated_time.getSeconds() + parseInt(estimatedWaiting));
						}

						ticket.estimated_time = estimated_time;
						
						await ctx.call("queue.tickets.patch", {
							id: ticket.id,
							estimated_time: estimated_time,
						});

						ticket.tickets_in_queue = +ticketsinQueue + 1 + appointmentsInQueue.length;
					}
				}

				return ticketList;
			},
		},
		// Tranferir um ticket de fila
		transferTicketToAnotherQueue: {
			rest: "PUT /:old_ticket_id/transfer",
			visibility: "published",
			params: {
				subject_id: { type: "number", convert: true },
				old_ticket_id: { type: "number", convert: true }
			},
			async handler(ctx) {
				const old_ticket = await this._get(ctx, { id: ctx.params.old_ticket_id });

				// Cria um ticket na nova fila
				let newTicket = await ctx.call("queue.tickets.createSequentialTicket", {
					origin: "quiosque",
					subject_id: ctx.params.subject_id,
					priority: false,
				});
				
				newTicket = newTicket.ticket;
				
				// actualiza as informações do utilizador no ticket
				newTicket.user_id = old_ticket[0].user_id;
				newTicket.student_number = old_ticket[0].student_number;
				newTicket.email = old_ticket[0].email;
				newTicket = await this._update(ctx, newTicket);

				// Dá como conluido o ticket na fila actual
				let hasAttended = old_ticket[0].status === "ATENDIDO";
				
				old_ticket[0].status = "TRANSFERIDO";
				old_ticket[0].final_time = new Date();
				old_ticket[0].attended_by_id = ctx.meta.user.id;
				old_ticket[0].id = undefined;
				
				await ctx.call("queue.tickets_history.create", old_ticket[0]);
				
				await this._remove(ctx, { id: ctx.params.old_ticket_id });

				if (hasAttended)
					await this.updateAttendenceAvgTime(ctx, old_ticket[0]);

				return newTicket;
			},
		},
		// Broadcast desk notification
		broadcastDeskNotification: {
			rest: "POST /broadcastDeskNotification",
			visibility: "published",
			params: { 
				notification_action: { type: "enum", values: ["CLOSE_DESK", "FASTER_ATTENDANCE"], optional: false, nullable: false },
				desk_id: { type: "number", positive: true, convert: true, optional: false, nullable: false },
			},
			async handler(ctx) {
				const deskOperator = await this.validateOperator(ctx);

				// Get subjects by operator id
				const subjects_by_operator = await ctx.call("queue.subject_desk_operators.getAssociationOperating", {
					operator_id: deskOperator.id,
				});

				let subjectIdsList = [];
				
				for (const item_subject_by_operator of subjects_by_operator) {
					subjectIdsList.push(item_subject_by_operator.subject_id);
				}

				// Get all tickets from all subjects and desk
				const all_tickets = await ctx.call("queue.tickets.getQueues", {
					subjects: subjectIdsList,
					desk_id: ctx.params.desk_id,
				});

				for (const item_ticket of all_tickets.rows) {
					// If it is an ongoing ticket don´t send notifications
					if (item_ticket.status === "EM_ATENDIMENTO") {
						continue;
					}

					let alert_type_key = "";

					if (ctx.params.notification_action === "CLOSE_DESK") {
						// If it is to close the desk and if it is possible to attend the ticket in another desk 
						// (because the same ticket appears in another desk) don´t send notifications
						// Get subject_desk_operators by subject_id
						const count_subject_desk_operators_by_subject = await ctx.call("queue.subject_desk_operators.getCountBySubject", {
							subject_id: item_ticket.subject_id,
						});

						if (count_subject_desk_operators_by_subject > 1) {
							// Means that more than one desk is attending the same ticket
							continue;
						}

						// Send close desk notification
						alert_type_key = "QUEUE_CLOSE_DESK";
					} else {
						// Send faster attendance notification
						alert_type_key = "QUEUE_FASTER_ATTENDANCE";
					}

					// Get desk_name
					const desk = await ctx.call("queue.desks.get", { id: ctx.params.desk_id });

					// Send close desk notification or faster attendance notification
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: alert_type_key,
						user_id: item_ticket.user_id,
						user_data: { email: item_ticket.email },
						data: { 
							desk_name: desk[0].name, 
						},
						variables: {},
						external_uuid: null,
						medias: [],
					});
				}
				
				return true;
			},
		},

	},

	/**
	 * Events
	 */
	events: {
		"queue.tickets.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {

		/**
		 * formate date
		 * @param {*} date
		 * @returns
		 */
		formatDate(date) {
			let d = new Date(date),
				month = "" + (d.getMonth() + 1),
				day = "" + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = "0" + month;
			if (day.length < 2) day = "0" + day;

			return [year, month, day].join("-");
		},

		clearCache() {
			// this.logger.info(`Cache cleaned ... ${this.fullName}.*`);
			// this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			
			return Promise.resolve();
		},

		/**
		 *  Validate tickets creation (origin)
		 * @param {*} ctx
		 */
		async validateSequetialTicketCreation(ctx) {
			ctx.params.created_at = new Date();
			ctx.params.status = "EM_ESPERA";
			ctx.params.call_number = 0;
			ctx.params.type = ctx.params.priority ? "P" : "S";
			
			if (ctx.meta.device.type === "MOBILE") {
				ctx.params.origin = "mobile";
			} else if (ctx.meta.device.type === "WEB") {
				ctx.params.origin = "web";
			} else if (ctx.meta.device.type === "KIOSK") {
				ctx.params.origin = "quiosque";
			} else if (ctx.meta.device.type === "GENERIC") {
				ctx.params.origin = "GENERIC";
			} else if (ctx.meta.device.type === "BO") {
				ctx.params.origin = "bo";
			} else {
				throw new Errors.ValidationError("Invalid origin", "TICKET_INVALID_ORIGIN");
			}

			if (ctx.params.origin == "web" || ctx.params.origin == "mobile") {
				const tickets = await this._find(ctx, {
					query: {
						user_id: ctx.meta.user.id,
						status: "EM_ESPERA",
						subject_id: ctx.params.subject_id,
						type: ["S", "P"]
					},
				});
				
				if (tickets.length > 0) {
					throw new Errors.ValidationError(
						"Use only can have one ticket per subject",
						"USER_ALREADY_HAVE_TICKET",
					);
				}
			}
		},
		/**
		 * Validate appointment's creation (origin, valid hours)
		 * @param {*} ctx
		 */
		async validateAppointmentCreation(ctx) {
			ctx.params.created_at = new Date();
			ctx.params.status = "EM_ESPERA";
			ctx.params.call_number = 0;
			ctx.params.type = "M";

			if (ctx.meta.device.type === "MOBILE") {
				ctx.params.origin = "mobile";
			} else if (ctx.meta.device.type === "WEB") {
				ctx.params.origin = "web";
			} else {
				throw new Errors.ValidationError("Invalid origin", "APPOINTMENT_INVALID_ORIGIN");
			}

			// valida hora da marcação (se é válida e está disponivel)
			const list = await ctx.call("queue.subjects.appointments", {
				id: ctx.params.subject_id.toString(),
				day: ctx.params.appointment_schedule.getTime() + "",
			});

			if (
				Array.from(list.hours).findIndex(
					(x) => x.getTime() === ctx.params.appointment_schedule.getTime(),
				) == -1
			) {
				throw new Errors.ValidationError("Invalid hour", "APPOINTMENT_INVALID_HOUR");
			}
		},
		/**
		 * Returns ticket sequential number
		 * @param {*} ctx
		 * @param {*} type Ticket type {"S"-Sequential,"M"- Appointment,"P" - Priority }
		 * @param {*} subject_id Subject id
		 */
		 async getTicketNumber(ctx, type, subject_id, attendance_day) {
			let queue = await ctx.call("queue.tickets_queue.find", {
				query: { type: type, subject_id: subject_id, attendance_day: attendance_day },
			});

			if (queue.length == 0) {
				queue = await ctx.call("queue.tickets_queue.create", {
					type: type,
					subject_id: subject_id,
					attendance_day: attendance_day,
					last: 0,
				});
			}

			const currdate = new Date();
			currdate.setHours(0, 0, 0, 0);

			// Caso o ultimo ticket seja tirado no dia anterior, a contagem regressa ao inicio
			if (queue[0].updated_at < currdate) {
				queue[0].last = 0;
			}

			return Number.parseInt(queue[0].last + 1);
		},
		/**
		 * Update ticket number iterator
		 * @param {*} ctx
		 * @param {*} type Ticket type {"S"-Sequential,"M"- Appointment,"P" - Priority }
		 * @param {*} subject_id Subject id
		 */
		async updateTicketNumber(ctx, type, subject_id, ticket_number, attendance_day) {
			let queue = await ctx.call("queue.tickets_queue.find", {
				query: { type: type, subject_id: subject_id, attendance_day: attendance_day },
			});

			queue[0].last = Number.parseInt(ticket_number);
			
			await ctx.call("queue.tickets_queue.update", queue[0]);
		},
		/**
		 * Update attendence average time with new attendence time
		 * @param {*} ctx
		 * @param {*} ticket closed ticket
		 */
		async updateAttendenceAvgTime(ctx, ticket) {
			const ticketAttendenceTime =
				(ticket.final_time.getTime() - ticket.initial_time.getTime()) / 1000;
			
			const subject = await ctx.call("queue.subjects.get", { id: ticket.subject_id });
			
			subject[0].n_tickets = subject[0].n_tickets + 1;

			if (subject[0].n_tickets === 2) {
				// First ticket for queue
				subject[0].avg_time = ticketAttendenceTime;
			} else {
				subject[0].avg_time =
					(subject[0].avg_time * (subject[0].n_tickets - 1) + ticketAttendenceTime) /
					subject[0].n_tickets;
			}

			await ctx.call("queue.subjects.patch", {
				id: subject[0].id,
				n_tickets: subject[0].n_tickets,
				avg_time: +subject[0].avg_time,
			});
		},
		/**
		 * Validate if logger user is desk operator
		 * @param {*} ctx
		 */
		async validateOperator(ctx) {
			const deskOperator = await ctx.call("queue.desk_operators.find", {
				query: { user_id: ctx.meta.user.id, active: true },
			});

			if (deskOperator.length == 0) {
				throw new Errors.ValidationError("Invalid user", "INVALID_DESK_OPERATOR");
			}

			return deskOperator[0];
		},
		getTicketConcatedNumber(ticketNumber, ticketType, subjectCode) {
			if (ticketType == "S") {
				return "" + subjectCode + ticketNumber;
			} else {
				return "" + ticketType + " " + subjectCode + ticketNumber;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
