"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.tickets_history",
	table: "ticket_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "ticket_history")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"sequencial_number",
			"order_number",
			"created_at",
			"estimated_time",
			"origin",
			"call_number",
			"initial_time",
			"final_time",
			"appointment_number",
			"appointment_schedule",
			"priority",
			"notes",
			"status",
			"subject_id",
			"email",
			"student_number",
			"type",
			"user_id",
			"attended_by_id",
			"attended_on",
			"appointment_reason",
			"ticket_code",
			"call_times",
		],
		defaultWithRelateds: ["subject", "attended_on", "attended_by"],
		withRelateds: {
			subject(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.subjects", "subject", "subject_id", "id");
			},
			attended_on(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "queue.desks", "attended_on", "attended_on", "id");
			},
			attended_by(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "attended_by_id", "attended_by_id", "id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			id: { type: "number", optional: true },
			sequencial_number: { type: "number", optional: true, nullable: true },
			order_number: { type: "number", optional: true },
			estimated_time: { type: "date", convert: true, optional: true },
			origin: { type: "string", max: 10 },
			call_number: { type: "number", default: 0, optional: true },
			initial_time: { type: "date", optional: true, nullable: true, convert: true },
			final_time: { type: "date", optional: true, nullable: true, convert: true },
			appointment_number: { type: "number", optional: true, nullable: true },
			appointment_schedule: { type: "date", convert: true, optional: true, nullable: true },
			appointment_reason: { type: "string", max: 255, optional: true },
			priority: { type: "boolean", optional: true, default: false },
			notes: { type: "string", max: 200, optional: true },
			status: { type: "string", optional: true, default: 1 },
			subject_id: { type: "number" },
			email: { type: "string", max: 50, optional: true },
			student_number: { type: "string", max: 50, optional: true },
			type: { type: "string", max: 1, optional: true },
			user_id: { type: "number", optional: true, nullable: true },
			attended_by_id: { type: "number", optional: true, nullable: true },
			attended_on: { type: "number", optional: true, nullable: true },
			ticket_code: { type: "string", max: 25 },
		},
		hooks: {},
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getTicketsByUser: {
			rest: "GET /user",
			visibility: "published",
			scope: "queue:ticket_history:read",
			params: {
				student_number: { type: "string", optional: true },
				email: { type: "string", optional: true },
			},
			async handler(ctx) {
				if (
					(ctx.params.student_number == null || ctx.params.student_number == "") &&
					(ctx.params.email == null || ctx.params.email == "")
				) {
					throw new Errors.ValidationError(
						"Required one filter (Student number or email)",
						"REQUIRED_FILTER",
					);
				}
				ctx.params.email = ctx.params.email == null ? "" : ctx.params.email;
				ctx.params.student_number =
					ctx.params.student_number == null ? "" : ctx.params.student_number;
				this.logger.info(ctx.params);
				return await this._find(ctx, {
					query: (qb) => {
						qb.where("status", "=", "ATENDIDO");
						if (ctx.params.student_number) {
							qb.andWhere("student_number", "=", ctx.params.student_number);
						}
						if (ctx.params.email) {
							qb.andWhere("email", "=", ctx.params.email);
						}
					},
					withRelated: ["subject"],
				});
			},
		},

		getTicketsByLoggedUser: {
			rest: "GET /user/:type",
			visibility: "published",
			scope: "queue:ticket_history:read",
			params: {
				type: { type: "string" },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
			},
			async handler(ctx) {
				if (ctx.params.type == "S" || ctx.params.type == "P") {
					ctx.params.type = ["S", "P"];
				}
				const pag = {
					page: ctx.params.page,
					pageSize: ctx.params.pageSize,
					limit: ctx.params.limit,
					offset: ctx.params.offset,
					sort: ctx.params.sort,
				};

				return this._list(ctx, {
					...pag,
					query: { user_id: ctx.meta.user.id, type: ctx.params.type },
				});
			},
		},

		// Numero de senhas em papel e digital
		getTicketsPaperDigital: {
			rest: "GET /ticketpaperanddigital",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler() {
				const total_number_digital = await this.adapter
					.getDB("ticket_history")
					.count("id")
					.whereIn("origin", ["web", "mobile"]);
				const total_number_paper = await this.adapter
					.getDB("ticket_history")
					.count("id")
					.where("origin", "quiosque");
				const result = {
					total_ticket_digital: total_number_digital[0].count,
					total_ticket_paper: total_number_paper[0].count,
				};
				return result;
			},
		},

		//Tempo medio de espera de atendimento
		getAvgWaitingTime: {
			rest: "GET /avgwaitingtime",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_tickets_attended = await this.tickestAttended(ctx);
				let time_calculated = [];
				for (const ticket of list_tickets_attended) {
					const start_attend_time = ticket.initial_time.getTime();
					const emit_ticket_time = ticket.created_at.getTime();
					const calculation = start_attend_time - emit_ticket_time;
					time_calculated.push(calculation);
				}
				let timeBetweenEmitAndAttended = 0;
				for (const calculations of time_calculated) {
					timeBetweenEmitAndAttended += calculations;
				}
				const ms_avg =
					time_calculated.length > 0 ? timeBetweenEmitAndAttended / time_calculated.length : 0;
				const sec_avg =
					time_calculated.length > 0
						? timeBetweenEmitAndAttended / time_calculated.length / 1000
						: 0;
				const compose = this.formatHours(ms_avg);
				const result = {
					avg_hours: compose,
					avg_sec: sec_avg.toFixed(2),
					avg_ms: ms_avg.toFixed(2),
				};
				return result;
			},
		},

		// Numero total de senhas atendidas
		getTotalAttended: {
			rest: "GET /totalnumberattended",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const total_tickets_attended = await this._count(ctx, {
					query: {
						query: {
							status: "ATENDIDO",
						},
					},
				});
				const result = { total_number: total_tickets_attended };
				return result;
			},
		},

		// Numero de senhas canceladas ou que nao compareceu
		getTicketsCanceledNotAppear: {
			rest: "GET /ticketscancelednotappear",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler() {
				const total_number_tickets = await this.adapter
					.getDB("ticket_history")
					.count("id")
					.whereIn("status", ["CANCELADO", "NAO_COMPARECEU"]);
				const result = { total_number: total_number_tickets[0].count };
				return result;
			},
		},

		// Tempo medio atendimento
		getAvgAttendanceTime: {
			rest: "GET /avgattendancetime",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_tickets_attended = await this.tickestAttended(ctx);
				let calculations_list = [];
				for (const ticket of list_tickets_attended) {
					const start_date = ticket.initial_time.getTime();
					const end_date = ticket.final_time.getTime();
					const calculation = end_date - start_date;
					calculations_list.push(calculation);
				}
				let final_calculation = 0;
				for (const calculations of calculations_list) {
					final_calculation += calculations;
				}

				let ms_avg =
					calculations_list.length > 0 ? final_calculation / calculations_list.length : 0;
				let sec_avg =
					calculations_list.length > 0 ? final_calculation / calculations_list.length / 1000 : 0;
				const compose = this.formatHours(ms_avg);
				const result = {
					avg_hours: compose,
					avg_sec: sec_avg.toFixed(2),
					avg_ms: ms_avg.toFixed(2),
				};
				return result;
			},
		},

		// Tempo medio entre a 1 chamada e o inicio do atendimento
		getAvgBetweenCallAndAttendence: {
			rest: "GET /avgbetweencallattendence",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_tickets_attended = await this.tickestAttended(ctx);

				let calculations_list = [];
				for (const ticket of list_tickets_attended) {
					try {
						const call_date = ticket.call_times[ticket.call_times.length - 1].getTime();
						const star_date = ticket.initial_time.getTime();
						const calculation = star_date - call_date;
						calculations_list.push(calculation);
					} catch (er) {
						calculations_list.push(0);
					}
				}
				let final_calculation = 0;
				for (const calculations of calculations_list) {
					final_calculation += calculations;
				}

				const ms_avg =
					calculations_list.length > 0 ? final_calculation / calculations_list.length : 0;
				const sec_avg =
					calculations_list.length > 0 ? final_calculation / calculations_list.length / 1000 : 0;
				const compose = this.formatHours(ms_avg);
				const result = {
					avg_hours: compose,
					avg_sec: sec_avg.toFixed(2),
					avg_ms: ms_avg.toFixed(2),
				};
				return result;
			},
		},
		// Percentagem de tickets atendidos por todos os serviços ou so por 1
		getPercentageTickets: {
			rest: "GET /percentagetickets/:service",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				if (ctx.params.service === "ALL") {
					return this.getPercentagesAllServices(ctx);
				} else {
					const service = await ctx.call("queue.services.get", { id: ctx.params.service });
					if (service.length === 0) {
						throw new Errors.EntityNotFoundError("operator_id", ctx.params.operator_id);
					}
					if (service.length !== 0) {
						if (service[0].active === false) {
							throw new Errors.ValidationError("Service not active", "SERVICE_NOT_ACTIVE", {
								service: "service_id",
							});
						} else {
							return this.getPercentageByService(ctx);
						}
					}
				}
			},
		},

		// Todos os tickets atendidos de forma nao sequenciais
		getAvgNotSequencialTickets: {
			rest: "GET /averagerecoveredtickets",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_tickets_attended = await this.tickestAttended(ctx);

				let total_recovered = 0;
				for (const ticket of list_tickets_attended) {
					if (ticket.call_number > 1) {
						total_recovered += 1;
					}
				}
				const avg =
					list_tickets_attended.length > 0
						? (total_recovered / list_tickets_attended.length).toFixed(2)
						: 0;
				const result = {
					average_recovered: avg,
				};
				return result;
			},
		},

		//Todos os tickets atendidos fora do objetivo por subject activo
		getTickestMetOutOfGoal: {
			rest: "GET /ticketsoutofgoal",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_subject = await ctx.call("queue.subjects.find", {
					query: {
						active: true,
					},
				});
				for (const subject of list_subject) {
					let number_of_tickets = 0;
					const ticktes_list = await this._find(ctx, {
						query: {
							status: "ATENDIDO",
							subject_id: subject.id,
						},
					});
					for (const ticket of ticktes_list) {
						const calculations = ticket.final_time.getTime() - ticket.initial_time.getTime();
						if (calculations > subject.estimated_time * 1000) {
							number_of_tickets += 1;
						}
					}
					subject.out_of_goal = number_of_tickets;
				}
				return list_subject;
			},
		},

		//Todos os tickest que foram transferidos
		getTicketTransfered: {
			rest: "GET /totalticketstransfered",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_tickets = await this._count(ctx, {
					query: {
						status: "TRANSFERIDO",
					},
				});
				const result = {
					total_number: list_tickets,
				};
				return result;
			},
		},
		//Todos os tickets em espera actualmente
		getTicketsActualWaiting: {
			rest: "GET /totalticketsactualwaiting",
			scope: "queue:ticket_history:read",
			visibility: "published",
			async handler(ctx) {
				const total_number_waiting = await ctx.call("queue.tickets.find", {
					query: {
						status: "EM_ESPERA",
					},
				});
				const result = {
					total_number: total_number_waiting.length,
				};
				return result;
			},
		},
		//Todos so tickets em atendimento actualmente
		getTicketsInAttendance: {
			rest: "GET /totalticketsattendance",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const total_number_attendance = await ctx.call("queue.tickets.find", {
					query: {
						status: "EM_ATENDIMENTO",
					},
				});
				const result = {
					total_number: total_number_attendance.length,
				};
				return result;
			},
		},
		//Numero de balcoes a atender e nao atender
		getDesksStatus: {
			rest: "GET /totaldesksstatus",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const desks_open = await ctx.call("queue.desks.find", {
					query: {
						actual_state: true,
						active: true,
					},
				});
				const desks_close = await ctx.call("queue.desks.find", {
					query: {
						actual_state: false,
						active: true,
					},
				});
				const result = {
					total_number_open: desks_open.length,
					total_number_close: desks_close.length,
				};
				return result;
			},
		},
		// Numero de serviços a operar e a nao operar e mostrar os serviços e nos a operar meter as filas que estao a operar
		getServicesStatus: {
			rest: "GET /totalservicesstatus",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const list_associations_actives_operating = await ctx.call(
					"queue.subject_desk_operators.find",
					{
						query: {
							active: true,
							operating: true,
						},
					},
				);

				//get todos os subjects referentes a associacoes operating
				let list_subjects = [];
				for (const associations of list_associations_actives_operating) {
					const subject = await ctx.call("queue.subjects.find", {
						query: {
							id: associations.subject_id,
						},
					});
					const exist = list_subjects.find((elem) => elem === subject[0]);
					if (exist !== undefined) {
						list_subjects.push(subject[0]);
					}
				}

				// get services dos subjects das associacoes operating
				let list_services = [];
				for (const subjects of list_subjects) {
					const service = await ctx.call("queue.services.find", {
						query: {
							id: subjects.service_id,
						},
					});
					let exist_service = list_services.find((elem) => elem === service[0]);
					if (exist_service !== undefined) {
						list_services.push(service[0]);
					}
				}

				//get todos os serviços para a saber quais serviços nao estao operating
				let total_service = await ctx.call("queue.services.find", {
					query: {
						active: true,
					},
				});

				let services_not_operating = [];
				for (let service_all of total_service) {
					const remove = list_services.find((elem) => elem.id === service_all.id);
					if (remove === undefined) {
						services_not_operating.push(service_all);
					}
				}

				//get das filas que estao a operar por serviço
				for (const service of list_services) {
					let subject_list_service = [];
					const exist = list_subjects.find((elem) => elem.id === service.id);
					if (exist !== undefined) {
						subject_list_service.push(exist);
					}
					service.subjects = subject_list_service;
				}
				// construir result
				const result = {
					services_open: {
						total_number: list_services.length,
						services: list_services,
					},
					services_close: {
						total_number: total_service.length - list_services.length,
						services: services_not_operating,
					},
				};
				return result;
			},
		},
		// Todos os atendimentos a cima da media de atendimento
		getAboveAvgAttendance: {
			rest: "GET /attendanceaboveavg",
			scope: "queue:ticket_history:stats",
			visibility: "published",
			async handler(ctx) {
				const avg = await ctx.call("queue.tickets_history.getAvgAttendanceTime");
				const list_tickets_attended = await this._find(ctx, {
					query: {
						status: "ATENDIDO",
					},
				});
				let count = 0;
				for (const ticket of list_tickets_attended) {
					const star_date = ticket.initial_time.getTime();
					const final_date = ticket.final_time.getTime();
					const calculation = final_date - star_date;
					if (calculation > parseInt(avg.avg_ms).toFixed(0)) {
						count += 1;
					}
				}
				const result = {
					total_number: count,
				};
				return result;
			},
		},

	    // Listar os todos os tickets em histórico com filtros
		getTickectsHistoryAndSubjects: {
			rest: "GET /tickets_history_subjects",
			visibility: "published",
			scope: "queue:ticket_history:read",
			params: {
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
				service_id: { type: "number", positive: true, convert: true, optional: true },
				subject_id: { type: "number", positive: true, convert: true, optional: true },
				desk_id: { type: "number", positive: true, convert: true, optional: true },
				operator_id: { type: "number", positive: true, convert: true, optional: true },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
			},
			async handler(ctx) {
				ctx.params.sort = "-id";
				ctx.params.withRelated = ["subject", "attended_by", "user"];
				ctx.params.query = ctx.params.query || {};

				if (ctx.params.subject_id !== null && ctx.params.subject_id !== undefined) {
					ctx.params.query.subject_id = ctx.params.subject_id;
				}

				if (ctx.params.desk_id !== null && ctx.params.desk_id !== undefined) {
					ctx.params.query.attended_on = ctx.params.desk_id;
				}

				if (ctx.params.operator_id !== null && ctx.params.operator_id !== undefined) {
					ctx.params.query.attended_by_id = ctx.params.operator_id;
				}

				if ( (ctx.params.start_date !== null && ctx.params.start_date !== undefined) ||
					 (ctx.params.end_date !== null && ctx.params.end_date !== undefined) ||
					 (ctx.params.service_id !== null && ctx.params.service_id !== undefined) ) {
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.start_date !== null && ctx.params.start_date !== undefined) {
							qb
								.where(qb1 => {
									qb1.where(this.adapter.raw("\"estimated_time\"::date"), ">=", moment(ctx.params.start_date).toDate());
									qb1.orWhereNull("estimated_time");
								});
						}

						if (ctx.params.end_date !== null && ctx.params.end_date !== undefined) {	
							qb
								.where(qb1 => {
									qb1.where(this.adapter.raw("\"estimated_time\"::date"), "<=",  moment(ctx.params.end_date).toDate());
									qb1.orWhereNull("estimated_time");
								});							
						}

						if (ctx.params.service_id !== null && ctx.params.service_id !== undefined) {
							qb
								.whereIn(
									"subject_id",
									qb1 => qb1
										.from("subject")
										.select("id")
										.where("service_id", "=", ctx.params.service_id)
								);
						}
					};
				}
				
				let params = this.sanitizeParams(ctx, ctx.params, true);

				return this._list(ctx, params);
			},
		},		

		// Listar os tickets passíveis de serem rechamados
		getTickectsHistoryToRecall: {
			rest: "GET /tickects_history_to_recall",
			visibility: "published",
			scope: "queue:ticket_history:read",
			params: {
				subjects:
					[
						{ type: "array", item: { type: "number", positive: true, convert: true, optional: true } },
						{ type: "number", positive: true, convert: true, optional: true }
					],
				desk_id: { type: "number", positive: true, convert: true },
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
			},
			async handler(ctx) {
				ctx.params.sort = "-id";
				ctx.params.withRelated = ["subject"];
				ctx.params.query = ctx.params.query || {};
				ctx.params.query.subject_id = ctx.params.subjects;
				delete ctx.params.subjects;
				ctx.params.query.status = ["NAO_COMPARECEU", "ATENDIDO"];

				ctx.params.extraQuery = (qb) => {
					qb
						.where(qb1 => {
							qb1.where(this.adapter.raw("\"appointment_schedule\"::date"), "<=", moment(new Date(), "YYYY-MM-DD").endOf("day"));
							qb1.orWhereNull("appointment_schedule");
						})
					;
				};

				let params = this.sanitizeParams(ctx, ctx.params, true);

				return this._list(ctx, params);
			},
		},

		// Rechamar ticket de histórico
		recallTicketHistory: {
			rest: "POST /recall_ticket_history",
			visibility: "published",
			params: { 
				id: { type: "number", positive: true, convert: true, optional: false } 
			},
			async handler(ctx) {
				// Get ticket history
				const ticket_history = await this._get(ctx, { id: ctx.params.id });
				
				if (ticket_history[0].status !== "NAO_COMPARECEU" && ticket_history[0].status !== "ATENDIDO") {
					throw new Errors.ValidationError(
						"This ticket never is called",
						"TICKET_HAS_NOT_CALLED",
					);
				}

				// Insert ticket
				ticket_history[0].id = undefined;
				ticket_history[0].status = "EM_ESPERA";
				ticket_history[0].attended_by_id = null;
				ticket_history[0].attended_on = null;

				return await ctx.call("queue.tickets.create", ticket_history[0]);
			},
		},		

	},

	/**
	 * Events
	 */
	events: {
		"queue.tickets_history.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},

		async tickestAttended(ctx) {
			return await this._find(ctx, {
				query: {
					status: "ATENDIDO",
				},
			});
		},

		// Converter ms para o formato 00:00:00
		formatHours(ctx) {
			let ms = ctx % 1000;
			ctx = (ctx - ms) / 1000;
			let secs = ctx % 60;
			ctx = (ctx - secs) / 60;
			let mins = ctx % 60;
			let hrs = (ctx - mins) / 60;

			if (mins < 10) {
				mins = "0" + mins;
			}
			if (secs < 10) {
				secs = "0" + secs;
			}
			if (hrs < 10) {
				hrs = "0" + hrs;
			}
			const compose = hrs + ":" + mins + ":" + secs;

			return compose;
		},

		async getPercentagesAllServices(ctx) {
			const listOfSubjects = await ctx.call("queue.subjects.find", {
				query: {
					active: true,
				},
			});

			const list_all_tickets_attended = await this.tickestAttended(ctx);

			let list_result = [];
			for (let subject of listOfSubjects) {
				const list_ticket_attended = await this._find(ctx, {
					query: {
						status: "ATENDIDO",
						subject_id: subject.id,
					},
				});

				const percentage =
					list_all_tickets_attended.length > 0
						? ((list_ticket_attended.length * 100) / list_all_tickets_attended.length).toFixed(2)
						: 0;

				subject.total = list_ticket_attended.length;
				subject.percentage = percentage;
				list_result.push(subject);
			}
			return list_result;
		},

		async getPercentageByService(ctx) {
			const listofsubjects = await ctx.call("queue.subjects.find", {
				query: {
					service_id: ctx.params.service,
					active: true,
				},
			});
			let total_value = 0;
			for (const subject of listofsubjects) {
				const total_tickets = await this._find(ctx, {
					query: {
						status: "ATENDIDO",
						subject_id: subject.id,
					},
				});
				total_value += total_tickets.length;
			}
			for (const subject of listofsubjects) {
				const total_tickets_subject = await this._find(ctx, {
					query: {
						status: "ATENDIDO",
						subject_id: subject.id,
					},
				});
				const percentage =
					total_tickets_subject.length > 0
						? ((total_tickets_subject.length * 100) / total_value).toFixed(2)
						: 0;
				subject.percentage = percentage;
			}
			return listofsubjects;
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
