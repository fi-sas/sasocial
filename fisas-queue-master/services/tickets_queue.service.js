"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.tickets_queue",
	table: "ticket_queue",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "ticket_queue")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "last", "type", "subject_id", "updated_at", "attendance_day"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			last: { type: "number", optional: true },
			// last_called: { type: "number", optional: true },
			// ongoing: { type: "string", max: 5, optional: true },
			type: { type: "string", max: 1 },
			subject_id: { type: "number" },
			updated_at: { type: "date", optional: true, nullable: false, convert: true },
			attendance_day: { type: "date", optional: true, nullable: true, convert: true },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
