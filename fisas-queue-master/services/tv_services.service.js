"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "queue.tv_services",
	table: "tv_service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("queue", "tv_service")],

	/**
	 * Settings
	 */

	settings: {
		fields: ["id", "tv_id", "service_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			id: { type: "number", optional: true },
			tv_id: { type: "string" },
			service_id: { type: "number" },
		},
	},
	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.tv_id = ctx.meta.device[0].uuid;
					const list = await this._find(ctx, {
						query: { tv_id: ctx.meta.device[0].uuid, service_id: ctx.params.service_id },
					});
					if (list.length > 0) {
						throw new Errors.ValidationError(
							"Service already registred on tv",
							"SERVICE_ALREADY_REGISTERED",
						);
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getServicesByTv: {
			rest: "GET /tv/services",
			visibility: "published",
			cache: false,
			async handler(ctx) {
				const subjects = [];
				for (const tv_service of await this._find(ctx, {
					query: { tv_id: ctx.meta.device.uuid },
				})) {
					subjects.push(
						await ctx.call("queue.subjects.getSubjectsByService", {
							service_id: tv_service.service_id,
						}),
					);
				}
				return subjects;
			},
		},
		removeByServiceId: {
			params: { service_id: { type: "number" } },
			async handler(ctx) {
				const tvServicesList = await this._find(ctx, {
					query: { service_id: ctx.params.service_id },
				});
				for (const tvService of tvServicesList) {
					await this._remove(ctx, { id: tvService.id });
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
