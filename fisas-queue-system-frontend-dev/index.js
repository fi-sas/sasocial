"use strict";
const path = require("path");
const { app, BrowserWindow, Menu } = require("electron");
/// const {autoUpdater} = require('electron-updater');
const { is } = require("electron-util");
const unhandled = require("electron-unhandled");
const debug = require("electron-debug");
const contextMenu = require("electron-context-menu");
const config = require("./config");
const menu = require("./menu");
require("electron-reload")(__dirname);

unhandled();
debug();
contextMenu();

// Note: Must match `build.appId` in package.json
app.setAppUserModelId("com.company.AppName");

// Uncomment this before publishing your first version.
// It's commented out as it throws an error if there are no published versions.
// if (!is.development) {
// 	const FOUR_HOURS = 1000 * 60 * 60 * 4;
// 	setInterval(() => {
// 		autoUpdater.checkForUpdates();
// 	}, FOUR_HOURS);
//
// 	autoUpdater.checkForUpdates();
// }

// Prevent window from being garbage collected
let mainWindow;

const createMainWindow = async () => {
	const win = new BrowserWindow({
		title: app.name,
		show: false,
		width: 600,
		height: 400,
		webPreferences: {
            nodeIntegration: true
        }
	});

	win.on("ready-to-show", () => {
		win.show();
	});

	win.on("closed", () => {
		// Dereference the window
		// For multiple windows store them in an array
		mainWindow = undefined;
	});

	await win.loadFile(path.join(__dirname, "login.html"));

	return win;
};

// Prevent multiple instances of the app
if (!app.requestSingleInstanceLock()) {
	app.quit();
}

app.on("second-instance", () => {
	if (mainWindow) {
		if (mainWindow.isMinimized()) {
			mainWindow.restore();
		}

		mainWindow.show();
	}
});

app.on("window-all-closed", () => {
	if (!is.macos) {
		app.quit();
	}
});

app.on("activate", async () => {
	if (!mainWindow) {
		mainWindow = await createMainWindow();
	}
});

const axios = require("axios");

axios.get("http://localhost:7100/api/v1/tickets").then(response => {
	console.log(response.data.link.total);
});

void function recallTicket() {
	axios.get("http://localhost:7100/api/v1/tickets/1").then(response => {
		console.log(response.data.data[0].number);
	});
};

void function getNextTicker() {
	axios.get("http://localhost:7100/api/v1/tickets/1").then(response => {
		console.log(response.data.data[0].number);
	});
};

void function getPreviousTicket() {
	axios.get("http://localhost:7100/api/v1/tickets/1").then(response => {
		console.log(response.data.data[0].number);
	});
};

void function serviceStart() {
	axios
		.patch("http://localhost:7100/api/v1/tickets/11", {
			callTimestamp: "1970-01-01T00:00:01.000Z",
			counter_id: 1,
			user_id: 1
		})
		.then(function(response) {
			console.log(response);
		})
		.catch(function(error) {
			console.log(error);
		});
};
/*

  void function serviceFinish() {

	axios.patch('http://localhost:7100/api/v1/tickets/11', {
	answered: true,
	nextCallTimestamp: '1970-01-01T00:00:01.000Z',
	serviceDuration:

  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
       
  

  void function serviceForward() {

	axios.post('http://localhost:7100/api/v1/tickets/11', {
	WrongTicketid: // Passar como parametro do ticket anterior	
	forwarded: true,
	nextCallTimestamp: '1970-01-01T00:00:01.000Z',
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
       
  }
  */
