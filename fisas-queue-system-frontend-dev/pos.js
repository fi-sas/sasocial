const axios = require("axios");

var flag = true; //Varivel para parar o for loop cada vez que há um click



function getActiveServices() {
	return axios
		.get("http://localhost:7100/api/v1/services/7/posServices")
		.then(response => {
			container = document.getElementById("services");

			counts = response.data.data[0].length;

			for (var i = 0; i < counts; i++) {
				if (!flag) return;
				console.log(response.data.data[0][i]);
				container.innerHTML +=
					'<a onClick="newTicket(this);" data-value="' +
					response.data.data[0][i].service_id +
					'" "><div class="rectangle"  id ="body' +
					i +
					'"><div class="circle" id="letter' +
					i +
					'">1</div><label class="serviceName"  id="name' +
					i +
					'"></label></div></a>';

				document.getElementById("name" + i).innerHTML =
					response.data.data[0][i].name;
				document.getElementById("letter" + i).innerHTML =
					response.data.data[0][i].letter;

				if (response.data.data[0][i].active == false) {
					document.getElementById("body" + i).style.background = "#c23e3e";
					document.getElementById("letter" + i).style.background = "#c44e59";
				}
			}

			flag = false;
		});
}

function printTicket() {
	axios
		.get("http://localhost:7100/api/v1/tickets/7/printedTicket")
		.then(response => {
			document.querySelector("div").style.display = "block";
			console.log(
				response.data.data[0][0].letter + response.data.data[0][0].number
			);

			document.getElementById("senhaTirada").innerHTML =
				"Senha Tirada : " +
				response.data.data[0][0].letter +
				" " +
				response.data.data[0][0].number;

			document.getElementById("alerta").style.visibility = "block";
		});
}

function newTicket(d) {
	console.log(d.getAttribute("data-value"));

	getActiveServices().then(async response => {
		await axios.get(
			"http://localhost:7100/api/v1/tickets/7/7/7/" +
				d.getAttribute("data-value") +
				"/newTicket"
		);

		 printTicket();
	});

	
}
