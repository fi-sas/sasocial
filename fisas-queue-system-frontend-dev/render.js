const axios = require("axios");
var Timer = require("easytimer.js").Timer;
var timerInstance = new Timer();

var userID;
var serviceChanged;

//Total de tickets por Serviço e Unidade Orgânica;
function getTotalTickets() {
	return axios
		.get("http://localhost:7100/api/v1/queues/1/7/countTickets")
		.then(response => {
			var totalTickets = response.data.data[0][0].total;
			return totalTickets;
		});
}

function ticketBoard() {
	return axios
		.get("http://localhost:7100/api/v1/queues/1/7/getQueueTicket")
		.then(response => {
			var allTickets = response.data.data[0][0];

			console.log(allTickets);

			axios
				.get("http://localhost:7100/api/v1/tickets/1/7/spNextTicket")
				.then(response => {
					if (response.data.data[0].proximoTicket != null) {
					}
					console.log(response.data.data[0].proximoTicket);
				});

			timerInstance.addEventListener("secondsUpdated", function(e) {
				document.getElementById(
					"attendingTime"
				).innerHTML = timerInstance.getTimeValues().toString();
			});

			timerInstance.start();

			getTotalTickets().then(totalTickets => {
				document.getElementById("pendingTickets").innerHTML = totalTickets;

				if (totalTickets == 0) {
					
					timerInstance.stop();
				}
			});
			return allTickets;
		});
}

function previousTicketBoard() {
	return axios
		.get("http://localhost:7100/api/v1/queues/20/getPreviousTicket")
		.then(async response => {
			var allTickets = response.data.data[0][0];

			console.log(allTickets);

			timerInstance.addEventListener("secondsUpdated", function(e) {
				document.getElementById(
					"attendingTime"
				).innerHTML = timerInstance.getTimeValues().toString();
			});

			timerInstance.start();

			getTotalTickets().then(totalTickets => {
				document.getElementById("pendingTickets").innerHTML = totalTickets;

				if (totalTickets == 0) {
					
					timerInstance.stop();
				}
			});

			axios
				.get("http://localhost:7100/api/v1/tickets/20/spCurrentTicket")
				.then(response => {
					if (response.data.data[0].currentTicket != null) {
						document.getElementById("currentTicket").innerHTML =
							response.data.data[0].currentTicket;
					} else {
						document.getElementById("currentTicket").innerHTML = "ND";
					}
				})
				.then(response => {
					axios
						.get("http://localhost:7100/api/v1/tickets/1/7/spNextTicket")
						.then(response => {
							if (response.data.data[0].proximoTicket != null) {
								document.getElementById("nextTicket").innerHTML =
									response.data.data[0].proximoTicket;
							} else {
								document.getElementById("nextTicket").innerHTML = "ND";
							}
						});
				});

			return allTickets;
		});
}

//Retorna o proximo  ticket na fila Associado a um serviço e unidade orgânica;
function getCurrentTicket() {
	timerInstance.addEventListener("secondsUpdated", function(e) {
		document.getElementById(
			"attendingTime"
		).innerHTML = timerInstance.getTimeValues().toString();
	});

	timerInstance.start();

	getTotalTickets().then(totalTickets => {
		document.getElementById("pendingTickets").innerHTML = totalTickets;

		if (totalTickets == 0) {

			timerInstance.stop();
		}
	});

	axios
		.get(
			"http://localhost:7100/api/v1/tickets/+ " +
				localStorage.getItem("userID") +
				"/20/1/7/nextTicket",
			{}
		)
		.then(response => {
			axios
				.get("http://localhost:7100/api/v1/tickets/20/spCurrentTicket")
				.then(response => {
					if (response.data.data[0].currentTicket != null) {
						document.getElementById("currentTicket").innerHTML =
							response.data.data[0].currentTicket;
					} else {
						document.getElementById("currentTicket").innerHTML = "ND";
					}
				})
				.then(response => {
					axios
						.get("http://localhost:7100/api/v1/tickets/1/7/spNextTicket")
						.then(response => {
							if (response.data.data[0].proximoTicket != null) {
								document.getElementById("nextTicket").innerHTML =
									response.data.data[0].proximoTicket;
							} else {
								document.getElementById("nextTicket").innerHTML = "ND";
							}
						});
				});
		});
}

// Função onde é atribuido cada duração do tempo de atendimento a uma senha;
async function nextTicket() {
	await axios.get(
		"http://localhost:7100/api/v1/tickets/'" +
			document.getElementById("attendingTime").innerHTML +
			"'/20/setServiceDuration",
		{}
	),
		timerInstance.reset();

	getCurrentTicket();
}

async function previousTicket() {
	await axios
		.get("http://localhost:7100/api/v1/tickets/20/previousTicket")
		.then(async response => {
			timerInstance.reset();
		});

	await previousTicketBoard();
}

//Rechamar a senha actual
function recallTicket() {
	previousTicketBoard();
}

async function forwardedTicket() {
	previousTicketBoard().then(allTickets => {
		axios
			.get(
				"http://localhost:7100/api/v1/tickets/" +
					allTickets.id +
					"/" +
					localStorage.getItem("serviceChanged") +
					"/20/'" +
					document.getElementById("attendingTime").innerHTML +
					"'/ticketForwarded"
			)
			.then(response => {
				axios.get(
					"http://localhost:7100/api/v1/tickets/'" +
						document.getElementById("attendingTime").innerHTML +
						"'/20/setServiceDuration",
					{}
				);
				timerInstance.reset();
			});
	});
}

//Senha não atendida

async function absentTicket() {
	axios.get(
		"http://localhost:7100/api/v1/tickets/'" +
			document.getElementById("attendingTime").innerHTML +
			"'/20/absentTicket",
		{}
	),
		await axios.get(
			"http://localhost:7100/api/v1/tickets/'" +
				document.getElementById("attendingTime").innerHTML +
				"'/20/setServiceDuration",
			{}
		);
	timerInstance.reset();

	getCurrentTicket();
}

//Verifica se existe um user id na base de dados e passa o seu valor

function getWorkerID() {
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;

	if (email == "" || password == "") {
		document.getElementById("loginVal").style.display = "";
	} else {
		axios
			.get(
				"http://localhost:7100/api/v1/workers/'" +
					email +
					"'/'" +
					password +
					"'/login"
			)
			//Receber senha anterior
			.then(response => {
				userID = response.data.data[0][0].user_id;
				console.log(userID);
				localStorage.setItem("userID", userID);
				location.replace("index.html");
				return userID;
			})
			.catch(error => {
				document.getElementById("loginVal").style.display = "";
			});
	}
}

function getServices() {
	previousTicketBoard().then(allTickets => {
		document.getElementById("form1").style.display = "";

		let dropdown = document.getElementById("services");
		dropdown.length = 0;
		let defaultOption = document.createElement("option");
		defaultOption.text = "Choose a Service";

		axios
			.get("http://localhost:7100/api/v1/queues/" + allTickets.id + "/services")
			//Receber senha anterior
			.then(response => {
				let option;

				for (let i = 0; i < response.data.data[0].length; i++) {
					option = document.createElement("option");
					option.value = response.data.data[0][i].service_id;
					option.text =
						response.data.data[0][i].letter +
						"-" +
						response.data.data[0][i].name;
					dropdown.add(option);
				}

				serviceChanged = document.getElementById("services").value;
				console.log(serviceChanged);
				localStorage.setItem("serviceChanged", serviceChanged);

				return serviceChanged;
			});
	});
}

function swapPause() {
	var image = document.getElementById("play");

	if (image.src.match("./static/bt_service_desk_play.png")) {
		image.src = "./static/bt_service_desk_pause.png";
		document.getElementById("closeDesk").disabled = true;
		document.getElementById("absent").disabled = true; 
		document.getElementById("forwarded").disabled = true; 
		document.getElementById("recall").disabled = true;
		document.getElementById("previous").disabled = true;
		document.getElementById("next").disabled = true;   
		timerInstance.pause();
	} else {
		image.src = "./static/bt_service_desk_play.png";
		document.getElementById("closeDesk").disabled = false;
		document.getElementById("absent").disabled = false; 
		document.getElementById("forwarded").disabled = false; 
		document.getElementById("recall").disabled = false;
		document.getElementById("previous").disabled = false;
		document.getElementById("next").disabled = false;
		timerInstance.start();
	}
}

function closeDeskOption() {
	document.getElementById("formOption").style.display = "";
}

function closeDesk() {
	if (document.getElementById("no").checked) {
		rate_value = document.getElementById("no").value;
		localStorage.removeItem("userID");
	} else if (document.getElementById("yes").checked) {
		rate_value = document.getElementById("yes").value;
		localStorage.removeItem("userID");
		
	}

	ticketBoard().then( async allTickets => {
		

		await axios.get(
			"http://localhost:7100/api/v1/tickets/'"+ document.getElementById("attendingTime").innerHTML +"'/" +
				rate_value +"/"+
				allTickets.id+
				"/closeDeskTimer"
				
		).then( response => {

			console.log("closeDeskTimer")
			axios.get(
				"http://localhost:7100/api/v1/tickets/'" +
					document.getElementById("attendingTime").innerHTML +
					"'/20/setServiceDuration",
				{}
			)

		}).then( response => {

			axios.get("http://localhost:7100/api/v1/tickets/20/disableCounter");

		}).then(response => {

			location.replace("login.html");
		})


	})

	
}
