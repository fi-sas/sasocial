# [1.41.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.40.0...v1.41.0) (2022-06-01)


### Features

* **ubike:** change file extension ([8d747cd](https://gitlab.com/fi-sas/fisas-reports/commit/8d747cd63efbf5f97f8123eabb9bb9e3069b5ec1))
* **ubike:** new msu reports ([ca537b5](https://gitlab.com/fi-sas/fisas-reports/commit/ca537b51a6e1b00293a57aee0642536a529f7f5a))
* **ubike:** new ubike reports ([ecb1b0f](https://gitlab.com/fi-sas/fisas-reports/commit/ecb1b0fc34e221ed65ce33be09f3c1c01dc6e5f2))

# [1.40.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.39.0...v1.40.0) (2022-06-01)


### Features

* **ubike:** new report list bikes ([d13f1a4](https://gitlab.com/fi-sas/fisas-reports/commit/d13f1a43ec7cc635f31f5ec484aefccd804027d9))

# [1.39.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.38.0...v1.39.0) (2022-05-18)


### Features

* **reports:** ubike kms traveled report ([9cd29b3](https://gitlab.com/fi-sas/fisas-reports/commit/9cd29b3d92f4680185e1ad8175d1e940f747e81f))

# [1.38.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.37.0...v1.38.0) (2022-05-17)


### Features

* **reports:** ubike applications report ([ea3368f](https://gitlab.com/fi-sas/fisas-reports/commit/ea3368ffc4b7812dc11b7fec7a684074a5d07ee7))

# [1.37.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.36.0...v1.37.0) (2022-04-11)


### Features

* **reports:** add parent names to ALO_CAN_RAW report ([2da42ad](https://gitlab.com/fi-sas/fisas-reports/commit/2da42ad5cce07720d6b500e9dded40fee0c5bc4f))

# [1.36.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.35.0...v1.36.0) (2022-04-06)


### Features

* **reports:** new accommodation report ([ad71958](https://gitlab.com/fi-sas/fisas-reports/commit/ad71958381db637524fd9e83b58f4d7a67d7719d))

# [1.35.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.34.0...v1.35.0) (2022-03-28)


### Features

* **reports:** add new field to accommodation dados em bruto report ([c53461e](https://gitlab.com/fi-sas/fisas-reports/commit/c53461e4b7754a8eb2e0e0442240d989943fb6ea))

# [1.34.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.33.0...v1.34.0) (2022-03-14)


### Features

* **social scholarship:** add new fields to experience certificate ([fb21f1e](https://gitlab.com/fi-sas/fisas-reports/commit/fb21f1e53d943a6093c8f613270a7602a0937488))

# [1.33.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.32.0...v1.33.0) (2022-02-10)


### Bug Fixes

* update template sales report ([bfd5451](https://gitlab.com/fi-sas/fisas-reports/commit/bfd5451b97e1f4ba0d96ae9d654c3775b037a690))


### Features

* add new template charge detailed report ([530d9d9](https://gitlab.com/fi-sas/fisas-reports/commit/530d9d913f855b2f26080b463b5c3307ed494913))

# [1.32.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.31.0...v1.32.0) (2022-02-02)


### Features

* **reports:** add new reprots food ([8bd0f77](https://gitlab.com/fi-sas/fisas-reports/commit/8bd0f77c1fdfc2dd1043dbe5c69db2135ab00738))

# [1.31.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.30.0...v1.31.0) (2022-02-01)


### Features

* **food:** add new food reports ([7ddc728](https://gitlab.com/fi-sas/fisas-reports/commit/7ddc728084948f1ddd552584887f7f1e64de81a6))

# [1.30.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.29.1...v1.30.0) (2022-01-28)


### Features

* **cc:** add payment_method_name to charges report ([af80e62](https://gitlab.com/fi-sas/fisas-reports/commit/af80e62f4438d472d175b6b2bde15ee12809ac29))

## [1.29.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.29.0...v1.29.1) (2022-01-27)


### Bug Fixes

* **cc:** wrong params into reports seed ([2d56a6c](https://gitlab.com/fi-sas/fisas-reports/commit/2d56a6cc27bbd2ef2815d83bb57d7f9f7da72a94))

# [1.29.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.28.0...v1.29.0) (2022-01-27)


### Bug Fixes

* **cc:** change report formats ([278815f](https://gitlab.com/fi-sas/fisas-reports/commit/278815fcd7872ad62abab4e74697bb8dbc3312fc))


### Features

* **cc:** add cc sales templates ([075a5d1](https://gitlab.com/fi-sas/fisas-reports/commit/075a5d1bd6efb8e9a20e8eb0e3071bba476935e0))

# [1.28.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.27.0...v1.28.0) (2022-01-26)


### Features

* **reports:** add cc sales reports ([f10cc82](https://gitlab.com/fi-sas/fisas-reports/commit/f10cc822a7a1503fc76f1621ee962dab0f7a7442))

# [1.27.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.26.1...v1.27.0) (2022-01-18)


### Features

* reports for calendar events ([fa827ee](https://gitlab.com/fi-sas/fisas-reports/commit/fa827ee362f70bab0145cabdccb13c8ae87491a1))

## [1.26.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.26.0...v1.26.1) (2022-01-04)


### Bug Fixes

* **reports:** send filename to media ([b92fdb5](https://gitlab.com/fi-sas/fisas-reports/commit/b92fdb5a81b428eea5247df9050f66c970f5435b))

# [1.26.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.25.0...v1.26.0) (2021-12-06)


### Features

* payment monthly report add field ([a024965](https://gitlab.com/fi-sas/fisas-reports/commit/a0249659544a2bb833dadabf946d0ce649f163b6))

# [1.25.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.24.1...v1.25.0) (2021-12-02)


### Features

* payment monthly report ([02855a3](https://gitlab.com/fi-sas/fisas-reports/commit/02855a37798ad9f2b59bafb9daf8db0d778c6eef))

## [1.24.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.24.0...v1.24.1) (2021-11-23)


### Bug Fixes

* **aco:** iban field with wrong mapping ([95e03a3](https://gitlab.com/fi-sas/fisas-reports/commit/95e03a3922737bf635344d8f155f5a62ddc46a0d))

# [1.24.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.23.1...v1.24.0) (2021-11-22)


### Bug Fixes

* **food:** update stock report ([c20ec43](https://gitlab.com/fi-sas/fisas-reports/commit/c20ec439a9cfde0a7bcc4adc72d89962bdb16bc9))


### Features

* **report:** new food report stock inventory ([df785da](https://gitlab.com/fi-sas/fisas-reports/commit/df785da0bbc57c324ac8a530a1148ce91e9f4586))

## [1.23.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.23.0...v1.23.1) (2021-11-17)


### Bug Fixes

* **accommodation:** fix sepa report ([c45fc4f](https://gitlab.com/fi-sas/fisas-reports/commit/c45fc4f1cda6cec128f618acc7fefff45211bf0e))

# [1.23.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.22.1...v1.23.0) (2021-11-17)


### Features

* **sepa:** add accommodation sepa report ([dd1bca9](https://gitlab.com/fi-sas/fisas-reports/commit/dd1bca9fe5defeec91058f1ceca697ec709de3a3))

## [1.22.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.22.0...v1.22.1) (2021-11-16)


### Bug Fixes

* **accommodation:** add fields autz dd ([249c7b7](https://gitlab.com/fi-sas/fisas-reports/commit/249c7b7911f951e5347d921dcae788db218b635b))
* **cc:** add reports ([82850fa](https://gitlab.com/fi-sas/fisas-reports/commit/82850faeb312f96d4ef549b73a4ebe6e52f1fec8))

# [1.22.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.21.0...v1.22.0) (2021-11-09)


### Features

* add profile  social_scholarship_experience ([c6d1839](https://gitlab.com/fi-sas/fisas-reports/commit/c6d1839a76b86e159dcb3340a65594a66a70d54c))

# [1.21.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.20.0...v1.21.0) (2021-10-28)


### Bug Fixes

* **cc_report:** change output type to xlsx ([d767278](https://gitlab.com/fi-sas/fisas-reports/commit/d7672781b36fc70977a87f9824e5818f209a22fb))
* **cc:** cc reports changes ([2716f87](https://gitlab.com/fi-sas/fisas-reports/commit/2716f87cf2a5634c98b7be84ac53bc4fafed0bbb))


### Features

* social scholarship experience offer report ([091ff18](https://gitlab.com/fi-sas/fisas-reports/commit/091ff1803ba5c6de190b942411ca6a812ce1d38c))

# [1.20.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.19.0...v1.20.0) (2021-10-22)


### Features

* reports for emergency fund v1 ([7d7ea6c](https://gitlab.com/fi-sas/fisas-reports/commit/7d7ea6cf1995c6888f4f490a766d61b4e36b83ba))

# [1.19.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.18.1...v1.19.0) (2021-10-19)


### Features

* **seed-templates:** add template and seed calendar ([c3469c7](https://gitlab.com/fi-sas/fisas-reports/commit/c3469c7f7818fac4a07a0a61557e73a9aa717aad))

## [1.18.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.18.0...v1.18.1) (2021-10-07)


### Bug Fixes

* **templates:** update template SOCIAL_SCHOLARSHIP_EXPERIENCE ([c0205b6](https://gitlab.com/fi-sas/fisas-reports/commit/c0205b6c72b7d5d67763a15a35c798a9e64a02bd))

# [1.18.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.17.1...v1.18.0) (2021-09-29)


### Features

* **templates:** add billings report template ([a8ed551](https://gitlab.com/fi-sas/fisas-reports/commit/a8ed551cff00400c17d59024f36128da36b478d1))

## [1.17.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.17.0...v1.17.1) (2021-09-22)


### Bug Fixes

* **templates:** add secundary email to accommodation application raw ([fa97f0d](https://gitlab.com/fi-sas/fisas-reports/commit/fa97f0d495184a376bfbe32402d13546558cd7c5))

# [1.17.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.16.0...v1.17.0) (2021-09-17)


### Bug Fixes

* **templates:** update template mobility declaration ([19aa3d4](https://gitlab.com/fi-sas/fisas-reports/commit/19aa3d428195085d73075b6f88a926612015a331))


### Features

* **templates:** adicionar novo template lista de applicações mobilidade ([dc872de](https://gitlab.com/fi-sas/fisas-reports/commit/dc872de4eea5835162d297d9f65c1882d4f19c83))

# [1.16.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.15.0...v1.16.0) (2021-09-15)


### Features

* **template-mobility:** add template list mobility ([d46cbc5](https://gitlab.com/fi-sas/fisas-reports/commit/d46cbc52642151cd10e7b8ae7935f2c23448919c))

# [1.15.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.14.2...v1.15.0) (2021-09-13)


### Bug Fixes

* **templates:** add accommodation application iban and allow direct debit ([dc73020](https://gitlab.com/fi-sas/fisas-reports/commit/dc7302079524698ad9a087bbe25ee0a1996820cb))


### Features

* **template:** add iban ([c7cdef4](https://gitlab.com/fi-sas/fisas-reports/commit/c7cdef44f287d22b65ffb8a32329351ddae9d188))

## [1.14.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.14.1...v1.14.2) (2021-09-02)


### Bug Fixes

* **reports:** change current accoun close cash account report to pdf ([bf48da1](https://gitlab.com/fi-sas/fisas-reports/commit/bf48da1d6a466cb3fd419ff11ddb2f72de50113c))
* **templates:** change misspelled template names ([769ef25](https://gitlab.com/fi-sas/fisas-reports/commit/769ef25917e73deaffd903a656a193a33eb77923))

## [1.14.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.14.0...v1.14.1) (2021-08-30)


### Bug Fixes

* **report:** change convert to xlsx on close cash account report ([abe2de7](https://gitlab.com/fi-sas/fisas-reports/commit/abe2de7b23a0fc80a79c14116cbff144eee4285f))

# [1.14.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.13.0...v1.14.0) (2021-08-24)


### Features

* **template:** Certificado Presença Saude ([c1a787f](https://gitlab.com/fi-sas/fisas-reports/commit/c1a787f7b0861b0674c119846b2d3099d148f667))

# [1.13.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.12.4...v1.13.0) (2021-08-23)


### Features

* **Saude:** Declaração de Presença ([0d2f3a1](https://gitlab.com/fi-sas/fisas-reports/commit/0d2f3a1483f4557e2658caf38c9a76e787b6f6d7))

## [1.12.4](https://gitlab.com/fi-sas/fisas-reports/compare/v1.12.3...v1.12.4) (2021-08-19)


### Bug Fixes

* **template:** fix dgs_code on mobility sub23 declaration ([4e7f340](https://gitlab.com/fi-sas/fisas-reports/commit/4e7f3407ddbdb671772dd7e0fa4b60f21b03ac60))

## [1.12.3](https://gitlab.com/fi-sas/fisas-reports/compare/v1.12.2...v1.12.3) (2021-07-30)


### Bug Fixes

* **reports:** cc report file rename ([3748485](https://gitlab.com/fi-sas/fisas-reports/commit/37484854836b18bbc872c387a3825f3f4d5ce675))

## [1.12.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.12.1...v1.12.2) (2021-07-27)


### Bug Fixes

* **seed:** template bolsa ([df8e400](https://gitlab.com/fi-sas/fisas-reports/commit/df8e4008955f8658ae6184a4b2b4b78c3cb112eb))
* **template:** upodate template candidaturas bolsa ([e7b7973](https://gitlab.com/fi-sas/fisas-reports/commit/e7b7973cd09b3ec3549aa7b1f28b4aa434c609c4))

## [1.12.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.12.0...v1.12.1) (2021-07-16)


### Bug Fixes

* **reports:** add missing date format ([7beaa28](https://gitlab.com/fi-sas/fisas-reports/commit/7beaa28275bc94837f1eba8801e6141e6c515560))
* **templates:** update templates ([6e4b98e](https://gitlab.com/fi-sas/fisas-reports/commit/6e4b98e8065633927d467d491298f24d8007c8b2))

# [1.12.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.11.1...v1.12.0) (2021-07-12)


### Features

* **reports:** add print action, to accept stream ([2a38f30](https://gitlab.com/fi-sas/fisas-reports/commit/2a38f30a2a07c63b9e06be6b84e86bc67d051ec8))

## [1.11.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.11.0...v1.11.1) (2021-07-09)


### Bug Fixes

* **temmplate-seed:** update file report and seed ([2e45190](https://gitlab.com/fi-sas/fisas-reports/commit/2e45190821075a7ac5693f1350eddcc8067f0c70))

# [1.11.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.10.0...v1.11.0) (2021-07-01)


### Bug Fixes

* **seed:** change bus sub23 application to declaration ([2ae3728](https://gitlab.com/fi-sas/fisas-reports/commit/2ae3728762565ac3444b313395b76c2d00f3f8b7))


### Features

* **reports:** add mobility sub 23 declaration template ([dc3c54c](https://gitlab.com/fi-sas/fisas-reports/commit/dc3c54c5c066eec2bcb507ac8932f2e2a2e0b265))

# [1.10.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.9.3...v1.10.0) (2021-06-29)


### Features

* **reports:** add remove report ([caffb5b](https://gitlab.com/fi-sas/fisas-reports/commit/caffb5be948c68f706ce8b1842dcc733d09d2ab8))

## [1.9.3](https://gitlab.com/fi-sas/fisas-reports/compare/v1.9.2...v1.9.3) (2021-06-16)


### Bug Fixes

* **templates:** add missing organic_unit field ([5bcdcf2](https://gitlab.com/fi-sas/fisas-reports/commit/5bcdcf2a2adcd4c86786bbc35fcd3bc0d77ad05c))

## [1.9.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.9.1...v1.9.2) (2021-06-15)


### Bug Fixes

* **reports:** fix errors on reports templates ([5568431](https://gitlab.com/fi-sas/fisas-reports/commit/55684310b342e27136827f1648cb9439f893d0e5))

## [1.9.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.9.0...v1.9.1) (2021-06-15)


### Bug Fixes

* **reports:** fix errors on reports templates ([180c4b5](https://gitlab.com/fi-sas/fisas-reports/commit/180c4b53b7d68f88eacaae51e4473ef13ee65217))

# [1.9.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.8.1...v1.9.0) (2021-06-07)


### Features

* **reports_template:** add report volunteering experience user interest ([9f93af3](https://gitlab.com/fi-sas/fisas-reports/commit/9f93af32d1309230ec53a92d55d95b5514559a07))
* **seed:** add volunteering experience user selection ([258ecd0](https://gitlab.com/fi-sas/fisas-reports/commit/258ecd07d2f30a07524e5ec0919bd6007cb26896))

## [1.8.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.8.0...v1.8.1) (2021-06-01)


### Bug Fixes

* **cc:** cashAccountCloseReport fix fields names ([b0685e8](https://gitlab.com/fi-sas/fisas-reports/commit/b0685e8907ec6ec8a5f42407465a130c2108dfef))
* **cc:** change templates name/description ([70b0df7](https://gitlab.com/fi-sas/fisas-reports/commit/70b0df7951704bd4671ea2015dd2ececf36e7d97))

# [1.8.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.7...v1.8.0) (2021-06-01)


### Features

* **cc:** add cashAccountClose report ([604c80b](https://gitlab.com/fi-sas/fisas-reports/commit/604c80b557aa40c9fc03c424856809e36c2db196))

## [1.7.7](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.6...v1.7.7) (2021-05-31)


### Bug Fixes

* **reports:** fix accommodation application report ([121daa8](https://gitlab.com/fi-sas/fisas-reports/commit/121daa835f38730e1d35fe6c6aee4bca817ba423))

## [1.7.6](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.5...v1.7.6) (2021-05-31)


### Bug Fixes

* **reports:** add missing fields to accomodation application report ([4b9598f](https://gitlab.com/fi-sas/fisas-reports/commit/4b9598f29249e62aa68b8c68b9b6e526238f93c1))

## [1.7.5](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.4...v1.7.5) (2021-05-21)


### Bug Fixes

* **seed:** scholarship seed error ([300833e](https://gitlab.com/fi-sas/fisas-reports/commit/300833ec1dbc7fe0fbbff9c32edaecf5bd44cb77))

## [1.7.4](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.3...v1.7.4) (2021-05-18)


### Bug Fixes

* **reports:** remove authorization for list reports ([44aee7c](https://gitlab.com/fi-sas/fisas-reports/commit/44aee7c225f3a11326b2c6f3a31735fecc322884))

## [1.7.3](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.2...v1.7.3) (2021-05-14)


### Bug Fixes

* **reports:** only set unread true on complete ([b57a4d7](https://gitlab.com/fi-sas/fisas-reports/commit/b57a4d7da15adc4804fdc6e06b5af0b82edee2af))

## [1.7.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.1...v1.7.2) (2021-05-14)


### Bug Fixes

* **reports:** send id field on list ([d29752d](https://gitlab.com/fi-sas/fisas-reports/commit/d29752d0586957bc08cc4329c47f23d2093640fb))

## [1.7.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.7.0...v1.7.1) (2021-05-14)


### Bug Fixes

* **reports:** misspelled created_at ([1157db0](https://gitlab.com/fi-sas/fisas-reports/commit/1157db0068e30e9b62f714de35c91cc5f7f91705))

# [1.7.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.6.0...v1.7.0) (2021-05-14)


### Features

* **reports:** add unread field and read endpoint ([11a918c](https://gitlab.com/fi-sas/fisas-reports/commit/11a918c499932388a8b796e2512316cf5b682f52))

# [1.6.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.5.3...v1.6.0) (2021-05-14)


### Bug Fixes

* **templates:** update template files od scholarship ([a4fa35f](https://gitlab.com/fi-sas/fisas-reports/commit/a4fa35f253536cb7a416662f1c2f24ef698f667a))


### Features

* **reports:** add feedback event on report complete ([b958d13](https://gitlab.com/fi-sas/fisas-reports/commit/b958d13b170a48540b7b48a0774dd39621d5cdfe))
* **seed-scholarship:** add template experience ([4a1a845](https://gitlab.com/fi-sas/fisas-reports/commit/4a1a845bc10c4685f078e97f2ae2bc7e8a26c307))

## [1.5.3](https://gitlab.com/fi-sas/fisas-reports/compare/v1.5.2...v1.5.3) (2021-05-07)


### Bug Fixes

* **reports:** change accommodation application template ([3139ae5](https://gitlab.com/fi-sas/fisas-reports/commit/3139ae519997ba91d890bf5b361f78418daa5e2d))
* **reports:** remove unused options ([0d8e4a7](https://gitlab.com/fi-sas/fisas-reports/commit/0d8e4a76fe46a823c7593ffd1333c2afa3aaed5e))

## [1.5.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.5.1...v1.5.2) (2021-05-06)


### Bug Fixes

* **reportes:** logo missing caracter ([573da8e](https://gitlab.com/fi-sas/fisas-reports/commit/573da8ee8806dfca31700630f0ee2c4b4ae6d286))
* **reports:** change document type ([39cf450](https://gitlab.com/fi-sas/fisas-reports/commit/39cf450711002da7e7ba072e2a2f3a9eef28b47b))

## [1.5.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.5.0...v1.5.1) (2021-05-04)


### Bug Fixes

* **seeds:** update seeds and template files ([34d8ed6](https://gitlab.com/fi-sas/fisas-reports/commit/34d8ed6f11bf313cdbe205510276e5138d9e2e4f))

# [1.5.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.4.0...v1.5.0) (2021-04-29)


### Features

* **templates:** add ubike applications list template ([8dad150](https://gitlab.com/fi-sas/fisas-reports/commit/8dad15085b67ceeb30433295ec632fcbbb45f2d5))
* **templates:** add ubike quest template ([766f3bf](https://gitlab.com/fi-sas/fisas-reports/commit/766f3bfa18245a028db1c98a47bc582c67530ac4))
* **templates:** add ubike stats template ([0f12c63](https://gitlab.com/fi-sas/fisas-reports/commit/0f12c63a5d3f4db802f19d4de3cce2fbc4261921))

# [1.4.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.3.0...v1.4.0) (2021-04-27)


### Features

* **reports:** add accommodation extras map template ([ae4b50e](https://gitlab.com/fi-sas/fisas-reports/commit/ae4b50e307e56ad3345dc2c2325c19a4a97dec6e))
* **reports:** add accommodation reigme map template ([352424e](https://gitlab.com/fi-sas/fisas-reports/commit/352424ea9a0e378c2092b0e3307f664d2d8dd99e))
* **reports:** add accommodation scholarship map template ([901b763](https://gitlab.com/fi-sas/fisas-reports/commit/901b763edbca30632460b840460db1c9287a460d))

# [1.3.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.2.1...v1.3.0) (2021-04-27)


### Features

* **files:** add template reports ([9d183cc](https://gitlab.com/fi-sas/fisas-reports/commit/9d183ccee4420aa1d641d34617e9dd4f4a8a1856))
* **seed:** add seed volunteering ([9505806](https://gitlab.com/fi-sas/fisas-reports/commit/9505806f5a87a796ebd29631ceff2f64c7898c69))
* **seeds:** add volunteering seeds ([d61230d](https://gitlab.com/fi-sas/fisas-reports/commit/d61230d29cee1bfb06c7481b2f7dfe096fa5bad8))

## [1.2.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.2.0...v1.2.1) (2021-04-26)


### Bug Fixes

* **reports:** fix recursive events ([c8d1bb2](https://gitlab.com/fi-sas/fisas-reports/commit/c8d1bb2be82e98bc1653f93a175fa13e357e110e))

# [1.2.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.1.1...v1.2.0) (2021-04-21)


### Features

* **templates:** add new template and seed ([de7879f](https://gitlab.com/fi-sas/fisas-reports/commit/de7879f425d47fbdeeae2fc46b1193014980c79a))

## [1.1.1](https://gitlab.com/fi-sas/fisas-reports/compare/v1.1.0...v1.1.1) (2021-04-20)


### Bug Fixes

* **accomodation:** change accommodation application report template ([63f8204](https://gitlab.com/fi-sas/fisas-reports/commit/63f82048156acb044e59c123eb33bea7671002e9))

# [1.1.0](https://gitlab.com/fi-sas/fisas-reports/compare/v1.0.0...v1.1.0) (2021-03-24)


### Features

* **seeds:** add seed and template doc ([d15ff81](https://gitlab.com/fi-sas/fisas-reports/commit/d15ff819a79c5056d4653bbd16440a9039f6beff))
* **seeds:** add template and seed ms_scholarship ([7ab4bbb](https://gitlab.com/fi-sas/fisas-reports/commit/7ab4bbb97cda3068b68fda49bd00d3eb89902771))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **docker-compose:** create docker-compose to prod and dev ([dd3bc8c](https://gitlab.com/fi-sas/fisas-reports/commit/dd3bc8c74b8a8c8d8345a8daa5122da6a217cc47))
* **post:** fix POST trying update ([e24c7cf](https://gitlab.com/fi-sas/fisas-reports/commit/e24c7cf4d0645b5849fe1a15a4f04d82da32eaa5))
* **template:** fix listing templates ([7de4747](https://gitlab.com/fi-sas/fisas-reports/commit/7de47478ab3ee9eb81a41c276353870dcc4dfb90))
* **templates:** add logs to catch to get error message ([5fb0cd3](https://gitlab.com/fi-sas/fisas-reports/commit/5fb0cd3da99cd9f1afba57c8b539b7edcc618558))
* **templates:** fix ms shutdown on template not found ([fb3a8ee](https://gitlab.com/fi-sas/fisas-reports/commit/fb3a8ee22281f3099ca915712f365c348336b2df))
* **templates:** fix overwriteen data ([60c8f82](https://gitlab.com/fi-sas/fisas-reports/commit/60c8f82232b0ab11a9c4d0a03f418ba0d155c5f8))
* **templates:** fix print template not found error ([e45b683](https://gitlab.com/fi-sas/fisas-reports/commit/e45b683f07f4c51c3c106212b9d552cd5230ce23))
* **templates:** fix set header after they are sent ([bb3f60e](https://gitlab.com/fi-sas/fisas-reports/commit/bb3f60e5299575d5dc5293ff008e1121a5bd4e78))
* **templates:** initialization args ([dd31824](https://gitlab.com/fi-sas/fisas-reports/commit/dd31824fff86ef4d9c5836ec3eda10fe854bcdc7))
* **templates:** validate template values is null ([7109e13](https://gitlab.com/fi-sas/fisas-reports/commit/7109e13da3c5cbb9c16c118c11bbef87fd4ac182))
* **test:** add dummy test to prevent pipeline warn ([b87cf17](https://gitlab.com/fi-sas/fisas-reports/commit/b87cf1769790237f034755c8909d0fb93d7b2a22))


### Features

* **cc:** add history-charges report template ([c303c3e](https://gitlab.com/fi-sas/fisas-reports/commit/c303c3eccde12adf13e9c1518bdbc270e4df7a18))
* now you can add the change the name of the report on creation ([c5158db](https://gitlab.com/fi-sas/fisas-reports/commit/c5158dbee2b6084dd3bd1efdf98b7bcdcd1ee915))
* **carbone:** update carbone version ([0f3927d](https://gitlab.com/fi-sas/fisas-reports/commit/0f3927dfa8b1d78f4e111e9ae558ee4678114cc8))
* **reports:** initial version of MS reports ([996c111](https://gitlab.com/fi-sas/fisas-reports/commit/996c111d745381f7ed07d14445f841f790cba1a4))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-reports/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-03-03)


### Bug Fixes

* **test:** add dummy test to prevent pipeline warn ([b87cf17](https://gitlab.com/fi-sas/fisas-reports/commit/b87cf1769790237f034755c8909d0fb93d7b2a22))


### Features

* **cc:** add history-charges report template ([c303c3e](https://gitlab.com/fi-sas/fisas-reports/commit/c303c3eccde12adf13e9c1518bdbc270e4df7a18))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-reports/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-02-22)


### Features

* now you can add the change the name of the report on creation ([c5158db](https://gitlab.com/fi-sas/fisas-reports/commit/c5158dbee2b6084dd3bd1efdf98b7bcdcd1ee915))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-reports/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-25)


### Bug Fixes

* **templates:** validate template values is null ([7109e13](https://gitlab.com/fi-sas/fisas-reports/commit/7109e13da3c5cbb9c16c118c11bbef87fd4ac182))

# 1.0.0-rc.1 (2020-12-29)


### Bug Fixes

* **docker-compose:** create docker-compose to prod and dev ([dd3bc8c](https://gitlab.com/fi-sas/fisas-reports/commit/dd3bc8c74b8a8c8d8345a8daa5122da6a217cc47))
* **post:** fix POST trying update ([e24c7cf](https://gitlab.com/fi-sas/fisas-reports/commit/e24c7cf4d0645b5849fe1a15a4f04d82da32eaa5))
* **template:** fix listing templates ([7de4747](https://gitlab.com/fi-sas/fisas-reports/commit/7de47478ab3ee9eb81a41c276353870dcc4dfb90))
* **templates:** add logs to catch to get error message ([5fb0cd3](https://gitlab.com/fi-sas/fisas-reports/commit/5fb0cd3da99cd9f1afba57c8b539b7edcc618558))
* **templates:** fix ms shutdown on template not found ([fb3a8ee](https://gitlab.com/fi-sas/fisas-reports/commit/fb3a8ee22281f3099ca915712f365c348336b2df))
* **templates:** fix overwriteen data ([60c8f82](https://gitlab.com/fi-sas/fisas-reports/commit/60c8f82232b0ab11a9c4d0a03f418ba0d155c5f8))
* **templates:** fix print template not found error ([e45b683](https://gitlab.com/fi-sas/fisas-reports/commit/e45b683f07f4c51c3c106212b9d552cd5230ce23))
* **templates:** fix set header after they are sent ([bb3f60e](https://gitlab.com/fi-sas/fisas-reports/commit/bb3f60e5299575d5dc5293ff008e1121a5bd4e78))
* **templates:** initialization args ([dd31824](https://gitlab.com/fi-sas/fisas-reports/commit/dd31824fff86ef4d9c5836ec3eda10fe854bcdc7))


### Features

* **carbone:** update carbone version ([0f3927d](https://gitlab.com/fi-sas/fisas-reports/commit/0f3927dfa8b1d78f4e111e9ae558ee4678114cc8))
* **reports:** initial version of MS reports ([996c111](https://gitlab.com/fi-sas/fisas-reports/commit/996c111d745381f7ed07d14445f841f790cba1a4))
