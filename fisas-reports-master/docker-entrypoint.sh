#!/usr/bin/env bash
set -Eeo pipefail

file_env() {
    local var="$1"
    local fileVar="${var}_FILE"
    local def="${2:-}"

    if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
        echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
        exit 1
    fi
    local val="$def"
    if [ "${!var:-}" ]; then
        val="${!var}"
    elif [ "${!fileVar:-}" ]; then
        val="$(<"${!fileVar}")"
    fi
    export "$var"="$val"
    unset "$fileVar"
}

for env_var in $(printenv | cut -f1 -d"=" | egrep FILE$ | sed "s/_FILE//g"); do
    file_env $env_var
done

npm run knex:migration:migrate
npm run knex:seed:run


exec "$@"
