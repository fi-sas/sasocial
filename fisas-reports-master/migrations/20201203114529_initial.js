exports.up = function (knex) {
	return knex.schema
		.createTable("template", (table) => {
			table.increments();
			table.string("key", 255).notNullable();
			table.string("name", 100).notNullable();
			table.string("description", 260);
			table.integer("file_id").unsigned().notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.json("options");
			table.json("data");
			table.unique(["key"]);
		})
		.createTable("report", (table) => {
			table.increments();
			table.string("name", 100).notNullable();
			table
				.enu("status", ["WAITING", "PROCCESSING", "READY", "FAILED"], {
					useNative: true,
					enumName: "report_status",
				})
				.defaultTo("WAITING")
				.notNullable();
			table.integer("template_id").unsigned().notNullable().references("id").inTable("template");
			table.integer("user_id").unsigned().notNullable();
			table.integer("file_id").unsigned().nullable();
			table.datetime("created_at").notNullable();
		});
};

exports.down = function (knex) {
	return knex.schema.dropTable("report").dropTable("template");
};
