exports.up = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.string("confirmation_path").nullable();
		table.json("extra_info").nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.dropColumn("confirmation_path");
		table.dropColumn("extra_info");
	});
};
