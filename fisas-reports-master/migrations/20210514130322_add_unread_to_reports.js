exports.up = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.boolean("unread").notNullable().defaultTo(true);
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.dropColumn("unread");
	});
};
