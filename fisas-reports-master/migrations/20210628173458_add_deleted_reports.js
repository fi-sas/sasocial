exports.up = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.boolean("deleted").notNullable().defaultTo(false);
		table.datetime("deleted_at").nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("report", (table) => {
		table.dropColumn("deleted_at");
		table.dropColumn("deleted");
	});
};
