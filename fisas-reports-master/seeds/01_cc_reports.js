const templates = [
	{
		key: "CC_USER_REPORT",
		name: "CC/Pagamentos - Relatorio utilizador Conta Corrente (movimentos)",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_USER_REPORT_HISTORY_CHARGES",
		name: "CC/Pagamentos - Relatorio utilizador Conta Corrente (histórico carregamentos)",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_CASH_ACCOUNT_CLOSE_REPORT",
		name: "CC/Pagamentos - Relatorio Fecho de Caixa",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"pdf","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"cash_account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_CHARGES_PERIOD_REPORT",
		name: "CC/Pagamentos - Relatorio Carregamentos",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"xlsx","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"cash_account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_CHARGES_DETAILED_PERIOD_REPORT",
		name: "CC/Pagamentos - Relatorio Carregamentos detalhado",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"xlsx","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"cash_account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_MOVEMENTS_PERIOD_REPORT",
		name: "CC/Pagamentos - Relatorio Movimentos",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"xlsx","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"Nome do utilizador"},"cash_account":{"name":"Nome da conta"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_MOVEMENTS_SALES_RESUME_PERIOD_REPORT",
		name: "CC/Pagamentos - Relatorio Vendas resumido",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"xlsx","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"--"},"account":{"name":"Principal"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CC_MOVEMENTS_SALES_DETAILED_PERIOD_REPORT",
		name: "CC/Pagamentos - Relatorio Vendas detalhe",
		description: "Conta corrente",
		file_id: 1,
		options: `{"convertTo":"xlsx","enum":{"OPERATIONS":{"CHARGE":"Carregamento","CANCEL":"Cancelamento","RECEIPT":"Recibo","INVOICE":"Fatura","REFUND":"Devolução","INVOICE_RECEIPT":"Fatura/Recibo","CHARGE_NOT_IMMEDIATE":"Carregamento Não Imediato","CREDIT_NOTE":"Nota de Crédito"},"STATUS":{"PAID":"Pago","PENDING":"Pendente","CONFIRMED":"Confirmado","CANCELLED":"Cancelado","CANCELLED_LACK_PAYMENT":"Falta de pagamento"}}}`,
		data: `{"user":{"name":"--"},"account":{"name":"Principal"},"movements":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
