const templates = [
	{
		key: "ALO_CAN_RAW",
		name: "Alojamento - Dados em bruto candidaturas",
		description: "todos os dados das candidaturas por ano letivo",
		file_id: 1,
		options: `{"convertTo": "xlsx","enum": {"STATUS": {"closed": "Contrato fechado","queued": "Em lista de espera","pending": "Em despacho","analysed": "Em analise","assigned": "Colocado","rejected": "Rejeitou","reopened": "Reaberto","cancelled": "Cancelada","confirmed": "Confirmado","contracted": "Contratado","unassigned": "Não colocado"},"YES_NO": {"true": "Sim","false": "Não"},"DECISION": {"ASSIGN": "Colocado","ENQUEUE": "Em lista de espera","UNASSIGN": "Não colocado"},"GENDER": {"M": "Masculino","F": "Feminino","U": "Outro"}}}`,
		data: `{"applications":[],"academic_year":"2020-2021"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ALO_CLOTHES_MAP",
		name: "Alojamento - Mapa de Controlo de Roupa",
		description: "Alojamento Mapa de Controlo de Roupa",
		file_id: 1,
		options: `{"convertTo":"pdf"}`,
		data: `{"residence":{"name":"Residencia"},"rooms":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ALO_RES_CAN",
		name: "Alojamento - Resultados de candidaturas ao alojamento",
		description: "Resultados de candidaturas ao alojamento",
		file_id: 1,
		options: `{"convertTo":"pdf","enum":{"STATUS":{"closed":"Contrato Fechado","queued":"Em lista de espera","pending":"Em despacho","analysed":"Em analise","assigned":"Colocado","rejected":"Rejeitou","cancelled":"Cancelada","confirmed":"Confirmado","contracted":"Contratado","unassigned":"Não colocado"}}}`,
		data: `{"residence_name":"Nome Residencia","application_phase":1,"academic_year":"2020-2021","applications":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ACCOMMODATION_APPLICATION_REPORT",
		name: "Alojamento - Impressão de candidatura",
		description: "Todas as informações da candidatura de um aluno",
		file_id: 1,
		options: `{"convertTo":"pdf","enum":{"STATUS":{"closed":"Contrato Fechado","queued":"Em lista de espera","pending":"Em despacho","analysed":"Em analise","assigned":"Colocado","rejected":"Rejeitou","cancelled":"Cancelada","confirmed":"Confirmado","contracted":"Contratado","unassigned":"Não colocado","withdrawal":"Desistiu"},"YES_NO":{"true":"Sim","false":"Não"}, "GENDER":{"M":"Masculino","F":"Feminino","O":"Outro"},"DECISION":{"ASSIGN":"Colocado","ENQUEUE":"Em lista de espera","UNASSIGN":"Não colocado"},"BILLING":{"waiting":"Pendente","proccessing":"Em processamento","proccessed":"Processado","verifieing":"Em Verificação","verified":"Vereficado","error_in_proccessing":"Erro em processamento","error_in_verifieing":"Erro em verificação"}}}`,
		data: `{ "full_name": "--", "applicant_birth_date": "--", "nationality": "--", "gender": "--", "identification": "--", "document_type": "--", "tin": "--", "incapacity": "--", "incapacity_type": "--", "incapacity_degree": "--", "father_name": "--", "mother_name": "--", "email": "--", "secundary_email": "--", "phone_1": "--", "phone_2": "--", "address": "--", "postal_code": "--", "city": "--", "country": "--", "household_distance": "--", "household_elements_number": "--", "househould_elements_in_university": "--", "household_total_income": "--", "patrimony_category.description": "--", "organic_unit": {     "name": "--" }, "course": {     "course_degree": {     "name": "--"     },     "name": "--" }, "course_year": "--", "student_number": "--", "registry_validity": "--", "admission_date": "--", "residence": {     "name": "--" }, "second_option_residence": {     "name": "--" }, "application_phase": "--", "submission_date": "--", "submitted_out_of_date": "--", "regime": {     "translations": [     {     "name": "--"     }     ] }, "extras": "--", "applied_scholarship": "--", "scholarship_value": "--", "observations": "--", "assigned_residence": {     "name": "--" }, "room": {     "name": "--" }, "iban": "--", "swift": "--", "applicant_consent": "--", "consent_date": "--", "decision": "--", "status": "--", "start_date": "--", "end_date": "--", "opposition_request": "--", "opposition_answer": "--", "has_opposed": "--", "charged_deposit": "--", "paid_deposit": "--", "available_deposit": "--", "available_retroactive": "--" }`,
		created_at: new Date(),
		updated_at: new Date(),
	},

	{
		key: "ACCOMMODATION_APPLICATION_SCHOOLARSHIP_REPORT",
		name: "Alojamento - Impressão de mapa dos alunos bolseiros",
		description: "Informações da candidatura dos alunos bolseiros",
		file_id: 1,
		options: `{	"convertTo": "xlsx","enum": {"STATUS": {"closed": "Contrato fechado",	"queued": "Em lista de espera","pending": "Em despacho",	"analysed": "Em analise",	"assigned": "Colocado",	"rejected": "Rejeitou",	"reopened": "Reaberto",	"cancelled": "Cancelada","confirmed": "Confirmado","contracted": "Contratado",	"unassigned": "Não colocado"},	"YES_NO": {	"true": "Sim","false": "Não"}}}`,
		data: `{"assignedResidence": {"name": "--"},"residence": {"name": "--"	},	"application_phase": "--",	"full_name": "--","identification": "--",	"tin": "--",	"student_number": "--","organic_unit": {"name": "--"	},	"email": "--",	"phone_1": "--",	"has_scholarship": "--",	"start_date": "--",	"end_date": "--",	"status": "--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},

	{
		key: "ACCOMMODATION_APPLICATION_REGIME_REPORT",
		name: "Alojamento - Impressão de mapa de candidaturas c/regimes",
		description: "Informações das candidaturas c/regimes ",
		file_id: 1,
		options: `{	"convertTo": "xlsx","enum": {"STATUS": {"closed": "Contrato fechado",	"queued": "Em lista de espera","pending": "Em despacho",	"analysed": "Em analise",	"assigned": "Colocado",	"rejected": "Rejeitou",	"reopened": "Reaberto",	"cancelled": "Cancelada","confirmed": "Confirmado","contracted": "Contratado",	"unassigned": "Não colocado"},	"YES_NO": {	"true": "Sim","false": "Não"}}}`,
		data: `{"assignedResidence": {"name": "--"},"residence": {"name": "--"	},	"application_phase": "--",	"full_name": "--","identification": "--",	"tin": "--",	"student_number": "--","organic_unit": {"name": "--"	},	"email": "--",	"phone_1": "--",	"has_scholarship": "--",	"start_date": "--",	"end_date": "--",	"status": "--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ACCOMMODATION_APPLICATION_EXTRAS_REPORT",
		name: "Alojamento - Impressão de mapa de candidaturas c/extras",
		description: "Informações das candidaturas c/extras ",
		file_id: 1,
		options: `{	"convertTo": "xlsx","enum": {"STATUS": {"closed": "Contrato fechado",	"queued": "Em lista de espera","pending": "Em despacho",	"analysed": "Em analise",	"assigned": "Colocado",	"rejected": "Rejeitou",	"reopened": "Reaberto",	"cancelled": "Cancelada","confirmed": "Confirmado","contracted": "Contratado",	"unassigned": "Não colocado"},	"YES_NO": {	"true": "Sim","false": "Não"}}}`,
		data: `{"assignedResidence": {"name": "--"},"residence": {"name": "--"	},	"application_phase": "--",	"full_name": "--","identification": "--",	"tin": "--",	"student_number": "--","organic_unit": {"name": "--"	},	"email": "--",	"phone_1": "--","extras": "--",	"start_date": "--",	"end_date": "--",	"status": "--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ACCOMMODATION_BILLING_REPORT",
		name: "Alojamento - Impressão dos processamentos efetuados",
		description: "Informações dos processamentos efetuados",
		file_id: 1,
		options: `{	"convertTo": "xlsx","enum": {"STATUS": {"PROCESSED": "Processado",	"REVIEWED": "Validado","BILLED": "Faturado", "PAID": "Pago", "CANCELLED": "cancelado"},	"YES_NO": {	"true": "Sim","false": "Não"}}}`,
		data: `{"billings":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ACCOMMODATION_BILLING_SEPA_REPORT",
		name: "Alojamento - Impressão dos SEPA efetuados",
		description: "Informações dos SEPA efetuados",
		file_id: 1,
		options: `{	"convertTo": "xlsx","enum": {"STATUS": {"PROCESSED": "Processado",	"REVIEWED": "Validado","BILLED": "Faturado", "PAID": "Pago", "CANCELLED": "cancelado"},	"YES_NO": {	"true": "Sim","false": "Não"}}}`,
		data: `{"billings":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "ACCOMMODATION_APPLICATION_TARIFFS_REPORT",
		name: "Alojamento - Impressão de mapa de tarifários",
		description: "Informações dos tarifários das candidaturas ",
		file_id: 1,
		options: `{	"convertTo": "xlsx"}`,
		data: `{"applications":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
