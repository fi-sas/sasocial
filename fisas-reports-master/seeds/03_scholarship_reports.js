const templates = [
	{
		key: "SOCIAL_SCHOLARSHIP_PAYMENT_MONTHLY_REPORT",
		name: "Bolsa Colaboradores - report mensal da experiencia do aluno",
		description: "Todas as informações do report mensal da experiencia do aluno",
		file_id: 1,
		options: `{"convertTo":"pdf"}`,
		data: `{"experience_id":"--", "date":"--", "full_name_student":"--", "course_name":"--", "full_name_advisor": "--", "total_hours": "--", "total_value":"--", "value_transfer":"--", "iban_student":"--", "cc_transfer":"--", "cc":"--", "attendances[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_HOURS_REPORT",
		name: "Bolsa Colaboradores - report de horas da experiencia do aluno",
		description: "Horas do aluno",
		file_id: 1,
		options: `{"convertTo":"pdf"}`,
		data: `{"experience":"", "application":"", "attendances": "", "totalHours": "", "date":"" }`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_CERTIFICATE",
		name: "Bolsa Colaboradores - certificado de participação",
		description: "Certificado de participação.",
		file_id: 1,
		options: `{"convertTo":"pdf"}`,
		data: `{"experience_name":"--", "student_name":"--", "student_number":"--", "course_name":"--" , "hours":"--" , "school_name":"--", "academic_year":"--", "responsable":"--", "advisor":"--", "date":"--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_APPLICATION",
		name: "Bolsa Colaboradores - candidatura",
		description: "Informação da candidatura",
		file_id: 1,
		options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"}, "STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
		data: `{"date":"--", "application": ""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_USER_INTEREST",
		name: "Bolsa Colaboradores - Manifestações de interesses",
		description: "Informação manifestações de interesse",
		file_id: 1,
		options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
		data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": ""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_USER_INTEREST_AND_SELECTION",
		name: "Bolsa Colaboradores - Manifestações de interesses",
		description: "Lista de manifestações de interesse e seleção",
		file_id: 1,
		options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
		data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": "", "user_interests_selection[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_MONTHLY_REPORT",
		name: "Report mensal - Bolsa de colaboradores",
		description: "Report de horas e avalição mensal",
		file_id: 1,
		options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
		data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": "", "user_interests_selection[]":"", "report": ""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_EXPERIENCE",
		name: "Report experiencia - Bolsa de colaboradores",
		description: "Resumo da experiencia",
		file_id: 1,
		options: `{"convertTo":"pdf"}`,
		data: `{"experience":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_OFFER",
		name: "Experiência - Oferta - Bolsa de colaboradores",
		description: "Experiência - Oferta",
		file_id: 171,
		options: `{"convertTo":"pdf", "enum":{"STATUS":{ "SUBMITTED":"Submetida","RETURNED":"Devolvida","ANALYSED":"Analisada", "APPROVED":"Aprovada", "PUBLISHED":"Publicada", "REJECTED":"Rejeitada", "CANCELLED":"Cancelada", "SEND_SEEM":"Enviada", "EXTERNAL_SYSTEM":"Sistema Externo", "SELECTION":"Seleção", "IN_COLABORATION":"Em Colaboração", "CLOSED":"Fechada", "CANCELLED":"Cancelada", "DISPATCH":"Em Despacho" }}}`,
		data: `{"date":"--", "submission_date":"--", "collaboration_start_date":"--", "collaboration_end_date":"--", "publish_date":"--", "experience_name":"--", "experience_responsable":"--", "organic_unit":"--", "active_collaborators":"--", "necessary_collaborators":"--", "status":"--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
