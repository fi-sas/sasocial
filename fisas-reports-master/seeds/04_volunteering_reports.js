const templates = [
    {
        key: "VOLUNTEERING_EXPERIENCE_CERTIFICATE",
        name: "Voluntariado - certificado de participação em ação de voluntariado",
        description: "Certificado de participação numa ação de voluntariado",
        file_id: 1,
        options: `{"convertTo":"pdf"}`,
        data: `{"experience_name":"--", "student_name":"--", "school_name": "--", "academic_year": "--", "responsable":"--", "advisor":"--", "date":"--"}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
    {
        key: "VOLUNTEERING_APPLICATION",
        name: "Voluntariado - candidatura",
        description: "Informação da candidatura",
        file_id: 1,
        options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"}}}`,
        data: `{"date":"--", "name":"--", "address":"--", "code_postal":"--",
        "location":"--", "birthdate":"--", "nationality":"--", "tin":"--", 
        "identification":"--", "student_number":"--", "mobile_phone":"--", "phone":"--",
        "email":"--", "academic_year":"--", "course_year":"--", "course_name":"--", "course_degree":"--",
        "school_name":"--", "iban":"--", "status":"--", "organic_units[]":"", "area_interests[]":"",
        "has_collaborated_last_year":"--", "previous_activity_description":"--", "has_profissional_experience":"--", "job_description":"--",
        "foreign_languages":"--", "language_skills[]":"", "skills[]": ""}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
    {
        key: "VOLUNTEERING_EXPERIENCE_USER_INTEREST",
        name: "Voluntariado - Inscrições ações de voluntariado",
        description: "Informação inscrições de voluntariado",
        file_id: 1,
        options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
        data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": ""}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
    {
        key: "VOLUNTEERING_EXPERIENCE_USER_INTEREST_AND_SELECTION",
        name: "Voluntariado - Incrições ações de voluntariado",
        description: "Lista de inscrições e seleções de voluntariado",
        file_id: 1,
        options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
        data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": "", "user_interests_selection[]":""}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
    {
        key: "VOLUNTEERING_LIST_APPLICATIONS",
        name: "Voluntariado - Lista de candidaturas",
        description: "Lista de candidaturas ao voluntariado",
        file_id: 1,
        options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
        data: `{"date":"--", "academic_year": "--", "application[]":""}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
    {
        key: "VOLUNTEERING_EXPERIENCE_USER_SELECTION",
        name: "Voluntariado - Selecionados ações de voluntariado",
        description: "Informação selecionados acções de voluntariado",
        file_id: 1,
        options: `{"convertTo":"pdf", "enum":{"YES_NO":{"true":"Sim","false":"Não"},"STATUS":{ "SUBMITTED":"Submetida","ANALYSED":"Em análise","INTERVIEWED":"Em entrevista", "APPROVED":"Aprovada", "WAITING":"Em fila de espera", "NOT_SELECTED":"Não selecionado", "ACCEPTED":"Aceite", "COLABORATION":"Em colaboração", "WITHDRAWAL":"Em desistência", "DECLINED":"Rejeitada", "CANCELLED":"Cancelada", "CLOSED":"Fechada" }}}`,
        data: `{"date":"--", "experience_name":"--", "experience_responsable":"--", "experience_advisor":"--", "organic_unit":"--", "user_interests[]": ""}`,
        created_at: new Date(),
        updated_at: new Date(),
    },
]

    exports.seed = (knex) => {
        return Promise.all(
            templates.map((template) =>
                knex("template")
                    .select()
                    .where("key", template.key)
                    .then(async (rows) => {
                        if (rows.length === 0) {
                            return knex("template").insert(template).returning("id");
                        }
                        return true;
                    })
                    .catch((err) => {
                        // eslint-disable-next-line no-console
                        console.error(err);
                    }),
            ),
        );
    };