const templates = [
	{
		key: "UBIKE_APPLICATIONS",
		name: "UBike - Informações das candidaturas",
		description: "Informações das candidaturas",
		file_id: 1,
		options: `{"convertTo": "xlsx","enum": {"DAILY_COMMUTE": { "Walk": "A pé", "Car": "Carro", "Motorcycle": "Mota", "Bus": "Autocarro", "Train": "Comboio", "Bike": "Bicicleta", "Other": "Outro"},"GENDER": { "M": "Masculino", "F": "Feminino"},"DOCUMENT_TYPE": { "RESIDENCE_AUTHORIZATION": "Autorização de Residência", "BI": "Bilhete de Identidade", "ANGOLAN_BI": "Bilhete de Identidade Angolano", "FOREIGN_BI": "Bilhete de Identidade Estrangeiro", "MILITARY_BI": "Bilhete de Identidade Militar", "CC": "Cartão de Cidadão", "RESIDENCE_CARD": "Cartão de Residência", "UE_RESIDENCE_CARD": "Cartão de Residência permanente da UE", "UE_CITIZEN_CERTIFICATE": "Certificado de registo da UE", "PASSPORT": "Passaporte"},"STATUS": { "elaboration": "Em elaboração", "submitted": "Submetida", "analysis": "Em analise", "cancelled": "Cancelada", "dispatch": "Em despacho", "assigned": "Colocado", "enqueued": "Em Espera", "unassigned": "Não colocado", "suspended": "Suspensa", "complaint": "Queixa", "accepted": "Aceite", "contracted": "Contratad", "rejected": "Rejeitada", "quiting": "Sair", "withdrawal": "Desistencia", "closed": "Contrato fechado"},"DECISION": { "assigned": "Colocado", "enqueued": "Em Espera", "unassigned": "Não colocado"},"YES_NO": { "true": "Sim", "false": "Não"}}}`,
		data: `{ "applications": [ { "full_name": "--","student_number": "--","daily_commute": "--","daily_kms": "--","daily_co2": "--","gender": "--","birth_date": "--","weight": "--","height": "--","phone_number": "--","email": "--","bike": {"identification": "--" },"created_at": "--","start_date": "--", "end_date": "--","decision": "--", "status": "--" }]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_STATS",
		name: "UBike - Estatisticas",
		description: "UBike - Estatisticas",
		file_id: 1,
		options: `{"convertTo": "pdf"}`,
		data: `{"stats":[{"total_bikes": "--","bikes_in_use": "--","bikes_usage_perc": "--","aggregate_data": {"count_trips": "--","sum_distance": "--","sum_energy": "--","sum_time": "--","average_time": "--","sum_watts": "--","sum_co2_emissions_saved": "--","average_height_diff": "--","max_height_diff": "--","average_avg_speed": "--","max_avg_speed": "--","bus_saved_co2": "--","train_saved_co2": "--","gasoline_saved_co2": "--","diesel_saved_co2": "--","gpl_saved_co2": "--","eletric_saved_co2": "--"}}]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_QUEST",
		name: "UBike - Questionário",
		description: "Questionário",
		file_id: 1,
		options: `{"convertTo": "pdf", "enum": { "DAILY_COMMUTE": { "Walk": "A pé", "Car": "Carro", "Motorcycle": "Mota", "Bus": "Autocarro", "Train": "Comboio", "Bike": "Bicicleta", "Other": "Outro" }, "GENDER": { "M": "Masculino", "F": "Feminino" }, "CONSUMPTION": { "Electric": "Elétrica", "GPL": "GPL", "Diesel": "Diesel", "Gasoline": "Gasolina" }    }}`,
		data: `{"application": {"institute": "--","profile": "--","gender": "--","daily_kms": "--","number_of_times": "--","avg_weekly_distance": "--","daily_commute": "--","travel_time_school": "--","typology_first_preference": {"name": "--"},"travel_time_school_bike": "--","daily_consumption": "--","avg_vehicle_consumption_100": "--"}}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_APPLICATIONS_REPORT",
		name: "UBike - Listagem das candidaturas",
		description: "Listagem das candidaturas",
		file_id: 1,
		options: `{"convertTo": "xlsx", "enum": { "STATUS": { "elaboration": "Em elaboração", "submitted": "Submetida", "analysis": "Em analise", "cancelled": "Cancelada", "dispatch": "Em despacho", "assigned": "Colocado", "enqueued": "Em Espera", "unassigned": "Não colocado", "suspended": "Suspensa", "complaint": "Queixa", "accepted": "Aceite", "contracted": "Contratad", "rejected": "Rejeitada", "quiting": "Sair", "withdrawal": "Desistencia", "closed": "Contrato fechado"}}}`,
		data: `{ "applications": [ { "full_name": "--","organic_unit": "--","tin": "--","bike": {"identification": "--" },"created_at": "--","start_date": "--", "end_date": "--","assignment_date": "--", "status": "--" }]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_KMS_TRAVELED_REPORT",
		name: "UBike - Quilómetros percorridos",
		description: "Quilómetros percorridos",
		file_id: 1,
		options: `{"convertTo": "xlsx" }`,
		data: `{ "trips": [ { "user_id": "--","name": "--","email": "--", "bike_id": "--","distance": "--" }]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_TERM_OF_ACCEPT",
		name: "UBike - Termo de aceitação",
		description: "Termo de aceitação",
		file_id: 1,
		options: `{"convertTo": "pdf" }`,
		data: `{"application": {"full_name": "--", "perfil": "--", "organic_unit": "--", "identification": "--", "city": "--", "bike": {"identification": "--", "serial_number": "--" }, "emitted_day": "--", "emitted_month": "--","emitted_year": "--"}}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "UBIKE_DELIVERY_RECEPTION",
		name: "UBike - Auto de Entrega Recepção",
		description: "Auto de Entrega Recepção",
		file_id: 1,
		options: `{"convertTo": "pdf" }`,
		data: `{"application": {"full_name": "--", "email": "--", "phone_number": "--", "student_number": "--", "bike": {"identification": "--", "serial_number": "--" }, "emitted_day": "--", "emitted_month": "--","emitted_year": "--"}}`,
		created_at: new Date(),
		updated_at: new Date(),
},
{
		key: "UBIKE_BIKE_STATS",
		name: "UBike - Lista de Bicicletas",
		description: "Lista de Bicicletas",
		file_id: 1,
		options: `{"convertTo": "pdf" }`,
		data: `{ "bikes": [ { "identification": "--", "name": "--" , "full_name": "--" , "total_user": "--","total_bike": "--" }]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
