const templates = [
	{
		key: "MOBILITY_APPLICATION_DECLARATION",
		name: "Mobilidade - Declaração da candidatura",
		description: "Declação provisória da candidatura",
		file_id: 1,
		options: `{"convertTo": "pdf"}`,
		data: `{}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "MOBILITY_SUB_23_DECLARATION",
		name: "Mobilidade - Declaração para passe Sub 23",
		description: "Declaração para passe Sub 23",
		file_id: 1,
		options: `{
			"convertTo": "pdf"
		}`,
		data: `{}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "MOBILITY_SUB_23_DECLARATION_LIST",
		name: "Mobilidade - Lista de declaração para passe Sub 23",
		description: "Lista de declaração para passe Sub 23",
		file_id: 1,
		options: `{"convertTo": "xlsx", "enum":{ "STATUS":{ "submitted":"Submetida","in_validation":"Em validação","validated":"Validada", "emitted":"Emitida", "rejected":"Rejeitada"}}}`,
		data: `{"declaration":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "MOBILITY_APPLICATION_LIST",
		name: "Mobilidade - Lista de candidaturas",
		description: "Lista de candidaturas",
		file_id: 1,
		options: `{"convertTo": "xlsx", "enum":{ "STATUS":{ "submitted":"Submetida","approved":"Aprovada","not_approved":"Não aprovada", "accepted":"Aceite", "rejected":"Rejeitada", "withdrawal":"Desistiu", "closed":"Fechada", "canceled":"Cancelada", "quiting":"Em desistência", "suspended":"Suspensa"}}}`,
		data: `{"applications":[]}`,
		created_at: new Date(),
		updated_at: new Date(),
	}
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
