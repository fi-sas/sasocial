const templates = [
	{
		key: "HEALTH_ATTENDANCE_DECLARATION",
		name: "Saúde - Declaração de Presença",
		description: "Declaração de Presença",
		file_id: 1,
		options: `{"convertTo": "pdf"}`,
		data: `{"name":"Nome Utente","niss":"Numero Utente","date":"Data Consulta","start_hour":"Hora Inicio","end_hour":"Hora Fim","specialty_name":"Nome Especialidade", "specialty_place":"Lugar Especialidade", "certificate_date": "Data Emissao Certificado"}`,
		created_at: new Date(),
		updated_at: new Date(),
	}
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
