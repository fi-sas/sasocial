const templates = [
	{
		key: "CALENDAR_EVENTS_SUBSCRIPTIONS",
		name: "Calendário - Inscrições / Manifestações de interesse",
		description: "Inscrições / Manifestações de interesse",
		file_id: 1,
		options: `{"convertTo": "pdf"}`,
		data: `{"event":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CALENDAR_RECURSIVE_EVENTS_SUBSCRIPTIONS",
		name: "Calendário - Inscrições / Manifestações de interesse",
		description: "Inscrições / Manifestações de interesse",
		file_id: 1,
		options: `{"convertTo": "pdf"}`,
		data: `{"event":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CALENDAR_EXPORT_GOOGLE_ICAL",
		name: "Calendário - Exportação Calendário",
		description: "Exportação Calendário",
		file_id: 1,
		options: `{"convertTo": "xlsx"}`,
		data: `{"event":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "CALENDAR_EXPORT_OUTLOOK",
		name: "Calendário - Exportação Calendário",
		description: "Exportação Calendário",
		file_id: 1,
		options: `{"convertTo": "xlsx"}`,
		data: `{"event":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
