const templates = [
	{
		key: "EMERGENCY_FUND_PAYMENT_MAP_REPORT",
		name: "Fundo de emergência - report mapa de pagamento",
		description: "Todas as informações do report mapa de pagamento",
		file_id: 76,
		options: `{"convertTo":"xlsx", "enum": {"EXPENSE_STATUS": {"SUBMITTED": "Submetida", "VALIDATED": "Validada", "PROCESSED": "Processada", "CORRECTED": "Corrigida", "PAID": "Paga"},	"PAYMENT_MAP_STATUS": {"GENERATED": "Gerado", "VALIDATED": "Validado", "PAID": "Pago"}}}`,
		data: `{"notes":"--", "date":"--", "status":"--", "total_expense_value":"--", "existing_value":"--", "final_balance":"--", "expenses[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_PAYMENT_MAP_REPORT_BY_SUPPORT",
		name: "Fundo de emergência - report mapa de pagamento por âmbito",
		description: "Todas as informações do report mapa de pagamento por âmbito",
		file_id: 79,
		options: `{"convertTo":"xlsx", "enum": {"EXPENSE_STATUS": {"SUBMITTED": "Submetida", "VALIDATED": "Validada", "PROCESSED": "Processada", "CORRECTED": "Corrigida", "PAID": "Paga"},	"PAYMENT_MAP_STATUS": {"GENERATED": "Gerado", "VALIDATED": "Validado", "PAID": "Pago"}}}`,
		data: `{"notes":"--", "date":"--", "status":"--", "total_expense_value":"--", "support_description":"--", "existing_value":"--", "final_balance":"--", "expenses[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_PAYMENT_MAP_REPORT_BY_STUDENT",
		name: "Fundo de emergência - report mapa de pagamento por aluno",
		description: "Todas as informações do report mapa de pagamento por aluno",
		file_id: 78,
		options: `{"convertTo":"xlsx", "enum": {"EXPENSE_STATUS": {"SUBMITTED": "Submetida", "VALIDATED": "Validada", "PROCESSED": "Processada", "CORRECTED": "Corrigida", "PAID": "Paga"},	"PAYMENT_MAP_STATUS": {"GENERATED": "Gerado", "VALIDATED": "Validado", "PAID": "Pago"}}}`,
		data: `{"notes":"--", "date":"--", "status":"--", "total_expense_value":"--", "student_number":"--", "existing_value":"--", "final_balance":"--", "expenses[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_GLOBAL_REPORT",
		name: "Fundo de emergência - report mapa de despesas global por período de tempo",
		description: "Todas as informações do report mapa de despesas global por período de tempo",
		file_id: 92,
		options: `{"convertTo":"pdf"}`,
		data: `{"date":"--", "start_date":"--", "end_date":"--", "expenses[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_REPORT_BY_SUPPORT",
		name: "Fundo de emergência - report mapa de despesas por âmbito por período de tempo",
		description: "Todas as informações do report mapa de despesas por âmbito por período de tempo",
		file_id: 93,
		options: `{"convertTo":"pdf"}`,
		data: `{"date":"--", "start_date":"--", "end_date":"--", "expenses[]":"", "support_id":"--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_REPORT_BY_STUDENT",
		name: "Fundo de emergência - report mapa de despesas por aluno por período de tempo",
		description: "Todas as informações do report mapa de despesas por aluno por período de tempo",
		file_id: 94,
		options: `{"convertTo":"pdf"}`,
		data: `{"date":"--", "start_date":"--", "end_date":"--", "expenses[]":"", "student_number":"--"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_STUDENT_REPORT",
		name: "Fundo de emergência - report mapa de alunos / candidatos por mês",
		description: "Todas as informações do report mapa de alunos / candidatos por mês",
		file_id: 89,
		options: `{"convertTo":"pdf"}`,
		data: `{"date":"--", "start_date":"--", "end_date":"--", "students_data[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "EMERGENCY_FUND_RAW_DATA_REPORT",
		name: "Fundo de emergência - report listagem dados em bruto",
		description: "Todas as informações do report listagem dados em bruto",
		file_id: 85,
		options: `{"convertTo":"pdf", "enum": {"STATUS": {"SUBMITTED": "Submetida", "ANALYZED": "Analisada", "INTERVIEWED": "Em Entrevista", "ORDERED": "Em Despacho", "APPROVED": "Aprovada", "CANCELLED": "Cancelada", "REJECTED": "Rejeitada", "CLOSED": "Fechada"}}}`,
		data: `{"date":"--", "start_date":"--", "end_date":"--", "raw_data[]":""}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
