const templates = [
	{
		key: "FOOD_WHAREHOUSE_STOCK_REPORT",
		name: "Alimentação - Relatório de inventário em armazém",
		description: "Alimentação - Relatório de inventário em armazém",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{}}`,
		data: `{ "stocks": [], "wharehouse": { "name": "-" }}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "FOOD_RESERVATIONS_REPORT",
		name: "Alimentação - Relatório de reservas de cantinas",
		description: "Alimentação - Relatório de reservas de cantinas",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{"meal": {
            "lunch": "Almoço",
            "dinner": "Jantar"
        }}}`,
		data: `{ "reservations": [], "created_at": "2021-01-01 00:00:00", "start_date": "2021-01-01", "end_date": "2021-01-01"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "FOOD_ORDERS_REVENUE_REPORT",
		name: "Alimentação - Relatório de receita de pedidos de bar",
		description: "Alimentação - Relatório de receita de pedidos de bar",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{}}`,
		data: `{ "rows": [], "created_at": "2021-01-01 00:00:00", "start_date": "2021-01-01", "end_date": "2021-01-01"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "FOOD_ORDERS_REVENUE_PRODUCT_REPORT",
		name: "Alimentação - Relatório de receita de produtos",
		description: "Alimentação - Relatório de receita de produtos",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{}}`,
		data: `{ "products": [], "created_at": "2021-01-01 00:00:00", "start_date": "2021-01-01", "end_date": "2021-01-01"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
	{
		key: "FOOD_RESERVATIONS_REVENUE_REPORT",
		name: "Alimentação - Relatório de receita das reservas",
		description: "Alimentação - Relatório de receita das reservas",
		file_id: 1,
		options: `{"convertTo":"ods","enum":{}}`,
		data: `{ "total": 0, "rows": [], "created_at": "2021-01-01 00:00:00", "start_date": "2021-01-01", "end_date": "2021-01-01"}`,
		created_at: new Date(),
		updated_at: new Date(),
	},
];

exports.seed = (knex) => {
	return Promise.all(
		templates.map((template) =>
			knex("template")
				.select()
				.where("key", template.key)
				.then(async (rows) => {
					if (rows.length === 0) {
						return knex("template").insert(template).returning("id");
					}
					return true;
				})
				.catch((err) => {
					// eslint-disable-next-line no-console
					console.error(err);
				}),
		),
	);
};
