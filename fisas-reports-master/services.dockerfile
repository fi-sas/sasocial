FROM node:12-buster-slim

# remove all old version of LibreOffice
#RUN apt remove --purge libreoffice*
#RUN apt autoremove --purge

# Download LibreOffice debian package. Select the right one (64-bit or 32-bit) for your OS.
# Get the latest from http://download.documentfoundation.org/libreoffice/stable
# or download the version currently "carbone-tested":
COPY LibreOffice_7.1.4_Linux_x86-64_deb_langpack_pt.tar.gz ./
COPY LibreOffice_7.1.4_Linux_x86-64_deb.tar.gz ./

# Install required dependencies on ubuntu server for LibreOffice 5.0+
RUN echo deb http://ftp.us.debian.org/debian buster main contrib >> /etc/apt/sources.list;
RUN apt-get upgrade;
# RUN echo deb http://archive.ubuntu.com/ubuntu groovy universe multiverse >> /etc/apt/sources.list;
RUN apt-get update -y && apt-get install -y && apt install -y libxinerama1 libfontconfig1 libdbus-glib-1-2 libcairo2 libcups2 libglu1-mesa libsm6 locales

# Set locale
RUN locale-gen pt_PT.UTF-8
ENV LANG pt_PT.UTF-8
ENV LANGUAGE pt_PT:pt
ENV LC_ALL pt_PT.UTF-8

# Uncompress package
RUN tar -zxvf LibreOffice_7.1.4_Linux_x86-64_deb.tar.gz
WORKDIR /LibreOffice_7.1.4.2_Linux_x86-64_deb/DEBS

# Install LibreOffice
RUN dpkg -i *.deb

WORKDIR /
RUN tar -zxvf LibreOffice_7.1.4_Linux_x86-64_deb_langpack_pt.tar.gz
WORKDIR /LibreOffice_7.1.4.2_Linux_x86-64_deb_langpack_pt/DEBS

# Install LibreOffice Language Pack
RUN dpkg -i *.deb

# If you want to use Microsoft fonts in reports, you must install the fonts
# Andale Mono, Arial Black, Arial, Comic Sans MS, Courier New, Georgia, Impact,
# Times New Roman, Trebuchet, Verdana,Webdings)
#COPY ttf-mscorefonts-installer_3.6_all.deb ./
#RUN apt install ./ttf-mscorefonts-installer_3.6_all.deb
RUN apt install -y ttf-mscorefonts-installer

# If you want to use special characters, such as chinese ideograms, you must install a font that support them
# For example:
RUN apt install -y fonts-wqy-zenhei

ARG NODE_ENV

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm i

COPY . .

RUN chmod +x ./docker-entrypoint.sh

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]

CMD npm run start:$NODE_ENV
