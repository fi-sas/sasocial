"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const QueueService = require("moleculer-bull");
const carbone = require("carbone");
const fs = require("fs");
const _ = require("lodash");
const FileType = require("file-type");

/**
 * urls
params
data
options
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "reports.reports",
	table: "report",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("reports", "reports"), QueueService(process.env.REDIS_QUEUE)],

	/**
	 * QUEUES
	 */
	queues: {
		"reports.proccess"(job) {
			this.logger.info("NEW JOB RECEIVED!");

			return this.proccessReport(
				job.data.report,
				job.data.template,
				job.data.options,
				job.data.data,
			).then(() => {
				this.logger.info("ANTES DE MANDAR O RESOLVE");
				return Promise.resolve({
					done: true,
					id: job.data.id,
					worker: process.pid,
				});
			});
			//.catch(err => {
			//	this.logger.info("ANTES DE MANDAR O REJECT");
			//	return Promise.reject(err);
			//});
		},
	},

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"status",
			"template_id",
			"user_id",
			"file_id",
			"confirmation_path",
			"extra_info",
			"unread",
			"deleted",
			"deleted_at",
			"created_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			status: {
				type: "enum",
				values: ["WAITING", "PROCCESSING", "READY", "FAILED"],
			},
			template_id: { type: "number", convert: true },
			user_id: { type: "number", convert: true },
			file_id: { type: "number", convert: true, optional: true },
			created_at: { type: "date", optional: true },
			confirmation_path: { type: "string", optional: true },
			extra_info: { type: "object", optional: true },
			unread: { type: "boolean", optional: true, default: true },
			deleted: { type: "boolean", optional: true, default: false },
			deleted_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.status = "WAITING";
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.unread = true;
					ctx.params.deleted = false;
					ctx.params.deleted_at = null;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function userOnly(ctx) {
					ctx.params.query = ctx.params.query ? ctx.params.query : {};
					ctx.params.query.user_id = ctx.meta.user.id;
					ctx.params.query.deleted = false;
					ctx.params.fields = ctx.params.fields || [
						"id",
						"name",
						"status",
						"created_at",
						"unread",
						"file_id",
					];
					ctx.params.sort = ctx.params.sort || "-created_at";
				},
			],
		},
		after: {
			createFromStream: [
				function addJob(ctx, res) {
					this.addJob(ctx, res[0]);
					delete res[0].data;
					return res;
				},
			],
			create: [
				function addJob(ctx, res) {
					this.addJob(ctx, res[0]);
					delete res[0].data;
					return res;
				},
			],
			list: [
				function addUnreadCount(ctx, res) {
					return ctx
						.call("reports.reports.count", {
							query: {
								unread: true,
								user_id: ctx.meta.user.id,
								deleted: false,
							},
						})
						.then((total_unread) => {
							res.rows.forEach((row) => {
								row.total_unread = total_unread;
							});

							return res;
						});
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		createFromStream: {
			handler(ctx) {
				if (ctx.params && typeof ctx.params === "object" && typeof ctx.params.pipe === "function") {
					return new Promise((resolve, reject) => {
						ctx.params.setEncoding("UTF8"); // Set the encoding to be utf8.
						let data = "";

						// Handle stream events --> data, end, and error
						ctx.params.on("data", (chunk) => {
							data += chunk;
						});

						ctx.params.on("end", () => {
							ctx.params = JSON.parse(data);
							ctx.params.created_at = new Date();
							ctx.params.status = "WAITING";
							ctx.params.user_id = ctx.meta.user.id || 1;
							ctx.params.unread = true;
							ctx.params.deleted = false;
							ctx.params.deleted_at = null;
							let params = ctx.params;
							resolve(this._create(ctx, _.omit(params, ["data"])));
						});

						ctx.params.on("error", (err) => {
							reject(err);
						});
					});
				} else {
					return ctx.call("reports.reports.create", ctx.params);
				}
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			authorization: false,
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
				],
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		setReaded: {
			rest: "PUT /:id/read",
			visibility: "published",
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const reports = await ctx.call("reports.reports.find", {
					fields: ["id"],
					query: {
						user_id: ctx.meta.user.id || 1,
						id: ctx.params.id,
					},
				});

				if (reports.length > 0) {
					return ctx
						.call("reports.reports.patch", {
							id: ctx.params.id,
							unread: false,
						})
						.then(() => {
							return [];
						});
				}

				return [];
			},
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			scope: null,
			params: {
				id: { type: "number", convert: true, integer: true, positive: true },
			},
			async handler(ctx) {
				const reports = await ctx.call("reports.reports.find", {
					fields: ["id"],
					query: {
						user_id: ctx.meta.user.id || 1,
						deleted: false,
						id: ctx.params.id,
					},
				});

				if (reports.length > 0) {
					return ctx
						.call("reports.reports.patch", {
							id: ctx.params.id,
							deleted: true,
							deleted_at: new Date(),
						})
						.then(() => {
							ctx.call("media.files.remove", { id: reports[0].file_id });
							return [];
						});
				}

				return [];
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		addJob(ctx, report) {
			this.logger.info("CREATE JOB RECEIVED!", ctx.params);

			ctx
				.call("reports.templates.get", {
					id: report.template_id,
				})
				.then((template) => {
					this.createJob(
						"reports.proccess",
						{
							id: report.id,
							pid: process.pid,
							report,
							template: template[0],
							options: ctx.params.options,
							data: ctx.params.data,
						},
						{
							attempts: 1,
							removeOnComplete: true,
							removeOnFail: true,
						},
					);
				});
			return true;
		},
		proccessReport(report, template, options, data) {
			if (!options.convertTo) {
				options.convertTo = "pdf";
			}

			let replacement = "_";
			// eslint-disable-next-line no-useless-escape
			let illegalRe = /[\s\/\?<>\\:\*\|"]/g;
			// eslint-disable-next-line no-control-regex
			let controlRe = /[\x00-\x1f\x80-\x9f]/g;
			let reservedRe = /^\.+$/;
			// eslint-disable-next-line security/detect-unsafe-regex
			let windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
			// eslint-disable-next-line no-useless-escape
			let windowsTrailingRe = /[\. ]+$/;

			return new Promise((resolve, reject) => {
				try {
					carbone.render(`/uploads/${template.file.path}`, data, options, (err1, result) => {
						if (err1) {
							return reject(err1);
						}
						// write the result
						const filename = template.name
							.replace(illegalRe, replacement)
							.replace(controlRe, replacement)
							.replace(reservedRe, replacement)
							.replace(windowsReservedRe, replacement)
							.replace(windowsTrailingRe, replacement)
							.toLowerCase()
							.concat("_", Date.now(), ".", options.convertTo);
						FileType.fromBuffer(result)
							.then((mimetype) => {
								fs.writeFileSync("/uploads/".concat(filename), result);

								const file_data = {
									file_category_id: 1,
									public: false,
									type: "DOCUMENT",
									mime_type: mimetype.mime,
									path: filename,
									filename: filename,
									weight: 0,
									updated_at: new Date(),
									created_at: new Date(),
								};

								return this.broker
									.call("media.files.create", file_data)
									.then((file) => {
										return this.broker
											.call("reports.reports.patch", {
												id: report.id,
												file_id: file[0].id,
											})
											.catch((error_update) => {
												return reject(error_update);
											});
									})
									.then(() => {
										return resolve();
									})
									.catch((error_files) => {
										return reject(error_files);
									});
							})
							.catch((error_mimetype) => {
								return reject(error_mimetype);
							});
					});
				} catch (error_carbone) {
					return reject(error_carbone);
				}
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.getQueue("reports.proccess").empty();

		this.getQueue("reports.proccess").on("progress", (jobID, progress) => {
			this.logger.info(`Job #${jobID} progress is ${progress}%`);
		});

		this.getQueue("reports.proccess").on("active", (job, res) => {
			this.logger.info(`Job #${job.id} active!. Result:`, res);
			this.broker.call("reports.reports.patch", {
				id: job.data.report.id,
				status: "PROCCESSING",
			});
		});

		this.getQueue("reports.proccess").on("completed", (job, res) => {
			this.logger.info(`Job #${job.id} completed!. Result:`, res);

			this.broker
				.call("reports.reports.patch", {
					id: job.data.report.id,
					unread: true,
					status: "READY",
				})
				.then((report) => {
					if (report[0].confirmation_path) {
						// SEND CONFIRMAITION TO OTHER MS
						this.broker.emit(report[0].confirmation_path, { ...report[0] });
					}
				});
		});

		this.getQueue("reports.proccess").on("failed", (job, err) => {
			this.logger.info(`Job #${job.id} failed!. Result:`, err);
			this.broker.call("reports.reports.patch", {
				id: job.data.report.id,
				status: "FAILED",
				error_message: err.message,
			});
		});
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
