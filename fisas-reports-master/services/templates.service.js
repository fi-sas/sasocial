"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError, EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;
const Stream = require("stream");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "reports.templates",
	table: "template",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("reports", "templates")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"key",
			"name",
			"description",
			"file_id",
			"options",
			"data",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			key: { type: "string" },
			name: { type: "string" },
			description: { type: "string", optional: true },
			file_id: { type: "number", convert: true },
			options: { type: "object", optional: true },
			data: { type: "object", optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					const templates = ctx.call("reports.templates.find", {
						query: {
							key: ctx.params.key,
						},
					});

					if (templates.length > 0) {
						throw new ValidationError(
							"Already exist a template with this key",
							"ERR_DUPLICATED_KEY_TEMPLATE",
							{
								field: "key",
							},
						);
					}

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				async function sanatizeParams(ctx) {
					const templates = await ctx.call("reports.templates.find", {
						query: {
							key: ctx.params.key,
						},
					});

					if (templates.length > 0 && templates[0].id != ctx.params.id) {
						throw new ValidationError(
							"Already exist a template with this key",
							"ERR_DUPLICATED_KEY_TEMPLATE",
							{
								field: "key",
							},
						);
					}

					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		printFromStream: {
			visibility: "public",
			handler(ctx) {
				if (ctx.params && typeof ctx.params === "object" && typeof ctx.params.pipe === "function") {
					// YES, IS A STREAM
					return new Promise((resolve, reject) => {
						ctx.params.setEncoding("UTF8"); // Set the encoding to be utf8.
						let data = "";

						// Handle stream events --> data, end, and error
						ctx.params.on("data", (chunk) => {
							data += chunk;
						});

						ctx.params.on("end", () => {
							ctx.params = JSON.parse(data);
							resolve();
						});

						ctx.params.on("error", (err) => {
							reject(err);
						});
					})
						.then(() => {
							const check = this.broker.validator.compile({
								key: { type: "string" },
								name: { type: "string", optional: true },
								options: { type: "object", optional: true },
								data: { type: "object", optional: true },
								confirmation_path: { type: "string", optional: true },
								extra_info: { type: "object", optional: true },
							});
							const res = check(ctx.params);
							if (res === true) {
								return Promise.resolve();
							} else {
								return Promise.reject(new ValidationError("Entity validation error!", null, res));
							}
						})
						.then(() => {
							return ctx
								.call("reports.templates.find", {
									query: {
										key: ctx.params.key,
									},
								})
								.then((templates) => {
									if (templates.length < 1) {
										throw new EntityNotFoundError(this.name, ctx.params.key);
									}

									const template = templates[0];

									template.data = template.data !== null ? template.data : {};
									template.options = template.options !== null ? template.options : {};

									template.options = ctx.params.options
										? Object.assign(ctx.params.options, template.options)
										: template.options;
									template.data = ctx.params.data
										? Object.assign(template.data, ctx.params.data)
										: template.data;

									const reportData = new Stream.Readable();
									reportData.push(
										JSON.stringify({
											name: ctx.params.name ? ctx.params.name : template.name,
											template_id: template.id,
											options: template.options,
											data: template.data,
											confirmation_path: ctx.params.confirmation_path,
											extra_info: ctx.params.extra_info,
										}),
									);
									// no more data
									reportData.push(null);
									return ctx.call("reports.reports.createFromStream", reportData);
								});
						});
				} else {
					// IS NOT A STREAM SO CALL THE ORIGINAL ACTION
					return ctx.call("reports.templates.print", ctx.params);
				}
			},
		},
		print: {
			visibility: "public",
			params: {
				key: { type: "string" },
				name: { type: "string", optional: true },
				options: { type: "object", optional: true },
				data: { type: "object", optional: true },
				confirmation_path: { type: "string", optional: true },
				extra_info: { type: "object", optional: true },
			},
			handler(ctx) {
				return ctx
					.call("reports.templates.find", {
						query: {
							key: ctx.params.key,
						},
					})
					.then((templates) => {
						if (templates.length < 1) {
							throw new EntityNotFoundError(this.name, ctx.params.key);
						}

						const template = templates[0];

						template.data = template.data !== null ? template.data : {};
						template.options = template.options !== null ? template.options : {};

						template.options = ctx.params.options
							? Object.assign(ctx.params.options, template.options)
							: template.options;
						template.data = ctx.params.data
							? Object.assign(template.data, ctx.params.data)
							: template.data;

						return ctx.call("reports.reports.create", {
							name: ctx.params.name ? ctx.params.name : template.name,
							template_id: template.id,
							options: template.options,
							data: template.data,
							confirmation_path: ctx.params.confirmation_path,
							extra_info: ctx.params.extra_info,
						});
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
