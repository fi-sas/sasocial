# [1.1.0](https://gitlab.com/fi-sas/fisas-sasocial/compare/v1.0.1...v1.1.0) (2022-04-28)


### Features

* **gateway:** add regenerate gateway endpoints action ([54bb9ee](https://gitlab.com/fi-sas/fisas-sasocial/commit/54bb9eebb7ddf45df4563fa6ec71e8ddf8dddeca))

## [1.0.1](https://gitlab.com/fi-sas/fisas-sasocial/compare/v1.0.0...v1.0.1) (2022-04-01)


### Bug Fixes

* **backups:** checkServicePeriod not waiting result ([3df66f8](https://gitlab.com/fi-sas/fisas-sasocial/commit/3df66f8e36cabf320852512dc453ad01588a975b))

# 1.0.0 (2022-03-29)


### Bug Fixes

* **backups:** change crons functions ([5c4d02b](https://gitlab.com/fi-sas/fisas-sasocial/commit/5c4d02b1a787e796b4f621d3248fcb539d0e1dc6))
