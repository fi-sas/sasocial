FROM node:12-buster

ARG NODE_ENV

RUN apt-get update -y && apt-get install -y lsb-release

RUN  sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

RUN apt-get update -y && apt-get install -y postgresql-client-12

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm i

COPY . .

RUN chmod +x ./docker-entrypoint.sh

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]

CMD npm run start:$NODE_ENV
