"use strict";
const Cron = require("moleculer-cron");
const { Errors } = require("@fisas/ms_core").Helpers;
const { execute } = require("@getvim/execute");
const makeDir = require("make-dir");
const compress = require("gzipme");
const gunzip = require("gunzip-file");
const fs = require("fs");
const path = require("path");
const ms = require("ms");
const fastFolderSizeSync = require("fast-folder-size/sync");
const cakebase = require("./utils/cakebase");
/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "sasocial.backups",

	mixins: [Cron],

	crons: [
		{
			name: "backupHourly",
			cronTime: "* * * * *",
			onTick: function () {
				this.logger.info("Backup Hour Cron Job Started.");
				this.getLocalService("sasocial.backups")
					.actions.makeBackup({ period: "hourly" });
			},
			runOnInit: false,
		},
		{
			name: "backupDaily",
			cronTime: "0 0 * * *",
			onTick: function () {
				this.logger.info("Backup Daily Cron Job Daily.");
				this.getLocalService("sasocial.backups")
					.actions.makeBackup({ period: "daily" });
			},
			runOnInit: false,
		},
		{
			name: "backupWeekly",
			cronTime: "0 0 * * 0", // EVERY SUNDAY
			onTick: function () {
				this.logger.info("Backup Monthly Cron Job Weekly.");
				this.getLocalService("sasocial.backups")
					.actions.makeBackup({ period: "weekly" });
			},
			runOnInit: false,
		},
		{
			name: "backupMonthly",
			cronTime: "0 0 1 * *",
			onTick: function () {
				this.logger.info("Backup Monthly Cron Job Monthly.");
				this.getLocalService("sasocial.backups")
					.actions.makeBackup({ period: "monthly" });
			},
			runOnInit: false,
		},
		{
			name: "backupYearly",
			cronTime: "0 0 1 1 *",
			onTick: function () {
				this.logger.info("Backup Yearly Cron Job Yearly.");
				this.getLocalService("sasocial.backups")
					.actions.makeBackup({ period: "yearly" });
			},
			runOnInit: false,
		},
	],

	/**
	 * Settings
	 */
	settings: {
	},
	hooks: {

	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/*nodes: {
			rest: "GET /nodes",
			visibility: "published",
			handler(ctx) {
				return this.broker.call("$node.list");
			}
		},
		services: {
			rest: "GET /services",
			visibility: "published",
			async handler(ctx) {
				return this.broker.call("$node.services").then(res => {
					return res.map(r => r);
				});

			}
		},
		actions: {
			rest: "GET /actions",
			visibility: "published",
			handler(ctx) {
				return this.broker.call("$node.actions");
			}
		},
		metrics: {
			rest: "GET /metrics",
			visibility: "published",
			handler(ctx) {
				return this.broker.call("$node.metrics");
			}
		},
		health: {
			rest: "GET /health",
			visibility: "published",
			handler(ctx) {
				return this.broker.call("$node.health");
			}
		},*/
		getServices: {
			visibility: "published",
			rest: "GET /",
			scope: "sasocial:system:backups",
			params: {
			},
			async handler() {
				const services = await this.services_database.get(e => true);
				for (let s of services) {
					const backups = await this.backups_database.get(e => e.service === s.name);
					let temp = [];
					temp = backups.filter(s => s.period === "hourly");
					s.backups_hourly = temp.slice(Math.max(temp.length - 5, 0));
					temp = backups.filter(s => s.period === "daily");
					s.backups_daily = temp.slice(Math.max(temp.length - 5, 0));
					temp = backups.filter(s => s.period === "weekly");
					s.backups_weekly = temp.slice(Math.max(temp.length - 5, 0));
					temp = backups.filter(s => s.period === "monthly");
					s.backups_monthly = temp.slice(Math.max(temp.length - 5, 0));
					temp = backups.filter(s => s.period === "yearly");
					s.backups_yearly = temp.slice(Math.max(temp.length - 5, 0));
					temp = backups.filter(s => s.period === "others");
					s.backups_others = temp.slice(Math.max(temp.length - 5, 0));
				}

				services.unshift({
					name: "general",
					total_folder_size: fastFolderSizeSync(process.env.ROOT_DIR),
					total_services: services.length || 0,
				});

				return services;
			}
		},
		updatePeriodsService: {
			rest: "PUT /:name",
			visibility: "published",
			scope: "sasocial:system:backups",
			params: {
				name: { type: "string", },
				hourly: { type: "boolean", default: false },
				daily: { type: "boolean", default: true },
				weekly: { type: "boolean", default: false },
				monthly: { type: "boolean", default: false },
				yearly: { type: "boolean", default: false },
			},
			async handler(ctx) {
				await this.services_database.update(e => e.name === ctx.params.name, {
					hourly: ctx.params.hourly,
					daily: ctx.params.daily,
					weekly: ctx.params.weekly,
					monthly: ctx.params.monthly,
					yearly: ctx.params.yearly,
				});
				return this.getServiceToConfigs(ctx.params.name);
			}
		},
		makeBackup: {
			rest: "POST /all",
			visibility: "published",
			scope: "sasocial:system:backups",
			params: {
				period: { type: "enum", values: ["hourly", "daily", "weekly", "monthly", "yearly", "other"], default: "others" }
			},
			handler(ctx) {
				this.logger.info(`Backup for period ${ctx.params.period} started...`);
				this.getSasocialServices().then(async services => {
					this.logger.info(services.length + " services founded!");
					this.logger.info(services);

					for (let service of services) {
						const checkService = await this.checkServicePeriod(service, ctx.params.period);

						if (service.settings.backup_info.SERVICE_NAME &&
							checkService) {
							await this.updateServiceConfigStatus(service.settings.backup_info.SERVICE_NAME, "WAITING");
						}
					}

					for (let service of services) {
						this.logger.info(`Processing ${service.name}...`);
						await this.addServiceToConfigs(service);

						const checkService = await this.checkServicePeriod(service, ctx.params.period);
						if (service.settings.backup_info.SERVICE_NAME && checkService) {

							const service_path = this.getServicePath(service.settings.backup_info.SERVICE_NAME);
							try {
								await this.checkFolderStructureService(service.settings.backup_info.SERVICE_NAME);
							} catch (ex) {
								this.logger.error("Erro while creating folder structure!");
								this.logger.error(ex);
							}
							this.logger.info("Folder...");
							const backup_data = await this.takePGbackup(
								service.settings.backup_info.DB_USER,
								service.settings.backup_info.DB_PASS,
								service.settings.backup_info.DATABASE,
								service.settings.backup_info.DB_HOST,
								5432,//service.settings.backup_info.DB_PORT,
								service_path,
								ctx.params.period,
								service.settings.backup_info.SERVICE_NAME
							);
							this.logger.info("Add backup...");
							await this.addBackupToService(service, backup_data);

							this.logger.info(`Starting cleanup of ${service.settings.backup_info.SERVICE_NAME}...`);
							await this.deleteOldBackups(service, ctx.params.period);
						}

					}
				});
			}
		},
		restore: {
			rest: "POST /restore",
			visibility: "published",
			scope: "sasocial:system:backups",
			params: {
				password: { type: "string" },
				service_name: { type: "string" },
				period: { type: "string" },
				filename: { type: "string" },
			},
			async handler(ctx) {
				const valid = await ctx.call("authorization.users.checkPassword", {
					user_id: ctx.meta.user.id,
					password: ctx.params.password,
				});

				if (!valid) {
					throw new Errors.ValidationError("INVALID_PASSWORD", "The password is not correct", {});
				}

				let configs = this.loadServicesInfo();
				const service_config = configs.services.find(s => s.name === ctx.params.service_name);
				if (service_config) {
					const services_sasocial = await this.getSasocialServices();
					const service_sasocial = services_sasocial.find(s => s.name === service_config.path);

					if (service_sasocial) {
						const service_path = this.getServicePath(service_sasocial.settings.backup_info.SERVICE_NAME);
						await this.restorePGbackup(
							service_sasocial.settings.backup_info.DB_USER,
							service_sasocial.settings.backup_info.DB_PASS,
							service_sasocial.settings.backup_info.DATABASE,
							service_sasocial.settings.backup_info.DB_HOST,
							5432,//service.settings.backup_info.DB_PORT,
							service_path,
							ctx.params.period,
							ctx.params.filename
						);
					}
				}

				return true;
			}
		}
	},

	/**
	 * Events
	 */
	events: {
		"$services.changed"(ctx) {
			this.logger.info("$services.changed");
		},
	},
	/**
	 * Methods
	 */
	methods: {
		getServicePath(service_name) {
			const ROOT_DIR = process.env.ROOT_DIR;
			return ROOT_DIR.concat("/".concat(service_name));
		},
		async checkFolderStructureService(service_name) {
			const ROOT_DIR = process.env.ROOT_DIR;
			const SERVICE_DIR = this.getServicePath(service_name);

			makeDir(ROOT_DIR);
			makeDir(SERVICE_DIR);
			makeDir(SERVICE_DIR.concat("/hourly"));
			makeDir(SERVICE_DIR.concat("/daily"));
			makeDir(SERVICE_DIR.concat("/weekly"));
			makeDir(SERVICE_DIR.concat("/monthly"));
			makeDir(SERVICE_DIR.concat("/yearly"));
			makeDir(SERVICE_DIR.concat("/others"));
			return Promise.resolve(true);
		},
		async takePGbackup(username, password, database, dbHost, dbPort, path, period, service_name) {

			if (!["hourly", "daily", "weekly", "monthly", "yearly"].includes(period)) {
				period = "others";
			}

			// defining backup file name
			const date = new Date();
			const today = `${date.toISOString()}`;
			const backupFile = `${path}/${period}/pg-backup-${today}.tar`;

			this.logger.info("Start pg dump....");
			await this.updateServiceConfigStatus(service_name, "PROCESSING");
			return new Promise((resolve, reject) => {
				execute(`pg_dump --dbname=postgresql://${username}:${password}@${dbHost}:${dbPort}/${database} -f ${backupFile} -F t `)
					.then(async () => {
						// add these lines to compress the backup file
						await compress(backupFile, {});
						fs.unlinkSync(backupFile);
						this.logger.info("Zipped backup created");

						await this.updateServiceConfigStatus(service_name, "DONE");
						resolve({ period, date: new Date(), path: `pg-backup-${today}.tar.gz`, deleted: false });
					})
					.catch((err) => {
						this.logger.error("POSTGREST Database backup error:");
						this.logger.error(err);
						fs.unlinkSync(backupFile);
						this.updateServiceConfigStatus(service_name, "ERROR");
						reject(err);
					});

			});
		},
		async restorePGbackup(username, password, database, dbHost, dbPort, path, period, filename) {

			if (!["hourly", "daily", "weekly", "monthly", "yearly"].includes(period)) {
				period = "others";
			}

			// defining backup file name
			const backupFile = `${path}/${period}/${filename}`;
			const unzipBackupFile = backupFile.replace(".gz", "");

			return new Promise((resolve, reject) => {
				gunzip(backupFile, unzipBackupFile, async () => {
					execute(`pg_restore --dbname=postgresql://${username}:${password}@${dbHost}:${dbPort}/${database} -c -v ${unzipBackupFile}`)
						.then(async (res) => {
							this.logger.info("Command:");
							this.logger.info(`pg_restore --dbname=postgresql://${username}:${password}@${dbHost}:${dbPort}/${database} -c -v ${unzipBackupFile}`);
							this.logger.info("Command result:");
							this.logger.info(res);

							fs.unlinkSync(unzipBackupFile);
							this.logger.info("Unzipped backup deleted");

							resolve(true);
						})
						.catch((err) => {
							reject(err);
							this.logger.error("POSTGREST Database restore error:");
							this.logger.error(err);
							fs.unlinkSync(unzipBackupFile);
						});
				});

			});
		},
		async getSasocialServices() {
			return this.broker.call("$node.services").then(services => {
				return services.map(s => ({ name: s.name, settings: s.settings })).filter(s => s.name.includes(".sasocial"));
			});
		},
		async deleteOldBackups(service, period) {
			const founded = await this.getServiceToConfigs(service.settings.backup_info.SERVICE_NAME);

			if (!founded) {
				await this.addServiceToConfigs(service);
				return this.deleteOldBackups(service, period);
			}

			const temp = {};
			temp[`last_${period}_cleanup`] = new Date();
			await this.services_database.update(s => s.name === service.settings.backup_info.SERVICE_NAME,
				{ ...temp });

			const deleteAfter = {
				"hourly": ms("1d"),
				"daily": ms("2w"),
				"weekly": ms("3w"),
				"monthly": ms("16w"),
				"yearly": ms("1y"),
				"others": ms("3y"),
			};

			const backups_to_keep = await this.backups_database.get(b =>
				b.service === service.settings.backup_info.SERVICE_NAME &&
				b.period === period &&
				b.deleted === false &&
				new Date(b.date).getTime() > Date.now() - deleteAfter[period]
			);
			const backups_to_keep_files = backups_to_keep.map(btk => btk.path);

			const service_path = this.getServicePath(service.settings.backup_info.SERVICE_NAME);
			const ROOT_DIR = service_path.concat("/").concat(period);
			fs.readdir(ROOT_DIR, (err, files) => {
				if (err) {
					this.logger.error("Folder to cleanup not found:");
					this.logger.error(err);
				} else {
					files.forEach(file => {
						const fileDir = path.join(ROOT_DIR, file);

						if (!backups_to_keep_files.includes(file)) {
							fs.unlinkSync(fileDir);

							this.logger.info(`The file ${file} has been deleted!`);
							this.backups_database.get(b =>
								b.service === service.settings.backup_info.SERVICE_NAME &&
								b.path === file, {
								deleted: true,
								deleted_at: new Date(),
							});

						}
					});
				}

			});

		},
		async getServiceToConfigs(service_name) {
			const founded = await this.services_database.get(s => s.name === service_name);

			if (founded.length === 0) {
				return false;
			}

			return founded[0];
		},
		async updateServiceConfigStatus(service_name, status) {
			return this.services_database.update(s => s.name === service_name,
				{ status: status });
		},
		async addServiceToConfigs(service) {
			const new_config = {
				status: "IDLE",
				hourly: false,
				daily: true,
				weekly: false,
				monthly: false,
				yearly: false,
				last_backup: null,
				last_hourly_backup: null,
				last_daily_backup: null,
				last_weekly_backup: null,
				last_monthly_backup: null,
				last_yearly_backup: null,
				last_hourly_cleanup: null,
				last_daily_cleanup: null,
				last_weekly_cleanup: null,
				last_monthly_cleanup: null,
				last_yearly_cleanup: null,
				backups: [],
			};
			const founded = await this.getServiceToConfigs(service.settings.backup_info.SERVICE_NAME);

			if (founded) {
				await this.services_database.update(s => s.name === service.settings.backup_info.SERVICE_NAME,
					{ path: service.name });
			}

			if (!founded) {
				await this.services_database.set({ path: service.name, name: service.settings.backup_info.SERVICE_NAME, ...new_config });
			}

			return true;
		},
		async checkServicePeriod(service, period) {
			const founded = await this.getServiceToConfigs(service.settings.backup_info.SERVICE_NAME);

			if (!founded) {
				await this.addServiceToConfigs(service);
				return this.checkServicePeriod(service, period);
			}

			if (period === "others") {
				return true;
			}

			return !!founded[period];
		},
		async addBackupToService(service, backup_data) {
			const founded = this.getServiceToConfigs(service.settings.backup_info.SERVICE_NAME);
			//const founded = configs.services.find(s => s.name === service.settings.backup_info.SERVICE_NAME);

			if (!founded) {
				await this.addServiceToConfigs(service);
				return this.addBackupToService(service, backup_data);
			}

			await this.services_database.update(s => s.name === service.settings.backup_info.SERVICE_NAME,
				{ last_backup: new Date() });

			if (backup_data.period) {
				const temp = {};
				temp[`last_${backup_data.period}_backup`] = new Date();
				await this.services_database.update(s => s.name === service.settings.backup_info.SERVICE_NAME,
					{ ...temp });
			}

			this.backups_database.set({ ...backup_data, service: service.settings.backup_info.SERVICE_NAME });

			return true;
		}

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		try {
			this.logger.info("Database Starting...");
			this.services_database = cakebase(process.env.ROOT_DIR + "/services.json");
			this.backups_database = cakebase(process.env.ROOT_DIR + "/backups.json");
		} catch (ex) {
			this.logger.error("Error starting database:");
			this.logger.error(ex);
		}
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
