"use strict";
const Cron = require("moleculer-cron");

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "sasocial.cache",

	mixins: [Cron],

	crons: [
	],

	/**
	 * Settings
	 */
	settings: {
		services: [],
	},
	hooks: {

	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		cacheInfo: {
			rest: "GET /info",
			visibility: "published",
			scope: "sasocial:system:cache",
			handler() {
				return this.broker.cacher.client.serverInfo;
			}
		},
		searchKeys: {
			rest: "GET /",
			visibility: "published",
			scope: "sasocial:system:cache",
			params: {
				search: { type: "string" },
			},
			handler(ctx) {

				return new Promise((resolve) => {

					const fullkeys = [];

					const stream = this.broker.cacher.client.scanStream({
						match: ctx.params.search
					});
					stream.on("data", (keys) => {
						fullkeys.push(keys);
					});
					stream.on("end", () => {
						resolve(fullkeys);
					});
				});
			}
		},
		deleteAllKeys: {
			rest: "DELETE /delete_all",
			visibility: "published",
			scope: "sasocial:system:cache",
			params: {
			},
			handler(ctx) {
				return new Promise((resolve) => {

					const stream = this.broker.cacher.client.scanStream({
						match: "*"
					});
					stream.on("data", (keys) => {
						if (keys.length) {
							const pipeline = this.broker.cacher.client.pipeline();
							keys.forEach((key) => {
								pipeline.del(key);
							});
							pipeline.exec();
						}
					});
					stream.on("end", () => {
						resolve(true);
					});
				});
			}
		},
		deleteKeys: {
			rest: "DELETE /delete",
			visibility: "published",
			scope: "sasocial:system:cache",
			params: {
				search: { type: "string", optional: true },
				keys: { type: "array", items: "string", optional: true },
			},
			handler(ctx) {
				if (ctx.params.keys) {
					const pipeline = this.broker.cacher.client.pipeline();
					ctx.params.keys.forEach((key) => {
						pipeline.del(key);
					});
					pipeline.exec();
					return true;
				}

				return new Promise((resolve) => {

					const stream = this.broker.cacher.client.scanStream({
						match: ctx.params.search
					});
					stream.on("data", (keys) => {
						if (keys.length) {
							const pipeline = this.broker.cacher.client.pipeline();
							keys.forEach((key) => {
								pipeline.del(key);
							});
							pipeline.exec();
						}
					});
					stream.on("end", () => {
						resolve(true);
					});
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {
	},
	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
