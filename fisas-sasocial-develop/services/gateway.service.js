"use strict";
const Cron = require("moleculer-cron");

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "sasocial.gateway",

	mixins: [Cron],

	crons: [
	],

	/**
	 * Settings
	 */
	settings: {
		services: [],
	},
	hooks: {

	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		regenerateAllAutoAliases: {
			visibility: "published",
			rest: "POST /regenerate",
			handler(ctx) {
				return ctx.call("api.regenerateAllAutoAliases");
			}
		}
	},

	/**
	 * Events
	 */
	events: {
	},
	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
