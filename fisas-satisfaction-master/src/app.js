import express from 'express';
import path from 'path';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import log4js from 'log4js';

import setValidators from './custom_validations';
import apiComposerValidations from './middleware/apiComposerValidations';
import responseEnhancer from './middleware/response-formatter';
import requestParameters from './middleware/request-parameters';
import { errorHandler, logErrors } from './middleware/error-handlers';
import tokenInfo from './middleware/tokenInfo';
import internalRequest from './middleware/internal-request';
import logger from './logger';

import indexRouter from './routes/index';
import { NotFoundError } from './errors';

// Swagger Specs
import linkSpecs from './config/components/schemas.link.spec.json';
import parametersSpecs from './config/components/parameters.spec.json';
import responsesSpecs from './config/components/responses.spec.json';
import securitySchemesSpecs from './config/components/securitySchemes.bearerAuth.json';
import securitySpecs from './config/security.json';


//    __________  ____  _   __       ______  ____ _____
//   / ____/ __ \/ __ \/ | / /      / / __ \/ __ ) ___/
//  / /   / /_/ / / / /  |/ /  __  / / / / / __  \__ \
// / /___/ _, _/ /_/ / /|  /  / /_/ / /_/ / /_/ /__/ /
// \____/_/ |_|\____/_/ |_/   \____/\____/_____/____/

// Uncomment to use cron jobs

// import cron from 'node-cron';

// cron.schedule('* * * * *', () => {
//   console.log('running a task every minute');
// });


//     __  ________    ____  ____  ____  _____________________  ___    ____
//    /  |/  / ___/   / __ )/ __ \/ __ \/_  __/ ___/_  __/ __ \/   |  / __ \
//   / /|_/ /\__ \   / __  / / / / / / / / /  \__ \ / / / /_/ / /| | / /_/ /
//  / /  / /___/ /  / /_/ / /_/ / /_/ / / /  ___/ // / / _, _/ ___ |/ ____/
// /_/  /_//____/  /_____/\____/\____/ /_/  /____//_/ /_/ |_/_/  |_/_/
setValidators();

/**
 * Routes
 */
const app = express();

// Use logged middleware to log all requests
app.use(log4js.connectLogger(logger, { level: 'auto' }));

// Set default render engine
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

// Add CORS headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Language-ID, X-Language-Acronym');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD');
  next();
});

// Helmet helps you secure your Express apps by setting various HTTP headers.
// It's not a silver bullet, but it can help!
// https://github.com/helmetjs/helmet
app.use(helmet());

/**
 * Use response enhancer (formatter)
 * Use request parameters to access ordering, filters and pagination configurations
 */
app.use(responseEnhancer());
app.use(requestParameters());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: '5mb' }));
app.use(express.static(path.join(__dirname, '..', 'public')));

/**
 * Use internal request detection
 */
app.use(internalRequest);

// API composer validations -- used for validating resources from other MS
app.use(apiComposerValidations);
app.use(tokenInfo);

/*
 * Assign Routes to Application
 */
app.use('/', indexRouter);

// Usage of auth middleware:
// Up top: import requireAuth from './middleware/auth'
//
// app.use('/users', requireAuth, usersRouter);
//
// Inside your routes, you will have available in req.authInfo the information
// decoded from the token


/*
 * Swagger documentation route
 */
const swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'FI@SAS API Microservice',
      version: '0.0.0',
      description: 'Microservice API for the FI@SAS project.',
    },
    components: {
      schemas: {
        Link: linkSpecs,
      },
      parameters: parametersSpecs,
      responses: responsesSpecs,
      securitySchemes: securitySchemesSpecs,
    },
    security: securitySpecs,
  },
  apis: ['./routes/*'],
});

delete swaggerSpec.swagger;
swaggerSpec.openapi = '3.0.n';

swaggerSpec.components.schemas = Object.assign(swaggerSpec.components.schemas, require('./config/components/schemas.errors.spec.json'));

swaggerSpec.components.responses = require('./config/components/responses.spec.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// catch 404 and forward to error handler
app.use(() => {
  throw new NotFoundError();
});

app.use(logErrors);
app.use(errorHandler);

export default app;
