/**
 * validateExistence bookshelf plugin
 *
 * Extends bookshelf Model with validateExistence method.
 * validateExistence receives id as parameter and a model name.
 * If existence is confirmed, does nothing. Otheriwse, throws a NotFoundError with
 * received model name in error message
 */
/* eslint-disable no-param-reassign, func-names */

import { NotFoundError } from '../errors';

module.exports = function (bookshelf) {
  /*
   * Create a new Model to replace `bookshelf.Model`
   */
  const Model = bookshelf.Model.extend({}, {
    async validateExistence(id, model = 'Model') {
      const c = await this.where({ id }).count();
      if (c === 0) {
        throw new NotFoundError(`${model} not found`);
      }
    },
  });

  /*
   * Replace `bookshelf.Model`
   */
  bookshelf.Model = Model;
};
