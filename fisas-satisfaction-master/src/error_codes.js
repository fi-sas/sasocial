export const AuthenticationMissingToken = 900;
export const AuthenticationInvalidToken = 901;
export const AuthenticationLackOfPermissions = 904;

export const ValidationRequiredValue = 100;
export const ValidationInvalidFormat = 101;
export const ValidationDuplicateUniqueValue = 102;
export const ValidationMissingRelatedEntity = 103;
export const InvalidFilterParameter = 110;
export const InvalidSortParameter = 111;
export const InvalidWithRelatedParameter = 112;
export const InvalidLanguageId = 113;
export const InvalidGroupId = 114;
export const InvalidFileId = 115;
export const InvalidUserId = 116;
export const InvalidServiceId = 117;
export const InvalidPaymentMethodId = 118;

export const InvalidDeleteDueToRelatedEntityExistence = 300;

export const ResourceNotFound = 404;
