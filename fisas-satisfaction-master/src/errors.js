import { ResourceNotFound } from './error_codes';
import { hashCode } from './helpers/hashCode';

export class SimpleError extends Error {
  /**
   * constructor for generic error
   * @param {Object} error Code and message of error
   */
  constructor(err = {}) {
    super(err.message || 'Server Error');
    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);
    this.code = err.code || 500;
    this.status = 500;
  }
}

export class ValidationError extends Error {
  /**
   * constructor for validation error
   * @param {Array} errors Array of {code, message, index, field} objects
   */
  constructor(errors = []) {
    super('Validation error');
    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);

    this.errors = errors;
    this.status = 400;
  }
}

export class ParameterError extends Error {
  /**
   * constructor for parameter error
   * @param {Array} errors Array of {code, message, parameter} objects
   */
  constructor(errors = []) {
    super('Parameter error');
    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);

    this.errors = errors;
    this.status = 400;
  }
}

export class NotFoundError extends Error {
  /**
   * constructor for not found error
   * @param {Object} error Code and message of error
   */
  constructor(message = 'Not Found') {
    super(message);
    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);
    this.code = ResourceNotFound;
    this.status = 404;
  }
}

export class JoiValidationError extends ValidationError {
  constructor(err, index = 0) {
    super([{
      code: hashCode(err.details[0].message),
      message: err.details[0].message,
      field: err.details[0].path[0],
      index,
    }]);
  }
}
