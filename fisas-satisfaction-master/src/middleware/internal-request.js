/**
 * Express middleware for detection of internal requests (from fiasas netwrok).
 * Usage: const isInternal = req.internalRequest // true/false
 * @public
 */
export default function internalRequest(req, res, next) {
  req.internalRequest = (req.get('Host').endsWith(':3000'));
  return next();
}
