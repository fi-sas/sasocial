/**
 * Request middleware to fetch filters, ordering, pagination and withRelated parameters
 * according to stablished rules.
 * Usage:
 *  req.parameters.ordering // returns [ { field, order:'ASC'|'DESC' } ]
 *  req.parameters.filters // returns [ { field, value } ]
 *  req.parameters.pagination // returns { limit, offset } or null
 *  req.parameters.withRelated // returns ['entities'] or null
 */
import { ParameterError } from '../errors';
import { InvalidFilterParameter, InvalidSortParameter, InvalidWithRelatedParameter } from '../error_codes';


/**
 * Function to fetch filters from request parameters.
 * @param {Object} res An express response object.
 * @returns {Array} filters An array of objects with field and value property
 * @private
 */
const fetchFiltersParameters = (req) => {
  const queryData = { ...req.query } || {};
  const filters = [];

  ['sort', 'limit', 'offset', 'token', 'withRelated'].forEach((keyword) => {
    delete queryData[keyword];
  });

  Object.keys(queryData).forEach((field) => {
    const value = queryData[field];
    if (value) {
      filters.push({ field, value });
    }
  });

  return filters;
};

/**
 * Function to fetch ordering from request parameters.
 * @param {Object} res An express response object.
 * @returns {Array} ordering An array of objects with field property
 * and order property ('ASC' or 'DESC')
 * @private
 */
const fetchOrderingParameters = (req) => {
  let ordering = [];
  if (req.query.sort) {
    ordering = req.query.sort.split(',').map((piece) => {
      let field = piece; let
        order = 'ASC';
      if (piece.indexOf('-') === 0) {
        field = field.substr(1);
        order = 'DESC';
      }
      return { field, order };
    });
  }
  return ordering;
};

/**
 * Function to fetch pagination data from request parameters. Pagination data includes
 * limit and offset parameters and should both be numeric values. If some pagination value
 * is not received in request, default values are applied. If specific value of limit=-1 is
 * received, then no pagination is to be applied and null is returned.
 *
 * @param {Object} res An express response object.
 * @returns {Object*} res Pagination object with limit and offset properties
 * (default values 10 and 0) or null (if limit=-1)
 * @private
 */
const fetchPaginationParameters = (req) => {
  const queryLimit = parseInt(req.query.limit, 10);
  const queryOffset = parseInt(req.query.offset, 10);

  if (queryLimit === -1) {
    return null;
  }

  const offset = queryOffset > 0 ? queryOffset : 0;
  const limit = queryLimit > 0 ? queryLimit : 10;

  return { offset, limit };
};

/**
 * Function to fetch withRelated parameter.
 * If no parameter is received, returns null. If explicit value of 'false' is received,
 * an empty array is returned. Otherwise, splits the comma-separated parameter value.
 *
 * @param {Object} res An express response object.
 * @returns {Array*} Array of strings from withRelated query parameter - empty array if 'false'
 * and null if no parameter is received.
 * @private
 */
const fetchWithRelatedParameters = (req) => {
  if (req.query.withRelated === 'false') {
    return [];
  }
  return req.query.withRelated ? req.query.withRelated.split(',') : null;
};

/**
 * Function to parse filters of the received field from boolean to integer value.
 * Returns the replaced filters array.
 *
 * @param {Array} filters Array of filters.
 * @param {String} field Field name.
 * @returns {Array} Array of filters with parsed values for field filters
 * @private
 */
const parseBooleanFilters = (filters, field) => {
  filters.forEach((filter) => {
    const parsedFilter = filter;
    if (filter.field === field) {
      parsedFilter.value = (filter.value === 'true') ? 1 : 0;
    }
    return parsedFilter;
  });

  return filters;
};

/**
 * Function to parse filters according to the parser received.
 *
 * @param {Array} filters Array of filters.
 * @param {Object} parser Object with each field to be parsed as key and its
 * parser function as value
 * @returns {Array} Array of filters with parsed values
 * @private
 */
const parseFilters = (filters, parser) => {
  filters.forEach((filter) => {
    const parsedFilter = filter;
    if (parser[filter.field]) {
      const fn = parser[filter.field];
      parsedFilter.value = fn(filter.value);
    }
    return parsedFilter;
  });

  return filters;
};

/**
 * Function to validate the parameters according to allowed filter and ordering fields.
 * If any invalid parameter is found, a ParameterError is thrown with list of found errors
 * with message, parameter and code (InvalidFilterParameter/InvalidSortParameter).
 * If one of the filter or ordering fields parameters is not received, no validation for
 * filter or ordering is performed.
 *
 * @param {Object} parameters Parameters to validate.
 * @param {Array*} filterFields Array of allowed fields in filters parameters.
 * @param {Array*} orderingFields Array of allowed fields in ordering parameters.
 * @param {Array*} withRelatedValues Array of allowed relation names to be in withRelated parameter.
 * @private
 */
const validate = (parameters, filterFields, orderingFields, withRelatedValues) => {
  let invalidFilterFields = [];
  let invalidOrderingFields = [];
  let invalidWithRelatedRelations = [];

  if (filterFields) {
    invalidFilterFields = parameters.filters.filter(f => !filterFields.includes(f.field));
  }

  if (orderingFields) {
    invalidOrderingFields = parameters.ordering.filter(f => !orderingFields.includes(f.field));
  }

  if (parameters.withRelated && withRelatedValues) {
    invalidWithRelatedRelations = parameters.withRelated.filter(
      r => !withRelatedValues.includes(r),
    );
  }

  if (invalidFilterFields.length || invalidOrderingFields.length
    || invalidWithRelatedRelations.length) {
    const errors = [];
    invalidFilterFields.forEach((f) => {
      const { field } = f;
      errors.push({ code: InvalidFilterParameter, message: `${field} is not a valid filter parameter`, parameter: field });
    });

    invalidOrderingFields.forEach((f) => {
      const { field } = f;
      errors.push({ code: InvalidSortParameter, message: `${field} is not a valid sort parameter`, parameter: field });
    });

    invalidWithRelatedRelations.forEach((r) => {
      errors.push({ code: InvalidWithRelatedParameter, message: `${r} is not a valid withRelated parameter`, parameter: r });
    });

    throw new ParameterError(errors);
  }
};

/**
 * Function to fetch filters from request parameters.
 * @param {Object} res An express response object.
 * @returns {Object} parameters An object with properties:
 *  - ordering: array [ { field, order:'ASC'|'DESC' } ]
 *  - filters: array [ { field, [ value ] } ]
 *  - pagination: object with { limit, offset } or null if limit parameter equals -1
 *  - withRelated: null or array of strings sent in query parameter 'withRelated' separated by
 * comma. If 'withRelated' parameter is received with 'false' value, an empty array is set. If
 * no value is received, null is used.
 *  - validate: function to validate current request parameters using received filter and
 *  ordering fields and withRelated possible values. If not valid, a ParameterError is thrown.
 *  - parseBooleanFilters: function to parse received filters of the received field
 * from boolean to numeric values
 *  - parseFilters: function to parse and update the request filters according to the
 * parser object received (from field to function to be applied)
 * @private
 */
const fetchParameters = (req) => {
  const parameters = {};

  // Set ordering array property
  parameters.ordering = fetchOrderingParameters(req);

  // Set filters array property
  parameters.filters = fetchFiltersParameters(req);

  // Set pagination object property
  parameters.pagination = fetchPaginationParameters(req);

  // Set withRelated array property
  parameters.withRelated = fetchWithRelatedParameters(req);

  // Set parseBooleanFilters function property
  parameters.parseBooleanFilters = ((filters, field) => parseBooleanFilters(filters, field));

  // Set validate function property
  parameters.validate = (
    (filterFields = null, orderingFields = null, withRelatedValues = null) => validate(
      parameters,
      filterFields,
      orderingFields,
      withRelatedValues,
    )
  );

  // Set parseFilters function property
  parameters.parseFilters = (
    (parser) => {
      parameters.filters = parseFilters(parameters.filters, parser);
      return true;
    }
  );

  parameters.addFilter = (field, value) => {
    parameters.filters = parameters.filters.filter(f => f.field !== field);
    parameters.filters.push({ field, value });
  };

  parameters.getFilterValue = (field) => {
    let filterValue = null;
    parameters.filters.forEach((f) => {
      if (f.field === field) {
        filterValue = f.value;
      }
    });
    return filterValue;
  };

  return parameters;
};

/**
 * Express middleware for fetch request parameters.
 * @returns {Function} An express middleware for enhance an express response object.
 * @public
 */
const requestParameters = () => (req, res, next) => {
  req.parameters = fetchParameters(req);
  next();
};

export default requestParameters;
