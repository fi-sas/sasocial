/**
 * Response enhancer middleware to apply stablished format.
 * Usage:
 *  res.formatter.ok( { id: 1, name: 'test' } );
 *    // or res.formatter.ok( [{ id: 1, name: 'test' }]);
 *  res.formatter.notFound( { code: 101, message: 'Error' });
 *    // or res.formatter.notFound( [{ code: 101, message: 'Error' }] );
 *
 * From https://github.com/aofleejay/express-response-formatter
 */

import methods from './response-methods';


/**
 * Function to generate a success response.
 * @param {Object} response Response information.
 * @param {*} response.data Data field (object or array of objects)
 * @param {Object=} response.link Link field
 * @returns {Object} Formatted response.
 * @private
 */
const generateSuccessResponse = ({ data = [], link }) => {
  let preparedData = data;
  if (!(preparedData instanceof Array)) {
    preparedData = [preparedData];
  }
  const preparedLink = link;
  preparedLink.total = preparedLink.total || preparedData.length;
  return {
    status: 'success',
    link: preparedLink,
    data: preparedData,
    errors: [],
  };
};


/**
 * Function to generate an error response.
 * @param {Object|Array} errors Array or single error object.
 * @returns {Object} Formatted response.
 * @private
 */
const generateErrorResponse = (errors = []) => {
  let preparedErrors = errors;
  if (!(preparedErrors instanceof Array)) {
    preparedErrors = [preparedErrors];
  }
  return {
    status: 'error',
    link: {},
    data: [],
    errors: preparedErrors,
  };
};


/**
 * Function to generate formatter object.
 * @param {Object} res An express response object.
 * @returns {Object} Formatter object that contain response formatter functions.
 * @private
 */
const generateFormatters = (res) => {
  const formatter = {};
  let responseBody = {};

  methods.forEach((method) => {
    if (method.isSuccess) {
      if (parseInt(method.code, 10) !== 204) {
        formatter[method.name] = (data, link = {}) => {
          responseBody = generateSuccessResponse({ data, link });
          res.status(method.code).json(responseBody);
        };
      } else {
        // No content exception
        formatter[method.name] = () => {
          res.status(204).send();
        };
      }
    } else {
      formatter[method.name] = (errors = []) => {
        responseBody = generateErrorResponse(errors);
        res.status(method.code).json(responseBody);
      };
    }
  });

  return formatter;
};

/**
 * Express middleware for enhance response with stuff of response formatter.
 * @returns {Function} An express middleware for enhance an express response object.
 * @public
 */
const responseEnhancer = () => (req, res, next) => {
  res.formatter = generateFormatters(res);
  next();
};

export default responseEnhancer;
