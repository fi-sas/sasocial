export default [
  {
    // Regex to match the endpoints that should be validated by this rule
    // resource: /api\/v1\/(categories|posts)(\/[^/]*)?(\?.*)?$/m,

    // Use path to indicate where the ids to validate are located.
    // Example:
    // path: 'language_id',
    // path: 'translations.[language_id]'
    // path: 'template.translations.[file_id]',

    // Deprecated but supported: Three possible cases:
    // arrayPath: 'translations',
    // indexPath: 'language_id',
    // Finds the ids in the 'language_id' index of the 'translations' array of the request body

    // arrayPath: 'translations',
    // indexPath: null,
    // Finds the ids as the elements of the 'translations' array of the request body

    // singlePath: 'language_id',
    // Finds the id in the 'language_id' index of the request body

    // Target MS where the ids found should be validated
    // targetMS: process.env.MS_CONFIG_ENDPOINT,

    // Target endpoint to validate the endpoints found
    // targetResource: 'languages',

    // Array of endpoints that should be validated
    // endpoints: ['POST', 'PUT', 'PATCH'],

    // The error code returned in case of an invalid id found
    // code: Codes.InvalidLanguageId,

    // The error message returned in case of an invalid id found
    // message: 'The language id provided is not valid.',
  }
];
