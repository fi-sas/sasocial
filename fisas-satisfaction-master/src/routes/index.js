/**
 * @swagger
 *
 * components:
 *  schemas:
 *   Thing:
 *    - type: object
 *      properties:
 *        id:
 *          type: integer
 *          nullable: false
 *          description: The Thing ID.
 *        message:
 *          type: string
 *          nullable: false
 *          description: The Thing message.
 */
import { Router } from 'express';
import { getPaginationMetaData } from '../helpers/metadata';
import requireAuth from '../middleware/auth';
import requirePermission from '../middleware/scopePermissions';

const indexRouter = new Router();

/**
 * @swagger
 * /:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns a list of Things
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/components/parameters/perPageParemeter'
 *       - $ref: '#/components/parameters/pageParameter'
 *       - $ref: '#/components/parameters/sortByParameter'
 *       - name: messageFilter
 *         description: Additional parameter to filter message content
 *         in: query
 *         schema:
 *          type: string
 *     responses:
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Thing'
 */
indexRouter.get('/', (req, res) => {
  const things = [
    {
      id: 1,
      message: 'Hello World!',
    },
  ];
  const link = getPaginationMetaData(req.parameters.pagination, req);
  res.formatter.ok(things, link);
});

/**
 * @swagger
 * /created:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 201 Created response
 *     produces:
 *       - application/json
 *     responses:
 *       201:
 *         description: An empty created success response
 */
indexRouter.get('/created', (req, res) => {
  res.formatter.created();
});

/**
 * @swagger
 * /no-content:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 204 No Content response
 *     produces:
 *     responses:
 *       204:
 *         description: An empty no content success response
 */
indexRouter.get('/no-content', (req, res) => {
  res.formatter.noContent();
});

/**
 * @swagger
 * /bad:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 400 Bad Request response
 *     produces:
 *       - application/json
 *     responses:
 *       '400':
 *          $ref: '#/components/responses/BadRequest'
 */
indexRouter.get('/bad', (req, res) => {
  res.formatter.badRequest();
});

/**
 * @swagger
 * /unauthorized:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 401 Unauthorized response
 *     produces:
 *       - application/json
 *     responses:
 *       '401':
 *          $ref: '#/components/responses/Unauthorized'
 *       '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: string
 *                  enum: [success]
 *                link:
 *                  $ref: '#/components/schemas/Link'
 *                errors:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Error'
 *                  maxItems: 0
 *                  example: []
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Thing'
 *                  maxItems: 0
 *                  example: []
 */
indexRouter.get('/unauthorized', (req, res) => {
  res.formatter.unauthorized();
});

/**
 * @swagger
 * /onlyauth:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 200 OK response if authenticated
 *     produces:
 *       - application/json
 *     responses:
 *       '401':
 *          $ref: '#/components/responses/Unauthorized'
 */
indexRouter.get('/onlyauth', requireAuth, (req, res) => {
  res.formatter.ok();
});

/**
 * @swagger
 * /onlyallowed:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 200 OK response if authenticated
 *     produces:
 *       - application/json
 *     responses:
 *       '401':
 *          $ref: '#/components/responses/Unauthorized'
 *       '403':
 *          $ref: '#/components/responses/Forbidden'
 */
indexRouter.get('/onlyallowed', requireAuth, requirePermission, (req, res) => {
  res.formatter.ok();
});

/**
 * @swagger
 * /forbidden:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 403 Forbidden response
 *     produces:
 *       - application/json
 *     responses:
 *      '403':
 *          $ref: '#/components/responses/Forbidden'
 */
indexRouter.get('/forbidden', (req, res) => {
  res.formatter.forbidden();
});

/**
 * @swagger
 * /not-found:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 404 Not Found response
 *     produces:
 *       - application/json
 *     responses:
 *       '404':
 *          $ref: '#/components/responses/NotFound'
 */
indexRouter.get('/not-found', (req, res) => {
  res.formatter.notFound();
});

/**
 * @swagger
 * /error:
 *   get:
 *     tags:
 *       - Index
 *     description: Returns an empty 500 Server Error response
 *     produces:
 *       - application/json
 *     responses:
 *      '500':
 *        $ref: '#/components/responses/ServerError'
 */
indexRouter.get('/error', (req, res) => {
  res.formatter.serverError();
});

export default indexRouter;
